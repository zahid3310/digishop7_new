@extends('layouts.app')

@section('title', 'Print Pos')


<style type="text/css">
    @media print {
        /*a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }*/

        .card {
            width: 145mm;
            padding-right: 0px;
            padding-left: 15px;
            margin-top: 20px;
            color: black;
        }

        /*.card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }*/
    }
    
    .table td, .table th {
        padding: 0px !important;
    }
    
    .pb-2, .py-2 {
        padding-bottom: 0px !important;
    }
    
    .pt-2, .py-2 {
        padding-top: 0px !important;
    }
    .mt-3, .my-3 {
        margin-top: 0px !important;
    }
</style>

@section('content')
    <div class="main-content marginTopPrint">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row d-print-none">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Print Pos</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <input type="hidden" id="invoice_type" value="{{ $invoice['type'] }}">

                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">

                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">Order/Memo</h2>

                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight:bold;">{{ isset($user_info['organization_name']) ? $user_info['organization_name'] : $user_info['name'] }}</h2>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ isset($user_info['contact_number']) ? $user_info['contact_number'] : $user_info['phone'] }}</p>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12"></div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-6 col-6">
                                        <address style="margin-bottom: 0px;padding-bottom: 0px;margin-top: 0px;padding-top: 0px">
                                            <strong>Billed To:</strong><br>
                                            {{ $invoice['customer_name'] }}
                                            @if($invoice['address'] != null)
                                               <br> <?php echo $invoice['address']; ?> <br>
                                            @endif
                                            @if($invoice['address'] == null)
                                                <br>
                                            @endif
                                            {{ $invoice['phone'] }}
                                        </address>
                                    </div>

                                    <div style="font-size: 16px;text-align: right;" class="col-sm-6 col-6">
                                        <address style="margin-bottom: 0px;padding-bottom: 0px;margin-top: 0px;padding-top: 0px">
                                            <strong>Invoice Date:</strong>
                                            {{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}<br>

                                            <strong>Delivery Date:</strong>
                                            {{ date('d-m-Y', strtotime(date('Y-m-d'))) }}<br>

                                             <strong>SM</strong> : {{$customer_sr_name->sr_name}}
                                            <br>
                                            <strong>Area</strong> : {{$invoice->customer->area_id != null ? $invoice->customer->area->name : ''}}
                                            
                                        </address>
                                    </div>
                                </div>

                                <div style="margin-top: 0px !important" class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Sales summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Sales # {{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }}</h3>
                                        </div>
                                    </div>
                                </div>

                                <table class="table">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th style="text-align:left;width: 10%">No.</th>
                                                <th style="text-align:left;width: 50%">Item</th>
                                                <th style="text-align:right;width: 10%">Rate</th>
                                                <th style="text-align:right;width: 20%">Qty</th>
                                                <th style="display: none;">Dis</th>
                                                <th style="text-align:right;width: 10%">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">

                                            @if(!empty($entries) && ($entries->count() > 0))

                                            <?php $sub_total = 0; ?>

                                            @foreach($entries as $key => $value)
                                            <tr>
                                                <td class="text-left">{{ $key + 1 }}</td>
                                                <td style="text-align:left;">
                                                    @if($value['product_type'] == 1)
                                                        <?php echo $value['product_entry_name']; ?>
                                                    @else
                                                        <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                                    @endif
                                                </td>
                                                <td class="text-right">{{ $value['rate'] }}</td>
                                                <td class="text-right">{{ $value['quantity'] . ' ' .$value->convertedUnit->name }}</td>
                                                <td class="text-center" style="display: none;">
                                                    <?php $total_dis = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; ?>
                                                    @if($value['discount_type'] == 0)
                                                        <?php echo number_format($total_dis,2,'.',','); ?><br>
                                                        <?php echo '('.$value['discount_amount'].'%'.')'; ?>
                                                    @else
                                                        <?php echo number_format($value['discount_amount'],2,'.',','); ?>
                                                    @endif
                                                </td>
                                                <td class="text-right">{{ $value['total_amount'] }}</td>
                                            </tr>

                                            <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                            @endforeach
                                            @endif
                                            
                                            <tr>
                                                <td style="font-size: 16px" colspan="4" class="text-right">Invoice Total</td>
                                                <td style="font-size: 16px" class="text-right">{{ number_format($invoice['invoice_amount'],2,'.',',') }}</td>
                                            </tr>
                                            
                                            <tr>
                                                <td style="font-size: 16px" colspan="4" class="text-right">Previous Due</td>
                                                <td style="font-size: 16px" class="text-right">{{ number_format($previous_due,2,'.',',') }}</td>
                                            </tr>

                                            <tr style="display:none;">
                                                <td style="font-size: 16px" colspan="4" class="text-right">VAT ({{ $invoice['vat_type'] == 0 ? $invoice['total_vat'].'%' : 'BDT' }})</td>
                                                <td style="font-size: 16px">{{ $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : number_format($invoice['total_vat'],2,'.',',') }}</td>
                                            </tr>

                                            <!--<tr style="line-height: 0px">-->
                                            <!--    <td style="font-size: 16px" colspan="4" class="border-0 text-right">-->
                                            <!--        Discount ({{ $invoice['total_discount_type'] == 0 ? $invoice['total_discount_amount'].'%' : 'BDT' }})</td>-->
                                            <!--    <td style="font-size: 16px" class="border-0 text-right">{{ $invoice['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$invoice['total_vat'])/100))*$invoice['total_discount_amount'])/100) : number_format($invoice['total_discount_amount'],2,'.',',') }}</td>-->
                                            <!--</tr>-->

                                            <tr style="line-height: 16px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="4" class="border-0 text-right">Net Total</td>
                                                <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                    {{ number_format($invoice['invoice_amount'] + $previous_due,2,'.',',') }}
                                                </td>
                                            </tr>

                                            <tr style="line-height: 16px">
                                                <td style="font-size: 16px" colspan="4" class="border-0 text-right">Paid</td>
                                                <td style="font-size: 16px" class="border-0 text-right">
                                                    {{ number_format($invoice['cash_given'],2,'.',',') }}
                                                </td>
                                            </tr>
                                            
                                            <tr style="line-height: 16px;">
                                                <td style="font-size: 16px" colspan="4" class="border-0 text-right">Net Dues</td>
                                                <td style="font-size: 16px" class="border-0 text-right">
                                                    {{ number_format($invoice['invoice_amount'] + $previous_due - $invoice['cash_given'],2,'.',',') }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            
                                @if($invoice['invoice_note'] != null)
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 style="font-size: 16px"><strong>Note :</strong> {{ $invoice['invoice_note'] }}</h6>
                                    </div>
                                </div>
                                @endif
                                
                                <br>

                                <div class="row">
                                    <div class="col-6">
                                    <p style="text-decoration: overline dotted;">Owner Signature</p>
                                    </div>

                                    <div class="col-6">
                                    <p style="text-decoration: overline dotted;text-align: right;">Customer Signature</p>
                                    </div>

                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        javascript:window.print();
    });

    window.onafterprint = function(e){
        var site_url        = $('.site_url').val();
        var invoiceType     = $("#invoice_type").val();

        if (invoiceType == 0)
        {
            window.location.replace(site_url + '/invoices');
        }

        if (invoiceType == 1)
        {
            window.location.replace(site_url + '/invoices?type=1');
        }

        if (invoiceType == 4)
        {
            window.location.replace(site_url + '/invoices?type=4');
        }
    };
</script>
@endsection