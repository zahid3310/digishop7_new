@extends('layouts.app')

@section('title', 'Customer Due List')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Customer Due List</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Customer Due List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['organization_name'] }}</h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['address'] }}</p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['contact_number'] }}</p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">Customer Due List</h4>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="{{ route('due_list_report_customer_index') }}" enctype="multipart/form-data">
                                    <div class="form-group row">

                                        <div class="col-lg-4 col-md-4 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="0" selected>--All Customers--</option>
                                                    @if(!empty($customers) && ($customers->count() > 0))
                                                    @foreach($customers as $key => $value)
                                                        <option {{ isset($_GET['customer_id']) && ($_GET['customer_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                 

                                        <div class="col-lg-3 col-md-3 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select style="width: 100" class="form-control select2" name="sr_id">
                                                    <option value="0" selected>--All Sr--</option>
                                                    @foreach($srList as $list)
                                                    <option value="{{$list->id}}" {{ $list->id == $sr_id ? 'selected' : '' }}>{{$list->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>



                                        <div class="col-lg-3 col-md-3 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="area" style="width: 100" class="form-control select2" name="area_id">
                                                    <option value="0" selected>--All Area--</option>
                                                    @foreach($areaList as $list)
                                                    <option value="{{$list->id}}" {{ $list->id == $area_id ? 'selected' : '' }}>{{$list->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center">SL</th>
                                            <th style="text-align: center">Customer</th>
                                            <th style="text-align: center">Address</th>
                                            <th style="text-align: center">Phone</th>
                                            <th style="text-align: center">Advance</th>
                                            <!-- <th style="text-align: center">Dues</th> -->
                                            <th style="text-align: center">Balance</th>
                                            <th style="text-align: center" class="d-print-none">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                            $i                  = 1;
                                        ?>
                                        @foreach($data as $key => $value)
                                        <tr>
                                            <td style="text-align: center;">{{ $i }}</td>
                                            <td style="text-align: left;">{{ $value['customer_name'] }} </td>
                                            <td style="text-align: left;">{{ $value['address'] }}</td>
                                            <td style="text-align: center;">{{ $value['phone'] }}</td>
                                            <td style="text-align: right;">{{ number_format($value['customer_advance'],2,'.',',') }}</td>
                                            <!-- <td style="text-align: right;">{{ number_format($value['due_amount'],2,'.',',') }}</td> -->
                                            <td style="text-align: right;">{{ number_format($value['due_amount'],2,'.',',') }}</td>
                                            <td style="text-align: center;" class="d-print-none">
                                                <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a style="cursor: pointer" class="dropdown-item" data-toggle="modal" data-target="#myModal_{{$value['customer_id']}}" >Details</a>
                                                        </div>
                                                    </div>
                                            </td>
                                        </tr>
                                         
                                        <?php $i++; ?>

                                        <div id="myModal_{{$value['customer_id']}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title mt-0" id="myModalLabel">Select Date Range</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <form method="get" action="{{ route('due_list_customer_details', $value['customer_id']) }}" enctype="multipart/form-data" target="_blank">

                                                        <div style="padding-top: 0px !important" class="modal-body">
                                                            <div style="margin-bottom: 0px !important" class="form-group row">
                                                                <label for="example-text-input" class="col-md-12 col-form-label">From Date *</label>
                                                                <div class="col-md-12">
                                                                    <input style="cursor: pointer" id="from_date" name="from_date" type="date" class="form-control" value="<?= date("2020-01-01") ?>" required>
                                                                </div>
                                                            </div>

                                                            <div style="margin-bottom: 0px !important" class="form-group row">
                                                                <label for="example-text-input" class="col-md-12 col-form-label">To Date *</label>
                                                                <div class="col-md-12">
                                                                    <input style="cursor: pointer" id="to_date" name="to_date" type="date" class="form-control" value="<?= date("Y-m-d") ?>" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Search</button>
                                                            <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                        <tr>
                                            <th colspan="4" style="text-align: right;">Total</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_advance_amount,2,'.',',') }}</th>
                                            <!-- <th colspan="1" style="text-align: right;">{{ number_format($total_due_amount,2,'.',',') }}</th> -->
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_balance_amount,2,'.',',') }}</th>
                                            <th colspan="1" style="text-align: right;" class="d-print-none"></th>
                                        </tr>

                                    </tbody>
                                    
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

<!-- <script type="text/javascript">
    $(document).on("change", "#type" , function() {

            var site_url        = $('.site_url').val();
            var type      = $("#type").val();

            $('#sr').empty();
            $('#area').empty();
            $.get(site_url + '/reports/other-info/list/' + type, function(data){

                if (data.sr_name.name != null) {
                    var sr_id = data.sr_name.id; 
                    var sr_name = data.sr_name.name; 
                }else
                {
                    var sr_id = '0'; 
                    var sr_name = 'Not Found';
                }

                if (data.area_name.area_name != null) {
                    var area_id = data.area_name.area_id; 
                    var area_name = data.area_name.area_name; 
                }else
                {
                    var area_id = '0'; 
                    var area_name = 'Not Found';
                }

                $('#sr').append(`<option value="`+sr_id+`" selected>`+sr_name+`</option>`)
                $('#area').append(`<option value="`+area_id+`" selected>`+area_name+`</option>`)
            });
        });
</script> -->
@endsection