<!DOCTYPE html>
<html>

<head>
    <title>Supplier Ledger Details</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>  
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: center;line-height: .8">
                        <p style="font-size: 40px"><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p style="font-size: 20px">{{ $user_info['address'] }}</p>
                        <p style="font-size: 20px">{{ $user_info['contact_number'] }}</p>
                        <p style="font-size: 20px">{{ $user_info['contact_email'] }}</p>
                        <p style="font-size: 14px;text-align: right">{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Ledger Details</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                    <th style="text-align: center">Supplier Name</th>
                                    <th style="text-align: center">Supplier Phone</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                    
                                    <td style="text-align: center">
                                        @if($customer_name != null)
                                            {{ $customer_name['name'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                    
                                    <td style="text-align: center">
                                        @if($customer_name != null)
                                            {{ $customer_name['phone'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>sl</th>
                                    <th>bill#</th>
                                    <th>date</th>
                                    <th>supplier</th>
                                    <th style="text-align: right">Bill Amount</th>
                                    <th style="text-align: right">Discount</th>
                                    <th style="text-align: right">Payable</th>
                                    <th style="text-align: right">Paid</th>
                                    <th style="text-align: right">Due</th>
                                    <th style="text-align: right">Return Amount</th>
                                    <th style="text-align: right">Receivable</th>
                                </tr>
                            </thead>

                            <tbody>

                                @if(!empty($data))

                                <?php $serial = 1; ?>

                                @foreach($data as $key => $value)
                                    <tr>
                                        <td>{{ $serial }}</td>
                                        <td>
                                            @if($value['type'] == 1)
                                            <a href="{{ route('bills_show', $value['bill_id']) }}" target="_blank">
                                                {{ $value['bill_number'] }} 
                                            </a>
                                            @else
                                                {{ $value['bill_number'] }} 
                                            @endif
                                            <br> {{ $value['bill_note'] }}
                                        </td>
                                        <td>{{ $value['bill_date'] }}</td>
                                        <td>{{ $value['customer_name'] }}</td>
                                        <td style="text-align: right">{{ number_format($value['bill_amount'],2,'.',',') }}</td>
                                        <td style="text-align: right">
                                            {{ number_format($value['total_discount'] + $value['discount'],2,'.',',') }}

                                            @if($value['total_discount_note'] != null) 
                                                <br>
                                            @endif

                                            {{ $value['total_discount_note'] != null ? $value['total_discount_note'] : '' }}
                                        </td>
                                        <td style="text-align: right">{{ number_format($value['bill_amount'],2,'.',',') }}</td>
                                        <td style="text-align: right">{{ number_format($value['paid_amount'],2,'.',',') }}</td>
                                        <td style="text-align: right">{{ number_format($value['due_amount'],2,'.',',') }}</td>
                                        <td style="text-align: right">{{ number_format($value['return_amount'],2,'.',',') }}</td>
                                        <td style="text-align: right">{{ number_format($value['return_due'],2,'.',',') }}</td>
                                    </tr>

                                    <?php $serial++; ?>

                                @endforeach
                                @endif

                            </tbody>
                            <tr>
                                <th style="text-align: right" colspan="4">Total</th>
                                <th style="text-align: right">{{ number_format($total_bill_amount,2,'.',',') }}</th>
                                <th style="text-align: right">{{ number_format($total_discount_amount,2,'.',',') }}</th>
                                <th style="text-align: right">{{ number_format($total_payable,2,'.',',') }}</th>
                                <th style="text-align: right">{{ number_format($total_paid_amount,2,'.',',') }}</th>
                                <th style="text-align: right">{{ number_format($total_due_amount,2,'.',',') }}</th>
                                <th style="text-align: right">{{ number_format($total_purchase_return,2,'.',',') }}</th>
                                <th style="text-align: right">{{ number_format($total_return_due,2,'.',',') }}</th>
                            </tr>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>