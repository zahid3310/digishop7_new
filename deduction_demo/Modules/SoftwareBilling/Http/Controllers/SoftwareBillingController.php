<?php

namespace Modules\SoftwareBilling\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use App\Models\Users;
use Response;
use Validator;
use Auth;
use DB;

class SoftwareBillingController extends Controller
{
    public function index()
    {   
        $user     = Users::find(1);
        $response = Http::get(billingApiBaseUrl().'/get-transaction-history/'.$user['billing_id']); 
        $data     = json_decode($response);

        return view('softwarebilling::index', compact('data'));
    }

    public function smsPurchase()
    {   
        $user     = Users::find(1);
        $response = Http::get(billingApiBaseUrl().'/get-pending-payment-request/'.$user['billing_id']); 
        $data     = json_decode($response);

        return view('softwarebilling::sms_purchase', compact('data'));
    }

    public function smsRate($sms_type)
    {   
        $response = Http::get(billingApiBaseUrl().'/get-sms-rate/'.$sms_type); 
        $data     = json_decode($response);

        return Response::json($data);
    }

    public function smsPurchaseStore(Request $request)
    {
        $rules = array(
            'date'              => 'required | date',
            'sms_type'          => 'required | integer',
            'sms_rate'          => 'required | numeric',
            'quantity'          => 'required | numeric',
            'total_payable'     => 'required | numeric',
            'paid_amount'       => 'required | numeric',
            'payment_methode'   => 'required | integer',
            'transaction_id'    => 'required | string',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $billing_id     = Auth::user()->billing_id;
        $data           = $request->all();

        DB::beginTransaction();

        try{

            $response = Http::get(billingApiBaseUrl().'/sms-purchase-request?billing_id='.$billing_id.'&date='.$data['date'].'&sms_type='.$data['sms_type'].'&sms_rate='.$data['sms_rate'].'&quantity='.$data['quantity'].'&total_payable='.$data['total_payable'].'&paid_amount='.$data['paid_amount'].'&payment_methode='.$data['payment_methode'].'&transaction_id='.$data['transaction_id']); 
            $data     = json_decode($response);

            if ($data = 1)
            {
                return back()->with("success","Request Submitted Successfully !!");
            }

            if ($data = 0)
            {
                return back()->with("unsuccess","Try Again !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }
}
