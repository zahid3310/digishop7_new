<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class JournalEntries extends Model
{  
    protected $table = "journal_entries";

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoices','invoice_id');
    }

    public function bill()
    {
        return $this->belongsTo('App\Models\Bills','bill_id');
    }

    public function salesReturn()
    {
        return $this->belongsTo('App\Models\SalesReturn','sales_return_id');
    }

    public function purchaseReturn()
    {
        return $this->belongsTo('App\Models\PurchaseReturn','purchase_return_id');
    }

    public function income()
    {
        return $this->belongsTo('App\Models\Incomes','income_id');
    }

    public function expense()
    {
        return $this->belongsTo('App\Models\Expenses','expense_id');
    }
    
    public function account()
    {
        return $this->belongsTo('App\Models\Accounts','account_id');
    }

    public function customerSettlement()
    {
        return $this->belongsTo('App\Models\Settlements','settlement_id');
    }

    public function balanceTransfer()
    {
        return $this->belongsTo('App\Models\BalanceTransfers','balance_transfer_id');
    }

    public function payment()
    {
        return $this->belongsTo('App\Models\Payments','payment_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }
}
