@extends('layouts.app')

@section('title', 'Return Issues')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('issues_return_issue_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

                                <div class="row">
                                    <div style="background-color: #D2D2D2;height: 620px;padding-top: 10px;overflow-y: auto;overflow-x: hidden;" class="col-md-7">
                                        <div style="display: none" class="inner-repeater mb-4 issueDetails">
                                            <div data-repeater-list="inner-group" class="inner form-group row">
                                                <div id="return_product_list" class="inner col-lg-12 ml-md-auto">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 620px;padding-top: 10px" class="col-md-5">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label">Select DSM * </label>
                                                    <select style="width: 100%" id="sr_id" name="sr_id" class="form-control select2" onchange="issueList()">
                                                       <option value="">--Select DSM--</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Select Issue Number * </label>
                                                    <select style="width: 100%" id="issue_id" name="issue_id" class="form-control select2" onchange="findIssueList()">
                                                       <option value="">--Select Issue Number--</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="return_date">Return Date *</label>
                                                    <input id="return_date" name="return_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                                </div>
                                            </div>

                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label for="productname">Total</label>
                                                    <input id="total_amount" name="total_amount" type="text" class="form-control" placeholder="Total Amount" readonly>
                                                </div>
                                            </div>

                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                    <label for="return_note">Note</label>
                                                    <input id="return_note" name="return_note" type="text" placeholder="Enter Return Note" class="form-control">
                                                </div>
                                            </div>

                                            <div style="display: none;margin-top: 20px" class="button-items col-lg-12 issueDetails">
                                                <button name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('all_return_issues') }}">Close</a></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ url('public/common_javascripts/common.js') }}"></script>

    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();
            var type      = (new URL(location.href)).searchParams.get('return_type');

            $("#sr_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 6)
                    {
                        return result['text'];
                    }
                },
            });

            calculate(1);
        });

        function getCategoryList(x)
        {
            var site_url  = $('.site_url').val();
            var group_id  = $('#group_id_'+x).val();

            $("#product_category_id_"+x).select2({
                ajax: { 
                    url:  site_url + '/groups/category-list-ajax/' + group_id,
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });
        }
    </script>

    <script type="text/javascript">
        function getItemPrice(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var main_unit_id        = $("#main_unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param-main-unit/'+ product_entry_id + '/' + main_unit_id, function(data){
  
                var convertedUnitName = $('#converted_unit_id_'+x).find(":selected").text();
                var mainStock         = $("#main_stock_"+x).val();
                var convertedStock    = (parseFloat(mainStock)/parseFloat(data.conversion_rate)).toFixed(2);
                var mainStock         = $("#main_stock_"+x).val(parseFloat(convertedStock).toFixed(2));

                $("#quantity_"+x).val(convertedStock);
                $("#rate_"+x).val(parseFloat(data.sell_price_pr).toFixed(2));
            });
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#converted_unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#converted_unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#main_stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed();
                    var mainStock         = $("#main_stock_"+x).val(parseFloat(convertedStock).toFixed(2));

                    $("#quantity_"+x).val(convertedStock);
                    $("#rate_"+x).val(parseFloat(data.sell_price).toFixed(2));
                    $("#main_unit_show_"+x).html(convertedUnitName);
                }

            });
        }

        function getCategoryList(x)
        {
            var site_url  = $('.site_url').val();
            var group_id  = $('#group_id_'+x).val();

            $("#product_category_id_"+x).select2({
                ajax: { 
                    url:  site_url + '/groups/category-list-ajax/' + group_id,
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });
        }
    </script>

    <script type="text/javascript">
        function issueList()
        {
            var site_url        = $(".site_url").val();
            var sr_id           = $("#sr_id").val();

            $("#issue_id").select2({
                ajax: { 
                url:  site_url + '/srissues/issues-list-sr/' + sr_id,
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        }

        function findIssueList()
        {
            var site_url        = $(".site_url").val();
            var sr_id           = $("#sr_id").val();
            var issue_id        = $("#issue_id").val();

            $.get(site_url + '/srissues/return-issues-product-list/'+ sr_id + '/' + issue_id, function(data){

                if (data != '')
                {
                    $('.issueDetails').show();
                }
                else
                {
                    $('.issueDetails').hide();
                }

                var return_product_list = '';
                var serial  = 1;
                $.each(data, function(j, data_list_j)
                {
                    var header  = data_list_j.order_details;
                    $.each(data_list_j, function(i, data_list)
                    {
                        if($.isNumeric(i))
                        {
                            if (serial == serial)
                            {
                                var group_label         = '<label class="hidden-xs" for="productname">Group *</label>\n';
                                var brand_label         = '<label class="hidden-xs" for="productname">Brand *</label>\n';
                                var category_label      = '<label class="hidden-xs" for="productname">Category *</label>\n';
                                var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                                var unit_label          = '<label class="hidden-xs" for="productname">Unit</label>\n';
                                var stock_label         = '<label class="hidden-xs" for="productname">Stock</label>\n';
                                var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                                var rate_label          = '<label class="hidden-xs" for="productname">Rate</label>\n';
                                var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                                var return_label        = '<label class="hidden-xs" for="productname">Return</label>\n';
                            }
                            else
                            {
                                var group_label         = '';
                                var brand_label         = '';
                                var category_label      = '';
                                var product_label       = '';
                                var unit_label          = '';
                                var stock_label         = '';
                                var rate_label          = '';
                                var quantity_label      = '';
                                var rate_label          = '';
                                var amount_label        = '';
                                var return_label        = '';
                            }

                            var quantity        = parseFloat(data_list.quantity);
                            var return_quantity = '<input id="return_quantity_'+ serial +'" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="" oninput="calculate('+ serial +')" />\n' 
                            
                            var unit_list    = '';
                            $.each(data_list.unit_data, function(i, data_list_unit)
                            {
                                if (data_list_unit.unit_id == data_list.converter_unit_id)
                                {

                                    unit_list    += '<option value = "' + data_list_unit.unit_id + '" selected>' + data_list_unit.unit_name + '</option>';
                                }
                                else
                                {
                                    unit_list    += '<option value = "' + data_list_unit.unit_id + '">' + data_list_unit.unit_name + '</option>';

                                }  
                            });

                            if (quantity != 0)
                            {
                                return_product_list += ' ' + '<div class="row di_'+serial+'">' +
                                                            '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12">\n' +
                                                                '<h4>' + header + '</h4>' +
                                                            '</div>\n' +
                                                            '<span style="margin-top: 35px;padding-left: 10px;font-weight:bold;font-size:14px">'+serial +'.</span>'+
                                                            '<input id="order_id" type="hidden" class="inner form-control" value="'+ data_list.order_id +'" name="order_id[]" />\n' +
                                                                
                                                            '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-4 col-md-4 col-sm-6 col-12">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Group *</label>\n' +
                                                                group_label +
                                                                '<select style="width: 100%" class="inner form-control single_select2 groupId" id="group_id_'+serial+'" required>\n' +
                                                                    '<option value="'+data_list.group_id+'">' + data_list.group_name + '</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-4 col-md-4 col-sm-6 col-12">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Brand *</label>\n' +
                                                                brand_label +
                                                                '<select style="width: 100%" class="inner form-control single_select2" id="brand_id_'+serial+'" required>\n' +
                                                                    '<option value="'+data_list.brand_id+'">' + data_list.brand_name + '</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="margin-bottom: 5px;padding-right: 10px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Category *</label>\n' +
                                                                category_label +
                                                                '<select style="width: 125%" class="inner form-control single_select2" id="product_category_id_'+serial+'" onchange="getProductList('+serial+')"required>\n' +
                                                                    '<option value="'+data_list.category_id+'">' + data_list.category_name + '</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +
                                                            
                                                            '<div style="margin-bottom: 5px;padding-left: 10px" class="col-lg-6 col-md-6 col-sm-6 col-12">\n' +
                                                                '<label style="display: none;width: 100%" class="show-xs" for="productname">Product *</label>\n' +
                                                                product_label +
                                                                '<input type="text" class="inner form-control" value="'+ data_list.product_name +'" readonly />\n' +
                                                                '<input id="product_entries_'+serial+'" name="product_entries[]" type="hidden" value="'+ data_list.product_entry_id +'" readonly />\n' +
                                                            '</div>\n' +

                                                            '<input id="main_unit_id_'+ serial +'" type="hidden" class="inner form-control" value="'+ data_list.unit_id +'" name="main_unit_id[]" />\n' +
                                                            '<input id="main_stock_'+ serial +'" type="hidden" class="inner form-control" value="'+ quantity +'" name="main_stock[]" />\n' +

                                                            '<div style="padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                                unit_label +
                                                                '<select style="width: 100%;cursor: pointer" name="converted_unit_id[]" class="inner form-control CUID" id="converted_unit_id_'+serial+'" required onchange="getConversionParam('+serial+')">\n' +
                                                                unit_list +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                                quantity_label  +
                                                                '<input id="quantity_'+ serial +'" type="text" class="inner form-control" value="'+ quantity +'" readonly />\n' +
                                                                '<span id="main_unit_show_'+serial+'" style="color: black">' + data_list.converter_unit + '</span>' +
                                                            '</div>\n' +

                                                            '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Return Qty</label>\n' +
                                                                return_label +
                                                                return_quantity +
                                                            '</div>\n' +

                                                            '<div style="margin-bottom: 5px;padding-left: 10px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Rate</label>\n' +
                                                                rate_label  +
                                                                '<input type="text" name="rate[]" class="inner form-control" id="rate_'+ serial +'" placeholder="Rate" value="'+ data_list.rate +'" required  readonly/>\n' +
                                                            '</div>\n' +

                                                            '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                                '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                                amount_label +
                                                                '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+ serial +'" placeholder="Total" value="0" readonly/>\n' + 
                                                            '</div>\n' +

                                                        '</div>\n' +

                                                        '<hr style="margin-top: 5px !important;margin-bottom: 5px !important;background: #000!important;">';

                                                    serial++;
                                                    
                                header = '';
                            }  
                        }
                        else
                        {

                        }                    
                    });
                });

                $("#return_product_list").empty();
                $("#return_product_list").append(return_product_list);

                calculate(1);
            });
        }
    </script>

    <script type="text/javascript">
        function calculate(x)
        {
            var quantity                = $("#quantity_"+x).val();
        	var return_quantity         = $("#return_quantity_"+x).val();
            var rate                    = $("#rate_"+x).val();

            if (quantity == '')
            {
                var quantityCal         = 0;
            }
            else
            {
                var quantityCal         = $("#quantity_"+x).val();
            }

            if (return_quantity == '')
            {
                var returnQuantityCal   = 0;
            }
            else
            {
                var returnQuantityCal   = $("#return_quantity_"+x).val();
            }

            if (rate == '')
            {
                var rateCal             = 0;
            }
            else
            {
                var rateCal             = $("#rate_"+x).val();
            }

            //Calculating Subtotal Amount

            var AmountIn  =  (parseFloat(rateCal)*parseFloat(returnQuantityCal));

            $("#amount_"+x).val(parseFloat(AmountIn).toFixed(2));

            var totalAmount       = 0;
            $('.amount').each(function()
            {
                totalAmount       += parseFloat($(this).val());
            });

            $("#total_amount").val(0);
            $("#total_amount").val(totalAmount.toFixed());
        }
    </script>
@endsection