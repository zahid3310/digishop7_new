<?php

namespace Modules\Branch\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Branches;
use DB;
use Response;

class BranchController extends Controller
{
    public function index()
    {
        $branches  = Branches::orderBy('id', 'ASC')->get();

        return view('branch::index', compact('branches'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'branch_name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $branch                     = new Branches;
            $branch->name               = $data['branch_name'];
            $branch->organization_name  = $data['organization_name'];
            $branch->address            = $data['address'];
            $branch->contact_number     = $data['contact_number'];
            $branch->email              = $data['email'];
            $branch->website            = $data['website'];
            $branch->printer_type       = $data['printer_type'];
            $branch->created_by         = $user_id;

            if($request->hasFile('logo'))
            {   
                $companyLogo            = $request->file('logo');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'company-profile-images/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $branch->logo           = $logoUrl;
            }
            
            if ($branch->save())
            {
                return redirect()->route('branch_index')->with("success","Branch Created Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function edit($id)
    {
        $branches       = Branches::orderBy('id', 'ASC')->get();
        $find_branch    = Branches::find($id);

        return view('branch::edit', compact('branches', 'find_branch'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'branch_name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $branch                     = Branches::find($id);
            $branch->name               = $data['branch_name'];
            $branch->organization_name  = $data['organization_name'];
            $branch->address            = $data['address'];
            $branch->contact_number     = $data['contact_number'];
            $branch->email              = $data['email'];
            $branch->website            = $data['website'];
            $branch->printer_type       = $data['printer_type'];
            $branch->updated_by         = $user_id;
            
            if($request->hasFile('logo'))
            {   
                if ($branch->logo != null)
                {
                    unlink('public/'.$branch->logo);
                }

                $companyLogo            = $request->file('logo');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'company-profile-images/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $branch->logo           = $logoUrl;
            }

            if ($branch->save())
            {
                return redirect()->route('branch_index')->with("success","Branch Updated Successfully !!");
            }

        }catch (\Exception $exception){
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }
}
