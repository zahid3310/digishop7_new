<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Models
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\SalesReturn;
use App\Models\PurchaseReturn;
use App\Models\PurchaseReturnEntries;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\Expenses;
use App\Models\ExpenseEntries;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\Branches;
use App\Models\Users;
use App\Models\DailyAttendance;
use Carbon\Carbon;
use Response;
use Artisan;
use Auth;
use DB;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth()->user()->status == 1)
        {
            return view('home');
        }
        else
        {
            return back();
        }
    }

    public function dashboardItems()
    {
        $branch_id                  = Auth::user()->branch_id;
        $data['total_employee']     = Customers::where('contact_type', 2)->where('branch_id', $branch_id)->where('status', 1)->count();
        $data['today_present']      = DailyAttendance::where('date', date('Y-m-d'))->where('status', 'Present')->count();
        $data['today_absent']       = DailyAttendance::where('date', date('Y-m-d'))->where('status', 'Absent')->count();;
        $data['today_latecome']     = 0;

        return Response::json($data);
    }

    public function check()
    {
        return view('check');
    }
}
