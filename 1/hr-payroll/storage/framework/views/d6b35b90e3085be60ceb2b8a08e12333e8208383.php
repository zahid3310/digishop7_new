

<?php $__env->startSection('title', 'All Purchases'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">All Purchases</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Purchases</a></li>
                                    <li class="breadcrumb-item active">All Purchases</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <?php if(Session::has('success')): ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <?php echo Session::get('success'); ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            <?php if(Session::has('unsuccess')): ?>
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                <?php echo Session::get('unsuccess'); ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            <?php if(Session::has('errors')): ?>
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>
                                
                            <div class="card-body table-responsive">
                                
                                <div class="row">
                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">From</label>
                                            <div class="col-md-8">
                                                <input style="cursor: pointer" id="search_from_date" type="date" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">To</label>
                                            <div class="col-md-8">
                                                <input style="cursor: pointer" id="search_to_date" type="date" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Supplier </label>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <select style="width: 75%" id="customer_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                        <option value="0">All</option>
                                                    </select>

                                                    <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="searchPayment()">
                                                        <i class="bx bx-search font-size-24"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th>Purchase#</th>
                                            <th>Supplier</th>
                                            <th>Total Payable</th>
                                            <th>Given</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="bill_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 1 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $.get(site_url + '/bills/bill/list/load', function(data){

                billList(data);
                
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var site_url                = $('.site_url').val();
            var search_from_date        = $('#search_from_date').val();
            var search_to_date          = $('#search_to_date').val();
            var search_customer         = $('#customer_id').val();

            if (search_from_date != '')
            {
                var from_date = $('#search_from_date').val();
            }
            else
            {
                var from_date = 0;
            }

            if (search_to_date != '')
            {
                var to_date = $('#search_to_date').val();
            }
            else
            {
                var to_date = 0;
            }

            if (search_customer != '')
            {
                var customer = $('#customer_id').val();
            }
            else
            {
                var customer = 0;
            }

            $.get(site_url + '/bills/bill/search/list/' + from_date + '/' + to_date + '/' + customer, function(data){

                billList(data);

            });
        }

        function billList(data)
        {
            var bill_list = '';
            var site_url  = $('.site_url').val();
            $.each(data, function(i, bill_data)
            {   
                var serial              = parseFloat(i) + 1;
                var edit_url            = site_url + '/bills/edit/' + bill_data.id;
                var print_url           = site_url + '/bills/show/' + bill_data.id;
                var purchase_return     = site_url + '/purchasereturn?supplier_id=' + bill_data.vendor_id + '&bill_id=' + bill_data.id;

                if (bill_data.return_id != null)
                {
                    var return_sign  = '&nbsp;' + '&nbsp;' + '<i class="bx bx-repost font-size-15">' + '</i>';
                }
                else
                {
                    var return_sign  = '';
                }

                if (bill_data.contact_type == 0)
                {
                    var type = 'Customer';
                }

                if (bill_data.contact_type == 1)
                {
                    var type = 'Supplier';
                }

                if (bill_data.contact_type == 2)
                {
                    var type = 'Employee';
                }

                bill_list += '<tr>' +
                                    '<td>' +
                                        serial +
                                    '</td>' +
                                    '<td>' +
                                       formatDate(bill_data.bill_date) +
                                    '</td>' +
                                    '<td>' +
                                       'BILL - ' + bill_data.bill_number.padStart(6, '0') + return_sign +
                                    '</td>' +
                                    '<td>' +
                                        bill_data.customer_name +
                                    '</td>' +
                                    '<td>' +
                                        parseFloat(bill_data.bill_amount).toFixed(2) +
                                    '</td>' +
                                    '<td>' +
                                        parseFloat(bill_data.cash_given).toFixed(2) +
                                    '</td>' +
                                    '<td>' +
                                        '<div class="dropdown">' +
                                            '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                            '</a>' +
                                            '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                '<?php if(Auth::user()->role == 1): ?>' +
                                                '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                '<?php endif; ?>' +
                                                '<a class="dropdown-item" href="' + print_url +'" target="_blank">' + 'Show' + '</a>' +
                                                '<a class="dropdown-item" href="' + purchase_return +'" target="_blank">' + 'Purchase Return' + '</a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</td>' +
                                '</tr>';
            });

            $("#bill_list").empty();
            $("#bill_list").append(bill_list);
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/1/hr-payroll/Modules/Bills/Resources/views/all_bills.blade.php ENDPATH**/ ?>