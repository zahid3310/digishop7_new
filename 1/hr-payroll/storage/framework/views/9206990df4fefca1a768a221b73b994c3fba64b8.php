

<?php $__env->startSection('title', 'Leave Application'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Leave Application</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                                    <li class="breadcrumb-item active">Leave Application</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

            					<form id="FormSubmit" action="<?php echo e(route('leave_application_store')); ?>" method="post" files="true" enctype="multipart/form-data">
            					<?php echo e(csrf_field()); ?>


                                <div style="margin-bottom: 0px !important" class="form-group row">
                                    <div class="col-md-3"></div>

                                    <div class="col-md-6 form-group">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-3 col-form-label">Employee *</label>
                                            <div class="col-md-9">
                                                <select style="width: 75%" id="customer_id" name="customer_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9" required>
                                                   <option value="">--Select Employee--</option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="getDeData()">
                                                    <i class="bx bx-search font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3"></div>
                                </div>

                                <hr id="deMargin" style="margin-top: 0px !important;display: none">

                                <div id="deDiv" style="margin-bottom: 0px !important;display: none" class="form-group row">
                                    <div class="col-md-4 form-group">
                                        Basic Information
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th style="text-align: right;width: 50%;padding: 4px !important;" scope="row">Employee ID</th>
                                                    <td style="text-align: left;width: 50%;padding: 4px !important;" id="emp_id"></td>
                                                </tr>

                                                <tr>
                                                    <th style="text-align: right;width: 50%;padding: 4px !important;" scope="row">Name</th>
                                                    <td style="text-align: left;width: 50%;padding: 4px !important;" id="emp_name"></td>
                                                </tr>

                                                <tr>
                                                    <th style="text-align: right;width: 50%;padding: 4px !important;" scope="row">Contact Number</th>
                                                    <td style="text-align: left;width: 50%;padding: 4px !important;" id="emp_contact"></td>
                                                </tr>

                                                <tr>
                                                    <th style="text-align: right;width: 50%;padding: 4px !important;" scope="row">Joining Date</th>
                                                    <td style="text-align: left;width: 50%;padding: 4px !important;" id="emp_joining_date"></td>
                                                </tr>

                                                <tr>
                                                    <th style="text-align: right;width: 50%;padding: 4px !important;" scope="row">Designation</th>
                                                    <td style="text-align: left;width: 50%;padding: 4px !important;" id="emp_designation"></td>
                                                </tr>

                                                <tr>
                                                    <th style="text-align: right;width: 50%;padding: 4px !important;" scope="row">Department</th>
                                                    <td style="text-align: left;width: 50%;padding: 4px !important;" id="emp_department"></td>
                                                </tr>

                                                <tr>
                                                    <th style="text-align: right;width: 50%;padding: 4px !important;" scope="row">Section</th>
                                                    <td style="text-align: left;width: 50%;padding: 4px !important;" id="emp_section"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-8 form-group">
                                        Leave Information
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr id="otherTblHead"></tr>
                                            </thead>

                                            <tbody>
                                                <tr id="otherTblAuthorizedLeave"></tr>

                                                <tr id="otherTblAvailedLeave"></tr>
                                                
                                                <tr id="otherTblBalance">
                                                    <th style="padding: 4px !important;font-weight: bold" scope="row">Balance</th>
                                                    <td style="padding: 4px !important;" colspan="2"></td>
                                                    <td style="padding: 4px !important;"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div style="margin-bottom: 0px !important" class="form-group row">
                                    <div class="col-md-4 form-group">
                                        <label for="productname">Leave Type *</label>
                                        <select style="border-radius: 4px;cursor: pointer" class="form-control" id="leave_type" name="leave_type">
                                            <?php if($leave_categories->count() > 0): ?>
                                            <option style="padding: 10px" value="">--Select Leave Type--</option>
                                            <?php $__currentLoopData = $leave_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $leave_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option style="padding: 10px" value="<?php echo e($leave_category['id']); ?>"><?php echo e($leave_category['name']); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="productname">Start Date *</label>
                                        <input id="start_date" name="start_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="productname">End Date *</label>
                                        <input id="end_date" name="end_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <label for="productname">Reason For Leave</label>
                                        <textarea class="form-control" name="note" rows="4" cols="50"></textarea>
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div style="margin-bottom: 0px !important" class="form-group row">
                                    <div class="col-md-4 form-group">
                                        <label for="productname">Reliever's Name *</label>
                                        <select style="border-radius: 4px;cursor: pointer" class="form-control" id="reliever_id" name="reliever_id">
                                            <option style="padding: 10px" value="">--Select Reliever's Name--</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="productname">Reliever's Contact</label>
                                        <input id="reliever_contact" name="reliever_contact" type="text" class="form-control">
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="productname">Upload Attachment</label>
                                        <input id="attachment" name="attachment" type="file">
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                    	<button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                    	<button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('leave_category_index')); ?>">Close</a></button>
                                    </div>
                                </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    $( document ).ready(function() {
        
        var site_url  = $('.site_url').val();

        $("#customer_id").select2({
            ajax: { 
            url:  site_url + '/invoices/customer/list/invoices',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if ((result['contact_type'] == 2) && result['id'] != 0)
                {   
                    if (result['designation'] != null)
                    {
                        var designation =  ' || ' + result['designation'];
                    }
                    else
                    {
                        var designation = '';
                    }

                    return result['text'] + designation;
                }
            },
        });

        $("#reliever_id").select2({
            ajax: { 
            url:  site_url + '/invoices/customer/list/invoices',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if ((result['contact_type'] == 2) && result['id'] != 0)
                {   
                    if (result['designation'] != null)
                    {
                        var designation =  ' || ' + result['designation'];
                    }
                    else
                    {
                        var designation = '';
                    }

                    return result['text'] + designation;
                }
            },
        });
    });

    function getDeData()
    {
        var site_url    = $('.site_url').val();
        var employee_id = $('#customer_id').val();

        $("#deMargin").show();
        $("#deDiv").show();

        $.get(site_url + '/attendance/leave-application/get-de-data/'+ employee_id, function(data){
            console.log(data);
            $("#emp_id").html(data.employee_info.employee_id);
            $("#emp_name").html(data.employee_info.name);
            $("#emp_contact").html(data.employee_info.phone);
            $("#emp_joining_date").html(data.employee_info.joining_date);
            $("#emp_designation").html(data.employee_info.designation);
            $("#emp_department").html(data.employee_info.department_name);
            $("#emp_section").html(data.employee_info.section_name);

            var list    = '';
            var list1   = '';
            var list2   = '';
            var list3   = '';

            var total_list1 = 0;
            var total_list2 = 0;
            var total_list3 = 0;
            $.each(data.other_info.short_name, function(i, other_data)
            {
                list += '<th style="padding: 4px !important;" scope="col">'+ other_data +'</th>';
                list1 += '<th style="padding: 4px !important;" scope="col">'+ data.other_info.authorized_leave[i] +'</th>';
                list2 += '<th style="padding: 4px !important;" scope="col">'+ data.other_info.availed_leave[i] +'</th>';

                total_list1 += parseFloat(data.other_info.authorized_leave[i]);
                total_list2 += parseFloat(data.other_info.availed_leave[i]);
                total_list3 += parseFloat(data.other_info.authorized_leave[i]) - parseFloat(data.other_info.availed_leave[i]);

                var tot      = parseFloat(data.other_info.authorized_leave[i]) - parseFloat(data.other_info.availed_leave[i]);

                list3 += '<th style="padding: 4px !important;" scope="col">'+ tot +'</th>';
            });

            var list_result     = '<th style="padding: 4px !important;font-weight: bold" scope="col">Leave Type</th>' +
                                    list + 
                                  '<th style="padding: 4px !important;" scope="col">Total</th>';

            var list_result1    = '<th style="padding: 4px !important;font-weight: bold" scope="row">Authorized Leave</th>' +
                                    list1 + 
                                  '<th style="padding: 4px !important;" scope="col">'+ total_list1 +'</th>';

            var list_result2    = '<th style="padding: 4px !important;font-weight: bold" scope="row">Availed</th>' +
                                    list2 + 
                                  '<th style="padding: 4px !important;" scope="col">'+ total_list2 +'</th>';
            
            var list_result3    = '<th style="padding: 4px !important;font-weight: bold" scope="row">Balance</th>' +
                                    list3 + 
                                  '<th style="padding: 4px !important;" scope="col">'+ total_list3 +'</th>';                  

            $("#otherTblHead").empty();
            $("#otherTblHead").append(list_result);

            $("#otherTblAuthorizedLeave").empty();
            $("#otherTblAuthorizedLeave").append(list_result1);

            $("#otherTblAvailedLeave").empty();
            $("#otherTblAvailedLeave").append(list_result2);

            $("#otherTblBalance").empty();
            $("#otherTblBalance").append(list_result3);
        });
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/1/hr-payroll/Modules/Attendance/Resources/views/leave_application/create.blade.php ENDPATH**/ ?>