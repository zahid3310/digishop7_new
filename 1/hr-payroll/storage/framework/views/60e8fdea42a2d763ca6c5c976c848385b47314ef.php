<!DOCTYPE html>
<html>

<head>
    <title>Profit & Loss</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p><?php echo e($user_info['address']); ?></p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Profit & Loss</h6>
                    </div>

                    <div class="ibox-content">
                        <!-- Header Start -->
                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center"><?php echo e($from_date); ?> <strong>To</strong> <?php echo e($to_date); ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Header End -->

                        <!-- Sales Statement Start -->
                        <div class="row">
                            <div class="col-md-6">
                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead class="theight">
                                        <tr style="background:#ddd;">
                                            <th style="text-align: center;" colspan="4">DEBIT</th>
                                        </tr>

                                        <tr style="background:#ddd;">
                                            <th style="text-align: center;width: 2%">SL</th>
                                            <th style="text-align: center;width: 10%">ACCOUNT HEAD</th>
                                            <th style="text-align: center;width: 10%">AMOUNT</th>
                                            <th style="text-align: center;width: 10%">BALANCE</th>
                                        </tr>
                                    </thead>

                                    <tbody class="theight">
                                        <?php
                                            $debit_balance    = 0;
                                        ?>
                                        <!-- <tr>
                                            <th colspan="3" style="text-align: right;">Opening Balance</th>
                                            <th style="text-align: right;"><?php echo e(number_format($opening_result,0,'.',',')); ?></th>

                                            <th colspan="3" style="text-align: right;">Opening Balance</th>
                                            <th style="text-align: right;"><?php echo e(number_format($opening_result,0,'.',',')); ?></th>
                                        </tr> -->

                                        <?php
                                            $debit_balance   = $debit_balance + $result['sales'] + $result['purchase_return'] + $result['stock_sales_value'];
                                        ?>

                                        <tr>
                                            <td style="text-align: center;">1</td>
                                            <td style="text-align: left;">Sales</td>
                                            <td style="text-align: right;"><?php echo e(number_format($result['sales'],0,'.',',')); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($debit_balance,0,'.',',')); ?></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: center;">2</td>
                                            <td style="text-align: left;">Purchase Return</td>
                                            <td style="text-align: right;"><?php echo e(number_format($result['purchase_return'],0,'.',',')); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($debit_balance,0,'.',',')); ?></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: center;">3</td>
                                            <td style="text-align: left;">Stock Value</td>
                                            <td style="text-align: right;"><?php echo e(number_format($result['stock_sales_value'],0,'.',',')); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($debit_balance,0,'.',',')); ?></td>
                                        </tr>

                                        <?php $k = 4; ?>
                                        <?php $__currentLoopData = $result['income']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $income_key => $income_value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                            $debit_balance   = $debit_balance + $income_value->sum('amount');
                                        ?>
                                        <tr>
                                            <td style="text-align: center;"><?php echo e($k); ?></td>
                                            <td style="text-align: left;"><?php echo e($income_key); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($income_value->sum('amount'),0,'.',',')); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($debit_balance,0,'.',',')); ?></td>
                                        </tr>
                                        <?php $k++; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>

                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="3" style="text-align: right;">Total</th>
                                            <th style="text-align: right;"><?php echo e(number_format($debit_balance,0,'.',',')); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <div class="col-md-6">
                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead class="theight">
                                        <tr style="background:#ddd;">
                                            <th style="text-align: center;" colspan="4">CREDIT</th>
                                        </tr>

                                        <tr style="background:#ddd;">
                                            <th style="text-align: center;width: 2%">SL</th>
                                            <th style="text-align: center;width: 10%">ACCOUNT HEAD</th>
                                            <th style="text-align: center;width: 10%">AMOUNT</th>
                                            <th style="text-align: center;width: 10%">BALANCE</th>
                                        </tr>
                                    </thead>

                                    <tbody class="theight">
                                        <?php
                                            $credit_balance   = 0;
                                        ?>
                                        <!-- <tr>
                                            <th colspan="3" style="text-align: right;">Opening Balance</th>
                                            <th style="text-align: right;"><?php echo e(number_format($opening_result,0,'.',',')); ?></th>

                                            <th colspan="3" style="text-align: right;">Opening Balance</th>
                                            <th style="text-align: right;"><?php echo e(number_format($opening_result,0,'.',',')); ?></th>
                                        </tr> -->

                                        <?php
                                            $credit_balance  = $credit_balance + $result['purchase'] + $result['sales_return'];
                                        ?>
                                        <tr>
                                            <td style="text-align: center;">1</td>
                                            <td style="text-align: left;">Purchase</td>
                                            <td style="text-align: right;"><?php echo e(number_format($result['purchase'],0,'.',',')); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($credit_balance,0,'.',',')); ?></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: center;">2</td>
                                            <td style="text-align: left;">Sales Return</td>
                                            <td style="text-align: right;"><?php echo e(number_format($result['sales_return'],0,'.',',')); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($credit_balance,0,'.',',')); ?></td>
                                        </tr>

                                        <?php $k1 = 3; ?>
                                        <?php $__currentLoopData = $result['expense']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $expense_key => $expense_value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                            $credit_balance   = $credit_balance + $expense_value->sum('amount');
                                        ?>
                                        <tr>
                                            <td style="text-align: center;"><?php echo e($k1); ?></td>
                                            <td style="text-align: left;"><?php echo e($expense_key); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($expense_value->sum('amount'),0,'.',',')); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($credit_balance,0,'.',',')); ?></td>
                                        </tr>
                                        <?php $k1++; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>

                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="3" style="text-align: right;">Total</th>
                                            <th style="text-align: right;"><?php echo e(number_format($credit_balance,0,'.',',')); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead class="theight">
                                        <tr style="background:#ddd;">
                                            <th style="text-align: center;" colspan="4">
                                                <?php if($debit_balance - $credit_balance > 0): ?>
                                                    NET PROFIT : <?php echo number_format($debit_balance - $credit_balance,0,'.',','); ?>
                                                <?php endif; ?>

                                                <?php if($debit_balance - $credit_balance < 0): ?>
                                                    LOSS : <?php echo number_format(abs($debit_balance - $credit_balance),0,'.',','); ?>
                                                <?php endif; ?>
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <!-- Sales Statement End -->

                        <!-- Footer Start -->
                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/1/hr-payroll/Modules/Reports/Resources/views/general_ledger_print.blade.php ENDPATH**/ ?>