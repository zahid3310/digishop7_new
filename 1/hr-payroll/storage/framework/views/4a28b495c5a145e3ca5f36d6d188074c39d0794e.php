

<?php $__env->startSection('title', 'Edit Sales'); ?>

<style type="text/css">
    body[data-sidebar=dark].vertical-collpsed {
        min-height: 0px !important;
    }
    
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Order</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">Edit Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                            Payment Successfull !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                            Payment Not Added !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('invoices_update', $find_invoice['id'])); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                <?php echo e(csrf_field()); ?>


                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Customer *</label>
                                            <div class="col-md-8">
                                                <select style="width: 75%" id="customer_id" name="customer_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9" required>
                                                   <option value="<?php echo e($find_invoice['customer_id']); ?>" selected><?php echo e($find_invoice['contact_name']); ?></option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Name </label>
                                            <div class="col-md-8">
                                                <input id="customer_name_s" name="customer_name" type="text" class="form-control" value="<?php echo e($find_invoice['customer_name']); ?>" placeholder="Customer Name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Ref.</label>
                                            <div class="col-md-8">
                                                <select style="width: 75%" id="reference_id" name="reference_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                   <option value="<?php echo e(customersTableDetails($find_invoice['reference_id'])['id']); ?>" selected><?php echo e(customersTableDetails($find_invoice['reference_id'])['name'] != null ? customersTableDetails($find_invoice['reference_id'])['name'] : '--Select Reference--'); ?></option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal1">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Address </label>
                                            <div class="col-md-8">
                                                <input id="customer_address" name="customer_address" type="text" class="form-control" value="<?php echo e($find_invoice['customer_address']); ?>" placeholder="Address">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label id="bbShow" for="productname" class="col-md-4 col-form-label"><?php echo e($find_invoice['previous_due_type'] == 1 ? 'Rec.' : 'Adv.'); ?></label>
                                            <div class="col-md-8">
                                                <input id="balance" name="previous_due" type="text" class="form-control" value="<?php echo e($find_invoice['previous_due']); ?>" readonly>
                                                <input id="bbBalance" type="hidden" name="balance_type" class="form-control" value="<?php echo e($find_invoice['previous_due_type']); ?>">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Pho. </label>
                                            <div class="col-md-8">
                                                <input id="customer_phone" name="customer_phone" type="text" class="form-control" value="<?php echo e($find_invoice['customer_phone']); ?>" placeholder="Phone">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Date</label>
                                            <div class="col-md-8">
                                                <input id="selling_date" name="selling_date" type="text" value="<?php echo e(date('d-m-Y', strtotime($find_invoice['invoice_date']))); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <div class="col-md-12">
                                                <i id="add_field_button" style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button col-md-3"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 250px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        <?php $__currentLoopData = $find_invoice_entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div style="margin-bottom: 0px !important" class="row di_<?php echo e($key); ?>">
                                                <div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label class="hidden-xs" for="productname"></label>
                                                            <div class="form-check form-check-right">
                                                                <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="isCollected_<?php echo e($key); ?>" <?php if($value['is_collected'] != 0): ?> checked <?php endif; ?>>
                                                                <input type="hidden" id="isCollectedVal_<?php echo e($key); ?>" name="is_collected[]" value="<?php echo e($value['is_collected']); ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-10">
                                                            <label class="hidden-xs" for="productname">Product *</label>
                                                            <label style="display: none" class="show-xs" for="productname">Product *</label>
                                                            <select style="width: 100%" name="product_entries[]" class="inner form-control select2 productEntries" id="product_entries_<?php echo e($key); ?>" onchange="getItemPrice(<?php echo e($key); ?>)" required>
                                                                <option value="<?php echo e($value['item_id']); ?>">
                                                                <?php
                                                                    $vari       = ProductVariationName($value['item_id']);
                                                                    $variation  = $vari != null ? ' - ' . $vari : '';

                                                                    if ($value['product_type'] == 1)
                                                                    {
                                                                        if ($value['pcs_per_cartoon'] != 0)
                                                                        {
                                                                            $pcs_per_cartoon = $value['pcs_per_cartoon'];
                                                                        }
                                                                        else
                                                                        {
                                                                            $pcs_per_cartoon = 1;
                                                                        }

                                                                        $pcs        = $value['stock_in_hand']/(($value['height']*$value['width'])/144);
                                                                        $cartoon    = $pcs/$pcs_per_cartoon;
                                                                    }
                                                                ?>

                                                                <?php echo e($value['item_name'] . $variation . '( ' . str_pad($value['product_code'], 6, "0", STR_PAD_LEFT) . ' )'); ?>

                                                                </option>
                                                            </select>
                                                            <?php if($value['product_type'] == 1): ?>
                                                            <span id="stock_show_<?php echo e($key); ?>" style="color: black;font-size: 10px"><?php echo e('Stock : '.round($value['stock_in_hand'], 2) . ' SFT ' . ' | ' . round($cartoon, 2) . ' Cart ' . ' | ' . round($pcs, 2) . ' PCS '); ?></span>
                                                            <?php else: ?>
                                                            <?php
                                                                $conversion_rate_find   = App\Models\UnitConversions::where('main_unit_id', $value['main_unit_id'])
                                                                                        ->where('converted_unit_id', $value['conversion_unit_id'])
                                                                                        ->where('product_entry_id', $value['product_entry_id'])
                                                                                        ->first();
                                                                $branch_inv             = App\Models\BranchInventories::where('product_entry_id', $value['product_entry_id'])->where('branch_id', Auth::user()->branch_id)->first();

                                                                if ($value['main_unit_id'] == $value['conversion_unit_id'])
                                                                {
                                                                    if (Auth::user()->branch_id == 1)
                                                                    {
                                                                        $converted_quantity   = $value['stock_in_hand'];
                                                                    }
                                                                    else
                                                                    {
                                                                        $converted_quantity   = $branch_inv['stock_in_hand'];
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (Auth::user()->branch_id == 1)
                                                                    {
                                                                        $converted_quantity   = $conversion_rate_find != null ? $value['stock_in_hand']*$conversion_rate_find['conversion_rate'] : $value['stock_in_hand'];
                                                                    }
                                                                    else
                                                                    {
                                                                        $converted_quantity   = $conversion_rate_find != null ? $branch_inv['stock_in_hand']*$conversion_rate_find['conversion_rate'] : $branch_inv['stock_in_hand'];
                                                                    }
                                                                }
                                                            ?>
                                                            <span id="stock_show_<?php echo e($key); ?>" style="color: black;font-size: 10px"><?php echo e('Stock : '.round($converted_quantity, 2) . '( ' . $value->convertedUnit->name . ' )'); ?></span>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <input type="hidden" name="stock[]" class="inner form-control" id="stock_<?php echo e($key); ?>" placeholder="Stock" value="<?php echo e($value['stock_in_hand']); ?>" />
                                                <input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_<?php echo e($key); ?>" value="<?php echo e($value['main_unit_id']); ?>" />
                                                <input type="hidden" class="inner form-control" id="main_unit_name_<?php echo e($key); ?>" />

                                                <?php if($value['product_type'] != 1): ?>
                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">
                                                    <label class="hidden-xs" for="productname">Unit</label>
                                                    <label style="display: none" class="show-xs" for="productname">Unit</label>
                                                    <select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_<?php echo e($key); ?>" onchange="getConversionParam(<?php echo e($key); ?>)">
                                                    <option value="<?php echo e($value['conversion_unit_id']); ?>"><?php echo e($value->convertedUnit->name); ?></option>
                                                    </select>
                                                </div>
                                                <?php else: ?>
                                                <div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">
                                                    <label class="hidden-xs" for="productname">Unit</label>
                                                    <label style="display: none" class="show-xs" for="productname">Unit</label>
                                                    <select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_<?php echo e($key); ?>" onchange="getConversionParam(<?php echo e($key); ?>)">
                                                    <option value="">--Select Unit--</option>
                                                    </select>
                                                </div>
                                                <?php endif; ?>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">SFT/Qty *</label>
                                                    <label style="display: none" class="show-xs" for="productname">SFT/Qty *</label>
                                                    <input type="text" name="quantity[]" class="inner form-control quantityCheck" id="quantity_<?php echo e($key); ?>" value="<?php echo e(round($value['quantity'], 2)); ?>" placeholder="Quantity" oninput="getItemPriceBackCalculation(<?php echo e($key); ?>)" required/>
                                                </div>

                                                <?php if($value['product_type'] == 1): ?>
                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-2 col-sm-12 col-12 form-group">
                                                    <label class="hidden-xs" for="productname">Cart</label>
                                                    <label style="display: none" class="show-xs" for="productname">Cart</label>
                                                    <input type="text" name="cartoon[]" class="inner form-control" id="cartoon_<?php echo e($key); ?>" value="<?php echo e($value['cartoon']); ?>" oninput="getItemPrice(<?php echo e($key); ?>)" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-2 col-sm-12 col-12 form-group">
                                                    <label class="hidden-xs" for="productname">PCS</label>
                                                    <label style="display: none" class="show-xs" for="productname">PCS</label>
                                                    <input type="text" name="pcs[]" class="inner form-control" id="pcs_<?php echo e($key); ?>" value="<?php echo e($value['pcs']); ?>" onchange="getItemPrice(<?php echo e($key); ?>)" />
                                                </div>
                                                <?php else: ?>
                                                <div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">Free/SFT *</label>
                                                    <label style="display: none" class="show-xs" for="productname">Free/SFT *</label>
                                                    <input type="text" name="free_quantity[]" class="inner form-control" id="free_quantity_<?php echo e($key); ?>" value="<?php echo e(round($value['free_quantity'], 2)); ?>" placeholder="Quantity" oninput="getItemPriceBackCalculation(<?php echo e($key); ?>)" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-2 col-sm-12 col-12 form-group">
                                                    <label class="hidden-xs" for="productname">Cart</label>
                                                    <label style="display: none" class="show-xs" for="productname">Cart</label>
                                                    <input type="text" name="cartoon[]" class="inner form-control" id="cartoon_<?php echo e($key); ?>" value="<?php echo e($value['cartoon']); ?>" oninput="getItemPrice(<?php echo e($key); ?>)" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-2 col-sm-12 col-12 form-group">
                                                    <label class="hidden-xs" for="productname">PCS</label>
                                                    <label style="display: none" class="show-xs" for="productname">PCS</label>
                                                    <input type="text" name="pcs[]" class="inner form-control" id="pcs_<?php echo e($key); ?>" value="<?php echo e($value['pcs']); ?>" onchange="getItemPrice(<?php echo e($key); ?>)" />
                                                </div>
                                                <?php endif; ?>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">Rate *</label>
                                                    <label style="display: none" class="show-xs" for="productname">Rate *</label>
                                                    <input type="text" name="rate[]" class="inner form-control" id="rate_<?php echo e($key); ?>" value="<?php echo e(round($value['rate'], 2)); ?>" placeholder="Rate" oninput="calculateActualAmount(<?php echo e($key); ?>)" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                    
                                                    <div class="row">
                                                        <div  style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">
                                                            <label class="hidden-xs" style="padding-top: 19px" for="productname"></label>
                                                            <label class="show-xs" style="display: none;padding-top: 19px" for="productname"></label>
                                                            <select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_<?php echo e($key); ?>" oninput="calculateActualAmount(<?php echo e($key); ?>)">
                                                                <option value="1" <?php echo e($value['discount_type'] == 1 ? 'selected' : ''); ?>>BDT</option>
                                                                <option value="0" <?php echo e($value['discount_type'] == 0 ? 'selected' : ''); ?>>%</option>
                                                            </select>
                                                        </div>

                                                        <div  style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">
                                                            <label class="hidden-xs" style="padding-bottom: 0px" for="productname">Discount</label>
                                                            <label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>
                                                            <input type="text" name="discount[]" class="inner form-control" id="discount_<?php echo e($key); ?>" value="<?php echo e($value['discount_amount']); ?>" placeholder="Discount" oninput="calculateActualAmount(<?php echo e($key); ?>)"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">Total</label>
                                                    <label style="display: none" class="show-xs" for="productname">Total</label>
                                                    <input type="text" name="amount[]" class="inner form-control amount" id="amount_<?php echo e($key); ?>" value="<?php echo e(round($value['total_amount'], 2)); ?>" placeholder="Total"/>
                                                </div>
                                                
                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">Action</label>
                                                    <label style="display: none" class="show-xs" for="productname">Action *</label>
                                                    <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field" data-val="<?php echo e($key); ?>"></i>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 90px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Sub Total</label>
                                            <div class="col-md-7">
                                                <input type="text" id="subTotalBdt" class="form-control">
                                                <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">VAT</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control" id="vat_type_0" name="vat_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="0" <?php echo e($find_invoice['vat_type'] == 0 ? 'selected' : ''); ?>>%</option>
                                                    <option style="padding: 10px" value="1" <?php echo e($find_invoice['vat_type'] == 1 ? 'selected' : ''); ?>>BDT</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="vat_amount_0" type="text" class="form-control width-xs" name="vat_amount" value="<?php echo e($find_invoice['total_vat']); ?>" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 90px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Discount</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                               <select style="border-radius: 4px;cursor: pointer" class="form-control totalDiscountType" id="total_discount_type_0" name="total_discount_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="1" <?php echo e($find_invoice['total_discount_type'] == 1 ? 'selected' : ''); ?>>BDT</option>
                                                    <option style="padding: 10px" value="0" <?php echo e($find_invoice['total_discount_type'] == 0 ? 'selected' : ''); ?>>%</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="total_discount_0" type="text" class="form-control totalDiscount" name="total_discount_amount" value="<?php echo e($find_invoice['total_discount_amount']); ?>" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Dis. Note</label>
                                            <div class="col-md-7">
                                                <input id="total_discount_note" type="text" class="form-control width-xs" name="total_discount_note" value="<?php echo e($find_invoice['total_discount_note']); ?>" placeholder="Discount Note">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Coupon</label>
                                            <div class="col-md-7">
                                                <input id="coupon_code" type="number" class="form-control width-xs couponCode" name="coupon_code" onchange="couponMembership()" placeholder="Coupon/Membership" value="<?php echo e($find_invoice['discount_code']); ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 90px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Ship. Cost</label>
                                            <div class="col-md-7">
                                                <input type="text" id="shipping_cost" name="shipping_cost" class="form-control" value="<?php echo e($find_invoice['shipping_cost']); ?>" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">T/Payable</label>
                                            <div class="col-md-7">
                                                <input type="text" id="totalBdt" class="form-control">
                                                <input style="display: none" type="text" id="totalBdtShow" name="total_amount" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 90px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Given</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="cash_given" name="cash_given" placeholder="Cash Given" oninput="calculateChangeAmount()" value="<?php echo e($find_invoice['cash_given']); ?>">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Change</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="change_amount" name="change_amount" placeholder="Change Amount" value="<?php echo e($find_invoice['change_amount']); ?>" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Acc. Head</label>
                                            <div class="col-md-7">
                                                <select style="width: 100%;cursor: pointer;" id="account_id" name="account_id" class="form-control" required>
                                                    <option value="">--Select Account Head--</option>
                                                    <?php if($accounts->count() > 0): ?>
                                                    <?php $__currentLoopData = $accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($account->id); ?>" <?php echo e($find_invoice['account_id'] == $account->id ? 'selected' : ''); ?>><?php echo e($account->account_name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 30px;padding-top: 13px;text-align: right" class="col-md-12">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <div class="col-md-12">
                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Send SMS
                                                    </label>
                                                </div>

                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Non Masking
                                                    </label>
                                                </div>

                                                <div style="padding-left: 10px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Masking
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 140px;padding-top: 13px;padding-right: 0px;padding-left: 0px" class="col-md-12">
                                        <div style="background-color: #D2D2D2;height: 120px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap_current_balance getMultipleRowCurrentBalance">
                                            <?php if($current_balance->count() > 0): ?>
                                            <?php $__currentLoopData = $current_balance; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key_current_balance => $current_balance_value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="row di_current_balance_<?php echo e($key_current_balance); ?>">
                                                <div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <?php if($key_current_balance == 0): ?>
                                                        <label class="hidden-xs" for="productname">Date *</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Date *</label>
                                                    <input id="current_balance_payment_date" name="current_balance_payment_date[]" type="text" value="<?php echo e(date('d-m-Y', strtotime($current_balance_value['date']))); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                                </div>

                                                <div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <?php if($key_current_balance == 0): ?>
                                                        <label class="hidden-xs" for="productname"> Paid *</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname"> Paid *</label>
                                                    <input type="text" class="form-control" value="<?php echo e($current_balance_value['amount']); ?>" name="current_balance_amount_paid[]" />
                                                </div>

                                                <div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                    <?php if($key_current_balance == 0): ?>
                                                        <label class="hidden-xs" for="productname">Paid Through *</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Paid Through *</label>
                                                    <select id="current_balance_paid_through_<?php echo e($key_current_balance); ?>" style="cursor: pointer" name="current_balance_paid_through[]" class="form-control single_select2">
                                                        <?php if(!empty($paid_accounts) && ($paid_accounts->count() > 0)): ?>
                                                        <?php $__currentLoopData = $paid_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $paid_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option <?php echo e($paid_account['id'] == $current_balance_value['account_id'] ? 'selected' : ''); ?> value="<?php echo e($paid_account['id']); ?>"><?php echo e($paid_account['account_name']); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <?php endif; ?>
                                                    </select>
                                                </div>

                                                <div style="" class="col-lg-2 col-md-3 col-sm-6 col-6">
                                                    <?php if($key_current_balance == 0): ?>
                                                        <label class="hidden-xs" for="productname">AC Info.</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Account Info.</label>
                                                    <input type="text" name="current_balance_account_information[]" class="form-control" id="current_balance_account_information_<?php echo e($key_current_balance); ?>" value="<?php echo e($current_balance_value['account_information']); ?>" placeholder="Account Information" />
                                                </div>

                                                <div style="" class="col-lg-2 col-md-3 col-sm-6 col-6">
                                                    <?php if($key_current_balance == 0): ?>
                                                        <label class="hidden-xs" for="productname">Note</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Note</label>
                                                    <input type="text" name="current_balance_note[]" class="form-control" id="current_balance_note_<?php echo e($key_current_balance); ?>" value="<?php echo e($current_balance_value['note']); ?>" placeholder="Note" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group">
                                                    <?php if($key_current_balance == 0): ?>
                                                        <label class="hidden-xs" for="productname">Action</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Action</label>
                                                    <?php if($key_current_balance == 0): ?>
                                                    <i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonCurrentBalance()"></i>
                                                    <?php else: ?>
                                                    <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field_current_balance" data-val="<?php echo e($key_current_balance); ?>"></i>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <input type="hidden" name="current_balance_id[]" value="<?php echo e($current_balance_value['id']); ?>">
                                            <input type="hidden" name="current_balance_account_id[]" value="<?php echo e($current_balance_value['account_id']); ?>">
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>

                                            <div style="display: none" class="row justify-content-end">
                                                <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                                    <input id="add_field_button_current_balance" type="button" class="btn btn-success btn-block inner add_field_button_current_balance" value="Add"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px !important;padding-bottom: 0px !important" class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Update & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('invoices_index')); ?>">Close</a></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control commonCustomerClass">
                        </div>
                    </div>

                    <input id="contact_type" name="contact_type" type="hidden" value="0" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Reference</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name1" name="customer_name" type="text" class="form-control commonReferenceClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number1" name="mobile_number" type="text" class="form-control commonReferenceClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address1" name="address" type="text" class="form-control commonReferenceClass">
                        </div>
                    </div>

                    <input id="contact_type1" name="contact_type" type="hidden" value="3" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(url('public/common_javascripts/common.js')); ?>"></script>
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();
            
            $("#reference_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 3)
                    {
                        return result['text'];
                    }
                },
            });

            var customerId      = $("#customer_id").val();

            $.get(site_url + '/invoices/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(toFixed(data.balance, 2));    
            });

            $('#add_field_button_current_balance').click();

            calculateActualAmount();
        });
        
        function toFixed(num, fixed) {
            var re = new RegExp('^-?\\d+(?:\.\\d{0,' + (fixed || -1) + '})?');
            return num.toString().match(re)[0];
        }
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = $("#contact_type").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '<?php echo e(csrf_token()); ?>' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                            $('.commonCustomerClass').val('');
                        }
                        
                        $("#customer_id").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });

        $('#submitBtn1').click(function() {
            
            var customer_name               = $("#customer_name1").val();
            var address                     = $("#address1").val();
            var mobile_number               = $("#mobile_number1").val();
            var contact_type                = $("#contact_type1").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '<?php echo e(csrf_token()); ?>' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton1').click();
                            $('.commonReferenceClass').val('');
                        }
                        
                        $("#reference_id").empty();
                        $('#reference_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });    
    </script>

    <script type="text/javascript">
        function getItemPrice(x)
        {
            var site_url    = $(".site_url").val();
            var entry_id    = $("#product_entries_"+x).val();
            var cartoon_val = $("#cartoon_"+x).val();
            var pcs_val     = $("#pcs_"+x).val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    if (data.product_entries.type == 1)
                    {
                        $("#u_id_"+x).hide();
                        $("#free_sft_id_"+x).show();
                        $("#cartoon_id_"+x).show();
                        $("#pcs_id_"+x).show();

                        var pcs_equivalent_sft  = (parseFloat(data.product_entries.height)*parseFloat(data.product_entries.width))/144;
                        var stock_in_pcs        = parseFloat(data.product_entries.stock_in_hand)/parseFloat(pcs_equivalent_sft);
                        var stock_in_cart       = parseFloat(stock_in_pcs)/parseFloat(data.product_entries.pcs_per_cartoon);

                        //
                            if (data.product_entries.stock_in_hand == '' || data.product_entries.stock_in_hand == null)
                            {
                                var stockInHand  = 0;
                            }
                            else
                            {
                                var stockInHand  = data.product_entries.stock_in_hand;
                            }

                        //
                            if (cartoon_val == '')
                            {
                                var cartoonVal  = 0;
                            }
                            else
                            {
                                var cartoonVal  = parseFloat(cartoon_val);
                            }

                        //
                            if (pcs_val == '')
                            {
                                var pcsVal  = 0;
                            }
                            else
                            {
                                var pcsVal  = parseFloat(pcs_val);
                            }

                        if (cartoonVal != 0)
                        {
                            var qtyVal1  = parseFloat(cartoonVal)*parseFloat(data.product_entries.pcs_per_cartoon)*parseFloat(pcs_equivalent_sft);
                        }
                        else
                        {
                            var qtyVal1  = 0;
                        }

                        if (pcsVal != 0)
                        {
                            var qtyVal2  = parseFloat(pcsVal)*parseFloat(pcs_equivalent_sft);

                            if (cartoonVal == '')
                            {
                                var pcsToCartoon = parseFloat(pcsVal)/parseFloat(data.product_entries.pcs_per_cartoon);

                                //x = 50; y = 15; res = x % y; x = (x - res) / y; [result = 3]
                                var cartAmount = (parseFloat(pcsVal) - (parseFloat(pcsVal)%parseFloat(data.product_entries.pcs_per_cartoon)))/parseFloat(data.product_entries.pcs_per_cartoon);
                                var pcsAmount  = parseFloat(pcsVal) - (parseFloat(cartAmount)*parseFloat(data.product_entries.pcs_per_cartoon));

                                $("#cartoon_"+x).val(toFixed(cartAmount, 2));
                                $("#pcs_"+x).val(toFixed(pcsAmount, 2));
                            }
                        }
                        else
                        {
                            var qtyVal2  = 0;
                        }

                        var qtyVal = parseFloat(qtyVal1) + parseFloat(qtyVal2);

                        if (stock_in_cart > 0)
                        {
                            var stock_in_cart  = stock_in_cart;
                        }
                        else
                        {
                           var stock_in_cart   = 0; 
                        }

                        if (stock_in_pcs > 0)
                        {
                            var stock_in_pcs  = stock_in_pcs;
                        }
                        else
                        {
                           var stock_in_pcs   = 0; 
                        }
                        
                        $("#stock_"+x).val(toFixed(stockInHand, 2));
                        $("#stock_show_"+x).html('Stock : ' + toFixed(stockInHand, 2) + ' SFT ' + ' | ' + toFixed(stock_in_cart, 2) + ' Cart ' + ' | ' + toFixed(stock_in_pcs, 2) + ' PCS ');

                        $("#rate_"+x).val(toFixed(data.product_entries.buy_price, 2));
                        $("#discount_"+x).val(0);
                        $("#quantity_"+x).val(toFixed(qtyVal, 2));
                        
                        calculateActualAmount(x);
                    }
                    else
                    {
                        $("#u_id_"+x).show();
                        $("#free_sft_id_"+x).hide();
                        $("#cartoon_id_"+x).hide();
                        $("#pcs_id_"+x).hide();

                        var list    = '';
                        $.each(data.unit_conversions, function(i, data_list)
                        {   
                            list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                        });

                        $("#unit_id_"+x).empty();
                        $("#unit_id_"+x).append(list);

                        //
                            if (data.product_entries.stock_in_hand == '' || data.product_entries.stock_in_hand == null)
                            {
                                var stockInHand  = 0;
                            }
                            else
                            {
                                var stockInHand  = data.product_entries.stock_in_hand;
                            }

                        //
                            if (data.product_entries.unit_id != null)
                            {
                                var unit = '( ' + data.product_entries.unit_name + ' )';
                            }
                            else
                            {
                                var unit = '';
                            }

                        $("#cartoon_"+x).val(0);
                        $("#pcs_"+x).val(0);
          
                        $("#rate_"+x).val(toFixed(data.product_entries.buy_price, 2));
                        $("#quantity_"+x).val(1);
                        $("#main_unit_id_"+x).val(data.product_entries.unit_id);
                        $("#main_unit_name_"+x).val(data.product_entries.unit_name);
                        $("#discount_"+x).val(0);
                        $("#stock_"+x).val(toFixed(stockInHand, 2));
                        $("#stock_show_"+x).html('Stock : ' + toFixed(stockInHand, 2) + unit);
          
                        calculateActualAmount(x);
                    }
                });
            }
        }

        function getItemPriceBackCalculation(x)
        {
            var site_url        = $(".site_url").val();
            var entry_id        = $("#product_entries_"+x).val();
            var sft_val         = $("#quantity_"+x).val();
            var free_sft_val    = 0;

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    if (data.product_entries.type == 1)
                    {
                        var pcs_equivalent_sft  = (parseFloat(data.product_entries.height)*parseFloat(data.product_entries.width))/144;
                        var stock_in_pcs        = parseFloat(data.product_entries.stock_in_hand)/parseFloat(pcs_equivalent_sft);
                        var stock_in_cart       = parseFloat(stock_in_pcs)/parseFloat(data.product_entries.pcs_per_cartoon);

                        //
                            if (sft_val == '')
                            {
                                var sftVal  = 0;
                            }
                            else
                            {
                                var sftVal  = parseFloat(sft_val);
                            }

                        //
                            if (free_sft_val == '')
                            {
                                var freeSftVal  = 0;
                            }
                            else
                            {
                                var freeSftVal  = parseFloat(free_sft_val);
                            }

                        var findPcs = (parseFloat(sft_val) + parseFloat(free_sft_val))/parseFloat(pcs_equivalent_sft);

                        //x = 50; y = 15; res = x % y; x = (x - res) / y; [result = 3]
                        var cartVal = (parseFloat(findPcs) - (parseFloat(findPcs)%parseFloat(data.product_entries.pcs_per_cartoon)))/parseFloat(data.product_entries.pcs_per_cartoon);

                        var pcsVal  = parseFloat(findPcs) - (parseFloat(cartVal)*parseFloat(data.product_entries.pcs_per_cartoon));

                        $("#cartoon_"+x).val(toFixed(cartVal, 2));
                        $("#pcs_"+x).val(toFixed(pcsVal, 2));
                    }

                    calculateActualAmount(x);
                });
            }
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock));

                    $("#stock_show_"+x).html('Stock : ' + toFixed(convertedStock, 2) + ' ( ' + convertedUnitName + ' )');
                    $("#rate_"+x).val(toFixed(data.purchase_price, 2));

                    calculateActualAmount(x);
                }

            });
        }

        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 500;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = <?php echo e($entries_count); ?>;
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {   
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var checkbox_label      = '<label style="margin-top: 15px" class="hidden-xs" for="productname"></label>\n';
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var cartoon_label       = '<label class="hidden-xs" for="productname">Cart</label>\n';
                    var unit_label          = '<label class="hidden-xs" for="productname">Unit</label>\n';
                    var pcs_label           = '<label class="hidden-xs" for="productname">PCS</label>\n';
                    var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">SFT/Qty *</label>\n';
                    var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">Discount</label>\n';
                    var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 19px" for="productname"></label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }
                else
                {
                    var checkbox_label = '';
                    var product_label  = '';
                    var cartoon_label  = '';
                    var unit_label     = '';
                    var pcs_label      = '';
                    var rate_label     = '';
                    var quantity_label = '';
                    var discount_label = '';
                    var type_label     = '';
                    var amount_label   = '';
                    var action_label   = '';

                    var add_btn        = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-3 col-12">\n' +
                                                        '<div class="row">\n' +
                                                            '<div class="col-md-2">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname"></label>\n' +
                                                                checkbox_label +
                                                                '<div class="form-check form-check-right">\n' +
                                                                    '<input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="isCollected_'+x+'">\n' +
                                                                    '<input type="hidden" id="isCollectedVal_'+x+'" name="is_collected[]">\n' +
                                                                '</div>\n' +
                                                            '</div>\n' +
                                                            '<div class="col-md-10">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                                product_label +
                                                                '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                                    '<option value="">' + '--Select Product--' + '</option>' +
                                                                '</select>\n' +
                                                                '<span id="stock_show_'+x+'" style="color: black;font-size: 10px">' + '</span>' +
                                                            '</div>' +
                                                        '</div>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +

                                                    '<div id="u_id_'+x+'" style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                        unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_'+x+'" onchange="getConversionParam('+x+')">\n' +
                                                        '<option value="">--Select Unit--</option>\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">SFT/Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control" id="quantity_'+x+'" placeholder="Quantity" oninput="getItemPriceBackCalculation('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div id="cartoon_id_'+x+'" style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Cart</label>\n' +
                                                        cartoon_label +
                                                        '<input type="text" name="cartoon[]" class="inner form-control" id="cartoon_'+x+'" placeholder="Cartoon" oninput="getItemPrice('+x+')" />\n' +
                                                    '</div>\n' +

                                                    '<div id="pcs_id_'+x+'" style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">PCS</label>\n' +
                                                        pcs_label +
                                                        '<input type="text" name="pcs[]" class="inner form-control" id="pcs_'+x+'" placeholder="PCS" onchange="getItemPrice('+x+')" />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 19px" for="productname"></label>\n' +
                                                                type_label +
                                                                '<select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_'+x+'" oninput="calculateActualAmount('+x+')">\n' +
                                                                    '<option value="1">BDT</option>' +
                                                                    '<option value="0" selected>%</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" name="discount[]" class="inner form-control" id="discount_'+x+'" placeholder="Discount" oninput="calculateActualAmount('+x+')"/>\n' +
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                        amount_label +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

                var site_url = $(".site_url").val();
                $(".productEntries").select2({
                    ajax: { 
                    url:  site_url + '/bills/product-list-load-bill',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                    },
                });

                var productEId      = $("#productEntryIdAddProduct").val();
                var productEName    = $("#productEntryNameAddProduct").val();
                var pRate           = $("#productEntryRateAddProduct").val();
                var pQty            = $("#productEntryQuantityAddProduct").val();
                
                if ((productEId != 0) && (productEName != 0))
                {
                    $('#product_entries_'+x).append('<option value="'+productEId+'" selected>'+ productEName +'</option>');
                    $('#quantity_'+x).val(parseFloat(pQty));
                    $('#product_entries_'+x).change();

                    var productEId      = $("#productEntryIdAddProduct").val(0);
                    var productEName    = $("#productEntryNameAddProduct").val(0);
                    var pRate           = $("#productEntryRateAddProduct").val(0);
                    var pQty            = $("#productEntryQuantityAddProduct").val(0);
                    var catId           = $("#product_category_id").val();

                    if (catId == 4)
                    {
                        $("#isCollected_"+x).prop('checked', true);
                    }
                }   
            }                                    
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            var rate               = $("#rate_"+x).val();
            var quantity           = $("#quantity_"+x).val();
            var stock              = $("#stock_"+x).val();
            var discount           = $("#discount_"+x).val();
            var discountType       = $("#discount_type_"+x).val();
            var vatType            = $("#vat_type_0").val();
            var vatAmount          = $("#vat_amount_0").val();
            var taxType            = $("#tax_type_0").val();
            var taxAmount          = $("#tax_amount_0").val();
            var totalDiscount      = $("#total_discount_0").val();
            var totalDiscountType  = $("#total_discount_type_0").val();
            var shippingCost       = $("#shipping_cost").val();

            if (shippingCost == '')
            {
                var shippingCostCal   = 0;
            }
            else
            {
                var shippingCostCal   = $("#shipping_cost").val();
            }

            if (rate == '')
            {
                var rateCal           = 0;
            }
            else
            {
                var rateCal           = $("#rate_"+x).val();
            }

            if (quantity == '')
            {
                var quantityCal       = 1;
            }
            else
            {
                var quantityCal       = $("#quantity_"+x).val();
            }

            if (discount == '')
            {
                var discountCal       = 1;
            }
            else
            {
                var discountCal       = $("#discount_"+x).val();
            }

            if (discount == '')
            {
                var discountTypeCal   = 0;
            }
            else
            {
                if (discountType == 0)
                {
                    var discountTypeCal     = (parseFloat(discountCal)*parseFloat(rateCal)*parseFloat(quantityCal))/100;
                }
                else
                {
                    var discountTypeCal     = $("#discount_"+x).val();
                }
            }

            var AmountIn              =  (parseFloat(rateCal)*parseFloat(quantityCal)) - parseFloat(discountTypeCal);
     
            $("#amount_"+x).val(toFixed(AmountIn, 2));

            //Calculating Subtotal Amount
            var total       = 0;

            $('.amount').each(function()
            {
                total       += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(toFixed(total, 2));
            $("#subTotalBdtShow").val(toFixed(total, 2));

            if (vatAmount == '')
            {   
                $("#vat_amount_0").val(0);
                var vatCal         = 0;
            }
            else
            {
                var vatCal         = $("#vat_amount_0").val();
            }

            if (vatType == 0)
            {
                var vatTypeCal     = (parseFloat(vatCal)*(parseFloat(total)))/100;
            }
            else
            {
                var vatTypeCal     = $("#vat_amount_0").val();
            }

            if (taxAmount == '')
            {   
                $("#tax_amount_0").val(0);
                var taxCal         = 0;
            }
            else
            {
                var taxCal         = $("#tax_amount_0").val();
            }

            if (taxType == 0)
            {
                var taxTypeCal     = (parseFloat(taxCal)*(parseFloat(total)))/100;
            }
            else
            {
                var taxTypeCal     = $("#tax_amount_0").val();
            }

            if (totalDiscount > 0)
            {
                if (totalDiscountType == 0)
                {
                    var totalDiscountTypeCal     = (parseFloat(totalDiscount)*(parseFloat(total) + parseFloat(vatTypeCal)))/100;
                }
                else
                {
                    var totalDiscountTypeCal     = $("#total_discount_0").val();
                }
            }
            else
            {
                var totalDiscountTypeCal     = 0;
            }

            var totalShow = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountTypeCal) + parseFloat(shippingCostCal);

            $("#totalBdtShow").val(toFixed(totalShow, 2));
            $("#totalBdt").val(toFixed(totalShow, 2));

            //Checking Overselling Start
            // var check_quantity  = parseFloat(quantity);
            // var check_stock     = parseFloat(stock);

            // if (check_quantity > check_stock)
            // {   
            //     $("#quantity_"+x).val(check_stock);
            // }
            //Checking Overselling End

            calculateChangeAmount();
        }
    </script>

    <script type="text/javascript">
        var max_fields_current_balance = 50;                           //maximum input boxes allowed
        var wrapper_current_balance    = $(".input_fields_wrap_current_balance");      //Fields wrapper
        var add_button_current_balance = $(".add_field_button_current_balance");       //Add button ID
        var index_no_current_balance   = 1;

        //For apending another rows start
        var c = <?php echo e($current_balance_count > 0 ? $current_balance_count : -1); ?>;
        $(add_button_current_balance).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(c < max_fields_current_balance)
            {   
                c++;

                var serial = c + 1;

                if (serial == 1)
                {
                    var amount_paid_label   = '<label class="hidden-xs" for="productname">Amount Paid *</label>\n';
                    var date_label          = '<label class="hidden-xs" for="productname">Date *</label>\n';
                    var paid_through_label  = '<label class="hidden-xs" for="productname">Paid Through *</label>\n';
                    var account_info_label  = '<label class="hidden-xs" for="productname">Account Info.</label>\n';
                    var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button_current_balance">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonCurrentBalance()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var amount_paid_label   = '';
                    var date_label          = '';
                    var paid_through_label  = '';
                    var account_info_label  = '';
                    var note_label          = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field_current_balance" data-val="'+c+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRowCurrentBalance').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_current_balance_'+c+'">' +
                                                    '<input type="hidden" name="current_balance_id[]" value="-1">' +
                                                    '<input type="hidden" name="current_balance_account_id[]" value="-1">' +
                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Date *</label>\n' +
                                                        date_label +
                                                        '<input id="current_balance_payment_date" name="current_balance_payment_date[]" type="text" value="<?php echo e(date("d-m-Y")); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Amount Paid *</label>\n' +
                                                        amount_paid_label +
                                                        '<input type="text" name="current_balance_amount_paid[]" class="form-control currentBalancePaidAmount" id="current_balance_amount_paid_'+c+'" value="0" oninput="checkCurrentBalance('+c+')" required />\n' + 
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid Through *</label>\n' +
                                                        paid_through_label  +
                                                        '<select id="current_balance_paid_through_'+c+'" style="cursor: pointer" name="current_balance_paid_through[]" class="form-control single_select2">\n' +
                                                            '<?php if(!empty($paid_accounts) && ($paid_accounts->count() > 0)): ?>\n' +
                                                            '<?php $__currentLoopData = $paid_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $paid_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>\n' +
                                                                '<option value="<?php echo e($paid_account['id']); ?>"><?php echo e($paid_account['account_name']); ?></option>\n' +
                                                            '<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>\n' +
                                                            '<?php endif; ?>\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Account Info.</label>\n' +
                                                        account_info_label +
                                                        '<input type="text" name="current_balance_account_information[]" class="form-control" id="current_balance_account_information_'+c+'" placeholder="Account Information"/>\n' + 
                                                    '</div>\n' + 

                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                        note_label +
                                                        '<input type="text" name="current_balance_note[]" class="form-control" id="current_balance_note_'+c+'" placeholder="note"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +

                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
                }                                    
        });
        //For apending another rows end

        $(wrapper_current_balance).on("click",".remove_field_current_balance", function(e)
        {
            e.preventDefault();

            var c = $(this).attr("data-val");

            $('.di_current_balance_'+c).remove(); c--;

            calculateActualAmount(c);
        });
    </script>

    <script type="text/javascript">
        $(document).on("change", "#defaultCheck2" , function() {

            var checkbox_value    = $("#defaultCheck2")[0].checked;

            if (checkbox_value == true)
            {
                $("#vatShow").show();
                $("#taxShow").show();
            }
            else
            {
                $("#vatShow").hide();
                $("#taxShow").hide();
            }
        });

        function couponMembership()
        {
            var site_url      = $(".site_url").val();
            var coupon_code   = $("#coupon_code").val();
    
            $('.DiscountType').val(1);
            $('.DiscountAmount').val(0);

            $.get(site_url + '/invoices/search/coupon-code/'+ coupon_code, function(data_coupon){

                if (data_coupon == '')
                {
                    alert('Invalid Coupon Code or Membership Card !!');
                    $('#coupon_code').val('');
                }
                else
                {
                    var state = 0;

                    $('.productEntries').each(function()
                    {
                        var entry_id    = $(this).val();
                        var value_x     = $(this).prop("id");
                        
                        if (data_coupon[0].product_id != null)
                        {
                            for (var i = data_coupon.length - 1; i >= 0; i--)
                            {   
                                if (data_coupon[i].product_id == entry_id)
                                {
                                    var explode    = value_x.split('_');
                                    var di_id      = explode[2];

                                    $('#discount_type_'+di_id).val(data_coupon[i].discount_type);
                                    $('#discount_'+di_id).val(data_coupon[i].discount_amount);
                                    
                                    calculateActualAmount(di_id);

                                    state++;
                                }
                            }
                        }
                        else
                        {
                            var explode    = value_x.split('_');
                            var di_id      = explode[2];

                            $('#discount_type_'+di_id).val(data_coupon[0].discount_type);
                            $('#discount_'+di_id).val(data_coupon[0].discount_amount);
                            
                            calculateActualAmount(di_id);
                        }    
                    });
                }
            });
        }

        function calculateChangeAmount()
        {
            var tAmount             = $("#totalBdtShow").val();
            var cGiven              = $("#cash_given").val();

            if (tAmount != '')
            {
                var totalAmount     = $("#totalBdtShow").val();
            }
            else
            {
                var totalAmount     = 0;
            }

            if (cGiven != '')
            {
                var cashGiven          = $("#cash_given").val();
            }
            else
            {
                var cashGiven          = 0;
            }

            var changeAmount    = parseFloat(cashGiven) - parseFloat(totalAmount);

            if (changeAmount > 0)
            {   
                $("#change_amount").val(0);
                $("#change_amount").val(parseFloat(changeAmount));
                $("#amount_paid_0").val(parseFloat(cashGiven) - parseFloat(changeAmount));
            }

            if (parseFloat(changeAmount) < 0)
            {   
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(cashGiven));
            }

            if (parseFloat(changeAmount) == 0)
            {   
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(cashGiven));
            }
        }

        function adjustAdvancePayment()
        {
            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            if ($('#adjustAdvancePayment').is(":checked"))
            {
                $("#adjustment").val(0);
                $("#customer_advance_amount").val(0);
                calculateChangeAmount();
            }
            else
            {
                $.get(site_url + '/invoices/adjust-advance-payment/' + customerId, function(data){

                    var payable  = $("#totalBdtShow").val();

                    if (parseFloat(payable) <= parseFloat(data))
                    {
                        $("#adjustment").val(toFixed(payable, 2));
                    }
                    else
                    {
                        $("#adjustment").val(toFixed(data, 2));
                    }

                    $("#customer_advance_amount").val(toFixed(data, 2));
                    calculateChangeAmount();
                });
            }
        }

        function checkBalance(id)
        {
            var total  = 0;
            $('.paidAmount').each(function()
            {
                total  += parseFloat($(this).val());
            });

            var payable_amount  = $("#totalBdt").val();
            var amount_paid     = $("#amount_paid_"+id).val();

            if (parseFloat(total) > parseFloat(payable_amount))
            {
                $("#amount_paid_"+id).val(0);
            }
        }

        function checkCurrentBalance(id)
        {
            var total  = 0;
            $('.currentBalancePaidAmount').each(function()
            {
                total  += parseFloat($(this).val());
            });

            var cash_given      = $("#cash_given").val();
            var amount_paid     = $("#current_balance_amount_paid_"+id).val();

            if (parseFloat(total) > parseFloat(cash_given))
            {
                $("#current_balance_amount_paid_"+id).val(0);
            }
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/1/hr-payroll/Modules/Invoices/Resources/views/edit.blade.php ENDPATH**/ ?>