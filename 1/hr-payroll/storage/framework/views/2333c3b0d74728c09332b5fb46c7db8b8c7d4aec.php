

<?php $__env->startSection('title', 'Add Trade License'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Add Trade License</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Registers</a></li>
                                    <li class="breadcrumb-item active">Add Trade License</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('store_trade_license')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                <?php echo e(csrf_field()); ?>


                                <div class="row">
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">License No *</label>
                                        <input name="license_no" type="text" class="form-control" placeholder="লাইসেন্স নং">
                                    </div>                                    

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">License ID *</label>
                                        <input name="license_id" type="text" class="form-control" placeholder="লাইসেন্স আইডি">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Name of business organization *</label>
                                        <input name="business_name" type="text" class="form-control" placeholder="ব্যবসা প্রতিষ্ঠানের নাম">
                                    </div>                                    

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Owner name *</label>
                                        <input name="owner_name" type="text" class="form-control" placeholder="মালিকের নাম">
                                    </div>                                    

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Type of business *</label>
                                        <input name="type_of_business" type="text" class="form-control" placeholder="ব্যবসার ধরন">
                                    </div>                                    

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Ward no *</label>
                                        <input name="ward_no" type="text" class="form-control" placeholder="ওয়ার্ড নং">
                                    </div>


                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Road / Mouza / Area / Mahalla *</label>
                                        <input name="area_name" type="text" class="form-control" placeholder="রাস্তা/মৌজা/এলাকা/মহল্লা">
                                    </div>
                                    

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Address of business organization</label>
                                        <input name="business_address" type="text" class="form-control" placeholder="ব্যবসা প্রতিষ্ঠানের ঠিকানা">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Mobile No</label>
                                        <input name="mobile_number" type="number" class="form-control" placeholder="মোবাইল নং">
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('customers_index')); ?>">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Contacts</h4>

                                <div style="margin-right: 10px" class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-1">Search : </div>
                                    <div class="col-md-2">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Sl</th>
                                            <th>License No</th>
                                            <th>License ID</th>
                                            <th>Name of business organization</th>
                                            <th>Owner name</th>
                                            <th>Type of business</th>
                                            <th>Ward no</th>
                                            <th>Road / Mouza / Area / Mahalla</th>
                                            <th>Address of business organization</th>
                                            <th>Mobile No</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $__currentLoopData = $lists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($loop->index+1); ?></td>
                                            <td><?php echo e($list->license_no); ?></td>
                                            <td><?php echo e($list->license_id); ?></td>
                                            <td><?php echo e($list->business_name); ?></td>
                                            <td><?php echo e($list->owner_name); ?></td>
                                            <td><?php echo e($list->type_of_business); ?></td>
                                            <td><?php echo e($list->ward_no); ?></td>
                                            <td><?php echo e($list->area_name); ?></td>
                                            <td><?php echo e($list->business_address); ?></td>
                                            <td><?php echo e($list->mobile_number); ?></td>
                                            <td>
  
                                                <div class="dropdown">
                                                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                        <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" style="">
                                                        <a class="dropdown-item" href="<?php echo e(route('edit_trade_license', $list['id'])); ?>">Edit</a>
                                                    </div>
                                                </div>
        
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/1/hr-payroll/Modules/Customers/Resources/views/add_trade_license.blade.php ENDPATH**/ ?>