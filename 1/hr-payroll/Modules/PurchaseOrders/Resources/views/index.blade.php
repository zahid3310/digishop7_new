@extends('layouts.app')

@section('title', 'Purchase Order')

<style type="text/css">
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Purchase</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Purchases</a></li>
                                    <li class="breadcrumb-item active">New Purchase</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div style="margin-bottom: -15px" class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('purchase_orders_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Supplier *</label>
                                            <div class="col-md-8">
                                               <select style="width: 75%" id="customer_id" name="vendor_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" required>
                                                   <option value="2">Walk-In Supplier</option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label id="bbShow" for="productname" class="col-md-4 col-form-label">Paya. </label>
                                            <div class="col-md-8">
                                                <input id="balance" name="previous_due" type="text" value="" class="form-control" value="0" readonly>
                                                <input id="bbBalance" type="hidden" name="balance_type" class="form-control" value="1">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Note </label>
                                            <div class="col-md-8">
                                                <input id="bill_note" name="bill_note" type="text" value="" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Date </label>
                                            <div class="col-md-8">
                                                <input id="selling_date" name="selling_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px;margin-left: 1px;" class="form-group row">
                                            <i id="add_field_button" style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button col-md-3"></i>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 420px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        <div class="row di_0">
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 65px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Sub Total</label>
                                            <div class="col-md-7">
                                                <input type="text" id="subTotalBdt" class="form-control">
                                                <input style="display: none"  type="text" id="subTotalBdtShow" name="total_amount" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 65px;padding-top: 13px" class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <div class="col-md-12">
                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Send SMS
                                                    </label>
                                                </div>

                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Non Masking
                                                    </label>
                                                </div>

                                                <div style="padding-left: 10px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Masking
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 65px;padding-top: 13px" class="col-md-5">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Save & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('purchase_orders_index') }}">Close</a></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>

                                    <div style="display: none;" class="row justify-content-end">
                                        <div class="col-lg-1 col-md-2 form-group">
                                            <input id="pos_add_button" type="button" class="btn btn-success btn-block inner add_field_button_pos" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Supplier</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control commonSupplierClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control commonSupplierClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control commonSupplierClass">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 1 && result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });

            $('#add_field_button').click();

            $("#product_entries_0").select2({
                ajax: { 
                url:  site_url + '/bills/product-list-load-bill',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            var type = $("#bbBalance").val();

            if (type == 1)
            {
                $("#bbShow").html('Paya.');
                $("#bbBalance").val(type);
            }
            else
            {
                $("#bbShow").html('Adv.');
                $("#bbBalance").val(type);
            }

            var customerId      = $("#customer_id").val();

            $.get(site_url + '/bills/calculate-supplier-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(toFixed(data.balance, 2));

                if (data.type == 1)
                {
                    $("#bbShow").html('Paya.');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', false);
                }
                else
                {
                    $("#bbShow").html('Adv.');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', true);
                }     
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }
        
        function toFixed(num, fixed) {
            var re = new RegExp('^-?\\d+(?:\.\\d{0,' + (fixed || -1) + '})?');
            return num.toString().match(re)[0];
        }
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = 1;
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                            $('.commonSupplierClass').val('');
                            $("#bbBalance").val(0);
                            $("#balance").val(0);
                        }
                        
                        $("#customer_id").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        }); 

        $('#submitBtn1').click(function() {
            
            var product_category_id             = $("#product_category_id").val();
            var product_sub_category_id         = $("#product_sub_category_id").val();
            var status                          = $("#status").val();
            var product_name                    = $("#product_name").val();
            var product_code                    = $("#product_code").val();
            var unit_id                         = $("#unit_id").val();
            var product_type                    = $("#product_type").val();
            var buying_price                    = $("#buying_price").val();
            var selling_price                   = $("#selling_price").val();
            var alert_quantity                  = $("#alert_quantity").val();
            var site_url                        = $('.site_url').val();

            if (product_name == '' || product_category_id == '' || buying_price == '' || selling_price == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }

            $.ajax({
    
                type:   'get',
                url:    site_url + '/bills/from-bill/store/product/',
                data:   { product_category_id : product_category_id, product_sub_category_id : product_sub_category_id, status : status, product_name : product_name, product_code : product_code, unit_id : unit_id, product_type : product_type, buying_price : buying_price, selling_price : selling_price, alert_quantity : alert_quantity,_token: '{{csrf_token()}}' },
    
                success: function (data) {
                    console.log(data);
                    if(data != 0)
                    {
                        $('#CloseButton1').click();
                        $('#add_field_button').click();
                    }

                    $("#product_id").val(data.id);
                }
    
            });
        });    
    </script>

    <script type="text/javascript">
        function getItemPrice(x)
        {
            var site_url    = $(".site_url").val();
            var entry_id    = $("#product_entries_"+x).val();
            var cartoon_val = $("#cartoon_"+x).val();
            var pcs_val     = $("#pcs_"+x).val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    if (data.product_entries.type == 1)
                    {
                        $("#u_id_"+x).hide();
                        $("#free_sft_id_"+x).show();
                        $("#cartoon_id_"+x).show();
                        $("#pcs_id_"+x).show();

                        var pcs_equivalent_sft  = (parseFloat(data.product_entries.height)*parseFloat(data.product_entries.width))/144;
                        var stock_in_pcs        = parseFloat(data.product_entries.stock_in_hand)/parseFloat(pcs_equivalent_sft);
                        var stock_in_cart       = parseFloat(stock_in_pcs)/parseFloat(data.product_entries.pcs_per_cartoon);

                        //
                            if (data.product_entries.stock_in_hand == '' || data.product_entries.stock_in_hand == null)
                            {
                                var stockInHand  = 0;
                            }
                            else
                            {
                                var stockInHand  = data.product_entries.stock_in_hand;
                            }

                        //
                            if (cartoon_val == '')
                            {
                                var cartoonVal  = 0;
                            }
                            else
                            {
                                var cartoonVal  = parseFloat(cartoon_val);
                            }

                        //
                            if (pcs_val == '')
                            {
                                var pcsVal  = 0;
                            }
                            else
                            {
                                var pcsVal  = parseFloat(pcs_val);
                            }

                        if (cartoonVal != 0)
                        {
                            var qtyVal1  = parseFloat(cartoonVal)*parseFloat(data.product_entries.pcs_per_cartoon)*parseFloat(pcs_equivalent_sft);
                        }
                        else
                        {
                            var qtyVal1  = 0;
                        }

                        if (pcsVal != 0)
                        {
                            var qtyVal2  = parseFloat(pcsVal)*parseFloat(pcs_equivalent_sft);

                            if (cartoonVal == '')
                            {
                                var pcsToCartoon = parseFloat(pcsVal)/parseFloat(data.product_entries.pcs_per_cartoon);

                                //x = 50; y = 15; res = x % y; x = (x - res) / y; [result = 3]
                                var cartAmount = (parseFloat(pcsVal) - (parseFloat(pcsVal)%parseFloat(data.product_entries.pcs_per_cartoon)))/parseFloat(data.product_entries.pcs_per_cartoon);
                                var pcsAmount  = parseFloat(pcsVal) - (parseFloat(cartAmount)*parseFloat(data.product_entries.pcs_per_cartoon));

                                $("#cartoon_"+x).val(toFixed(cartAmount, 2));
                                $("#pcs_"+x).val(toFixed(pcsAmount, 2));
                            }
                        }
                        else
                        {
                            var qtyVal2  = 0;
                        }

                        var qtyVal = parseFloat(qtyVal1) + parseFloat(qtyVal2);

                        if (stock_in_cart > 0)
                        {
                            var stock_in_cart  = stock_in_cart;
                        }
                        else
                        {
                           var stock_in_cart   = 0; 
                        }

                        if (stock_in_pcs > 0)
                        {
                            var stock_in_pcs  = stock_in_pcs;
                        }
                        else
                        {
                           var stock_in_pcs   = 0; 
                        }

                        $("#rate_"+x).val(toFixed(data.product_entries.buy_price, 2));
                        $("#discount_"+x).val(0);
                        $("#quantity_"+x).val(toFixed(qtyVal, 2));
                        $("#stock_"+x).val(toFixed(stockInHand, 2));
                        $("#stock_show_"+x).html('Stock : ' + toFixed(stockInHand, 2) + ' SFT ' + ' | ' + toFixed(stock_in_cart, 2) + ' Cart ' + ' | ' + toFixed(stock_in_pcs, 2) + ' PCS ');
          
                        calculateActualAmount(x);
                    }
                    else
                    {
                        $("#u_id_"+x).show();
                        $("#free_sft_id_"+x).hide();
                        $("#cartoon_id_"+x).hide();
                        $("#pcs_id_"+x).hide();

                        var list    = '';
                        $.each(data.unit_conversions, function(i, data_list)
                        {   
                            list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                        });

                        $("#unit_id_"+x).empty();
                        $("#unit_id_"+x).append(list);

                        //
                            if (data.product_entries.stock_in_hand == '' || data.product_entries.stock_in_hand == null)
                            {
                                var stockInHand  = 0;
                            }
                            else
                            {
                                var stockInHand  = data.product_entries.stock_in_hand;
                            }

                        //
                            if (data.product_entries.unit_id != null)
                            {
                                var unit = '( ' + data.product_entries.unit_name + ' )';
                            }
                            else
                            {
                                var unit = '';
                            }

                        $("#cartoon_"+x).val(0);
                        $("#pcs_"+x).val(0);
          
                        $("#rate_"+x).val(toFixed(data.product_entries.buy_price, 2));
                        $("#quantity_"+x).val(1);
                        $("#main_unit_id_"+x).val(data.product_entries.unit_id);
                        $("#main_unit_name_"+x).val(data.product_entries.unit_name);
                        $("#discount_"+x).val(0);
                        $("#stock_"+x).val(toFixed(stockInHand, 2));
                        $("#stock_show_"+x).html('Stock : ' + toFixed(stockInHand, 2) + unit);
          
                        calculateActualAmount(x);
                    }
                });
            }
        }

        function getItemPriceBackCalculation(x)
        {
            var site_url        = $(".site_url").val();
            var entry_id        = $("#product_entries_"+x).val();
            var sft_val         = $("#quantity_"+x).val();
            var free_sft_val    = $("#free_quantity_"+x).val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    if (data.product_entries.type == 1)
                    {
                        var pcs_equivalent_sft  = (parseFloat(data.product_entries.height)*parseFloat(data.product_entries.width))/144;
                        var stock_in_pcs        = parseFloat(data.product_entries.stock_in_hand)/parseFloat(pcs_equivalent_sft);
                        var stock_in_cart       = parseFloat(stock_in_pcs)/parseFloat(data.product_entries.pcs_per_cartoon);

                        //
                            if (sft_val == '')
                            {
                                var sftVal  = 0;
                            }
                            else
                            {
                                var sftVal  = parseFloat(sft_val);
                            }

                        //
                            if (free_sft_val == '')
                            {
                                var freeSftVal  = 0;
                            }
                            else
                            {
                                var freeSftVal  = parseFloat(free_sft_val);
                            }

                        var findPcs = (parseFloat(sft_val) + parseFloat(free_sft_val))/parseFloat(pcs_equivalent_sft);

                        //x = 50; y = 15; res = x % y; x = (x - res) / y; [result = 3]
                        var cartVal = (parseFloat(findPcs) - (parseFloat(findPcs)%parseFloat(data.product_entries.pcs_per_cartoon)))/parseFloat(data.product_entries.pcs_per_cartoon);

                        var pcsVal  = parseFloat(findPcs) - (parseFloat(cartVal)*parseFloat(data.product_entries.pcs_per_cartoon));

                        $("#cartoon_"+x).val(toFixed(cartVal, 2));
                        $("#pcs_"+x).val(toFixed(pcsVal, 2));
                    }

                    calculateActualAmount(x);
                });
            }
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock));

                    $("#stock_show_"+x).html('Stock : ' + toFixed(convertedStock, 2) + ' ( ' + convertedUnitName + ' )');
                    $("#rate_"+x).val(toFixed(data.purchase_price, 2));

                    calculateActualAmount(x);
                }

            });
        }

        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = -1;
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {   
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var cartoon_label       = '<label class="hidden-xs" for="productname">Cart</label>\n';
                    var unit_label          = '<label class="hidden-xs" for="productname">Unit</label>\n';
                    var pcs_label           = '<label class="hidden-xs" for="productname">PCS</label>\n';
                    var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">SFT/Qty *</label>\n';
                    var free_quantity_label = '<label class="hidden-xs" for="productname">Free/SFT *</label>\n';
                    var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">Discount</label>\n';
                    var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 19px" for="productname"></label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }
                else
                {
                    var product_label       = '';
                    var unit_label          = '';
                    var cartoon_label       = '';
                    var pcs_label           = '';
                    var rate_label          = '';
                    var quantity_label      = '';
                    var free_quantity_label = '';
                    var discount_label      = '';
                    var type_label          = '';
                    var amount_label        = '';
                    var action_label        = '';

                    var add_btn        = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px" class="col-lg-2 col-md-2 col-sm-3 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Product--' + '</option>' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +

                                                    '<div id="u_id_'+x+'" style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                        unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_'+x+'" onchange="getConversionParam('+x+')">\n' +
                                                        '<option value="">--Select Unit--</option>\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">SFT/Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control" id="quantity_'+x+'" placeholder="Quantity" oninput="getItemPriceBackCalculation('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div id="free_sft_id_'+x+'" style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Free/SFT *</label>\n' +
                                                        free_quantity_label  +
                                                        '<input type="text" name="free_quantity[]" class="inner form-control" id="free_quantity_'+x+'" placeholder="Free Quantity" oninput="calculateActualAmount('+x+')" value="0.00" />\n' +
                                                    '</div>\n' +

                                                    '<div id="cartoon_id_'+x+'" style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Cart</label>\n' +
                                                        cartoon_label +
                                                        '<input type="text" name="cartoon[]" class="inner form-control" id="cartoon_'+x+'" placeholder="Cartoon" oninput="getItemPrice('+x+')" />\n' +
                                                    '</div>\n' +

                                                    '<div id="pcs_id_'+x+'" style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">PCS</label>\n' +
                                                        pcs_label +
                                                        '<input type="text" name="pcs[]" class="inner form-control" id="pcs_'+x+'" placeholder="PCS" onchange="getItemPrice('+x+')" />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 19px" for="productname"></label>\n' +
                                                                type_label +
                                                                '<select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_'+x+'" oninput="calculateActualAmount('+x+')">\n' +
                                                                    '<option value="1">BDT</option>' +
                                                                    '<option value="0" selected>%</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" name="discount[]" class="inner form-control" id="discount_'+x+'" placeholder="Discount" oninput="calculateActualAmount('+x+')"/>\n' +
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                        amount_label +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

                var site_url = $(".site_url").val();
                $(".productEntries").select2({
                    ajax: { 
                    url:  site_url + '/bills/product-list-load-bill',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                    },
                });
            }                                    
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            var rate                    = $("#rate_"+x).val();
            var quantity                = $("#quantity_"+x).val();
            var cartoon                 = $("#cartoon_"+x).val();
            var free_quantity           = $("#free_quantity_"+x).val();;
            var stock                   = $("#stock_"+x).val();
            var discount                = $("#discount_"+x).val();
            var discountType            = $("#discount_type_"+x).val();
            var vatType                 = $("#vat_type_0").val();
            var vatAmount               = $("#vat_amount_0").val();
            var taxType                 = $("#tax_type_0").val();
            var taxAmount               = $("#tax_amount_0").val();
            var totalDiscount           = $("#total_discount_0").val();
            var totalDiscountType       = $("#total_discount_type_0").val();

            if (rate == '')
            {
                var rateCal             = 0;
            }
            else
            {
                var rateCal             = $("#rate_"+x).val();
            }

            if (quantity == '')
            {
                var quantityCal         = 1;
            }
            else
            {
                var quantityCal         = $("#quantity_"+x).val();
            }

            if (cartoon == '')
            {
                var cartoonCal          = 1;
            }
            else
            {
                var cartoonCal          = $("#cartoon_"+x).val();
            }

            if (free_quantity == '')
            {
                var freeQuantityCal     = 1;
            }
            else
            {
                var freeQuantityCal     = $("#free_quantity_"+x).val();
            }

            if (discount == '')
            {
                var discountCal         = 1;
            }
            else
            {
                var discountCal         = $("#discount_"+x).val();
            }

            if (discount == '')
            {
                var discountTypeCal     = 0;
            }
            else
            {
                if (discountType == 0)
                {
                    var discountTypeCal     = (parseFloat(discountCal)*parseFloat(rateCal)*(parseFloat(quantityCal)))/100;
                }
                else
                {
                    var discountTypeCal     = $("#discount_"+x).val();
                }
            }

            var AmountIn              =  (parseFloat(rateCal)*parseFloat(quantityCal)) - parseFloat(discountTypeCal) - (parseFloat(rateCal)*parseFloat(freeQuantityCal)*parseFloat(cartoonCal));
     
            $("#amount_"+x).val(toFixed(AmountIn, 2));

            //Calculating Subtotal Amount
            var total       = 0;

            $('.amount').each(function()
            {
                total       += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(toFixed(total, 2));
            $("#subTotalBdtShow").val(toFixed(total, 2));
        }
    </script>

    <script type="text/javascript">
        $(document).on("change", "#defaultCheck2" , function() {

            var checkbox_value    = $("#defaultCheck2")[0].checked;

            if (checkbox_value == true)
            {
                $("#vatShow").show();
                $("#taxShow").show();
            }
            else
            {
                $("#vatShow").hide();
                $("#taxShow").hide();
            }
        });

        $(document).on("change", "#customer_id" , function() {

            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            $.get(site_url + '/bills/calculate-supplier-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data.balance);

                if (data.type == 1)
                {
                    $("#bbShow").html('Paya.');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', false);
                }
                else
                {
                    $("#bbShow").html('Adv.');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', true);
                }
                
            });
        });

        function adjustAdvancePayment()
        {
            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            if ($('#adjustAdvancePayment').is(":checked"))
            {
                $("#adjustment").val(0);
                $("#customer_advance_amount").val(0);
                calculateChangeAmount();
            }
            else
            {
                $.get(site_url + '/bills/adjust-advance-payment/' + customerId, function(data){

                    var payable  = $("#totalBdtShow").val();

                    if (parseFloat(payable) <= parseFloat(data))
                    {
                        $("#adjustment").val(toFixed(payable, 2));
                    }
                    else
                    {
                        $("#adjustment").val(toFixed(data, 2));
                    }

                    $("#customer_advance_amount").val(toFixed(data, 2));
                    calculateChangeAmount();
                });
            }
        }
    </script>
@endsection