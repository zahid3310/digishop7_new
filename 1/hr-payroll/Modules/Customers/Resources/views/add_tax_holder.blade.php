@extends('layouts.app')

@section('title', 'Add Tax Holder')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Add Tax Holder</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Registers</a></li>
                                    <li class="breadcrumb-item active">Add Tax Holder</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('store_tax_holder') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div class="row">
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Holding No *</label>
                                        <input name="holding_no" type="text" class="form-control" placeholder="হোল্ডিং নং" required>
                                    </div>                                    

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Taxpayer ID *</label>
                                        <input name="taxpayer_id" type="text" class="form-control" placeholder="করদাতা আইডি" required>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Taxpayer Name *</label>
                                        <input name="taxpayer_name" type="text" class="form-control" placeholder="করদাতা নাম" required>
                                    </div>


                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Road / Mouza / Area / Mahalla *</label>
                                        <input name="area_name" type="text" class="form-control" placeholder="রাস্তা/মৌজা/এলাকা/মহল্লা" required>
                                    </div>
                                    

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Mobile Mo</label>
                                        <input name="mobile_number" type="number" class="form-control" placeholder="মোবাইল নং">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Type</label>
                                        <select class="form-control select2" name="type">
                                            <option value="1">Residential</option>
                                            <option value="2">Commercial</option>
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('customers_index') }}">Close</a></button>
                                    </div>
                                </div>



                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Contacts</h4>

                                <div style="margin-right: 10px" class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-1">Search : </div>
                                    <div class="col-md-2">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Sl</th>
                                            <th>Holding No</th>
                                            <th>Taxpayer ID</th>
                                            <th>Taxpayer Name</th>
                                            <th>Road / Mouza / Area / Mahalla</th>
                                            <th>Mobile No</th>
                                            <th>Type</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($lists as $key=> $list)
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td>{{$list->holding_no}}</td>
                                            <td>{{$list->taxpayer_id}}</td>
                                            <td>{{$list->taxpayer_name}}</td>
                                            <td>{{$list->area_name}}</td>
                                            <td>{{$list->mobile_number}}</td>
                                            <td>
                                                @if($list->type == 1)
                                                Residential
                                                @else
                                                Commercial
                                                @endif
                                                
                                            </td>
                                            <td>
                                                <div class="dropdown">
                                                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                        <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" style="">
                                                        <a class="dropdown-item" href="{{ route('edit_tax_holder', $list['id']) }}">Edit</a>
                                                    </div>
                                                </div>
        
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
