@extends('layouts.app')

@section('title', 'Show')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Purchase Return</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Purchase Return</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                    <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">{{ userDetails()->organization_name }}</h2>
                                    <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ userDetails()->address }}</p>
                                    <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ userDetails()->contact_number }}</p>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12"></div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-4">
                                        <address>
                                            <strong>Return To:</strong><br>
                                            {{ $purchase_return['customer_name'] }}
                                            @if($purchase_return['address'] != null)
                                               <br> <?php echo $purchase_return['address']; ?> <br>
                                            @endif
                                            @if($purchase_return['address'] == null)
                                                <br>
                                            @endif
                                            {{ $purchase_return['phone'] }}
                                        </address>
                                    </div>

                                    <div class="col-sm-4">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div style="font-size: 16px" class="col-sm-4 text-sm-right">
                                        <address class="mt-2 mt-sm-0">
                                            <strong>Return Date:</strong><br>
                                            {{ date('d-m-Y', strtotime($purchase_return['purchase_return_date'])) }}<br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-size-15 font-weight-bold">Return summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Return # {{ 'PR - ' . str_pad($purchase_return['purchase_return_number'], 6, "0", STR_PAD_LEFT) }}</h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-nowrap">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th style="width: 70px;">No.</th>
                                                <th>Item</th>
                                                <th class="text-right">Rate</th>
                                                <th class="text-right">Qty</th>
                                                <th class="text-center">Unit</th>
                                                <th class="text-right">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">

                                            @if(!empty($entries) && ($entries->count() > 0))

                                            <?php $sub_total = 0; ?>

                                            @foreach($entries as $key => $value)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $value['product_entry_name'] }}</td>
                                                <td class="text-right">{{ number_format($value['rate'],2,'.',',') }}</td>
                                                <td class="text-right">{{ $value['quantity'] }}</td>
                                                <td class="text-center">SFT</td>
                                                <td class="text-right">{{ number_format($value['total_amount'],2,'.',',') }}</td>
                                            </tr>

                                            <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                            @endforeach
                                            @endif
                                            
                                           <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="5" class="text-right">Total</td>
                                                <td style="font-size: 16px;font-weight: bold" class="text-right">
                                                    {{ number_format($sub_total,2,'.',',') }}
                                                </td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="5" class="text-right">Less</td>
                                                <td style="font-size: 16px;font-weight: bold" class="text-right">
                                                    {{ number_format($purchase_return->less,2,'.',',') }}
                                                </td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="5" class="text-right">Net Total</td>
                                                <td style="font-size: 16px;font-weight: bold" class="text-right">
                                                    {{ number_format($sub_total - $purchase_return->less,2,'.',',') }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                @if($purchase_return['return_note'] != null)
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6><strong>Note :</strong> {{ $purchase_return['return_note'] }}</h6>
                                    </div>
                                </div>
                                @endif

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection