@extends('layouts.app')

@section('title', 'Edit Information')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Information</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Company Information</a></li>
                                    <li class="breadcrumb-item active">Edit Information</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('branch_update', $find_branch['id']) }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div style="margin-bottom: 0px !important" class="form-group row">
                                    <div class="col-md-3 form-group">
                                        <label for="productname">Branch Name *</label>
                                        <input type="text" name="branch_name" class="inner form-control" id="branch_name" value="{{ $find_branch['name'] }}" required />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname">Organization Name</label>
                                        <input type="text" name="organization_name" class="inner form-control" id="organization_name" placeholder="Organization Name" value="{{ $find_branch['organization_name'] }}" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname">Address</label>
                                        <input type="text" name="address" class="inner form-control" id="address" placeholder="Address" value="{{ $find_branch['address'] }}" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname">Contact Number</label>
                                        <input type="text" name="contact_number" class="inner form-control" id="contact_number" placeholder="Contact Number" value="{{ $find_branch['contact_number'] }}" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname">Email</label>
                                        <input type="text" name="email" class="inner form-control" id="email" placeholder="Email Address" value="{{ $find_branch['email'] }}" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname">Website</label>
                                        <input type="text" name="website" class="inner form-control" id="website" placeholder="Website Link" value="{{ $find_branch['website'] }}" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname">Printer Type</label>
                                        <select style="cursor: pointer" class="form-control" id="printer_type" name="printer_type">
                                            <option value="0" {{ $find_branch['printer_type'] == 0 ? 'selected' : '' }}>58 mm Thermal Receipt</option>
                                            <option value="1" {{ $find_branch['printer_type'] == 1 ? 'selected' : '' }}>80 mm Thermal Receipt</option>
                                            <option value="2" {{ $find_branch['printer_type'] == 2 ? 'selected' : '' }}>A4 Size Paper</option>
                                            <option value="3" {{ $find_branch['printer_type'] == 3 ? 'selected' : '' }}>Letter Size Paper</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname">Logo</label>
                                        <input class="form-control" type="file" name="logo" >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Previous Logo</label>
                                    <div class="col-md-10">
                                        @if($find_branch['logo'] != null)
                                        <img style="height: 60px;width: 200px" src="{{ url('public/'.$find_branch['logo']) }}">
                                        @endif
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('branch_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection