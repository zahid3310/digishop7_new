<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Accounts extends Model
{  
    protected $table = "accounts";

    public function parentAccountType()
    {
        return $this->belongsTo('App\Models\ParentAccountTypes','parent_account_type_id');
    }

    public function accountType()
    {
        return $this->belongsTo('App\Models\AccountTypes','account_type_id');
    }
}
