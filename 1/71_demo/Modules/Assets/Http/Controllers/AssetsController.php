<?php

namespace Modules\Assets\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Assets;
use DB;
use Response;

class AssetsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $assets         = Assets::orderBy('id', 'ASC')->get();
    
        return view('assets::index', compact('assets'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'  => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $asset                  = new Assets;
            $asset->name            = $data['name'];
            $asset->quantity        = $data['quantity'];
            $asset->asset_value     = $data['asset_value'];
            $asset->created_by      = $user_id;

            if ($asset->save())
            {   
                DB::commit();
                return back()->with("success","Asset Added Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function edit($id)
    {
        $find_asset     = Assets::find($id);
        $assets         = Assets::orderBy('id', 'ASC')->get();
    
        return view('assets::edit', compact('assets', 'find_asset'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'name'  => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $asset                  = Assets::find($id);
            $asset->name            = $data['name'];
            $asset->quantity        = $data['quantity'];
            $asset->asset_value     = $data['asset_value'];
            $asset->updated_by      = $user_id;

            if ($asset->save())
            {   
                DB::commit();
                return redirect()->route('assets_index')->with("success","Asset Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }
}
