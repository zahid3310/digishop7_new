<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('purchasereturn')->group(function() {
    Route::get('/', 'PurchaseReturnController@index')->name('purchase_return_index');
    Route::post('/store', 'PurchaseReturnController@store')->name('purchase_return_store');
    Route::get('/show/{id}', 'PurchaseReturnController@show')->name('purchase_return_show');
    Route::get('/delete/{id}', 'PurchaseReturnController@delete')->name('purchase_return_delete');
    Route::get('/bill-list-by-customer/{id}', 'PurchaseReturnController@billList')->name('purchase_return_invoice_list');
    Route::get('/bill-entries-list-by-bill/{id}', 'PurchaseReturnController@billEntriesList')->name('purchase_return_invoice__entries_list');
    Route::get('/purchase-return/list/load', 'PurchaseReturnController@purchaseReturnListLoad')->name('purchase_return_list_load');
    Route::get('/purchase-return/search/list/{id}', 'PurchaseReturnController@purchaseReturnListSearch')->name('purchase_return_list_search');
    Route::get('/find-bill-details/{id}', 'PurchaseReturnController@findBillDetails')->name('purchase_return_find_bill_details');
    Route::get('/calculate-opening-balance/{id}', 'PurchaseReturnController@calculateOpeningBalance');
});
