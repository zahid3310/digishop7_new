<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('items')->group(function() {
    Route::get('/', 'ItemsController@index')->name('items_index');
    Route::post('/store', 'ItemsController@store')->name('items_store');
    Route::get('/edit/{id}', 'ItemsController@edit')->name('items_edit');
    Route::post('/update/{id}', 'ItemsController@update')->name('items_update');
});