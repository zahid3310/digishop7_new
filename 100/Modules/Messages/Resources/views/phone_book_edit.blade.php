@extends('layouts.app')

@section('title', 'Edit Phone Book')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.pb')}} {{ __('messages.edit')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.pb')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.contact')}} {{ __('messages.edit')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                @if(Auth::user()->role == 1)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('messages_phone_book_update', $find_phone_books['id']) }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div style="margin-bottom: 0px !important" class="form-group row">
                                    <div class="col-md-4 form-group">
                                        <label for="productname">{{ __('messages.name')}} *</label>
                                        <input type="text" name="name" class="inner form-control" id="name" value="{{ $find_phone_books['name'] }}" required />
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="productname">{{ __('messages.phone')}} *</label>
                                        <input type="text" name="phone" class="inner form-control" id="phone" value="{{ $find_phone_books['phone'] }}" required />
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="productname">{{ __('messages.address')}}</label>
                                        <input type="text" name="address" class="inner form-control" id="address" value="{{ $find_phone_books['address'] }}" />
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">{{ __('messages.update')}}</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('messages_phone_book_index') }}">{{ __('messages.close')}}</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
                @endif

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">{{ __('messages.all_phone_contact')}}</h4>

                                <br>

                                <table id="datatable" class="table table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.sl')}}</th>
                                            <th>{{ __('messages.name')}}</th>
                                            <th>{{ __('messages.phone')}}</th>
                                            <th>{{ __('messages.address')}}</th>
                                            <th>{{ __('messages.action')}}</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        @if(!empty($phone_books) && ($phone_books->count() > 0))
                                        @foreach($phone_books as $key => $phone_book)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $phone_book['name'] }}</td>
                                                <td>{{ $phone_book['phone'] }}</td>
                                                <td>{{ $phone_book['address'] }}</td>
                                                <td>
                                                    @if(Auth::user()->role == 1)
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('messages_phone_book_edit', $phone_book['id']) }}">{{ __('messages.edit')}}</a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection