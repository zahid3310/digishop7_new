<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class IssueReturnEntries extends Model
{  
    protected $table = "issue_return_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function issueReturn()
    {
        return $this->belongsTo('App\Models\IssueReturn','issue_return_id');
    }

}
