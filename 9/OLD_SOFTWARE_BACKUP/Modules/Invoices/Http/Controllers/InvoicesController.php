<?php

namespace Modules\Invoices\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\PaidThroughAccounts;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Users;
use App\Models\SrItems;
use App\Models\FreeItems;
use App\Models\AccountTransactions;
use Response;
use DB;
use View;
use Carbon\Carbon;
use charlieuki\ReceiptPrinter\ReceiptPrinter as ReceiptPrinter;

class InvoicesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoices           = Invoices::orderBy('id', 'DESC')->first();
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();

        return view('invoices::index', compact('paid_accounts', 'invoices'));
    }

    public function AllSales()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                                ->get();

        return view('invoices::all_sales', compact('paid_accounts'));
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('invoices::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'          => 'required',
            'customer_id'           => 'required',
            'product_entries.*'     => 'required',
            'amount.*'              => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];
             
            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }

            $data_find                          = Invoices::orderBy('created_at', 'DESC')->first();
            $invoice_number                     = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

            $invoice                            = new Invoices;;
            $invoice->invoice_number            = $invoice_number;
            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->sr_id                     = $data['sr_id'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $data['total_amount'];
            $invoice->total_buy_price           = round($buy_price, 2);
            $invoice->total_discount            = $discount;
            $invoice->invoice_note              = $data['invoice_note'];
            $invoice->total_vat                 = $vat;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->created_by                = $user_id;

            if (isset($data['type']))
            {
                $invoice->type = $data['type'];
            }

            if ($invoice->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'            => $invoice['id'],
                        'product_id'            => $product_buy_price['product_id'],
                        'product_entry_id'      => $value,
                        'free_product_entry_id' => $data['free_items'][$key],
                        'customer_id'           => $invoice['customer_id'],
                        'reference_id'          => $data['reference_id'],
                        'buy_price'             => round($product_buy_price['buy_price'], 2),
                        'rate'                  => $data['rate'][$key],
                        'quantity'              => $data['quantity'][$key],
                        'free_quantity'         => $data['free_quantity'][$key],
                        'total_amount'          => $data['amount'][$key],
                        'discount_type'         => $data['discount_type'][$key],
                        'discount_amount'       => $data['discount'][$key],
                        'created_by'            => $user_id,
                        'created_at'            => date('Y-m-d H:i:s'),
                    ];

                    if ($data['sr_id'] != 0)
                    {
                        $sr_items[] = [
                            'sales_id'          => $invoice['id'],
                            'sr_id'             => $data['sr_id'],
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'product_id'        => $product['product_id'],
                            'product_entry_id'  => $value,
                            'type'              => 2,
                            'quantity'          => $data['quantity'][$key],
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }

                    if ($data['free_items'][$key] != 0)
                    {
                        $sr_items[] = [
                            'sales_id'          => $invoice['id'],
                            'sr_id'             => $data['sr_id'],
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'product_id'        => $product['product_id'],
                            'product_entry_id'  => $data['free_items'][$key],
                            'type'              => 2,
                            'quantity'          => $data['free_quantity'][$key],
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }

                    if ($data['free_quantity'][$key] != 0)
                    {
                        $free_items[] = [
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'invoice_id'        => $invoice['id'],
                            'customer_id'       => $data['customer_id'],
                            'sr_id'             => $data['sr_id'],
                            'product_entry_id'  => $value,
                            'total_quantity'    => $data['quantity'][$key],
                            'free_quantity'     => $data['free_quantity'][$key],
                            'purchase_price'    => round($product_buy_price['buy_price'], 2),
                            'sell_price'        => $data['rate'][$key],
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('invoice_entries')->insert($invoice_entries);

                if ($data['sr_id'] != 0)
                {
                    DB::table('sr_items')->insert($sr_items);
                }

                if (isset($free_items))
                {
                    DB::table('free_items')->insert($free_items);
                }

                // stockOut($data, $item_id=null);

                if (isset($data['amount_paid']))
                {
                    $data_find        = Payments::orderBy('id', 'DESC')->first();
                    $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {   
                        if ($data['amount_paid'][$i] > 0)
                        {
                            $account_transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through_id'       => $data['paid_through'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 0,  // 0 = In , 1 = Out
                                'transaction_head'      => 'sales',
                                'associated_id'         => $invoice->id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payments = [
                                'payment_number'        => $payment_number,
                                'customer_id'           => $data['customer_id'],
                                'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'paid_through'          => $data['paid_through'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 0,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payment_id = DB::table('payments')->insertGetId($payments);      

                            if ($payment_id)
                            {   
                                $payment_entries = [
                                        'payment_id'        => $payment_id,
                                        'invoice_id'        => $invoice['id'],
                                        'amount'            => $data['amount_paid'][$i],
                                        'created_by'        => $user_id,
                                        'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('payment_entries')->insert($payment_entries);  
                            }

                            $update_invoice_dues                = Invoices::find($invoice['id']);
                            $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                            $update_invoice_dues->save();

                            $payment_number++;
                        }
                    }

                    if (isset($account_transactions))
                    {
                        DB::table('account_transactions')->insert($account_transactions);
                    }
                }

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Sales Created Successfully !!");
                }
                else
                {
                    $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                            ->select('invoices.*',
                                                     'customers.name as customer_name',
                                                     'customers.address as address',
                                                     'customers.phone as phone')
                                            ->find($invoice['id']);

                    $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->where('invoice_entries.invoice_id', $invoice['id'])
                                            ->select('invoice_entries.*',
                                                     'product_entries.product_type as product_type',
                                                     'product_entries.name as product_entry_name',
                                                     'units.name as unit_name',
                                                     'products.name as product_name')
                                            ->get();  
                                 
                    $user_info  = Users::find(1);

                    return View::make('invoices::show_pos')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.address as address',
                                             'customers.phone as phone')
                                    ->find($id);

        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->where('invoice_entries.invoice_id', $id)
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.name as product_entry_name',
                                             'products.name as product_name')
                                    ->get();  
                     
        $user_info  = Users::find(1);;

        return view('invoices::show', compact('entries', 'invoice', 'user_info'));
    }

    public function showPos($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.address as address',
                                             'customers.phone as phone')
                                    ->find($id);

        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->where('invoice_entries.invoice_id', $id)
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.name as product_entry_name',
                                             'products.name as product_name')
                                    ->get();   
                     
        $user_info  = Users::find(1);

        return view('invoices::show_pos', compact('entries', 'invoice', 'user_info'));
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $products               = Products::orderBy('products.total_sold', 'DESC')
                                            ->get();

        $product_entry          = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->where('product_entries.stock_in_hand', '!=', null)
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                         GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                         GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                         GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                         GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                         GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                         GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                         GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                         GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT products.id) as product_id
                                                        ')
                                            ->orderBy('product_entries.total_sold', 'DESC')
                                            ->get();

        $product_entry          = $product_entry->sortBy('name')->all();
        $product_entries        = collect($product_entry);

        $find_invoice           = Invoices::leftjoin('customers as customers', 'customers.id', '=', 'invoices.customer_id')
                                            ->leftjoin('customers as srs', 'srs.id', '=', 'invoices.sr_id')
                                            ->select('invoices.*',
                                                 'customers.id as customer_id',
                                                 'customers.name as contact_name',
                                                 'srs.id as sr_id',
                                                 'srs.name as sr_name')
                                            ->find($id);

        $find_invoice_entries   = InvoiceEntries::leftjoin('customers', 'customers.id', 'invoice_entries.customer_id')  
                                            ->leftjoin('product_entries AS main_product', 'main_product.id', '=', 'invoice_entries.product_entry_id')
                                            ->leftjoin('product_entries AS free_product', 'free_product.id', '=', 'invoice_entries.free_product_entry_id')
                                            ->where('invoice_entries.invoice_id', $id)
                                            ->select('invoice_entries.*',
                                                    'customers.id as customer_id',
                                                    'customers.name as customer_name',
                                                    'main_product.id as item_id',
                                                    'main_product.stock_in_hand as stock_in_hand',
                                                    'main_product.name as item_name',
                                                    'free_product.id as free_item_id',
                                                    'free_product.name as free_item_name')
                                            ->get();

        $entries_count          = $find_invoice_entries->count();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->where('payment_entries.invoice_id', $id)
                                        ->selectRaw('payment_entries.*, 
                                                     payments.paid_through as paid_through_id,
                                                     payments.account_information as account_information,
                                                     payments.payment_date as payment_date,
                                                     payments.note as note')
                                        ->get();

        $payment_entries_count  = $payment_entries->count();
        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();
        
        return view('invoices::edit', compact('products', 'product_entries', 'find_invoice', 'find_invoice_entries', 'entries_count', 'payment_entries', 'payment_entries_count', 'paid_accounts'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'      => 'required',
            'customer_id'       => 'required',
            'product_entries.*' => 'required',
            'amount.*'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            // $adjustment     = $data['adjustment_amount'];
            $vat            = $data['vat_amount'];
            // $tax            = $data['tax_amount'];
            
            $invoice        = Invoices::find($id);

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }

            //Calculate Due Amount

                if ($data['total_amount'] > $invoice['invoice_amount']) 
                {
                    $invoice_dues = $invoice['due_amount'] + ($data['total_amount'] - $invoice['invoice_amount']);

                }
                
                if ($data['total_amount'] < $invoice['invoice_amount'])
                {
                    $invoice_dues = $invoice['due_amount'] - ($invoice['invoice_amount'] - $data['total_amount']);
                }

                if ($data['total_amount'] == $invoice['invoice_amount'])
                {
                    $invoice_dues = $invoice['due_amount'];
                }
            
            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->sr_id                     = $data['sr_id'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $invoice_dues;
            $invoice->total_buy_price           = round($buy_price, 2);
            $invoice->total_discount            = $discount;
            $invoice->invoice_note              = $data['invoice_note'];
            $invoice->total_vat                 = $vat;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->discount_code             = $data['coupon_code'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->updated_by                = $user_id;

            if ($invoice->save())
            {
                $item_id                = InvoiceEntries::where('invoice_id', $invoice['id'])->get();
                $item_delete            = InvoiceEntries::where('invoice_id', $invoice['id'])->delete();
                $delete_sr_items        = SrItems::where('sales_id', $invoice['id'])->delete();
                $delete_free_items      = FreeItems::where('invoice_id', $invoice['id'])->delete();

                if (isset($data['payment_id']))
                {
                    foreach ($data['payment_id'] as $key_p => $value_p)
                    {
                        $payment_delete  = Payments::where('id', $value_p)->delete();
                    }

                    $update_invoice                = Invoices::find($invoice->id);
                    $update_invoice->due_amount    = $update_invoice['invoice_amount'];
                    $update_invoice->updated_by    = $user_id;
                    $update_invoice->save();
                }

                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'            => $invoice['id'],
                        'product_id'            => $product_buy_price['product_id'],
                        'product_entry_id'      => $value,
                        'free_product_entry_id' => $data['free_items'][$key],
                        'customer_id'           => $invoice['customer_id'],
                        'reference_id'          => $data['reference_id'],
                        'buy_price'             => round($product_buy_price['buy_price'], 2),
                        'rate'                  => $data['rate'][$key],
                        'quantity'              => $data['quantity'][$key],
                        'free_quantity'         => $data['free_quantity'][$key],
                        'total_amount'          => $data['amount'][$key],
                        'discount_type'         => $data['discount_type'][$key],
                        'discount_amount'       => $data['discount'][$key],
                        'created_by'            => $user_id,
                        'created_at'            => date('Y-m-d H:i:s'),
                    ];

                    if ($data['sr_id'] != 0)
                    {
                        $sr_items[] = [
                            'sales_id'          => $invoice['id'],
                            'sr_id'             => $data['sr_id'],
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'product_id'        => $product['product_id'],
                            'product_entry_id'  => $value,
                            'type'              => 2,
                            'quantity'          => $data['quantity'][$key],
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }

                    if ($data['free_items'][$key] != 0)
                    {
                        $sr_items[] = [
                            'sales_id'          => $invoice['id'],
                            'sr_id'             => $data['sr_id'],
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'product_id'        => $product['product_id'],
                            'product_entry_id'  => $data['free_items'][$key],
                            'type'              => 2,
                            'quantity'          => $data['free_quantity'][$key],
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }

                    if ($data['free_quantity'][$key] != 0)
                    {
                        $free_items[] = [
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'invoice_id'        => $invoice['id'],
                            'customer_id'       => $data['customer_id'],
                            'sr_id'             => $data['sr_id'],
                            'product_entry_id'  => $value,
                            'total_quantity'    => $data['quantity'][$key],
                            'free_quantity'     => $data['free_quantity'][$key],
                            'purchase_price'    => round($product_buy_price['buy_price'], 2),
                            'sell_price'        => $data['rate'][$key],
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('invoice_entries')->insert($invoice_entries);

                if ($data['sr_id'] != 0)
                {
                    DB::table('sr_items')->insert($sr_items);
                }

                if (isset($free_items))
                {
                    DB::table('free_items')->insert($free_items);
                }

                $transaction_delete = AccountTransactions::where('transaction_head', 'sales')->where('associated_id', $invoice['id'])->delete();

                if (isset($data['amount_paid']))
                {
                    $data_find        = Payments::orderBy('id', 'DESC')->first();
                    $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {   
                        if ($data['amount_paid'][$i] > 0)
                        {
                            $account_transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through_id'       => $data['paid_through'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 0,  // 0 = In , 1 = Out
                                'transaction_head'      => 'sales',
                                'associated_id'         => $invoice->id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payments = [
                                'payment_number'        => $payment_number,
                                'customer_id'           => $data['customer_id'],
                                'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'paid_through'          => $data['paid_through'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 0,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payment_id = DB::table('payments')->insertGetId($payments);      

                            if ($payment_id)
                            {   
                                $payment_entries = [
                                        'payment_id'        => $payment_id,
                                        'invoice_id'        => $invoice['id'],
                                        'amount'            => $data['amount_paid'][$i],
                                        'created_by'        => $user_id,
                                        'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('payment_entries')->insert($payment_entries);  
                            }

                            $update_invoice_dues                = Invoices::find($invoice['id']);
                            $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                            $update_invoice_dues->save();

                            $payment_number++;
                        }
                    }

                    if (isset($account_transactions))
                    {
                        DB::table('account_transactions')->insert($account_transactions);
                    }
                }

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    if (isset($data['type']))
                    {
                        return redirect()->to('invoices'.'?sales_type='.$data['type'])->with("success","Sales Updated Successfully !!");
                    }
                    else
                    {
                        return redirect()->route('invoices_index')->with("success","Sales Updated Successfully !!");
                    }
                }
                else
                {
                    if (Auth::user()->service_type == 1)
                    {
                        return redirect()->route('invoices_show', $invoice['id']);
                    }
                    else
                    {
                        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                            ->select('invoices.*',
                                                     'customers.name as customer_name',
                                                     'customers.address as address',
                                                     'customers.phone as phone')
                                            ->find($invoice['id']);

                        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                                ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                ->where('invoice_entries.invoice_id', $invoice['id'])
                                                ->select('invoice_entries.*',
                                                         'product_entries.product_type as product_type',
                                                         'product_entries.name as product_entry_name',
                                                         'units.name as unit_name',
                                                         'products.name as product_name')
                                                ->get();  
                                     
                        $user_info  = Users::find(1);

                        return View::make('invoices::show_pos')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info);
                    }
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function productList()
    {
        $data       = ProductEntries::where('product_entries.stock_in_hand', '>', 0)
                                    ->Where('product_entries.stock_in_hand', '!=', null)
                                    ->orderBy('product_entries.total_sold', 'DESC')
                                    ->select('product_entries.*')
                                    ->get();

        return Response::json($data);
    }

    public function makePayment($id)
    {
        $data       = Invoices::find($id);

        return Response::json($data);
    }

    public function storePayment(Request $request)
    {
        $user_id                    = Auth::user()->id;
        $data                       = $request->all();

        DB::beginTransaction();

        try{

            if (isset($data['amount_paid']))
            {
                $data_find        = Payments::orderBy('id', 'DESC')->first();
                $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                for($i = 0; $i < count($data['amount_paid']); $i++)
                {   
                    if ($data['amount_paid'][$i] > 0)
                    {
                        $payments = [
                            'payment_number'        => $payment_number,
                            'customer_id'           => $data['customer_id'],
                            'payment_date'          => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $data['amount_paid'][$i],
                            'paid_through'          => $data['paid_through'][$i],
                            'account_information'   => $data['account_information'][$i],
                            'note'                  => $data['note'][$i],
                            'type'                  => 0,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $payment_id = DB::table('payments')->insertGetId($payments);      

                        if ($payment_id)
                        {   
                            $payment_entries = [
                                    'payment_id'        => $payment_id,
                                    'invoice_id'        => $data['invoice_id'],
                                    'amount'            => $data['amount_paid'][$i],
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            DB::table('payment_entries')->insert($payment_entries);  
                        }

                        $update_invoice_dues                = Invoices::find($data['invoice_id']);
                        $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                        $update_invoice_dues->save();

                        $payment_number++;
                    }
                }
            }
            
            DB::commit();
            return Response::json(1);
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            dd($exception);
            return Response::json(0);
        }
    }

    public function productPriceList($id)
    {
        $data       = ProductEntries::find($id);

        return Response::json($data);
    }

    public function productPriceListBySr($product_entry_id, $sr_id)
    {
        $data   = SrItems::leftjoin('product_entries', 'product_entries.id', 'sr_items.product_entry_id')
                                    ->where('sr_items.sr_id', $sr_id)
                                    ->where('sr_items.product_entry_id', $product_entry_id)
                                    ->groupBy('sr_items.product_entry_id')
                                    ->select(DB::raw("GROUP_CONCAT(DISTINCT sr_items.product_entry_id) as product_entry_id"),
                                             DB::raw("GROUP_CONCAT(DISTINCT sr_items.product_id) as product_id"),
                                             DB::raw("GROUP_CONCAT(DISTINCT product_entries.sell_price) as sell_price"),
                                             DB::raw("SUM(IF(sr_items.type='1',sr_items.quantity,0)) -  SUM(IF(sr_items.type='2',sr_items.quantity,0)) - SUM(IF( sr_items.type='3',sr_items.quantity,0)) as stock_in_hand")
                                            )
                                    ->first();

        return Response::json($data);
    }

    public function invoiceListLoad()
    {
        $data           = Invoices::leftjoin('sales_return', 'sales_return.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->where('invoices.type', 1)
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                            'sales_return.id as return_id',
                                            'customers.name as customer_name',
                                            'customers.phone as phone')
                                    ->distinct('invoices.id')
                                    ->take(20)
                                    ->get();

        return Response::json($data);
    }

    public function invoiceListSearch($from_date, $to_date, $customer, $invoice)
    {
        $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
        $search_by_customer     = $customer != 0 ? $customer : 0;
        $search_by_invoice      = $invoice != 0 ? $invoice : 0;

        $data           = Invoices::leftjoin('sales_return', 'sales_return.invoice_id', 'invoices.id')
                                        ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                        ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                        ->where('invoices.type', 1)
                                        ->whereBetween('invoices.invoice_date', [$search_by_from_date, $search_by_to_date])
                                        ->when($search_by_customer != 0, function ($query) use ($search_by_customer) {
                                            return $query->where('customers.id', $search_by_customer);
                                        })
                                        ->when($search_by_invoice != 0, function ($query) use ($search_by_invoice) {
                                            return $query->where('invoices.id', $search_by_invoice);
                                        })
                                        ->orderBy('invoices.created_at', 'DESC')
                                        ->select('invoices.*',
                                                 'sales_return.id as return_id',
                                                 'customers.name as customer_name',
                                                 'customers.phone as phone')
                                        ->distinct('invoices.id')
                                        ->take(20)
                                        ->get();

        return Response::json($data);
    }

    public function posSearchProduct($id)
    {
        $user_info      = userDetails();
        $data           = ProductEntries::where('product_entries.product_code', $id)
                                        ->where('product_entries.stock_in_hand', '>', 0)
                                        ->first();

        return Response::json($data);
    }

    public function customerStore(Request $request)
    {
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        $customers                              = new Customers;
        $customers->name                        = $data['customer_name'];
        $customers->address                     = $data['address'];
        $customers->phone                       = $data['mobile_number'];
        $customers->contact_type                = $data['contact_type'];
        $customers->created_by                  = $user_id;

        if ($customers->save())
        {   
            return Response::json($customers);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function customersListInvoice()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Customers::orderBy('customers.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Customers::where('customers.name', 'LIKE', "%$search%")
                                    ->orWhere('customers.phone', 'LIKE', "%$search%")
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "phone" =>'', "contact_type" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name'], "phone" =>$value['phone'], "contact_type" =>$value['contact_type']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function couponCode($id)
    {
        $today          = Carbon::today();

        $data           = Discounts::leftjoin('discount_products', 'discount_products.discount_id', 'discounts.id')
                                ->where('discounts.coupon_code', $id)
                                ->orWhere('discounts.card_number', $id)
                                ->whereDate('discounts.expire_date', '>=', $today->format('Y-m-d'))
                                ->where('discounts.status', 1)
                                ->select('discount_products.product_id as product_id', 
                                         'discounts.discount_type as discount_type',
                                         'discounts.discount_amount as discount_amount',
                                         'discounts.expire_date as expire_date'
                                        )
                                ->get();

        return Response::json($data);
    }

    public function printInvoicesList()
    {
        $user_id        = Auth::user()->id;
        $data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->where('invoices.type', 1)
                                    ->where('invoices.created_by', $user_id)
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                            'customers.name as customer_name',
                                            'customers.phone as phone',
                                            'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function printInvoicesSearch($date,$customer,$invoice_number)
    {
        $search_by_date              = $date != 0 ? date('Y-m-d', strtotime($date)) : 0;
        $search_by_invoice_number    = $invoice_number != 0 ? $invoice_number : 0;
        $search_by_invoice_customer  = $customer != 0 ? $customer : 0;
        $user_info                   = userDetails();

        $data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->where('invoices.type', 1)
                                    ->when($search_by_date != 0, function ($query) use ($search_by_date) {
                                        return $query->where('invoices.invoice_date', 'LIKE', "%$search_by_date%");
                                    })
                                    ->when($search_by_invoice_number != 0, function ($query) use ($search_by_invoice_number) {
                                        return $query->where('invoices.invoice_number', 'LIKE', "%$search_by_invoice_number%");
                                    })
                                    ->when($search_by_invoice_customer != 0, function ($query) use ($search_by_invoice_customer) {
                                        return $query->where('customers.name', 'LIKE', "%$search_by_invoice_customer%");
                                    })
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.phone as phone',
                                             'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function calculateOpeningBalance($customer_id)
    {
        $invoices               = Invoices::where('invoices.customer_id', $customer_id)
                                            ->select('invoices.*')
                                            ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->leftjoin('sales_return', 'sales_return.id', 'payment_entries.sales_return_id')
                                                ->where('payments.customer_id', $customer_id)
                                                ->where('payments.type', 2)
                                                ->select('payment_entries.*',
                                                         'payments.customer_id as customer_id')
                                                ->get();

        $invoice_amount         = $invoices->sum('invoice_amount');
        $return_amount          = $invoices->sum('return_amount');
        $paid_amount            = $invoice_amount - $invoices->sum('due_amount');
        $return_paid_amount     = $payment_entries_return->sum('amount');

        $balance                = $invoice_amount - $paid_amount - $return_paid_amount; 

        return Response::json($balance);
    }

    public function srProductStock($sr_id, $product_entry_id)
    {
        $data       = SrItems::leftjoin('issues', 'issues.id', 'sr_items.issue_id')
                                    ->leftjoin('customers', 'customers.id', 'sr_items.sr_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'sr_items.product_entry_id')
                                    ->where('sr_items.sr_id', $sr_id)
                                    ->where('sr_items.product_entry_id', $product_entry_id)
                                    ->groupBy('sr_items.product_entry_id')
                                    ->select(DB::raw('group_concat(distinct sr_items.product_entry_id) as product_entry_id'),
                                             DB::raw('group_concat(distinct product_entries.name) as product_name'),
                                             DB::raw("SUM(IF(sr_items.type='1',sr_items.quantity,0)) AS receive_quantity"),
                                             DB::raw("SUM(IF(sr_items.type='2',sr_items.quantity,0)) AS sold_quantity"),
                                             DB::raw("SUM(IF( sr_items.type='3',sr_items.quantity,0)) AS return_quantity")
                                            )
                                    ->first();
        

        return Response::json($data);
    }
}