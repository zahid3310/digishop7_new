<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class DealerDamageEntries extends Model
{  
    protected $table = "dealer_damage_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function unit()
    {
        return $this->belongsTo('App\Models\Units','main_unit_id');
    }

    public function convertedUnit()
    {
        return $this->belongsTo('App\Models\Units','conversion_unit_id');
    }

}
