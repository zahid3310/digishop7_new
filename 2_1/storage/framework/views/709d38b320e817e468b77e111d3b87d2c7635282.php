<!DOCTYPE html>
<html>

<head>
    <title>Customers Rate Print</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<style type="text/css">
    @page  {
      size: A4 landscape;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p><?php echo e($user_info['address']); ?></p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Customers Rate Print</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">CUSTOMER</th>
                                    <th style="text-align: center">PRODUCT</th>
                                    <th style="text-align: center">REPORT TYPE</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">
                                        <?php if($customer_name != null): ?>
                                            <?php echo e($customer_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>

                                    <td style="text-align: center">
                                        <?php if($product_name != null): ?>
                                            <?php echo e($product_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>

                                    <td style="text-align: center">
                                        <?php if($report_type == 1): ?>
                                            CUSTOMER WISE
                                        <?php else: ?>
                                            PRODUCT WISE
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <?php if($report_type == 1): ?>
                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 15%">Category</th>
                                    <th style="text-align: center;width: 8%">CODE</th>
                                    <th style="text-align: center;width: 25%">Product</th>
                                    <th style="text-align: center;width: 10%">Brand</th>
                                    <th style="text-align: center;width: 10%">Whole Sale Rate</th>
                                    <th style="text-align: center;width: 10%">Retail Rate</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td style="color: black" colspan="10"><?php echo e($key1); ?></td>
                                </tr>

                                <?php
                                    $i                      = 1; 
                                ?>

                                <?php $__currentLoopData = $value1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($value->count() > 0): ?>
                                <tr>
                                    <td style="text-align: center;vertical-align: middle" rowspan="1"><?php echo e($i); ?></td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="1"><?php echo e($value['category_name']); ?></td>

                                    <td style="text-align: center;"><?php echo e(str_pad($value['product_code'], 6, "0", STR_PAD_LEFT)); ?></td>
                                    <td style="text-align: left;">
                                        <?php echo e($value['name']); ?>

                                        <?php
                                            if ($value['product_type'] == 2)
                                            {
                                                echo ' - ' . $value['variations'];
                                            } 
                                        ?>
                                    </td>
                                    <td style="text-align: center;"><?php echo e($value['brand_name']); ?></td>
                                    <td style="text-align: center;"><?php echo e($value['whole_sale_price']); ?></td>
                                    <td style="text-align: right;"><?php echo e($value['retail_price']); ?></td>
                                </tr>
                                <?php $i++; ?>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                        <?php endif; ?>

                        <?php if($report_type == 2): ?>
                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 15%">Customer Name</th>
                                    <th style="text-align: center;width: 10%">Whole Sale Rate</th>
                                    <th style="text-align: center;width: 10%">Retail Rate</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td style="color: black" colspan="10">
                                        <?php echo ProductName($key1); ?>
                                        <?php 
                                            $variations = ProductVariationName($key1);

                                            if($variations != null)
                                            {
                                                echo ' - ' . $variations; 
                                            }
                                        ?>
                                    </td>
                                </tr>

                                <?php
                                    $i                      = 1; 
                                ?>

                                <?php $__currentLoopData = $value1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($value->count() > 0): ?>
                                <tr>
                                    <td style="text-align: center;vertical-align: middle" rowspan="1"><?php echo e($i); ?></td>
                                    <td style="text-align: left;"><?php echo e($value['customer_name']); ?></td>
                                    <td style="text-align: right;"><?php echo e($value['whole_sale_price']); ?></td>
                                    <td style="text-align: right;"><?php echo e($value['retail_price']); ?></td>
                                </tr>
                                <?php $i++; ?>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                        <?php endif; ?>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/2_1/Modules/Reports/Resources/views/product_customers_print.blade.php ENDPATH**/ ?>