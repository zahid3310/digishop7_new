

<?php $__env->startSection('title', 'Project URL'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">ALL URLS</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Settings</a></li>
                                    <li class="breadcrumb-item active">ALL URLS</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('urls_update')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                <?php echo e(csrf_field()); ?>


                                    <div data-repeater-list="inner-group" class="inner form-group mb-0 row">
                                        <div class="inner col-lg-12 ml-md-auto input_fields_wrap getMultipleRow">

                                            <?php if((!empty($urls)) && ($urls->count() > 0)): ?>
                                            <?php $__currentLoopData = $urls; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_<?php echo e($key); ?>"> 
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                                        <?php if($key == 0): ?>
                                                            <label class="hidden-xs" for="productname">Name *</label>
                                                        <?php endif; ?>
                                                        <label style="display: none" class="show-xs" for="productname">Name *</label>
                                                        <input type="text" name="name[]" class="inner form-control" id="name_<?php echo e($key); ?>" value="<?php echo e($value['name']); ?>" required />
                                                    </div>

                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                                        <?php if($key == 0): ?>
                                                            <label class="hidden-xs" for="productname">Url *</label>
                                                        <?php endif; ?>
                                                        <label style="display: none" class="show-xs" for="productname">Url *</label>
                                                        <input type="text" name="url[]" class="inner form-control" id="url<?php echo e($key); ?>" value="<?php echo e($value['url']); ?>" required />
                                                    </div>

                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                                        <?php if($key == 0): ?>
                                                            <label class="hidden-xs" for="productname">Module *</label>
                                                        <?php endif; ?>
                                                        <label style="display: none" class="show-xs" for="productname">Module *</label>
                                                        <select id="module_id_<?php echo e($key); ?>" style="width: 100%" class="form-control select2" name="module_id[]">
                                                            <option value="">--Select Module--</option>
                                                            <?php if(!empty($modules)): ?>
                                                                <?php $__currentLoopData = $modules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $module): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option <?php echo e($value['module_id'] == $module['id'] ? 'selected' : ''); ?> value="<?php echo e($module->id); ?>"><?php echo e($module->name); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <?php endif; ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-lg-1 col-md-1 col-sm-12 col-12 form-group remove_field" data-val="<?php echo e($key); ?>">
                                                        <?php if($key == 0): ?>
                                                            <label class="hidden-xs" for="productname">Action</label>
                                                        <?php endif; ?>
                                                        <label style="display: none" class="show-xs" for="productname">Action</label>
                                                        <i class="btn btn-success btn-block bx bx-trash-alt font-size-20"></i>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-11 col-sm-11"></div>
                                        <div class="col-lg-1 col-md-2 col-sm-6 col-12 form-group">
                                            <div id="add_field_button" class="add_field_button">
                                                <i class="btn btn-success btn-block bx bx-plus font-size-20"></i>
                                            </div>
                                        </div>
                                    </div>

                                    <hr style="margin-top: 0px">

                                    <div class="form-group row">
                                        <div class="button-items col-md-12">
                                            <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="">Close</a></button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var entry_count  = <?php echo e($url_count); ?>;

            if (entry_count == 0)
            {
                $('#add_field_button').click();
            }
        });    
    </script>

    <script type="text/javascript">
        var max_fields       = 500;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x       = <?php echo e($url_count); ?>;
        $(add_button).click(function(e)
        {   
            e.preventDefault();

            if(x < max_fields)
            {   
                x++;

                if (x == 1)
                {
                    var name_label          = '<label class="hidden-xs" for="productname">Name *</label>\n';
                    var url_label           = '<label class="hidden-xs" for="productname">Url *</label>\n';
                    var module_label        = '<label class="hidden-xs" for="productname">Module *</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';
                }
                else
                {
                    var name_label          = '';
                    var url_label           = '';
                    var module_label        = '';
                    var action_label        = '';
                }

                $('.getMultipleRow').append(' ' + '<div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_'+x+'">' +
                                                    '<div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Name *</label>\n' +
                                                        name_label +
                                                        '<input type="text" name="name[]" class="inner form-control" id="name_'+x+'" placeholder="Name" required />\n' +
                                                    '</div>\n' +

                                                    '<div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Url *</label>\n' +
                                                        url_label +
                                                        '<input type="text" name="url[]" class="inner form-control" id="url_'+x+'" placeholder="URL" required />\n' +
                                                    '</div>\n' +

                                                    '<div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Module *</label>\n' +
                                                        module_label +
                                                        '<select id="module_id_'+x+'" style="width: 100%" class="form-control single_select2" name="module_id[]">\n' +
                                                            '<option value="">--Select Module--</option>\n' +
                                                            '<?php if(!empty($modules)): ?>' +
                                                                '<?php $__currentLoopData = $modules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $module): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>' +
                                                                '<option value="<?php echo e($module->id); ?>"><?php echo e($module->name); ?></option>\n' +
                                                                '<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>' +
                                                            '<?php endif; ?>' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div class="col-lg-1 col-md-1 col-sm-12 col-12 form-group remove_field" data-val="'+x+'">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i class="btn btn-success btn-block bx bx-trash-alt font-size-20"></i>\n' + 
                                                    '</div>\n' + 
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/2_1/Modules/AccessLevel/Resources/views/urls_edit.blade.php ENDPATH**/ ?>