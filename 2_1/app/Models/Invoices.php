<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    protected $table = "invoices";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function majorCategory()
    {
        return $this->belongsTo('App\Models\Categories','major_category_id');
    }

    public function invoiceEntries()
    {
        return $this->hasMany(InvoiceEntries::class, "invoice_id");
    }
}
