<?php

namespace Modules\Products\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Units;
use App\Models\BillEntries;
use App\Models\Branches;
use App\Models\ProductVariations;
use App\Models\ProductVariationValues;
use App\Models\ProductVariationEntries;
use App\Models\ProductSuppliers;
use App\Models\ProductCustomers;
use DB;
use Response;
use App\Models\LotNumbers;
use App\Imports\ProductsImport;
use Maatwebsite\Excel\Facades\Excel;

class ProductsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $product            = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.generic_name) as generic_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.sell_price) as sell_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                 GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                 GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                 GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                 GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                 GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                 GROUP_CONCAT(DISTINCT products.id) as product_id
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product_id         = array_values($product->sortByDesc('product_code')->take(1)->toArray());

        $product            = $product->sortBy('name')->all();
        $products           = collect($product);
        $units              = Units::orderBy('id', 'ASC')
                                    ->get();
        $variations         = ProductVariations::orderBy('id', 'ASC')->get();

        return view('products::index', compact('products', 'product_id', 'units', 'variations'));
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_category_id'   => 'required',
            'buying_price'          => 'required',
            'selling_price'         => 'required',
            'product_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $data_find                  = ProductEntries::orderBy('id', 'DESC')
                                                        ->first();

            $code                       = $data_find != null ? $data_find['product_code'] + 1 : 1;

            $product                    = new ProductEntries;
            $product->product_id        = $data['product_category_id'];
            $product->brand_id          = $data['brand_id'];
            $product->product_location  = $data['product_location'];
            $product->name              = $data['product_name'];
            $product->product_code      = $code;
            $product->sell_price        = $data['selling_price'];
            $product->buy_price         = $data['buying_price'];
            $product->generic_name      = $data['generic_name'];

            if ($data['unit_id'] != null)
            {
                $product->unit_id       = $data['unit_id'];
            }

            if (($data['stock_quantity'] != null) && ($data['stock_quantity'] > 0))
            {
                $product->stock_in_hand   = $data['stock_quantity'];
                $product->opening_stock   = $data['stock_quantity'];
            }

            // $product->tax_type                      = $data['tax_type'];
            // $product->selling_price_exclusive_tax   = $data['selling_price_exclusive_tax'];
            // $product->vat_percentage                = $data['vat_percentage'];
            // $product->service_charge                = $data['service_charge'];
            $product->status                        = $data['status'];
            $product->alert_quantity                = $data['alert_quantity'];
            $product->product_type                  = $data['product_type'];
            $product->created_by                    = $user_id;

            if ($product->save())
            {   
                if ($data['product_type'] == 2)
                {
                    foreach ($data['variation_id'] as $key => $value)
                    {   
                        if (($value != null) && ($data['variation_value'] != null))
                        {
                            $variation_values[] = [
                                'product_entry_id'      => $product['id'],
                                'variation_id'          => $value,
                                'variation_value_id'    => $data['variation_value'][$key],
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];
                        }
                    }

                    DB::table('product_variation_entries')->insert($variation_values);
                }

                if ($data['stock_quantity'] > 0)
                {
                    $find_lot_number = LotNumbers::orderBy('id', 'DESC')->first();
                    $lot_number      = $find_lot_number != null ? $find_lot_number['lot_number'] + 1 : 1;

                    $lot_entries[] = [
                        'lot_number'        => $lot_number,
                        'product_id'        => $data['product_category_id'],
                        'product_entry_id'  => $product->id,
                        'quantity'          => $data['stock_quantity'],
                        'expire_date'       => date('Y-m-d', strtotime($data['expire_date'])),
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];

                    DB::table('lot_numbers')->insert($lot_entries);
                }
                
                DB::commit();
                return back()->with("success","Product Added Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $product            = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.generic_name) as generic_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_location) as product_location,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.sell_price) as sell_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                 GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                 GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                 GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                 GROUP_CONCAT(DISTINCT products.id) as product_id
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();


        $product            = $product->sortBy('name')->all();
        $products           = collect($product);

        $product_entries    = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_location) as product_location,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.generic_name) as generic_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.sell_price) as sell_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                 GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                 GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                 GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                 GROUP_CONCAT(DISTINCT products.id) as product_id
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->find($id);


        $entry_count        = $product_entries->count();
        $product_id         = ProductEntries::select('product_entries.*')
                                    ->orderBy('product_entries.product_code', 'DESC')
                                    ->first();

        $units              = Units::orderBy('id', 'ASC')
                                    ->get();

        $variation_entries  = ProductVariationEntries::where('product_entry_id', $id)
                                                        ->orderBy('id', 'ASC')
                                                        ->get();

        $varia_entry_count  = $variation_entries->count();
        $variations         = ProductVariations::orderBy('id', 'ASC')->get();
        $variation_values   = ProductVariationValues::orderBy('id', 'ASC')->get();
        $find_lot           = LotNumbers::where('product_entry_id', $id)->whereNull('bill_id')->first();

        return view('products::edit', compact('products', 'product_entries', 'entry_count', 'product_id', 'units', 'variation_entries', 'varia_entry_count', 'variations', 'variation_values', 'find_lot'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_category_id'   => 'required',
            'buying_price'          => 'required',
            'selling_price'         => 'required',
            'product_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $product_lot                = ProductEntries::find($id);
            $product                    = ProductEntries::find($id);
            $product->product_id        = $data['product_category_id'];
            $product->brand_id          = $data['brand_id'];
            $product->product_location  = $data['product_location'];
            $product->name              = $data['product_name'];
            $product->sell_price        = $data['selling_price'];
            $product->buy_price         = $data['buying_price'];
            $product->generic_name      = $data['generic_name'];

            if ($data['unit_id'] != null)
            {
                $product->unit_id       = $data['unit_id'];
            }

            $product->stock_in_hand     = ($product_lot['stock_in_hand'] - $product_lot['opening_stock']) + $data['stock_quantity'];
            $product->opening_stock     = $data['stock_quantity'];

            // $product->tax_type                      = $data['tax_type'];
            // $product->selling_price_exclusive_tax   = $data['selling_price_exclusive_tax'];
            // $product->vat_percentage                = $data['vat_percentage'];
            // $product->service_charge                = $data['service_charge'];
            $product->status                        = $data['status'];
            $product->alert_quantity                = $data['alert_quantity'];
            $product->product_type                  = $data['product_type'];
            $product->created_by                    = $user_id;

            if ($product->save())
            {   
                $find_product_variation_entries = ProductVariationEntries::where('product_entry_id', $product['id'])->delete();
                
                if ($data['product_type'] == 2)
                {   
                    foreach ($data['variation_id'] as $key => $value)
                    {   
                        if (($value != null) && ($data['variation_value'] != null))
                        {
                            $variation_values[] = [
                                'product_entry_id'      => $product['id'],
                                'variation_id'          => $value,
                                'variation_value_id'    => $data['variation_value'][$key],
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];
                        }
                    }

                    DB::table('product_variation_entries')->insert($variation_values);
                }

                $find_lot = LotNumbers::where('product_entry_id', $product->id)->whereNull('bill_id')->first();

                if (($find_lot == null) && ($data['stock_quantity'] > 0)) 
                {
                    $find_lot_number = LotNumbers::orderBy('id', 'DESC')->first();
                    $lot_number      = $find_lot_number != null ? $find_lot_number['lot_number'] + 1 : 1;

                    $lot_entries[] = [
                        'lot_number'        => $lot_number,
                        'product_id'        => $data['product_category_id'],
                        'product_entry_id'  => $product->id,
                        'quantity'          => $data['stock_quantity'],
                        'expire_date'       => date('Y-m-d', strtotime($data['expire_date'])),
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];

                    DB::table('lot_numbers')->insert($lot_entries);
                }

                if ($find_lot != null)
                { 
                    $find_lot->quantity    = $data['stock_quantity'];
                    $find_lot->expire_date = date('Y-m-d', strtotime($data['expire_date']));
                    $find_lot->save();
                }

                DB::commit();
                return redirect()->route('products_index')->with("success","Product Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function barcodePrint()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $bill_id            = isset($_GET['bill_id']) ? $_GET['bill_id'] : 0;

        $product_entries    = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                 GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                 GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product            = $product_entries->sortBy('name')->all();
        $product_entries    = collect($product);

        $bill_entries       = BillEntries::where('bill_entries.bill_id', $bill_id)
                                    ->select('bill_entries.*')
                                    ->get();

        $entry_count        = $bill_entries->count();

        $user_info          = userDetails();

        return view('products::barcodes', compact('product_entries', 'user_info', 'bill_entries', 'bill_id', 'entry_count'));
    }

    public function barcodePrintPrint(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_entry_id.*'  => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $product_entry_id   = $request->product_entry_id;
        $quantity           = $request->quantity;
        $print_type         = $request->type_id;
        $label_size         = $request->label_size;
        $margin             = 'margin-left: '.$request->margin.'px'.';margin-right: '.$request->margin.'px';
        $user_info          = userDetails();

        foreach ($request->product_entry_id as $key => $value)
        {
            $product_entries    = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->where('product_entries.id', $value)
                                    ->orderBy('product_entries.total_sold', 'DESC')
                                    ->select('product_entries.*', 'products.name as product_name')
                                    ->first();

            $data[$product_entries['id']]['product_name_show']      = isset($request->product_name) ? 1 : 0;
            $data[$product_entries['id']]['organization_name_show'] = isset($request->organization_name) ? 1 : 0;
            $data[$product_entries['id']]['sell_price_show']        = isset($request->product_price) ? 1 : 0;
            $data[$product_entries['id']]['name']                   = $product_entries['name'];
            $data[$product_entries['id']]['organization_name']      = $user_info['organization_name'];
            $data[$product_entries['id']]['product_code']           = $product_entries['product_code'];
            $data[$product_entries['id']]['sell_price']             = $product_entries['sell_price'];
            $data[$product_entries['id']]['quantity']               = $quantity[$key];
        }

        return view('products::barcodes_print', compact('product_entries', 'data', 'print_type', 'user_info', 'label_size', 'margin'));
    }

    public function barcodePrintAjax($product_id, $product_entry_id)
    {
        $tables             = TableNameByUsers();
        $table_id           = Auth::user()->associative_contact_id;

        $product_entries    = $tables['product_entries']
                                    ->where('product_entries_'.$table_id.'.product_id', $product_id)
                                    ->orderBy('product_entries_'.$table_id.'.total_sold', 'DESC')
                                    ->select('product_entries_'.$table_id.'.*')
                                    ->get();

        $product_entries->sortBy('name');


        return Response::json($product_entries);
    }

    public function productListAjax()
    {
        $user_id            = Auth::user()->id;
        $user_branch_id     = Auth::user()->branch_id;
        $user_role          = Auth::user()->role;

        $product_entries    = ProductEntries::where('product_entries.product_id', '!=', 1)->get();
        $product_entries->sortBy('name');

        return Response::json($product_entries);
    }

    public function producCategorytList()
    {
        $user_id            = Auth::user()->id;
        $user_branch_id     = Auth::user()->branch_id;
        $user_role          = Auth::user()->role;

        $data               = Products::orderBy('products.total_sold', 'DESC')
                                    ->where('products.id', '!=', 1)
                                    ->select('products.*')
                                    ->get();

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData      = Products::where('products.id', '!=', 1)
                                    ->orderBy('products.total_sold', 'DESC')
                                    ->take(50)
                                    ->get();
        }
        else
        { 
            $search         = $_GET['searchTerm'];   
            $fetchData      = Products::where('products.name', 'LIKE', "%$search%")
                                    ->where('products.id', '!=', 1)
                                    ->orderBy('products.total_sold', 'DESC')
                                    ->take(50)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[] = array("id"=>$value['id'], "text"=>$value['name']);

            $i++;
        }

        return Response::json($data);
    }

    public function productBrandList()
    {
        $user_id            = Auth::user()->id;
        $user_branch_id     = Auth::user()->branch_id;
        $user_role          = Auth::user()->role;

        $data               = Categories::orderBy('categories.created_at', 'DESC')
                                            ->select('categories.*')
                                            ->get();

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData      = Categories::orderBy('categories.created_at', 'DESC')
                                            ->take(50)
                                            ->get();
        }
        else
        { 
            $search         = $_GET['searchTerm'];   
            $fetchData      = Categories::where('categories.name', 'LIKE', "%$search%")
                                            ->orderBy('categories.created_at', 'DESC')
                                            ->take(50)
                                            ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[] = array("id"=>$value['id'], "text"=>$value['name']);

            $i++;
        }

        return Response::json($data);
    }

    public function productList()
    {
        if(!isset($_GET['searchTerm']))
        {   
            $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                        ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                        ->where('product_entries.product_id', '!=', 1)
                                        ->groupBy('product_entries.id')
                                        ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                     GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                     GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                     GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                    ')
                                        ->orderBy('product_entries.product_id', 'ASC')
                                        ->take(10)
                                        ->get();

            $product            = $data->sortBy('name')->all();
            $fetchData          = collect($product);
        }
        else
        { 
            $search     = $_GET['searchTerm'];

            $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                        ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                        ->where('product_entries.product_id', '!=', 1)
                                        ->where('product_entries.name', 'LIKE', "%$search%")
                                        ->groupBy('product_entries.id')
                                        ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                     GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                     GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                     GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                    ')
                                        ->orderBy('product_entries.product_id', 'ASC')
                                        ->take(10)
                                        ->get();

            $product            = $data->sortBy('name')->all();
            $fetchData          = collect($product);
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($value['variations'] != null)
            {
                $variations = ' - ' . $value['variations'];
            }
            else
            {
                $variations = '';
            }

            $product_name   = $value['name'] . $variations;

            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[]     = array("id"=>$value['id'], "text"=>$product_name);

            $i++;
        }
   
        return Response::json($data);
    }

    public function productStoreProduct(Request $request)
    {
        $user_id                = Auth::user()->id;
        $data                   = $request->all();

        $products               = new Products;
        $products->name         = $data['product_name'];
        $products->status       = 1;
        $products->created_by   = $user_id;

        if ($products->save())
        {   
            return Response::json($products);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function productStoreBrand(Request $request)
    {
        $user_id                = Auth::user()->id;
        $data                   = $request->all();

        $brand                  = new Categories;
        $brand->name            = $data['brand_name'];
        $brand->status          = 1;
        $brand->created_by      = $user_id;

        if ($brand->save())
        {   
            return Response::json($brand);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function openingStock()
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End
        // $code_from          = isset($_GET['code_from']) ? $_GET['code_from'] : 0;
        // $code_to            = isset($_GET['code_to']) ? $_GET['code_to'] : 0;

        $product_entries    = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->whereNull('product_entries.stock_in_hand')
                                    // ->whereBetween('product_entries.product_code', [$code_from, $code_to])
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                 GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                 GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product            = $product_entries->sortBy('name')->all();
        $product_entries    = collect($product);

        return view('products::opening_stock', compact('product_entries'));
    }

    public function storeOpeningStock(Request $request)
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End

        $rules = array(
            'product_entry_id'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            foreach ($data['stock_in_hand'] as $key => $value)
            {   
                if ($value != null)
                {
                    $product_entry                       = ProductEntries::find($data['product_entry_id'][$key]);
                    $product_entry->stock_in_hand        = $value;
                    $product_entry->opening_stock        = $value;
                    $product_entry->updated_by           = $user_id;
                    $product_entry->save();
                }
            }

            return back()->with("success","Stock Updated Successfully !!");

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function categoryIndex()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_role          = Auth::user()->role;
        $products           = Products::where('products.id', '!=', 1)->orderBy('products.id', 'DESC')->get();

        $units              = Units::orderBy('id', 'ASC')->get();

        return view('products::category_index', compact('products', 'units'));
    }

    public function categoryStore(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $product                    = new Products;
            $product->name              = $data['product_name'];
            $product->status            = $data['status'];
            $product->created_by        = $user_id;
            
            if ($product->save())
            {
                return redirect()->route('products_category_index')->with("success","Product Category Created Successfully !!");
            }

        }catch (\Exception $exception){
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function categoryEdit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $products           = Products::where('products.id', '!=', 1)
                                        ->orderBy('products.id', 'DESC')
                                        ->get();

        $find_product       = Products::find($id);
        $units              = Units::orderBy('id', 'ASC')
                                        ->get();

        return view('products::category_edit', compact('products', 'find_product', 'units'));
    }

    public function categoryUpdate(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $product                    = Products::find($id);
            $product->name              = $data['product_name'];
            $product->status              = $data['status'];
            $product->updated_by        = $user_id;
            
            if ($product->save())
            {
                return redirect()->route('products_category_index')->with("success","Product Category Updated Successfully !!");
            }

        }catch (\Exception $exception){
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function unitsIndex()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $units              = Units::select('units.*')
                                    ->orderBy('units.id', 'DESC')
                                    ->get();

        return view('products::units_index', compact('units'));
    }

    public function unitsstore(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $table                    = new Units;
            $table->name              = $data['name'];
            $table->created_by        = $user_id;
            
            if ($table->save())
            {
                return redirect()->route('products_units_index')->with("success","Unit Created Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function unitsEdit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $units              = Units::select('units.*')
                                    ->orderBy('units.id', 'DESC')
                                    ->get();

        $find_unit          = Units::find($id);

        return view('products::units_edit', compact('find_unit', 'units'));
    }

    public function unitsUpdate(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $product                    = Units::find($id);
            $product->name              = $data['name'];
            $product->updated_by        = $user_id;
            
            if ($product->save())
            {
                return redirect()->route('products_units_index')->with("success","Unit Updated Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function variationValuesListLoad($id)
    {
        $data    = ProductVariationValues::where('product_variation_id', $id)->get();

        return Response::json($data);
    }

    //Product Variations
    public function variationsIndex()
    {
        $variations     = ProductVariations::leftjoin('product_variation_values', 'product_variation_values.product_variation_id', 'product_variations.id')
                                    ->groupBy('product_variations.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_variations.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_variations.name) as name,
                                                 GROUP_CONCAT(product_variation_values.name SEPARATOR ", ") as product_variation_values
                                                ')
                                    ->orderBy('product_variations.id', 'DESC')
                                    ->get();

        return view('products::variation_index', compact('variations'));
    }

    public function variationsstore(Request $request)
    {
        $rules = array(
            'name'              => 'required',
            'value_name.*'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $product_variation               = new ProductVariations;
            $product_variation->name         = $data['name'];
            $product_variation->created_by   = $user_id;
            
            if ($product_variation->save())
            {   
                foreach ($data['value_name'] as $key => $value)
                {
                    $variation_values[] = [
                        'product_variation_id'      => $product_variation['id'],
                        'name'                      => $value,
                        'created_by'                => $user_id,
                        'created_at'                => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('product_variation_values')->insert($variation_values);

                DB::commit();
                return redirect()->route('products_variations_index')->with("success","Variation Created Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function variationsEdit($id)
    {
        $variations                 = ProductVariations::leftjoin('product_variation_values', 'product_variation_values.product_variation_id', 'product_variations.id')
                                                        ->groupBy('product_variations.id')
                                                        ->selectRaw('GROUP_CONCAT(DISTINCT product_variations.id) as id,
                                                                     GROUP_CONCAT(DISTINCT product_variations.name) as name,
                                                                     GROUP_CONCAT(product_variation_values.name SEPARATOR ", ") as product_variation_values
                                                                    ')
                                                        ->orderBy('product_variations.id', 'DESC')
                                                        ->get();

        $find_variation             = ProductVariations::find($id);
        $variation_values           = ProductVariationValues::where('product_variation_id', $id)->get();
        $variation_values_count     = $variation_values->count();

        return view('products::variation_edit', compact('variations', 'find_variation', 'variation_values', 'variation_values_count'));
    }

    public function variationsUpdate(Request $request, $id)
    {
        $rules = array(
            'name'              => 'required',
            'value_name.*'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $product_variation               = ProductVariations::find($id);
            $product_variation->name         = $data['name'];
            $product_variation->updated_by   = $user_id;
            
            if ($product_variation->save())
            {   
                foreach ($data['value_name'] as $key => $value)
                {   
                    if ($data['type'][$key] == 0)
                    {
                        $find_old_data                = ProductVariationValues::find($data['value_ids'][$key]);
                        $find_old_data->name          = $value;
                        $find_old_data->updated_by    = $user_id;
                        $find_old_data->save();
                    }

                    if ($data['type'][$key] == 1)
                    {
                        $variation_values[] = [
                            'product_variation_id'      => $product_variation['id'],
                            'name'                      => $value,
                            'created_by'                => $user_id,
                            'created_at'                => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                if (isset($variation_values))
                {
                    DB::table('product_variation_values')->insert($variation_values);
                }

                DB::commit();
                return redirect()->route('products_variations_index')->with("success","Variation Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function getSupplierList($product_id)
    {
        $suppliers    = ProductSuppliers::leftjoin('customers', 'customers.id', 'product_suppliers.supplier_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'product_suppliers.product_entry_id')
                                        ->where('product_suppliers.product_entry_id', $product_id)
                                        ->selectRaw('product_suppliers.*, customers.name as supplier_name, product_entries.name as product_name')
                                        ->get();

        return Response::json($suppliers);
    }

    public function supplierListUpdate(Request $request)
    {
        $rules = array(
            'product_id'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id      = Auth::user()->id;
        $data         = $request->all();

        DB::beginTransaction();

        try{
            $data_delete  = ProductSuppliers::where('product_entry_id', $data['product_id'])->delete();

            foreach ($data['sup_id'] as $key => $value)
            {
                $suppliers[] = [
                    'product_entry_id'     => $data['product_id'],
                    'supplier_id'          => $data['sup_id'][$key],
                    'whole_sale_price'     => $data['whole_rate'][$key],
                    'retail_price'         => $data['retail_rate'][$key],
                    'created_by'           => $user_id,
                    'created_at'           => date('Y-m-d H:i:s'),
                ];
            }

            if (isset($suppliers))
            {
                DB::table('product_suppliers')->insert($suppliers);

                DB::commit();

                return redirect()->route('products_index')->with("success","Suppliers List Updated Successfully !!");
            }

            DB::rollback();
            return redirect()->route('products_index')->with("unsuccess","Not Updated !!");

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function getCustomerList($product_id)
    {
        $customers      = ProductCustomers::leftjoin('customers', 'customers.id', 'product_customers.customer_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'product_customers.product_entry_id')
                                        ->where('product_customers.product_entry_id', $product_id)
                                        ->selectRaw('product_customers.*, customers.name as supplier_name, product_entries.name as product_name')
                                        ->get();

        return Response::json($customers);
    }

    public function customerListUpdate(Request $request)
    {
        $rules = array(
            'product_id_customer'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id      = Auth::user()->id;
        $data         = $request->all();

        DB::beginTransaction();

        try{
            $data_delete  = ProductCustomers::where('product_entry_id', $data['product_id_customer'])->delete();

            foreach ($data['cus_id'] as $key => $value)
            {
                $customers[] = [
                    'product_entry_id'     => $data['product_id_customer'],
                    'customer_id'          => $data['cus_id'][$key],
                    'whole_sale_price'     => $data['whole_rate'][$key],
                    'retail_price'         => $data['retail_rate'][$key],
                    'created_by'           => $user_id,
                    'created_at'           => date('Y-m-d H:i:s'),
                ];
            }

            if (isset($customers))
            {
                DB::table('product_customers')->insert($customers);

                DB::commit();

                return redirect()->route('products_index')->with("success","Customers List Updated Successfully !!");
            }

            DB::rollback();
            return redirect()->route('products_index')->with("unsuccess","Not Updated !!");

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function importProductList(Request $request)
    {
        $rows   = Excel::toArray(new ProductsImport, $request->file('import_excel')); 

        DB::beginTransaction();

        try{
            foreach ($rows[0] as $key => $value)
            {   
                if ($key != 0)
                {
                    $name           = preg_replace('/[^(\x20-\x7F)]*/','', $value[1]);
                    $generic_name   = preg_replace('/[^(\x20-\x7F)]*/','', $value[2]);
                    $category_name  = preg_replace('/[^(\x20-\x7F)]*/','', $value[3]);
                    $self_name      = preg_replace('/[^(\x20-\x7F)]*/','', $value[4]);
                    $brand_name     = preg_replace('/[^(\x20-\x7F)]*/','', $value[5]);
                    $pack_size      = preg_replace('/[^(\x20-\x7F)]*/','', $value[6]);
                    $stock          = $value[7];
                    $unit_name      = preg_replace('/[^(\x20-\x7F)]*/','', $value[8]);
                    $buy_price      = $value[9];
                    $sell_price     = $value[10];

                    $find_category  = Products::where('name', $category_name)->first();
                    $find_brand     = Categories::where('name', $brand_name)->first();
                    $find_unit      = Units::where('name', $unit_name)->first();


                    if ($find_category != null)
                    {
                        $category_id  = $find_category->id;
                    }
                    else
                    {
                        $product_category               = new Products;
                        $product_category->name         = $category_name;
                        $product_category->status       = 1;
                        $product_category->created_by   = Auth::user()->id;
                        $product_category->save();

                        $category_id  = $product_category->id;
                    }

                    if ($find_brand != null)
                    {
                        $brand_id  = $find_brand->id;
                    }
                    else
                    {
                        $product_brand               = new Categories;
                        $product_brand->name         = $brand_name;
                        $product_brand->created_by   = Auth::user()->id;
                        $product_brand->status       = 1;
                        $product_brand->save();

                        $brand_id  = $product_brand->id;
                    }

                    if ($find_unit != null)
                    {
                        $unit_id  = $find_unit->id;
                    }
                    else
                    {
                        $product_unit               = new Units;
                        $product_unit->name         = $unit_name;
                        $product_unit->created_by   = Auth::user()->id;
                        $product_unit->save();

                        $unit_id  = $product_unit->id;
                    }

                    $data_find                  = ProductEntries::orderBy('id', 'DESC')
                                                            ->first();

                    $code                       = $data_find != null ? $data_find['product_code'] + 1 : 1;

                    $product_entry                    = new ProductEntries;
                    $product_entry->product_id        = $category_id;
                    $product_entry->brand_id          = $brand_id;
                    $product_entry->name              = $name;
                    $product_entry->generic_name      = $generic_name;
                    $product_entry->product_code      = $code;
                    $product_entry->buy_price         = $buy_price;
                    $product_entry->sell_price        = $sell_price;
                    $product_entry->unit_id           = $unit_id;
                    $product_entry->status            = 1;
                    $product_entry->alert_quantity    = $stock;
                    $product_entry->product_type      = 1;
                    $product_entry->created_by        = Auth::user()->id;
                    $product_entry->save();
                }
            }

            DB::commit();
            return back()->with("success","Product Added Successfully !!");

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function productListLoadAjax()
    {
        $product            = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.generic_name) as generic_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.sell_price) as sell_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                 GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                 GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                 GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                 GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                 GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                 GROUP_CONCAT(DISTINCT products.id) as product_id
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->take(100)
                                    ->get();

        $product            = $product->sortBy('name')->all();
        $products           = collect($product);

        return Response::json($products);
    }

    public function productListSearch($product_name, $product_code, $product_manufacturer, $product_category)
    {
        $search_by_name     = $product_name != 0 ? trim($product_name) : 0;
        $search_by_code     = $product_code != 0 ? ltrim($product_code, "0") : 0;
        $search_by_brand    = $product_manufacturer != 0 ? $product_manufacturer : 0;
        $search_by_category = $product_category != 0 ? $product_category : 0;

        $data           = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->when($search_by_name != 0, function ($query) use ($search_by_name) {
                                        return $query->where('product_entries.id', $search_by_name);
                                    })
                                    ->when($search_by_code != 0, function ($query) use ($search_by_code) {
                                        return $query->where('product_entries.product_code', $search_by_code);
                                    })
                                    ->when($search_by_brand != 0, function ($query) use ($search_by_brand) {
                                        return $query->where('product_entries.brand_id', $search_by_brand);
                                    })
                                    ->when($search_by_category != 0, function ($query) use ($search_by_category) {
                                        return $query->where('product_entries.product_id', $search_by_category);
                                    })
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.generic_name) as generic_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.sell_price) as sell_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                 GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                 GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                 GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                 GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                 GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                 GROUP_CONCAT(DISTINCT products.id) as product_id
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->take(100)
                                    ->get();
                                   
        return Response::json($data);
    }
}
