@extends('layouts.app')

@section('title', 'Show')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="padding: 10px;padding-top: 25px" class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if($user_info['header_image'] == null)
                                        <div class="col-md-2 col-xs-12 col-sm-12">
                                            <img style="width: 150px;height: 150px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                        </div>
                                        <div class="col-md-8 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 30px">{{ $user_info['organization_name'] }}</h2>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><span style="font-weight: bold">Mobile : </span>{{ $user_info['contact_number'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><span style="font-weight: bold">Email : </span>{{ $user_info['contact_email'] }}</p>
                                        </div>
                                        <div class="col-md-2 col-xs-12 col-sm-12"></div>
                                    @else
                                        <img class="float-left" src="{{ url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-4 col-6">
                                        <address>
                                            <strong>Billed To:</strong><br>
                                            <span style="font-weight: bold">Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </span>{{ $invoice['customer_name'] }}
                                            @if($invoice['address'] != null)
                                               <br> <span style="font-weight: bold">Address &nbsp;: </span> <?php echo $invoice['address']; ?> <br>
                                            @endif
                                            @if($invoice['address'] == null)
                                                <br>
                                            @endif
                                            <span style="font-weight: bold">Mobile &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </span>{{ $invoice['phone'] }}
                                        </address>
                                    </div>

                                    <div class="col-sm-4 hidden-xs">
                                        <address></address>
                                    </div>

                                    <div style="font-size: 16px" class="col-sm-4 col-6 text-sm-right">
                                        <address>
                                            <strong>Invoice Date:</strong><br>
                                            {{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}<br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6 hidden-xs">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Sales summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Sales # {{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }}</h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-nowrap">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th style="width: 70px;">No.</th>
                                                <th>Item</th>
                                                <th>Brand</th>
                                                <th class="text-right">Rate</th>
                                                <th class="text-right">Quantity</th>
                                                <th class="text-right">Discount</th>
                                                <th class="text-right">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">

                                            @if(!empty($entries) && ($entries->count() > 0))

                                            <?php 
                                                $sub_total = 0; 
                                                $total_qty = 0; 
                                            ?>

                                            @foreach($entries as $key => $value)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>
                                                    @if($value['product_type'] == 1)
                                                        <?php echo $value['product_entry_name']; ?>
                                                    @else
                                                        <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                                    @endif
                                                </td>
                                                <td>{{ $value->productEntries->brand_id != null ? $value->productEntries->brand->name :'' }}</td>
                                                <td class="text-right">{{ number_format($value['rate'],2,'.',',') }}</td>
                                                <td class="text-right">{{ number_format($value['quantity'],2,'.',',') . ' '  }} {{ $value->productEntries->unit_id != null ? $value->productEntries->unit->name :'' }}</td>
                                                <td class="text-right">
                                                    <?php $total_dis = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; ?>
                                                    @if($value['discount_type'] == 0)
                                                        <?php echo number_format($total_dis,2,'.',','); ?><br>
                                                        <?php echo '('.$value['discount_amount'].'%'.')'; ?>
                                                    @else
                                                        <?php echo number_format($value['discount_amount'],2,'.',','); ?>
                                                    @endif
                                                </td>
                                                <td class="text-right">{{ number_format($value['total_amount'],2,'.',',') }}</td>
                                            </tr>

                                            <?php 
                                                $sub_total = $sub_total + $value['total_amount'];
                                                $total_qty = $total_qty + $value['quantity']; 
                                            ?>

                                            @endforeach
                                            @endif
                                            
                                            <tr>
                                                <td style="font-size: 16px" class="text-right" colspan="3">Total Quantity : {{ $total_qty . ' PCS '}}</td>
                                                <td style="font-size: 16px;font-weight: bold" colspan="3" class="text-right">Sub Total</td>
                                                <td style="font-size: 16px;font-weight: bold" class="text-right">{{ number_format($sub_total,2,'.',',') }}</td>
                                            </tr>

                                            <tr>
                                                <td style="font-size: 16px;font-weight: bold" colspan="6" class="text-right">VAT ({{ $invoice['vat_type'] == 0 ? $invoice['total_vat'].'%' : 'BDT' }})</td>
                                                <td style="font-size: 16px;font-weight: bold" class="text-right">{{ $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : number_format($invoice['total_vat'],2,'.',',') }}</td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="6" class="border-0 text-right">
                                                    Discount On Total Amount ({{ $invoice['total_discount_type'] == 0 ? $invoice['total_discount_amount'].'%' : 'BDT' }})</td>
                                                <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">{{ $invoice['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$invoice['total_vat'])/100))*$invoice['total_discount_amount'])/100) : number_format($invoice['total_discount_amount'],2,'.',',') }}</td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="6" class="border-0 text-right">Total</td>
                                                <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                    {{ number_format($invoice['invoice_amount'],2,'.',',') }}
                                                </td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="6" class="border-0 text-right">Paid</td>
                                                <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                    {{ number_format($invoice['invoice_amount'] - $invoice['due_amount'],2,'.',',') }}
                                                </td>
                                            </tr>
                                            
                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="6" class="border-0 text-right">Dues</td>
                                                <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                    {{ number_format($invoice['due_amount'],2,'.',',') }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 style="font-size: 16px"><strong>Note :</strong> {{ $invoice['invoice_note'] }}</h6>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-8"></div>
                                    <div class="col-md-4">
                                        <img style="height: 50px;width: 150px;padding: 0px;float: right;margin-bottom: 10px" src="{{ url('public/'. userDetails()->signature) }}">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8"></div>
                                    <div class="col-md-4">
                                        <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">Proprietor Signature</span> </h6>
                                    </div>
                                </div>

                                <div style="padding-top: 30px" class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="text-align: center;">
                                    <div class="col-md-12">
                                        <p style="line-height: 18px;text-align: center;padding: 0px;font-size: 20px">বিশেষ দ্রষ্টব্য:ক্রয়কৃত পোশাক পরিবর্তনের জন্য অনুগ্রহপূর্বক ক্যাশ মেমো অবশ্যই সঙ্গে আনবেন।</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection