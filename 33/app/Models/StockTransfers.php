<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class StockTransfers extends Model
{  
    protected $table = "stock_transfers";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function transferFrom()
    {
        return $this->belongsTo('App\Models\Branches','transfer_from');
    }

    public function transferTo()
    {
        return $this->belongsTo('App\Models\Branches','transfer_to');
    }

    public function productEntries()
    {
        return $this->belongsTo('App\Models\ProductEntries','product_entry_id');
    }
}
