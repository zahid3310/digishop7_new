

<?php $__env->startSection('title', 'Edit Phone Book'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Phone Book</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Phone Book</a></li>
                                    <li class="breadcrumb-item active">Edit Contact</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <?php if(Auth::user()->role == 1): ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('messages_phone_book_update', $find_phone_books['id'])); ?>" method="post" files="true" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>


                                <div style="margin-bottom: 0px !important" class="form-group row">
                                    <div class="col-md-4 form-group">
                                        <label for="productname">Name *</label>
                                        <input type="text" name="name" class="inner form-control" id="name" value="<?php echo e($find_phone_books['name']); ?>" required />
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="productname">Phone *</label>
                                        <input type="text" name="phone" class="inner form-control" id="phone" value="<?php echo e($find_phone_books['phone']); ?>" required />
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="productname">Address</label>
                                        <input type="text" name="address" class="inner form-control" id="address" value="<?php echo e($find_phone_books['address']); ?>" />
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('messages_phone_book_index')); ?>">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
                <?php endif; ?>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Phone Contacts</h4>

                                <br>

                                <table id="datatable" class="table table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <?php if(!empty($phone_books) && ($phone_books->count() > 0)): ?>
                                        <?php $__currentLoopData = $phone_books; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $phone_book): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e($phone_book['name']); ?></td>
                                                <td><?php echo e($phone_book['phone']); ?></td>
                                                <td><?php echo e($phone_book['address']); ?></td>
                                                <td>
                                                    <?php if(Auth::user()->role == 1): ?>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="<?php echo e(route('messages_phone_book_edit', $phone_book['id'])); ?>">Edit</a>
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/33/live/Modules/Messages/Resources/views/phone_book_edit.blade.php ENDPATH**/ ?>