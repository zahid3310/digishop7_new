<?php

namespace Modules\Expenses\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\Bills;
use App\Models\InvoiceEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\ExpenseCategories;
use App\Models\Expenses;
use App\Models\Transactions;
use App\Models\Users;
use App\Models\PaidThroughAccounts;
use App\Models\AccountTransactions;
use DB;
use Response;

class ExpensesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        $expense_categories     = ExpenseCategories::where('expense_categories.id', '!=', 1)
                                                    ->where('expense_categories.id', '!=', 2)
                                                    ->get();

        $expenses               = Expenses::leftjoin('expense_categories', 'expense_categories.id', 'expenses.expense_category_id')
                                        ->orderBy('expenses.created_at', 'DESC')
                                        ->select('expenses.*',
                                         'expense_categories.name as category_name')
                                        ->get();

        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();

        return view('expenses::index', compact('expense_categories', 'expenses', 'paid_accounts'));
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('expenses::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'expense_date'          => 'required',
            'expense_category_id'   => 'required',
            'amount'                => 'required',
            'paid_through'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $data_find                      = Expenses::orderBy('created_at', 'DESC')->first();
            $expense_number                 = $data_find != null ? $data_find['expense_number'] + 1 : 1;
   
            $expense                        = new Expenses;
            $expense->expense_number        = $expense_number;
            $expense->expense_date          = date('Y-m-d', strtotime($data['expense_date']));
            $expense->expense_category_id   = $data['expense_category_id'];
            $expense->amount                = $data['amount'];
            $expense->paid_through_id       = $data['paid_through'];
            $expense->account_information   = $data['account_information'];
            $expense->note                  = $data['note'];
            $expense->created_by            = $user_id;

            if ($expense->save())
            {   
                $account_transaction                      = new AccountTransactions;
                $account_transaction->transaction_date    = date('Y-m-d', strtotime($data['expense_date']));
                $account_transaction->amount              = $data['amount'];
                $account_transaction->paid_through_id     = $data['paid_through'];
                $account_transaction->account_information = $data['account_information'];
                $account_transaction->note                = $data['note'];
                $account_transaction->type                = 0;
                $account_transaction->transaction_head    = 'expense';
                $account_transaction->associated_id       = $expense->id;
                $account_transaction->created_by          = $user_id;
                $account_transaction->save()

                DB::commit();
                return back()->with("success","Expense Created Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function categoryStore(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'expense_category_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $expense_categories              = new ExpenseCategories;
            $expense_categories->name        = $data['expense_category_name'];
            $expense_categories->created_by  = $user_id;

            if ($expense_categories->save())
            {   
                DB::commit();
                return back()->with("success","Category Created Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End 

        return view('expenses::show');
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $expense_categories     = ExpenseCategories::where('expense_categories.id', '!=', 1)
                                                    ->where('expense_categories.id', '!=', 2)
                                                    ->get();

        $expenses               = Expenses::leftjoin('expense_categories', 'expense_categories.id', 'expenses.expense_category_id')
                                                    ->orderBy('expenses.created_at', 'DESC')
                                                    ->select('expenses.*',
                                                             'expense_categories.name as category_name')
                                                    ->get();

        $expense_find           = Expenses::leftjoin('expense_categories', 'expense_categories.id', 'expenses.expense_category_id')
                                                    ->orderBy('expenses.created_at', 'DESC')
                                                    ->select('expenses.*',
                                                             'expense_categories.name as category_name',
                                                             'expense_categories.id as category_id')
                                                    ->find($id);

        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();

        return view('expenses::expense_edit', compact('expense_categories', 'expenses', 'expense_find', 'paid_accounts'));
    }

    public function categoryEdit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $expense_categories     = ExpenseCategories::where('expense_categories.id', '!=', 1)
                                                    ->where('expense_categories.id', '!=', 2)
                                                    ->get();
                                                    
        $expenses               = Expenses::leftjoin('expense_categories', 'expense_categories.id', 'expenses.expense_category_id')
                                                    ->orderBy('expenses.created_at', 'DESC')
                                                    ->select('expenses.*',
                                                     'expense_categories.name as category_name')
                                                    ->get();

        $expense_category_find  = ExpenseCategories::find($id);

        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();

        return view('expenses::expense_category_edit', compact('expense_categories', 'expenses', 'expense_category_find', 'paid_accounts'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'expense_date'          => 'required',
            'expense_category_id'   => 'required',
            'amount'                => 'required',
            'paid_through'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $expense                        = Expenses::find($id);
            $expense->expense_date          = date('Y-m-d', strtotime($data['expense_date']));
            $expense->expense_category_id   = $data['expense_category_id'];
            $expense->amount                = $data['amount'];
            $expense->paid_through_id       = $data['paid_through'];
            $expense->account_information   = $data['account_information'];
            $expense->note                  = $data['note'];
            $expense->updated_by            = $user_id;

            if ($expense->save())
            {   
                $delete_transaction                       = AccountTransactions::where('transaction_head', 'expense')->where('associated_id', $expense->id)->delete();
                $account_transaction                      = new AccountTransactions;
                $account_transaction->transaction_date    = date('Y-m-d', strtotime($data['expense_date']));
                $account_transaction->amount              = $data['amount'];
                $account_transaction->paid_through_id     = $data['paid_through'];
                $account_transaction->account_information = $data['account_information'];
                $account_transaction->note                = $data['note'];
                $account_transaction->type                = 0;
                $account_transaction->transaction_head    = 'expense';
                $account_transaction->associated_id       = $expense->id;
                $account_transaction->updated_by          = $user_id;
                $account_transaction->save();
                
                DB::commit();
                return redirect()->route('expenses_index')->with("success","Expense Updated Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function categoryUpdate(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'expense_category_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $expense_categories              = ExpenseCategories::find($id);
            $expense_categories->name        = $data['expense_category_name'];
            $expense_categories->updated_by  = $user_id;

            if ($expense_categories->save())
            {   
                DB::commit();
                return redirect()->route('expenses_index')->with("success","Category Updated Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function destroy($id)
    {
    }

    public function expenseListLoad()
    {
        $data           = Expenses::leftjoin('expense_categories', 'expense_categories.id', 'expenses.expense_category_id')
                                ->orderBy('expenses.created_at', 'DESC')
                                ->select('expenses.*',
                                         'expense_categories.name as category_name')
                                ->take(100)
                                ->get();

        return Response::json($data);
    }

    public function expenseListSearch($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data           = Expenses::leftjoin('expense_categories', 'expense_categories.id', 'expenses.expense_category_id')
                                ->where('expense_categories.name', 'LIKE', "%$id%")
                                ->orWhere('expenses.expense_date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number, $table_id) {
                                    return $query->orWhere('expenses.expense_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('expenses.created_at', 'DESC')
                                ->select('expenses.*',
                                         'expense_categories.name as category_name')
                                ->take(100)
                                ->get();
        }
        else
        {
            $data           = Expenses::leftjoin('expense_categories', 'expense_categories.id', 'expenses.expense_category_id')
                                ->orderBy('expenses.created_at', 'DESC')
                                ->select('expenses.*',
                                         'expense_categories.name as category_name')
                                ->take(100)
                                ->get();
        }
        
        return Response::json($data);
    }

    public function employeeSalaryIndex()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('expenses::employee_salary_index');
    }

    public function employeeSalaryStore(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'payment_date'          => 'required',
            'user_id'               => 'required',
            'amount'                => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $data_find                      = Expenses::orderBy('created_at', 'DESC')->first();
            $expense_number                 = $data_find != null ? $data_find['expense_number'] + 1 : 1;
   
            $expense                        = new Expenses;
            $expense->expense_number        = $expense_number;
            $expense->expense_date          = date('Y-m-d', strtotime($data['payment_date']));
            $expense->expense_category_id   = $data['expense_category_id'];
            $expense->amount                = $data['amount'];
            $expense->note                  = $data['note'];
            $expense->user_id               = $data['user_id'];
            $expense->salary_type           = $data['salary_type'];
            $expense->created_by            = $user_id;

            if ($expense->save())
            {
                DB::commit();
                return back()->with("success","Salary Paid Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function employeeSalaryEdit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $find_expense           = Expenses::leftjoin('customers', 'customers.id', 'expenses.user_id')
                                    ->select('expenses.*',
                                             'customers.name as user_name')
                                    ->find($id);

        return view('expenses::employee_salary_edit', compact('find_expense'));
    }

    public function employeeSalaryUpdate(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'payment_date'          => 'required',
            'user_id'               => 'required',
            'amount'                => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $expense                        = Expenses::find($id);
            $expense->expense_date          = date('Y-m-d', strtotime($data['payment_date']));
            $expense->expense_category_id   = $data['expense_category_id'];
            $expense->amount                = $data['amount'];
            $expense->note                  = $data['note'];
            $expense->user_id               = $data['user_id'];
            $expense->salary_type           = $data['salary_type'];
            $expense->created_by            = $user_id;

            if ($expense->save())
            {
                DB::commit();
                return redirect()->route('expenses_employee_salary_index')->with("success","Salary Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function employeeSalaryListLoad()
    {
        $data           = Expenses::leftjoin('expense_categories', 'expense_categories.id', 'expenses.expense_category_id')
                                ->leftjoin('customers', 'customers.id', 'expenses.user_id')
                                ->where('expenses.expense_category_id', 1)
                                ->orderBy('expenses.created_at', 'DESC')
                                ->select('expenses.*',
                                         'customers.name as user_name')
                                ->take(100)
                                ->get();

        return Response::json($data);
    }

    public function employeeSalaryListSearch($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data           = Expenses::leftjoin('expense_categories', 'expense_categories.id', 'expenses.expense_category_id')
                                    ->leftjoin('customers', 'customers.id', 'expenses.user_id')
                                    ->orWhere('expense_categories.name', 'LIKE', "%$id%")
                                    ->orWhere('customers.name', 'LIKE', "%$id%")
                                    ->orWhere('expenses.expense_date', 'LIKE', "%$search_by_date%")
                                    ->where('expenses.expense_category_id', 1)
                                    ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number, $table_id) {
                                        return $query->orWhere('expenses.expense_number', 'LIKE', "%$search_by_payment_number%");
                                    })
                                    ->orderBy('expenses.created_at', 'DESC')
                                    ->select('expenses.*',
                                             'customers.name as user_name')
                                    ->take(100)
                                    ->get();
        }
        else
        {
            $data           = Expenses::leftjoin('expense_categories', 'expense_categories.id', 'expenses.expense_category_id')
                                    ->leftjoin('customers', 'customers.id', 'expenses.user_id')
                                    ->where('expenses.expense_category_id', 1)
                                    ->orderBy('expenses.created_at', 'DESC')
                                    ->select('expenses.*',
                                             'customers.name as user_name')
                                    ->take(100)
                                    ->get();
        }
        
        return Response::json($data);
    }
}
