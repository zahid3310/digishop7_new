<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ProductVariationEntries extends Model
{  
    protected $table = "product_variation_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function VariationName()
    {
        return $this->belongsTo('App\Models\ProductVariations','variation_id');
    }

    public function VariationValue()
    {
        return $this->belongsTo('App\Models\ProductVariationValues','variation_value_id');
    }
}