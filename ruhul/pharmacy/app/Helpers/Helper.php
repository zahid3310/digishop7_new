<?php
//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\SalesReturn;
use App\Models\SalesReturnEntries;
use App\Models\PurchaseReturn;
use App\Models\PurchaseReturnEntries;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\ExpenseCategories;
use App\Models\Expenses;
use App\Models\Transactions;
use App\Models\Users;
use App\Models\Units;
use App\Models\PaidThroughAccounts;
use App\Models\Discounts;
use App\Models\DiscountProducts;
use App\Models\URLS;
use App\Models\Permissions;
use App\Models\Modules;
use App\Models\ModulesAccess;

function userAccess($user_id)
{
    $current_route      = Route::currentRouteName();

    $access             = Permissions::leftjoin('urls', 'urls.id', 'permissions.url_id')
                                ->where('permissions.user_id', $user_id)
                                ->where('urls.url', $current_route)
                                ->first();

    return $access['access_level'];
}

function moduleAccess($user_id)
{
    $access     = ModulesAccess::leftjoin('modules', 'modules.id', 'modules_access.module_id')
                                ->where('modules_access.user_id', $user_id)
                                ->get();

    return $access;
}

function stockOut($data, $item_id)
{
    //Here Item Id is used for update purpose
    if ($item_id != null)
    {
        foreach ($item_id as $key => $value)
        {
            $old_item_entry_id[]    = $value['product_entry_id'];
            $old_items_stock[]      = $value['quantity'];
        }

        foreach ($old_item_entry_id as $key2 => $value2)
        { 
            $quantity_add_to_product_entry                   = ProductEntries::find($value2);
            $quantity_add_to_product_entry->stock_in_hand    = $quantity_add_to_product_entry['stock_in_hand'] + $old_items_stock[$key2];
            $quantity_add_to_product_entry->total_sold       = $quantity_add_to_product_entry['total_sold'] - $old_items_stock[$key2];
            $quantity_add_to_product_entry->save();
        }
    }

    foreach ($data['product_entries'] as $key4 => $value4)
    {
        $product_entries                    = ProductEntries::find($value4);
        $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['quantity'][$key4];
        $product_entries->total_sold        = $product_entries['total_sold'] + $data['quantity'][$key4];
        $product_entries->save();
    }

    return 0;
}

function stockIn($data, $item_id)
{
    if ($item_id != null)
    {
        foreach ($item_id as $key => $value)
        {
            $old_item_entry_id[]    = $value['product_entry_id'];
            $old_items_stock[]      = $value['quantity'];
        }

        foreach ($old_item_entry_id as $key2 => $value2)
        { 
            $quantity_add_to_product_entry                   = ProductEntries::find($value2);
            $quantity_add_to_product_entry->stock_in_hand    = $quantity_add_to_product_entry['stock_in_hand'] - $old_items_stock[$key2];
            // $quantity_add_to_product_entry->buy_price        = $data['rate'][$key2];
            $quantity_add_to_product_entry->save();
        }
    }

    foreach ($data['product_entries'] as $key4 => $value4)
    {
        $product_entries                    = ProductEntries::find($value4);
        $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] + $data['quantity'][$key4];
        $product_entries->buy_price         = $data['rate'][$key4];
        $product_entries->sell_price        = $data['sell_price'][$key4];
        $product_entries->save();
    }

    return 0;
}

function stockInReturn($data, $item_id)
{
    foreach ($data['product_entries'] as $key4 => $value4)
    {
        $product_entries                    = ProductEntries::find($value4);
        $product_entries->total_sold        = $product_entries['total_sold'] - $data['return_quantity'][$key4];
        $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] + $data['return_quantity'][$key4];
        $product_entries->save();
    }

    return 0;
}

function stockOutReturn($data, $item_id)
{
    foreach ($data['product_entries'] as $key4 => $value4)
    {
        $product_entries                    = ProductEntries::find($value4);
        $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['return_quantity'][$key4];
        $product_entries->save();
    }

    return 0;
}

function createCustomer($data)
{
    $customer                       = new Customers;
    $customer->name                 = $data['customer_name'];
    $customer->phone                = $data['mobile_number'];
    $customer->address              = $data['address'];
    $customer->nid_number           = $data['nid_number'];
    $customer->alternative_contact  = $data['alternative_mobile_number'];
    $customer->contact_type         = $data['contact_type'];
    $customer->save();

    return $customer['id'];
}

function addPayment($data, $invoice_bill_id, $customer_id ,$type)
{
    $user_id                    = Auth::user()->id;

    $data_find                  = Payments::orderBy('created_at', 'DESC')->first();
    $payment_number             = $data_find != null ? $data_find['payment_number'] + 1 : 1;
    
    $payment                    = new Payments;
    $payment->payment_number    = $payment_number;
    $payment->customer_id       = $customer_id;
    $payment->payment_date      = date('Y-m-d', strtotime($data['selling_date']));
    $payment->amount            = $data['amount_paid'];
    $payment->paid_through      = $data['paid_through'];
    $payment->note              = $data['note'];
    $payment->type              = $type;
    $payment->created_by        = $user_id;

    if ($payment->save())
    {   
        if ($type == 0)
        {
            $payment_entries[] = [
                'invoice_id'        => $invoice_bill_id,
                'payment_id'        => $payment['id'],
                'amount'            => $data['amount_paid'],
                'created_by'        => $user_id,
                'created_at'        => date('Y-m-d H:i:s'),
            ];

            $transactions[] = [
                'customer_id'           => $customer_id,
                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                'invoice_id'            => $invoice_bill_id,
                'payment_id'            => $payment['id'],
                'paid_through'          => $payment['paid_through'],
                'type'                  => $type,
                'note'                  => $data['note'],
                'amount'                => $data['amount_paid'],
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $update_dues                = Invoices::find($invoice_bill_id);
            $update_dues->due_amount    = $update_dues['due_amount'] - $data['amount_paid'];
            $update_dues->save();
        }
        else
        {
            $payment_entries[] = [
                'bill_id'           => $invoice_bill_id,
                'payment_id'        => $payment['id'],
                'amount'            => $data['amount_paid'],
                'created_by'        => $user_id,
                'created_at'        => date('Y-m-d H:i:s'),
            ];

            $transactions[] = [
                'customer_id'           => $customer_id,
                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                'bill_id'               => $invoice_bill_id,
                'payment_id'            => $payment['id'],
                'paid_through'          => $payment['paid_through'],
                'type'                  => $type,
                'note'                  => $data['note'],
                'amount'                => $data['amount_paid'],
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $update_bill_dues                = Bills::find($invoice_bill_id);
            $update_bill_dues->due_amount    = $update_bill_dues['due_amount'] - $data['amount_paid'];
            $update_bill_dues->save();
        }

        DB::table('payment_entries')->insert($payment_entries);
        DB::table('transactions')->insert($transactions);

        return 0;
    }
}

function addPaymentReturn($data, $invoice_bill_id, $customer_id ,$type)
{
    $user_id                    = Auth::user()->id;

    $data_find                  = Payments::orderBy('created_at', 'DESC')->first();
    $payment_number             = $data_find != null ? $data_find['payment_number'] + 1 : 1;
    
    $payment                    = new Payments;
    $payment->payment_number    = $payment_number;
    $payment->customer_id       = $customer_id;
    $payment->payment_date      = date('Y-m-d', strtotime($data['return_date']));
    $payment->amount            = $data['amount_paid'];
    $payment->paid_through      = $data['paid_through'];
    $payment->note              = $data['note'];
    $payment->type              = $type;
    $payment->created_by        = $user_id;

    if ($payment->save())
    {   
        if ($type == 2)
        {
            $payment_entries[] = [
                'sales_return_id'   => $invoice_bill_id,
                'payment_id'        => $payment['id'],
                'amount'            => $data['amount_paid'],
                'created_by'        => $user_id,
                'created_at'        => date('Y-m-d H:i:s'),
            ];

            $transactions[] = [
                'customer_id'           => $customer_id,
                'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                'sales_return_id'       => $invoice_bill_id,
                'payment_id'            => $payment['id'],
                'paid_through'          => $payment['paid_through'],
                'type'                  => $type,
                'note'                  => $data['note'],
                'amount'                => $data['amount_paid'],
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $update_dues                = SalesReturn::find($invoice_bill_id);
            $update_dues->due_amount    = $update_dues['due_amount'] - $data['amount_paid'];
            $update_dues->save();

            // $find_invoice_id                    = $tables['sales_return_entries']->where('sales_return_id', $invoice_bill_id)->first();
            // $update_invoice_dues                = $tables['invoices']->find($find_invoice_id['invoice_id']);
            // $update_invoice_dues->return_amount = $update_invoice_dues['return_amount'] - $data['amount_paid'];
            // $update_invoice_dues->save();
        }

        if ($type == 3)
        {
            $payment_entries[] = [
                'purchase_return_id'    => $invoice_bill_id,
                'payment_id'            => $payment['id'],
                'amount'                => $data['amount_paid'],
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $transactions[] = [
                'customer_id'           => $customer_id,
                'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                'purchase_return_id'    => $invoice_bill_id,
                'payment_id'            => $payment['id'],
                'paid_through'          => $payment['paid_through'],
                'type'                  => $type,
                'note'                  => $data['note'],
                'amount'                => $data['amount_paid'],
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $update_return_dues              = PurchaseReturn::find($invoice_bill_id);
            $update_return_dues->due_amount  = $update_return_dues['due_amount'] - $data['amount_paid'];
            $update_return_dues->save();

            // $find_bill_id                    = $tables['purchase_return_entries']->where('purchase_return_id', $invoice_bill_id)->first();
            // $update_bill_dues                = $tables['bills']->find($find_bill_id['bill_id']);
            // $update_bill_dues->return_amount = $update_bill_dues['return_amount'] - $data['amount_paid'];
            // $update_bill_dues->save();
        }

        DB::table('payment_entries')->insert($payment_entries);
        DB::table('transactions')->insert($transactions);

        return 0;
    }
}

function userDetails()
{
    $user_id    = Auth::user()->id;

    $user       = Users::find(1);

    return $user;
}

function stockSellValue($product_id)
{
    $product_entries    = ProductEntries::where('product_entries.id', $product_id)
                                ->get();

    $stock_value   = 0;
    foreach ($product_entries as $key => $value)
    {
       $stock_value   = $stock_value + ($value['stock_in_hand']*$value['sell_price']);
    }

    return $stock_value;
}

function stockPurchaseValue($product_id)
{
    $product_entries    = ProductEntries::where('product_entries.id', $product_id)
                                ->get();

    $stock_value   = 0;
    foreach ($product_entries as $key => $value)
    {
       $stock_value   = $stock_value + ($value['stock_in_hand']*$value['buy_price']);
    }

    return $stock_value;
}

function TableNameByUsers()
{
    $table_id                                       = Auth::user()->associative_contact_id;

    //Create Table Name By Adding User ID At The End Of The Table Name
    $categories_table_name_by_users                 = 'categories_' . $table_id;
    $sub_categories_table_name_by_users             = 'sub_categories_' . $table_id;
    $items_table_name_by_users                      = 'items_' . $table_id;
    $products_table_name_by_users                   = 'products_' . $table_id;
    $product_entries_table_name_by_users            = 'product_entries_' . $table_id;
    $customers_table_name_by_users                  = 'customers_' . $table_id;
    $invoices_table_name_by_users                   = 'invoices_' . $table_id;
    $invoice_entries_table_name_by_users            = 'invoice_entries_' . $table_id;
    $sales_return_table_name_by_users               = 'sales_return_' . $table_id;
    $sales_return_entries_table_name_by_users       = 'sales_return_entries_' . $table_id;
    $purchase_return_table_name_by_users            = 'purchase_return_' . $table_id;
    $purchase_return_entries_table_name_by_users    = 'purchase_return_entries_' . $table_id;
    $bills_table_name_by_users                      = 'bills_' . $table_id;
    $bill_entries_table_name_by_users               = 'bill_entries_' . $table_id;
    $payments_table_name_by_users                   = 'payments_' . $table_id;
    $payment_entries_table_name_by_users            = 'payment_entries_' . $table_id;
    $expense_categories_table_name_by_users         = 'expense_categories_' . $table_id;
    $expenses_table_name_by_users                   = 'expenses_' . $table_id;
    $transactions_table_name_by_users               = 'transactions_' . $table_id;
    $units_table_name_by_users                      = 'units_' . $table_id;
    $paid_through_accounts_table_name_by_users      = 'paid_through_accounts_' . $table_id;
    $discounts_table_name_by_users                  = 'discounts_' . $table_id;
    $discount_products_table_name_by_users          = 'discount_products_' . $table_id;
    $urls_table_name_by_users                       = 'urls_' . $table_id;
    $permissions_table_name_by_users                = 'permissions_' . $table_id;
    $modules_table_name_by_users                    = 'modules_' . $table_id;
    $modules_access_table_name_by_users             = 'modules_access_' . $table_id;

    //Declear The Model Name
    $Categories                                     = new Categories;
    $Subcategories                                  = new Subcategories;
    $Items                                          = new Items;
    $Products                                       = new Products;
    $ProductEntries                                 = new ProductEntries;
    $Customers                                      = new Customers;
    $Invoices                                       = new Invoices;
    $InvoiceEntries                                 = new InvoiceEntries;
    $SalesReturn                                    = new SalesReturn;
    $SalesReturnEntries                             = new SalesReturnEntries;
    $PurchaseReturn                                 = new PurchaseReturn;
    $PurchaseReturnEntries                          = new PurchaseReturnEntries;
    $Bills                                          = new Bills;
    $BillEntries                                    = new BillEntries;
    $Payments                                       = new Payments;
    $PaymentEntries                                 = new PaymentEntries;
    $ExpenseCategories                              = new ExpenseCategories;
    $Expenses                                       = new Expenses;
    $Transactions                                   = new Transactions;
    $Units                                          = new Units;
    $PaidThroughAccounts                            = new PaidThroughAccounts;
    $Discounts                                      = new Discounts;
    $DiscountProducts                               = new DiscountProducts;
    $Urls                                           = new URLS;
    $Permissions                                    = new Permissions;
    $Modules                                        = new Modules;
    $ModulesAccess                                  = new ModulesAccess;

    //Set The Table Name By Users ID
    $Categories->setTable($categories_table_name_by_users);
    $Subcategories->setTable($sub_categories_table_name_by_users);
    $Items->setTable($items_table_name_by_users);
    $Products->setTable($products_table_name_by_users);
    $ProductEntries->setTable($product_entries_table_name_by_users);
    $Customers->setTable($customers_table_name_by_users);
    $Invoices->setTable($invoices_table_name_by_users);
    $InvoiceEntries->setTable($invoice_entries_table_name_by_users);
    $SalesReturn->setTable($sales_return_table_name_by_users);
    $SalesReturnEntries->setTable($sales_return_entries_table_name_by_users);
    $PurchaseReturn->setTable($purchase_return_table_name_by_users);
    $PurchaseReturnEntries->setTable($purchase_return_entries_table_name_by_users);
    $Bills->setTable($bills_table_name_by_users);
    $BillEntries->setTable($bill_entries_table_name_by_users);
    $Payments->setTable($payments_table_name_by_users);
    $PaymentEntries->setTable($payment_entries_table_name_by_users);
    $ExpenseCategories->setTable($expense_categories_table_name_by_users);
    $Expenses->setTable($expenses_table_name_by_users);
    $Transactions->setTable($transactions_table_name_by_users);
    $Units->setTable($units_table_name_by_users);
    $PaidThroughAccounts->setTable($paid_through_accounts_table_name_by_users);
    $Discounts->setTable($discounts_table_name_by_users);
    $DiscountProducts->setTable($discount_products_table_name_by_users);
    $Urls->setTable($urls_table_name_by_users);
    $Permissions->setTable($permissions_table_name_by_users);
    $Modules->setTable($modules_table_name_by_users);
    $ModulesAccess->setTable($modules_access_table_name_by_users);

    //Keep All Table Names In An Array
    $tables['categories']               = $Categories;
    $tables['sub_categories']           = $Subcategories;
    $tables['items']                    = $Items;
    $tables['products']                 = $Products;
    $tables['product_entries']          = $ProductEntries;
    $tables['customers']                = $Customers;
    $tables['invoices']                 = $Invoices;
    $tables['invoice_entries']          = $InvoiceEntries;
    $tables['sales_return']             = $SalesReturn;
    $tables['sales_return_entries']     = $SalesReturnEntries;
    $tables['purchase_return']          = $PurchaseReturn;
    $tables['purchase_return_entries']  = $PurchaseReturnEntries;
    $tables['bills']                    = $Bills;
    $tables['bill_entries']             = $BillEntries;
    $tables['payments']                 = $Payments;
    $tables['payment_entries']          = $PaymentEntries;
    $tables['expense_categories']       = $ExpenseCategories;
    $tables['expenses']                 = $Expenses;
    $tables['transactions']             = $Transactions;
    $tables['units']                    = $Units;
    $tables['paid_through_accounts']    = $PaidThroughAccounts;
    $tables['discounts']                = $Discounts;
    $tables['discount_products']        = $DiscountProducts;
    $tables['urls']                     = $Urls;
    $tables['permissions']              = $Permissions;
    $tables['modules']                  = $Modules;
    $tables['modules_access']           = $ModulesAccess;

    return $tables;
}

function accessLevel($route)
{
    //All Routes start

    //products Module Start
        $products_create        = 'products_create';
        $products_store         = 'products_store';
        $products_edit          = 'products_edit';
        $products_update        = 'products_update';
    //products Module End

    //Invoice Module Start
        $invoices_edit       = 'invoices_edit';
        $invoices_update     = 'invoices_update';
    //Invoice Module End

    //Bill Module Start
       $bills_index    = 'bills_index';
       $bills_create   = 'bills_create';
       $bills_store    = 'bills_store';
       $bills_edit     = 'bills_edit';
       $bills_update   = 'bills_update';
       $bills_show     = 'bills_show';
    //Bill Module End

    //Payments Module Start
        $payments_edit              = 'payments_edit';
        $payments_update            = 'payments_update';
    //Payments Module End

    //expenses Module Start
        $expenses_edit             = 'expenses_edit';
        $expenses_update           = 'expenses_update';
    //expenses Module End

    //Users Module Start
        $users_index             = 'users_index';
        $users_create            = 'users_create';
        $users_store             = 'users_store';
        $users_edit              = 'users_edit';
        $users_update            = 'users_update';
    //Users Module End

    //Customers Module Start
      $customers_edit            = 'customers_edit';
      $customers_update          = 'customers_update';
    //Customers Module End

    //All Routes End

    if (Auth::user()->role == 0)
    {
        if ( ($route == $products_create) || 
            ($route == $products_store) || 
            ($route == $products_edit) || 
            ($route == $products_update) ||
            ($route == $invoices_edit) || 
            ($route == $invoices_update) || 
            ($route == $bills_index) || 
            ($route == $bills_create) || 
            ($route == $bills_store) || 
            ($route == $bills_edit) ||
            ($route == $bills_update) || 
            ($route == $bills_show) ||  
            ($route == $expenses_edit) ||  
            ($route == $expenses_update) ||  
            ($route == $payments_edit) || 
            ($route == $users_index) || 
            ($route == $users_create) || 
            ($route == $users_store) || 
            ($route == $users_edit) || 
            ($route == $users_update) )
        {
            return "No";
        }
    else{
      return "accepted";
    }
    }   
    else
    {
            return "accepted";
    }
}

function salesReturn($invoice_id)
{
    $returns                = SalesReturn::leftjoin('customers', 'customers.id', 'sales_return.customer_id')
                                            ->where('sales_return.invoice_id', $invoice_id)
                                            ->select('sales_return.sales_return_date as sales_return_date',
                                                'sales_return.id as id',
                                                'sales_return.invoice_id as invoice_id',
                                                'sales_return.sales_return_number as sales_return_number',
                                                'sales_return.customer_id as customer_id',
                                                'customers.name as customer_name',
                                                'sales_return.return_amount as return_amount',
                                                'sales_return.due_amount as due_amount',
                                                'sales_return.return_note as return_note',
                                                'sales_return.vat_type as vat_type',
                                                'sales_return.total_vat as total_vat',
                                                'sales_return.tax_type as tax_type',
                                                'sales_return.total_tax as total_tax')
                                            ->get();

    $return_entries         = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.sales_return_id')
                                            ->leftjoin('products', 'products.id', 'sales_return_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'sales_return_entries.product_entry_id')
                                            ->where('sales_return.invoice_id', $invoice_id)
                                            ->select('sales_return_entries.*',
                                                     'product_entries.name as product_entry_name',
                                                     'products.name as product_name',
                                                     'product_entries.product_code as product_code')
                                            ->get();

    $total_return_amount        = 0;
    $total_paid_amount          = 0;
    $total_due_amount           = 0;

    if ($returns->count() > 0)
    {
        foreach ($returns as $key => $value)
        {   
            $sub_total_value                            = $return_entries->where('invoice_id', $value['id'])->sum('total_amount');

            $data[$value['id']]['sales_return_number']  = 'SR - ' . str_pad($value['sales_return_number'], 6, "0", STR_PAD_LEFT);
            $data[$value['id']]['sales_return_date']    = date('d-m-Y', strtotime($value['sales_return_date']));
            $data[$value['id']]['sales_return_id']      = $value['id'];
            $data[$value['id']]['customer_name']        = $value['customer_name'];
            $data[$value['id']]['invoice_id']           = $value['invoice_id'];
            $data[$value['id']]['return_amount']        = $value['return_amount'];
            $data[$value['id']]['paid_amount']          = $value['return_amount'] - $value['due_amount'];
            $data[$value['id']]['due_amount']           = $value['due_amount'];
            $data[$value['id']]['return_note']          = $value['return_note'];
            $data[$value['id']]['sub_total']            = $sub_total_value;
            $data[$value['id']]['return_entries']       = $return_entries->where('sales_return_id', $value['id']);

            $total_return_amount                        = $total_return_amount + $value['return_amount'];
            $total_paid_amount                          = $total_paid_amount + ($value['return_amount'] - $value['due_amount']);
            $total_due_amount                           = $total_due_amount + $value['due_amount'];
        }
    }
    else
    {
        $data                       = [];
        $total_return_amount        = 0;
        $total_paid_amount          = 0;
        $total_due_amount           = 0;
    }

    return ['data'                  => $data,
            'total_return_amount'   => $total_return_amount,
            'total_paid_amount'     => $total_paid_amount,
            'total_due_amount'      => $total_due_amount,
        ];
}

function purchaseReturn($bill_id)
{
    $returns                = PurchaseReturn::leftjoin('customers', 'customers.id', 'purchase_return.customer_id')
                                            ->where('purchase_return.bill_id', $bill_id)
                                            ->select('purchase_return.purchase_return_date as purchase_return_date',
                                                'purchase_return.id as id',
                                                'purchase_return.bill_id as bill_id',
                                                'purchase_return.purchase_return_number as purchase_return_number',
                                                'purchase_return.customer_id as customer_id',
                                                'customers.name as customer_name',
                                                'purchase_return.return_amount as return_amount',
                                                'purchase_return.due_amount as due_amount',
                                                'purchase_return.return_note as return_note',
                                                'purchase_return.vat_type as vat_type',
                                                'purchase_return.total_vat as total_vat',
                                                'purchase_return.tax_type as tax_type',
                                                'purchase_return.total_tax as total_tax')
                                            ->get();

    $return_entries         = PurchaseReturnEntries::leftjoin('purchase_return', 'purchase_return.id', 'purchase_return_entries.purchase_return_id')
                                            ->leftjoin('products', 'products.id', 'purchase_return_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'purchase_return_entries.product_entry_id')
                                            ->where('purchase_return.bill_id', $bill_id)
                                            ->select('purchase_return_entries.*',
                                                     'product_entries.name as product_entry_name',
                                                     'products.name as product_name',
                                                     'product_entries.product_code as product_code')
                                            ->get();

    $total_return_amount        = 0;
    $total_paid_amount          = 0;
    $total_due_amount           = 0;
    $total_tax_amount           = 0;
    $total_vat_amount           = 0;

    if ($returns->count() > 0)
    {
        foreach ($returns as $key => $value)
        {   
            $sub_total_value                                = $return_entries->where('bill_id', $value['id'])->sum('total_amount');

            $data[$value['id']]['purchase_return_number']   = 'PR - ' . str_pad($value['purchase_return_number'], 6, "0", STR_PAD_LEFT);
            $data[$value['id']]['purchase_return_date']     = date('d-m-Y', strtotime($value['purchase_return_date']));
            $data[$value['id']]['purchase_return_id']       = $value['id'];
            $data[$value['id']]['customer_name']            = $value['customer_name'];
            $data[$value['id']]['invoice_id']               = $value['invoice_id'];
            $data[$value['id']]['return_amount']            = $value['return_amount'];
            $data[$value['id']]['paid_amount']              = $value['return_amount'] - $value['due_amount'];
            $data[$value['id']]['due_amount']               = $value['due_amount'];
            $data[$value['id']]['return_note']              = $value['return_note'];
            $data[$value['id']]['sub_total']                = $sub_total_value;
            $data[$value['id']]['return_entries']           = $return_entries->where('purchase_return_id', $value['id']);

            $total_return_amount                            = $total_return_amount + $value['return_amount'];
            $total_paid_amount                              = $total_paid_amount + ($value['return_amount'] - $value['due_amount']);
            $total_due_amount                               = $total_due_amount + $value['due_amount'];
        }
    }
    else
    {
        $data = [];
    }

    return ['data'                  => $data,
            'total_return_amount'   => $total_return_amount,
            'total_paid_amount'     => $total_paid_amount,
            'total_due_amount'      => $total_due_amount,
            'total_tax_amount'      => $total_tax_amount,
            'total_vat_amount'      => $total_vat_amount,
        ];
}

function discountProducts($discount_id)
{
    $discounts      = DiscountProducts::leftjoin('product_entries', 'product_entries.id', 'discount_products.product_id')
                            ->where('discount_products.discount_id', $discount_id)
                            ->orderBy('discount_products.discount_id', 'DESC')
                            ->select('product_entries.name as product_name')
                            ->get();

    return $discounts;
}

function openingBalanceStore($amount, $customer_id, $contact_type)
{
    if ($contact_type == 0)
    {
        $data_find                  = Invoices::orderBy('created_at', 'DESC')->first();
        $invoice_number             = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

        $invoice                    = new Invoices;
        $invoice->invoice_number    = $invoice_number;
        $invoice->customer_id       = $customer_id;
        $invoice->invoice_date      = date('Y-m-d');
        $invoice->invoice_amount    = $amount;
        $invoice->due_amount        = $amount;
        $invoice->total_buy_price   = 0;
        $invoice->total_discount    = 0;
        $invoice->created_by        = Auth::user()->id;

        if ($invoice->save())
        {
            $product_entry_id       = ProductEntries::where('product_id', 1)->first();

            $invoice_entries[] = [
                'invoice_id'        => $invoice['id'],
                'product_id'        => 1,
                'product_entry_id'  => $product_entry_id['id'],
                'customer_id'       => $customer_id,
                'buy_price'         => 0,
                'rate'              => $amount,
                'quantity'          => 1,
                'total_amount'      => $amount,
                'discount_type'     => 1,
                'discount_amount'   => 0,
                'created_by'        => Auth::user()->id,
                'created_at'        => date('Y-m-d H:i:s'),
            ];

            DB::table('invoice_entries')->insert($invoice_entries);
        }
    }
    else
    {
        $data_find                  = Bills::orderBy('created_at', 'DESC')->first();
        $bill_number                = $data_find != null ? $data_find['bill_number'] + 1 : 1;

        $bill                       = new Bills;
        $bill->bill_number          = $bill_number;
        $bill->vendor_id            = $customer_id;
        $bill->bill_date            = date('Y-m-d');
        $bill->bill_amount          = $amount;
        $bill->due_amount           = $amount;
        $bill->total_discount       = 0;
        $bill->created_by           = Auth::user()->id;

        if ($bill->save())
        {
            $product_entry_id       = ProductEntries::where('product_id', 1)->first();

            $bill_entries[] = [
                'bill_id'           => $bill['id'],
                'product_id'        => 1,
                'product_entry_id'  => $product_entry_id['id'],
                'vendor_id'         => $customer_id,
                'rate'              => $amount,
                'quantity'          => 1,
                'total_amount'      => $amount,
                'discount_type'     => 1,
                'discount_amount'   => 0,
                'created_by'        => Auth::user()->id,
                'created_at'        => date('Y-m-d H:i:s'),
            ];

            DB::table('bill_entries')->insert($bill_entries);
        }
    }

    return 0;
}

function openingBalanceUpdate($amount, $customer_id, $contact_type)
{
    if ($contact_type == 0)
    {
        $invoice                    = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                                ->where('invoices.customer_id', $customer_id)
                                                ->where('invoice_entries.product_id', 1)
                                                ->select('invoices.*')
                                                ->first();

        $payment_amount             = Payments::leftjoin('payment_entries', 'payment_entries.payment_id', 'payments.id')
                                                ->where('payments.customer_id', $customer_id)
                                                ->where('payment_entries.invoice_id', $invoice['id'])
                                                ->sum('payments.amount');

        $invoice->invoice_amount    = $amount;
        $invoice->due_amount        = $amount - $payment_amount;
        $invoice->updated_by        = Auth::user()->id;

        if ($invoice->save())
        {
            $invoice_entries_delete = InvoiceEntries::where('invoice_id', $invoice['id'])->delete();
            $product_entry_id       = ProductEntries::where('product_id', 1)->first();

            $invoice_entries[] = [
                'invoice_id'        => $invoice['id'],
                'product_id'        => 1,
                'product_entry_id'  => $product_entry_id['id'],
                'customer_id'       => $customer_id,
                'buy_price'         => 0,
                'rate'              => $amount,
                'quantity'          => 1,
                'total_amount'      => $amount,
                'discount_type'     => 1,
                'discount_amount'   => 0,
                'updated_by'        => Auth::user()->id,
                'updated_at'        => date('Y-m-d H:i:s'),
            ];

            DB::table('invoice_entries')->insert($invoice_entries);
        }
    }
    else
    {
        $bill                       = Bills::leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                                ->where('bills.vendor_id', $customer_id)
                                                ->where('bill_entries.product_id', 1)
                                                ->select('bills.*')
                                                ->first();

        $payment_amount             = Payments::leftjoin('payment_entries', 'payment_entries.payment_id', 'payments.id')
                                                ->where('payments.customer_id', $customer_id)
                                                ->where('payment_entries.bill_id', $bill['id'])
                                                ->sum('payments.amount');

        $bill->bill_amount          = $amount;
        $bill->due_amount           = $amount - $payment_amount;
        $bill->updated_by           = Auth::user()->id;

        if ($bill->save())
        {   
            $bill_entries_delete    = BillEntries::where('bill_id', $bill['id'])->delete();
            $product_entry_id       = ProductEntries::where('product_id', 1)->first();

            $bill_entries[] = [
                'bill_id'           => $bill['id'],
                'product_id'        => 1,
                'product_entry_id'  => $product_entry_id['id'],
                'vendor_id'         => $customer_id,
                'rate'              => $amount,
                'quantity'          => 1,
                'total_amount'      => $amount,
                'discount_type'     => 1,
                'discount_amount'   => 0,
                'created_by'        => Auth::user()->id,
                'created_at'        => date('Y-m-d H:i:s'),
            ];

            DB::table('bill_entries')->insert($bill_entries);
        }
    }
    
    return 0;
}

function customersTableDetails($id)
{
    $customers  = Customers::find($id);

    return $customers;
}

function ProductVariationName($product_entry_id)
{
    $product            = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations')
                                            ->find($product_entry_id);

    return $product['variations'];
}

function ProductName($product_entry_id)
{
    $product            = ProductEntries::find($product_entry_id);

    return $product['name'];
}

/**
    Console log function nahian vi 
*/

function console_log($output, $with_script_tags = true)
{
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . 
    ');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}