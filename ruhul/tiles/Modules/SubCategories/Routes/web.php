<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('subcategories')->group(function() {
    Route::get('/', 'SubCategoriesController@index')->name('sub_categories_index');
    Route::post('/store', 'SubCategoriesController@store')->name('sub_categories_store');
    Route::get('/edit/{id}', 'SubCategoriesController@edit')->name('sub_categories_edit');
    Route::post('/update/{id}', 'SubCategoriesController@update')->name('sub_categories_update');
});
