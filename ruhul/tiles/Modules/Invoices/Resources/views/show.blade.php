<!DOCTYPE html >
<html>

<head>
    <meta charset="utf-8">
    <title>Cash Memo</title>
    <link href="https://fonts.maateen.me/bangla/font.css" rel="stylesheet">

    <style>
        *{margin:0;padding:0;outline:0}

        body {
            font-size:14px;
            line-height:18px;
            color:#000;
            height:292mm;
            width: 203mm;/*297*/
            margin:0 auto;
            font-family: 'Bangla', Arial, sans-serif !important;  
        }

        table,th {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 10px;
        }

        td {
            border-left: 1px solid black;
            border-right: 1px solid black;
            padding: 10px;
        }

        .bg_templete{
            background:url(images/bg3.png) no-repeat;
            background-size: 100%;
            -webkit-print-color-adjust: exact;
        }

        .templete{
            width:90%;
            background:none;
            margin:0 auto;
        }
        
        .clear{
            overflow:hidden;
        }

        .text-center{
            text-align: center;
        }

        .text-end{
            text-align: end;
        }

        .item2 {
            grid-area: com_logo;
        }

        .item3 {
            grid-area: com_info;
        }

        .item4 {
            grid-area: other_info;
        }

        .grid-container {
            display: grid;
            grid-template-areas: "com_logo com_info other_info ";
            padding-top: 10px;
        }

        .grid-container div {
            text-align: center;
        }

        .grid-container-aline-end {
            text-align: end !important;
        }

        .grid-container-aline-start {
            text-align: start !important;
        }

        .com_logo_img {
            height: 80px;
            width: 80px;
        }

        .tr-height{
            padding: 10px;
        }

        .item11 { grid-area: sig_text2; }
        .item22 { grid-area: some_info; }
        .item33 { grid-area: sig1; }
        .item44 { grid-area: sig2; }
        .item55 { grid-area: sig_text1; }

        .grid-container-1 {
            display: grid;
            grid-template-areas:
            ' sig1 sig1 some_info some_info sig2 sig2'
            ' sig_text1 sig_text1 some_info some_info sig_text2 sig_text2';
        }

        .signaturesection1 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 45px;
        }

        .signaturesection2 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 45px;
        }

        @page {
            size: A4;
            page-break-after: always;
        }
    </style>
</head>

<body>
    <div class="bg_templete clear">

        <div class="templete clear">

            <br>
            <br>

            <div class="grid-container">
                <div class="item2 grid-container-aline-start">
                    <img class="com_logo_img" src="{{ url('public/'.userDetails()->logo) }}"alt="qr sample"
                    />
                </div>

                <div class="item3">
                    <p>ক্যাশ মেমো</p>
                    <h2 style="line-height: 35px;font-size: 30px">রংধনু ট্রেডার্স</h2>
                    <h2>টাইলস এন্ড স্যানিটারী</h2>
                    <h3>প্রো: মোঃ তোফাজ্জল হোসেন খান</h3>
                    <p>বাথরুমের ফিটিং এবং টাইলসের দোকান - টাইলসের বিভিন্ন ডিজাইনের সংগ্রহ</p>
                </div>

                <div class="item4 grid-container-aline-end">

                    <p style="padding-right: 25px;">ডিলার:</p>
                    <p>মীর সিরামিক লিঃ</p>
                    <p>মীর সিরামিক লিঃ</p>
                    <p>মীর সিরামিক লিঃ</p>
                    <p>মীর সিরামিক লিঃ</p>
                    <p>মীর সিরামিক লিঃ</p>
                    
                </div>
            </div>

            <div style="padding-bottom: 10px" class="text-center">
                <h4 >মাহবুব মোল্লা কনকর্ড টাওয়ার, জেলখানার মোড়, নরসিংদী।</h4>
                <p >মোবাইল: ০১৭২-০০০০০০০, ০১৭২-০০০০০০০, ম্যানেজার: ০১৭২-০০০০০০০</p>
            </div>

            <hr>

            <div style="font-size: 18px;padding-top: 10px">
                <strong>Serial -  </strong>{{ $invoice['invoice_number'] }}<strong style="float: right;">Date : <span>{{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}</span></strong>
            </div>

            <div style="font-size: 18px">
                <strong>Name - </strong>{{ $invoice['customer_name'] }}
            </div>

            <div style="font-size: 18px">
                <strong>Address - </strong>{{ $invoice['address'] }}
            </div>

            <div style="padding-top: 10px;padding-bottom: 20px">
                <table style="width: 100%">
                    <tr>
                        <th style="font-size: 18px">নং</th>
                        <th style="font-size: 18px">মালের বিবরণ</th>
                        <th style="font-size: 18px">পরিমান</th>
                        <th style="font-size: 18px">দর</th>
                        <th style="font-size: 18px">টাকা</th>
                    </tr>

                    @if($entries->count() > 0)

                    <?php
                        $total_amount   = 0;
                    ?>

                    @foreach($entries as $key => $value)
                    <?php
                        $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                    ?>

                    <tr class="tr-height">
                        <td style="text-align: center">{{ $key + 1 }}</td>
                        <td style="padding-left: 30px">{{ $value['product_entry_name'] }} <br> {{ $value['brand_name'] . ' - ' . $value['product_name'] . ' - ' . $value['height'] . ' X ' . $value['width'] }}</td>
                        <td style="text-align: center">{{ $value['cartoon'] . ' Ctn ' . $value['pcs'] . ' pcs ' }} <br> {{ $value['quantity'] . ' SFT ' }}</td>
                        <td style="text-align: center">{{ $value['rate'] }}</td>
                        <td style="text-align: center">{{ $value['total_amount'] }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <th style="text-align: left" colspan="2">In Words : <span style="font-weight: normal;">{{ numberTowords($total_amount) }}</span></th>
                        <th style="text-align: right"><strong>Total</strong></th>
                        <th></th>
                        <th style="text-align: center">{{ $total_amount }}</th>
                    </tr>
                </table>
            </div>

            <div class="grid-container-1">

                <div class="item33"></div>  
                <div class="item55"><p class="signaturesection1">ক্রেতার স্বাক্ষর</p></div>

                <div class="item22" style="text-align: center;">
                    <h3>শুক্রবার দোকান বন্ধ। </h3>
                    <div>
                        <p>আল্লাহ কোন জাতির ভাগ্যের অবস্থার পরিবর্তন করেন না </p>
                        <p>যতক্ষণ না সে তার অবস্থার পরিবর্তন করে।  আল-হাদীস </p>
                    </div>
                </div>

                <div class="item44"></div>
                <div class="item1"><p class="signaturesection2">বিক্রেতার স্বাক্ষর</p></div>
            </div>
        </div>
    </div>
</body>

</html>