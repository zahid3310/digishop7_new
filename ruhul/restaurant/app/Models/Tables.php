<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Tables extends Model
{  
    protected $table = "tables";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

}
