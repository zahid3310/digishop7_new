<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class SrItems extends Model
{  
    protected $table = "sr_items";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function sr()
    {
        return $this->belongsTo('App\Models\Customers','sr_id');
    }

    public function issue()
    {
        return $this->belongsTo('App\Models\Issues','issue_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Products','product_id');
    }

    public function productEntries()
    {
        return $this->belongsTo('App\Models\ProductEntries','product_entry_id');
    }
}
