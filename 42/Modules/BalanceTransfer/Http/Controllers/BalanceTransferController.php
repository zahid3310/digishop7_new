<?php

namespace Modules\BalanceTransfer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

use App\Models\BalanceTransfers;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\Checks;
use Response;
use DB;

class BalanceTransferController extends Controller
{
    public function index()
    {
        $branch_id          = Auth::user()->branch_id;
        $paid_accounts      = Accounts::where('account_type_id', 4)
                                    ->whereNotIn('id', [2,3])
                                    ->where('status', 1)
                                    ->get();
        $transfers          = BalanceTransfers::where('branch_id', $branch_id)->orderBy('created_at', 'DESC')->get();

        return view('balancetransfer::index', compact('paid_accounts', 'transfers'));
    }

    public function create()
    {
        return view('balancetransfer::create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'transfer_date' => 'required',
            'transfer_from' => 'required',
            'transfer_to'   => 'required',
            'amount'        => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        if ($data['transfer_from'] == $data['transfer_to'])
        {
            return back()->with("unsuccess","Not Added");
        }

        DB::beginTransaction();

        try{
            $transfer                   = new BalanceTransfers;
            $transfer->transaction_date = date('Y-m-d', strtotime($data['transfer_date']));
            $transfer->transfer_from    = $data['transfer_from'];
            $transfer->transfer_to      = $data['transfer_to'];
            $transfer->amount           = $data['amount'];
            $transfer->note             = $data['note'];
            $transfer->branch_id        = $branch_id;
            $transfer->created_by       = $user_id;

            if ($transfer->save())
            {       
                //Financial Accounting Part Start
                credit($customer_id=null, $date=$data['transfer_date'], $account_id=$data['transfer_from'], $amount=$data['amount'], $note=$data['note'], $transaction_head='balance-transfer', $income_id=null, $expense_id=null, $balance_transfer_id=$transfer->id, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                debit($customer_id=null, $date=$data['transfer_date'], $account_id=$data['transfer_to'], $amount=$data['amount'], $note=$data['note'], $transaction_head='balance-transfer', $income_id=null, $expense_id=null, $balance_transfer_id=$transfer->id, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //Financial Accounting Part End

                DB::commit();
                return redirect()->route('balance_transfer_index')->with("success","Transfer Created Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        return view('balancetransfer::show');
    }

    public function edit($id)
    {
        $branch_id          = Auth::user()->branch_id;
        $paid_accounts      = Accounts::where('account_type_id', 4)
                                    ->whereNotIn('id', [2,3])
                                    ->where('status', 1)
                                    ->get();
        $transfers          = BalanceTransfers::where('branch_id', $branch_id)->orderBy('created_at', 'DESC')->get();
        $find_transfer      = BalanceTransfers::find($id);

        return view('balancetransfer::edit', compact('paid_accounts', 'transfers', 'find_transfer'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'transfer_date' => 'required',
            'transfer_from' => 'required',
            'transfer_to'   => 'required',
            'amount'        => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        if ($data['transfer_from'] == $data['transfer_to'])
        {
            return back()->with("unsuccess","Not Updated");
        }

        DB::beginTransaction();

        try{
            $transfer                   = BalanceTransfers::find($id);
            $transfer->transaction_date = date('Y-m-d', strtotime($data['transfer_date']));
            $transfer->transfer_from    = $data['transfer_from'];
            $transfer->transfer_to      = $data['transfer_to'];
            $transfer->amount           = $data['amount'];
            $transfer->note             = $data['note'];
            $transfer->updated_by       = $user_id;

            if ($transfer->save())
            {   
                $jour_ent_debit     = JournalEntries::where('balance_transfer_id', $transfer->id)
                                        ->where('transaction_head', 'balance-transfer')
                                        ->where('debit_credit', 1)
                                        ->first();

                $jour_ent_credit    = JournalEntries::where('balance_transfer_id', $transfer->id)
                                        ->where('transaction_head', 'balance-transfer')
                                        ->where('debit_credit', 0)
                                        ->first();

                ///Financial Accounting Part Start
                creditUpdate($jour_ent_debit['id'], $customer_id=null, $date=$data['transfer_date'], $account_id=$data['transfer_from'], $amount=$data['amount'], $note=$data['note'], $transaction_head='balance-transfer', $income_id=null, $expense_id=null, $balance_transfer_id=$transfer->id, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                debitUpdate($jour_ent_credit['id'], $customer_id=null, $date=$data['transfer_date'], $account_id=$data['transfer_to'], $amount=$data['amount'], $note=$data['note'], $transaction_head='balance-transfer', $income_id=null, $expense_id=null, $balance_transfer_id=$transfer->id, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //Financial Accounting Part End

                DB::commit();
                return redirect()->route('balance_transfer_index')->with("success","Transfer Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function destroy($id)
    {
    }

    //Print Check
    public function printCheckIndex()
    {
        $branch_id       = Auth::user()->branch_id;
        $checks          = Checks::where('branch_id', $branch_id)->orderBy('created_at', 'DESC')->get();

        return view('balancetransfer::checks.index', compact('checks'));
    }

    public function printCheckStore(Request $request)
    {
        $rules = array(
            'd1'        => 'required',
            'd2'        => 'required',
            'm1'        => 'required',
            'm1'        => 'required',
            'y1'        => 'required',
            'y1'        => 'required',
            'y1'        => 'required',
            'y1'        => 'required',
            'pay_to'    => 'required',
            'amount'    => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $check                   = new Checks;
            $check->d1               = $data['d1'];
            $check->d2               = $data['d2'];
            $check->m1               = $data['m1'];
            $check->m2               = $data['m2'];
            $check->y1               = $data['y1'];
            $check->y2               = $data['y2'];
            $check->y3               = $data['y3'];
            $check->y4               = $data['y4'];
            $check->pay_to           = $data['pay_to'];
            $check->amount           = $data['amount'];
            $check->branch_id        = $branch_id;
            $check->created_by       = $user_id;

            if ($check->save())
            {   
                DB::commit();
                return redirect()->route('balance_transfer_print_check_index')->with("success","Check Added Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function printCheckShow($id)
    {   
        $check  = Checks::find($id);

        return view('balancetransfer::checks.show', compact('check'));
    }

    public function printCheckEdit($id)
    {
        $branch_id       = Auth::user()->branch_id;
        $find_check      = Checks::find($id);
        $checks          = Checks::where('branch_id', $branch_id)->orderBy('created_at', 'DESC')->get();

        return view('balancetransfer::checks.edit', compact('find_check', 'checks'));
    }

    public function printCheckUpdate(Request $request, $id)
    {
        $rules = array(
            'd1'        => 'required',
            'd2'        => 'required',
            'm1'        => 'required',
            'm1'        => 'required',
            'y1'        => 'required',
            'y1'        => 'required',
            'y1'        => 'required',
            'y1'        => 'required',
            'pay_to'    => 'required',
            'amount'    => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $check                   = Checks::find($id);
            $check->d1               = $data['d1'];
            $check->d2               = $data['d2'];
            $check->m1               = $data['m1'];
            $check->m2               = $data['m2'];
            $check->y1               = $data['y1'];
            $check->y2               = $data['y2'];
            $check->y3               = $data['y3'];
            $check->y4               = $data['y4'];
            $check->pay_to           = $data['pay_to'];
            $check->amount           = $data['amount'];
            $check->branch_id        = $branch_id;
            $check->updated_by       = $user_id;

            if ($check->save())
            {   
                DB::commit();
                return redirect()->route('balance_transfer_print_check_index')->with("success","Check Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }
}
