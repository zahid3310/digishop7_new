@extends('layouts.app')

@section('title', 'Purchase Summary')

<style type="text/css">
    @media print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page {
            margin: 0cm ! important;
        }
    }

    .table td, .table th {
        font-size: 12px !important;
    }
</style>

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Purchase Summary</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Purchase Summary</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['organization_name'] }}</h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['address'] }}</p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['contact_number'] }}</p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">Purchase Summary Report</h4>
                                        <p style="text-align: center"><strong>From</strong> {{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }} <strong>To</strong> {{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="{{ route('purchase_summary_index') }}" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div style="margin-bottom: 10px" class="col-lg-4 col-md-4 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="{{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="{{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        @if(Auth()->user()->branch_id == 1)
                                        <div class="col-lg-2 col-md-2 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select style="width: 100%" id="branch_id" name="branch_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" required>
                                                   @if($branches->count() > 0)
                                                   @foreach($branches as $key => $value)
                                                   <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                                   @endforeach
                                                   @endif
                                                </select>
                                            </div>
                                        </div>
                                        @endif

                                        <div class="col-lg-4 col-md-4 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="0" selected>-- All Supplier --</option>
                                                    @if(!empty($customers) && ($customers->count() > 0))
                                                    @foreach($customers as $key => $value)
                                                        <option {{ isset($_GET['customer_id']) && ($_GET['customer_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Bill#</th>
                                            <th>Date</th>
                                            <th>Supplier</th>
                                            <th style="text-align: right">Bill Amount</th>
                                            <th style="text-align: right">Discount</th>
                                            <th style="text-align: right">Payable</th>
                                            <th style="text-align: right">Paid</th>
                                        </tr>
                                    </thead>


                                    <tbody>

                                        @if(!empty($data))

                                        <?php $serial = 1; ?>

                                        @foreach($data as $key => $value)
                                        
                                            <tr>
                                                <td>{{ $serial }}</td>
                                                <td>
                                                    <a href="{{ route('bills_show', $value['bill_id']) }}" target="_blank">
                                                        {{ $value['bill_number'] }}
                                                    </a>
                                                </td>
                                                <td>{{ $value['bill_date'] }}</td>
                                                <td>{{ $value['customer_name'] }}</td>
                                                <td style="text-align: right">{{ number_format($value['bill_amount'],0,'.',',') }}</td>
                                                <td style="text-align: right">
                                                    {{ number_format($value['total_discount'] + $value['discount'],0,'.',',') }}

                                                    @if($value['total_discount_note'] != null) 
                                                        <br>
                                                    @endif

                                                    {{ $value['total_discount_note'] != null ? $value['total_discount_note'] : '' }}
                                                </td>
                                                <td style="text-align: right">{{ number_format($value['bill_amount'],2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format($value['paid_amount'],0,'.',',') }}</td>
                                            </tr>

                                            <?php $serial++; ?>

                                        @endforeach
                                        @endif

                                    </tbody>
                                    <tr>
                                        <th style="text-align: right" colspan="4">Total</th>
                                        <th style="text-align: right">{{ number_format($total_invoice_amount,0,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_discount_amount,0,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_receivable,0,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_paid_amount,0,'.',',') }}</th>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection