<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('bills')->group(function() {
    Route::get('/', 'BillsController@index')->name('bills_index');
    Route::get('/all-bills', 'BillsController@AllBills')->name('bills_all_bills');
    Route::post('/store', 'BillsController@store')->name('bills_store');
    Route::get('/edit/{id}', 'BillsController@edit')->name('bills_edit');
    Route::post('/update/{id}', 'BillsController@update')->name('bills_update');
    Route::get('/show/{id}', 'BillsController@show')->name('bills_show');
    Route::get('/product/entries/list', 'BillsController@ProductEntriesList')->name('bills_product_entries_list');
    Route::get('/product/entries/list/invoice', 'BillsController@ProductEntriesListInvoice')->name('bills_product_entries_list_invoice');
    Route::get('/bill/list/load', 'BillsController@billListLoad')->name('bills_list_load');
    Route::get('/bill/search/list/{from_date}/{to_date}/{customer_name}', 'BillsController@billListSearch')->name('bills_list_search');
    Route::get('/from-bill/store/product', 'BillsController@storeProduct');
    Route::get('/bill-product-list', 'BillsController@billProductList');
    Route::get('/bill-product-entries-list/{id}', 'BillsController@posSearchProductBill');
    Route::get('/product/entries/list/bill/{id}', 'BillsController@ProductEntriesListBill');

    Route::get('/product-list-load-bill', 'BillsController@productListLoadBill')->name('bills_product_list_load');
    Route::get('/get-conversion-param/{product_entry_id}/{unit_id}', 'BillsController@getConversionParam')->name('bills_get_conversion_param');
    Route::get('/calculate-supplier-opening-balance/{supplier_id}', 'BillsController@calculateOpeningBalance')->name('bills_calculate_opening_balance');
    Route::get('/adjust-advance-payment/{customer_id}', 'BillsController@adjustAdvancePayment')->name('bills_adjust_advance_payment');
});