

<?php $__env->startSection('title', 'Print Pos'); ?>

<?php if($user_info['pos_printer'] == 0): ?>
<style type="text/css">
    @media  print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page  {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 60mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }
    }
</style>
<?php endif; ?>

<?php if($user_info['pos_printer'] == 1): ?>
<style type="text/css">
    @media  print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page  {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 90mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }
    }
</style>
<?php endif; ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content marginTopPrint">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row d-print-none">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Print Pos</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <?php if($user_info['pos_printer'] == 0): ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div id="hedColor" style="margin-top: 0px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold"><?php echo e($user_info['address']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 18px;font-weight: bold"><?php echo e($user_info['contact_number']); ?></p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 18px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">Invoice</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 14px">Date : <?php echo e(date('d/m/Y', strtotime($invoice['invoice_date']))); ?></span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 14px"><?php echo e(date('h:i a', strtotime(now()))); ?></span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 14px">Invoice# : <?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?></span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">
                                    
                                </div>

                                <div style="border-top: 2px dashed black;border-bottom: 2px dashed black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px;" class="col-2"><strong>Qt</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-4"><strong>Item</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Price</strong></div>
                                    <div style="font-size: 12px;text-align: right;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Total</strong></div>
                                </div>

                                <?php if(!empty($entries) && ($entries->count() > 0)): ?>

                                    <?php 
                                        $sub_total              = 0; 
                                        $total_discount         = 0;
                                    ?>

                                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <div style="padding-bottom: 10px;" id="DetailsFontSize" class="row">

                                        <?php 
                                            $discount           = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; 
                                            $total_discount     = $total_discount + $discount;
                                        ?>

                                        <div style="font-size: 12px" class="col-2">
                                            <?php echo e($value['quantity']); ?> <br> <?php echo e($value->convertedUnit->name); ?>

                                        </div>

                                        <div style="font-size: 12px" class="col-4">
                                            <?php if($value['product_type'] == 1): ?>
                                                <?php echo $value['product_entry_name']; ?>
                                            <?php else: ?>
                                                <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                            <?php endif; ?>
                                        </div>
                                        <div style="font-size: 12px" class="col-3">
                                            <?php echo e(number_format($value['rate'])); ?>

                                        </div>
                                        <div style="font-size: 12px;text-align: right" class="col-3"><?php echo e(number_format($value['total_amount'])); ?></div>
                                    </div>

                                    <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>GROSS Total :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong><?php echo e(number_format($sub_total)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-bottom: 10px" class="row"></div> -->

                                <?php
                                    $total_vat_amount       = $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : $invoice['total_vat']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>Total VAT :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong><?php echo e(number_format($total_vat_amount)); ?></strong></div>
                                </div>

                                <?php
                                    $total_discount_amount       = $invoice['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$invoice['total_vat'])/100))*$invoice['total_discount_amount'])/100) : $invoice['total_discount_amount']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>Total Discount :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong><?php echo e(number_format($total_discount + $total_discount_amount)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;" class="row">
                                    <div style="font-size: 14px" class="col-6"><strong>NET Total :</strong></div>
                                    <div style="font-size: 14px;text-align: right" class="col-6"><strong><?php echo e(number_format($sub_total + $total_vat_amount - $total_discount_amount)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px;" class="row"></div> -->
                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px;" class="row"></div> -->

                               <!--  <div class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-12">Payments</div>
                                </div> -->

                                <div style="padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 14px;font-weight: bold" class="col-6">Total Paid</div>
                                    <div style="font-size: 14px;text-align: right;font-weight: bold" class="col-6"><?php echo e(number_format($invoice['invoice_amount'] - $invoice['due_amount'])); ?></div>
                                </div>

                                <!-- <div style="padding-left: 15px;border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 16px" class="col-6">-TOTAL DUES</div>
                                    <div style="font-size: 16px;text-align: right" class="col-6"><?php echo e(number_format($invoice['due_amount'],2,'.',',')); ?></div>
                                </div> -->

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 12px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if($user_info['pos_printer'] == 1): ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div id="hedColor" style="margin-top: 0px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight: bold"><?php echo e($user_info['address']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 20px;font-weight: bold"><?php echo e($user_info['contact_number']); ?></p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 20px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">Invoice</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 16px">Date : <?php echo e(date('d/m/Y', strtotime($invoice['invoice_date']))); ?></span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 16px">Time : <?php echo e(date('h:i a', strtotime(now()))); ?></span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 16px">Invoice# : <?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?></span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">
                                    
                                </div>

                                <div style="border-top: 2px dashed black;border-bottom: 2px dashed black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-2"><strong>Qty</strong></div>
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-4"><strong>Item Name</strong></div>
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Price</strong></div>
                                    <div style="font-size: 16px;text-align: right;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>T.Price</strong></div>
                                </div>

                                <?php if(!empty($entries) && ($entries->count() > 0)): ?>

                                    <?php 
                                        $sub_total              = 0; 
                                        $total_discount         = 0;
                                    ?>

                                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <div style="padding-bottom: 10px;" id="DetailsFontSize" class="row">

                                        <?php 
                                            $discount           = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; 
                                            $total_discount     = $total_discount + $discount;
                                        ?>

                                        <div style="font-size: 16px" class="col-2">
                                            <?php echo e($value['quantity']); ?> <br> <?php echo e($value->convertedUnit->name); ?>

                                        </div>

                                        <div style="font-size: 16px" class="col-4">
                                            <?php if($value['product_type'] == 1): ?>
                                                <?php echo $value['product_entry_name']; ?>
                                            <?php else: ?>
                                                <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                            <?php endif; ?>
                                        </div>
                                        <div style="font-size: 16px" class="col-3">
                                            <?php echo e(number_format($value['rate'])); ?>

                                        </div>
                                        <div style="font-size: 16px;text-align: right" class="col-3"><?php echo e(number_format($value['total_amount'])); ?></div>
                                    </div>

                                    <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>GROSS Total :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong><?php echo e(number_format($sub_total)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-bottom: 10px" class="row"></div> -->

                                <?php
                                    $total_vat_amount       =  $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : $invoice['total_vat']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>Total VAT :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong><?php echo e(number_format($total_vat_amount)); ?></strong></div>
                                </div>

                                <?php
                                    $total_discount_amount       = $invoice['total_discount_type'] == 0 ? ((($sub_total + $total_vat_amount)*$invoice['total_discount_amount'])/100) : $invoice['total_discount_amount']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>Total Discount :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong><?php echo e(number_format($total_discount + $total_discount_amount)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;" class="row">
                                    <div style="font-size: 20px" class="col-6"><strong>NET Total :</strong></div>
                                    <div style="font-size: 20px;text-align: right" class="col-6"><strong><?php echo e(number_format($sub_total + $total_vat_amount - $total_discount_amount)); ?></strong></div>
                                </div>

                                <!-- <div style="border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 20px" class="col-12"><strong>VAT Included</strong></div>
                                </div> -->

                                <!-- <div style="padding-top: 10px;" class="row"></div> -->
                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px;" class="row"></div> -->

                               <!--  <div class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-12">Payments</div>
                                </div> -->

                                <div style="padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-6">Total Paid</div>
                                    <div style="font-size: 18px;text-align: right;font-weight: bold" class="col-6"><?php echo e(number_format($invoice['invoice_amount'] - $invoice['due_amount'])); ?></div>
                                </div>

                                <!-- <div style="padding-left: 15px;border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 16px" class="col-6">-TOTAL DUES</div>
                                    <div style="font-size: 16px;text-align: right" class="col-6"><?php echo e(number_format($invoice['due_amount'],2,'.',',')); ?></div>
                                </div> -->

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 16px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if($user_info['pos_printer'] == 2 || $user_info['pos_printer'] == 3): ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <?php if($user_info['header_image'] == null): ?>
                                        <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-4 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px"><?php echo e($user_info['organization_name']); ?></h2>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['address']); ?></p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['contact_number']); ?></p>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    <?php else: ?>
                                        <img class="float-left" src="<?php echo e(url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image'])); ?>" alt="logo" style="width: 100%" />
                                    <?php endif; ?>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-4 col-6">
                                        <address>
                                            <strong>Billed To:</strong><br>
                                            <?php echo e($invoice['customer_name']); ?>

                                            <?php if($invoice['address'] != null): ?>
                                               <br> <?php echo $invoice['address']; ?> <br>
                                            <?php endif; ?>
                                            <?php if($invoice['address'] == null): ?>
                                                <br>
                                            <?php endif; ?>
                                            <?php echo e($invoice['phone']); ?>

                                        </address>
                                    </div>

                                    <div class="col-sm-4 hidden-xs">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div style="font-size: 16px" class="col-sm-4 col-6 text-sm-right">
                                        <address>
                                            <strong>Invoice Date:</strong><br>
                                            <?php echo e(date('d-m-Y', strtotime($invoice['invoice_date']))); ?><br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6 hidden-xs">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Sales summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Sales # <?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?></h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-nowrap">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th style="width: 70px;">No.</th>
                                                <th>Item</th>
                                                <th class="text-right">Rate</th>
                                                <th class="text-right">Quantity</th>
                                                <th class="text-center">Unit</th>
                                                <th class="text-right">Discount</th>
                                                <th class="text-right">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">

                                            <?php if(!empty($entries) && ($entries->count() > 0)): ?>

                                            <?php $sub_total = 0; ?>

                                            <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($key + 1); ?></td>
                                                <td>
                                                    <?php if($value['product_type'] == 1): ?>
                                                        <?php echo $value['product_entry_name']; ?>
                                                    <?php else: ?>
                                                        <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td class="text-right"><?php echo e(number_format($value['rate'],2,'.',',')); ?></td>
                                                <td class="text-right"><?php echo e(number_format($value['quantity'],2,'.',',')); ?></td>
                                                <td class="text-center"><?php echo e($value->convertedUnit->name); ?></td>
                                                <td class="text-right">
                                                    <?php $total_dis = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; ?>
                                                    <?php if($value['discount_type'] == 0): ?>
                                                        <?php echo number_format($total_dis,2,'.',','); ?><br>
                                                        <?php echo '('.$value['discount_amount'].'%'.')'; ?>
                                                    <?php else: ?>
                                                        <?php echo number_format($value['discount_amount'],2,'.',','); ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td class="text-right"><?php echo e(number_format($value['total_amount'],2,'.',',')); ?></td>
                                            </tr>

                                            <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                            
                                            <tr>
                                                <td style="font-size: 16px" colspan="6" class="text-right">Sub Total</td>
                                                <td style="font-size: 16px" class="text-right"><?php echo e(number_format($sub_total,2,'.',',')); ?></td>
                                            </tr>

                                            <tr>
                                                <td style="font-size: 16px" colspan="6" class="text-right">VAT (<?php echo e($invoice['vat_type'] == 0 ? $invoice['total_vat'].'%' : 'BDT'); ?>)</td>
                                                <td style="font-size: 16px" class="text-right"><?php echo e($invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : number_format($invoice['total_vat'],2,'.',',')); ?></td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px" colspan="6" class="border-0 text-right">
                                                    Discount On Total Amount (<?php echo e($invoice['total_discount_type'] == 0 ? $invoice['total_discount_amount'].'%' : 'BDT'); ?>)</td>
                                                <td style="font-size: 16px" class="border-0 text-right"><?php echo e($invoice['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$invoice['total_vat'])/100))*$invoice['total_discount_amount'])/100) : number_format($invoice['total_discount_amount'],2,'.',',')); ?></td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="6" class="border-0 text-right">Total</td>
                                                <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                    <?php echo e(number_format($invoice['invoice_amount'],2,'.',',')); ?>

                                                </td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px" colspan="6" class="border-0 text-right">Paid</td>
                                                <td style="font-size: 16px" class="border-0 text-right">
                                                    <?php echo e(number_format($invoice['invoice_amount'] - $invoice['due_amount'],2,'.',',')); ?>

                                                </td>
                                            </tr>
                                            
                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px" colspan="6" class="border-0 text-right">Dues</td>
                                                <td style="font-size: 16px" class="border-0 text-right">
                                                    <?php echo e(number_format($invoice['due_amount'],2,'.',',')); ?>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <?php if($invoice['invoice_note'] != null): ?>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 style="font-size: 16px"><strong>Note :</strong> <?php echo e($invoice['invoice_note']); ?></h6>
                                    </div>
                                </div>
                                <?php endif; ?>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="position: fixed;bottom: 0">
                                    <!-- <h4 class="float-right font-size-16">Order # 12345</h4> -->
                                    <!-- <div class="col-md-4">
                                        <img class="float-left" src="<?php echo e(url('public/az-ai.png')); ?>" alt="logo" height="20"/>
                                    </div>

                                    <div class="col-md-4">
                                        <h2 style="text-align: center"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="line-height: 0px;text-align: center"><?php echo e($user_info['address']); ?></p>
                                        <p style="line-height: 0px;text-align: center"><?php echo e($user_info['contact_number']); ?></p>
                                    </div>

                                    <div class="col-md-4">
                                        <p style="line-height: 18px;text-align: right;padding: 0px">Phone - 01718937082<br>01711418731<br>01711418731</p>
                                    </div> -->
                                    <?php if($user_info['footer_image'] != null): ?>
                                        <img class="float-left" src="<?php echo e(url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image'])); ?>" alt="logo" style="width: 100%" />
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    // $( document ).ready(function() {
    //     javascript:window.print();
    // });

    // window.onafterprint = function(e){
    //     var site_url  = $('.site_url').val();
    //     window.location.replace(site_url + '/invoices');
    // };
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/26/Modules/Invoices/Resources/views/show_pos.blade.php ENDPATH**/ ?>