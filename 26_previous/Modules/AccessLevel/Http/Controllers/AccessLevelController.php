<?php

namespace Modules\AccessLevel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\URLS;
use App\Models\Modules;
use App\Models\Permissions;
use App\Models\ModulesAccess;
use App\Models\Users;
use Response;
use DB;

class AccessLevelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $urls              = Urls::get();
        $url_count         = $urls->count();
        $modules           = Modules::get();

        return view('accesslevel::urls_edit', compact('urls', 'url_count', 'modules'));
    }

    public function update(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'name'          => 'required',
            'url'           => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $url_list               = Urls::where('name','!=', null)->delete();

            foreach ($data['name'] as $key => $value)
            {
                $urls[] = [
                    'name'          => $value,
                    'url'           => $data['url'][$key],
                    'module_id'     => $data['module_id'][$key],
                    'created_by'    => $user_id,
                    'created_at'    => date('Y-m-d H:i:s'),
                ];
            }

            DB::table('urls')->insert($urls);
            DB::commit();

            return back()->with("success","URLS Added Successfully !!");

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function setAccessIndex()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_id            = isset($_GET['user_id']) ? $_GET['user_id'] : 0;
        $urls               = Urls::get();
        $modules            = Modules::orderBy('created_at', 'ASC')->get();
        // $module_permissions = $tables['modules_access']->orderBy('created_at', 'ASC')->get();

        if ($user_id != 0)
        {
            $permission     = Permissions::leftjoin('urls', 'urls.id', 'permissions.url_id')
                                            ->where('permissions.user_id', $user_id)
                                            ->select('permissions.*', 'urls.name as url_name')
                                            ->get();

            $module_access  = ModulesAccess::leftjoin('modules', 'modules.id', 'modules_access.module_id')
                                            ->where('modules_access.user_id', $user_id)
                                            ->select('modules_access.*',
                                                     'modules.id as module_id',
                                                     'modules.name as module_name')
                                            ->get();

            if ($permission->count() == 0)
            {   
                $user_find              = Users::find($user_id);
                $user_role              = $user_find['role'];

                foreach ($urls as $key => $value)
                {
                    $permissions_data[] = [
                        'url_id'        => $value['id'],
                        'user_id'       => $user_id,
                        'access_level'  => $user_role == 1 ? 1 : 0,
                        'created_by'    => Auth::user()->id,
                        'created_at'    => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('permissions')->insert($permissions_data);
            }
            else
            {   
                $user_find              = Users::find($user_id);
                $user_role              = $user_find['role'];
                
                foreach ($urls as $key1 => $value1)
                {   
                    $find_data          = Permissions::where('permissions.user_id', $user_id)
                                                    ->where('permissions.url_id', $value1['id'])
                                                    ->count();

                    if ($find_data == 0)
                    {
                        $permissions_data_new[] = [
                            'url_id'        => $value1['id'],
                            'user_id'       => $user_id,
                            'access_level'  => $user_role == 1 ? 1 : 0,
                            'created_by'    => Auth::user()->id,
                            'created_at'    => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                if (isset( $permissions_data_new))
                {
                     DB::table('permissions')->insert($permissions_data_new);
                }
            }

            if ($module_access->count() == 0)
            {
                foreach ($modules as $key11 => $value11)
                {
                    $moduleAccess[] = [
                        'module_id'     => $value11['id'],
                        'user_id'       => $user_id,
                        'display'       => 1,
                        'created_by'    => Auth::user()->id,
                        'created_at'    => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('modules_access')->insert($moduleAccess);
            }
            else
            {
                foreach ($modules as $key12 => $value12)
                {   
                    $find_module_data   = ModulesAccess::where('modules_access.user_id', $user_id)
                                                    ->where('modules_access.module_id', $value12['id'])
                                                    ->count();

                    if ($find_module_data == 0)
                    {
                        $module_permissions_data_new[] = [
                            'module_id'     => $value12['id'],
                            'user_id'       => $user_id,
                            'display'       => 1,
                            'created_by'    => Auth::user()->id,
                            'created_at'    => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                if (isset( $module_permissions_data_new))
                {
                    DB::table('modules_access')->insert($module_permissions_data_new);
                }
            }

            $permissions        = Permissions::leftjoin('urls', 'urls.id', 'permissions.url_id')
                                            ->where('permissions.user_id', $user_id)
                                            ->select('permissions.*', 'urls.name as url_name', 'urls.module_id as module_id')
                                            ->get();

            $module_permissions = ModulesAccess::leftjoin('modules', 'modules.id', 'modules_access.module_id')
                                            ->where('modules_access.user_id', $user_id)
                                            ->select('modules_access.*', 'modules.name as module_name', 'modules.id as module_id')
                                            ->get();
        }
        else
        {
            $permissions        = [];
            $module_permissions = [];
        }
        
        $users             = Users::get();
        $url_count         = $urls->count();

        return view('accesslevel::set_access_edit', compact('urls', 'url_count', 'permissions', 'users', 'user_id', 'modules', 'module_permissions'));
    }

    public function setAccessUpdate(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'url_id.*'          => 'required',
            'permission.*'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            foreach ($data['url_id'] as $key => $value)
            {   
                $permission_list                = Permissions::where('permissions.user_id', $data['user_id'])
                                                            ->where('permissions.url_id', $value)
                                                            ->first();

                $permission_list->access_level  = $data['permission'][$key];
                $permission_list->save();
            }

            foreach ($data['module_id'] as $key1 => $value1)
            {   
                $module_permission_list                 = ModulesAccess::where('modules_access.user_id', $data['user_id'])
                                                            ->where('modules_access.module_id', $value1)
                                                            ->first();

                $module_permission_list->display        = $data['module_permission'][$key1];
                $module_permission_list->save();
            }

            DB::commit();
            return back()->with("success","Permissions Added Successfully !!");

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }
}
