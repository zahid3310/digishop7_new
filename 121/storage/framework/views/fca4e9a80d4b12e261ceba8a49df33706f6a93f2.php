<!DOCTYPE html>
<html>
<head>
    <title>Bank Book</title>

    <link rel="icon" href="<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/bk_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/bk_assets/css/style.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/bk_assets/css/custom.css')); ?>">

    <script type="text/javascript">
        function printDiv(divName) {
            var printContents       = document.getElementById(divName).innerHTML;
            var originalContents    = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
</head>

<style type="text/css" media="print">
    @page  {
        size: auto;   /* auto is the initial value */
        margin: 15mm 5mm 5mm 10mm;   /* this affects the margin in the printer settings */
    }
</style>

<body id="print-container-body">
    <input style="float:right" type="button" onclick="printDiv('printableArea')" value="Print" />
    <div id="printableArea">
        <div style="display: none;">
            <button id="btnExport">Export to excel</button>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="" style="">
                            <div style="width:20%;">
                                <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                            </div>

                            <div class="company-head" style="text-align: center; min-height: 82px;">
                                <h3><strong><?php echo e(userDetails()->organization_name); ?></strong></h3>
                                <p><?php echo e(userDetails()->address); ?></p>
                                <p><?php echo e(userDetails()->contact_number); ?></p>
                                <p><?php echo e(userDetails()->contact_email); ?></p>
                                <p><?php echo e(userDetails()->website); ?></p>
                                <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>              
                            </div>
                            
                            <div class="ibox-title" style="height:25px;">
                              <h6 style="font-size: 13px; margin-top: 0px;">BANK BOOK</h6>
                            </div>

                            <div class="ibox-content">
                                <table class="table table-striped table-hover dataTables-example topics" style="margin-bottom: 5px;">
                                    <tbody class="top-header">
                                        <?php if($data['acc_head'] == null): ?>
                                        <tr>
                                            <td colspan="6" style="border-top: none;">DATE FROM : <?php echo e(date('d-m-Y', strtotime($data['from_date'])) . ' To ' . date('d-m-Y', strtotime($data['to_date']))); ?></td>
                                            <td colspan="6" style="text-align:right; border-top: none;">BALANCE : <?php echo e(number_format($data['balance'],2,'.',',')); ?></td>
                                        </tr>
                                        <?php else: ?>
                                        <tr>
                                            <td colspan="6" style="border-top: none;">DATE FROM : <?php echo e(date('d-m-Y', strtotime($data['from_date'])) . ' To ' . date('d-m-Y', strtotime($data['to_date']))); ?></td>
                                            <td colspan="6" style="text-align:right; border-top: none;"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="6" style="border-top: none;">ACCOUNT# : <?php echo e($data['acc_head']['HeadName']); ?></td>
                                            <td colspan="6" style="text-align:right; border-top: none;">BALANCE : <?php echo e(number_format($data['balance'],2,'.',',')); ?></td>
                                        </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                                    <thead class="theight">
                                        <tr>
                                            <th style="width: 5%; text-align: center;">SL#</th>
                                            <th style="width: 10%; text-align: center;">DATE</th>
                                            <th style="width: 15%; text-align: center;">ACCOUNTS HEAD NAME</th>
                                            <th style="width: 15%; text-align: center;">PROJECT</th>
                                            <th style="width: 15%; text-align: center;">REGISTER</th>
                                            <th style="width: 10%; text-align: center;">PARTICULARS</th>
                                            <th style="width: 10%; text-align: center;">VR.NO</th>
                                            <th style="width: 10%; text-align: center;">RECEIPT</th>
                                            <th style="width: 10%; text-align: center;">PAYMENT</th>
                                        </tr>
                                    </thead>
                                  
                                    <tbody class="theight">
                                        <tr class="gradeC">
                                            <td style="text-align: right;" colspan="7">Opening Balance =</td>
                                            <td style="text-align: right;" colspan="2"><?php echo e(number_format(0,2,'.',',')); ?></td>
                                        </tr>

                                        <?php
                                            $total_receipt = 0;
                                            $total_payment = 0;
                                        ?>
                                        <?php if($data['result']->count() > 0): ?>
                                        <?php $__currentLoopData = $data['result']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="gradeC">
                                            <td style="text-align: center;"><?php echo e($key + 1); ?></td>
                                            <td style="text-align: center;"><?php echo e(date('d-m-Y', strtotime($result->VoucherDate))); ?></td>
                                            <td style="text-align: left;"><?php echo e($result->cbaHeadName->HeadName); ?></td>
                                            <td style="text-align: left;"><?php echo e($result->ProjectID != null ? $result->projectName->ProjectName : ''); ?></td>
                                            <td style="text-align: left;"><?php echo e($result->RegisterID != null ?  $result->registerName->ClientName : ''); ?></td>
                                            <td style="text-align: left;"><?php echo e($result->Particulars); ?></td>
                                            <td style="text-align: center;"><?php echo e($result->VoucherType. '/' . $result->VoucherNumber); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($result->DebitAmount,2,'.',',')); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($result->CreditAmount,2,'.',',')); ?></td>
                                        </tr>

                                        <?php
                                            $total_receipt = $total_receipt + $result->DebitAmount;
                                            $total_payment = $total_payment + $result->CreditAmount;
                                        ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                
                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="7" style="text-align: right;">SUB TOTAL =</th>
                                            <th colspan="1" style="text-align: right;"><?php echo e(number_format($total_receipt,2,'.',',')); ?></th>
                                            <th colspan="1" style="text-align: right;"><?php echo e(number_format($total_payment,2,'.',',')); ?></th>
                                        </tr>

                                        <tr>
                                            <th colspan="7" style="text-align: right;">BALANCE =</th>
                                            <th colspan="2" style="text-align: right;"><?php echo e(number_format($total_receipt - $total_payment,2,'.',',')); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>

                                <table class="table table-striped table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <td style="font-size:8px;">&copy; <?php echo date("Y"); ?> <a style="color: black" target="blank" href="http://www.cyberdynetechnologyltd.com">Cyberdyne Technology Ltd. | Contact: 01715-515755 </a></td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="<?php echo e(url('public/bk_assets/js/jquery-2.1.1.js')); ?>"></script>
        <script src="<?php echo e(url('public/bk_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
        <script src="<?php echo e(url('public/bk_assets/js/jquery.base64.js')); ?>"></script>

        <script type="text/javascript">
            var isCtrl = false;$(document).keyup(function (e) {
            if(e.which == 17) isCtrl=false;
            }).keydown(function (e) {
                if(e.which == 17) isCtrl=true;
                if(e.which == 69 && isCtrl == true) {
                    $("#tblExport").btechco_excelexport({
                        containerid: "tblExport"
                       , datatype: $datatype.Table
                    });
                    return false;
                }
            });
        </script>
    </div>
</body>
</html><?php /**PATH /home/digishop7/public_html/121/Modules/AccountsReport/Resources/views/bankBook/print.blade.php ENDPATH**/ ?>