<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="<?php echo e(route('home')); ?>" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="">
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>Admin</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="" href="<?php echo e(route('branch_edit', 1)); ?>">Company Info</a> </li>
                        <li> <a class="" href="<?php echo e(route('chart_of_accounts_index')); ?>">Chart of Accounts Head</a> </li>
                        <li> <a class="" href="<?php echo e(route('chart_of_projects_index')); ?>">Chart of Projects</a> </li>
                        <li> <a class="" href="<?php echo e(route('chart_of_registers_index')); ?>">Chart of Registers</a> </li>
                        <!-- <li> <a class="" href="#">Finance Year</a> </li> -->
                        <!-- <li> <a class="" href="#">Database Backup</a> </li> -->
                    </ul>
                </li>

                <li class="">
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-file-invoice-dollar"></i><span>My Account</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="" href="<?php echo e(route('accounts_report_current_balance_index')); ?>" target="_blank">Current Balance</a> </li>
                        <!-- <li> <a class="" href="#">Personal Statement</a> </li> -->
                        <!-- <li> <a class="" href="#">All Statement</a> </li> -->
                    </ul>
                </li>

                <li class="<?php echo e(Route::currentRouteName() == 'cash_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'cash_receipt_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'bank_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'contra_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'journal_voucher_edit' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Route::currentRouteName() == 'cash_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'cash_receipt_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'bank_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'contra_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'journal_voucher_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fab fa-adn"></i><span>Accounts</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="" href="<?php echo e(route('cash_payment_voucher_index')); ?>">Cash Payment Voucher</a> </li>
                        <li> <a class="" href="<?php echo e(route('cash_receipt_voucher_index')); ?>">Cash Receipt Voucher</a> </li>
                        <li> <a class="" href="<?php echo e(route('bank_payment_voucher_index')); ?>">Bank Payment Voucher</a> </li>
                        <li> <a class="" href="<?php echo e(route('bank_receipt_voucher_index')); ?>">Bank Receipt Voucher</a> </li>
                        <li> <a class="" href="<?php echo e(route('journal_voucher_index')); ?>">Journal Voucher</a> </li>
                        <li> <a class="" href="<?php echo e(route('contra_voucher_index')); ?>">Contra Voucher</a> </li>
                        <!-- <li> <a class="" href="#">Cheque State Change</a> </li> -->
                        <!-- <li> <a class="" href="#">Bank Reconcilation</a> </li> -->
                        <li class=""> <a class="<?php echo e(Route::currentRouteName() == 'cash_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'cash_receipt_voucher_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'bank_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'contra_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'journal_voucher_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('voucher_posting_index')); ?>">Voucher Posting</a> </li>
                        <li> <a class="" href="<?php echo e(route('list_of_voucher')); ?>">List of Voucher</a> </li>
                    </ul>
                </li>

                <li class="">
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>Account Reports</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="" href="<?php echo e(route('accounts_report_accounts_voucher_index')); ?>">Accounts Voucher</a> </li>
                        <li> <a class="" href="<?php echo e(route('accounts_report_cash_book_index')); ?>">Cash Book</a> </li>
                        <li> <a class="" href="<?php echo e(route('accounts_report_bank_book_index')); ?>">Bank Book</a> </li>
                        <!-- <li> <a class="" href="#">Journal Book</a> </li> -->
                        <li> <a class="" href="<?php echo e(route('accounts_report_ledger_book_index')); ?>">Ledger Book</a> </li>
                        <!-- <li> <a class="" href="#">Detail Annual Phase Cost</a> </li> -->
                        <!-- <li> <a class="" href="#">Daily Transaction</a> </li> -->
                        <!-- <li> <a class="" href="#">Transaction of Cheque</a> </li> -->
                        <!-- <li> <a class="" href="#">Bank Reconcilation</a> </li> -->
                        <!-- <li> <a class="" href="#">Project Wise Summary</a> </li> -->
                        <!-- <li> <a class="" href="#">Register Wise Summary</a> </li> -->
                        <!-- <li> <a class="" href="#">Accounts Wise Summary</a> </li> -->
                        <!-- <li> <a class="" href="#">Transactions of Voucher</a> </li> -->
                        <!-- <li> <a class="" href="#">Trial Balance</a> </li> -->
                        <!-- <li> <a class="" href="#">Trial Balance With Trns</a> </li> -->
                        <!-- <li> <a class="" href="#">Balance Sheet</a> </li> -->
                        <!-- <li> <a class="" href="#">Income Statement</a> </li> -->
                    </ul>
                </li>

                <li style="display: none;" class="">
                    <a class="has-arrow waves-effect">
                       <i class="fas fa-strikethrough"></i><span>Payroll</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="#">List of Salary Sheet</a> </li>
                        <li> <a href="#">Process Salary Sheet</a> </li>
                        <li class="">
                            <a class="has-arrow waves-effect">
                                Grades
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="" href="#">List of Grades</a> </li>
                                <li> <a href="#">New Grade</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Salary Statements
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="" href="#">List of Statements</a> </li>
                                <li> <a href="#">New Statement</a> </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="">
                    <a class="has-arrow waves-effect">
                        <i class="far fa-user-circle"></i><span>User Management</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="" href="<?php echo e(route('users_index')); ?>">Add User</a> </li>
                        <li> <a class="" href="<?php echo e(route('users_index_all')); ?>">User List</a> </li>
                    </ul>
                </li>

                <li class="">
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-user-alt-slash"></i><span>Roles & Pesmissions</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <!-- <li> <a class="" href="#">Add Role</a> </li> -->
                        <!-- <li> <a class="" href="#">Role List</a> </li> -->
                        <li> <a class="" href="<?php echo e(route('set_access_index')); ?>">Permission</a> </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div><?php /**PATH /home/digishop7/public_html/121/resources/views/layouts/headers.blade.php ENDPATH**/ ?>