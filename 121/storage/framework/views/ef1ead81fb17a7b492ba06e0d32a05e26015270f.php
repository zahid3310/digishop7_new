

<?php $__env->startSection('title', 'Dashboard'); ?>

<style type="text/css">
    .card-counter{
        box-shadow: 2px 2px 10px #DADADA;
        margin: 5px;
        padding: 20px 10px;
        background-color: #fff;
        height: 100px;
        border-radius: 5px;
        transition: .3s linear all;
    }

    .card-counter:hover{
        box-shadow: 4px 4px 20px #DADADA;
        transition: .3s linear all;
    }

    .card-counter.primary{
        background-color: #007bff;
        color: #FFF;
    }

    .card-counter.danger{
        background-color: #ef5350;
        color: #FFF;
    }  

    .card-counter.success{
        background-color: #66bb6a;
        color: #FFF;
    }  

    .card-counter.info{
        background-color: #26c6da;
        color: #FFF;
    }  

    .card-counter i{
        font-size: 5em;
        opacity: 0.2;
    }

    .card-counter .count-numbers{
        position: absolute;
        right: 35px;
        top: 20px;
        font-size: 32px;
        display: block;
    }

    .card-counter .count-name{
        position: absolute;
        right: 35px;
        top: 65px;
        font-style: italic;
        text-transform: capitalize;
        display: block;
        font-size: 18px;
    }

    @media  screen and (max-width: 48em) {
        .row-offcanvas {
            position: relative;
            -webkit-transition: all 0.25s ease-out;
            -moz-transition: all 0.25s ease-out;
            transition: all 0.25s ease-out;
        }

        .row-offcanvas-left .sidebar-offcanvas {
            left: -33%;
        }

        .row-offcanvas-left.active {
            left: 33%;
            margin-left: -6px;
        }

        .sidebar-offcanvas {
            position: absolute;
            top: 0;
            width: 33%;
            height: 100%;
        }
    }

    @media  screen and (max-width: 34em) {
        .row-offcanvas-left .sidebar-offcanvas {
            left: -45%;
        }

        .row-offcanvas-left.active {
            left: 45%;
            margin-left: -6px;
        }

        .sidebar-offcanvas {
            width: 45%;
        }
    }
    
    .card {
        overflow: hidden;
    }
    
    .card-block .rotate {
        z-index: 8;
        float: right;
        height: 100%;
    }
    
    .card-block .rotate i {
        color: rgba(20, 20, 20, 0.15);
        position: absolute;
        left: 0;
        left: auto;
        right: -10px;
        bottom: 0;
        display: block;
        -webkit-transform: rotate(-44deg);
        -moz-transform: rotate(-44deg);
        -o-transform: rotate(-44deg);
        -ms-transform: rotate(-44deg);
        transform: rotate(-44deg);
    }

    .display-1 {
        font-size: 2rem !important;
        color: white !important;
    }

    a, button {
        outline: 0!important;
        /*display: none;*/
    }

    .canvasjs-chart-credit
    {
        display: none !important;
    }
</style>

<?php $__env->startSection('content'); ?>
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Dashboard</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-md-3">
                    <div class="card-counter success">
                        <i class="fa fa-database"></i>
                        <span id="totalUnpostedVoucher" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Total Unposted Voucher</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter info">
                        <i class="fa fa-users"></i>
                        <span id="todaysPostedVoucher" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Posted Voucher</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter danger">
                        <i class="fa fa-database"></i>
                        <span id="todaysUnpostedVoucher" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Unposted Voucher</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter info">
                        <i class="fa fa-users"></i>
                        <span id="customerDues" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">SMS Balance</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter info">
                        <i class="fa fa-users"></i>
                        <span id="todaysCashReceipt" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Cash Receipt</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter success">
                        <i class="fa fa-database"></i>
                        <span id="todaysCashPayment" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Cash Payment</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter success">
                        <i class="fa fa-database"></i>
                        <span id="todaysBankReceipt" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Bank Receipt</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter primary">
                        <i class="fa fa-envelope"></i>

                        <span id="todaysBankPayment" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Bank Payment</span>
                    </div>
                </div>

                <!-- <div class="col-md-3">
                    <div class="card-counter danger">
                        <i class="fa fa-database"></i>
                        <span id="totalCustomers" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Total Customers</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter danger">
                        <i class="fa fa-database"></i>
                        <span id="totalSuppliers" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Total Suppliers</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter primary">
                        <i class="fa fa-users"></i>
                        <span id="totalInvoices" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Total Invoices</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter primary">
                        <i class="fa fa-users"></i>
                        <span id="totalBills" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Total Bills</span>
                    </div>
                </div> -->
            </div>
            
            <br>
            
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4 float-sm-left">Monthly Receipt Summary</h4>
                            <div class="clearfix"></div>
                            <div id="chartContainer4" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4 float-sm-left">Monthly Payment Summary</h4>
                            <div class="clearfix"></div>
                            <div id="chartContainer5" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4 float-sm-left">Account Summary</h4>
                            <div class="clearfix"></div>
                            <div id="chartContainer3" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>

                <!--<div class="col-lg-12">-->
                <!--    <div class="card">-->
                <!--        <div class="card-body">-->
                <!--            <h4 class="card-title mb-4 float-sm-left">Project Wise Payment</h4>-->
                <!--            <div class="clearfix"></div>-->
                <!--            <div id="chartContainer6" style="height: 370px; width: 100%;"></div>-->
                <!--        </div>-->
                <!--    </div>-->
                <!--</div>-->
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            var site_url  = $('.site_url').val();

            var site_url  = $('.site_url').val();

            $.get(site_url + '/dashboard/items', function(data){

                $("#totalUnpostedVoucher").html(data.total_unposted_voucher);
                $("#todaysPostedVoucher").html(data.todays_posted_voucher);
                $("#todaysUnpostedVoucher").html(data.todays_unposted_voucher);
                $("#todaysCashReceipt").html(parseFloat(data.todays_cash_receipt));
                $("#todaysCashPayment").html(parseFloat(data.todays_cash_payment));
                $("#todaysBankReceipt").html(parseFloat(data.todays_bank_receipt));
                $("#todaysBankPayment").html(parseFloat(data.todays_bank_payment));

                var myChart = echarts.init(document.getElementById('chartContainer3'));
                var option;
                option = {
                    legend: {},
                    tooltip: {},
                    dataset: {
                        source: [
                            ['JAN', 50000000, 35000000],
                            ['FEB', 70000000, 65000000],
                            ['MAR', 60000000, 55000000],
                            ['APR', 30000000, 15000000],
                            ['MAY', 50000000, 50000000],
                            ['JUN', 50000000, 50000000],
                            ['JULY', 60000000, 40000000],
                            ['AUG', 60000000, 58000000],
                            ['SEP', 50000000, 35000000],
                            ['OCT', 70000000, 65000000],
                            ['NOV', 60000000, 55000000],
                            ['DEC', 30000000, 15000000]
                        ]
                    },
    
                    xAxis: [
                        { type: 'category', gridIndex: 0 },
                        { type: 'category', gridIndex: 1 }
                    ],
    
                    yAxis: [{ gridIndex: 0 }, { gridIndex: 1 }],
                    grid: [{ bottom: '0%' }, { top: '10%' }],
    
                    series: [
                        // These series are in the second grid.
                        { name: 'Payable', type: 'bar', xAxisIndex: 1, yAxisIndex: 1 },
                        { name: 'Receivable', type: 'bar', xAxisIndex: 1, yAxisIndex: 1 }
                    ]
                };
    
                option && myChart.setOption(option);
    
                var myChart1 = echarts.init(document.getElementById('chartContainer4'));
                var option1;
                option1 = {
                    title: {
                        // text: 'Referer of a Website',
                        // subtext: 'Fake Data',
                        left: 'center'
                    },
    
                    tooltip: {
                        trigger: 'item'
                    },
    
                    legend: {
                        orient: 'vertical',
                        left: 'right'
                    },
    
                    series: [
                        {
                            name: 'Receipt Summary',
                            type: 'pie',
                            radius: '50%',
                            data: [
                                { value: parseFloat(data.this_month_cash_receipt), name: 'Cash' },
                                { value: parseFloat(data.this_month_bank_receipt), name: 'Bank' }
                            ],
                            emphasis: {
                            itemStyle: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                          }
                        }
                    ]
                };
    
                option1 && myChart1.setOption(option1);
    
                var myChart2 = echarts.init(document.getElementById('chartContainer5'));
                var option2;
                option2 = {
                    title: {
                        // text: 'Referer of a Website',
                        // subtext: 'Fake Data',
                        left: 'center'
                    },
    
                    tooltip: {
                        trigger: 'item'
                    },
    
                    legend: {
                        orient: 'vertical',
                        left: 'right'
                    },
    
                    series: [
                        {
                            name: 'Payment Summary',
                            type: 'pie',
                            radius: '50%',
                            data: [
                                { value: parseFloat(data.this_month_cash_payment), name: 'Cash' },
                                { value: parseFloat(data.this_month_bank_payment), name: 'Bank' }
                            ],
                            emphasis: {
                            itemStyle: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                          }
                        }
                    ]
                };
    
                option2 && myChart2.setOption(option2);
    
                var myChart3 = echarts.init(document.getElementById('chartContainer6'));
                var option3;
                option3 = {
                  tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                      // Use axis to trigger tooltip
                      type: 'shadow' // 'shadow' as default; can also be 'line' or 'shadow'
                    }
                  },
                  legend: {},
                  grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                  },
                  xAxis: {
                    type: 'value'
                  },
                  yAxis: {
                    type: 'category',
                    data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                  },
                  series: [
                    {
                      name: 'Project - 01',
                      type: 'bar',
                      stack: 'total',
                      label: {
                        show: true
                      },
                      emphasis: {
                        focus: 'series'
                      },
                      data: [320, 302, 301, 334, 390, 330, 320]
                    },
                    {
                      name: 'Project - 02',
                      type: 'bar',
                      stack: 'total',
                      label: {
                        show: true
                      },
                      emphasis: {
                        focus: 'series'
                      },
                      data: [120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                      name: 'Project - 03',
                      type: 'bar',
                      stack: 'total',
                      label: {
                        show: true
                      },
                      emphasis: {
                        focus: 'series'
                      },
                      data: [220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                      name: 'Project - 05',
                      type: 'bar',
                      stack: 'total',
                      label: {
                        show: true
                      },
                      emphasis: {
                        focus: 'series'
                      },
                      data: [150, 212, 201, 154, 190, 330, 410]
                    },
                    {
                      name: 'Project - 06',
                      type: 'bar',
                      stack: 'total',
                      label: {
                        show: true
                      },
                      emphasis: {
                        focus: 'series'
                      },
                      data: [820, 832, 901, 934, 1290, 1330, 1320]
                    }
                  ]
                };
    
                option3 && myChart3.setOption(option3);
            });
        });
    
        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
    <!-- echarts js -->
    <script src="<?php echo e(url('public/admin_panel_assets/libs/echarts/echarts.min.js')); ?>"></script>
    <!-- echarts init -->
    <script src="<?php echo e(url('public/admin_panel_assets/js/pages/echarts.init.js')); ?>"></script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/121/resources/views/home.blade.php ENDPATH**/ ?>