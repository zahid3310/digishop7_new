@extends('layouts.app')

@section('title', 'Chart of Accounts')

<link href="{{ url('public/tree-plugin/jsTree/style.min.css') }}" rel="stylesheet">

<style type="text/css">
    #treePane ul {
        list-style: none !important;
    }

    #treePane .node {
        color: red !important;
        background-image: url("{{url('public/tree-plugin/jsTree/plus.gif')}}");
        background-position: left center !important;
        background-repeat: no-repeat !important;
        padding-left: 12px !important;
        cursor: pointer !important;
    }

    #treePane .open {
        background-image: url("{{url('public/tree-plugin/jsTree/minus.gif')}}");
    }

    #treePane ul li {
        background-image: url("{{url('public/tree-plugin/jsTree/treeview-default-line.gif')}}");
        background-repeat: no-repeat !important;
        padding-left: 20px !important;
        margin-left: -42px !important;
    }

    .custom-menu {
        /*display: none !important;*/
        z-index: 1000 !important;
        position: absolute !important;
        overflow: hidden !important;
        border: 1px solid #CCC !important;
        white-space: nowrap !important;
        font-family: sans-serif !important;
        background: #FFF !important;
        color: #333 !important;
        border-radius: 5px !important;
        padding: 0 !important;
    }

    .custom-menuT {
        /*display: none !important;*/
        z-index: 1000 !important;
        position: absolute !important;
        overflow: hidden !important;
        border: 1px solid #CCC !important;
        white-space: nowrap !important;
        font-family: sans-serif !important;
        background: #FFF !important;
        color: #333 !important;
        border-radius: 5px !important;
        padding: 0 !important;
    }

    /* Each of the items in the list */
    .custom-menu li {
        padding: 8px 12px !important;
        cursor: pointer !important;
        list-style-type: none !important;
    }

    .custom-menuT li {
        padding: 8px 12px !important;
        cursor: pointer !important;
        list-style-type: none !important;
    }

    .custom-menu li:hover {
        background-color: #DEF !important;
    }

    .custom-menuT li:hover {
        background-color: #DEF !important;
    }

    /* TransactableCss */
    .customT-menu {
        display: none !important;
        z-index: 1000 !important;
        position: absolute !important;
        overflow: hidden !important;
        border: 1px solid #CCC !important;
        white-space: nowrap !important;
        font-family: sans-serif !important;
        background: #FFF !important;
        color: #333 !important;
        border-radius: 5px !important;
        padding: 0 !important;
    }

    /* Each of the items in the list */
    .customT-menu li {
        padding: 8px 53px 8px 17px !important;
        cursor: pointer !important;
        list-style-type: none !important;
    }

    .customT-menu li:hover {
        background-color: #DEF !important;
    }

    .modal-content {
        width: 60% !important;
        margin-left: 218px !important;
    }

    .modal-body {
        padding-bottom: 0px !important;
    }
</style>

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Chart of Accounts</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Accounts</a></li>
                                    <li class="breadcrumb-item active">Chart of Accounts</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <div class="row">
                                    <?php function getchild($itemQueryResult, $parentId){ ?>
                                        <ul id="uls_<?php echo $parentId;?>">
                                        <?php foreach($itemQueryResult as $key => $info){
                                            if(($info->PtnID == $parentId)  && ($info->process_status == 0)){

                                                $info->process_status = 1; //processed, If a row use one time next time it will not work

                                                //IsTransactable= 0 = Non-Transactable, 1 = Transactable
                                                if($info->IsTransactable == 0){ ?>
                                                    <li>
                                                        <label id="<?php echo $info->id;?>" class="node" onmousedown="mouseDown(<?php echo $info->id;?>,<?php echo '\''.$info->GroupID.'\'';?>,<?php echo '\''.$info->HeadName.'\'';?>)">

                                                            <?php
                                                                if ($info->code != null)
                                                                {
                                                                    echo $info->HeadName;
                                                                }
                                                                else
                                                                {
                                                                    echo $info->HeadName;
                                                                }
                                                            ?>
                                                        </label>

                                                        <?php getchild($itemQueryResult->sortBy('HeadName'), $info->id);?>

                                                    </li><?php  
                                                }
                                                else{?>
                                                    <li id="<?php echo $info->id;?>" onmousedown="mouseDownT(<?php echo $info->id;?>,<?php echo '\''.$info->GroupID.'\'';?>,<?php echo '\''.$info->HeadName.'\'';?>)">

                                                        <?php
                                                            if ($info->code != null)
                                                            {
                                                                echo $info->HeadName;
                                                            }
                                                            else
                                                            {
                                                                echo $info->HeadName;
                                                            }
                                                        ?>

                                                    </li>
                                                <?php
                                                }
                                            }
                                         } //foreach end
                                        ?>
                                        </ul>
                                    <?php }?>

                                    <div id="treePane">
                                        <ul id="uls_0">
                                            <li>
                                                <?php foreach($itemQueryResult as $key=>$info){?>
                                                    <?php if($info->process_status == 0){ ?>

                                                        <label id="<?php echo $info->id;?>" class="node" onmousedown="mouseDownOFF(<?php echo $info->id;?>)">

                                                            <?php
                                                                if ($info->code != null)
                                                                {
                                                                    echo $info->HeadName;
                                                                }
                                                                else
                                                                {
                                                                    echo $info->HeadName;
                                                                }
                                                            ?>
                                                                
                                                        </label>

                                                        <?php
                                                            $info->process_status = 1; //If a row use one time next time it will not work

                                                            getchild($itemQueryResult, $info->id);
                                                    }
                                                }?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>

    <input type="hidden" id="HeadCompanyID" value="{{ Auth()->user()->branch_id }}"/>

    <!--ContextMenuOptions(Non Translatable) Start-->
    <ul class='custom-menu'>
        <!--<li data-toggle="modal"  class="open-myModal4" href="#myModal4"  >New Transactable Item</li>-->
        <li data-toggle="modal" class="open-nonTransactable" href="#nonTransactable" >New Parent/Control Head</li>
        <li data-toggle="modal" class="open-myModal4" href="#myModal4">New Transactable Head</li>
        <hr style="margin:0px;border-color:#ddd">
        <li data-toggle="modal" class="open-EditNonTransactable" href="#EditNonTransactable" >Edit Parent/Control Head</li> 
        <!-- <li data-toggle="modal"  class="open-Edit_Transactable" href="#Edit_Transactable" >Edit Transactable Head</li> -->
        <!-- <hr style="margin:0px;border-color:#ddd"> -->
        <!-- <li data-toggle="modal" class="open-printList" href="#printList">Print Chart of Accounts Head</li> -->
        <!-- <li data-toggle="modal" class="open-printList" href="#printList1">Print Transactable Accounts Head</li> -->
    </ul>
    <!---ContextMenuOptions(Non Translatable) End-->

    <!--ContextMenuOptions(Non Translatable) Start-->
    <!-- <ul class='custom-menuT'>
        <li data-toggle="modal" class="open-myModal4" href="#myModal4">Transactable Head</li>
        <li data-toggle="modal" class="open-printList" href="#printList">Print Chart of Accounts Head</li>
        <li data-toggle="modal" class="open-printList" href="#printList1">Print Transactable Accounts Head</li>
    </ul> -->
    <!---ContextMenuOptions(Non Translatable) End-->
    
    <!--ContextMenuOptions(Translatable) Start-->
    <ul class='custom-menuT'>
        <li data-toggle="modal" class="open-Edit_Transactable" href="#Edit_Transactable">Edit Transactable Head</li>
        <!-- <li data-toggle="modal" id="Edit_bAccount" class="open-Edit_BankAccount" href="#Edit_BankAccount">Edit Bank Account</li> -->
        <!-- <li data-toggle="modal" class="open-printList" href="#printList">Print This List</li> -->
    </ul>
    <!--ContextMenuOptions(Translatable) End-->
@endsection

@section('scripts')
<script src="{{ url('public/tree-plugin/jsTree/jstree.min.js') }}"></script>

<script type="text/javascript">
    (function() {
        var treePaneNodes = $('#treePane').find(".node");
        treePaneNodes.next("ul").hide();
        $('#treePane').find(".open").next("ul").show();

        treePaneNodes.on('click', function() 
        {
            $(this).next("ul").first().toggle();
            $(this).toggleClass("open");
        })
    })();
</script>

<script type="text/javascript">
    // JAVASCRIPT (jQuery)
    //GlobalVariableDeclaration 
    //DataFromNonTransactable
    var non_ptnID;
    var non_GroupID;
    var non_tra_HeadName;
    var non_Transactable_HeadName;
    var HeadCompanyID   = $('#HeadCompanyID').val();

    //DataFromTransactable//`HeadID``HeadName``PtnID``PtnGroupCode``GroupID``CompanyID``IsTransactable`
    var tra_HeadID;
    // Trigger action when the contexmenu is about to be shown

    function mouseDown(PtnID,GroupID,HeadName) {
        //non_ptnID = "";
        non_ptnID                   = PtnID;
        non_GroupID                 = GroupID;
        non_tra_HeadName            = HeadName;
        non_Transactable_HeadName   = "";
        $('#'+PtnID).bind("contextmenu", function (event) {
        
            // Avoid the real one
            event.preventDefault();
            
            // Show contextmenu
            $(".custom-menu").finish().toggle(100).
            
            // In the right position (the mouse)
            css({
                top: event.pageY-25 + "px",
                left: event.pageX+20 + "px"
            });    
        });
    }

    // If the document is clicked somewhere
    $(document).bind("mousedown", function (e) {
        
        // If the clicked element is not the menu
        if (!$(e.target).parents(".custom-menu").length > 0) {
            
            // Hide it
            $(".custom-menu").hide(100);
        }
        //$(document).click(function(){$(".custom-menu").hide(100);});
    });

    $(document).bind("mousedown", function (e) {
        
        // If the clicked element is not the menu
        if (!$(e.target).parents(".custom-menuT").length > 0) {
            
            // Hide it
            $(".custom-menuT").hide(100);
        }
        //$(document).click(function(){$(".customT-menu").hide(100);});
    });

    // If the menu element is clicked
    $(".custom-menu li").click(function(){
        
        // This is the triggered action name
        switch($(this).attr("data-action")) {
            
            // A case for each action. Your actions here
            case "first": alert(id); break;
            case "second": alert("second"); break;
            case "third": alert("third"); break;
        }
      
        // Hide it AFTER the action was triggered
        $(".custom-menu").hide(100);
    });
      
    //Transanctable
    // Trigger action when the contexmenu is about to be shown
    function mouseDownT(HeadID, PtnID) {
        //alert(value);
        tra_HeadID  = HeadID;
        non_ptnID   = PtnID;

        $('#'+HeadID).bind("contextmenu", function (event) {
            // Avoid the real one
            event.preventDefault();
            
            // Show contextmenu
            $(".custom-menuT").finish().toggle(100).
          
            // In the right position (the mouse)
            css({
                top: event.pageY-25 + "px",
                left: event.pageX+20 + "px"
            });
        });
       //var editBank = $("#uls_33").val();
    }

    // If the document is clicked somewhere
    $(document).bind("mousedown", function (e) {
      
        // If the clicked element is not the menu
        if (!$(e.target).parents(".custom-menuT").length > 0) {
            
            // Hide it
            $(".custom-menuT").hide(100);
        }
        //$(document).click(function(){$(".customT-menu").hide(100);});
    });

    // If the menu element is clicked
    $(".custom-menuT li").click(function(){
    
        // This is the triggered action name
        switch($(this).attr("data-action")) {
            
            // A case for each action. Your actions here
            case "first": alert(id); break;
            case "second": alert("second"); break;
            case "third": alert("third"); break;
        }
      
        // Hide it AFTER the action was triggered
        $(".custom-menuT").hide(100);
    }); 
</script>

<!--TrsansactableModalCodeSection Start-->
    <script type="text/javascript">
        //javaScriptForTransactablevar non_ptnID;   var non_GroupID;
        $(document).on("click", ".open-myModal4", function () {
            document.getElementById("notesmodal").reset();
            $("#purpose-head>option:selected").text("");
            $("#purpose-head>option:selected").val("");

             $(".modal-body #new_tra_PtnID").val( non_ptnID );
             $(".modal-body #new_tra_GroupID").val( non_GroupID );
             $(".modal-body #Tra_HeadName").val( non_tra_HeadName );
             $(".modal-body #new_tra_HeadName").val( non_Transactable_HeadName );
             $(".modal-body #new_tra_HeadCompanyID").val( HeadCompanyID );
             $(".modal-body #new_bank_HeadCompanyID").val( HeadCompanyID );
             
            // alert(non_tra_HeadName);
            if(non_tra_HeadName == 'Cash In Hand') {
                $("#purpose-head>option:selected").text(non_tra_HeadName);
                $("#purpose-head>option:selected").val('cash_ac');
            }else if(non_tra_HeadName == 'Cash At Bank'){
                $("#purpose-head>option:selected").text(non_tra_HeadName);
                $("#purpose-head>option:selected").val('bank_ac');
            }else {
                $("#purpose-head>option:selected").text("Others");
                $("#purpose-head>option:selected").val("others");
            }           
        });
    </script>

    <div id="myModal4" class="modal inmodal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">New Transactable Head Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="form-horizontal form_class_new_trans" id="notesmodal" role="form" method="get" enctype="multipart/form-data">   
                  
                    <div class="modal-body">
                        <input type="hidden" name="BranchID" id="new_tra_HeadCompanyID">
                        <input type="hidden" name="new_tra_PtnID" id="new_tra_PtnID" class="form-control"> 
                        <input type="hidden" name="is_transactable" id="is_transactable" value="1" class="form-control">
                        <input type="hidden" name="type" id="type" value="1" class="form-control">

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Head Name</label>
                            <div class="col-lg-7">
                                <input type="text" name="new_tra_HeadName" id="new_tra_HeadName"  placeholder="Please input Item Name" class="form-control" required> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Code</label>
                            <div class="col-lg-7">
                                <input type="text" name="tran_code" id="tran_code"  placeholder="Please input Code" class="form-control"> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Position</label>
                            <div class="col-lg-7">
                                <input type="text" name="new_tra_Position" id="new_tra_Position"  placeholder="Please input position" class="form-control"> 
                            </div>
                        </div>

                        <div style="padding: 0px;display: none;" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Opening Debit Amount</label>
                            <div class="col-md-7">
                                <input type="text" name="opening_debit" id="opening_debit"  placeholder="Please input Debit amount" class="form-control"> 
                            </div>
                        </div>

                        <div style="padding: 0px;display: none;" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Opening Credit Amount</label>
                            <div class="col-md-7">
                                <input type="text" name="opening_credit" id="opening_credit"  placeholder="Please input credit amount" class="form-control" > 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Accounts Head Purpose</label>
                            <div class="col-md-7">
                                <select id="purpose-head" name="purpose-head" class="form-control" value="">
                                    <option value="">--Select Name--</option>
                                </select>
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Parent Group</label>
                            <div class="col-md-7">
                                <input type="text" id="Tra_HeadName"  placeholder="" class="form-control" readonly> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Group ID</label>
                            <div class="col-md-7">
                                <input type="text" name="new_tra_GroupID" id="new_tra_GroupID"  placeholder="" class="form-control" readonly> 
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-primary" data-toggle="modal" class="open-new_bank_ac_form" href="#new_bank_ac_form" data-dismiss="modal">New Bank A/C Form</button> -->
                        <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Submit</button>
                        <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <script type="text/javascript">
    jQuery(document).ready(function ($) {
        $("#notesmodal").submit(function () {
            $.ajax({
                type: "GET",
                url: "{!! route('chart_of_accounts_store') !!}",
                data: $('form.form_class_new_trans').serialize(),
                success: function (insert_id) {
                    console.log(JSON.stringify(insert_id));
                    // $("#myModal4").modal('hide');
                    var PtnID               = $('#new_tra_PtnID').val();
                    var nodeName            = $('#new_tra_HeadName').val();
                    document.getElementById("new_tra_HeadName").select();
                    var transactablezType   = 1; //this is only for Transactable Items
                    var ul = document.getElementById("uls_"+PtnID);//getting ul's id
                    
                    if(transactablezType == 1)
                    {
                        var li = document.createElement("li");
                            li.setAttribute("id",insert_id);
                            var functoinStr = "mouseDownT("+ insert_id +")";
                            li.setAttribute("onmousedown", functoinStr);
                            li.appendChild(document.createTextNode(nodeName));
                        ul.appendChild(li);
                    }
                    else
                    {
                        var li = document.createElement("li");
                        
                        var itemLabel = document.createElement("Label");
                            //itemLabel.setAttribute("for", nodeName);
                            itemLabel.innerHTML = nodeName;
                            itemLabel.setAttribute("class","node");
                            itemLabel.setAttribute("id",insert_id);
                            var functoinStr = "mouseDown("+ insert_id +")";
                            itemLabel.setAttribute("onmousedown", functoinStr);
                        li.appendChild(itemLabel);
                        var new_ul = document.createElement("ul");
                            new_ul.setAttribute("id","uls_"+insert_id);
                        li.appendChild(new_ul);
                        
                        ul.appendChild(li);
                    }
                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                }
            });
            return false;
        });
    });
    </script>
<!---------TrsansactableModalCodeSection End------------>

<!--Edit_Transactable modal Start-->
    <script type="text/javascript">
        //javaScriptForEditTransactable 
        $(document).on("click", ".open-Edit_Transactable", function () 
        {
            document.getElementById("EditTransactableForm").reset();
            
            $("#edit_tra_HeadID").val(tra_HeadID);
            
            var site_url   = $(".site_url").val();
            $.getJSON(site_url + "/accounts/chart-of-accounts/edit/" + tra_HeadID, function(data)
            {
                // alert(data.ActiveStatus);
         
                $("#edit_tra_HeadName").val(data.HeadName);
                $("#Edit_tran_code").val(data.code);
                $("#edit_tra_Position").val(data.Position);
                $("#edit_opening_debit").val(data.OpDebitAmount);
                $("#edit_opening_credit").val(data.OpCreditAmount);

                if(data.ActiveStatus == 1)
                {
                    $('input:radio[name="ActiveStatuss"]').filter('[value= 1]').attr('checked', true);
                }
                else
                {
                    $('input:radio[name="ActiveStatuss"]').filter('[value= 0]').attr('checked', true);
                }
                
            });            
        });
    </script>

    <div id="Edit_Transactable" class="modal inmodal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Edit Transactable Head Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="form-horizontal form_class_transactable_edit" id="EditTransactableForm" role="form" method="GET" enctype="multipart/form-data">
                    <div class="modal-body">
                        <input type="hidden" id="edit_tra_HeadID" name="edit_tra_HeadID" />
                        <input type="hidden" name="type" id="type" value="1" class="form-control">

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Head Name</label>

                            <div class="col-lg-7">
                                <input type="text" name="edit_tra_HeadName" id="edit_tra_HeadName" class="form-control" required> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Code</label>
                            <div class="col-lg-7">
                                <input type="text" name="Edit_tran_code" id="Edit_tran_code"  placeholder="Please input Code" class="form-control"> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Position</label>

                            <div class="col-lg-7">
                                <input type="text" name="edit_tra_Position" id="edit_tra_Position"  class="form-control"> 
                            </div>
                        </div>

                        <div style="padding: 0px;display: none" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Edit opening Debit Amount</label>

                            <div class="col-lg-7">
                                <input type="text" name="edit_opening_debit" id="edit_opening_debit"  placeholder="Please input Debit amount" class="form-control" > 
                            </div>
                        </div>

                        <div style="padding: 0px;display: none" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Edit opening Credit Amount</label>

                            <div class="col-lg-7">
                                <input type="text" name="edit_opening_credit" id="edit_opening_credit"  placeholder="Please input credit amount" class="form-control" > 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Active Status</label>
                            <div class="col-lg-7">
                                <div class="inline-radio"><label> <input type="radio" value="1" name="ActiveStatuss"> Active </label></div>
                                <div class="inline-radio"><label> <input type="radio" value="0" name="ActiveStatuss"> Inactive </label></div>
                            </div>
                        </div>
                    
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <input type="submit" name="submit" class="btn btn-primary" id="submitnote" value="Update" />
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    jQuery(document).ready(function ($) {
        $("#EditTransactableForm").submit(function () {
            $.ajax({
                type: "GET",
                url: "{!! route('chart_of_accounts_update') !!}",
                data: $('form.form_class_transactable_edit').serialize(),
                success: function (insert_id) {
                    $("#Edit_Transactable").modal('hide');

                    var PtnID       = $('#edit_tra_HeadID').val();
                    var nodeName    = $('#edit_tra_HeadName').val();
                    
                    document.getElementById(PtnID).innerHTML = nodeName;
                    
                },
                error: function () {
                    alert("failure");
                }
            });
            return false;
        });
    });
    </script>

    <script type="text/javascript">
        //javaScriptForEditTransactable 
        $(document).on("click", ".open-Edit_BankAccount", function () {
            document.getElementById("edit_BankAccount").reset();
            
             $(".ibox-content #edit_tra_HeadID").val( tra_HeadID );
            
            $.getJSON("", function(data) {
                
                //alert(data.OpDebitAmount);
                if(data.notBankAccount == 0) {
                    $("#Edit_BankAccount").modal('hide');
                    alert("This is not a Bank Account!");
                } else {
                    $(".ibox-content #edit_bank_BankName").val( data.BankName );
                    $(".ibox-content #edit_bank_AccountNum").val( data.AccountNum );
                    $(".ibox-content #edit_bank_AccountName").val( data.AccountName );
                    $(".ibox-content #edit_bank_AccountType").val( data.AccountType );
                    $(".ibox-content #edit_bank_BankBranch").val( data.BankBranch );
                    $(".ibox-content #edit_bank_debitBalance").val( data.OpDebitAmount );
                    $(".ibox-content #edit_bank_creditBalance").val( data.OpCreditAmount );
                    $(".ibox-content #accHeadID").val( tra_HeadID );
                    $(".ibox-content #bankID").val( data.id );
                }
            });
             //$(".ibox-content #scheduleName").html('Are you sure you want to delete \'' + id + '\' Schedule ?');            
        });
    </script>
    
    <div class="modal inmodal" id="Edit_BankAccount" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
                <div class="ibox-title">
                    <h5>Edit Transactable Head Form</h5>
                </div>

            <form class="form-horizontal form_class" id="edit_BankAccount" role="form" method="post" enctype="multipart/form-data">
                <div class="ibox-content">
                    <!-- <input type="hidden" id="edit_tra_HeadID" name="edit_tra_HeadID" value="" /> -->
                    <input type="hidden" name="BranchID" id="new_bank_HeadCompanyID" value="">
                    <input type="hidden" name="accHeadID" id="accHeadID" value="">
                    <input type="hidden" name="bankID" id="bankID" value="">
                    <!--<p>Sign in today for more expirience.</p>-->
                    <div class="form-group"><label class="col-lg-4 control-label">Bank Name</label>

                        <div class="col-lg-8"><input type="text" name="edit_bank_BankName" id="edit_bank_BankName"  placeholder="" class="form-control" required> 
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-4 control-label">Account Number</label>

                        <div class="col-lg-8"><input type="text" name="edit_bank_AccountNum" id="edit_bank_AccountNum"  placeholder="" class="form-control" required> 
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-4 control-label">Account Title</label>

                        <div class="col-lg-8"><input type="text" name="edit_bank_AccountName" id="edit_bank_AccountName"  placeholder="" class="form-control" required> 
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-4 control-label">Account Type</label>

                        <div class="col-lg-8">
                            <select class="form-control " name="edit_bank_AccountType" id="edit_bank_AccountType" required>
                                <option value="ATM Card">ATM Card</option>
                                <option value="Credit Card">Credit Card</option>
                                <option value="Current Account">Current Account</option>
                                <option value="Debit Card">Debit Card</option>
                                <option value="Fixed Deposit">Fixed Deposit</option>
                                <option value="Saving Account">Saving Account</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-4 control-label" >Bank Branch</label>

                        <div class="col-lg-8"><input type="text" name="edit_bank_BankBranch" id="edit_bank_BankBranch"  placeholder="" class="form-control" required> 
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-4 control-label" >Opening Debit Balance</label>

                        <div class="col-lg-8"><input type="text" name="edit_bank_debitBalance" id="edit_bank_debitBalance"  placeholder="" class="form-control"> 
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-4 control-label" >Opening Credit Balance</label>

                        <div class="col-lg-8"><input type="text" name="edit_bank_creditBalance" id="edit_bank_creditBalance"  placeholder="" class="form-control"> 
                        </div>
                    </div>
                   
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <input type="submit" name="submit" class="btn btn-primary" id="submitnote" value="Edit" />
                </div>
            </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    jQuery(document).ready(function ($) {
        $("#edit_BankAccount").submit(function () {
            $.ajax({
                type: "POST",
                url: "",
                data: $('form.form_class').serialize(),
                success: function () {
                    $("#Edit_BankAccount").modal('hide');
                    //alert(accountNumber);
                    //$("#"+tra_HeadID).val( accountNumber );
                    //document.getElementById('tra_HeadID').innerHTML = nodeName;
                    var PtnID = $('#accHeadID').val();
                    var nodeName = $('#edit_bank_AccountNum').val();
                    
                    document.getElementById(PtnID).innerHTML = nodeName;

                    
                },
                error: function () {
                    alert("failure");
                }
            });
            return false;
        });
    });
    </script>
<!---------Edit_BankAccount modal End------------>

<!--Non-TrsansactableModalCodeSection Start-->
    <script type="text/javascript">
        $(document).on("click", ".open-nonTransactable", function () {
            document.getElementById("nonTransactableForm").reset();
            
             $(".modal-content #new_non_PtnID").val( non_ptnID );
             $(".modal-content #new_non_GroupID").val( non_GroupID );
             $(".modal-content #new_non_tra_Head").val( non_tra_HeadName );
        });
    </script>

    <div id="nonTransactable" class="modal inmodal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">New Non Transactable Head Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="form-horizontal form_class_new_non_trans" id="nonTransactableForm" role="form" method="get" enctype="multipart/form-data">
                    <div class="modal-body">
                        <input type="hidden" name="new_non_PtnID" id="new_non_PtnID" class="form-control">
                        <input type="hidden" name="is_transactable" id="is_transactable" value="0" class="form-control">
                        <input type="hidden" name="type" id="type" value="0" class="form-control">

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Head Name</label>
                            <div class="col-lg-7">
                                <input type="text" name="new_non_HeadName" id="new_non_HeadName"  placeholder="Please Enter Head Name" class="form-control" required> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Code</label>
                            <div class="col-lg-7">
                                <input type="text" name="Non_code" id="Non_code"  placeholder="Please input Code" class="form-control"> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Position</label>
                            <div class="col-lg-7">
                                <input type="text" name="new_non_Position" id="new_non_Position"  placeholder="Please Enter Position" class="form-control"> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label" >Parent Group</label>
                            <div class="col-lg-7">
                                <input type="text" name="new_non_tra_Head" id="new_non_tra_Head"  placeholder="" class="form-control" readonly> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label" >Group ID</label>
                            <div class="col-lg-7">
                                <input type="text" name="new_non_GroupID" id="new_non_GroupID"  placeholder="" class="form-control" readonly> 
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Submit</button>
                            <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    jQuery(document).ready(function ($) {
        $("#nonTransactableForm").submit(function () {
            $.ajax({
                type: "GET",
                url: "{!! route('chart_of_accounts_store') !!}",
                data: $('form.form_class_new_non_trans').serialize(),
                success: function (insert_id) {
                    // $("#nonTransactable").modal('hide');
                    var PtnID               = $('#new_non_PtnID').val();
                    var nodeName            = $('#new_non_HeadName').val();
                    document.getElementById("new_non_HeadName").select();
                    var nodePosition        = $('#new_non_Position').val();
                    var GroupID             = $('#new_non_GroupID').val();
                    var transactablezType   = 0; //this is only for Non_Transactable Items
                    var ul                  = document.getElementById("uls_"+PtnID);//getting ul's id
                    
                    if(transactablezType == 1)
                    {
                        var li  = document.createElement("li");
                            li.setAttribute("id",insert_id);
                            var functoinStr = "mouseDownT("+ insert_id +")";
                            li.setAttribute("onmousedown", functoinStr);
                            li.appendChild(document.createTextNode(nodeName));
                        ul.appendChild(li);
                    }
                    else
                    {
                        var li          = document.createElement("li");
                        var itemLabel   = document.createElement("Label");
                            //itemLabel.setAttribute("for", nodeName);
                            itemLabel.innerHTML = nodeName;
                            itemLabel.setAttribute("class","node");
                            itemLabel.setAttribute("id",insert_id);
                            var functoinStr = "mouseDown("+ insert_id +",'"+ GroupID +"')";
                            itemLabel.setAttribute("onmousedown", functoinStr);
                        li.appendChild(itemLabel);
                        var new_ul = document.createElement("ul");
                            new_ul.setAttribute("id","uls_"+insert_id);

                        li.appendChild(new_ul);
                        ul.appendChild(li);
                    }
                },
                error: function (error) {
                    alert("failure");
                }
            });
            return false;
        });
    });
    </script>
<!---------Non-TrsansactableModalCodeSection End------------>

<!--EditNon-Transactable Start-->
    <script type="text/javascript">
        //javaScriptForEditNonTransactable
        $(document).on("click", ".open-EditNonTransactable", function ()
        {
            document.getElementById("EditNonTransactableForm").reset();

            //alert(non_ptnID);
             $("#Edit_Non_HeadID").val(non_ptnID);

             var site_url   = $(".site_url").val();
             $.getJSON(site_url + "/accounts/chart-of-accounts/edit/" + non_ptnID, function(data)
             {
                // alert(data.Position);
                
                $("#Edit_Non_code").val(data.code);
                $("#Edit_Non_HeadName").val(data.HeadName);   
                $("#Edit_Non_Position").val(data.Position); 

                if(data.ActiveStatus == 1)
                {
                    $('input:radio[name="ActiveStatus"]').filter('[value= 1]').attr('checked', true);
                }
                else
                {
                    $('input:radio[name="ActiveStatus"]').filter('[value= 0]').attr('checked', true);
                }
            });           
        }); 
    </script>
        
    <div id="EditNonTransactable" class="modal inmodal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Edit Non Transactable Item form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="form-horizontal form_class_edit_trans" id="EditNonTransactableForm" role="form" method="GET" enctype="multipart/form-data">
                    <div class="modal-body">
                        <!--<p>Sign in today for more expirience.</p>-->
                        <input type="hidden" id="Edit_Non_HeadID" name="Edit_Non_HeadID" value="" />
                        <input type="hidden" name="type" id="type" value="0" class="form-control">

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Item Name</label>
                            <div class="col-lg-7">
                                <input type="text" name="Edit_Non_HeadName" id="Edit_Non_HeadName"  placeholder="Please input Item Name" class="form-control" required> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Code</label>
                            <div class="col-lg-7">
                                <input type="text" name="Edit_Non_code" id="Edit_Non_code"  placeholder="Please input Code" class="form-control"> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Position</label>
                            <div class="col-lg-7">
                            <input type="text" name="Edit_Non_Position" id="Edit_Non_Position"  placeholder="Please input position" class="form-control"> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Active Status</label>
                            <div class="col-lg-7">
                                <div class="inline-radio"><label> <input type="radio" value="1" name="ActiveStatus"> Active </label></div>
                                <div class="inline-radio"><label> <input type="radio" value="0" name="ActiveStatus"> Inactive </label></div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Update</button>
                        <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    jQuery(document).ready(function ($) {
        $("#EditNonTransactableForm").submit(function () {
            $.ajax({
            // alert(Edit_Non_HeadName);
                type: "GET",
                url: "{!! route('chart_of_accounts_update') !!}",
                data: $('form.form_class_edit_trans').serialize(),
                success: function (insert_id) {
                    $("#EditNonTransactable").modal('hide');
                    var ItemID      = $('#Edit_Non_HeadID').val();
                    var nodeName    = $('#Edit_Non_HeadName').val(); 
                    document.getElementById(ItemID).innerHTML = nodeName;
                },
                error: function () {
                    alert("failure");
                }
            });
            return false;
        });
    });
    </script>
<!---------EditNon-Transactable End------------>

<!--###################### Print List Modal Start #################-->
    <script>
        //javaScriptForTransactablevar non_ptnID;   var non_GroupID;
        $(document).on("click", ".open-printList", function () {
            //document.getElementById("printList").reset();

           // $(".ibox-content #new_tra_PtnID").val( non_ptnID );                
        });
    </script>

    <div class="modal inmodal" id="printList" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
                <div class="ibox-title">
                    <h5>Print Chart of Accounts Head</h5>
                </div>
        <form class="form-horizontal" target="_blank" action="" role="form" method="post" enctype="multipart/form-data">
            <!-- <form class="form-horizontal form_class" id="categoryForm" role="form" method="post" enctype="multipart/form-data"> -->
                <div class="ibox-content">
                    <!--<p>Sign in today for more expirience.</p>-->

                    <!-- <input type="hidden" name="CompanyID" value="" /> -->

                    <div class="form-group"><label class="col-lg-4 control-label">Category</label>

                        <div class="col-lg-7">
                            <select id="PrintCategory" name="PrintCategory" class="form-control select2">
                                <option value=""></option>
                                <option value="">Select an option</option>
                                <?php
                                    foreach($itemQueryResult as $key=>$PrintNameInfo) {
                                        if($PrintNameInfo->IsTransactable == 0) {
                                        ?>
                                            <option value="<?php echo $PrintNameInfo->HeadID; ?>" ><?php echo $PrintNameInfo->HeadName; ?></option>
                                        <?php
                                    }}
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" style="padding-top: 10px;"><label class="col-lg-4 control-label">Name</label>
                        <div class="col-lg-7">
                            <select id="PrintName" name="PrintName" class="form-control select2">
                                <option value=""></option>
                                <option value="">Select an option</option>
                                <?php
                                    foreach($itemQueryResult as $key=>$PrintNameInfo) {
                                        if($PrintNameInfo->IsTransactable == 1) {
                                        ?>
                                            <option value="<?php echo $PrintNameInfo->HeadID; ?>" ><?php echo $PrintNameInfo->HeadName; ?></option>
                                        <?php
                                    }}
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <!--<button type="submit" class="btn btn-primary">Submit</button>-->
                    <input type="submit" name="submit" class="btn btn-primary" id="" value="Submit" />
                </div>
            </form>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="printList1" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
                <div class="ibox-title">
                    <h5>Print Transactable Accounts Head</h5>
                </div>
        <form class="form-horizontal" target="_blank" action="" role="form" method="post" enctype="multipart/form-data">
            <!-- <form class="form-horizontal form_class" id="categoryForm" role="form" method="post" enctype="multipart/form-data"> -->
                <div class="ibox-content">
                    <!--<p>Sign in today for more expirience.</p>-->

                    <!-- <input type="hidden" name="CompanyID" value="" /> -->

                    <div class="form-group"><label class="col-lg-4 control-label">Category</label>

                        <div class="col-lg-7">
                            <select id="PrintCategory" name="PrintCategory" class="form-control select2">
                                <option value=""></option>
                                <option value="">Select an option</option>
                                <?php
                                    foreach($itemQueryResult as $key=>$PrintNameInfo) {
                                        if($PrintNameInfo->IsTransactable == 0) {
                                        ?>
                                            <option value="<?php echo $PrintNameInfo->HeadID; ?>" ><?php echo $PrintNameInfo->HeadName; ?></option>
                                        <?php
                                    }}
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" style="padding-top: 10px;"><label class="col-lg-4 control-label">Name</label>
                        <div class="col-lg-7">
                            <select id="PrintName" name="PrintName" class="form-control select2">
                                <option value=""></option>
                                <option value="">Select an option</option>
                                <?php
                                    foreach($itemQueryResult as $key=>$PrintNameInfo) {
                                        if($PrintNameInfo->IsTransactable == 1) {
                                        ?>
                                            <option value="<?php echo $PrintNameInfo->HeadID; ?>" ><?php echo $PrintNameInfo->HeadName; ?></option>
                                        <?php
                                    }}
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <!--<button type="submit" class="btn btn-primary">Submit</button>-->
                    <input type="submit" name="submit" class="btn btn-primary" id="" value="Submit" />
                </div>
            </form>
            </div>
        </div>
    </div>          
<!--###################### Print List Modal End #################-->
@endsection