@extends('layouts.app')

@section('title', 'Edit Users')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Users</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Settings</a></li>
                                    <li class="breadcrumb-item active">Edit Users</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('users_update',$user_find['id']) }}" method="post" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Name *</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user_find['name'] }}" name="name" id="name" placeholder="Enter Name" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">User Name *</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user_find['email'] }}" name="user_name" id="user_name" readonly>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Password</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="" name="password" id="password" placeholder="Enter Password">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Role</label>
                                    <div class="col-md-10">
                                        <select style="cursor: pointer" class="form-control" name="role">
                                            <option {{ $user_find['role'] == 1 ? 'selected' : '' }} value="1">Super Admin</option>
                                            <option {{ $user_find['role'] == 2 ? 'selected' : '' }} value="2">Billing Department</option>
                                            <option {{ $user_find['role'] == 3 ? 'selected' : '' }} value="3">Customer Support</option>
                                            <option {{ $user_find['role'] == 4 ? 'selected' : '' }} value="4">Developer</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Branch</label>
                                    <div class="col-md-10">
                                        <select style="cursor: pointer" id="branch_id" class="form-control" name="branch_id">
                                            @if($branches->count() > 0)
                                            @foreach($branches as $key => $value)
                                            <option value="{{ $value['id'] }}" {{ $user_find['branch_id'] == $value['id'] ? 'selected' : '' }}>{{ $value['name'] }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Status</label>
                                    <div class="col-md-10">
                                        <select style="cursor: pointer" class="form-control" name="status">
                                            <option {{ $user_find['status'] == 1 ? 'selected' : '' }} value="1">Active</option>
                                            <option {{ $user_find['status'] == 0 ? 'selected' : '' }} value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                    	<button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                    	<button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('users_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection