@extends('layouts.app')

@section('title', 'Duty Roster')

@push('scripts')
<style type="text/css">
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>
@endpush

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Duty Roster</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Attendance</a></li>
                                    <li class="breadcrumb-item active">Duty Roster</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <hr style="margin-top: 0px">
                            
                                <form id="FormSubmit" action="{{ route('duty_roster_index') }}" method="GET">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                            <label style="text-align: right" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Search </label>
                                        </div>
                                        
                                        <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                            <select id="department_id" style="width: 100" class="form-control select2" name="department_id">
                                                <option value="{{ $department_name != null ? $department_name['name'] : '' }}">{{ $department_name != null ? $department_name['name'] : '--All Department--' }}</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                            <select id="section_id" style="width: 100" class="form-control select2" name="section_id">
                                                <option value="{{ $section_name != null ? $section_name['name'] : '' }}">{{ $section_name != null ? $section_name['name'] : '--All Section--' }}</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                            <select id="customer_id" style="width: 100" class="form-control select2" name="customer_id">
                                                <option value="{{ $customer_name != null ? $customer_name['name'] : '' }}">{{ $customer_name != null ? $customer_name['name'] : '--All Employee--' }}</option>
                                            </select>
                                        </div>
                                        

                                        <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                                        </div>

                                        <div class="col-lg-1 col-md-1 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        </div>
                                    </div>
                                </form>

                                <hr style="margin-top: 0px">

                                <form id="FormSubmit" action="{{ route('duty_roster_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">

                                {{ csrf_field() }}

                                <div class="row">
                                    <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                                        <thead class="theight">
                                            <tr style="background:#ddd;">
                                                <th style="text-align: center;width: 3%">SL</th>
                                                <!--<th style="text-align: center;width: 7%">Machine ID</th>-->
                                                <th style="text-align: center;width: 7%">ID</th>
                                                <th style="text-align: center;width: 20%">EMPLOYEE</th>
                                                <th style="text-align: center;width: 10%">FRI</th>
                                                <th style="text-align: center;width: 10%">SAT</th>
                                                <th style="text-align: center;width: 10%">SUN</th>
                                                <th style="text-align: center;width: 10%">MON</th>
                                                <th style="text-align: center;width: 10%">TUE</th>
                                                <th style="text-align: center;width: 10%">WED</th>
                                                <th style="text-align: center;width: 10%">THU</th>
                                            </tr>
                                        </thead>

                                        <tbody class="theight">
                                            <?php $i = 1; ?>
                                            @foreach($result as $key => $value)
                                            <input type="hidden" name="employee_id[]" value="{{ $value['employee_id'] }}">
                                            <input type="hidden" name="id[]" value="{{ $value['id'] }}">
                                            <tr> 
                                                <td style="text-align: center;">{{ $i }}</td>
                                                <!--<td style="text-align: center;">{{ $value['id'] }}</td>-->
                                                <td style="text-align: center;">{{ $value['employee_id'] }}</td>
                                                <td style="text-align: left;">
                                                    <strong>Name : </strong>{{ $value['name'] != null ? $value['name'] : '' }} <br>
                                                    <strong>Designation : </strong>{{ $value['designation'] != null ? $value['designation'] : '' }} <br>
                                                    <strong>Department : </strong>{{ $value['department'] != null ? $value['department'] : '' }} <br>
                                                    <strong>Section : </strong>{{ $value['section'] != null ? $value['section'] : '' }}
                                                </td>
                                                <td style="text-align: center;">
                                                    <select style="cursor: pointer;width: 100%" name="fri[]" class="form-control" required>
                                                        <option value="0" {{ $value['fri'] == 0 ? 'selected' : '' }}>W/H</option>
                                                        @if($shifts->count() > 0)
                                                        @foreach($shifts as $shift)
                                                        <option value="{{ $shift->id }}" {{ $value['fri'] == $shift->id ? 'selected' : '' }}>{{ $shift->name }}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </td>
                                                
                                                <td style="text-align: center;">
                                                    <select style="cursor: pointer;width: 100%" name="sat[]" class="form-control" required>
                                                        <option value="0" {{ $value['sat'] == 0 ? 'selected' : '' }}>W/H</option>
                                                        @if($shifts->count() > 0)
                                                        @foreach($shifts as $shift)
                                                        <option value="{{ $shift->id }}" {{ $value['sat'] == $shift->id ? 'selected' : '' }}>{{ $shift->name }}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </td>
                                                
                                                <td style="text-align: center;">
                                                    <select style="cursor: pointer;width: 100%" name="sun[]" class="form-control" required>
                                                        <option value="0" {{ $value['sun'] == 0 ? 'selected' : '' }}>W/H</option>
                                                        @if($shifts->count() > 0)
                                                        @foreach($shifts as $shift)
                                                        <option value="{{ $shift->id }}" {{ $value['sun'] == $shift->id ? 'selected' : '' }}>{{ $shift->name }}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </td>
                                                
                                                <td style="text-align: center;">
                                                    <select style="cursor: pointer;width: 100%" name="mon[]" class="form-control" required>
                                                        <option value="0" {{ $value['mon'] == 0 ? 'selected' : '' }}>W/H</option>
                                                        @if($shifts->count() > 0)
                                                        @foreach($shifts as $shift)
                                                        <option value="{{ $shift->id }}" {{ $value['mon'] == $shift->id ? 'selected' : '' }}>{{ $shift->name }}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </td>
                                                
                                                <td style="text-align: center;">
                                                    <select style="cursor: pointer;width: 100%" name="tue[]" class="form-control" required>
                                                        <option value="0" {{ $value['tue'] == 0 ? 'selected' : '' }}>W/H</option>
                                                        @if($shifts->count() > 0)
                                                        @foreach($shifts as $shift)
                                                        <option value="{{ $shift->id }}" {{ $value['tue'] == $shift->id ? 'selected' : '' }}>{{ $shift->name }}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </td>
                                                
                                                <td style="text-align: center;">
                                                    <select style="cursor: pointer;width: 100%" name="wed[]" class="form-control" required>
                                                        <option value="0" {{ $value['wed'] == 0 ? 'selected' : '' }}>W/H</option>
                                                        @if($shifts->count() > 0)
                                                        @foreach($shifts as $shift)
                                                        <option value="{{ $shift->id }}" {{ $value['wed'] == $shift->id ? 'selected' : '' }}>{{ $shift->name }}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </td>
                                                
                                                <td style="text-align: center;">
                                                    <select style="cursor: pointer;width: 100%" name="thu[]" class="form-control" required>
                                                        <option value="0" {{ $value['thu'] == 0 ? 'selected' : '' }}>W/H</option>
                                                        @if($shifts->count() > 0)
                                                        @foreach($shifts as $shift)
                                                        <option value="{{ $shift->id }}" {{ $value['thu'] == $shift->id ? 'selected' : '' }}>{{ $shift->name }}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <hr style="margin-top: 0px">

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('duty_roster_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {

        var site_url        = $('.site_url').val();

        $("#customer_id").select2({
            ajax: { 
            url:  site_url + '/invoices/customer/list/invoices',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['contact_type'] == 2 || result['id'] == 0)
                {
                    return result['text'];
                }
            },
        });
        
        $("#section_id").select2({
            ajax: { 
            url:  site_url + '/attendance/duty-roster/get-all-section',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                return result['text'];
            },
        });
        
        $("#department_id").select2({
            ajax: { 
            url:  site_url + '/attendance/duty-roster/get-all-department',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                return result['text'];
            },
        });
    });
</script>
@endsection