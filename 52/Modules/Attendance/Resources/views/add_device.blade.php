@extends('layouts.app')

@section('title', 'Add Devices')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Add Devices</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Attendance</a></li>
                                    <li class="breadcrumb-item active">Add Devices</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {!! Session::get('success') !!}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    @endif

                    @if(Session::has('unsuccess'))
                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                        {!! Session::get('unsuccess') !!}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    @endif

                    @if(Session::has('errors'))
                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                        {!! 'Some required fields are missing..!! Please try again..' !!}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    @endif

                    <div class="col-12">
                        <div class="card">
                            <br>
                            <form id="FormSubmit" action="{{ route('attendance_devices_store') }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group"></div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                        <input type="text" name="device_ip" class="inner form-control" id="device_ip" placeholder="Enter Device IP" />
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">ADD</button>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group"></div>
                                </div>
                            </form>
                            
                            <div class="card-body table-responsive">
                                <table class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Device Name</th>
                                            <th>Device ID</th>
                                            <th>Device IP</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($devices) && (count($devices) > 0))
                                        @foreach($devices as $key => $device)
                                            <tr>
                                                <td>{{ $key }}</td>
                                                <td>{{ $device['device_name'] }}</td>
                                                <td>{{ $device['device_id'] }}</td>
                                                <td>{{ $device['device_ip'] }}</td>
                                                <td>
                                                    
                                                </td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('attendance_delete_user', $employee['uid']) }}">Delete User</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
        });
    </script>
@endsection