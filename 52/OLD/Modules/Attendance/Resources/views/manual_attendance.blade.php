@extends('layouts.app')

@section('title', 'Manual Attendance')

@push('scripts')
<style type="text/css">
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>
@endpush

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Manual Attendance</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Attendance</a></li>
                                    <li class="breadcrumb-item active">Manual Attendance</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <hr style="margin-top: 0px">
                            
                                <form id="FormSubmit" action="{{ route('attendance_manual_attendance_index') }}" method="GET">
                                    <div class="row">
                                        <div class="col-lg-1 col-md-1 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                            <label style="text-align: right" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Search </label>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                            <input id="date" name="date" type="text" value="{{ isset($_GET['date']) ? date('d-m-Y', strtotime($_GET['date'])) : date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                            <select id="customer_id" style="width: 100" class="form-control select2" name="customer_id">
                                                <option value="{{ $customer_name != null ? $customer_name['name'] : '' }}">{{ $customer_name != null ? $customer_name['name'] : '--All--' }}</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                                        </div>

                                        <div class="col-lg-1 col-md-1 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        </div>
                                    </div>
                                </form>

                                <hr style="margin-top: 0px">

                                <form id="FormSubmit" action="{{ route('attendance_manual_attendance_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">

                                {{ csrf_field() }}

                                <div class="row">
                                    <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                                        <thead class="theight">
                                            <tr style="background:#ddd;">
                                                <th style="text-align: center;width: 3%">SL</th>
                                                <th style="text-align: center;width: 7%">ID</th>
                                                <th style="text-align: center;width: 25%">NAME</th>
                                                <th style="text-align: center;width: 10%">DESIGNATION</th>
                                                <th style="text-align: center;width: 15%">DATE</th>
                                                <th style="text-align: center;width: 5%">STATUS</th>
                                                <th style="text-align: center;width: 15%">IN TIME</th>
                                                <th style="text-align: center;width: 15%">OUT TIME</th>
                                            </tr>
                                        </thead>

                                        <tbody class="theight">
                                            <?php $i = 1; ?>
                                            @foreach($result as $key => $value)
                                            <input type="hidden" name="employee_id[]" value="{{ $value['employee_id'] }}">
                                            <input type="hidden" name="date" value="{{ isset($_GET['date']) ? date('d-m-Y', strtotime($_GET['date'])) : date('d-m-Y') }}">
                                            <tr> 
                                                <td style="text-align: center;">{{ $i }}</td>
                                                <td style="text-align: center;">{{ $value['employee_id'] }}</td>
                                                <td style="text-align: left;">{{ $value['name'] != null ? $value['name'] : '' }}</td>
                                                <td style="text-align: center;">{{ $value['designation'] != null ? $value['designation'] : '' }}</td>
                                                <td style="text-align: center;">{{ date('d-m-Y', strtotime($date)) }}</td>
                                                <td style="text-align: center;"><strong>{{ $value['in_time'] != 0 ? 'Present' : 'Absent' }}</strong></td>
                                                <td style="text-align: center;">
                                                    <input type="time" id="appt" name="in_time[]" min="00:00" max="24:00" value="{{ $value['in_time'] != 0 ? $value['in_time'] : '' }}">
                                                </td>
                                                <td style="text-align: center;">
                                                    <input type="time" id="appt" name="out_time[]" min="00:00" max="24:00" value="{{ $value['out_time'] != 0 ? $value['out_time'] : '' }}">
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <hr style="margin-top: 0px">

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('attendance_manual_attendance_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {

        var site_url        = $('.site_url').val();

        $("#customer_id").select2({
            ajax: { 
            url:  site_url + '/invoices/customer/list/invoices',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['contact_type'] == 2 || result['id'] == 0)
                {
                    return result['text'];
                }
            },
        });
    });
</script>
@endsection