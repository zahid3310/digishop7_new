

<?php $__env->startSection('title', 'Show'); ?>

<?php $__env->startPush('scripts'); ?>
<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Payroll</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Payroll</a></li>
                                    <li class="breadcrumb-item active">Pay Slip Print</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div style="padding: 10px" class="d-print-none">
                                <div class="float-right">
                                    <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                </div>
                            </div>

                            <div style="margin-top: 20px" class="row">
                                <div style="border-right: 1px dotted black" class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div style="text-align: left" class="col-md-6">
                                                <img style="width: 100px;height: 100px;padding-top: 0px;text-align: left" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                                            </div>
                                            <div style="text-align: right" class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h5 style="line-height: 1.5;"><span style="background-color: gray;padding-left: 5px;padding-right: 5px;padding-top: 2px;padding-bottom: 2px;color: white">Pay Slip</span></h5>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h5 style="line-height: 1.5"><span style="background-color: gray;padding-left: 5px;padding-right: 5px;padding-top: 2px;padding-bottom: 2px;color: white">Office Copy</span></h5>
                                                    </div>

                                                    <?php
                                                        if ($pay_slips->monthlySalarySheets->month == 1)
                                                        {
                                                            $month  = 'January';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 2)
                                                        {
                                                            $month  = 'February';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 3)
                                                        {
                                                            $month  = 'March';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 4)
                                                        {
                                                            $month  = 'April';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 5)
                                                        {
                                                            $month  = 'May';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 6)
                                                        {
                                                            $month  = 'Jun';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 7)
                                                        {
                                                            $month  = 'July';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 8)
                                                        {
                                                            $month  = 'August';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 9)
                                                        {
                                                            $month  = 'September';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 10)
                                                        {
                                                            $month  = 'October';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 11)
                                                        {
                                                            $month  = 'November';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 12)
                                                        {
                                                            $month  = 'December';
                                                        }
                                                    ?>

                                                    <div class="col-md-12">
                                                        <h5 style="line-height: 1.5"><span style="background-color: gray;padding-left: 5px;padding-right: 5px;padding-top: 2px;padding-bottom: 2px;color: white"><?php echo e($month . ', ' . $pay_slips->monthlySalarySheets->year); ?></span></h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="margin-top: 0px;margin-bottom: 0px" class="card-body">
                                        <div class="row">
                                            <div style="font-size: 15px" class="col-md-6">
                                                <div class="row">
                                                    <div style="margin-top: 0px" class="col-12">
                                                        <address style="margin-bottom: 0px !important">
                                                            <strong>ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong> <?php echo e($pay_slips->employee->id); ?>

                                                        </address>
                                                        <address style="margin-bottom: 0px !important">
                                                            <strong>NAME &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong> <?php echo e($pay_slips->employee->name); ?>

                                                        </address>
                                                        <address style="margin-bottom: 0px !important">
                                                            <strong>DESIGNATION &nbsp;&nbsp;&nbsp;: </strong> <?php echo e($pay_slips->employee->designation); ?>

                                                        </address>

                                                        <address style="margin-bottom: 0px !important">
                                                            <strong>JOINING DATE &nbsp;&nbsp;: </strong> <?php echo e(date('d-m-Y', strtotime($pay_slips->employee->joining_date))); ?>

                                                        </address>
                                                    </div>
                                                </div>
                                            </div>

                                            <div style="font-size: 15px" class="col-md-6">
                                                <table  style="width:100%">
                                                    <tr>
                                                        <th style="width: 50%">Basic</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->basic,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">House Rent</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->house_rent,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Medical Allow.</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->medical,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Convence</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->convence,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Food</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->food,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Mobile</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->mobile,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Others</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->other,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Gross</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->gross,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Absent Deduction</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->gross - $pay_slips->monthlySalarySheets->net_payable,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Net Payable</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->net_payable,2,'.',',')); ?></th>
                                                    </tr>

                                                    <tr>
                                                        <th style="width: 50%">Paid</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->amount,2,'.',',')); ?></th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>

                                        <div style="margin-top: 50px" class="row">
                                            <div class="col-md-6">
                                                <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">Received By </span> </h6>
                                            </div>
                                            <div class="col-md-6">
                                                <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">Approved By</span> </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div style="text-align: left" class="col-md-6">
                                                <img style="width: 100px;height: 100px;padding-top: 0px;text-align: left" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                                            </div>
                                            <div style="text-align: right" class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h5 style="line-height: 1.5;"><span style="background-color: gray;padding-left: 5px;padding-right: 5px;padding-top: 2px;padding-bottom: 2px;color: white">Pay Slip</span></h5>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h5 style="line-height: 1.5"><span style="background-color: gray;padding-left: 5px;padding-right: 5px;padding-top: 2px;padding-bottom: 2px;color: white">Employee Copy</span></h5>
                                                    </div>

                                                    <?php
                                                        if ($pay_slips->monthlySalarySheets->month == 1)
                                                        {
                                                            $month  = 'January';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 2)
                                                        {
                                                            $month  = 'February';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 3)
                                                        {
                                                            $month  = 'March';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 4)
                                                        {
                                                            $month  = 'April';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 5)
                                                        {
                                                            $month  = 'May';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 6)
                                                        {
                                                            $month  = 'Jun';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 7)
                                                        {
                                                            $month  = 'July';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 8)
                                                        {
                                                            $month  = 'August';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 9)
                                                        {
                                                            $month  = 'September';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 10)
                                                        {
                                                            $month  = 'October';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 11)
                                                        {
                                                            $month  = 'November';
                                                        }
                                                        elseif ($pay_slips->monthlySalarySheets->month == 12)
                                                        {
                                                            $month  = 'December';
                                                        }
                                                    ?>

                                                    <div class="col-md-12">
                                                        <h5 style="line-height: 1.5"><span style="background-color: gray;padding-left: 5px;padding-right: 5px;padding-top: 2px;padding-bottom: 2px;color: white"><?php echo e($month . ', ' . $pay_slips->monthlySalarySheets->year); ?></span></h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="margin-top: 0px;margin-bottom: 0px" class="card-body">
                                        <div class="row">
                                            <div style="font-size: 15px" class="col-md-6">
                                                <div class="row">
                                                    <div style="margin-top: 0px" class="col-12">
                                                        <address style="margin-bottom: 0px !important">
                                                            <strong>ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong> <?php echo e($pay_slips->employee->id); ?>

                                                        </address>
                                                        <address style="margin-bottom: 0px !important">
                                                            <strong>NAME &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong> <?php echo e($pay_slips->employee->name); ?>

                                                        </address>
                                                        <address style="margin-bottom: 0px !important">
                                                            <strong>DESIGNATION &nbsp;&nbsp;&nbsp;: </strong> <?php echo e($pay_slips->employee->designation); ?>

                                                        </address>

                                                        <address style="margin-bottom: 0px !important">
                                                            <strong>JOINING DATE &nbsp;&nbsp;: </strong> <?php echo e(date('d-m-Y', strtotime($pay_slips->employee->joining_date))); ?>

                                                        </address>
                                                    </div>
                                                </div>
                                            </div>

                                            <div style="font-size: 15px" class="col-md-6">
                                                <table  style="width:100%">
                                                    <tr>
                                                        <th style="width: 50%">Basic</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->basic,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">House Rent</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->house_rent,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Medical Allow.</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->medical,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Convence</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->convence,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Food</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->food,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Mobile</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->mobile,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Others</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->other,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Gross</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->gross,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Absent Deduction</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->salaryStatements->gross - $pay_slips->monthlySalarySheets->net_payable,2,'.',',')); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 50%">Net Payable</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->monthlySalarySheets->net_payable,2,'.',',')); ?></th>
                                                    </tr>

                                                    <tr>
                                                        <th style="width: 50%">Paid</th>
                                                        <th style="width: 50%;text-align: right"><?php echo e(number_format($pay_slips->amount,2,'.',',')); ?></th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>

                                        <div style="margin-top: 50px" class="row">
                                            <div class="col-md-6">
                                                <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">Received By </span> </h6>
                                            </div>
                                            <div class="col-md-6">
                                                <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">Approved By</span> </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/52/Modules/Payroll/Resources/views/PaySlips/show.blade.php ENDPATH**/ ?>