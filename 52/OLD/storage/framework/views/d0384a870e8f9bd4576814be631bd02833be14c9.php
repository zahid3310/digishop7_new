

<?php $__env->startSection('title', 'Purchase Report'); ?>

<?php $__env->startSection('styles'); ?>
    <style type="text/css">
        @media  print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page  {
                margin: 0cm ! important;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Purchase Report</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Purchase Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['address']); ?></p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['contact_number']); ?></p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">Purchase Report</h4>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><strong>From</strong> <?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?> <strong>To</strong> <?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="<?php echo e(route('purchase_report_index')); ?>" enctype="multipart/form-data">
                                    <div class="form-group row">
                                        <div style="margin-bottom: 10px" class="col-lg-5 col-md-5 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="<?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="<?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        <div class="col-lg-5 col-md-5 col-sm-8 col-6 d-print-none">
                                            <div class="form-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="0" selected>--All Suppliers--</option>
                                                    <?php if(!empty($customers) && ($customers->count() > 0)): ?>
                                                    <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option <?php echo e(isset($_GET['customer_id']) && ($_GET['customer_id'] == $value['id']) ? 'selected' : ''); ?> value="<?php echo e($value['id']); ?>"><?php echo e($value['name'] . ' | ' . $value['phone']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Details</th>
                                            <th>Product Name</th>
                                            <th>Product Code</th>
                                            <th style="text-align: right">Rate</th>
                                            <th style="text-align: right">Quantity</th>
                                            <th style="text-align: right">Discount</th>
                                            <th style="text-align: right">Amount</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php 
                                            $total_return_am        = 0; 
                                            $total_return_paid_am   = 0;
                                        ?>
                                        <?php if(!empty($data)): ?>
                                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td colspan="7"><strong><?php echo e($value['bill_date']); ?>, <?php echo e($value['bill_number']); ?>, <?php echo e($value['vendor_name']); ?></strong></td>
                                            </tr>

                                            <?php if(!empty($value['bill_entries'])): ?>

                                            <?php $serial = 0; ?>

                                            <?php $__currentLoopData = $value['bill_entries']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td style="text-align: right"><?php echo e($serial + 1); ?></td>
                                                    <td><?php echo e($value1->product_name); ?> <br> <?php echo e($value1->product_entry_name); ?></td>
                                                    <td><?php echo e(str_pad($value1->product_code, 6, "0", STR_PAD_LEFT)); ?></td>
                                                    <td style="text-align: right"><?php echo e(number_format($value1->rate,2,'.',',')); ?></td>
                                                    <td style="text-align: right"><?php echo e($value1->quantity); ?></td>
                                                    <td style="text-align: right">
                                                        <?php echo e($value1->discount_type == 1 ? number_format($value1->discount_amount,2,'.',',') : number_format((($value1->discount_amount*$value1['rate']*$value1['quantity'])/100),2,'.',',')); ?>

                                                        <?php if(($value1->discount_type == 0) && ($value1->discount_amount > 0)): ?> 
                                                            <br> <?php echo '('.$value1->discount_amount.'%)'; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td style="text-align: right"><?php echo e(number_format($value1->total_amount,2,'.',',')); ?></td>
                                                </tr>

                                                <?php $serial++; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            <?php $purchase_return = purchaseReturn($value['bill_id']); ?>

                                            <?php if(!empty($data)): ?>
                                            <?php $__currentLoopData = $purchase_return['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value3): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td colspan="7"><strong><?php echo e($value3['purchase_return_date']); ?>, <?php echo e($value3['purchase_return_number']); ?>, <?php echo e($value3['customer_name']); ?></strong></td>
                                                </tr>

                                                <?php if(!empty($value3['return_entries'])): ?>

                                                    <?php $serial1 = 0; ?>

                                                    <?php $__currentLoopData = $value3['return_entries']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key4 => $value4): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr>
                                                            <td style="text-align: right"><?php echo e($serial1 + 1); ?></td>
                                                            <td><?php echo e($value4->product_name); ?> <br> <?php echo e($value4->product_entry_name); ?></td>
                                                            <td><?php echo e(str_pad($value4->product_code, 10, "0", STR_PAD_LEFT)); ?></td>
                                                            <td style="text-align: right"><?php echo e(number_format($value4->rate,2,'.',',')); ?></td>
                                                            <td style="text-align: right"><?php echo e($value4->quantity); ?></td>
                                                            <td style="text-align: right">0.00</td>
                                                            <td style="text-align: right"><?php echo e(number_format($value4->total_amount,2,'.',',')); ?></td>
                                                        </tr>

                                                        <?php $serial1++; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>

                                            <?php endif; ?>

                                            <tr>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><?php if($value['bill_note'] != null): ?><span style="float: left"><strong>Note :</strong> <?php echo e($value['bill_note']); ?></span><?php endif; ?><strong>Sub Total</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong><?php echo e(number_format($value['sub_total'],2,'.',',')); ?></strong></td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Adjustment <?php echo e(($value['adjustment_type'] == 0) ? '('.$value['adjustment'].'%'.')' : ''); ?></strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong><?php echo e(($value['adjustment_type'] == 0) ? number_format($value['adjustment_perc'],2,'.',',') : number_format($value['adjustment'],2,'.',',')); ?></strong></td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Vat <?php echo e(($value['vat_type'] == 0) ? '('.$value['vat'].'%'.')' : ''); ?></strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong><?php echo e(($value['vat_type'] == 0) ? number_format($value['vat_perc'],2,'.',',') : number_format($value['vat'],2,'.',',')); ?></strong></td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Tax <?php echo e(($value['tax_type'] == 0) ? '('.$value['tax'].'%'.')' : ''); ?></strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong><?php echo e(($value['tax_type'] == 0) ? number_format($value['tax_perc'],2,'.',',') : number_format($value['tax'],2,'.',',')); ?></strong></td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Payable</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong><?php echo e(number_format($value['bill_amount'],2,'.',',')); ?></strong></td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Purchase Return</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong><?php echo e(number_format($purchase_return['total_return_amount'],2,'.',',')); ?></strong></td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Net Payable</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong><?php echo e(number_format($value['bill_amount'] - $purchase_return['total_return_amount'],2,'.',',')); ?></strong></td>
                                            </tr>

                                            <tr style="padding: 0px !important">
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Paid</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong><?php echo e(number_format($value['paid_amount'] - $value['return_paid'] - $value['return_due'],2,'.',',')); ?></strong></td>
                                            </tr>

                                            <tr style="padding: 0px !important">
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Dues</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong><?php echo e(number_format($value['due_amount'],2,'.',',')); ?></strong></td>
                                            </tr>

                                            <tr style="padding: 0px !important">
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Receivable</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong><?php echo e(number_format($value['return_amount'] - $value['return_paid'],2,'.',',')); ?></strong></td>
                                            </tr>

                                            <?php
                                                $total_return_am        = $total_return_am + $purchase_return['total_return_amount'];
                                                $total_return_paid_am   = $total_return_paid_am + $purchase_return['total_paid_amount'];
                                            ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Adjustment</strong></td>
                                            <td style="text-align: right;padding-bottom: 0px !important;"><strong><?php echo e(number_format($total_adjustment_amount,2,'.',',')); ?></strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Vat</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;"><strong><?php echo e(number_format($total_vat_amount,2,'.',',')); ?></strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Tax</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;"><strong><?php echo e(number_format($total_tax_amount,2,'.',',')); ?></strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Payable</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;"><strong><?php echo e(number_format($total_bill_amount,2,'.',',')); ?></strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Purchase Return</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;"><strong><?php echo e(number_format($total_return_am,2,'.',',')); ?></strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Net Payable</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;"><strong><?php echo e(number_format($total_bill_amount - $total_return_am,2,'.',',')); ?></strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Paid</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;"><strong><?php echo e(number_format($total_paid_amount - $total_return_paid - $total_return_due,2,'.',',')); ?></strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Dues</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong><?php echo e(number_format($total_due_amount,2,'.',',')); ?></strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Receivable</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong><?php echo e(number_format($total_return_due,2,'.',',')); ?></strong></td>
                                        </tr>

                                    </tbody>
                                    
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/52/Modules/Reports/Resources/views/purchase_report.blade.php ENDPATH**/ ?>