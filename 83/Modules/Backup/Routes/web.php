<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('backup')->group(function() {
    Route::get('/', 'BackupController@index')->name('backup_index');
    Route::get('/store', 'BackupController@store')->name('backup_store');
    Route::get('/download/{id}', 'BackupController@download')->name('backup_download');
});
