<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('softwarebilling')->group(function() {
    Route::get('/', 'SoftwareBillingController@index')->name('software_billing_index');
    Route::get('/sms-purchase', 'SoftwareBillingController@smsPurchase')->name('software_billing_sms_purchase');
    Route::get('/sms-rate/{sms_type}', 'SoftwareBillingController@smsRate')->name('software_billing_sms_rate');
    Route::post('/sms-purchase-store', 'SoftwareBillingController@smsPurchaseStore')->name('software_billing_sms_purchase_store');
});
