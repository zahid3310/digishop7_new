<!DOCTYPE html>
<html>

<head>
    <title>Loan Details</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>  
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: center;line-height: .8">
                        <p style="font-size: 40px"><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p style="font-size: 20px">{{ $user_info['address'] }}</p>
                        <p style="font-size: 20px">{{ $user_info['contact_number'] }}</p>
                        <p style="font-size: 20px">{{ $user_info['contact_email'] }}</p>
                        <p style="font-size: 14px;text-align: right">{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Loan Details</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                    <th style="text-align: center">Loan Register</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                    
                                    <td style="text-align: center">
                                        @if($customer_name != null)
                                            {{ $customer_name['name'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 10%">Date</th>
                                    <th style="text-align: center;width: 10%">Transaction#</th>
                                    <th style="text-align: center;width: 10%">Payment Methode</th>
                                    <th style="text-align: center;width: 10%">Account Information</th>
                                    <th style="text-align: center;width: 10%">Note</th>
                                    <th style="text-align: center;width: 10%">Received</th>
                                    <th style="text-align: center;width: 10%">Paid</th>
                                    <th style="text-align: center;width: 10%">Balance</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php
                                    $i           = 1;
                                    $sub_total   = 0;
                                    $balance     = 0;
                                ?>
                                @if(count($data) > 0)
                                @foreach($data as $key => $value)

                                    <tr>
                                        <?php
                                            if (isset($value['income_date']))
                                            {
                                                $trans_number   = 'INC - '.str_pad($value['income_number'], 6, "0", STR_PAD_LEFT);
                                                $date           = date('d-m-Y', strtotime($value['income_date']));
                                                $loan           = $value['amount'];
                                                $paid           = 0;
                                                $balance        = $balance + $value['amount'];
                                            }

                                            if (isset($value['expense_date']))
                                            {
                                                $trans_number   = 'EXP - '.str_pad($value['expense_number'], 6, "0", STR_PAD_LEFT);
                                                $date           = date('d-m-Y', strtotime($value['expense_date']));
                                                $paid           = $value['amount'];
                                                $loan           = 0;
                                                $balance        = $balance - $value['amount'];
                                            }
                                        ?>

                                        <td style="text-align: center;">{{ $date }}</a>
                                        </td>
                                        <td style="text-align: center;">{{ $trans_number }}</td>
                                        <td style="text-align: center;">{{ $value['paid_through_acount_name'] }}</td>
                                        <td style="text-align: center;">{{ $value['account_information'] }}</td>
                                        <td style="text-align: center;">{{ $value['note'] }}</td>
                                        <td style="text-align: right;">{{ $loan > 0 ? number_format($loan,2,'.',',') : '' }}</td>
                                        <td style="text-align: right;">{{ $paid > 0 ? number_format($paid,2,'.',',') : '' }}</td>
                                        <td style="text-align: right;">{{ number_format($balance,2,'.',',') }}</td>
                                    </tr>
                                 
                                <?php 
                                    $i++;
                                    $sub_total = $sub_total + $value['amount'];
                                ?>
                                @endforeach
                                @endif
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="7" style="text-align: right;">Balance</th>
                                    <th colspan="1" style="text-align: right;">{{ number_format($balance,2,'.',',') }}</th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>