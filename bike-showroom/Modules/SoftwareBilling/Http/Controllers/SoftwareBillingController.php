<?php

namespace Modules\SoftwareBilling\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use App\Models\Users;

class SoftwareBillingController extends Controller
{
    public function index()
    {   
        $user     = Users::find(1);
        $response = Http::get('https://digishop7.com/digishop-billing/get-transaction-history/'.$user['billing_id']); 
        $data     = json_decode($response);

        return view('softwarebilling::index', compact('data'));
    }
}
