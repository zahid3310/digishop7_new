

<?php $__env->startSection('title', 'Due Invoice List'); ?>

<style type="text/css">
    @media  print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page  {
            margin: 0cm ! important;
        }
    }

    .table td, .table th {
        font-size: 11px !important;
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Due Invoice List</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Due Invoice List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['address']); ?></p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['contact_number']); ?></p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">Due Invoice List</h4>
                                        <p style="text-align: center"><strong>From</strong> <?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?> <strong>To</strong> <?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="<?php echo e(route('sales_invoice_due_index')); ?>" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div style="margin-bottom: 10px" class="col-lg-4 col-md-4 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="<?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="<?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        <?php if(Auth::user()->branch_id == 1): ?>
                                        <div class="col-lg-2 col-md-2 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select style="width: 100%" id="branch_id" name="branch_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" required>
                                                   <?php if($branches->count() > 0): ?>
                                                   <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                   <option value="<?php echo e($value['id']); ?>" <?php echo e($value['id'] == $branch_id ? 'selected' : ''); ?>><?php echo e($value['name']); ?></option>
                                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                   <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php endif; ?>

                                        <div class="col-lg-4 col-md-4 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="0" selected>-- All Customers --</option>
                                                    <?php if(!empty($customers) && ($customers->count() > 0)): ?>
                                                    <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option <?php echo e(isset($_GET['customer_id']) && ($_GET['customer_id'] == $value['id']) ? 'selected' : ''); ?> value="<?php echo e($value['id']); ?>"><?php echo e($value['name']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Invoice#</th>
                                            <th>Date</th>
                                            <th>Due Date</th>
                                            <th>Customer</th>
                                            <th style="text-align: right">Receivable</th>
                                            <th style="text-align: right">Received</th>
                                            <th style="text-align: right">Due</th>
                                            <th style="text-align: right">Total Due</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <?php 
                                            $serial             = 1; 
                                            $total_receivable   = 0;
                                            $total_due          = 0;
                                        ?>
                                        
                                        <?php if($invoices->count() > 0): ?>
                                        <?php $__currentLoopData = $invoices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($value->due_amount > 0): ?>
                                            <tr>
                                                <td><?php echo e($serial); ?></td>
                                                <td>
                                                    <a href="<?php echo e(route('invoices_show', $value['id'])); ?>" target="_blank">
                                                        <?php echo e('INV - ' . str_pad($value['invoice_number'], 6, "0", STR_PAD_LEFT)); ?>

                                                    </a>
                                                </td>
                                                <td><?php echo e($value['invoice_date']); ?></td>
                                                <td><?php echo e($value['due_date']); ?></td>
                                                <td><?php echo e($value->customer->name); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($value['invoice_amount'],2,'.',',')); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($value['invoice_amount'] - $value['due_amount'],2,'.',',')); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($value['due_amount'],2,'.',',')); ?></td>
                                                <td style="text-align: right">
                                                   <?php echo customerBalance($value['customer_id'], $branch_id); ?> 
                                                </td>
                                            </tr>

                                            <?php 
                                                $serial++; 
                                                $total_receivable   = $total_receivable + $value['invoice_amount'];
                                                $total_due          = $total_due + $value['due_amount'];
                                            ?>
                                        <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>

                                    </tbody>
                                    <tr>
                                        <th style="text-align: right" colspan="6">Total</th>
                                        <th style="text-align: right"><?php echo e(number_format($total_receivable,2,'.',',')); ?></th>
                                        <th style="text-align: right"><?php echo e(number_format($total_due,2,'.',',')); ?></th>
                                        <th style="text-align: right"></th>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/66/Modules/Reports/Resources/views/invoice_due_report.blade.php ENDPATH**/ ?>