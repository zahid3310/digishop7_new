@extends('layouts.app')

@section('title', 'All Purchases')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.all_purchases')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.all_purchases')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.all_purchases')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {!! Session::get('success') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('unsuccess'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! Session::get('unsuccess') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('errors'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! 'Some required fields are missing..!! Please try again..' !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif
                                
                            <div class="card-body table-responsive">
                                
                                <div style="margin-right: 0px" class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-1">{{ __('messages.search')}} : </div>
                                    <div class="col-md-2">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            
                                            <th>{{ __('messages.sl')}}</th>
                                            <th>{{ __('messages.date')}}</th>
                                            <th>{{ __('messages.purchase')}}#</th>
                                            <th>{{ __('messages.supplier')}}</th>
                                            <th>{{ __('messages.total_payable')}}</th>
                                            <th>{{ __('messages.cash_given')}}</th>
                                            <th>{{ __('messages.action')}}</th>
                                        </tr>
                                    </thead>

                                    <tbody id="bill_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $.get(site_url + '/bills/bill/list/load', function(data){

                billList(data);
                
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var search_text     = $('#searchPayment').val();
            var site_url        = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/bills/bill/search/list/' + search_text, function(data){

                billList(data);

            });
        }

        function billList(data)
        {
            var bill_list = '';
            var site_url  = $('.site_url').val();
            $.each(data, function(i, bill_data)
            {   
                var serial              = parseFloat(i) + 1;
                var edit_url            = site_url + '/bills/edit/' + bill_data.id;
                var print_url           = site_url + '/bills/show/' + bill_data.id;
                var purchase_return     = site_url + '/purchasereturn?supplier_id=' + bill_data.vendor_id + '&bill_id=' + bill_data.id;
                var barcode_print       = site_url + '/products/barcode-print?bill_id=' + bill_data.id;

                if (bill_data.return_id != null)
                {
                    var return_sign  = '&nbsp;' + '&nbsp;' + '<i class="bx bx-repost font-size-15">' + '</i>';
                }
                else
                {
                    var return_sign  = '';
                }

                if (bill_data.contact_type == 0)
                {
                    var type = 'Customer';
                }

                if (bill_data.contact_type == 1)
                {
                    var type = 'Supplier';
                }

                if (bill_data.contact_type == 2)
                {
                    var type = 'Employee';
                }

                bill_list += '<tr>' +
                                    '<td>' +
                                        serial +
                                    '</td>' +
                                    '<td>' +
                                       formatDate(bill_data.bill_date) +
                                    '</td>' +
                                    '<td>' +
                                       'BILL - ' + bill_data.bill_number.padStart(6, '0') + return_sign +
                                    '</td>' +
                                    '<td>' +
                                        bill_data.customer_name +
                                    '</td>' +
                                    '<td>' +
                                       bill_data.bill_amount +
                                    '</td>' +
                                    '<td>' +
                                       bill_data.cash_given +
                                    '</td>' +
                                    '<td>' +
                                        '<div class="dropdown">' +
                                            '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                            '</a>' +
                                            '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                '@if(Auth::user()->role == 1)' +
                                                '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                '@endif' +
                                                '<a class="dropdown-item" href="' + print_url +'" target="_blank">' + 'Show' + '</a>' +
                                                '<a class="dropdown-item" href="' + barcode_print +'" target="_blank">' + 'Print Bar Code' + '</a>' +
                                                '<a class="dropdown-item" href="' + purchase_return +'" target="_blank">' + 'Purchase Return' + '</a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</td>' +
                                '</tr>';
            });

            $("#bill_list").empty();
            $("#bill_list").append(bill_list);
        }
    </script>
@endsection