@extends('layouts.app')

@section('title', 'Edit Branch')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.edit')}} {{ __('messages.branch')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.branch')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.edit')}} {{ __('messages.branch')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('branch_update', $find_branch['id']) }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div style="margin-bottom: 0px !important" class="form-group row">
                                    <div class="col-md-3 form-group">
                                        <label for="productname">{{ __('messages.branch_name')}} *</label>
                                        <input type="text" name="branch_name" class="inner form-control" id="branch_name" value="{{ $find_branch['name'] }}" required />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname">{{ __('messages.organization_name')}}</label>
                                        <input type="text" name="organization_name" class="inner form-control" id="organization_name" placeholder="Organization Name" value="{{ $find_branch['organization_name'] }}" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname">{{ __('messages.address')}}</label>
                                        <input type="text" name="address" class="inner form-control" id="address" placeholder="Address" value="{{ $find_branch['address'] }}" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname">{{ __('messages.phone')}}</label>
                                        <input type="text" name="contact_number" class="inner form-control" id="contact_number" placeholder="{{ __('messages.phone')}}" value="{{ $find_branch['contact_number'] }}" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname">{{ __('messages.email')}}</label>
                                        <input type="text" name="email" class="inner form-control" id="email" placeholder="Email Address" value="{{ $find_branch['email'] }}" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname">{{ __('messages.website')}}</label>
                                        <input type="text" name="website" class="inner form-control" id="website" placeholder="{{ __('messages.website_link')}}" value="{{ $find_branch['website'] }}" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname">{{ __('messages.printer_type')}}</label>
                                        <select style="cursor: pointer" class="form-control" id="printer_type" name="printer_type">
                                            <option value="0" {{ $find_branch['printer_type'] == 0 ? 'selected' : '' }}>58 mm Thermal Receipt</option>
                                            <option value="1" {{ $find_branch['printer_type'] == 1 ? 'selected' : '' }}>80 mm Thermal Receipt</option>
                                            <option value="2" {{ $find_branch['printer_type'] == 2 ? 'selected' : '' }}>A4 Size Paper</option>
                                            <option value="3" {{ $find_branch['printer_type'] == 3 ? 'selected' : '' }}>Letter Size Paper</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname">{{ __('messages.logo')}}</label>
                                        <input class="form-control" type="file" name="logo" >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Previous Logo</label>
                                    <div class="col-md-10">
                                        @if($find_branch['logo'] != null)
                                        <img style="height: 60px;width: 200px" src="{{ url('public/'.$find_branch['logo']) }}">
                                        @endif
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">{{ __('messages.update')}}</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('branch_index') }}">{{ __('messages.close')}}</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">{{ __('messages.all_branch')}}</h4>

                                <br>

                                <table id="datatable" class="table table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.sl')}}</th>
                                            <th>{{ __('messages.branch_name')}}</th>
                                            <th>{{ __('messages.address')}}</th>
                                            <th>{{ __('messages.phone')}}</th>
                                            <th>{{ __('messages.logo')}}</th>
                                            <th>{{ __('messages.action')}}</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        @if(!empty($branches) && ($branches->count() > 0))
                                        @foreach($branches as $key => $branch)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $branch['name'] }}</td>
                                                <td>{{ $branch['address'] }}</td>
                                                <td>{{ $branch['contact_number'] }}</td>
                                                <td>
                                                    <img style="height: 40px;width: 40px" src="{{ url('public/'.$branch->logo) }}">
                                                </td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('branch_edit', $branch['id']) }}">{{ __('messages.edit')}}</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection