<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App;
use Config;

class SetLocale
{
    /**
     *
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $languages = ['en', 'bn'];
    
    public function handle($request, Closure $next)
    {
        if(Cache()->has('locale') && in_array(Cache()->get('locale'), $this->languages))
        {
            app()->setLocale(Cache()->get('locale'));
        }
        return $next($request);
    }
}
