<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table = "customers";
    
    public function department()
    {
        return $this->belongsTo('App\Models\Departments','department_id');
    }
    
    public function section()
    {
        return $this->belongsTo('App\Models\Sections','section_id');
    }
    
    public function reference()
    {
        return $this->belongsTo('App\Models\Customers','reference_id');
    }
    
    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }
}
