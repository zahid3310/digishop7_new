<!DOCTYPE html>
<html>

<head>
    <title>Production Report</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">
</head>

<style type="text/css" media="print">        
    @page {
        size: auto;   
        margin: 15mm 5mm 5mm 10mm;
    }

    #print-footer {
        display: none;
        position: fixed;
        bottom: 0;
        left:0;
    }

    #hide-print {
        display: none;
    }

    #print-footer {
        display: none;
    }

    body {
        margin-bottom:10mm;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #ddd;
    }
    
    fontSizeC {
        font-size: 10px !important;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: center; min-height: 82px;">
                        <p><strong style="font-size:17px;">{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Production Report</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">{{ __('messages.from_date')}}</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="font-size: 10px;text-align: center;width: 4%">{{ __('messages.sl')}}</th>
                                    <th style="font-size: 10px;text-align: center;width: 8%">{{ __('messages.T/date')}}</th>
                                    <th style="font-size: 10px;text-align: center;width: 8%">Production Number</th>
                                    <th style="font-size: 10px;text-align: center;width: 15%">Description</th>
                                    <th style="font-size: 10px;text-align: center;width: 8%">Stock</th>
                                    <th style="font-size: 10px;text-align: center;width: 9%">U/M</th>
                                    <th style="font-size: 10px;text-align: center;width: 8%">Rate</th>
                                    <th style="font-size: 10px;text-align: center;width: 8%">Send To Production</th>
                                    <th style="font-size: 10px;text-align: center;width: 8%">Back To Production</th>
                                    <th style="font-size: 10px;text-align: center;width: 8%">Production Cost</th>
                                    <th style="font-size: 10px;text-align: center;width: 8%">Stock</th>
                                    <th style="font-size: 10px;text-align: center;width: 8%">Stock Value</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i                      = 1;
                                    $total_production_cost  = 0;
                                    $total_stock_value      = 0;
                                ?>

                                @if(!empty($data))
                                @foreach($data as $key => $value)
                                <tr>
                                    <td style="font-size: 10px;text-align: center;vertical-align: middle" rowspan="{{ $value->productionEntries->count() }}">{{ $i }}</td>
                                    <td style="font-size: 10px;text-align: center;vertical-align: middle" rowspan="{{ $value->productionEntries->count() }}">{{ date('d-m-Y', strtotime($value->date)) }}</td>
                                    <td style="font-size: 10px;text-align: center;vertical-align: middle" rowspan="{{ $value->productionEntries->count() }}">{{ $value->production_number }}</td>
                                   
                                    <td style="font-size: 10px;text-align: left;">{{ $value->productionEntries[0]->productEntries->name }}</td>
                                    <td style="font-size: 10px;text-align: center;">{{ $value->productionEntries[0]->stock_quantity }}</td>
                                    <td style="font-size: 10px;text-align: center;">{{ $value->productionEntries[0]->unit->name }}</td>
                                    <td style="font-size: 10px;text-align: right;">{{ $value->productionEntries[0]->rate }}</td>
                                    <td style="font-size: 10px;text-align: right;">{{ $value->productionEntries[0]->quantity }}</td>
                                    <td style="font-size: 10px;text-align: right;">{{ $value->productionEntries[0]->return_quantity }}</td>
                                    <td style="font-size: 10px;text-align: right;">{{ ($value->productionEntries[0]->quantity - $value->productionEntries[0]->return_quantity)*$value->productionEntries[0]->rate }}</td>
                                    <td style="font-size: 10px;text-align: right;">{{ $value->productionEntries[0]->stock_quantity -$value->productionEntries[0]->quantity - $value->productionEntries[0]->return_quantity }}</td>
                                    <td style="font-size: 10px;text-align: right;">{{ ($value->productionEntries[0]->stock_quantity - $value->productionEntries[0]->quantity - $value->productionEntries[0]->return_quantity)*$value->productionEntries[0]->rate }}</td>
                                </tr>

                                <?php
                                    $total_production_cost  = $total_production_cost + (($value->productionEntries[0]->quantity - $value->productionEntries[0]->return_quantity)*$value->productionEntries[0]->rate);
                                    $total_stock_value      = $total_stock_value + (($value->productionEntries[0]->stock_quantity - $value->productionEntries[0]->quantity - $value->productionEntries[0]->return_quantity)*$value->productionEntries[0]->rate);
                                ?>

                                @foreach($value->productionEntries as $key1 => $value1)
                                @if($key1 != 0)

                                <?php
                                    $total_production_cost  = $total_production_cost + (($value1->quantity - $value1->return_quantity)*$value1->rate);
                                    $total_stock_value      = $total_stock_value + (($value1->stock_quantity - $value1->quantity - $value1->return_quantity)*$value1->rate);
                                ?>

                                <tr>
                                    <td style="font-size: 10px;text-align: left;">{{ $value1->productEntries->name }}</td>
                                    <td style="font-size: 10px;text-align: center;">{{ $value1->stock_quantity }}</td>
                                    <td style="font-size: 10px;text-align: center;">{{ $value1->unit->name }}</td>
                                    <td style="font-size: 10px;text-align: right;">{{ $value1->rate }}</td>
                                    <td style="font-size: 10px;text-align: right;">{{ $value1->quantity }}</td>
                                    <td style="font-size: 10px;text-align: right;">{{ $value1->return_quantity }}</td>
                                    <td style="font-size: 10px;text-align: right;">{{ ($value1->quantity - $value1->return_quantity)*$value1->rate }}</td>
                                    <td style="font-size: 10px;text-align: right;">{{ $value1->stock_quantity - $value1->quantity - $value1->return_quantity }}</td>
                                    <td style="font-size: 10px;text-align: right;">{{ ($value1->stock_quantity - $value1->quantity - $value1->return_quantity)*$value1->rate }}</td>
                                </tr>
                                @endif
                                @endforeach

                                <?php
                                    $i++;
                                ?>
                                @endforeach
                                @endif

                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="9" style="text-align: right;">{{ __('messages.total')}}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_production_cost }}</th>
                                    <th colspan="1" style="text-align: right;"></th>
                                    <th colspan="1" style="text-align: right;">{{ $total_stock_value }}</th>
                                    
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>