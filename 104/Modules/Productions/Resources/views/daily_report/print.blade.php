<!DOCTYPE html>
<html>

<head>
    <title>Daily Production Report</title>
    <link rel="icon" href="{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">
    
    <style>
        .form-control, .single-line {border: 1px solid #000!important;}
    </style>

    <style type="text/css" media="print">
        @page {
            size: auto;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom: 10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>   
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="ibox float-e-margins">
                <div style="padding: 0px !important;margin: 0px !important;">
                    <div class="row">
                        <div class="col-md-4 col-xs-4 col-sm-4">
                            <img style="height: 80px; margin: 5px 0 0 5px;float: right" src="{{ url('public/'.userDetails()->logo) }}">
                        </div>
                        <div class="col-md-4 col-xs-4 col-sm-4">
                            <h2 style="text-align: center;font-size: 22px;font-weight: bold;">সাথী অটো রাইস মিল (প্রাঃ) লিঃ</h2>
                            <p style="line-height: 10px;text-align: center;font-size: 16px;">বাঁকা ব্রিক্স ফিল্ড, জীবননগর, চুয়াডাঙ্গা ।</p>
                        </div>
                        <div class="col-md-4 col-xs-4 col-sm-4"></div>
                    </div>

                    <br>

                    <div style="padding: 0px !important;margin: 0px !important;padding-top: 10px !important;" class="ibox-content">
                        <div style="border-right: 1px dotted black;padding: 0px !important;margin: 0px !important;padding-top: 10px !important;" class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                            <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                                <tbody style="border-color: white;" class="theight">
                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>তারিখ</strong></span>
                                            <span class="col-md-4 col-sm-4 col-xs-4 col-lg-4"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ date('d-m-Y', strtotime($production['date'])) }}"></span>
                                        </td>
                                    </tr>

                                    <?php 
                                        $total_kg               = 0;
                                        $total_dor              = 0;
                                        $total_count            = 0;
                                        $dor                    = 1;
                                    ?>
                                    @if($cauls->count() > 0)
                                    @foreach($cauls as $caul)
                                    <?php
                                        $conv               = App\Models\UnitConversions::where('product_entry_id', $caul->product_entry_id)->first();
                                        $conv_rate_caul     = $conv != null ? $conv['conversion_rate'] : 1;
                                    ?>

                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>চাউল</strong></span>
                                            <span class="col-md-3 col-sm-3 col-xs-3 col-lg-3"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $caul->productEntries->name }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>বস্তা</strong></span>
                                            <span class="col-md-3 col-sm-3 col-xs-3 col-lg-3"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $caul->quantity }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>কেজি</strong></span>
                                            <span class="col-md-3 col-sm-3 col-xs-3 col-lg-3"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $conv != null ? $caul->quantity*$conv_rate_caul : 0 }}"></span>
                                        </td>
                                    </tr>
                                    <?php 
                                        $total_kg               = $total_kg + ($conv != null ? $caul->quantity*$conv_rate_caul : 0);
                                        $total_dor              = $total_dor + $caul->rate;  
                                        $total_count++;
                                        $dor                    = ($total_dor/$total_count)/$conv_rate_caul;
                                    ?>
                                    @endforeach
                                    @endif
                                    
                                    <?php  
                                        $total_count = $total_count > 0 ? $total_count : 1;
                                    ?>

                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-6 col-sm-6 col-xs-6 col-lg-6"><strong></strong></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;;text-align: right;" class="col-md-3 col-sm-3 col-xs-3 col-lg-3"><strong>মোট কেজি</strong></span>
                                            <span class="col-md-3 col-sm-3 col-xs-3 col-lg-3"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $total_kg }}"></span>
                                        </td>
                                    </tr>

                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;;" class="col-md-4 col-sm-4 col-xs-4 col-lg-4"><strong>সর্বমোট চাউলের পরিমাণ</strong></span>
                                            <span class="col-md-3 col-sm-3 col-xs-3 col-lg-3"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $total_kg }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;;text-align: right;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>কেজি</strong></span>
                                            <span class="col-md-3 col-sm-3 col-xs-3 col-lg-3"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $dor }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>দর</strong></span>
                                        </td>
                                    </tr>

                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;;" class="col-md-5 col-sm-5 col-xs-5 col-lg-5"><strong></strong></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>মোট</strong></span>
                                            <span class="col-md-5 col-sm-5 col-xs-5 col-lg-5"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $dor*$total_kg }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>টাকা</strong></span>
                                        </td>
                                    </tr>

                                    <?php $total_joma = 0; ?>
                                    @if($finis_gds_extr->count() > 0)
                                    @foreach($finis_gds_extr as $finis_gds_extr_val)
                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><strong>{{ $finis_gds_extr_val->productEntries->name }}</strong></span>
                                            <span style="padding: 0px" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $finis_gds_extr_val->quantity }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><strong>বস্তা X</strong></span>
                                            <span style="padding: 0px" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $finis_gds_extr_val->rate }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>দর</strong></span>
                                            <span style="padding: 0px" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $finis_gds_extr_val->amount }}"></span>
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>টাকা</strong></span>
                                        </td>
                                    </tr>
                                    <?php $total_joma = $total_joma + $finis_gds_extr_val->amount; ?>
                                    @endforeach
                                    @endif

                                    <?php 
                                        $sorbomot_joma = $total_joma + ($dor*$total_kg);
                                    ?>

                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-4 col-sm-4 col-xs-4 col-lg-4"><strong>সর্বমোট জমা</strong></span>
                                            <span class="col-md-7 col-sm-7 col-xs-7 col-lg-7"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $sorbomot_joma }}"></span>
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>টাকা</strong></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                            <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                                <tbody style="border-color: white;" class="theight">
                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>তারিখ</strong></span>
                                            <span class="col-md-4 col-sm-4 col-xs-4 col-lg-4"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ date('d-m-Y', strtotime($production['date'])) }}"></span>
                                        </td>
                                    </tr>

                                    <?php $total_dhan_taka = 0; $total_dhan_mon = 0; ?>
                                    @if($dhans->count() > 0)
                                    @foreach($dhans as $dhan)
                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-3 col-sm-3 col-xs-3 col-lg-3"><strong>{{ $dhan->productEntries->name }}</strong></span>

                                            <span class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $dhan->bosta }}"></span>
                                            <span style="padding-top: 7px;font-size: 12px !important;padding-left: 0px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>বস্তা</strong></span>

                                            <span class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $dhan->quantity }}"></span>
                                            <span style="padding-top: 7px;font-size: 12px !important;padding-left: 0px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>মণ</strong></span>

                                            <span class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $dhan->amount }}"></span>
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>টাকা</strong></span>
                                        </td>
                                    </tr>
                                    <?php
                                        $total_dhan_taka = $total_dhan_taka + $dhan->amount; 
                                        $total_dhan_mon  = $total_dhan_mon + $dhan->quantity; 
                                    ?>
                                    @endforeach
                                    @endif

                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;;" class="col-md-5 col-sm-5 col-xs-5 col-lg-5"><strong></strong></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>মোট</strong></span>
                                            <span class="col-md-5 col-sm-5 col-xs-5 col-lg-5"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $total_dhan_taka }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>টাকা</strong></span>
                                        </td>
                                    </tr>

                                    <!-- <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-3 col-sm-3 col-xs-3 col-lg-3"><strong>সিরিয়ালে ঢালা হিসাব</strong></span>
                                            <span class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control"></span>

                                            <span style="padding: 0px;padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>বস্তা</strong></span>
                                            <span class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;text-align: right;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>কেজি</strong></span>
                                            <span class="col-md-3 col-sm-3 col-xs-3 col-lg-3"><input style="padding: 0px;padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control"></span>
                                        </td>
                                    </tr> -->

                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><strong>মিলিং চার্জ</strong></span>
                                            <span style="padding: 0px" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $total_dhan_mon }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><strong>মন X</strong></span>
                                            <span style="padding: 0px" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $production['milling_charge'] }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>দর</strong></span>
                                            <span style="padding: 0px" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $total_dhan_mon*$production['milling_charge'] }}"></span>
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>টাকা</strong></span>
                                        </td>
                                    </tr>

                                    <?php $total_bosta_amount = 0; ?>
                                    @if($bostas->count() > 0)
                                    @foreach($bostas as $bosta)
                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><strong>{{ $bosta->productEntries->name }}</strong></span>
                                            <span style="padding: 0px" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $bosta->quantity }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><strong>পিস X</strong></span>
                                            <span style="padding: 0px" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $bosta->rate }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>দর</strong></span>
                                            <span style="padding: 0px" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $bosta->amount }}"></span>
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>টাকা</strong></span>
                                        </td>
                                    </tr>
                                    <?php $total_bosta_amount = $total_bosta_amount + $bosta->amount; ?>
                                    @endforeach
                                    @endif

                                    <!-- <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-4 col-sm-4 col-xs-4 col-lg-4">অন্যান্য খরচ<strong></strong></span>
                                            <span class="col-md-7 col-sm-7 col-xs-7 col-lg-7"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control"></span>
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>টাকা</strong></span>
                                        </td>
                                    </tr> -->

                                    <?php
                                        $sorbomot_khoroc = $total_dhan_taka + $total_bosta_amount + ($total_dhan_mon*$production['milling_charge']);

                                        $profit_loss = $sorbomot_joma - $sorbomot_khoroc;
                                    ?>

                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span class="col-md-2 col-sm-2 col-xs-2 col-lg-2"></span>
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-4 col-sm-4 col-xs-4 col-lg-4">সর্বমোট খরচ<strong></strong></span>
                                            <span class="col-md-5 col-sm-5 col-xs-5 col-lg-5"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $sorbomot_khoroc }}"></span>
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><strong>টাকা</strong></span>
                                        </td>
                                    </tr>

                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-4 col-sm-4 col-xs-4 col-lg-4">লাভ হয়েছে<strong></strong></span>
                                            <span class="col-md-5 col-sm-5 col-xs-5 col-lg-5"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $profit_loss > 0 ? $profit_loss : '' }}"></span>
                                        </td>
                                    </tr>

                                    <tr style="border: 1px solid white !important">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-4 col-sm-4 col-xs-4 col-lg-4">লস হয়েছে<strong></strong></span>
                                            <span class="col-md-5 col-sm-5 col-xs-5 col-lg-5"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $profit_loss < 0 ? abs($profit_loss) : '' }}"></span>
                                        </td>
                                    </tr>

                                    <?php
                                        $total_kg = $total_kg > 0 ? $total_kg : 1;
                                        $porta = round(($sorbomot_khoroc - $total_joma)/$total_kg, 2);
                                        $ex    = explode('.', $porta);
                                        $taka  = isset($ex[0]) ? $ex[0] : 0;
                                        $poisa = isset($ex[1]) ? $ex[1] : 0;

                                        $resio = round($total_kg/$total_dhan_mon, 2);
                                        $ex1    = explode('.', $resio);
                                        $taka1  = isset($ex1[0]) ? $ex1[0] : 0;
                                        $poisa1 = isset($ex1[1]) ? $ex1[1] : 0;

                                    ?>

                                    <tr style="border: 1px solid white !important;padding: 10px">
                                        <td style="padding: 10px"></td>
                                    </tr>
                                    <tr style="border: 1px solid white !important;">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><strong>পড়তা</strong></span>
                                            <span style="padding:  0px !important" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $taka }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><strong>টাকা</strong></span>
                                            <span style="padding:  0px !important" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $poisa }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><strong>পয়সা</strong></span>
                                        </td>
                                    </tr>

                                    <tr style="border: 1px solid white !important;">
                                        <td style="text-align: center;border: 1px solid white !important">
                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><strong>রেশিও</strong></span>
                                            <span style="padding:  0px !important" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $taka1 }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><strong>কেজি</strong></span>
                                            <span style="padding:  0px !important" class="col-md-1 col-sm-1 col-xs-1 col-lg-1"><input style="padding-top: 7px;font-size: 12px !important;" readonly type="text" class="form-control" value="{{ $poisa1 }}"></span>

                                            <span style="padding-top: 7px;font-size: 12px !important;" class="col-md-2 col-sm-2 col-xs-2 col-lg-2"><strong>গ্রাম</strong></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div style="margin-top: 40px" class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                            <div class="col-md-2 col-sm-2 col-xs-2 col-lg-2">
                                <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">স্টক ম্যানেজারের স্বাক্ষর </span> </h6>
                            </div>
                            
                            <div class="col-md-2 col-sm-2 col-xs-2 col-lg-2">
                                <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">কম্পিউটার অপারেটর স্বাক্ষর  </span> </h6>
                            </div>
                            
                            <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                                <h6 style="text-align: center"> <span style="border-top: 1px dotted black;padding: 5px">জেনারেল ম্যানেজারের স্বাক্ষর</span> </h6>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                                <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">পক্ষে : সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</span> </h6>
                            </div>
                        </div>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>
</body>
</html>