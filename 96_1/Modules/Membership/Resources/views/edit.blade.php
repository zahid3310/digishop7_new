@extends('layouts.app')

@section('title', 'Membership')


@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Membership</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Registers</a></li>
                                    <li class="breadcrumb-item active">Add Membership</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('memberships_update',$data->id) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div class="row">

                                	<div  class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Customer</label>
                                        <input name="customers_name" type="text" class="form-control" value="{{$data->customers_name}}" readonly="">
                                    </div>

                                    <div  class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Membership Package</label>
                                        <select class="form-control select2" name="membership_package_id" id="membership_package_id">
                                        	<option value="" hidden="">--Membership Package--</option>
                                        	@foreach($MembershipPackages as $key => $list)
                                        	<option value="{{$list->id}}" @if($list->id == $data->membership_package_id) selected  @endif>{{$list->package_name}}</option>
                                        	@endforeach
                                        </select>
                                    </div>

                                    <div  class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Discount</label>
                                        <input name="discount" type="number" class="form-control Membershipdiscount" placeholder="Discount" value="{{$data->discount}}">
                                    </div>

                                    <div  class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Card Number</label>
                                        <input name="card_number" type="text" class="form-control" placeholder="Card Number" value="{{$data->card_number}}">
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('membership_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Membership Lists</h4>

                                <div style="margin-right: 10px" class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-1">Search : </div>
                                    <div class="col-md-2">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Member Name</th>
                                            <th>Contact Number</th>
                                            <th>Card Number</th>
                                            <th>Membership Package</th>
                                            <th>Discount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        @foreach($memberships as $key=> $list)
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td>{{$list->customers_name}}</td>
                                            <td>{{$list->customers_phone}}</td>
                                            <td>{{$list->card_number}}</td>
                                            <td>{{$list->package_name}}</td>
                                            <td>{{$list->discount}}</td>
                                            <td>
                                                <a href="{{route('memberships_edit',$list->id)}}" class="btn btn-primary">Edit</a>

                                                <a href="{{route('memberships_delete',$list->id)}}" class="btn btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$( document ).ready(function() {

			var site_url  = $('.site_url').val();

			$("#customer_id").select2({
				ajax: { 
					url:  site_url + '/invoices/customer/list/invoices',
					type: "get",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
			searchTerm: params.term // search term
		};
	},
	processResults: function (response) {
		return {
			results: response
		};
	},
	cache: true
	},

	minimumInputLength: 0,
	escapeMarkup: function(result) {
		return result;
	},
	templateResult: function (result) {
		if (result.loading) return 'Searching...';

		if ((result['contact_type'] == 0 || result['contact_type'] == 1) && result['id'] != 0)
		{
			return result['text'];
		}
	},
	});

	});
	</script>

	<script type="text/javascript">
			var site_url  = $('.site_url').val();
			$(document).on("change", "#membership_package_id" , function() {

            var site_url        = $('.site_url').val();
            var membership_package_id      = $("#membership_package_id").val();
            $(".Membershipdiscount").empty();
            $.get(site_url + '/membership/discount/' + membership_package_id, function(data)
            {
                $(".Membershipdiscount").val(data.discount);
                
            });
        });
	</script>

@endsection