@extends('layouts.app')

@section('title', 'Print Pos')

@if($user_info['pos_printer'] == 0)
<style type="text/css">
    @media print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 60mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }
    }
</style>
@endif

@if($user_info['pos_printer'] == 1)
<style type="text/css">
    @media print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 90mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }
    }
</style>
@endif

@if($user_info['pos_printer'] == 2 || $user_info['pos_printer'] == 3)
<style type="text/css">
    @media print {
        #footerInvoice {
            position: fixed;
            bottom: 60;
        }
    }

    .textstyle {
        background-color: white; /* Changing background color */
        font-weight: bold; /* Making font bold */
        border-radius: 20px; /* Making border radius */
        border: 3px solid black; /* Making border radius */
        width: auto; /* Making auto-sizable width */
        height: auto; /* Making auto-sizable height */
        padding: 5px 10px 5px 10px; /* Making space around letters */
        font-size: 18px; /* Changing font size */
    }

    .column-bordered-table thead td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table tfoot tr {
        border-top: 1px solid #c3c3c3;
        border-bottom: 1px solid #c3c3c3;
    }

    table,th {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 2px;
    }

    td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        padding: 2px;
    }

    @page {
        size: A4;
        page-break-after: always;
    }
</style>
@endif

@section('content')
    <div class="main-content marginTopPrint">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row d-print-none">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Print Pos</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                @if($user_info['pos_printer'] == 0)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div id="hedColor" style="margin-top: 0px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold">{{ $user_info['organization_name'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold">{{ $user_info['address'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 18px;font-weight: bold">{{ $user_info['contact_number'] }}</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 18px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">Invoice</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 14px">Date : {{ date('d/m/Y', strtotime($invoice['invoice_date'])) }}</span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 14px">{{ date('h:i a', strtotime(now())) }}</span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 14px">Invoice# : {{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }}</span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">
                                    
                                </div>

                                <div style="border-top: 2px dashed black;border-bottom: 2px dashed black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px;" class="col-2"><strong>Qt</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-4"><strong>Item</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Price</strong></div>
                                    <div style="font-size: 12px;text-align: right;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Total</strong></div>
                                </div>

                                @if(!empty($entries) && ($entries->count() > 0))

                                    <?php 
                                        $sub_total              = 0; 
                                        $total_discount         = 0;
                                    ?>

                                    @foreach($entries as $key => $value)

                                    <div style="padding-bottom: 10px;" id="DetailsFontSize" class="row">

                                        <?php 
                                            $discount           = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; 
                                            $total_discount     = $total_discount + $discount;
                                        ?>

                                        <div style="font-size: 12px" class="col-2">
                                            {{ $value['quantity'] }} <br> {{ $value->convertedUnit->name }}
                                        </div>

                                        <div style="font-size: 12px" class="col-4">
                                            @if($value['product_type'] == 1)
                                                <?php echo $value['product_entry_name']; ?>
                                            @else
                                                <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                            @endif
                                        </div>
                                        <div style="font-size: 12px" class="col-3">
                                            {{ number_format($value['rate']) }}
                                        </div>
                                        <div style="font-size: 12px;text-align: right" class="col-3">{{ number_format($value['total_amount']) }}</div>
                                    </div>

                                    <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                    @endforeach
                                @endif

                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>GROSS Total :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong>{{ number_format($sub_total) }}</strong></div>
                                </div>

                                <!-- <div style="padding-bottom: 10px" class="row"></div> -->

                                <?php
                                    $total_vat_amount       = $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : $invoice['total_vat']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>Total VAT :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong>{{ number_format($total_vat_amount) }}</strong></div>
                                </div>

                                <?php
                                    $total_discount_amount       = $invoice['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$invoice['total_vat'])/100))*$invoice['total_discount_amount'])/100) : $invoice['total_discount_amount']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>Total Discount :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong>{{ number_format($total_discount + $total_discount_amount) }}</strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;" class="row">
                                    <div style="font-size: 14px" class="col-6"><strong>NET Total :</strong></div>
                                    <div style="font-size: 14px;text-align: right" class="col-6"><strong>{{ number_format($sub_total + $total_vat_amount - $total_discount_amount) }}</strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px;" class="row"></div> -->
                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px;" class="row"></div> -->

                               <!--  <div class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-12">Payments</div>
                                </div> -->

                                <div style="padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 14px;font-weight: bold" class="col-6">Total Paid</div>
                                    <div style="font-size: 14px;text-align: right;font-weight: bold" class="col-6">{{ number_format($invoice['cash_given']) }}</div>
                                </div>

                                <!-- <div style="padding-left: 15px;border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 16px" class="col-6">-TOTAL DUES</div>
                                    <div style="font-size: 16px;text-align: right" class="col-6">{{ number_format($invoice['due_amount'],2,'.',',') }}</div>
                                </div> -->

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 12px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if($user_info['pos_printer'] == 1)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div id="hedColor" style="margin-top: 0px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight: bold">{{ $user_info['organization_name'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight: bold">{{ $user_info['address'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 20px;font-weight: bold">{{ $user_info['contact_number'] }}</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 20px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">Invoice</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 16px">Date : {{ date('d/m/Y', strtotime($invoice['invoice_date'])) }}</span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 16px">Time : {{ date('h:i a', strtotime(now())) }}</span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 16px">Invoice# : {{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }}</span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">
                                    
                                </div>

                                <div style="border-top: 2px dashed black;border-bottom: 2px dashed black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-2"><strong>Qty</strong></div>
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-4"><strong>Item Name</strong></div>
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Price</strong></div>
                                    <div style="font-size: 16px;text-align: right;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>T.Price</strong></div>
                                </div>

                                @if(!empty($entries) && ($entries->count() > 0))

                                    <?php 
                                        $sub_total              = 0; 
                                        $total_discount         = 0;
                                    ?>

                                    @foreach($entries as $key => $value)

                                    <div style="padding-bottom: 10px;" id="DetailsFontSize" class="row">

                                        <?php 
                                            $discount           = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; 
                                            $total_discount     = $total_discount + $discount;
                                        ?>

                                        <div style="font-size: 16px" class="col-2">
                                            {{ $value['quantity'] }} <br> {{ $value->convertedUnit->name }}
                                        </div>

                                        <div style="font-size: 16px" class="col-4">
                                            @if($value['product_type'] == 1)
                                                <?php echo $value['product_entry_name']; ?>
                                            @else
                                                <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                            @endif
                                        </div>
                                        <div style="font-size: 16px" class="col-3">
                                            {{ number_format($value['rate']) }}
                                        </div>
                                        <div style="font-size: 16px;text-align: right" class="col-3">{{ number_format($value['total_amount']) }}</div>
                                    </div>

                                    <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                    @endforeach
                                @endif

                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>GROSS Total :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong>{{ number_format($sub_total) }}</strong></div>
                                </div>

                                <!-- <div style="padding-bottom: 10px" class="row"></div> -->

                                <?php
                                    $total_vat_amount       =  $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : $invoice['total_vat']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>Total VAT :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong>{{  number_format($total_vat_amount) }}</strong></div>
                                </div>

                                <?php
                                    $total_discount_amount       = $invoice['total_discount_type'] == 0 ? ((($sub_total + $total_vat_amount)*$invoice['total_discount_amount'])/100) : $invoice['total_discount_amount']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>Total Discount :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong>{{ number_format($total_discount + $total_discount_amount) }}</strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;" class="row">
                                    <div style="font-size: 20px" class="col-6"><strong>NET Total :</strong></div>
                                    <div style="font-size: 20px;text-align: right" class="col-6"><strong>{{ number_format($sub_total + $total_vat_amount - $total_discount_amount) }}</strong></div>
                                </div>

                                <!-- <div style="border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 20px" class="col-12"><strong>VAT Included</strong></div>
                                </div> -->

                                <!-- <div style="padding-top: 10px;" class="row"></div> -->
                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px;" class="row"></div> -->

                               <!--  <div class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-12">Payments</div>
                                </div> -->

                                <div style="padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-6">Total Paid</div>
                                    <div style="font-size: 18px;text-align: right;font-weight: bold" class="col-6">{{ number_format($invoice['cash_given']) }}</div>
                                </div>

                                <!-- <div style="padding-left: 15px;border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 16px" class="col-6">-TOTAL DUES</div>
                                    <div style="font-size: 16px;text-align: right" class="col-6">{{ number_format($invoice['due_amount'],2,'.',',') }}</div>
                                </div> -->

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 16px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if($user_info['pos_printer'] == 2 || $user_info['pos_printer'] == 3)
                    <div style="padding: 10px;padding-top: 25px" class="row">
                    <div style="padding-bottom: 30px" class="d-print-none col-md-12">
                        <div class="float-right">
                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 22px;padding-top: 10px;font-weight: bold">{{ $user_info['organization_name'] }}</h2>
                                        <p style="line-height: 1;font-size: 20px;font-weight: bold" class="text-center">Sonjon Kumar Ghosh</p>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        {{ QrCode::size(70)->generate("string") }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px"> Baten Kha Mor, Chapainawabganj-6300 </p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">Mob: 01916555577, 01916555588 & 01916555599</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 13px">Email: ratonandsons@gmail.com</p>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 15px" class="col-md-8">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Invoice No : - </strong>{{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }} <br>
                                            <span style="font-weight: bold">SR/Customer :   </span>{{ $invoice['customer_name'] }}
                                        </address>
                                    </div>
                                    <div style="font-size: 15px;" class="col-md-4 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Date :  </strong>
                                            {{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}<br><br>
                                        </address>
                                    </div>
                                    <div style="font-size: 15px" class="col-md-12">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Address : </strong> {{ $invoice->customer->address != null ? $invoice->customer->address : ''  }}
                                        </address>

                                        <address style="margin-bottom: 0px !important">
                                            <strong>Mobile : </strong> {{ $invoice->customer->phone != null ? $invoice->customer->phone : ''  }}
                                        </address>
                                    </div>
                                </div>

                                <div style="padding-top: 10px;padding-bottom: 20px">
                                    <table style="width: 100%">
                                        <tr>
                                            <th style="font-size: 18px;width: 5%">SL</th>
                                            <th style="font-size: 18px;width: 60%">Item Name & Code</th>
                                            <th style="font-size: 18px;width: 10%">Quantity</th>
                                            <th style="font-size: 18px;width: 15%">Rate</th>
                                            <th style="font-size: 18px;width: 10%">Taka</th>
                                        </tr>

                                        @if($entries->count() > 0)

                                        <?php
                                            $total_amount                   = 0;
                                        ?>

                                        @foreach($entries as $key => $value)
                                        <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $variation_name = ProductVariationName($value['product_entry_id']);

                                            if ($value['product_code'] != null)
                                            {
                                                $productCode  = ' - '.$value['product_code'];
                                            }
                                            else
                                            {
                                                $productCode  = '';
                                            }

                                            if ($value['product_name'] != null)
                                            {
                                                $category  = ' - '.$value['product_name'];
                                            }
                                            else
                                            {
                                                $category  = '';
                                            }

                                            if ($value['brand_name'] != null)
                                            {
                                                $brandName  = $value['brand_name'];
                                            }
                                            else
                                            {
                                                $brandName  = '';
                                            }

                                            if ($value['unit_name'] != null)
                                            {
                                                $unit  = ' '.$value['unit_name'];
                                            }
                                            else
                                            {
                                                $unit  = '';
                                            }

                                            if ($variation_name != null)
                                            {
                                                $variation  = ' '.$variation_name;
                                            }
                                            else
                                            {
                                                $variation  = '';
                                            }

                                            $pre_dues = $invoice['previous_due'];
                                            $net_paya = round($total_amount, 2);
                                            $paid     = round($invoice['cash_given'], 2);
                                            $dues     = round($net_paya - $paid, 2);
                                        ?>

                                        <tr class="tr-height">
                                            <td style="text-align: center">{{ $key + 1 }}</td>
                                            <td style="padding-left: 30px">{{ $value['product_entry_name'] . $productCode }}</td>
                                            <td style="text-align: center">{{ $value['quantity'] }}</td>
                                            <td style="text-align: center">{{ $value['rate'] . $unit }}</td>
                                            <td style="text-align: center">{{ round($value['total_amount'], 2) }}</td>
                                        </tr>
                                        @endforeach
                                        @endif

                                        <?php
                                            if ($invoice['vat_type'] == 0)
                                            {
                                                $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $vat_amount  = $invoice['total_vat'];
                                            }

                                            if ($invoice['total_discount_type'] == 0)
                                            {
                                                $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $discount_on_total_amount  = $invoice['total_discount_amount'];
                                            }
                                        ?>

                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="4"><strong>Total</strong></th>
                                            <th style="text-align: center">{{ $net_paya != 0 ? round($net_paya - $invoice['total_discount']) : '' }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4">Discount</strong></th>
                                            <th style="text-align: center">{{ round($discount_on_total_amount + $invoice['total_discount']) }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4">Payable</strong></th>
                                            <th style="text-align: center">{{ round($invoice['invoice_amount']) }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Previous Due</strong></th>
                                            <th style="text-align: center">{{ $pre_dues  != 0 ? round($pre_dues) : '' }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Total Due</strong></th>
                                            <th style="text-align: center">{{ $invoice['invoice_amount'] + $pre_dues ? round($invoice['invoice_amount'] + $pre_dues) : '' }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Paid</strong></th>
                                            <th style="text-align: center">{{ $paid != 0 ? round($paid, 2) : '' }}</th>
                                        </tr>

                                        <!--<tr>-->
                                        <!--    <th style="text-align: right" colspan="4"><strong>Due Balance</strong></th>-->
                                        <!--    <th style="text-align: center">{{ $invoice['invoice_amount'] + $pre_dues - $paid != 0 ? round($invoice['invoice_amount'] + $pre_dues - $paid) : '' }}</th>-->
                                        <!--</tr>  -->
                                        
                                        @if($invoice['cash_given'] < $invoice['invoice_amount'])
                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Due Balance</strong></th>
                                            <th style="text-align: center">{{ $invoice['invoice_amount'] + $pre_dues - $paid != 0 ? round($invoice['invoice_amount'] + $pre_dues - $paid) : '' }}</th>
                                        </tr>
                                        @elseif($invoice['cash_given'] > $invoice['invoice_amount'])
                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Change Amount</strong></th>
                                            <th style="text-align: center">{{ $invoice['change_amount'] != 0 ? round($invoice['change_amount']) : '' }}</th>
                                        </tr>
                                        @endif
                                    </table>
                                </div>

                                <br>
                                <br>
                                <br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">Customer Sign </span> </h6>
                                    </div>
                                    <div class="col-md-6">
                                        <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">Raton & Sons Sign</span> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 22px;padding-top: 10px;font-weight: bold">{{ $user_info['organization_name'] }}</h2>
                                        <p style="line-height: 1;font-size: 20px;font-weight: bold" class="text-center">Sonjon Kumar Ghosh</p>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        {{ QrCode::size(70)->generate("string") }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px"> Baten Kha Mor, Chapainawabganj-6300 </p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">Mob: 01916555577, 01916555588 & 01916555599</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 13px">Email: ratonandsons@gmail.com</p>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 15px" class="col-md-8">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Invoice No : - </strong>{{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }} <br>
                                            <span style="font-weight: bold">SR/Customer :   </span>{{ $invoice['customer_name'] }}
                                        </address>
                                    </div>
                                    <div style="font-size: 15px;" class="col-md-4 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Date :  </strong>
                                            {{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}<br><br>
                                        </address>
                                    </div>
                                    <div style="font-size: 15px" class="col-md-12">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Address : </strong> {{ $invoice->customer->address != null ? $invoice->customer->address : ''  }}
                                        </address>

                                        <address style="margin-bottom: 0px !important">
                                            <strong>Mobile : </strong> {{ $invoice->customer->phone != null ? $invoice->customer->phone : ''  }}
                                        </address>
                                    </div>
                                </div>

                                <div style="padding-top: 10px;padding-bottom: 20px">
                                    <table style="width: 100%">
                                        <tr>
                                            <th style="font-size: 18px;width: 5%">SL</th>
                                            <th style="font-size: 18px;width: 60%">Item Name & Code</th>
                                            <th style="font-size: 18px;width: 10%">Quantity</th>
                                            <th style="font-size: 18px;width: 15%">Rate</th>
                                            <th style="font-size: 18px;width: 10%">Taka</th>
                                        </tr>

                                        @if($entries->count() > 0)

                                        <?php
                                            $total_amount                   = 0;
                                        ?>

                                        @foreach($entries as $key => $value)
                                        <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $variation_name = ProductVariationName($value['product_entry_id']);

                                            if ($value['product_code'] != null)
                                            {
                                                $productCode  = ' - '.$value['product_code'];
                                            }
                                            else
                                            {
                                                $productCode  = '';
                                            }

                                            if ($value['product_name'] != null)
                                            {
                                                $category  = ' - '.$value['product_name'];
                                            }
                                            else
                                            {
                                                $category  = '';
                                            }

                                            if ($value['brand_name'] != null)
                                            {
                                                $brandName  = $value['brand_name'];
                                            }
                                            else
                                            {
                                                $brandName  = '';
                                            }

                                            if ($value['unit_name'] != null)
                                            {
                                                $unit  = ' '.$value['unit_name'];
                                            }
                                            else
                                            {
                                                $unit  = '';
                                            }

                                            if ($variation_name != null)
                                            {
                                                $variation  = ' '.$variation_name;
                                            }
                                            else
                                            {
                                                $variation  = '';
                                            }

                                            $pre_dues = $invoice['previous_due'];
                                            $net_paya = round($total_amount, 2);
                                            $paid     = round($invoice['cash_given'], 2);
                                            $dues     = round($net_paya - $paid, 2);
                                        ?>

                                        <tr class="tr-height">
                                            <td style="text-align: center">{{ $key + 1 }}</td>
                                            <td style="padding-left: 30px">{{ $value['product_entry_name'] . $productCode }}</td>
                                            <td style="text-align: center">{{ $value['quantity'] }}</td>
                                            <td style="text-align: center">{{ $value['rate'] . $unit }}</td>
                                            <td style="text-align: center">{{ round($value['total_amount'], 2) }}</td>
                                        </tr>
                                        @endforeach
                                        @endif

                                        <?php
                                            if ($invoice['vat_type'] == 0)
                                            {
                                                $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $vat_amount  = $invoice['total_vat'];
                                            }

                                            if ($invoice['total_discount_type'] == 0)
                                            {
                                                $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $discount_on_total_amount  = $invoice['total_discount_amount'];
                                            }
                                        ?>

                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="4"><strong>Total</strong></th>
                                            <th style="text-align: center">{{ $net_paya != 0 ? round($net_paya - $invoice['total_discount']) : '' }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4">Discount</strong></th>
                                            <th style="text-align: center">{{ round($discount_on_total_amount + $invoice['total_discount']) }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4">Payable</strong></th>
                                            <th style="text-align: center">{{ round($invoice['invoice_amount']) }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Previous Due</strong></th>
                                            <th style="text-align: center">{{ $pre_dues  != 0 ? round($pre_dues) : '' }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Total Due</strong></th>
                                            <th style="text-align: center">{{ $invoice['invoice_amount'] + $pre_dues ? round($invoice['invoice_amount'] + $pre_dues) : '' }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Paid</strong></th>
                                            <th style="text-align: center">{{ $paid != 0 ? round($paid, 2) : '' }}</th>
                                        </tr>

                                        <!--<tr>-->
                                        <!--    <th style="text-align: right" colspan="4"><strong>Due Balance</strong></th>-->
                                        <!--    <th style="text-align: center">{{ $invoice['invoice_amount'] + $pre_dues - $paid != 0 ? round($invoice['invoice_amount'] + $pre_dues - $paid) : '' }}</th>-->
                                        <!--</tr>  -->
                                        
                                        @if($invoice['cash_given'] < $invoice['invoice_amount'])
                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Due Balance</strong></th>
                                            <th style="text-align: center">{{ $invoice['invoice_amount'] + $pre_dues - $paid != 0 ? round($invoice['invoice_amount'] + $pre_dues - $paid) : '' }}</th>
                                        </tr>
                                        @elseif($invoice['cash_given'] > $invoice['invoice_amount'])
                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Change Amount</strong></th>
                                            <th style="text-align: center">{{ $invoice['change_amount'] != 0 ? round($invoice['change_amount']) : '' }}</th>
                                        </tr>
                                        @endif
                                    </table>
                                </div>

                                <br>
                                <br>
                                <br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">Customer Sign </span> </h6>
                                    </div>
                                    <div class="col-md-6">
                                        <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">Raton & Sons Sign</span> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        javascript:window.print();
    });

    window.onafterprint = function(e){
        var site_url  = $('.site_url').val();
        window.location.replace(site_url + '/invoices');
    };
</script>
@endsection