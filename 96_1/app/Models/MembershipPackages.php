<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MembershipPackages extends Model
{
    protected $table = "memberships_package";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }
}
