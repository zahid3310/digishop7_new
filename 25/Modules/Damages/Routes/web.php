<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('damages')->group(function() {
    Route::get('/', 'DamagesController@index')->name('damages_index');
    Route::get('/create', 'DamagesController@create')->name('damages_create');
    Route::post('/store', 'DamagesController@store')->name('damages_store');
    Route::get('/edit/{id}', 'DamagesController@edit')->name('damages_edit');
    Route::post('/update/{id}', 'DamagesController@update')->name('damages_update');
});

Route::prefix('receive-from-customer')->group(function() {
    Route::get('/', 'DamagesController@ReceiveFromCustomerIndex')->name('damages_receive_from_customer_index');
    Route::get('/create', 'DamagesController@ReceiveFromCustomerCreate')->name('damages_receive_from_customer_create');
    Route::post('/store', 'DamagesController@ReceiveFromCustomerStore')->name('damages_receive_from_customer_store');
    Route::get('/edit/{id}', 'DamagesController@ReceiveFromCustomerEdit')->name('damages_receive_from_customer_edit');
    Route::post('/update/{id}', 'DamagesController@ReceiveFromCustomerUpdate')->name('damages_receive_from_customer_update');
    Route::get('/damage-from-customer-price-list/{product_entry_id}', 'DamagesController@productPriceList')->name('damages_receive_from_customer_price_list');
    Route::get('/damage-from-customer-transfer/{id}', 'DamagesController@receiveFromCustomerTransfer')->name('damages_receive_from_customer_transfer');
    Route::post('/damage-from-customer-transfer-store/{id}', 'DamagesController@receiveFromCustomerTransferStore')->name('damages_receive_from_customer_transfer_store');
});

Route::prefix('transfer-to-dp')->group(function() {
    Route::get('/', 'DamagesController@TransferToDpIndex')->name('damages_transfer_to_dp_index');
    Route::get('/edit/{id}', 'DamagesController@TransferToDpEdit')->name('damages_transfer_to_dp_edit');
    Route::post('/update/{id}', 'DamagesController@TransferToDpUpdate')->name('damages_transfer_to_dp_update');
});

Route::prefix('received-from-dp')->group(function() {
    Route::get('/', 'DamagesController@ReceiveFromDpIndex')->name('damages_receive_from_dp_index');
    Route::get('/create', 'DamagesController@ReceiveFromDpCreate')->name('damages_receive_from_dp_create');
    Route::post('/store', 'DamagesController@ReceiveFromDpStore')->name('damages_receive_from_dp_store');
    Route::get('/edit/{id}', 'DamagesController@ReceiveFromDpEdit')->name('damages_receive_from_dp_edit');
    Route::post('/update/{id}', 'DamagesController@ReceiveFromDpUpdate')->name('damages_receive_from_dp_update');
});

Route::prefix('return-to-customer')->group(function() {
    Route::get('/', 'DamagesController@ReturnToCustomerIndex')->name('damages_return_to_customer_index');
    Route::get('/create', 'DamagesController@ReturnToCustomerCreate')->name('damages_return_to_customer_create');
    Route::post('/store', 'DamagesController@ReturnToCustomerStore')->name('damages_return_to_customer_store');
    Route::get('/edit/{id}', 'DamagesController@ReturnToCustomerEdit')->name('damages_return_to_customer_edit');
    Route::post('/update/{id}', 'DamagesController@ReturnToCustomerUpdate')->name('damages_return_to_customer_update');
});