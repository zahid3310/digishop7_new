<!DOCTYPE html>
<html>

<head>
    <title>SM Wise Customer Sales Report</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">
</head>

<style type="text/css">        
    @page {
        size: auto;   
        margin: 15mm 5mm 5mm 10mm;
    }

    #print-footer {
        display: none;
        position: fixed;
        bottom: 0;
        left:0;
    }

    #hide-print {
        display: none;
    }

    #print-footer {
        display: none;
    }

    body {
        margin-bottom:10mm;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #ddd;
    }

    .table td, .table th {
        font-size: 12px !important;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">SM Wise Customer Sales Report</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">SM</th>
                                    <th style="text-align: center">AREA</th>
                                    <th style="text-align: center">STORE</th>
                                    <th style="text-align: center">DATE FROM</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">
                                        @if($sm_name != null)
                                            {{ $sm_name->name }}
                                        @else
                                            ALL
                                        @endif
                                    </td>

                                    <td style="text-align: center">
                                        @if($sm_name  != null)
                                            {{ $sm_name->area_name  }}
                                        @elseif($area_name != null)
                                            {{$area_name->name}}
                                        @else
                                            All
                                        @endif
                                    </td>

                                    <td style="text-align: center">
                                        @if($sm_name  != null)
                                            {{ $sm_name->stores_name  }}
                                        @elseif($store_name != null)
                                            {{$store_name->name}}
                                        @else
                                            All
                                        @endif
                                    </td>

                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>

                                </tr>
                            </tbody>
                        </table>

                       <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 20%">CUSTOMER NAME</th>
                                    <th style="text-align: center;width: 10%">SALES</th>
                                    <th style="text-align: center;width: 10%">RECEIVED</th>
                                    <th style="text-align: center;width: 10%">DUE LIMIT</th>
                                    <th style="text-align: center;width: 10%">DUES</th>
                                    <th style="text-align: center;width: 37%">DAILY REPORT</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php

                                    $i = 1;
                                    $total_sales = 0; 
                                    $total_received = 0;
                                    $total_due_limit = 0;
                                    $total_due = 0;
                                ?>
                                @foreach($data as $key=> $value)

                                <?php
                                    $sub_total_sales                = 0;
                                    $sub_total_received             = 0;
                                    $sub_total_due_limit            = 0;
                                    $sub_total_due                  = 0;
                                ?>

                                @foreach($value as $key1 => $value1)


                                <?php 

                                    $sub_total_sales = $sub_total_sales +  $value1['invoice_amount'];
                                    $sub_total_received = $sub_total_received +  $value1['cash_given'];
                                    $sub_total_due_limit = $sub_total_due_limit +  $value1['credit_limit'];
                                    $sub_total_due        = $sub_total_due +  $value1['customer_balance'];

                                ?>
                                        
                                @endforeach


                           
                                <tr>
                                    <td style="text-align: center;vertical-align: middle;" rowspan="{{ count($value) }}">{{ $i++ }}</td>
                                    <td style="text-align: left;vertical-align: middle;" rowspan="{{ count($value) }}">{{$value[0]['customer_name']}}</td>

                                    <td style="text-align: right;" rowspan="{{ count($value) }}">{{ $sub_total_sales }}</td>
                                    <td style="text-align: right;" rowspan="{{ count($value) }}">{{ $sub_total_received }}</td>
                                    <td style="text-align: right;" rowspan="{{ count($value) }}">{{$value[0]['credit_limit']}}</td>
                                    <!--<td style="text-align: right;" rowspan="{{ count($value) }}">{{$sub_total_sales -$sub_total_received }}</td>-->
                                    <td style="text-align: right;" rowspan="{{ count($value) }}">{{$value[0]['customer_balance']}}</td>

                                    <td style="text-align: right;" rowspan="{{ count($value) }}"></td>
                                </tr>


                                @foreach($value as $key1 => $value1)
                                <tr></tr>    
                                @endforeach

                                <?php
                                    $total_sales = $total_sales + $sub_total_sales;
                                    $total_received = $total_received + $sub_total_received;
                                    $total_due_limit = $total_due_limit + $sub_total_due_limit;
                                    $total_due      = $total_due + $sub_total_due;
                                ?>
                                
                                
                                @endforeach
                               
                               <tr>
                                    <td style="text-align: right;font-weight:bold" colspan="2">Total</td>
                                    <td style="text-align: right;">{{ $total_sales }}</td>
                                    <td style="text-align: right;">{{ $total_received }}</td>
                                    <td style="text-align: right;">{{ $total_due_limit }}</td>
                                    <td style="text-align: right;">{{ $total_due }}</td>
                                    <td style="text-align: right;"></td>
                                </tr>
                            </tbody>


                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script> -->
<!-- <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script> -->
<!-- <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script> -->

</body>
</html>