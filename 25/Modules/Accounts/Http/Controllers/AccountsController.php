<?php

namespace Modules\Accounts\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Response;
use Auth;
use DB;

//Models
use App\Models\ParentAccountTypes;
use App\Models\AccountTypes;
use App\Models\Accounts;

class AccountsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    ## Account Type Crude Start
    public function accountTypesIndex()
    {
    	$parent_accounts 	= ParentAccountTypes::get();
    	$account_types  	= AccountTypes::get();

        return view('accounts::account_types.index', compact('parent_accounts', 'account_types'));
    }

    public function accountTypesStore(Request $request)
    {
        $rules = array(
            'name'      			=> 'required',
            'parent_account_id'   	=> 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
        	$account_type                    		= new AccountTypes;
            $account_type->account_name             = $data['name'];
            $account_type->parent_account_type_id   = $data['parent_account_id'];
            $account_type->description       		= $data['description'];
            $account_type->status       			= $data['status'];
            
            if ($account_type->save())
            {	
            	DB::commit();
                return redirect()->route('account_types_index')->with("success","Account Type Created Successfully !!");
            }
                
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function accountTypesEdit($id)
    {
    	$parent_accounts 	= ParentAccountTypes::get();
    	$account_types  	= AccountTypes::get();
    	$find_account_type  = AccountTypes::find($id);

        return view('accounts::account_types.edit', compact('parent_accounts', 'account_types', 'find_account_type'));
    }

    public function accountTypesUpdate(Request $request, $id)
    {
        $rules = array(
            'name'      			=> 'required',
            'parent_account_id'   	=> 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
        	$account_type                    		= AccountTypes::find($id);
            $account_type->account_name             = $data['name'];
            $account_type->parent_account_type_id   = $data['parent_account_id'];
            $account_type->description       		= $data['description'];
            $account_type->status       			= $data['status'];
            
            if ($account_type->save())
            {	
            	DB::commit();
                return redirect()->route('account_types_index')->with("success","Account Type Updated Successfully !!");
            }
                
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }
    ## Account Type Crude End

    ## Chart of Accounts Crude Start
    public function chartOfAccountIndex()
    {
    	$account_types 	= AccountTypes::leftjoin('parent_account_types', 'parent_account_types.id', 'account_types.parent_account_type_id')
    								->select('account_types.*', 'parent_account_types.account_name as parent_account_name')
    								->get();

    	$accounts 		= Accounts::whereNotIn('id', [2,3,4,5,6,7,8,9,10,11,12,13])->get();

    	$account_types 	= $account_types->groupBy('parent_account_name');

        return view('accounts::chart_of_accounts.index', compact('accounts', 'account_types'));
    }

    public function chartOfAccountStore(Request $request)
    {
        $rules = array(
            'name'      			=> 'required',
            'account_type_id'   	=> 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
        	$find_parent_account_type       	= AccountTypes::find($data['account_type_id']);
        	$account                    		= new Accounts;
            $account->account_name          	= $data['name'];
            $account->account_type_id   		= $data['account_type_id'];
            $account->parent_account_type_id   	= $find_parent_account_type['parent_account_type_id'];
            $account->description       		= $data['description'];
            $account->status       				= $data['status'];
            
            if ($account->save())
            {	
            	DB::commit();
                return redirect()->route('chart_of_accounts_index')->with("success","Account Created Successfully !!");
            }
                
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function chartOfAccountEdit($id)
    {
    	$account_types 	= AccountTypes::leftjoin('parent_account_types', 'parent_account_types.id', 'account_types.parent_account_type_id')
    								->select('account_types.*', 'parent_account_types.account_name as parent_account_name')
    								->get();

    	$accounts 		= Accounts::whereNotIn('id', [2,3,4,5,6,7,8,9,10,11,12,13])->get();
    	$account_types 	= $account_types->groupBy('parent_account_name');
    	$find_account   = Accounts::find($id);

        return view('accounts::chart_of_accounts.edit', compact('account_types', 'find_account', 'accounts'));
    }

    public function chartOfAccountUpdate(Request $request, $id)
    {
		$rules = array(
            'name'      			=> 'required',
            'account_type_id'   	=> 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
        	$find_parent_account_type       	= AccountTypes::find($data['account_type_id']);
        	$account                    		= Accounts::find($id);
            $account->account_name          	= $data['name'];
            $account->account_type_id   		= $data['account_type_id'];
            $account->parent_account_type_id   	= $find_parent_account_type['parent_account_type_id'];
            $account->description       		= $data['description'];
            $account->status       				= $data['status'];
            
            if ($account->save())
            {	
            	DB::commit();
                return redirect()->route('chart_of_accounts_index')->with("success","Account Created Successfully !!");
            }
                
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }
    ## Chart of Accounts Crude End
}
