<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('accounts/account-types')->group(function() {
    Route::get('/', 'AccountsController@accountTypesIndex')->name('account_types_index');
    Route::post('/store', 'AccountsController@accountTypesStore')->name('account_types_store');
    Route::get('/edit/{id}', 'AccountsController@accountTypesEdit')->name('account_types_edit');
    Route::post('/update/{id}', 'AccountsController@accountTypesUpdate')->name('account_types_update');
});

Route::prefix('accounts/chart-of-accounts')->group(function() {
    Route::get('/', 'AccountsController@chartOfAccountIndex')->name('chart_of_accounts_index');
    Route::post('/store', 'AccountsController@chartOfAccountStore')->name('chart_of_accounts_store');
    Route::get('/edit/{id}', 'AccountsController@chartOfAccountEdit')->name('chart_of_accounts_edit');
    Route::post('/update/{id}', 'AccountsController@chartOfAccountUpdate')->name('chart_of_accounts_update');
});
