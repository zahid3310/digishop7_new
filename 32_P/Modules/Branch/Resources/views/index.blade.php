@extends('layouts.app')

@section('title', 'Branch')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.branch')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.branch')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.manage_branch')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('branch_store') }}" method="post" files="true" enctype="multipart/form-data">
            					{{ csrf_field() }}

                                <div style="margin-bottom: 0px !important" class="form-group row">
                                    <div class="col-md-6 form-group">
                                        <label for="productname">{{ __('messages.branch_name')}} *</label>
                                        <input type="text" name="branch_name" class="inner form-control" id="branch_name" placeholder="{{ __('messages.branch_name')}}" required />
                                    </div>

                                    <div class="col-md-6 form-group">
                                        <label for="productname">{{ __('messages.description')}}</label>
                                        <input type="text" name="description" class="inner form-control" id="description" placeholder="{{ __('messages.description')}}" />
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                    	<button type="submit" class="btn btn-primary waves-effect waves-light">{{ __('messages.save')}}</button>
                                    	<button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('branch_index') }}">{{ __('messages.close')}}</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<h4 class="card-title">{{ __('messages.all_branch')}}</h4>

                                <br>

                                <table id="datatable" class="table table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.sl')}}</th>
                                            <th>{{ __('messages.branch_name')}}</th>
                                            <th>{{ __('messages.description')}}</th>
                                            <th>{{ __('messages.action')}}</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                    	@if(!empty($branches) && ($branches->count() > 0))
                                    	@foreach($branches as $key => $branch)
	                                        <tr>
	                                            <td>{{ $key + 1 }}</td>
                                                <td>{{ $branch['name'] }}</td>
                                                <td>{{ $branch['description'] }}</td>
	                                            <td>
	                                            	<div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('branch_edit', $branch['id']) }}">{{ __('messages.edit')}}</a>
                                                        </div>
                                                    </div>
	                                            </td>
	                                        </tr>
	                                    @endforeach
	                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection