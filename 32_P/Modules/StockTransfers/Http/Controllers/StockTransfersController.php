<?php

namespace Modules\StockTransfers\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

use App\Models\StockTransfers;
use App\Models\Branches;
use App\Models\ProductEntries;
use App\Models\BranchInventories;
use Response;
use DB;

class StockTransfersController extends Controller
{
    public function index()
    {
        $branches           = Branches::orderBy('created_at', 'DESC')->get();
        $transfers          = StockTransfers::orderBy('created_at', 'DESC')->get();

        return view('stocktransfers::index', compact('transfers', 'branches'));
    }

    public function create()
    {
        return view('stocktransfers::create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'transfer_date' => 'required',
            'transfer_from' => 'required',
            'product_id'    => 'required',
            'transfer_to'   => 'required',
            'quantity'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $transfer                   = new StockTransfers;
            $transfer->date             = date('Y-m-d', strtotime($data['transfer_date']));
            $transfer->product_entry_id = $data['product_id'];
            $transfer->transfer_from    = $data['transfer_from'];
            $transfer->transfer_to      = $data['transfer_to'];
            $transfer->quantity         = $data['quantity'];
            $transfer->note             = $data['note'];
            $transfer->created_by       = $user_id;

            if ($transfer->save())
            {   
                $find_from_branch_inventory = BranchInventories::where('product_entry_id', $data['product_id'])
                                                                ->where('branch_id', $data['transfer_from'])
                                                                ->first();

                $find_to_branch_inventory   = BranchInventories::where('product_entry_id', $data['product_id'])
                                                                ->where('branch_id', $data['transfer_to'])
                                                                ->first();

                if ($data['transfer_from'] == 1)
                {
                    $from_branch_item                    = ProductEntries::find($data['product_id']);
                    $from_branch_item->stock_in_hand     = $from_branch_item['stock_in_hand'] - $data['quantity'];
                    $from_branch_item->save();
                }
                else
                {
                    if ($find_from_branch_inventory == null)
                    {
                        DB::rollback();
                        return back()->with('unsuccess', 'Do not have available stock to transfer');
                    }
                    else
                    {
                        $find_from_branch_inventory->stock_in_hand     = $find_from_branch_inventory['quantity'] - $data['quantity'];
                        $find_from_branch_inventory->save();
                    }
                }

                if ($data['transfer_to'] == 1)
                {
                    $to_branch_item                    = ProductEntries::find($data['product_id']);
                    $to_branch_item->stock_in_hand     = $from_branch_item['stock_in_hand'] + $data['quantity'];
                    $to_branch_item->save();
                }
                else
                {
                    if ($find_to_branch_inventory == null)
                    {
                        $new_to_branch_item                    = new BranchInventories;
                        $new_to_branch_item->product_entry_id  = $data['product_id'];
                        $new_to_branch_item->branch_id         = $data['transfer_to'];
                        $new_to_branch_item->stock_in_hand     = $data['quantity'];
                        $new_to_branch_item->created_by        = $user_id;
                        $new_to_branch_item->save();
                    }
                    else
                    {
                        $find_to_branch_inventory->stock_in_hand     = $find_to_branch_inventory['stock_in_hand'] + $data['quantity'];
                        $find_to_branch_inventory->save();
                    }
                }

                DB::commit();
                return redirect()->route('stock_transfer_index')->with("success","Transfer Created Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        return view('stocktransfers::show');
    }

    public function edit($id)
    {
        $branches           = Branches::orderBy('created_at', 'DESC')->get();
        $transfers          = StockTransfers::orderBy('created_at', 'DESC')->get();
        $find_transfer      = StockTransfers::find($id);

        return view('stocktransfers::edit', compact('branches', 'transfers', 'find_transfer'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'transfer_date' => 'required',
            'transfer_from' => 'required',
            'product_id'    => 'required',
            'transfer_to'   => 'required',
            'quantity'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $old_transfer               = StockTransfers::find($id);
            $transfer                   = StockTransfers::find($id);
            $transfer->date             = date('Y-m-d', strtotime($data['transfer_date']));
            $transfer->product_entry_id = $data['product_id'];
            $transfer->transfer_from    = $data['transfer_from'];
            $transfer->transfer_to      = $data['transfer_to'];
            $transfer->quantity         = $data['quantity'];
            $transfer->note             = $data['note'];
            $transfer->updated_by       = $user_id;

            if ($transfer->save())
            {   
                $find_from_branch_inventory = BranchInventories::where('product_entry_id', $data['product_id'])
                                                                ->where('branch_id', $data['transfer_from'])
                                                                ->first();

                $find_to_branch_inventory   = BranchInventories::where('product_entry_id', $data['product_id'])
                                                                ->where('branch_id', $data['transfer_to'])
                                                                ->first();

                if ($data['transfer_from'] == 1)
                {
                    $from_branch_item                    = ProductEntries::find($data['product_id']);
                    $from_branch_item->stock_in_hand     = $from_branch_item['stock_in_hand'] + $old_transfer['stock_in_hand'] - $data['quantity'];
                    $from_branch_item->save();
                }
                else
                {
                    if ($find_from_branch_inventory == null)
                    {
                        DB::rollback();
                        return back()->with('unsuccess', 'Do not have available stock to transfer');
                    }
                    else
                    {
                        $find_from_branch_inventory->stock_in_hand     = $find_from_branch_inventory['quantity'] + $old_transfer['stock_in_hand'] - $data['quantity'];
                        $find_from_branch_inventory->save();
                    }
                }

                if ($data['transfer_to'] == 1)
                {
                    $to_branch_item                    = ProductEntries::find($data['product_id']);
                    $to_branch_item->stock_in_hand     = $from_branch_item['stock_in_hand'] - $old_transfer['stock_in_hand'] + $data['quantity'];
                    $to_branch_item->save();
                }
                else
                {
                    if ($find_to_branch_inventory == null)
                    {
                        $new_to_branch_item                    = new BranchInventories;
                        $new_to_branch_item->product_entry_id  = $data['product_id'];
                        $new_to_branch_item->branch_id         = $data['transfer_to'];
                        $new_to_branch_item->stock_in_hand     = $data['quantity'];
                        $new_to_branch_item->created_by        = $user_id;
                        $new_to_branch_item->save();
                    }
                    else
                    {
                        $find_to_branch_inventory->stock_in_hand     = $find_to_branch_inventory['stock_in_hand'] - $data['quantity'] + $data['quantity'];
                        $find_to_branch_inventory->save();
                    }
                }

                DB::commit();
                return redirect()->route('stock_transfer_index')->with("success","Transfer Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function destroy($id)
    {
    }
}
