@extends('layouts.app')

@section('title', 'Area Add')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.add_area')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.registers')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.add_area')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('area_zone_update',$zone->id) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div class="row">

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-4 form-group">
                                        <label for="example-text-input" class=" col-form-label">Area *</label>
                                        <select class="form-control select2" name="area_id" required="">
                                            <option value="" hidden="">--Select Area--</option>
                                            @foreach($area as $key => $item)
                                            <option value="{{$item->id}}" {{ $zone->area_id == $item->id ? 'selected' : '' }}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-4 form-group">
                                        <label for="example-text-input" class=" col-form-label">Zone Name *</label>
                                        <input name="name" type="text" class="form-control" placeholder="Zone Name" value="{{$zone->name}}" required>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-4 form-group">
                                        <label for="example-text-input" class=" col-form-label">Position Number *</label>
                                        <input name="position" type="text" class="form-control" placeholder="Position Number" value="{{$zone->position}}" required>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12" style="text-align: right;">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">{{ __('messages.save')}}</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('customers_index') }}">{{ __('messages.close')}}</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th width="5%">{{ __('messages.sl')}}</th>
                                            <th width="40%">{{ __('messages.area')}}</th>
                                            <th width="40%">Zone Name</th>
                                            <th width="5%">Position</th>
                                            <th width="10%">Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        @foreach($lists as $list)
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td>{{$list->area_name}}</td>
                                            <td>{{$list->name}}</td>
                                            <td>{{$list->position}}</td>
                                             <td>
                                               <!--  <a data-toggle="modal"  data-target="#exampleModalLong" data-id="{{$list->id}}" data-area-id="{{$list->area_id}}" data-area-zone="{{$list->name}}" class="btn btn-info areaZoneModel">Edit</a> -->

                                                <a href="{{route('area_zone_edit',$list->id)}}" class="btn btn-info areaZoneModel">Edit</a>

                                            </td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
    
@endsection

@section('scripts')
<!-- <script>
    $(".areaZoneModel").click(function(){
        var zone_id   = $(this).data('id');
        var area_id   = $(this).data('area-id');
        var zone_name = $(this).data('area-zone');

        $('#zone_id').val(zone_id);
        $('#area_id').val(area_id);
        $('#zone_name').val(zone_name);
    });
</script> -->
@endsection
