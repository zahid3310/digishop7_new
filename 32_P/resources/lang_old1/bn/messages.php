<?php

return [
	

    'menu' => 'তালিকা',
    'dashboard' => 'ড্যাশবোর্ড',
    /////////////////////////////////////////////////////////////
    

    'sales' => 'বিক্রয়',
    'daily_sales' => 'দৈনিক বিক্রয়',
    'list_of_sales' => 'বিক্রয় তালিকা',
    'sales_return' => 'বিক্রয় রিটার্ন',
    'invoice_wise_collection' => 'ইনভয়েস অনুযায়ী সংগ্রহ ',
    /////////////////////////////////////////////////////////////

    'list_of_receive/purchase' => 'গ্রহণ/ক্রয়ের তালিকা',
    'receive/purchase_return' => 'গ্রহণ/ক্রয় রিটার্ন',
    'purchase_return' =>  'ক্রয় রিটার্ন',

    // ////////////////////////////////////////

    'Send_Production'=> 'উত্পাদন প্রেরণ',
    'new_send' => 'নতুন প্রেরণ',
    'list_of_send' => 'প্রেরণের তালিকা',
    'rece_finished_goods' => 'সমাপ্ত পণ্য',
    'new_receive' =>'নতুন গ্রহণ',
    'list_of_receive' =>'গ্রহণের তালিকা',
    'new_transfer' =>'নতুন স্থানান্তর',
   

    'purchase' => 'ক্রয়',
    'purchase_items' => 'ক্রয়ের আইটেম',
    'list_of_purchase' => 'ক্রয়ের তালিকা',
    'purchase_return' => 'ক্রয় রিটার্ন',
    'bill_wise_payment' => 'বিল অনুযায়ী পরিশোধ',
    /////////////////////////////////////////////////////////////


    'accounts' => 'হিসাব',
    'expenses' => 'ব্যয়',
    'incomes' => 'আয়',
    'balance_transfer' => 'ব্যালান্স হস্তান্তর',
    'cash_book' => 'ক্যাশ বুক',
    'ledger_book' => 'লেজের বুক',
  	/////////////////////////////////////////////////////////////


    'messaging' => 'বার্তা পাঠানো',
    'send_meg' => 'বার্তা পাঠান',
    /////////////////////////////////////////////////////////////


    'reports' => 'রিপোর্টস',
    'sales_statement' => 'বিক্রয় বিবৃতি',
    'sales_summary' => 'বিক্রয় সারসংক্ষেপ',
    'product_wise_sales' => 'পণ্য অনুযায়ী বিক্রয়',


    'purchase_statement' => 'ক্রয় বিবৃতি',
    'purchase_summary' => 'ক্রয় সারসংক্ষেপ',


    'list_of_expense' => 'ব্যয়ের তালিকা',
    'list_of_income' => 'আয়ের তালিকা',
    'income_statement' => 'আয়ের সারসংক্ষেপ',


    
    'payment' => 'পেমেন্ট',
    's_payment' => 'সরবরাহকারীর পেমেন্ট',
    'c_payment' => 'গ্রাহকের পেমেন্ট',
    'make_payment'=>'অর্থ প্রদান',
    'make_new_payment'=>'নতুন অর্থ প্রদান',

    'mis' => 'এম.আই.এস',
    'current_balance' => 'বর্তমান হিসাব',
    'stock_status' => 'স্টক অবস্থা',
    'buyer_Ledger' => 'ক্রেতা লেজার',
    'supplier_Ledger' => 'সরবরাহকারী লেজার',
    'due_ledger' => 'বাকি লেজার',
    'SR/Customer_Ledger' => 'এসআর / গ্রাহক লেজার',

    'basic_report' => 'জরুরী ক্রয়',
    'list_items' => 'উপাদানের তালিকা',
    'emergency_purchase' => 'জরুরী ক্রয়',
    'item_wise_supplier' => 'আইটেম অনুযায়ী সরবরাহকারী',
    'item_wise_customer' => 'আইটেম অনুযায়ী গ্রাহক',
    'buyer_list' => 'ক্রেতা তালিকা',
    'supplier_list' => 'সরবরাহকারী তালিকা',
    'item_wise_SR/Customer'=> 'আইটেম অনুযায়ী এসআর / গ্রাহক',
    'SR/Customer_list' => 'এসআর / গ্রাহক তালিকা',
    'daily_report' => 'দৈনিক হিসাব',
    /////////////////////////////////////////////////////////////

    'variations' => 'বিভিন্নতা',
    'brand_name' => 'ব্র্যান্ড নাম',
    //////////////////////////////////////////////

     'ac_info' => 'এ সি তথ্য',
     'update' => 'আপডেট',
     'update_print' => 'আপডেট & প্রিন্ট',

    ///////////////////////////////////////////////

    'basic_settings' => 'বেসিক সেটিংস',
    'product' => 'পণ্য',
    'create_product'=> 'পণ্য তৈরি করুন',
    'major_category' => 'মেজর ক্যাটাগরি',
    'product_code'=> 'প্রোডাক্ট কোড',
    'product_name'=> 'প্রোডাক্ট নাম',
    'retail_price'=> 'রিটেল প্রাইস',
    'wholesale_price'=> 'পাইকারি প্রাইস',
    'product_type'=> 'পণ্যের ধরন',
    'stock_quantity'=> 'স্টক পরিমাণ',
    'alert_quantity'=> 'সতর্কতা পরিমাণ',
    'single'=> 'সিঙ্গেল',
    'active' => 'একটিভ',
    'inactive'=> 'ইন্যাক্টিভ',
    'select_major_category'=> 'সিলেক্ট মেজর ক্যাটাগরি',
    'select_category' => 'সিলেক্ট ক্যাটাগরি',
    'select_sub_category' => 'সিলেক্ট সাব  ক্যাটাগরি',
    'select_unit'=> 'সিলেক্ট ইউনিট',
    'list_product' => 'পণ্য তালিকা',
    'add_product' => 'পণ্য যুক্ত করুন',
    'add_customer' => 'কাস্টমার যুক্ত করুন',
    'product_category' => 'প্রোডাক্ট ক্যাটাগরি',
    'product_category_name' => 'প্রোডাক্ট ক্যাটাগরি নাম',
    'other' => 'অন্যান্য',
    'all_major_category'=> 'সকল মেজর ক্যাটাগরি',
    'all_unit'=> 'সকল ইউনিট',
    'variation' => 'ভেরিয়েশন',
    'variation_list' => 'ভেরিয়েশন তালিকা',
    'add_variation_value' => 'ভেরিয়েশন মান যুক্ত করুন',
    'value' => 'মান',
    'all_store'=>'সমস্ত স্টোর',
    'select_product' => 'পণ্য নির্বাচন করুন',
    'quantity' => 'পরিমাণ',
    'a4_size_printer' => 'এ 4 সাইজ প্রিন্টার',
    'label_printer' => 'লেবেল প্রিন্টার',
    'barcode_label_size' => 'বারকোড লেবেল সাইজ',
    'print_type'=>'প্রিন্ট টাইপ',
    'organization_name'=>'প্রতিষ্ঠানের নাম',
    '40labels_per_sheet'=>'৪০ শীট প্রতি লেবেল',
    '36labels_per_sheet'=>'৩৬ শীট প্রতি লেবেল',
    '22labels_per_sheet'=>'২২ শীট প্রতি লেবেল',
    'page_margin' => 'পেজ মার্জিন',
    'information_show_labels' => 'লেবেলে প্রদর্শিত তথ্য',
    'product_price'=>'পণ্যের দাম',
    'print_preview'=>'প্রিন্ট পূর্বরূপ',
    'opening_stock'=>'ওপেনিং স্টক',

    'add_categories' => 'ক্যাটাগরি যুক্ত করুন',
    'brand' => 'ব্র্যান্ড/উত্পাদনকারী',
    'add_unit_measure' => 'ইউনিট পরিমাপ যোগ করুন',
    'product_variations' => 'পণ্যের বিভিন্নতা',
    'print_barcode' => 'প্রিন্ট বারকোড',
    'bulk_opening_stock' => 'বাল্ক ওপেনিং স্টক',
    'bulk_product_update' => 'বাল্ক পণ্য আপডেট',
    'add_major_categories' => 'মেজোর ক্যাটাগরি যুক্ত করুন',
    'add_sr/customer' => 'এস আর /কাস্টমার যুক্ত করুন ',


    'registers' => 'নিবন্ধন',
    'add_BuyerCustomer' => 'ক্রেতা/গ্রাহক যুক্ত করুন',
    'add_supplier' => 'সরবরাহকারী যোগ করুন',
    'add_employee' => 'কর্মচারী যুক্ত করুন',
    'add_reference' => 'রেফারেন্স যুক্ত করুন',
    'add_area'     => 'এলাকা যুক্ত করুন',
    'paid_through' => 'প্রদত্ত মাধ্যমে',

    'create_message' => 'পাঠ্য বার্তা তৈরি করুন',
    'pb' => 'ফোন-বুক ',



    'security_system' => 'নিরাপত্তা ব্যবস্থা',
    'add_user' => 'ব্যবহারকারী যুক্ত করুন',
    'list_user' => 'ব্যবহারকারীর তালিকা',
    'permission' => 'অনুমতি সেট করুন',
    'add_branch' => 'ব্রাঞ্চ যুক্ত করুন',
    'branch' => 'ব্রাঞ্চ',
    'branch_name' => 'ব্রাঞ্চ নাম',
    'all_branch' => 'সকল ব্রাঞ্চ',
    /////////////////////////////////////////////////////////////


    /**************************Menu End******************************/

    // Navber
    'stock_out' => "মজুত শেষ",

    // Footer
    'ctl' => 'সাইবারডাইন টেকনোলজি লিমিটেড',
    'contact' => 'যোগাযোগ',
    'contact_no' => '০১৭১৫৩১৭১৩৩',



    // Home Page
    'today_sale' => "আজকের বিক্রয়",
    'today_sale_return' => "আজকের বিক্রয় রিটার্ন",
    'today_expense' => "আজকের ব্যয়",
    'sms_balance' => "এসএমএস ব্যালেন্স",
    'c_dues' => "গ্রাহকের পাওনা",
    's_dues' => "সরবরাহকারী বকেয়া",
    't_sales' => "মোট বিক্রয়",
    't_purchase' => "মোট ক্রয়",
    't_customer' => "মোট গ্রাহক",
    't_suppliers' => "মোট সরবরাহকারী",
    't_invoices' => "মোট চালান",
    't_bills' => "মোট বিল",
    'top_selling_products' => "শীর্ষ বিক্রয় পণ্য",
    'stock_out_products' => "স্টক আউট পণ্য",
    'due_invoices' => "বাকি চালান",
    'year' => "বছর",
   	'month' => "মাস",
   	'week' => "সপ্তাহ",
    'sl' => "ক্রমিক",
    'code' => "কোড",
    'name' => "নাম",
    'date' => "তারিখ",
    'invoice' => "চালান",
    'amount' => "পরিমাণ",




    // Daily sales Page
   'customer' => 'গ্রাহক',
   'walk_in_customer' => 'গ্রাহক পদচারণা',
   'reference' => 'রেফারেন্স',
   'select_reference' => 'রেফারেন্স নির্বাচন করুন',
   'balance' => 'ব্যালান্স',
   'phone' => 'ফোন',
   'note' => 'বিঃদ্রঃ',
   'barcode' => 'বারকোড',
   'customer_name' => 'ক্রেতার নাম',
   'scan_product_code' => 'পণ্য কোড স্ক্যান',
   'no_product_found' => 'কোন পণ্য পাওয়া যায় নি',
   'sub_total' => 'উপ মোট',
   'vat' => 'ভ্যাট',
   'discount' => 'ছাড়',
   'dis_note' => 'ছাড়ের মন্তব্য',
   'coupon' => 'কুপন',
   'total_payable' => 'মোট প্রদেয়',
   'cash_given' => 'নগদ প্রদান',
   'change' => 'পরিবর্তন',
   'change_amount' => 'এমাউন্ট পরিবর্তন',
   'send_sms' => 'বার্তা পাঠান',
   'masking' => 'মাস্কিং',
   'non_masking' => 'অ-মাস্কিং',
   'voice' => 'ভয়েস',
   'save' => 'সংরক্ষণ',
   'save_print' => 'সংরক্ষণ এবং মুদ্রণ',
   'close' => 'বন্ধ',
   'print_invoice' => 'চালান মুদ্রণ',
   'action' => 'প্রক্রিয়া',
   'account_information' => 'হিসাবের তথ্য',
   'ac_info' => 'হিসাবের তথ্য',
   'amount_paid' => 'অর্থ প্রদান',
   'add_new_customer' => 'নতুন গ্রাহক যুক্ত করুন',
   'add_new_reference' => 'নতুন রেফারেন্স যুক্ত করুন',
   'add_new_customer' => 'নতুন গ্রাহক যুক্ত করুন',
   'mobile_no' => 'মোবাইল নম্বর',
   'alt_phone' => 'বিকল্প ফোন নম্বর',
   'nid_number' => 'এনআইডি নম্বর',
   'opening_balance' => 'ওপেনিং ব্যালান্স',
   'address' => 'ঠিকানা',
   'unit' => 'ইউনিট',
   'qty' => 'পরিমাণ',
   'rate' => 'দাম',
   'total' => 'মোট',
   'sr/customer' => 'এস আর/কাস্টমার',
   'sr/customer_payment' => 'এস আর/কাস্টমার পেমেন্টস',
   'select_sr/customer' => 'এসআর/গ্রাহক নির্বাচন করুন',


   // All Sales
   'all_sales' => 'সমস্ত বিক্রয়',
   'paid' => 'প্রদত্ত',
   'paid_to' => 'প্রদান করা হয়েছে',
   'due' => 'বাকি',
   'from_date' => 'তারিখ হইতে',
   'to_date' => 'তারিখ পর্যন্ত',

   // SALES RETURN
   'return_date' => 'রিটার্ন তারিখ',
   'select_customer' => 'গ্রাহক নির্বাচন করুন',
   'order_number' => 'অর্ডার নম্বর',
   'return_amount' => 'ফেরতের পরিমাণ',
   'all_sales_return' => 'সমস্ত বিক্রয় ফেরত',
   'search' => 'অনুসন্ধান',
   'return' => 'ফেরত',

   // PAYMENTS
   'payment_type' => 'অর্থপ্রদানের ধরণ',
   'payment_date' => 'অর্থপ্রদানের তারিখ',
   'payment_number' => 'পেমেন্ট নম্বর',
   'payment_details' => 'পেমেন্ট বিবরণ',
   'search_contact' => 'পরিচিতি অনুসন্ধান',
   'total_receivables' => 'মোট গ্রহণযোগ্য',
   'total_received' => 'মোট প্রাপ্ত',
   'order' => 'অর্ডার',
   'payment_list' => 'মুল্য পরিশোধ তালিকা',
   'type' => 'প্রকার ',
   'total_paid' => 'মোট পরিশোধিত',
   'payable' => 'পরিশোধনীয়',
   'total_due' => 'মোট বাকি',

    // //////////////////////

   'transfer_to_production' => 'উত্পাদনে স্থানান্তর করুন',
   'list_of_transfer' => 'স্থানান্তর তালিকা',
   'items' => 'আইটেম',
   'edit_transfer' => 'স্থানান্তর সম্পাদনা করুন',

   /////////////////////////////////////////////////

   'receive_finished_goods' => 'সমাপ্ত পণ্য গ্রহণ',
   'production_number' => 'উত্পাদন সংখ্যা',
   'list_receive_finished_goods' => 'সমাপ্ত পণ্য গ্রহণ তালিকা',

   // Purchase items
   'walk_in_supplier' => 'সরবরাহকারী পদচারণা',
   'supplier'=> 'সরবরাহকারী',
   'add_new_supplier' => 'নতুন সরবরাহকারী যুক্ত করুন',
   'category' => 'ক্যাটাগরি',
   'stock' => 'স্টক',

    //Purchases list
   'all_purchases' => 'সমস্ত ক্রয়',

   //Purchases Return
   'select_supplier' => 'সরবরাহকারী নির্বাচন করুন',
   'all_purchases_return' =>  'সমস্ত ক্রয়ের রিটার্ন',
   'purchase_number' => 'ক্রয়  নম্বর',
   'return_number' => 'রিটার্ন নম্বর',
   'bill_number' => 'বিল নম্বর',
   'coupon/membership' => 'কুপন/মেম্বারশিপ',

   // Account
   'add' => 'যুক্ত করা',
   'expense_date' => 'ব্যয়ের তারিখ',
   'expense_category' => 'ব্যয়ের ক্যাটাগরি',
   'expense_list' => 'ব্যয়ের তালিকা',
   'expense_number' => 'ব্যয়ের নম্বর',

   'income_date' => 'আয়ের তারিখ',
   'income_category' => 'আয়ের ক্যাটাগরি',
   'income_list' => 'আয়ের তালিকা',
   'income_number' => 'আয়ের নম্বর',

   'create_income_category' => 'নতুন আয়ের বিভাগ তৈরি করুন',
   'create_expense_category' => 'নতুন ব্যয়ের বিভাগ তৈরি করুন',

   'category_name' => 'ক্যাটাগরি নাম',

   'transfer_balance'=> 'ব্যালেন্স স্থানান্তর',
   'all_transfer'=> 'সমস্ত স্থানান্তর',
   'transfer_date' => 'স্থানান্তর তারিখ',
   'transfer_from' => 'স্থানান্তর হইতে',
   'transfer_to' => 'স্থানান্তর করা',
   'delete' => 'মুছে ফেলুন ',
   'edit' => 'সম্পাদন করুন',
   'transfer_note'=> 'ট্রান্সফার বিঃদ্রঃ',
   'select_account' => 'নির্বাচন অ্যাকাউন্ট',

   // Message 
    'sms_type' => 'এসএমএস টাইপ',
    'select_msg' => 'বার্তা নির্বাচন করুন',
    'contact_category' => 'যোগাযোগ ক্যাটাগরি',
    'select_contact_category' => 'যোগাযোগ ক্যাটাগরি নির্বাচন করুন',
    'select_contact' => 'যোগাযোগ নির্বাচন করুন',
    'employee' => 'কর্মচারী',
    'message' => 'বার্তা',
    'msg_body' => 'বার্তাংশ',
    'send' => 'পাঠান',
    'contact_type' => 'যোগাযোগ ধরন',
    'image' => 'ইমেজ',
    'all_contact' => 'সকল যোগাযোগ',
    'meg_title'=>'বার্তা শিরোনাম',
    'title'=>'শিরোনাম',
    'all_meg'=> 'সকল বার্তা',


    // STATEMENT OF SALES
    'to'=>'পর্যন্ত',
    'customer_company' => 'গ্রাহক সংস্থা',
    'sales_id' => 'বিক্রয় আইডি',
    'sales_by' => 'বিক্রয় দ্বারা',
    'item_name' => 'আইটেম নাম',
    'select_option'=> 'বিকল্প নির্বাচন',
    'print' => 'প্রিন্ট',
    'all' => 'সকল',
    'sales/return' => "বিক্রয়/রিটার্ন",
    

       //Sales Summery
     'invoice_amount' =>'চালান পরিমাণ',
     'invoice_number' =>'চালান নম্বর',
     'receivable' =>'গ্রহণযোগ্য',
     'received' =>'গৃহীত',
     'all_customer'=> 'সমস্ত গ্রাহক',

     // Product Wise Sales Report
     'product_wise_sales_report' => 'পণ্য অনুসারে বিক্রয় প্রতিবেদন',

      // Statement of sale
     'supplier_company' => 'সরবরাহকারী সংস্থা',
     'purchase_id' => 'সরবরাহকারী আইডি',
     'purchase_by' => 'সরবরাহকারী কর্তৃক',
     'receive/purchase_summary' => 'সংক্ষিপ্ত গ্রহণ / ক্রয়',

     // PURCHASE SUMMARY
     'all_supplier'=> 'সমস্ত সরবরাহকারী',
      'bill_amount'=> 'বিলের পরিমাণ',
      'receive_or_purchase_report' => 'গ্রহণ / ক্রয়',

      // Expense Report
     'expenses_report' => 'ব্যয় রিপোর্ট',
     'all_category' => 'সমস্ত বিভাগ',

     // Income Report
     'income_report' => 'আয়ের রিপোর্ট',

     // All Product
     'purchase_price' => 'ক্রয় মূল্য',
     'sell_price' => 'বিক্রয় মূল্য',
     'status' => 'অবস্থা',

     // Production
     'production' => 'উৎপাদন',
     'stock_transfer' => 'স্টক ট্রান্সফার',
     'sales_to_sr/customer' => 'ক্রেতার নিকট বিক্রয় ',

   // Security System
   'user_access' => 'ব্যবহারকারী অ্যাক্সেস',
   'setting' => 'সেটিংস',
   'user' => 'ব্যবহারকারী',
   'username'=> 'ব্যবহারকারীর নাম',
   'select_user' => 'ব্যবহারকারী নির্বাচন করুন',
   'password'=> 'পাসওয়ার্ড',
   'role'=>'রোল',
   'user_list'=> 'ব্যবহারকারীর তালিকা',
   'all_user' => 'সকল  ব্যবহারকারী',
   'edit_user' => 'ব্যবহারকারী সম্পাদনা করুন',
   'manage_branch' => 'শাখা পরিচালনা করুন',


     // Reports

     'list_of_item_print' => 'আইটেম প্রিন্টের তালিকা',
     'stock_in_hand' => 'মজুদ মাল',
     'emergency_Item_list'=> 'জরুরী আইটেমের তালিকা',
     'u/m' => ' ইউ/এম',
     'report_type'=> 'রিপোর্ট টাইপ',
     'supplier_wise'=> 'সরবরাহকারী অনুসারে',
     'product_wise'=>'পণ্য অনুসারে',
     'customer_wise'=>'কাস্টমার অনুসারে',
     'wholesale_rate'=> 'পাইকারি দর',
     'retail_rate'=> 'রিটেল দর',
     'customer_rate' => 'কাস্টমার দর',
     'payment_account'=> 'পেমেন্ট একাউন্ট',
     'account_name' => 'হিসাবের নাম',
     'in' => 'ইন',
     'out'=> 'আউট ',
     'product_report' => 'পণ্য রিপোর্ট',
     'stocK_report' => 'স্টক রিপোর্ট',
     'sold' => 'বিক্রি',
     'stock_value'=>'স্টক মূল্য',
     'sell_value'=> 'বিক্রয় মূল্য',
     'show_all' => 'সমস্ত প্রদর্শন করুন',
     'due_available'=> 'বকেয়া প্রাপ্য',
     'details' => 'বিস্তারিত',
     'select_date_range' => 'তারিখের সীমা নির্বাচন করুন',
     'customer_ledger' => 'কাস্টমার লেজের',
     'list_customer_payment' => 'গ্রাহক প্রদানের তালিকা',
     'list_supplier_payment' => 'সরবরাহকারী প্রদানের তালিকা',
     'item_code'=> 'আইটেম কোড',
     'profit'=> 'লাভ',
     'loss'=> 'ক্ষতি',
     'supplier_name' => 'সরবরাহকারী নাম',
     'unit_price'=> 'ইউনিট প্রাইস',
     'expense_statement' => 'ব্যয় বিবৃতি',
     'expense_note' => 'ব্যয় বিঃদ্রঃ',
     'income_note'=>'আয় বিঃদ্রঃ',
     'T/date' => 'টি / তারিখ',
     'T/price' => 'টি/প্রাইস',
     'T/payable' => 'টি/পরিশোধযোগ্য',
     'T/discount' => 'টি/ডিসকাউন্ট',
     'N/payable' => 'এন/পরিশোধযোগ্য ',
     'number' => 'নম্বর',
     'head' => 'হেড',
     'particulars'=>'পার্টিকুলারস',
     'entry_by'=> 'দ্বারা প্রবেশ',
     'general_ledger' => 'জেনারেল লেজার',
     'account_head'=>'একাউন্ট হেড',
     'item_category'=>'আইটেম ক্যাটাগরি',
     'purchase_type'=>'ক্রয়ের ধরণ',
     'select_branch' => 'শাখা নির্বাচন করুন',
     'sales_type'=> 'বিক্রয় প্রকার',
     'add_new_product_category' => 'নতুন পণ্য বিভাগ যুক্ত করুন',
     'add_new_major_category' => 'নতুন প্রধান বিভাগ যুক্ত করুন',
     'already_added' => 'ইতিমধ্যে যোগ করা হয়েছে',
     'set_unit_conversion' => 'ইউনিট রূপান্তর সেট করুন',
     'paid_through_account'=>'অ্যাকাউন্টের মাধ্যমে অর্থ প্রদান করুন',
     'description'=> 'বর্ণনা',
     'all_accounts'=> 'সমস্ত অ্যাকাউন্ট',
     'edit_accounts'=> 'অ্যাকাউন্টগুলি সম্পাদনা করুন',
     'add_contact' => 'পরিচিতি যোগ করুন',
     'all_phone_contact'=> 'সমস্ত ফোন যোগাযোগ',
     'description_deposit'=> 'বিবরণ/জমা',
     'description_withdraw'=> 'বিবরণ/খরচ',
     'taka'=> 'টাকা',
     'previous_dues'=> 'পূর্বের জের',
     'show' => 'প্রদর্শন',
     'payable_to' => 'প্রদেয়',
     'purchase_date' => 'ক্রয় তারিখ',
     'bill' => 'বিল',
     'no' => 'নং',
     'price' => 'মূল্য',
     'store_name' => 'দোকানের নাম',
     'enter_variation_name' => 'ভেরিয়েশন মান লিখুন',
     'previous_image' => 'পূর্ববর্তী ইমেজ',
      'joining_date' => 'যোগদান তারিখ',
     'designation' => 'উপাধি',
     'associative_user_id' => 'সহযোগী ব্যবহারকারী আইডি',
     'salary'=> 'বেতন',
     'print' => 'প্রিন্ট',
];