<?php

namespace Modules\Damages\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Damages;
use App\Models\DamageEntries;
use Carbon\Carbon;
use Response;
use DB;
use View;

class DamagesController extends Controller
{
    public function index()
    {   
        $damages = Damages::leftjoin('damage_entries', 'damage_entries.damage_id', 'damages.id')
                            ->groupBy('damage_entries.damage_id')
                            ->selectRaw('GROUP_CONCAT(DISTINCT damages.id) as id,
                                         GROUP_CONCAT(DISTINCT damages.damage_date) as damage_date,
                                         SUM(damage_entries.quantity) as total_quantity,
                                         count(damage_entries.id) as total_item,
                                         SUM(damage_entries.quantity*damage_entries.buy_price) as damage_value
                                        ')
                            // ->orderBy('damages.created_at', 'DESC')
                            ->get();

        return view('damages::index', compact('damages'));
    }

    public function create()
    {
        return view('damages::create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'damage_date'           => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();
      // dd($data);
        DB::beginTransaction();

        try{
            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }

            $damage                  = new Damages;;
            $damage->damage_date     = date('Y-m-d', strtotime($data['damage_date']));
            $damage->total_buy_price = $buy_price;
            $damage->damage_note     = $data['note'];
            $damage->created_by      = $user_id;

            if ($damage->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $damage_entries[] = [
                        'damage_id'         => $damage['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'pcs'                => $data['pcs'][$key] != null ? $data['pcs'][$key] : 0,
                        'cartoon'            => $data['cartoon'][$key] != null ? $data['cartoon'][$key] : 0,
                        'buy_price'          => $product_buy_price['buy_price'] != null ? $product_buy_price['buy_price'] : 0,
                        'quantity'           => $data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('damage_entries')->insert($damage_entries);

                //Here Item Id is used for update purpose
                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['quantity'][$key4];
                    $product_entries->save();
                }

                DB::commit();

                return back()->with("success","Damage Created Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        return view('damages::show');
    }

    public function edit($id)
    {
        $find_damage            = Damages::select('damages.*')->find($id);

        $find_damage_entries    = DamageEntries::leftjoin('product_entries', 'product_entries.id', 'damage_entries.product_entry_id')
                                            ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                            ->where('damage_entries.damage_id', $id)
                                            ->select('damage_entries.*',
                                                    'product_entries.id as item_id',
                                                    'product_entries.product_code as product_code',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                                    'sizes.height as height',
                                                    'sizes.width as width',
                                                    'product_entries.name as item_name')
                                            ->get();

        $entries_count          = $find_damage_entries->count();

        return view('damages::edit', compact('find_damage', 'find_damage_entries', 'entries_count'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'damage_date'           => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }

            $damage                  = Damages::find($id);
            $damage->damage_date     = date('Y-m-d', strtotime($data['damage_date']));
            $damage->total_buy_price = $buy_price;
            $damage->damage_note     = $data['note'];
            $damage->created_by      = $user_id;

            if ($damage->save())
            {   
                $item_id     = DamageEntries::where('damage_id', $damage['id'])->get();
                $item_delete = DamageEntries::where('damage_id', $damage['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $damage_entries[] = [
                        'damage_id'         => $damage['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'pcs'                => $data['pcs'][$key] != null ? $data['pcs'][$key] : 0,
                        'cartoon'            => $data['cartoon'][$key] != null ? $data['cartoon'][$key] : 0,
                        'buy_price'          => $product_buy_price['buy_price'] != null ? $product_buy_price['buy_price'] : 0,
                        'quantity'           => $data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('damage_entries')->insert($damage_entries);

                //Here Item Id is used for update purpose
                if ($item_id != null)
                {
                    foreach ($item_id as $key => $value)
                    {
                        $old_item_entry_id[]    = $value['product_entry_id'];
                        $old_items_stock[]      = $value['quantity'];
                    }

                    foreach ($old_item_entry_id as $key2 => $value2)
                    { 
                        $quantity_add_to_product_entry                   = ProductEntries::find($value2);
                        $quantity_add_to_product_entry->stock_in_hand    = $quantity_add_to_product_entry['stock_in_hand'] + $old_items_stock[$key2];
                        $quantity_add_to_product_entry->save();
                    }
                }

                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['quantity'][$key4];
                    $product_entries->save();
                }

                DB::commit();

                return redirect()->route('damages_index')->with("success","Damage Updated Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function destroy($id)
    {
        //
    }
}
