<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Sizes extends Model
{  
    protected $table = "sizes";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }
}
