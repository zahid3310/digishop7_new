<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class PurchaseOrder extends Model
{  
    protected $table = "purchase_orders";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\Customers','vendor_id');
    }

}
