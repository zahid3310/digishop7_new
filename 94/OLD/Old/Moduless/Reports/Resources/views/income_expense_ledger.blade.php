@extends('layouts.app')

@section('title', 'Income/Expense Ledger')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Income/Expense Ledger</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Income/Expense Ledger</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="margin-left: 150px;margin-right: 150px" class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <form method="get" action="{{ route('income_expense_ledger_print') }}" enctype="multipart/form-data" target="_blank">
	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"> From Date </label>
	                                    <div class="col-md-9">
	                                    	<div class="row">
	                                    		<div class="input-group-append col-md-5">
	                                    			<span style="border-radius: 0px" class="input-group-text"><i class="mdi mdi-calendar"></i></span>
		                                    		<input style="border-radius: 0px" type="text" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" value="{{ date('d-m-Y') }}" name="from_date">
		                                    	</div>

		                                    	<div class="input-group-append col-md-7">
		                                    		<label style="text-align: right" for="productname" class="col-md-2 col-form-label"> To </label>
		                                    		<span style="border-radius: 0px" class="input-group-text"><i class="mdi mdi-calendar"></i></span>
		                                    		<input style="border-radius: 0px" type="text" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" value="{{ date('d-m-Y') }}" name="to_date">
		                                    	</div>
	                                    	</div>
	                                    </div>
	                                </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Account/Ledger </label>
                                        <div class="col-md-9">
                                            <select style="width: 100%" id="account_id" name="account_id" class="form-control select2">
                                               <option value="0">All</option>
                                               @if($paid_accounts->count() > 0)
                                               @foreach($paid_accounts as $key => $value)
                                               <option value="{{ $value['id'] }}"> {{ $value['name'] }} </option>
                                               @endforeach
                                               @endif
                                            </select>
                                        </div>
                                    </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"></label>
	                                    <div class="col-md-9">
	                                        <button style="border-radius: 0px;background-color: #297049;color: white" class="btn" type="submit" target="_blank">
	                                        	Print
	                                    	</button>
	                                    </div>
	                                </div>
                            	</form>
                                
                            </div>

                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
        
</script>
@endsection