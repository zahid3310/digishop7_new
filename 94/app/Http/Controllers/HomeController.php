<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;

//Models
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\SalesReturn;
use App\Models\PurchaseReturn;
use App\Models\PurchaseReturnEntries;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\Expenses;
use App\Models\ExpenseEntries;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\Branches;
use App\Models\Users;
use Carbon\Carbon;
use Response;
use Artisan;
use Auth;
use DB;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth()->user()->status == 1)
        {
            $user                   = Users::find(1);
            $response_notification  = Http::get('https://digishop7.com/digishop-billing/get-notification/'.$user['billing_id']);
            // $response_sms           = Http::get('https://digishop7.com/digishop-billing/get-sms-balance/'.$user['billing_id']); 
            $response_sms           = 0; 
            $notification           = json_decode($response_notification);
            $sms_balance            = json_decode($response_sms);
            
            return view('home', compact('notification', 'sms_balance'));
        }
        else
        {
            return back();
        }
    }

    public function dashboardItems()
    {
        $branch_id                  = Auth::user()->branch_id;
        $total_customers            = Customers::where('contact_type', 0)->where('branch_id', $branch_id)->count();
        $total_suppliers            = Customers::where('contact_type', 1)->where('branch_id', $branch_id)->count();
        $todays_sells               = JournalEntries::where('date', date('Y-m-d'))->where('transaction_head', 'sales')->where('debit_credit', 0)->where('branch_id', $branch_id)->sum('amount');
        $todays_sells_return        = JournalEntries::where('date', date('Y-m-d'))->where('transaction_head', 'sales-return')->where('debit_credit', 0)->where('branch_id', $branch_id)->sum('amount');
        $todays_purchase            = JournalEntries::where('date', date('Y-m-d'))->where('transaction_head', 'purchase')->where('debit_credit', 0)->where('branch_id', $branch_id)->sum('amount');
        $todays_purchase_return     = JournalEntries::where('date', date('Y-m-d'))->where('transaction_head', 'purchase-return')->where('debit_credit', 0)->where('branch_id', $branch_id)->sum('amount');
        $todays_expense             = JournalEntries::where('date', date('Y-m-d'))->where('transaction_head', 'expense')->where('debit_credit', 0)->where('branch_id', $branch_id)->sum('amount');

        $sales_summary              = Invoices::whereYear('invoice_date', date('Y'))
                                                ->select(DB::raw('MONTH(invoice_date) month'),
                                                       DB::raw('SUM(invoice_amount) total'))
                                                ->groupBy(DB::raw('MONTH(invoice_date)'))
                                                ->get();

        $purchase_summary           = Bills::whereYear('bill_date', date('Y'))
                                                ->select(DB::raw('MONTH(bill_date) month'),
                                                       DB::raw('SUM(bill_amount) total'))
                                                ->groupBy(DB::raw('MONTH(bill_date)'))
                                                ->get();


        $data['sales_summary']              = $sales_summary;
        $data['purchase_summary']           = $purchase_summary;
        $data['total_customers']            = $total_customers;
        $data['total_suppliers']            = $total_suppliers;
        $data['todays_sells']               = $todays_sells;
        $data['todays_sells_return']        = $todays_sells_return;
        $data['todays_purchase']            = $todays_purchase;
        $data['todays_purchase_return']     = $todays_purchase_return;
        $data['todays_expenses']            = $todays_expense;
        $data['total_sells']                = JournalEntries::where('transaction_head', 'sales')->where('debit_credit', 0)->where('branch_id', $branch_id)->sum('amount');
        $data['total_purchase']             = JournalEntries::where('transaction_head', 'purchase')->where('debit_credit', 0)->where('branch_id', $branch_id)->sum('amount');;
        $data['customer_dues']              = Customers::where('contact_type', 0)->where('branch_id', $branch_id)->sum('balance');
        $data['supplier_dues']              = Customers::where('contact_type', 1)->where('branch_id', $branch_id)->sum('balance');;

        return Response::json($data);
    }

    public function stockOutItems()
    {
        $products   = ProductEntries::whereRaw('product_entries.stock_in_hand <= product_entries.alert_quantity')
                                ->count();

        return Response::json($products);
    }

    public function check()
    {
        return view('check');
    }
}
