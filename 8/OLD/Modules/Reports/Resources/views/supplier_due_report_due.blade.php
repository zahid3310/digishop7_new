@extends('layouts.app')

@section('title', 'Supplier Due List')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Supplier Due List</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Supplier Due List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['organization_name'] }}</h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['address'] }}</p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['contact_number'] }}</p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">Supplier Due List</h4>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><strong>From</strong> {{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }} <strong>To</strong> {{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="{{ route('due_report_supplier_index_due') }}" enctype="multipart/form-data">
                                    <div class="form-group row">
                                        <div style="margin-bottom: 10px" class="col-lg-3 col-md-3 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="{{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="{{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-3 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="supplier_id" style="width: 100" class="form-control select2" name="supplier_id">
                                                    <option value="{{ $customer_name != null ? $customer_name['id'] : 0 }}" selected>{{ $customer_name != null ? $customer_name['name'] : '--All Suppliers--' }}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="ledger_type" style="width: 100" class="form-control select2" name="ledger_type">
                                                    <option value="0" selected>--Show All--</option>
                                                    <option {{ $ledger_type == 1 ? 'selected' : '' }} value="1">Dues Available</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center">Serial</th>
                                            <th style="text-align: center">Supplier</th>
                                            <th style="text-align: center">Phone</th>
                                            <th style="text-align: center">Advance</th>
                                            <th style="text-align: center">Balance</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                            $i                  = 1;
                                        ?>
                                        @foreach($data as $key => $value)
                                        <tr>
                                            <td style="text-align: center;">{{ $i }}</td>
                                            <td style="text-align: left;">{{ $value['customer_name'] }} </td>
                                            <td style="text-align: center;">{{ $value['phone'] }}</td>
                                            <td style="text-align: right;">{{ number_format($value['supplier_advance'],2,'.',',') }}</td>
                                            <td style="text-align: right;">{{ number_format($value['due_amount'],2,'.',',') }}</td>
                                        </tr>
                                         
                                        <?php $i++; ?>

                                        <div id="myModal_{{$value['customer_id']}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title mt-0" id="myModalLabel">Select Date Range</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <form method="get" action="{{ route('due_report_supplier_details', $value['customer_id']) }}" enctype="multipart/form-data" target="_blank">

                                                        <div style="padding-top: 0px !important" class="modal-body">
                                                            <div style="margin-bottom: 0px !important" class="form-group row">
                                                                <label for="example-text-input" class="col-md-12 col-form-label">From Date *</label>
                                                                <div class="col-md-12">
                                                                    <input style="cursor: pointer" id="from_date" name="from_date" type="date" class="form-control" value="<?= date("2020-01-01") ?>" required>
                                                                </div>
                                                            </div>

                                                            <div style="margin-bottom: 0px !important" class="form-group row">
                                                                <label for="example-text-input" class="col-md-12 col-form-label">To Date *</label>
                                                                <div class="col-md-12">
                                                                    <input style="cursor: pointer" id="to_date" name="to_date" type="date" class="form-control" value="<?= date("Y-m-d") ?>" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Search</button>
                                                            <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                        <tr>
                                            <th colspan="4" style="text-align: right;">TOTAL</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_due_amount,2,'.',',') }}</th>
                                        </tr>

                                    </tbody>
                                    
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        
        var site_url  = $('.site_url').val();

        $("#supplier_id").select2({
            ajax: { 
            url:  site_url + '/invoices/customer/list/invoices',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['contact_type'] == 1 || result['id'] == 0)
                {
                    return result['text'];
                }
            },
        });
    });
</script>
@endsection