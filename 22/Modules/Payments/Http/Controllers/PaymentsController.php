<?php

namespace Modules\Payments\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\Bills;
use App\Models\InvoiceEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\PaidThroughAccounts;
use Response;
use DB;

class PaymentsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('payments::index');
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $tables         = TableNameByUsers();
        $table_id       = Auth::user()->associative_contact_id;

        $customers      = Customers::where('contact_type', 0)
                                    ->orWhere('contact_type', 1)
                                    ->orderBy('created_at', 'DESC')
                                    ->get();

        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $find_customer  = Customers::where('id', $customer_id)->first();
        $paid_accounts  = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();

        return view('payments::create', compact('customers', 'customer_id', 'find_customer', 'paid_accounts'));
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_id'   => 'required',
            'type'          => 'required',
            'payment_date'  => 'required',
            'amount'        => 'required',
            'paid.*'        => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        if ($data['amount'] == 0 || $data['payment_date'] == null)
        {
            return redirect()->back()->withInput();
        }

        DB::beginTransaction();

        try{
            $data_find                  = Payments::orderBy('id', 'DESC')->first();
            $payment_number             = $data_find != null ? $data_find['payment_number'] + 1 : 1;
            
            $payment                    = new Payments;
            $payment->payment_number    = $payment_number;
            $payment->customer_id       = $data['customer_id'];
            $payment->payment_date      = date('Y-m-d', strtotime($data['payment_date']));
            $payment->amount            = $data['amount'];
            $payment->paid_through      = $data['paid_through'];
            $payment->note              = $data['note'];

            if ($data['type'] == 0)
            {
                $payment->type              = 4;
            }
            elseif($data['type'] == 1)
            {
                $payment->type              = 5;
            }
            else
            {
                $payment->type              = $data['type'];
            }
            
            $payment->created_by        = $user_id;

            $find_customer              = $data['customer_id'];
            $find_type                  = $data['type'];

            if ($payment->save())
            {   
                if ($data['type'] == 0)
                {
                    foreach ($data['invoice_id'] as $key => $value)
                    {   
                        if ($data['paid'][$key] != 0)
                        {
                            $payment_entries[] = [
                                'invoice_id'        => $value,
                                'payment_id'        => $payment['id'],
                                'amount'            => $data['paid'][$key],
                                'created_by'        => $user_id,
                                'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            $transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                'invoice_id'            => $value,
                                'payment_id'            => $payment['id'],
                                'paid_through'          => $payment['paid_through'],
                                'type'                  => $data['type'],
                                'note'                  => $data['note'],
                                'amount'                => $data['paid'][$key],
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $update_dues                = Invoices::find($value);
                            $update_dues->due_amount    = $update_dues['due_amount'] - $data['paid'][$key];
                            $update_dues->save();
                        }
                    }
                }
                
                if ($data['type'] == 1)
                {
                    foreach ($data['bill_id'] as $key => $value)
                    {
                        if ($data['paid'][$key] != 0)
                        {
                            $payment_entries[] = [
                                'bill_id'           => $value,
                                'payment_id'        => $payment['id'],
                                'amount'            => $data['paid'][$key],
                                'created_by'        => $user_id,
                                'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            $transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                'bill_id'               => $value,
                                'payment_id'            => $payment['id'],
                                'paid_through'          => $payment['paid_through'],
                                'type'                  => $data['type'],
                                'note'                  => $data['note'],
                                'amount'                => $data['paid'][$key],
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $update_bill_dues                = Bills::find($value);
                            $update_bill_dues->due_amount    = $update_bill_dues['due_amount'] - $data['paid'][$key];
                            $update_bill_dues->save();
                        }
                    }
                }

                if ($data['type'] == 2)
                {
                    foreach ($data['sales_return_id'] as $key => $value)
                    {
                        if ($data['paid'][$key] != 0)
                        {
                            $payment_entries[] = [
                                'sales_return_id'   => $value,
                                'payment_id'        => $payment['id'],
                                'amount'            => $data['paid'][$key],
                                'created_by'        => $user_id,
                                'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            $transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                'sales_return_id'       => $value,
                                'payment_id'            => $payment['id'],
                                'paid_through'          => $payment['paid_through'],
                                'type'                  => $data['type'],
                                'note'                  => $data['note'],
                                'amount'                => $data['paid'][$key],
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $update_return_dues                     = SalesReturn::find($value);
                            $update_return_dues->due_amount         = $update_return_dues['due_amount'] - $data['paid'][$key];
                            $update_return_dues->save();

                            // $find_invoice_id                        = $tables['sales_return_entries']->where('sales_return_id', $value)->first(); 
                            // $update_invoice_dues                    = $tables['invoices']->find($find_invoice_id['invoice_id']);
                            // $update_invoice_dues->return_amount     = $update_invoice_dues['return_amount'] - $data['paid'][$key];
                            // $update_invoice_dues->save();
                        }
                    }
                }

                if ($data['type'] == 3)
                {
                    foreach ($data['purchase_return_id'] as $key => $value)
                    {
                        if ($data['paid'][$key] != 0)
                        {
                            $payment_entries[] = [
                                'purchase_return_id'    => $value,
                                'payment_id'            => $payment['id'],
                                'amount'                => $data['paid'][$key],
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                'purchase_return_id'    => $value,
                                'payment_id'            => $payment['id'],
                                'paid_through'          => $payment['paid_through'],
                                'type'                  => $data['type'],
                                'note'                  => $data['note'],
                                'amount'                => $data['paid'][$key],
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $update_return_dues                 = PurchaseReturn::find($value);
                            $update_return_dues->due_amount     = $update_return_dues['due_amount'] - $data['paid'][$key];
                            $update_return_dues->save();

                            // $find_bill_id                       = $tables['purchase_return_entries']->where('purchase_return_id', $value)->first();   
                            // $update_bill_dues                   = $tables['bills']->find($find_bill_id['bill_id']);
                            // $update_bill_dues->return_amount    = $update_bill_dues['return_amount'] - $data['paid'][$key];
                            // $update_bill_dues->save();
                        }
                    }
                }

                DB::table('payment_entries')->insert($payment_entries);
                DB::table('transactions')->insert($transactions);

                DB::commit();
                return back()->with("success","Payment Created Successfully !!")->with("find_customer", $find_customer)->with('find_type', $find_type);
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.")->with('find_customer', $find_customer)->with('find_type', $find_type);
            }
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added")->with('find_customer', $find_customer)->with('find_type', $find_type);
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $payment        = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                ->select('payments.*',
                                         'customers.name as customer_name',
                                         'customers.address as address',
                                         'customers.phone as phone')
                                ->find($id);

        if ($payment['type'] == 0)
        {
            $entries    = PaymentEntries::leftjoin('invoices', 'invoices.id', 'payment_entries.invoice_id')
                                ->where('payment_entries.payment_id', $id)
                                ->select('payment_entries.*', 'invoices.invoice_number as number')
                                ->get();
        }
        else
        {
            $entries    = PaymentEntries::leftjoin('bills', 'bills.id', 'payment_entries.bill_id')
                                ->where('payment_entries.payment_id', $id)
                                ->select('payment_entries.*', 'bills.bill_number as number')
                                ->get();
        }

        $user_info  = userDetails();

        return view('payments::show', compact('entries', 'payment', 'user_info'));
    }

    public function edit($id)
    {
        dd($entries);
        return view('payments::edit');
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $find_payment   = Payments::find($id);
        $entries        = PaymentEntries::where('payment_entries.payment_id', $id)
                                ->select('payment_entries.*')
                                ->get();

        DB::beginTransaction();

        //0=invoice,1=bill
        if ($find_payment['type'] == 0)
        {
            foreach ($entries as $key => $value)
            {
                $invoice                = Invoices::find($value['invoice_id']);
                $invoice->due_amount    = $invoice['due_amount'] + $value['amount'];
                $invoice->save();
            }
        }

        if ($find_payment['type'] == 1)
        {
            foreach ($entries as $key1 => $value1)
            {
                $bill                = Bills::find($value1['bill_id']);
                $bill->due_amount    = $bill['due_amount'] + $value1['amount'];
                $bill->save();
            }
        }

        if ($find_payment['type'] == 2)
        {
            foreach ($entries as $key2 => $value2)
            {
                $update_return_dues                 = SalesReturn::find($value2['sales_return_id']);
                $update_return_dues->due_amount     = $update_return_dues['due_amount'] + $value2['amount'];
                $update_return_dues->save();

                // $find_invoice_id                    = $tables['sales_return_entries']->where('sales_return_id', $value2['sales_return_id'])->first();
                // $update_invoice_dues                = $tables['invoices']->find($find_invoice_id['invoice_id']);
                // $update_invoice_dues->return_amount = $update_invoice_dues['return_amount'] + $value2['amount'];
                // $update_invoice_dues->save();
            }
        }

        if ($find_payment['type'] == 3)
        {
            foreach ($entries as $key3 => $value3)
            {
                $update_return_dues              = PurchaseReturn::find($value3['purchase_return_id']);
                $update_return_dues->due_amount  = $update_return_dues['due_amount'] + $value3['amount'];
                $update_return_dues->save();

                // $find_bill_id                    = $tables['purchase_return_entries']->where('purchase_return_id', $value3['purchase_return_id'])->first();
                // $update_bill_dues                = $tables['bills']->find($find_bill_id['bill_id']);
                // $update_bill_dues->return_amount = $update_bill_dues['return_amount'] + $value3['amount'];
                // $update_bill_dues->save();
            }
        }

        if ($find_payment->delete())
        {   
            $find_type          = $find_payment['type'];
            $select_customer    = $find_payment['customer_id'];

            DB::commit();

            return back()->with("success","Payment Deleted Successfully !!")->with("find_customer", $select_customer)->with('find_type', $find_type);
        }
        else
        {
            DB::rollback();

            return back()->with("unsuccess","Something Went Wrong.Please Try Again.")->with('find_customer', $select_customer)->with('find_type', $find_type);
        }    
    }

    public function contactList($id)
    {
        $customers      = Customers::find($id);

        $receivable     = Invoices::where('customer_id', $id)
                                ->selectRaw('SUM(invoice_amount) as total_receivable')
                                ->first();

        $received       = Payments::where('customer_id', $id)
                                ->whereIn('payments.type', [0,4])
                                ->selectRaw('SUM(amount) as total_received')
                                ->first();

        $invoices       = Invoices::where('customer_id', $id)
                                ->where('due_amount', '>', 0)
                                ->selectRaw('invoices.*')
                                ->get();

        $data['name']           = $customers['name'];
        $data['phone']          = $customers['phone'];
        $data['address']        = $customers['address'];
        $data['receivable']     = $receivable['total_receivable'];
        $data['received']       = $received['total_received'];
        $data['invoices']       = $invoices;

        return Response::json($data);
    }

    public function contactListBill($id)
    {
        $customers      = Customers::find($id);

        $payable        = Bills::where('vendor_id', $id)
                                ->selectRaw('SUM(bill_amount) as total_payable')
                                ->first();

        $paid           = Payments::where('customer_id', $id)
                                ->whereIn('payments.type', [1,5])
                                ->selectRaw('SUM(amount) as total_paid')
                                ->first();

        $bills          = Bills::where('vendor_id', $id)
                                ->where('due_amount', '>', 0)
                                ->selectRaw('bills.*')
                                ->get();

        $data['name']           = $customers['name'];
        $data['phone']          = $customers['phone'];
        $data['address']        = $customers['address'];
        $data['payable']        = $payable['total_payable'];
        $data['paid']           = $paid['total_paid'];
        $data['bills']          = $bills;

        return Response::json($data);
    }

    public function contactListSalesReturn($id)
    {
        $customers      = Customers::find($id);

        $payable        = SalesReturn::where('customer_id', $id)
                                ->selectRaw('SUM(return_amount) as total_payable')
                                ->first();

        $paid           = Payments::where('customer_id', $id)
                                ->where('payments.type', 2)
                                ->selectRaw('SUM(amount) as total_paid')
                                ->first();

        $sale_returns   = SalesReturn::where('customer_id', $id)
                                ->where('due_amount', '>', 0)
                                ->selectRaw('sales_return.*')
                                ->get();

        $data['name']           = $customers['name'];
        $data['phone']          = $customers['phone'];
        $data['address']        = $customers['address'];
        $data['payable']        = $payable['total_payable'];
        $data['paid']           = $paid['total_paid'];
        $data['sale_returns']   = $sale_returns;

        return Response::json($data);
    }

    public function contactListPurchaseReturn($id)
    {
        $customers          = Customerrs::find($id);

        $payable            = PurchaseReturn::where('customer_id', $id)
                                ->selectRaw('SUM(return_amount) as total_payable')
                                ->first();

        $paid               = Payments::where('customer_id', $id)
                                ->where('payments.type', 2)
                                ->selectRaw('SUM(amount) as total_paid')
                                ->first();

        $purchase_returns   = PurchaseReturn::where('customer_id', $id)
                                ->where('due_amount', '>', 0)
                                ->selectRaw('purchase_return.*')
                                ->get();

        $data['name']               = $customers['name'];
        $data['phone']              = $customers['phone'];
        $data['address']            = $customers['address'];
        $data['payable']            = $payable['total_payable'];
        $data['paid']               = $paid['total_paid'];
        $data['purchase_returns']   = $purchase_returns;

        return Response::json($data);
    }

    public function paymentList($id)
    {
        if ($id == 0)
        {
            $data           = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->select('payments.*',
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                        ->orderBy('payments.id', 'DESC')
                                        ->get();
        }
        else
        {
            $data           = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->where('payments.customer_id', $id)
                                        ->select('payments.*', 
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                        ->orderBy('payments.id', 'DESC')
                                        ->get();
        }

        return Response::json($data);
    }

    public function paymentListSearch($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data           = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->where('customers.name', 'LIKE', "%$id%")
                                        ->orWhere('payments.payment_date', 'LIKE', "%$search_by_date%")
                                        ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number, $table_id) {
                                            return $query->orWhere('payments.payment_number', 'LIKE', "%$search_by_payment_number%");
                                        })
                                        ->select('payments.*', 
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->get();
        }
        else
        {
            $data           = Payments::join('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->select('payments.*', 
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->get();
        }

        return Response::json($data);
    }
}
