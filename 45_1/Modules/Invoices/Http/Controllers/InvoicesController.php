<?php

namespace Modules\Invoices\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\PaidThroughAccounts;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\UnitConversions;
use App\Models\Users;
use App\Models\AccountTransactions;
use App\Models\BranchInventories;
use App\Models\ExpenseCategories;
use App\Models\Expenses;
use App\Models\Transactions;
use App\Models\CurrentBalance;
use Response;
use DB;
use View;
use Carbon\Carbon;

class InvoicesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoices           = Invoices::orderBy('id', 'DESC')->first();
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();

        return view('invoices::index', compact('paid_accounts', 'invoices'));
    }

    public function AllSales()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End


        // $invoice_entries =  InvoiceEntries::get();

        // foreach ($invoice_entries as $key => $value)
        // {
        //     $find_product                               = ProductEntries::find($value['product_entry_id']);
        //     $find_invoice_entry                         = InvoiceEntries::find($value['id']);
        //     $find_invoice_entry->main_unit_id           = $find_product['unit_id'];
        //     $find_invoice_entry->conversion_unit_id     = $find_product['unit_id'];
        //     $find_invoice_entry->save();
        // }

        // dd('done');
 
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                                ->get();

        return view('invoices::all_sales', compact('paid_accounts'));
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('invoices::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'          => 'required',
            'customer_id'           => 'required',
            'product_entries.*'     => 'required',
            'amount.*'              => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            // $adjustment     = $data['adjustment_amount'];
            $vat            = $data['vat_amount'];
            // $tax            = $data['tax_amount'];
             
            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }

            $data_find                          = Invoices::orderBy('created_at', 'DESC')->first();
            $invoice_number                     = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

            $invoice                            = new Invoices;;
            $invoice->invoice_number            = $invoice_number;
            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->customer_name             = $data['customer_name'];
            $invoice->customer_phone            = $data['customer_phone'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $data['total_amount'];
            $invoice->total_buy_price           = $buy_price;
            $invoice->total_discount            = $discount;
            // $invoice->adjustment_type           = $data['adjustment_type'];
            // $invoice->total_adjustment          = $adjustment;
            // $invoice->adjustment_note           = $data['adjustment_note'];
            $invoice->invoice_note              = $data['invoice_note'];
            // $invoice->total_tax                 = $tax;
            $invoice->total_vat                 = $vat;
            $invoice->branch_id                 = $branch_id;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->previous_due              = $data['previous_due'];
            $invoice->adjusted_amount           = $data['adjustment'];
            $invoice->major_category_id         = $data['major_category_id'] != 0 ? $data['major_category_id'] : null;
            $invoice->created_by                = $user_id;

            if ($invoice->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'        => $invoice['id'],
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value,
                        'brand_id'          => $product_buy_price['brand_id'],
                        'main_unit_id'      => $data['main_unit_id'][$key],
                        'conversion_unit_id'=> $data['unit_id'][$key],
                        'customer_id'       => $invoice['customer_id'],
                        'reference_id'      => $data['reference_id'],
                        'buy_price'         => $product_buy_price['buy_price'],
                        'rate'              => $data['rate'][$key],
                        'quantity'          => $data['quantity'][$key],
                        'total_amount'      => $data['amount'][$key],
                        'discount_type'     => $data['discount_type'][$key],
                        'discount_amount'   => $data['discount'][$key],
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('invoice_entries')->insert($invoice_entries);

                stockOut($data, $item_id=null);

                //Condition 1
                if (isset($data['amount_paid']))
                {
                    $data_find        = Payments::orderBy('id', 'DESC')->first();
                    $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {
                        if ($data['amount_paid'][$i] > 0)
                        {
                            $account_transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through_id'       => $data['paid_through'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'note'                  => $data['note'][$i] . '(বিক্রয় বাবদ আদায়)',
                                'type'                  => 0,  // 0 = In , 1 = Out
                                'transaction_head'      => 'sales',
                                'associated_id'         => $invoice->id,
                                'branch_id'             => $branch_id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payments = [
                                'payment_number'        => $payment_number,
                                'customer_id'           => $data['customer_id'],
                                'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'paid_through'          => $data['paid_through'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 0,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payment_id = DB::table('payments')->insertGetId($payments);      

                            if ($payment_id)
                            {
                                $payment_entries = [
                                        'payment_id'        => $payment_id,
                                        'invoice_id'        => $invoice['id'],
                                        'amount'            => $data['amount_paid'][$i],
                                        'created_by'        => $user_id,
                                        'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('payment_entries')->insert($payment_entries);  
                            }

                            $update_invoice_dues                = Invoices::find($invoice['id']);
                            $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                            $update_invoice_dues->save();

                            $payment_number++;
                        }
                    }

                    if (isset($account_transactions))
                    {
                        DB::table('account_transactions')->insert($account_transactions);
                    }
                }

                if ($data['balance_type'] == 1)
                {
                    $op_bal  = $data['previous_due'] + $data['total_amount'];
                }
                else
                {
                    $op_bal  = $data['total_amount'];
                }

                $paidAmount  = $data['cash_given'] + $data['adjustment'];

                //Condition 2
                if (($paidAmount > $data['total_amount']) && ($paidAmount <= $op_bal))
                {
                    //This Code is for invoice wise Payment Start
                    $old_due_invoices_con_1   = Invoices::where('customer_id', $data['customer_id'])
                                                        ->where('due_amount', '>', 0)
                                                        ->orderBy('created_at', 'ASC')
                                                        ->get();

                    $excess_amount_con_1      = $paidAmount - $data['total_amount'];

                    foreach ($old_due_invoices_con_1 as $key_con_1 => $value_con_1)
                    {
                        if ($excess_amount_con_1 > 0)
                        {
                            if ($value_con_1->due_amount <= $excess_amount_con_1)
                            {
                                $data_find_con_1_1        = Payments::orderBy('id', 'DESC')->first();
                                $payment_number_con_1_1   = $data_find_con_1_1 != null ? $data_find_con_1_1['payment_number'] + 1 : 1;

                                $account_transactions_con_1[] = [
                                    'customer_id'           => $data['customer_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $value_con_1->due_amount,
                                    'paid_through_id'       => $data['paid_through'][0],
                                    'account_information'   => $data['account_information'][0],
                                    'note'                  => $data['note'][0] . '(বিক্রয় বাবদ আদায়)',
                                    'type'                  => 0,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'sales',
                                    'associated_id'         => $value_con_1->id,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payments_1 = [
                                    'payment_number'        => $payment_number_con_1_1,
                                    'customer_id'           => $data['customer_id'],
                                    'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $value_con_1->due_amount,
                                    'account_information'   => $data['account_information'][0],
                                    'paid_through'          => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 0,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payment_id_con_1_1 = DB::table('payments')->insertGetId($payments_1);      

                                if ($payment_id_con_1_1)
                                {
                                    $payment_entries_con_1_1 = [
                                            'payment_id'        => $payment_id_con_1_1,
                                            'invoice_id'        => $value_con_1->id,
                                            'amount'            => $value_con_1->due_amount,
                                            'created_by'        => $user_id,
                                            'created_at'        => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('payment_entries')->insert($payment_entries_con_1_1);  
                                }

                                $update_invoice_dues_con_1_1                = Invoices::find($value_con_1['id']);
                                $update_invoice_dues_con_1_1->due_amount    = $update_invoice_dues_con_1_1['due_amount'] - $value_con_1->due_amount;
                                $update_invoice_dues_con_1_1->save();

                                $excess_amount_addable = $value_con_1->due_amount;
                            }

                            if ($value_con_1->due_amount > $excess_amount_con_1)
                            {
                                $data_find_con_1_2        = Payments::orderBy('id', 'DESC')->first();
                                $payment_number_con_1_2   = $data_find_con_1_2 != null ? $data_find_con_1_2['payment_number'] + 1 : 1;

                                $account_transactions_con_1[] = [
                                    'customer_id'           => $data['customer_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $excess_amount_con_1,
                                    'paid_through_id'       => $data['paid_through'][0],
                                    'account_information'   => $data['account_information'][0],
                                    'note'                  => $data['note'][0] . '(বিক্রয় বাবদ আদায়)',
                                    'type'                  => 0,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'sales',
                                    'associated_id'         => $value_con_1->id,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payments_con_1_2 = [
                                    'payment_number'        => $payment_number_con_1_2,
                                    'customer_id'           => $data['customer_id'],
                                    'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $excess_amount_con_1,
                                    'account_information'   => $data['account_information'][0],
                                    'paid_through'          => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 0,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payment_id_con_1_2 = DB::table('payments')->insertGetId($payments_con_1_2);      

                                if ($payment_id_con_1_2)
                                {
                                    $payment_entries_con_1_2 = [
                                            'payment_id'        => $payment_id_con_1_2,
                                            'invoice_id'        => $value_con_1->id,
                                            'amount'            => $excess_amount_con_1,
                                            'created_by'        => $user_id,
                                            'created_at'        => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('payment_entries')->insert($payment_entries_con_1_2);  
                                }

                                $update_invoice_dues_con_1_2                = Invoices::find($value_con_1['id']);
                                $update_invoice_dues_con_1_2->due_amount    = $update_invoice_dues_con_1_2['due_amount'] - $excess_amount_con_1;
                                $update_invoice_dues_con_1_2->save();

                                $excess_amount_addable = $excess_amount_con_1;
                            }

                            $excess_amount_con_1 = $excess_amount_con_1 - $excess_amount_addable;
                        }
                    }

                    if (isset($account_transactions_con_1))
                    {
                        DB::table('account_transactions')->insert($account_transactions_con_1);
                    }
                }

                //Condition 3
                if ($paidAmount > $op_bal)
                {
                    //This Code is for invoice wise Payment Start
                    $old_due_invoices_con_2     = Invoices::where('customer_id', $data['customer_id'])
                                                            ->where('due_amount', '>', 0)
                                                            ->orderBy('created_at', 'ASC')
                                                            ->get();

                    $excess_amount_con_2        = $data['previous_due'];

                    foreach ($old_due_invoices_con_2 as $key_con_2 => $value_con_2)
                    {
                        if ($excess_amount_con_2 > 0)
                        {
                            if ($value_con_2->due_amount <= $excess_amount_con_2)
                            {
                                $data_find_con_2_1        = Payments::orderBy('id', 'DESC')->first();
                                $payment_number_con_2_1   = $data_find_con_2_1 != null ? $data_find_con_2_1['payment_number'] + 1 : 1;

                                $account_transactions_con_2[] = [
                                    'customer_id'           => $data['customer_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $value_con_2->due_amount,
                                    'paid_through_id'       => $data['paid_through'][0],
                                    'account_information'   => $data['account_information'][0],
                                    'note'                  => $data['note'][0] . '(বিক্রয় বাবদ আদায়)',
                                    'type'                  => 0,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'sales',
                                    'associated_id'         => $value_con_2->id,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payments_con_2_1 = [
                                    'payment_number'        => $payment_number_con_2_1,
                                    'customer_id'           => $data['customer_id'],
                                    'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $value_con_2->due_amount,
                                    'account_information'   => $data['account_information'][0],
                                    'paid_through'          => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 0,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payment_id_con_2_1 = DB::table('payments')->insertGetId($payments_con_2_1);      

                                if ($payment_id_con_2_1)
                                {
                                    $payment_entries_con_2_1 = [
                                            'payment_id'        => $payment_id_con_2_1,
                                            'invoice_id'        => $value_con_2['id'],
                                            'amount'            => $value_con_2->due_amount,
                                            'created_by'        => $user_id,
                                            'created_at'        => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('payment_entries')->insert($payment_entries_con_2_1);  
                                }

                                $update_invoice_dues_con_2_1                = Invoices::find($value_con_2['id']);
                                $update_invoice_dues_con_2_1->due_amount    = $update_invoice_dues_con_2_1['due_amount'] - $value_con_2->due_amount;
                                $update_invoice_dues_con_2_1->save();

                                $excess_cal = $value_con_2->due_amount;
                            }

                            if ($value_con_2->due_amount > $excess_amount_con_2)
                            {
                                $data_find_con_2_2        = Payments::orderBy('id', 'DESC')->first();
                                $payment_number_con_2_2   = $data_find_con_2_2 != null ? $data_find_con_2_2['payment_number'] + 1 : 1;

                                $account_transactions_con_2[] = [
                                    'customer_id'           => $data['customer_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $excess_amount_con_2,
                                    'paid_through_id'       => $data['paid_through'][0],
                                    'account_information'   => $data['account_information'][0],
                                    'note'                  => $data['note'][0] . '(বিক্রয় বাবদ আদায়)',
                                    'type'                  => 0,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'sales',
                                    'associated_id'         => $value_con_2->id,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payments_con_2_2 = [
                                    'payment_number'        => $payment_number_con_2_2,
                                    'customer_id'           => $data['customer_id'],
                                    'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $excess_amount_con_2,
                                    'account_information'   => $data['account_information'][0],
                                    'paid_through'          => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 0,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payment_id_con_2_2 = DB::table('payments')->insertGetId($payments_con_2_2);      

                                if ($payment_id_con_2_2)
                                {
                                    $payment_entries_con_2_2 = [
                                            'payment_id'        => $payment_id_con_2_2,
                                            'invoice_id'        => $value_con_2['id'],
                                            'amount'            => $excess_amount_con_2,
                                            'created_by'        => $user_id,
                                            'created_at'        => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('payment_entries')->insert($payment_entries_con_2_2);  
                                }

                                $update_invoice_dues_con_2_2                = Invoices::find($value_con_2['id']);
                                $update_invoice_dues_con_2_2->due_amount    = $update_invoice_dues_con_2_2['due_amount'] - $excess_amount_con_2;
                                $update_invoice_dues_con_2_2->save();

                                $excess_cal     = $excess_amount_con_2;
                            }

                            $excess_amount_con_2 = $excess_amount_con_2 - $excess_cal;
                        }
                    }

                    if (isset($account_transactions_con_2))
                    {
                        DB::table('account_transactions')->insert($account_transactions_con_2);
                    }
                    //This Code is for invoice wise Payment End
                }

                //For Advance Paymenr Adjustment
                if (isset($data['adjust_cash_given']))
                {
                    $extra_paid         = $paidAmount - $data['adjustment'];
                    $adjusted_amount    = $data['adjustment'];

                    $account_transactions_customer_advance_adjust = [
                        'customer_id'           => $data['customer_id'],
                        'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                        'amount'                => abs($adjusted_amount),
                        'paid_through_id'       => 1,
                        'note'                  => '(অগ্রীম থেকে সমন্বয়)',
                        'type'                  => 1,  // 0 = In , 1 = Out
                        'transaction_head'      => 'customer-advance-adjustment',
                        'associated_id'         => $invoice->id,
                        'branch_id'             => $branch_id,
                        'created_by'            => $user_id,
                        'created_at'            => date('Y-m-d H:i:s'),
                    ];

                    DB::table('account_transactions')->insert($account_transactions_customer_advance_adjust);

                    //Update advance payment in customer table start
                    $find_customer                              = Customers::find($data['customer_id']);
                    $find_customer->customer_advance_payment    = $find_customer['customer_advance_payment'] - $data['adjustment'];
                    $find_customer->save();
                    //Update advance payment in customer table end
                }
                else
                {
                    if ($data['change_amount'] > 0)
                    {
                        $advance_amount_advance_amount_con_2  = $data['change_amount'];

                        $account_transactions_advance_amount_con_2 = [
                            'customer_id'           => $data['customer_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                            'amount'                => $advance_amount_advance_amount_con_2,
                            'paid_through_id'       => 1,
                            'note'                  => '(অগ্রীম বাবদ আদায়)',
                            'type'                  => 0,  // 0 = In , 1 = Out
                            'transaction_head'      => 'customer-advance',
                            'associated_id'         => $invoice->id,
                            'branch_id'             => $branch_id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        DB::table('account_transactions')->insert($account_transactions_advance_amount_con_2);  
                    }
                }

                if ($data['change_amount'] > 0)
                {
                    //Update advance payment in customer table start
                    $find_customer                              = Customers::find($data['customer_id']);
                    $find_customer->customer_advance_payment    = $find_customer['customer_advance_payment'] + $data['change_amount'];
                    $find_customer->save();
                    //Update advance payment in customer table end
                }

                $transaction_data['date']           = date('Y-m-d', strtotime($data['selling_date']));
                $transaction_data['type']           = 0;
                $transaction_data['account_head']   = 'sales';
                $transaction_data['transaction_id'] = $invoice->id;
                $transaction_data['customer_id']    = $data['customer_id'];
                $transaction_data['note']           = 'বিক্রয় বাবদ গ্রহণযোগ্য';
                $transaction_data['amount']         = $data['total_amount'];
                $transaction_data['paid_through']   = null;
                transactions($transaction_data);

                if ($paidAmount > 0)
                {
                    if ($data['adjustment'] > 0)
                    {
                        $transaction_data['date']           = date('Y-m-d', strtotime($data['selling_date']));
                        $transaction_data['type']           = 1;
                        $transaction_data['account_head']   = 'customer-advance-adjustment';
                        $transaction_data['transaction_id'] = $invoice->id;
                        $transaction_data['customer_id']    = $data['customer_id'];
                        $transaction_data['note']           = 'পূর্বের অগ্রিম থেকে সমন্বয়';
                        $transaction_data['amount']         = $data['adjustment'];
                        $transaction_data['paid_through']   = null;
                        transactions($transaction_data);
                    }

                    $transaction_data['date']           = date('Y-m-d', strtotime($data['selling_date']));
                    $transaction_data['type']           = 1;
                    $transaction_data['account_head']   = 'sales';
                    $transaction_data['transaction_id'] = $invoice->id;
                    $transaction_data['customer_id']    = $data['customer_id'];
                    $transaction_data['note']           = 'বিক্রয় বাবদ আদায়';
                    $transaction_data['amount']         = $paidAmount;
                    $transaction_data['paid_through']   = null;
                    transactions($transaction_data);
                }

                if (!empty($data['expense_category_id']))
                {
                    foreach ($data['expense_category_id'] as $key => $value)
                    {
                        if ($value != 0)
                        {
                            $expense_find                   = Expenses::orderBy('created_at', 'DESC')->first();
                            $expense_number                 = $expense_find != null ? $expense_find['expense_number'] + 1 : 1;
                   
                            $expense                        = new Expenses;
                            $expense->expense_number        = $expense_number;
                            $expense->expense_date          = date('Y-m-d', strtotime($data['selling_date']));
                            $expense->customer_id           = $data['customer_id'];
                            $expense->invoice_id            = $invoice->id;
                            $expense->expense_category_id   = $value;
                            $expense->amount                = $data['expense_amount'][$key];
                            $expense->paid_through_id       = 1;
                            $expense->note                  = $data['expense_note'][$key];
                            $expense->created_by            = $user_id;

                            if ($expense->save())
                            {   
                                $account_transaction                      = new AccountTransactions;
                                $account_transaction->transaction_date    = date('Y-m-d', strtotime($data['selling_date']));
                                $account_transaction->amount              = $data['expense_amount'][$key];
                                $account_transaction->paid_through_id     = 1;
                                $account_transaction->note                = $data['expense_note'][$key];
                                $account_transaction->type                = 1;
                                $account_transaction->transaction_head    = 'expense';
                                $account_transaction->associated_id       = $expense->id;
                                $account_transaction->created_by          = $user_id;
                                $account_transaction->save();
                            }
                        }
                    }
                }

                //Current balance 
                if (isset($data['current_balance_amount_paid']))
                {
                    for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                    {
                        if ($data['current_balance_amount_paid'][$i] > 0)
                        {
                            $current_balance[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['current_balance_amount_paid'][$i],
                                'paid_through_id'       => $data['current_balance_paid_through'][$i],
                                'account_information'   => $data['current_balance_account_information'][$i],
                                'note'                  => $data['current_balance_note'][$i] . '(বিক্রয় বাবদ আদায়)',
                                'type'                  => 0,  // 0 = In , 1 = Out
                                'transaction_head'      => 'sales',
                                'associated_id'         => $invoice->id,
                                'branch_id'             => $branch_id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];
                        }
                    }

                    if (isset($current_balance))
                    {
                        DB::table('current_balance')->insert($current_balance);
                    }
                }

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Sales Created Successfully !!");
                }
                else
                {
                    $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                            ->select('invoices.*',
                                                     'customers.name as customer_name',
                                                     'customers.address as address',
                                                     'customers.phone as phone')
                                            ->find($invoice['id']);

                    $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->where('invoice_entries.invoice_id', $invoice['id'])
                                            ->select('invoice_entries.*',
                                                     'product_entries.product_type as product_type',
                                                     'product_entries.name as product_entry_name',
                                                     'units.name as unit_name',
                                                     'products.name as product_name')
                                            ->get();  
                                 
                    $user_info  = Users::find(1);
                    $expenses   = Expenses::where('invoice_id', $id)->get();

                    if ($user_info['pos_printer'] == 0)
                    {
                        return View::make('invoices::show_pos')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info)->with("expenses", $expenses);
                    }
                    else
                    {
                        return View::make('invoices::show')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info)->with("expenses", $expenses);
                    }
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.address as address',
                                             'customers.phone as phone')
                                    ->find($id);
        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->where('invoice_entries.invoice_id', $id)
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.name as product_entry_name',
                                             'products.name as product_name')
                                    ->get();
        $expenses   = Expenses::where('invoice_id', $id)->get(); 
        $user_info  = Users::find(1);

        return View::make('invoices::show')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info)->with("expenses", $expenses);
    }

    public function showPos($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.address as address',
                                             'customers.phone as phone')
                                    ->find($id);
        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->where('invoice_entries.invoice_id', $id)
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.name as product_entry_name',
                                             'products.name as product_name')
                                    ->get();         
        $user_info  = Users::find(1);
        $expenses   = Expenses::where('invoice_id', $id)->get();
        
        return View::make('invoices::show_pos')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info)->with("expenses", $expenses);
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $products               = Products::orderBy('products.total_sold', 'DESC')
                                            ->where('products.status', 1)
                                            ->get();

        $product_entry          = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->where('product_entries.stock_in_hand', '!=', null)
                                            ->where('product_entries.status', 1)
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                         GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                         GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                         GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                         GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                         GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                         GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                         GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                         GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT products.id) as product_id
                                                        ')
                                            ->orderBy('product_entries.total_sold', 'DESC')
                                            ->get();

        $product_entry          = $product_entry->sortBy('name')->all();
        $product_entries        = collect($product_entry);
        $find_invoice           = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                            ->select('invoices.*',
                                                 'customers.id as customer_id',
                                                 'customers.name as contact_name')
                                            ->find($id);

        $find_customer          = Customers::find($find_invoice['customer_id']);
        $find_invoice_entries   = InvoiceEntries::leftjoin('customers', 'customers.id', 'invoice_entries.customer_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                            ->where('invoice_entries.invoice_id', $id)
                                            ->select('invoice_entries.*',
                                                    'customers.id as customer_id',
                                                    'customers.name as customer_name',
                                                    'product_entries.id as item_id',
                                                    'product_entries.product_code as product_code',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.name as item_name')
                                            ->get();

        $entries_count          = $find_invoice_entries->count();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->where('payment_entries.invoice_id', $id)
                                        ->selectRaw('payment_entries.*, 
                                                     payments.paid_through as paid_through_id,
                                                     payments.account_information as account_information,
                                                     payments.payment_date as payment_date,
                                                     payments.note as note')
                                        ->get();

        $payment_entries_count  = $payment_entries->count();

        $current_balance        = CurrentBalance::where('transaction_head', 'sales')
                                                ->where('associated_id', $id)
                                                ->selectRaw('current_balance.*')
                                                ->get();

        $current_balance_count  = $current_balance->count();

        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();

        if ((Auth::user()->branch_id != $find_invoice->branch_id) && (Auth::user()->branch_id != 1))
        {
            return back()->with('unsuccess', 'You are not allowed to edit this invoice');
        }

        $expenses       = Expenses::where('invoice_id', $id)->get();
        $expense_count  = $expenses->count();
        
        return view('invoices::edit', compact('products', 'product_entries', 'find_invoice', 'find_invoice_entries', 'entries_count', 'payment_entries', 'payment_entries_count', 'paid_accounts', 'expenses', 'expense_count', 'find_customer', 'current_balance', 'current_balance_count'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'      => 'required',
            'customer_id'       => 'required',
            'product_entries.*' => 'required',
            'amount.*'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            // $adjustment = $data['adjustment_amount'];
            $vat           = $data['vat_amount'];
            // $tax        = $data['tax_amount'];
            
            $old_invoice   = Invoices::find($id);
            $invoice       = Invoices::find($id);

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }

            //Calculate Due Amount

                if ($data['total_amount'] > $invoice['invoice_amount']) 
                {
                    $invoice_dues = $invoice['due_amount'] + ($data['total_amount'] - $invoice['invoice_amount']);

                }
                
                if ($data['total_amount'] < $invoice['invoice_amount'])
                {
                    $invoice_dues = $invoice['due_amount'] - ($invoice['invoice_amount'] - $data['total_amount']);
                }

                if ($data['total_amount'] == $invoice['invoice_amount'])
                {
                    $invoice_dues = $invoice['due_amount'];
                }
            
            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->customer_name             = $data['customer_name'];
            $invoice->customer_phone            = $data['customer_phone'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $invoice_dues;
            $invoice->total_buy_price           = $buy_price;
            $invoice->total_discount            = $discount;
            // $invoice->adjustment_type   = $data['adjustment_type'];
            // $invoice->total_adjustment  = $adjustment;
            // $invoice->adjustment_note   = $data['adjustment_note'];
            $invoice->invoice_note              = $data['invoice_note'];
            // $invoice->total_tax                 = $tax;
            $invoice->total_vat                 = $vat;
            // $invoice->branch_id                 = $branch_id;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->discount_code             = $data['coupon_code'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->previous_due              = $data['previous_due'];
            $invoice->adjusted_amount           = $data['adjustment'];
            $invoice->major_category_id         = $data['major_category_id'] != 0 ? $data['major_category_id'] : null;
            $invoice->updated_by                = $user_id;

            if ($invoice->save())
            {
                $item_id                = InvoiceEntries::where('invoice_id', $invoice['id'])->get();
                $item_delete            = InvoiceEntries::where('invoice_id', $invoice['id'])->delete();

                if (isset($data['payment_id']))
                {
                    foreach ($data['payment_id'] as $key_p => $value_p)
                    {
                        $payment_delete  = Payments::where('id', $value_p)->delete();
                    }
                }

                $update_invoice                = Invoices::find($invoice->id);
                $update_invoice->due_amount    = $update_invoice['invoice_amount'];
                $update_invoice->updated_by    = $user_id;
                $update_invoice->save();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'        => $invoice['id'],
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value,
                        'brand_id'          => $product_buy_price['brand_id'],
                        'main_unit_id'      => $data['main_unit_id'][$key],
                        'conversion_unit_id'=> $data['unit_id'][$key],
                        'customer_id'       => $invoice['customer_id'],
                        'reference_id'      => $data['reference_id'],
                        'buy_price'         => $product_buy_price['buy_price'],
                        'rate'              => $data['rate'][$key],
                        'quantity'          => $data['quantity'][$key],
                        'total_amount'      => $data['amount'][$key],
                        'discount_type'     => $data['discount_type'][$key],
                        'discount_amount'   => $data['discount'][$key],
                        'created_by'        => $user_id,
                    ];
                }

                DB::table('invoice_entries')->insert($invoice_entries);

                stockOut($data, $item_id);

                $transaction_delete     = AccountTransactions::whereIn('transaction_head', ['sales', 'customer-advance', 'customer-advance-adjustment'])
                                                        ->where('associated_id', $invoice['id'])
                                                        ->delete();

                
                $find_customer_cash                             = Customers::find($data['customer_id']);
                $find_customer_cash->customer_advance_payment   = $find_customer_cash['customer_advance_payment'] - $old_invoice['change_amount'];
                $find_customer_cash->save();
                
                if ($data['balance_type'] == 1)
                {
                    $op_bal  = $data['previous_due'] + $data['total_amount'];
                }
                else
                {
                    $op_bal  = $data['total_amount'];
                }

                //Condition 1
                if (isset($data['amount_paid']))
                {
                    $data_find        = Payments::orderBy('id', 'DESC')->first();
                    $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {
                        if ($data['amount_paid'][$i] > 0)
                        {
                            $account_transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through_id'       => $data['paid_through'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'note'                  => $data['note'][$i] . '(বিক্রয় বাবদ আদায়)',
                                'type'                  => 0,  // 0 = In , 1 = Out
                                'transaction_head'      => 'sales',
                                'associated_id'         => $invoice->id,
                                'branch_id'             => $branch_id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payments = [
                                'payment_number'        => $payment_number,
                                'customer_id'           => $data['customer_id'],
                                'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'paid_through'          => $data['paid_through'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 0,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payment_id = DB::table('payments')->insertGetId($payments);      

                            if ($payment_id)
                            {
                                $payment_entries = [
                                        'payment_id'        => $payment_id,
                                        'invoice_id'        => $invoice['id'],
                                        'amount'            => $data['amount_paid'][$i],
                                        'created_by'        => $user_id,
                                        'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('payment_entries')->insert($payment_entries);  
                            }

                            $update_invoice_dues                = Invoices::find($invoice['id']);
                            $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                            $update_invoice_dues->save();

                            $payment_number++;
                        }
                    }

                    if (isset($account_transactions))
                    {
                        DB::table('account_transactions')->insert($account_transactions);
                    }
                }

                $paidAmount  = $data['cash_given'] + $data['adjustment'];

                //Condition 2
                if (($paidAmount > $data['total_amount']) && ($paidAmount <= $op_bal))
                {
                    //This Code is for invoice wise Payment Start
                    $old_due_invoices_con_1   = Invoices::where('customer_id', $data['customer_id'])
                                                        ->where('due_amount', '>', 0)
                                                        ->where('id', '!=', $id)
                                                        ->orderBy('created_at', 'ASC')
                                                        ->get();

                    $excess_amount_con_1      = $paidAmount - $data['total_amount'];

                    foreach ($old_due_invoices_con_1 as $key_con_1 => $value_con_1)
                    {
                        if ($excess_amount_con_1 > 0)
                        {
                            if ($value_con_1->due_amount <= $excess_amount_con_1)
                            {
                                $data_find_con_1_1        = Payments::orderBy('id', 'DESC')->first();
                                $payment_number_con_1_1   = $data_find_con_1_1 != null ? $data_find_con_1_1['payment_number'] + 1 : 1;

                                $account_transactions_con_1[] = [
                                    'customer_id'           => $data['customer_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $value_con_1->due_amount,
                                    'paid_through_id'       => $data['paid_through'][0],
                                    'account_information'   => $data['account_information'][0],
                                    'note'                  => $data['note'][0] . '(বিক্রয় বাবদ আদায়)',
                                    'type'                  => 0,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'sales',
                                    'associated_id'         => $value_con_1->id,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payments_1 = [
                                    'payment_number'        => $payment_number_con_1_1,
                                    'customer_id'           => $data['customer_id'],
                                    'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $value_con_1->due_amount,
                                    'account_information'   => $data['account_information'][0],
                                    'paid_through'          => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 0,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payment_id_con_1_1 = DB::table('payments')->insertGetId($payments_1);      

                                if ($payment_id_con_1_1)
                                {
                                    $payment_entries_con_1_1 = [
                                            'payment_id'        => $payment_id_con_1_1,
                                            'invoice_id'        => $value_con_1->id,
                                            'amount'            => $value_con_1->due_amount,
                                            'created_by'        => $user_id,
                                            'created_at'        => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('payment_entries')->insert($payment_entries_con_1_1);  
                                }

                                $update_invoice_dues_con_1_1                = Invoices::find($value_con_1['id']);
                                $update_invoice_dues_con_1_1->due_amount    = $update_invoice_dues_con_1_1['due_amount'] - $value_con_1->due_amount;
                                $update_invoice_dues_con_1_1->save();

                                $excess_amount_addable = $value_con_1->due_amount;
                            }

                            if ($value_con_1->due_amount > $excess_amount_con_1)
                            {
                                $data_find_con_1_2        = Payments::orderBy('id', 'DESC')->first();
                                $payment_number_con_1_2   = $data_find_con_1_2 != null ? $data_find_con_1_2['payment_number'] + 1 : 1;

                                $account_transactions_con_1[] = [
                                    'customer_id'           => $data['customer_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $excess_amount_con_1,
                                    'paid_through_id'       => $data['paid_through'][0],
                                    'account_information'   => $data['account_information'][0],
                                    'note'                  => $data['note'][0] . '(বিক্রয় বাবদ আদায়)',
                                    'type'                  => 0,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'sales',
                                    'associated_id'         => $value_con_1->id,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payments_con_1_2 = [
                                    'payment_number'        => $payment_number_con_1_2,
                                    'customer_id'           => $data['customer_id'],
                                    'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $excess_amount_con_1,
                                    'account_information'   => $data['account_information'][0],
                                    'paid_through'          => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 0,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payment_id_con_1_2 = DB::table('payments')->insertGetId($payments_con_1_2);      

                                if ($payment_id_con_1_2)
                                {
                                    $payment_entries_con_1_2 = [
                                            'payment_id'        => $payment_id_con_1_2,
                                            'invoice_id'        => $value_con_1->id,
                                            'amount'            => $excess_amount_con_1,
                                            'created_by'        => $user_id,
                                            'created_at'        => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('payment_entries')->insert($payment_entries_con_1_2);  
                                }

                                $update_invoice_dues_con_1_2                = Invoices::find($value_con_1['id']);
                                $update_invoice_dues_con_1_2->due_amount    = $update_invoice_dues_con_1_2['due_amount'] - $excess_amount_con_1;
                                $update_invoice_dues_con_1_2->save();

                                $excess_amount_addable = $excess_amount_con_1;
                            }

                            $excess_amount_con_1 = $excess_amount_con_1 - $excess_amount_addable;
                        }
                    }

                    if (isset($account_transactions_con_1))
                    {
                        DB::table('account_transactions')->insert($account_transactions_con_1);
                    }
                }

                //Condition 3
                if ($paidAmount > $op_bal)
                {
                    //This Code is for invoice wise Payment Start
                    $old_due_invoices_con_2     = Invoices::where('customer_id', $data['customer_id'])
                                                            ->where('due_amount', '>', 0)
                                                            ->orderBy('created_at', 'ASC')
                                                            ->get();

                    $excess_amount_con_2        = $data['previous_due'];

                    foreach ($old_due_invoices_con_2 as $key_con_2 => $value_con_2)
                    {
                        if ($excess_amount_con_2 > 0)
                        {
                            if ($value_con_2->due_amount <= $excess_amount_con_2)
                            {
                                $data_find_con_2_1        = Payments::orderBy('id', 'DESC')->first();
                                $payment_number_con_2_1   = $data_find_con_2_1 != null ? $data_find_con_2_1['payment_number'] + 1 : 1;

                                $account_transactions_con_2[] = [
                                    'customer_id'           => $data['customer_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $value_con_2->due_amount,
                                    'paid_through_id'       => $data['paid_through'][0],
                                    'account_information'   => $data['account_information'][0],
                                    'note'                  => $data['note'][0] . '(বিক্রয় বাবদ আদায়)',
                                    'type'                  => 0,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'sales',
                                    'associated_id'         => $value_con_2->id,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payments_con_2_1 = [
                                    'payment_number'        => $payment_number_con_2_1,
                                    'customer_id'           => $data['customer_id'],
                                    'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $value_con_2->due_amount,
                                    'account_information'   => $data['account_information'][0],
                                    'paid_through'          => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 0,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payment_id_con_2_1 = DB::table('payments')->insertGetId($payments_con_2_1);      

                                if ($payment_id_con_2_1)
                                {
                                    $payment_entries_con_2_1 = [
                                            'payment_id'        => $payment_id_con_2_1,
                                            'invoice_id'        => $value_con_2['id'],
                                            'amount'            => $value_con_2->due_amount,
                                            'created_by'        => $user_id,
                                            'created_at'        => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('payment_entries')->insert($payment_entries_con_2_1);  
                                }

                                $update_invoice_dues_con_2_1                = Invoices::find($value_con_2['id']);
                                $update_invoice_dues_con_2_1->due_amount    = $update_invoice_dues_con_2_1['due_amount'] - $value_con_2->due_amount;
                                $update_invoice_dues_con_2_1->save();

                                $excess_cal = $value_con_2->due_amount;
                            }

                            if ($value_con_2->due_amount > $excess_amount_con_2)
                            {
                                $data_find_con_2_2        = Payments::orderBy('id', 'DESC')->first();
                                $payment_number_con_2_2   = $data_find_con_2_2 != null ? $data_find_con_2_2['payment_number'] + 1 : 1;

                                $account_transactions_con_2[] = [
                                    'customer_id'           => $data['customer_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $excess_amount_con_2,
                                    'paid_through_id'       => $data['paid_through'][0],
                                    'account_information'   => $data['account_information'][0],
                                    'note'                  => $data['note'][0] . '(বিক্রয় বাবদ আদায়)',
                                    'type'                  => 0,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'sales',
                                    'associated_id'         => $value_con_2->id,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payments_con_2_2 = [
                                    'payment_number'        => $payment_number_con_2_2,
                                    'customer_id'           => $data['customer_id'],
                                    'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $excess_amount_con_2,
                                    'account_information'   => $data['account_information'][0],
                                    'paid_through'          => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 0,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payment_id_con_2_2 = DB::table('payments')->insertGetId($payments_con_2_2);      

                                if ($payment_id_con_2_2)
                                {
                                    $payment_entries_con_2_2 = [
                                            'payment_id'        => $payment_id_con_2_2,
                                            'invoice_id'        => $value_con_2['id'],
                                            'amount'            => $excess_amount_con_2,
                                            'created_by'        => $user_id,
                                            'created_at'        => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('payment_entries')->insert($payment_entries_con_2_2);  
                                }

                                $update_invoice_dues_con_2_2                = Invoices::find($value_con_2['id']);
                                $update_invoice_dues_con_2_2->due_amount    = $update_invoice_dues_con_2_2['due_amount'] - $excess_amount_con_2;
                                $update_invoice_dues_con_2_2->save();

                                $excess_cal     = $excess_amount_con_2;
                            }

                            $excess_amount_con_2 = $excess_amount_con_2 - $excess_cal;
                        }
                    }

                    if (isset($account_transactions_con_2))
                    {
                        DB::table('account_transactions')->insert($account_transactions_con_2);
                    }
                    //This Code is for invoice wise Payment End
                }

                //For Advance Paymenr Adjustment
                if ($data['adjustment'] > 0)
                {
                    $extra_paid         = $paidAmount - $data['adjustment'];
                    $adjusted_amount    = $data['adjustment'];

                    $account_transactions_customer_advance_adjust = [
                        'customer_id'           => $data['customer_id'],
                        'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                        'amount'                => abs($adjusted_amount),
                        'paid_through_id'       => 1,
                        'note'                  => '(অগ্রীম থেকে সমন্বয়)',
                        'type'                  => 1,  // 0 = In , 1 = Out
                        'transaction_head'      => 'customer-advance-adjustment',
                        'associated_id'         => $invoice->id,
                        'branch_id'             => $branch_id,
                        'created_by'            => $user_id,
                        'created_at'            => date('Y-m-d H:i:s'),
                    ];

                    DB::table('account_transactions')->insert($account_transactions_customer_advance_adjust);

                    //Update advance payment in customer table start
                    $find_customer                              = Customers::find($data['customer_id']);
                    $find_customer->customer_advance_payment    = $find_customer['customer_advance_payment'] + $update_invoice['adjusted_amount'] - $data['adjustment'];
                    $find_customer->save();
                    //Update advance payment in customer table end
                }
                else
                {
                    if ($data['change_amount'] > 0)
                    {
                        $advance_amount_advance_amount_con_2  = $data['change_amount'];

                        $account_transactions_advance_amount_con_2 = [
                            'customer_id'           => $data['customer_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                            'amount'                => $advance_amount_advance_amount_con_2,
                            'paid_through_id'       => 1,
                            'note'                  => '(অগ্রীম বাবদ আদায়)',
                            'type'                  => 0,  // 0 = In , 1 = Out
                            'transaction_head'      => 'customer-advance',
                            'associated_id'         => $invoice->id,
                            'branch_id'             => $branch_id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        DB::table('account_transactions')->insert($account_transactions_advance_amount_con_2);  
                    }
                }

                if ($data['change_amount'] > 0)
                {
                    //Update advance payment in customer table start
                    $find_customer                              = Customers::find($data['customer_id']);
                    $find_customer->customer_advance_payment    = $find_customer['customer_advance_payment'] + $data['change_amount'];
                    $find_customer->save();
                    //Update advance payment in customer table end
                }

                $transactions   = Transactions::whereIn('account_head', ['sales', 'previoue-due-collection', 'customer-advance-adjustment'])
                                                ->where('invoice_id', $invoice['id'])
                                                ->delete();

                $transaction_data['date']           = date('Y-m-d', strtotime($data['selling_date']));
                $transaction_data['type']           = 0;
                $transaction_data['account_head']   = 'sales';
                $transaction_data['transaction_id'] = $invoice->id;
                $transaction_data['customer_id']    = $data['customer_id'];
                $transaction_data['note']           = 'বিক্রয় বাবদ গ্রহণযোগ্য';
                $transaction_data['amount']         = $data['total_amount'];
                $transaction_data['paid_through']   = null;
                transactions($transaction_data);

                if ($paidAmount > 0)
                {
                    if ($data['adjustment'] > 0)
                    {
                        $transaction_data['date']           = date('Y-m-d', strtotime($data['selling_date']));
                        $transaction_data['type']           = 1;
                        $transaction_data['account_head']   = 'customer-advance-adjustment';
                        $transaction_data['transaction_id'] = $invoice->id;
                        $transaction_data['customer_id']    = $data['customer_id'];
                        $transaction_data['note']           = 'পূর্বের অগ্রিম থেকে সমন্বয়';
                        $transaction_data['amount']         = $data['adjustment'];
                        $transaction_data['paid_through']   = null;
                        transactions($transaction_data);
                    }

                    $transaction_data['date']           = date('Y-m-d', strtotime($data['selling_date']));
                    $transaction_data['type']           = 1;
                    $transaction_data['account_head']   = 'sales';
                    $transaction_data['transaction_id'] = $invoice->id;
                    $transaction_data['customer_id']    = $data['customer_id'];
                    $transaction_data['note']           = 'বিক্রয় বাবদ আদায়';
                    $transaction_data['amount']         = $paidAmount;
                    $transaction_data['paid_through']   = null;
                    transactions($transaction_data);
                }

                $expenses_delete = Expenses::where('invoice_id', $id)->delete();
                if (!empty($data['expense_category_id']))
                {
                    foreach ($data['expense_category_id'] as $key => $value)
                    {
                        if ($value != 0)
                        {
                            $expense_find                   = Expenses::orderBy('created_at', 'DESC')->first();
                            $expense_number                 = $expense_find != null ? $expense_find['expense_number'] + 1 : 1;
                   
                            $expense                        = new Expenses;
                            $expense->expense_number        = $expense_number;
                            $expense->expense_date          = date('Y-m-d', strtotime($data['selling_date']));
                            $expense->customer_id           = $data['customer_id'];
                            $expense->invoice_id            = $invoice->id;
                            $expense->expense_category_id   = $value;
                            $expense->amount                = $data['expense_amount'][$key];
                            $expense->paid_through_id       = 1;
                            $expense->note                  = $data['expense_note'][$key];
                            $expense->created_by            = $user_id;

                            if ($expense->save())
                            {   
                                $account_transaction                      = new AccountTransactions;
                                $account_transaction->transaction_date    = date('Y-m-d', strtotime($data['selling_date']));
                                $account_transaction->amount              = $data['expense_amount'][$key];
                                $account_transaction->paid_through_id     = 1;
                                $account_transaction->note                = $data['expense_note'][$key];
                                $account_transaction->type                = 1;
                                $account_transaction->transaction_head    = 'expense';
                                $account_transaction->associated_id       = $expense->id;
                                $account_transaction->created_by          = $user_id;
                                $account_transaction->save();
                            }
                        }
                    }
                }

                $delete_current_balance = CurrentBalance::where('transaction_head', 'sales')
                                                ->where('associated_id', $id)
                                                ->delete();
                //Current balance 
                if (isset($data['current_balance_amount_paid']))
                {
                    for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                    {
                        if ($data['current_balance_amount_paid'][$i] > 0)
                        {
                            $current_balance[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['current_balance_amount_paid'][$i],
                                'paid_through_id'       => $data['current_balance_paid_through'][$i],
                                'account_information'   => $data['current_balance_account_information'][$i],
                                'note'                  => $data['current_balance_note'][$i] . '(বিক্রয় বাবদ আদায়)',
                                'type'                  => 0,  // 0 = In , 1 = Out
                                'transaction_head'      => 'sales',
                                'associated_id'         => $invoice->id,
                                'branch_id'             => $branch_id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];
                        }
                    }

                    if (isset($current_balance))
                    {
                        DB::table('current_balance')->insert($current_balance);
                    }
                }

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return redirect()->route('invoices_all_sales')->with("success","Sales Updated Successfully !!");
                }
                else
                {
                    $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                        ->select('invoices.*',
                                                 'customers.name as customer_name',
                                                 'customers.address as address',
                                                 'customers.phone as phone')
                                        ->find($invoice['id']);

                    $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->where('invoice_entries.invoice_id', $invoice['id'])
                                            ->select('invoice_entries.*',
                                                     'product_entries.product_type as product_type',
                                                     'product_entries.name as product_entry_name',
                                                     'units.name as unit_name',
                                                     'products.name as product_name')
                                            ->get();  
                                 
                    $user_info  = Users::find(1);
                    $expenses   = Expenses::where('invoice_id', $id)->get();

                    if ($user_info['pos_printer'] == 0)
                    {
                        return View::make('invoices::show_pos')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info)->with("expenses", $expenses);
                    }
                    else
                    {
                        return View::make('invoices::show')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info)->with("expenses", $expenses);
                    }
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function productList()
    {
        $data       = ProductEntries::where('product_entries.stock_in_hand', '>', 0)
                                    ->Where('product_entries.stock_in_hand', '!=', null)
                                    ->where('product_entries.status', 1)
                                    ->orderBy('product_entries.total_sold', 'DESC')
                                    ->select('product_entries.*')
                                    ->get();

        return Response::json($data);
    }

    public function makePayment($id)
    {
        $data       = Invoices::find($id);

        return Response::json($data);
    }

    public function storePayment(Request $request)
    {
        $user_id                    = Auth::user()->id;
        $data                       = $request->all();

        DB::beginTransaction();

        try{

            if (isset($data['amount_paid']))
            {
                $data_find        = Payments::orderBy('id', 'DESC')->first();
                $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                for($i = 0; $i < count($data['amount_paid']); $i++)
                {   
                    if ($data['amount_paid'][$i] > 0)
                    {
                        $payments = [
                            'payment_number'        => $payment_number,
                            'customer_id'           => $data['customer_id'],
                            'payment_date'          => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $data['amount_paid'][$i],
                            'paid_through'          => $data['paid_through'][$i],
                            'account_information'   => $data['account_information'][$i],
                            'note'                  => $data['note'][$i],
                            'type'                  => 0,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $payment_id = DB::table('payments')->insertGetId($payments);      

                        if ($payment_id)
                        {   
                            $payment_entries = [
                                    'payment_id'        => $payment_id,
                                    'invoice_id'        => $data['invoice_id'],
                                    'amount'            => $data['amount_paid'][$i],
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            DB::table('payment_entries')->insert($payment_entries);  
                        }

                        $update_invoice_dues                = Invoices::find($data['invoice_id']);
                        $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                        $update_invoice_dues->save();

                        $payment_number++;
                    }
                }
            }
            
            DB::commit();
            return Response::json(1);
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            dd($exception);
            return Response::json(0);
        }
    }

    public function productPriceList($id)
    {
        $branch_id                      = Auth()->user()->branch_id;
        $data['product_entries']        = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                    ->selectRaw('product_entries.*, units.name as unit_name')
                                                    ->find($id);

        $data['branch_product']         = BranchInventories::where('product_entry_id', $id)->where('branch_id', $branch_id)->first();
        $data['branch_id']              = Auth()->user()->branch_id;

        $data1                          = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                        ->where('product_entries.id', $id)
                                                        ->selectRaw('units.id as unit_id, units.name as unit_name')
                                                        ->get()
                                                        ->toArray();

        $data2                          = UnitConversions::leftjoin('units', 'units.id', 'unit_conversions.converted_unit_id')
                                                    ->where('unit_conversions.product_entry_id', $id)
                                                    ->selectRaw('unit_conversions.converted_unit_id as unit_id, units.name as unit_name')
                                                    ->get()
                                                    ->toArray();

        $data['unit_conversions']       = collect(array_merge($data1, $data2));

        return Response::json($data);
    }

    public function invoiceListLoad()
    {
        $branch_id      = Auth()->user()->branch_id;
        $data           = Invoices::leftjoin('sales_return', 'sales_return.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->where('invoices.type', 1)
                                    ->when($branch_id != 1, function ($query) use ($branch_id) {
                                        return $query->where('invoices.branch_id', $branch_id);
                                    })
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                            'sales_return.id as return_id',
                                            'customers.name as customer_name',
                                            'customers.phone as phone')
                                    ->distinct('invoices.id')
                                    ->take(20)
                                    ->get();

        return Response::json($data);
    }

    public function invoiceListSearch($from_date, $to_date, $customer, $invoice)
    {
        $branch_id              = Auth()->user()->branch_id;
        $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
        $search_by_customer     = $customer != 0 ? $customer : 0;
        $search_by_invoice      = $invoice != 0 ? $invoice : 0;

        $data           = Invoices::leftjoin('sales_return', 'sales_return.invoice_id', 'invoices.id')
                                        ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                        ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                        ->when($search_by_from_date != 0 && $search_by_to_date != 0, function ($query) use ($search_by_from_date, $search_by_to_date) {
                                            return $query->whereBetween('invoices.invoice_date', [$search_by_from_date, $search_by_to_date]);
                                        })
                                        ->when($search_by_customer != 0, function ($query) use ($search_by_customer) {
                                            return $query->where('customers.id', $search_by_customer);
                                        })
                                        ->when($search_by_invoice != 0, function ($query) use ($search_by_invoice) {
                                            return $query->where('invoices.id', $search_by_invoice);
                                        })
                                        ->where('invoices.type', 1)
                                        ->when($branch_id != 1, function ($query) use ($branch_id) {
                                            return $query->where('invoices.branch_id', $branch_id);
                                        })
                                        ->orderBy('invoices.created_at', 'DESC')
                                        ->select('invoices.*',
                                                 'sales_return.id as return_id',
                                                 'customers.name as customer_name',
                                                 'customers.phone as phone')
                                        ->distinct('invoices.id')
                                        ->take(100)
                                        ->get();

        return Response::json($data);
    }

    public function posSearchProduct($id)
    {
        $data           = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_code', $id)
                                    ->where('product_entries.stock_in_hand', '>', 0)
                                    ->where('product_entries.status', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->first();

        return Response::json($data);
    }

    public function customerStore(Request $request)
    {
        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        $customers                              = new Customers;
        $customers->name                        = $data['customer_name'];
        $customers->address                     = $data['address'];
        $customers->phone                       = $data['mobile_number'];
        $customers->contact_type                = $data['contact_type'];
        $customers->branch_id                   = $branch_id;
        $customers->created_by                  = $user_id;

        if ($customers->save())
        {   
            return Response::json($customers);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function customersListInvoice()
    {
        $branch_id      = Auth()->user()->branch_id;

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Customers::orderBy('customers.created_at', 'DESC')
                                    ->when($branch_id != 1, function ($query) use ($branch_id) {
                                        return $query->where('customers.branch_id', $branch_id);
                                    })
                                    ->select('customers.*')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Customers::where('customers.name', 'LIKE', "%$search%")
                                    ->orWhere('customers.phone', 'LIKE', "%$search%")
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->when($branch_id != 1, function ($query) use ($branch_id) {
                                        return $query->where('customers.branch_id', $branch_id);
                                    })
                                    ->select('customers.*')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "phone" =>'', "contact_type" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name'], "phone" =>$value['phone'], "contact_type" =>$value['contact_type']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function couponCode($id)
    {
        $today          = Carbon::today();

        $data           = Discounts::leftjoin('discount_products', 'discount_products.discount_id', 'discounts.id')
                                ->where('discounts.coupon_code', $id)
                                ->orWhere('discounts.card_number', $id)
                                ->whereDate('discounts.expire_date', '>=', $today->format('Y-m-d'))
                                ->where('discounts.status', 1)
                                ->select('discount_products.product_id as product_id', 
                                         'discounts.discount_type as discount_type',
                                         'discounts.discount_amount as discount_amount',
                                         'discounts.expire_date as expire_date'
                                        )
                                ->get();

        return Response::json($data);
    }

    public function printInvoicesList()
    {
        $user_id        = Auth::user()->id;
        $data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->where('invoices.created_by', $user_id)
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                            'customers.name as customer_name',
                                            'customers.phone as phone',
                                            'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function printInvoicesSearch($date,$customer,$invoice_number)
    {
        $search_by_date              = $date != 0 ? date('Y-m-d', strtotime($date)) : 0;
        $search_by_invoice_number    = $invoice_number != 0 ? $invoice_number : 0;
        $search_by_invoice_customer  = $customer != 0 ? $customer : 0;
        $user_info                   = userDetails();

        $data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->when($search_by_date != 0, function ($query) use ($search_by_date) {
                                        return $query->where('invoices.invoice_date', 'LIKE', "%$search_by_date%");
                                    })
                                    ->when($search_by_invoice_number != 0, function ($query) use ($search_by_invoice_number) {
                                        return $query->where('invoices.invoice_number', 'LIKE', "%$search_by_invoice_number%");
                                    })
                                    ->when($search_by_invoice_customer != 0, function ($query) use ($search_by_invoice_customer) {
                                        return $query->where('customers.name', 'LIKE', "%$search_by_invoice_customer%");
                                    })
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.phone as phone',
                                             'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function calculateOpeningBalance($customer_id)
    {
        $invoices               = Invoices::where('invoices.customer_id', $customer_id)
                                            ->select('invoices.*')
                                            ->get();

        $customer_advance       = Customers::find($customer_id);

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->leftjoin('sales_return', 'sales_return.id', 'payment_entries.sales_return_id')
                                                ->where('payments.customer_id', $customer_id)
                                                ->where('payments.type', 2)
                                                ->select('payment_entries.*',
                                                         'payments.customer_id as customer_id')
                                                ->get();

        $invoice_amount         = $invoices->sum('invoice_amount');
        $return_amount          = $invoices->sum('return_amount');
        $paid_amount            = $invoice_amount - $invoices->sum('due_amount');
        $return_paid_amount     = $payment_entries_return->sum('amount');

        $due                    = $invoice_amount - $paid_amount - $return_paid_amount;
        $advance                = $customer_advance['customer_advance_payment'];

        if ($due > 0)
        {
            $data['balance']   = $due;
            $data['type']      = 1; //1 = previous dues
        }

        if ($advance > 0)
        {
            $data['balance']   = $advance;
            $data['type']      = 2; //2 = advance amount
        }

        if (($due == 0) && ($advance == 0))
        {
            $data['balance']   = 0;
            $data['type']      = 1; //1 = previous dues
        }

        return Response::json($data);
    }

    public function productListLoadInvoice()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('product_entries.status', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('product_entries.name', 'LIKE', "%$search%")
                                    ->where('product_entries.status', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->take(100)
                                    ->get();
        }

        foreach ($fetchData as $key => $value)
        {   
            if ($value['product_type'] == 2)
            {
                $variations  = ' - ' . $value['variations'];
            }
            else
            {
                $variations  = '';
            }

            if ($value['brand_name'] != null)
            {
                $brand  = ' - '.$value['brand_name'];
            }
            else
            {
                $brand  = '';
            }

            if ($value['product_code'] != null)
            {
                $code  = $value['product_code'];
            }
            else
            {
                $code  = '';
            }

            $name   = $value['name'] . $variations . ' ' . '( ' . $code . $brand . ' )';

            $data[] = array("id"=>$value['id'], "text"=>$name);
        }

        return Response::json($data);
    }

    public function expenseCategories()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = ExpenseCategories::whereNotIn('expense_categories.id', [1,2])->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = ExpenseCategories::where('expense_categories.name', 'LIKE', "%$search%")
                                            ->whereNotIn('expense_categories.id', [1,2])
                                            ->get();
        }

        foreach ($fetchData as $key => $value)
        {   
            $data[] = array("id"=>$value['id'], "text"=>$value['name']);
        }

        return Response::json($data);
    }

    public function adjustAdvancePayment($customer_id)
    {
        $data  = Customers::select('customer_advance_payment')->where('id', $customer_id)->first();

        return Response::json($data['customer_advance_payment']);
    }
}
