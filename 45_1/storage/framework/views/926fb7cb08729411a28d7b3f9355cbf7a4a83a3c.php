

<?php $__env->startSection('title', 'Stock Details Report'); ?>

<style type="text/css">
    @media  print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page  {
            margin: 0cm ! important;
        }

        ::-webkit-scrollbar {
            display: none;
        }
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Stock Details Report</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Stock Details Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="line-height: 0px;text-align: center"><?php echo e($user_info['address']); ?></p>
                                        <p style="line-height: 0px;text-align: center"><?php echo e($user_info['contact_number']); ?></p>
                                        <h4 style="line-height: 10px;text-align: center">Stock Details Report</h4>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="<?php echo e(route('stock_report_details', $product['id'])); ?>" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <div class="input-daterange input-group">
                                                <h5>Product Category Name : <?php echo e($product['name']); ?></h5>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="product_id">
                                                    <option value="0" selected>-- All Products --</option>
                                                    <?php if(!empty($products) && ($products->count() > 0)): ?>
                                                    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option <?php echo e(isset($_GET['product_id']) && ($_GET['product_id'] == $value['id']) ? 'selected' : ''); ?> value="<?php echo e($value['id']); ?>"><?php echo e($value['name']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                        </div>

                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 d-print-none">
                                            <div class="float-right">
                                                <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                                <!-- <a href="#" class="btn btn-primary w-md waves-effect waves-light">Send</a> -->
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Product Name</th>
                                            <th>Product Code</th>
                                            <th style="text-align: right">Stock In Hand</th>
                                            <th style="text-align: right">Unit</th>
                                            <th style="text-align: right">Sold</th>
                                            <th style="text-align: right">Stock Purchase Value</th>
                                            <th style="text-align: right">Stock Sell Value</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <?php if(!empty($data) && ($data->count() > 0)): ?>
                                        <?php
                                            $total_stock_in_hand        = 0;
                                            $total_stock_sell_value     = 0;
                                            $total_stock_purchase_value = 0;
                                        ?>
                                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                            <?php
                                                $total_stock_in_hand        = $total_stock_in_hand + $value['stock_in_hand'];
                                                $stock_sell_value           = $value['stock_in_hand']*$value['sell_price'];
                                                $stock_purchase_value       = $value['stock_in_hand']*$value['buy_price'];
                                                $total_stock_sell_value     = $total_stock_sell_value + $stock_sell_value;
                                                $total_stock_purchase_value = $total_stock_sell_value + $stock_purchase_value;
                                            ?>

                                            <tr>
                                                <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e($value['name']); ?></td>
                                                <td><?php echo e(str_pad($value['product_code'], 6, "0", STR_PAD_LEFT)); ?></td>
                                                <td style="text-align: right"><?php echo e($value['stock_in_hand']); ?></td>
                                                <td style="text-align: right"><?php echo e($value['unit_name']); ?></td>
                                                <td style="text-align: right"><?php echo e($value['total_sold']); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($stock_purchase_value,2,'.',',')); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($stock_sell_value,2,'.',',')); ?></td>
                                            </tr>
                                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>

                                    <?php if(isset($total_stock_in_hand)): ?>
                                        <tr>
                                            <th style="text-align: right" colspan="6">Total</th>
                                            <th style="text-align: right"><?php echo e(number_format($total_stock_purchase_value,2,'.',',')); ?></th>
                                            <th style="text-align: right"><?php echo e(number_format($total_stock_sell_value,2,'.',',')); ?></th>
                                        </tr>
                                    <?php endif; ?>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/45_1/Modules/Reports/Resources/views/stock_details_report.blade.php ENDPATH**/ ?>