<!DOCTYPE html>
<html>

<head>
    <title>Salary Sheet</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>   
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p><?php echo e($user_info['address']); ?></p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Salary Sheet</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Month of salary sheet</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <?php
                                        if ($_GET['month'] == 1)
                                        {
                                            $month  = 'January';
                                        }
                                        elseif ($_GET['month'] == 1)
                                        {
                                            $month  = 'February';
                                        }
                                        elseif ($_GET['month'] == 3)
                                        {
                                            $month  = 'March';
                                        }
                                        elseif ($_GET['month'] == 4)
                                        {
                                            $month  = 'April';
                                        }
                                        elseif ($_GET['month'] == 5)
                                        {
                                            $month  = 'May';
                                        }
                                        elseif ($_GET['month'] == 6)
                                        {
                                            $month  = 'Jun';
                                        }
                                        elseif ($_GET['month'] == 7)
                                        {
                                            $month  = 'July';
                                        }
                                        elseif ($_GET['month'] == 8)
                                        {
                                            $month  = 'August';
                                        }
                                        elseif ($_GET['month'] == 9)
                                        {
                                            $month  = 'September';
                                        }
                                        elseif ($_GET['month'] == 10)
                                        {
                                            $month  = 'October';
                                        }
                                        elseif ($_GET['month'] == 11)
                                        {
                                            $month  = 'November';
                                        }
                                        elseif ($_GET['month'] == 12)
                                        {
                                            $month  = 'December';
                                        }
                                    ?>
                                    <td style="text-align: center"><?php echo e($month . ', ' . $_GET['year']); ?></td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 15%">EMPLOYEE NAME</th>
                                    <th style="text-align: center;width: 10%">GRADE</th>
                                    <th style="text-align: center;width: 10%">BASIC</th>
                                    <th style="text-align: center;width: 10%">HOUSE RENT</th>
                                    <th style="text-align: center;width: 10%">MEDICAL</th>
                                    <th style="text-align: center;width: 10%">CONVINCE</th>
                                    <th style="text-align: center;width: 10%">FOOD</th>
                                    <th style="text-align: center;width: 10%">MOBILE</th>
                                    <th style="text-align: center;width: 10%">OTHERS</th>
                                    <th style="text-align: center;width: 10%">GROSS</th>
                                    <th style="text-align: center;width: 10%">Attendence</th>
                                    <th style="text-align: center;width: 10%">Net Payable</th>

                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php $i = 1; $total = 0;$totalNetPayable =0; ?>
                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td style="text-align: center;"><?php echo e($i); ?></td>
                                    <td style="text-align: left;"><?php echo e($value->salaryStatements->employee->name); ?></td>
                                    <td style="text-align: center;"><?php echo e($value->salaryStatements->grade->name); ?></td>
                                    <td style="text-align: right;"><?php echo e($value->salaryStatements->basic); ?></td>
                                    <td style="text-align: right;"><?php echo e($value->salaryStatements->house_rent); ?></td>
                                    <td style="text-align: right;"><?php echo e($value->salaryStatements->medical); ?></td>
                                    <td style="text-align: right;"><?php echo e($value->salaryStatements->convence); ?></td>
                                    <td style="text-align: right;"><?php echo e($value->salaryStatements->food); ?></td>
                                    <td style="text-align: right;"><?php echo e($value->salaryStatements->mobile); ?></td>
                                    <td style="text-align: right;"><?php echo e($value->salaryStatements->others); ?></td>
                                    <td style="text-align: right;"><?php echo e($value->salaryStatements->gross); ?></td>
                                    <td style="text-align: right;"><?php echo e($value->total_attendance); ?> days</td>
                                    <td style="text-align: right;">
                                        <?php echo e(round(($value->salaryStatements->gross / 30) * $value->total_attendance)); ?>

                                    </td>
                                </tr>
                                <?php
                                 $i++; 
                                 $total = $total + $value->salaryStatements->gross; 
                                 $totalNetPayable = $totalNetPayable + (($value->salaryStatements->gross / 30) * $value->total_attendance); 
                                 ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                <tr>
                                    <td style="text-align: right" colspan="10"><strong>TOTAL</strong></td>
                                    <td style="text-align: right"><strong><?php echo e($total); ?></strong></td>
                                    <td style="text-align: right"></td>
                                    <td style="text-align: right"><?php echo e($totalNetPayable); ?></td>
                                </tr>
                            </tbody>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/dollar-enterprise/rice-mill/Modules/Reports/Resources/views/monthly_salary_report_print.blade.php ENDPATH**/ ?>