

<?php $__env->startSection('title', 'Make Salary Sheet'); ?>

<style type="text/css">
    .table td, .table th {
        padding: 0px !important;
    }

    .button-items .btn {
        margin-bottom: 5px;
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Make Salary Sheet</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Payroll</a></li>
                                    <li class="breadcrumb-item active">Make Salary Sheet</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <form id="FormSubmit" action="<?php echo e(route('process_monthly_salary_store')); ?>" method="post" files="true" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>


                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div style="padding: 0.5rem !important" class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Select Month *</label>
                                        <select style="cursor: pointer" id="month" class="form-control select2" name="month">
                                            <option value="1" <?php echo e(date('m') == '01' ? 'selected' : ''); ?>>January</option>
                                            <option value="2" <?php echo e(date('m') == '02' ? 'selected' : ''); ?>>February</option>
                                            <option value="3" <?php echo e(date('m') == '03' ? 'selected' : ''); ?>>March</option>
                                            <option value="4" <?php echo e(date('m') == '04' ? 'selected' : ''); ?>>Appril</option>
                                            <option value="5" <?php echo e(date('m') == '05' ? 'selected' : ''); ?>>May</option>
                                            <option value="6" <?php echo e(date('m') == '06' ? 'selected' : ''); ?>>Jun</option>
                                            <option value="7" <?php echo e(date('m') == '07' ? 'selected' : ''); ?>>July</option>
                                            <option value="8" <?php echo e(date('m') == '08' ? 'selected' : ''); ?>>August</option>
                                            <option value="9" <?php echo e(date('m') == '09' ? 'selected' : ''); ?>>September</option>
                                            <option value="10" <?php echo e(date('m') == '10' ? 'selected' : ''); ?>>October</option>
                                            <option value="11" <?php echo e(date('m') == '11' ? 'selected' : ''); ?>>November</option>
                                            <option value="12" <?php echo e(date('m') == '12' ? 'selected' : ''); ?>>December</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Select Year *</label>
                                        <select style="cursor: pointer" id="year" class="form-control select2" name="year">
                                            <option value="2018" <?php echo e(date('Y') == '2018' ? 'selected' : ''); ?>>2018</option>
                                            <option value="2019" <?php echo e(date('Y') == '2019' ? 'selected' : ''); ?>>2019</option>
                                            <option value="2021" <?php echo e(date('Y') == '2021' ? 'selected' : ''); ?>>2021</option>
                                            <option value="2022" <?php echo e(date('Y') == '2022' ? 'selected' : ''); ?>>2022</option>
                                            <option value="2023" <?php echo e(date('Y') == '2023' ? 'selected' : ''); ?>>2023</option>
                                            <option value="2024" <?php echo e(date('Y') == '2024' ? 'selected' : ''); ?>>2024</option>
                                            <option value="2025" <?php echo e(date('Y') == '2025' ? 'selected' : ''); ?>>2025</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
 
                    <div style="padding-left: 15px" class="form-check form-check-right">
                        <button style="border-radius: 0px;padding: 0.3rem .5rem !important" type="submit" class="btn btn-primary waves-effect waves-light col-md-12">Generate Salary Sheet</button>
                    </div>


                    <div style="padding-top: 10px" class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <table id="contactList" class="table table-striped">

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url    = $(".site_url").val();

            $.get(site_url + '/payroll/process-monthly-salary/monthly-salary-data/', function(data){

                var list    = '';
                    list    += '<tr>' +
                                '<th id="select-all" style="width: 4%" class="active">' +
                                    '<input id="selectAll" type="checkbox" class="select-all" onclick="checkVal()" />' +
                                '</th>' +
                                '<th style="width: 6%" class="success">' + 'SL' + '</th>' +
                                '<th style="width: 15%" class="success">' + 'Employee Name' + '</th>' +
                                '<th style="width: 15%" class="success">' + 'Salary Grade' + '</th>' +

                                '<th style="width: 15%;text-align: center" class="success">' + 'Gross Salary' + '</th>' + 
                                '<th style="width: 15%" class="success">' + 'Attendance' + '</th>' +  
                            '</tr>';

                var serial  = 1;
                $.each(data, function(i, data_list)
                {   
                    list += '<tr>' +
                                '<td class="active">' +
                                    '<input id="checkbox_'+i+'" type="checkbox" class="select-item checkbox" onclick="checkValUnik('+i+')" />' +
                                    '<input id="check_status_'+i+'" type="hidden" name="check_box_id[]" class="checkboxId" value="0" />' +
                                    '<input type="hidden" name="employee_id[]" value="'+ data_list.employee_id +'" />' +
                                    '<input type="hidden" name="statement_id[]" value="'+ data_list.id +'" />' +
                                '</td>' +
                                '<td class="success">' + serial + '</td>' +
                                '<td class="success">' + data_list.employee_name + '</td>' +
                                '<td class="success">' + data_list.grade_name + '</td>' +
                                '<td style="text-align: center" class="success">' + data_list.gross + '</td>' +
                                '<td class="success">' + '<input name="total_attendance[]" class="form-controll"  />' + '</td>' +
                            '</tr>';
                    serial++;
                });

                $("#contactList").empty();
                $("#contactList").append(list);

            });

        });
    </script>

    <script type="text/javascript">
        function checkVal()
        {
            if ($('#selectAll').is(":checked"))
            {
                $('.checkbox').prop('checked', true);
                $('.checkboxId').val(1);
            }
            else
            {
                $('.checkbox').prop('checked', false);
                $('.checkboxId').val(0);
            }
        }

        function checkValUnik(i)
        {
            if ($('#checkbox_'+i).is(":checked") == true)
            {
                $('#check_status_'+i).val(1);
            }
            else
            {
                $('#check_status_'+i).val(0);
            }
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/dollar-enterprise/rice-mill/Modules/Payroll/Resources/views/ProcessMonthlySalary/create.blade.php ENDPATH**/ ?>