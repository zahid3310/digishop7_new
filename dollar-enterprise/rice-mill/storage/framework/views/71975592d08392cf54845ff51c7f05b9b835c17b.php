

<?php $__env->startSection('title', 'Print Checks'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Print Checks </h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Accounts</a></li>
                                    <li class="breadcrumb-item active">Print Checks</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('balance_transfer_print_check_store')); ?>" method="post" files="true" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>


                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Date *</label>
                                        <input type="text" style="width: .90cm;height: .8cm;padding-left: 10px" placeholder="D" maxlength="1" name="d1">
                                        <input type="text" style="width: .90cm;height: .8cm;padding-left: 10px" placeholder="D" maxlength="1" name="d2">
                                        <input type="text" style="width: .90cm;height: .8cm;padding-left: 10px" placeholder="M" maxlength="1" name="m1">
                                        <input type="text" style="width: .90cm;height: .8cm;padding-left: 10px" placeholder="M" maxlength="1" name="m2">
                                        <input type="text" style="width: .90cm;height: .8cm;padding-left: 10px" placeholder="Y" maxlength="1" name="y1">
                                        <input type="text" style="width: .90cm;height: .8cm;padding-left: 10px" placeholder="Y" maxlength="1" name="y2">
                                        <input type="text" style="width: .90cm;height: .8cm;padding-left: 10px" placeholder="Y" maxlength="1" name="y3">
                                        <input type="text" style="width: .90cm;height: .8cm;padding-left: 10px" placeholder="Y" maxlength="1" name="y4">
                                    </div>

                                    <div class="col-lg-5 col-md-5 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Pay To *</label>
                                        <input type="text" name="pay_to" class="inner form-control" id="pay_to" placeholder="Name of Bearer" required />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Amount *</label>
                                        <input type="text" name="amount" class="inner form-control" id="amount" placeholder="Amount" required />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('balance_transfer_print_check_index')); ?>">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Checks</h4>

                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Date</th>
                                            <th>Pay To</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php if(!empty($checks) && ($checks->count() > 0)): ?>
                                        <?php $__currentLoopData = $checks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $check): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e($check['d1'].$check['d2'].'-'.$check['m1'].$check['m2'].'-'.$check['y1'].$check['y2'].$check['y3'].$check['y4']); ?></td>
                                                <td><?php echo e($check->pay_to); ?></td>
                                                <td><?php echo e($check->amount); ?></td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="<?php echo e(route('balance_transfer_print_check_show', $check['id'])); ?>" target="_blank">print</a>
                                                            <a class="dropdown-item" href="<?php echo e(route('balance_transfer_print_check_edit', $check['id'])); ?>">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/dollar-enterprise/rice-mill/Modules/BalanceTransfer/Resources/views/checks/index.blade.php ENDPATH**/ ?>