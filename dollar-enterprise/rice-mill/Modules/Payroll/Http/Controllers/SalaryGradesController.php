<?php

namespace Modules\Payroll\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\SalaryGrades;
use DB;
use Response;

class SalaryGradesController extends Controller
{
    public function index()
    {   
        $salary_grades = SalaryGrades::orderBy('position', 'ASC')->get();

        return view('payroll::SalaryGrades.index', compact('salary_grades'));
    }

    public function create()
    {   
        return view('payroll::SalaryGrades.create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'          => 'required|string',
            'position'      => 'nullable|integer',
            'basic_salary'  => 'nullable|numeric',
            'house_rent'    => 'nullable|numeric',
            'medical'       => 'nullable|numeric',
            'convence'      => 'nullable|numeric',
            'food'          => 'nullable|numeric',
            'mobile_bill'   => 'nullable|numeric',
            'others'        => 'nullable|numeric',
            'gross'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $grades                = new SalaryGrades;
            $grades->name          = $data['name'];
            $grades->position      = $data['position'];
            $grades->basic         = $data['basic_salary'];
            $grades->house_rent    = $data['house_rent'];
            $grades->medical       = $data['medical'];
            $grades->convence      = $data['convence'];
            $grades->food          = $data['food'];
            $grades->mobile        = $data['mobile_bill'];
            $grades->others        = $data['others'];
            $grades->gross         = $data['gross'];
            $grades->created_by    = $user_id;

            if ($grades->save())
            {   
                DB::commit();
                return back()->with("success","Grade Added Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        return view('payroll::show');
    }

    public function edit($id)
    {   
        $find_grade = SalaryGrades::find($id);

        return view('payroll::SalaryGrades.edit', compact('find_grade'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required|string',
            'position'      => 'nullable|integer',
            'basic_salary'  => 'nullable|numeric',
            'house_rent'    => 'nullable|numeric',
            'medical'       => 'nullable|numeric',
            'convence'      => 'nullable|numeric',
            'food'          => 'nullable|numeric',
            'mobile_bill'   => 'nullable|numeric',
            'others'        => 'nullable|numeric',
            'gross'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $grades                = SalaryGrades::find($id);
            $grades->name          = $data['name'];
            $grades->position      = $data['position'];
            $grades->basic         = $data['basic_salary'];
            $grades->house_rent    = $data['house_rent'];
            $grades->medical       = $data['medical'];
            $grades->convence      = $data['convence'];
            $grades->food          = $data['food'];
            $grades->mobile        = $data['mobile_bill'];
            $grades->others        = $data['others'];
            $grades->gross         = $data['gross'];
            $grades->updated_by    = $user_id;

            if ($grades->save())
            {   
                DB::commit();
                return redirect()->route('salary_grades_index')->with("success","Grade Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }
}
