<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="{{ route('home') }}" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>Reports</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li class="">
                            <a class="has-arrow waves-effect">
                                Sales
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('sales_statement_index') }}">Statement of Sales</a> </li>
                                <li> <a href="{{ route('sales_summary_index') }}">Sales Summary</a> </li>
                                <!-- <li> <a href="{{ route('product_wise_sales_report_index') }}">Product Wise Sales</a> </li> -->
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Purchase
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('purchase_statement_index') }}">Statement of Pur.</a> </li>
                                <li> <a href="{{ route('purchase_summary_index') }}">Purchase Summary</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Accounts
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('expense_report_index') }}">List of Expense</a> </li>
                                <li> <a href="{{ route('income_report_index') }}">List of Income</a> </li>
                                <li> <a href="{{ route('income_statement_index') }}">Income Statement</a> </li>
                                <!-- <li> <a href="{{ route('collection_report_index') }}">Daily Report</a> </li> -->
                            </ul>
                        </li>

                        <!-- <li class="">
                            <a class="has-arrow waves-effect">
                                Payments
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('customer_payment_report_index') }}">Customer Payments</a> </li>
                                <li> <a href="{{ route('supplier_payment_report_index') }}">Supplier Payments</a> </li>
                            </ul>
                        </li> -->

                        <li class="">
                            <a class="has-arrow waves-effect">
                                MIS
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('current_balance_index') }}">Current Balance</a> </li>
                                <li> <a href="{{ route('stock_report_index') }}">Stock Status</a> </li>
                                <li> <a href="{{ route('due_report_customer_index') }}">Buyer Ledger</a> </li>
                                <li> <a href="{{ route('due_report_supplier_index') }}">Supplier Ledger</a> </li>
                                <li> <a href="{{ route('due_ledger_index') }}">Due Ledger</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Basic Report
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('item_list_index') }}">List of Items</a> </li>
                                <li> <a href="{{ route('emergency_item_list_index') }}">Emergency Purchase</a> </li>
                                <li> <a href="{{ route('product_suppliers_index') }}">Item Wise Supplier</a> </li>
                                <li> <a href="{{ route('product_customers_index') }}">Item Wise Customer</a> </li>
                                <!-- <li> <a href="{{ route('register_list_index').'?type=0' }}" target="_blank">Buyer List</a> </li> -->
                                <!-- <li> <a href="{{ route('register_list_index').'?type=1' }}" target="_blank">Supplier List</a> </li> -->
                            </ul>
                        </li>
                        
                        <!-- <li> <a href="#">List of Sending SMS</a> </li>
                        <li> <a href="{{ route('salary_report_index') }}">Salary Report</a> </li> -->

                    </ul>
                </li>

                <li class="{{ 
                Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>Basic Settings</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">

                        <li>
                            <a class="has-arrow waves-effect">
                                Security System
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('users_index') }}">Add User</a> </li>
                                <li> <a href="{{ route('users_index_all') }}">List of User</a> </li>
                                <li> <a href="{{ route('set_access_index') }}">Set Permission</a> </li>
                            </ul>
                        </li>
                        
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>