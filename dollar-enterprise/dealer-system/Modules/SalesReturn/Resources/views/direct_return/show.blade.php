@extends('layouts.app')

@section('title', 'Show')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Direct Return</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Direct Return</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if($user_info['header_image'] == null)
                                        <div class="col-md-12 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">{{ $user_info['organization_name'] }}</h2>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_number'] }}</p>
                                        </div>
                                        <div class="col-md-12 col-xs-12 col-sm-12"></div>
                                    @else
                                        <img class="float-left" src="{{ url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-4 col-6">
                                        <address>
                                            <strong>Return By:</strong><br>
                                            {{ $sales_return->customer_id != null ? $sales_return->customer->name : '' }}<br> 
                                            {{ $sales_return->customer_id != null ? $sales_return->customer->address : '' }} <br>
                                            {{ $sales_return->customer_id != null ? $sales_return->customer->phone : '' }}
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6 hidden-xs">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Memo No : {{ $sales_return['return_number'] }}</h3>
                                        </div>
                                        
                                        <div class="col-md-6 hidden-xs">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Date: {{ date('d-m-Y', strtotime($sales_return['return_date'])) }}</h3>
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered" style="width: 100%">
                                        <tr>
                                            <th style="font-size: 18px;width: 5%;text-align: center">নং</th>
                                            <th style="font-size: 18px;width: 60%;text-align: center">মালের বিবরণ</th>
                                            <th style="font-size: 18px;width: 15%;text-align: center">পরিমান</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center">দর</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center">টাকা</th>
                                        </tr>
                    
                                        @if($sales_return->directReturnEntries->count() > 0)
                    
                                        <?php
                                            $total_amount   = 0;
                                        ?>
                    
                                        @foreach($sales_return->directReturnEntries as $key => $value)
                                        <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $variation_name = ProductVariationName($value['product_entry_id']);
                    
                                            if ($value->productEntries->product_code != null)
                                            {
                                                $productCode  = ' - '.$value->productEntries->product_code;
                                            }
                                            else
                                            {
                                                $productCode  = '';
                                            }
                    
                                            if ($value->productEntries->name != null)
                                            {
                                                $category  = ' - '.$value->productEntries->name;
                                            }
                                            else
                                            {
                                                $category  = '';
                                            }
                    
                                           
                    
                                            
                    
                                            if ($value->productEntries->unit_id != null)
                                            {
                                                $unit  = ' '.$value->productEntries->unit->name;
                                            }
                                            else
                                            {
                                                $unit  = ' SFT';
                                            }
                    
                                            if ($variation_name != null)
                                            {
                                                $variation  = ' '.$variation_name;
                                            }
                                            else
                                            {
                                                $variation  = '';
                                            }
                                        ?>
                    
                                        <tr class="tr-height">
                                            <td style="text-align: center">{{ $key + 1 }}</td>
                                            <td style="padding-left: 30px">{{ $productCode . $category . ' - ' . $value->productEntries->name}}</td>
                                            <td style="text-align: center">{{ $value['quantity'] . $unit }}</td>
                                            <td style="text-align: center">{{ $value['rate'] }}</td>
                                            <td style="text-align: center">{{ round($value['total_amount'], 2) }}</td>
                                        </tr>
                                        @endforeach
                                        @endif
                    
                                        <tr>
                                            <th style="text-align: left" colspan="2">In Words : <span style="font-weight: normal;">{{ numberTowords($total_amount) }}</span></th>
                                            <th></th>
                                            <th style="text-align: right"><strong>Total</strong></th>
                                            <th style="text-align: center">{{ round($total_amount, 2) }}</th>
                                        </tr>
                                    </table>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="position: fixed;bottom: 0">
                                    @if($user_info['footer_image'] != null)
                                        <img class="float-left" src="{{ url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection