

<?php
    if (isset($_GET['type']) && ($_GET['type'] == 0))
    {
        $title = 'Sales Return Payment';
    }

    if (isset($_GET['type']) && ($_GET['type'] == 1))
    {
        $title = 'Purchase Return Payment';
    }
?>

<?php $__env->startSection('title', $title); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Return Payment</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Return Payment</a></li>
                                    <li class="breadcrumb-item active"><?php echo e($title); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <form id="FormSubmit" action="<?php echo e(route('sales_return_payment_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                <?php echo e(csrf_field()); ?>


                <div class="row">
                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
                                            <label class="control-label">Contact Type *</label>
                                            <select id="type" style="width: 100%;cursor: pointer" class="form-control" name="type">
                                                <?php if(isset($_GET['type']) && ($_GET['type'] == 1)): ?>
                                                <option value="1">Supplier</option>
                                                <?php endif; ?>
                                                <?php if(isset($_GET['type']) && ($_GET['type'] == 0)): ?>
                                                <option value="0">Customer</option>
                                                <?php endif; ?>
                                                
                                                <?php if(!isset($_GET['type'])): ?>
                                                <option value="0">Customers</option>
                                                <option value="1">Supplier</option>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                    	<div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
	                                        <label class="control-label">Search Contact *</label>
	                                        <select id="contact_id" name="customer_id" style="width: 100%;cursor: pointer" class="form-control select2 contact_id" onchange="searchContact()">
	                                        	<option value="">--Select Contact--</option>
												<?php if(!empty($customers)): ?>
                                                    <?php 
                                                        if (isset($_GET['type']))
                                                        {
                                                            $customers = $customers->where('contact_type', $_GET['type']);
                                                        }
                                                        else
                                                        {
                                                            $customers = $customers;
                                                        }
                                                    ?>
													<?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<option <?php if(isset($find_customer)): ?> <?php echo e($find_customer['id'] == $customer['id'] ? 'selected' : ''); ?> <?php endif; ?> value="<?php echo e($customer->id); ?>"><?php echo e($customer->name); ?> <?php echo e($customer->phone != null ? ' | ' . $customer->phone : ''); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												<?php endif; ?>
	                                        </select>
	                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

        						<div class="row">
    			                	<div class="col-sm-6">
    			                        <div class="form-group">
    			                            <label for="payment_date">Payment Date *</label>
    			                            <input id="payment_date" name="payment_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
    			                        </div>
    			                    </div>

    			                    <div class="col-sm-6">
    			                        <div class="form-group">
    			                            <label for="amount">Amount *</label>
    			                            <input id="amount" name="amount" type="text" class="form-control">
    			                        </div>
    			                    </div>

    			                    <div class="col-sm-6 form-group">
    		                            <label class="control-label">Paid Through</label>
                                        <select style="cursor: pointer" name="paid_through" class="form-control select2">
                                            <?php if(!empty($paid_accounts) && ($paid_accounts->count() > 0)): ?>
                                            <?php $__currentLoopData = $paid_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $paid_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($paid_account['id']); ?>"><?php echo e($paid_account['name']); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
    		                        </div>

    		                        <div class="col-sm-6">
    			                        <div class="form-group">
    			                            <label for="note">Note</label>
    			                            <input id="note" name="note" type="text" class="form-control">
    			                        </div>
    			                    </div>
    		                	</div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('sales_return_payment_create')); ?>">Close</a></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </form>

                <hr>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title"><?php echo e($title . ' '); ?> List</h4>

                                <div style="margin-right: 10px" class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-1">Search : </div>
                                    <div class="col-md-2">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th>Contact</th>
                                            <th>Paid Through</th>
                                            <th>Note</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="payment_list"></tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Are you sure want to delete the payment ?</label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="delete_btn" class="btn btn-primary waves-effect waves-light delete_btn">Delete</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url    = $('.site_url').val();
            var contact_id  = $('#contact_id').val();

            if (contact_id == '')
            {
                var contact_id_find = 0;
            }
            else
            {
                var contact_id_find = $('#contact_id').val();
            }

            $.get(site_url + '/payments/sales-return-payment/get-sales-return-payment-data/' + contact_id_find, function(data){

                paymentList(data);
                
            });
        });

        function searchContact()
        {
            var site_url    = $('.site_url').val();
            var contact_id  = $('#contact_id').val();

            $.get(site_url + '/payments/settlements/get-balance/' + contact_id, function(data){

                $("#amount").val(data.balance);
            });

            $.get(site_url + '/payments/sales-return-payment/get-sales-return-payment-data/' + contact_id, function(data){

                paymentList(data);
            });
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }
    </script>

    <script type="text/javascript">
        $('.delete_btn').click(function () {
            var site_url            = $('.site_url').val();
            var id                  = $('.paymentId').val();
            window.location.href    = site_url + "/payments/sales-return-payment/delete/"+id;
        })
    </script>

    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }
    </script>

    <script type="text/javascript">
        function paymentList(data)
        {
            var payment_list     = '';
            var site_url            = $('.site_url').val();
            $.each(data, function(i, payment_data)
            {
                var serial      = parseFloat(i) + 1;

                if (payment_data.note == null)
                {
                    var note = '';
                }
                else
                {
                    var note = payment_data.note;
                }

                var locationValue   = (new URL(location.href)).searchParams.get('type');

                if (payment_data.type == locationValue)
                {
                    payment_list += '<tr>' +
                                        '<input class="form-control paymentId" type="hidden" name="settlement_id[]" value="' +  payment_data.id + '">' +
                                        '<td style="text-align: left">' +
                                            serial +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            formatDate(payment_data.date) +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           payment_data.customer_name +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           payment_data.paid_through_account_name +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           note +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           payment_data.amount +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                }
            });

            $("#payment_list").empty();
            $("#payment_list").append(payment_list);
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/dollar-enterprise/dealer-system/Modules/Payments/Resources/views/sales_return_payment_index.blade.php ENDPATH**/ ?>