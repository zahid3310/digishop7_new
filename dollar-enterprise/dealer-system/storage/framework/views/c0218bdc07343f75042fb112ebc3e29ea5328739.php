

<?php $__env->startSection('title', 'Show'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Purchase Return</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Purchase Return</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <?php if($user_info['header_image'] == null): ?>
                                        <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-4 col-xs-12 col-sm-12">
                                            <h2 style="text-align: center"><?php echo e($user_info['organization_name']); ?></h2>
                                            <p style="line-height: 0px;text-align: center"><?php echo e($user_info['address']); ?></p>
                                            <p style="line-height: 0px;text-align: center"><?php echo e($user_info['contact_number']); ?></p>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    <?php else: ?>
                                        <img class="float-left" src="<?php echo e(url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image'])); ?>" alt="logo" style="width: 100%" />
                                    <?php endif; ?>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <address>
                                            <strong>Return To:</strong><br>
                                            <?php echo e($purchase_return['customer_name']); ?>

                                            <?php if($purchase_return['address'] != null): ?>
                                               <br> <?php echo $purchase_return['address']; ?> <br>
                                            <?php endif; ?>
                                            <?php if($purchase_return['address'] == null): ?>
                                                <br>
                                            <?php endif; ?>
                                            <?php echo e($purchase_return['phone']); ?>

                                        </address>
                                    </div>

                                    <div class="col-sm-4">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div class="col-sm-4 text-sm-right">
                                        <address class="mt-2 mt-sm-0">
                                            <strong>Return Date:</strong><br>
                                            <?php echo e(date('d-m-Y', strtotime($purchase_return['purchase_return_date']))); ?><br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h3 class="font-size-15 font-weight-bold">Return summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 class="font-size-15 font-weight-bold float-right">Return # <?php echo e('PR - ' . str_pad($purchase_return['purchase_return_number'], 6, "0", STR_PAD_LEFT)); ?></h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-nowrap">
                                        <thead>
                                            <tr>
                                                <th style="width: 70px;">No.</th>
                                                <th>Item</th>
                                                <th class="text-right">Rate</th>
                                                <th class="text-right">Qty</th>
                                                <th class="text-center">Unit</th>
                                                <th class="text-right">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php if(!empty($entries) && ($entries->count() > 0)): ?>

                                            <?php $sub_total = 0; ?>

                                            <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e($value['product_name']); ?><br><?php echo e($value['product_entry_name']); ?></td>
                                                <td class="text-right"><?php echo e(number_format($value['rate'],2,'.',',')); ?></td>
                                                <td class="text-right"><?php echo e($value['quantity']); ?></td>
                                                <td class="text-center"><?php echo e($value->convertedUnit->name); ?></td>
                                                <td class="text-right"><?php echo e(number_format($value['total_amount'],2,'.',',')); ?></td>
                                            </tr>

                                            <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                            
                                            <tr>
                                                <td colspan="5" class="text-right">Sub Total</td>
                                                <td class="text-right"><?php echo e(number_format($sub_total,2,'.',',')); ?></td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td colspan="5" class="border-0 text-right">Vat/Tax/Adjustment</td>
                                                <td class="border-0 text-right"><?php echo e(number_format($purchase_return['return_amount'] - $sub_total,2,'.',',')); ?></td>
                                            </tr>
                                           
                                            <tr style="line-height: 0px">
                                                <td colspan="5" class="border-0 text-right">
                                                    <strong>Total</strong></td>
                                                <td class="border-0 text-right"><h4 class="m-0"><?php echo e(number_format($purchase_return['return_amount'],2,'.',',')); ?></h4></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <?php if($purchase_return['return_note'] != null): ?>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6><strong>Note :</strong> <?php echo e($purchase_return['return_note']); ?></h6>
                                    </div>
                                </div>
                                <?php endif; ?>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="position: fixed;bottom: 0">
                                    <!-- <h4 class="float-right font-size-16">Order # 12345</h4> -->
                                    <!-- <div class="col-md-4">
                                        <img class="float-left" src="<?php echo e(url('public/az-ai.png')); ?>" alt="logo" height="20"/>
                                    </div>

                                    <div class="col-md-4">
                                        <h2 style="text-align: center"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="line-height: 0px;text-align: center"><?php echo e($user_info['address']); ?></p>
                                        <p style="line-height: 0px;text-align: center"><?php echo e($user_info['contact_number']); ?></p>
                                    </div>

                                    <div class="col-md-4">
                                        <p style="line-height: 18px;text-align: right;padding: 0px">Phone - 01718937082<br>01711418731<br>01711418731</p>
                                    </div> -->
                                    <?php if($user_info['footer_image'] != null): ?>
                                        <img class="float-left" src="<?php echo e(url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image'])); ?>" alt="logo" style="width: 100%" />
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/dollar-enterprise/dealer-system/Modules/PurchaseReturn/Resources/views/show.blade.php ENDPATH**/ ?>