

<?php $__env->startSection('title', 'Show'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Direct Return</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Direct Return</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <?php if($user_info['header_image'] == null): ?>
                                        <div class="col-md-12 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px"><?php echo e($user_info['organization_name']); ?></h2>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['address']); ?></p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['contact_number']); ?></p>
                                        </div>
                                        <div class="col-md-12 col-xs-12 col-sm-12"></div>
                                    <?php else: ?>
                                        <img class="float-left" src="<?php echo e(url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image'])); ?>" alt="logo" style="width: 100%" />
                                    <?php endif; ?>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-4 col-6">
                                        <address>
                                            <strong>Return By:</strong><br>
                                            <?php echo e($sales_return->customer_id != null ? $sales_return->customer->name : ''); ?><br> 
                                            <?php echo e($sales_return->customer_id != null ? $sales_return->customer->address : ''); ?> <br>
                                            <?php echo e($sales_return->customer_id != null ? $sales_return->customer->phone : ''); ?>

                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6 hidden-xs">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Memo No : <?php echo e($sales_return['return_number']); ?></h3>
                                        </div>
                                        
                                        <div class="col-md-6 hidden-xs">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Date: <?php echo e(date('d-m-Y', strtotime($sales_return['return_date']))); ?></h3>
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered" style="width: 100%">
                                        <tr>
                                            <th style="font-size: 18px;width: 5%;text-align: center">নং</th>
                                            <th style="font-size: 18px;width: 60%;text-align: center">মালের বিবরণ</th>
                                            <th style="font-size: 18px;width: 15%;text-align: center">পরিমান</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center">দর</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center">টাকা</th>
                                        </tr>
                    
                                        <?php if($sales_return->directReturnEntries->count() > 0): ?>
                    
                                        <?php
                                            $total_amount   = 0;
                                        ?>
                    
                                        <?php $__currentLoopData = $sales_return->directReturnEntries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $variation_name = ProductVariationName($value['product_entry_id']);
                    
                                            if ($value->productEntries->product_code != null)
                                            {
                                                $productCode  = ' - '.$value->productEntries->product_code;
                                            }
                                            else
                                            {
                                                $productCode  = '';
                                            }
                    
                                            if ($value->productEntries->name != null)
                                            {
                                                $category  = ' - '.$value->productEntries->name;
                                            }
                                            else
                                            {
                                                $category  = '';
                                            }
                    
                                           
                    
                                            
                    
                                            if ($value->productEntries->unit_id != null)
                                            {
                                                $unit  = ' '.$value->productEntries->unit->name;
                                            }
                                            else
                                            {
                                                $unit  = ' SFT';
                                            }
                    
                                            if ($variation_name != null)
                                            {
                                                $variation  = ' '.$variation_name;
                                            }
                                            else
                                            {
                                                $variation  = '';
                                            }
                                        ?>
                    
                                        <tr class="tr-height">
                                            <td style="text-align: center"><?php echo e($key + 1); ?></td>
                                            <td style="padding-left: 30px"><?php echo e($productCode . $category . ' - ' . $value->productEntries->name); ?></td>
                                            <td style="text-align: center"><?php echo e($value['quantity'] . $unit); ?></td>
                                            <td style="text-align: center"><?php echo e($value['rate']); ?></td>
                                            <td style="text-align: center"><?php echo e(round($value['total_amount'], 2)); ?></td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                    
                                        <tr>
                                            <th style="text-align: left" colspan="2">In Words : <span style="font-weight: normal;"><?php echo e(numberTowords($total_amount)); ?></span></th>
                                            <th></th>
                                            <th style="text-align: right"><strong>Total</strong></th>
                                            <th style="text-align: center"><?php echo e(round($total_amount, 2)); ?></th>
                                        </tr>
                                    </table>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="position: fixed;bottom: 0">
                                    <?php if($user_info['footer_image'] != null): ?>
                                        <img class="float-left" src="<?php echo e(url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image'])); ?>" alt="logo" style="width: 100%" />
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/dollar-enterprise/dealer-system/Modules/SalesReturn/Resources/views/direct_return/show.blade.php ENDPATH**/ ?>