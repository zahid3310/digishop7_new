<!DOCTYPE html>
<html>

<head>
    <title>Stock Transfer Report</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">
</head>

<style type="text/css">        
    @page  {
        size: auto;   
        margin: 15mm 5mm 5mm 10mm;
    }

    #print-footer {
        display: none;
        position: fixed;
        bottom: 0;
        left:0;
    }

    #hide-print {
        display: none;
    }

    #print-footer {
        display: none;
    }

    body {
        margin-bottom:10mm;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #ddd;
    }

    .table td, .table th {
        font-size: 12px !important;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p><?php echo e($user_info['address']); ?></p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Stock Transfer Report</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                    <th style="text-align: center">Branch</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center"><?php echo e($from_date); ?> <strong>To</strong> <?php echo e($to_date); ?></td>

                                    <td style="text-align: center">
                                        <?php if($branch_name != null): ?>
                                            <?php echo e($branch_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%;color: black;font-size: 30px" colspan="6"><strong>Receive List</strong></th>
                                </tr>
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">SL</th>
                                    <th style="text-align: center;width: 10%">DATE</th>
                                    <th style="text-align: center;width: 15%">RECEIVE FROM</th>
                                    <th style="text-align: center;width: 15%">NOTE</th>
                                    <th style="text-align: center;width: 15%">PRODUCT NAME</th>
                                    <th style="text-align: center;width: 10%">QUANTITY</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php if($receive->count() > 0): ?>
                                <?php $__currentLoopData = $receive; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $rec): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td style="text-align: center;"><?php echo e($key + 1); ?></td>
                                    <td style="text-align: center;"><?php echo e(date('d-m-Y', strtotime($rec['date']))); ?></td>
                                    <td style="text-align: center;"><?php echo e($rec->transferFrom->name); ?></td>
                                    <td style="text-align: left;"><?php echo e($rec->note); ?></td>
                                    <td style="text-align: left;"><?php echo e($rec->productEntries->name); ?></td>
                                    <td style="text-align: center;"><?php echo e($rec->quantity. ' ' . $rec->convertedUnit->name); ?></td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </tbody>

                            <!-- <tfoot class="tfheight">
                                <tr>
                                    <th colspan="5" style="text-align: right;">TOTAL</th>
                                    <th colspan="1" style="text-align: right;"></th>
                                    
                                </tr>
                            </tfoot> -->
                        </table>

                        <hr>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%;color: black;font-size: 30px" colspan="6"><strong>Transfer List</strong></th>
                                </tr>
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">SL</th>
                                    <th style="text-align: center;width: 10%">DATE</th>
                                    <th style="text-align: center;width: 15%">TRANSFER TO</th>
                                    <th style="text-align: center;width: 15%">NOTE</th>
                                    <th style="text-align: center;width: 15%">PRODUCT NAME</th>
                                    <th style="text-align: center;width: 10%">QUANTITY</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php if($transfer->count() > 0): ?>
                                <?php $__currentLoopData = $transfer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1=> $trans): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td style="text-align: center;"><?php echo e($key1 + 1); ?></td>
                                    <td style="text-align: center;"><?php echo e(date('d-m-Y', strtotime($trans['date']))); ?></td>
                                    <td style="text-align: center;"><?php echo e($trans->transferTo->name); ?></td>
                                    <td style="text-align: left;"><?php echo e($trans->note); ?></td>
                                    <td style="text-align: left;"><?php echo e($trans->productEntries->name); ?></td>
                                    <td style="text-align: center;"><?php echo e($trans->quantity . ' ' . $trans->convertedUnit->name); ?></td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </tbody>

                            <!-- <tfoot class="tfheight">
                                <tr>
                                    <th colspan="5" style="text-align: right;">TOTAL</th>
                                    <th colspan="1" style="text-align: right;"></th>
                                    
                                </tr>
                            </tfoot> -->
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/dollar-enterprise/dealer-system/Modules/Reports/Resources/views/stock_transfer_report_print.blade.php ENDPATH**/ ?>