<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="<?php echo e(route('home')); ?>" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="<?php echo e(Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_show' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_show' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fab fa-adn"></i><span>Accounts</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('expenses_index')); ?>">Expenses</a> </li>

                        <li> <a class="<?php echo e(Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('incomes_index')); ?>">Incomes</a> </li>
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('balance_transfer_index')); ?>">Balance Transfer</a> 
                        </li>

                        <li> <a class="<?php echo e(Route::currentRouteName() == 'balance_transfer_print_check_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_show' ? 'mm-active' : ''); ?>" href="<?php echo e(route('balance_transfer_print_check_index')); ?>">Print Check</a> 
                        </li>
                    </ul>
                </li>

                <li class="<?php echo e(Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                       <i class="fas fa-strikethrough"></i><span>Payroll</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('process_monthly_salary_index')); ?>">List of Salary Sheet</a> </li>
                        <li> <a href="<?php echo e(route('process_monthly_salary_create')); ?>">Process Salary Sheet</a> </li>
                    </ul>
                </li>

                <!-- <li>
                    <a class="has-arrow waves-effect">
                        <i class="far fa-envelope"></i><span>Attendance</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('attendance_import_user_index')); ?>">List of Employees</a> </li>
                    </ul>
                </li> -->

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="far fa-envelope"></i><span>Messaging</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('messages_send_index')); ?>">Send Message</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>Reports</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('monthly_alary_report_index')); ?>">Monthly Salary Sheet</a> </li>
                        <li> <a href="<?php echo e(route('attendance_report_index')); ?>">Attendance Report</a> </li>
                        
                        <li class="">
                            <a class="has-arrow waves-effect">
                                Accounts
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('expense_report_index')); ?>">List of Expense</a> </li>
                                <li> <a href="<?php echo e(route('income_report_index')); ?>">List of Income</a> </li>
                                <li> <a href="<?php echo e(route('general_ledger_index')); ?>">Ledger Book</a> </li>
                                <li> <a href="<?php echo e(route('income_expense_ledger_index')); ?>">Cash Book</a> </li>
                                <li> <a href="<?php echo e(route('income_statement_index')); ?>">Bank Book</a> </li>
                                <li> <a href="<?php echo e(route('daily_collection_report_index')); ?>">Daily Report</a> </li>
                            </ul>
                        </li>

                    </ul>
                </li>
                
                <li class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>Basic Settings</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Registers
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : ''); ?>" href="<?php echo e(route('customers_index').'?contact_type=0'); ?>">Add SR/Customer</a> </li>
                                <li> <a class="<?php echo e(Request::getQueryString() == 'contact_type=1' ? 'mm-active' : ''); ?>" href="<?php echo e(route('customers_index').'?contact_type=1'); ?>">Add Supplier</a> </li>
                                <li> <a class="<?php echo e(Request::getQueryString() == 'contact_type=2' ? 'mm-active' : ''); ?>" href="<?php echo e(route('customers_index').'?contact_type=2'); ?>">Add Employee</a> </li>
                                <li> <a class="<?php echo e(Request::getQueryString() == 'contact_type=3' ? 'mm-active' : ''); ?>" href="<?php echo e(route('customers_index').'?contact_type=3'); ?>">Add Reference</a> </li>
                            </ul>
                        </li>

                        <li class="<?php echo e(Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || 
                        Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' ||
                        Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || 
                            Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Accounts
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('account_types_index')); ?>">Account Types</a> </li>
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('chart_of_accounts_index')); ?>">Chart of Accounts</a> </li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow waves-effect">
                                Messaging
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('messages_index')); ?>">Create Text Message</a> </li>
                                <li> <a href="<?php echo e(route('messages_phone_book_index')); ?>">Phone Book</a> </li>
                            </ul>
                        </li>

                        <li class="<?php echo e(Route::currentRouteName() == 'salary_grades_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_grades_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' ||
                                      Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' ||
                                      Route::currentRouteName() == 'salary_increaments_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_increaments_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'salary_grades_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_grades_create' ? 'mm-active' : ''|| Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'salary_increaments_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_increaments_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Payroll Settings
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li class="<?php echo e(Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : ''); ?>">
                                    <a class="<?php echo e(Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                        Grades
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="<?php echo e(Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('salary_grades_index')); ?>">List of Grades</a> </li>
                                        <li> <a href="<?php echo e(route('salary_grades_create')); ?>">New Grade</a> </li>
                                    </ul>
                                </li>

                                <li class="<?php echo e(Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : ''); ?>">
                                    <a class="<?php echo e(Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                        Salary Statements
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="<?php echo e(Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('salary_statements_index')); ?>">List of Statements</a> </li>
                                        <li> <a href="<?php echo e(route('salary_statements_create')); ?>">New Statement</a> </li>
                                    </ul>
                                </li>

                                <!-- <li class="<?php echo e(Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : ''); ?>">
                                    <a class="<?php echo e(Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                        Increaments
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="<?php echo e(Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('salary_increaments_index')); ?>">List of Increaments</a> </li>
                                        <li> <a href="<?php echo e(route('salary_increaments_create')); ?>">Add Increament</a> </li>
                                    </ul>
                                </li> -->
                            </ul>
                        </li>

                        <?php if(Auth()->user()->branch_id == 1): ?>
                        <li>
                            <a class="has-arrow waves-effect">
                                Security System
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <!-- <li> <a href="<?php echo e(route('branch_index')); ?>">Add Branch</a> </li> -->
                                <li> <a href="<?php echo e(route('users_index')); ?>">Add User</a> </li>
                                <li> <a href="<?php echo e(route('users_index_all')); ?>">List of User</a> </li>
                                <li> <a href="<?php echo e(route('set_access_index')); ?>">Set Permission</a> </li>
                            </ul>
                        </li>
                         <?php endif; ?>
                        
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</div><?php /**PATH /home/digishop7/public_html/dollar-enterprise/financial-accounting/resources/views/layouts/headers.blade.php ENDPATH**/ ?>