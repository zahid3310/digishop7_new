<!DOCTYPE html>
<html>

<head>
    <title>Summary Report</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">
</head>

<style type="text/css" media="print">        
    @page {
        size: auto;   
        margin: 15mm 5mm 5mm 10mm;
    }

    #print-footer {
        display: none;
        position: fixed;
        bottom: 0;
        left:0;
    }

    #hide-print {
        display: none;
    }

    #print-footer {
        display: none;
    }

    body {
        margin-bottom:10mm;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #ddd;
    }
    
    fontSizeC {
        font-size: 10px !important;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Summary Report</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">{{ __('messages.from_date')}}</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="font-size: 10px;text-align: center;width: 5%">{{ __('messages.sl')}}</th>
                                    <th style="font-size: 10px;text-align: center;width: 8%">Production#</th>
                                    <th style="font-size: 10px;text-align: center;width: 7%">{{ __('messages.T/date')}}</th>
                                    <th style="font-size: 10px;text-align: center;width: 15%">Description</th>
                                    <th style="font-size: 10px;text-align: center;width: 4%">Stock</th>
                                    <th style="font-size: 10px;text-align: center;width: 4%">U/M</th>
                                    <th style="font-size: 10px;text-align: center;width: 4%">Rate</th>
                                    <th style="font-size: 10px;text-align: center;width: 4%">Quantity</th>
                                    <th style="font-size: 10px;text-align: center;width: 4%">Damage</th>
                                    <th style="font-size: 10px;text-align: center;width: 4%">Stock Value</th>


                                    <th style="font-size: 10px;text-align: center;width: 7%">{{ __('messages.T/date')}}</th>
                                    <th style="font-size: 10px;text-align: center;width: 15%">Description</th>
                                    <th style="font-size: 10px;text-align: center;width: 4%">Stock</th>
                                    <th style="font-size: 10px;text-align: center;width: 4%">U/M</th>
                                    <th style="font-size: 10px;text-align: center;width: 4%">Rate</th>
                                    <th style="font-size: 10px;text-align: center;width: 4%">Send To Production</th>
                                    <th style="font-size: 10px;text-align: center;width: 4%">Back To Production</th>
                                    <th style="font-size: 10px;text-align: center;width: 4%">Production Cost</th>


                                    <th style="font-size: 10px;text-align: center;width: 4%">Difference</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php
                                    $i                      = 1;
                                    $total_sale_value       = 0;
                                    $total_production_cost  = 0;
                                ?>

                                @foreach($result as $key => $value)
                                <tr>
                                    <td style="font-size: 10px;text-align: center;vertical-align: middle" rowspan="{{ $value['row_count'] }}">{{ $i }}</td>
                                    <td style="font-size: 10px;text-align: center;vertical-align: middle" rowspan="{{ $value['row_count'] }}">{{ $value['production_number'] }}</td>

                                    
                                    <!-- Finished Goods Items -->
                                        @if(collect($value['finished_good_items'])->count() > 0)
                                        @foreach(collect($value['finished_good_items']) as $key1 => $value1)
                                        @if($key1 == 0)
                                            
                                            <td style="font-size: 10px;text-align: center">{{ $value1['date'] }}</td>
                                            <td style="font-size: 10px;text-align: left">{{ $value1['name'] }}</td>
                                            <td style="font-size: 10px;text-align: center">{{ $value1['stock_quantity'] }}</td>
                                            <td style="font-size: 10px;text-align: center">{{ $value1['unit_name'] }}</td>
                                            <td style="font-size: 10px;text-align: center">{{ $value1['rate'] }}</td>
                                            <td style="font-size: 10px;text-align: center">{{ $value1['quantity'] }}</td>
                                            <td style="font-size: 10px;text-align: center">{{ $value1['damage_quantity'] }}</td>
                                            <td style="font-size: 10px;text-align: center">{{ ($value1['quantity'] - $value1['damage_quantity'])*$value1['rate'] }}</td>

                                            <?php
                                                $total_sale_value    = $total_sale_value + ($value1['quantity'] - $value1['damage_quantity'])*$value1['rate'];
                                            ?>

                                        @endif
                                        @endforeach
                                        @else
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        @endif
                                    <!-- Finished Goods Items -->


                                    <!-- Production Items -->
                                        @if(collect($value['production_items'])->count() > 0)
                                        @foreach(collect($value['production_items']) as $key4 => $value4)
                                        @if($key4 == 0)
                                            
                                            <td style="font-size: 10px;text-align: center">{{ $value4['date'] }}</td>
                                            <td style="font-size: 10px;text-align: left">{{ $value4['name'] }}</td>
                                            <td style="font-size: 10px;text-align: center">{{ $value4['stock_quantity'] }}</td>
                                            <td style="font-size: 10px;text-align: center">{{ $value4['unit_name'] }}</td>
                                            <td style="font-size: 10px;text-align: center">{{ $value4['rate'] }}</td>
                                            <td style="font-size: 10px;text-align: center">{{ $value4['quantity'] }}</td>
                                            <td style="font-size: 10px;text-align: center">{{ $value4['return_quantity'] }}</td>
                                            <td style="font-size: 10px;text-align: center">{{ ($value4['quantity'] - $value4['return_quantity'])*$value4['rate'] }}</td>


                                            <?php
                                                $total_production_cost    = $total_production_cost + ($value4['quantity'] - $value4['return_quantity'])*$value4['rate'];
                                            ?>
                                        @endif
                                        @endforeach
                                        @else
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        @endif
                                    <!-- Production Items -->

                                    <td style="font-size: 10px;text-align: center;vertical-align: middle" rowspan="{{ $value['row_count'] }}">{{ $value['total_sale_value'] - $value['total_production_cost'] }}</td>
                                </tr>




                                @for($m=0; $m<$value['row_count']; $m++)
                                @if($m != 0)
                                <tr>
                                    <td style="font-size: 10px;text-align: center">{{ isset($value['finished_good_items'][$m]) ? $value['finished_good_items'][$m]['date'] : '' }}</td>
                                    <td style="font-size: 10px;text-align: left">{{ isset($value['finished_good_items'][$m]) ? $value['finished_good_items'][$m]['name'] : '' }}</td>
                                    <td style="font-size: 10px;text-align: center">{{ isset($value['finished_good_items'][$m]) ? $value['finished_good_items'][$m]['stock_quantity'] : '' }}</td>
                                    <td style="font-size: 10px;text-align: center">{{ isset($value['finished_good_items'][$m]) ? $value['finished_good_items'][$m]['unit_name'] : '' }}</td>
                                    <td style="font-size: 10px;text-align: center">{{ isset($value['finished_good_items'][$m]) ? $value['finished_good_items'][$m]['rate'] : '' }}</td>
                                    <td style="font-size: 10px;text-align: center">{{ isset($value['finished_good_items'][$m]) ? $value['finished_good_items'][$m]['quantity'] : '' }}</td>
                                    <td style="font-size: 10px;text-align: center">{{ isset($value['finished_good_items'][$m]) ? $value['finished_good_items'][$m]['damage_quantity'] : '' }}</td>
                                    <td style="font-size: 10px;text-align: center">
                                        {{ ((isset($value['finished_good_items'][$m]) ? $value['finished_good_items'][$m]['quantity'] : 0) - (isset($value['finished_good_items'][$m]) ? $value['finished_good_items'][$m]['damage_quantity'] : 0))*(isset($value['finished_good_items'][$m]) ? $value['finished_good_items'][$m]['rate'] : 0) }}
                                    </td>

                                    <td style="font-size: 10px;text-align: center">{{ isset($value['production_items'][$m]) ? $value['production_items'][$m]['date'] : '' }}</td>
                                    <td style="font-size: 10px;text-align: left">{{ isset($value['production_items'][$m]) ? $value['production_items'][$m]['name'] : '' }}</td>
                                    <td style="font-size: 10px;text-align: center">{{ isset($value['production_items'][$m]) ? $value['production_items'][$m]['stock_quantity'] : '' }}</td>
                                    <td style="font-size: 10px;text-align: center">{{ isset($value['production_items'][$m]) ? $value['production_items'][$m]['unit_name'] : '' }}</td>
                                    <td style="font-size: 10px;text-align: center">{{ isset($value['production_items'][$m]) ? $value['production_items'][$m]['rate'] : '' }}</td>
                                    <td style="font-size: 10px;text-align: center">{{ isset($value['production_items'][$m]) ? $value['production_items'][$m]['quantity'] : '' }}</td>
                                    <td style="font-size: 10px;text-align: center">{{ isset($value['production_items'][$m]) ? $value['production_items'][$m]['return_quantity'] : '' }}</td>
                                    <td style="font-size: 10px;text-align: center">
                                        {{ ((isset($value['production_items'][$m]) ? $value['production_items'][$m]['quantity'] : 0) - (isset($value['production_items'][$m]) ? $value['production_items'][$m]['return_quantity'] : 0))*(isset($value['production_items'][$m]) ? $value['production_items'][$m]['rate'] : 0) }}
                                    </td>
                                </tr>

                                <?php
                                    $total_sale_value       = $total_sale_value + ((isset($value['finished_good_items'][$m]) ? $value['finished_good_items'][$m]['quantity'] : 0) - (isset($value['finished_good_items'][$m]) ? $value['finished_good_items'][$m]['damage_quantity'] : 0))*(isset($value['finished_good_items'][$m]) ? $value['finished_good_items'][$m]['rate'] : 0);
                                    $total_production_cost  = $total_production_cost + ((isset($value['production_items'][$m]) ? $value['production_items'][$m]['quantity'] : 0) - (isset($value['production_items'][$m]) ? $value['production_items'][$m]['return_quantity'] : 0))*(isset($value['production_items'][$m]) ? $value['production_items'][$m]['rate'] : 0);
                                ?>
                                @endif
                                @endfor




                                <?php $i++; ?>
                                @endforeach
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="9" style="text-align: right;">{{ __('messages.total')}}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_sale_value }}</th>
                                    <th colspan="7" style="text-align: right;">{{ __('messages.total')}}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_production_cost }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_sale_value - $total_production_cost }}</th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>