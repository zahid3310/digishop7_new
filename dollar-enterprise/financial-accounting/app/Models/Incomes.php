<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Incomes extends Model
{
    protected $table = "incomes";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function incomeCategory()
    {
        return $this->belongsTo('App\Models\IncomeCategories','income_category_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function paidThroughAccount()
    {
        return $this->belongsTo('App\Models\Accounts','paid_through_id');
    }

    public function account()
    {
        return $this->belongsTo('App\Models\Accounts','account_id');
    }
}
