

<?php $__env->startSection('title', 'Balance Transfer'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.transfer_balance')); ?> </h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.transfer_balance')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.transfer_balance')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('balance_transfer_store')); ?>" method="post" files="true" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>


                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label"><?php echo e(__('messages.transfer_date')); ?> *</label>
                                        <input id="transfer_date" name="transfer_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label"><?php echo e(__('messages.transfer_from')); ?> *</label>
                                        <select style="width: 100%" id="transfer_from" name="transfer_from" class="form-control select2" required>
                                           <option value="">--<?php echo e(__('messages.select_account')); ?>--</option>
                                           <?php if($paid_accounts->count() > 0): ?>
                                           <?php $__currentLoopData = $paid_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $from_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <option value="<?php echo e($from_account->id); ?>"><?php echo e($from_account->name); ?></option>
                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                           <?php endif; ?>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label"><?php echo e(__('messages.transfer_to')); ?> *</label>
                                        <select style="width: 100%" id="transfer_to" name="transfer_to" class="form-control select2" required>
                                           <option value="">--<?php echo e(__('messages.select_account')); ?>--</option>
                                           <?php if($paid_accounts->count() > 0): ?>
                                           <?php $__currentLoopData = $paid_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $to_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <option value="<?php echo e($to_account->id); ?>"><?php echo e($to_account->name); ?></option>
                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                           <?php endif; ?>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label"><?php echo e(__('messages.note')); ?></label>
                                        <input type="text" name="note" class="inner form-control" id="note" placeholder="<?php echo e(__('messages.transfer_note')); ?>" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label"><?php echo e(__('messages.amount')); ?> *</label>
                                        <input type="text" name="amount" class="inner form-control" id="amount" placeholder="<?php echo e(__('messages.amount')); ?>" required />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light"><?php echo e(__('messages.save')); ?></button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('balance_transfer_index')); ?>"><?php echo e(__('messages.close')); ?></a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title"><?php echo e(__('messages.all_transfer')); ?></h4>

                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th><?php echo e(__('messages.sl')); ?>#</th>
                                            <th><?php echo e(__('messages.date')); ?></th>
                                            <th><?php echo e(__('messages.transfer_from')); ?></th>
                                            <th><?php echo e(__('messages.transfer_to')); ?></th>
                                            <th><?php echo e(__('messages.transfer_note')); ?></th>
                                            <th><?php echo e(__('messages.amount')); ?></th>
                                            <th><?php echo e(__('messages.action')); ?></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php if(!empty($transfers) && ($transfers->count() > 0)): ?>
                                        <?php $__currentLoopData = $transfers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $transfer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e(date('d-m-Y', strtotime($transfer['transaction_date']))); ?></td>
                                                <td><?php echo e($transfer->transfer_from != null ? $transfer->transferFrom->name : ''); ?></td>
                                                <td><?php echo e($transfer->transfer_to != null ? $transfer->transferTo->name : ''); ?></td>
                                                <td><?php echo e($transfer->note); ?></td>
                                                <td><?php echo e($transfer->amount); ?></td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="<?php echo e(route('balance_transfer_edit', $transfer['id'])); ?>">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/dollar-enterprise/production-based/Modules/BalanceTransfer/Resources/views/index.blade.php ENDPATH**/ ?>