

<?php $__env->startSection('title', 'Back To Warehouse'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.back_to_warehouse')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.production')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.back_to_warehouse')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

            					<form id="FormSubmit" action="<?php echo e(route('productions_return_to_warehouse_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					<?php echo e(csrf_field()); ?>


                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="return_date"><?php echo e(__('messages.return_date')); ?> *</label>
                                            <input id="return_date" name="return_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo e(__('messages.production_number')); ?> * </label>
                                            <select id="production_number" name="production_number" class="form-control select2 col-md-12" onchange="productionDetails()" required>
                                                <option value="0">--Select Production Number--</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="return_note"><?php echo e(__('messages.note')); ?></label>
                                            <input id="return_note" name="return_note" type="text" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px;" class="row">
                                    <div style="background-color: #D2D2D2;padding-top: 13px;height: 230px;overflow-y: auto;overflow-x: auto" class="col-md-12">
                                        <div class="inner-repeater mb-4">
                                            <div data-repeater-list="inner-group" class="inner form-group row">
                                                <div id="production_enrty_list" class="inner col-lg-12 ml-md-auto">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                
                                    <div style="background-color: #F4F4F7;height: 60px;padding-top: 13px" class="col-md-12">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label"><?php echo e(__('messages.return_amount')); ?></label>
                                            <div class="col-md-7">
                                                <input id="totalReturnedBdt" type="text" class="form-control width-xs" name="total_return_amount" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div style="margin-top: 20px !important;padding-bottom: 0px !important" class="form-group row">
                                    <div class="button-items col-lg-12">
                                        <button style="border-radius: 0px !important" name="print" type="submit" class="btn btn-primary waves-effect waves-light"><?php echo e(__('messages.save')); ?></button>
                                        <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('sales_return_index')); ?>"><?php echo e(__('messages.close')); ?></a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<h4 class="card-title"><?php echo e(__('messages.all_return_list')); ?></h4>

                                <div style="margin-right: 0px" class="row">
                                    <div class="col-lg-8 col-md-6 col-sm-4 col-4"></div>
                                    <div class="col-lg-1 col-md-2 col-sm-4 col-4"><?php echo e(__('messages.search')); ?>: </div>
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-4">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th><?php echo e(__('messages.sl')); ?></th>
                                            <th><?php echo e(__('messages.date')); ?></th>
                                            <th><?php echo e(__('messages.production')); ?>#</th>
                                            <th><?php echo e(__('messages.amount')); ?></th>
                                            <th><?php echo e(__('messages.action')); ?></th>
                                        </tr>
                                    </thead>

                                    <tbody id="return_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Are you sure want to delete the entry ?</label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="delete_btn" class="btn btn-primary waves-effect waves-light delete_btn">Delete</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#production_number").select2({
                ajax: { 
                url:  site_url + '/productions/return-to-warehouse/production-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            if (location.search != "")
            {
                var locationValue = (new URL(location.href)).searchParams.get('production_number'); 
            
                if (locationValue != null)
                {
                    var selected_production_number = (new URL(location.href)).searchParams.get('production_number');
                    $.get(site_url + '/productions/return-to-warehouse/find-production-number/' + selected_production_number, function(data){
                        var production_number_name = data.production_number;
                        $("#production_number").empty().append('<option value="'+ data.id +'">' + production_number_name + '</option>').val(data.id);
                    });
                }
            }

            $.get(site_url + '/productions/return-to-warehouse/return-list/', function(data){

                returnList(data);
            });
        });

        function searchPayment()
        {
            var search_text     = $('#searchPayment').val();
            var site_url        = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/productions/return-to-warehouse/return-search-list/' + search_text, function(data){

                returnList(data);
            });
        }
    </script>

    <script type="text/javascript">
        function productionDetails()
        {
        	var site_url  	        = $('.site_url').val();
        	var production_number   = $('#production_number').val();

        	$.get(site_url + '/productions/return-to-warehouse/production-entries-list-by-production-number/' + production_number, function(data){

                var production_enrty_list   = '';
                var sub_total               = 0;
                var serial                  = 1;

                $.each(data.production_entries, function(i, production_entry_data)
                {    
                    if (serial == 1)
                    {
                        var product_label       = '<label class="hidden-xs" for="productname"><?php echo e(__('messages.product')); ?> *</label>\n';
                        var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                        var unit_label          = '<label class="hidden-xs" for="productname"><?php echo e(__('messages.unit')); ?></label>\n';
                        var return_unit_label   = '<label class="hidden-xs" for="productname">R/Unit</label>\n';
                        var quantity_label      = '<label class="hidden-xs" for="productname"><?php echo e(__('messages.qty')); ?> *</label>\n';
                        var amount_label        = '<label class="hidden-xs" for="productname"><?php echo e(__('messages.amount')); ?></label>\n';
                        var action_label        = '<label class="hidden-xs" for="productname"><?php echo e(__('messages.return')); ?> <?php echo e(__('messages.qty')); ?></label>\n';
                    }
                    else
                    {
                        var product_label       = '';
                        var rate_label          = '';
                        var unit_label          = '';
                        var return_unit_label   = '';
                        var quantity_label      = '';
                        var amount_label        = '';
                        var action_label        = '';
                    }

                    sub_total           += parseFloat(production_entry_data.total_amount);

                    if (production_entry_data.quantity == 0)
                    {
                        var return_quantity = '<input id="return_quantity_'+ serial +'" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="0" oninput="calculate('+ serial +')" readonly />\n' 
                    }
                    else
                    {
                        var return_quantity = '<input id="return_quantity_'+ serial +'" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="0" oninput="calculate('+ serial +')" />\n' 
                    }

                    var units    = '';
                    $.each(production_entry_data.unit_conversions, function(i, data_list_unit)
                    {   
                        units    += '<option value = "' + data_list_unit.unit_id + '">' + data_list_unit.unit_name + '</option>';
                    });

                    units    += '<option value = "' + production_entry_data.converted_unit_id + '" selected>' + production_entry_data.conversion_unit_name + '</option>';

                    production_enrty_list += ' ' + '<div style="margin-bottom: 0px !important" class="mb-3 row align-items-center">' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+serial+'" value="'+ production_entry_data.main_unit_id +'" readonly />\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-4 col-md-4 col-sm-6 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<input type="text" class="inner form-control" value="'+ production_entry_data.entry_name + ' ( ' + pad(production_entry_data.product_code, 6) + ' )' +'" readonly />\n' +
                                                        '<input id="product_entry_id_'+serial+'" name="product_entries[]" type="hidden" value="'+ production_entry_data.product_entry_id +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+serial+'" value="'+production_entry_data.main_unit_id+'" />\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                        unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_'+serial+'" >\n' +
                                                        '<option value="'+ production_entry_data.conversion_unit_id +'">'+ production_entry_data.conversion_unit_name +'</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input id="rate_'+ serial +'" type="text" class="inner form-control rate" value="'+ production_entry_data.rate +'" readonly />\n' + 
                                                        '<input name="rate[]" type="hidden" value="'+ production_entry_data.rate +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label +
                                                        '<input id="quantity_'+ serial +'" type="text" class="inner form-control" value="'+ production_entry_data.quantity +'" readonly />\n' +
                                                        '<input name="quantity[]" type="hidden" class="quantity" value="'+ production_entry_data.original_quantity +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Amount</label>\n' +
                                                        amount_label +
                                                        '<input id="amount_'+ serial +'" type="text" class="inner form-control amount" value="'+ production_entry_data.total_amount +'" readonly />\n' + 
                                                        '<input name="amount[]" type="hidden" value="'+ production_entry_data.total_amount +'" readonly />\n' +
                                                    '</div>\n' + 

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Return Qty</label>\n' +
                                                        action_label +
                                                        return_quantity +
                                                    '</div>\n' +
                                                '</div>\n';

                    serial++;
                });

                $("#production_enrty_list").empty();
                $("#production_enrty_list").append(production_enrty_list);
            });
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entry_id_"+x).val();
            var unit_id             = $("#unit_id"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                var mainStock         = $("#quantity_"+x).val();
                // var convertToMain     = 
                var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                $("#quantity_"+x).val(convertedStock);
                $("#rate_"+x).val(parseFloat(data.purchase_price).toFixed(2));

                console.log(data.purchase_price);

            });
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function calculate(x)
        {
        	var return_quantity        = $('.returnQuantity').map((_,el) => el.value).get();
         	var amount 				   = $('.amount').map((_,el) => el.value).get();
         	var quantity			   = $('.quantity').map((_,el) => el.value).get();
         	var rate				   = $('.rate').map((_,el) => el.value).get();

            var totalBdt               = $("#totalBdt").val();
            var subTotal               = $("#subTotalBdt").val();

            var total                  = 0;
            var total_quantity         = 0;
            var total_return_quantity  = 0;

         	for (var i = 0; i < return_quantity.length; i++)
         	{
         		if (return_quantity[i] > 0)
         		{	
                    if (return_quantity[i] > quantity[i])
                    {
                        $('#return_quantity_'+x).val(0);
                    }
                    else
                    {
                        var result   = (parseFloat(amount[i])/parseFloat(quantity[i]))*parseFloat(return_quantity[i]);

                        total       += parseFloat(result);
                    }
         		}

                total_quantity          += parseFloat(quantity[i]);
                total_return_quantity   += parseFloat(return_quantity[i]);
         	}

            var total_result       = parseFloat(total);

         	$('#totalReturnedBdt').val(total_result.toFixed(2));
        }

        function returnList(data)
        {
            var return_list  = '';
            var sl_no        = 1;
            $.each(data, function(i, return_data)
            {
                var serial              = parseFloat(i) + 1;
                var site_url            = $('.site_url').val();
                var edit_url            = site_url + '/productions/return-to-warehouse/edit/' + return_data.id;
                var print_url           = site_url + '/productions/return-to-warehouse/show/' + return_data.id;
                var delete_url          = site_url + '/productions/return-to-warehouse/delete/' + return_data.id;

                return_list += '<tr>' +
                                    '<td>' +
                                        sl_no +
                                    '</td>' +
                                    '<td>' +
                                       formatDate(return_data.date) +
                                    '</td>' +
                                    '<td>' +
                                        return_data.production_number +
                                    '</td>' +
                                    '<td>' +
                                       return_data.total_amount +
                                    '</td>' +
                                    '<td>' +
                                        '<div class="dropdown">' +
                                            '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                            '</a>' +
                                            '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                // '<a class="dropdown-item" href="' + edit_url +'">' + '<?php echo e(__('messages.edit')); ?>' + '</a>' +
                                                // '<a class="dropdown-item" href="' + print_url +'" target="_blank">' + '<?php echo e(__('messages.show')); ?>' + '</a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</td>' +
                                '</tr>';

                                sl_no++;
            });

            $("#return_list").empty();
            $("#return_list").append(return_list);
        }

        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>

    <script type="text/javascript">
        $('.delete_btn').click(function () {
            var site_url            = $('.site_url').val();
            var id                  = $('.salesReturnId').val();
            window.location.href    = site_url + "/salesreturn/delete/"+id;
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/dollar-enterprise/production-based/Modules/Productions/Resources/views/production_return/index.blade.php ENDPATH**/ ?>