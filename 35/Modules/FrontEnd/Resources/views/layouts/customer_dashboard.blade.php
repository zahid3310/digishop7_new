@extends('frontend::layouts.app')

@section('main_body')
<!-- End Header -->
<main class="main">
            <div class="page-content mt-10 pt-4 pb-4">
                <section class="contact-section">
                    <div class="container">
                        <div class="row">
                          
                     

                                <aside class="col-lg-3 col-md-3 col-xs-3 blog-sidebar sidebar-fixed sticky-sidebar-wrapper">
                            <div class="sidebar-overlay">
                                <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
                            </div>
                            <a href="#" class="sidebar-toggle"><i class="fas fa-chevron-left"></i></a>
                            <div class="sidebar-content">
                                <div class="sticky-sidebar" data-sticky-options="{'top': 89, 'bottom': 70}">
                                   
                                    @include('frontend::layouts.customer_dashboard_menu')
                                   
                                </div>
                            </div>
                        </aside>
                            	
                            <div class="col-lg-9 col-md-9 col-xs-9">
                                @if (Session::has('registration_success'))
                                <div class="alert alert-message alert-light alert-primary alert-link mb-4">
                                    <h4 class="alert-title">{{Session::get('registration_success')}}.</h4>
                                    <p>Now you can login</p>
                                    <button type="button" class="btn btn-link btn-close">
                                        <i class="d-icon-times"></i>
                                    </button>
                                </div>
                                @endif

                                <form class="ml-lg-2 pt-8 pb-10 pl-4 pr-4 pl-lg-6 pr-lg-6 grey-section" action="{{route('customer_registration')}}" method="POST">
                                    @csrf
                                    <h3 class="ls-m mb-1">Customer Profile</h3>
                                    <div class="row">

                                        <div class="col-md-6 mb-4">
                                            <input class="form-control" name="name" type="text" placeholder="Enter Your FullName" value="{{$customer_info[0]->name}}" required>
                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <input class="form-control" name="username" type="text" placeholder="Enter Your Username" value="{{$customer_info[0]->username}}" required disabled="">
                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <input class="form-control" name="phone" type="text" placeholder="Enter Your Phone Number" value="{{$customer_info[0]->phone}}" required>
                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <input class="form-control" name="email" type="text" placeholder="Enter Your Email" value="{{$customer_info[0]->email}}">
                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <input class="form-control" name="password" type="password" placeholder="Password" required>
                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <button type="submit" class="btn btn-md btn-success mb-2">Update</button>
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End About Section-->

            </div>
        </main>
<!-- End Main -->
@endsection