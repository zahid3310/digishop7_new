<div class="widget widget-categories">
	<h3 class="widget-title">Categories</h3>
	<ul class="widget-body filter-items search-ul">

		<li><a href="{{ route('customer_order_list')}}" class="{{ (last(request()->segments()) == 'customer-order-list') ? 'dashboard-menu-active' : '' }}"><i class="fas fa-list"></i> Order List</a></li>

		<li><a href="{{ route('customer_dashboard')}}" class="{{ (last(request()->segments()) == 'customerDashboard') ? 'dashboard-menu-active' : '' }}"><i class="fas fa-user-circle"></i> Profile</a></li>

		<li><a href="{{ route('customer_logout')}}"><i class="fas fa-sign-in-alt"></i>  Logout</a></li>
	</ul>
</div>

