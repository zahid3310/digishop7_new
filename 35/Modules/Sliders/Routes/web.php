<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('sliders')->group(function() {
    Route::get('/', 'SlidersController@index')->name("sliders_index");
    Route::get('/create', 'SlidersController@create')->name("sliders_create");
    Route::post('/store', 'SlidersController@store')->name("sliders_store");
    Route::get('/edit/{id}', 'SlidersController@edit')->name("sliders_edit");
    Route::post('/update/{id}', 'SlidersController@update')->name("sliders_update");
    Route::get('/delete/{id}', 'SlidersController@destroy')->name("sliders_delete");
});
