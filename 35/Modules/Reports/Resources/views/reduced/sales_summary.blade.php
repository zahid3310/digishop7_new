@extends('layouts.app')

@section('title', 'Sales Summary')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales Summary</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Sales Summary</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['organization_name'] }}</h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['address'] }}</p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['contact_number'] }}</p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">Sales Summary Report</h4>
                                        <p style="text-align: center"><strong>{{ monthName($month) . ', ' . date('Y', strtotime($year))  }}</strong></p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="{{ route('sales_summary_index_reduced') }}" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-12 d-print-none">
                                            <select id="month" style="width: 100" class="form-control select2" name="month">
                                                <option value="">--Select Month--</option>
                                                <option value="01" {{ $month == '01' ? 'selected' : '' }}>January</option>
                                                <option value="02" {{ $month == '02' ? 'selected' : '' }}>February</option>
                                                <option value="03" {{ $month == '03' ? 'selected' : '' }}>March</option>
                                                <option value="04" {{ $month == '04' ? 'selected' : '' }}>April</option>
                                                <option value="05" {{ $month == '05' ? 'selected' : '' }}>May</option>
                                                <option value="06" {{ $month == '06' ? 'selected' : '' }}>Jun</option>
                                                <option value="07" {{ $month == '07' ? 'selected' : '' }}>July</option>
                                                <option value="08" {{ $month == '08' ? 'selected' : '' }}>August</option>
                                                <option value="09" {{ $month == '09' ? 'selected' : '' }}>September</option>
                                                <option value="10" {{ $month == '10' ? 'selected' : '' }}>October</option>
                                                <option value="11" {{ $month == '11' ? 'selected' : '' }}>November</option>
                                                <option value="12" {{ $month == '12' ? 'selected' : '' }}>December</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-12 d-print-none">
                                            <select id="year" style="width: 100" class="form-control select2" name="year">
                                                <option value="">--Select Year--</option>
                                                <option value="2020" {{ $year == '2020' ? 'selected' : '' }}>2020</option>
                                                <option value="2021" {{ $year == '2021' ? 'selected' : '' }}>2021</option>
                                                <option value="2022" {{ $year == '2022' ? 'selected' : '' }}>2022</option>
                                                <option value="2023" {{ $year == '2023' ? 'selected' : '' }}>2023</option>
                                                <option value="2024" {{ $year == '2024' ? 'selected' : '' }}>2024</option>
                                                <option value="2025" {{ $year == '2025' ? 'selected' : '' }}>2025</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="0" selected>-- All Customers --</option>
                                                    @if(!empty($customers) && ($customers->count() > 0))
                                                    @foreach($customers as $key => $value)
                                                        <option {{ isset($_GET['customer_id']) && ($_GET['customer_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <!--<th>Invoice#</th>-->
                                            <th>Date</th>
                                            <th>Customer</th>
                                            <th style="text-align: right">Invoice Amount</th>
                                            <th style="text-align: right">Discount</th>
                                            <th style="text-align: right">Receivable</th>
                                            <th style="text-align: right">Received</th>
                                            <th style="text-align: right">Due</th>
                                            <th style="text-align: right">Return Amount</th>
                                            <th style="text-align: right">Payable</th>
                                        </tr>
                                    </thead>


                                    <tbody>

                                        @if(!empty($data))

                                        <?php $serial = 1; ?>

                                        @foreach($data as $key => $value)
                                        
                                            <tr>
                                                <td>{{ $serial }}</td>
                                                <!--<td>{{ $value['invoice_number'] }}</td>-->
                                                <td>{{ $value['invoice_date'] }}</td>
                                                <td>{{ $value['customer_name'] }}</td>
                                                <td style="text-align: right">{{ number_format($value['invoice_amount'] * $factor,2,'.',',') }}</td>
                                                <td style="text-align: right">
                                                    {{ number_format($value['total_discount'] + $value['discount'],2,'.',',') }}

                                                    @if($value['total_discount_note'] != null) 
                                                        <br>
                                                    @endif

                                                    {{ $value['total_discount_note'] != null ? $value['total_discount_note'] : '' }}
                                                </td>
                                                <td style="text-align: right">{{ number_format($value['invoice_amount'] * $factor,2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format($value['paid_amount'] * $factor,2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format($value['due_amount'] * $factor,2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format($value['return_amount'] * $factor,2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format($value['return_due'] * $factor,2,'.',',') }}</td>
                                            </tr>

                                            <?php $serial++; ?>

                                        @endforeach
                                        @endif

                                    </tbody>
                                    <tr>
                                        <th style="text-align: right" colspan="3">Total</th>
                                        <th style="text-align: right">{{ number_format($total_invoice_amount * $factor,2,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_discount_amount * $factor,2,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_receivable * $factor,2,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_paid_amount * $factor,2,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_due_amount * $factor,2,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_sales_return * $factor,2,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_return_due * $factor,2,'.',',') }}</th>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection