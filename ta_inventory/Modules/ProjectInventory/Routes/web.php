<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('projectinventory')->group(function() {
    Route::get('/', 'ProjectInventoryController@index')->name('use_project_list');
    Route::get('/create', 'ProjectInventoryController@create')->name('use_project_index');
    Route::get('/register-customer/list', 'ProjectInventoryController@registerCustomerList');
    Route::post('/store', 'ProjectInventoryController@store')->name('project_invoices_store');
    Route::get('/show/{id}', 'ProjectInventoryController@show')->name('project_invoices_show');
    Route::get('/projectinventory/list/load', 'ProjectInventoryController@projectinventoryListLoad')->name('projectinventory_list_load');

    Route::get('/projectList', 'ProjectInventoryController@projectList');
});
