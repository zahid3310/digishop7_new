<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentsExamResultEntries extends Model
{
    protected $table = 'students_exam_result_entries';

    public function studentExamResult()
    {
        return $this->belongsTo('App\Models\StudentsExamResult','student_exam_id');
    }
}
