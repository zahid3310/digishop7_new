@extends('layouts.app')

@section('title', 'New Marks Input')

@push('scripts')
<style type="text/css">
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>
@endpush

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Marks Input</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">OMR</a></li>
                                    <li class="breadcrumb-item active">New Marks Input</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('marks_inputs_tore') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">

                                {{ csrf_field() }}

                                <hr style="margin-top: 0px">

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="row col-md-12">
                                        <div style="margin-bottom: 5px;display:flex;" class="form-group col-md-6">
                                            <label for="exam_name" class="col-md-3 col-form-label">Exam Name</label>
                                            <div class="col-md-9">
                                                <input id="exam_name" name="exam_name" type="text" value="" class="form-control" placeholder="">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display:flex;" class="form-group col-md-6">
                                            <label for="subject_code" class="col-md-5 col-form-label">Upload Mark Sheet(.txt File)</label>
                                            <div class="col-md-7">
                                                <input id="subject_code" name="mark_sheet" type="file" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <hr style="margin-top: 10px"> 

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('marks_input_index') }}">Close</a></button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection