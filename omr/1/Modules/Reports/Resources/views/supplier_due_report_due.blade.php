@extends('layouts.app')

@section('title', 'Supplier Due List')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.Supplier_due_list')}}</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.reports')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.Supplier_due_list')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['organization_name'] }}</h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['address'] }}</p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['contact_number'] }}</p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ __('messages.Supplier_due_list')}}</h4>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><strong>{{ __('messages.from_date')}}</strong> {{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }} <strong>{{ __('messages.to_date')}}</strong> {{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="{{ route('due_report_supplier_index_due') }}" enctype="multipart/form-data">
                                    <div class="form-group row">
                                        <div style="margin-bottom: 10px;display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="{{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="{{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        @if(Auth()->user()->branch_id == 1)
                                        <div style="display: none" class="col-lg-2 col-md-2 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select style="width: 100%" id="branch_id" name="branch_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" onchange="getBranchCustomers()" required>
                                                   @if($branches->count() > 0)
                                                   @foreach($branches as $key => $value)
                                                   <option value="{{ $value['id'] }}" {{ $value->id == $branch_id ? 'selected' : '' }}>{{ $value['name'] }}</option>
                                                   @endforeach
                                                   @endif
                                                </select>
                                            </div>
                                        </div>
                                        @endif
                                        
                                        <div class="col-lg-3 col-md-3 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="references_id" style="width: 100" class="form-control select2" name="references_id">
                                                    <option value="0" selected>-- All References --</option>
                                                    @if(!empty($references) && ($references->count() > 0))
                                                    @foreach($references as $key => $value)
                                                        <option {{ isset($_GET['references_id']) && ($_GET['references_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-3 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="customer_id" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="{{ $customer_name != null ? $customer_name['id'] : 0 }}" selected>{{ $customer_name != null ? $customer_name['name'] : '--All Customers--' }}</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                    

                                        <div style="display: none" class="col-lg-2 col-md-2 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="ledger_type" style="width: 100" class="form-control select2" name="ledger_type">
                                                    <option value="0" selected>--{{ __('messages.show_all')}}--</option>
                                                    <option {{ $ledger_type == 1 ? 'selected' : '' }} value="1">Dues Available</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="{{ __('messages.search')}}"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;font-size:18px!important">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center">নং</th>
                                            <th style="text-align: center">প্রতিষ্ঠানের নাম</th>
                                            <th style="text-align: center;width: 10%">রেফারেন্স</th>
                                            <th style="text-align: center">প্রপাইটার নাম</th>th>
                                            <th style="text-align: center">ঠিকানা</th>
                                            <th style="text-align: center">মোবাইল নং</th>
                                            <th style="text-align: center">টাকা</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                            $i = 1;
                                        ?>
                                        @foreach($data as $key => $value)
                                        <tr>
                                            <td style="text-align: center;">{{ $i }}</td>
                                            <td style="text-align: left;">{{ $value['customer_name'] }} </td>
                                            <td style="text-align: left;">{{ $value['reference_name'] }} </td>
                                            <td style="text-align: left;">{{ $value['proprietor_name'] }} </td>
                                            <td style="text-align: left;">{{ $value['address'] }}</td>
                                            <td style="text-align: center;">{{ $value['phone'] }}</td>
                                            <td style="text-align: right;">
                                                @if($value['balance'] < 0)
                                                <span style="color: red">{{ number_format($value['balance'],0,'.',',') }}</span>
                                                @else
                                                {{ number_format($value['balance'],0,'.',',') }}
                                                @endif
                                            </td>
                                        </tr>
                                         
                                        <?php $i++; ?>
                                        @endforeach

                                        <tr>
                                            <th colspan="6" style="text-align: right;">{{ __('messages.total')}}</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_balance,0,'.',',') }}</th>
                                        </tr>

                                    </tbody>
                                    
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        
        var site_url    = $('.site_url').val();
        var branch_ids  = $("#branch_id").val();

        $("#customer_id").select2({
            ajax: { 
            url:  site_url + '/invoices/customer/list/invoices',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['contact_type'] == 1 || result['id'] == 0)
                {
                    return result['text'];
                }
            },
        });
    });

    function pad (str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }
    
    function getBranchCustomers()
    {
        var site_url    = $('.site_url').val();
        var branch_id  = $("#branch_id").val();
         
        $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/reports/branch-contacts/get-contacts/' + branch_id,
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },
    
                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 1 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
        });
    }
</script>
@endsection