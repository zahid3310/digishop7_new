@extends('layouts.app')

@section('title', 'Dashboard')

<style type="text/css">
    .card-counter{
        box-shadow: 2px 2px 10px #DADADA;
        margin: 5px;
        padding: 20px 10px;
        background-color: #fff;
        height: 100px;
        border-radius: 5px;
        transition: .3s linear all;
    }

    .card-counter:hover{
        box-shadow: 4px 4px 20px #DADADA;
        transition: .3s linear all;
    }

    .card-counter.primary{
        background-color: #007bff;
        color: #FFF;
    }

    .card-counter.danger{
        background-color: #ef5350;
        color: #FFF;
    }  

    .card-counter.success{
        background-color: #66bb6a;
        color: #FFF;
    }  

    .card-counter.info{
        background-color: #26c6da;
        color: #FFF;
    }  

    .card-counter i{
        font-size: 5em;
        opacity: 0.2;
    }

    .card-counter .count-numbers{
        position: absolute;
        right: 35px;
        top: 20px;
        font-size: 32px;
        display: block;
    }

    .card-counter .count-name{
        position: absolute;
        right: 35px;
        top: 65px;
        font-style: italic;
        text-transform: capitalize;
        display: block;
        font-size: 18px;
    }

    @media screen and (max-width: 48em) {
        .row-offcanvas {
            position: relative;
            -webkit-transition: all 0.25s ease-out;
            -moz-transition: all 0.25s ease-out;
            transition: all 0.25s ease-out;
        }

        .row-offcanvas-left .sidebar-offcanvas {
            left: -33%;
        }

        .row-offcanvas-left.active {
            left: 33%;
            margin-left: -6px;
        }

        .sidebar-offcanvas {
            position: absolute;
            top: 0;
            width: 33%;
            height: 100%;
        }
    }

    @media screen and (max-width: 34em) {
        .row-offcanvas-left .sidebar-offcanvas {
            left: -45%;
        }

        .row-offcanvas-left.active {
            left: 45%;
            margin-left: -6px;
        }

        .sidebar-offcanvas {
            width: 45%;
        }
    }
    
    .card {
        overflow: hidden;
    }
    
    .card-block .rotate {
        z-index: 8;
        float: right;
        height: 100%;
    }
    
    .card-block .rotate i {
        color: rgba(20, 20, 20, 0.15);
        position: absolute;
        left: 0;
        left: auto;
        right: -10px;
        bottom: 0;
        display: block;
        -webkit-transform: rotate(-44deg);
        -moz-transform: rotate(-44deg);
        -o-transform: rotate(-44deg);
        -ms-transform: rotate(-44deg);
        transform: rotate(-44deg);
    }

    .display-1 {
        font-size: 2rem !important;
        color: white !important;
    }

    a, button {
        outline: 0!important;
        /*display: none;*/
    }

    .canvasjs-chart-credit
    {
        display: none !important;
    }

    .f-size
    {
        font-size: 25px !important;
    }

    .a-size
    {
        font-size: 15px !important;
    }
</style>

@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">{{ __('messages.dashboard')}}</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.dashboard')}}</a></li>
                                <li class="breadcrumb-item active">{{ __('messages.dashboard')}}</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <!-- <div class="row">
                <div class="col-md-3">
                    <div class="card-counter success">
                        <i class="fa fa-database"></i>
                        <span id="todaysSales" class="count-numbers f-size">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">{{ __('messages.today_sale')}}</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter info">
                        <i class="fa fa-users"></i>
                        <span id="todaysSalesReturn" class="count-numbers f-size">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">{{ __('messages.today_sale_return')}}</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter success">
                        <i class="fa fa-database"></i>
                        <span id="todaysPurchase" class="count-numbers f-size">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">{{ __('messages.todays_purchase')}}</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter primary">
                        <i class="fa fa-envelope"></i>

                        <?php
                            $url        = "https://isms.mimsms.com/miscapi/C200115962790185531cd6.84787578/getBalance";
                            $response   = @file_get_contents($url);
                            $tmt        = substr($response, strrpos($response, ' ' ) + 1);
                            
                          
         
                            if($tmt == 1003)
                            {
                               $blance = 0; 
                            }else
                            {
                               $blance = $tmt;
                            }
                        ?>

                        <span class="count-numbers f-size">
                            <h4 style="color: white" class="mb-0">{{round($blance/0.30)}}</h4>
                        </span>
                        <span class="count-name a-size">{{ __('messages.sms_balance')}}</span>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            var site_url  = $('.site_url').val();

            $.get(site_url + '/dashboard/items', function(data){
            });
        });
    </script>
@endsection

@push('scripts')
    <!-- echarts js -->
    <script src="{{ url('public/admin_panel_assets/libs/echarts/echarts.min.js') }}"></script>
    <!-- echarts init -->
    <script src="{{ url('public/admin_panel_assets/js/pages/echarts.init.js') }}"></script>
    <!-- <script src="{{ url('public/admin_panel_assets/js/canvasCharts.min.js') }}"></script> -->
@endpush