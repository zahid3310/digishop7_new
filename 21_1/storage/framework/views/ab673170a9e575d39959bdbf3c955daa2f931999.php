

<?php $__env->startSection('title', 'Stock Transfer'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Transfer Stock </h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Stock Transfer</a></li>
                                    <li class="breadcrumb-item active">Stock Transfer</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Transfers</h4>

                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Date</th>
                                            <th>Transfer From</th>
                                            <th>Transfer To</th>
                                            <th>Note</th>
                                            <th style="text-align: center">Quantity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                       
                                        
                                        <?php if(!empty($transfers) && ($transfers->count() > 0)): ?>
                                        <?php $__currentLoopData = $transfers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $transfer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e(date('d-m-Y', strtotime($transfer['date']))); ?></td>
                                                <td><?php echo e($transfer->transfer_from != null ? $transfer->transferFrom->name : ''); ?></td>
                                                <td><?php echo e($transfer->transfer_to != null ? $transfer->transferTo->name : ''); ?></td>
                                                <td><?php echo e($transfer->note); ?></td>
                                                <td style="text-align: center"><?php echo e($transfer->quantity); ?></td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <!-- <a class="dropdown-item" href="<?php echo e(route('stock_transfer_edit', $transfer['id'])); ?>">Edit</a> -->
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    $( document ).ready(function() {
        
        var site_url  = $('.site_url').val();

        $("#product_id").select2({
            ajax: { 
            url:  site_url + '/reports/sales-statement/product-list',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['id'] != 0)
                {
                    return result['text'];
                }
            },
        });
    });

    function getItemPrice()
    {
        //For getting item commission information from items table start
        var entry_id  = $("#product_id").val();
        var site_url  = $(".site_url").val();

        if(entry_id)
        {
            $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                var list    = '';
                $.each(data.unit_conversions, function(i, data_list)
                {   
                    list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                });

                $("#unit_id").empty();
                $("#unit_id").append(list);

                $("#main_unit_id").val(data.product_entries.unit_id);
            });
        }
        //For getting item commission information from items table end
    }   
</script>

<script type="text/javascript">
    function preventDoubleClick()
    {
        $('.enableOnInput').prop('disabled', true)
        $('#FormSubmit').submit();
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/21_1/Modules/StockTransfers/Resources/views/show.blade.php ENDPATH**/ ?>