<!DOCTYPE html>
<html>

<head>
    <title>Supplier Ledger Details</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>  
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: center;line-height: .8">
                        <p style="font-size: 40px"><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p style="font-size: 20px">{{ $user_info['address'] }}</p>
                        <p style="font-size: 20px">{{ $user_info['contact_number'] }}</p>
                        <p style="font-size: 20px">{{ $user_info['contact_email'] }}</p>
                        <p style="font-size: 14px;text-align: right">{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Ledger Details</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center;font-size: 12px !important">Date From</th>
                                    <th style="text-align: center;font-size: 12px !important">Supplier Name</th>
                                    <th style="text-align: center;font-size: 12px !important">Supplier Phone</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center;font-size: 12px !important">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                    
                                    <td style="text-align: center;font-size: 12px !important">
                                        @if($customer_name != null)
                                            {{ $customer_name['name'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                    
                                    <td style="text-align: center;font-size: 12px !important">
                                        @if($customer_name != null)
                                            {{ $customer_name['phone'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 10%;font-size: 12px !important">Date</th>
                                    <th style="text-align: center;width: 10%;font-size: 12px !important">Details</th>
                                    <th style="text-align: center;width: 10%;font-size: 12px !important">Transaction#</th>
                                    <th style="text-align: center;width: 10%;font-size: 12px !important">Description</th>
                                    <th style="text-align: center;width: 10%;font-size: 12px !important">Paid Through</th>
                                    <th style="text-align: center;width: 10%;font-size: 12px !important">Payable</th>
                                    <th style="text-align: center;width: 10%;font-size: 12px !important">Paid</th>
                                    <th style="text-align: center;width: 10%;font-size: 12px !important">Balance</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr>
                                    <td style="text-align: right;font-size: 12px !important" colspan="5"><strong>Opening Balance</strong></a>
                                    </td>
                                    <td style="text-align: right;font-size: 12px !important"><strong>{{ $opening_balance }}</strong></td>
                                    <td style="text-align: center;font-size: 12px !important"></td>
                                    <td style="text-align: right;font-size: 12px !important"><strong>{{ $opening_balance }}</strong></td>
                                </tr>

                                <?php
                                    $i           = 1;
                                    $sub_total   = 0;
                                ?>
                                @if($data->count() > 0)
                                @foreach($data as $key => $value)

                                    <tr>
                                        <?php
                                            if ($value['type'] == 0)
                                            {
                                                $debit  = $value['amount'];
                                                $credit = 0;
                                            }
                                            else
                                            {
                                                $credit = $value['amount'];
                                                $debit  = 0;
                                            }

                                            if ($value['account_head'] == 'supplier-advance-adjustment')
                                            {
                                                $sub_total      = $sub_total + $debit + $credit;
                                            }
                                            else
                                            {
                                                $sub_total      = $sub_total + $debit - $credit;
                                            }

                                            if ($value['account_head'] == 'purchase')
                                            {
                                                $trans_number   = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                            }
                                            elseif ($value['account_head'] == 'previoue-due-paid')
                                            {
                                                $trans_number   = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                            }
                                            elseif ($value['account_head'] == 'purchase-return')
                                            {
                                                $trans_number   = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                            }
                                            elseif ($value['account_head'] == 'purchase')
                                            {
                                                $trans_number   = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                            }
                                            elseif ($value['account_head'] == 'previoue-due-paid')
                                            {
                                                $trans_number   = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                            }
                                            elseif ($value['account_head'] == 'purchase-return')
                                            {
                                                $trans_number   = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                            }
                                            elseif ($value['account_head'] == 'payment')
                                            {
                                                $trans_number   = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                            }
                                            elseif ($value['account_head'] == 'customer-opening-balance')
                                            {
                                                $trans_number   = '';
                                            }
                                            elseif ($value['account_head'] == 'supplier-opening-balance')
                                            {
                                                $trans_number   = '';
                                            }
                                            elseif ($value['account_head'] == 'supplier-advance-adjustment')
                                            {
                                                $trans_number   = '';
                                            }
                                            else {
                                                $trans_number   = '';
                                            }
                                        ?>

                                        <td style="text-align: center;font-size: 12px !important">{{ date('d-m-Y', strtotime($value['date'])) }}</a>
                                        </td>

                                        <td style="text-align: center;font-size: 12px !important">{{ $value['note'] }}</td>
                                        <td style="text-align: center;font-size: 12px !important">{{ $trans_number }}</td>
                                        <td style="text-align: left;font-size: 12px !important">
                                            @if($value->invoice_id != null)
                                            <?php $detailsData = invoiceDetails($value->invoice_id); ?>
                                            @foreach($detailsData as $key1 => $detail_data)
                                            <strong>Item Name : </strong> {{ $detail_data->productEntries->name }}, 
                                            <strong>Qty : </strong> {{ $detail_data->quantity }},
                                            <strong>Unit Price : </strong> {{ $detail_data->rate }},
                                            <strong>Shipping Cost : </strong> {{ $detail_data->shipping_cost != null ? $detail_data->shipping_cost : 0 }},
                                            <strong>Total Price : </strong> {{ $detail_data->total_amount }}
                                            <hr style="margin: 0px">
                                            @endforeach
                                            @endif
                                        </td>
                                        <td style="text-align: center;font-size: 12px !important">{{ $value->paid_through_id != null ? $value->paidThroughAccounts->name : '' }}</td>
                                        <td style="text-align: right;font-size: 12px !important">{{ number_format($debit,2,'.',',') }}</td>
                                        <td style="text-align: right;font-size: 12px !important">{{ number_format($credit,2,'.',',') }}</td>
                                        <td style="text-align: right;font-size: 12px !important">{{ number_format($sub_total,2,'.',',') }}</td>
                                    </tr>
                                 
                                <?php $i++; ?>
                                @endforeach
                                @endif
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="7" style="text-align: right;font-size: 12px !important">TOTAL</th>
                                    <th colspan="1" style="text-align: right;font-size: 12px !important">{{ number_format($sub_total,2,'.',',') }}</th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>