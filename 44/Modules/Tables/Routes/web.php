<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('tables')->group(function() {
    Route::get('/', 'TablesController@index')->name('tables_index');
    Route::post('/store', 'TablesController@store')->name('tables_store');
    Route::get('/edit/{id}', 'TablesController@edit')->name('tables_edit');
    Route::post('/update/{id}', 'TablesController@update')->name('tables_update');
});

Route::prefix('tables/order')->group(function() {
    Route::get('/', 'TablesController@tableOrderIndex')->name('tables_order_index');
});
