<?php

namespace Modules\Tables\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Tables;
use App\Models\Invoices;

class TablesController extends Controller
{
    public function tableOrderIndex()
    {   
        $tables     = Tables::orderBy('created_at', 'ASC')->get();
        $invoices   = Invoices::where('type', 3)->get();

        return view('tables::order.index', compact('tables', 'invoices'));
    }

    public function index()
    {
        $tables     = Tables::orderBy('created_at', 'ASC')->get();

        return view('tables::tables.index', compact('tables'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'  => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $table               = new Tables;
            $table->name         = $data['name'];
            $table->status       = $data['status'];
            $table->created_by   = $user_id;

            if ($table->save())
            {   
                return back()->with("success","Table Created Successfully !!");
            }else
            {
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){

            return back()->with("unsuccess","Not Added");
        }
    }

    public function edit($id)
    {
        $tables         = Tables::orderBy('created_at', 'ASC')->get();
        $find_table     = Tables::find($id);

        return view('tables::tables.edit', compact('tables', 'find_table'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'name'  => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $table               = Tables::find($id);
            $table->name         = $data['name'];
            $table->status       = $data['status'];
            $table->updated_by   = $user_id;

            if ($table->save())
            {   
                return redirect()->route('tables_index')->with("success","Table Updated Successfully !!");
            }else
            {
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){

            return back()->with("unsuccess","Not Added");
        }
    }
}
