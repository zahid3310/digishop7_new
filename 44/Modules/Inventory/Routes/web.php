<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('inventory')->group(function() {
    Route::get('/', 'InventoryController@index')->name('daily_use');
    Route::get('/use-list', 'InventoryController@getUseList')->name('all_use_list');
    Route::post('/inventory-store', 'InventoryController@store')->name('inventory_store');
    Route::get('/inventory/show/{id}', 'InventoryController@inventoryShow')->name('inventory_show');
    Route::get('/inventory-statement', 'InventoryController@inventoryStatement')->name('inventory_statement_index');
    Route::get('/product-wise-summary', 'InventoryController@inventoryProductWiseStatement')->name('inventory_product_wise_summary_index');

    Route::get('/product-wise-summary-report', 'InventoryController@inventoryProductWiseStatementReport')->name('product_wise_use_report_print');

    Route::get('/inventory-summary', 'InventoryController@inventorySummary')->name('inventory_summary_index');
    Route::get('/inventory-statement-print', 'InventoryController@inventoryStatementPrint')->name('inventory_statement_print');
});
