<?php

namespace Modules\SrIssues\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\PaidThroughAccounts;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Users;
use App\Models\Issues;
use App\Models\IssueEntries;
use App\Models\SrItems;
use App\Models\IssueReturn;
use App\Models\IssueReturnEntries;
use App\Models\UnitConversions;
use Response;
use DB;
use View;
use Carbon\Carbon;

class SrIssuesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $issues           = Issues::orderBy('id', 'DESC')->first();

        return view('srissues::index', compact('issues'));
    }

    public function AllIssues()
    {
        return view('srissues::all_issues');
    }

    public function store(Request $request)
    {
        $rules = array(
            'issue_date'            => 'required',
            'sr_id'                 => 'required',
            'product_entries.*'     => 'required',
            'quantity.*'            => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $data_find              = Issues::orderBy('created_at', 'DESC')->first();
            $issue_number           = $data_find != null ? $data_find['issue_number'] + 1 : 1;

            $issue                  = new Issues;;
            $issue->issue_number    = $issue_number;
            $issue->sr_id           = $data['sr_id'];
            $issue->issue_date      = date('Y-m-d', strtotime($data['issue_date']));
            $issue->total_quantity  = $data['total_quantity'];
            $issue->issue_note      = $data['issue_note'];
            $issue->created_by      = $user_id;

            if ($issue->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $issue_entries[] = [
                        'issue_id'           => $issue['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'sr_id'              => $data['sr_id'],
                        'quantity'           => $data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];

                    $conversion_rate_find    = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key])
                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                        ->where('product_entry_id', $value)
                                        ->first();

                    $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $data['quantity'][$key]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key];

                    $sr_items[] = [
                        'issue_id'          => $issue['id'],
                        'sr_id'             => $data['sr_id'],
                        'date'              => date('Y-m-d', strtotime($data['issue_date'])),
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'type'              => 1,
                        'quantity'          => $converted_quantity_to_main_unit,
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];

                    //Decrease Stock Start
                        $product_buy_price->stock_in_hand     = $product_buy_price['stock_in_hand'] - $converted_quantity_to_main_unit;
                        $product_buy_price->total_sold        = $product_buy_price['total_sold'] + $converted_quantity_to_main_unit;
                        $product_buy_price->save();
                    //Decrease Stock End
                }

                DB::table('issue_entries')->insert($issue_entries);
                DB::table('sr_items')->insert($sr_items);

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Issue Created Successfully !!");
                }
                else
                {
                    $issue      = Issues::leftjoin('customers', 'customers.id', 'issues.sr_id')
                                            ->select('issues.*',
                                                     'customers.name as sr_name',
                                                     'customers.address as address',
                                                     'customers.phone as phone')
                                            ->find($issue['id']);

                    $entries    = IssueEntries::leftjoin('products', 'products.id', 'issue_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'issue_entries.product_entry_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->where('issue_entries.issue_id', $issue['id'])
                                            ->select('issue_entries.*',
                                                     'product_entries.product_type as product_type',
                                                     'product_entries.name as product_entry_name',
                                                     'units.name as unit_name',
                                                     'products.name as product_name')
                                            ->get();  
                                 
                    $user_info  = Users::find(1);

                    return View::make('srissues::show')->with("issue", $issue)->with("entries", $entries)->with("user_info", $user_info);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        $issue      = Issues::leftjoin('customers', 'customers.id', 'issues.sr_id')
                                ->select('issues.*',
                                         'customers.name as sr_name',
                                         'customers.address as address',
                                         'customers.phone as phone')
                                ->find($id);

        $entries    = IssueEntries::leftjoin('products', 'products.id', 'issue_entries.product_id')
                                ->leftjoin('product_entries', 'product_entries.id', 'issue_entries.product_entry_id')
                                ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                ->where('issue_entries.issue_id', $id)
                                ->select('issue_entries.*',
                                         'product_entries.product_type as product_type',
                                         'product_entries.name as product_entry_name',
                                         'units.name as unit_name',
                                         'products.name as product_name')
                                ->get();  
                     
        $user_info  = Users::find(1);;

        return view('srissues::show', compact('entries', 'issue', 'user_info'));
    }

    public function edit($id)
    {
        $products               = Products::where('products.id', '!=', 1)
                                            ->orderBy('products.total_sold', 'DESC')
                                            ->get();

        $product_entry          = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->where('product_entries.product_id', '!=', 1)
                                            ->where('product_entries.stock_in_hand', '!=', null)
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                         GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                         GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                         GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                         GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                         GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                         GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                         GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                         GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT products.id) as product_id
                                                        ')
                                            ->orderBy('product_entries.total_sold', 'DESC')
                                            ->get();

        $product_entry          = $product_entry->sortBy('name')->all();
        $product_entries        = collect($product_entry);

        $find_issue             = Issues::leftjoin('customers', 'customers.id', 'issues.sr_id')
                                            ->select('issues.*',
                                                 'customers.id as sr_id',
                                                 'customers.name as sr_name')
                                            ->find($id);

        $find_issue_entries     = IssueEntries::leftjoin('customers', 'customers.id', 'issue_entries.sr_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'issue_entries.product_entry_id')
                                            ->where('issue_entries.issue_id', $id)
                                            ->select('issue_entries.*',
                                                    'customers.id as sr_id',
                                                    'customers.name as sr_name',
                                                    'product_entries.id as item_id',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.name as item_name')
                                            ->get();

        $entries_count          = $find_issue_entries->count();
        
        return view('srissues::edit', compact('products', 'product_entries', 'find_issue', 'find_issue_entries', 'entries_count'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'issue_date'            => 'required',
            'sr_id'                 => 'required',
            'product_entries.*'     => 'required',
            'quantity.*'            => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $issue                  = Issues::find($id);
            $issue->sr_id           = $data['sr_id'];
            $issue->issue_date      = date('Y-m-d', strtotime($data['issue_date']));
            $issue->total_quantity  = $data['total_quantity'];
            $issue->issue_note      = $data['issue_note'];
            $issue->updated_by      = $user_id;

            if ($issue->save())
            {
                $issue_entry        = IssueEntries::where('issue_id', $issue['id'])->get();
                $issue_entry_delete = IssueEntries::where('issue_id', $issue['id'])->delete();
                $sr_items_delete    = SrItems::where('issue_id', $issue['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $issue_entries[] = [
                        'issue_id'           => $issue['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'sr_id'              => $data['sr_id'],
                        'quantity'           => $data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];

                    $conversion_rate_find    = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key])
                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                        ->where('product_entry_id', $value)
                                        ->first();

                    $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $data['quantity'][$key]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key];

                    $sr_items[] = [
                        'issue_id'          => $issue['id'],
                        'sr_id'             => $data['sr_id'],
                        'date'              => date('Y-m-d', strtotime($data['issue_date'])),
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'type'              => 1,
                        'quantity'          => $converted_quantity_to_main_unit,
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('issue_entries')->insert($issue_entries);
                DB::table('sr_items')->insert($sr_items);

                //Decrease Stock Start
                    foreach ($issue_entry as $key => $value)
                    {
                        $old_item_entry_id[]      = $value['product_entry_id'];
                        $old_main_unit_id[]       = $value['main_unit_id'];
                        $old_conversion_unit_id[] = $value['conversion_unit_id'];
                        $old_items_stock[]        = $value['quantity'];
                    }

                    foreach ($old_item_entry_id as $key2 => $value2)
                    {   

                        $old_conversion_rate_find    = UnitConversions::where('main_unit_id', $old_main_unit_id[$key2])
                                        ->where('converted_unit_id', $old_conversion_unit_id[$key2])
                                        ->where('product_entry_id', $value2)
                                        ->first();

                        $old_converted_quantity_to_main_unit          = $old_conversion_rate_find != null ? $old_items_stock[$key2]/$old_conversion_rate_find['conversion_rate'] : $old_items_stock[$key2];
                        
                        $quantity_add_to_product_entry                = ProductEntries::find($value2);
                        $quantity_add_to_product_entry->stock_in_hand = $quantity_add_to_product_entry['stock_in_hand'] + $old_converted_quantity_to_main_unit;
                        $quantity_add_to_product_entry->total_sold    = $quantity_add_to_product_entry['total_sold'] - $old_converted_quantity_to_main_unit;
                        $quantity_add_to_product_entry->save();
                    }

                    foreach ($data['product_entries'] as $key4 => $value4)
                    {   
                        $new_conversion_rate_find    = UnitConversions::where('main_unit_id', $old_main_unit_id[$key4])
                                        ->where('converted_unit_id', $old_conversion_unit_id[$key4])
                                        ->where('product_entry_id', $value4)
                                        ->first();

                        $new_converted_quantity_to_main_unit = $new_conversion_rate_find != null ? $data['quantity'][$key4]/$new_conversion_rate_find['conversion_rate'] : $data['quantity'][$key4];
                        
                        $product_entries                     = ProductEntries::find($value4);
                        $product_entries->stock_in_hand      = $product_entries['stock_in_hand'] - $new_converted_quantity_to_main_unit;
                        $product_entries->total_sold         = $product_entries['total_sold'] + $new_converted_quantity_to_main_unit;
                        $product_entries->save();
                    }
                //Decrease Stock End

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return redirect()->route('issues_index')->with("success","Issue Updated Successfully !!");
                }
                else
                {
                    $issue      = Issues::leftjoin('customers', 'customers.id', 'issues.sr_id')
                                            ->select('issues.*',
                                                     'customers.name as sr_name',
                                                     'customers.address as address',
                                                     'customers.phone as phone')
                                            ->find($issue['id']);

                    $entries    = IssueEntries::leftjoin('products', 'products.id', 'issue_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'issue_entries.product_entry_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->where('issue_entries.issue_id', $issue['id'])
                                            ->select('issue_entries.*',
                                                     'product_entries.product_type as product_type',
                                                     'product_entries.name as product_entry_name',
                                                     'units.name as unit_name',
                                                     'products.name as product_name')
                                            ->get();  
                                 
                    $user_info  = Users::find(1);

                    return View::make('srissues::show')->with("issue", $issue)->with("entries", $entries)->with("user_info", $user_info);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function calculateOpeningBalance($sr_id)
    {
        $data       = SrItems::where('sr_items.sr_id', $sr_id)
                                    ->groupBy('sr_items.sr_id')
                                    ->select(DB::raw("SUM(IF(sr_items.type='1',sr_items.quantity,0)) AS receive_quantity"),
                                             DB::raw("SUM(IF(sr_items.type='2',sr_items.quantity,0)) AS sold_quantity"),
                                             DB::raw("SUM(IF( sr_items.type='3',sr_items.quantity,0)) AS return_quantity")
                                            )
                                    ->first();

        $balance    = $data['receive_quantity'] - $data['sold_quantity'] - $data['return_quantity']; 

        return Response::json($balance);
    }

    public function issuesListLoad()
    {
        $user_id        = Auth::user()->id;
        $data           = Issues::leftjoin('issue_entries', 'issue_entries.issue_id', 'issues.id')
                                    ->leftjoin('customers', 'customers.id', 'issues.sr_id')
                                    ->leftjoin('users', 'users.id', 'issues.created_by')
                                    ->where('issues.created_by', $user_id)
                                    ->orderBy('issues.created_at', 'DESC')
                                    ->select('issues.*',
                                            'customers.name as sr_name',
                                            'customers.phone as phone',
                                            'users.name as user_name')
                                    ->distinct('issues.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function issueListSearch($from_date, $to_date, $sr, $issue)
    {
        $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
        $search_by_sr           = $sr != 0 ? $sr : 0;
        $search_by_issue        = $issue != 0 ? $issue : 0;

        $data           = Issues::leftjoin('customers', 'customers.id', 'issues.sr_id')
                                        ->leftjoin('issue_entries', 'issue_entries.issue_id', 'issues.id')
                                        ->whereBetween('issues.issue_date', [$search_by_from_date, $search_by_to_date])
                                        ->when($search_by_sr != 0, function ($query) use ($search_by_sr) {
                                            return $query->where('customers.id', $search_by_sr);
                                        })
                                        ->when($search_by_issue != 0, function ($query) use ($search_by_issue) {
                                            return $query->where('issues.id', $search_by_issue);
                                        })
                                        ->orderBy('issues.created_at', 'DESC')
                                        ->select('issues.*',
                                                 'customers.name as sr_name',
                                                 'customers.phone as phone')
                                        ->distinct('issues.id')
                                        ->take(20)
                                        ->get();

        return Response::json($data);
    }

    public function printIssuesSearch($date, $sr, $issue)
    {
        $search_by_date         = $date != 0 ? date('Y-m-d', strtotime($date)) : 0;
        $search_by_sr           = $sr != 0 ? $sr : 0;
        $search_by_issue        = $issue != 0 ? $issue : 0;

        $data           = Issues::leftjoin('customers', 'customers.id', 'issues.sr_id')
                                        ->leftjoin('issue_entries', 'issue_entries.issue_id', 'issues.id')
                                        ->leftjoin('users', 'users.id', 'issues.created_by')
                                        ->where('issues.issue_date', $search_by_date)
                                        ->when($search_by_sr != 0, function ($query) use ($search_by_sr) {
                                            return $query->where('customers.id', $search_by_sr);
                                        })
                                        ->when($search_by_issue != 0, function ($query) use ($search_by_issue) {
                                            return $query->where('issues.id', $search_by_issue);
                                        })
                                        ->orderBy('issues.created_at', 'DESC')
                                        ->select('issues.*',
                                                 'customers.name as sr_name',
                                                 'users.name as user_name',
                                                 'customers.phone as phone')
                                        ->distinct('issues.id')
                                        ->take(20)
                                        ->get();

        return Response::json($data);
    }

    public function printIssuesList()
    {
        $user_id        = Auth::user()->id;
        $data           = Issues::leftjoin('issue_entries', 'issue_entries.issue_id', 'issues.id')
                                    ->leftjoin('customers', 'customers.id', 'issues.sr_id')
                                    ->leftjoin('users', 'users.id', 'issues.created_by')
                                    ->where('issues.created_by', $user_id)
                                    ->orderBy('issues.created_at', 'DESC')
                                    ->select('issues.*',
                                            'customers.name as sr_name',
                                            'customers.phone as phone',
                                            'users.name as user_name')
                                    ->distinct('issues.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function issuesList()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Issues::orderBy('issues.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Issues::where('issues.issue_number', 'LIKE', "%$search%")
                                    ->orderBy('issues.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            $issueNumber  = 'SIN - '.str_pad($value['issue_number'], 6, "0", STR_PAD_LEFT);

            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[]         = array("id"=>$value['id'], "text"=>$issueNumber);

            $i++;
        }
   
        return Response::json($data);
    }

    //Return Start
        public function returnIssues()
        {
            return view('srissues::return');
        }

        public function returnIssueProductList($from_dates, $to_dates, $sr_id, $issue_id)
        {
            $date       = date('Y-m-d');
            $from_date  = date('Y-m-d', strtotime($from_dates));
            $to_date    = date('Y-m-d', strtotime($to_dates));

            $data['return_list']     = SrItems::leftjoin('issues', 'issues.id', 'sr_items.issue_id')
                                            ->leftjoin('customers', 'customers.id', 'sr_items.sr_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'sr_items.product_entry_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->whereBetween('sr_items.date', [$from_date, $to_date])
                                            ->where('sr_items.sr_id', $sr_id)
                                            ->when($issue_id != 0, function ($query) use ($issue_id) {
                                                return $query->where('sr_items.issue_id', $issue_id);
                                            })
                                            ->groupBy('sr_items.product_entry_id')
                                            ->select(DB::raw('group_concat(distinct sr_items.product_entry_id) as product_entry_id'),
                                                     DB::raw('group_concat(distinct product_entries.name) as product_name'),
                                                     DB::raw('group_concat(distinct units.id) as unit_id'),
                                                     DB::raw('group_concat(distinct units.name) as unit_name'),
                                                     DB::raw("SUM(IF(sr_items.type='1',sr_items.quantity,0)) AS receive_quantity"),
                                                     DB::raw("SUM(IF(sr_items.type='2',sr_items.quantity,0)) AS sold_quantity"),
                                                     DB::raw("SUM(IF( sr_items.type='3',sr_items.quantity,0)) AS return_quantity")
                                                    )
                                            ->get();
            
            foreach ($data['return_list'] as $key => $value)
            {
                $d1                      = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                        ->where('product_entries.id', $value['product_entry_id'])
                                                        ->selectRaw('units.id as unit_id, units.name as unit_name')
                                                        ->get()
                                                        ->toArray();

                $d2                      = UnitConversions::leftjoin('units', 'units.id', 'unit_conversions.converted_unit_id')
                                                        ->where('unit_conversions.product_entry_id', $value['product_entry_id'])
                                                        ->selectRaw('unit_conversions.converted_unit_id as unit_id, units.name as unit_name')
                                                        ->get()
                                                        ->toArray();

                $data[$value['product_entry_id']]['unit_data'] = collect(array_merge($d1, $d2));
            }

            return Response::json($data);
        }

        public function returnIssueStore(Request $request)
        {
            $rules = array(
                'return_date'   => 'required',
                'sr_id'         => 'required',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id    = Auth::user()->id;
            $data       = $request->all();
 
            DB::beginTransaction();

            try{
                $data_find                     = IssueReturn::orderBy('id', 'DESC')->first();
                $return_number                 = $data_find != null ? $data_find['return_number'] + 1 : 1;

                $issue_return                  = new IssueReturn;;
                $issue_return->return_number   = $return_number;
                $issue_return->date            = date('Y-m-d', strtotime($data['return_date']));
                $issue_return->issue_id        = $data['issue_id'] != 0 ? $data['issue_id'] : null;
                $issue_return->sr_id           = $data['sr_id'];
                $issue_return->total_quantity  = array_sum($data['return_quantity']);
                $issue_return->created_by      = $user_id;

                if ($issue_return->save())
                {
                    foreach ($data['product_entries'] as $key => $value)
                    {   
                        if ($data['return_quantity'][$key] != 0)
                        {
                            $product    = ProductEntries::find($value);

                            $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key])
                                        ->where('converted_unit_id', $data['converted_unit_id'][$key])
                                        ->where('product_entry_id', $value)
                                        ->first();

                            $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $data['return_quantity'][$key]/$conversion_rate_find['conversion_rate'] : $data['return_quantity'][$key];

                            $issue_return_entries[] = [
                                'issue_return_id'    => $issue_return['id'],
                                'issue_id'           => $data['issue_id'] != 0 ? $data['issue_id'] : null,
                                'sr_id'              => $data['sr_id'],
                                'product_id'         => $product['product_id'],
                                'product_entry_id'   => $value,
                                'main_unit_id'       => $data['main_unit_id'][$key],
                                'conversion_unit_id' => $data['converted_unit_id'][$key],
                                'quantity'           => $converted_quantity_to_main_unit,
                                'created_by'         => $user_id,
                                'created_at'         => date('Y-m-d H:i:s'),
                            ];

                            $sr_items[] = [
                                'issue_return_id'   => $issue_return['id'],
                                'issue_id'          => $data['issue_id'] != 0 ? $data['issue_id'] : null,
                                'sr_id'             => $data['sr_id'],
                                'date'              => date('Y-m-d', strtotime($data['return_date'])),
                                'product_id'        => $product['product_id'],
                                'product_entry_id'  => $value,
                                'main_unit_id'       => $data['main_unit_id'][$key],
                                'conversion_unit_id' => $data['converted_unit_id'][$key],
                                'type'              => 3,
                                'quantity'          => $converted_quantity_to_main_unit,
                                'created_by'        => $user_id,
                                'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            //Increase Main Stock Start
                                $product->stock_in_hand     = $product['stock_in_hand'] + $converted_quantity_to_main_unit;
                                $product->total_sold        = $product['total_sold'] - $converted_quantity_to_main_unit;
                                $product->save();
                            //Increase Main Stock End
                        }
                    }

                    DB::table('issue_return_entries')->insert($issue_return_entries);
                    DB::table('sr_items')->insert($sr_items);

                    DB::commit();

                    if ($data['print'] == 1)
                    {
                        return back()->with("success","Products Received Successfully !!");
                    }
                    else
                    {
                       
                    }
                }
                else
                {
                    DB::rollback();
                    return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
                }

            }catch (\Exception $exception){
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }

        public function AllReturns()
        {
            return view('srissues::all_returns');
        }

        public function returnsListLoad()
        {
            $user_id        = Auth::user()->id;
            $data           = IssueReturn::leftjoin('issue_return_entries', 'issue_return_entries.issue_return_id', 'issue_return.id')
                                        ->leftjoin('customers', 'customers.id', 'issue_return.sr_id')
                                        ->leftjoin('users', 'users.id', 'issue_return.created_by')
                                        ->where('issue_return.created_by', $user_id)
                                        ->orderBy('issue_return.created_at', 'DESC')
                                        ->select('issue_return.*',
                                                'customers.name as sr_name',
                                                'customers.phone as phone',
                                                'users.name as user_name')
                                        ->distinct('issue_return.id')
                                        ->take(100)
                                        ->get();

            return Response::json($data);
        }

        public function returnsListSearch($from_date, $to_date, $sr, $issue)
        {
            $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
            $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
            $search_by_sr           = $sr != 0 ? $sr : 0;
            $search_by_issue        = $issue != 0 ? $issue : 0;

            $data           = Issues::leftjoin('customers', 'customers.id', 'issues.sr_id')
                                            ->leftjoin('issue_entries', 'issue_entries.issue_id', 'issues.id')
                                            ->whereBetween('issues.issue_date', [$search_by_from_date, $search_by_to_date])
                                            ->when($search_by_sr != 0, function ($query) use ($search_by_sr) {
                                                return $query->where('customers.id', $search_by_sr);
                                            })
                                            ->when($search_by_issue != 0, function ($query) use ($search_by_issue) {
                                                return $query->where('issues.id', $search_by_issue);
                                            })
                                            ->orderBy('issues.created_at', 'DESC')
                                            ->select('issues.*',
                                                     'customers.name as sr_name',
                                                     'customers.phone as phone')
                                            ->distinct('issues.id')
                                            ->take(20)
                                            ->get();

            return Response::json($data);
        }

        public function issueReturnList()
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = IssueReturn::orderBy('issue_return.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = IssueReturn::where('issue_return.return_number', 'LIKE', "%$search%")
                                        ->orderBy('issue_return.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }

            $data = array();
            $i    = 0;
            foreach ($fetchData as $key => $value)
            {   
                $returnNumber  = 'IRN - '.str_pad($value['return_number'], 6, "0", STR_PAD_LEFT);

                if ($i == 0)
                {
                    $data[] = array("id"=>0, "text"=>'All');
                }

                $data[]         = array("id"=>$value['id'], "text"=>$returnNumber);

                $i++;
            }
       
            return Response::json($data);
        }

        public function allReturnsListSearch($from_date, $to_date, $sr, $issue)
        {
            $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
            $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
            $search_by_sr           = $sr != 0 ? $sr : 0;
            $search_by_issue        = $issue != 0 ? $issue : 0;

            $data           = IssueReturn::leftjoin('customers', 'customers.id', 'issue_return.sr_id')
                                            ->leftjoin('issue_return_entries', 'issue_return_entries.issue_return_id', 'issue_return.id')
                                            ->whereBetween('issue_return.date', [$search_by_from_date, $search_by_to_date])
                                            ->when($search_by_sr != 0, function ($query) use ($search_by_sr) {
                                                return $query->where('customers.id', $search_by_sr);
                                            })
                                            ->when($search_by_issue != 0, function ($query) use ($search_by_issue) {
                                                return $query->where('issue_return.id', $search_by_issue);
                                            })
                                            ->orderBy('issue_return.created_at', 'DESC')
                                            ->select('issue_return.*',
                                                     'customers.name as sr_name',
                                                     'customers.phone as phone')
                                            ->distinct('issue_return.id')
                                            ->take(20)
                                            ->get();

            return Response::json($data);
        }

        public function editReturn($id)
        {
            $find_return            = IssueReturn::leftjoin('customers', 'customers.id', 'issue_return.sr_id')
                                        ->select('issue_return.*')
                                        ->find($id);

            $find_ireturn_entries   = IssueReturnEntries::leftjoin('customers', 'customers.id', 'issue_return_entries.sr_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'issue_return_entries.product_entry_id')
                                                ->leftjoin('issues', 'issues.id', 'issue_return_entries.issue_id')
                                                ->where('issue_return_entries.issue_return_id', $id)
                                                ->select('issue_return_entries.*',
                                                        'customers.id as sr_id',
                                                        'customers.name as sr_name',
                                                        'customers.name as sr_name',
                                                        'issues.id as issue_id',
                                                        'issues.issue_number as issue_number',
                                                        'product_entries.name as product_name')
                                                ->get();

            $entries_count          = $find_ireturn_entries->count();
            
            return view('srissues::edit_return', compact('find_return', 'find_ireturn_entries','entries_count'));
        }

        public function updateReturn(Request $request, $id)
        {
            $rules = array(
                'return_date'   => 'required',
                'sr_id'         => 'required',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                $issue_return                  = IssueReturn::find($id);
                $issue_return->date            = date('Y-m-d', strtotime($data['return_date']));
                $issue_return->issue_id        = $data['issue_id'] != 0 ? $data['issue_id'] : null;
                $issue_return->sr_id           = $data['sr_id'];
                $issue_return->total_quantity  = array_sum($data['return_quantity']);
                $issue_return->updated_by      = $user_id;

                if ($issue_return->save())
                {   
                    $old_issue_return_entries    = IssueReturnEntries::where('issue_return_id', $id)->get();
                    $delete_issue_return_entries = IssueReturnEntries::where('issue_return_id', $id)->delete();
                    $delete_sr_items             = SrItems::where('issue_return_id', $id)->delete();

                    foreach ($data['product_entries'] as $key => $value)
                    {   
                        if ($data['return_quantity'][$key] != 0)
                        {
                            $product    = ProductEntries::find($value);

                            $issue_return_entries[] = [
                                'issue_return_id'   => $issue_return['id'],
                                'issue_id'          => $data['issue_id'] != 0 ? $data['issue_id'] : null,
                                'sr_id'             => $data['sr_id'],
                                'product_id'        => $product['product_id'],
                                'product_entry_id'  => $value,
                                'quantity'          => $data['return_quantity'][$key],
                                'created_by'        => $user_id,
                                'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            $sr_items[] = [
                                'issue_return_id'   => $issue_return['id'],
                                'issue_id'          => $data['issue_id'] != 0 ? $data['issue_id'] : null,
                                'sr_id'             => $data['sr_id'],
                                'date'              => date('Y-m-d', strtotime($data['return_date'])),
                                'product_id'        => $product['product_id'],
                                'product_entry_id'  => $value,
                                'type'              => 3,
                                'quantity'          => $data['return_quantity'][$key],
                                'created_by'        => $user_id,
                                'created_at'        => date('Y-m-d H:i:s'),
                            ];
                        }
                    }

                    DB::table('issue_return_entries')->insert($issue_return_entries);
                    DB::table('sr_items')->insert($sr_items);

                    //Increase Main Stock Start
                        foreach ($old_issue_return_entries as $key => $value)
                        {
                            $old_item_entry_id[]    = $value['product_entry_id'];
                            $old_items_stock[]      = $value['quantity'];
                        }

                        foreach ($old_item_entry_id as $key2 => $value2)
                        { 
                            $quantity_add_to_product_entry                   = ProductEntries::find($value2);
                            $quantity_add_to_product_entry->stock_in_hand    = $quantity_add_to_product_entry['stock_in_hand'] - $old_items_stock[$key2];
                            $quantity_add_to_product_entry->total_sold       = $quantity_add_to_product_entry['total_sold'] + $old_items_stock[$key2];
                            $quantity_add_to_product_entry->save();
                        }

                        foreach ($data['product_entries'] as $key4 => $value4)
                        {
                            $product_entries                    = ProductEntries::find($value4);
                            $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] + $data['return_quantity'][$key4];
                            $product_entries->total_sold        = $product_entries['total_sold'] - $data['return_quantity'][$key4];
                            $product_entries->save();
                        }
                    //Increase Main Stock End

                    DB::commit();

                    if ($data['print'] == 1)
                    {
                        return back()->with("success","Products Received Successfully !!");
                    }
                    else
                    {
                       
                    }
                }
                else
                {
                    DB::rollback();
                    return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
                }

            }catch (\Exception $exception){
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }

        public function returnsConvertedUnitList($product_entry_id)
        {
            $data1                          = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                        ->where('product_entries.id', $product_entry_id)
                                                        ->selectRaw('units.id as unit_id, units.name as unit_name')
                                                        ->get()
                                                        ->toArray();

            $data2                          = UnitConversions::leftjoin('units', 'units.id', 'unit_conversions.converted_unit_id')
                                                        ->where('unit_conversions.product_entry_id', $product_entry_id)
                                                        ->selectRaw('unit_conversions.converted_unit_id as unit_id, units.name as unit_name')
                                                        ->get()
                                                        ->toArray();

            $data                           = collect(array_merge($data1, $data2));


            return Response::json($data);
        }
    //Return End
}
