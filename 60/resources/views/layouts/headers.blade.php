<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">{{ __('messages.menu')}}</li>
                @if(Auth::user()->dashboard == 1)
                <li>
                    <a href="{{ route('home') }}" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span>{{ __('messages.dashboard')}}</span>
                    </a>
                </li>
                @endif
                
                @if(Auth::user()->purchase == 1)
                <li class="{{ 
                Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-suitcase"></i><span>{{ __('messages.Pur/Rec_From_DP')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('bills_index') }}">{{ __('messages.Pur/Rec_Item')}}</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' }}" href="{{ route('bills_all_bills') }}">{{ __('messages.List_Pur/Rec')}}</a> </li>
                        <li> <a class="{{ Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' }}" href="{{ route('purchase_return_index') }}">{{ __('messages.Pur/Rec_Return')}}</a> </li>
                        <li> <a class="{{ Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' }}" href="{{ route('payments_create').'?payment_type=1' }}">{{ __('messages.bill_wise_payment')}}</a> </li>
                    </ul>
                </li>
                @endif
                
                
                @if(Auth::user()->purchase_order == 1)
                <li class="{{ 
                Route::currentRouteName() == 'purchase_orders_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_orders_show' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'purchase_orders_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_orders_show' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-suitcase"></i><span>{{ __('messages.purchase_order')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('purchase_orders_index') }}">{{ __('messages.create_purchase_order')}}</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'purchase_orders_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_orders_show' ? 'mm-active' : '' }}" href="{{ route('purchase_orders_all_po') }}">{{ __('messages.list_purchase_order')}}</a> </li>
                    </ul>
                </li>
                @endif
                
                
                @if(Auth::user()->issue_sm == 1)
                <li class="{{ 
                Route::currentRouteName() == 'issues_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'issues_return_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'issues_show' ? 'mm-active' : '' || Route::currentRouteName() == 'issues_return_show' ? 'mm-active' : '' }}">
                    <a class="{{ 
                            Route::currentRouteName() == 'issues_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'issues_return_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fa fa-baby-carriage"></i><span>{{ __('messages.issue_sm')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="" href="{{ route('issues_index') }}">{{ __('messages.Issue_Items_SM')}}</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'issues_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'issues_show' ? 'mm-active' : '' }}" href="{{ route('all_issues') }}">{{ __('messages.List_Issues')}}</a> </li>
                        <li> <a class="" href="{{ route('issues_return_issues') }}">{{ __('messages.Return_SM')}}</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'issues_return_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'issues_return_show' ? 'mm-active' : '' }}" href="{{ route('all_return_issues') }}">{{ __('messages.List_Returns')}}</a> </li>
                        <li> <a class="" href="{{ route('generate_invoice') }}">{{ __('messages.Generate_Invoice')}}</a> </li>
                    </ul>
                </li>
                @endif
                
                
                @if(Auth::user()->orders == 1)
                <li class="{{ 
                Route::currentRouteName() == 'orders_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'orders_show' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'orders_show_pos' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'orders_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'orders_show' ? 'mm-active' : '' || Route::currentRouteName() == 'orders_show_pos' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fa fa-clipboard"></i><span>{{ __('messages.order')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('orders_index') }}">{{ __('messages.order_from_customer')}}</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'orders_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'orders_show' ? 'mm-active' : '' || Route::currentRouteName() == 'orders_show_pos' ? 'mm-active' : '' }}" href="{{ route('orders_all_sales') }}">{{ __('messages.order_list')}}</a> </li>
                    </ul>
                </li>
                @endif
                
                
                @if(Auth::user()->sales == 1)
                <li class="{{ 
                Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-shopping-basket"></i><span>{{ __('messages.sales')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('invoices_index') }}">{{ __('messages.new_sales')}}</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' }}" href="{{ route('invoices_all_sales') }}">{{ __('messages.list_of_sales')}}</a> </li>
                        <li> <a class="{{ Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' }}"  href="{{ route('sales_return_index') }}">{{ __('messages.Return_From_Customer')}}</a> </li>
                        <li> <a class="{{ Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' }}" href="{{ route('payments_create').'?payment_type=0' }}">{{ __('messages.invoice_wise_collection')}}</a> </li>
                    </ul>
                </li>
                @endif
                
                
                @if(Auth::user()->damage == 1)
                <li class="{{ 
                Route::currentRouteName() == 'damages_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'damages_transfer_to_dp_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'damages_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'damages_transfer_to_dp_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-dumpster"></i><span>{{ __('messages.Damages')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">

                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.Direct_Damages')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('damages_create') }}">{{ __('messages.New_Damage')}}</a> </li>
                                <li> <a href="{{ route('damages_index') }}">{{ __('messages.List_Damages')}}</a> </li>
                            </ul>
                        </li>

                        <li class="{{ Route::currentRouteName() == 'damages_receive_from_customer_index' ? 'mm-active' : '' || Route::currentRouteName() == 'damages_receive_from_customer_create' ? 'mm-active' : '' || Route::currentRouteName() == 'damages_receive_from_customer_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'damages_receive_from_customer_transfer' ? 'mm-active' : '' }}">
                            <a class="has-arrow waves-effect {{ Route::currentRouteName() == 'damages_receive_from_customer_index' ? 'mm-active' : '' || Route::currentRouteName() == 'damages_receive_from_customer_create' ? 'mm-active' : '' || Route::currentRouteName() == 'damages_receive_from_customer_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'damages_receive_from_customer_transfer' ? 'mm-active' : '' }}">
                                {{ __('messages.Receive_From_SM/Cus')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('damages_receive_from_customer_create') }}">{{ __('messages.New_Receive')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'damages_receive_from_customer_edit' ? 'mm-active' : '' }}" href="{{ route('damages_receive_from_customer_index') }}">{{ __('messages.List_Receives')}}</a> </li>
                            </ul>
                        </li>

                        <li> <a class="{{ Route::currentRouteName() == 'damages_transfer_to_dp_edit' ? 'mm-active' : '' }}" href="{{ route('damages_transfer_to_dp_index') }}"> {{ __('messages.Transfer_DP/Supplier')}}</a> </li>
                        <li class="{{ Route::currentRouteName() == 'damages_receive_from_dp_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'damages_receive_from_dp_create' ? 'mm-active' : '' }}">
                            <a class="has-arrow waves-effect {{ Route::currentRouteName() == 'damages_receive_from_dp_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'damages_receive_from_dp_create' ? 'mm-active' : '' }}">
                                {{ __('messages.Receive_From DP/Sup')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('damages_receive_from_dp_create') }}">{{ __('messages.New_Receive')}}</a> </li>
                                <li> <a href="{{ route('damages_receive_from_dp_index') }}">{{ __('messages.List_Receives')}}</a> </li>
                            </ul>
                        </li>

                        <li class="{{ Route::currentRouteName() == 'damages_return_to_customer_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'damages_return_to_customer_create' ? 'mm-active' : '' }}">
                            <a class="has-arrow waves-effect {{ Route::currentRouteName() == 'damages_return_to_customer_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'damages_return_to_customer_create' ? 'mm-active' : '' }}">
                                {{ __('messages.Return_SM/Cus')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('damages_return_to_customer_create') }}">{{ __('messages.New_Return')}}</a> </li>
                                <li> <a href="{{ route('damages_return_to_customer_index') }}">{{ __('messages.Return_list')}}</a> </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                @endif
                
                
                @if(Auth::user()->accounts == 1)
                <li class="{{ 
                Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fab fa-adn"></i><span>{{ __('messages.accounts')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' }}" href="{{ route('expenses_index') }}">{{ __('messages.expenses')}}</a> </li>

                        <li> <a class="{{ 
                            Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' }}" href="{{ route('incomes_index') }}">{{ __('messages.incomes')}}</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : '' }}" href="{{ route('balance_transfer_index') }}">{{ __('messages.balance_transfer')}}</a> 
                        </li>
                    </ul>
                </li>
                @endif
                
                
                @if(Auth::user()->messaging == 1)
                <li>
                    <a class="has-arrow waves-effect">
                        <i class="far fa-envelope"></i><span>{{ __('messages.messaging')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('messages_send_index') }}">{{ __('messages.send_meg')}}</a> </li>
                    </ul>
                </li>
                @endif
                
                
                @if(Auth::user()->reports == 1)
                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>{{ __('messages.reports')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.order')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('order_report_index') }}">SM Order Statement</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a href="{{ route('free_items_statement_index') }}">
                                {{ __('messages.statement_free_items')}} 
                            </a>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                 {{ __('messages.issue_sm')}} 
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('sr_statement_index') }}">{{ __('messages.sm_statement')}}</a> </li>
                                <li> <a href="{{ route('sr_stock_statement_index') }}">{{ __('messages.sm_stock_status')}}</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.sales_sm_to_Customer')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('sales_statement_index') }}">{{ __('messages.sales_statement')}}</a> </li>
                                <li> <a href="{{ route('sales_summary_index') }}">{{ __('messages.sales_summary')}}</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.purchase')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('purchase_statement_index') }}">{{ __('messages.purchase_statement')}}</a> </li>
                                <li> <a href="{{ route('purchase_summary_index') }}">{{ __('messages.purchase_summary')}}</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.accounts')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('expense_report_index') }}">{{ __('messages.list_of_expense')}}</a> </li>
                                <li> <a href="{{ route('income_report_index') }}">{{ __('messages.list_of_income')}}</a> </li>
                                <li> <a href="{{ route('income_expense_ledger_index') }}">{{ __('messages.cash_book')}}</a> </li>
                                <li> <a href="{{ route('income_statement_index') }}">{{ __('messages.ledger_book')}}</a> </li>
                                <li> <a href="{{ route('loan_report_index') }}">{{ __('messages.loan_statement')}}</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.payment')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('customer_payment_report_index') }}">{{ __('messages.c_payment')}}</a> </li>
                                <li> <a href="{{ route('supplier_payment_report_index') }}">{{ __('messages.s_payment')}}</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.mis')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('current_balance_print') }}">{{ __('messages.current_balance')}}</a> </li>
                                <li> <a href="{{ route('stock_report_index') }}">{{ __('messages.stock_status')}}</a> </li>
                                <li> <a href="{{ route('due_report_customer_index') }}">{{ __('messages.Customer_Ledger')}}</a> </li>
                                <li> <a href="{{ route('due_report_supplier_index') }}">{{ __('messages.supplier_Ledger')}}</a> </li>
                                <li> <a href="{{ route('due_list_report_customer_index') }}">{{ __('messages.Customer_due_list')}}</a> </li>

                                <li> <a href="{{ route('manual_due_list_report_customer_index') }}">{{ __('messages.Customer_manual_due_list')}}</a> </li>
                                <li> <a href="{{ route('due_list_supplier_index') }}">{{ __('messages.Supplier_due_list')}}</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.basic_report')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('item_list_index') }}"> {{ __('messages.list_items')}}</a> </li>
                                <li> <a href="{{ route('emergency_item_list_index') }}">{{ __('messages.emergency_purchase')}}</a> </li>
                                <li> <a href="{{ route('product_suppliers_index') }}">{{ __('messages.item_wise_supplier')}}</a> </li>
                                <li> <a href="{{ route('product_customers_index') }}">{{ __('messages.item_wise_customer')}}</a> </li>
                                <li> <a href="{{ route('register_list_index').'?type=0' }}" target="_blank">{{ __('messages.buyer_list')}}</a> </li>
                                <li> <a href="{{ route('register_list_index').'?type=1' }}" target="_blank">{{ __('messages.supplier_list')}}</a> </li>
                            </ul>
                        </li>
                        
                        <!-- <li> <a href="#">List of Sending SMS</a> </li>
                        <li> <a href="{{ route('salary_report_index') }}">Salary Report</a> </li> -->

                    </ul>
                </li>
                @endif
                
                
                @if(Auth::user()->basic_settings == 1)
                <li class="{{ 
                Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' ||
                Request::getQueryString() == 'contact_type=4' ? 'mm-active' : '' || 
                Request::getQueryString() == 'contact_type=5' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_areas_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_stores_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' ||
                    Request::getQueryString() == 'contact_type=4' ? 'mm-active' : '' || 
                    Request::getQueryString() == 'contact_type=5' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_areas_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_stores_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>{{ __('messages.basic_settings')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">

                        <li class="{{ Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_areas_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_stores_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_areas_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_stores_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                {{ __('messages.product')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' }}" href="{{ route('products_index') }}">{{ __('messages.add_product')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }}" href="{{ route('products_category_index') }}">{{ __('messages.add_product_group')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' }}" href="{{ route('categories_index') }}" href="{{ route('categories_index') }}">{{ __('messages.brand_manufacturer')}}</a> </li>
                                <!-- <li> <a href="#">Add Generic</a> </li> -->
                                <li> <a class="{{ Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' }}" href="{{ route('products_units_index') }}">{{ __('messages.add_unit_measure')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_areas_edit' ? 'mm-active' : '' }}" href="{{ route('products_areas_index') }}">{{ __('messages.add_area')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_stores_edit' ? 'mm-active' : '' }}" href="{{ route('products_stores_index') }}">{{ __('messages.add_store')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' }}" href="{{ route('products_variations_index') }}">{{ __('messages.product_variations')}}</a> </li>
                                <li> <a href="{{ route('products_barcode_print') }}">{{ __('messages.print_barcode')}}</a> </li>
                                <li> <a href="{{ route('products_opening_stock') }}">{{ __('messages.bulk_opening_stock')}}</a> </li>
                                <li> <a href="{{ route('products_bulk_product_list_update') }}">{{ __('messages.bulk_product_update')}}</a> </li>
                            </ul>
                        </li>

                        <li class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=4' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=5' ? 'mm-active' : '' }}">
                            <a class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=4' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=5' ? 'mm-active' : '' }} has-arrow waves-effect">
                                {{ __('messages.registers')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=1' }}">{{__('messages.add_supplier_dp')}}</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=4' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=4' }}">{{__('messages.add_sm')}}</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=0' }}">{{__('messages.add_customer')}}</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=2' }}">{{ __('messages.add_employee')}}</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=3' }}">{{__('messages.add_reference')}}</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=5' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=5' }}">{{__('messages.add_loan_register')}}</a> </li>
                            </ul>
                        </li>

                        <li class="{{ Route::currentRouteName() == 'paid_through_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'paid_through_accounts_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'paid_through_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'paid_through_accounts_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                {{ __('messages.accounts')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Route::currentRouteName() == 'paid_through_accounts_edit' ? 'mm-active' : '' }}" href="{{ route('paid_through_accounts_index') }}">{{ __('messages.paid_through')}}</a> </li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow waves-effect">
                                {{ __('messages.messaging')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('messages_index') }}">{{ __('messages.create_message')}}</a> </li>
                                <li> <a href="{{ route('messages_phone_book_index') }}">{{ __('messages.pb')}}</a> </li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow waves-effect">
                                {{ __('messages.security_system')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('users_index') }}">{{ __('messages.add_user')}}</a> </li>
                                <li> <a href="{{ route('users_index_all') }}">{{ __('messages.list_user')}}</a> </li>
                                <li> <a href="{{ route('set_access_index') }}">{{ __('messages.permission')}}</a> </li>
                            </ul>
                        </li>

                        <li> <a href="{{ route('assets_index') }}">{{__('messages.fixed_assets')}}</a></li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
    </div>
</div>