@extends('layouts.app')

@section('title', 'Bulk Product Update')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Bulk Product Update</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Products</a></li>
                                    <li class="breadcrumb-item active">Bulk Product Update</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif


                                <form action="{{route('products_bulk_product_list_update')}}"  method="get" enctype="multipart/form-data">
                                  
                                    <div class="row">
                                    <div class="col-md-5">
                                        <select class="form-control select2" name="brand_id">
                                            <option value="0" selected="">All Brand</option>
                                            @if(!empty($brand) && ($brand->count() > 0))
                                            @foreach($brand as $key=> $list)
                                            <option {{ isset($_GET['brand_id']) && ($_GET['brand_id'] == $list->id) ? 'selected' : '' }} value="{{$list->id}}">{{$list->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <select class="form-control select2" name="group_id">
                                            <option value="0" selected="">All Group</option>
                                            @if(!empty($group) && ($group->count() > 0))
                                            @foreach($group as $key=> $list)
                                            <option {{ isset($_GET['group_id']) && ($_GET['group_id'] == $list->id) ? 'selected' : '' }} value="{{$list->id}}">{{$list->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <button data-repeater-create type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                </div>

                                <hr>
                                </form>

                                <form id="FormSubmit" action="{{ route('products_bulk_product_list_update_save') }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                @if($product_entries->count() > 0)
                                @foreach($product_entries as $key => $value)
                                <div class="row">

                                    <input type="hidden" name="product_id[]" class="inner form-control" id="product_id" value="{{ $value['id'] }}" />
                                    
                                    <div class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">
                                        @if($key == 0)
                                        <label for="productname" class="col-form-label">Pro.Code</label>
                                        @endif
                                        <input type="text" name="code[]" class="inner form-control barCode" id="bar_code_0" value="{{ $value['product_code'] }}" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        @if($key == 0)
                                        <label for="productname" class="col-form-label">Product Name *</label>
                                        @endif
                                        <input type="text" name="product_name[]" class="inner form-control" id="product_name" value="{{ $value['name'] }}" required />
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        @if($key == 0)
                                        <label for="brandname" class="col-form-label">Brand Name *</label>
                                        @endif
                                        <input type="text" class="inner form-control" value="{{ $value['brand_name'] }}" required />
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        @if($key == 0)
                                        <label for="groupname" class="col-form-label">Group Name *</label>
                                        @endif
                                        <input type="text" class="inner form-control" value="{{ $value['group_name'] }}" required />
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        @if($key == 0)
                                        <label for="productname" class="col-form-label">Purchase Price *</label>
                                        @endif
                                        <input type="text" name="buying_price[]" class="inner form-control" id="buying_price_0" value="{{ $value['buy_price'] }}" required />
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        @if($key == 0)
                                        <label for="productname" class="col-form-label">Sell Price *</label>
                                        @endif
                                        <input type="text" name="selling_price[]" class="inner form-control" id="selling_price_0" value="{{ $value['sell_price'] }}" required />
                                    </div>
                                </div>
                                @endforeach
                                @endif

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('products_bulk_product_list_update') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection