<?php

namespace Modules\Attendance\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Model
use Rats\Zkteco\Lib\ZKTeco;
use App\Models\Devices;
use App\Models\MachineDataLog;
use Validator;
use Auth;
use Response;
use DB;

class AttendanceController extends Controller
{
    public function index()
    {
        $zk = new ZKTeco('192.168.0.205');
        $zk->connect();
        $employees = $zk->getUser();
// dd($employees);

        return view('attendance::all_device_users', compact('employees'));
    }

    public function importUser()
    {
        $zk = new ZKTeco('192.168.0.201');
        $zk->connect();

        $uid = array_key_last($zk->getUser());

        return view('attendance::import_employee_to_device', compact('uid'));
    }

    public function importUserStore(Request $request)
    {
        $data = $request->all();

        $zk = new ZKTeco('192.168.0.201');
        $zk->connect();
 
        $zk->setUser($data['uid'],$data['userid'],$data['name'],$password='',$role=0);

        return back();
    }

    public function deleteUser($uid)
    {
        $zk = new ZKTeco('192.168.0.201');
        $zk->connect();
        $zk->removeUser($uid); 

        return redirect()->route('attendance_import_user_index');
    }

    public function deviceList()
    {
        // $zk = new ZKTeco('192.168.0.201');
        // $zk->connect();
        // dd('sdasd');
        $devices = Devices::get();

        return view('attendance::add_device', compact('devices'));
    }

    public function deviceStore(Request $request)
    {
        $rules = array(
            'device_ip'  => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $device               = new Devices;
            $device->device_name  = $data['device_name'];
            // $device->device_id    = ;
            // $device->device_ip    = ;
            $device->created_by   = $user_id;

            if ($device->save())
            {   
                return back()->with("success","Device Added Successfully !!");
            }
        }
        catch (\Exception $exception){
            return back()->with("unsuccess","Device Not Added");
        }
    }

    public function pullData()
    {
        $zk = new ZKTeco('192.168.0.205');
        $zk->connect();
        $serial     = explode('=', $zk->serialNumber());
        $machine_id = $serial[1];

        $tas_json_decode   = $zk->getAttendance();

        if($tas_json_decode)
        {
            $tas_machine_data = array();
            foreach($tas_json_decode as $data)
            {
                $machine_data = array(
                    'UserID'        => trim($data['uid']),
                    'DateTime'      => trim($data['timestamp']),
                    'MachineID'     => trim($machine_id),
                    'InOutStatus'   => trim($data['state'])
                );

                $tas_machine_data[] = $machine_data;
            }

            DB::table('tbl_tas_machine_data')->insert($tas_machine_data);

            $insert_data = DB::table('tbl_tas_machine_data_log')->insert($tas_machine_data);

            if($insert_data)
            {
                return back()->with('success', 'Data inserted successfully !! !!');
            }
            else
            {
                return back()->with('unsuccess', 'Something wrong.Try again !!');
            }
        }
        else
        {
            return back()->with('unsuccess', 'Something wrong.Try again !!');
        }
    }

    public function attendanceList()
    {
        $data = MachineDataLog::orderBy('timestamp', 'DESC')->get();

        return view('attendance::attendance_list', compact('data'));
    }

    public function importUserUpdate(Request $request)
    {
        $data   = $request->all();
        $zk     = new ZKTeco('192.168.0.205');
        $zk->connect();
    
        DB::beginTransaction();

        try{
            $zk->clearAdmin();

            foreach ($data['employee_id'] as $key => $value)
            {
                $zk->setUser($data['uid'][$key],$value,$data['employee_name'][$key],$password='',$role=0);
            }

            DB::commit();
            return back()->with("success","Successfully Updated");
        }catch (\Exception $exception){
            DB::rollback($exception);
            return back()->with("unsuccess","Not Added");
        }
    }
}
