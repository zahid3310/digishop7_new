@extends('layouts.app')

@section('title', 'Edit Variation')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Variation List</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Variations</a></li>
                                    <li class="breadcrumb-item active">Variation List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('products_variations_update', $find_variation['id']) }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group row">

                                    <div class="col-md-12 form-group">
                                        <label for="productname">Name *</label>
                                        <input type="text" name="name" class="inner form-control" id="name" value="{{ $find_variation['name'] }}" required />
                                    </div>
                                </div>

                                <div style="margin-top: -35px" data-repeater-list="inner-group" class="inner form-group mb-0 row">
                                    <div class="inner col-lg-12 ml-md-auto input_fields_wrap getMultipleRow">

                                        <hr>

                                        <h5>Add Variation Values</h5>

                                        @if((!empty($variation_values)) && ($variation_values->count() > 0))
                                        @foreach($variation_values as $key => $variation_value)
                                        <div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_{{$key}}">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                                <input type="text" class="form-control" name="value_name[]" value="{{ $variation_value['name'] }}" required>
                                                <input type="hidden" name="value_ids[]" value="{{ $variation_value['id'] }}">
                                                <input type="hidden" name="type[]" value="0">
                                            </div>
                                            
                                            <div class="col-lg-1 col-md-2 col-sm-6 col-6 form-group" data-val="{{$key}}">
                                                <i class="btn btn-success btn-block bx bx-trash-alt font-size-20"></i>
                                            </div>
                                        </div>
                                        @endforeach
                                        @endif

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6"></div>
                                    <div class="col-lg-1 col-md-2 col-sm-6 col-12 form-group">
                                        <div id="add_field_button" class="add_field_button">
                                            <i class="btn btn-success btn-block bx bx-plus font-size-20"></i>
                                        </div>
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-lg-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('products_variations_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Stores</h4>

                                <br>

                                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Variation</th>
                                            <th>Values</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        @if(!empty($variations) && ($variations->count() > 0))
                                        @foreach($variations as $key => $variation)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $variation['name'] }}</td>
                                                <td>{{ $variation['product_variation_values'] }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('products_variations_edit', $variation['id']) }}">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x       = {{$variation_values_count}};
        $(add_button).click(function(e)
        {   
            e.preventDefault();

            if(x < max_fields)
            {   
                x++;

                $('.getMultipleRow').append(' ' + '<div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_'+x+'">' +
                                                    '<div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">\n' +
                                                        '<input type="text" class="form-control" name="value_name[]" placeholder="Enter Variation Value" required>\n' +
                                                        '<input type="hidden" name="type[]" value="1">\n' +
                                                    '</div>\n' +

                                                    '<div class="col-lg-1 col-md-2 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                        '<i class="btn btn-success btn-block bx bx-trash-alt font-size-20"></i>\n' + 
                                                    '</div>\n' + 
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;
        });
    </script>
@endsection
