<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ProductEntriesRaw extends Model
{  
    protected $table = "product_entries_raw";

    public function productEntries()
    {
        return $this->belongsTo('App\Models\ProductEntries','product_entry_id');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Stores','store_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }
}
