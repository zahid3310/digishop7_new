<?php

namespace Modules\Payments\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\Bills;
use App\Models\InvoiceEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\PaidThroughAccounts;
use App\Models\Transactions;
use App\Models\AccountTransactions;
use App\Models\CurrentBalance;
use Response;
use DB;

class PaymentsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('payments::index');
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $branch_id      = Auth::user()->branch_id;
        $customers      = Customers::where('contact_type', 0)
                                    ->orWhere('contact_type', 1)
                                    ->when($branch_id != 1, function ($query) use ($branch_id) {
                                        return $query->where('customers.branch_id', $branch_id);
                                    })
                                    ->orderBy('created_at', 'DESC')
                                    ->get();

        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $find_customer  = Customers::where('id', $customer_id)->first();
        $paid_accounts  = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();

        return view('payments::create', compact('customers', 'customer_id', 'find_customer', 'paid_accounts'));
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_id'   => 'required',
            'type'          => 'required',
            'payment_date'  => 'required',
            'amount'        => 'required',
            'paid.*'        => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        if ($data['amount'] == 0 || $data['payment_date'] == null)
        {
            return redirect()->back()->withInput();
        }

        DB::beginTransaction();

        try{
            $data_find                  = Payments::orderBy('id', 'DESC')->first();
            $payment_number             = $data_find != null ? $data_find['payment_number'] + 1 : 1;
            
            $payment                    = new Payments;
            $payment->payment_number    = $payment_number;
            $payment->customer_id       = $data['customer_id'];
            $payment->payment_date      = date('Y-m-d', strtotime($data['payment_date']));
            $payment->amount            = $data['amount'];
            $payment->paid_through      = $data['paid_through'];
            $payment->note              = $data['note'];
            $payment->type              = $data['type'];
            $payment->created_by        = $user_id;

            $find_customer              = $data['customer_id'];
            $find_type                  = $data['type'];

            if ($payment->save())
            {   
                if ($data['type'] == 0)
                {
                    $used_amount  = 0;
                    if (isset($data['invoice_id']))
                    {
                        foreach ($data['invoice_id'] as $key => $value)
                        {
                            if ($data['paid'][$key] != 0)
                            {
                                $payment_entries[] = [
                                    'invoice_id'        => $value,
                                    'payment_id'        => $payment['id'],
                                    'amount'            => $data['paid'][$key],
                                    'branch_id'         => $branch_id,
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                $account_transactions[] = [
                                    'customer_id'           => $data['customer_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                    'amount'                => $data['paid'][$key],
                                    'paid_through_id'       => $payment['paid_through'],
                                    'note'                  => $data['note'] . '(বকেয়া বাবদ আদায়)',
                                    'type'                  => 0,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'sales',
                                    'associated_id'         => $value,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $current_balance[] = [
                                    'customer_id'           => $data['customer_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                    'amount'                => $data['paid'][$key],
                                    'paid_through_id'       => $payment['paid_through'],
                                    'note'                  => $data['note'] . '(বকেয়া বাবদ আদায়)',
                                    'type'                  => 0,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'sales',
                                    'associated_id'         => $value,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $update_dues                = Invoices::find($value);
                                $update_dues->due_amount    = $update_dues['due_amount'] - $data['paid'][$key];
                                $update_dues->save();

                                $used_amount  = $used_amount + $data['paid'][$key];
                            }
                        }
                    }

                    $excess_amount = $data['amount'] - $used_amount;

                    if ($excess_amount > 0)
                    {
                        $account_transactions_advance = [
                            'customer_id'           => $data['customer_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $excess_amount,
                            'paid_through_id'       => 1,
                            'note'                  => '(অগ্রীম বাবদ আদায়)',
                            'type'                  => 0,  // 0 = In , 1 = Out
                            'transaction_head'      => 'customer-advance',
                            'associated_id'         => $payment->id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $current_balance = [
                            'customer_id'           => $data['customer_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $excess_amount,
                            'paid_through_id'       => $data['paid_through'],
                            'note'                  => '(অগ্রীম বাবদ আদায়)',
                            'type'                  => 0,  // 0 = In , 1 = Out
                            'transaction_head'      => 'customer-advance',
                            'associated_id'         => $payment->id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        DB::table('account_transactions')->insert($account_transactions_advance);
                        DB::table('current_balance')->insert($current_balance);

                        $update_customer_advance                            = Customers::find($data['customer_id']);
                        $update_customer_advance->customer_advance_payment  = $update_customer_advance['customer_advance_payment'] + $excess_amount;
                        $update_customer_advance->save();
                    }

                    if ($data['amount'] > 0)
                    {
                        $transaction_data['date']           = date('Y-m-d', strtotime($data['payment_date']));
                        $transaction_data['type']           = 1;
                        $transaction_data['account_head']   = 'previoue-due-collection';
                        $transaction_data['transaction_id'] = $payment->id;
                        $transaction_data['customer_id']    = $data['customer_id'];
                        $transaction_data['note']           = 'পূর্বের বকেয়া আদায়';
                        $transaction_data['amount']         = $data['amount'];
                        $transaction_data['paid_through']   = $data['paid_through'];
                        transactions($transaction_data);
                    }
                }
                
                if ($data['type'] == 1)
                {
                    $used_amount  = 0;
                    if (isset($data['bill_id']))
                    {
                        foreach ($data['bill_id'] as $key => $value)
                        {
                            if ($data['paid'][$key] != 0)
                            {
                                $payment_entries[] = [
                                    'bill_id'           => $value,
                                    'payment_id'        => $payment['id'],
                                    'amount'            => $data['paid'][$key],
                                    'branch_id'         => $branch_id,
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                $account_transactions[] = [
                                    'customer_id'           => $data['customer_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                    'amount'                => $data['paid'][$key],
                                    'paid_through_id'       => $payment['paid_through'],
                                    'note'                  => $data['note'] . '(বকেয়া বাবদ প্রদান)',
                                    'type'                  => 1,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'purchase',
                                    'associated_id'         => $value,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $current_balance[] = [
                                    'customer_id'           => $data['customer_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                    'amount'                => $data['paid'][$key],
                                    'paid_through_id'       => $payment['paid_through'],
                                    'note'                  => $data['note'] . '(বকেয়া বাবদ প্রদান)',
                                    'type'                  => 1,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'purchase',
                                    'associated_id'         => $value,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $update_bill_dues                = Bills::find($value);
                                $update_bill_dues->due_amount    = $update_bill_dues['due_amount'] - $data['paid'][$key];
                                $update_bill_dues->save();

                                $used_amount  = $used_amount + $data['paid'][$key];
                            }
                        }
                    }

                    $excess_amount = $data['amount'] - $used_amount;

                    if ($excess_amount > 0)
                    {
                        $account_transactions_advance = [
                            'customer_id'           => $data['customer_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $excess_amount,
                            'paid_through_id'       => 1,
                            'note'                  => '(অগ্রীম বাবদ প্রদান)',
                            'type'                  => 1,  // 0 = In , 1 = Out
                            'transaction_head'      => 'supplier-advance',
                            'associated_id'         => $payment->id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $current_balance = [
                            'customer_id'           => $data['customer_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $excess_amount,
                            'paid_through_id'       => $data['paid_through'],
                            'note'                  => '(অগ্রীম বাবদ প্রদান)',
                            'type'                  => 1,  // 0 = In , 1 = Out
                            'transaction_head'      => 'supplier-advance',
                            'associated_id'         => $payment->id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        DB::table('account_transactions')->insert($account_transactions_advance);

                        $update_customer_advance                            = Customers::find($data['customer_id']);
                        $update_customer_advance->supplier_advance_payment  = $update_customer_advance['supplier_advance_payment'] + $excess_amount;
                        $update_customer_advance->save();
                    }

                    if ($data['amount'] > 0)
                    {
                        $transaction_data['date']           = date('Y-m-d', strtotime($data['payment_date']));
                        $transaction_data['type']           = 0;
                        $transaction_data['account_head']   = 'previoue-due-paid';
                        $transaction_data['transaction_id'] = $payment->id;
                        $transaction_data['customer_id']    = $data['customer_id'];
                        $transaction_data['note']           = 'পূর্বের বকেয়া প্রদান';
                        $transaction_data['amount']         = $data['amount'];
                        $transaction_data['paid_through']   = $data['paid_through'];
                        transactions($transaction_data);
                    }
                }

                if ($data['type'] == 2)
                {
                    foreach ($data['sales_return_id'] as $key => $value)
                    {
                        if ($data['paid'][$key] != 0)
                        {
                            $payment_entries[] = [
                                'sales_return_id'   => $value,
                                'payment_id'        => $payment['id'],
                                'amount'            => $data['paid'][$key],
                                'branch_id'         => $branch_id,
                                'created_by'        => $user_id,
                                'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            $account_transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                'amount'                => $data['paid'][$key],
                                'paid_through_id'       => $payment['paid_through'],
                                'note'                  => $data['note'] . '(রিটার্ন বাবদ প্রদান)',
                                'type'                  => 1,  // 0 = In , 1 = Out
                                'transaction_head'      => 'sales-return',
                                'associated_id'         => $value,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $current_balance[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                'amount'                => $data['paid'][$key],
                                'paid_through_id'       => $payment['paid_through'],
                                'note'                  => $data['note'] . '(রিটার্ন বাবদ প্রদান)',
                                'type'                  => 1,  // 0 = In , 1 = Out
                                'transaction_head'      => 'sales-return',
                                'associated_id'         => $value,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $update_return_dues                     = SalesReturn::find($value);
                            $update_return_dues->due_amount         = $update_return_dues['due_amount'] - $data['paid'][$key];
                            $update_return_dues->save();

                            // $find_invoice_id                        = $tables['sales_return_entries']->where('sales_return_id', $value)->first(); 
                            // $update_invoice_dues                    = $tables['invoices']->find($find_invoice_id['invoice_id']);
                            // $update_invoice_dues->return_amount     = $update_invoice_dues['return_amount'] - $data['paid'][$key];
                            // $update_invoice_dues->save();
                        }
                    }

                    if ($data['amount'] > 0)
                    {
                        $transaction_data['date']           = date('Y-m-d', strtotime($data['payment_date']));
                        $transaction_data['type']           = 0;
                        $transaction_data['account_head']   = 'sales-return';
                        $transaction_data['transaction_id'] = $payment->id;
                        $transaction_data['customer_id']    = $data['customer_id'];
                        $transaction_data['note']           = 'পূর্বের বিক্রয় রিটার্ন প্রদান';
                        $transaction_data['amount']         = $data['amount'];
                        $transaction_data['paid_through']   = $data['paid_through'];
                        transactions($transaction_data);
                    }
                }

                if ($data['type'] == 3)
                {
                    foreach ($data['purchase_return_id'] as $key => $value)
                    {
                        if ($data['paid'][$key] != 0)
                        {
                            $payment_entries[] = [
                                'purchase_return_id'    => $value,
                                'payment_id'            => $payment['id'],
                                'amount'                => $data['paid'][$key],
                                'branch_id'             => $branch_id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $account_transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                'amount'                => $data['paid'][$key],
                                'paid_through_id'       => $payment['paid_through'],
                                'note'                  => $data['note'] . '(রিটার্ন বাবদ গ্রহণ)',
                                'type'                  => 0,  // 0 = In , 1 = Out
                                'transaction_head'      => 'purchase-return',
                                'associated_id'         => $value,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $current_balance[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                'amount'                => $data['paid'][$key],
                                'paid_through_id'       => $payment['paid_through'],
                                'note'                  => $data['note'] . '(রিটার্ন বাবদ গ্রহণ)',
                                'type'                  => 0,  // 0 = In , 1 = Out
                                'transaction_head'      => 'purchase-return',
                                'associated_id'         => $value,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $update_return_dues                 = PurchaseReturn::find($value);
                            $update_return_dues->due_amount     = $update_return_dues['due_amount'] - $data['paid'][$key];
                            $update_return_dues->save();

                            // $find_bill_id                       = $tables['purchase_return_entries']->where('purchase_return_id', $value)->first();   
                            // $update_bill_dues                   = $tables['bills']->find($find_bill_id['bill_id']);
                            // $update_bill_dues->return_amount    = $update_bill_dues['return_amount'] - $data['paid'][$key];
                            // $update_bill_dues->save();
                        }
                    }

                    if ($data['amount'] > 0)
                    {
                        $transaction_data['date']           = date('Y-m-d', strtotime($data['payment_date']));
                        $transaction_data['type']           = 1;
                        $transaction_data['account_head']   = 'purchase-return';
                        $transaction_data['transaction_id'] = $payment->id;
                        $transaction_data['customer_id']    = $data['customer_id'];
                        $transaction_data['note']           = 'পূর্বের ক্রয় রিটার্ন বাবদ আয়';
                        $transaction_data['amount']         = $data['amount'];
                        $transaction_data['paid_through']   = $data['paid_through'];
                        transactions($transaction_data);
                    }
                }

                if (isset($payment_entries))
                {
                    DB::table('payment_entries')->insert($payment_entries);
                }

                if (isset($account_transactions))
                {
                    DB::table('account_transactions')->insert($account_transactions);
                }

                if (isset($current_balance))
                {
                    DB::table('current_balance')->insert($current_balance);
                }

                DB::commit();
                return back()->with("success","Payment Created Successfully !!")->with("find_customer", $find_customer)->with('find_type', $find_type);
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.")->with('find_customer', $find_customer)->with('find_type', $find_type);
            }
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added")->with('find_customer', $find_customer)->with('find_type', $find_type);
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $payment        = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                ->select('payments.*',
                                         'customers.name as customer_name',
                                         'customers.address as address',
                                         'customers.phone as phone')
                                ->find($id);

        if ($payment['type'] == 0)
        {
            $entries    = PaymentEntries::leftjoin('invoices', 'invoices.id', 'payment_entries.invoice_id')
                                ->where('payment_entries.payment_id', $id)
                                ->select('payment_entries.*', 'invoices.invoice_number as number')
                                ->get();
        }
        else
        {
            $entries    = PaymentEntries::leftjoin('bills', 'bills.id', 'payment_entries.bill_id')
                                ->where('payment_entries.payment_id', $id)
                                ->select('payment_entries.*', 'bills.bill_number as number')
                                ->get();
        }

        $user_info  = userDetails();

        return view('payments::show', compact('entries', 'payment', 'user_info'));
    }

    public function edit($id)
    {
        dd($entries);
        return view('payments::edit');
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $find_payment   = Payments::find($id);
        $entries        = PaymentEntries::where('payment_entries.payment_id', $id)
                                ->select('payment_entries.*')
                                ->get();

        DB::beginTransaction();

        //0=invoice,1=bill
        if ($find_payment['type'] == 0)
        {
            foreach ($entries as $key => $value)
            {
                $invoice                = Invoices::find($value['invoice_id']);
                $invoice->due_amount    = $invoice['due_amount'] + $value['amount'];
                $invoice->save();

                $delete_current_balance     = CurrentBalance::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'sales')->where('associated_id', $value['invoice_id'])->delete();
            }

            $find_transaction  = AccountTransactions::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'customer-advance')->where('associated_id', $id)->first();

            $update_customer                            = Customers::find($find_payment['customer_id']);
            $update_customer->customer_advance_payment  = $update_customer['customer_advance_payment'] - $find_transaction['amount'];
            $update_customer->save();

            $delete_find_transaction    = AccountTransactions::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'customer-advance')->where('associated_id', $id)->delete();
            $delete_transaction         = AccountTransactions::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'sales')->where('associated_id', $id)->delete();
            $delete_trans               = Transactions::where('customer_id', $find_payment['customer_id'])->where('account_head', 'previoue-due-collection')->where('payment_id', $id)->delete();
            $del_current_balance        = CurrentBalance::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'customer-advance')->where('associated_id', $id)->delete();
        }

        if ($find_payment['type'] == 1)
        {
            foreach ($entries as $key1 => $value1)
            {
                $bill                = Bills::find($value1['bill_id']);
                $bill->due_amount    = $bill['due_amount'] + $value1['amount'];
                $bill->save();

                $delete_current_balance     = CurrentBalance::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'purchase')->where('associated_id', $value['bill_id'])->delete();
            }

            $find_transaction  = AccountTransactions::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'supplier-advance')->where('associated_id', $id)->first();

            $update_customer                            = Customers::find($find_payment['customer_id']);
            $update_customer->supplier_advance_payment  = $update_customer['supplier_advance_payment'] - $find_transaction['amount'];
            $update_customer->save();

            $delete_find_transaction    = AccountTransactions::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'supplier-advance')->where('associated_id', $id)->delete();
            $delete_transaction         = AccountTransactions::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'purchase')->where('associated_id', $id)->delete();
            $delete_trans               = Transactions::where('customer_id', $find_payment['customer_id'])->where('account_head', 'previoue-due-paid')->where('payment_id', $id)->delete();
            $del_current_balance        = CurrentBalance::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'supplier-advance')->where('associated_id', $id)->delete();
        }

        if ($find_payment['type'] == 2)
        {
            foreach ($entries as $key2 => $value2)
            {
                $update_return_dues             = SalesReturn::find($value2['sales_return_id']);
                $update_return_dues->due_amount = $update_return_dues['due_amount'] + $value2['amount'];
                $update_return_dues->save();

                $delete_current_balance         = CurrentBalance::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'sales-return')->where('associated_id', $value['sales_return_id'])->delete();
            }

            $delete_transaction     = AccountTransactions::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'sales-return')->where('associated_id', $id)->delete();
            $delete_trans           = Transactions::where('customer_id', $find_payment['customer_id'])->where('account_head', 'sales-return')->where('payment_id', $id)->delete();
            $del_current_balance    = CurrentBalance::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'sales-return')->where('associated_id', $id)->delete();
        }

        if ($find_payment['type'] == 3)
        {
            foreach ($entries as $key3 => $value3)
            {
                $update_return_dues              = PurchaseReturn::find($value3['purchase_return_id']);
                $update_return_dues->due_amount  = $update_return_dues['due_amount'] + $value3['amount'];
                $update_return_dues->save();

                $delete_current_balance          = CurrentBalance::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'purchase-return')->where('associated_id', $value['purchase_return_id'])->delete();
            }

            $delete_transaction     = AccountTransactions::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'purchase-return')->where('associated_id', $id)->delete();
            $delete_trans           = Transactions::where('customer_id', $find_payment['customer_id'])->where('account_head', 'purchase-return')->where('payment_id', $id)->delete();
            $del_current_balance    = CurrentBalance::where('customer_id', $find_payment['customer_id'])->where('transaction_head', 'purchase-return')->where('associated_id', $id)->delete();
        }

        if ($find_payment->delete())
        {
            $find_type          = $find_payment['type'];
            $select_customer    = $find_payment['customer_id'];

            DB::commit();

            return back()->with("success","Payment Deleted Successfully !!")->with("find_customer", $select_customer)->with('find_type', $find_type);
        }
        else
        {
            DB::rollback();

            return back()->with("unsuccess","Something Went Wrong.Please Try Again.")->with('find_customer', $select_customer)->with('find_type', $find_type);
        }    
    }

    public function contactList($id)
    {
        $customers      = Customers::find($id);

        $receivable     = Invoices::where('customer_id', $id)
                                ->selectRaw('SUM(invoice_amount) as total_receivable')
                                ->first();

        $received       = Payments::where('customer_id', $id)
                                ->where('payments.type', 0)
                                ->selectRaw('SUM(amount) as total_received')
                                ->first();

        $invoices       = Invoices::where('customer_id', $id)
                                ->where('due_amount', '>', 0)
                                ->selectRaw('invoices.*')
                                ->get();

        $data['name']           = $customers['name'];
        $data['phone']          = $customers['phone'];
        $data['address']        = $customers['address'];
        $data['receivable']     = $receivable['total_receivable'];
        $data['received']       = $received['total_received'];
        $data['invoices']       = $invoices;

        return Response::json($data);
    }

    public function contactListBill($id)
    {
        $customers      = Customers::find($id);

        $payable        = Bills::where('vendor_id', $id)
                                ->selectRaw('SUM(bill_amount) as total_payable')
                                ->first();

        $paid           = Payments::where('customer_id', $id)
                                ->where('payments.type', 1)
                                ->selectRaw('SUM(amount) as total_paid')
                                ->first();

        $bills          = Bills::where('vendor_id', $id)
                                ->where('due_amount', '>', 0)
                                ->selectRaw('bills.*')
                                ->get();

        $data['name']           = $customers['name'];
        $data['phone']          = $customers['phone'];
        $data['address']        = $customers['address'];
        $data['payable']        = $payable['total_payable'];
        $data['paid']           = $paid['total_paid'];
        $data['bills']          = $bills;

        return Response::json($data);
    }

    public function contactListSalesReturn($id)
    {
        $customers      = Customers::find($id);

        $payable        = SalesReturn::where('customer_id', $id)
                                ->selectRaw('SUM(return_amount) as total_payable')
                                ->first();

        $paid           = Payments::where('customer_id', $id)
                                ->where('payments.type', 2)
                                ->selectRaw('SUM(amount) as total_paid')
                                ->first();

        $sale_returns   = SalesReturn::where('customer_id', $id)
                                ->where('due_amount', '>', 0)
                                ->selectRaw('sales_return.*')
                                ->get();

        $data['name']           = $customers['name'];
        $data['phone']          = $customers['phone'];
        $data['address']        = $customers['address'];
        $data['payable']        = $payable['total_payable'];
        $data['paid']           = $paid['total_paid'];
        $data['sale_returns']   = $sale_returns;

        return Response::json($data);
    }

    public function contactListPurchaseReturn($id)
    {
        $customers          = Customerrs::find($id);

        $payable            = PurchaseReturn::where('customer_id', $id)
                                ->selectRaw('SUM(return_amount) as total_payable')
                                ->first();

        $paid               = Payments::where('customer_id', $id)
                                ->where('payments.type', 2)
                                ->selectRaw('SUM(amount) as total_paid')
                                ->first();

        $purchase_returns   = PurchaseReturn::where('customer_id', $id)
                                ->where('due_amount', '>', 0)
                                ->selectRaw('purchase_return.*')
                                ->get();

        $data['name']               = $customers['name'];
        $data['phone']              = $customers['phone'];
        $data['address']            = $customers['address'];
        $data['payable']            = $payable['total_payable'];
        $data['paid']               = $paid['total_paid'];
        $data['purchase_returns']   = $purchase_returns;

        return Response::json($data);
    }

    public function paymentList($id)
    {
        $tables         = TableNameByUsers();
        $table_id       = Auth::user()->associative_contact_id;

        if ($id == 0)
        {
            $data           = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*',
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                        ->take(20)
                                        ->get();
        }
        else
        {
            $data           = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->where('payments.customer_id', $id)
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                        ->take(20)
                                        ->get();
        }

        return Response::json($data);
    }

    public function paymentListSearch($id)
    {
        $tables         = TableNameByUsers();
        $table_id       = Auth::user()->associative_contact_id;

        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data           = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->where('customers.name', 'LIKE', "%$id%")
                                        ->orWhere('payments.payment_date', 'LIKE', "%$search_by_date%")
                                        ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number, $table_id) {
                                            return $query->orWhere('payments.payment_number', 'LIKE', "%$search_by_payment_number%");
                                        })
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                        ->take(20)
                                        ->get();
        }
        else
        {
            $data           = Payments::join('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                        ->take(20)
                                        ->get();
        }

        return Response::json($data);
    }
}
