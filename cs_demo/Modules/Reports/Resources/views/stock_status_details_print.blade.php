<!DOCTYPE html>
<html>

<head>
    <title>Stock Status Details</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>  
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Stock Status Details</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: left">
                                        <strong>Branch Name :</strong> {{ $branch_name != null ? $branch_name['name'] : '' }} <br>
                                        <strong>Date Form :</strong> {{ $from_date }} <strong>Date To :</strong> {{ $to_date }} <br>
                                        <strong>Product Category :</strong> {{ $category_name != null ? $category_name['name'] : '' }} <br>
                                        <strong>Product : </strong> {{ $product_name != null ? $product_name['name'] : '' }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%;vertical-align: bottom" rowspan="2">SL</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">PRODUCT CODE</th>
                                    <th style="text-align: center;width: 15%;vertical-align: bottom" rowspan="2">PRODUCT NAME</th>
                                    <th style="text-align: center;width: 6%;vertical-align: bottom" rowspan="2">U/M</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom" rowspan="2">OPENING STOCK</th>
                                    <th style="text-align: center;width: 9%" colspan="4">Inward</th>
                                    <th style="text-align: center;width: 9%" colspan="4">Outward</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom" rowspan="2">CLOSING BALANCE</th>
                                </tr>

                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 9%;vertical-align: bottom">FROM TRANSFER</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom">PURCHASE</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom">SALES RETURN</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom">TOTAL</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom">TO TRANSFER</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom">SALES</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom">PURCHASE RETURN</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom">TOTAL</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                @foreach($data as $key => $value)
                                <tr>
                                    <td style="text-align: left;font-weight: bold" colspan="12">Product Category : {{ $key }}</td>
                                </tr>

                                <?php
                                    $i                  = 1;
                                ?>

                                @foreach($data[$key] as $key1 => $value1)
                                <tr>
                                    <td style="text-align: center">{{ $i }}</td>
                                    <td style="text-align: center">{{ $value1['product_code'] }}</td>
                                    <td style="text-align: left">{{ $value1['product_name'] }}</td>
                                    <td style="text-align: center">{{ $value1['unit_name'] }}</td>
                                    <td style="text-align: right">{{ $value1['opening_stock'] }}</td>
                                    <td style="text-align: right">{{ $value1['from_transfers'] }}</td>
                                    <td style="text-align: right">{{ $value1['purchase'] }}</td>
                                    <td style="text-align: right">{{ $value1['sale_return'] }}</td>
                                    <td style="text-align: right">{{ $value1['inward'] }}</td>
                                    <td style="text-align: right">{{ $value1['to_transfers'] }}</td>
                                    <td style="text-align: right">{{ $value1['sales'] }}</td>
                                    <td style="text-align: right">{{ $value1['purchase_return'] }}</td>
                                    <td style="text-align: right">{{ $value1['outward'] }}</td>
                                    <td style="text-align: right">{{ $value1['closing_balance'] }}</td>
                                </tr>

                                <?php
                                    $i++;
                                ?>
                                @endforeach
                                @endforeach
                                
                            </tbody>

                            <tfoot class="tfheight">
                               <!--  <tr>
                                    <th colspan="8" style="text-align: right;">TOTAL</th>
                                </tr> -->
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>