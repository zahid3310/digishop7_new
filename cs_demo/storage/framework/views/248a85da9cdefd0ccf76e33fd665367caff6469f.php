

<?php $__env->startSection('title', 'Dashboard'); ?>

<style type="text/css">
    .card-counter{
        box-shadow: 2px 2px 10px #DADADA;
        margin: 5px;
        padding: 20px 10px;
        background-color: #fff;
        height: 100px;
        border-radius: 5px;
        transition: .3s linear all;
    }

    .card-counter:hover{
        box-shadow: 4px 4px 20px #DADADA;
        transition: .3s linear all;
    }

    .card-counter.primary{
        background-color: #007bff;
        color: #FFF;
    }

    .card-counter.danger{
        background-color: #ef5350;
        color: #FFF;
    }  

    .card-counter.success{
        background-color: #66bb6a;
        color: #FFF;
    }  

    .card-counter.info{
        background-color: #26c6da;
        color: #FFF;
    }  

    .card-counter i{
        font-size: 5em;
        opacity: 0.2;
    }

    .card-counter .count-numbers{
        position: absolute;
        right: 35px;
        top: 20px;
        font-size: 32px;
        display: block;
    }

    .card-counter .count-name{
        position: absolute;
        right: 35px;
        top: 65px;
        font-style: italic;
        text-transform: capitalize;
        display: block;
        font-size: 18px;
    }

    @media  screen and (max-width: 48em) {
        .row-offcanvas {
            position: relative;
            -webkit-transition: all 0.25s ease-out;
            -moz-transition: all 0.25s ease-out;
            transition: all 0.25s ease-out;
        }

        .row-offcanvas-left .sidebar-offcanvas {
            left: -33%;
        }

        .row-offcanvas-left.active {
            left: 33%;
            margin-left: -6px;
        }

        .sidebar-offcanvas {
            position: absolute;
            top: 0;
            width: 33%;
            height: 100%;
        }
    }

    @media  screen and (max-width: 34em) {
        .row-offcanvas-left .sidebar-offcanvas {
            left: -45%;
        }

        .row-offcanvas-left.active {
            left: 45%;
            margin-left: -6px;
        }

        .sidebar-offcanvas {
            width: 45%;
        }
    }
    
    .card {
        overflow: hidden;
    }
    
    .card-block .rotate {
        z-index: 8;
        float: right;
        height: 100%;
    }
    
    .card-block .rotate i {
        color: rgba(20, 20, 20, 0.15);
        position: absolute;
        left: 0;
        left: auto;
        right: -10px;
        bottom: 0;
        display: block;
        -webkit-transform: rotate(-44deg);
        -moz-transform: rotate(-44deg);
        -o-transform: rotate(-44deg);
        -ms-transform: rotate(-44deg);
        transform: rotate(-44deg);
    }

    .display-1 {
        font-size: 2rem !important;
        color: white !important;
    }

    a, button {
        outline: 0!important;
        /*display: none;*/
    }

    .canvasjs-chart-credit
    {
        display: none !important;
    }
</style>

<?php $__env->startSection('content'); ?>
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Dashboard</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-md-3">
                    <div class="card-counter success">
                        <i class="fa fa-database"></i>
                        <span id="todaysSales" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Sales</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter info">
                        <i class="fa fa-users"></i>
                        <span id="todaysSalesReturn" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Sales Return</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter danger">
                        <i class="fa fa-database"></i>
                        <span id="todaysExpense" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Expense</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter primary">
                        <i class="fa fa-envelope"></i>

                        <?php
                            // $url        = "https://esms.mimsms.com/miscapi/C20072945fa0e98b1d9f42.77965009/getBalance";
                            // $response   = file_get_contents($url);
                            // $tmt        = substr($response, strrpos($response, ' ' ) + 1); 
                        ?>

                        <span class="count-numbers">
                            <h4 style="color: white;font-size: 30;" class="mb-0">0</h4>
                        </span>
                        <span class="count-name">SMS Balance</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div style="padding-top: 20px !important" class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4 float-sm-left">Top Selling Products</h4>
                            <!-- <div class="float-sm-right">
                                <ul class="nav nav-pills">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Week</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Month</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#">Year</a>
                                    </li>
                                </ul>
                            </div> -->
                            <div class="clearfix"></div>
                            <div id="chartContainer1" style="height: 395px; width: 100%;"></div>
                        </div>
                    </div>
                </div>

                <div style="padding-top: 16px !important" class="col-lg-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card-counter info">
                                <i class="fa fa-users"></i>
                                <span id="customerDues" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Customer Dues</span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card-counter info">
                                <i class="fa fa-users"></i>
                                <span id="supplierDues" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Supplier Dues</span>
                            </div>
                        </div>

                        <div style="padding-top: 15px !important" class="col-md-6">
                            <div class="card-counter success">
                                <i class="fa fa-database"></i>
                                <span id="totalSells" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Total Sales</span>
                            </div>
                        </div>

                        <div style="padding-top: 15px !important" class="col-md-6">
                            <div class="card-counter success">
                                <i class="fa fa-database"></i>
                                <span id="totalPurchase" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Total Purchase</span>
                            </div>
                        </div>

                        <div style="padding-top: 15px !important" class="col-md-6">
                            <div class="card-counter danger">
                                <i class="fa fa-database"></i>
                                <span id="totalCustomers" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Total Customers</span>
                            </div>
                        </div>

                        <div style="padding-top: 15px !important" class="col-md-6">
                            <div class="card-counter danger">
                                <i class="fa fa-database"></i>
                                <span id="totalSuppliers" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Total Suppliers</span>
                            </div>
                        </div>

                        <div style="padding-top: 15px !important" class="col-md-6">
                            <div class="card-counter primary">
                                <i class="fa fa-users"></i>
                                <span id="totalInvoices" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Total Invoices</span>
                            </div>
                        </div>

                        <div style="padding-top: 15px !important" class="col-md-6">
                            <div class="card-counter primary">
                                <i class="fa fa-users"></i>
                                <span id="totalBills" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Total Bills</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4 float-sm-left">Sales Summary</h4>
                            <div class="clearfix"></div>
                            <div id="chartContainer3" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4 float-sm-left">Purchase Summary</h4>
                            <div class="clearfix"></div>
                            <div id="chartContainer4" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Stock Out Products</h4>
                            <div style="height: 334px" class="table-responsive">
                                <table class="table table-centered table-nowrap mb-0">
                                    <thead>
                                        <th>SL.</th>
                                        <th>Code</th>
                                        <th>Name</th>
                                    </thead>

                                    <tbody id="product_list">
                                    </tbody>
                                </table>
                            </div>
                            <!-- end table-responsive -->
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4 float-sm-left">Due Invoices</h4>
                            <div class="float-sm-right">
                                <ul class="nav nav-pills">
                                    <li style="cursor: pointer" class="nav-item">
                                        <a id="DueInvoiceWeek" class="nav-link" onclick="dueInvoiceClick()">Week</a>
                                    </li>
                                    <li style="cursor: pointer" class="nav-item">
                                        <a id="DueInvoiceMonth" class="nav-link" onclick="dueInvoiceClick()">Month</a>
                                    </li>
                                    <li style="cursor: pointer" class="nav-item">
                                        <a id="DueInvoiceYear" class="nav-link" onclick="dueInvoiceClick()">Year</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <div style="height: 334px" class="table-responsive">
                                <table class="table table-centered table-nowrap mb-0">
                                    <thead>
                                        <th>SL.</th>
                                        <th>Date</th>
                                        <th>Invoice#</th>
                                        <th style="text-align: right">Amount</th>
                                    </thead>

                                    <tbody id="invoice_list">
                                    </tbody>
                                </table>
                            </div>
                            <!-- end table-responsive -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            var site_url  = $('.site_url').val();

            $.get(site_url + '/dashboard/items', function(data){
    
                var product_list = '';
                var serial       = 0;
                $.each(data.products, function(i, product_data)
                {
                    if (product_data.stock_in_hand == null)
                    {
                        var stock = 0;
                    }
                    else
                    {
                        var stock = product_data.stock_in_hand;
                    }

                    if (product_data.stock_in_hand <= product_data.alert_quantity)
                    {   
                        serial++;

                        product_list += '<tr>' +
                                        '<td style="text-align: left">' +
                                            serial +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            pad(product_data.product_code, 6) +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           product_data.name +
                                        '</td>' +
                                    '</tr>';
                    }
                });

                $("#product_list").empty();
                $("#product_list").append(product_list);

                var top_product_list = '';
                $.each(data.top_products, function(j, top_product_data)
                {
                    var serial1      = parseFloat(j) + 1;

                    if (top_product_data.total_sold == null)
                    {
                        var top_sold = 0;
                    }
                    else
                    {
                        var top_sold = top_product_data.total_sold;
                    }

                    if(j == 0)
                    {
                        $("#top_sell_item_quantity").empty();
                        $("#top_sell_item_quantity").append(top_sold);
                        $("#top_sel_item_name").empty();
                        $("#top_sel_item_name").append(top_product_data.product_name + "<br>" + top_product_data.name);
                    }

                    top_product_list += '<tr>' +
                                            '<td style="width: 80%">' +
                                                '<p class="mb-0">' + top_product_data.product_name + "<br>" + top_product_data.name + '</p>' +
                                            '</td>' +
                                            '<td style="width: 20%">' +
                                               '<h5 class="mb-0">' + top_sold + '</h5>' +
                                            '</td>' +
                                        '</tr>';
                });
                
                $("#top_product_list").empty();
                $("#top_product_list").append(top_product_list);


                var invoice_list     = '';
                var sl               = 0;
                $.each(data.due_invoices, function(i, invoice_data)
                {
                    var show_invoice = site_url + '/invoices/show/' + invoice_data.id;

                    sl++;

                    invoice_list += '<tr>' +
                                        '<td style="text-align: left">' +
                                            sl +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            invoice_data.invoice_date +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            '<a href="'+ show_invoice +'" target="_blank">' +
                                                'INV - ' + pad(invoice_data.invoice_number, 6) +
                                            '</a>' +
                                        '</td>' +
                                        '<td style="text-align: right">' +
                                           invoice_data.due_amount.toFixed() +
                                        '</td>' +
                                    '</tr>';
                });

                $("#invoice_list").empty();
                $("#invoice_list").append(invoice_list);

                $("#totalCustomers").empty();
                $("#totalCustomers").append(data.total_customers);

                $("#totalSuppliers").empty();
                $("#totalSuppliers").append(data.total_suppliers);

                $("#totalProducts").empty();
                $("#totalProducts").append(data.total_products);

                $("#todaysSales").empty();
                $("#todaysSales").append(data.todays_sells.toFixed());

                $("#todaysSalesReturn").empty();
                $("#todaysSalesReturn").append(data.todays_sells_return.toFixed());

                $("#todaysExpense").empty();
                $("#todaysExpense").append(data.todays_expenses.toFixed());

                $("#totalInvoices").empty();
                $("#totalInvoices").append(data.total_invoices);

                $("#totalBills").empty();
                $("#totalBills").append(data.total_bills);

                $("#totalSells").empty();
                $("#totalSells").append(data.total_sells.toFixed());

                $("#totalPurchase").empty();
                $("#totalPurchase").append(data.total_purchase.toFixed());

                $("#customerDues").empty();
                $("#customerDues").append(data.customer_dues.toFixed());

                $("#supplierDues").empty();
                $("#supplierDues").append(data.supplier_dues.toFixed());


                //Top Selling Products Start

                    //Variations
                        if (data.top_products[0].variations != null)
                        {
                            var variation1  = ' - ' + data.top_products[0].variations;
                        }
                        else
                        {
                            var variation1  = '';
                        }

                        if (data.top_products[1].variations != null)
                        {
                            var variation2  = ' - ' + data.top_products[1].variations;
                        }
                        else
                        {
                            var variation2  = '';
                        }

                        if (data.top_products[2].variations != null)
                        {
                            var variation3  = ' - ' + data.top_products[2].variations;
                        }
                        else
                        {
                            var variation3  = '';
                        }

                        if (data.top_products[3].variations != null)
                        {
                            var variation4  = ' - ' + data.top_products[3].variations;
                        }
                        else
                        {
                            var variation4  = '';
                        }

                        if (data.top_products[4].variations != null)
                        {
                            var variation5  = ' - ' + data.top_products[4].variations;
                        }
                        else
                        {
                            var variation5  = '';
                        }

                    var arr_length   = data.top_products.length;
                
                    if (data.top_products[0].total_sold > 0)
                    {
                        var chart = new CanvasJS.Chart("chartContainer1", {
                            // theme: "light2", // "light1", "light2", "dark1", "dark2"
                            // exportEnabled: false,
                            animationEnabled: true,
                            // title: {
                            //   text: "Top Selling Products"
                            // },
                            data: [{
                              type: "pie",
                              startAngle: 25,
                              toolTipContent: "<b>{label}</b>: {y}%",
                              // showInLegend: "true",
                              legendText: "{}",
                              indexLabelFontSize: 14,
                              indexLabel: "{label} : {y}%",
                              dataPoints: [
                                
                                    { y: ((parseFloat(data.top_products[0].total_sold) * 100)/parseFloat(data.top_products_total_sold)).toFixed(2), label: '"'+ data.top_products[0].name + variation1 +'"' },
                                    { y: ((parseFloat(data.top_products[1].total_sold) * 100)/parseFloat(data.top_products_total_sold)).toFixed(2), label: '"'+ data.top_products[1].name + variation2 +'"' },
                                    { y: ((parseFloat(data.top_products[2].total_sold) * 100)/parseFloat(data.top_products_total_sold)).toFixed(2), label: '"'+ data.top_products[2].name + variation3 +'"' },
                                    { y: ((parseFloat(data.top_products[3].total_sold) * 100)/parseFloat(data.top_products_total_sold)).toFixed(2), label: '"'+ data.top_products[3].name + variation4 +'"' },
                                    { y: ((parseFloat(data.top_products[4].total_sold) * 100)/parseFloat(data.top_products_total_sold)).toFixed(2), label: '"'+ data.top_products[4].name +  variation5 +'"' }
                                ]
                            }]
                        });

                        setTimeout(function(){ 
                            chart.render(); }, 
                        3000);

                        function toggleDataSeries(e) {
                            if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                                e.dataSeries.visible = false;
                            }
                            else {
                                e.dataSeries.visible = true;
                            }

                            setTimeout(function(){ 
                                chart.render(); }, 
                            3000);
                        }
                    }
                //Top Selling Products End

                //Sales Summary Chart Start
                    var mounth               = [];
                    var amount               = [];

                    $.each(data.sales_summary,function (e, value) {

                        if (value.month == 1)
                        {
                            var SetMonth = "Jan";
                        }

                        if (value.month == 2)
                        {
                            var SetMonth = "Feb";
                        }

                        if (value.month == 3)
                        {
                            var SetMonth = "Mar";
                        }

                        if (value.month == 4)
                        {
                            var SetMonth = "Apr";
                        }

                        if (value.month == 5)
                        {
                            var SetMonth = "May";
                        }

                        if (value.month == 6)
                        {
                            var SetMonth = "Jun";
                        }

                        if (value.month == 7)
                        {
                            var SetMonth = "Jul";
                        }

                        if (value.month == 8)
                        {
                            var SetMonth = "Aug";
                        }

                        if (value.month == 9)
                        {
                            var SetMonth = "Sep";
                        }

                        if (value.month == 10)
                        {
                            var SetMonth = "Oct";
                        }

                        if (value.month == 11)
                        {
                            var SetMonth = "Nov";
                        }

                        if (value.month == 12)
                        {
                            var SetMonth = "Dec";
                        }

                        mounth.push(SetMonth);
                        amount.push(value.total);
                    });

                    var myChart = echarts.init(document.getElementById('chartContainer3'));

                    colors = ['#00C8C4'];
                    option = {
                        color:colors,
                        tooltip : {
                            trigger: 'axis',
                            
                        },
                        grid: {
                            left: '12%',
                            right: '1%',
                            bottom: '8%',
                            top:'10%',
                        },
                        xAxis : [
                            {
                                type : 'category',
                                data : mounth
                            }
                        ],
                        yAxis : [
                            {
                                type : 'value',
                            }
                        ],
                        series : [
                            {
                                name:'Sales',
                                type:'bar',
                                data:amount
                            }
                        ]
                    };

                    var salesY = option;

                    setTimeout(function(){ 
                        myChart.setOption(salesY); }, 
                    3000);
                //Sales Summary Chart End

                //Purchase Summary Chart Start
                    var mounth               = [];
                    var amount               = [];

                    $.each(data.purchase_summary,function (e, value) {

                        if (value.month == 1)
                        {
                            var SetMonth = "Jan";
                        }

                        if (value.month == 2)
                        {
                            var SetMonth = "Feb";
                        }

                        if (value.month == 3)
                        {
                            var SetMonth = "Mar";
                        }

                        if (value.month == 4)
                        {
                            var SetMonth = "Apr";
                        }

                        if (value.month == 5)
                        {
                            var SetMonth = "May";
                        }

                        if (value.month == 6)
                        {
                            var SetMonth = "Jun";
                        }

                        if (value.month == 7)
                        {
                            var SetMonth = "Jul";
                        }

                        if (value.month == 8)
                        {
                            var SetMonth = "Aug";
                        }

                        if (value.month == 9)
                        {
                            var SetMonth = "Sep";
                        }

                        if (value.month == 10)
                        {
                            var SetMonth = "Oct";
                        }

                        if (value.month == 11)
                        {
                            var SetMonth = "Nov";
                        }

                        if (value.month == 12)
                        {
                            var SetMonth = "Dec";
                        }

                        mounth.push(SetMonth);
                        amount.push(value.total);
                    });

                    var myChart1 = echarts.init(document.getElementById('chartContainer4'));

                    colors = ['#28a745'];
                    option = {
                        color:colors,
                        tooltip : {
                            trigger: 'axis',
                            
                        },
                        grid: {
                            left: '12%',
                            right: '1%',
                            bottom: '8%',
                            top:'10%',
                        },
                        xAxis : [
                            {
                                type : 'category',
                                data : mounth
                            }
                        ],
                        yAxis : [
                            {
                                type : 'value',
                            }
                        ],
                        series : [
                            {
                                name:'Purchase',
                                type:'bar',
                                data:amount
                            }
                        ]
                    };

                    var salesY1 = option;
                    setTimeout(function(){ 
                        myChart1.setOption(salesY1); }, 
                    3000);
                //Purchase Summary Chart End
            });

            $("#DueInvoiceYear").addClass("active"); 
        });
    
        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }

        function dueInvoiceClick()
        {
            var click_id  = $(this).id;

            console.log(click_id);
        }
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
    <!-- echarts js -->
    <script src="<?php echo e(url('public/admin_panel_assets/libs/echarts/echarts.min.js')); ?>"></script>
    <!-- echarts init -->
    <script src="<?php echo e(url('public/admin_panel_assets/js/pages/echarts.init.js')); ?>"></script>
    <script src="<?php echo e(url('public/admin_panel_assets/js/canvasCharts.min.js')); ?>"></script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/cs_demo/resources/views/home.blade.php ENDPATH**/ ?>