<!DOCTYPE html >
<html>

<head>
    <meta charset="utf-8">
    <title>Return Cash Memo</title>
    <link href="https://fonts.maateen.me/bangla/font.css" rel="stylesheet">

    <style>
        *{margin:0;padding:0;outline:0}

        body {
            font-size:14px;
            line-height:18px;
            color:#000;
            height:292mm;
            width: 203mm;/*297*/
            margin:0 auto;
            font-family: 'Bangla', Arial, sans-serif !important;  
        }

        table,th {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 10px;
        }

        td {
            border-left: 1px solid black;
            border-right: 1px solid black;
            padding: 10px;
        }

        .bg_templete{
            background:url(images/bg3.png) no-repeat;
            background-size: 100%;
            -webkit-print-color-adjust: exact;
        }

        .templete{
            width:90%;
            background:none;
            margin:0 auto;
        }
        
        .clear{
            overflow:hidden;
        }

        .text-center{
            text-align: center;
        }

        .text-end{
            text-align: end;
        }

        .item2 {
            grid-area: com_logo;
        }

        .item3 {
            grid-area: com_info;
        }

        .item4 {
            grid-area: other_info;
        }

        .grid-container {
            display: grid;
            grid-template-areas: "com_logo com_info other_info ";
            /*padding-top: 10px;*/
        }

        .grid-container div {
            text-align: center;
        }

        .grid-container-aline-end {
            text-align: end !important;
        }

        .grid-container-aline-start {
            text-align: start !important;
        }

        .com_logo_img {
            height: 80px;
            width: 80px;
        }

        .tr-height{
            padding: 10px;
        }

        .item11 { grid-area: sig_text2; }
        .item22 { grid-area: some_info; }
        .item33 { grid-area: sig1; }
        .item44 { grid-area: sig2; }
        .item55 { grid-area: sig_text1; }

        .grid-container-1 {
            display: grid;
            grid-template-areas:
            ' sig1 sig1 some_info some_info sig2 sig2'
            ' sig_text1 sig_text1 some_info some_info sig_text2 sig_text2';
        }

        .signaturesection1 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 45px;
        }

        .signaturesection2 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 45px;
        }

        @page  {
            size: A4;
            page-break-after: always;
        }
    </style>
</head>

<body>
    <div class="bg_templete clear">

        <div class="templete clear">

            <br>
            <br>
            <p style="line-height: 2;text-align: center"><span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 20px">Return Cash Memo</span></p>
            <div class="grid-container">
                <div class="item2 grid-container-aline-start">
                    <img class="com_logo_img" src="<?php echo e(url('public/'.userDetails()->logo)); ?>"alt="qr sample"
                    />
                </div>

                <div class="item3">
                    <h2 style="line-height: 1;font-size: 60px"><?php echo e(userDetails()->organization_name); ?></h2>
                    <h4 style="line-height: 1;font-size: 20px"><?php echo e(userDetails()->address); ?></h4>
                    <p style="line-height: 1;font-size: 20px"><?php echo e(userDetails()->contact_number); ?></p>
                    <p style="line-height: 1;font-size: 20px"><?php echo e(userDetails()->contact_email); ?></p>
                    
                </div>

                <div class="item4 grid-container-aline-end">

                    <p style="padding-right: 25px;line-height: 1;font-size: 28px"></p>
                    
                </div>
            </div>

            <hr>

            <div style="font-size: 18px;padding-top: 10px;line-height: 1.5">
                <strong>Memo No &nbsp;:  </strong><?php echo e($sales_return['sales_return_number']); ?><strong style="float: right;">Date : <span><?php echo e(date('d-m-Y', strtotime($sales_return['sales_return_date']))); ?></span></strong>
            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong><?php echo e($sales_return['customer_name'] != null ? $sales_return['customer_name'] : $sales_return['contact_name']); ?>

            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Address &nbsp;&nbsp;: </strong><?php echo e($sales_return['customer_address'] != null ? $sales_return['customer_address'] : $sales_return['contact_address']); ?>

            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Phone &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong><?php echo e($sales_return['customer_phone'] != null ? $sales_return['customer_phone'] : $sales_return['contact_phone']); ?>

            </div>

            <div style="padding-top: 10px;padding-bottom: 20px">
                <table style="width: 100%">
                    <tr>
                        <th style="font-size: 18px;width: 5%">SL</th>
                        <th style="font-size: 18px;width: 60%">Description</th>
                        <th style="font-size: 18px;width: 15%">QTY</th>
                        <th style="font-size: 18px;width: 10%">Rate</th>
                        <th style="font-size: 18px;width: 10%">Total</th>
                    </tr>

                    <?php if($entries->count() > 0): ?>

                    <?php
                        $total_amount   = 0;
                    ?>

                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                        $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                        $variation_name = ProductVariationName($value['product_entry_id']);

                        if ($value['product_code'] != null)
                        {
                            $productCode  = ' - '.$value['product_code'];
                        }
                        else
                        {
                            $productCode  = '';
                        }

                        if ($value['product_name'] != null)
                        {
                            $category  = ' - '.$value['product_name'];
                        }
                        else
                        {
                            $category  = '';
                        }

                        if ($value['brand_name'] != null)
                        {
                            $brandName  = $value['brand_name'];
                        }
                        else
                        {
                            $brandName  = '';
                        }

                        if (($value['height'] != null) && ($value['width'] != null))
                        {
                            $dimension  = ' - '.$value['height'] . ' X ' . $value['width'];
                        }
                        else
                        {
                            $dimension  = '';
                        }

                        if ($value['unit_name'] != null)
                        {
                            $unit  = ' '.$value['unit_name'];
                        }
                        else
                        {
                            $unit  = ' SFT';
                        }

                        if ($variation_name != null)
                        {
                            $variation  = ' '.$variation_name;
                        }
                        else
                        {
                            $variation  = '';
                        }
                    ?>

                    <tr class="tr-height">
                        <td style="text-align: center"><?php echo e($key + 1); ?></td>
                        <td style="padding-left: 30px"><?php echo e($brandName . $productCode . $category . ' - ' . $value['product_entry_name'] . $dimension . $variation); ?></td>
                        <td style="text-align: center"><?php echo e($value['quantity'] . $unit); ?></td>
                        <td style="text-align: center"><?php echo e($value['rate']); ?></td>
                        <td style="text-align: center"><?php echo e(round($value['total_amount'], 2)); ?></td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

                    <tr>
                        <th style="text-align: left" colspan="2">In Words : <span style="font-weight: normal;"><?php echo e(numberTowords($total_amount)); ?></span></th>
                        <th></th>
                        <th style="text-align: right"><strong>Total</strong></th>
                        <th style="text-align: center"><?php echo e(round($total_amount, 2)); ?></th>
                    </tr>
                </table>
            </div>

            <div class="grid-container-1">

                <div class="item33"></div>  
                <div class="item55"><p class="signaturesection1" style="font-size: 18px;text-align: center">Customer Signature</p></div>

                <div class="item22" style="text-align: center;">
                    <h3 style="font-size: 18px"> </h3>
                    <div>
                        <p style="font-size: 18px"></p>
                        <p style="font-size: 18px"> </p>
                    </div>
                </div>

                <div class="item44"></div>
                <div class="item1"><p class="signaturesection2" style="font-size: 18px;text-align: center">Authorization Signature</p></div>
            </div>
        </div>
    </div>
</body>

</html><?php /**PATH /home/digishop7/public_html/46_1/Modules/SalesReturn/Resources/views/show.blade.php ENDPATH**/ ?>