<!DOCTYPE html>
<html>

<head>
    <title>Cash Book</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p><?php echo e($user_info['address']); ?></p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Cash Book</h6>
                    </div>

                    <div class="ibox-content">
                        <!-- Header Start -->
                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center"><?php echo e($from_date); ?> <strong>To</strong> <?php echo e($to_date); ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Header End -->

                        <!-- Sales Statement Start -->
                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 8%">DATE</th>
                                    <th style="text-align: center;width: 10%">V.ID</th>
                                    <th style="text-align: center;width: 10%">PARTICULARS</th>
                                    <th style="text-align: center;width: 15%">DESCRIPTION</th>
                                    <th style="text-align: center;width: 8%">DEBIT</th>
                                    <th style="text-align: center;width: 8%">CREDIT</th>
                                    <th style="text-align: center;width: 10%">BALANCE</th>
                                    <th style="text-align: center;width: 15%">ENTRY BY</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php
                                    $i                = 1;
                                    $total_debit      = 0;
                                    $total_credit     = 0;
                                    $total_balance    = $opening_balance;
                                ?>

                                <tr>
                                    <th colspan="7" style="text-align: right;">Opening Balance</th>
                                    <th style="text-align: right;"><?php echo e($opening_balance); ?></th>
                                    <th style="text-align: right;"></th>
                                </tr>

                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php
                                    if ($value['type'] == 1)
                                    {
                                        $debit          = $value['amount'];
                                        $total_debit    = $total_debit + $value['amount'];
                                        $credit         = 0;
                                    }

                                    if ($value['type'] == 0)
                                    {
                                        $credit         = $value['amount'];
                                        $total_credit   = $total_credit + $value['amount'];
                                        $debit          = 0;
                                    }

                                    if ($value['type'] == 2)
                                    {   
                                        if ($value['transfer_from'] == 1)
                                        {
                                            $debit          = $value['amount'];
                                            $total_debit    = $total_debit + $value['amount'];
                                            $credit         = 0;
                                        }
                                        
                                        if ($value['transfer_to'] == 1)
                                        {
                                            $credit         = $value['amount'];
                                            $total_credit   = $total_credit + $value['amount'];
                                            $debit          = 0;
                                        }
                                    }

                                    $total_balance      = $total_balance + $credit - $debit;

                                    if ($value['transaction_head'] == 'sales')
                                    {   
                                        preg_match('#\((.*?)\)#', $value->note, $match);
                                        $trans_number     = 'INV - '.str_pad($value->invoice->invoice_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head = $match[1] == 'বকেয়া বাবদ আদায়' ?  'Previous Due Collection': 'Sales of items';
                                        $transaction_note = $match[1] == 'বকেয়া বাবদ আদায়' ? str_replace("(বকেয়া বাবদ আদায়)", "", $value->note) : str_replace("(বিক্রয় বাবদ আদায়)", "", $value->note);
                                        $register         = $value->invoice->customer->name;
                                        $payment_methode  = $value->paid_through_id != null ? $value->paidThroughAccounts->name : '';
                                        $text             = 'Received from ';
                                    }
                                    elseif ($value['transaction_head'] == 'sales-return')
                                    {   
                                        $trans_number       = 'SR - '.str_pad($value->salesReturn->sales_return_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = 'Sales Return of items';
                                        $transaction_note   = str_replace("(রিটার্ন বাবদ ব্যায়)", "", $value->note);
                                        $register           = $value->salesReturn->customer->name;
                                        $payment_methode    = $value->paid_through_id != null ? $value->paidThroughAccounts->name : '';
                                        $text               = 'Return Back to ';
                                    }
                                    elseif ($value['transaction_head'] == 'purchase')
                                    {   
                                        preg_match('#\((.*?)\)#', $value->note, $match);
                                        $trans_number       = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = $match[1] == 'বকেয়া বাবদ প্রদান' ?  'Previous Due Paid': 'Purchase of items';
                                        $transaction_note   = $match[1] == 'বকেয়া বাবদ প্রদান' ?str_replace("(বকেয়া বাবদ প্রদান)", "", $value->note) : str_replace("(ক্রয় বাবদ প্রদান)", "", $value->note);
                                        $register           = $value->bill->vendor->name;
                                        $payment_methode    = $value->paid_through_id != null ? $value->paidThroughAccounts->name : '';
                                        $text               = 'Paid to ';
                                    }
                                    elseif ($value['transaction_head'] == 'purchase-return')
                                    {
                                        $trans_number       = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = 'Purchase Return of items';
                                        $transaction_note   = str_replace("(রিটার্ন বাবদ আয়)", "", $value->note);;
                                        $register           = $value->purchaseReturn->customer->name;
                                        $payment_methode    = $value->paid_through_id != null ? $value->paidThroughAccounts->name : '';
                                        $text               = 'Received Back from ';
                                    }
                                    elseif ($value['transaction_head'] == 'customer-advance')
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = 'Customer Advance';
                                        $transaction_note   = str_replace("(অগ্রীম বাবদ আদায়)", "", $value->note);
                                        $register           = $value->customer->name;
                                        $payment_methode    = $value->paid_through_id != null ? $value->paidThroughAccounts->name : '';
                                        $text               = 'Advance Received from ';
                                    }
                                    elseif ($value['transaction_head'] == 'supplier-advance')
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = 'Supplier Advance';
                                        $transaction_note   = str_replace("(অগ্রীম বাবদ প্রদান)", "", $value->note);
                                        $register           = $value->customer->name;
                                        $payment_methode    = $value->paid_through_id != null ? $value->paidThroughAccounts->name : '';
                                        $text               = 'Advance Paid To ';
                                    }
                                    elseif ($value['transaction_head'] == 'income')
                                    {
                                        $trans_number       = 'INC - '.str_pad($value->income->income_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = 'Income';
                                        $transaction_note   = $value->note;
                                        $register           = $value->income->incomeCategory->name;
                                        $payment_methode    = $value->paid_through_id != null ? $value->paidThroughAccounts->name : '';
                                        $text               = 'Received from ';
                                    }
                                    elseif ($value['transaction_head'] == 'expense')
                                    {
                                        $trans_number       = 'EXP - '.str_pad($value->expense->expense_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = 'Expense';
                                        $transaction_note   = $value->note;
                                        $register           = $value->expense->expenseCategory->name;
                                        $payment_methode    = $value->paid_through_id != null ? $value->paidThroughAccounts->name : '';
                                        $text               = 'Against ';
                                    }
                                    elseif ($value['transaction_head'] == 'customer-settlement')
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = '';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customerSettlement->customer->name;
                                        $payment_methode    = '';
                                        $text               = 'Return Customer Advance To';
                                    }
                                    elseif ($value['transaction_head'] == 'supplier-settlement')
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = '';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customerSettlement->customer->name;
                                        $payment_methode    = '';
                                        $text               = 'Return Back Advance From ';
                                    }
                                    else
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = '';
                                        $register           = '';
                                        $transaction_note   = '';
                                        $payment_methode    = '';
                                        $text               = '';
                                    }
                                ?>

                                <tr>
                                    <td style="text-align: center;"><?php echo e($i); ?></td>
                                    <td style="text-align: center;"><?php echo e($value['transaction_date']); ?></td>
                                    <?php if($value['transaction_head'] == null): ?>
                                    <td style="text-align: center;" colspan="2">Transfer From <strong><?php echo e($value->transferFrom->name); ?></strong> To  <strong><?php echo e($value->transferTo->name); ?></td>
                                    <?php else: ?>
                                    <td style="text-align: center;"><?php echo e($trans_number); ?></td>
                                    <td style="text-align: center;"><strong><?php echo e($transaction_head); ?></strong> <br> <?php echo e($transaction_note); ?></td>
                                    <?php endif; ?>
                                    <td style="text-align: left;"><?php echo e($text . ' ' . $register); ?></td>
                                    <td style="text-align: right;"><?php echo e($debit); ?></td>
                                    <td style="text-align: right;"><?php echo e($credit); ?></td>
                                    <td style="text-align: right;"><?php echo e($total_balance >= 0 ? $total_balance : '('.abs($total_balance).')'); ?></td>
                                    <td style="text-align: left;"><?php echo e(isset($value->createdBy->name) ? $value->createdBy->name : ''); ?></td>
                                </tr>

                                <?php
                                    $i++;
                                ?>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="5" style="text-align: right;">TOTAL</th>
                                    <th style="text-align: right;"><?php echo e($total_debit); ?></th>
                                    <th style="text-align: right;"><?php echo e($total_credit); ?></th>
                                    <th style="text-align: right;"><?php echo e($total_balance >= 0 ? $total_balance : '('.abs($total_balance).')'); ?></th>
                                    <th style="text-align: right;"></th>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- Sales Statement End -->

                        <!-- Footer Start -->
                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/46_1/Modules/Reports/Resources/views/income_expense_ledger_print.blade.php ENDPATH**/ ?>