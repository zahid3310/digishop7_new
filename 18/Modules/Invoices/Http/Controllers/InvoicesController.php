<?php

namespace Modules\Invoices\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\PaidThroughAccounts;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Users;
use App\Models\AccountTransactions;
use App\Models\Challans;
use App\Models\ChallanEntries;
use App\Models\ChallanItems;
use Carbon\Carbon;
use Response;
use DB;
use View;

class InvoicesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoices           = Invoices::where('invoices.type', 1)->orderBy('id', 'DESC')->first();
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();

        return view('invoices::index', compact('paid_accounts', 'invoices'));
    }

    public function AllSales()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                                ->get();

        return view('invoices::all_sales', compact('paid_accounts'));
    }
    
    public function AllUndeliveredSales()
    {
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                                ->get();

        return view('invoices::all_undelivered_invoice_list', compact('paid_accounts'));
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('invoices::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'          => 'required|date',
            'customer_id'           => 'required|integer',
            'product_entries.*'     => 'required|integer',
            'amount.*'              => 'required|numeric',
            'customer_id'           => 'required|integer',
            'reference_id'          => 'nullable|integer',
            'customer_name'         => 'nullable|string|max:255',
            'customer_phone'        => 'nullable|numeric',
            'invoice_note'          => 'nullable|string',
            'selling_date'          => 'required|date',
            'product_entries.*'     => 'required|integer',
            'rate.*'                => 'required|numeric',
            'quantity.*'            => 'required|numeric',
            'discount_type.*'       => 'required|integer',
            'discount_amount.*'     => 'nullable|numeric',
            'amount.*'              => 'required|numeric',
            'total_amount'          => 'required|numeric',
            'total_discount_type'   => 'nullable|integer',
            'vat_type'              => 'nullable|numeric',
            'vat_amount'            => 'nullable|numeric',
            'total_discount_amount' => 'nullable|numeric',
            'total_discount_note'   => 'nullable|string',
            'cash_given'            => 'nullable|numeric',
            'change_amount'         => 'nullable|numeric',
            'coupon_code'           => 'nullable|numeric',
            'account_information.*' => 'nullable|string',
            'paid_through.*'        => 'nullable|integer',
            'note.*'                => 'nullable|string',
            'amount_paid.*'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];
             
            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }

            $data_find                          = Invoices::orderBy('created_at', 'DESC')->first();
            // $invoice_number                     = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

            $invoice                            = new Invoices;;
            $invoice->invoice_number            = $data['invoice_number'];
            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->customer_name             = $data['customer_name'];
            $invoice->customer_phone            = $data['customer_phone'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $data['total_amount'];
            $invoice->total_buy_price           = $buy_price;
            $invoice->total_discount            = $discount;
            $invoice->customer_address          = $data['customer_address'];
            $invoice->total_vat                 = $vat;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->created_by                = $user_id;

            if ($invoice->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'         => $invoice['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'customer_id'        => $invoice['customer_id'],
                        'pcs'                => $data['pcs'][$key] != null ? $data['pcs'][$key] : 0,
                        'cartoon'            => $data['cartoon'][$key] != null ? $data['cartoon'][$key] : 0,
                        'reference_id'       => $data['reference_id'],
                        'buy_price'          => $product_buy_price['buy_price'] != null ? $product_buy_price['buy_price'] : 0,
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'total_amount'       => $data['amount'][$key],
                        'discount_type'      => $data['discount_type'][$key],
                        'discount_amount'    => $data['discount'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];

                    $challan_items[] = [
                        'invoice_id'         => $invoice['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'pcs'                => $data['pcs'][$key] != null ? $data['pcs'][$key] : 0,
                        'cartoon'            => $data['cartoon'][$key] != null ? $data['cartoon'][$key] : 0,
                        'invoice_quantity'   => $data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('invoice_entries')->insert($invoice_entries);

                DB::table('challan_items')->insert($challan_items);

                if (isset($data['generate_challan']))
                {
                    stockOut($data, $item_id=null);
                }

                if (isset($data['amount_paid']))
                {
                    $data_find        = Payments::orderBy('id', 'DESC')->first();
                    $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {   
                        if ($data['amount_paid'][$i] > 0)
                        {   
                            $account_transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through_id'       => $data['paid_through'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 0,  // 0 = In , 1 = Out
                                'transaction_head'      => 'sales',
                                'associated_id'         => $invoice->id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payments = [
                                'payment_number'        => $payment_number,
                                'customer_id'           => $data['customer_id'],
                                'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'paid_through'          => $data['paid_through'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 0,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payment_id = DB::table('payments')->insertGetId($payments);      

                            if ($payment_id)
                            {   
                                $payment_entries = [
                                        'payment_id'        => $payment_id,
                                        'invoice_id'        => $invoice['id'],
                                        'amount'            => $data['amount_paid'][$i],
                                        'created_by'        => $user_id,
                                        'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('payment_entries')->insert($payment_entries);  
                            }

                            $update_invoice_dues                = Invoices::find($invoice['id']);
                            $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                            $update_invoice_dues->save();

                            $payment_number++;
                        }
                    }

                    if (isset($account_transactions))
                    {
                        DB::table('account_transactions')->insert($account_transactions);
                    }
                }

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Sales Created Successfully !!");
                }
                else
                {
                    $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')                       
                                    ->select('invoices.*',
                                             'customers.name as contact_name',
                                             'customers.address as contact_address',
                                             'customers.phone as contact_phone')
                                    ->find($invoice['id']);

                    $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('invoice_entries.invoice_id', $invoice['id'])
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.type as type',
                                             'product_entries.name as product_entry_name',
                                             'product_entries.product_code as product_code',
                                             'units.name as unit_name',
                                             'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                             'sizes.height as height',
                                             'sizes.width as width',
                                             'categories.name as brand_name',
                                             'products.name as product_name')
                                    ->get();  
                                 
                    $user_info  = Users::find(1);

                    return View::make('invoices::show')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')                       
                                    ->select('invoices.*',
                                             'customers.name as contact_name',
                                             'customers.address as contact_address',
                                             'customers.phone as contact_phone')
                                    ->find($id);

        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('customers', 'customers.id', 'invoice_entries.customer_id')
                                    ->where('invoice_entries.invoice_id', $id)
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.type as type',
                                             'product_entries.name as product_entry_name',
                                             'product_entries.product_code as product_code',
                                             'customers.contact_type as contact_type',
                                             'units.name as unit_name',
                                             'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                             'sizes.height as height',
                                             'sizes.width as width',
                                             'categories.name as brand_name',
                                             'products.name as product_name')
                                    ->get();  
                     
        $user_info  = Users::find(1);

        return view('invoices::show', compact('entries', 'invoice', 'user_info'));
    }

    public function challan($id)
    {
        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')                       
                                    ->select('invoices.*',
                                             'customers.name as contact_name',
                                             'customers.address as contact_address',
                                             'customers.phone as contact_phone')
                                    ->find($id);

        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('invoice_entries.invoice_id', $id)
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.type as type',
                                             'product_entries.name as product_entry_name',
                                             'product_entries.product_code as product_code',
                                             'units.name as unit_name',
                                             'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                             'sizes.height as height',
                                             'sizes.width as width',
                                             'categories.name as brand_name',
                                             'products.name as product_name')
                                    ->get();  

        $user_info  = Users::find(1);

        return view('invoices::challan', compact('entries', 'invoice', 'user_info'));
    }

    public function showPos($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.address as address',
                                             'customers.phone as phone')
                                    ->find($id);

        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->where('invoice_entries.invoice_id', $id)
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.name as product_entry_name',
                                             'products.name as product_name')
                                    ->get();   
                     
        $user_info  = Users::find(1);

        return view('invoices::show', compact('entries', 'invoice', 'user_info'));
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $find_invoice           = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                            ->select('invoices.*',
                                                 'customers.id as customer_id',
                                                 'customers.name as contact_name',
                                                 'customers.phone as contact_phone')
                                            ->find($id);

        $find_invoice_entries   = InvoiceEntries::leftjoin('customers', 'customers.id', 'invoice_entries.customer_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                            ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->where('invoice_entries.invoice_id', $id)
                                            ->select('invoice_entries.*',
                                                    'customers.id as customer_id',
                                                    'customers.name as customer_name',
                                                    'product_entries.id as item_id',
                                                    'product_entries.type as product_type',
                                                    'units.name as unit_name',
                                                    'product_entries.product_code as product_code',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                                    'sizes.height as height',
                                                    'sizes.width as width',
                                                    'product_entries.name as item_name')
                                            ->get();

        $entries_count          = $find_invoice_entries->count();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->where('payment_entries.invoice_id', $id)
                                        ->selectRaw('payment_entries.*, 
                                                     payments.paid_through as paid_through_id,
                                                     payments.account_information as account_information,
                                                     payments.payment_date as payment_date,
                                                     payments.note as note')
                                        ->get();

        $payment_entries_count  = $payment_entries->count();
        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();
 
        return view('invoices::edit', compact('find_invoice', 'find_invoice_entries', 'entries_count', 'payment_entries', 'payment_entries_count', 'paid_accounts'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'          => 'required|date',
            'customer_id'           => 'required|integer',
            'product_entries.*'     => 'required|integer',
            'amount.*'              => 'required|numeric',
            'customer_id'           => 'required|integer',
            'reference_id'          => 'nullable|integer',
            'customer_name'         => 'nullable|string|max:255',
            'customer_phone'        => 'nullable|numeric',
            'invoice_note'          => 'nullable|string',
            'selling_date'          => 'required|date',
            'product_entries.*'     => 'required|integer',
            'rate.*'                => 'required|numeric',
            'quantity.*'            => 'required|numeric',
            'discount_type.*'       => 'required|integer',
            'discount_amount.*'     => 'nullable|numeric',
            'amount.*'              => 'required|numeric',
            'total_amount'          => 'required|numeric',
            'total_discount_type'   => 'nullable|integer',
            'vat_type'              => 'nullable|numeric',
            'vat_amount'            => 'nullable|numeric',
            'total_discount_amount' => 'nullable|numeric',
            'total_discount_note'   => 'nullable|string',
            'cash_given'            => 'nullable|numeric',
            'change_amount'         => 'nullable|numeric',
            'coupon_code'           => 'nullable|numeric',
            'account_information.*' => 'nullable|string',
            'paid_through.*'        => 'nullable|integer',
            'note.*'                => 'nullable|string',
            'amount_paid.*'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];
            $invoice        = Invoices::find($id);

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }

            //Calculate Due Amount

                if ($data['total_amount'] > $invoice['invoice_amount']) 
                {
                    $invoice_dues = $invoice['due_amount'] + ($data['total_amount'] - $invoice['invoice_amount']);

                }
                
                if ($data['total_amount'] < $invoice['invoice_amount'])
                {
                    $invoice_dues = $invoice['due_amount'] - ($invoice['invoice_amount'] - $data['total_amount']);
                }

                if ($data['total_amount'] == $invoice['invoice_amount'])
                {
                    $invoice_dues = $invoice['due_amount'];
                }
         
            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->customer_name             = $data['customer_name'];
            $invoice->customer_phone            = $data['customer_phone'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $invoice_dues;
            $invoice->total_buy_price           = $buy_price;
            $invoice->total_discount            = $discount;
            $invoice->customer_address          = $data['customer_address'];
            $invoice->total_vat                 = $vat;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->discount_code             = $data['coupon_code'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->updated_by                = $user_id;

            if ($invoice->save())
            {
                $item_id                = InvoiceEntries::where('invoice_id', $invoice['id'])->get();
                $item_delete            = InvoiceEntries::where('invoice_id', $invoice['id'])->delete();

                if (isset($data['payment_id']))
                {
                    foreach ($data['payment_id'] as $key_p => $value_p)
                    {
                        $payment_delete  = Payments::where('id', $value_p)->delete();
                    }

                    $update_invoice                = Invoices::find($invoice->id);
                    $update_invoice->due_amount    = $update_invoice['invoice_amount'];
                    $update_invoice->updated_by    = $user_id;
                    $update_invoice->save();
                }

                foreach ($data['product_entries'] as $key => $value)
                {   
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'         => $invoice['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'customer_id'        => $invoice['customer_id'],
                        'pcs'                => $data['pcs'][$key] != null ? $data['pcs'][$key] : 0,
                        'cartoon'            => $data['cartoon'][$key] != null ? $data['cartoon'][$key] : 0,
                        'reference_id'       => $data['reference_id'],
                        'buy_price'          => $product_buy_price['buy_price'] != null ? $product_buy_price['buy_price'] : 0,
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'total_amount'       => $data['amount'][$key],
                        'discount_type'      => $data['discount_type'][$key],
                        'discount_amount'    => $data['discount'][$key],
                        'created_by'         => $user_id,
                    ];

                    $old_chalan_item    = ChallanItems::where('invoice_id', $invoice->id)
                                                    ->where('product_entry_id', $value)
                                                    ->first();

                    if ($old_chalan_item != null)
                    {
                        $challan_items[] = [
                            'invoice_id'         => $invoice['id'],
                            'product_id'         => $product_buy_price['product_id'],
                            'product_entry_id'   => $value,
                            'pcs'                => $data['pcs'][$key] != null ? $data['pcs'][$key] : 0,
                            'cartoon'            => $data['cartoon'][$key] != null ? $data['cartoon'][$key] : 0,
                            'invoice_quantity'   => $data['quantity'][$key],
                            'delivered_quantity' => $old_chalan_item['delivered_quantity'],
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];
                    }
                    else
                    {
                        $challan_items[] = [
                            'invoice_id'         => $invoice['id'],
                            'product_id'         => $product_buy_price['product_id'],
                            'product_entry_id'   => $value,
                            'pcs'                => $data['pcs'][$key] != null ? $data['pcs'][$key] : 0,
                            'cartoon'            => $data['cartoon'][$key] != null ? $data['cartoon'][$key] : 0,
                            'invoice_quantity'   => $data['quantity'][$key],
                            'delivered_quantity' => 0,
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                $old_chalan_item_delete     = ChallanItems::where('invoice_id', $invoice->id)->delete();

                DB::table('invoice_entries')->insert($invoice_entries);
                DB::table('challan_items')->insert($challan_items);

                stockOut($data, $item_id);

                $transaction_delete = AccountTransactions::where('transaction_head', 'sales')->where('associated_id', $invoice['id'])->delete();

                if (isset($data['amount_paid']))
                {   
                    $data_find_payment  = Payments::orderBy('id', 'DESC')->first();
                    $payment_number     = $data_find_payment != null ? $data_find_payment['payment_number'] + 1 : 1;

                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {   
                        if ($data['amount_paid'][$i] > 0)
                        {   
                            $account_transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through_id'       => $data['paid_through'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 0,  // 0 = In , 1 = Out
                                'transaction_head'      => 'sales',
                                'associated_id'         => $invoice->id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payments = [
                                'payment_number'        => $payment_number,
                                'customer_id'           => $data['customer_id'],
                                'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through'          => $data['paid_through'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 0,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payment_id = DB::table('payments')->insertGetId($payments);      

                            if ($payment_id)
                            {   
                                $payment_entries = [
                                        'payment_id'        => $payment_id,
                                        'invoice_id'        => $invoice['id'],
                                        'amount'            => $data['amount_paid'][$i],
                                        'created_by'        => $user_id,
                                        'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('payment_entries')->insert($payment_entries);  
                            }

                            $update_invoice_dues                = Invoices::find($invoice['id']);
                            $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                            $update_invoice_dues->save();
 
                            $payment_number++;
                        }
                    }

                    if (isset($account_transactions))
                    {
                        DB::table('account_transactions')->insert($account_transactions);
                    }
                }

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return redirect()->route('invoices_index')->with("success","Sales Updated Successfully !!");
                }
                else
                {
                    if (Auth::user()->service_type == 1)
                    {
                        return redirect()->route('invoices_show', $invoice['id']);
                    }
                    else
                    {
                        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')                       
                                    ->select('invoices.*',
                                             'customers.name as contact_name',
                                             'customers.address as contact_address',
                                             'customers.phone as contact_phone')
                                    ->find($invoice['id']);

                        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('invoice_entries.invoice_id', $invoice['id'])
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.type as type',
                                             'product_entries.name as product_entry_name',
                                             'product_entries.product_code as product_code',
                                             'units.name as unit_name',
                                             'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                             'sizes.height as height',
                                             'sizes.width as width',
                                             'categories.name as brand_name',
                                             'products.name as product_name')
                                    ->get();  
                                     
                        $user_info  = Users::find(1);

                        return View::make('invoices::show')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info);
                    }
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function productList()
    {
        $data       = ProductEntries::where('product_entries.stock_in_hand', '>', 0)
                                    ->Where('product_entries.stock_in_hand', '!=', null)
                                    ->orderBy('product_entries.total_sold', 'DESC')
                                    ->select('product_entries.*')
                                    ->get();

        return Response::json($data);
    }

    public function makePayment($id)
    {
        $data       = Invoices::find($id);

        return Response::json($data);
    }

    public function storePayment(Request $request)
    {
        $user_id                    = Auth::user()->id;
        $data                       = $request->all();

        DB::beginTransaction();

        try{

            if (isset($data['amount_paid']))
            {
                $data_find        = Payments::orderBy('id', 'DESC')->first();
                $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                for($i = 0; $i < count($data['amount_paid']); $i++)
                {   
                    if ($data['amount_paid'][$i] > 0)
                    {   
                        $account_transactions[] = [
                            'customer_id'           => $data['customer_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $data['amount_paid'][$i],
                            'paid_through_id'       => $data['paid_through'][$i],
                            'account_information'   => $data['account_information'][$i],
                            'note'                  => $data['note'][$i],
                            'type'                  => 0,  // 0 = In , 1 = Out
                            'transaction_head'      => 'sales',
                            'associated_id'         => $data['invoice_id'],
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $payments = [
                            'payment_number'        => $payment_number,
                            'customer_id'           => $data['customer_id'],
                            'payment_date'          => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $data['amount_paid'][$i],
                            'paid_through'          => $data['paid_through'][$i],
                            'account_information'   => $data['account_information'][$i],
                            'note'                  => $data['note'][$i],
                            'type'                  => 0,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $payment_id = DB::table('payments')->insertGetId($payments);      

                        if ($payment_id)
                        {
                            $payment_entries = [
                                    'payment_id'        => $payment_id,
                                    'invoice_id'        => $data['invoice_id'],
                                    'amount'            => $data['amount_paid'][$i],
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            DB::table('payment_entries')->insert($payment_entries);  
                        }

                        $update_invoice_dues                = Invoices::find($data['invoice_id']);
                        $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                        $update_invoice_dues->save();

                        $payment_number++;
                    }
                }

                if (isset($account_transactions))
                {
                    DB::table('account_transactions')->insert($account_transactions);
                }
            }
            
            DB::commit();
            return Response::json(1);
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return Response::json($exception);
        }
    }

    public function productPriceList($id)
    {
        $data   = ProductEntries::leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                ->selectRaw('product_entries.id as id,
                                             product_entries.type as type,
                                             product_entries.unit_id as unit_id,
                                             units.name as unit_name,
                                             product_entries.sell_price as sell_price,
                                             product_entries.buy_price as buy_price,
                                             product_entries.stock_in_hand as stock_in_hand,
                                             sizes.height as height,
                                             sizes.width as width,
                                             product_entries.pcs_per_cartoon as pcs_per_cartoon
                                            ')
                                            ->find($id);

        return Response::json($data);
    }

    public function invoiceListLoad()
    {
        $data           = Invoices::leftjoin('sales_return', 'sales_return.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->where('invoices.type', 1)
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                            'sales_return.id as return_id',
                                            'customers.name as contact_name',
                                            'customers.phone as contact_phone')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function invoiceListSearch($from_date, $to_date, $customer_type, $memo_number, $name, $phone, $customer)
    {
        $search_by_from_date        = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date          = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
     
        $explode_invoice  = explode(' - ', $memo_number);

        if (isset($explode_invoice[1]))
        {
            $invoice_number  = ltrim($explode_invoice[1],"0");;
        }
        else
        {
            $invoice_number  = 0;
        }

        $data           = Invoices::leftjoin('sales_return', 'sales_return.invoice_id', 'invoices.id')
                                        ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                        ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                        ->where('invoices.type', 1)
                                        ->when($search_by_from_date != 0 && $search_by_to_date != 0, function ($query) use ($search_by_from_date, $search_by_to_date) {
                                            return $query->whereBetween('invoices.invoice_date', [$search_by_from_date, $search_by_to_date]);
                                        })
                                        ->when($customer_type == 1, function ($query) use ($customer_type) {
                                            return $query->where('customers.contact_type', 0);
                                        })
                                        ->when($customer_type == 2, function ($query) use ($customer_type) {
                                            return $query->where('customers.contact_type', 4);
                                        })
                                        ->when($customer_type == 3, function ($query) use ($customer_type) {
                                            return $query->where('invoices.customer_id', 1);
                                        })
                                        ->when($invoice_number != 0, function ($query) use ($invoice_number) {
                                            return $query->where('invoices.invoice_number', $invoice_number);
                                        })
                                        ->when($phone != 0, function ($query) use ($phone) {
                                            return $query->where('invoices.customer_phone', $phone);
                                        })
                                        ->when($name != 0, function ($query) use ($name) {
                                            return $query->where('invoices.customer_name', 'LIKE', "%$name%");
                                        })
                                        ->when($customer != 0, function ($query) use ($customer) {
                                            return $query->where('invoices.customer_id', $customer);
                                        })
                                        ->orderBy('invoices.created_at', 'DESC')
                                        ->select('invoices.*',
                                                 'sales_return.id as return_id',
                                                 'customers.name as contact_name',
                                                 'customers.phone as contact_phone')
                                        ->distinct('invoices.id')
                                        ->take(100)
                                        ->get();

        return Response::json($data);
    }

    public function posSearchProduct($id)
    {
        $data           = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('invoices.type', 1)
                                    ->where('product_entries.product_code', $id)
                                    ->where('product_entries.stock_in_hand', '>', 0)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->first();

        return Response::json($data);
    }

    public function customerStore(Request $request)
    {
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        $customers                              = new Customers;
        $customers->name                        = $data['customer_name'];
        $customers->address                     = $data['address'];
        $customers->phone                       = $data['mobile_number'];
        $customers->contact_type                = $data['contact_type'];
        $customers->created_by                  = $user_id;

        if ($customers->save())
        {   
            return Response::json($customers);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function customersListInvoice()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Customers::orderBy('customers.created_at', 'DESC')
                                    ->take(500)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Customers::where('customers.name', 'LIKE', "%$search%")
                                    ->orWhere('customers.phone', 'LIKE', "%$search%")
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "phone" =>'', "address" =>'', "contact_type" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name'], "phone" =>$value['phone'], "address" =>$value['address'], "contact_type" =>$value['contact_type']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function couponCode($id)
    {
        $today          = Carbon::today();

        $data           = Discounts::leftjoin('discount_products', 'discount_products.discount_id', 'discounts.id')
                                ->where('discounts.coupon_code', $id)
                                ->orWhere('discounts.card_number', $id)
                                ->whereDate('discounts.expire_date', '>=', $today->format('Y-m-d'))
                                ->where('discounts.status', 1)
                                ->select('discount_products.product_id as product_id', 
                                         'discounts.discount_type as discount_type',
                                         'discounts.discount_amount as discount_amount',
                                         'discounts.expire_date as expire_date'
                                        )
                                ->get();

        return Response::json($data);
    }

    public function printInvoicesList()
    {
        $user_id        = Auth::user()->id;
        $data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->where('invoices.type', 1)
                                    ->where('invoices.created_by', $user_id)
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                            'customers.name as customer_name',
                                            'customers.phone as phone',
                                            'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function printInvoicesSearch($date,$customer,$invoice_number)
    {
        $search_by_date              = $date != 0 ? date('Y-m-d', strtotime($date)) : 0;
        $search_by_invoice_number    = $invoice_number != 0 ? $invoice_number : 0;
        $search_by_invoice_customer  = $customer != 0 ? $customer : 0;
        $user_info                   = userDetails();

        $data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->where('invoices.type', 1)
                                    ->when($search_by_date != 0, function ($query) use ($search_by_date) {
                                        return $query->where('invoices.invoice_date', 'LIKE', "%$search_by_date%");
                                    })
                                    ->when($search_by_invoice_number != 0, function ($query) use ($search_by_invoice_number) {
                                        return $query->where('invoices.invoice_number', 'LIKE', "%$search_by_invoice_number%");
                                    })
                                    ->when($search_by_invoice_customer != 0, function ($query) use ($search_by_invoice_customer) {
                                        return $query->where('customers.name', 'LIKE', "%$search_by_invoice_customer%");
                                    })
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.phone as phone',
                                             'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function calculateOpeningBalance($customer_id)
    {
        $invoices               = Invoices::where('invoices.customer_id', $customer_id)
                                            ->select('invoices.*')
                                            ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->leftjoin('sales_return', 'sales_return.id', 'payment_entries.sales_return_id')
                                                ->where('payments.customer_id', $customer_id)
                                                ->where('payments.type', 2)
                                                ->select('payment_entries.*',
                                                         'payments.customer_id as customer_id')
                                                ->get();

        $due_amount             = $invoices->sum('due_amount');
        $return_amount          = $invoices->sum('return_amount');
        $return_paid_amount     = $payment_entries_return->sum('amount');
        $return_due_amount      = $return_amount - $return_paid_amount;

        $balance                = $due_amount + $return_due_amount; 

        return Response::json($balance);
    }

    public function challanList($invoice_id)
    {
        $challans  = Challans::where('invoice_id', $invoice_id)->orderBy('id', 'ASC')->get();

        return view('invoices::challan_list', compact('challans', 'invoice_id'));
    }

    public function generateChallan($invoice_id)
    {
        $challan_items  = ChallanItems::leftjoin('product_entries', 'product_entries.id', 'challan_items.product_entry_id')
                                    ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->where('challan_items.invoice_id', $invoice_id)
                                    ->select('challan_items.*',
                                             'product_entries.id as item_id',
                                             'product_entries.type as product_type',
                                             'units.name as unit_name',
                                             'product_entries.product_code as product_code',
                                             'product_entries.stock_in_hand as stock_in_hand',
                                             'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                             'sizes.height as height',
                                             'sizes.width as width',
                                             'product_entries.name as item_name')
                                            ->get();

        $find_invoice   = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->select('invoices.*',
                                         'customers.id as customer_id',
                                         'customers.name as contact_name',
                                         'customers.phone as contact_phone')
                                    ->find($invoice_id);

        return view('invoices::generate_challan', compact('challan_items', 'invoice_id', 'find_invoice'));
    }

    public function generateChallanStore(Request $request, $invoice_id)
    {
        $rules = array(
            'product_entries.*'     => 'required|integer',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $data_find                          = Challans::orderBy('created_at', 'DESC')->first();
            $challan_number                     = $data_find != null ? $data_find['challan_number'] + 1 : 1;

            $challan                            = new Challans;;
            $challan->challan_number            = $challan_number;
            $challan->date                      = date('Y-m-d', strtotime($data['challan_date']));
            $challan->invoice_id                = $invoice_id;
            $challan->created_by                = $user_id;

            if ($challan->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $challan_entries[] = [
                        'challan_id'         => $challan['id'],
                        'invoice_id'         => $invoice_id,
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'pcs'                => $data['challan_pcs'][$key] != null ? $data['challan_pcs'][$key] : 0,
                        'cartoon'            => $data['challan_cart'][$key] != null ? $data['challan_cart'][$key] : 0,
                        'quantity'           => $data['challan_quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];

                    $update_challan_item    = ChallanItems::where('invoice_id', $invoice_id)
                                                            ->where('product_id', $product_buy_price['product_id'])
                                                            ->where('product_entry_id', $value)
                                                            ->first();

                    $update_challan_item->delivered_quantity    = $update_challan_item['delivered_quantity'] + $data['challan_quantity'][$key];
                    $update_challan_item->save();

                    //update main product stock start
                    $product_buy_price->stock_in_hand     = $product_buy_price['stock_in_hand'] - $data['challan_quantity'][$key];
                    $product_buy_price->total_sold        = $product_buy_price['total_sold'] + $data['challan_quantity'][$key];
                    $product_buy_price->save();
                    //update main product stock end
                }

                DB::table('challan_entries')->insert($challan_entries);

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Sales Created Successfully !!");
                }
                else
                {
                    return redirect()->route('invoices_show_challan', $challan->id);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function editChallan($id, $invoice_id)
    {
        $challan        = Challans::find($id);

        $entries        = ChallanEntries::leftjoin('product_entries', 'product_entries.id', 'challan_entries.product_entry_id')
                                    ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->where('challan_entries.challan_id', $id)
                                    ->select('challan_entries.*',
                                             'product_entries.id as item_id',
                                             'product_entries.type as product_type',
                                             'units.name as unit_name',
                                             'product_entries.product_code as product_code',
                                             'product_entries.stock_in_hand as stock_in_hand',
                                             'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                             'sizes.height as height',
                                             'sizes.width as width',
                                             'product_entries.name as item_name')
                                            ->get();

        $find_invoice   = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->select('invoices.*',
                                         'customers.id as customer_id',
                                         'customers.name as contact_name',
                                         'customers.phone as contact_phone')
                                    ->find($invoice_id);
        
        $user_info      = Users::find(1);

        return view('invoices::edit_challan', compact('entries', 'challan', 'user_info', 'find_invoice'));
    }

    public function generateChallanUpdate(Request $request, $challan_id, $invoice_id)
    {
        $rules = array(
            'product_entries.*'     => 'required|integer',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $challan                            = Challans::find($challan_id);
            $challan->date                      = date('Y-m-d', strtotime($data['challan_date']));
            $challan->updated_by                = $user_id;

            if ($challan->save())
            {   
                foreach ($data['product_entries'] as $key => $value)
                {   
                    if ($data['challan_quantity'][$key] != null)
                    {
                        $product_buy_price = ProductEntries::find($value);

                        $challan_entries[] = [
                            'challan_id'         => $challan['id'],
                            'invoice_id'         => $invoice_id,
                            'product_id'         => $product_buy_price['product_id'],
                            'product_entry_id'   => $value,
                            'pcs'                => $data['challan_pcs'][$key] != null ? $data['challan_pcs'][$key] : 0,
                            'cartoon'            => $data['challan_cart'][$key] != null ? $data['challan_cart'][$key] : 0,
                            'quantity'           => $data['challan_quantity'][$key],
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];

                        $update_challan_item    = ChallanItems::where('invoice_id', $invoice_id)
                                                                ->where('product_id', $product_buy_price['product_id'])
                                                                ->where('product_entry_id', $value)
                                                                ->first();

                        $update_challan_item->delivered_quantity    = $update_challan_item['delivered_quantity'] + $data['challan_quantity'][$key] - $data['old_challan_quantity'][$key];
                        $update_challan_item->save();

                        //update main product stock start
                        $product_buy_price->stock_in_hand     = $product_buy_price['stock_in_hand'] + $data['old_challan_quantity'][$key] - $data['challan_quantity'][$key];
                        $product_buy_price->save();
                        //update main product stock end
                    }
                }

                if(isset($challan_entries))
                {   
                    $challan_entries_delete  = ChallanEntries::where('challan_id', $challan_id)->delete();
                    DB::table('challan_entries')->insert($challan_entries);
                }

                DB::commit();

                if ($data['print'] == 1)
                {
                    return redirect()->route('invoices_all_challan_list', $challan->id)->with("success","Challan Updated Successfully !!");
                }
                else
                {
                    return redirect()->route('invoices_show_challan', $challan->id);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function showChallan($id)
    {
        $challan    = Challans::find($id);

        $entries    = ChallanEntries::where('challan_entries.challan_id', $id)
                                    ->select('challan_entries.*')
                                    ->get();  
                     
        $user_info  = Users::find(1);

        return view('invoices::challan_show', compact('entries', 'challan', 'user_info'));
    }

    public function deletetChallan($challan_id)
    {
        DB::beginTransaction();

        try{
            $challans     = ChallanEntries::where('challan_id', $challan_id)->get();

            foreach ($challans as $key => $value)
            {
                $update_challan_item                        = ChallanItems::where('invoice_id', $value['invoice_id'])->where('product_entry_id', $value['product_entry_id'])->first();
                $update_challan_item->delivered_quantity    = $update_challan_item['delivered_quantity'] - $value['quantity'];
                $update_challan_item->save();

                $update_product_entries                   = ProductEntries::find($value['product_entry_id']);
                $update_product_entries->stock_in_hand    = $update_product_entries['stock_in_hand'] + $value['quantity'];
                $update_product_entries->save();
            }

            $delete_chalan  = Challans::find($challan_id);

            if ($delete_chalan->delete())
            {   
                DB::commit();
                return back()->with('success', 'Challan deleted successfully');
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function allChallanList()
    {
        $challans  = Challans::orderBy('created_at', 'DESC')->get();

        return view('invoices::all_challan_list', compact('challans'));
    }
    
    public function undeliveredInvoiceListLoad()
    {
        $data           = ChallanItems::leftjoin('invoices', 'invoices.id', 'challan_items.invoice_id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->orderBy('challan_items.created_at', 'DESC')
                                    ->select('challan_items.*',
                                            'invoices.invoice_date as invoice_date',
                                            'invoices.invoice_number as invoice_number',
                                            'customers.name as contact_name',
                                            'customers.phone as contact_phone')
                                    ->distinct('challan_items.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function undeliveredInvoiceListSearch($from_date, $to_date, $customer_type, $memo_number, $name, $phone, $customer)
    {
        $search_by_from_date        = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date          = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
     
        $explode_invoice  = explode(' - ', $memo_number);

        if (isset($explode_invoice[1]))
        {
            $invoice_number  = ltrim($explode_invoice[1],"0");;
        }
        else
        {
            $invoice_number  = 0;
        }

        $data           = ChallanItems::leftjoin('invoices', 'invoices.id', 'challan_items.invoice_id')
                                        ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                        ->when($search_by_from_date != 0 && $search_by_to_date != 0, function ($query) use ($search_by_from_date, $search_by_to_date) {
                                            return $query->whereBetween('invoices.invoice_date', [$search_by_from_date, $search_by_to_date]);
                                        })
                                        ->when($customer_type == 1, function ($query) use ($customer_type) {
                                            return $query->where('customers.contact_type', 0);
                                        })
                                        ->when($customer_type == 2, function ($query) use ($customer_type) {
                                            return $query->where('customers.contact_type', 4);
                                        })
                                        ->when($customer_type == 3, function ($query) use ($customer_type) {
                                            return $query->where('invoices.customer_id', 1);
                                        })
                                        ->when($invoice_number != 0, function ($query) use ($invoice_number) {
                                            return $query->where('invoices.invoice_number', $invoice_number);
                                        })
                                        ->when($phone != 0, function ($query) use ($phone) {
                                            return $query->where('invoices.customer_phone', $phone);
                                        })
                                        ->when($name != 0, function ($query) use ($name) {
                                            return $query->where('invoices.customer_name', 'LIKE', "%$name%");
                                        })
                                        ->when($customer != 0, function ($query) use ($customer) {
                                            return $query->where('invoices.customer_id', $customer);
                                        })
                                        ->orderBy('challan_items.created_at', 'DESC')
                                        ->select('challan_items.*',
                                            'invoices.invoice_date as invoice_date',
                                            'invoices.invoice_number as invoice_number',
                                            'customers.name as contact_name',
                                            'customers.phone as contact_phone')
                                        ->distinct('challan_items.id')
                                        ->get();

        return Response::json($data);
    }
}
