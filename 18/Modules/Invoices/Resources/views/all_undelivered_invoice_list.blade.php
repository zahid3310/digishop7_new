@extends('layouts.app')

@section('title', 'All Sales')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">All Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">All Sales</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                            Payment Successfull !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                            Payment Not Added !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div style="min-height: 300px" class="card">
                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {!! Session::get('success') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('unsuccess'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! Session::get('unsuccess') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('errors'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! 'Some required fields are missing..!! Please try again..' !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            <div class="card-body table-responsive">

                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <div style="margin-bottom: 0px;margin-top: 0px" class="form-group row">
                                            <label for="productname" class="col-md-12 col-form-label">From Date</label>
                                            <div class="col-md-12">
                                                <input style="cursor: pointer" id="search_from_date" type="date" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <div style="margin-bottom: 0px;margin-top: 0px" class="form-group row">
                                            <label for="productname" class="col-md-12 col-form-label">To Date</label>
                                            <div class="col-md-12">
                                                <input style="cursor: pointer" id="search_to_date" type="date" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <div style="margin-bottom: 0px;margin-top: 0px" class="form-group row">
                                            <label for="productname" class="col-md-12 col-form-label">Customer Type</label>
                                            <div class="col-md-12">
                                                <select style="width: 100%" id="customer_type" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                    <option value="0">All</option>
                                                    <option value="1">Customer</option>
                                                    <option value="2">Retailer</option>
                                                    <option value="3">Walk-In Customer</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <div style="margin-bottom: 0px;margin-top: 0px" class="form-group row">
                                            <label for="productname" class="col-md-12 col-form-label">Memo Number</label>
                                            <div class="col-md-12">
                                                <input id="search_by_memo_number" type="text" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <div style="margin-bottom: 0px;margin-top: 0px" class="form-group row">
                                            <label for="productname" class="col-md-12 col-form-label">Walk-In Customer Name</label>
                                            <div class="col-md-12">
                                                <input id="search_by_customer_name" type="text" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <div style="margin-bottom: 0px;margin-top: 0px" class="form-group row">
                                            <label for="productname" class="col-md-12 col-form-label">Walk-In Customer Phone</label>
                                            <div class="col-md-12">
                                                <input id="search_by_customer_phone" type="text" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <div style="margin-bottom: 0px;margin-top: 0px" class="form-group row">
                                            <label style="text-align: left;padding-left: 0px" for="productname" class="col-md-12 col-form-label">Registered Customer/Retailer </label>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <select style="width: 75%" id="customer_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                        <option value="0">All</option>
                                                    </select>

                                                    <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="searchPayment()">
                                                        <i class="bx bx-search font-size-24"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th>Invoice#</th>
                                            <th>Customer</th>
                                            <th>Quantity</th>
                                            <th>Delivered</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="invoice_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0 || result['contact_type'] == 4 || result['id'] == 0)
                    {
                        if (result['address'] != null)
                        {
                            var address =  ' || ' + result['address'];
                        }
                        else
                        {
                            var address = '';
                        }

                        return result['text'] + address;
                    }
                },
            });

            $.get(site_url + '/invoices/undelivered/invoice/list/load', function(data){

                invoiceList(data);   
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var site_url                = $('.site_url').val();
            var search_from_date        = $('#search_from_date').val();
            var search_to_date          = $('#search_to_date').val();
            var search_customer_type    = $('#customer_type').val();
            var search_memo_number      = $('#search_by_memo_number').val();
            var search_by_name          = $('#search_by_customer_name').val();
            var search_by_phone         = $('#search_by_customer_phone').val();
            var search_customer         = $('#customer_id').val();

            //
                if (search_from_date != '')
                {
                    var from_date = $('#search_from_date').val();
                }
                else
                {
                    var from_date = 0;
                }

            //
                if (search_to_date != '')
                {
                    var to_date = $('#search_to_date').val();
                }
                else
                {
                    var to_date = 0;
                }


            //
                if (search_customer_type != '')
                {
                    var customerType = $('#customer_type').val();
                }
                else
                {
                    var customerType = 0;
                }

            //
                if (search_memo_number != '')
                {
                    var memoNumber = $('#search_by_memo_number').val();
                }
                else
                {
                    var memoNumber = 0;
                }

            //
                if (search_by_name != '')
                {
                    var name = $('#search_by_customer_name').val();
                }
                else
                {
                    var name = 0;
                }

            //
                if (search_by_phone != '')
                {
                    var phone = $('#search_by_customer_phone').val();
                }
                else
                {
                    var phone = 0;
                }

            //
                if (search_customer != '')
                {
                    var customer = $('#customer_id').val();
                }
                else
                {
                    var customer = 0;
                }

            $.get(site_url + '/invoices/undelivered/invoice/search/list/' + from_date + '/' + to_date + '/' + customerType + '/' + memoNumber + '/' + name + '/' + phone + '/' + customer, function(data){

                invoiceList(data);

            });
        }
    </script>

    <script type="text/javascript">
        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>

    <script type="text/javascript">
        function invoiceList(data)
        {
            var invoice_list = '';
            var sl_no        = 1;
            $.each(data, function(i, invoice_data)
            {
                var serial              = parseFloat(i) + 1;
                var site_url            = $('.site_url').val();
                var challan_list        = site_url + '/invoices/challan-list/' + invoice_data.invoice_id;

                if (invoice_data.customer_name != null)
                {
                    var customer  = invoice_data.customer_name;
                }
                else
                {
                    var customer  = invoice_data.contact_name;
                }

                if (invoice_data.contact_phone != null)
                {
                    var phone  = ' | ' + invoice_data.contact_phone;
                }
                else
                {
                    if (invoice_data.customer_phone != null)
                    {
                        var phone  = ' | ' + invoice_data.customer_phone;
                    }
                    else
                    {
                        var phone  = '';
                    }
                }

                if(invoice_data.invoice_quantity != invoice_data.delivered_quantity)
                {
                    invoice_list += '<tr>' +
                                    '<td>' +
                                        sl_no +
                                    '</td>' +
                                    '<td>' +
                                       formatDate(invoice_data.invoice_date) +
                                    '</td>' +
                                    '<td>' +
                                        'INV - ' + invoice_data.invoice_number.padStart(6, '0') +
                                    '</td>' +
                                    '<td>' +
                                        customer + phone +
                                    '</td>' +
                                    '<td>' +
                                       parseFloat(invoice_data.invoice_quantity).toFixed(2) +
                                    '</td>' +
                                    '<td>' +
                                       parseFloat(invoice_data.delivered_quantity).toFixed(2) +
                                    '</td>' +
                                    '<td>' +
                                        '<div class="dropdown">' +
                                            '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                            '</a>' +
                                            '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                '<a class="dropdown-item" href="' + challan_list +'" target="_blank">' + 'Challan List' + '</a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</td>' +
                                '</tr>';

                                sl_no++;
                }
                
            });

            $("#invoice_list").empty();
            $("#invoice_list").append(invoice_list);
        }
    </script>
@endsection