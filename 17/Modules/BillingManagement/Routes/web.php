<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('billingmanagement')->group(function() {
    Route::get('/', 'BillingManagementController@index')->name('billing_systems_index');
    Route::post('/store', 'BillingManagementController@store')->name('billing_systems_store');
    Route::get('/edit/{id}', 'BillingManagementController@edit')->name('billing_systems_edit');
    Route::post('/update/{id}', 'BillingManagementController@update')->name('billing_systems_update');
    Route::get('/billing/list/load', 'BillingManagementController@billingSystemListLoad')->name('billing_systems_list_load');
    Route::get('/billing/search/list/{id}', 'BillingManagementController@billingSystemListSearch')->name('billing_systems_list_search');
});
