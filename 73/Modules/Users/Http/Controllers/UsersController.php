<?php

namespace Modules\Users\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

//Models
use App\Models\Users;
use App\Models\Branches;
use DB;
use Auth;
use Hash;
use Schema;
use File;
use Artisan;

class UsersController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $branches  = Branches::get();

        return view('users::index', compact('branches'));
    }

    public function indexAll()
    {
        $users     = Users::where('id', '!=', 1)->get();
        $branches  = Branches::get();

        return view('users::users_list', compact('users', 'branches'));
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('users::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'name'          => 'required',
            'user_name'     => 'unique:users,email',
            'password'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $users                       = new Users();
            $users->name                 = $data['name'];
            $users->email                = $data['user_name'];
            $users->password             = Hash::make($data['password']);
            $users->role                 = $data['role'];
            $users->status               = $data['status'];
            $users->branch_id            = $data['branch_id'];
            $users->created_by           = $user_id;
            $users->save();

            if ($users->save())
            {   
                $image_upload                       = Users::find($users->id);

                if($request->hasFile('header_image'))
                {
                    $image1                     = $request->file('header_image');
                    $image_name1                = time().'.'.$image1->getClientOriginalExtension();
                    $file_path1                 = 'public/images/customer_header';
                    $rowImage1                  = $image1;
                    $savingPath1                = $file_path1;
                    $imageName1                 = $image_name1;
                    $resizeImage1               = $this->compressAndResize($rowImage1, $savingPath1, $imageName1, 10000, 1000,150);
                    $image_upload->header_image = $image_name1;
                }

                if($request->hasFile('footer_image'))
                {
                    $image2                     = $request->file('footer_image');
                    $image_name2                = time().'.'.$image2->getClientOriginalExtension();
                    $file_path2                 = 'public/images/customer_footer';
                    $rowImage2                  = $image2;
                    $savingPath2                = $file_path2;
                    $imageName2                 = $image_name2;
                    $resizeImage2               = $this->compressAndResize($rowImage2, $savingPath2, $imageName2, 10000, 1000,50);
                    $image_upload->footer_image = $image_name2;
                }

                $image_upload->save();

                DB::commit();
                return redirect()->route('users_index_all')->with("success","Users Created Successfully !!");
            }
            else
            {   
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('users::show');
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $users      = Users::where('id', '!=', 1)->get();
        $user_find  = Users::find($id);
        $branches   = Branches::get();

        return view('users::edit', compact('users', 'user_find', 'branches'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required',
            'user_name'     => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $users                   = Users::find($id);
            $users->name             = $data['name'];
            $users->email            = $data['user_name'];

            if ($data['password'] != null)
            {
                $users->password     = Hash::make($data['password']);
            }
            
            $users->role             = $data['role'];
            $users->branch_id        = $data['branch_id'];
            $users->status           = $data['status'];
            $users->updated_by       = $user_id;

            if ($users->save())
            {   
                return redirect()->route('users_index_all')->with("success","Users Updated Successfully !!");
            }

        }catch (\Exception $exception){
        dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function destroy($id)
    {
        return redirect()->route('users_index')->with("success","User Can Not Be Deleted !!");
    }

    public function truncate()
    {
        //Users Access Level Start
        if (Auth::user()->id != 1)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        ini_set('max_execution_time', 8000);
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        DB::table('customers')->truncate();
        DB::table('categories')->truncate();
        DB::table('sub_categories')->truncate();
        DB::table('items')->truncate();
        DB::table('products')->truncate();
        DB::table('product_entries')->truncate();
        DB::table('bills')->truncate();
        DB::table('bill_entries')->truncate();
        DB::table('invoices')->truncate();
        DB::table('invoice_entries')->truncate();
        DB::table('sales_return')->truncate();
        DB::table('sales_return_entries')->truncate();
        DB::table('purchase_return')->truncate();
        DB::table('purchase_return_entries')->truncate();
        DB::table('expense_categories')->truncate();
        DB::table('expenses')->truncate();
        DB::table('payments')->truncate();
        DB::table('payment_entries')->truncate();
        DB::table('transactions')->truncate();
        DB::table('paid_through_accounts')->truncate();
        DB::table('discounts')->truncate();
        DB::table('discount_products')->truncate();
        DB::table('units')->truncate();
        DB::table('modules_access')->truncate();
        DB::table('permissions')->truncate();

        //Insert dumy data start
            $user_id                        = Auth::user()->id;
            $tables                         = TableNameByUsers();

            $customers_data[] = [
                'name'           => 'Walk-In Customer',
                'contact_type'   => 0,
                'created_at'     => date('Y-m-d H:i:s'),
            ];

            DB::table('customers')->insert($customers_data);

            $suppliers_data[] = [
                'name'           => 'Walk-In Supplier',
                'contact_type'   => 1,
                'created_at'     => date('Y-m-d H:i:s'),
            ];

            DB::table('customers')->insert($suppliers_data);

            $units_data[] = [
                'name'           => 'Pcs',
                'created_at'     => date('Y-m-d H:i:s'),
            ];

            DB::table('units')->insert($units_data);

            $paid_through_accounts_data[] = [
                'name'           => 'Cash',
                'created_at'     => date('Y-m-d H:i:s'),
            ];

            DB::table('paid_through_accounts')->insert($paid_through_accounts_data);

            $expense_categories_data[] = [
                'name'           => 'Employee Salary',
                'created_at'     => date('Y-m-d H:i:s'),
            ];

            DB::table('expense_categories')->insert($expense_categories_data);
        //Insert dumy data end

        return redirect()->route('users_index')->with("success","User Tables Truncated Successfully !!");
    }

    public function cacheClear()
    {
        //Users Access Level Start
        if (Auth::user()->id != 1)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        Artisan::call('config:cache');
        Artisan::call('cache:clear');
        Artisan::call('view:clear');

        return back();
    }

    public function editProfile()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_id    = Auth::user()->id;
        $user       = Users::find($user_id);

        return view('users::edit_profile', compact('user'));
    }

    public function updateProfile(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'name'          => 'required',
            'user_name'     => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $users                   = Users::find($user_id);
            $users->name             = $data['name'];
            $users->email            = $data['user_name'];

            if ($data['password'] != null)
            {
                $users->password     = Hash::make($data['password']);
            }
            
            $users->updated_by       = $user_id;

            if($request->hasFile('image'))
            {   
                if (Auth::user()->photo != null)
                {
                    unlink('public/'.Auth::user()->photo);
                }

                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'company-profile-images/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $users->photo           = $logoUrl;
            }

            if ($users->save())
            {   
                return redirect()->route('users_edit_profile')->with("success","Profile Updated Successfully !!");
            }else
            {
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
        dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function editSettings()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_id    = Auth::user()->id;
        $user       = Users::find(1);

        return view('users::edit_settings', compact('user'));
    }

    public function updateSettings(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $users                       = Users::find(1);
            $users->organization_name    = $data['organization_name'];
            $users->address              = $data['address'];
            $users->contact_number       = $data['contact_number'];
            $users->contact_email        = $data['contact_email'];
            $users->website              = $data['website'];
            $users->sales_show           = $data['sales_show'];
            $users->vat_reg_number       = $data['vat_reg_number'];
            $users->mushak               = $data['mushak'];
            $users->vat_type             = $data['vat_type'];
            $users->vat_amount           = $data['vat_amount'];
            $users->pos_printer          = $data['pos_printer'];

            if($request->hasFile('header_image'))
            {   
                if($users['header_image'] != null)
                {
                    unlink('public/images/customer_header_'.$users->associative_contact_id.'_'.$users['header_image']);
                }

                $image1              = $request->file('header_image');
                $image_name1         = time().'.'.$image1->getClientOriginalExtension();
                $file_path1          = 'public/images/customer_header_'.$users->associative_contact_id.'_';
                $rowImage1           = $image1;
                $savingPath1         = $file_path1;
                $imageName1          = $image_name1;
                $resizeImage1        = $this->compressAndResize($rowImage1, $savingPath1, $imageName1, 10000, 1000,150);
                $users->header_image = $image_name1;
            }

            if($request->hasFile('footer_image'))
            {   
                if($users['footer_image'] != null)
                {
                    unlink('public/images/customer_footer_'.$users->associative_contact_id.'_'.$users['footer_image']);
                }

                $image2              = $request->file('footer_image');
                $image_name2         = time().'.'.$image2->getClientOriginalExtension();
                $file_path2          = 'public/images/customer_footer_'.$users->associative_contact_id.'_';
                $rowImage2           = $image2;
                $savingPath2         = $file_path2;
                $imageName2          = $image_name2;
                $resizeImage2        = $this->compressAndResize($rowImage2, $savingPath2, $imageName2, 10000, 1000,50);
                $users->footer_image = $image_name2;
            }

            if($request->hasFile('logo'))
            {   
                if (Auth::user()->logo != null)
                {
                    unlink('public/'.Auth::user()->logo);
                }

                $companyLogo            = $request->file('logo');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'company-profile-images/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $users->logo           = $logoUrl;
            }

            if ($users->save())
            {   
                return back()->with("success","Information Updated Successfully !!");
            }

        }catch (\Exception $exception){
        dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function compressAndResize($source_url, $destination_url, $imageName , $quality, $desired_width, $desired_height)
    {
        $info = getimagesize($source_url);
        if ($info['mime'] == 'image/jpeg')
        {
            list($width, $height)   = getimagesize($source_url);
            $src                    = imagecreatefromjpeg($source_url);
            $dst                    = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        elseif ($info['mime'] == 'image/png'){
            list($width, $height)   = getimagesize($source_url);
            $src                    = imagecreatefrompng($source_url);
            $dst                    = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        elseif ($info['mime'] == 'image/gif'){
            list($width, $height)   = getimagesize($source_url);
            $src                    = imagecreatefromgif($source_url);
            $dst                    = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        return $destination_url.$imageName;
    }
}
