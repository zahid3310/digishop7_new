<?php

namespace Modules\Invoices\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\UnitConversions;
use App\Models\Users;
use App\Models\BranchInventories;
use App\Models\Expenses;
use App\Models\Accounts;
use App\Models\JournalEntries;
use Response;
use DB;
use View;
use Carbon\Carbon;

class InvoicesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoices           = Invoices::orderBy('id', 'DESC')->first();
        $paid_accounts      = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();
        $accounts           = Accounts::where('account_type_id',9)->where('status', 1)->get();

        return view('invoices::index', compact('paid_accounts', 'invoices', 'accounts'));
    }

    public function AllSales()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
 
        $paid_accounts      = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();

        return view('invoices::all_sales', compact('paid_accounts'));
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('invoices::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'          => 'required',
            'customer_id'           => 'required',
            'product_entries.*'     => 'required',
            'amount.*'              => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];
             
            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {   
                    if($data['main_unit_id'][$key] == $data['unit_id'][$key])
                    {
                        $product    = ProductEntries::find($value);
                        $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                    }
                    else
                    {
                        $product1   = UnitConversions::where('product_entry_id', $value)
                                                        ->where('main_unit_id', $data['main_unit_id'][$key])
                                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                                        ->first();
                       
                        $buy_price  = $buy_price + ($product1['purchase_price']*$data['quantity'][$key]);
                    }
                }

            $data_find                          = Invoices::orderBy('created_at', 'DESC')->first();
            $invoice_number                     = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

            $invoice                            = new Invoices;
            $invoice->invoice_number            = $invoice_number;
            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->customer_name             = $data['customer_name'];
            $invoice->customer_phone            = $data['customer_phone'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->due_date                  = date('Y-m-d', strtotime($data['due_date']));
            $invoice->shipping_cost             = $data['shipping_cost'];
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $data['total_amount'];
            $invoice->total_buy_price           = $buy_price;
            $invoice->total_discount            = $discount;
            $invoice->invoice_note              = $data['invoice_note'];
            $invoice->total_vat                 = $vat;
            $invoice->branch_id                 = $branch_id;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->previous_due              = $data['previous_due'];
            $invoice->previous_due_type         = $data['balance_type'];
            $invoice->adjusted_amount           = $data['adjustment'];
            $invoice->major_category_id         = $data['major_category_id'] != 0 ? $data['major_category_id'] : null;
            $invoice->account_id                = $data['account_id'];
            $invoice->created_by                = $user_id;

            if ($invoice->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'        => $invoice['id'],
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value,
                        'main_unit_id'      => $data['main_unit_id'][$key],
                        'conversion_unit_id'=> $data['unit_id'][$key],
                        'customer_id'       => $invoice['customer_id'],
                        'reference_id'      => $data['reference_id'],
                        'buy_price'         => $product_buy_price['buy_price'],
                        'rate'              => $data['rate'][$key],
                        'quantity'          => $data['quantity'][$key],
                        'total_amount'      => $data['amount'][$key],
                        'discount_type'     => $data['discount_type'][$key],
                        'discount_amount'   => $data['discount'][$key],
                        'branch_id'         => $branch_id,
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('invoice_entries')->insert($invoice_entries);

                stockOut($data, $item_id=null);

                //Financial Accounting Start
                    debit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=8, $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    credit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=$data['account_id'], $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

                    //Insert into journal_entries 
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {
                                debit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                credit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=8, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                            }
                        }
                    }

                    customerBalanceUpdate($data['customer_id']);
                //Financial Accounting End

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Sales Created Successfully !!");
                }
                else
                {            
                    $user_info  = Users::find(1);

                    if ($user_info['pos_printer'] == 0)
                    {
                        return redirect()->route('invoices_show_pos', $invoice->id);
                    }
                    else
                    {
                        return redirect()->route('invoices_show', $invoice->id);
                    }
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.address as address',
                                             'customers.phone as phone')
                                    ->find($id);
        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->where('invoice_entries.invoice_id', $id)
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.name as product_entry_name',
                                             'products.name as product_name')
                                    ->get();  
        $user_info  = Users::find(1);

        return View::make('invoices::show')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info);
    }

    public function showPos($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.address as address',
                                             'customers.phone as phone')
                                    ->find($id);
        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->where('invoice_entries.invoice_id', $id)
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.name as product_entry_name',
                                             'products.name as product_name')
                                    ->get();         
        $user_info  = Users::find(1);

        return View::make('invoices::show_pos')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info);
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $find_invoice           = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                            ->select('invoices.*',
                                                 'customers.id as customer_id',
                                                 'customers.name as contact_name')
                                            ->find($id);

        //Check if the invoice is created from this branch
        if ($find_invoice['branch_id'] != Auth::user()->branch_id)
        {
            return back()->with('unsuccess', 'This sales has not created from your branch !!');
        }                                
        //Check if the invoice is created from this branch

        $find_customer          = Customers::find($find_invoice['customer_id']);
        $find_invoice_entries   = InvoiceEntries::leftjoin('customers', 'customers.id', 'invoice_entries.customer_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                            ->where('invoice_entries.invoice_id', $id)
                                            ->select('invoice_entries.*',
                                                    'customers.id as customer_id',
                                                    'customers.name as customer_name',
                                                    'product_entries.id as item_id',
                                                    'product_entries.product_code as product_code',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.name as item_name')
                                            ->get();

        $entries_count          = $find_invoice_entries->count();

        $current_balance        = JournalEntries::whereIn('transaction_head', ['payment-receive'])
                                                ->where('invoice_id', $id)
                                                ->where('debit_credit', 1)
                                                ->selectRaw('journal_entries.*')
                                                ->get();

        $current_balance_count  = $current_balance->count();

        $paid_accounts          = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();
        $accounts               = Accounts::where('account_type_id',9)->where('status', 1)->get();

        if ((Auth::user()->branch_id != $find_invoice->branch_id) && (Auth::user()->branch_id != 1))
        {
            return back()->with('unsuccess', 'You are not allowed to edit this invoice');
        }

        //Previous Opening Balance Calculation End

        return view('invoices::edit', compact('find_invoice', 'find_invoice_entries', 'entries_count', 'paid_accounts', 'find_customer', 'current_balance', 'current_balance_count', 'accounts'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'      => 'required',
            'customer_id'       => 'required',
            'product_entries.*' => 'required',
            'amount.*'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat           = $data['vat_amount'];
            $old_invoice   = Invoices::find($id);
            $invoice       = Invoices::find($id);

            $branch_id  = $old_invoice['branch_id'];

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {   
                    if($data['main_unit_id'][$key] == $data['unit_id'][$key])
                    {
                        $product    = ProductEntries::find($value);
                        $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                    }
                    else
                    {
                        $product1   = UnitConversions::where('product_entry_id', $value)
                                                        ->where('main_unit_id', $data['main_unit_id'][$key])
                                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                                        ->first();
                       
                        $buy_price  = $buy_price + ($product1['purchase_price']*$data['quantity'][$key]);
                    }
                }

            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->customer_name             = $data['customer_name'];
            $invoice->customer_phone            = $data['customer_phone'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->due_date                  = date('Y-m-d', strtotime($data['due_date']));
            $invoice->shipping_cost             = $data['shipping_cost'];
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $data['total_amount'];
            $invoice->total_buy_price           = $buy_price;
            $invoice->total_discount            = $discount;
            $invoice->invoice_note              = $data['invoice_note'];
            $invoice->total_vat                 = $vat;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->adjusted_amount           = $data['adjustment'];
            $invoice->major_category_id         = $data['major_category_id'] != 0 ? $data['major_category_id'] : null;
            $invoice->account_id                = $data['account_id'];
            $invoice->updated_by                = $user_id;

            if ($invoice->save())
            {
                $item_id                = InvoiceEntries::where('invoice_id', $invoice['id'])->get();
                $item_delete            = InvoiceEntries::where('invoice_id', $invoice['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'        => $invoice['id'],
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value,
                        'brand_id'          => $product_buy_price['brand_id'],
                        'main_unit_id'      => $data['main_unit_id'][$key],
                        'conversion_unit_id'=> $data['unit_id'][$key],
                        'customer_id'       => $invoice['customer_id'],
                        'reference_id'      => $data['reference_id'],
                        'buy_price'         => $product_buy_price['buy_price'],
                        'rate'              => $data['rate'][$key],
                        'quantity'          => $data['quantity'][$key],
                        'total_amount'      => $data['amount'][$key],
                        'discount_type'     => $data['discount_type'][$key],
                        'discount_amount'   => $data['discount'][$key],
                        'branch_id'         => $old_invoice['branch_id'],
                        'updated_by'        => $user_id,
                    ];
                }

                DB::table('invoice_entries')->insert($invoice_entries);

                stockOut($data, $item_id);

                $jour_ent_debit     = JournalEntries::where('invoice_id', $invoice->id)
                                        ->where('transaction_head', 'sales')
                                        ->where('debit_credit', 1)
                                        ->first();

                $jour_ent_credit    = JournalEntries::where('invoice_id', $invoice->id)
                                        ->where('transaction_head', 'sales')
                                        ->where('debit_credit', 0)
                                        ->first();
                //Financial Accounting Start
                    debitUpdate($jour_ent_debit['id'], $customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=8, $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    creditUpdate($jour_ent_credit['id'], $customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=$data['account_id'], $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //Financial Accounting End

                //Insert into journal_entries Start
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {   
                                $pay_debit  = JournalEntries::find($data['current_balance_id'][$i]);
                                $pay_credit = JournalEntries::find($data['current_balance_id'][$i] + 1);

                                if ($pay_debit != null)
                                { 
                                    debitUpdate($pay_debit['id'], $customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                                else
                                {
                                    debit($customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }

                                if ($pay_credit != null)
                                { 
                                    creditUpdate($pay_credit['id'], $customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=8, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                                else
                                {
                                    credit($customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=8, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                            }
                            else
                            {
                                $pay_debit  = JournalEntries::where('id', $data['current_balance_id'][$i])->delete();
                                $pay_credit = JournalEntries::where('id', $data['current_balance_id'][$i] + 1)->delete();
                            }
                        }
                    }

                    customerBalanceUpdate($data['customer_id']);
                //Insert into journal_entries End

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return redirect()->route('invoices_all_sales')->with("success","Sales Updated Successfully !!");
                }
                else
                {      
                    $user_info  = Users::find(1);

                    if ($user_info['pos_printer'] == 0)
                    {
                        return redirect()->route('invoices_show_pos', $invoice->id);
                    }
                    else
                    {
                        return redirect()->route('invoices_show', $invoice->id);
                    }
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function productList()
    {
        $data       = ProductEntries::where('product_entries.stock_in_hand', '>', 0)
                                    ->Where('product_entries.stock_in_hand', '!=', null)
                                    ->orderBy('product_entries.total_sold', 'DESC')
                                    ->select('product_entries.*')
                                    ->get();

        return Response::json($data);
    }

    public function makePayment($id)
    {
        $data       = Invoices::find($id);

        return Response::json($data);
    }

    public function storePayment(Request $request)
    {
        $user_id                    = Auth::user()->id;
        $data                       = $request->all();

        DB::beginTransaction();

        try{

            if (isset($data['amount_paid']))
            {
                $data_find        = Payments::orderBy('id', 'DESC')->first();
                $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                for($i = 0; $i < count($data['amount_paid']); $i++)
                {   
                    if ($data['amount_paid'][$i] > 0)
                    {
                        $payments = [
                            'payment_number'        => $payment_number,
                            'customer_id'           => $data['customer_id'],
                            'payment_date'          => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $data['amount_paid'][$i],
                            'paid_through'          => $data['paid_through'][$i],
                            'account_information'   => $data['account_information'][$i],
                            'note'                  => $data['note'][$i],
                            'type'                  => 0,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $payment_id = DB::table('payments')->insertGetId($payments);      

                        if ($payment_id)
                        {   
                            $payment_entries = [
                                    'payment_id'        => $payment_id,
                                    'invoice_id'        => $data['invoice_id'],
                                    'amount'            => $data['amount_paid'][$i],
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            DB::table('payment_entries')->insert($payment_entries);  
                        }

                        $update_invoice_dues                = Invoices::find($data['invoice_id']);
                        $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                        $update_invoice_dues->save();

                        $payment_number++;
                    }
                }
            }
            
            DB::commit();
            return Response::json(1);
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            dd($exception);
            return Response::json(0);
        }
    }

    public function productPriceList($id)
    {
        $branch_id                      = Auth()->user()->branch_id;
        $data['product_entries']        = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                    ->selectRaw('product_entries.*, units.name as unit_name')
                                                    ->find($id);

        $data['branch_product']         = BranchInventories::where('product_entry_id', $id)->where('branch_id', $branch_id)->first();
        $data['branch_id']              = Auth()->user()->branch_id;

        $data1                          = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                        ->where('product_entries.id', $id)
                                                        ->selectRaw('units.id as unit_id, units.name as unit_name')
                                                        ->get()
                                                        ->toArray();

        $data2                          = UnitConversions::leftjoin('units', 'units.id', 'unit_conversions.converted_unit_id')
                                                    ->where('unit_conversions.product_entry_id', $id)
                                                    ->selectRaw('unit_conversions.converted_unit_id as unit_id, units.name as unit_name')
                                                    ->get()
                                                    ->toArray();

        $data['unit_conversions']       = collect(array_merge($data1, $data2));

        return Response::json($data);
    }

    public function invoiceListLoad()
    {
        $branch_id      = Auth()->user()->branch_id;
        $data           = Invoices::leftjoin('sales_return', 'sales_return.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->where('invoices.type', 1)
                                    ->where('invoices.branch_id', $branch_id)
                                    ->orderBy('invoices.invoice_date', 'DESC')
                                    ->orderBy('invoices.id', 'ASC')
                                    ->select('invoices.*',
                                            'sales_return.id as return_id',
                                            'customers.name as customer_name',
                                            'customers.phone as phone')
                                    ->distinct('invoices.id')
                                    ->take(200)
                                    ->get();

        return Response::json($data);
    }

    public function invoiceListSearch($from_date, $to_date, $customer, $invoice)
    {
        $branch_id              = Auth()->user()->branch_id;
        $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
        $search_by_customer     = $customer != 0 ? $customer : 0;
        $search_by_invoice      = $invoice != 0 ? $invoice : 0;

        $data           = Invoices::leftjoin('sales_return', 'sales_return.invoice_id', 'invoices.id')
                                        ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                        ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                        ->when($search_by_from_date != 0 && $search_by_to_date != 0, function ($query) use ($search_by_from_date, $search_by_to_date) {
                                            return $query->whereBetween('invoices.invoice_date', [$search_by_from_date, $search_by_to_date]);
                                        })
                                        ->when($search_by_customer != 0, function ($query) use ($search_by_customer) {
                                            return $query->where('customers.id', $search_by_customer);
                                        })
                                        ->when($search_by_invoice != 0, function ($query) use ($search_by_invoice) {
                                            return $query->where('invoices.id', $search_by_invoice);
                                        })
                                        ->where('invoices.type', 1)
                                        ->where('invoices.branch_id', $branch_id)
                                        ->orderBy('invoices.invoice_date', 'DESC')
                                        ->select('invoices.*',
                                                 'sales_return.id as return_id',
                                                 'customers.name as customer_name',
                                                 'customers.phone as phone')
                                        ->distinct('invoices.id')
                                        ->take(100)
                                        ->get();

        return Response::json($data);
    }

    public function posSearchProduct($id)
    {
        $data           = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_code', $id)
                                    ->where('product_entries.stock_in_hand', '>', 0)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->first();

        return Response::json($data);
    }

    public function customerStore(Request $request)
    {
        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        $customers                              = new Customers;
        $customers->name                        = $data['customer_name'];
        $customers->address                     = $data['address'];
        $customers->phone                       = $data['mobile_number'];
        $customers->contact_type                = $data['contact_type'];
        $customers->branch_id                   = $branch_id;
        $customers->created_by                  = $user_id;

        if ($customers->save())
        {   
            return Response::json($customers);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function customersListInvoice()
    {
        $branch_id      = Auth()->user()->branch_id;

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Customers::orderBy('customers.created_at', 'DESC')
                                    ->where('customers.branch_id', $branch_id)
                                    ->select('customers.*')
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Customers::where('customers.name', 'LIKE', "$search%")
                                    // ->orWhere('customers.phone', 'LIKE', "$search%")
                                    ->where('customers.branch_id', $branch_id)
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->select('customers.*')
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "phone" =>'', "contact_type" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name'], "phone" =>$value['phone'], "contact_type" =>$value['contact_type']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function couponCode($id)
    {
        $today          = Carbon::today();

        $data           = Discounts::leftjoin('discount_products', 'discount_products.discount_id', 'discounts.id')
                                ->where('discounts.coupon_code', $id)
                                ->orWhere('discounts.card_number', $id)
                                ->whereDate('discounts.expire_date', '>=', $today->format('Y-m-d'))
                                ->where('discounts.status', 1)
                                ->select('discount_products.product_id as product_id', 
                                         'discounts.discount_type as discount_type',
                                         'discounts.discount_amount as discount_amount',
                                         'discounts.expire_date as expire_date'
                                        )
                                ->get();

        return Response::json($data);
    }

    public function printInvoicesList()
    {
        $user_id        = Auth::user()->id;
        $data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->where('invoices.created_by', $user_id)
                                    ->orderBy('invoices.invoice_date', 'DESC')
                                    ->select('invoices.*',
                                            'customers.name as customer_name',
                                            'customers.phone as phone',
                                            'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function printInvoicesSearch($date,$customer,$invoice_number)
    {
        $search_by_date              = $date != 0 ? date('Y-m-d', strtotime($date)) : 0;
        $search_by_invoice_number    = $invoice_number != 0 ? $invoice_number : 0;
        $search_by_invoice_customer  = $customer != 0 ? $customer : 0;
        $user_info                   = userDetails();

        $data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->when($search_by_date != 0, function ($query) use ($search_by_date) {
                                        return $query->where('invoices.invoice_date', 'LIKE', "%$search_by_date%");
                                    })
                                    ->when($search_by_invoice_number != 0, function ($query) use ($search_by_invoice_number) {
                                        return $query->where('invoices.invoice_number', 'LIKE', "%$search_by_invoice_number%");
                                    })
                                    ->when($search_by_invoice_customer != 0, function ($query) use ($search_by_invoice_customer) {
                                        return $query->where('customers.name', 'LIKE', "%$search_by_invoice_customer%");
                                    })
                                    ->orderBy('invoices.invoice_date', 'DESC')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.phone as phone',
                                             'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function calculateOpeningBalance($customer_id)
    {
        $data  = calculateOpeningBalance($customer_id);

        return Response::json($data);
    }

    public function productListLoadInvoice()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('product_entries.name', 'LIKE', "%$search%")
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->take(100)
                                    ->get();
        }

        foreach ($fetchData as $key => $value)
        {   
            if ($value['product_type'] == 2)
            {
                $variations  = ' - ' . $value['variations'];
            }
            else
            {
                $variations  = '';
            }

            if ($value['brand_name'] != null)
            {
                $brand  = ' - '.$value['brand_name'];
            }
            else
            {
                $brand  = '';
            }

            if ($value['product_code'] != null)
            {
                $code  = $value['product_code'];
            }
            else
            {
                $code  = '';
            }

            $name   = $value['name'] . $variations . ' ' . '( ' . $code . $brand . ' )';

            $data[] = array("id"=>$value['id'], "text"=>$name);
        }

        return Response::json($data);
    }

    public function expenseCategories()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = ExpenseCategories::whereNotIn('expense_categories.id', [1,2])->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = ExpenseCategories::where('expense_categories.name', 'LIKE', "%$search%")
                                            ->whereNotIn('expense_categories.id', [1,2])
                                            ->get();
        }

        foreach ($fetchData as $key => $value)
        {   
            $data[] = array("id"=>$value['id'], "text"=>$value['name']);
        }

        return Response::json($data);
    }

    public function adjustAdvancePayment($customer_id)
    {
        $data  = Customers::find($customer_id);

        if ($data['balance'] < 0)
        {
            $result = abs($data['balance']);
        }
        else
        {
            $result = 0;
        }

        return Response::json($result);
    }
}
