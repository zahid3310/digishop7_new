

<?php $__env->startSection('title', 'Sales'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Orders</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">New Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('invoices_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                <?php echo e(csrf_field()); ?>


                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Customer *</label>
                                            <div class="col-md-8">
                                                <select style="width: 75%" id="customer_id" name="customer_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9" required>
                                                   <option value="1">Walk-In Customer</option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Name </label>
                                            <div class="col-md-8">
                                                <input id="customer_name_s" name="customer_name" type="text" value="" class="form-control" placeholder="Customer Name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Refe.</label>
                                            <div class="col-md-8">
                                                <select style="width: 75%" id="reference_id" name="reference_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                   <option value="">Select Reference</option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal1">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Address </label>
                                            <div class="col-md-8">
                                                <input id="customer_address" name="customer_address" type="text" value="" class="form-control" placeholder="Address">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Bal. </label>
                                            <div class="col-md-8">
                                                <input id="balance" name="balance" type="text" value="" class="form-control" value="0">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Pho. </label>
                                            <div class="col-md-8">
                                                <input id="customer_phone" name="customer_phone" type="text" value="" class="form-control" placeholder="Phone">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Date *</label>
                                            <div class="col-md-8">
                                                <input id="selling_date" name="selling_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal3">
                                                    Add Product
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="display: none">
                                    <select style="padding: 6px;border-radius: 4px;cursor: pointer" class="taxType" id="tax_type_0" name="tax_type" oninput="calculateActualAmount(0)">
                                        <option style="padding: 10px" value="1" selected>BDT</option>
                                        <option style="padding: 10px" value="0">%</option>
                                    </select>
                                    <input id="tax_amount_0" type="text" class="form-control width-xs taxAmount" name="tax_amount" value="0" oninput="calculateActualAmount(0)">
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 280px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        <?php if(isset($find_invoice_entries)): ?>
                                        <?php $__currentLoopData = $find_invoice_entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div style="margin-bottom: 0px !important" class="row di_<?php echo e($key); ?>">
                                                <div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname">Product *</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Product *</label>
                                                    <select style="width: 100%" name="product_entries[]" class="inner form-control select2 productEntries" id="product_entries_<?php echo e($key); ?>" onchange="getItemPrice(<?php echo e($key); ?>)" required>
                                                        <option value="<?php echo e($value['item_id']); ?>">
                                                        <?php
                                                            $vari       = ProductVariationName($value['item_id']);
                                                            $variation  = $vari != null ? ' - ' . $vari : '';

                                                            if ($value['product_type'] == 1)
                                                            {
                                                                if ($value['pcs_per_cartoon'] != 0)
                                                                {
                                                                    $pcs_per_cartoon = $value['pcs_per_cartoon'];
                                                                }
                                                                else
                                                                {
                                                                    $pcs_per_cartoon = 1;
                                                                }

                                                                $pcs        = $value['stock_in_hand']/(($value['height']*$value['width'])/144);
                                                                $cartoon    = $pcs/$pcs_per_cartoon;
                                                            }
                                                        ?>

                                                        <?php echo e($value['item_name'] . $variation . '( ' . str_pad($value['product_code'], 6, "0", STR_PAD_LEFT) . ' )'); ?>

                                                        </option>
                                                    </select>
                                                    <?php if($value['product_type'] == 1): ?>
                                                    <span id="stock_show_<?php echo e($key); ?>" style="color: black"><?php echo e('Stock : '.round($value['stock_in_hand'], 2) . ' SFT ' . ' | ' . round($cartoon, 2) . ' Cart ' . ' | ' . round($pcs, 2) . ' PCS '); ?></span>
                                                    <?php else: ?>
                                                    <span id="stock_show_<?php echo e($key); ?>" style="color: black"><?php echo e('Stock : '.round($value['stock_in_hand'], 2) . '( ' . $value['unit_name'] . ' )'); ?></span>
                                                    <?php endif; ?>
                                                </div>

                                                <input type="hidden" name="stock[]" class="inner form-control" id="stock_<?php echo e($key); ?>" placeholder="Stock" value="<?php echo e($value['stock_in_hand']); ?>" />

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-2 col-sm-12 col-12 form-group">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname">Cart</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Cart</label>
                                                    <input type="text" name="cartoon[]" class="inner form-control" id="cartoon_<?php echo e($key); ?>" value="<?php echo e($value['cartoon']); ?>" oninput="getItemPrice(<?php echo e($key); ?>)" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-2 col-sm-12 col-12 form-group">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname">PCS</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">PCS</label>
                                                    <input type="text" name="pcs[]" class="inner form-control" id="pcs_<?php echo e($key); ?>" value="<?php echo e($value['pcs']); ?>" onchange="getItemPrice(<?php echo e($key); ?>)" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname">Qty *</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Qty *</label>
                                                    <input type="text" name="quantity[]" class="inner form-control quantityCheck" id="quantity_<?php echo e($key); ?>" value="<?php echo e(round($value['quantity'], 2)); ?>" placeholder="Quantity" oninput="getItemPriceBackCalculation(<?php echo e($key); ?>)" required/>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname">Rate *</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Rate *</label>
                                                    <input type="text" name="rate[]" class="inner form-control" id="rate_<?php echo e($key); ?>" value="<?php echo e(round($value['rate'], 2)); ?>" placeholder="Rate" oninput="calculateActualAmount(<?php echo e($key); ?>)" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                    
                                                    <div class="row">
                                                        <div  style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">
                                                            <?php if($key == 0): ?>
                                                                <label class="hidden-xs" style="padding-top: 15px" for="productname"></label>
                                                            <?php endif; ?>
                                                            <label class="show-xs" style="display: none;padding-top: 17px" for="productname"></label>
                                                            <select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_<?php echo e($key); ?>" oninput="calculateActualAmount(<?php echo e($key); ?>)">
                                                                <option value="1" <?php echo e($value['discount_type'] == 1 ? 'selected' : ''); ?>>BDT</option>
                                                                <option value="0" <?php echo e($value['discount_type'] == 0 ? 'selected' : ''); ?>>%</option>
                                                            </select>
                                                        </div>

                                                        <div  style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">
                                                            <?php if($key == 0): ?>
                                                                <label class="hidden-xs" style="padding-bottom: 0px" for="productname">Discount</label>
                                                            <?php endif; ?>
                                                            <label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>
                                                            <input type="text" name="discount[]" class="inner form-control" id="discount_<?php echo e($key); ?>" value="<?php echo e($value['discount_amount']); ?>" placeholder="Discount" oninput="calculateActualAmount(<?php echo e($key); ?>)"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname">Total</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Total</label>
                                                    <input type="text" name="amount[]" class="inner form-control amount" id="amount_<?php echo e($key); ?>" value="<?php echo e(round($value['total_amount'], 2)); ?>" placeholder="Total"/>
                                                </div>
                                                
                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname">Action</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Action *</label>
                                                    <?php if($key == 0): ?>
                                                        <i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button"></i>
                                                    <?php else: ?>
                                                        <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field" data-val="<?php echo e($key); ?>"></i>
                                                    <?php endif; ?> 
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>
                                            <div class="row di_0">
                                            </div>
                                        <?php endif; ?>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 225px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Sub Total</label>
                                            <div class="col-md-7">
                                                <input type="text" id="subTotalBdt" class="form-control">
                                                <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">VAT</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control" id="vat_type_0" name="vat_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="0" <?php echo e(Auth::user()->vat_type == 0 ? 'selected' : ''); ?>>%</option>
                                                    <option style="padding: 10px" value="1" <?php echo e(Auth::user()->vat_type == 1 ? 'selected' : ''); ?>>BDT</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="vat_amount_0" type="text" class="form-control width-xs" name="vat_amount" value="<?php echo e(Auth::user()->vat_amount); ?>" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Discount</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control totalDiscountType" id="total_discount_type_0" name="total_discount_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="1" selected>BDT</option>
                                                    <option style="padding: 10px" value="0">%</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="total_discount_0" type="text" class="form-control totalDiscount" name="total_discount_amount" value="0" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Dis. Note</label>
                                            <div class="col-md-7">
                                                <input id="total_discount_note" type="text" class="form-control width-xs" name="total_discount_note" value="<?php echo e(old('total_discount_note')); ?>" placeholder="Discount Note">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Coupon</label>
                                            <div class="col-md-7">
                                                <input id="coupon_code" type="number" class="form-control width-xs couponCode" name="coupon_code" onchange="couponMembership()" placeholder="Coupon/Membership">
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 225px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Total Payable</label>
                                            <div class="col-md-7">
                                                <input type="text" id="totalBdt" class="form-control">
                                                <input style="display: none" type="text" id="totalBdtShow" name="total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Cash Given</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="cash_given" name="cash_given" placeholder="Cash Given" oninput="calculateChangeAmount()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Change</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="change_amount" name="change_amount" placeholder="Change Amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Send SMS</label>
                                            <div class="col-md-12">
                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Non Masking
                                                    </label>
                                                </div>

                                                <div style="padding-left: 10px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Masking
                                                    </label>
                                                </div>

                                                <div style="padding-left: 10px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Voice
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 225px;padding-top: 13px;padding-right: 0px" class="col-md-6">
                                        <div style="background-color: #D2D2D2;height: 210px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap_payment getMultipleRowPayment">
                                            <div class="row row di_payment_0">
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>

                                    <div style="display: none;" class="row justify-content-end">
                                        <div class="col-lg-1 col-md-2 form-group">
                                            <input id="pos_add_button" type="button" class="btn btn-success btn-block inner add_field_button_pos" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px !important;padding-bottom: 0px !important" class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="generateChallan" name="generate_challan" checked>
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="generateChallan">
                                                        Generate Challan
                                                    </label>
                                                </div>
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Save & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('invoices_index')); ?>">Close</a></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control commonCustomerClass">
                        </div>
                    </div>

                    <input id="contact_type" name="contact_type" type="hidden" value="0" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Reference</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name1" name="customer_name" type="text" class="form-control commonReferenceClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number1" name="mobile_number" type="text" class="form-control commonReferenceClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address1" name="address" type="text" class="form-control commonReferenceClass">
                        </div>
                    </div>

                    <input id="contact_type1" name="contact_type" type="hidden" value="3" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal2" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Print Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label for="productname" class="col-md-4 col-form-label">Date</label>
                                <div class="col-md-8">
                                    <input style="cursor: pointer" id="search_date" type="date" value="<?= date("Y-m-d") ?>" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Customer </label>
                                <div class="col-md-8">
                                    <input id="customer" type="text" class="form-control"  placeholder="Enter Customer Name">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Search Invoice </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <input style="width: 95%" type="text" class="form-control col-lg-9 col-md-9 col-sm-9 col-9" id="invoiceNumber" placeholder="Enter Invoice ID">
                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="printInvoicesSearch()">
                                            <i class="bx bx-search font-size-24"></i>
                                        </span>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Order#</th>
                                <th>Customer</th>
                                <th>Amount</th>
                                <th>Paid</th>
                                <th>Due</th>
                                <th>Creator</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody id="print_invoice_list">
                        </tbody>
                    </table>
                </div>
                
                <div class="modal-footer">
                    <button id="CloseButton2" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal3" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">

                    <input type="hidden" id="productEntryIdAddProduct" />
                    <input type="hidden" id="productEntryNameAddProduct" />
                    <input type="hidden" id="productEntryRateAddProduct" />

                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                            <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Major Category *</label>
                            <select style="cursor: pointer" class="form-control" id="type" name="type">
                                <option value="1">Tiles</option>
                                <option value="0" selected>Single Products</option>
                            </select>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                            <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Category *</label>
                            <select style="width: 100%" id="product_category_id" name="product_category_id" class="form-control select2 col-lg-12 col-md-12 col-sm-12 col-12" required>
                               <option value="">--Select Product Category--</option>
                           </select>
                        </div>

                        <div style="margin-top: 8px" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                            <label for="productname">Brand</label>
                            <select style="width: 100%" id="brand_id" name="brand_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10">
                               <option value="">--Select Brand--</option>
                            </select>
                        </div>

                        <div style="margin-top: 8px" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                            <label for="productname">Product Code</label>
                            <input type="text" name="code" class="inner form-control" id="product_code" value="" />
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                            <label for="productname">Product Name *</label>
                            <input type="text" name="product_name" class="inner form-control" id="product_name" placeholder="Product Name" required />
                        </div>

                        <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group tilesTypeUnit">
                            <label for="productname">Unit *</label>
                            <select id="unit_id" style="width: 100%" class="form-control select2 unit" name="unit_id">
                                <option value="">--Select Unit--</option>
                                <?php if(!empty($units)): ?>
                                    <?php $__currentLoopData = $units; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $unit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($unit->id); ?>"><?php echo e($unit->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </select>
                        </div>

                        <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group tilesType">
                            <label for="productname">Size *</label>
                            <select style="width: 100%" id="size_id" name="size_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10">
                               <option value="">--Select Size--</option>
                            </select>
                        </div>

                        <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group tilesType">
                            <label for="productname">Pcs/Cartoon *</label>
                            <input type="text" name="per_cartoon_pcs" class="inner form-control" id="per_cartoon_pcs" placeholder="PCS Per Cartoon" />
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                            <label for="productname">Purchase Price *</label>
                            <input type="text" name="buying_price" class="inner form-control" id="buying_price" placeholder="Purchase Price" value="0" required />
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                            <label for="productname">Sell Price *</label>
                            <input type="text" name="selling_price" class="inner form-control" id="selling_price" placeholder="Sell Price" value="0" required />
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                            <label for="productname">Product Type</label>
                            <select style="cursor: pointer" id="product_type" class="form-control" name="product_type">
                                <option value="1">Single</option>
                                <option value="2">Variable</option>
                            </select>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                            <label style="padding-left: 0px" for="productname" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Alert Quantity</label>
                            <input type="text" name="alert_quantity" class="inner form-control" id="alert_quantity" placeholder="Alert Quantity" />
                        </div>

                        <div style="display: none" class="col-md-12 variationClass">
                            <hr style="margin-top: 0px !important">

                            <div data-repeater-list="inner-group" class="inner form-group mb-0 row">
                                <div class="inner col-lg-12 ml-md-auto input_fields_wrap_variation getMultipleRowVariation">

                                    <h5>Add Variations</h5>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 col-sm-6"></div>
                                <div class="col-lg-1 col-md-2 col-sm-6 col-12 form-group">
                                    <div id="add_field_button_variation" class="add_field_button_variation">
                                        <i class="btn btn-success btn-block bx bx-plus font-size-20"></i>
                                    </div>
                                </div>
                            </div>

                            <hr style="margin-top: 0px !important">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="submitBtn3" class="btn btn-primary waves-effect waves-light">Save</button>
                        <button id="CloseButton3" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if ((result['contact_type'] == 0 || result['contact_type'] == 4) && result['id'] != 0)
                    {   
                        if (result['address'] != null)
                        {
                            var address =  ' || ' + result['address'];
                        }
                        else
                        {
                            var address = '';
                        }

                        return result['text'] + address;
                    }
                },
            });

            $("#reference_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 3)
                    {
                        return result['text'];
                    }
                },
            });

            $("#product_category_id").select2({
                ajax: { 
                    url:  site_url + '/products/product-category-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#brand_id").select2({
                ajax: { 
                    url:  site_url + '/products/brand-brand-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#size_id").select2({
                ajax: { 
                    url:  site_url + '/products/size-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $('#add_field_button').click();
            $('#add_field_button_payment').click();

            $(".productEntries").select2({
                ajax: { 
                url:  site_url + '/bills/product-list-load-bill',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            var typeId  = $("#type").val();

            if (typeId == 1)
            {
                $(".tilesType").show();
                $(".tilesTypeUnit").hide();
            }
            else
            {
                $(".tilesType").hide(); 
                $(".tilesTypeUnit").show();
            }
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = $("#contact_type").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '<?php echo e(csrf_token()); ?>' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                            $('.commonCustomerClass').val('');
                        }
                        
                        $("#customer_id").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });

        $('#submitBtn1').click(function() {
            
            var customer_name               = $("#customer_name1").val();
            var address                     = $("#address1").val();
            var mobile_number               = $("#mobile_number1").val();
            var contact_type                = $("#contact_type1").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '<?php echo e(csrf_token()); ?>' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton1').click();
                            $('.commonReferenceClass').val('');
                        }
                        
                        $("#reference_id").empty();
                        $('#reference_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        }); 

        $('#submitBtn3').click(function() {
            
            var product_type            = $("#type").val();
            var product_category_id     = $("#product_category_id").val();
            var brand_id                = $("#brand_id").val();
            var size_id                 = $("#size_id").val();
            var per_cartoon_pcs         = $("#per_cartoon_pcs").val();
            var product_code            = $("#product_code").val();
            var product_name            = $("#product_name").val();
            var unit_id                 = $("#unit_id").val();
            var variation_type          = $("#product_type").val();
            var buying_price            = $("#buying_price").val();
            var selling_price           = $("#selling_price").val();
            var alert_quantity          = $("#alert_quantity").val();
            var site_url                = $('.site_url').val();

            var variation_id = [];
            $('.VId').each(function() {
                variation_id.push($(this).val());
            });

            var variation_value = [];
            $('.VValue').each(function() {
                variation_value.push($(this).val());
            });

            if (product_name == '' || product_category_id == '' || buying_price == '' || selling_price == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }

            $.ajax({
    
                type:   'get',
                url:    site_url + '/invoices/from-invoicex-store-product/',
                data:   { product_type : product_type, product_category_id : product_category_id, brand_id : brand_id, size_id : size_id, per_cartoon_pcs : per_cartoon_pcs, product_name : product_name, product_code : product_code, unit_id : unit_id, variation_type : variation_type, buying_price : buying_price, selling_price : selling_price, alert_quantity : alert_quantity, variation_id : variation_id, variation_value : variation_value,_token: '<?php echo e(csrf_token()); ?>' },
    
                success: function (data) {
                    console.log(data);
                    if(data != 0)
                    {   
                        $("#productEntryIdAddProduct").val(data.id);
                        $("#productEntryNameAddProduct").val(data.name);
                        $("#productEntryRateAddProduct").val(data.sell_price);
                        $('#CloseButton3').click();
                        $('#add_field_button').click();
                    }

                    $("#product_code").val('');
                    $("#product_name").val('');
                    $("#buying_price").val(0);
                    $("#selling_price").val(0);
                    $("#alert_quantity").val(0);
                }
    
            });
        });    
    </script>

    <script type="text/javascript">
        function getItemPrice(x)
        {
            var site_url    = $(".site_url").val();
            var entry_id    = $("#product_entries_"+x).val();
            var cartoon_val = $("#cartoon_"+x).val();
            var pcs_val     = $("#pcs_"+x).val();

            if(entry_id)
            {   
                if ($('#isCollected_'+x).is(":checked"))
                {
                    $('#isCollectedVal_'+x).val(entry_id);
                }
                else
                {
                    $('#isCollectedVal_'+x).val(0);
                }

                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    if (data.type == 1)
                    {
                        var pcs_equivalent_sft  = (parseFloat(data.height)*parseFloat(data.width))/144;
                        var stock_in_pcs        = parseFloat(data.stock_in_hand)/parseFloat(pcs_equivalent_sft);
                        var stock_in_cart       = parseFloat(stock_in_pcs)/parseFloat(data.pcs_per_cartoon);

                        //
                            if (data.stock_in_hand == '' || data.stock_in_hand == null)
                            {
                                var stockInHand  = 0;
                            }
                            else
                            {
                                var stockInHand  = data.stock_in_hand;
                            }

                        //
                            if (cartoon_val == '')
                            {
                                var cartoonVal  = 0;
                            }
                            else
                            {
                                var cartoonVal  = parseFloat(cartoon_val);
                            }

                        //
                            if (pcs_val == '')
                            {
                                var pcsVal  = 0;
                            }
                            else
                            {
                                var pcsVal  = parseFloat(pcs_val);
                            }

                        if (cartoonVal != 0)
                        {
                            var qtyVal1  = parseFloat(cartoonVal)*parseFloat(data.pcs_per_cartoon)*parseFloat(pcs_equivalent_sft);
                        }
                        else
                        {
                            var qtyVal1  = 0;
                        }

                        if (pcsVal != 0)
                        {
                            var qtyVal2  = parseFloat(pcsVal)*parseFloat(pcs_equivalent_sft);

                            if (cartoonVal == '')
                            {
                                var pcsToCartoon = parseFloat(pcsVal)/parseFloat(data.pcs_per_cartoon);

                                //x = 50; y = 15; res = x % y; x = (x - res) / y; [result = 3]
                                var cartAmount = (parseFloat(pcsVal) - (parseFloat(pcsVal)%parseFloat(data.pcs_per_cartoon)))/parseFloat(data.pcs_per_cartoon);
                                var pcsAmount  = parseFloat(pcsVal) - (parseFloat(cartAmount)*parseFloat(data.pcs_per_cartoon));

                                $("#cartoon_"+x).val(parseFloat(cartAmount).toFixed(2));
                                $("#pcs_"+x).val(parseFloat(pcsAmount).toFixed());
                            }
                        }
                        else
                        {
                            var qtyVal2  = 0;
                        }

                        var qtyVal = parseFloat(qtyVal1) + parseFloat(qtyVal2);

                        $("#rate_"+x).val(parseFloat(data.sell_price).toFixed(2));
                        $("#discount_"+x).val(0);
                        $("#quantity_"+x).val(parseFloat(qtyVal).toFixed(2));
                        $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
                        $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' SFT ' + ' | ' + parseFloat(stock_in_cart).toFixed(2) + ' Cart ' + ' | ' + parseFloat(stock_in_pcs).toFixed() + ' PCS ');
            
                        calculateActualAmount(x);
                    }
                    else
                    {
                        //
                            if (data.stock_in_hand == '' || data.stock_in_hand == null)
                            {
                                var stockInHand  = 0;
                            }
                            else
                            {
                                var stockInHand  = data.stock_in_hand;
                            }

                        //
                            if (data.unit_id != null)
                            {
                                var unit = '( ' + data.unit_name + ' )';
                            }
                            else
                            {
                                var unit = '';
                            }

                        $("#cartoon_"+x).val(0);
                        $("#pcs_"+x).val(0);
          
                        $("#rate_"+x).val(parseFloat(data.sell_price).toFixed(2));
                        $("#discount_"+x).val(0);
                        $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
                        $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + unit);
          
                        calculateActualAmount(x);
                    }
                });
            }
        }

        function getItemPriceBackCalculation(x)
        {
            var site_url    = $(".site_url").val();
            var entry_id    = $("#product_entries_"+x).val();
            var sft_val     = $("#quantity_"+x).val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    if (data.type == 1)
                    {
                        var pcs_equivalent_sft  = (parseFloat(data.height)*parseFloat(data.width))/144;
                        var stock_in_pcs        = parseFloat(data.stock_in_hand)/parseFloat(pcs_equivalent_sft);
                        var stock_in_cart       = parseFloat(stock_in_pcs)/parseFloat(data.pcs_per_cartoon);

                        //
                            if (sft_val == '')
                            {
                                var sftVal  = 0;
                            }
                            else
                            {
                                var sftVal  = parseFloat(sft_val);
                            }

                        var findPcs = parseFloat(sft_val)/parseFloat(pcs_equivalent_sft);

                        //x = 50; y = 15; res = x % y; x = (x - res) / y; [result = 3]
                        var cartVal = (parseFloat(findPcs) - (parseFloat(findPcs)%parseFloat(data.pcs_per_cartoon)))/parseFloat(data.pcs_per_cartoon);

                        var pcsVal  = parseFloat(findPcs) - (parseFloat(cartVal)*parseFloat(data.pcs_per_cartoon));

                        $("#cartoon_"+x).val(parseFloat(cartVal).toFixed(2));
                        $("#pcs_"+x).val(parseFloat(pcsVal).toFixed());
                    }

                    calculateActualAmount(x);
                });
            }
        }

        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }

        function addButtonPayment()
        {
            $('.add_field_button_payment').click();
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = <?php echo e(isset($entries_count) ? $entries_count : -1); ?>;
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {   
                x++;

                var serial = x + 1;

                if (serial == 1)
                {
                    var checkbox_label      = '<label style="margin-top: 15px" class="hidden-xs" for="productname"></label>\n';
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var cartoon_label       = '<label class="hidden-xs" for="productname">Cart</label>\n';
                    var pcs_label           = '<label class="hidden-xs" for="productname">PCS</label>\n';
                    var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">SFT/Qty *</label>\n';
                    var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">Discount</label>\n';
                    var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 14px" for="productname"></label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButton()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var checkbox_label = '';
                    var product_label  = '';
                    var cartoon_label  = '';
                    var pcs_label      = '';
                    var rate_label     = '';
                    var quantity_label = '';
                    var discount_label = '';
                    var type_label     = '';
                    var amount_label   = '';
                    var action_label   = '';

                    var add_btn        = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRow').append(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-3 col-12">\n' +
                                                        '<div class="row">\n' +
                                                            '<div class="col-lg-2 col-md-2 col-sm-2 col-12">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname"></label>\n' +
                                                                checkbox_label +
                                                                '<div class="form-check form-check-right">\n' +
                                                                    '<input class="form-check-input" type="checkbox" id="isCollected_'+x+'">\n' +
                                                                    '<input type="hidden" id="isCollectedVal_'+x+'" name="is_collected[]" value="0">\n' +
                                                                '</div>\n' +
                                                            '</div>\n' +
                                                            '<div class="col-lg-10 col-md-10 col-sm-10 col-12">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                                product_label +
                                                                '<select style="width: 100%" name="product_entries[]" class="inner form-control select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                                    '<option value="">' + '--Select Product--' + '</option>' +
                                                                '</select>\n' +
                                                                '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                            '</div>' +
                                                        '</div>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">SFT/Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control" id="quantity_'+x+'" placeholder="SFT/Quantity" oninput="getItemPriceBackCalculation('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Cart</label>\n' +
                                                        cartoon_label +
                                                        '<input type="text" name="cartoon[]" class="inner form-control" id="cartoon_'+x+'" placeholder="Cartoon" oninput="getItemPrice('+x+')" />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">PCS</label>\n' +
                                                        pcs_label +
                                                        '<input type="text" name="pcs[]" class="inner form-control" id="pcs_'+x+'" placeholder="PCS" onchange="getItemPrice('+x+')" />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 14px" for="productname"></label>\n' +
                                                                type_label +
                                                                '<select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_'+x+'" oninput="calculateActualAmount('+x+')">\n' +
                                                                    '<option value="1">BDT</option>' +
                                                                    '<option value="0" selected>%</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" name="discount[]" class="inner form-control" id="discount_'+x+'" placeholder="Discount" oninput="calculateActualAmount('+x+')"/>\n' +
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                        amount_label +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

                var site_url = $(".site_url").val();
                $(".productEntries").select2({
                    ajax: { 
                    url:  site_url + '/bills/product-list-load-bill',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                    },
                });

                var productEId      = $("#productEntryIdAddProduct").val();
                var productEName    = $("#productEntryNameAddProduct").val();
                var pRate           = $("#productEntryRateAddProduct").val();
                
                if ((productEId != 0) && (productEName != 0))
                {
                    $('#product_entries_'+x).append('<option value="'+productEId+'" selected>'+ productEName +'</option>');
                    $('#rate_'+x).val(parseFloat(pRate));
                }   
            }                                    
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            var rate                    = $("#rate_"+x).val();
            var quantity                = $("#quantity_"+x).val();
            var stock                   = $("#stock_"+x).val();
            var discount                = $("#discount_"+x).val();
            var discountType            = $("#discount_type_"+x).val();
            var vatType                 = $("#vat_type_0").val();
            var vatAmount               = $("#vat_amount_0").val();
            var taxType                 = $("#tax_type_0").val();
            var taxAmount               = $("#tax_amount_0").val();
            var totalDiscount           = $("#total_discount_0").val();
            var totalDiscountType       = $("#total_discount_type_0").val();

            if (rate == '')
            {
                var rateCal             = 0;
            }
            else
            {
                var rateCal             = $("#rate_"+x).val();
            }

            if (quantity == '')
            {
                var quantityCal         = 1;
            }
            else
            {
                var quantityCal         = $("#quantity_"+x).val();
            }

            if (discount == '')
            {
                var discountCal         = 1;
            }
            else
            {
                var discountCal         = $("#discount_"+x).val();
            }

            if (discount == '')
            {
                var discountTypeCal     = 0;
            }
            else
            {
                if (discountType == 0)
                {
                    var discountTypeCal     = (parseFloat(discountCal)*parseFloat(rateCal)*parseFloat(quantityCal))/100;
                }
                else
                {
                    var discountTypeCal     = $("#discount_"+x).val();
                }
            }

            var AmountIn              =  (parseFloat(rateCal)*parseFloat(quantityCal)) - parseFloat(discountTypeCal);
     
            $("#amount_"+x).val(parseFloat(AmountIn).toFixed(2));

            //Calculating Subtotal Amount
            var total       = 0;

            $('.amount').each(function()
            {
                total       += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(parseFloat(total).toFixed(2));
            $("#subTotalBdtShow").val(parseFloat(total).toFixed(2));

            if (vatAmount == '')
            {   
                $("#vat_amount_0").val(0);
                var vatCal         = 0;
            }
            else
            {
                var vatCal         = $("#vat_amount_0").val();
            }

            if (vatType == 0)
            {
                var vatTypeCal     = (parseFloat(vatCal)*(parseFloat(total)))/100;
            }
            else
            {
                var vatTypeCal     = $("#vat_amount_0").val();
            }

            if (taxAmount == '')
            {   
                $("#tax_amount_0").val(0);
                var taxCal         = 0;
            }
            else
            {
                var taxCal         = $("#tax_amount_0").val();
            }

            if (taxType == 0)
            {
                var taxTypeCal     = (parseFloat(taxCal)*(parseFloat(total)))/100;
            }
            else
            {
                var taxTypeCal     = $("#tax_amount_0").val();
            }

            if (totalDiscount > 0)
            {   
                if (totalDiscountType == 0)
                {
                    var totalDiscountTypeCal     = (parseFloat(totalDiscount)*(parseFloat(total) + parseFloat(vatTypeCal)))/100;
                }
                else
                {
                    var totalDiscountTypeCal     = $("#total_discount_0").val();
                }
            }
            else
            {
                var totalDiscountTypeCal     = 0;
            }

            var totalShow = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountTypeCal);

            $("#totalBdtShow").val(parseFloat(totalShow).toFixed());
            $("#totalBdt").val(parseFloat(totalShow).toFixed());

            //Checking Overselling Start
            // var check_quantity  = parseFloat(quantity);
            // var check_stock     = parseFloat(stock);

            // if (check_quantity > check_stock)
            // {   
            //     $("#quantity_"+x).val(check_stock);
            // }
            //Checking Overselling End

            calculateChangeAmount();
        }
    </script>

    <script type="text/javascript">
        var max_fields_payment       = 50;                           //maximum input boxes allowed
        var wrapper_payment          = $(".input_fields_wrap_payment");      //Fields wrapper
        var add_button_payment       = $(".add_field_button_payment");       //Add button ID
        var index_no_payment         = 1;

        //For apending another rows start
        var y = -1;
        $(add_button_payment).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(y < max_fields_payment)
            {   
                y++;

                var serial = y + 1;

                if (serial == 1)
                {
                    var amount_paid_label   = '<label class="hidden-xs" for="productname">Paid *</label>\n';
                    var paid_through_label  = '<label class="hidden-xs" for="productname">Paid Through *</label>\n';
                    var account_info_label  = '<label class="hidden-xs" for="productname">Ac/ Info</label>\n';
                    var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button_payment">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonPayment()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var amount_paid_label   = '';
                    var paid_through_label  = '';
                    var account_info_label  = '';
                    var note_label          = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field_payment" data-val="'+y+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRowPayment').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_payment_'+y+'">' +
                                                    '<div style="margin-bottom: 5px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid *</label>\n' +
                                                        amount_paid_label +
                                                        '<input type="text" name="amount_paid[]" class="form-control" id="amount_paid_'+y+'" value="0" required />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid Through *</label>\n' +
                                                        paid_through_label  +
                                                        '<select id="paid_through_'+y+'" style="cursor: pointer" name="paid_through[]" class="form-control single_select2">\n' +
                                                            '<?php if(!empty($paid_accounts) && ($paid_accounts->count() > 0)): ?>\n' +
                                                            '<?php $__currentLoopData = $paid_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $paid_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>\n' +
                                                                '<option value="<?php echo e($paid_account['id']); ?>"><?php echo e($paid_account['name']); ?></option>\n' +
                                                            '<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>\n' +
                                                            '<?php endif; ?>\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Ac/Info</label>\n' +
                                                        account_info_label +
                                                        '<input type="text" name="account_information[]" class="form-control" id="account_information_'+y+'" placeholder="Account Information"/>\n' + 
                                                    '</div>\n' + 

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                        note_label +
                                                        '<input type="text" name="note[]" class="form-control" id="note_'+y+'" placeholder="note"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +

                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
                }                                    
        });
        //For apending another rows end

        $(wrapper_payment).on("click",".remove_field_payment", function(e)
        {
            e.preventDefault();

            var y = $(this).attr("data-val");

            $('.di_payment_'+y).remove(); y--;
        });
    </script>

    <script type="text/javascript">
        $(document).on("change", "#defaultCheck2" , function() {

            var checkbox_value    = $("#defaultCheck2")[0].checked;

            if (checkbox_value == true)
            {
                $("#vatShow").show();
                $("#taxShow").show();
            }
            else
            {
                $("#vatShow").hide();
                $("#taxShow").hide();
            }
        });

        function couponMembership()
        {
            var site_url      = $(".site_url").val();
            var coupon_code   = $("#coupon_code").val();
    
            $('.DiscountType').val(1);
            $('.DiscountAmount').val(0);

            $.get(site_url + '/invoices/search/coupon-code/'+ coupon_code, function(data_coupon){

                if (data_coupon == '')
                {
                    alert('Invalid Coupon Code or Membership Card !!');
                    $('#coupon_code').val('');
                }
                else
                {
                    var state = 0;

                    $('.productEntries').each(function()
                    {
                        var entry_id    = $(this).val();
                        var value_x     = $(this).prop("id");
                        
                        if (data_coupon[0].product_id != null)
                        {
                            for (var i = data_coupon.length - 1; i >= 0; i--)
                            {   
                                if (data_coupon[i].product_id == entry_id)
                                {
                                    var explode    = value_x.split('_');
                                    var di_id      = explode[2];

                                    $('#discount_type_'+di_id).val(data_coupon[i].discount_type);
                                    $('#discount_'+di_id).val(data_coupon[i].discount_amount);
                                    
                                    calculateActualAmount(di_id);

                                    state++;
                                }
                            }
                        }
                        else
                        {
                            var explode    = value_x.split('_');
                            var di_id      = explode[2];

                            $('#discount_type_'+di_id).val(data_coupon[0].discount_type);
                            $('#discount_'+di_id).val(data_coupon[0].discount_amount);
                            
                            calculateActualAmount(di_id);
                        }    
                    });
                }
            });
        }

        function calculateChangeAmount()
        {
            var tAmount             = $("#totalBdtShow").val();
            var cGiven              = $("#cash_given").val();

            if (tAmount != '')
            {
                var totalAmount     = $("#totalBdtShow").val();
            }
            else
            {
                var totalAmount     = 0;
            }

            if (cGiven != '')
            {
                var cashGiven          = $("#cash_given").val();
            }
            else
            {
                var cashGiven          = 0;
            }

            var changeAmount    = parseFloat(cashGiven) - parseFloat(totalAmount);

            if (changeAmount > 0)
            {   
                $("#change_amount").val(0);
                $("#change_amount").val(parseFloat(changeAmount));
                $("#amount_paid_0").val(parseFloat(cashGiven) - parseFloat(changeAmount));
            }

            if (parseFloat(changeAmount) < 0)
            {   
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(cashGiven));
            }

            if (parseFloat(changeAmount) == 0)
            {   
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(cashGiven));
            }
        }
    </script>

    <script type="text/javascript">
        function printInvoiceList()
        {
            var site_url  = $('.site_url').val();

            $.get(site_url + '/invoices/print-invoices-list', function(data){

                var invoice_list = '';
                var sl_no        = 1;
                $.each(data, function(i, invoice_data)
                {
                    var serial              = parseFloat(i) + 1;
                    var site_url            = $('.site_url').val();
                    var print_url_pos       = site_url + '/invoices/show-pos/' + invoice_data.id;

                    if (invoice_data.customer_name != null)
                    {
                        var customer  = invoice_data.customer_name;
                    }
                    else
                    {
                        var customer  = invoice_data.contact_name;
                    }

                    invoice_list += '<tr>' +
                                        '<td>' +
                                            sl_no +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(invoice_data.invoice_date) +
                                        '</td>' +
                                        '<td>' +
                                            'INV - ' + invoice_data.invoice_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            customer + 
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.invoice_amount +
                                        '</td>' +
                                        '<td>' +
                                           (parseFloat(invoice_data.invoice_amount) - parseFloat(invoice_data.due_amount)) +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.due_amount +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.user_name +
                                        '</td>' +
                                        '<td>' +
                                            '<a href="'+ print_url_pos +'">' +
                                                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +'<i class="fa fa-print">' + '</i>' +
                                            '</a>' +
                                        '</td>' +
                                    '</tr>';

                                    sl_no++;
                });

                $("#print_invoice_list").empty();
                $("#print_invoice_list").append(invoice_list);
                
            });
        }

        function printInvoicesSearch()
        {
            var site_url            = $('.site_url').val();
            var date_val            = $('#search_date').val();
            var invoiceNumber_val   = $('#invoiceNumber').val();
            var customer_val        = $('#customer').val();

            if (date_val != '')
            {
                var date = $('#search_date').val();
            }
            else
            {
                var date  = 0;
            }

            if (invoiceNumber_val != '')
            {
                var invoiceNumber = $('#invoiceNumber').val();
            }
            else
            {
                var invoiceNumber  = 0;
            }

            if (customer_val != '')
            {
                var customer = $('#customer').val();
            }
            else
            {
                var customer  = 0;
            }

            $.get(site_url + '/invoices/print-invoices-search/' + date + '/' + customer + '/' + invoiceNumber , function(data){

                var invoice_list = '';
                var sl_no        = 1;
                $.each(data, function(i, invoice_data)
                {
                    var serial              = parseFloat(i) + 1;
                    var site_url            = $('.site_url').val();
                    var print_url_pos       = site_url + '/invoices/show-pos/' + invoice_data.id;

                    if (invoice_data.customer_name != null)
                    {
                        var customer  = invoice_data.customer_name;
                    }
                    else
                    {
                        var customer  = invoice_data.contact_name;
                    }

                    invoice_list += '<tr>' +
                                        '<td>' +
                                            sl_no +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(invoice_data.invoice_date) +
                                        '</td>' +
                                        '<td>' +
                                            'INV - ' + invoice_data.invoice_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            customer + 
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.invoice_amount +
                                        '</td>' +
                                        '<td>' +
                                           (parseFloat(invoice_data.invoice_amount) - parseFloat(invoice_data.due_amount)) +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.due_amount +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.user_name +
                                        '</td>' +
                                        '<td>' +
                                            '<a href="'+ print_url_pos +'">' +
                                                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +'<i class="fa fa-print">' + '</i>' +
                                            '</a>' +
                                        '</td>' +
                                    '</tr>';

                                    sl_no++;
                });

                $("#print_invoice_list").empty();
                $("#print_invoice_list").append(invoice_list);
                
            });
        }

        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        $(document).on("change", "#customer_id" , function() {

            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            $.get(site_url + '/invoices/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data);
                
            });
        });
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap_variation");      //Fields wrapper
        var add_button       = $(".add_field_button_variation");       //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var z       = 0;
        $(add_button).click(function(e)
        {
            e.preventDefault();

            if(z < max_fields)
            {   
                z++;

                $('.getMultipleRowVariation').append(' ' + '<div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_variation_'+z+'">' +
                                                    '<div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">\n' +
                                                        '<select style="width: 100%;cursor: pointer" name="variation_id" class="inner form-control single_select2 VId" id="variation_id_'+z+'" required onchange="variationValue('+z+')">\n' +
                                                            '<option>--Select Variations--</option>\n' +
                                                            '<?php if(!empty($variations) && ($variations->count() > 0)): ?>' +
                                                            '<?php $__currentLoopData = $variations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $variation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>' +
                                                                '<option value="<?php echo e($variation['id']); ?>"><?php echo e($variation['name']); ?></option>\n' +
                                                            '<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>' +
                                                            '<?php endif; ?>' +
                                                        '</select>\n' +

                                                    '</div>\n' +

                                                    '<div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">\n' +
                                                        '<select style="width: 100%;cursor: pointer" name="variation_value" class="inner form-control single_select2 VValue" id="variation_value_'+z+'" required>\n' +
                                                            '<option>--Select Variation Value--</option>\n' +
                                                        '</select>\n' +

                                                    '</div>\n' +

                                                    '<div class="col-lg-1 col-md-2 col-sm-6 col-6 form-group remove_field_variation" data-val="'+z+'">\n' + 
                                                        '<i class="btn btn-success btn-block bx bx-trash-alt font-size-20"></i>\n' + 
                                                    '</div>\n' + 
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field_variation", function(e)
        {
            e.preventDefault();

            var z = $(this).attr("data-val");

            $('.di_variation_'+z).remove(); z--;
        });
    </script>

    <script type="text/javascript">
        function variationValue(x)
        {
            var site_url        = $(".site_url").val();
            var variationId     = $("#variation_id_"+x).val();

            $.get(site_url + '/products/get-variation-value-list/'+ variationId, function(data){
                
                var list5 = '';
                var list7 = '';

                $.each(data, function(i, data_list)
                {   
                    list5 += '<option value = "' +  data_list.id + '">' + data_list.name + '</option>';
                });

                list7 += '<option value = "">' + '--Select Variation Value--' +'</option>';

                $("#variation_value_"+x).empty();
                $("#variation_value_"+x).append(list7);
                $("#variation_value_"+x).append(list5);

            });
        }

        $(document).on('change', '#product_type', function() {
            
            var productType = $('#product_type').val();

            if (productType == 2)
            {
                $(".variationClass").show();
            }

            if (productType == 1)
            {
                $(".variationClass").hide();
            }
        });
    </script>

    <script type="text/javascript">
        $('#type').change(function() {
            
            var typeId  = $("#type").val();

            if (typeId == 1)
            {
                $(".tilesType").show();
                $(".tilesTypeUnit").hide();
            }
            else
            {
                $(".tilesType").hide(); 
                $(".tilesTypeUnit").show();
            }
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/39/Modules/Invoices/Resources/views/index.blade.php ENDPATH**/ ?>