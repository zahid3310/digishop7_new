<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderEntries extends Model
{
    protected $table = "purchase_order_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\Customers','vendor_id');
    }

    public function purchaseOrder()
    {
        return $this->belongsTo('App\Models\PurchaseOrders','bill_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Products','product_id');
    }
}
