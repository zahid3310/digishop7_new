@extends('layouts.app')

@section('title', 'Leave Application')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Leave Application</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                                    <li class="breadcrumb-item active">Leave Application</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {!! Session::get('success') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('unsuccess'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! Session::get('unsuccess') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('errors'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! 'Some required fields are missing..!! Please try again..' !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif
                        
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<h4 class="card-title">All Leave Application</h4>

                                <br>

                                <table id="datatable" class="table table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Apply Date</th>
                                            <th>Employee</th>
                                            <th>Type</th>
                                            <th>Date</th>
                                            <th>Day's</th>
                                            <th>Reason</th>
                                            <th>Reliever</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                    	@if(!empty($leave_applications) && ($leave_applications->count() > 0))
                                    	@foreach($leave_applications as $key => $leave_application)
	                                        <tr>
	                                            <td>{{ $key + 1 }}</td>
                                                <td>{{ date('M j, Y', strtotime($leave_application['application_date'])) }}</td>
                                                <td>{{ $leave_application->employee->name }}</td>
                                                <td>{{ $leave_application->leaveCategory->name }}</td>
                                                <td>{{ date('M j, Y', strtotime($leave_application['start_date'])) }} To {{ date('M j, Y', strtotime($leave_application['end_date'])) }}</td>
                                                <td>{{ $leave_application['total_days'] . ' Days' }}</td>
                                                <td>{{ $leave_application['note'] }}</td>
                                                <td>{{ $leave_application->reliever->name }}</td>
                                                <td>{{ $leave_application['status'] == 1 ? 'Approved' : 'Pending' }}</td>
	                                            <td>
	                                            	<div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <!-- <a class="dropdown-item" href="{{ route('leave_application_edit', $leave_application['id']) }}">Edit</a> -->
                                                        </div>
                                                    </div>
	                                            </td>
	                                        </tr>
	                                    @endforeach
	                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection