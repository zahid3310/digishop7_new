@extends('layouts.app')

@section('title', 'List of Vouchers')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">List of Vouchers</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Accounts</a></li>
                                    <li class="breadcrumb-item active">List of Vouchers</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;text-align: center">SL</th>
                                            <th style="width: 10%;text-align: center">Date</th>
                                            <th style="width: 10%;text-align: center">Voucher#</th>
                                            <th style="width: 10%;text-align: center">Money Receipt#</th>
                                            <th style="width: 10%;text-align: center">Voucher Type</th>
                                            <th style="width: 10%;text-align: center">Total Amount</th>
                                            <th style="width: 10%;text-align: center">Status</th>
                                            <th style="width: 10%;text-align: center">Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        @if(!empty($data['voucherList']) && ($data['voucherList']->count() > 0))
                                        @foreach($data['voucherList'] as $key => $voucherList)
                                            <tr>
                                                <td style="text-align: center">{{ $key + 1 }}</td>
                                                <td style="text-align: center">{{ date('d-m-Y', strtotime($voucherList->VoucherDate)) }}</td>
                                                <td style="text-align: center">{{ $voucherList->Type.'/'.$voucherList->VoucherNumber }}</td>
                                                <td style="text-align: center">{{ $voucherList->MoneyReceiptNo }}</td>
                                                <td style="text-align: center">{{ $voucherList->Type }}</td>
                                                <td style="text-align: center">{{ $voucherList->TotalAmount }}</td>
                                                <td style="text-align: center">
                                                    @if($voucherList->Status == 0)
                                                    <span style="color: red;font-weight: bold">Unposted</span>
                                                    @elseif($voucherList->Status == 1)
                                                    <span style="color: green;font-weight: bold">Posted</span>
                                                    @endif
                                                </td>
                                                <td style="text-align: center">
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('voucher_print', $voucherList->id) }}" target="_blank">Print</a>
                                                            @if($voucherList->Type == 'CP')
                                                            <a class="dropdown-item" href="{{ route('cash_payment_voucher_edit', $voucherList->id) }}" target="_blank">Edit</a>
                                                            @elseif($voucherList->Type == 'CR')
                                                            <a class="dropdown-item" href="{{ route('cash_receipt_voucher_edit', $voucherList->id) }}" target="_blank">Edit</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection