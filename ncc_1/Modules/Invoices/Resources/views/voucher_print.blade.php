<!DOCTYPE html>
<html>
<head>
    <title>
        @if($data->Type == 'CP')
            CASH PAYMENT VOUCHER
        @elseif($data->Type == 'CR')
            CASH RECEIPT VOUCHER
        @elseif($data->Type == 'BP')
            BANK PAYMENT VOUCHER
        @elseif($data->Type == 'JR')
            JOURNAL VOUCHER
        @elseif($data->Type == 'CN')
            CONTRA VOUCHER
        @endif
    </title>

    <link rel="icon" href="{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/bk_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/bk_assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/bk_assets/css/custom.css') }}">

    <script type="text/javascript">
        function printDiv(divName) {
            var printContents       = document.getElementById(divName).innerHTML;
            var originalContents    = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
</head>

<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 15mm 5mm 5mm 10mm;   /* this affects the margin in the printer settings */
    }
</style>

<body id="print-container-body">
    <input style="float:right" type="button" onclick="printDiv('printableArea')" value="Print Voucher" />
    <div id="printableArea">
        <div style="display: none;">
            <button id="btnExport">Export to excel</button>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="" style="">
                            <div style="width:20%;">
                                <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                            </div>

                            <div class="company-head" style="text-align: center; min-height: 82px;">
                                <h3><strong>{{ userDetails()->organization_name }}</strong></h3>
                                <p>{{ userDetails()->address }}</p>
                                <p>{{ userDetails()->contact_number }}</p>
                                <p>{{ userDetails()->contact_email }}</p>
                                <p>{{ userDetails()->website }}</p>
                                <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>              
                            </div>
                            
                            @if($data->Type == 'CP' || $data->Type == 'CR')
                            <div class="ibox-title" style="height:25px;">
                              <h6 style="font-size: 13px; margin-top: 0px;">CREDIT VOUCHER</h6>
                            </div>

                            <div class="ibox-content">
                                <table class="table table-striped table-hover dataTables-example topics" style="margin-bottom: 5px;">
                                    <tbody class="top-header">
                                        <tr>
                                            <td colspan="6" style="border-top: none;">VOUCHER DATE: {{ date('d-m-Y', strtotime($data->VoucherDate)) }}</td>
                                            <td colspan="6" style="text-align:right; border-top: none;">VOUCHER NUMBER: {{ $data->Type.'/'.$data->VoucherNumber }}</td>
                                        </tr>

                                        <tr >
                                            <td colspan="8" style="border-top: none;">MONEY RECEIPT NO: {{ $data->MoneyReceiptNo }}</td>
                                            <td colspan="4" style="text-align:right; border-top: none;">VOUCHER TYPE: CASH_RECEIPT</td>
                                        </tr>

                                        <tr >
                                            <td colspan="12" style="border-top: none;">NARRATION: {{ $data->Narration }} </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                                    <thead class="theight">
                                        <tr>
                                            <th style="width: 5%; text-align: center;">SL#</th>
                                            <th style="width: 20%; text-align: center;">ACCOUNTS HEAD NAME</th>
                                            <th style="width: 20%; text-align: center;">PROJECT NAME</th>
                                            <th style="width: 15%; text-align: center;">REGISTER NAME</th>
                                            <th style="width: 20%; text-align: center;">PARTICULARS</th>
                                            <th style="width: 10%; text-align: center;">DEBIT</th>
                                            <th style="width: 10%; text-align: center;">CREDIT</th>
                                        </tr>
                                    </thead>
                                  
                                    <tbody class="theight">
                                        <?php
                                            $total_debit    = 0;
                                            $total_credit   = 0;
                                        ?>
                                        @if($data->accountTransactions->count() > 0)
                                        @foreach($data->accountTransactions as $key => $val)
                                        <tr class="gradeC">
                                            <td style="text-align: center;">{{ $key + 1 }}</td>
                                            <td>{{ $val->HeadID != null ? $val->headName->HeadName : '' }}</td>
                                            <td>{{ $val->ProjectID != null ? $val->projectName->ProjectName : '' }}</td>
                                            <td>{{ $val->RegisterID != null ? $val->registerName->ClientName : '' }}</td>
                                            <td>{{ $val->Particulars }}</td>
                                            <td style="text-align: right;">{{ number_format($val->DebitAmount, 2) }}</td>
                                            <td style="text-align: right;">{{ number_format($val->CreditAmount, 2) }}</td>
                                        </tr>

                                        <?php
                                            $total_debit    = $total_debit + $val->DebitAmount;
                                            $total_credit   = $total_credit + $val->CreditAmount;
                                        ?>
                                        @endforeach
                                        @endif
                                    </tbody>
                                
                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="5" style="text-align: right;">SUB TOTAL =</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_debit, 2) }}</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_credit, 2) }}</th>
                                        </tr>
                                    </tfoot>
                                </table>

                                <p class="text-taka"><strong>AMOUNT IN WORD: {{ numberTowords($total_debit) }}</strong>
                                </p>

                                <table class="table table-striped table-hover dataTables-example" style="margin-top:50px;">
                                    <thead class="signature">
                                        <tr>
                                           <td style="text-decoration: overline;"><strong>PREPARED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: center;"><strong>CHECKED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: right;"><strong>APPROVED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: right;"><strong>RECEIVED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: right;"><strong>POSTED BY</strong></td>
                                        </tr>
                                    </thead>
                                </table>

                                <table class="table table-striped table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <td style="font-size:8px;">&copy; <?php echo date("Y"); ?> <a target="blank" href="http://www.cyberdynetechnologyltd.com">Cyberdyne Technology Ltd. | Contact: 01715-515755 </a></td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            @endif

                            @if($data->Type == 'BP' || $data->Type == 'BR')
                            <div class="ibox-title" style="height:25px;">
                              <h6 style="font-size: 13px; margin-top: 0px;">DEBIT VOUCHER</h6>
                            </div>

                            <div class="ibox-content">
                                <table class="table table-striped table-hover dataTables-example topics" style="margin-bottom: 5px;">
                                    <tbody class="top-header">
                                        <tr>
                                            <td colspan="6" style="border-top: none;">VOUCHER DATE: {{ date('d-m-Y', strtotime($data->VoucherDate)) }}</td>
                                            <td colspan="6" style="text-align:right; border-top: none;">VOUCHER NUMBER: {{ $data->Type.'/'.$data->VoucherNumber }}</td>
                                        </tr>

                                        <tr >
                                            <td colspan="8" style="border-top: none;">MONEY RECEIPT NO: {{ $data->MoneyReceiptNo }}</td>
                                            <td colspan="4" style="text-align:right; border-top: none;">VOUCHER TYPE: BANK_PAYMENT</td>
                                        </tr>

                                        <tr >
                                            <td colspan="12" style="border-top: none;">NARRATION: {{ $data->Narration }} </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                                    <thead class="theight">
                                        <tr>
                                            <th style="width: 5%; text-align: center;">SL#</th>
                                            <th style="width: 20%; text-align: center;">ACCOUNTS HEAD NAME</th>
                                            <th style="width: 20%; text-align: center;">PROJECT NAME</th>
                                            <th style="width: 15%; text-align: center;">REGISTER NAME</th>
                                            <th style="width: 20%; text-align: center;">PARTICULARS</th>
                                            <th style="width: 10%; text-align: center;">DEBIT</th>
                                            <th style="width: 10%; text-align: center;">CREDIT</th>
                                        </tr>
                                    </thead>
                                  
                                    <tbody class="theight">
                                        <?php
                                            $total_debit    = 0;
                                            $total_credit   = 0;
                                        ?>
                                        @if($data->accountTransactions->count() > 0)
                                        @foreach($data->accountTransactions as $key => $val)
                                        <tr class="gradeC">
                                            <td style="text-align: center;">{{ $key + 1 }}</td>
                                            <td>{{ $val->HeadID != null ? $val->headName->HeadName : '' }}</td>
                                            <td>{{ $val->ProjectID != null ? $val->projectName->ProjectName : '' }}</td>
                                            <td>{{ $val->RegisterID != null ? $val->registerName->ClientName : '' }}</td>
                                            <td>{{ $val->Particulars }}</td>
                                            <td style="text-align: right;">{{ number_format($val->DebitAmount, 2) }}</td>
                                            <td style="text-align: right;">{{ number_format($val->CreditAmount, 2) }}</td>
                                        </tr>

                                        <?php
                                            $total_debit    = $total_debit + $val->DebitAmount;
                                            $total_credit   = $total_credit + $val->CreditAmount;
                                        ?>
                                        @endforeach
                                        @endif
                                    </tbody>
                                
                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="5" style="text-align: right;">SUB TOTAL =</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_debit, 2) }}</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_credit, 2) }}</th>
                                        </tr>
                                    </tfoot>
                                </table>

                                <p class="text-taka"><strong>AMOUNT IN WORD: {{ numberTowords($total_debit) }}</strong>
                                </p>

                                <div class="row">
                                    <div class="col-md-8">
                                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                                            <thead class="theight">
                                                <tr>
                                                    <th style="width: 5%; text-align: center;">SL#</th>
                                                    <th style="width: 20%; text-align: center;">TYPE</th>
                                                    <th style="width: 20%; text-align: center;">NUMBER</th>
                                                    <th style="width: 15%; text-align: center;">DATE</th>
                                                    <th style="width: 20%; text-align: center;">AMOUNT</th>
                                                </tr>
                                            </thead>
                                          
                                            <tbody class="theight">
                                                @if($cheques->count() > 0)
                                                @foreach($cheques as $key => $val_cheque)
                                                <tr class="gradeC">
                                                    <td style="text-align: center;">{{ $key + 1 }}</td>
                                                    <td>{{ $val_cheque->Type }}</td>
                                                    <td>{{ $val_cheque->ChequeNumber }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($val_cheque->VoucherDate)) }}</td>
                                                    <td style="text-align: right">{{ $val_cheque->Amount }}</td>
                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-4"></div>
                                </div>

                                <table class="table table-striped table-hover dataTables-example" style="margin-top:50px;">
                                    <thead class="signature">
                                        <tr>
                                           <td style="text-decoration: overline;"><strong>PREPARED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: center;"><strong>CHECKED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: right;"><strong>APPROVED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: right;"><strong>RECEIVED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: right;"><strong>POSTED BY</strong></td>
                                        </tr>
                                    </thead>
                                </table>

                                <table class="table table-striped table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <td style="font-size:8px;">&copy; <?php echo date("Y"); ?> <a target="blank" href="http://www.cyberdynetechnologyltd.com">Cyberdyne Technology Ltd. | Contact: 01715-515755 </a></td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            @endif

                            @if($data->Type == 'JR')
                            <div class="ibox-title" style="height:25px;">
                              <h6 style="font-size: 13px; margin-top: 0px;">JOURNAL VOUCHER</h6>
                            </div>

                            <div class="ibox-content">
                                <table class="table table-striped table-hover dataTables-example topics" style="margin-bottom: 5px;">
                                    <tbody class="top-header">
                                        <tr>
                                            <td colspan="6" style="border-top: none;">VOUCHER DATE: {{ date('d-m-Y', strtotime($data->VoucherDate)) }}</td>
                                            <td colspan="6" style="text-align:right; border-top: none;">VOUCHER NUMBER: {{ $data->Type.'/'.$data->VoucherNumber }}</td>
                                        </tr>

                                        <tr >
                                            <td colspan="8" style="border-top: none;">NARRATION: {{ $data->Narration }}</td>
                                            <td colspan="4" style="text-align:right; border-top: none;">VOUCHER TYPE: JOURNAL_VOUCHER</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                                    <thead class="theight">
                                        <tr>
                                            <th style="width: 5%; text-align: center;">SL#</th>
                                            <th style="width: 20%; text-align: center;">ACCOUNTS HEAD NAME</th>
                                            <th style="width: 20%; text-align: center;">PROJECT NAME</th>
                                            <th style="width: 15%; text-align: center;">REGISTER NAME</th>
                                            <th style="width: 20%; text-align: center;">PARTICULARS</th>
                                            <th style="width: 10%; text-align: center;">DEBIT</th>
                                            <th style="width: 10%; text-align: center;">CREDIT</th>
                                        </tr>
                                    </thead>
                                  
                                    <tbody class="theight">
                                        <?php
                                            $total_debit    = 0;
                                            $total_credit   = 0;
                                        ?>
                                        @if($data->accountTransactions->count() > 0)
                                        @foreach($data->accountTransactions as $key => $val)
                                        <tr class="gradeC">
                                            <td style="text-align: center;">{{ $key + 1 }}</td>
                                            <td>{{ $val->HeadID != null ? $val->headName->HeadName : '' }}</td>
                                            <td>{{ $val->ProjectID != null ? $val->projectName->ProjectName : '' }}</td>
                                            <td>{{ $val->RegisterID != null ? $val->registerName->ClientName : '' }}</td>
                                            <td>{{ $val->Particulars }}</td>
                                            <td style="text-align: right;">{{ number_format($val->DebitAmount, 2) }}</td>
                                            <td style="text-align: right;">{{ number_format($val->CreditAmount, 2) }}</td>
                                        </tr>

                                        <?php
                                            $total_debit    = $total_debit + $val->DebitAmount;
                                            $total_credit   = $total_credit + $val->CreditAmount;
                                        ?>
                                        @endforeach
                                        @endif
                                    </tbody>
                                
                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="5" style="text-align: right;">SUB TOTAL =</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_debit, 2) }}</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_credit, 2) }}</th>
                                        </tr>
                                    </tfoot>
                                </table>

                                <p class="text-taka"><strong>AMOUNT IN WORD: {{ numberTowords($total_debit) }}</strong>
                                </p>

                                <table class="table table-striped table-hover dataTables-example" style="margin-top:50px;">
                                    <thead class="signature">
                                        <tr>
                                           <td style="text-decoration: overline;"><strong>PREPARED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: center;"><strong>CHECKED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: right;"><strong>APPROVED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: right;"><strong>RECEIVED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: right;"><strong>POSTED BY</strong></td>
                                        </tr>
                                    </thead>
                                </table>

                                <table class="table table-striped table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <td style="font-size:8px;">&copy; <?php echo date("Y"); ?> <a target="blank" href="http://www.cyberdynetechnologyltd.com">Cyberdyne Technology Ltd. | Contact: 01715-515755 </a></td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            @endif

                            @if($data->Type == 'CN')
                            <div class="ibox-title" style="height:25px;">
                              <h6 style="font-size: 13px; margin-top: 0px;">CONTRA VOUCHER</h6>
                            </div>

                            <div class="ibox-content">
                                <table class="table table-striped table-hover dataTables-example topics" style="margin-bottom: 5px;">
                                    <tbody class="top-header">
                                        <tr>
                                            <td colspan="6" style="border-top: none;">VOUCHER DATE: {{ date('d-m-Y', strtotime($data->VoucherDate)) }}</td>
                                            <td colspan="6" style="text-align:right; border-top: none;">VOUCHER NUMBER: {{ $data->Type.'/'.$data->VoucherNumber }}</td>
                                        </tr>

                                        <tr >
                                            <td colspan="4" style="text-align:right; border-top: none;">VOUCHER TYPE: CONTRA_VOUCHER</td>
                                            <td colspan="12" style="border-top: none;">NARRATION: {{ $data->Narration }} </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                                    <thead class="theight">
                                        <tr>
                                            <th style="width: 5%; text-align: center;">SL#</th>
                                            <th style="width: 20%; text-align: center;">ACCOUNTS HEAD NAME</th>
                                            <th style="width: 20%; text-align: center;">PARTICULARS</th>
                                            <th style="width: 10%; text-align: center;">DEBIT</th>
                                            <th style="width: 10%; text-align: center;">CREDIT</th>
                                        </tr>
                                    </thead>
                                  
                                    <tbody class="theight">
                                        <?php
                                            $total_debit    = 0;
                                            $total_credit   = 0;
                                        ?>
                                        @if($data->accountTransactions->count() > 0)
                                        @foreach($data->accountTransactions as $key => $val)
                                        <tr class="gradeC">
                                            <td style="text-align: center;">{{ $key + 1 }}</td>
                                            <td>{{ $val->HeadID != null ? $val->bankName->AccountNum : '' }}</td>
                                            <td>{{ $val->Particulars }}</td>
                                            <td style="text-align: right;">{{ number_format($val->DebitAmount, 2) }}</td>
                                            <td style="text-align: right;">{{ number_format($val->CreditAmount, 2) }}</td>
                                        </tr>

                                        <?php
                                            $total_debit    = $total_debit + $val->DebitAmount;
                                            $total_credit   = $total_credit + $val->CreditAmount;
                                        ?>
                                        @endforeach
                                        @endif
                                    </tbody>
                                
                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="3" style="text-align: right;">SUB TOTAL =</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_debit, 2) }}</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_credit, 2) }}</th>
                                        </tr>
                                    </tfoot>
                                </table>

                                <p class="text-taka"><strong>AMOUNT IN WORD: {{ numberTowords($total_debit) }}</strong>
                                </p>

                                <div class="row">
                                    <div class="col-md-8">
                                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                                            <thead class="theight">
                                                <tr>
                                                    <th style="width: 5%; text-align: center;">SL#</th>
                                                    <th style="width: 20%; text-align: center;">TYPE</th>
                                                    <th style="width: 20%; text-align: center;">NUMBER</th>
                                                    <th style="width: 15%; text-align: center;">DATE</th>
                                                    <th style="width: 20%; text-align: center;">AMOUNT</th>
                                                </tr>
                                            </thead>
                                          
                                            <tbody class="theight">
                                                @if($cheques->count() > 0)
                                                @foreach($cheques as $key => $val_cheque)
                                                <tr class="gradeC">
                                                    <td style="text-align: center;">{{ $key + 1 }}</td>
                                                    <td>{{ $val_cheque->Type }}</td>
                                                    <td>{{ $val_cheque->ChequeNumber }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($val_cheque->VoucherDate)) }}</td>
                                                    <td style="text-align: right">{{ $val_cheque->Amount }}</td>
                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-4"></div>
                                </div>

                                <table class="table table-striped table-hover dataTables-example" style="margin-top:50px;">
                                    <thead class="signature">
                                        <tr>
                                           <td style="text-decoration: overline;"><strong>PREPARED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: center;"><strong>CHECKED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: right;"><strong>APPROVED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: right;"><strong>RECEIVED BY</strong></td>
                                            <td style="text-decoration: overline; text-align: right;"><strong>POSTED BY</strong></td>
                                        </tr>
                                    </thead>
                                </table>

                                <table class="table table-striped table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <td style="font-size:8px;">&copy; <?php echo date("Y"); ?> <a target="blank" href="http://www.cyberdynetechnologyltd.com">Cyberdyne Technology Ltd. | Contact: 01715-515755 </a></td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ url('public/bk_assets/js/jquery-2.1.1.js') }}"></script>
        <script src="{{ url('public/bk_assets/js/jquery.btechco.excelexport.js') }}"></script>
        <script src="{{ url('public/bk_assets/js/jquery.base64.js') }}"></script>

        <script type="text/javascript">
            var isCtrl = false;$(document).keyup(function (e) {
            if(e.which == 17) isCtrl=false;
            }).keydown(function (e) {
                if(e.which == 17) isCtrl=true;
                if(e.which == 69 && isCtrl == true) {
                    $("#tblExport").btechco_excelexport({
                        containerid: "tblExport"
                       , datatype: $datatype.Table
                    });
                    return false;
                }
            });
        </script>
    </div>
</body>
</html>