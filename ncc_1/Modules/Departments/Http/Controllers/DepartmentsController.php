<?php

namespace Modules\Departments\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Departments;
use DB;
use Response;

class DepartmentsController extends Controller
{
    public function index()
    {
        $departments  = Departments::orderBy('id', 'ASC')->get();

        return view('departments::index', compact('departments'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $department                    = new Departments;
            $department->name              = $data['name'];
            $department->description       = $data['description'];
            $department->created_by        = $user_id;
            
            if ($department->save())
            {
                return redirect()->route('department_index')->with("success","Department Created Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function edit($id)
    {
        $departments        = Departments::orderBy('id', 'ASC')->get();
        $find_department    = Departments::find($id);

        return view('departments::edit', compact('departments', 'find_department'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $department                    = Departments::find($id);
            $department->name              = $data['name'];
            $department->description       = $data['description'];
            $department->updated_by        = $user_id;
            
            if ($department->save())
            {
                return redirect()->route('department_index')->with("success","Department Updated Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }
}
