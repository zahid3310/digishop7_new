<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('accounts/account-types')->group(function() {
    Route::get('/', 'AccountsController@accountTypesIndex')->name('account_types_index');
    Route::post('/store', 'AccountsController@accountTypesStore')->name('account_types_store');
    Route::get('/edit/{id}', 'AccountsController@accountTypesEdit')->name('account_types_edit');
    Route::post('/update/{id}', 'AccountsController@accountTypesUpdate')->name('account_types_update');
});

Route::prefix('accounts/chart-of-accounts')->group(function() {
    Route::get('/', 'AccountsController@chartOfAccountIndex')->name('chart_of_accounts_index');
    Route::get('/store', 'AccountsController@chartOfAccountStore')->name('chart_of_accounts_store');
    Route::get('/edit/{id}', 'AccountsController@chartOfAccountEdit')->name('chart_of_accounts_edit');
    Route::get('/update', 'AccountsController@chartOfAccountUpdate')->name('chart_of_accounts_update');
});

Route::prefix('accounts/chart-of-projects')->group(function() {
    Route::get('/', 'AccountsController@chartOfProjectsIndex')->name('chart_of_projects_index');
    Route::get('/store', 'AccountsController@chartOfProjectsStore')->name('chart_of_projects_store');
    Route::get('/edit/{id}', 'AccountsController@chartOfProjectsEdit')->name('chart_of_projects_edit');
    Route::get('/update', 'AccountsController@chartOfProjectsUpdate')->name('chart_of_projects_update');
});

Route::prefix('accounts/chart-of-registers')->group(function() {
    Route::get('/', 'AccountsController@chartOfRegistersIndex')->name('chart_of_registers_index');
    Route::get('/store', 'AccountsController@chartOfRegistersStore')->name('chart_of_registers_store');
    Route::get('/edit/{id}', 'AccountsController@chartOfRegistersEdit')->name('chart_of_registers_edit');
    Route::get('/update', 'AccountsController@chartOfRegistersUpdate')->name('chart_of_registers_update');
});