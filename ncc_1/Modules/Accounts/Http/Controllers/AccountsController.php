<?php

namespace Modules\Accounts\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Response;
use Auth;
use DB;

//Models
use App\Models\ParentAccountTypes;
use App\Models\AccountTypes;
use App\Models\Accounts;
use App\Models\AccountHead;
use App\Models\Projects;
use App\Models\Registers;

class AccountsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    ## Account Type Crude Start
    public function accountTypesIndex()
    {
    	$parent_accounts 	= ParentAccountTypes::get();
    	$account_types  	= AccountTypes::get();

        return view('accounts::account_types.index', compact('parent_accounts', 'account_types'));
    }

    public function accountTypesStore(Request $request)
    {
        $rules = array(
            'name'      			=> 'required',
            'parent_account_id'   	=> 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
        	$account_type                    		= new AccountTypes;
            $account_type->account_name             = $data['name'];
            $account_type->parent_account_type_id   = $data['parent_account_id'];
            $account_type->description       		= $data['description'];
            $account_type->status       			= $data['status'];
            
            if ($account_type->save())
            {	
            	DB::commit();
                return redirect()->route('account_types_index')->with("success","Account Type Created Successfully !!");
            }
                
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function accountTypesEdit($id)
    {
    	$parent_accounts 	= ParentAccountTypes::get();
    	$account_types  	= AccountTypes::get();
    	$find_account_type  = AccountTypes::find($id);

        return view('accounts::account_types.edit', compact('parent_accounts', 'account_types', 'find_account_type'));
    }

    public function accountTypesUpdate(Request $request, $id)
    {
        $rules = array(
            'name'      			=> 'required',
            'parent_account_id'   	=> 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
        	$account_type                    		= AccountTypes::find($id);
            $account_type->account_name             = $data['name'];
            $account_type->parent_account_type_id   = $data['parent_account_id'];
            $account_type->description       		= $data['description'];
            $account_type->status       			= $data['status'];
            
            if ($account_type->save())
            {	
            	DB::commit();
                return redirect()->route('account_types_index')->with("success","Account Type Updated Successfully !!");
            }
                
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }
    ## Account Type Crude End

    ## Chart of Accounts Crude Start
        public function chartOfAccountIndex()
        {
            $itemQueryResult    = AccountHead::where('ActiveStatus', 1)
                                        ->selectRaw('Position, 
                                                     PtnGroupCode,
                                                     id, 
                                                     code, 
                                                     HeadName, 
                                                     PtnID, 
                                                     GroupID, 
                                                     IsTransactable, 
                                                     0 as process_status')
                                        ->orderBy('id', 'ASC')
                                        ->get();

            return view('accounts::chart_of_accounts.index', compact('itemQueryResult'));
        }

        public function chartOfAccountStore(Request $request)
        {
            $user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                if ($data['type'] == 0)
                {
                    $parentQuery            = Accounts::where('id', $data['new_non_PtnID'])->get();
                    $PtnGroupCodeParent     = isset($parentQuery[0]["PtnGroupCode"]) ? $parentQuery[0]["PtnGroupCode"] : "";
                    //getting parent's PtnGroupCode
                    $parentChild            = Accounts::where('PtnID', $data['new_non_PtnID'])->get();
                    $parentChildCount       = $parentChild->count();
                    $parentChildCount++;
                    $PtnGroupCode           = $PtnGroupCodeParent."-".$parentChildCount;

                    $account                 = new Accounts;
                    $account->code           = $data['Non_code'];
                    $account->Position       = $data['new_non_Position'];
                    $account->HeadName       = $data['new_non_HeadName'];
                    $account->PtnID          = $data['new_non_PtnID'];
                    $account->PtnGroupCode   = $PtnGroupCode;
                    $account->GroupID        = $data['new_non_GroupID'];
                    $account->IsTransactable = $data['is_transactable'];
                    $account->ActiveStatus   = 1;
                }

                if ($data['type'] == 1)
                {
                    $parentQuery            = Accounts::where('id', $data['new_tra_PtnID'])->get();
                    $PtnGroupCodeParent     = isset($parentQuery[0]["PtnGroupCode"]) ? $parentQuery[0]["PtnGroupCode"] : "";
                    //getting parent's PtnGroupCode
                    $parentChild            = Accounts::where('PtnID', $data['new_tra_PtnID'])->get();
                    $parentChildCount       = $parentChild->count();
                    $parentChildCount++;
                    $PtnGroupCode           = $PtnGroupCodeParent."-".$parentChildCount;

                    $account                 = new Accounts;
                    $account->code           = $data['tran_code'];
                    $account->Position       = $data['new_tra_Position'];
                    $account->HeadName       = $data['new_tra_HeadName'];
                    $account->PtnID          = $data['new_tra_PtnID'];
                    $account->PtnGroupCode   = $PtnGroupCode;
                    $account->GroupID        = $data['new_tra_GroupID'];
                    $account->IsTransactable = $data['is_transactable'];
                    $account->PurposeHeadName= $data['purpose-head'];
                    $account->OpDebitAmount  = $data['opening_debit'];
                    $account->OpCreditAmount = $data['opening_credit'];
                    $account->ActiveStatus   = 1;
                }

                if ($account->save())
                {	
                	DB::commit();
                    return $account->id;
                }     
            }catch (\Exception $exception){
                DB::rollback();
                return json_encode($exception);
            }
        }

        public function chartOfAccountEdit($id)
        {
        	$find_account   = Accounts::find($id);

            return Response::json($find_account);
        }

        public function chartOfAccountUpdate(Request $request)
        {
    		$user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                if ($data['type'] == 0)
                {
                    $account                 = Accounts::find($data['Edit_Non_HeadID']);
                    $account->Position       = $data['Edit_Non_Position'];
                    $account->HeadName       = $data['Edit_Non_HeadName'];
                    $account->code           = $data['Edit_Non_code'];
                    $account->ActiveStatus   = $data['ActiveStatus'];
                }

                if ($data['type'] == 1)
                {
                    $account                 = Accounts::find($data['edit_tra_HeadID']);
                    $account->Position       = $data['edit_tra_Position'];
                    $account->HeadName       = $data['edit_tra_HeadName'];
                    $account->code           = $data['Edit_tran_code'];
                    $account->OpDebitAmount  = $data['edit_opening_debit'];
                    $account->OpCreditAmount = $data['edit_opening_credit'];
                    $account->ActiveStatus   = $data['ActiveStatuss'];
                }

                if ($account->save())
                {   
                    DB::commit();
                    return $account->id;
                }     
            }catch (\Exception $exception){
                DB::rollback();
                return json_encode($exception);
            }
        }
    ## Chart of Accounts Crude End

    ## Chart of Projects Crude Start
        public function chartOfProjectsIndex()
        {
            $itemQueryResult    = Projects::where('ActiveStatus', 1)
                                        ->selectRaw('PtnGroupCode,
                                                     id, 
                                                     ProjectName as HeadName, 
                                                     PtnID, 
                                                     GroupID, 
                                                     IsTransactable, 
                                                     0 as process_status')
                                        ->orderBy('id', 'ASC')
                                        ->get();

            return view('accounts::chart_of_projects.index', compact('itemQueryResult'));
        }

        public function chartOfProjectsStore(Request $request)
        {
            $user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                if ($data['type'] == 0)
                {
                    $CompanyID          = Auth()->user()->branch_id;
                    $PtnID              = $data['New_Non_PtnID'];

                    //getting parent's PtnGroupCode
                    $GroupIdQuery       = Projects::where('id', $PtnID)->get();
                    $GetGroupId         = isset($GroupIdQuery[0]["GroupID"]) ? $GroupIdQuery[0]["GroupID"] : "";
                    $PtnGroupCodeParent = isset($GroupIdQuery[0]["PtnGroupCode"]) ? $GroupIdQuery[0]["PtnGroupCode"] : "";
                    $GroupID            = $GetGroupId + 1;

                    //getting parent's PtnGroupCode
                    $parentChild        = Projects::where('PtnID', $PtnID)->get();
                    $parentChildCount   = $parentChild->count();
                    $parentChildCount++;
                    $PtnGroupCode       = $PtnGroupCodeParent."-".$parentChildCount;

                    $project                    = new Projects;
                    $project->PtnID             = $PtnID;
                    $project->CompanyID         = $CompanyID;
                    $project->PtnGroupCode      = $PtnGroupCode;
                    $project->ProjectName       = $data['New_Non_ProjectName'];
                    $project->GroupID           = $GroupID;
                    $project->OrderID           = $parentChildCount;
                    $project->IsTransactable    = $data['is_transactable'];
                    $project->ActiveStatus      = 1;
                }

                if ($data['type'] == 1)
                {
                    $CompanyID          = Auth()->user()->branch_id;
                    $PtnID              = $data['new_tra_PtnID'];

                    //getting parent's PtnGroupCode
                    $GroupIdQuery       = Projects::where('id', $PtnID)->get();
                    $GetGroupId         = isset($GroupIdQuery[0]["GroupID"]) ? $GroupIdQuery[0]["GroupID"] : "";
                    $PtnGroupCodeParent = isset($GroupIdQuery[0]["PtnGroupCode"]) ? $GroupIdQuery[0]["PtnGroupCode"] : "";
                    $GroupID            = $GetGroupId + 1;

                    //getting parent's PtnGroupCode
                    $parentChild        = Projects::where('PtnID', $PtnID)->get();
                    $parentChildCount   = $parentChild->count();
                    $parentChildCount++;
                    $PtnGroupCode       = $PtnGroupCodeParent."-".$parentChildCount;

                    $project                    = new Projects;
                    $project->PtnID             = $PtnID;
                    $project->CompanyID         = $CompanyID;
                    $project->PtnGroupCode      = $PtnGroupCode;
                    $project->ProjectName       = $data['new_tra_HeadName'];
                    $project->GroupID           = $GroupID;
                    $project->OrderID           = $parentChildCount;
                    $project->IsTransactable    = $data['is_transactable'];
                    $project->ActiveStatus      = 1;
                }

                if ($project->save())
                {   
                    DB::commit();
                    return $project->id;
                }     
            }catch (\Exception $exception){
                DB::rollback();
                return json_encode($exception);
            }
        }

        public function chartOfProjectsEdit($id)
        {
            $find_account   = Projects::find($id);

            return Response::json($find_account);
        }

        public function chartOfProjectsUpdate(Request $request)
        {
            $user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                if ($data['type'] == 0)
                {
                    $project                    = Projects::find($data['Edit_Non_HeadID']);
                    $project->ProjectName       = $data['edit_non_HeadName'];
                    $project->ActiveStatus      = $data['ActiveStatus'];
                }

                if ($data['type'] == 1)
                {
                    $project                    = Projects::find($data['editTraHeadID']);
                    $project->ProjectName       = $data['edit_tra_HeadName'];
                    $project->ActiveStatus      = $data['ActiveStatuss'];
                }

                if ($project->save())
                {   
                    DB::commit();
                    return $project->id;
                }     
            }catch (\Exception $exception){
                DB::rollback();
                return json_encode($exception);
            }
        }
    ## Chart of Projects Crude End

    ## Chart of Registers Crude Start
    public function chartOfRegistersIndex()
    {
        $itemQueryResult    = Registers::where('ActiveStatus', 1)
                                    ->selectRaw('PtnGroupCode,
                                                 id, 
                                                 ClientName as HeadName, 
                                                 PtnID, 
                                                 GroupID, 
                                                 IsTransactable, 
                                                 0 as process_status')
                                    ->orderBy('id', 'ASC')
                                    ->get();

        return view('accounts::chart_of_registers.index', compact('itemQueryResult'));
    }

    public function chartOfRegistersStore(Request $request)
    {
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            if ($data['type'] == 0)
            {
                $CompanyID          = Auth()->user()->branch_id;
                $PtnID              = $data['New_Non_PtnID'];

                //getting parent's PtnGroupCode
                $GroupIdQuery       = Registers::where('id', $PtnID)->get();
                $GetGroupId         = isset($GroupIdQuery[0]["GroupID"]) ? $GroupIdQuery[0]["GroupID"] : "";
                $PtnGroupCodeParent = isset($GroupIdQuery[0]["PtnGroupCode"]) ? $GroupIdQuery[0]["PtnGroupCode"] : "";
                $GroupID            = $GetGroupId + 1;

                //getting parent's PtnGroupCode
                $parentChild        = Registers::where('PtnID', $PtnID)->get();
                $parentChildCount   = $parentChild->count();
                $parentChildCount++;
                $PtnGroupCode       = $PtnGroupCodeParent."-".$parentChildCount;

                $register                    = new Registers;
                $register->PtnID             = $PtnID;
                $register->CompanyID         = $CompanyID;
                $register->PtnGroupCode      = $PtnGroupCode;
                $register->ClientName        = $data['New_Non_ClientName'];
                $register->GroupID           = $GroupID;
                $register->OrderID           = $parentChildCount;
                $register->IsTransactable    = $data['is_transactable'];
                $register->ActiveStatus      = 1;
            }

            if ($data['type'] == 1)
            {
                $CompanyID          = Auth()->user()->branch_id;
                $PtnID              = $data['new_tra_PtnID'];

                //getting parent's PtnGroupCode
                $GroupIdQuery       = Registers::where('id', $PtnID)->get();
                $GetGroupId         = isset($GroupIdQuery[0]["GroupID"]) ? $GroupIdQuery[0]["GroupID"] : "";
                $PtnGroupCodeParent = isset($GroupIdQuery[0]["PtnGroupCode"]) ? $GroupIdQuery[0]["PtnGroupCode"] : "";
                $GroupID            = $GetGroupId + 1;

                //getting parent's PtnGroupCode
                $parentChild        = Registers::where('PtnID', $PtnID)->get();
                $parentChildCount   = $parentChild->count();
                $parentChildCount++;
                $PtnGroupCode       = $PtnGroupCodeParent."-".$parentChildCount;

                $register                       = new Registers;
                $register->PtnID                = $PtnID;
                $register->CompanyID            = $CompanyID;
                $register->PtnGroupCode         = $PtnGroupCode;
                $register->ClientName           = $data['new_tra_HeadName'];
                $register->ClientAddress        = $data['Address'];
                $register->ClientEmail          = $data['clientEmail'];
                $register->ClientContactNumber  = $data['ContactNumber'];
                $register->SMSNumber            = $data['smsnumber'];
                $register->GroupID              = $GroupID;
                $register->OrderID              = $parentChildCount;
                $register->IsTransactable       = $data['is_transactable'];
                $register->ActiveStatus         = 1;
            }

            if ($register->save())
            {   
                DB::commit();
                return $register->id;
            }     
        }catch (\Exception $exception){
            DB::rollback();
            return json_encode($exception);
        }
    }

    public function chartOfRegistersEdit($id)
    {
        $find_account   = Registers::find($id);

        return Response::json($find_account);
    }

    public function chartOfRegistersUpdate(Request $request)
    {
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            if ($data['type'] == 0)
            {
                $register                    = Registers::find($data['edit_non_HeadID']);
                $register->ClientName        = $data['edit_non_HeadName'];
                $register->ActiveStatus      = $data['ActiveStatus'];
            }

            if ($data['type'] == 1)
            {
                $register                       = Registers::find($data['editTraHeadID']);
                $register->ClientName           = $data['edit_tra_ClientName'];
                $register->ClientAddress        = $data['edit_tra_ClientAddress'];
                $register->ClientEmail          = $data['edit_tra_ClientEmail'];
                $register->ClientContactNumber  = $data['edit_tra_ClientContactNumber'];
                $register->SMSNumber            = $data['editsmsnumber'];
                $register->IsTransactable       = $data['is_transactable'];
                $register->ActiveStatus         = $data['ActiveStatuss'];
            }

            if ($register->save())
            {   
                DB::commit();
                return $register->id;
            }     
        }catch (\Exception $exception){
            DB::rollback();
            return json_encode($exception);
        }
    }
    ## Chart of Registers Crude End
}
