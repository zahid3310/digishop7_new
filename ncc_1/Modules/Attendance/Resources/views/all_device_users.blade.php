@extends('layouts.app')

@section('title', 'List of Employees')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">List of Employees</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Attendance</a></li>
                                    <li class="breadcrumb-item active">List of Employees</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {!! Session::get('success') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('unsuccess'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! Session::get('unsuccess') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('errors'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! 'Some required fields are missing..!! Please try again..' !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        <div class="card">
                            <button style="float: left" type="button" class="btn btn-success col-md-2">
                                <a style="color: black" href="{{ route('attendance_pull_data') }}">Get Data</a>
                            </button>
                            <hr>
                            <form method="post" action="{{ route('attendance_import_user_update') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body table-responsive">
                                    <table class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                        <thead>
                                            <tr>
                                                <th style="width: 5%;text-align: center">SL#</th>
                                                <th style="text-align: center">Employee ID</th>
                                                <th style="text-align: center">Employee Name</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php $i = 1; ?>
                                            @if(!empty($employees) && (count($employees) > 0))
                                            @foreach($employees as $key => $employee)
                                                <tr>
                                                    <td style="text-align: center">{{ $i }}</td>
                                                    <td>
                                                        <input class="form-control" type="text" name="employee_id[]" value="{{ $employee['userid'] }}">
                                                        <input class="form-control" type="hidden" name="old_employee_id[]" value="{{ $employee['userid'] }}">
                                                        <input class="form-control" type="hidden" name="uid[]" value="{{ $employee['uid'] }}">
                                                    </td>
                                                    <td>
                                                        <input class="form-control" type="text" name="employee_name[]" value="{{ $employee['name'] }}">
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>

                                    <div class="form-group row">
                                        <div class="col-md-5"></div>
                                        <div class="col-md-2">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Update Data</button>
                                        </div>
                                        <div class="col-md-5"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
        });
    </script>
@endsection