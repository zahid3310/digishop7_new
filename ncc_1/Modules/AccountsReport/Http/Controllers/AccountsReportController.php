<?php

namespace Modules\AccountsReport\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Models
use DB;
use Auth;
use Response;
use Carbon\Carbon;
use App\Models\AccountTransaction;

class AccountsReportController extends Controller
{
    public function cashBookIndex()
    {
        return view('accountsreport::cashBook.index');
    }

    public function cashBookPrint()
    {
        $branch_id  = Auth()->user()->branch_id;
        $date       = date('Y-m-d');
        $from_date  = isset($_GET['from_date']) ? date('Y-m-d 00:00:00', strtotime($_GET['from_date'])) : date('Y-m-d 00:00:00', strtotime($date));
        $to_date    = isset($_GET['to_date']) ? date('Y-m-d 00:00:00', strtotime($_GET['to_date'])) : date('Y-m-d 00:00:00', strtotime($date));

        $data_list      = AccountTransaction::whereBetween('VoucherDate', [$from_date, $to_date])
                                        ->where('IsPosted', 1)
                                        ->where('CompanyID', $branch_id)
                                        ->where('PurposeAccHeadName', 'cash_ac')
                                        ->whereIn('VoucherType', ['CP', 'CR', 'JR', 'CN'])
                                        ->get();

        $cash_in_hand_debit     = $data_list->sum('DebitAmount');
        $cash_in_hand_credit    = $data_list->sum('CreditAmount');
        $cash_in_hand           = $cash_in_hand_debit - $cash_in_hand_credit;

        $data['from_date']      = $from_date;
        $data['to_date']        = $to_date;
        $data['result']         = $data_list;
        $data['cash_in_hand']   = $cash_in_hand;

        return view('accountsreport::cashBook.print', compact('data'));
    }

    public function bankBookIndex()
    {
        return view('accountsreport::bankBook.index');
    }

    public function bankBookPrint()
    {   
        $branch_id  = Auth()->user()->branch_id;
        $date       = date('Y-m-d');
        $from_date  = isset($_GET['from_date']) ? date('Y-m-d 00:00:00', strtotime($_GET['from_date'])) : date('Y-m-d 00:00:00', strtotime($date));
        $to_date    = isset($_GET['to_date']) ? date('Y-m-d 00:00:00', strtotime($_GET['to_date'])) : date('Y-m-d 00:00:00', strtotime($date));

        $data_list      = AccountTransaction::whereBetween('VoucherDate', [$from_date, $to_date])
                                        ->where('IsPosted', 1)
                                        ->where('CompanyID', $branch_id)
                                        ->where('PurposeAccHeadName', 'bank_ac')
                                        ->whereIn('VoucherType', ['BP', 'BR', 'JR', 'CN'])
                                        ->get();

        $bank_debit             = $data_list->sum('DebitAmount');
        $bank_credit            = $data_list->sum('CreditAmount');
        $balance                = $bank_debit - $bank_credit;

        $data['from_date']      = $from_date;
        $data['to_date']        = $to_date;
        $data['result']         = $data_list;
        $data['balance']        = $balance;

        return view('accountsreport::bankBook.print', compact('data'));
    }
}
