

<?php $__env->startSection('title', 'Add Devices'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Add Devices</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Attendance</a></li>
                                    <li class="breadcrumb-item active">Add Devices</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <?php if(Session::has('success')): ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php echo Session::get('success'); ?>

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <?php endif; ?>

                    <?php if(Session::has('unsuccess')): ?>
                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                        <?php echo Session::get('unsuccess'); ?>

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <?php endif; ?>

                    <?php if(Session::has('errors')): ?>
                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                        <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <?php endif; ?>

                    <div class="col-12">
                        <div class="card">
                            <br>
                            <form id="FormSubmit" action="<?php echo e(route('attendance_devices_store')); ?>" method="post" files="true" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group"></div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                        <input type="text" name="device_ip" class="inner form-control" id="device_ip" placeholder="Enter Device IP" />
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">ADD</button>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group"></div>
                                </div>
                            </form>
                            
                            <div class="card-body table-responsive">
                                <table class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Device Name</th>
                                            <th>Device ID</th>
                                            <th>Device IP</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php if(!empty($devices) && (count($devices) > 0)): ?>
                                        <?php $__currentLoopData = $devices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $device): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($key); ?></td>
                                                <td><?php echo e($device['device_name']); ?></td>
                                                <td><?php echo e($device['device_id']); ?></td>
                                                <td><?php echo e($device['device_ip']); ?></td>
                                                <td>
                                                    
                                                </td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="<?php echo e(route('attendance_delete_user', $employee['uid'])); ?>">Delete User</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/ncc_1/Modules/Attendance/Resources/views/add_device.blade.php ENDPATH**/ ?>