<!DOCTYPE html>
<html>

<head>
    <title>Supplier Payment</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p><?php echo e($user_info['address']); ?></p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">List of Supplier Payment</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                    <th style="text-align: center">SUPPLIER</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center"><?php echo e($from_date); ?> <strong>To</strong> <?php echo e($to_date); ?></td>

                                    <td style="text-align: center">
                                        <?php if($supplier_name != null): ?>
                                            <?php echo e($supplier_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 10%">PAYMENT DATE</th>
                                    <th style="text-align: center;width: 10%">PAYMENT NUMBER</th>
                                    <th style="text-align: center;width: 15%">SUPPLIER</th>
                                    <th style="text-align: center;width: 10%">PAID THROUGH</th>
                                    <th style="text-align: center;width: 12%">ACCOUNT INFORMATION</th>
                                    <th style="text-align: center;width: 10%">INVOICE NUMBER</th>
                                    <th style="text-align: center;width: 10%">AMOUNT</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i                  = 1;
                                    $total_amount       = 0;
                                ?>
                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td style="text-align: center;vertical-align: middle" rowspan="<?php echo e(collect($data[$key]['payment_entries'])->count()); ?>"><?php echo e($i); ?></td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="<?php echo e(collect($data[$key]['payment_entries'])->count()); ?>"><?php echo e(date('d-m-Y', strtotime($value['payments']['payment_date']))); ?></td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="<?php echo e(collect($data[$key]['payment_entries'])->count()); ?>"><?php echo e(str_pad($value['payments']['payment_number'], 6, "0", STR_PAD_LEFT)); ?></td>
                                    <td style="text-align: left;vertical-align: middle" rowspan="<?php echo e(collect($data[$key]['payment_entries'])->count()); ?>"><?php echo e($value['payments']['supplier_name']); ?></td>
                                    <td style="text-align: left;vertical-align: middle" rowspan="<?php echo e(collect($data[$key]['payment_entries'])->count()); ?>"><?php echo e($value['payments']['paid_through_account_name']); ?></td>
                                    <td style="text-align: left;vertical-align: middle" rowspan="<?php echo e(collect($data[$key]['payment_entries'])->count()); ?>"><?php echo e($value['payments']['account_information']); ?></td>

                                    <td style="text-align: center;"><?php echo e(str_pad($data[$key]['payment_entries'][0]['bill_number'], 6, "0", STR_PAD_LEFT)); ?></td>
                                    <td style="text-align: right;"><?php echo e($data[$key]['payment_entries'][0]['amount']); ?></td>
                                </tr>

                                <?php $__currentLoopData = $data[$key]['payment_entries']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($key1 != 0): ?>
                                <tr>
                                    <td style="text-align: center;"><?php echo e(str_pad($value1['bill_number'], 6, "0", STR_PAD_LEFT)); ?></td>
                                    <td style="text-align: right;"><?php echo e($value1['amount']); ?></td>
                                </tr>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                <?php
                                    $i++;
                                    $total_amount      = $total_amount + $value['payments']['amount'];
                                ?>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="7" style="text-align: right;">TOTAL</th>
                                    <th colspan="1" style="text-align: right;"><?php echo e($total_amount); ?></th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/ncc_1/Modules/Reports/Resources/views/supplier_payment_report_print.blade.php ENDPATH**/ ?>