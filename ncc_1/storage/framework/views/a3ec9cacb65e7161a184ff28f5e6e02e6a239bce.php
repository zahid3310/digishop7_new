

<?php $__env->startSection('title', 'List of Employees'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">List of Employees</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Attendance</a></li>
                                    <li class="breadcrumb-item active">List of Employees</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <?php if(Session::has('success')): ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <?php echo Session::get('success'); ?>

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <?php endif; ?>

                        <?php if(Session::has('unsuccess')): ?>
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            <?php echo Session::get('unsuccess'); ?>

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <?php endif; ?>

                        <?php if(Session::has('errors')): ?>
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <?php endif; ?>

                        <div class="card">
                            <button style="float: left" type="button" class="btn btn-success col-md-2">
                                <a style="color: black" href="<?php echo e(route('attendance_pull_data')); ?>">Get Data</a>
                            </button>
                            <hr>
                            <form method="post" action="<?php echo e(route('attendance_import_user_update')); ?>" enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                <div class="card-body table-responsive">
                                    <table class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                        <thead>
                                            <tr>
                                                <th style="width: 5%;text-align: center">SL#</th>
                                                <th style="text-align: center">Employee ID</th>
                                                <th style="text-align: center">Employee Name</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php if(!empty($employees) && (count($employees) > 0)): ?>
                                            <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td style="text-align: center"><?php echo e($i); ?></td>
                                                    <td>
                                                        <input class="form-control" type="text" name="employee_id[]" value="<?php echo e($employee['userid']); ?>">
                                                        <input class="form-control" type="hidden" name="old_employee_id[]" value="<?php echo e($employee['userid']); ?>">
                                                        <input class="form-control" type="hidden" name="uid[]" value="<?php echo e($employee['uid']); ?>">
                                                    </td>
                                                    <td>
                                                        <input class="form-control" type="text" name="employee_name[]" value="<?php echo e($employee['name']); ?>">
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>

                                    <div class="form-group row">
                                        <div class="col-md-5"></div>
                                        <div class="col-md-2">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Update Data</button>
                                        </div>
                                        <div class="col-md-5"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/ncc_1/Modules/Attendance/Resources/views/all_device_users.blade.php ENDPATH**/ ?>