

<?php $__env->startSection('title', 'Show'); ?>

<style>
    table,th {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 2px;
    }

    td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        border-bottom: 1px solid black;
        padding: 2px;
    }

    @page  {
        size: A4;
        page-break-after: always;
    }

    /*.color1 {
        background-color: #E2E2E2;
    }
    .color2 {
        background-color: #E2EAE7;
    }
    .color3 {
        background-color: #E0EAE9;
    }
    .color4 {
        background-color: #E1E0DD;
    }
    .color5 {
        background-color: #D6DFDE;
    }*/

    .color1 {
        background-color: white;
    }
    .color2 {
        background-color: white;
    }
    .color3 {
        background-color: white;
    }
    .color4 {
        background-color: white;
    }
    .color5 {
        background-color: white;
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Production</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Finished Goods</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="padding: 10px;padding-top: 0px" class="row">
                    <div style="padding-bottom: 10px" class="d-print-none col-md-12">
                        <div class="float-right">
                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <!-- <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="<?php echo e(url('public/'.userDetails()->logo)); ?>"> -->
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 27px;padding-top: 10px;font-weight: bold"><?php echo e($user_info['organization_name'] . ' ' . '(' . date('d/m/Y', strtotime($productions['date'])) .')'); ?></h2>
                                        <p style="line-height: 2;font-size: 20px;font-weight: bold" class="text-center"><?php echo e($productions->customer->name); ?></p>
                                    </div>
                                    <div style="text-align: right" class="col-md-2 col-sm-2 col-lg-2 col-2"></div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px"> <?php echo e($user_info['address']); ?> </p> -->
                                        <!-- <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">Mob: 01916555577, 01916555588 & 01916555599</p> -->
                                        <!-- <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 13px">Email: ratonandsons@gmail.com</p> -->
                                    </div>
                                </div>

                                <hr>

                                <div style="padding-top: 10px;padding-bottom: 20px">
                                    <table style="width: 100%">
                                        <thead class="theight">
                                            <tr>
                                                <th style="font-size: 18px;width: 5%;text-align: center">ক্রমিক নং</th>
                                                <th style="font-size: 18px;width: 20%;text-align: center">মালের বিবরণ</th>
                                                <th style="font-size: 18px;width: 10%;text-align: center">বস্তা</th>
                                                <th style="font-size: 18px;width: 10%;text-align: center">কেজি</th>
                                                <th style="font-size: 18px;width: 10%;text-align: center">দর</th>
                                                <th style="font-size: 18px;width: 10%;text-align: center">টাকা</th>
                                                <th style="font-size: 18px;width: 10%;text-align: center">বিক্রি</th>
                                            </tr>
                                        </thead>

                                        <?php if($entries->count() > 0): ?>

                                        <?php
                                            $total_amount       = 0;
                                            $total_weg_amount   = 0;
                                            $total_rice_amount  = 0;
                                        ?>

                                        <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="tr-height">
                                            <td style="text-align: center"><?php echo e($key + 1); ?></td>
                                            <td style="text-align: left"><?php echo e($value->productEntries->name); ?></td>
                                            <td style="text-align: center"><?php echo e($value->quantity); ?></td>
                                            <td style="text-align: center"><?php echo e($value->kg_quantity); ?></td>
                                            <td style="text-align: center"><?php echo e(number_format($value->rate,0,'.',',')); ?></td>
                                            <td style="text-align: center"><?php echo e(number_format(($value->rate*$value->quantity) + ($value->kg_rate*$value->kg_quantity),0,'.',',')); ?></td>
                                            <td style="text-align: center"><?php echo e(number_format($value->productEntries->sell_price,0,'.',',')); ?></td>
                                        </tr>

                                        <?php
                                            if ($value->productEntries->product_id != 2)
                                            {
                                                $total_weg_amount   = $total_weg_amount + ($value->rate*$value->quantity) + ($value->kg_rate*$value->kg_quantity);
                                            }

                                            if ($value->productEntries->product_id == 2)
                                            {
                                                $total_rice_amount   = $total_rice_amount + ($value->rate*$value->quantity) + ($value->kg_rate*$value->kg_quantity);
                                            }
                                        ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>

                                        <tr>
                                            <th colspan="1"></th>
                                            <th style="text-align: left" colspan="4">মোট ধান</th>
                                            <th style="text-align: left" colspan="2">
                                                <?php
                                                    $tot = 0;
                                                    foreach($productions->production->productionEntries as $val)
                                                    {
                                                        $tot = $tot + $val['quantity'];
                                                    }
                                                ?>

                                                <?php echo e(number_format($tot,0,'.',',') . ' ' .  'বস্তা'); ?>

                                            </th>
                                        </tr>

                                        <tr>
                                            <th colspan="1"></th>
                                            <th style="text-align: left" colspan="4">অয়েঃ এর মূল্য</th>
                                            <th style="text-align: left" colspan="2"><?php echo e($total_weg_amount); ?></th>
                                        </tr>

                                        <tr>
                                            <th colspan="1"></th>
                                            <th style="text-align: left" colspan="4">পার্টি পাবে</th>
                                            <th style="text-align: left" colspan="2"><?php echo e(number_format($total_weg_amount - $total_rice_amount,0,'.',',')); ?></th>
                                        </tr>

                                        <tr>
                                            <th colspan="1"></th>
                                            <th style="text-align: left" colspan="4">পার্টির পূর্বের পাওনা</th>
                                            <th style="text-align: left" colspan="2">0</th>
                                        </tr> 

                                        <tr>
                                            <th colspan="1"></th>
                                            <th style="text-align: left" colspan="4">পার্টির বর্তমান পাওনা</th>
                                            <th style="text-align: left" colspan="2"><?php echo e(number_format($total_weg_amount - $total_rice_amount,0,'.',',')); ?></th>
                                        </tr> 
                                    </table>
                                </div>

                                <div class="grid-container-1">

                                    <div class="item33"></div>  
                                    <div class="item55"><p class="signaturesection1" style="font-size: 18px;text-align: center"></p></div>

                                    <div class="item22" style="text-align: center;">
                                        <div>
                                            <p style="font-size: 18px"> </p>
                                        </div>
                                    </div>

                                    <div class="item44"></div>
                                    <div class="item1"><p class="signaturesection2" style="font-size: 18px;text-align: center"></p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/ncc_1/Modules/Productions/Resources/views/received_finished_goods/show.blade.php ENDPATH**/ ?>