

<?php $__env->startSection('title', 'Edit Salary'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Salary</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Others</a></li>
                                    <li class="breadcrumb-item active">Edit Salary</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

            					<form id="FormSubmit" action="<?php echo e(route('expenses_employee_salary_update', $find_expense['id'])); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					<?php echo e(csrf_field()); ?>


            						<div class="row">
                                        <input id="expense_category_id" name="expense_category_id" type="hidden" value="1">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Select Employee * </label>
                                                <select style="width: 83%" id="user_id" name="user_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10" required>
                                                   <option value="<?php echo e($find_expense['user_id']); ?>" selected><?php echo e($find_expense['user_name']); ?></option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-2 col-md-2 col-sm-2 col-2" data-toggle="modal" data-target="#myModal">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="col-sm-4 form-group">
                                            <label class="control-label">Salary Type *</label>
                                            <select style="cursor: pointer;" name="salary_type" class="form-control">
                                                <option <?php echo e($find_expense['salary_type'] == 0 ? 'selected' : ''); ?> value="0">Monthly Salary</option>
                                                <option <?php echo e($find_expense['salary_type'] == 1 ? 'selected' : ''); ?> value="1">Service Charge</option>
                                            </select>
                                        </div>

					                	<div class="col-sm-4">
					                        <div class="form-group">
					                            <label for="payment_date">Payment Date *</label>
					                            <input id="payment_date" name="payment_date" type="text" value="<?php echo e(date('d-m-Y', strtotime($find_expense['expense_date']))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
					                        </div>
					                    </div>

					                    <div class="col-sm-6">
					                        <div class="form-group">
					                            <label for="amount">Amount *</label>
					                            <input id="amount" name="amount" type="text" class="form-control" value="<?php echo e($find_expense['amount']); ?>" required>
					                        </div>
					                    </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="note">Note</label>
                                                <input id="note" name="note" type="text" class="form-control" value="<?php echo e($find_expense['note']); ?>">
                                            </div>
                                        </div>
				                	</div>

                                    <hr style="margin-top: 0px">

                                    <div class="form-group row">
                                        <div class="button-items col-md-12">
                                            <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('expenses_employee_salary_index')); ?>">Close</a></button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <hr>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">Salary List</h4>
                                
                                <div style="margin-right: 0px" class="row">
                                    <div class="col-lg-9 col-md-6 col-sm-4 col-4"></div>
                                    <div class="col-lg-1 col-md-2 col-sm-4 col-4">Search : </div>
                                    <div class="col-lg-2 col-md-4 col-sm-4 col-4">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Employee Name</th>
                                            <th>Payment Date</th>
                                            <th>Salary Type</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="expense_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

    		</div>
		</div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Employee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control">
                        </div>
                    </div>

                    <input id="contact_type" name="contact_type" type="hidden" value="2" class="form-control">
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url        = $('.site_url').val();

            $("#user_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0)
                    {
                        var type = 'Customer';
                    }

                    if (result['contact_type'] == 1)
                    {
                        var type = 'Supplier';
                    }

                    if (result['contact_type'] == 2)
                    {
                        var type = 'Employee';
                    }

                    if (result['contact_type'] == 2)
                    {
                        return result['text'] + ' | ' + type;
                    }
                },
            });

            $.get(site_url + '/expenses-employee-salary/expense/employee-salary/list/load', function(data){

                var expense_list = '';
                $.each(data, function(i, expense_data)
                {   
                    var serial      = parseFloat(i) + 1;
                    var edit_url    = site_url + '/expenses-employee-salary/edit/' + expense_data.id;

                    if (expense_data.salary_type == 0)
                    {
                        var salaryType  = 'Monthly Salary';
                    }

                    if (expense_data.salary_type == 1)
                    {
                        var salaryType  = 'Service Charge';
                    }

                    expense_list += '<tr>' +
                                        '<td>' +
                                            serial +
                                        '</td>' +
                                        '<td>' +
                                           expense_data.user_name +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(expense_data.expense_date) +
                                        '</td>' +
                                        '<td>' +
                                           salaryType +
                                        '</td>' +
                                        '<td>' +
                                           expense_data.amount +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '<?php if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3): ?>' +
                                                    '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                    '<?php endif; ?>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                });

                $("#expense_list").empty();
                $("#expense_list").append(expense_list);
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var search_text     = $('#searchPayment').val();
            var site_url        = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/expenses-employee-salary/expense/expense/employee-salary/search/list/' + search_text, function(data){

                var expense_list = '';
                $.each(data, function(i, expense_data)
                {   
                    var serial      = parseFloat(i) + 1;
                    var edit_url    = site_url + '/expenses-employee-salary/edit/' + expense_data.id;

                    if (expense_data.salary_type == 0)
                    {
                        var salaryType  = 'Monthly Salary';
                    }

                    if (expense_data.salary_type == 1)
                    {
                        var salaryType  = 'Service Charge';
                    }

                    expense_list += '<tr>' +
                                        '<td>' +
                                            serial +
                                        '</td>' +
                                        '<td>' +
                                           expense_data.user_name +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(expense_data.expense_date) +
                                        '</td>' +
                                        '<td>' +
                                           salaryType +
                                        '</td>' +
                                        '<td>' +
                                           expense_data.amount +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '<?php if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3): ?>' +
                                                    '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                    '<?php endif; ?>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                });

                $("#expense_list").empty();
                $("#expense_list").append(expense_list);
            });
        }
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = $("#contact_type").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
    
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '<?php echo e(csrf_token()); ?>' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                        }
                        
                        $("#user_id").empty();
                        $('#user_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');

                        $("#customer_name").val('');
                        $("#mobile_number").val('');
                        $("#address").val('');
                    }
        
                });
            }
        });    
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/ncc_1/Modules/Expenses/Resources/views/employee_salary_edit.blade.php ENDPATH**/ ?>