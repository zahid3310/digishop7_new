<!DOCTYPE html >
<html>

<head>
    <meta charset="utf-8">
    <title>Return Challan</title>
    <link href="https://fonts.maateen.me/bangla/font.css" rel="stylesheet">

    <style>
        *{margin:0;padding:0;outline:0}

        body {
            font-size:14px;
            line-height:18px;
            color:#000;
            height:292mm;
            width: 203mm;/*297*/
            margin:0 auto;
            font-family: 'Bangla', Arial, sans-serif !important;  
        }

        table,th {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 10px;
        }

        td {
            border-left: 1px solid black;
            border-right: 1px solid black;
            padding: 10px;
        }

        .bg_templete{
            background:url(images/bg3.png) no-repeat;
            background-size: 100%;
            -webkit-print-color-adjust: exact;
        }

        .templete{
            width:90%;
            background:none;
            margin:0 auto;
        }
        
        .clear{
            overflow:hidden;
        }

        .text-center{
            text-align: center;
        }

        .text-end{
            text-align: end;
        }

        .item2 {
            grid-area: com_logo;
        }

        .item3 {
            grid-area: com_info;
        }

        .item4 {
            grid-area: other_info;
        }

        .grid-container {
            display: grid;
            grid-template-areas: "com_logo com_info other_info ";
            /*padding-top: 10px;*/
        }

        .grid-container div {
            text-align: center;
        }

        .grid-container-aline-end {
            text-align: end !important;
        }

        .grid-container-aline-start {
            text-align: start !important;
        }

        .com_logo_img {
            height: 80px;
            width: 80px;
        }

        .tr-height{
            padding: 10px;
        }

        .item11 { grid-area: img1; }
        .item21 { grid-area: text1; }
        .item31 { grid-area: img2; }
        .item41 { grid-area: text2; }
        .item51 { grid-area: img3; }
        .item61 { grid-area: text3; }
        .item71 { grid-area: img4; }
        .item81 { grid-area: text4; }


        .grid-container-1 {
          display: grid;
          grid-template-areas:
            'img1 img2 img3 img4'
            'text1 text2 text3 text4';
          grid-gap: 10px;
         
        }

        .grid-container-1 > div {
         
          text-align: center;

        }

        .signaturesection1 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 45px;
        }

        .signaturesection2 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 45px;
        }

        @page  {
            size: A4;
            page-break-after: always;
        }
    </style>
</head>

<body>
    <div class="bg_templete clear">

        <div class="templete clear">

            <br>
            <br>
            <p style="line-height: 2;text-align: center"><span style="border: 1px solid gray;padding: 5px;border-radius: 13px;font-size: 25px">রিটার্ন চালান</span></p>
            <div class="grid-container">
                <div class="item2 grid-container-aline-start">
                    <img class="com_logo_img" src="<?php echo e(url('public/'.userDetails()->logo)); ?>"alt="qr sample"
                    />
                </div>

                <div class="item3">
                    <h2 style="line-height: 1;font-size: 60px">রংধনু ট্রেডার্স</h2>
                    <h2 style="line-height: 1;font-size: 30px">টাইলস এন্ড স্যানিটারী</h2>
                    <h3 style="line-height: 1;font-size: 21px; text-align: center;">প্রো: মোঃ তোফাজ্জল হোসেন খান</h3>
                    
                </div>

                <div class="item4 grid-container-aline-end">

                    <p style="padding-right: 25px;line-height: 1;font-size: 28px">ডিলার:</p>
                    <p style="line-height: 1;font-size: 15px;">মীর সিরামিক লিঃ</p>
                    <p style="line-height: 1;font-size: 15px;">শেলটেক সিরামিক লিঃ</p>
                    <p style="line-height: 1;font-size: 15px;">ফু ওয়াং সিরামিক লিঃ</p>
                    <p style="line-height: 1;font-size: 15px;">সাউথইষ্ট সিরামিক লিঃ</p>
                    <p style="line-height: 1;font-size: 11px;">বাংলাদেশ হার্ডল্যান্ড সিরামিক </p>
                    
                </div>
            </div>

            
            <p style="line-height: 1;font-size: 20px; text-align: center;">সকল প্রকার দেশী-বিদেশী টাইলস, স্যানিটারী সামগ্রী বিক্রেতা ও সরবরাহকারী</p>

            <div style="padding-bottom: 10px" class="text-center">
                <h4 style="line-height: 1;font-size: 20px">মাহবুব মোল্লা কনকর্ড টাওয়ার, জেলখানার মোড়, নরসিংদী।</h4>
                <p style="line-height: 1;font-size: 20px">মোবাইল: ০১৭২৯-১৬৪৮৭৯, ০১৭৪৭-২৯৬২৬৮, ম্যানেজার: ০১৭৯৫-৮৫১২৬৭</p>
            </div>

            <hr>

            <div style="font-size: 18px;padding-top: 10px;line-height: 1.5">
                <strong>Memo No &nbsp;:  </strong><?php echo e($sales_return['sales_return_number']); ?><strong style="float: right;">Date : <span><?php echo e(date('d-m-Y', strtotime($sales_return['sales_return_date']))); ?></span></strong>
            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong><?php echo e($sales_return['customer_name'] != null ? $sales_return['customer_name'] : $sales_return['contact_name']); ?>

            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Address &nbsp;&nbsp;: </strong><?php echo e($sales_return['customer_address'] != null ? $sales_return['customer_address'] : $sales_return['contact_address']); ?>

            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Phone &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong><?php echo e($sales_return['customer_phone'] != null ? $sales_return['customer_phone'] : $sales_return['contact_phone']); ?>

            </div>

            <div style="padding-top: 10px;padding-bottom: 20px">
                <table style="width: 100%">
                    <tr>
                        <th style="font-size: 18px">নং</th>
                        <th style="font-size: 18px">মালের বিবরণ</th>
                        <th style="font-size: 18px">বর্গফুট</th>
                        <th style="font-size: 18px">কার্টুন</th>
                        <th style="font-size: 18px">পিছ</th>
                    </tr>

                    <?php if($entries->count() > 0): ?>

                    <?php
                        $total_stf   = 0;
                        $total_cart  = 0;
                        $total_pcs   = 0;
                    ?>

                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                        if ($value['type'] == 1)
                        {
                            $total_stf   = $total_stf + $value['quantity'];
                            $total_cart  = $total_cart + $value['cartoon'];
                            $total_pcs   = $total_pcs + $value['pcs'];
                        }

                        $variation_name = ProductVariationName($value['product_entry_id']);

                        if ($value['product_code'] != null)
                        {
                            $productCode  = ' - '.$value['product_code'];
                        }
                        else
                        {
                            $productCode  = '';
                        }

                        if ($value['brand_name'] != null)
                        {
                            $brandName  = ' - '.$value['brand_name'];
                        }
                        else
                        {
                            $brandName  = '';
                        }

                        if (($value['height'] != null) && ($value['width'] != null))
                        {
                            $dimension  = ' - '.$value['height'] . ' X ' . $value['width'];
                        }
                        else
                        {
                            $dimension  = '';
                        }

                        if ($value['unit_name'] != null)
                        {
                            $unit  = ' '.$value['unit_name'];
                        }
                        else
                        {
                            $unit  = '';
                        }
                    ?>

                    <tr class="tr-height">
                        <td style="text-align: center"><?php echo e($key + 1); ?></td>
                        <td style="padding-left: 30px"><?php echo e($value['product_entry_name']); ?> <br><?php echo e($value['product_name'] . $productCode . $brandName . $dimension); ?> <?php echo e($variation_name != null ? ' - ' . $variation_name : ''); ?></td>

                        <?php if($value['type'] == 1): ?>
                        <td style="text-align: center"><?php echo e($value['quantity']); ?></td>
                        <td style="text-align: center"><?php echo e($value['cartoon']); ?></td>
                        <td style="text-align: center"><?php echo e($value['pcs']); ?></td>
                        <?php else: ?>
                        <td style="text-align: center"></td>
                        <td style="text-align: center"><?php echo e($value['quantity'] . $unit); ?></td>
                        <td style="text-align: center"></td>
                        <?php endif; ?>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

                    <tr>
                        <th style="text-align: right" colspan="2">Total </th>
                        <th style="text-align: center"><?php echo e($total_stf); ?></th>
                        <th style="text-align: center"><?php echo e($total_cart); ?></th>
                        <th style="text-align: center"><?php echo e($total_pcs); ?></th>
                    </tr>
                </table>
            </div>

            <div class="grid-container-1">
                <div class="item11"></div>

                <div class="item21">
                    <p class="signaturesection1" style="font-size: 18px">ক্রেতার স্বাক্ষর</p>
                </div>

                <div class="item31"></div>  

                <div class="item41">
                    <p class="signaturesection1" style="font-size: 18px">ড্রাইভার স্বাক্ষর</p>
                </div>

                <div class="item51" style="font-size: 18px"></div>

                <div class="item61" style="font-size: 18px">
                    <p class="signaturesection2" style="font-size: 18px">স্টোর কিপার স্বাক্ষর</p>
                </div>

                <div class="item71"></div>

                <div class="item81">
                    <p class="signaturesection2" style="font-size: 18px">বিক্রেতার স্বাক্ষর</p>
                </div>  
            </div>

            <br>

            <div class="row">

                <div class="col-md-4"></div> 

                <div class="col-md-4" style="text-align: center;">
                    <h3 style="font-size: 18px">শুক্রবার দোকান বন্ধ। </h3>
                    <div>
                        <p style="font-size: 18px">আল্লাহ কোন জাতির ভাগ্যের অবস্থার পরিবর্তন করেন না </p>
                        <p style="font-size: 18px">যতক্ষণ না সে তার অবস্থার পরিবর্তন করে।  আল-হাদীস </p>
                    </div>
                </div>

                <div class="col-md-4"></div>
            </div>
        </div>
    </div>
</body>

</html><?php /**PATH /home/digishop7/public_html/ncc_1/Modules/SalesReturn/Resources/views/challan.blade.php ENDPATH**/ ?>