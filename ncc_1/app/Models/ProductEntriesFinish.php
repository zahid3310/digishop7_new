<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ProductEntriesFinish extends Model
{  
    protected $table = "product_entries_finish";

    public function productEntries()
    {
        return $this->belongsTo('App\Models\ProductEntries','product_entry_id');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Stores','store_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }
}
