@extends('layouts.app')

@section('title', 'Create Doctor Schedule')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Create Doctor Schedule</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Doctor Appointment</a></li>
                                    <li class="breadcrumb-item active">Create Doctor Schedule</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('doctor_schedule_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label class="col-form-label">Select Doctor * </label>
                                        <select id="doctor_id" name="doctor_id" class="form-control" required>
                                           <option value="1">--Walk In Doctor--</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="schedule_date" class="col-form-label">Schedule Date *</label>
                                        <input id="schedule_date" name="schedule_date" type="text" value="{{ Session::has('invoice_date') ? date('d-m-Y', strtotime(Session::get('invoice_date'))) : date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                    </div>

                                    


                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="from_time" class="col-form-label">From Time *</label>
                                        <input type="text" name="from_time" class="inner form-control" id="buying_price_0" placeholder="Example : 0:00 AM/PM"  required />
                                    </div>


                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="to_time" class="col-form-label">To Time *</label>
                                        <input type="text" name="to_time" class="inner form-control" id="buying_price_0" placeholder="Example : 0:00 AM/PM" required />
                                    </div>


                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-8 col-md-8 col-sm-8 col-8 col-form-label">Status</label>
                                        <div style="padding-right: 0px;padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12">
                                            <select style="cursor: pointer" class="form-control" name="status">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>

                                   
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('products_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')
@endsection
