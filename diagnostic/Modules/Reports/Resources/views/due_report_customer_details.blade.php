<!DOCTYPE html>
<html>

<head>
    <title>Customer Ledger Details</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>  
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: center;line-height: .8">
                        <p style="font-size: 40px"><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p style="font-size: 20px">{{ $user_info['address'] }}</p>
                        <p style="font-size: 20px">{{ $user_info['contact_number'] }}</p>
                        <p style="font-size: 20px">{{ $user_info['contact_email'] }}</p>
                        <p style="font-size: 14px;text-align: right">{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Customer Ledger Details</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center;font-size: 12px !important">Date From</th>
                                    <th style="text-align: center;font-size: 12px !important">Customer Name</th>
                                    <th style="text-align: center;font-size: 12px !important">Customer Phone</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center;font-size: 12px !important">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                    
                                    <td style="text-align: center;font-size: 12px !important">
                                        @if($customer_name != null)
                                            {{ $customer_name['name'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                    
                                    <td style="text-align: center;font-size: 12px !important">
                                        @if($customer_name != null)
                                            {{ $customer_name['phone'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >

                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">Date</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">Details</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">Transaction#</th>
                                    <th style="text-align: center;width: 9%" colspan="5">Description</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom" rowspan="2">Discount On Total Amount</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom" rowspan="2">Ship/Cost</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom" rowspan="2">Paid Through</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom" rowspan="2">Receivable</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom" rowspan="2">Received</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom" rowspan="2">Balance</th>
                                </tr>

                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 10%;vertical-align: bottom">Item Name</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">Qty.</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">U/Price</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">T/Price</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">Discount</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr>
                                    <td style="text-align: right;font-size: 12px !important" colspan="10"><strong>Opening Balance</strong></a>
                                    </td>
                                    <td style="text-align: right;font-size: 12px !important"><strong>{{ $opening_balance }}</strong></td>
                                    <td style="text-align: center;font-size: 12px !important"></td>
                                    <td style="text-align: right;font-size: 12px !important"><strong>{{ $opening_balance }}</strong></td>
                                </tr>

                                <?php
                                    $i           = 1;
                                    $sub_total   = $opening_balance;
                                ?>
                                @if($data->count() > 0)
                                @foreach($data as $key => $value)

                                <?php
                                    if ($value['type'] == 0)
                                    {
                                        $debit  = $value['amount'];
                                        $credit = 0;
                                    }
                                    else
                                    {
                                        $credit = $value['amount'];
                                        $debit  = 0;
                                    }

                                    if ($value['account_head'] == 'customer-advance-adjustment')
                                    {
                                        $sub_total      = $sub_total + $debit + $credit;
                                    }
                                    else
                                    {
                                        $sub_total      = $sub_total + $debit - $credit;
                                    }

                                    if ($value['account_head'] == 'sales')
                                    {
                                        $trans_number   = 'INV - '.str_pad($value->invoice->invoice_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'previoue-due-collection')
                                    {
                                        $trans_number   = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'sales-return')
                                    {
                                        $trans_number   = 'SR - '.str_pad($value->salesReturn->sales_return_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'payment')
                                    {
                                        $trans_number   = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'customer-opening-balance')
                                    {
                                        $trans_number   = '';
                                    }
                                    elseif ($value['account_head'] == 'customer-advance-adjustment')
                                    {
                                        $trans_number   = '';
                                    }
                                    elseif ($value['account_head'] == 'customer-settlement')
                                    {
                                        $trans_number   = '';
                                        $value['note']  = 'কাস্টমার সেটেলমেন্ট';
                                    }
                                    elseif ($value['account_head'] == 'sales-return-payment')
                                    {
                                        $trans_number   = '';
                                        $value['note']  = 'বিক্রয় রিটার্ন বাবদ প্রেরন';
                                    }
                                    else
                                    {
                                        $trans_number   = '';
                                    }

                                    if (($value['note'] == 'বিক্রয় বাবদ গ্রহণযোগ্য') || ($value['note'] == 'পূর্বের অগ্রিম থেকে সমন্বয়'))
                                    {
                                        if ($value['invoice_id'] != 0)
                                        {   
                                            $entries = \App\Models\InvoiceEntries::where('invoice_id', $value['invoice_id'])->get();
                                        }
                                        else
                                        {
                                            $entries = collect();
                                        }
                                    }
                                    elseif ($value['note'] == 'বিক্রয় রিটার্ন বাবদ প্রদান')
                                    {   
                                        if ($value['sales_return_id'] != 0)
                                        {
                                            $entries = \App\Models\SalesReturnEntries::where('sales_return_id', $value['sales_return_id'])->get();
                                        }
                                        else
                                        {
                                            $entries = collect();
                                        }
                                    }
                                    else
                                    {
                                        $entries = collect();
                                    }    
                                ?>

                                @if($value['account_head'] != 'customer-advance-adjustment')

                                    <tr>
                                        <td style="text-align: center;font-size: 12px !important;vertical-align: middle" @if($entries->count() > 0) rowspan="{{ $entries->count() > 0 ? $entries->count() : 1 }}" @else rowspan="1" @endif>{{ date('d-m-Y', strtotime($value['date'])) }}</a>
                                        </td>

                                        <td style="text-align: center;font-size: 12px !important;vertical-align: middle" @if($entries->count() > 0) rowspan="{{ $entries->count() > 0 ? $entries->count() : 1 }}" @else rowspan="1" @endif>{{ $value['note'] }}</td>
                                        <td style="text-align: center;font-size: 12px !important;vertical-align: middle" @if($entries->count() > 0) rowspan="{{ $entries->count() > 0 ? $entries->count() : 1 }}" @else rowspan="1" @endif>{{ $trans_number }}</td>


                                        @if(($value['note'] == 'বিক্রয় বাবদ গ্রহণযোগ্য') || ($value['note'] == 'পূর্বের অগ্রিম থেকে সমন্বয়') || ($value['note'] == 'বিক্রয় রিটার্ন বাবদ প্রদান'))
                                        @if($entries->count() > 0)
                                        @foreach($entries as $key1 => $detail_data1)
                                        @if($key1 == 0)
                                        <td style="text-align: left;font-size: 12px !important;">{{ $detail_data1->productEntries->name }}</td>
                                        <td style="text-align: center;font-size: 12px !important;">{{ $detail_data1->quantity }}</td>
                                        <td style="text-align: center;font-size: 12px !important;">{{ $detail_data1->rate }}</td>
                                        <td style="text-align: center;font-size: 12px !important;">{{ $detail_data1->total_amount }}</td>
                                        <td style="text-align: center;font-size: 12px !important;">{{ $detail_data1->discount_type == 1 ? $detail_data1->discount_amount : (($detail_data1->discount_amount*$detail_data1->quantity*$detail_data1->rate)/100) }}</td>
                                        @endif
                                        @endforeach
                                        @else
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        @endif

                                        @if($entries->count() > 0 && $entries->count() == 0)
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        @endif
                                        @else
                                        <td colspan="5"></td>
                                        @endif
                                        
                                        @if(($value['note'] == 'বিক্রয় বাবদ গ্রহণযোগ্য') || ($value['note'] == 'পূর্বের অগ্রিম থেকে সমন্বয়') || ($value['note'] == 'বিক্রয় রিটার্ন বাবদ প্রদান'))
                                        <td style="text-align: center;font-size: 12px !important;vertical-align: middle" @if($entries->count() > 0) rowspan="{{ $entries->count() > 0 ? $entries->count() : 1 }}" @else rowspan="1" @endif>{{ isset($value->invoice->total_discount_amount) ? $value->invoice->total_discount_amount : '' }}</td>
                                        @else
                                        <td style="text-align: center;font-size: 12px !important;vertical-align: middle" @if($entries->count() > 0) rowspan="{{ $entries->count() > 0 ? $entries->count() : 1 }}" @else rowspan="1" @endif></td>
                                        @endif
                                        
                                        @if(($value['note'] == 'বিক্রয় বাবদ গ্রহণযোগ্য') || ($value['note'] == 'পূর্বের অগ্রিম থেকে সমন্বয়') || ($value['note'] == 'বিক্রয় রিটার্ন বাবদ প্রদান'))
                                        <td style="text-align: center;font-size: 12px !important;vertical-align: middle" @if($entries->count() > 0) rowspan="{{ $entries->count() > 0 ? $entries->count() : 1 }}" @else rowspan="1" @endif>{{ isset($value->invoice->shipping_cost) ? $value->invoice->shipping_cost : '' }}</td>
                                        @else
                                        <td style="text-align: center;font-size: 12px !important;vertical-align: middle" @if($entries->count() > 0) rowspan="{{ $entries->count() > 0 ? $entries->count() : 1 }}" @else rowspan="1" @endif></td>
                                        @endif
                                        <td style="text-align: left;font-size: 12px !important;vertical-align: middle" @if($entries->count() > 0) rowspan="{{ $entries->count() > 0 ? $entries->count() : 1 }}" @else rowspan="1" @endif>
                                            <?php
                                                if ($value['note'] != 'বিক্রয় বাবদ গ্রহণযোগ্য') 
                                                {
                                                    if($value->paid_through != null)
                                                    {
                                                        echo $value->paidThroughAccounts->name;
                                                    }
                                                    else
                                                    {
                                                        $pMethode = customerPaymentMethode($value->customer_id, $value->invoice_id, $value->created_at);

                                                        echo $pMethode['name'];
                                                    }
                                                }
                                            ?>
                                        </td>
                                        <td style="text-align: right;font-size: 12px !important;vertical-align: middle" @if($entries->count() > 0) rowspan="{{ $entries->count() > 0 ? $entries->count() : 1 }}" @else rowspan="1" @endif>{{ number_format($debit,2,'.',',') }}</td>
                                        
                                        @if(isset($data[$key - 1]) && ($data[$key - 1]['account_head'] == 'customer-advance-adjustment'))
                                        <td style="text-align: right;font-size: 12px !important;vertical-align: middle" @if($entries->count() > 0) rowspan="{{ $entries->count() > 0 ? $entries->count() : 1 }}" @else rowspan="1" @endif>{{ number_format($credit - $data[$key - 1]['amount'],2,'.',',') }}</td>
                                        @else
                                        <td style="text-align: right;font-size: 12px !important;vertical-align: middle" @if($entries->count() > 0) rowspan="{{ $entries->count() > 0 ? $entries->count() : 1 }}" @else rowspan="1" @endif>{{ number_format($credit,2,'.',',') }}</td>
                                        @endif
                                         <td style="text-align: right;font-size: 12px !important;vertical-align: middle" @if($entries->count() > 0) rowspan="{{ $entries->count() > 0 ? $entries->count() : 1 }}" @else rowspan="1" @endif>{{ number_format($sub_total,2,'.',',') }}</td>
                                    </tr>

                                    @if(($value['note'] == 'বিক্রয় বাবদ গ্রহণযোগ্য') || ($value['note'] == 'পূর্বের অগ্রিম থেকে সমন্বয়') || ($value['note'] == 'বিক্রয় রিটার্ন বাবদ প্রদান'))
                                    @if($entries->count() > 0)
                                    @foreach($entries as $key2 => $detail_data2)
                                    @if($key2 != 0)
                                    <tr>
                                        <td style="text-align: left;font-size: 12px !important;">{{ $detail_data2->productEntries->name }}</td>
                                        <td style="text-align: center;font-size: 12px !important;">{{ $detail_data2->quantity }}</td>
                                        <td style="text-align: center;font-size: 12px !important;">{{ $detail_data2->rate }}</td>
                                        <td style="text-align: center;font-size: 12px !important;">{{ $detail_data2->total_amount }}</td>
                                        <td style="text-align: center;font-size: 12px !important;">{{ $detail_data2->discount_type == 1 ? $detail_data2->discount_amount : (($detail_data2->discount_amount*$detail_data2->quantity*$detail_data2->rate)/100) }}</td>
                                    </tr>
                                    @endif
                                    @endforeach
                                    @endif
                                    @endif
                                 
                                <?php $i++; ?>

                                @endif
                                @endforeach
                                @endif
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="13" style="text-align: right;font-size: 12px !important">TOTAL</th>
                                    <th colspan="1" style="text-align: right;font-size: 12px !important">{{ number_format($sub_total,2,'.',',') }}</th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>