<!DOCTYPE html>
<html>

<head>
    <title>General Ledger</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">General Ledger</h6>
                    </div>

                    <div class="ibox-content">
                        <!-- Header Start -->
                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Header End -->

                        <!-- Sales Statement Start -->
                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 2%">SL</th>
                                    <th style="text-align: center;width: 10%">ACCOUNT HEAD</th>
                                    <th style="text-align: center;width: 5%">AMOUNT</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $opening_income  = $opening_result['sales'] - $opening_result['sales_return'] - $opening_result['received'] - $opening_result['customer_advance_adj'] + $opening_result['customer_advance'];
                                    $opening_expense = $opening_result['purchase'] - $opening_result['purchase_return'] - $opening_result['paid'] - $opening_result['supplier_advance_adj'] + $opening_result['supplier_advance'];
                                ?>

                                <tr>
                                    <th colspan="2" style="text-align: right;">Opening Balance</th>
                                    <th style="text-align: right;">{{ $opening_income - $opening_expense }}</th>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">1</td>
                                    <td style="text-align: left;">Sales</td>
                                    <td style="text-align: right;">{{ $result['sales'] }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">2</td>
                                    <td style="text-align: left;">Sales Return</td>
                                    <td style="text-align: right;">{{ $result['sales_return'] }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">3</td>
                                    <td style="text-align: left;">Amount Received From Sales</td>
                                    <td style="text-align: right;">{{ $result['received'] - $result['customer_advance_adj'] }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">4</td>
                                    <td style="text-align: left;">Customer Advance Adjustment</td>
                                    <td style="text-align: right;">{{ $result['customer_advance_adj'] }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">5</td>
                                    <td style="text-align: left;">Customer Advance</td>
                                    <td style="text-align: right;">{{ $result['customer_advance'] }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">6</td>
                                    <td style="text-align: left;">Purchase</td>
                                    <td style="text-align: right;">{{ $result['purchase'] }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">7</td>
                                    <td style="text-align: left;">Purchase Return</td>
                                    <td style="text-align: right;">{{ $result['purchase_return'] }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">8</td>
                                    <td style="text-align: left;">Amount Paid For Purchase</td>
                                    <td style="text-align: right;">{{ $result['paid'] - $result['supplier_advance_adj'] }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">9</td>
                                    <td style="text-align: left;">Supplier Advance Adjustment</td>
                                    <td style="text-align: right;">{{ $result['supplier_advance_adj'] }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">10</td>
                                    <td style="text-align: left;">Supplier Advance</td>
                                    <td style="text-align: right;">{{ $result['supplier_advance'] }}</td>
                                </tr>

                                <?php
                                    $income  = $result['sales'] - $result['sales_return'] - $result['received'] - $result['customer_advance_adj'] + $result['customer_advance'];
                                    $expense = $result['purchase'] - $result['purchase_return'] - $result['paid'] - $result['supplier_advance_adj'] + $result['supplier_advance'];
                                ?>

                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="2" style="text-align: right;">Balance</th>
                                    <th style="text-align: right;">{{ $income - $expense }}</th>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- Sales Statement End -->

                        <!-- Footer Start -->
                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>