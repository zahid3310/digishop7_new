<?php

namespace Modules\Payroll\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\SalaryGrades;
use App\Models\SalaryStatements;
use App\Models\SalaryStatementTrns;
use App\Models\Customers;
use App\Models\MonthlySalarySheets;
use DB;
use Response;

class ProcessMonthlySalaryController extends Controller
{
    public function index()
    {
        $salary_statements = MonthlySalarySheets::select('monthly_salary_sheets.*','customers.name','customers.designation','departments.name as department_name')
                                ->leftjoin('customers','customers.id','monthly_salary_sheets.employee_id')
                                ->leftjoin('departments','departments.id','customers.department_id')
                                ->get();




        return view('payroll::ProcessMonthlySalary.index', compact('salary_statements'));
    }

    public function create()
    {
        $grades = SalaryGrades::orderBy('position', 'ASC')->get();

        return view('payroll::ProcessMonthlySalary.create', compact('grades'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'employee_id.*'   => 'required|integer',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            if(isset($data['employee_id'])) 
            {
                foreach ($data['employee_id'] as $key => $value)
                {   
                    $check = MonthlySalarySheets::where('employee_id', $value)->where('month', $data['month'])->where('year', $data['year'])->count();
    
                    if (($data['check_box_id'][$key] == 1) && ($check == 0))
                    {
                        $monthly_salary_sheets[] = [
                            'employee_id'       => $value,
                            'statement_id'      => $data['statement_id'][$key],
                            'month'             => $data['month'],
                            'year'              => $data['year'],
                            'generate_date'     => date('Y-m-d'),
                            'total_attendance'  => $data['total_attendance'][$key],
                            'advance'           => $data['advance'][$key],
                            'net_payable'       => $data['net_payable'][$key],
                            'paid'              => 0,
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }
                }
 
                if (isset($monthly_salary_sheets))
                {
                    DB::table('monthly_salary_sheets')->insert($monthly_salary_sheets);
   
                    DB::commit();
                    return back()->with("success","Salary Sheet Created Successfully !!");
                }
                else
                {
                    DB::rollback();
                    return back()->with("unsuccess","Salary Sheet Already Created !!");
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","No Employee Selected !!");
            }
        }catch (\Exception $exception){
            DB::rollback($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        return view('payroll::show');
    }

    public function edit($id)
    {
        $find_statement = SalaryStatements::find($id);
        $grades         = SalaryGrades::orderBy('position', 'ASC')->get();

        return view('payroll::SalaryStatements.edit', compact('find_statement', 'grades'));
    }

    public function delete($id)
    {
        $find_statement = MonthlySalarySheets::where('id', $id)->delete();

        return back()->with('success', 'Deletted Successfully !!');
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'employee_id'   => 'required|integer',
            'grade_id'      => 'nullable|integer',
            'position'      => 'nullable|integer',
            'basic_salary'  => 'nullable|numeric',
            'house_rent'    => 'nullable|numeric',
            'medical'       => 'nullable|numeric',
            'convence'      => 'nullable|numeric',
            'food'          => 'nullable|numeric',
            'mobile_bill'   => 'nullable|numeric',
            'others'        => 'nullable|numeric',
            'gross'         => 'nullable|numeric',
            'tax'           => 'nullable|numeric',
            'bonus'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $salary_statement                = SalaryStatements::find($id);
            $salary_statement->employee_id   = $data['employee_id'];
            $salary_statement->grade_id      = $data['grade_id'];
            $salary_statement->position      = $data['position'];
            $salary_statement->basic         = $data['basic_salary'];
            $salary_statement->house_rent    = $data['house_rent'];
            $salary_statement->medical       = $data['medical'];
            $salary_statement->convence      = $data['convence'];
            $salary_statement->food          = $data['food'];
            $salary_statement->mobile        = $data['mobile_bill'];
            $salary_statement->others        = $data['others'];
            $salary_statement->gross         = $data['gross'];
            $salary_statement->pf_amount     = $data['pf_amount'];
            $salary_statement->tax_amount    = $data['tax'];
            $salary_statement->bonus_amount  = $data['bonus'];
            $salary_statement->updated_by    = $user_id;

            if ($salary_statement->save())
            {   
                $find_employee                        = Customers::find($data['employee_id']);
                $salary_statement_trns                = new SalaryStatementTrns;
                $salary_statement_trns->employee_id   = $data['employee_id'];
                $salary_statement_trns->designation   = $find_employee->designation;
                $salary_statement_trns->statement_date= date('Y-m-d');
                $salary_statement_trns->grade_id      = $data['grade_id'];
                $salary_statement_trns->position      = $data['position'];
                $salary_statement_trns->basic         = $data['basic_salary'];
                $salary_statement_trns->house_rent    = $data['house_rent'];
                $salary_statement_trns->medical       = $data['medical'];
                $salary_statement_trns->convence      = $data['convence'];
                $salary_statement_trns->food          = $data['food'];
                $salary_statement_trns->mobile        = $data['mobile_bill'];
                $salary_statement_trns->others        = $data['others'];
                $salary_statement_trns->gross         = $data['gross'];
                $salary_statement_trns->pf_amount     = $data['pf_amount'];
                $salary_statement_trns->tax_amount    = $data['tax'];
                $salary_statement_trns->bonus_amount  = $data['bonus'];
                $salary_statement_trns->created_by    = $user_id;
                $salary_statement_trns->save();

                DB::commit();
                return redirect()->route('salary_statements_index')->with("success","Salary Statement Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function gradeDetails($grade_id)
    {
        $data = SalaryGrades::find($grade_id);

        return Response::json($data);
    }

    public function processMonthlySalary()
    {
        $data = SalaryStatements::leftjoin('customers', 'customers.id', 'salary_statements.employee_id')
                                ->leftjoin('salary_grades', 'salary_grades.id', 'salary_statements.grade_id')
                                ->leftjoin('departments', 'departments.id', 'customers.department_id')
                                ->selectRaw('salary_statements.*, 
                                            customers.name as employee_name,
                                            customers.designation as designation,
                                            departments.name as department,
                                            salary_grades.name as grade_name'
                                            )
                                ->get();

        return Response::json($data);
    }
}