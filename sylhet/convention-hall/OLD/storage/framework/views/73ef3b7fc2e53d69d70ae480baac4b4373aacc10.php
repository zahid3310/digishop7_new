

<?php $__env->startSection('title', 'Community Services'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Community Services</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Order Management</a></li>
                                    <li class="breadcrumb-item active">Community Services</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                        	<?php if(Session::has('success')): ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <?php echo Session::get('success'); ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            <?php if(Session::has('unsuccess')): ?>
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                <?php echo Session::get('unsuccess'); ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            <?php if(Session::has('errors')): ?>
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>
                            <div class="card-body table-responsive">
                            	<div style="padding: 0px" class="col-md-12">
                            		<button style="border-radius: 0px !important;float: right" type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#myModal">New Service</button>
                            	</div>

                                <h4 class="card-title">All Services</h4>

                                <br>

                                <table class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php if(!empty($services) && ($services->count() > 0)): ?>
                                        <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e($service['name']); ?></td>
                                                <td><?php echo e($service['price']); ?></td>
                                                <td><?php echo e($service['description']); ?></td>
                                                <td><?php echo e($service['status'] == 1 ? 'Active' : 'Inactive'); ?></td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="<?php echo e(route('community_service_edit', $service['id'])); ?>">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Service</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form id="FormSubmit" action="<?php echo e(route('community_service_store')); ?>" method="post" files="true" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>


                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="name" name="name" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Price *</label>
                        <div class="col-md-12">
                            <input id="price" name="price" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Description</label>
                        <div class="col-md-12">
                            <input id="description" name="description" type="text" class="form-control">
                        </div>
                    </div>


                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Floor Image</label>
                        <div class="col-md-12">
                            <input id="image" name="image" type="file" class="form-control">
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Status</label>
                        <div class="col-md-12">
                            <select style="cursor: pointer" class="form-control" name="status">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            	</form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var product_name  = $("#category_name").val();
            var site_url      = $('.site_url').val();

            if (product_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }

            $.ajax({
                type:   'get',
                url:    site_url + '/products/add-new-category/',
                data:   {  product_name : product_name, _token: '<?php echo e(csrf_token()); ?>' },
    
                success: function (data) {
                    if(data != 0)
                    {
                        $('#CloseButton').click();
                    }
                    
                    $("#product_category_id").empty();
                    $('#product_category_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    $('#category_name').val('');
                }
            });
        });   
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/23/Modules/OrderManagement/Resources/views/service_index.blade.php ENDPATH**/ ?>