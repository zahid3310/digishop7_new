@extends('layouts.app')

@section('title', 'Edit Sales')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Order</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">Edit Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                            Payment Successfull !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                            Payment Not Added !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('order_update', $find_invoice['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                {{ csrf_field() }}

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">

                                    <div class="col-md-5 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Customer *</label>
                                        <select style="width: 83%" id="customer_id" name="customer_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10" required>
                                            <option value="{{ $find_invoice['customer_id'] }}" selected>{{ $find_invoice['contact_name'] }}</option>
                                        </select>

                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center;" class="form-control col-lg-2 col-md-2 col-sm-2 col-2" data-toggle="modal" data-target="#myModal">
                                            <i class="bx bx-plus font-size-24"></i>
                                        </span>
                                    </div>


                                    <div class="col-md-3 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Booking Date *</label>
                                        <input id="selling_date" name="selling_date" type="text" value="{{ date('d-m-Y', strtotime($find_invoice['order_date'])) }}" class="form-control col-lg-12 col-md-12 col-sm-12 col-12" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                    </div>


                                    <div class="col-md-3 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Program From Date *</label>
                                        <input id="program_from_data" name="program_from_data" type="text" value="{{ date('d-m-Y', strtotime($find_invoice['program_from_data'])) }}" class="form-control col-lg-12 col-md-12 col-sm-12 col-12" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                    </div>

                                    <div class="col-md-3 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Program To Date *</label>
                                        <input id="program_to_data" name="program_to_data" type="text" value="{{ date('d-m-Y', strtotime($find_invoice['program_to_data'])) }}" class="form-control col-lg-12 col-md-12 col-sm-12 col-12" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                    </div>

                                    <div class="col-md-2 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Start Time *</label>
                                        <input  type="time" name="start_time"  value="{{$find_invoice['start_time'] }}" class="form-control col-lg-12 col-md-12 col-sm-12 col-12">
                                    </div>

                                    <div class="col-md-2 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">End Time *</label>
                                        <input  type="time" name="end_time" value="{{$find_invoice['end_time'] }}" class="form-control col-lg-12 col-md-12 col-sm-12 col-12">
                                    </div>

                                    <div class="col-md-3" >
                                        <div style="margin-bottom: 5px;margin-top: 35px;" class="form-group row">
                                            <i id="add_field_button" style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button col-md-3"></i>
                                        </div>
                                    </div>

                                    
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 415px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        @foreach($find_invoice_entries as $key => $value)
                                    
                                            <div style="margin-bottom: 0px !important" class="row di_{{$key}}">
                                                <div style="margin-bottom: 5px" class="col-lg-5 col-md-5 col-sm-6 col-12">
                                                    <label class="hidden-xs" for="productname">Product *</label>
                                                    <label style="display: none" class="show-xs" for="productname">Product *</label>
                                                    <select style="width: 100%" name="product_entries[]" class="inner form-control select2 productEntries" id="product_entries_{{$key}}" onchange="getItemPrice({{$key}})">
                                                        <option value="{{ $value['product_entry_id'] }}">
                                                        {{ $value['item_name'] }}
                                                        </option>
                                                    </select>
                                                   
                                                </div>


                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">Rate *</label>
                                                    <label style="display: none" class="show-xs" for="productname">Rate *</label>
                                                    <input type="text" name="rate[]" class="inner form-control" id="rate_{{$key}}" value="{{$value['rate']}}" placeholder="Rate" oninput="calculateActualAmount({{$key}})" />
                                                </div>

                                               
                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">Total</label>
                                                    <label style="display: none" class="show-xs" for="productname">Total</label>
                                                    <input type="text" name="amount[]" class="inner form-control amount" id="amount_{{$key}}" value="{{$value['total_amount']}}" placeholder="Total"/>
                                                </div>
                                                
                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">Action</label>
                                                    <label style="display: none" class="show-xs" for="productname">Action *</label>
                                                    
                                                    <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field" data-val="{{$key}}"></i>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div style="display: none">
                                        <select style="padding: 6px;border-radius: 4px;cursor: pointer" class="taxType" id="tax_type_0" name="tax_type" oninput="calculateActualAmount(0)">
                                            <option style="padding: 10px" value="1" {{ $find_invoice['tax_type'] == 1 ? 'selected' : '' }}>BDT</option>
                                            <option style="padding: 10px" value="0" {{ $find_invoice['tax_type'] == 0 ? 'selected' : '' }}>%</option>
                                        </select>
                                        <input id="tax_amount_0" type="text" class="form-control width-xs taxAmount" name="tax_amount" value="{{ $find_invoice['total_tax'] }}" oninput="calculateActualAmount(0)">
                                    </div>

                                    

                                    <div style="display: none;" class="row justify-content-end">
                                        <div class="col-lg-1 col-md-2 form-group">
                                            <input id="pos_add_button" type="button" class="btn btn-success btn-block inner add_field_button_pos" value="Add"/>
                                        </div>
                                    </div>
                                </div>


                                <div class="container">
                <div class="fixed-bottom" style="background: #fff; ">
                    <div class="row">
                        <div class="col-9">
                            <div class="m-2">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                          
                                            <td style="padding: 0px; line-height: 35px;color: green;"><strong>Total Payable:</strong></td>
                                            <td style="padding: 0px; line-height: 35px;font-size: 20px;"><strong><input id="totalBdtShow" name="total_amount"  class="form-control" readonly></strong></td>
                                            <td style="padding: 0px; line-height: 35px;color: green;"><strong> Cash Given:</strong></td>
                                            <td style="padding: 0px; line-height: 35px;">
                                                <input type="text" class="form-control" id="cash_given" name="cash_given" value="{{ $find_invoice['cash_given'] }}" oninput="calculateChangeAmount()">
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                       <div class="col-3 text-right">
                           <div style="margin-top: 10px;margin-right: 10px;">
                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Update Booking Confirm</button>
                           </div>
                       </div>
                   </div>
                </div>
            </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control commonCustomerClass">
                        </div>
                    </div>

                    <input id="contact_type" name="contact_type" type="hidden" value="0" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Reference</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name1" name="customer_name" type="text" class="form-control commonReferenceClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number1" name="mobile_number" type="text" class="form-control commonReferenceClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address1" name="address" type="text" class="form-control commonReferenceClass">
                        </div>
                    </div>

                    <input id="contact_type1" name="contact_type" type="hidden" value="3" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if ((result['contact_type'] == 0 || result['contact_type'] == 1) && result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });
            
            $("#reference_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 3)
                    {
                        return result['text'];
                    }
                },
            });
    
            $(".productEntries").select2({
                ajax: { 
                url:  site_url + '/bills/service-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
            
            var customerId      = $("#customer_id").val();

            $.get(site_url + '/invoices/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data);    
            });

            calculateActualAmount();

            $('#add_field_button_payment').click();
        });
    </script>

    <script type="text/javascript">
        // Get the input field
        var input = document.getElementById("product_code");

            // Execute a function when the user releases a key on the keyboard
            input.addEventListener("keyup", function(event) {
            // Number 13 is the "Enter" key on the keyboard
            if (event.keyCode === 13) {
                // Cancel the default action, if needed
                event.preventDefault();
                $("#alertMessage").hide();

                var site_url            = $(".site_url").val();
                var product_code_pos    = $("#product_code").val();

                if(product_code_pos != '')
                {
                    $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code_pos, function(data_pos){

                        if ($.isEmptyObject(data_pos))
                        {   
                            $('input[name=product_code').val('');
                            $("#alertMessage").show();
                        }
                        else
                        {
                            var state = 0;

                            $('.productEntries').each(function()
                            {
                                var entry_id    = $(this).val();
                                var value_x     = $(this).prop("id");

                                if (data_pos.id == entry_id)
                                {
                                    var explode          = value_x.split('_');
                                    var quantity_id      = explode[2];
                                    var quantity         = $('#quantity_'+quantity_id).val();
                                    var inc_quantity     = parseFloat(quantity) + 1;

                                    $('#quantity_'+quantity_id).val(inc_quantity);
                                    $('#quantity_'+quantity_id).change();
                                    $('input[name=product_code').val('');
                                    calculateActualAmount(quantity_id);

                                    state++;
                                }

                                if (entry_id == '')
                                {   
                                    var explode          = value_x.split('_');
                                    var quantity_id      = explode[2];
                                    
                                    ProductEntriesList(quantity_id);

                                    state++;
                                }
                            });

                            if (state == 0)
                            {   
                                $("#pos_add_button").click();
                            }
                            else
                            {
                                return false;
                            }
                        }
                    });
                } 
            }
        });

        function ProductEntriesList(x) 
        {
            //Barcode Scan Section Start
            var site_url            = $(".site_url").val();
            var product_code_pos    = $("#product_code").val();

            if (product_code_pos != undefined)
            {

                var product_code        = $("#product_code").val();
                $('input[name=product_code').val('');
            }
            else
            {
                var product_code        = $(".product_entries_"+x).val();
            }

            if (product_code)
            {
                $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code, function(data_pos){

                    if (data_pos.product_type == 2)
                    {
                        var variations  = ' - ' + data_pos.variations;
                    }
                    else
                    {
                        var variations  = '';
                    }

                    $('#product_entries_'+x).append('<option value="'+ data_pos.id +'" selected>'+ data_pos.name + variations + ' ' + '( ' + data_pos.product_code + ' )' +'</option>');
                    $('#product_entries_'+x).change();
                });
            }
        }

        function checkValue(value,arr)
        {
            var status = 'Not exist';
             
            for(var i=0; i<arr.length; i++)
            {
                var name = arr[i];
                if(name == value)
                {
                    status = 'Exist';
                    break;
                }
            }

            return status;
        }

        function getItemPrice(x)
        {
            //For getting item commission information from items table start
            var entry_id  = $("#product_entries_"+x).val();
            var site_url  = $(".site_url").val();

            if(entry_id)
            {
               $.get(site_url + '/invoices/service/price/list/'+ entry_id, function(data){
                    console.log(data)
                    $("#rate_"+x).val(parseFloat(data.price).toFixed(2));
                    $("#quantity_"+x).val(1);
                    // $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
                    // $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' ' + unit);
                    // $("#main_unit_id_"+x).val(data.product_entries.unit_id);
                    // $("#main_unit_name_"+x).val(data.product_entries.unit_name);
                    // $("#discount_"+x).val(0);

                    calculateActualAmount(x);

                });
            }
           
            //For getting item commission information from items table end
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                    $("#stock_show_"+x).html('Stock : ' + convertedStock + ' ( ' + convertedUnitName + ' )');
                    $("#rate_"+x).val(parseFloat(data.sell_price).toFixed(2));

                    calculateActualAmount(x);
                }

            });
        }

        function pad (str, max)
        {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = $("#contact_type").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                            $('.commonCustomerClass').val('');
                        }
                        
                        $("#customer_id").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });

        $('#submitBtn1').click(function() {
            
            var customer_name               = $("#customer_name1").val();
            var address                     = $("#address1").val();
            var mobile_number               = $("#mobile_number1").val();
            var contact_type                = $("#contact_type1").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton1').click();
                            $('.commonReferenceClass').val('');
                        }
                        
                        $("#reference_id").empty();
                        $('#reference_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });    
    </script>

    <script type="text/javascript">
        var max_fields       = 500;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = {{$entries_count}};
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var product_label       = '<label class="hidden-xs" for="productname">Floar *</label>\n';
                    var rate_label          = '<label class="hidden-xs" for="productname">Rent *</label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }
                else
                {
                    var product_label       = '';
                    var rate_label          = '';
                    var amount_label        = '';
                    var action_label        = '';
                    var add_btn             = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                ProductEntriesList(x);

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px" class="col-lg-5 col-md-5 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Floar --' + '</option>' +
                                                        '</select>\n' +
                                                        '<span style="display:none" id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +



                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="c-margin-left-11 col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                   
                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="c-margin-left-11 col-lg-3 col-md-3 col-sm-5 col-5">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                        amount_label +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

                var site_url = $(".site_url").val();
                $("#product_entries_"+x).select2({
                    ajax: { 
                    url:  site_url + '/bills/service-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                    },
                });
            }                                    
        });
        //For apending another rows end

        //For apending another rows start
        $(add_button_pos).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var product_label       = '<label class="hidden-xs" for="productname">Floar *</label>\n';
                    var rate_label          = '<label class="hidden-xs" for="productname">Rent *</label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }
                else
                {
                    var product_label       = '';
                    var rate_label          = '';
                    var amount_label        = '';
                    var action_label        = '';
                    var add_btn             = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                ProductEntriesList(x);

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px" class="col-lg-5 col-md-5 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Floar --' + '</option>' +
                                                        '</select>\n' +
                                                        '<span style="display:none" id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +


                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +


                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                        amount_label +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

                var site_url = $(".site_url").val();
                $("#product_entries_"+x).select2({
                    ajax: { 
                    url:  site_url + '/bills/service-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                    },
                });
            }
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        
        function calculateActualAmount(x)
        {
            var rate                    = $("#rate_"+x).val();

            if (rate == '')
            {
                var rateCal             = 0;
            }
            else
            {
                var rateCal             = $("#rate_"+x).val();
            }


            var AmountIn              =  parseFloat(rateCal);
     
            $("#amount_"+x).val(AmountIn);

            //Calculating Subtotal Amount
            var total       = 0;

            $('.amount').each(function()
            {
                total       += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(parseFloat(total));
            $("#subTotalBdtShow").val(parseFloat(total));

           
            
            
            var totalShow = parseFloat(total);

            $("#totalBdtShow").val(parseFloat(totalShow));
            $("#totalBdt").val(parseFloat(totalShow));

            calculateChangeAmount();
        }
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }

        function addButtonPayment()
        {
            $('.add_field_button_payment').click();
        }
    </script>

    <script type="text/javascript">
        var max_fields_payment       = 500;                                  //maximum input boxes allowed
        var wrapper_payment          = $(".input_fields_wrap_payment");      //Fields wrapper
        var add_button_payment       = $(".add_field_button_payment");       //Add button ID
        var index_no_payment         = 1;

        //For apending another rows start
        var y = {{$payment_entries_count}};
        $(add_button_payment).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(y < max_fields_payment)
            {   
                var serial = y + 1;

                if (serial == 1)
                {
                    var amount_paid_label   = '<label class="hidden-xs" for="productname">Amount Paid *</label>\n';
                    var date_label          = '<label class="hidden-xs" for="productname">Date *</label>\n';
                    var paid_through_label  = '<label class="hidden-xs" for="productname">Paid Through *</label>\n';
                    var account_info_label  = '<label class="hidden-xs" for="productname">AC Info.</label>\n';
                    var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button_payment">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonPayment()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var amount_paid_label   = '';
                    var date_label          = '';
                    var paid_through_label  = '';
                    var account_info_label  = '';
                    var note_label          = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field_payment" data-val="'+y+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRowPayment').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_payment_'+y+'">' +
                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Date *</label>\n' +
                                                        date_label +
                                                        '<input id="payment_date" name="payment_date[]" type="text" value="{{ date("d-m-Y") }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Amount Paid *</label>\n' +
                                                        amount_paid_label +
                                                        '<input type="text" name="amount_paid[]" class="form-control" id="amount_paid_'+y+'" value="0" required />\n' + 
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid Through *</label>\n' +
                                                        paid_through_label  +
                                                        '<select id="paid_through_'+y+'" style="cursor: pointer" name="paid_through[]" class="form-control single_select2">\n' +
                                                            '@if(!empty($paid_accounts) && ($paid_accounts->count() > 0))\n' +
                                                            '@foreach($paid_accounts as $key => $paid_account)\n' +
                                                                '<option value="{{ $paid_account['id'] }}">{{ $paid_account['name'] }}</option>\n' +
                                                            '@endforeach\n' +
                                                            '@endif\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-2 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Account Information</label>\n' +
                                                        account_info_label +
                                                        '<input type="text" name="account_information[]" class="form-control" id="account_information_'+y+'" placeholder="Account Information"/>\n' + 
                                                    '</div>\n' + 

                                                    '<div style="" class="col-lg-2 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                        note_label +
                                                        '<input type="text" name="note[]" class="form-control" id="note_'+y+'" placeholder="note"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +

                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

                y++;
            }                                    
        });
        //For apending another rows end

        $(wrapper_payment).on("click",".remove_field_payment", function(e)
        {
            e.preventDefault();

            var y = $(this).attr("data-val");

            $('.di_payment_'+y).remove(); y--;

            calculateActualAmount(y);
        });
    </script>

    <script type="text/javascript">
        $(document).on("change", "#defaultCheck2" , function() {

            var checkbox_value    = $("#defaultCheck2")[0].checked;

            if (checkbox_value == true)
            {
                $("#vatShow").show();
                $("#taxShow").show();
            }
            else
            {
                $("#vatShow").hide();
                $("#taxShow").hide();
            }
        });

        function couponMembership()
        {
            var site_url      = $(".site_url").val();
            var coupon_code   = $("#coupon_code").val();
    
            $('.DiscountType').val(1);
            $('.DiscountAmount').val(0);

            $.get(site_url + '/invoices/search/coupon-code/'+ coupon_code, function(data_coupon){

                if (data_coupon == '')
                {
                    alert('Invalid Coupon Code or Membership Card !!');
                    $('#coupon_code').val('');
                }
                else
                {
                    var state = 0;

                    $('.productEntries').each(function()
                    {
                        var entry_id    = $(this).val();
                        var value_x     = $(this).prop("id");
                        
                        if (data_coupon[0].product_id != null)
                        {
                            for (var i = data_coupon.length - 1; i >= 0; i--)
                            {   
                                if (data_coupon[i].product_id == entry_id)
                                {
                                    var explode    = value_x.split('_');
                                    var di_id      = explode[2];

                                    $('#discount_type_'+di_id).val(data_coupon[i].discount_type);
                                    $('#discount_'+di_id).val(data_coupon[i].discount_amount);
                                    
                                    calculateActualAmount(di_id);

                                    state++;
                                }
                            }
                        }
                        else
                        {
                            var explode    = value_x.split('_');
                            var di_id      = explode[2];

                            $('#discount_type_'+di_id).val(data_coupon[0].discount_type);
                            $('#discount_'+di_id).val(data_coupon[0].discount_amount);
                            
                            calculateActualAmount(di_id);
                        }    
                    });
                }
            });
        }

        function calculateChangeAmount()
        {
            var tAmount             = $("#totalBdtShow").val();
            var cGiven              = $("#cash_given").val();

            if (tAmount != '')
            {
                var totalAmount     = $("#totalBdtShow").val();
            }
            else
            {
                var totalAmount     = 0;
            }

            if (cGiven != '')
            {
                var cashGiven          = $("#cash_given").val();
            }
            else
            {
                var cashGiven          = 0;
            }

            var changeAmount    = parseFloat(cashGiven) - parseFloat(totalAmount);

            if (changeAmount > 0)
            {   
                $("#change_amount").val(0);
                $("#change_amount").val(parseFloat(changeAmount));
                $("#amount_paid_0").val(parseFloat(cashGiven) - parseFloat(changeAmount));
            }

            if (parseFloat(changeAmount) < 0)
            {   
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(cashGiven));
            }

            if (parseFloat(changeAmount) == 0)
            {   
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(cashGiven));
            }
        }

        $(document).on("change", "#customer_id" , function() {

            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            $.get(site_url + '/invoices/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data);
                
            });
        });
    </script>
@endsection