<?php

namespace Modules\OrderManagement\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\PaidThroughAccounts;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\UnitConversions;
use App\Models\Users;
use App\Models\AccountTransactions;
use App\Models\Orders;
use App\Models\OrderEntries;
use Response;
use DB;
use View;
use Carbon\Carbon;

class OrderManagementController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $invoices           = Invoices::orderBy('id', 'DESC')->first();
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();
        $products           = Products::select('id', 'name')->get();
        $product_entries    = ProductEntries::where('product_entries.maintain_stock', 0)
                                            ->orWhere(function($query)
                                            {
                                                $query->where('product_entries.maintain_stock', 1)
                                                      ->where('product_entries.stock_in_hand', '>', 0);

                                            })
                                            ->select('id', 'name', 'product_id', 'image')
                                            ->get();
        return view('ordermanagement::index', compact('paid_accounts', 'invoices', 'products', 'product_entries'));
    }


    public function orderList()
    {
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                                ->get();
        return view('ordermanagement::order_list', compact('paid_accounts'));
    }

    public function store(Request $request)
    {

        $rules = array(
            'selling_date'          => 'required',
            'customer_id'           => 'required',
            'product_entries.*'     => 'required',
            'amount.*'              => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();


      
        DB::beginTransaction();

        try{
            // $adjustment     = $data['adjustment_amount'];
            $vat            = $data['vat_amount'];
            // $tax            = $data['tax_amount'];
             
            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }


            $data_find                        = Orders::orderBy('created_at', 'DESC')->first();
            $order_number                     = $data_find != null ? $data_find['id'] + 1 : 1;

            $order                            = new Orders;;
            $order->order_number              = $order_number;
            $order->customer_id               = $data['customer_id'];
            $order->reference_id              = $data['reference_id'];
            $order->customer_name             = $data['customer_name'];
            $order->customer_phone            = $data['customer_phone'];
            $order->order_date                = date('Y-m-d', strtotime($data['selling_date']));
            $order->order_amount              = $data['total_amount'];
            $order->due_amount                = $data['total_amount'];
            $order->total_buy_price           = $buy_price;
            $order->total_discount            = $discount;
            $order->order_note                = $data['invoice_note'];
            $order->total_vat                 = $vat;
            $order->vat_type                  = $data['vat_type'];
            $order->cash_given                = $data['cash_given'];
            $order->total_discount_type       = $data['total_discount_type'];
            $order->total_discount_note       = $data['total_discount_note'];
            $order->order_status              = 1;
            $order->status                    = 1;
            $order->created_by                = $user_id;

            if ($order->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'        => $order['id'],
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value,
                        'main_unit_id'      => $data['main_unit_id'][$key],
                        // 'conversion_unit_id'=> $data['unit_id'][$key],
                        'customer_id'       => $order['customer_id'],
                        'reference_id'      => $data['reference_id'],
                        'buy_price'         => $product_buy_price['buy_price'],
                        'rate'              => $data['rate'][$key],
                        'quantity'          => $data['quantity'][$key],
                        'total_amount'      => $data['amount'][$key],
                        'discount_type'     => $data['discount_type'][$key],
                        'discount_amount'   => $data['discount'][$key],
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('order_entries')->insert($invoice_entries);

                if (isset($data['amount_paid']))
                {
                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {
                    $update_order_dues                = Orders::find($order['id']);
                    $update_order_dues->due_amount    = $update_order_dues['due_amount'] - $data['amount_paid'][$i];
                    $update_order_dues->save();
                }

                    // $data_find        = Payments::orderBy('id', 'DESC')->first();
                    // $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                    // for($i = 0; $i < count($data['amount_paid']); $i++)
                    // {   
                    //     if ($data['amount_paid'][$i] > 0)
                    //     {   
                    //         $account_transactions[] = [
                    //             'customer_id'           => $data['customer_id'],
                    //             'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                    //             'amount'                => $data['amount_paid'][$i],
                    //             'paid_through_id'       => $data['paid_through'][$i],
                    //             'account_information'   => $data['account_information'][$i],
                    //             'note'                  => $data['note'][$i],
                    //             'type'                  => 0,  // 0 = In , 1 = Out
                    //             'transaction_head'      => 'sales',
                    //             'associated_id'         => $invoice->id,
                    //             'created_by'            => $user_id,
                    //             'created_at'            => date('Y-m-d H:i:s'),
                    //         ];

                    //         $payments = [
                    //             'payment_number'        => $payment_number,
                    //             'customer_id'           => $data['customer_id'],
                    //             'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                    //             'amount'                => $data['amount_paid'][$i],
                    //             'account_information'   => $data['account_information'][$i],
                    //             'paid_through'          => $data['paid_through'][$i],
                    //             'note'                  => $data['note'][$i],
                    //             'type'                  => 0,
                    //             'created_by'            => $user_id,
                    //             'created_at'            => date('Y-m-d H:i:s'),
                    //         ];

                    //         $payment_id = DB::table('payments')->insertGetId($payments);      

                    //         if ($payment_id)
                    //         {   
                    //             $payment_entries = [
                    //                     'payment_id'        => $payment_id,
                    //                     'invoice_id'        => $invoice['id'],
                    //                     'amount'            => $data['amount_paid'][$i],
                    //                     'created_by'        => $user_id,
                    //                     'created_at'        => date('Y-m-d H:i:s'),
                    //             ];

                    //             DB::table('payment_entries')->insert($payment_entries);  
                    //         }

                    //         $update_invoice_dues                = Invoices::find($invoice['id']);
                    //         $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                    //         $update_invoice_dues->save();

                    //         $payment_number++;
                    //     }
                    // }

                    // if (isset($account_transactions))
                    // {
                    //     DB::table('account_transactions')->insert($account_transactions);
                    // }
                }

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Sales Created Successfully !!");
                }
                else
                {
                    $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                            ->select('invoices.*',
                                                     'customers.name as customer_name',
                                                     'customers.address as address',
                                                     'customers.phone as phone')
                                            ->find($invoice['id']);

                    $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->where('invoice_entries.invoice_id', $invoice['id'])
                                            ->select('invoice_entries.*',
                                                     'product_entries.product_type as product_type',
                                                     'product_entries.name as product_entry_name',
                                                     'units.name as unit_name',
                                                     'products.name as product_name')
                                            ->get();  
                                 
                    $user_info  = Users::find(1);

                    return View::make('invoices::show_pos')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function orderListLoad()
    {
        $data           = Orders::leftjoin('sales_return', 'sales_return.invoice_id', 'orders.id')
                                    ->leftjoin('customers', 'customers.id', 'orders.customer_id')
                                    ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'orders.id')
                                    ->leftjoin('users', 'users.id', 'orders.created_by')
                                    ->where('orders.type', 1)
                                    ->where('orders.status', 1)
                                    ->orderBy('orders.created_at', 'DESC')
                                    ->select('orders.*',
                                            'sales_return.id as return_id',
                                            'customers.name as customer_name',
                                            'customers.phone as phone',
                                            'users.name as waiter_name')
                                    ->distinct('orders.id')
                                    ->take(20)
                                    ->get();

        return Response::json($data);
    }

    public function orderCancel($id)
    {
        $order = Orders::findorfail($id);
        $order->order_status = 0;
        $order->save();
        return back();

    }

    public function orderStatusChange(Request $request)
    {
        $order = Orders::findorfail($request->invoice_id);
        $order->order_status = $request->order_status;
        $order->save();
        return back();
    }


    public function edit($id)
    {
        $products               = Products::orderBy('products.total_sold', 'DESC')
                                            ->get();
        

        $product_entry          = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->where('product_entries.stock_in_hand', '!=', null)
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                         GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                         GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                         GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                         GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                         GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                         GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                         GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                         GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT products.id) as product_id
                                                        ')
                                            ->orderBy('product_entries.total_sold', 'DESC')
                                            ->get();

        
        


        $product_entry          = $product_entry->sortBy('name')->all();
        $product_entries        = collect($product_entry);

        $find_invoice           = Orders::leftjoin('customers', 'customers.id', 'orders.customer_id')
                                            ->select('orders.*',
                                                 'customers.id as customer_id',
                                                 'customers.name as contact_name')
                                            ->find($id);

        $find_invoice_entries   = OrderEntries::leftjoin('customers', 'customers.id', 'order_entries.customer_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'order_entries.product_entry_id')
                                            ->where('order_entries.invoice_id', $id)
                                            ->select('order_entries.*',
                                                    'customers.id as customer_id',
                                                    'customers.name as customer_name',
                                                    'product_entries.id as item_id',
                                                    'product_entries.product_code as product_code',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.name as item_name')
                                            ->get();

        $entries_count          = $find_invoice_entries->count();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->where('payment_entries.invoice_id', $id)
                                        ->selectRaw('payment_entries.*, 
                                                     payments.paid_through as paid_through_id,
                                                     payments.account_information as account_information,
                                                     payments.payment_date as payment_date,
                                                     payments.note as note')
                                        ->get();

        $payment_entries_count  = $payment_entries->count();

        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();
        
        return view('ordermanagement::edit', compact('products', 'product_entries', 'find_invoice', 'find_invoice_entries', 'entries_count', 'payment_entries', 'payment_entries_count', 'paid_accounts'));

    }

    public function update(Request $request, $id)
    {


        $rules = array(
            'selling_date'      => 'required',
            'customer_id'       => 'required',
            'product_entries.*' => 'required',
            'amount.*'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            // $adjustment     = $data['adjustment_amount'];
            $vat            = $data['vat_amount'];
            // $tax            = $data['tax_amount'];
            
            $invoice        = Orders::find($id);

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }

            //Calculate Due Amount

                if ($data['total_amount'] > $invoice['order_amount']) 
                {
                    $invoice_dues = $invoice['due_amount'] + ($data['total_amount'] - $invoice['order_amount']);

                }
                
                if ($data['total_amount'] < $invoice['order_amount'])
                {
                    $invoice_dues = $invoice['due_amount'] - ($invoice['order_amount'] - $data['total_amount']);
                }

                if ($data['total_amount'] == $invoice['order_amount'])
                {
                    $invoice_dues = $invoice['due_amount'];
                }
            
                $invoice->customer_id               = $data['customer_id'];
                $invoice->reference_id              = $data['reference_id'];
                $invoice->customer_name             = $data['customer_name'];
                $invoice->customer_phone            = $data['customer_phone'];
                $invoice->order_date                = date('Y-m-d', strtotime($data['selling_date']));
                $invoice->delivery_date             = date('Y-m-d', strtotime($data['delivery_date']));
                $invoice->order_amount              = $data['total_amount'];
                $invoice->due_amount                = $invoice_dues;
                $invoice->total_buy_price           = $buy_price;
                $invoice->total_discount            = $discount;
                $invoice->order_note                = $data['invoice_note'];
                $invoice->total_vat                 = $vat;
                $invoice->vat_type                  = $data['vat_type'];
                $invoice->discount_code             = $data['coupon_code'];
                $invoice->total_discount_type       = $data['total_discount_type'];
                $invoice->total_discount_amount     = $data['total_discount_amount'];
                $invoice->total_discount_note       = $data['total_discount_note'];
                $invoice->cash_given                = $data['cash_given'];
                $invoice->change_amount             = $data['change_amount'];
                $invoice->updated_by                = $user_id;

            if ($invoice->save())
            {
                $item_id                = OrderEntries::where('invoice_id', $invoice['id'])->get();
                $item_delete            = OrderEntries::where('invoice_id', $invoice['id'])->delete();

                if (isset($data['payment_id']))
                {
                    foreach ($data['payment_id'] as $key_p => $value_p)
                    {
                        $payment_delete  = Payments::where('id', $value_p)->delete();
                    }
                }

                $update_invoice                = Orders::find($invoice->id);
                $update_invoice->due_amount    = $update_invoice['due_amount'];
                $update_invoice->updated_by    = $user_id;
                $update_invoice->save();

                foreach ($data['product_entries'] as $key => $value)
                {   
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'        => $invoice['id'],
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value,
                        'main_unit_id'      => $data['main_unit_id'][$key],
                        'conversion_unit_id'=> $data['unit_id'][$key],
                        'customer_id'       => $invoice['customer_id'],
                        'reference_id'      => $data['reference_id'],
                        'buy_price'         => $product_buy_price['buy_price'],
                        'rate'              => $data['rate'][$key],
                        'quantity'          => $data['quantity'][$key],
                        'total_amount'      => $data['amount'][$key],
                        'discount_type'     => $data['discount_type'][$key],
                        'discount_amount'   => $data['discount'][$key],
                        'created_by'        => $user_id,
                    ];
                }

                DB::table('order_entries')->insert($invoice_entries);

                stockOut($data, $item_id);

                $transaction_delete = AccountTransactions::where('transaction_head', 'sales')->where('associated_id', $invoice['id'])->delete();

                if (isset($data['amount_paid']))
                {
                    $data_find        = Payments::orderBy('id', 'DESC')->first();
                    $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {   
                        if ($data['amount_paid'][$i] > 0)
                        {   
                            $account_transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through_id'       => $data['paid_through'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 0,  // 0 = In , 1 = Out
                                'transaction_head'      => 'sales',
                                'associated_id'         => $invoice->id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payments = [
                                'payment_number'        => $payment_number,
                                'customer_id'           => $data['customer_id'],
                                'payment_date'          => date('Y-m-d', strtotime($data['payment_date'][$i])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through'          => $data['paid_through'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 0,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payment_id = DB::table('payments')->insertGetId($payments);      

                            if ($payment_id)
                            {   
                                $payment_entries = [
                                        'payment_id'        => $payment_id,
                                        'invoice_id'        => $invoice['id'],
                                        'amount'            => $data['amount_paid'][$i],
                                        'created_by'        => $user_id,
                                        'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('payment_entries')->insert($payment_entries);  
                            }

                            $update_invoice_dues                = Invoices::find($invoice['id']);
                            $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                            $update_invoice_dues->save();

                            $payment_number++;
                        }
                    }

                    if (isset($account_transactions))
                    {
                        DB::table('account_transactions')->insert($account_transactions);
                    }
                }

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return redirect()->route('invoices_index')->with("success","Sales Updated Successfully !!");
                }
                else
                {
                    if (Auth::user()->service_type == 1)
                    {
                        return redirect()->route('invoices_show', $invoice['id']);
                    }
                    else
                    {
                        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                            ->select('invoices.*',
                                                     'customers.name as customer_name',
                                                     'customers.address as address',
                                                     'customers.phone as phone')
                                            ->find($invoice['id']);

                        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                                ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                ->where('invoice_entries.invoice_id', $invoice['id'])
                                                ->select('invoice_entries.*',
                                                         'product_entries.product_type as product_type',
                                                         'product_entries.name as product_entry_name',
                                                         'units.name as unit_name',
                                                         'products.name as product_name')
                                                ->get();  
                                     
                        $user_info  = Users::find(1);

                        return View::make('invoices::show_pos')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info);
                    }
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }


    public function makePayment($id)
    {
        $data       = Orders::find($id);
        return Response::json($data);
    }

    public function storePayment(Request $request)
    {

        $user_id                    = Auth::user()->id;

        $data                       = $request->all();
        $order                      = Orders::find($data['invoice_id']);
        $Order_entries              = OrderEntries::where('invoice_id',$data['invoice_id'])->get();
        
        DB::beginTransaction();

        try{

            $invoice                            = new Invoices;
            $invoice->invoice_number            = $order->order_number;
            $invoice->customer_id               = $order->customer_id;
            $invoice->reference_id              = $order->reference_id;
            $invoice->customer_name             = $order->customer_name;
            $invoice->customer_phone            = $order->customer_phone;
            $invoice->invoice_date              = $order->order_date;
            $invoice->invoice_amount            = $order->order_amount;
            $invoice->due_amount                = $order->due_amount;
            $invoice->total_buy_price           = $order->total_buy_price;
            $invoice->total_discount            = $order->total_discount;
            $invoice->invoice_note              = $order->order_note;
            $invoice->total_vat                 = $order->total_vat;
            $invoice->vat_type                  = $order->vat_type;
            $invoice->total_discount_type       = $order->total_discount_type;
            $invoice->total_discount_amount     = $order->total_discount_amount;
            $invoice->total_discount_note       = $order->total_discount_note;
            $invoice->cash_given                = $order->cash_given;
            $invoice->change_amount             = $order->change_amount;
            $invoice->created_by                = $order->created_by;

            if ($invoice->save()) {
                foreach ($Order_entries  as $key => $value)
                {
                    $invoice_entries = new InvoiceEntries;
            
                        $invoice_entries->invoice_id        = $invoice->id;
                        $invoice_entries->product_id        = $value->product_id;
                        $invoice_entries->product_entry_id  = $value->product_entry_id;
                        $invoice_entries->main_unit_id      = $value->main_unit_id;
                        $invoice_entries->conversion_unit_id= $value->conversion_unit_id;
                        $invoice_entries->customer_id       = $value->customer_id;
                        $invoice_entries->buy_price         = $value->buy_price;
                        $invoice_entries->rate              = $value->rate;
                        $invoice_entries->quantity          = $value->quantity;
                        $invoice_entries->total_amount      = $value->total_amount;
                        $invoice_entries->discount_type     = $value->discount_type;
                        $invoice_entries->discount_amount   = $value->discount_amount;
                        $invoice_entries->created_by        = $value->created_by;

                        $invoice_entries->save();
                }
            }

            if (isset($data['amount_paid']))
            {
                $data_find        = Payments::orderBy('id', 'DESC')->first();
                $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                for($i = 0; $i < count($data['amount_paid']); $i++)
                {   
                    if ($data['amount_paid'][$i] > 0)
                    {
                        $payments = [
                            'payment_number'        => $payment_number,
                            'customer_id'           => $data['customer_id'],
                            'payment_date'          => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $data['amount_paid'][$i],
                            'paid_through'          => $data['paid_through'][$i],
                            'account_information'   => $data['account_information'][$i],
                            'note'                  => $data['note'][$i],
                            'type'                  => 0,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $payment_id = DB::table('payments')->insertGetId($payments);      

                        if ($payment_id)
                        {   
                            $payment_entries = [
                                    'payment_id'        => $payment_id,
                                    'invoice_id'        => $invoice->id,
                                    'amount'            => $data['amount_paid'][$i],
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            DB::table('payment_entries')->insert($payment_entries);  
                        }

                        $update_invoice_dues             = Invoices::where('invoice_number',$order->order_number)->first();
                        $update_invoice_dues->due_amount = $update_invoice_dues->due_amount - $data['amount_paid'][$i];
                        $update_invoice_dues->save();

                        $payment_number++;
                    }
                }

                    $update_invoice_dues             = Orders::where('order_number',$order->order_number)->first();
                    $update_invoice_dues->status     = 0;
                    $update_invoice_dues->save();
            }
            
            DB::commit();
            return Response::json(1);
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            dd($exception);
            return Response::json(0);
        }
    }


    public function confirmList()
    {
      
 
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                                ->get();

        return view('ordermanagement::confirm_list', compact('paid_accounts'));
    }


    
}
