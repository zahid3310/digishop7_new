<!DOCTYPE html>
<html>

<head>
    <title>Statement of Purchase</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p><?php echo e($user_info['address']); ?></p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Statement of Purchase</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                    <th style="text-align: center">ITEM CATEGORY</th>
                                    <th style="text-align: center">Purchase TYPE</th>
                                    <th style="text-align: center">Purchase BY</th>
                                    <th style="text-align: center">Purchase ID</th>
                                    <th style="text-align: center">Supplier Name</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center"><?php echo e($from_date); ?> <strong>To</strong> <?php echo e($to_date); ?></td>
                                    <td style="text-align: center">
                                        <?php if($item_category_name != null): ?>
                                            <?php echo e($item_category_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>

                                    <td style="text-align: center">
                                        <?php if($purchase_return == 0): ?>
                                            ALL
                                        <?php endif; ?>

                                        <?php if($purchase_return == 1): ?>
                                            Purchase
                                        <?php endif; ?>

                                        <?php if($purchase_return == 2): ?>
                                            Purchase Return
                                        <?php endif; ?>
                                    </td>

                                    <td style="text-align: center">
                                        <?php if($purchase_by_name != null): ?>
                                            <?php echo e($purchase_by_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>

                                    <td style="text-align: center">
                                        <?php if($purchase_id != null): ?>
                                            <?php echo e('BILL - ' . str_pad($purchase_id['bill_number'], 6, "0", STR_PAD_LEFT)); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>

                                    <td style="text-align: center">
                                        <?php if($supplier_name != null): ?>
                                            <?php echo e($supplier_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 6%">T/DATE</th>
                                    <th style="text-align: center;width: 7%">Purchase ID</th>
                                    <th style="text-align: center;width: 6%">ITEM CODE</th>
                                    <th style="text-align: center;width: 13%">NAME</th>
                                    <th style="text-align: center;width: 4%">U/M</th>
                                    <th style="text-align: center;width: 4%">Qty</th>
                                    <th style="text-align: center;width: 4%">UNIT PRICE</th>
                                    <th style="text-align: center;width: 6%">T/PRICE</th>
                                    <th style="text-align: center;width: 4%">DISCOUNT</th>
                                    <th style="text-align: center;width: 6%">T/PAYABLE</th>
                                    <th style="text-align: center;width: 4%">VAT</th>
                                    <th style="text-align: center;width: 4%">T/DISCOUNT</th>
                                    <th style="text-align: center;width: 6%">N/PAYABLE</th>
                                    <th style="text-align: center;width: 6%">PAID</th>
                                    <th style="text-align: center;width: 5%">DUE</th>
                                    <th style="text-align: center;width: 10%">Supplier NAME</th>
                                    <th style="text-align: center;width: 2%">TYPE</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i                  = 1;
                                    $total_t_price      = 0;
                                    $total_discount     = 0;
                                    $total_payable      = 0;
                                    $total_vat          = 0;
                                    $total_discount_t   = 0;
                                    $total_net_payable  = 0;
                                    $total_paid         = 0;
                                    $total_due          = 0;
                                ?>
                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td style="text-align: center;vertical-align: middle" rowspan="<?php echo e($value['row_span']); ?>"><?php echo e($i); ?></td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="<?php echo e($value['row_span']); ?>"><?php echo e($value['bill_date']); ?></td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="<?php echo e($value['row_span']); ?>"><?php echo e($value['bill_number']); ?></td>

                                    <td style="text-align: center;"><?php echo e(str_pad($value['bill_entries'][0]['product_code'], 6, "0", STR_PAD_LEFT)); ?></td>
                                    <td style="text-align: center;"><?php echo e($value['bill_entries'][0]['product_entry_name']); ?></td>
                                    <td style="text-align: center;"><?php echo e($value['bill_entries'][0]['unit_name']); ?></td>
                                    <td style="text-align: center;"><?php echo e($value['bill_entries'][0]['quantity']); ?></td>
                                    <td style="text-align: right;"><?php echo e($value['bill_entries'][0]['rate']); ?></td>
                                    <td style="text-align: right;"><?php echo e($value['bill_entries'][0]['rate'] * $value['bill_entries'][0]['quantity']); ?></td>
                                    <td style="text-align: right;">
                                        <?php
                                            if (isset($value['bill_entries'][0]['discount_type']))
                                            {
                                                if ($value['bill_entries'][0]['discount_type'] == 0)
                                                {
                                                    $discountAmount0   = ($value['bill_entries'][0]['rate'] * $value['bill_entries'][0]['quantity'] * $value['bill_entries'][0]['discount_amount'])/100;
                                                }
                                                else
                                                {
                                                    $discountAmount0   = $value['bill_entries'][0]['discount_amount'];
                                                }
                                            }
                                            else
                                            {
                                                $discountAmount0  = 0;
                                            }
                                        ?>

                                        <?php echo e($discountAmount0); ?>

                                    </td>
                                    <td style="text-align: right;"><?php echo e(($value['bill_entries'][0]['rate'] * $value['bill_entries'][0]['quantity']) - $discountAmount0); ?></td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="<?php echo e($value['row_span']); ?>"><?php echo e($value['vat_type'] == 0 ? $value['vat_perc'] : $value['vat']); ?></td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="<?php echo e($value['row_span']); ?>">
                                        <?php if(isset($value['total_discount_type'])): ?>
                                            <?php echo e($value['total_discount_type'] == 0 ? $value['total_discount_perc'] : $value['total_discount']); ?>

                                        <?php else: ?>
                                            0
                                        <?php endif; ?>
                                    </td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="<?php echo e($value['row_span']); ?>"><?php echo e($value['net_payable']); ?></td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="<?php echo e($value['row_span']); ?>"><?php echo e($value['paid_amount']); ?></td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="<?php echo e($value['row_span']); ?>"><?php echo e($value['net_payable'] - $value['paid_amount']); ?></td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="<?php echo e($value['row_span']); ?>"><?php echo e($value['customer_name']); ?></td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="<?php echo e($value['row_span']); ?>"><?php echo e($value['type']); ?></td>
                                </tr>
                                 
                                <?php
                                    $sub_total_t_price      = 0;
                                    $sub_total_discount     = 0;
                                    $sub_total_payable      = 0;
                                ?>

                                <?php $__currentLoopData = $value['bill_entries']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php
                                    if (isset($value1['discount_type']))
                                    {
                                        if ($value1['discount_type'] == 0)
                                        {
                                            $discountAmount   = ($value1['rate'] * $value1['quantity'] * $value1['discount_amount'])/100;
                                        }
                                        else
                                        {
                                            $discountAmount   = $value1['discount_amount'];
                                        }
                                    }
                                    else
                                    {
                                        $discountAmount   = 0;
                                    }

                                    $sub_total_t_price  = $sub_total_t_price + ($value1['rate'] * $value1['quantity']);
                                    $sub_total_discount = $sub_total_discount + $discountAmount;
                                    $sub_total_payable  = $sub_total_payable + (($value1['rate'] * $value1['quantity']) - $discountAmount);
                                ?>

                                <?php if($key1 != 0): ?>
                                <tr>
                                    <td style="text-align: center;"><?php echo e(str_pad($value1['product_code'], 6, "0", STR_PAD_LEFT)); ?></td>
                                    <td style="text-align: center;"><?php echo e($value1['product_entry_name']); ?></td>
                                    <td style="text-align: center;"><?php echo e($value1['unit_name']); ?></td>
                                    <td style="text-align: center;"><?php echo e($value1['quantity']); ?></td>
                                    <td style="text-align: right;"><?php echo e($value1['rate']); ?></td>
                                    <td style="text-align: right;"><?php echo e($value1['rate'] * $value1['quantity']); ?></td>
                                    <td style="text-align: right;">
                                        <?php
                                            if ($value1['discount_type'] == 0)
                                            {
                                                $discountAmount   = ($value1['rate'] * $value1['quantity'] * $value1['discount_amount'])/100;
                                            }
                                            else
                                            {
                                                $discountAmount   = $value1['discount_amount'];
                                            }
                                        ?>

                                        <?php echo e($discountAmount); ?>

                                    </td>
                                    <td style="text-align: right;"><?php echo e(($value1['rate'] * $value1['quantity']) - $discountAmount); ?></td>
                                </tr>

                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                <?php
                                    $i++;

                                    $total_t_price      = $total_t_price + $sub_total_t_price;
                                    $total_discount     = $total_discount + $sub_total_discount;
                                    $total_payable      = $total_payable + $sub_total_payable;
                                    $total_vat          = $total_vat +  ($value['vat_type'] == 0 ? $value['vat_perc'] : $value['vat']);
                                    $total_discount_t   = $total_discount_t + ($value['total_discount_type'] == 0 ? $value['total_discount_perc'] : $value['total_discount']);
                                    $total_net_payable  = $total_net_payable + $value['net_payable'];
                                    $total_paid         = $total_paid + $value['paid_amount'];
                                    $total_due          = $total_due + ($value['net_payable'] - $value['paid_amount']);
                                ?>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="8" style="text-align: right;">TOTAL</th>
                                    <th colspan="1" style="text-align: right;"><?php echo e($total_t_price); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e($total_discount); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e($total_payable); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e($total_vat); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e($total_discount_t); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e($total_net_payable); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e($total_paid); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e($total_due); ?></th>
                                    <th colspan="2" style="text-align: right;"></th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/40/Modules/Reports/Resources/views/purchase_statement_print.blade.php ENDPATH**/ ?>