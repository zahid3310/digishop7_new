<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class AccountTransactions extends Model
{  
    protected $table = "account_transactions";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function transferFrom()
    {
        return $this->belongsTo('App\Models\PaidThroughAccounts','transfer_from');
    }

    public function transferTo()
    {
        return $this->belongsTo('App\Models\PaidThroughAccounts','transfer_to');
    }
}
