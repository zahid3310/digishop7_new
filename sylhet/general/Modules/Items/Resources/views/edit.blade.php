@extends('layouts.app')

@section('title', 'Items')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Items</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Items</a></li>
                                    <li class="breadcrumb-item active">Items</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="card-title">Edit Item</h4>
                                <br>

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif
                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('items_update', $find_item['id']) }}" method="post" enctype="multipart/formdata">
            					{{ csrf_field() }}

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Name</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $find_item['name'] }}" name="name" id="name" placeholder="Enter Category Name" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Sub Category</label>
                                    <div class="col-md-10">
                                        <select class="form-control select2" name="sub_category_id">
                                            <option value="">Select Sub Category</option>
                                            @if(!empty($sub_categories) && ($sub_categories->count() > 0))
                                            @foreach($sub_categories as $key => $sub_category)
                                                <option {{ $find_item['sub_category_id'] == $sub_category['id'] ? 'selected' : '' }} value="{{ $sub_category['id'] }}">{{ $sub_category['name'] }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Publication Status</label>
                                    <div class="col-md-10">
                                        <select class="form-control" name="status">
                                            <option value="1" {{ $find_item['status'] == 1 ? 'selected' : '' }} >Active</option>
                                            <option value="0" {{ $find_item['status'] == 0 ? 'selected' : '' }} >Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                	<div class="col-md-2"></div>
                                    <div class="button-items col-md-2 pull-right">
                                    	<button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                    	<button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('home') }}">Close</a></button>
                                    </div>
                                	<div class="col-md-8"></div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">All Items</h4>
                                <br>

                                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Sub Category</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        @if(!empty($items) && ($items->count() > 0))
                                        @foreach($items as $key => $item)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $item['name'] }}</td>
                                                <td>{{ $item['sub_category_name'] != null ? $item['sub_category_name'] : 'N/A'}}</td>
                                                <td>{{ $item['status'] == 1 ? 'Active' : 'Inactive' }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('items_edit', $item['id']) }}">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }
    </script>
@endsection