<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/backup/store', 'BackupController@store')->name('backup_store');
Route::get('/backup/download/{id}', 'BackupController@download')->name('backup_download');
