@extends('layouts.app')

@section('title', 'List of Salary Sheets')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">List of Salary Sheets</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Payroll</a></li>
                                    <li class="breadcrumb-item active">List of Salary Sheets</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {!! Session::get('success') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('unsuccess'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! Session::get('unsuccess') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('errors'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! 'Some required fields are missing..!! Please try again..' !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        <div class="card">
                            <div class="card-body table-responsive">
                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;font-size: 12px">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%">SL#</th>
                                            <th style="width: 15%">MONTH</th>
                                            <th style="width: 15%">Employee</th>
                                            <th style="width: 10%">Designation</th>
                                            <th style="width: 10%">Department</th>
                                            <th style="width: 10%">Gross</th>
                                            <th style="width: 10%">Present</th>
                                            <th style="width: 10%">Advance</th>
                                            <th style="width: 10%">Payable</th>
                                            <th style="width: 5%">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($salary_statements) && ($salary_statements->count() > 0))
                                        @foreach($salary_statements as $key => $salary_statement)


                                            <?php
                                                if ($salary_statement->month == 1)
                                                {
                                                    $month  = 'January';
                                                }
                                                elseif ($salary_statement->month == 1)
                                                {
                                                    $month  = 'February';
                                                }
                                                elseif ($salary_statement->month == 3)
                                                {
                                                    $month  = 'March';
                                                }
                                                elseif ($salary_statement->month == 4)
                                                {
                                                    $month  = 'April';
                                                }
                                                elseif ($salary_statement->month == 5)
                                                {
                                                    $month  = 'May';
                                                }
                                                elseif ($salary_statement->month == 6)
                                                {
                                                    $month  = 'Jun';
                                                }
                                                elseif ($salary_statement->month == 7)
                                                {
                                                    $month  = 'July';
                                                }
                                                elseif ($salary_statement->month == 8)
                                                {
                                                    $month  = 'August';
                                                }
                                                elseif ($salary_statement->month == 9)
                                                {
                                                    $month  = 'September';
                                                }
                                                elseif ($salary_statement->month == 10)
                                                {
                                                    $month  = 'October';
                                                }
                                                elseif ($salary_statement->month == 11)
                                                {
                                                    $month  = 'November';
                                                }
                                                elseif ($salary_statement->month == 12)
                                                {
                                                    $month  = 'December';
                                                }
                                            ?>

                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $month . ', ' . $salary_statement->year }}</td>
                                                <td>{{ $salary_statement->name }}</td>
                                                <td>{{ $salary_statement->designation != null ? $salary_statement->designation : '' }}</td>
                                                <td>{{ $salary_statement->department_name != null ? $salary_statement->department_name : '' }}</td>
                                                <td style="text-align: right">{{ number_format($salary_statement->salaryStatements->gross,0,'.',',') }}</td>
                                                <td style="text-align: center">{{ $salary_statement->total_attendance }}</td>
                                                <td style="text-align: center">{{ $salary_statement->advance }}</td>
                                                <td style="text-align: right">{{ number_format($salary_statement->net_payable,0,'.',',') }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('process_monthly_salary_delete', $salary_statement->id) }}">Delete</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection