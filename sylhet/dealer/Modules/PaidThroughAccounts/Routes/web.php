<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('paidthroughaccounts')->group(function() {
    Route::get('/', 'PaidThroughAccountsController@index')->name('paid_through_accounts_index');
    Route::post('/store', 'PaidThroughAccountsController@store')->name('paid_through_accounts_store');
    Route::get('/edit/{id}', 'PaidThroughAccountsController@edit')->name('paid_through_accounts_edit');
    Route::post('/update/{id}', 'PaidThroughAccountsController@update')->name('paid_through_accounts_update');
});