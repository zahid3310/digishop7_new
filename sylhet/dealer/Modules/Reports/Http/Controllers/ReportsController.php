<?php

namespace Modules\Reports\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\ExpenseEntries;
use App\Models\SalesReturn;
use App\Models\SalesReturnEntries;
use App\Models\PurchaseReturn;
use App\Models\PurchaseReturnEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Expenses;
use App\Models\Incomes;
use App\Models\Users;
use App\Models\ProductVariations;
use App\Models\ProductVariationValues;
use App\Models\ProductSuppliers;
use App\Models\ProductCustomers;
use App\Models\SrItems;
use App\Models\FreeItems;
use App\Models\StockTransfers;
use App\Models\Orders;
use App\Models\OrderEntries;
use App\Models\Areas;
use App\Models\Stores;
use App\Models\MachineDataLog;
use App\Models\MonthlySalarySheets;
use App\Models\Accounts;
use App\Models\JournalEntries;
use Carbon\Carbon;
use Response;
use Auth;
use DB;

class ReportsController extends Controller
{
    public function stockReport()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $product_id     = isset($_GET['product_id']) ? $_GET['product_id'] : 0;

        $data           = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->when($product_id != 0, function ($query) use ($product_id) {
                                        return $query->where('products.id', $product_id);
                                    })
                                    ->select('product_entries.*', 
                                             'units.name as unit_name',
                                             'products.name as category_name')
                                    ->orderBy('product_entries.id', 'ASC')
                                    ->get();

        $products       = Products::orderBy('products.total_sold', 'DESC')->get();
        $user_info      = userDetails();

        return view('reports::stock_report', compact('data', 'user_info', 'products'));
    }

    public function stockDetailsReport($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $product_id     = isset($_GET['product_id']) ? $_GET['product_id'] : 0;

        $product        = Products::find($id);
        $data           = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                ->when($product_id != 0, function ($query) use ($product_id) {
                                    return $query->where('product_entries.id', $product_id);
                                })
                                ->where('product_entries.product_id', $id)
                                ->orderBy('product_entries.total_sold', 'DESC')
                                ->select('product_entries.*', 'units.name as unit_name')
                                ->get();

        $products       = ProductEntries::where('product_entries.product_id', $id)->orderBy('product_entries.total_sold', 'DESC')->get();
        $products->sortBy('name');
        $user_info      = userDetails();

        return view('reports::stock_details_report', compact('data', 'product', 'user_info', 'products'));
    }

    public function salesReport()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;

        $invoices               = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('invoices.customer_id', $customer_id);
                                                })
                                                ->select('invoices.invoice_date as invoice_date',
                                                    'invoices.id as id',
                                                    'invoices.invoice_number as invoice_number',
                                                    'invoices.customer_id as customer_id',
                                                    'customers.name as customer_name',
                                                    'invoices.invoice_amount as invoice_amount',
                                                    'invoices.return_amount as return_amount',
                                                    'invoices.due_amount as due_amount',
                                                    'invoices.invoice_note as invoice_note',
                                                    'invoices.adjustment_type as adjustment_type',
                                                    'invoices.total_adjustment as total_adjustment',
                                                    'invoices.vat_type as vat_type',
                                                    'invoices.total_vat as total_vat',
                                                    'invoices.tax_type as tax_type',
                                                    'invoices.total_tax as total_tax')
                                                ->get();

        $invoice_entries        = InvoiceEntries::leftjoin('invoices', 'invoices.id', 'invoice_entries.invoice_id')
                                                ->leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->select('invoice_entries.*',
                                                         'product_entries.name as product_entry_name',
                                                         'products.name as product_name',
                                                         'product_entries.product_code as product_code')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('payments.customer_id', $customer_id);
                                                })
                                                ->select('payment_entries.*')
                                                ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->leftjoin('sales_return', 'sales_return.id', 'payment_entries.sales_return_id')
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('payments.customer_id', $customer_id);
                                                })
                                                ->where('payments.type', 2)
                                                ->select('payment_entries.*',
                                                         'sales_return.invoice_id as return_invoice_id')
                                                ->get();

        $total_invoice_amount       = 0;
        $total_sales_return         = 0;
        $total_paid_amount          = 0;
        $total_return_paid          = 0;
        $total_due_amount           = 0;
        $total_adjustment_amount    = 0;
        $total_tax_amount           = 0;
        $total_vat_amount           = 0;
        $total_return_due           = 0;

        if ($invoices->count() > 0)
        {
            foreach ($invoices as $key => $value)
            {
                $sub_total_value                            = $invoice_entries->where('invoice_id', $value['id'])->sum('total_amount');
                $perc_adjustment                            = ($sub_total_value * $value['total_adjustment'])/100;
                $perc_vat                                   = ($sub_total_value * $value['total_vat'])/100;
                $perc_tax                                   = ($sub_total_value * $value['total_tax'])/100;
                $sales_return_paid                          = $payment_entries_return->where('return_invoice_id', $value['id'])->sum('amount');

                $data[$value['id']]['invoice_number']       = 'INV - ' . str_pad($value['invoice_number'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['invoice_date']         = date('d-m-Y', strtotime($value['invoice_date']));
                $data[$value['id']]['invoice_id']           = $value['id'];
                $data[$value['id']]['customer_name']        = $value['customer_name'];
                $data[$value['id']]['invoice_amount']       = $value['invoice_amount'];
                $data[$value['id']]['return_amount']        = $value['return_amount'];
                $data[$value['id']]['paid_amount']          = $payment_entries->where('invoice_id', $value['id'])->sum('amount');
                $data[$value['id']]['return_paid']          = $sales_return_paid;
                $data[$value['id']]['due_amount']           = $value['due_amount'];
                $data[$value['id']]['return_due']           = $value['return_amount'] - $sales_return_paid;
                $data[$value['id']]['invoice_note']         = $value['invoice_note'];
                $data[$value['id']]['adjustment_type']      = $value['adjustment_type'];
                $data[$value['id']]['adjustment']           = $value['total_adjustment'];
                $data[$value['id']]['adjustment_perc']      = $perc_adjustment;
                $data[$value['id']]['vat_type']             = $value['vat_type'];
                $data[$value['id']]['vat']                  = $value['total_vat'];
                $data[$value['id']]['vat_perc']             = $perc_vat;
                $data[$value['id']]['tax_type']             = $value['tax_type'];
                $data[$value['id']]['tax']                  = $value['total_tax'];
                $data[$value['id']]['tax_perc']             = $perc_tax;
                $data[$value['id']]['sub_total']            = $sub_total_value;
                $data[$value['id']]['invoice_entries']      = $invoice_entries->where('invoice_id', $value['id']);

                $total_invoice_amount                       = $total_invoice_amount + $value['invoice_amount'];
                $total_sales_return                         = $total_sales_return + $value['return_amount'];
                $total_paid_amount                          = $total_paid_amount + ($payment_entries->where('invoice_id', $value['id'])->sum('amount'));
                $total_due_amount                           = $total_due_amount + $value['due_amount'];
                $total_adjustment_amount                    = $total_adjustment_amount + ($value['adjustment_type'] == 0 ? $perc_adjustment : $value['total_adjustment']);
                $total_tax_amount                           = $total_tax_amount + ($value['tax_type'] == 0 ? $perc_tax : $value['total_tax']);
                $total_vat_amount                           = $total_vat_amount + ($value['vat_type'] == 0 ? $perc_vat : $value['total_vat']);
                $total_return_due                           = $total_return_due + ($value['return_amount'] - $sales_return_paid); 
                $total_return_paid                          = $total_return_paid + $sales_return_paid;
            }
        }
        else
        {
            $data                                       = [];
            $total_invoice_amount                       = 0;
            $total_paid_amount                          = 0;
            $total_due_amount                           = 0;
            $total_adjustment_amount                    = 0;
            $total_tax_amount                           = 0;
            $total_vat_amount                           = 0;
        }

        $customers              = Customers::get();
        $user_info              = userDetails();

        return view('reports::sales_report', compact('data', 'total_invoice_amount', 'total_sales_return', 'total_return_paid', 'total_paid_amount', 'total_return_due', 'total_due_amount', 'total_adjustment_amount', 'total_tax_amount', 'total_vat_amount', 'from_date', 'to_date', 'user_info', 'customers'));
    }

    public function salesSummary()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;

        $invoices               = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('invoices.customer_id', $customer_id);
                                                })
                                                ->where('invoices.type', 1)
                                                ->select('invoices.invoice_date as invoice_date',
                                                    'invoices.id as id',
                                                    'invoices.invoice_number as invoice_number',
                                                    'invoices.total_discount_type as total_discount_type',
                                                    'invoices.total_discount_note as total_discount_note',
                                                    'invoices.total_discount_amount as total_discount_amount',
                                                    'invoices.total_discount as total_discount',
                                                    'invoices.invoice_amount as invoice_amount',
                                                    'invoices.return_amount as return_amount',
                                                    'invoices.due_amount as due_amount',
                                                    'invoices.cash_given as cash_given',
                                                    'invoices.customer_id as customer_id',
                                                    'customers.name as customer_name')
                                                ->get();

        $total_invoice_amount   = 0;
        $total_discount_amount  = 0;
        $total_receivable       = 0;
        $total_paid_amount      = 0;

        if ($invoices->count() > 0)
        {
            foreach ($invoices as $key => $value)
            {
                if ($value['total_discount_type'] == 0)
                {
                    $sub_total      = $invoice_entries->where('invoice_id', $value['id'])->sum('total_amount');
                    $discount       = ($sub_total*$value['total_discount_amount'])/100;
                }
                else
                {
                    $discount       = $value['total_discount_amount'];
                }

                $data[$value['id']]['invoice_number']           = 'INV - ' . str_pad($value['invoice_number'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['invoice_date']             = date('d-m-Y', strtotime($value['invoice_date']));
                $data[$value['id']]['invoice_id']               = $value['id'];
                $data[$value['id']]['customer_name']            = $value['customer_name'];
                $data[$value['id']]['invoice_amount']           = $value['invoice_amount'];
                $data[$value['id']]['return_amount']            = $value['return_amount'];
                $data[$value['id']]['paid_amount']              = $value['cash_given'];
                $data[$value['id']]['due_amount']               = $value['due_amount'];
                $data[$value['id']]['discount']                 = $value['total_discount'];
                $data[$value['id']]['total_discount']           = $discount;
                $data[$value['id']]['total_discount_note']      = $value['total_discount_note'];
                $data[$value['id']]['total_discount_type']      = $value['total_discount_type'];
                $data[$value['id']]['total_discount_amount']    = $value['total_discount_amount'];

                $total_invoice_amount   = $total_invoice_amount + $value['invoice_amount'];
                $total_discount_amount  = $total_discount_amount + $discount;
                $total_receivable       = $total_receivable + ($value['invoice_amount']);
                $total_paid_amount      = $total_paid_amount + $value['cash_given'];
            }
        }
        else
        {
            $data = [];
        }

        $customers              = Customers::get();
        $user_info              = userDetails();

        return view('reports::sales_summary', compact('data', 'total_invoice_amount', 'total_receivable', 'total_paid_amount', 'from_date', 'to_date', 'user_info', 'customers', 'total_discount_amount'));
    }

    public function profitLoss()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $invoices               = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->select('invoices.id as id',
                                                         'invoices.invoice_amount as invoice_amount',
                                                         'invoices.return_amount as return_amount')
                                                ->get();

        $total_expense          = Expenses::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                                })
                                                ->select('expenses.*')
                                                ->sum('amount');

        $invoice_entries        = InvoiceEntries::leftjoin('invoices', 'invoices.id', 'invoice_entries.invoice_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->select('invoice_entries.*')
                                                ->get();

        $return_entries         = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.invoice_id')
                                                ->select('sales_return_entries.*')
                                                ->get();

        $total_invoice_amount       = 0;
        $total_purchase_costs       = 0;
        $total_return_purchase_cost = 0;

        if ($invoices->count() > 0)
        {
            foreach ($invoices as $key => $value)
            {
                $find_purchase_cost               = $invoice_entries->where('invoice_id', $value['id']);
                $find_return_purchase_cost        = $return_entries->where('invoice_id', $value['id']);

                if (!empty($find_purchase_cost))
                {
                    $sub_total_purchase_cost      = 0;
                    foreach ($find_purchase_cost as $key1 => $value1)
                    {   
                        $sub_total_purchase_cost  = $sub_total_purchase_cost + ($value1['buy_price']*$value1['quantity']);
                    }

                    $total_purchase_costs         = $total_purchase_costs + $sub_total_purchase_cost;
                }

                if (!empty($find_return_purchase_cost))
                {
                    $sub_total_return_purchase_cost         = 0;
                    foreach ($find_return_purchase_cost as $key2 => $value2)
                    {   
                        $sub_total_return_purchase_cost     = $sub_total_return_purchase_cost + ($value2['buy_price']*$value2['quantity']);
                    }

                    $total_return_purchase_cost             = $total_return_purchase_cost + $sub_total_return_purchase_cost;
                }

                    $total_invoice_amount         = $total_invoice_amount + $value['invoice_amount'] - $value['return_amount'];
            }

                    $total_purchase_cost          = $total_purchase_costs - $total_return_purchase_cost;
        }
        else
        {
            $total_purchase_cost        = 0;   
            $total_invoice_amount       = 0;     
        }

        $user_info  = Users::find(1);

        return view('reports::profit_loss', compact('from_date', 'to_date', 'user_info', 'total_invoice_amount', 'total_purchase_cost', 'total_expense'));
    }

    public function purchaseReport()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $supplier_id            = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;

        $bills                  = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')                   
                                                ->where('bills.type', 1)
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('bills.vendor_id', $supplier_id);
                                                })
                                                ->select('bills.bill_date as bill_date',
                                                    'bills.id as id',
                                                    'bills.bill_number as bill_number',
                                                    'bills.vendor_id as vendor_id',
                                                    'customers.name as vendor_name',
                                                    'bills.bill_amount as bill_amount',
                                                    'bills.due_amount as due_amount',
                                                    'bills.return_amount as return_amount',
                                                    'bills.bill_note as bill_note',
                                                    'bills.adjustment_type as adjustment_type',
                                                    'bills.total_adjustment as total_adjustment',
                                                    'bills.vat_type as vat_type',
                                                    'bills.total_vat as total_vat',
                                                    'bills.tax_type as tax_type',
                                                    'bills.total_tax as total_tax')
                                                ->get();

        $bill_entries           = BillEntries::leftjoin('bills', 'bills.id', 'bill_entries.bill_id')
                                                ->leftjoin('products', 'products.id', 'bill_entries.product_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->select('bill_entries.*',
                                                         'product_entries.name as product_entry_name',
                                                         'products.name as product_name',
                                                         'product_entries.product_code as product_code')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                        })
                                        ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('payments.customer_id', $supplier_id);
                                        })
                                        ->select('payment_entries.*')
                                        ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->leftjoin('purchase_return', 'purchase_return.id', 'payment_entries.purchase_return_id')
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('payments.customer_id', $supplier_id);
                                                })
                                                ->where('payments.type', 3)
                                                ->select('payment_entries.*',
                                                         'purchase_return.bill_id as return_bill_id')
                                                ->get();

        $total_bill_amount          = 0;
        $total_paid_amount          = 0;
        $total_due_amount           = 0;
        $total_adjustment_amount    = 0;
        $total_tax_amount           = 0;
        $total_vat_amount           = 0;
        $total_return_due           = 0;
        $total_return_paid          = 0;

        if ($bills->count() > 0)
        {
            foreach ($bills as $key => $value)
            {   
                $sub_total_value                            = $bill_entries->where('bill_id', $value['id'])->sum('total_amount');
                $perc_adjustment                            = ($sub_total_value * $value['total_adjustment'])/100;
                $perc_vat                                   = ($sub_total_value * $value['total_vat'])/100;
                $perc_tax                                   = ($sub_total_value * $value['total_tax'])/100;
                $purchase_return_paid                       = $payment_entries_return->where('return_bill_id', $value['id'])->sum('amount');

                $data[$value['id']]['bill_number']          = 'BILL - ' . str_pad($value['bill_number'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['bill_date']            = date('d-m-Y', strtotime($value['bill_date']));
                $data[$value['id']]['bill_id']              = $value['id'];
                $data[$value['id']]['vendor_name']          = $value['vendor_name'];
                $data[$value['id']]['bill_amount']          = $value['bill_amount'];
                $data[$value['id']]['paid_amount']          = $payment_entries->where('bill_id', $value['id'])->sum('amount');
                $data[$value['id']]['due_amount']           = $value['due_amount'];
                $data[$value['id']]['return_amount']        = $value['return_amount'];
                $data[$value['id']]['return_paid']          = $purchase_return_paid;
                $data[$value['id']]['return_due']           = $value['return_amount'] - $purchase_return_paid;
                $data[$value['id']]['bill_note']            = $value['bill_note'];
                $data[$value['id']]['adjustment_type']      = $value['adjustment_type'];
                $data[$value['id']]['adjustment']           = $value['total_adjustment'];
                $data[$value['id']]['adjustment_perc']      = $perc_adjustment;
                $data[$value['id']]['vat_type']             = $value['vat_type'];
                $data[$value['id']]['vat']                  = $value['total_vat'];
                $data[$value['id']]['vat_perc']             = $perc_vat;
                $data[$value['id']]['tax_type']             = $value['tax_type'];
                $data[$value['id']]['tax']                  = $value['total_tax'];
                $data[$value['id']]['tax_perc']             = $perc_tax;
                $data[$value['id']]['sub_total']            = $sub_total_value;
                $data[$value['id']]['bill_entries']         = $bill_entries->where('bill_id', $value['id']);

                $total_bill_amount                          = $total_bill_amount + $value['bill_amount'];
                $total_paid_amount                          = $total_paid_amount + ($payment_entries->where('bill_id', $value['id'])->sum('amount'));
                $total_due_amount                           = $total_due_amount + $value['due_amount'];
                $total_adjustment_amount                    = $total_adjustment_amount + ($value['adjustment_type'] == 0 ? $perc_adjustment : $value['total_adjustment']);
                $total_tax_amount                           = $total_tax_amount + ($value['tax_type'] == 0 ? $perc_tax : $value['total_tax']);
                $total_vat_amount                           = $total_vat_amount + ($value['vat_type'] == 0 ? $perc_vat : $value['total_vat']);
                $total_return_due                           = $total_return_due + ($value['return_amount'] - $purchase_return_paid);
                $total_return_paid                          = $total_return_paid + $purchase_return_paid;
            }
        }
        else
        {
            $data  =  [];   
        }

        $customers              = Customers::get();
        $user_info              = userDetails();

        return view('reports::purchase_report', compact('data', 'total_bill_amount', 'total_paid_amount', 'total_due_amount', 'total_adjustment_amount', 'total_return_due', 'total_return_paid' , 'total_vat_amount', 'total_tax_amount', 'from_date', 'to_date', 'user_info', 'customers'));
    }

    public function purchaseSummary()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $supplier_id            = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;

        $bills                  = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('bills.vendor_id', $supplier_id);
                                                })
                                                ->where('bills.type', 1)
                                                ->select('bills.bill_date as bill_date',
                                                    'bills.id as id',
                                                    'bills.bills_number as bills_number',
                                                    'bills.total_discount_type as total_discount_type',
                                                    'bills.total_discount_note as total_discount_note',
                                                    'bills.total_discount_amount as total_discount_amount',
                                                    'bills.total_discount as total_discount',
                                                    'bills.bills_amount as bills_amount',
                                                    'bills.return_amount as return_amount',
                                                    'bills.due_amount as due_amount',
                                                    'bills.cash_given as cash_given',
                                                    'bills.vendor_id as customer_id',
                                                    'customers.name as customer_name')
                                                ->get();

        $total_bill_amount      = 0;
        $total_discount_amount  = 0;
        $total_receivable       = 0;
        $total_paid_amount      = 0;

        if ($bills->count() > 0)
        {
            foreach ($bills as $key => $value)
            {   
                if ($value['total_discount_type'] == 0)
                {
                    $sub_total      = $invoice_entries->where('invoice_id', $value['id'])->sum('total_amount');
                    $discount       = ($sub_total*$value['total_discount_amount'])/100;
                }
                else
                {
                    $discount       = $value['total_discount_amount'];
                }

                $data[$value['id']]['invoice_number']           = 'INV - ' . str_pad($value['invoice_number'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['invoice_date']             = date('d-m-Y', strtotime($value['invoice_date']));
                $data[$value['id']]['invoice_id']               = $value['id'];
                $data[$value['id']]['customer_name']            = $value['customer_name'];
                $data[$value['id']]['invoice_amount']           = $value['invoice_amount'];
                $data[$value['id']]['return_amount']            = $value['return_amount'];
                $data[$value['id']]['paid_amount']              = $value['cash_given'];
                $data[$value['id']]['due_amount']               = $value['due_amount'];
                $data[$value['id']]['discount']                 = $value['total_discount'];
                $data[$value['id']]['total_discount']           = $discount;
                $data[$value['id']]['total_discount_note']      = $value['total_discount_note'];
                $data[$value['id']]['total_discount_type']      = $value['total_discount_type'];
                $data[$value['id']]['total_discount_amount']    = $value['total_discount_amount'];

                $total_invoice_amount   = $total_invoice_amount + $value['invoice_amount'];
                $total_discount_amount  = $total_discount_amount + $discount;
                $total_receivable       = $total_receivable + ($value['invoice_amount']);
                $total_paid_amount      = $total_paid_amount + $value['cash_given'];
            }
        }
        else
        {
            $data = [];
        }

        $customers              = Customers::where('branch_id', $branch_id)->get();
        $user_info              = userDetails();

        return view('reports::purchase_summary', compact('data', 'total_bill_amount', 'total_payable', 'total_purchase_return', 'total_paid_amount', 'total_return_paid', 'total_due_amount', 'total_return_due', 'from_date', 'to_date', 'user_info', 'customers', 'total_discount_amount'));
    }

    public function dueReportSupplier()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_info              = userDetails();
        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime('01-01-2020'));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $ledger_type            = isset($_GET['ledger_type']) ? $_GET['ledger_type'] : 0;

        $customers              = Customers::where('contact_type', 1)
                                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('customers.id', $customer_id);
                                            })
                                            ->where('id', '!=', 2)
                                            ->orderBy('name', 'ASC')
                                            ->get();

        $total_balance          = 0;

        if ($customers->count() > 0)
        {
            foreach ($customers as $key => $value)
            {
                if ($ledger_type == 1)
                {
                    if ($value->balance != 0)
                    {
                        $data[$value['id']]['customer_id']      = $value['id'];
                        $data[$value['id']]['customer_name']    = $value['name'];
                        $data[$value['id']]['phone']            = $value['phone'];
                        $data[$value['id']]['address']          = $value['address'];
                        $data[$value['id']]['balance']          = $value->balance;

                        $total_balance           = $total_balance + $value->balance;
                    }
                }
                else
                {
                    $data[$value['id']]['customer_id']      = $value['id'];
                    $data[$value['id']]['customer_name']    = $value['name'];
                    $data[$value['id']]['phone']            = $value['phone'];
                    $data[$value['id']]['address']          = $value['address'];
                    $data[$value['id']]['balance']          = $value->balance;
                    
                    $total_balance           = $total_balance + $value->balance;
                }

                // DB::beginTransaction();

                // try{
                //     if ($value->balance > 0)
                //     {
                //         $branch_id                  = Auth::user()->branch_id;
                //         $data_find                  = Bills::orderBy('created_at', 'DESC')->first();
                //         $bill_number                = $data_find != null ? $data_find['bill_number'] + 1 : 1;

                //         $bill                       = new Bills;
                //         $bill->bill_number          = $bill_number;
                //         $bill->vendor_id            = $value->id;
                //         $bill->bill_date            = date('Y-m-d');
                //         $bill->bill_amount          = $value->balance;
                //         $bill->due_amount           = $value->balance;
                //         $bill->total_discount       = 0;
                //         $bill->type                 = 2;
                //         $bill->branch_id            = $branch_id;
                //         $bill->created_by           = Auth::user()->id;
                //         $bill->save();

                //         //Financial Accounting Part Start
                //         debit($customer_id=$value->id, $date=date('Y-m-d'), $account_id=3, $amount=$value->balance, $note='Supplier Opening Balance', $transaction_head='supplier-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //         credit($customer_id=$value->id, $date=date('Y-m-d'), $account_id=9, $amount=$value->balance, $note='Supplier Opening Balance', $transaction_head='supplier-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //         //Financial Accounting Part End
                //     }

                //     if ($value->balance < 0)
                //     {
                //         $data_find                  = Payments::orderBy('id', 'DESC')->first();
                //         $payment_number             = $data_find != null ? $data_find['payment_number'] + 1 : 1;
                        
                //         $payment                    = new Payments;
                //         $payment->payment_number    = $payment_number;
                //         $payment->customer_id       = $value->id;
                //         $payment->payment_date      = date('Y-m-d');
                //         $payment->amount            = abs($value->balance);
                //         $payment->paid_through      = 1;
                //         $payment->type              = 1;
                //         $payment->created_by        = Auth::user()->id;

                //         if ($payment->save())
                //         { 
                //             //Financial Accounting Start
                //                 debit($customer_id=$value->id, $date=date('Y-m-d'), $account_id=9, $amount=abs($value->balance), $note='Supplier Opening Balance', $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //                 credit($customer_id=$value->id, $date=date('Y-m-d'), $account_id=1, $amount=abs($value->balance), $note='Supplier Opening Balance', $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //             //Financial Accounting End
                //         }
                //     }

                //     DB::commit();
                // }
                // catch (\Exception $exception)
                // {
                //     DB::rollback();
                //     dd($exception);
                // }

            }
        }
        
        if(!empty($data))
        {
            $data = $data;
        }
        else
        {
            $data = [];
        }

        $customer_name    = Customers::find($customer_id);

        return view('reports::supplier_due_report', compact('data', 'total_balance', 'from_date', 'to_date', 'user_info', 'customers', 'customer_name', 'ledger_type'));
    }

    public function dueReportSupplierDetails($supplier_id)
    {
        $user_info          = Users::find(1);
        $customer_name      = Customers::find($supplier_id);
        $branch_id          = $customer_name['branch_id'];
        $date               = date('Y-m-d');
        $from_date          = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date            = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $opening_debit      = JournalEntries::where('date','<', $from_date)
                                ->where('customer_id', $supplier_id)
                                ->whereIn('transaction_head', ['production', 'purchase', 'supplier-opening-balance', 'payment-receive', 'supplier-settlement'])
                                ->where('debit_credit', 1)
                                ->sum('amount');
        $opening_credit     = JournalEntries::where('date','<', $from_date)
                                ->where('customer_id', $supplier_id)
                                ->whereIn('transaction_head', ['payment-made', 'purchase-return', 'discount'])
                                ->where('debit_credit', 0)
                                ->sum('amount');
        $opening_balance    = $opening_debit - $opening_credit;

        $debit  = JournalEntries::whereBetween('journal_entries.date', [$from_date, $to_date])
                                ->where('customer_id', $supplier_id)
                                ->whereIn('transaction_head', ['production', 'purchase', 'supplier-opening-balance', 'payment-receive', 'supplier-settlement'])
                                ->where('debit_credit', 1)
                                ->get();
        $credit = JournalEntries::whereBetween('journal_entries.date', [$from_date, $to_date])
                                ->where('customer_id', $supplier_id)
                                ->whereIn('transaction_head', ['payment-made', 'purchase-return', 'discount'])
                                ->where('debit_credit', 0)
                                ->get();
        $data   = $debit->merge($credit)->sortBy('id');

        return view('reports::supplier_due_report_details', compact('data', 'from_date', 'to_date', 'user_info', 'customer_name', 'opening_balance'));
    }

    public function dueReportSupplierDue()
    {
        $user_info              = userDetails();
        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime('01-01-2020'));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $supplier_id            = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;
        $ledger_type            = 1;

        $due                    = Bills::where('due_amount', '>', 0)->get();

        $customers              = Customers::where('contact_type', 1)
                                            ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('customers.id', $supplier_id);
                                            })
                                            ->where('id', '!=', 2)
                                            ->orderBy('name', 'ASC')
                                            ->get();

        $total_due_amount           = 0;
        $total_advance              = 0;

        if ($customers->count() > 0)
        {
            foreach ($customers as $key => $value)
            {
                $due_amount         = $due->where('vendor_id', $value['id'])->sum('due_amount');

                if ($due_amount > 0)
                {
                    $data[$value['id']]['customer_id']      = $value['id'];
                    $data[$value['id']]['customer_name']    = $value['name'];
                    $data[$value['id']]['supplier_advance'] = $value['supplier_advance_payment'];
                    $data[$value['id']]['phone']            = $value['phone'];
                    $data[$value['id']]['address']          = $value['address'];
                    $data[$value['id']]['due_amount']       = $due_amount;

                    $total_due_amount        = $total_due_amount + $due_amount;
                    $total_advance           = $total_advance + $value['supplier_advance_payment'];
                }
                
            }
        }
        else
        {
            $data  =  [];   
        }
        
        if(!empty($data))
        {
            $data = $data;
        }
        else
        {
            $data  = [];
        }

        $customer_name    = Customers::find($supplier_id);
        
        return view('reports::supplier_due_report_due', compact('data', 'from_date', 'to_date', 'user_info', 'customers', 'customer_name', 'ledger_type', 'total_advance', 'total_due_amount'));
    }

    public function expenseReport()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $account_id             = isset($_GET['account_id']) ? $_GET['account_id'] : 0;

        $expenses               = Expenses::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                            })
                                            ->when($account_id != 0, function ($query) use ($account_id) {
                                                return $query->where('expenses.account_id', $account_id);
                                            })
                                            ->select('expenses.*')
                                            ->get();

        $accounts               = Accounts::where('parent_account_type_id',4)
                                            ->whereNotIn('id', [2,3,4,5,6,7,8,9,10,11])
                                            ->where('status', 1)
                                            ->get();

        $user_info  = Users::find(1);

        return view('reports::expense_report', compact('expenses', 'from_date', 'to_date', 'user_info', 'accounts', 'account_id'));
    }

    public function incomeReport()
    {
        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $account_id             = isset($_GET['account_id']) ? $_GET['account_id'] : 0;

        $incomes                = Incomes::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                return $query->whereBetween('incomes.income_date', [$from_date, $to_date]);
                                            })
                                            ->when($account_id != 0, function ($query) use ($account_id) {
                                                return $query->where('incomes.account_id', $account_id);
                                            })
                                            ->select('incomes.*')
                                            ->get();

        $accounts               = Accounts::where('parent_account_type_id',3)
                                            ->whereNotIn('id', [2,3,4,5,6,7,8,9,10,11])
                                            ->where('status', 1)
                                            ->get();

        $user_info  = Users::find(1);

        return view('reports::income_report', compact('incomes', 'from_date', 'to_date', 'user_info', 'accounts', 'account_id'));
    }

    public function salaryReport()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $employee_id            = isset($_GET['employee_id']) ? $_GET['employee_id'] : 0;

        $expenses               = Expenses::leftjoin('customers', 'customers.id', 'expenses.user_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                                })
                                                ->when($employee_id != 0, function ($query) use ($employee_id) {
                                                    return $query->where('expenses.user_id', $employee_id);
                                                })
                                                ->select('expenses.*',
                                                    'customers.name as employee_name',
                                                    'customers.joining_date as joining_date',
                                                    'customers.designation as designation',
                                                    'customers.salary as salary')
                                                ->get();

        $employees              = Customers::where('customers.contact_type', 3)
                                                ->orderBy('created_at', 'ASC')
                                                ->get();

        $total_service_charge   = 0;
        $total_salary           = 0;
        $total_amount           = 0;

        if ($expenses->count() > 0)
        {
            foreach ($employees as $key => $value)
            {   
                $service_charge     = $expenses->where('user_id', $value['id'])
                                                ->where('salary_type', 1)
                                                ->sum('amount');

                $salary             = $expenses->where('user_id', $value['id'])
                                                ->where('salary_type', 0)
                                                ->sum('amount');

                $amount             = $service_charge + $salary;


                $data[$value['id']]['employee_name']    = $value['name'];
                $data[$value['id']]['joining_date']     = $value['joining_date'];
                $data[$value['id']]['designation']      = $value['designation'];

                $data[$value['id']]['service_charge']   = $service_charge;
                $data[$value['id']]['salary']           = $salary;
                $data[$value['id']]['amount']           = $amount;

                $total_service_charge                   = $total_service_charge + $service_charge;
                $total_salary                           = $total_salary + $salary;
                $total_amount                           = $total_amount + $amount;
            }
        }
        else
        {
            $data   =  [];   
        }

        $user_info  = Users::find(1);

        return view('reports::salary_report', compact('data', 'total_service_charge', 'total_salary', 'total_amount', 'from_date', 'to_date', 'user_info', 'employees'));
    }

    public function collectionReport()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $tables                 = TableNameByUsers();
        $table_id               = Auth::user()->associative_contact_id;

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $type_id                = isset($_GET['type_id']) ? $_GET['type_id'] : 0;

        $invoices               = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->select('invoices.id as id',
                                                    'invoices.invoice_date as invoice_date',
                                                    'invoices.invoice_amount as invoice_amount',
                                                    'invoices.return_amount as return_amount',
                                                    'invoices.total_buy_price as total_buy_price',
                                                    'invoices.due_amount as due_amount')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                        })
                                        ->select('payment_entries.*')
                                        ->get();

        $expenses               = Expenses::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                        })
                                        ->select('expenses.*')
                                        ->get();

        $return_entries         = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.sales_return_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('sales_return.sales_return_date', [$from_date, $to_date]);
                                        })
                                        ->select('sales_return_entries.*',
                                                 'sales_return.sales_return_date as sales_return_date'
                                                 )
                                        ->get();

        $sales_return_entries   = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.invoice_id')
                                        ->select('sales_return_entries.*',
                                                 'sales_return.sales_return_date as sales_return_date')
                                        ->get();

        if ($type_id == 1 || $type_id == 0)
        {   
            $startDate          = new Carbon($from_date);
            $endDate            = new Carbon($to_date);
            $all_dates          = array();

            while ($startDate->lte($endDate))
            {
                $all_dates[] = $startDate->toDateString();
                $startDate->addDay();
            }

            if (!empty($all_dates))
            {   
                $total_invoice_amount                   = 0;
                $total_return_amount                    = 0;
                $total_due_amount                       = 0;
                $total_purchase_cost                    = 0;
                $total_expense                          = 0;
                $total_profit_loss                      = 0;

                foreach ($all_dates as $key => $value)
                {   
                    $invoices_purchase_cost             = $invoices->where('invoice_date', $value);

                    $total_return_purchase_cost         = 0;
                    foreach ($invoices_purchase_cost as $key1 => $value1)
                    {
                        $find_return_purchase_cost        = $sales_return_entries->where('invoice_id', $value1['id']);

                        if (!empty($find_return_purchase_cost))
                        {
                            $sub_total_return_purchase_cost      = 0;
                            foreach ($find_return_purchase_cost as $key2 => $value2)
                            {   
                                $sub_total_return_purchase_cost  = $sub_total_return_purchase_cost + ($value2['buy_price']*$value2['quantity']);
                            }

                            $total_return_purchase_cost          = $total_return_purchase_cost + $sub_total_return_purchase_cost;
                        }
                    }

                    $invoice_amount                     = $invoices->where('invoice_date', $value)->sum('invoice_amount');
                    $sales_return                       = $invoices->where('invoice_date', $value)->sum('return_amount');
                    $return_amount                      = $return_entries->where('sales_return_date', $value)->sum('total_amount');
                    $due_amount                         = $invoices->where('invoice_date', $value)->sum('due_amount');
                    $purchase_cost                      = $invoices->where('invoice_date', $value)->sum('total_buy_price');
                    $expense                            = $expenses->where('expense_date', $value)->sum('amount');

                    $invoice_amount_diff                = $invoice_amount - $sales_return;
                    $purchase_cost_diff                 = $purchase_cost - $total_return_purchase_cost;

                    $data[$value]['invoice_amount']     = $invoice_amount - $sales_return;
                    $data[$value]['return_amount']      = $return_amount;
                    $data[$value]['purchase_cost']      = $purchase_cost - $total_return_purchase_cost;
                    $data[$value]['due_amount']         = $due_amount;
                    $data[$value]['paid_amount']        = $invoice_amount - $due_amount;
                    $data[$value]['expense']            = $expense;
                    $data[$value]['profit_loss']        = $invoice_amount_diff - $purchase_cost_diff - $expense;

                    $total_invoice_amount               = $total_invoice_amount + ($invoice_amount - $sales_return);
                    $total_return_amount                = $total_return_amount + $sales_return;
                    $total_purchase_cost                = $total_purchase_cost + ($purchase_cost - $total_return_purchase_cost);
                    $total_due_amount                   = $total_due_amount + $due_amount;
                    $total_expense                      = $total_expense + $expense;
                    $total_profit_loss                  = $total_profit_loss + ($invoice_amount_diff - $purchase_cost_diff - $expense);
                }
            }
            else
            {
                $data = [];
            }
        }
        else
        {   
            $date                   = date('Y-m-d');
            $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
            $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

            $startDate              = new Carbon($from_date);
            $endDate                = new Carbon($to_date);
            $all_dates              = array();

            while ($startDate->lte($endDate))
            {
                $all_dates[] = $startDate->toDateString();
                $startDate->addDay();
            }

            if (!empty($all_dates))
            {   
                foreach ($all_dates as $key1 => $value1)
                {
                    $date_explode   = explode('-', $value1);
                    $date_format[]  = $date_explode[0].'-'.$date_explode[1];
                }

                $required_date      = array_unique($date_format);

                $total_invoice_amount                   = 0;
                $total_return_amount                    = 0;
                $total_due_amount                       = 0;
                $total_purchase_cost                    = 0;
                $total_expense                          = 0;
                $total_profit_loss                      = 0;

                foreach ($required_date as $key2 => $value2)
                {   
                    $invoice_amount                     = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                            })
                                                            ->where(DB::raw("(DATE_FORMAT(invoice_date,'%Y-%m'))"), $value2)
                                                            ->sum('invoices.invoice_amount');

                    $return_amount                      = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                            })
                                                            ->where(DB::raw("(DATE_FORMAT(invoice_date,'%Y-%m'))"), $value2)
                                                            ->sum('invoices.return_amount');

                    $due_amount                         = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                            })
                                                            ->where(DB::raw("(DATE_FORMAT(invoice_date,'%Y-%m'))"), $value2)
                                                            ->sum('invoices.due_amount');

                    $purchase_cost                      = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                            })
                                                            ->where(DB::raw("(DATE_FORMAT(invoice_date,'%Y-%m'))"), $value2)
                                                            ->sum('invoices.total_buy_price');

                    $sales_return                       = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.sales_return_id')
                                                            ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('sales_return.sales_return_date', [$from_date, $to_date]);
                                                            })
                                                            ->where(DB::raw("(DATE_FORMAT(sales_return_date,'%Y-%m'))"), $value2)
                                                            ->select('sales_return_entries.*')
                                                            ->get();

                    if (!empty($sales_return) && ($sales_return->count() > 0))
                    {   
                        $total_sales_return = 0;
                        foreach ($sales_return as $key => $value)
                        {
                            $total_sales_return = $total_sales_return + ($value['buy_price']*$value['quantity']);
                        }
                    }

                    $sales_return                       = $total_sales_return;
                    $expense                            = Expenses::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                                            })
                                                            ->where(DB::raw("(DATE_FORMAT(expense_date,'%Y-%m'))"), $value2)
                                                            ->sum('expenses.amount');

                    $return_entries                     = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.sales_return_id')
                                                            ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('sales_return.sales_return_date', [$from_date, $to_date]);
                                                            })
                                                            ->where(DB::raw("(DATE_FORMAT(sales_return_date,'%Y-%m'))"), $value2)
                                                            ->sum('sales_return.return_amount');

                    $invoices                           = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                            })
                                                            ->select('invoices.id as id',
                                                                'invoices.invoice_date as invoice_date',
                                                                'invoices.invoice_amount as invoice_amount',
                                                                'invoices.return_amount as return_amount',
                                                                'invoices.total_buy_price as total_buy_price',
                                                                'invoices.due_amount as due_amount')
                                                            ->get();


                    $invoice_amount_diff                = $invoice_amount - $return_amount;
                    $purchase_cost_diff                 = $purchase_cost - $sales_return;

                    $data[$value2]['invoice_amount']    = $invoice_amount_diff;
                    $data[$value2]['return_amount']     = $return_entries;
                    $data[$value2]['purchase_cost']     = $purchase_cost_diff;
                    $data[$value2]['due_amount']        = $due_amount;
                    $data[$value2]['paid_amount']       = $invoice_amount - $due_amount;
                    $data[$value2]['expense']           = $expense;
                    $data[$value2]['profit_loss']       = $invoice_amount_diff - $purchase_cost_diff - $expense;

                    $total_invoice_amount               = $total_invoice_amount + $invoice_amount_diff;
                    $total_return_amount                = $total_return_amount + $return_amount;
                    $total_purchase_cost                = $total_purchase_cost + $purchase_cost_diff;
                    $total_due_amount                   = $total_due_amount + $due_amount;
                    $total_expense                      = $total_expense + $expense;
                    $total_profit_loss                  = $total_profit_loss + ($invoice_amount_diff - $purchase_cost_diff - $expense);
                }
            }
            else
            {
                $datas = [];
            }  
        }                             

        $customers              = Customers::get();
        $user_info              = userDetails();

        return view('reports::collection_report', compact('type_id', 'data', 'total_invoice_amount', 'total_return_amount', 'total_due_amount', 'total_purchase_cost', 'total_expense', 'total_profit_loss', 'from_date', 'to_date', 'user_info', 'customers'));
    }

    public function salesSummaryReduced()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_info              = userDetails();

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;

        $invoices               = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('invoices.customer_id', $customer_id);
                                                })
                                                ->where('invoices.invoice_type', 1)
                                                ->select('invoices.invoice_date as invoice_date',
                                                    'invoices.id as id',
                                                    'invoices.invoice_number as invoice_number',
                                                    'invoices.customer_id as customer_id',
                                                    'invoices.adjustment_type as adjustment_type',
                                                    'invoices.adjustment_note as adjustment_note',
                                                    'customers.name as customer_name',
                                                    'invoices.invoice_amount as invoice_amount',
                                                    'invoices.return_amount as return_amount',
                                                    'invoices.due_amount as due_amount',
                                                    'invoices.total_adjustment as total_adjustment')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                        })
                                        ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('payments.customer_id', $customer_id);
                                        })
                                        ->select('payment_entries.*')
                                        ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->leftjoin('sales_return', 'sales_return.id', 'payment_entries.sales_return_id')
                                        ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('payments.customer_id', $customer_id);
                                        })
                                        ->where('payments.type', 2)
                                        ->select('payment_entries.*',
                                                 'sales_return.invoice_id as return_invoice_id')
                                        ->get();

        $invoice_entries        = InvoiceEntries::leftjoin('invoices', 'invoices.id', 'invoice_entries.invoice_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                        })
                                        ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('invoice_entries.customer_id', $customer_id);
                                        })
                                        ->where('invoices.invoice_type', 1)
                                        ->select('invoice_entries.*')
                                        ->get();

        $total_invoice_amount   = 0;
        $total_sales_return     = 0;
        $total_receivable       = 0;
        $total_paid_amount      = 0;
        $total_return_paid      = 0;
        $total_due_amount       = 0;
        $total_return_due       = 0;

        if ($invoices->count() > 0)
        {
            foreach ($invoices as $key => $value)
            {   
                if ($value['adjustment_type'] == 0)
                {
                    $sub_total   = $invoice_entries->where('invoice_id', $value['id'])->sum('total_amount');
                    $adjustment  = ($sub_total*$value['total_adjustment'])/100;
                }
                else
                {
                    $adjustment  = $value['total_adjustment'];
                }

                $sales_return_paid                      = $payment_entries_return->where('return_invoice_id', $value['id'])->sum('amount');

                $data[$value['id']]['invoice_number']   = 'INV - ' . str_pad($value['invoice_number'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['invoice_date']     = date('d-m-Y', strtotime($value['invoice_date']));
                $data[$value['id']]['invoice_id']       = $value['id'];
                $data[$value['id']]['customer_name']    = $value['customer_name'];
                $data[$value['id']]['invoice_amount']   = $value['invoice_amount'] - $adjustment;
                $data[$value['id']]['return_amount']    = $value['return_amount'];
                $data[$value['id']]['paid_amount']      = $payment_entries->where('invoice_id', $value['id'])->sum('amount');
                $data[$value['id']]['return_paid']      = $sales_return_paid;
                $data[$value['id']]['return_due']       = $value['return_amount'] - $sales_return_paid;
                $data[$value['id']]['due_amount']       = $value['due_amount'];
                $data[$value['id']]['adjustment']       = $adjustment;
                $data[$value['id']]['adjustment_note']  = $value['adjustment_note'];
                $data[$value['id']]['adjustment_type']  = $value['adjustment_type'];
                $data[$value['id']]['total_adjustment'] = $value['total_adjustment'];

                $total_invoice_amount                   = $total_invoice_amount + $value['invoice_amount'] - $adjustment;
                $total_sales_return                     = $total_sales_return + $value['return_amount'];
                $total_receivable                       = $total_receivable + ($value['invoice_amount'] -$value['return_amount']);
                $total_paid_amount                      = $total_paid_amount + $payment_entries->where('invoice_id', $value['id'])->sum('amount');
                $total_return_paid                      = $total_return_paid + $sales_return_paid;
                $total_due_amount                       = $total_due_amount + $value['due_amount'] - $adjustment;
                $total_return_due                       = $total_return_due + ($value['return_amount'] - $sales_return_paid);
            }

            $total_invoice_amount       = $total_invoice_amount*($user_info['sales_show']/100);
        }
        else
        {
            $data = [];
        }

        $customers                      = Customers::where('customers.contact_type', 0)
                                                ->orWhere('customers.contact_type', 1)
                                                ->orderBy('created_at', 'ASC')
                                                ->get();

        return view('reports::sales_summary_reduced', compact('data', 'total_invoice_amount', 'total_sales_return', 'total_receivable', 'total_paid_amount', 'total_return_paid', 'total_due_amount', 'total_return_due', 'from_date', 'to_date', 'user_info', 'customers'));
    }

    public function profitLossReduced()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_info              = userDetails();

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $invoices               = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->where('invoices.invoice_type', 1)
                                                ->select('invoices.id as id',
                                                         'invoices.invoice_amount as invoice_amount',
                                                         'invoices.return_amount as return_amount')
                                                ->get();

        $total_expense          = Expenses::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                                })
                                                ->select('expenses.*')
                                                ->sum('amount');

        $invoice_entries        = InvoiceEntries::leftjoin('invoices', 'invoices.id', 'invoice_entries.invoice_id')
                                        ->where('invoices.invoice_type', 1)
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                        })
                                        ->select('invoice_entries.*')
                                        ->get();

        $return_entries         = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.invoice_id')
                                        ->select('sales_return_entries.*')
                                        ->get();

        $total_purchase_cost    = Bills::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->select('bills.*')
                                                ->sum('bill_amount');

        $total_purchase_cost    = $total_purchase_cost*($user_info['sales_show']/100);

        $total_invoice_amount       = 0;

        if ($invoices->count() > 0)
        {
            foreach ($invoices as $key => $value)
            {
                $total_invoice_amount             = $total_invoice_amount + $value['invoice_amount'] - $value['return_amount'];
            }

            $total_invoice_amount       = $total_invoice_amount*($user_info['sales_show']/100);
        }
        else
        {     
            $total_invoice_amount       = 0;     
        }

        return view('reports::profit_loss_reduced', compact('from_date', 'to_date', 'user_info', 'total_invoice_amount', 'total_purchase_cost', 'total_expense'));
    }
    
    public function purchaseReportReduced()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_info              = userDetails();

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $supplier_id            = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;

        $bills                  = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('bills.vendor_id', $supplier_id);
                                                })
                                                ->select('bills.bill_date as bill_date',
                                                    'bills.id as id',
                                                    'bills.bill_number as bill_number',
                                                    'bills.vendor_id as customer_id',
                                                    'customers.name as customer_name',
                                                    'bills.adjustment_type as adjustment_type',
                                                    'bills.total_adjustment as total_adjustment',
                                                    'bills.adjustment_note as adjustment_note',
                                                    'bills.bill_amount as bill_amount',
                                                    'bills.return_amount as return_amount',
                                                    'bills.due_amount as due_amount',
                                                    'bills.total_adjustment as total_adjustment')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                        })
                                        ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('payments.customer_id', $supplier_id);
                                        })
                                        ->select('payment_entries.*')
                                        ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->leftjoin('purchase_return', 'purchase_return.id', 'payment_entries.purchase_return_id')
                                        ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('payments.customer_id', $supplier_id);
                                        })
                                        ->where('payments.type', 3)
                                        ->select('payment_entries.*',
                                                 'purchase_return.bill_id as return_bill_id')
                                        ->get();
                                                                        
        $bill_entries           = BillEntries::leftjoin('bills', 'bills.id', 'bill_entries.bill_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                        })
                                        ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('bill_entries.vendor_id', $supplier_id);
                                        })
                                        ->select('bill_entries.*')
                                        ->get();

        $total_bill_amount      = 0;
        $total_purchase_return  = 0;
        $total_payable          = 0;
        $total_paid_amount      = 0;
        $total_return_paid      = 0;
        $total_due_amount       = 0;
        $total_return_due       = 0;

        if ($bills->count() > 0)
        {
            foreach ($bills as $key => $value)
            {   
                if ($value['adjustment_type'] == 0)
                {
                    $sub_total   = $bill_entries->where('bill_id', $value['id'])->sum('total_amount');
                    $adjustment  = ($sub_total*$value['total_adjustment'])/100;
                }
                else
                {
                    $adjustment  = $value['total_adjustment'];
                }

                $purchase_return_paid                   = $payment_entries_return->where('return_bill_id', $value['id'])->sum('amount');

                $data[$value['id']]['bill_number']      = 'BILL - ' . str_pad($value['bill_number'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['bill_date']        = date('d-m-Y', strtotime($value['bill_date']));
                $data[$value['id']]['bill_id']          = $value['id'];
                $data[$value['id']]['customer_name']    = $value['customer_name'];
                $data[$value['id']]['bill_amount']      = $value['bill_amount'] - $adjustment;
                $data[$value['id']]['return_amount']    = $value['return_amount'];
                $data[$value['id']]['paid_amount']      = $payment_entries->where('bill_id', $value['id'])->sum('amount');
                $data[$value['id']]['return_paid']      = $purchase_return_paid;
                $data[$value['id']]['return_due']       = $value['return_amount'] - $purchase_return_paid;
                $data[$value['id']]['due_amount']       = $value['due_amount'];
                $data[$value['id']]['adjustment']       = $adjustment;
                $data[$value['id']]['adjustment_note']  = $value['adjustment_note'];
                $data[$value['id']]['adjustment_type']  = $value['adjustment_type'];
                $data[$value['id']]['total_adjustment'] = $value['total_adjustment'];

                $total_bill_amount                      = $total_bill_amount + $value['bill_amount'] - $adjustment;
                $total_purchase_return                  = $total_purchase_return + $value['return_amount'];
                $total_payable                          = $total_payable + ($value['bill_amount'] -$value['return_amount']);
                $total_paid_amount                      = $total_paid_amount + $payment_entries->where('bill_id', $value['id'])->sum('amount');
                $total_return_paid                      = $total_return_paid + $purchase_return_paid;
                $total_due_amount                       = $total_due_amount + $value['due_amount'];
                $total_return_due                       = $total_return_due + ($value['return_amount'] - $purchase_return_paid);
            }

            $total_bill_amount   = $total_bill_amount*($user_info['sales_show']/100);
        }
        else
        {
            $data  =  [];   
        }

        $customers              = $tables['customers']->get();

        return view('reports::purchase_summary_reduced', compact('data', 'total_bill_amount', 'total_payable', 'total_purchase_return', 'total_paid_amount', 'total_return_paid', 'total_due_amount', 'total_return_due', 'from_date', 'to_date', 'user_info', 'customers'));
    }

    //New Reports Developed At Cyberdyne Technology Ltd.
    public function salesStatement()
    {
        $smList = Customers::where('contact_type',4)->get();
        $areaList = Areas::all();
        return view('reports::sales_statement',compact('smList','areaList'));
    }

    public function salesStatementPrint()
    {
        $user_info              = userDetails();

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $invoice_number         = isset($_GET['invoice_number']) ? $_GET['invoice_number'] : 0;
        $category_id            = isset($_GET['category_id']) ? $_GET['category_id'] : 0;
        $product_code           = isset($_GET['product_code']) ? $_GET['product_code'] : 0;
        $product_id             = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $reference_id           = isset($_GET['reference_id']) ? $_GET['reference_id'] : 0;
        $user_id                = isset($_GET['user_id']) ? $_GET['user_id'] : 0;
        $sales_return           = isset($_GET['sales_return']) ? $_GET['sales_return'] : 0;
        $sr_id                  = isset($_GET['sm_id']) ? $_GET['sm_id'] : 0;

        $data1  = Invoices::when($customer_id != 0, function ($query) use ($customer_id) {
                                return $query->where('invoices.customer_id', $customer_id);
                            })
                            ->when($invoice_number != 0, function ($query) use ($invoice_number) {
                                return $query->where('invoices.id', $invoice_number);
                            })
                            
                            ->when($sr_id != 0, function ($query) use ($sr_id) {
                                return $query->where('invoices.sr_id', $sr_id);
                            })
                            
                            ->when($reference_id != 0, function ($query) use ($reference_id) {
                                return $query->where('invoices.reference_id', $reference_id);
                            })
                            ->when($user_id != 0, function ($query) use ($user_id) {
                                return $query->where('invoices.created_by', $user_id);
                            })
                            ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                            })
                            ->where('invoices.type', 1)
                            ->select('invoices.*')
                            ->with(['invoiceEntries' => function($query) use($category_id, $product_code, $product_id) {
                                    $query->when($category_id != 0, function ($query) use ($category_id) {
                                    return $query->where('invoice_entries.product_id', $category_id);
                                    });

                                    $query->when($product_code != 0, function ($query) use ($product_code) {
                                    return $query->where('invoice_entries.product_entry_id', $product_code);
                                    });

                                    $query->when($product_id != 0, function ($query) use ($product_id) {
                                    return $query->where('invoice_entries.product_entry_id', $product_id);
                                    });
                            }])
                            ->orderBy('invoices.created_at', 'ASC')
                            ->get();

        $data2  = SalesReturn::when($customer_id != 0, function ($query) use ($customer_id) {
                                return $query->where('sales_return.customer_id', $customer_id);
                            })
                            ->when($user_id != 0, function ($query) use ($user_id) {
                                return $query->where('sales_return.created_by', $user_id);
                            })
                            ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                return $query->whereBetween('sales_return.sales_return_date', [$from_date, $to_date]);
                            })
                            ->select('sales_return.*')
                            ->with(['salesReturnEntries' => function($query) use($category_id, $product_code, $product_id) {
                                    $query->when($category_id != 0, function ($query) use ($category_id) {
                                    return $query->where('sales_return_entries.product_id', $category_id);
                                    });

                                    $query->when($product_code != 0, function ($query) use ($product_code) {
                                    return $query->where('sales_return_entries.product_entry_id', $product_code);
                                    });

                                    $query->when($product_id != 0, function ($query) use ($product_id) {
                                    return $query->where('sales_return_entries.product_entry_id', $product_id);
                                    });
                            }])
                            ->orderBy('sales_return.created_at', 'ASC')
                            ->get();

        $data               = $data1->merge($data2);

        $item_category_name     = Products::find($category_id);
        $sales_by_name          = Users::find($user_id);
        $sales_id               = Invoices::find($invoice_number);
        $customer_name          = Customers::find($customer_id);
        $sm_name                = Customers::find($sr_id);
        $reference              = Customers::find($reference_id);

        return view('reports::sales_statement_print', compact('user_info', 'from_date', 'to_date', 'data', 'item_category_name', 'sales_return', 'sales_by_name', 'sales_id', 'customer_name','sm_name', 'reference', 'data'));
    }

    public function purchaseStatement()
    {
        return view('reports::purchase_statement');
    }

    public function purchaseStatementPrint()
    {
        $user_info              = userDetails();

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $supplier_id            = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;
        $bill_number            = isset($_GET['bill_number']) ? $_GET['bill_number'] : 0;
        $category_id            = isset($_GET['category_id']) ? $_GET['category_id'] : 0;
        $product_code           = isset($_GET['product_code']) ? $_GET['product_code'] : 0;
        $product_id             = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $user_id                = isset($_GET['user_id']) ? $_GET['user_id'] : 0;
        $purchase_return        = isset($_GET['purchase_return']) ? $_GET['purchase_return'] : 0;

        $bills                  = Bills::leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                                ->leftjoin('customers', 'customers.id', 'bill_entries.vendor_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                                ->where('bills.type', 1)
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->orWhere('bills.vendor_id', $supplier_id);
                                                })
                                                ->when($bill_number != 0, function ($query) use ($bill_number) {
                                                    return $query->orWhere('bills.id', $bill_number);
                                                })
                                                ->when($category_id != 0, function ($query) use ($category_id) {
                                                    return $query->orWhere('product_entries.product_id', $category_id);
                                                })
                                                ->when($product_code != 0, function ($query) use ($product_code) {
                                                    return $query->orWhere('product_entries.id', $product_code);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->orWhere('product_entries.id', $product_id);
                                                })
                                                ->when($user_id != 0, function ($query) use ($user_id) {
                                                    return $query->orWhere('bills.created_by', $user_id);
                                                })
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->select('bills.bill_date as bill_date',
                                                    'bills.id as id',
                                                    'bills.bill_number as bill_number',
                                                    'bills.vendor_id as vendor_id',
                                                    'customers.name as customer_name',
                                                    'bills.bill_amount as bill_amount',
                                                    'bills.return_amount as return_amount',
                                                    'bills.due_amount as due_amount',
                                                    'bills.bill_note as bill_note',
                                                    'bills.total_discount_type as total_discount_type',
                                                    'bills.total_discount_amount as total_discount_amount',
                                                    'bills.vat_type as vat_type',
                                                    'bills.total_vat as total_vat',
                                                    'bills.tax_type as tax_type',
                                                    'bills.total_tax as total_tax')
                                                ->get();

        $bill_entries           = BillEntries::leftjoin('bills', 'bills.id', 'bill_entries.bill_id')
                                                ->leftjoin('products', 'products.id', 'bill_entries.product_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                                ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->select('bill_entries.*',
                                                         'product_entries.name as product_entry_name',
                                                         'products.name as product_name',
                                                         'product_entries.product_code as product_code',
                                                         'units.name as unit_name')
                                                ->get();

        $purchase_returns       = PurchaseReturn::leftjoin('purchase_return_entries', 'purchase_return_entries.purchase_return_id', 'purchase_return.id')
                                                ->leftjoin('customers', 'customers.id', 'purchase_return_entries.customer_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'purchase_return_entries.product_entry_id')
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->orWhere('purchase_return.customer_id', $supplier_id);
                                                })
                                                ->when($category_id != 0, function ($query) use ($category_id) {
                                                    return $query->orWhere('product_entries.product_id', $category_id);
                                                })
                                                ->when($product_code != 0, function ($query) use ($product_code) {
                                                    return $query->orWhere('product_entries.id', $product_code);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->orWhere('product_entries.id', $product_id);
                                                })
                                                ->when($user_id != 0, function ($query) use ($user_id) {
                                                    return $query->orWhere('purchase_return.created_by', $user_id);
                                                })
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('purchase_return.purchase_return_date', [$from_date, $to_date]);
                                                })
                                                ->select('purchase_return.purchase_return_date as purchase_return_date',
                                                    'purchase_return.id as id',
                                                    'purchase_return.purchase_return_number as purchase_return_number',
                                                    'purchase_return.bill_id as bill_id',
                                                    'purchase_return.customer_id as customer_id',
                                                    'customers.name as customer_name',
                                                    'purchase_return.sub_total_amount as sub_total_amount',
                                                    'purchase_return.return_amount as return_amount',
                                                    'purchase_return.due_amount as due_amount',
                                                    'purchase_return.return_note as return_note',
                                                    'purchase_return.total_discount_type as total_discount_type',
                                                    'purchase_return.total_discount_amount as total_discount_amount',
                                                    'purchase_return.vat_type as vat_type',
                                                    'purchase_return.total_vat as total_vat')
                                                ->get();

        $purchase_return_entries= PurchaseReturnEntries::leftjoin('purchase_return', 'purchase_return.id', 'purchase_return_entries.purchase_return_id')
                                                ->leftjoin('products', 'products.id', 'purchase_return_entries.product_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'purchase_return_entries.product_entry_id')
                                                ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('purchase_return.purchase_return_date', [$from_date, $to_date]);
                                                })
                                                ->select('purchase_return_entries.*',
                                                         'product_entries.name as product_entry_name',
                                                         'products.name as product_name',
                                                         'product_entries.product_code as product_code',
                                                         'units.name as unit_name')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                                })
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('payments.customer_id', $supplier_id);
                                                })
                                                ->select('payment_entries.*')
                                                ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->leftjoin('purchase_return', 'purchase_return.id', 'payment_entries.purchase_return_id')
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('payments.customer_id', $supplier_id);
                                                })
                                                ->where('payments.type', 2)
                                                ->select('payment_entries.*',
                                                         'purchase_return.bill_id as return_bill_id')
                                                ->get();

        if ($bills->count() > 0)
        {
            if ((isset($_GET['purchase_return']) && $_GET['purchase_return'] == 1) || (isset($_GET['purchase_return']) && $_GET['purchase_return'] == 0))
            {
                foreach ($bills as $key => $value)
                {
                    $sub_total_value                             = $bill_entries->where('bill_id', $value['id'])->sum('total_amount');
                    $perc_vat                                    = ($sub_total_value * $value['total_vat'])/100;
                    $perc_total_discount                         = (($sub_total_value + $perc_vat)*$value['total_discount_amount'])/100;

                    $data1[$value['id']]['bill_number']          = 'BILL - ' . str_pad($value['bill_number'], 6, "0", STR_PAD_LEFT);
                    $data1[$value['id']]['bill_date']            = date('d-m-Y', strtotime($value['bill_date']));
                    $data1[$value['id']]['customer_name']        = $value['customer_name'];
                    $data1[$value['id']]['paid_amount']          = $payment_entries->where('bill_id', $value['id'])->sum('amount');
                    $data1[$value['id']]['vat_type']             = $value['vat_type'];
                    $data1[$value['id']]['vat']                  = $value['total_vat'];
                    $data1[$value['id']]['vat_perc']             = $perc_vat;
                    $data1[$value['id']]['total_discount_type']  = $value['total_discount_type'];
                    $data1[$value['id']]['total_discount']       = $value['total_discount_amount'];
                    $data1[$value['id']]['total_discount_perc']  = $perc_total_discount;
                    $data1[$value['id']]['sub_total']            = $sub_total_value;
                    $data1[$value['id']]['type']                 = 'P';

                    if ($value['vat_type'] == 0)
                    {
                        $net_payable_vat                        = $perc_vat;
                    }
                    else
                    {
                        $net_payable_vat                        = $value['total_vat'];
                    }

                    if ($value['total_discount_type'] == 0)
                    {
                        $net_payable_total_discount             = $perc_total_discount;
                    }
                    else
                    {
                        $net_payable_total_discount             = $value['total_discount_amount'];
                    }

                    $data1[$value['id']]['net_payable']          = $sub_total_value + $net_payable_vat - $net_payable_total_discount;
                    $data1[$value['id']]['total_buy_price']      = $value['total_buy_price'];
                    $data1[$value['id']]['profit_loss']          = ($sub_total_value + $net_payable_vat - $net_payable_total_discount) - $value['total_buy_price'];
                    $data1[$value['id']]['row_span']             = $bill_entries->where('bill_id', $value['id'])->count();
                    $data1[$value['id']]['bill_entries']      = array_values($bill_entries->where('bill_id', $value['id'])->toArray());
                }
            }
            else
            {
                $data1      = [];
            }
        }
        else
        {
            $data1                               = [];
        }

        if ($purchase_returns->count() > 0)
        {
            if ((isset($_GET['purchase_return']) && $_GET['purchase_return'] == 2) || (isset($_GET['purchase_return']) && $_GET['purchase_return'] == 0))
            {
                foreach ($purchase_returns as $key => $value)
                {   
                    $sub_total_value                             = $purchase_return_entries->where('purchase_return_id', $value['id'])->sum('total_amount');
                    $perc_vat                                    = ($sub_total_value * $value['total_vat'])/100;
                    $perc_total_discount                         = (($sub_total_value + $perc_vat)*$value['total_discount_amount'])/100;

                    $data2[$value['id']]['bill_number']          = 'PR - ' . str_pad($value['purchase_return_number'], 6, "0", STR_PAD_LEFT);
                    $data2[$value['id']]['bill_date']            = date('d-m-Y', strtotime($value['purchase_return_date']));
                    $data2[$value['id']]['customer_name']        = $value['customer_name'];
                    $data2[$value['id']]['paid_amount']          = $payment_entries->where('purchase_return_id', $value['id'])->sum('amount');
                    $data2[$value['id']]['vat_type']             = $value['vat_type'];
                    $data2[$value['id']]['vat']                  = ($value['total_vat']*$sub_total_value)/$value['sub_total_amount'];
                    $data2[$value['id']]['vat_perc']             = $perc_vat;
                    $data2[$value['id']]['total_discount_type']  = $value['total_discount_type'];
                    $data2[$value['id']]['total_discount']       = ($value['sub_total_amount'] + (($value['total_vat']*$sub_total_value)/$value['sub_total_amount']))*(($value['total_discount_amount']*$sub_total_value)/$value['sub_total_amount'])/($value['sub_total_amount'] + $value['total_vat']);
                    $data2[$value['id']]['total_discount_perc']  = $perc_total_discount;
                    $data2[$value['id']]['sub_total']            = $sub_total_value;
                    $data2[$value['id']]['type']                 = 'R';

                    if ($value['vat_type'] == 0)
                    {
                        $net_payable_vat                        = $perc_vat;
                    }
                    else
                    {
                        $net_payable_vat                        = ($value['total_vat']*$sub_total_value)/$value['sub_total_amount'];
                    }

                    if ($value['total_discount_type'] == 0)
                    {
                        $net_payable_total_discount             = $perc_total_discount;
                    }
                    else
                    {
                        $net_payable_total_discount             = ($value['sub_total_amount'] + (($value['total_vat']*$sub_total_value)/$value['sub_total_amount']))*(($value['total_discount_amount']*$sub_total_value)/$value['sub_total_amount'])/($value['sub_total_amount'] + $value['total_vat']);
                    }

                    if ($data2[$value['id']]['type'] == 'S')
                    {
                        $data2[$value['id']]['net_payable']          = $sub_total_value + $net_payable_vat - $net_payable_total_discount;
                    }
                    else
                    {
                        $data2[$value['id']]['net_payable']          = $sub_total_value + $net_payable_vat + $net_payable_total_discount;
                    }
                    
                    $data2[$value['id']]['total_buy_price']      = 0;
                    $data2[$value['id']]['profit_loss']          = 0;
                    $data2[$value['id']]['row_span']             = $purchase_return_entries->where('purchase_return_id', $value['id'])->count();
                    $data2[$value['id']]['bill_entries']      = array_values($purchase_return_entries->where('purchase_return_id', $value['id'])->toArray());
                }
            }
            else
            {
                $data2      = [];
            }     
        }
        else
        {
            $data2                               = [];
        }

        $data  = array_merge($data1, $data2);

        $item_category_name     = Products::find($category_id);
        $purchase_by_name       = Users::find($user_id);
        $purchase_id            = Bills::find($bill_number);
        $supplier_name          = Customers::find($supplier_id);

        return view('reports::purchase_statement_print', compact('user_info', 'from_date', 'to_date', 'data', 'item_category_name', 'purchase_return', 'purchase_by_name', 'purchase_id', 'supplier_name', 'data'));
    }

    public function itemList()
    {
        return view('reports::item_list');
    }

    public function itemListPrint()
    {
        $user_info              = userDetails();

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $category_id            = isset($_GET['category_id']) ? $_GET['category_id'] : 0;
        $product_id             = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $brand_id               = isset($_GET['brand_id']) ? $_GET['brand_id'] : 0;
        $variation_id           = isset($_GET['variation_id']) ? $_GET['variation_id'] : 0;

        $product            = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->when($category_id != 0, function ($query) use ($category_id) {
                                                return $query->orWhere('product_entries.product_id', $category_id);
                                            })
                                            ->when($product_id != 0, function ($query) use ($product_id) {
                                                return $query->orWhere('product_entries.id', $product_id);
                                            })
                                            ->when($brand_id != 0, function ($query) use ($brand_id) {
                                                return $query->orWhere('product_entries.brand_id', $brand_id);
                                            })
                                            ->when($variation_id != 0, function ($query) use ($variation_id) {
                                                return $query->orWhereIn('product_variation_values.id', $variation_id);
                                            })
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                         GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT products.id) as product_id
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->get();

        $products               = $product->sortBy('name')->all();
        $products               = collect($product)->groupBy('category_name');

        $item_category_name     = Products::find($category_id);
        $product_name           = ProductEntries::find($product_id);
        $brand_name             = Categories::find($brand_id);

        if ($variation_id != 0)
        {
            $variations             = ProductVariationValues::whereIn('id', $variation_id)->selectRaw('name')->get();
            $variation_name         = $variations->implode('name', ',');
        }
        else
        {
            $variation_name         = '';
        }
        
        return view('reports::item_list_print', compact('user_info', 'item_category_name', 'product_name', 'brand_name', 'products', 'variation_name'));
    }

    public function registerList()
    {
        $user_info              = userDetails();
        $type                   = isset($_GET['type']) ? $_GET['type'] : 0;

        $customers              = Customers::Where('customers.contact_type', $type)
                                            ->get();
        
        return view('reports::register_list_print', compact('user_info', 'customers'));
    }

    public function incomeStatement()
    {
        return view('reports::income_statement');
    }

    public function incomeStatementPrint()
    {
        $user_info  = Users::find(1);
        $date       = date('Y-m-d');
        $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $opening_data           = JournalEntries::where('journal_entries.date', '<', $from_date)
                                            ->where('journal_entries.account_id', '!=', 1)
                                            ->select('journal_entries.debit_credit', 'journal_entries.amount', 'journal_entries.account_id')
                                            ->get();

        $data                   = JournalEntries::whereBetween('journal_entries.date',[$from_date,$to_date])
                                            ->where('journal_entries.account_id', '!=', 1)
                                            ->select('journal_entries.*')
                                            ->orderBy('id', 'ASC')
                                            ->get();

        $paid_through_accounts  = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();;

        foreach ($paid_through_accounts as $key => $value)
        {   
            $opening_debit      = $opening_data->where('debit_credit', 0)->where('account_id', $value->id)->sum('amount');
            $opening_credit     = $opening_data->where('debit_credit', 1)->where('account_id', $value->id)->sum('amount');

            $opening_balance    = $opening_credit - $opening_debit;

            $d1 = $data->where('account_id', $value->id);

            $result[$value->id]['account_name']     = $value->account_name;
            $result[$value->id]['opening_balance']  = $opening_balance;
            $result[$value->id]['data']             = $d1;
        }

        return view('reports::income_statement_print', compact('user_info', 'from_date', 'to_date', 'result'));
    }

    public function currentBalance()
    {
        return view('reports::current_balance');
    }

    public function currentBalancePrint()
    {
        $user_info  = Users::find(1);
        $date       = date('Y-m-d');
        $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $result                 = JournalEntries::select('journal_entries.*')
                                            ->orderBy('id', 'ASC')
                                            ->get();

        $paid_through_accounts  = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();

        foreach ($paid_through_accounts as $key => $value)
        {   
            $debit      = $result->where('debit_credit', 0)->where('account_id', $value->id)->sum('amount');
            $credit     = $result->where('debit_credit', 1)->where('account_id', $value->id)->sum('amount');

            $balance    = $credit - $debit;

            $data[$value->id]['account_name']     = $value->account_name;
            $data[$value->id]['balance']          = $balance;
        }

        return view('reports::current_balance_print', compact('user_info', 'from_date', 'to_date', 'data'));
    }

    public function productSuppliers()
    {
        return view('reports::product_suppliers');
    }

    public function productSuppliersPrint()
    {
        $user_info              = userDetails();

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $supplier_id            = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;
        $product_id             = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $report_type            = isset($_GET['report_type']) ? $_GET['report_type'] : 0;

        if ($report_type == 1)
        {
            $product                = ProductSuppliers::leftjoin('product_entries', 'product_entries.id', 'product_suppliers.product_entry_id')
                                            ->leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_suppliers.supplier_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                return $query->where('product_suppliers.supplier_id', $supplier_id);
                                            })
                                            ->when($product_id != 0, function ($query) use ($product_id) {
                                                return $query->where('product_entries.id', $product_id);
                                            })
                                            ->where('product_entries.product_id', '!=', 1)
                                            ->groupBy('product_suppliers.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_suppliers.supplier_id) as supplier_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_suppliers.whole_sale_price) as whole_sale_price,
                                                         GROUP_CONCAT(DISTINCT product_suppliers.retail_price) as retail_price
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->get();
        
            $products               = $product->groupBy('supplier_name');
        }

        if ($report_type == 2)
        {
            $product                = ProductSuppliers::leftjoin('product_entries', 'product_entries.id', 'product_suppliers.product_entry_id')
                                            ->leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_suppliers.supplier_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                return $query->where('product_suppliers.supplier_id', $supplier_id);
                                            })
                                            ->when($product_id != 0, function ($query) use ($product_id) {
                                                return $query->where('product_entries.id', $product_id);
                                            })
                                            ->where('product_entries.product_id', '!=', 1)
                                            ->groupBy('product_suppliers.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_suppliers.supplier_id) as supplier_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT product_suppliers.whole_sale_price) as whole_sale_price,
                                                         GROUP_CONCAT(DISTINCT product_suppliers.retail_price) as retail_price
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->get();
        
            $products               = $product->groupBy('id');
        }
        
        $supplier_name              = Customers::find($supplier_id);
        $product_name               = ProductEntries::find($product_id);

        return view('reports::product_suppliers_print', compact('user_info', 'from_date', 'supplier_name', 'product_name' , 'to_date', 'products', 'report_type'));
    }

    public function productCustomers()
    {
        return view('reports::product_customers');
    }

    public function productCustomersPrint()
    {
        $user_info              = userDetails();

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $product_id             = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $report_type            = isset($_GET['report_type']) ? $_GET['report_type'] : 0;

        if ($report_type == 1)
        {
            $product                = ProductCustomers::leftjoin('product_entries', 'product_entries.id', 'product_customers.product_entry_id')
                                            ->leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_customers.customer_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                return $query->where('product_customers.customer_id', $customer_id);
                                            })
                                            ->when($product_id != 0, function ($query) use ($product_id) {
                                                return $query->where('product_entries.id', $product_id);
                                            })
                                            ->where('product_entries.product_id', '!=', 1)
                                            ->groupBy('product_customers.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_customers.customer_id) as customer_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT customers.name) as customer_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_customers.whole_sale_price) as whole_sale_price,
                                                         GROUP_CONCAT(DISTINCT product_customers.retail_price) as retail_price
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->get();
        
            $products               = $product->groupBy('customer_name');
        }

        if ($report_type == 2)
        {
            $product                = ProductCustomers::leftjoin('product_entries', 'product_entries.id', 'product_customers.product_entry_id')
                                            ->leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_customers.customer_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                return $query->where('product_customers.customer_id', $customer_id);
                                            })
                                            ->when($product_id != 0, function ($query) use ($product_id) {
                                                return $query->where('product_entries.id', $product_id);
                                            })
                                            ->where('product_entries.product_id', '!=', 1)
                                            ->groupBy('product_customers.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_customers.customer_id) as customer_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT customers.name) as customer_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT product_customers.whole_sale_price) as whole_sale_price,
                                                         GROUP_CONCAT(DISTINCT product_customers.retail_price) as retail_price
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->get();
        
            $products               = $product->groupBy('id');
        }
        
        $customer_name              = Customers::find($customer_id);
        $product_name               = ProductEntries::find($product_id);

        return view('reports::product_customers_print', compact('user_info', 'from_date', 'customer_name', 'product_name' , 'to_date', 'products', 'report_type'));
    }

    public function customerPaymentReport()
    {
         $areaList = Areas::all();
        return view('reports::customer_payment_report',compact('areaList'));
    }

    public function customerPaymentReportPrint()
    {
        $user_info              = userDetails();

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $sm_id                  = isset($_GET['sm_id']) ? $_GET['sm_id'] : 0;
        $area_id                = isset($_GET['area_id']) ? $_GET['area_id'] : 0;

        $payments               = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')               
                                                ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('payments.customer_id', $customer_id);
                                                })
                                                
                                                ->when($sm_id != 0, function ($query) use ($sm_id) {
                                                    return $query->where('customers.sr_id', $sm_id);
                                                })

                                                ->when($area_id != 0, function ($query) use ($area_id) {
                                                    return $query->where('customers.area_id', $area_id);
                                                })
                                                
                                                ->where('payments.type', 0)
                                                ->select('payments.*',
                                                         'paid_through_accounts.name as paid_through_account_name',
                                                         'customers.name as customer_name')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')         
                                                ->leftjoin('invoices', 'invoices.id', 'payment_entries.invoice_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                                })
                                                ->where('payment_entries.invoice_id', '!=', null)
                                                ->select('payment_entries.*', 'invoices.invoice_number as invoice_number')
                                                ->get();

        foreach ($payments as $key => $value)
        {
            $datas[$value['id']]['payments']           = $value;
            $datas[$value['id']]['payment_entries']    = array_values($payment_entries->where('payment_id', $value['id'])->toArray());
        }

        if (isset($datas))
        {
            $data  = array_values($datas);
        }
        else
        {
            $data  = [];
        }

        $customer_name          = Customers::find($customer_id);

        return view('reports::customer_payment_report_print', compact('data', 'from_date', 'to_date', 'user_info', 'customer_name'));
    }

    public function supplierPaymentReport()
    {
        return view('reports::supplier_payment_report');
    }

    public function supplierPaymentReportPrint()
    {
        $user_info              = userDetails();

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $supplier_id            = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;

        $payments               = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')               
                                                ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                                })
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('payments.customer_id', $supplier_id);
                                                })
                                                ->where('payments.type', 1)
                                                ->select('payments.*',
                                                         'paid_through_accounts.name as paid_through_account_name',
                                                         'customers.name as customer_name')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')         
                                                ->leftjoin('bills', 'bills.id', 'payment_entries.bill_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                                })
                                                ->where('payment_entries.bill_id', '!=', null)
                                                ->select('payment_entries.*', 'bills.bill_number as bill_number')
                                                ->get();

        foreach ($payments as $key => $value)
        {
            $datas[$value['id']]['payments']           = $value;
            $datas[$value['id']]['payment_entries']    = array_values($payment_entries->where('payment_id', $value['id'])->toArray());
        }

        if (isset($datas))
        {
            $data  = array_values($datas);
        }
        else
        {
            $data  = [];
        }

        $supplier_name          = Customers::find($supplier_id);

        return view('reports::supplier_payment_report_print', compact('data', 'from_date', 'to_date', 'user_info', 'supplier_name'));
    }

    public function emergencyItemList()
    {
        return view('reports::emergency_item_list');
    }

    public function emergencyItemListPrint()
    {
        $user_info              = userDetails();

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $category_id            = isset($_GET['category_id']) ? $_GET['category_id'] : 0;
        $product_id             = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $brand_id               = isset($_GET['brand_id']) ? $_GET['brand_id'] : 0;
        $variation_id           = isset($_GET['variation_id']) ? $_GET['variation_id'] : 0;

        $product            = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->when($category_id != 0, function ($query) use ($category_id) {
                                                return $query->orWhere('product_entries.product_id', $category_id);
                                            })
                                            ->when($product_id != 0, function ($query) use ($product_id) {
                                                return $query->orWhere('product_entries.id', $product_id);
                                            })
                                            ->when($brand_id != 0, function ($query) use ($brand_id) {
                                                return $query->orWhere('product_entries.brand_id', $brand_id);
                                            })
                                            ->when($variation_id != 0, function ($query) use ($variation_id) {
                                                return $query->orWhereIn('product_variation_values.id', $variation_id);
                                            })
                                            ->where('product_entries.product_id', '!=', 1)
                                            ->whereRaw('product_entries.stock_in_hand <= product_entries.alert_quantity')
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                         GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT products.id) as product_id
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->get();
        
        $products               = $product->sortBy('name')->all();
        $products               = collect($product)->groupBy('category_name');

        $item_category_name     = Products::find($category_id);
        $product_name           = ProductEntries::find($product_id);
        $brand_name             = Categories::find($brand_id);

        if ($variation_id != 0)
        {
            $variations             = ProductVariationValues::whereIn('id', $variation_id)->selectRaw('name')->get();
            $variation_name         = $variations->implode('name', ',');
        }
        else
        {
            $variation_name         = '';
        }
        
        return view('reports::emergency_item_list_print', compact('user_info', 'item_category_name', 'product_name', 'brand_name', 'products', 'variation_name'));
    }

    public function dueReportCustomerPrintDue()
    {
        $user_info              = userDetails();
        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime('01-01-2020'));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $ledger_type            = 1;

        $due                    = Invoices::where('due_amount', '>', 0)->get();

        $customers              = Customers::where('contact_type', 0)
                                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('customers.id', $customer_id);
                                            })
                                            ->where('id', '!=', 1)
                                            ->orderBy('name', 'ASC')
                                            ->get();

        $total_due_amount           = 0;
        $total_advance              = 0;

        if ($customers->count() > 0)
        {
            foreach ($customers as $key => $value)
            {
                $due_amount         = $due->where('customer_id', $value['id'])->sum('due_amount');

                if ($due_amount > 0)
                {
                    $data[$value['id']]['customer_id']      = $value['id'];
                    $data[$value['id']]['customer_name']    = $value['name'];
                    $data[$value['id']]['customer_advance'] = $value['customer_advance_payment'];
                    $data[$value['id']]['phone']            = $value['phone'];
                    $data[$value['id']]['address']          = $value['address'];
                    $data[$value['id']]['due_amount']       = $due_amount;

                    $total_due_amount        = $total_due_amount + $due_amount;
                    $total_advance           = $total_advance + $value['customer_advance_payment'];
                }
                
                $total_advance           = $total_advance + $value['customer_advance_payment'];
            }
        }
        
        if(!empty($data))
        {
            $data = $data;
        }
        else
        {
            $data = [];
        }

        $customer_name    = Customers::find($customer_id);

        return view('reports::due_report_customer_due', compact('data', 'total_due_amount', 'from_date', 'to_date', 'user_info', 'customers', 'customer_name', 'ledger_type', 'total_advance'));
    }

    public function dueReportCustomer()
    {
        $user_info              = userDetails();
        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime('01-01-2020'));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $ledger_type            = isset($_GET['ledger_type']) ? $_GET['ledger_type'] : 0;

        $customers              = Customers::where('contact_type', 0)
                                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('customers.id', $customer_id);
                                            })
                                            ->where('id', '!=', 1)
                                            ->orderBy('name', 'ASC')
                                            ->get();
        $total_balance              = 0;

        if ($customers->count() > 0)
        {
            foreach ($customers as $key => $value)
            {   
                if ($ledger_type == 1)
                {
                    if ($value->balance != 0)
                    {
                        $data[$value['id']]['customer_id']      = $value['id'];
                        $data[$value['id']]['customer_name']    = $value['name'];
                        $data[$value['id']]['phone']            = $value['phone'];
                        $data[$value['id']]['address']          = $value['address'];
                        $data[$value['id']]['balance']          = $value->balance;

                        $total_balance           = $total_balance + $value->balance;
                    }
                }
                else
                {
                    $data[$value['id']]['customer_id']      = $value['id'];
                    $data[$value['id']]['customer_name']    = $value['name'];
                    $data[$value['id']]['phone']            = $value['phone'];
                    $data[$value['id']]['address']          = $value['address'];
                    $data[$value['id']]['balance']          = $value->balance;
                    
                    $total_balance           = $total_balance + $value->balance;
                }

                // DB::beginTransaction();

                // try{
                //     if ($value->balance > 0)
                //     {
                //         $branch_id                  = Auth::user()->branch_id;
                //         $data_find                  = Invoices::orderBy('created_at', 'DESC')->first();
                //         $invoice_number             = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

                //         $invoice                    = new Invoices;
                //         $invoice->invoice_number    = $invoice_number;
                //         $invoice->customer_id       = $value->id;
                //         $invoice->invoice_date      = date('Y-m-d');
                //         $invoice->invoice_amount    = $value->id;
                //         $invoice->due_amount        = $value->balance;
                //         $invoice->total_buy_price   = 0;
                //         $invoice->total_discount    = 0;
                //         $invoice->type              = 2;
                //         $invoice->branch_id         = $branch_id;
                //         $invoice->created_by        = Auth::user()->id;
                //         $invoice->save();

                //         //Financial Accounting Part Start
                //         debit($customer_id=$value->id, $date=date('Y-m-d'), $account_id=8, $amount=$value->balance, $note='Customer Opening Balance', $transaction_head='customer-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //         credit($customer_id=$value->id, $date=date('Y-m-d'), $account_id=2, $amount=$value->balance, $note='Customer Opening Balance', $transaction_head='customer-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //         //Financial Accounting Part End
                //     }

                //     if ($value->balance < 0)
                //     {
                //         $data_find                  = Payments::orderBy('id', 'DESC')->first();
                //         $payment_number             = $data_find != null ? $data_find['payment_number'] + 1 : 1;
                        
                //         $payment                    = new Payments;
                //         $payment->payment_number    = $payment_number;
                //         $payment->customer_id       = $value->id;
                //         $payment->payment_date      = date('Y-m-d');
                //         $payment->amount            = abs($value->balance);
                //         $payment->paid_through      = 1;
                //         $payment->type              = 0;
                //         $payment->created_by        = Auth::user()->id;

                //         if ($payment->save())
                //         { 
                //             //Financial Accounting Start
                //                 debit($customer_id=$value->id, $date=date('Y-m-d'), $account_id=1, $amount=abs($value->balance), $note='Opening Advance Payments', $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //                 credit($customer_id=$value->id, $date=date('Y-m-d'), $account_id=8, $amount=abs($value->balance), $note='Opening Advance Payments', $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //             //Financial Accounting End
                //         }
                //     }

                //     DB::commit();
                // }
                // catch (\Exception $exception)
                // {
                //     DB::rollback();
                //     dd($exception);
                // }
            }
        }
        
        if(!empty($data))
        {
            $data = $data;
        }
        else
        {
            $data = [];
        }

        $customer_name    = Customers::find($customer_id);

        return view('reports::due_report_customer', compact('data', 'total_balance', 'from_date', 'to_date', 'user_info', 'customers', 'customer_name', 'ledger_type'));
    }

    public function dueReportCustomerPrint()
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End
        $user_info              = userDetails();

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id_search']) ? $_GET['customer_id_search'] : 0;

        $invoices               = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('invoices.customer_id', $customer_id);
                                                })
                                                ->select('invoices.customer_id as customer_id',
                                                    'customers.phone as phone',
                                                    'invoices.invoice_amount as invoice_amount',
                                                    'invoices.return_amount as return_amount',
                                                    'invoices.due_amount as due_amount')
                                                ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->leftjoin('sales_return', 'sales_return.id', 'payment_entries.sales_return_id')
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('payments.customer_id', $customer_id);
                                                })
                                                ->where('payments.type', 2)
                                                ->select('payment_entries.*',
                                                         'payments.customer_id as customer_id')
                                                ->get();

        $customers              = Customers::where('contact_type', 0)
                                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('customers.id', $customer_id);
                                            })
                                            ->orderBy('created_at', 'DESC')
                                            ->get();

        $total_invoice_amount       = 0;
        $total_paid_amount          = 0;
        $total_due_amount           = 0;
        $total_return_amount        = 0;
        $total_return_due_amount    = 0;

        if ($invoices->count() > 0)
        {
            foreach ($customers as $key => $value)
            {
                $invoice_amount         = $invoices->where('customer_id', $value['id'])->sum('invoice_amount'); 
                $return_amount          = $invoices->where('customer_id', $value['id'])->sum('return_amount'); 
                $due_amount             = $invoices->where('customer_id', $value['id'])->sum('due_amount');
                $paid_amount            = $invoice_amount - $due_amount;
                $sales_return_paid      = $payment_entries_return->where('customer_id', $value['id'])->sum('amount');
                $sale_diff              = $invoice_amount - $return_amount;
                $paid_diff              = $paid_amount - $sales_return_paid;

                $data[$value['id']]['customer_id']      = $value['id'];
                $data[$value['id']]['customer_name']    = $value['name'];
                $data[$value['id']]['phone']            = $value['phone'];
                $data[$value['id']]['invoice_amount']   = $invoice_amount;
                $data[$value['id']]['paid_amount']      = $paid_amount;
                $data[$value['id']]['due_amount']       = $due_amount;
                $data[$value['id']]['return_amount']    = $return_amount;
                $data[$value['id']]['return_due']       = $return_amount - $sales_return_paid;

                // if ($due_amount > 0)
                // {
                    $total_invoice_amount               = $total_invoice_amount + $invoice_amount;
                    $total_paid_amount                  = $total_paid_amount + $paid_amount;
                    $total_due_amount                   = $total_due_amount + $due_amount;
                    $total_return_amount                = $total_return_amount + $return_amount;
                    $total_return_due_amount            = $total_return_due_amount + ($return_amount - $sales_return_paid);
                // }
            }
        }
        else
        {
            $data  =  [];   
        }

        $customer_name    = Customers::find($customer_id);

        return view('reports::due_report_customer_print', compact('data', 'total_invoice_amount', 'total_paid_amount', 'total_due_amount', 'total_return_amount', 'total_return_due_amount', 'from_date', 'to_date', 'user_info', 'customers', 'customer_name'));
    }

    public function dueReportCustomerDetails($customer_id)
    {
        $user_info          = Users::find(1);
        $customer_name      = Customers::find($customer_id);
        $branch_id          = $customer_name['branch_id'];
        $date               = date('Y-m-d');
        $from_date          = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date            = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $opening_debit      = JournalEntries::where('date','<', $from_date)
                                        ->where('customer_id', $customer_id)
                                        ->whereIn('transaction_head', ['sales', 'customer-opening-balance', 'payment-made', 'customer-settlement'])
                                        ->where('debit_credit', 1)
                                        ->sum('amount');
        $opening_credit     = JournalEntries::where('date','<', $from_date)
                                        ->where('customer_id', $customer_id)
                                        ->whereIn('transaction_head', ['payment-receive', 'sales-return', 'discount'])
                                        ->where('debit_credit', 0)
                                        ->sum('amount');
        $opening_balance    = $opening_debit - $opening_credit;

        $credit = JournalEntries::whereBetween('journal_entries.date', [$from_date, $to_date])
                                ->where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['sales', 'customer-opening-balance', 'payment-made', 'customer-settlement', 'discount'])
                                ->where('debit_credit', 0)
                                ->get();
        $debit  = JournalEntries::whereBetween('journal_entries.date', [$from_date, $to_date])
                                ->where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['payment-receive', 'sales-return'])
                                ->where('debit_credit', 1)
                                ->get();
        $data   = $debit->merge($credit)->sortBy('id');

        return view('reports::due_report_customer_details', compact('data', 'from_date', 'to_date', 'user_info', 'customer_name', 'opening_balance'));
    }

    public function srStatement()
    {
        $areaList = Areas::all();
        return view('reports::sr_statement',compact('areaList'));
    }

    public function srStatementPrint()
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End
        $user_info          = Users::find(1);
        
        $date               = date('Y-m-d');
        $from_date          = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date            = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $sr_id              = isset($_GET['sr_id']) ? $_GET['sr_id'] : 0;
        $area_id            = isset($_GET['area_id']) ? $_GET['area_id'] : 0;
        $item_category_id   = isset($_GET['category_id']) ? $_GET['category_id'] : 0;
        $item_id            = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $item_code          = isset($_GET['product_code']) ? $_GET['product_code'] : 0;
        
        
        $sr_items           = SrItems::leftjoin('customers', 'customers.id', 'sr_items.sr_id')
                                        ->leftjoin('issues', 'issues.id', 'sr_items.issue_id')
                                        ->leftjoin('products', 'products.id', 'sr_items.product_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'sr_items.product_entry_id')
                                        ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                        ->whereBetween('sr_items.date', [$from_date, $to_date])
                                        ->when($sr_id != 0, function ($query) use ($sr_id) {
                                            return $query->where('sr_items.sr_id', $sr_id);
                                        })
                                        ->when($area_id != 0, function ($query) use ($area_id) {
                                            return $query->where('customers.area_id', $area_id);
                                        })
                                        ->when($item_category_id != 0, function ($query) use ($item_category_id) {
                                            return $query->where('sr_items.product_id', $item_category_id);
                                        })
                                        ->when($item_id != 0, function ($query) use ($item_id) {
                                            return $query->where('sr_items.product_entry_id', $item_id);
                                        })
                                        ->when($item_code != 0, function ($query) use ($item_code) {
                                            return $query->where('product_entries.product_code', $item_code);
                                        })
                                        ->selectRaw('sr_items.*, 
                                                     customers.name as sr_name,
                                                     issues.issue_number as issue_number,
                                                     products.name as product_category_name,
                                                     product_entries.name as product_name,
                                                     product_entries.product_code as product_code,
                                                     units.name as unit_name
                                                    ')
                                        ->get(); 
        
        $data                  = $sr_items->groupBy('sr_name');
        // dd($data);

        $sr_name            = Customers::find($sr_id);
        $item_category_name = Products::find($item_category_id);
        $item_name          = ProductEntries::find($item_id);
        $item_code          = ProductEntries::where('product_code', $item_code)->first();

        return view('reports::sr_statement_print', compact('data', 'from_date', 'to_date', 'user_info', 'sr_name', 'item_category_name', 'item_name', 'item_code'));
    }

    public function srStockStatement()
    {
        return view('reports::sr_stock_statement');
    }

    public function srStockStatementPrint()
    {
        $user_info          = Users::find(1);
        $sr_id              = isset($_GET['sr_id']) ? $_GET['sr_id'] : 0;
        $item_category_id   = isset($_GET['category_id']) ? $_GET['category_id'] : 0;
        $item_id            = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $item_code          = isset($_GET['product_code']) ? $_GET['product_code'] : 0;
        $srs                = Customers::where('contact_type', 4)
                                        ->when($sr_id != 0, function ($query) use ($sr_id) {
                                            return $query->where('customers.id', $sr_id);
                                        })
                                        ->get();

        foreach ($srs as $key => $value)
        {
            $sr_items       = SrItems::leftjoin('customers', 'customers.id', 'sr_items.sr_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'sr_items.product_entry_id')
                                        ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                        ->where('sr_items.sr_id', $value['id'])
                                        ->when($item_category_id != 0, function ($query) use ($item_category_id) {
                                            return $query->where('sr_items.product_id', $item_category_id);
                                        })
                                        ->when($item_id != 0, function ($query) use ($item_id) {
                                            return $query->where('sr_items.product_entry_id', $item_id);
                                        })
                                        ->when($item_code != 0, function ($query) use ($item_code) {
                                            return $query->where('product_entries.product_code', $item_code);
                                        })
                                        ->groupBy('sr_items.product_entry_id')
                                        ->select(DB::raw('group_concat(distinct customers.name) as sr_name'),
                                                 DB::raw('group_concat(distinct sr_items.product_entry_id) as product_entry_id'),
                                                 DB::raw('group_concat(distinct product_entries.name) as product_name'),
                                                 DB::raw('group_concat(distinct product_entries.product_code) as product_code'),
                                                 DB::raw('group_concat(distinct units.name) as unit_name'),
                                                 DB::raw("SUM(IF(sr_items.type='1',sr_items.quantity,0)) AS receive_quantity"),
                                                 DB::raw("SUM(IF(sr_items.type='2',sr_items.quantity,0)) AS sold_quantity"),
                                                 DB::raw("SUM(IF( sr_items.type='3',sr_items.quantity,0)) AS return_quantity")
                                                )
                                        ->get()
                                        ->toArray();
    
            if(!empty($sr_items))
            {
                $data[$value->name] = $sr_items;
            }
        }

        if(isset($data))
        {
            $data = $data;
        }
        else
        {
            $data = [];
        }

        $sr_name            = Customers::find($sr_id);
        $item_category_name = Products::find($item_category_id);
        $item_name          = ProductEntries::find($item_id);
        $item_code          = ProductEntries::where('product_code', $item_code)->first();
        
        return view('reports::sr_stock_statement_print', compact('data', 'user_info', 'sr_name', 'item_category_name', 'item_name', 'item_code'));
    }

    public function freeItemsStatement()
    {
        return view('reports::free_item_statement');
    }

    public function freeItemsStatementPrint()
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End
        $user_info          = Users::find(1);
        $date               = date('Y-m-d');
        $from_date          = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date            = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $sr_id              = isset($_GET['sr_id']) ? $_GET['sr_id'] : 0;
        $customer_id        = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $item_category_id   = isset($_GET['category_id']) ? $_GET['category_id'] : 0;
        $item_id            = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $item_code          = isset($_GET['product_code']) ? $_GET['product_code'] : 0;

        $free_items         = FreeItems::leftjoin('customers AS customerName', 'customerName.id', '=', 'free_items.customer_id')
                                        ->leftjoin('customers AS srName', 'srName.id', '=', 'free_items.sr_id')
                                        ->leftjoin('invoices', 'invoices.id', 'free_items.invoice_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'free_items.product_entry_id')
                                        ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                        ->whereBetween('free_items.date', [$from_date, $to_date])
                                        ->when($sr_id != 0, function ($query) use ($sr_id) {
                                            return $query->where('free_items.sr_id', $sr_id);
                                        })
                                        ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('free_items.customer_id', $customer_id);
                                        })
                                        ->when($item_category_id != 0, function ($query) use ($item_category_id) {
                                            return $query->where('product_entries.product_id', $item_category_id);
                                        })
                                        ->when($item_id != 0, function ($query) use ($item_id) {
                                            return $query->where('free_items.product_entry_id', $item_id);
                                        })
                                        ->when($item_code != 0, function ($query) use ($item_code) {
                                            return $query->where('product_entries.product_code', $item_code);
                                        })
                                        ->selectRaw('free_items.*, 
                                                     customerName.name as customer_name,
                                                     srName.name as sr_name,
                                                     invoices.invoice_number as invoice_number,
                                                     product_entries.name as product_name,
                                                     product_entries.product_code as product_code,
                                                     units.name as unit_name
                                                    ')
                                        ->get(); 

        $data               = $free_items->groupBy('customer_name');

        $sr_name            = Customers::find($sr_id);
        $customer_name      = Customers::find($customer_id);
        $item_category_name = Products::find($item_category_id);
        $item_name          = ProductEntries::find($item_id);
        $item_code          = ProductEntries::where('product_code', $item_code)->first();

        return view('reports::free_item_statement_print', compact('data', 'from_date', 'to_date', 'user_info', 'sr_name', 'customer_name', 'item_category_name', 'item_name', 'item_code'));
    }

    public function loanReport()
    {
        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('2021-01-01');
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;

        $incomes                = Incomes::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('incomes.income_date', [$from_date, $to_date]);
                                                })
                                                ->where('incomes.account_id', 12)
                                                ->get();

        $expenses               = Expenses::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                                })
                                                ->where('expenses.account_id', 13)
                                                ->get();

        $user_info  = Users::find(1);
        $customers  = Customers::where('contact_type', 5)->get();

        foreach ($customers as $key => $value)
        {   
            $income  = $incomes->where('customer_id', $value->id)->sum('amount');
            $expense = $expenses->where('customer_id', $value->id)->sum('amount');
            $balance = $income - $expense;

            $data[$key]['customer_id']   = $value->id;
            $data[$key]['customer_name'] = $value->name;
            $data[$key]['income']        = $income;
            $data[$key]['expense']       = $expense;
            $data[$key]['balance']       = $income - $expense;
        }

        if (isset($data))
        {
            $data = $data;
        }
        else
        {
            $data = [];
        }

        return view('reports::loan_report', compact('data', 'from_date', 'to_date', 'user_info', 'customer_id', 'customers'));
    }

    public function loanReportDetails($customer_id)
    {
        $user_info   = Users::find(1);
        $date        = date('Y-m-d');
        $from_date   = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date     = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $incomes                = Incomes::leftjoin('accounts', 'accounts.id', 'incomes.paid_through_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('incomes.income_date', [$from_date, $to_date]);
                                                })
                                                ->where('incomes.account_id', 12)
                                                ->where('incomes.customer_id', $customer_id)
                                                ->selectRaw('incomes.*, accounts.account_name as account_name')
                                                ->get()
                                                ->toArray();

        $expenses               = Expenses::leftjoin('accounts', 'accounts.id', 'expenses.paid_through_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                                })
                                                ->where('expenses.account_id', 13)
                                                ->where('expenses.customer_id', $customer_id)
                                                ->selectRaw('expenses.*, accounts.account_name as account_name')
                                                ->get()
                                                ->toArray();

        $user_info          = Users::find(1);
        $data               = array_merge($incomes, $expenses); 
        $customer_name      = Customers::find($customer_id);

        return view('reports::loan_report_details', compact('data', 'from_date', 'to_date', 'user_info', 'customer_name'));
    }

    public function IncomeExpenseLedger()
    {
        return view('reports::income_expense_ledger');
    }

    public function IncomeExpenseLedgerPrint()
    {
        $user_info  = Users::find(1);
        $date       = date('Y-m-d');
        $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $opening_data       = JournalEntries::where('journal_entries.date', '<', $from_date)
                                                ->where('journal_entries.account_id', 1)
                                                ->select('journal_entries.debit_credit', 'journal_entries.amount')
                                                ->get();

        $opening_debit      = $opening_data->where('debit_credit', 1)->sum('amount');
        $opening_credit     = $opening_data->where('debit_credit', 0)->sum('amount');
        $opening_balance    = $opening_debit - $opening_credit;

        $data               = JournalEntries::whereBetween('journal_entries.date',[$from_date,$to_date])
                                                ->where('journal_entries.account_id', 1)
                                                ->select('journal_entries.*')
                                                ->orderBy('id', 'ASC')
                                                ->get();

        return view('reports::income_expense_ledger_print', compact('user_info', 'from_date', 'to_date', 'data', 'opening_balance'));
    }

    public function orderReport()
    {
        $areaList       = Areas::all();

        return view('reports::order_report_index', compact('areaList'));
    }

    public function orderDetailsReport()
    {
        $user_info      = userDetails();
        $date           = date('Y-m-d');
        $from_date      = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date        = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $sr_id          = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $area_id        = isset($_GET['area_id']) ? $_GET['area_id'] : 0;

        $data           = OrderEntries::leftjoin('orders', 'orders.id', 'order_entries.invoice_id')
                                        ->leftjoin('customers', 'customers.id', 'orders.customer_id')
                                        ->where('orders.invoice_date', $date)
                                        ->when($sr_id != 0, function ($query) use ($sr_id) {
                                            return $query->where('orders.sr_id', $sr_id);
                                        })
                                        ->when($area_id != 0, function ($query) use ($area_id) {
                                            return $query->where('customers.area_id', $area_id);
                                        })
                                        ->select('order_entries.*')
                                        ->get();

        $data           = $data->groupBy('product_id');
        $customer_name  = Customers::find($sr_id);
        $area_name      = Areas::find($area_id);

        return view('reports::order_report_details', compact('user_info', 'from_date', 'to_date', 'data', 'customer_name', 'area_name'));
    }

    public function attendanceReport()
    {
        return view('reports::attendance_report');
    }

    public function attendanceReporttPrint()
    {
        $user_info              = userDetails();

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d 00:00:00', strtotime($_GET['from_date'])) : date('Y-m-d 00:00:00', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d 00:00:00', strtotime($_GET['to_date'])) : date('Y-m-d 00:00:00', strtotime($date));
        $employee_id            = isset($_GET['employee_id']) ? $_GET['employee_id'] : 0;

        $data                   = MachineDataLog::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->where(DB::Raw('DATE(DateTime)'), '>=', $from_date)
                                                                 ->where(DB::Raw('DATE(DateTime)'), '<=', $to_date);
                                                })
                                                ->when($employee_id != 0, function ($query) use ($employee_id) {
                                                    return $query->where('UserID', $employee_id);
                                                })
                                                ->orderBy('DateTime', 'ASC')
                                                ->select('AID as AID',
                                                         'UserID as UserID',
                                                          DB::Raw('DATE(DateTime) as DateTimeGr'),
                                                          'DateTime as DateTime',
                                                          'MachineID as MachineID',
                                                          'InOutStatus as InOutStatus',
                                                          'timestamp as timestamp'
                                                        )
                                                ->get();

        $emploees  = Customers::where('contact_type', 2)
                            ->when($employee_id != 0, function ($query) use ($employee_id) {
                                return $query->where('id', $employee_id);
                            })
                            ->get();
                            
        $data      = $data->groupBy('DateTimeGr');

        foreach ($data as $key => $value)
        {  
            foreach ($emploees as $key1 => $value1)
            { 
                $get_data   = $value->where('UserID', $value1->id);
                
                if($get_data->count() == 1)
                {   
                    $in_time    = collect();
                    $out_time   = collect();
                    $in_time    = $get_data->first();
                }
                else
                {   
                    $in_time    = collect();
                    $out_time   = collect();
                    $in_time    = $get_data->first();
                    $out_time   = $get_data->last();
                }                

                $result[$key][$value1->id]['name']        = $value1->name;
                $result[$key][$value1->id]['designation'] = $value1->designation;
                $result[$key][$value1->id]['in_time']     = isset($in_time['DateTime']) ? date('h:i A', strtotime($in_time['DateTime'])) : 0;
                $result[$key][$value1->id]['out_time']    = isset($out_time['DateTime']) ? date('h:i A', strtotime($out_time['DateTime'])) : 0;
            }
        }

        if (isset($result))
        {
            $result = $result;
        }
        else
        {
            $result = [];
        }

        $employee_name  = Customers::find($employee_id);

        return view('reports::attendance_report_print', compact('user_info', 'from_date', 'to_date', 'result', 'employee_name'));
    }

    public function monthlySalaryReport()
    {
        return view('reports::monthly_salary_report');
    }

    public function monthlySalaryReportPrint()
    {
        $user_info  = Users::find(1);
        $month      = $_GET['month'];
        $year       = $_GET['year'];

        $get_data   = MonthlySalarySheets::where('month', $month)->where('year', $year)->get();

        foreach ($get_data as $key => $value)
        {
            $data[$value['employee_id']]['department']          = $value->employee->department_id != null ? $value->employee->department->name : ''; 
            $data[$value['employee_id']]['employee_name']       = $value->employee->name; 
            $data[$value['employee_id']]['designation']         = $value->employee->designation; 
            $data[$value['employee_id']]['total_attendance']    = $value->total_attendance; 
            $data[$value['employee_id']]['gross']               = $value->salaryStatements->gross; 
            $data[$value['employee_id']]['payable']             = ($value->salaryStatements->gross/30)*$value->total_attendance; 
            $data[$value['employee_id']]['advance']             = $value->advance; 
            $data[$value['employee_id']]['net_payable']         = (($value->salaryStatements->gross/30)*$value->total_attendance) - $value->advance; 
        }
        
        if (isset($data))
        {
            $data   = collect($data)->groupBy('department');;
        }
        else
        {
            $data = [];
        }

        return view('reports::monthly_salary_report_print', compact('user_info', 'month', 'year', 'data'));
    }

    //Common Ajax Functions Start
        public function invoiceListAjax()
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = Invoices::orderBy('invoices.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = Invoices::where('invoices.invoice_number', 'LIKE', "%$search%")
                                        ->orderBy('invoices.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }

            $data = array();
            $i    = 0;
            foreach ($fetchData as $key => $value)
            {   
                $invoiceNumber  = 'INV - '.str_pad($value['invoice_number'], 6, "0", STR_PAD_LEFT);

                if ($i == 0)
                {
                    $data[] = array("id"=>0, "text"=>'All');
                }

                $data[]         = array("id"=>$value['id'], "text"=>$invoiceNumber);

                $i++;
            }
       
            return Response::json($data);
        }

        public function billListAjax()
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = Bills::orderBy('bills.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = Bills::where('bills.bill_number', 'LIKE', "%$search%")
                                        ->orderBy('bills.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }

            $data = array();
            $i    = 0;
            foreach ($fetchData as $key => $value)
            {   
                $billNumber     = 'BILL - '.str_pad($value['bill_number'], 6, "0", STR_PAD_LEFT);

                if ($i == 0)
                {
                    $data[] = array("id"=>0, "text"=>'All');
                }

                $data[]         = array("id"=>$value['id'], "text"=>$billNumber);

                $i++;
            }
       
            return Response::json($data);
        }

        public function usersListAjax()
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = Users::orderBy('users.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = Users::where('users.name', 'LIKE', "%$search%")
                                        ->orderBy('users.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }

            $data = array();
            $i    = 0;
            foreach ($fetchData as $key => $value)
            {   
                if ($i == 0)
                {
                    $data[] = array("id"=>0, "text"=>'All');
                }

                $data[] = array("id"=>$value['id'], "text"=>$value['name']);

                $i++;
            }
       
            return Response::json($data);
        }

        public function categoryListAjax()
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = Products::where('id', '!=', 1)
                                        ->orderBy('products.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = Products::where('id', '!=', 1)
                                        ->where('products.name', 'LIKE', "%$search%")
                                        ->orderBy('products.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }

            $data = array();
            $i    = 0;
            foreach ($fetchData as $key => $value)
            {   
                if ($i == 0)
                {
                    $data[] = array("id"=>0, "text"=>'All');
                }

                $data[] = array("id"=>$value['id'], "text"=>$value['name']);

                $i++;
            }
       
            return Response::json($data);
        }

        public function productListAjax()
        {
            if(!isset($_GET['searchTerm']))
            {   
                $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                         GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                         GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                         GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                         GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                         GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                         GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                         GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->take(100)
                                            ->get();

                $product            = $data->sortBy('name')->all();
                $fetchData          = collect($product);
            }
            else
            { 
                $search     = $_GET['searchTerm'];

                $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->where('product_entries.product_id', '!=', 1)
                                            ->where('product_entries.name', 'LIKE', "%$search%")
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                         GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                         GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                         GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                         GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                         GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                         GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                         GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->take(100)
                                            ->get();

                $product            = $data->sortBy('name')->all();
                $fetchData          = collect($product);
            }

            $data = array();
            $i    = 0;
            foreach ($fetchData as $key => $value)
            {   
                if ($value['variations'] != null)
                {
                    $variations = ' - ' . $value['variations'];
                }
                else
                {
                    $variations = '';
                }

                $product_name   = $value['name'] . $variations;

                if ($i == 0)
                {
                    $data[] = array("id"=>0, "text"=>'All');
                }

                $data[]         = array("id"=>$value['id'], "text"=>$product_name);

                $i++;
            }
       
            return Response::json($data);
        }

        public function productCodeListAjax()
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = ProductEntries::orderBy('product_entries.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = ProductEntries::where('product_entries.product_code', 'LIKE', "%$search%")
                                        ->orderBy('product_entries.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }

            $data = array();
            $i    = 0;
            foreach ($fetchData as $key => $value)
            {   
                $productCode  = str_pad($value['product_code'], 6, "0", STR_PAD_LEFT);

                if ($i == 0)
                {
                    $data[] = array("id"=>0, "text"=>'All');
                }

                $data[] = array("id"=>$value['id'], "text"=>$productCode);

                $i++;
            }
       
            return Response::json($data);
        }

        public function brandListAjax()
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = Categories::orderBy('categories.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = Categories::where('categories.name', 'LIKE', "%$search%")
                                        ->orderBy('categories.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }

            $data = array();
            $i    = 0;
            foreach ($fetchData as $key => $value)
            {   
                if ($i == 0)
                {
                    $data[] = array("id"=>0, "text"=>'All');
                }

                $data[] = array("id"=>$value['id'], "text"=>$value['name']);

                $i++;
            }
       
            return Response::json($data);
        }

        public function variationListAjax()
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = ProductVariationValues::orderBy('product_variation_values.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = ProductVariationValues::where('product_variation_values.name', 'LIKE', "%$search%")
                                        ->orderBy('product_variation_values.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }

            foreach ($fetchData as $key => $value)
            {   
                $data[] = array("id"=>$value['id'], "text"=>$value['name']);
            }
       
            return Response::json($data);
        }

        public function paidAccountListAjax()
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = PaidThroughAccounts::where('paid_through_accounts.name', 'LIKE', "%$search%")
                                        ->orderBy('paid_through_accounts.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }

            $data = array();
            $i    = 0;
            foreach ($fetchData as $key => $value)
            {   
                if ($i == 0)
                {
                    $data[] = array("id"=>0, "text"=>'All');
                }

                $data[] = array("id"=>$value['id'], "text"=>$value['name']);

                $i++;
            }
       
            return Response::json($data);
        }

        public function emergencyCategoryListAjax()
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = Products::orderBy('products.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = Products::where('products.name', 'LIKE', "%$search%")
                                        ->orderBy('products.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }

            $data = array();
            $i    = 0;
            foreach ($fetchData as $key => $value)
            {   
                if ($i == 0)
                {
                    $data[] = array("id"=>0, "text"=>'All');
                }

                $data[] = array("id"=>$value['id'], "text"=>$value['name']);

                $i++;
            }
       
            return Response::json($data);
        }

        public function emergencyProductListAjax()
        {
            if(!isset($_GET['searchTerm']))
            {   
                $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->whereRaw('product_entries.stock_in_hand <= product_entries.alert_quantity')
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                         GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                         GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                         GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                         GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                         GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                         GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                         GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->take(100)
                                            ->get();

                $product            = $data->sortBy('name')->all();
                $fetchData          = collect($product);
            }
            else
            { 
                $search     = $_GET['searchTerm'];

                $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->where('product_entries.name', 'LIKE', "%$search%")
                                            ->whereRaw('product_entries.stock_in_hand <= product_entries.alert_quantity')
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                         GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                         GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                         GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                         GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                         GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                         GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                         GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->take(100)
                                            ->get();

                $product            = $data->sortBy('name')->all();
                $fetchData          = collect($product);
            }

            $data = array();
            $i    = 0;
            foreach ($fetchData as $key => $value)
            {   
                if ($value['variations'] != null)
                {
                    $variations = ' - ' . $value['variations'];
                }
                else
                {
                    $variations = '';
                }

                $product_name   = $value['name'] . $variations;

                if ($i == 0)
                {
                    $data[] = array("id"=>0, "text"=>'All');
                }

                $data[]         = array("id"=>$value['id'], "text"=>$product_name);

                $i++;
            }
       
            return Response::json($data);
        }
    //Common Ajax Functions End
    
    
    public function dueListReportSupplier()
    {
        $user_info      = userDetails();
        $date           = date('Y-m-d');
        $from_date      = date('Y-m-d', strtotime('01-01-2020'));
        $to_date        = date('Y-m-d', strtotime($date));
        $supplier_id    = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;
        $ledger_type    = 1;

        $customers      = Customers::where('contact_type', 1)
                                    ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('customers.id', $supplier_id);
                                    })
                                    ->orderBy('created_at', 'DESC')
                                    ->get();

        $total_balance  = 0;
        if ($customers->count() > 0)
        {
            foreach ($customers as $key => $value)
            {
                if ($ledger_type == 1)
                {
                    if ($value->balance != 0)
                    {
                        $data[$value['id']]['customer_id']      = $value['id'];
                        $data[$value['id']]['customer_name']    = $value['name'];
                        $data[$value['id']]['phone']            = $value['phone'];
                        $data[$value['id']]['address']          = $value['address'];
                        $data[$value['id']]['balance']          = $value->balance;

                        $total_balance  = $total_balance + $value->balance;
                    }
                }
            }
        }
        
        if(!empty($data))
        {
            $data = $data;
        }
        else
        {
            $data = [];
        }

        $customer_name    = Customers::find($supplier_id);

        return view('reports::supplier_due_list_report', compact('data', 'from_date', 'to_date', 'user_info', 'customers', 'customer_name', 'ledger_type'));
    }

    public function dueListReportSupplierDetails($supplier_id)
    {
        $user_info  = Users::find(1);
        $branch_id  = Auth::user()->branch_id;
        $date       = date('Y-m-d');
        $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $bills                  = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('bills.vendor_id', $supplier_id);
                                                })
                                                // ->where('bills.type', 1)
                                                ->select('bills.bill_date as bill_date',
                                                    'bills.id as id',
                                                    'bills.type as type',
                                                    'bills.bill_number as bill_number',
                                                    'bills.bill_note as bill_note',
                                                    'bills.vendor_id as customer_id',
                                                    'customers.name as customer_name',
                                                    'bills.total_discount_type as total_discount_type',
                                                    'bills.total_discount_note as total_discount_note',
                                                    'bills.total_discount_amount as total_discount_amount',
                                                    'bills.bill_amount as bill_amount',
                                                    'bills.return_amount as return_amount',
                                                    'bills.due_amount as due_amount')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                        })
                                        ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('payments.customer_id', $supplier_id);
                                        })
                                        ->select('payment_entries.*')
                                        ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->leftjoin('purchase_return', 'purchase_return.id', 'payment_entries.purchase_return_id')
                                        ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('payments.customer_id', $supplier_id);
                                        })
                                        ->where('payments.type', 3)
                                        ->select('payment_entries.*',
                                                 'purchase_return.bill_id as return_bill_id')
                                        ->get();
                                                                        
        $bill_entries           = BillEntries::leftjoin('bills', 'bills.id', 'bill_entries.bill_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                        })
                                        ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('bill_entries.vendor_id', $supplier_id);
                                        })
                                        ->select('bill_entries.*')
                                        ->get();

        $total_bill_amount      = 0;
        $total_purchase_return  = 0;
        $total_payable          = 0;
        $total_paid_amount      = 0;
        $total_return_paid      = 0;
        $total_due_amount       = 0;
        $total_return_due       = 0;
        $total_discount_amount  = 0;

        if ($bills->count() > 0)
        {
            foreach ($bills as $key => $value)
            {   
                if ($value['due_amount'] > 0)
                {
                    if ($value['total_discount_type'] == 0)
                    {
                        $sub_total      = $bill_entries->where('bill_id', $value['id'])->sum('total_amount');
                        $discount       = ($sub_total*$value['total_discount_amount'])/100;
                    }
                    else
                    {
                        $discount       = $value['total_discount_amount'];
                    }

                    $purchase_return_paid   = $payment_entries_return->where('return_bill_id', $value['id'])->sum('amount');

                    $data[$value['id']]['bill_number']              = 'BILL - ' . str_pad($value['bill_number'], 6, "0", STR_PAD_LEFT);
                    $data[$value['id']]['bill_date']                = date('d-m-Y', strtotime($value['bill_date']));
                    $data[$value['id']]['bill_id']                  = $value['id'];
                    $data[$value['id']]['type']                  = $value['type'];
                    $data[$value['id']]['customer_name']            = $value['customer_name'];
                    $data[$value['id']]['bill_note']                = $value['bill_note'];
                    $data[$value['id']]['bill_amount']              = $value['bill_amount'];
                    $data[$value['id']]['return_amount']            = $value['return_amount'];
                    $data[$value['id']]['paid_amount']              = $payment_entries->where('bill_id', $value['id'])->sum('amount');
                    $data[$value['id']]['return_paid']              = $purchase_return_paid;
                    $data[$value['id']]['return_due']               = $value['return_amount'] - $purchase_return_paid;
                    $data[$value['id']]['due_amount']               = $value['due_amount'];
                    $data[$value['id']]['discount']                 = $value['total_discount'];
                    $data[$value['id']]['total_discount']           = $discount;
                    $data[$value['id']]['total_discount_note']      = $value['total_discount_note'];
                    $data[$value['id']]['total_discount_type']      = $value['total_discount_type'];
                    $data[$value['id']]['total_discount_amount']    = $value['total_discount_amount'];

                    $total_bill_amount          = $total_bill_amount + $value['bill_amount'];
                    $total_purchase_return      = $total_purchase_return + $value['return_amount'];
                    $total_payable              = $total_payable + $value['bill_amount'];
                    $total_paid_amount          = $total_paid_amount + $payment_entries->where('bill_id', $value['id'])->sum('amount');
                    $total_return_paid          = $total_return_paid + $purchase_return_paid;
                    $total_due_amount           = $total_due_amount + $value['due_amount'];
                    $total_return_due           = $total_return_due + ($value['return_amount'] - $purchase_return_paid);
                    $total_discount_amount      = $total_discount_amount + $discount;
                }
            }
        }
        else
        {
            $data  =  [];   
        }

        $user_info              = userDetails();
        $customer_name          = Customers::find($supplier_id);

        return view('reports::supplier_due_list_report_details', compact('data', 'total_bill_amount', 'total_payable', 'total_purchase_return', 'total_paid_amount', 'total_return_paid', 'total_due_amount', 'total_return_due', 'from_date', 'to_date', 'user_info', 'customer_name', 'total_discount_amount'));
    }
    
    
    public function dueListReportCustomer()
    {
        $user_info      = userDetails();
        $date           = date('Y-m-d');
        $from_date      = date('Y-m-d', strtotime('01-01-2020'));
        $to_date        = date('Y-m-d', strtotime($date));
        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $sr_id          = isset($_GET['sr_id']) ? $_GET['sr_id'] : 0;
        $area_id        = isset($_GET['area_id']) ? $_GET['area_id'] : 0;
        $ledger_type    = 1;

        $customers      = Customers::where('contact_type', 0)
                                    ->where('id', '!=', 1)
                                    ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('customers.id', $customer_id);
                                    })
                                    
                                    ->when($sr_id != 0, function ($query) use ($sr_id) {
                                        return $query->where('customers.sr_id', $sr_id);
                                    })
                                    
                                    ->when($area_id != 0, function ($query) use ($area_id) {
                                        return $query->where('customers.area_id', $area_id);
                                    })
                                    ->orderBy('created_at', 'DESC')
                                    ->get();

        $total_balance  = 0;
        if ($customers->count() > 0)
        {
            foreach ($customers as $key => $value)
            {   
                if ($ledger_type == 1)
                {
                    if ($value->balance != 0)
                    {
                        $data[$value['id']]['customer_id']      = $value['id'];
                        $data[$value['id']]['customer_name']    = $value['name'];
                        $data[$value['id']]['phone']            = $value['phone'];
                        $data[$value['id']]['address']          = $value['address'];
                        $data[$value['id']]['balance']          = $value->balance;

                        $total_balance = $total_balance + $value->balance;
                    }
                }
            }
        }
  
        if(!empty($data))
        {
            $data = $data;
        }
        else
        {
            $data = [];
        }

        $customer_name  = Customers::find($customer_id);
        $areaList       =  Areas::all();
        $srList         =  Customers::where('sr_id',null)->get();

        return view('reports::due_list_report_customer', compact('data', 'from_date', 'to_date', 'user_info', 'customers', 'customer_name', 'ledger_type','areaList','srList', 'sr_id', 'area_id'));
    }

    public function dueListReportCustomerDetails($customer_id)
    {
        $user_info  = Users::find(1);
        $date       = date('Y-m-d');
        $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $invoices               = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('invoices.customer_id', $customer_id);
                                                })
                                               
                                                // ->where('invoices.type', 1)
                                                ->select('invoices.invoice_date as invoice_date',
                                                    'invoices.id as id',
                                                    'invoices.invoice_number as invoice_number',
                                                    'invoices.type as type',
                                                    'invoices.total_discount_type as total_discount_type',
                                                    'invoices.total_discount_note as total_discount_note',
                                                    'invoices.total_discount_amount as total_discount_amount',
                                                    'invoices.total_discount as total_discount',
                                                    'invoices.invoice_amount as invoice_amount',
                                                    'invoices.return_amount as return_amount',
                                                    'invoices.due_amount as due_amount',
                                                    'invoices.customer_id as customer_id',
                                                    'customers.name as customer_name')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('payments.customer_id', $customer_id);
                                                })
                                                ->select('payment_entries.*')
                                                ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->leftjoin('sales_return', 'sales_return.id', 'payment_entries.sales_return_id')
                                        ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('payments.customer_id', $customer_id);
                                        })
                                        ->where('payments.type', 2)
                                        ->select('payment_entries.*',
                                                 'sales_return.invoice_id as return_invoice_id')
                                        ->get();

        $invoice_entries        = InvoiceEntries::leftjoin('invoices', 'invoices.id', 'invoice_entries.invoice_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                        })
                                        ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('invoice_entries.customer_id', $customer_id);
                                        })
                                        ->select('invoice_entries.*')
                                        ->get();

        $total_invoice_amount   = 0;
        $total_discount_amount  = 0;
        $total_sales_return     = 0;
        $total_receivable       = 0;
        $total_paid_amount      = 0;
        $total_return_paid      = 0;
        $total_due_amount       = 0;
        $total_return_due       = 0;

        if ($invoices->count() > 0)
        {
            foreach ($invoices as $key => $value)
            {   
                if ($value['due_amount'] > 0)
                {
                    if ($value['total_discount_type'] == 0)
                    {
                        $sub_total      = $invoice_entries->where('invoice_id', $value['id'])->sum('total_amount');
                        $discount       = ($sub_total*$value['total_discount_amount'])/100;
                    }
                    else
                    {
                        $discount       = $value['total_discount_amount'];
                    }

                    $sales_return_paid  = $payment_entries_return->where('return_invoice_id', $value['id'])->sum('amount');

                    $data[$value['id']]['invoice_number']           = 'INV - ' . str_pad($value['invoice_number'], 6, "0", STR_PAD_LEFT);
                    $data[$value['id']]['invoice_date']             = date('d-m-Y', strtotime($value['invoice_date']));
                    $data[$value['id']]['invoice_id']               = $value['id'];
                    $data[$value['id']]['type']               = $value['type'];
                    $data[$value['id']]['customer_name']            = $value['customer_name'];
                    $data[$value['id']]['invoice_amount']           = $value['invoice_amount'];
                    $data[$value['id']]['return_amount']            = $value['return_amount'];
                    $data[$value['id']]['paid_amount']              = $payment_entries->where('invoice_id', $value['id'])->sum('amount');
                    $data[$value['id']]['return_paid']              = $sales_return_paid;
                    $data[$value['id']]['return_due']               = $value['return_amount'] - $sales_return_paid;
                    $data[$value['id']]['due_amount']               = $value['due_amount'];
                    $data[$value['id']]['discount']                 = $value['total_discount'];
                    $data[$value['id']]['total_discount']           = $discount;
                    $data[$value['id']]['total_discount_note']      = $value['total_discount_note'];
                    $data[$value['id']]['total_discount_type']      = $value['total_discount_type'];
                    $data[$value['id']]['total_discount_amount']    = $value['total_discount_amount'];

                    $total_invoice_amount   = $total_invoice_amount + $value['invoice_amount'];
                    $total_discount_amount  = $total_discount_amount + $discount;
                    $total_sales_return     = $total_sales_return + $value['return_amount'];
                    $total_receivable       = $total_receivable + ($value['invoice_amount']);
                    $total_paid_amount      = $total_paid_amount + $payment_entries->where('invoice_id', $value['id'])->sum('amount');
                    $total_return_paid      = $total_return_paid + $sales_return_paid;
                    $total_due_amount       = $total_due_amount + $value['due_amount'];
                    $total_return_due       = $total_return_due + ($value['return_amount'] - $sales_return_paid);
                }  
            }
        }
        else
        {
            $data = [];
        }

        $customer_name          = Customers::find($customer_id);
        $user_info              = userDetails();

        return view('reports::due_list_report_customer_details', compact('data', 'total_invoice_amount', 'total_sales_return', 'total_receivable', 'total_paid_amount', 'total_return_paid', 'total_due_amount', 'total_return_due', 'from_date', 'to_date', 'user_info', 'customer_name', 'total_discount_amount'));
    }
    
        public function smWiseCustomerSalesReport()
    {
        $smList = Customers::where('contact_type',4)->get();
        $areaList = Areas::all();
        $storeList = Stores::all();
        return view('reports::sm_wise_customer_sales_report',compact('smList','areaList','storeList'));
    }

    public function smWiseCustomerSalesReportPrint()
    {
        $user_info              = userDetails();

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $sr_id                  = isset($_GET['sm_id']) ? $_GET['sm_id'] : 0;
        $area_id                = isset($_GET['area_id']) ? $_GET['area_id'] : 0;
        $store_id                = isset($_GET['store_id']) ? $_GET['store_id'] : 0;

        $data  = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                            ->when($sr_id != 0, function ($query) use ($sr_id) {
                                return $query->where('invoices.sr_id', $sr_id);
                            })

                            ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                            })
                            ->where('invoices.type', 1)
                            ->orWhere('customers.area_id', $area_id)
                            ->orWhere('customers.store_id', $store_id)
                            ->select('invoices.*')
                            ->orderBy('invoices.created_at', 'ASC')
                            ->get();

        $sm_name                = Customers::leftjoin('areas', 'areas.id', 'customers.area_id')
                                            ->leftjoin('stores', 'stores.id', 'customers.store_id')
                                            ->where('customers.id',$sr_id)
                                            ->select('customers.*','areas.name as area_name','stores.name as stores_name')
                                            ->first();



        
        $area_name   = Areas::find($area_id);
        $store_name  = Stores::find($store_id);

        return view('reports::sm_wise_customer_sales_report_print', compact('user_info', 'from_date', 'to_date', 'data','sm_name','area_name','store_name'));
    }
}