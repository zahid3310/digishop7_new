

<?php $__env->startSection('title', 'Show'); ?>

<style>
    table,th {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 2px;
        font-weight: bold;
        font-size: 18px
    }

    /*td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        padding: 2px;
        font-weight: bold;
        font-size: 18px
    }
*/
    td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        /*padding: 2px;*/
        font-weight: bold;
        font-size: 16px
    }

    table, th, td {
  border: 1px solid black!important;
  line-height: 21px!important;
  font-size: 19px!important;
  padding-top: 3px!important;
}

.border-none{
    border-bottom: 1px solid #fff!important;
    border-left: 1px solid #fff!important;
    border-right: 1px solid #fff!important;
    border-top: 1px solid #fff!important;
}
.border-none-2{
    border-bottom: 1px solid #fff!important;
    border-left: 1px solid #fff!important;
    border-top: 1px solid #fff!important;
}

    @page  {
        size: A4;
        page-break-after: always;
    }
</style>

<?php $__env->startSection('content'); ?>
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18"><?php echo e(__('messages.sales')); ?></h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.sales')); ?></a></li>
                                <li class="breadcrumb-item active"><?php echo e(__('messages.show')); ?></li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>

            <div style="padding: 10px;padding-top: 0px" class="row">
                <div style="padding-bottom: 5px" class="d-print-none col-md-12">
                    <div class="float-right">
                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                    </div>
                </div>

                <div style="border-right: 1px dotted black" class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                    <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                                </div>
                                <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                    <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 26px;padding-top: 10px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></h2>
                                    <p style="line-height: 1;font-size: 20px;font-weight: bold" class="text-center">প্রোঃ মোঃ মাসুদ রানা</p>
                                </div>
                                <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                    <?php echo e(QrCode::size(60)->generate("string")); ?>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px"> এমরান মার্কেট, গোয়ালপাড়া, চাঁপাইনবাবগঞ্জ । <span style="font-weight: bold"> ফোনঃ </span> ০৭৮১৫২৩০৯</p>
                                    <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">মোবাঃ ০১৭১৮৬২৯৪৫৩ Email : sazzadenterprise24@gmail.com</p>
                                    <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 13px">সর্বউৎকৃষ্টমানের বিস্কুট, চানাচুর, লজেন্স, স্পেশাল লাচ্ছা ও খিল সেমাই বিক্রেতা ।</p>
                                </div>
                            </div>

                            <hr style="margin: 5px !important">

                            <div class="row" style="line-height: 25px;">
                                <div style="font-size: 18px" class="col-md-7">
                                   
                                        <strong>ইনভয়েস/চালান নং - </strong><?php echo e(eng2bangMonth(date('m', strtotime($invoice['invoice_date'])))); ?>/<?php echo e(eng2bang(date('y', strtotime($invoice['invoice_date'])))); ?>/<?php echo e(eng2bang($invoice['invoice_number'])); ?>    
                                </div>

                                <div style="font-size: 18px" class="col-md-5 text-sm-right">
                                   
                                        <strong>তারিখঃ </strong>
                                        <span style="font-weight: bold; color: #556ee6;">
                                            <?php echo e(eng2bang(date('d-m-Y', strtotime($invoice['invoice_date'])))); ?>

                                        </span>    
                                </div>

                                <div style="font-size: 18px" class="col-md-12">
                                   
                                        <span style="font-weight: bold">এসআর/কাস্টমারের নামঃ  </span><span style="font-weight: bold; color: red;"><?php echo e($invoice['customer_name']); ?> </span><br>
                                        <span style="font-weight: bold">ফোনঃ </span><strong><?php echo e($invoice['customer_phone'] != null ? $invoice['customer_phone'] : $invoice->customer->phone); ?></strong>     
                                </div>
                            </div>

                            <div style="padding-top: 0px;padding-bottom: 0px">
                                <table style="width: 100%;">
                                    <tr>
                                        <th style="font-size: 18px;width: 5%;text-align: center">নং</th>
                                        <th colspan="2" style="font-size: 18px;width: 50%;text-align: center">বিবরণ</th>
                                        <th style="font-size: 18px;width: 15%;text-align: center">দর</th>
                                        <th style="font-size: 18px;width: 10%;text-align: center">পরিমান</th>
                                        <th style="font-size: 18px;width: 20%;text-align: center">মূল্য</th>
                                    </tr>

                                    <?php if($entries->count() > 0): ?>

                                    <?php
                                    $total_amount                   = 0;
                                    ?>

                                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php
                                    $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                    $variation_name = ProductVariationName($value['product_entry_id']);

                                    if ($value['product_code'] != null)
                                    {
                                        $productCode  = ' - '.$value['product_code'];
                                    }
                                    else
                                    {
                                        $productCode  = '';
                                    }

                                    if ($value['product_name'] != null)
                                    {
                                        $category  = ' - '.$value['product_name'];
                                    }
                                    else
                                    {
                                        $category  = '';
                                    }

                                    if ($value['brand_name'] != null)
                                    {
                                        $brandName  = $value['brand_name'];
                                    }
                                    else
                                    {
                                        $brandName  = '';
                                    }

                                    if ($value['unit_name'] != null)
                                    {
                                        $unit  = ' '.$value['unit_name'];
                                    }
                                    else
                                    {
                                        $unit  = '';
                                    }

                                    if ($variation_name != null)
                                    {
                                        $variation  = ' '.$variation_name;
                                    }
                                    else
                                    {
                                        $variation  = '';
                                    }

                                    $pre_dues = $invoice['previous_due'];
                                    $net_paya = round($total_amount, 2);
                                    $paid     = round($invoice['invoice_amount'] - $invoice['due_amount'], 2);
                                    $dues     = round($net_paya - $paid, 2);
                                    ?>

                                    <tr class="tr-height">
                                        <td style="text-align: center"><?php echo e(eng2bang($key + 1)); ?></td>
                                        <td colspan="2" style="padding-left: 30px"><?php echo e($value['product_entry_name'] . $variation); ?></td>
                                        <td style="text-align: center"><?php echo e(eng2bang($value['rate'])); ?></td>
                                        <td style="text-align: center"><?php echo e(eng2bang($value['quantity']) . $unit); ?></td>
                                        <td style="text-align: right"><?php echo e(eng2bang(round($value['total_amount'], 2))); ?></td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>

                                    <?php
                                    if ($invoice['vat_type'] == 0)
                                    {
                                        $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                    }
                                    else
                                    {
                                        $vat_amount  = $invoice['total_vat'];
                                    }

                                    if ($invoice['total_discount_type'] == 0)
                                    {
                                        $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                    }
                                    else
                                    {
                                        $discount_on_total_amount  = $invoice['total_discount_amount'];
                                    }
                                    ?>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2" style="width: 115px!important;"></th>
                                        <th style="text-align: left;text-align: right"><strong style="color: #556ee6;">মোট</strong></th>
                                        <th colspan="3" style="text-align: right;font-weight: bold; color: #556ee6;"><?php echo e($net_paya != 0 ? eng2bang(round($net_paya - $invoice['total_discount'])) : ''); ?></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>পূর্বের জের</strong></th>
                                        <th colspan="3" style="text-align: right"><?php echo e($pre_dues != 0 ? eng2bang(round($pre_dues)) : ''); ?></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong style="color: red;">সর্বমোট </strong></th>
                                        <th colspan="3" style="text-align: right;font-weight: bold; color: red;"><?php echo e($net_paya - $invoice['total_discount'] + $pre_dues != 0 ? eng2bang(round($net_paya - $invoice['total_discount'] + $pre_dues)) : ''); ?></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>জমা</strong></th>
                                        <th colspan="3" style="text-align: right"></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>জের</strong></th>
                                        <th colspan="3" style="text-align: right"></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>অন্যান্য জমা</strong></th>
                                        <th colspan="3" style="text-align: right"></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>বর্তমান জের</strong></th>
                                        <th colspan="3" style="text-align: right"></th>
                                    </tr>
                                </table>
                            </div>

                            <br>

                            <div class="row" style="padding-top:50px!important">
                                <div class="col-md-6">
                                    <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px;font-size: 18px">ক্রেতার স্বাক্ষর </span> </h6>
                                </div>
                                <div class="col-md-6">
                                    <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px;font-size: 18px">বিক্রেতার স্বাক্ষর</span> </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                    <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                                </div>
                                <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                    <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 26px;padding-top: 10px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></h2>
                                    <p style="line-height: 1;font-size: 20px;font-weight: bold" class="text-center">প্রোঃ মোঃ মাসুদ রানা</p>
                                </div>
                                <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                    <?php echo e(QrCode::size(60)->generate("string")); ?>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px"> এমরান মার্কেট, গোয়ালপাড়া, চাঁপাইনবাবগঞ্জ । <span style="font-weight: bold"> ফোনঃ </span> ০৭৮১৫২৩০৯</p>
                                    <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">মোবাঃ ০১৭১৮৬২৯৪৫৩ Email : sazzadenterprise24@gmail.com</p>
                                    <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 13px">সর্বউৎকৃষ্টমানের বিস্কুট, চানাচুর, লজেন্স, স্পেশাল লাচ্ছা ও খিল সেমাই বিক্রেতা ।</p>
                                </div>
                            </div>

                            <hr style="margin: 5px !important">

                            <div class="row" style="line-height: 25px;">
                                <div style="font-size: 18px" class="col-md-7">
                                   
                                        <strong>ইনভয়েস/চালান নং - </strong><?php echo e(eng2bangMonth(date('m', strtotime($invoice['invoice_date'])))); ?>/<?php echo e(eng2bang(date('y', strtotime($invoice['invoice_date'])))); ?>/<?php echo e(eng2bang($invoice['invoice_number'])); ?>    
                                </div>

                                <div style="font-size: 18px" class="col-md-5 text-sm-right">
                                   
                                        <strong>তারিখঃ </strong>
                                        <span style="font-weight: bold; color: #556ee6;">
                                            <?php echo e(eng2bang(date('d-m-Y', strtotime($invoice['invoice_date'])))); ?>

                                        </span>    
                                </div>

                                <div style="font-size: 18px" class="col-md-12">
                                   
                                        <span style="font-weight: bold">এসআর/কাস্টমারের নামঃ  </span><span style="font-weight: bold; color: red;"><?php echo e($invoice['customer_name']); ?> </span><br>
                                        <span style="font-weight: bold">ফোনঃ </span><strong><?php echo e($invoice['customer_phone'] != null ? $invoice['customer_phone'] : $invoice->customer->phone); ?></strong>     
                                </div>
                            </div>

                            <div style="padding-top: 0px;padding-bottom: 0px">
                                <table style="width: 100%;">
                                    <tr>
                                        <th style="font-size: 18px;width: 5%;text-align: center">নং</th>
                                        <th colspan="2" style="font-size: 18px;width: 50%;text-align: center">বিবরণ</th>
                                        <th style="font-size: 18px;width: 15%;text-align: center">দর</th>
                                        <th style="font-size: 18px;width: 10%;text-align: center">পরিমান</th>
                                        <th style="font-size: 18px;width: 20%;text-align: center">মূল্য</th>
                                    </tr>

                                    <?php if($entries->count() > 0): ?>

                                    <?php
                                    $total_amount                   = 0;
                                    ?>

                                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php
                                    $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                    $variation_name = ProductVariationName($value['product_entry_id']);

                                    if ($value['product_code'] != null)
                                    {
                                        $productCode  = ' - '.$value['product_code'];
                                    }
                                    else
                                    {
                                        $productCode  = '';
                                    }

                                    if ($value['product_name'] != null)
                                    {
                                        $category  = ' - '.$value['product_name'];
                                    }
                                    else
                                    {
                                        $category  = '';
                                    }

                                    if ($value['brand_name'] != null)
                                    {
                                        $brandName  = $value['brand_name'];
                                    }
                                    else
                                    {
                                        $brandName  = '';
                                    }

                                    if ($value['unit_name'] != null)
                                    {
                                        $unit  = ' '.$value['unit_name'];
                                    }
                                    else
                                    {
                                        $unit  = '';
                                    }

                                    if ($variation_name != null)
                                    {
                                        $variation  = ' '.$variation_name;
                                    }
                                    else
                                    {
                                        $variation  = '';
                                    }

                                    $pre_dues = $invoice['previous_due'];
                                    $net_paya = round($total_amount, 2);
                                    $paid     = round($invoice['invoice_amount'] - $invoice['due_amount'], 2);
                                    $dues     = round($net_paya - $paid, 2);
                                    ?>

                                    <tr class="tr-height">
                                        <td style="text-align: center"><?php echo e(eng2bang($key + 1)); ?></td>
                                        <td colspan="2" style="padding-left: 30px"><?php echo e($value['product_entry_name'] . $variation); ?></td>
                                        <td style="text-align: center"><?php echo e(eng2bang($value['rate'])); ?></td>
                                        <td style="text-align: center"><?php echo e(eng2bang($value['quantity']) . $unit); ?></td>
                                        <td style="text-align: right"><?php echo e(eng2bang(round($value['total_amount'], 2))); ?></td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>

                                    <?php
                                    if ($invoice['vat_type'] == 0)
                                    {
                                        $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                    }
                                    else
                                    {
                                        $vat_amount  = $invoice['total_vat'];
                                    }

                                    if ($invoice['total_discount_type'] == 0)
                                    {
                                        $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                    }
                                    else
                                    {
                                        $discount_on_total_amount  = $invoice['total_discount_amount'];
                                    }
                                    ?>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2" style="width: 115px!important;"></th>
                                        <th style="text-align: left;text-align: right"><strong style="color: #556ee6;">মোট</strong></th>
                                        <th colspan="3" style="text-align: right;font-weight: bold; color: #556ee6;"><?php echo e($net_paya != 0 ? eng2bang(round($net_paya - $invoice['total_discount'])) : ''); ?></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>পূর্বের জের</strong></th>
                                        <th colspan="3" style="text-align: right"><?php echo e($pre_dues != 0 ? eng2bang(round($pre_dues)) : ''); ?></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong style="color: red;">সর্বমোট </strong></th>
                                        <th colspan="3" style="text-align: right;font-weight: bold; color: red;"><?php echo e($net_paya - $invoice['total_discount'] + $pre_dues != 0 ? eng2bang(round($net_paya - $invoice['total_discount'] + $pre_dues)) : ''); ?></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>জমা</strong></th>
                                        <th colspan="3" style="text-align: right"></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>জের</strong></th>
                                        <th colspan="3" style="text-align: right"></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>অন্যান্য জমা</strong></th>
                                        <th colspan="3" style="text-align: right"></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>বর্তমান জের</strong></th>
                                        <th colspan="3" style="text-align: right"></th>
                                    </tr>
                                </table>
                            </div>

                            <br>

                            <div class="row" style="padding-top:50px!important">
                                <div class="col-md-6">
                                    <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px;font-size: 18px">ক্রেতার স্বাক্ষর </span> </h6>
                                </div>
                                <div class="col-md-6">
                                    <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px;font-size: 18px">বিক্রেতার স্বাক্ষর</span> </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
// $( document ).ready(function() {
//     javascript:window.print();
// });

// window.onafterprint = function(e){
//     var site_url  = $('.site_url').val();
//     window.location.replace(site_url + '/invoices');
// };
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/nasir/32/Modules/Invoices/Resources/views/show.blade.php ENDPATH**/ ?>