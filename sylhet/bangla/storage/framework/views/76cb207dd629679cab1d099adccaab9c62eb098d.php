

<?php $__env->startSection('title', 'Edit Users'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.edit_user')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.setting')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.edit_user')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

            					<form id="FormSubmit" action="<?php echo e(route('users_update',$user_find['id'])); ?>" method="post" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					<?php echo e(csrf_field()); ?>


                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label"><?php echo e(__('messages.name')); ?> *</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="<?php echo e($user_find['name']); ?>" name="name" id="name" placeholder="<?php echo e(__('messages.name')); ?>" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label"><?php echo e(__('messages.username')); ?> *</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="<?php echo e($user_find['email']); ?>" name="user_name" id="user_name" readonly>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label"><?php echo e(__('messages.password')); ?></label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="" name="password" id="password" placeholder="<?php echo e(__('messages.password')); ?>">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label"><?php echo e(__('messages.role')); ?></label>
                                    <div class="col-md-10">
                                        <select style="cursor: pointer" class="form-control" name="role">
                                            <?php if(Auth::user()->role == 1 || Auth::user()->id == 1): ?>
                                            <option <?php echo e($user_find['role'] == 1 ? 'selected' : ''); ?> value="1">Super Admin</option>
                                            <?php endif; ?>
                                            <option <?php echo e($user_find['role'] == 2 ? 'selected' : ''); ?> value="2">Admin</option>
                                            <option <?php echo e($user_find['role'] == 3 ? 'selected' : ''); ?> value="3">Employee</option>
                                        </select>
                                    </div>
                                </div>

                                <?php if(Auth::user()->role == 1): ?>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label"><?php echo e(__('messages.branch')); ?></label>
                                    <div class="col-md-10">
                                        <select style="cursor: pointer" id="branch_id" class="form-control" name="branch_id">
                                            <?php if($branches->count() > 0): ?>
                                            <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value['id']); ?>" <?php echo e($user_find['branch_id'] == $value['id'] ? 'selected' : ''); ?>><?php echo e($value['name']); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <?php endif; ?>
                                
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label"><?php echo e(__('messages.status')); ?></label>
                                    <div class="col-md-10">
                                        <select style="cursor: pointer" class="form-control" name="status">
                                            <option <?php echo e($user_find['status'] == 1 ? 'selected' : ''); ?> value="1"><?php echo e(__('messages.active')); ?></option>
                                            <option <?php echo e($user_find['status'] == 0 ? 'selected' : ''); ?> value="0"><?php echo e(__('messages.inactive')); ?></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                    	<button type="submit" class="btn btn-primary waves-effect waves-light"><?php echo e(__('messages.update')); ?></button>
                                    	<button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('users_index')); ?>"><?php echo e(__('messages.close')); ?></a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/nasir/32/Modules/Users/Resources/views/edit.blade.php ENDPATH**/ ?>