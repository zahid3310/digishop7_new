<?php

namespace Modules\Productions\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Productions;
use App\Models\ProductionEntries;
use App\Models\UnitConversions;
use Carbon\Carbon;
use Response;
use DB;
use View;

class ProductionsController extends Controller
{
    public function transferIndex()
    {
        $productions = Productions::leftjoin('production_entries', 'production_entries.production_id', 'productions.id')
                            ->where('productions.type', 0)
                            ->groupBy('production_entries.production_id')
                            ->selectRaw('GROUP_CONCAT(DISTINCT productions.id) as id,
                                         GROUP_CONCAT(DISTINCT productions.date) as date,
                                         GROUP_CONCAT(DISTINCT productions.production_number) as production_number,
                                         count(production_entries.id) as total_item,
                                         SUM(production_entries.quantity*production_entries.rate) as production_value
                                        ')
                            // ->orderBy('productions.created_at', 'DESC')
                            ->get();

        return view('productions::send_to_transfer.index', compact('productions'));
    }

    public function transferCreate()
    {
        $productions        = Productions::orderBy('productions.created_at', 'DESC')->first();
        $production_number  = $productions != null ? $productions['production_number'] + 1 : 1;

        return view('productions::send_to_transfer.create', compact('production_number'));
    }

    public function transferStore(Request $request)
    {
        $rules = array(
            'production_date'       => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $productions            = Productions::orderBy('productions.created_at', 'DESC')->first();
            $production_number      = $productions != null ? $productions['production_number'] + 1 : 1;

            $productions                        = new Productions;;
            $productions->date                  = date('Y-m-d', strtotime($data['production_date']));
            $productions->production_number     = $production_number;
            $productions->type                  = 0;
            $productions->total_amount          = $data['sub_total_amount'];
            $productions->note                  = $data['note'];
            $productions->created_by            = $user_id;

            if ($productions->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $production_entries[] = [
                        'production_id'      => $productions['id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'amount'             => $data['rate'][$key]*$data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('production_entries')->insert($production_entries);

                //Here Item Id is used for update purpose
                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['quantity'][$key4];
                    $product_entries->save();
                }

                DB::commit();

                return back()->with("success","Transfer To Production Created Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function transferEdit($id)
    {
        $find_production            = Productions::select('productions.*')->find($id);

        $find_production_entries    = ProductionEntries::leftjoin('product_entries', 'product_entries.id', 'production_entries.product_entry_id')
                                            ->where('production_entries.production_id', $id)
                                            ->select('production_entries.*',
                                                    'product_entries.id as item_id',
                                                    'product_entries.product_code as product_code',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.name as item_name')
                                            ->get();

        $entries_count          = $find_production_entries->count();

        return view('productions::send_to_transfer.edit', compact('find_production', 'find_production_entries', 'entries_count'));
    }

    public function transferUpdate(Request $request, $id)
    {
        $rules = array(
            'production_date'       => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            $productions                        = Productions::find($id);
            $productions->date                  = date('Y-m-d', strtotime($data['production_date']));
            $productions->type                  = 0;
            $productions->total_amount          = $data['sub_total_amount'];
            $productions->note                  = $data['note'];
            $productions->updated_by            = $user_id;

            if ($productions->save())
            {  
                $item_id     = ProductionEntries::where('production_id', $productions['id'])->get();
                $item_delete = ProductionEntries::where('production_id', $productions['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $production_entries[] = [
                        'production_id'      => $productions['id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'amount'             => $data['rate'][$key]*$data['quantity'][$key],
                        'updated_by'         => $user_id,
                        'updated_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('production_entries')->insert($production_entries);

                //Here Item Id is used for update purpose
                if ($item_id != null)
                {
                    foreach ($item_id as $key => $value)
                    {
                        $old_item_entry_id[]    = $value['product_entry_id'];
                        $old_items_stock[]      = $value['quantity'];
                    }

                    foreach ($old_item_entry_id as $key2 => $value2)
                    { 
                        $quantity_add_to_product_entry                   = ProductEntries::find($value2);
                        $quantity_add_to_product_entry->stock_in_hand    = $quantity_add_to_product_entry['stock_in_hand'] + $old_items_stock[$key2];
                        $quantity_add_to_product_entry->save();
                    }
                }

                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['quantity'][$key4];
                    $product_entries->save();
                }

                DB::commit();

                return redirect()->route('productions_transfer_to_production_index')->with("success","Transfer Updated Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function finishedGoodsIndex()
    {
        $productions = Productions::leftjoin('production_entries', 'production_entries.production_id', 'productions.id')
                            ->where('productions.type', 1)
                            ->groupBy('production_entries.production_id')
                            ->selectRaw('GROUP_CONCAT(DISTINCT productions.id) as id,
                                         GROUP_CONCAT(DISTINCT productions.date) as date,
                                         GROUP_CONCAT(DISTINCT productions.production_number) as production_number,
                                         count(production_entries.id) as total_item,
                                         SUM(production_entries.quantity*production_entries.rate) as production_value
                                        ')
                            // ->orderBy('productions.created_at', 'DESC')
                            ->get();

        return view('productions::received_finished_goods.index', compact('productions'));
    }

    public function finishedGoodsCreate()
    {
        return view('productions::received_finished_goods.create');
    }

    public function finishedGoodsStore(Request $request)
    {
        $rules = array(
            'production_date'       => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $productions                        = new Productions;
            $productions->date                  = date('Y-m-d', strtotime($data['production_date']));
            $productions->production_number     = $data['production_number'];
            $productions->type                  = 1;
            $productions->total_amount          = $data['sub_total_amount'];
            $productions->note                  = $data['note'];
            $productions->created_by            = $user_id;

            if ($productions->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $production_entries[] = [
                        'production_id'      => $productions['id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'amount'             => $data['rate'][$key]*$data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('production_entries')->insert($production_entries);

                //Here Item Id is used for update purpose
                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] + $data['quantity'][$key4];
                    $product_entries->save();
                }

                DB::commit();

                return back()->with("success","Transfer To Production Created Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function finishedGoodsEdit($id)
    {
        $find_production            = Productions::select('productions.*')->find($id);

        $find_production_entries    = ProductionEntries::leftjoin('product_entries', 'product_entries.id', 'production_entries.product_entry_id')
                                            ->where('production_entries.production_id', $id)
                                            ->select('production_entries.*',
                                                    'product_entries.id as item_id',
                                                    'product_entries.product_code as product_code',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.name as item_name')
                                            ->get();

        $entries_count          = $find_production_entries->count();

        return view('productions::received_finished_goods.edit', compact('find_production', 'find_production_entries', 'entries_count'));
    }

    public function finishedGoodsUpdate(Request $request, $id)
    {
        $rules = array(
            'production_date'       => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            $productions                        = Productions::find($id);
            $productions->date                  = date('Y-m-d', strtotime($data['production_date']));
            $productions->type                  = 1;
            $productions->total_amount          = $data['sub_total_amount'];
            $productions->note                  = $data['note'];
            $productions->updated_by            = $user_id;

            if ($productions->save())
            {  
                $item_id     = ProductionEntries::where('production_id', $productions['id'])->get();
                $item_delete = ProductionEntries::where('production_id', $productions['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $production_entries[] = [
                        'production_id'      => $productions['id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'amount'             => $data['rate'][$key]*$data['quantity'][$key],
                        'updated_by'         => $user_id,
                        'updated_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('production_entries')->insert($production_entries);

                //Here Item Id is used for update purpose
                if ($item_id != null)
                {
                    foreach ($item_id as $key => $value)
                    {
                        $old_item_entry_id[]    = $value['product_entry_id'];
                        $old_items_stock[]      = $value['quantity'];
                    }

                    foreach ($old_item_entry_id as $key2 => $value2)
                    { 
                        $quantity_add_to_product_entry                   = ProductEntries::find($value2);
                        $quantity_add_to_product_entry->stock_in_hand    = $quantity_add_to_product_entry['stock_in_hand'] - $old_items_stock[$key2];
                        $quantity_add_to_product_entry->save();
                    }
                }

                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] + $data['quantity'][$key4];
                    $product_entries->save();
                }

                DB::commit();

                return redirect()->route('productions_received_finished_goods_index')->with("success","Transfer Updated Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function rawMaterialsList()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('categories.id', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('product_entries.name', 'LIKE', "%$search%")
                                    ->where('product_entries.product_id', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->take(100)
                                    ->get();
        }

        foreach ($fetchData as $key => $value)
        {   
            if ($value['product_type'] == 2)
            {
                $variations  = ' - ' . $value['variations'];
            }
            else
            {
                $variations  = '';
            }

            if ($value['brand_name'] != null)
            {
                $brand  = ' - '.$value['brand_name'];
            }
            else
            {
                $brand  = '';
            }

            if ($value['product_code'] != null)
            {
                $code  = $value['product_code'];
            }
            else
            {
                $code  = '';
            }

            $name   = $value['name'] . $variations . ' ' . '( ' . $code . $brand . ' )';

            $data[] = array("id"=>$value['id'], "text"=>$name);
        }

        return Response::json($data);
    }
}
