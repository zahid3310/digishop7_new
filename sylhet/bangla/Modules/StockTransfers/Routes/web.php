<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('stocktransfers')->group(function() {
    Route::get('/', 'StockTransfersController@index')->name('stock_transfer_index');
    Route::post('/store', 'StockTransfersController@store')->name('stock_transfer_store');
    Route::get('/edit/{id}', 'StockTransfersController@edit')->name('stock_transfer_edit');
    Route::post('/update/{id}', 'StockTransfersController@update')->name('stock_transfer_update');
});
