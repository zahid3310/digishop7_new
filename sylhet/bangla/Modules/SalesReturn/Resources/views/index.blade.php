@extends('layouts.app')

@section('title', 'Sales Return')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.sales_return')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.sales')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.sales_return')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('sales_return_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="return_date">{{ __('messages.return_date')}} *</label>
                                            <input id="return_date" name="return_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">{{ __('messages.select_sr/customer')}} * </label>
                                            <select id="customer_id" name="customer_id" class="form-control select2 col-md-12" onchange="InvoiceList()" required>
                                               <option value="">-- {{ __('messages.select_sr/customer')}} --</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">{{ __('messages.order_number')}} * </label>
                                            <select id="invoice_id" name="invoice_id" class="form-control select2 col-md-12" onchange="invoiceDetails()" required>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="return_note">{{ __('messages.note')}}</label>
                                            <input id="return_note" name="return_note" type="text" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px;" class="row">
                                    <div style="background-color: #D2D2D2;padding-top: 13px;height: 230px;overflow-y: auto;overflow-x: auto" class="col-md-12">
                                        <div class="inner-repeater mb-4">
                                            <div data-repeater-list="inner-group" class="inner form-group row">
                                                <div id="invoice_entry_list" class="inner col-lg-12 ml-md-auto">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                
                                    <div style="background-color: #F4F4F7;height: 255px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.sub_total')}}</label>
                                            <div class="col-md-7">
                                                <input type="text" id="subTotalBdt" name="sub_total_amount" class="form-control">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.vat')}}</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control" id="vat_type_0" name="vat_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="0" {{ Auth::user()->vat_type == 0 ? 'selected' : '' }}>%</option>
                                                    <option style="padding: 10px" value="1" {{ Auth::user()->vat_type == 1 ? 'selected' : '' }}>BDT</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="vat_amount_0" type="text" class="form-control width-xs" name="vat_amount" value="{{ Auth::user()->vat_amount }}" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.discount')}}</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control totalDiscountType" id="total_discount_type_0" name="total_discount_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="1" selected>BDT</option>
                                                    <option style="padding: 10px" value="0">%</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="total_discount_0" type="text" class="form-control totalDiscount" name="total_discount_amount" value="0" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.dis_note')}}</label>
                                            <div class="col-md-7">
                                                <input id="total_discount_note" type="text" class="form-control width-xs" name="total_discount_note" value="{{ old('total_discount_note') }}" placeholder="{{ __('messages.dis_note')}}">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.coupon')}}</label>
                                            <div class="col-md-7">
                                                <input id="coupon_code" type="number" class="form-control width-xs couponCode" name="coupon_code" onchange="couponMembership()" placeholder="{{ __('messages.coupon/membership')}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 255px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.total_payable')}}</label>
                                            <div class="col-md-7">
                                                <input type="text" id="totalBdt" class="form-control">
                                                <input style="display: none" type="text" id="totalBdtShow" name="total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.return_amount')}}</label>
                                            <div class="col-md-7">
                                                <input id="totalReturnedBdt" type="text" class="form-control width-xs" name="total_return_amount" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #B0B0B0;height: 255px;padding-top: 13px;padding-right: 10px" class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="amount_paid">{{ __('messages.amount_paid')}}</label>
                                                    <input id="amount_paid" name="amount_paid" type="text" class="form-control" readonly>
                                                </div>
                                            </div>

                                            <div class="col-sm-4 form-group">
                                                <label class="control-label">{{ __('messages.paid_through')}}</label>
                                                <select style="cursor: pointer;" name="paid_through" class="form-control select2">
                                                    @if(!empty($paid_accounts) && ($paid_accounts->count() > 0))
                                                    @foreach($paid_accounts as $key => $paid_account)
                                                        <option value="{{ $paid_account['id'] }}">{{ $paid_account['name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>

                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="note">{{ __('messages.note')}}</label>
                                                    <input id="note" name="note" type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div style="margin-top: 20px !important;padding-bottom: 0px !important" class="form-group row">
                                    <div class="button-items col-lg-12">
                                        <button style="border-radius: 0px !important" name="print" type="submit" class="btn btn-primary waves-effect waves-light">{{ __('messages.save')}}</button>
                                        <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('sales_return_index') }}">{{ __('messages.close')}}</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<h4 class="card-title">{{ __('messages.all_sales_return')}}</h4>

                                <div style="margin-right: 0px" class="row">
                                    <div class="col-lg-8 col-md-6 col-sm-4 col-4"></div>
                                    <div class="col-lg-1 col-md-2 col-sm-4 col-4">{{ __('messages.search')}}: </div>
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-4">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.sl')}}</th>
                                            <th>{{ __('messages.date')}}</th>
                                            <th>{{ __('messages.return')}}#</th>
                                            <th>{{ __('messages.invoice')}}#</th>
                                            <th>{{ __('messages.customer')}}</th>
                                            <th>{{ __('messages.amount')}}</th>
                                            <th>{{ __('messages.paid')}}</th>
                                            <th>{{ __('messages.due')}}</th>
                                            <th>{{ __('messages.action')}}</th>
                                        </tr>
                                    </thead>

                                    <tbody id="sales_return_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Are you sure want to delete the entry ?</label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="delete_btn" class="btn btn-primary waves-effect waves-light delete_btn">Delete</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if ((result['contact_type'] == 0 || result['contact_type'] == 4) && result['id'] != 0)
                    {
                        if (result['address'] != null)
                        {
                            var address =  ' || ' + result['address'];
                        }
                        else
                        {
                            var address = '';
                        }

                        return result['text'] + address;
                    }
                },
            });

            if (location.search != "")
            {
                var locationValue = (new URL(location.href)).searchParams.get('customer_id'); 
            
                if (locationValue != null)
                {
                    var selected_customer_id = (new URL(location.href)).searchParams.get('customer_id');
                    $.get(site_url + '/salesreturn/find-customer-name/' + selected_customer_id, function(data){
                        var customer_name = data.name;
                        $("#customer_id").empty().append('<option value="'+ selected_customer_id +'">' + customer_name + '</option>').val(selected_customer_id);
                    });

                    var selected_invoice_id  = (new URL(location.href)).searchParams.get('invoice_id');
                    $.get(site_url + '/salesreturn/find-invoice-details/' + selected_invoice_id, function(data){
                        $("#invoice_id").empty().append('<option value = "' +  selected_invoice_id + '">' + 'INV - ' + data.invoice_number.padStart(6, '0') + ' | Date : ' + formatDate(data.invoice_date) + ' | Amount : ' + parseFloat(data.invoice_amount).toFixed() + '</option>').val(selected_invoice_id).trigger('change');
                    });
                }
            }

            $('#registeredCustomer').hide();
            $('#newCustomer').hide();
            $('.newCustomer').hide();

            var site_url        = $('.site_url').val();

            $.get(site_url + '/salesreturn/sales-return/list/load', function(data){

                var sales_return_list = '';
                $.each(data, function(i, sales_return_data)
                {   
                    var serial      = parseFloat(i) + 1;
                    var delete_url  = site_url + '/salesreturn/delete/' + sales_return_data.id;
                    var print_url   = site_url + '/salesreturn/show/' + sales_return_data.id;

                    sales_return_list += '<tr>' +
                                        '<input class="form-control salesReturnId" type="hidden" value="' +  sales_return_data.id + '">' +
                                        '<td>' +
                                            serial +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(sales_return_data.sales_return_date) +
                                        '</td>' +
                                        '<td>' +
                                           'SR - ' + sales_return_data.sales_return_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                           'INV - ' + sales_return_data.invoice_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            sales_return_data.customer_name +
                                        '</td>' +
                                        '<td>' +
                                           (sales_return_data.return_amount).toFixed(2) +
                                        '</td>' +
                                        '<td>' +
                                           (parseFloat(sales_return_data.return_amount) - parseFloat(sales_return_data.due_amount)).toFixed(2) +
                                        '</td>' +
                                        '<td>' +
                                           (sales_return_data.due_amount).toFixed(2) +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '@if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3)' +
                                                    '<a class="dropdown-item" href="' + delete_url +'" data-toggle="modal" data-target="#myModal">' + 'Delete' + '</a>' +
                                                    '@endif' +
                                                    '<a class="dropdown-item" href="' + print_url +'" target="_blank">' + 'Show' + '</a>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                });

                $("#sales_return_list").empty();
                $("#sales_return_list").append(sales_return_list);
            });
        });

        function searchPayment()
        {
            var search_text     = $('#searchPayment').val();
            var site_url        = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/salesreturn/sales-return/search/list/' + search_text, function(data){

                var sales_return_list = '';
                $.each(data, function(i, sales_return_data)
                {   
                    var serial      = parseFloat(i) + 1;
                    var delete_url  = site_url + '/salesreturn/delete/' + sales_return_data.id;
                    var print_url   = site_url + '/salesreturn/show/' + sales_return_data.id;

                    sales_return_list += '<tr>' +
                                        '<input class="form-control salesReturnId" type="hidden" value="' +  sales_return_data.id + '">' +
                                        '<td>' +
                                            serial +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(sales_return_data.sales_return_date) +
                                        '</td>' +
                                        '<td>' +
                                           'SR - ' + sales_return_data.sales_return_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                           'INV - ' + sales_return_data.invoice_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            sales_return_data.customer_name +
                                        '</td>' +
                                        '<td>' +
                                           (sales_return_data.return_amount).toFixed(2) +
                                        '</td>' +
                                        '<td>' +
                                           (parseFloat(sales_return_data.return_amount) - parseFloat(sales_return_data.due_amount)).toFixed(2) +
                                        '</td>' +
                                        '<td>' +
                                           (sales_return_data.due_amount).toFixed(2) +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '@if(Auth::user()->role == 1)' +
                                                    '<a class="dropdown-item" href="' + delete_url +'" data-toggle="modal" data-target="#myModal">' + 'Delete' + '</a>' +
                                                    '@endif' +
                                                    '<a class="dropdown-item" href="' + print_url +'" target="_blank">' + 'Show' + '</a>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                });

                $("#sales_return_list").empty();
                $("#sales_return_list").append(sales_return_list);
            });
        }
    </script>

    <script type="text/javascript">
    	function InvoiceList() 
        {
        	var customer_id 	= $('#customer_id').val();
        	var site_url      	= $(".site_url").val();

            if(customer_id)
            {   
                $.get(site_url + '/salesreturn/invoice-list-by-customer/'+ customer_id, function(data){

                    var list = '';

                    list 	+= '<option value = "">' + '--Select Invoice--' + '</option>';

                    $.each(data, function(i, data)
                    {   
                        var invoiceAmount   = parseFloat(data.invoice_amount).toFixed();
                        list += '<option value = "' +  data.id + '">' + 'INV - ' + data.invoice_number.padStart(6, '0') + ' | Date : ' + formatDate(data.invoice_date) + ' | Amount : ' + invoiceAmount + '</option>';

                    });

                    $("#invoice_id").empty();
                    $("#invoice_id").append(list);
                });
            }
        }

        function invoiceDetails()
        {
        	$("#totalDivShow").show();

        	var site_url  	= $('.site_url').val();
        	var invoice_id  = $('#invoice_id').val();

        	$.get(site_url + '/salesreturn/invoice-entries-list-by-invoice/' + invoice_id, function(data){

                var invoice_enrty_list      = '';
                var sub_total            = 0;
                var serial               = 1;
                $.each(data.invoice_entries, function(i, invoice_entry_data)
                {    
                    if (serial == 1)
                    {
                        var product_label       = '<label class="hidden-xs" for="productname">{{ __('messages.product')}} *</label>\n';
                        var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                        var unit_label          = '<label class="hidden-xs" for="productname">{{ __('messages.unit')}}</label>\n';
                        var return_unit_label   = '<label class="hidden-xs" for="productname">R/Unit</label>\n';
                        var quantity_label      = '<label class="hidden-xs" for="productname">{{ __('messages.qty')}} *</label>\n';
                        var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">{{ __('messages.discount')}}</label>\n';
                        var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n';
                        var amount_label        = '<label class="hidden-xs" for="productname">{{ __('messages.amount')}}</label>\n';
                        var action_label        = '<label class="hidden-xs" for="productname">{{ __('messages.return')}} {{ __('messages.qty')}}</label>\n';
                    }
                    else
                    {
                        var product_label       = '';
                        var rate_label          = '';
                        var unit_label          = '';
                        var return_unit_label   = '';
                        var quantity_label      = '';
                        var discount_label      = '';
                        var type_label          = '';
                        var amount_label        = '';
                        var action_label        = '';
                    }

                    sub_total           += parseFloat(invoice_entry_data.total_amount);

                    if (invoice_entry_data.discount_type == 1)
                    {
                        var discoumt_percent    =   '<div style="padding-left: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-md-5 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n' +
                                                                type_label +
                                                        '<select name="discount_type[]" class="form-control">\n' +
                                                            '<option value="1">BDT</option>' +
                                                        '</select>\n';
                    }
                    else
                    {
                        var discoumt_percent    =   '<div style="padding-left: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-md-5 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n' +
                                                                type_label +
                                                        '<select name="discount_type[]" class="form-control">\n' +
                                                            '<option value="0">%</option>' +
                                                        '</select>\n';
                    }

                    if (invoice_entry_data.quantity == 0)
                    {
                        var return_quantity = '<input id="return_quantity_'+ serial +'" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="0" oninput="calculate('+ serial +')" readonly />\n' 
                    }
                    else
                    {
                        var return_quantity = '<input id="return_quantity_'+ serial +'" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="0" oninput="calculate('+ serial +')" />\n' 
                    }

                    var units    = '';
                    $.each(invoice_entry_data.unit_conversions, function(i, data_list_unit)
                    {   
                        units    += '<option value = "' + data_list_unit.unit_id + '">' + data_list_unit.unit_name + '</option>';
                    });

                    units    += '<option value = "' + invoice_entry_data.converted_unit_id + '" selected>' + invoice_entry_data.conversion_unit_name + '</option>';

                    invoice_enrty_list += ' ' + '<div style="margin-bottom: 0px !important" class="mb-3 row align-items-center">' +
                                                    '<div style="padding: 10px;padding-bottom: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 co-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Category *</label>\n' +
                                                        '<input type="text" class="inner form-control" value="'+ invoice_entry_data.product_name +'" readonly />\n' +
                                                        '<input name="product_id[]" type="hidden" value="'+ invoice_entry_data.product_id +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+serial+'" value="'+ invoice_entry_data.main_unit_id +'" readonly />\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-3 col-md-3 col-sm-6 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<input type="text" class="inner form-control" value="'+ invoice_entry_data.entry_name + ' ( ' + pad(invoice_entry_data.product_code, 6) + ' )' +'" readonly />\n' +
                                                        '<input id="product_entry_id_'+serial+'" name="product_entries[]" type="hidden" value="'+ invoice_entry_data.product_entry_id +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+serial+'" value="'+invoice_entry_data.main_unit_id+'" />\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                        unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_'+serial+'" >\n' +
                                                        '<option value="'+ invoice_entry_data.conversion_unit_id +'">'+ invoice_entry_data.conversion_unit_name +'</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input id="rate_'+ serial +'" type="text" class="inner form-control rate" value="'+ invoice_entry_data.rate +'" readonly />\n' + 
                                                        '<input name="rate[]" type="hidden" value="'+ invoice_entry_data.rate +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label +
                                                        '<input id="quantity_'+ serial +'" type="text" class="inner form-control" value="'+ invoice_entry_data.quantity +'" readonly />\n' +
                                                        '<input name="quantity[]" type="hidden" class="quantity" value="'+ invoice_entry_data.original_quantity +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 10px;padding-bottom: 0px" class="col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" class="inner form-control" value="'+ invoice_entry_data.discount_amount +'"  readonly />\n' +
                                                                '<input name="discount_amount[]" type="hidden" value="'+ invoice_entry_data.discount_amount +'" />\n' +
                                                            '</div>\n' +

                                                            discoumt_percent +
                                                            
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Amount</label>\n' +
                                                        amount_label +
                                                        '<input id="amount_'+ serial +'" type="text" class="inner form-control amount" value="'+ invoice_entry_data.total_amount +'" readonly />\n' + 
                                                        '<input name="amount[]" type="hidden" value="'+ invoice_entry_data.total_amount +'" readonly />\n' +
                                                    '</div>\n' + 

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Return Qty</label>\n' +
                                                        action_label +
                                                        return_quantity +
                                                    '</div>\n' +
                                                '</div>\n';

                    serial++;
                });

                if (data.invoice.total_discount_type == 1)
                {
                    var total_discount_type    = '<option value="1" selected>' + 'BDT' + '</option>\n';
                }
                else
                {
                    var total_discount_type    = '<option value="0" selected>' + '%' + '</option>\n';
                }

                if (data.invoice.vat_type == 1)
                {
                    var vat_type    = '<option value="1" selected>' + 'BDT' + '</option>\n';
                }
                else
                {
                    var vat_type    = '<option value="0" selected>' + '%' + '</option>\n';
                }

                if (data.invoice.tax_type == 1)
                {
                    var tax_type    = '<option value="1" selected>' + 'BDT' + '</option>\n';
                }
                else
                {
                    var tax_type    = '<option value="0" selected>' + '%' + '</option>\n';
                }
 
                $("#total_discount_type_0").empty();
                $("#total_discount_type_0").append(total_discount_type);
                $("#vat_type_0").empty();
                $("#vat_type_0").append(vat_type);
                $("#tax_type_0").empty();
                $("#tax_type_0").append(tax_type);

                $("#subTotalBdtShow").html(sub_total.toFixed(2));
                $("#subTotalBdt").val(sub_total);
                $("#totalBdtShow").html(data.invoice.invoice_amount.toFixed(2));
                $("#totalBdt").val(data.invoice.invoice_amount);
                $("#total_discount_0").val(data.invoice.total_discount_amount);
                $("#total_discount_note").val(data.invoice.total_discount_note);
                $("#vat_amount_0").val(data.invoice.total_vat);
                $("#tax_amount_0").val(data.invoice.total_tax);

                $("#invoice_entry_list").empty();
                $("#invoice_entry_list").append(invoice_enrty_list);
            });
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entry_id_"+x).val();
            var unit_id             = $("#unit_id"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                var mainStock         = $("#quantity_"+x).val();
                // var convertToMain     = 
                var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                $("#quantity_"+x).val(convertedStock);
                $("#rate_"+x).val(parseFloat(data.purchase_price).toFixed(2));

                console.log(data.purchase_price);

            });
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function calculate(x)
        {
        	var return_quantity     = $('.returnQuantity').map((_,el) => el.value).get();
         	var amount 				= $('.amount').map((_,el) => el.value).get();
         	var quantity			= $('.quantity').map((_,el) => el.value).get();
         	var rate				= $('.rate').map((_,el) => el.value).get();

         	var vatType             = $("#vat_type_0").val();
            var vatAmount           = $("#vat_amount_0").val();
            var taxType             = $("#tax_type_0").val();
            var taxAmount           = $("#tax_amount_0").val();
            var totalDiscount       = $("#total_discount_0").val();
            var totalDiscountType   = $("#total_discount_type_0").val();
            var totalBdt            = $("#totalBdt").val();
            var subTotal            = $("#subTotalBdt").val();

            var total                   = 0;
            var total_quantity          = 0;
            var total_return_quantity   = 0;

         	for (var i = 0; i < return_quantity.length; i++)
         	{
         		if (return_quantity[i] > 0)
         		{	
         			var result   = (parseFloat(amount[i])/parseFloat(quantity[i]))*parseFloat(return_quantity[i]);

         			total   	+= parseFloat(result);
         		}

                total_quantity          += parseFloat(quantity[i]);
                total_return_quantity   += parseFloat(return_quantity[i]);
         	}

            if (vatType == 0)
            {
                var vatTypeCal     = (parseFloat(total)*parseFloat(vatAmount))/100;
            }
            else
            {
                var vatTypeCal     = (parseFloat(vatAmount)*parseFloat(total))/parseFloat(subTotal);
            }

            if (totalDiscountType == 0)
            {
                var totalDiscountCal     = ((parseFloat(total) + parseFloat(vatTypeCal))*parseFloat(totalDiscount))/100;
            }
            else
            {
                var totalDiscountCal     = (parseFloat(total)*parseFloat(totalDiscount))/(parseFloat(subTotal) + parseFloat(vatAmount));
            }

            if (taxType == 0)
            {
                var taxTypeCal     = (parseFloat(total)*parseFloat(taxAmount))/100;
            }
            else
            {
                var taxTypeCal     = $("#tax_amount_0").val();
            }
            var total_result       = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountCal);

         	$('#totalReturnedBdt').val(total_result.toFixed(2));
            $("#amount_paid").val(parseFloat(total_result.toFixed(2)));
        }

        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>

    <script type="text/javascript">
        $('.delete_btn').click(function () {
            var site_url            = $('.site_url').val();
            var id                  = $('.salesReturnId').val();
            window.location.href    = site_url + "/salesreturn/delete/"+id;
        });
    </script>
@endsection