<!DOCTYPE html>
<html>

<head>
    <title>Supplier Ledger Details</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>  
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: center;line-height: .8">
                        <p style="font-size: 40px"><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p style="font-size: 20px"><?php echo e($user_info['address']); ?></p>
                        <p style="font-size: 20px"><?php echo e($user_info['contact_number']); ?></p>
                        <p style="font-size: 20px"><?php echo e($user_info['contact_email']); ?></p>
                        <p style="font-size: 14px;text-align: right"><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Ledger Details</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                    <th style="text-align: center">Supplier Name</th>
                                    <th style="text-align: center">Supplier Phone</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center"><?php echo e($from_date); ?> <strong>To</strong> <?php echo e($to_date); ?></td>
                                    
                                    <td style="text-align: center">
                                        <?php if($customer_name != null): ?>
                                            <?php echo e($customer_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>
                                    
                                    <td style="text-align: center">
                                        <?php if($customer_name != null): ?>
                                            <?php echo e($customer_name['phone']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 10%">Date</th>
                                    <th style="text-align: center;width: 10%">Details</th>
                                    <th style="text-align: center;width: 10%">Transaction#</th>
                                    <th style="text-align: center;width: 10%">Payable</th>
                                    <th style="text-align: center;width: 10%">Paid</th>
                                    <th style="text-align: center;width: 10%">Balance</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php
                                    $i           = 1;
                                    $sub_total   = 0;
                                ?>
                                <?php if($data->count() > 0): ?>
                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <tr>
                                        <?php
                                            if ($value['type'] == 0)
                                            {
                                                $debit  = $value['amount'];
                                                $credit = 0;
                                            }
                                            else
                                            {
                                                $credit = $value['amount'];
                                                $debit  = 0;
                                            }

                                            if ($value['account_head'] == 'supplier-advance-adjustment')
                                            {
                                                $sub_total      = $sub_total + $debit + $credit;
                                            }
                                            else
                                            {
                                                $sub_total      = $sub_total + $credit - $debit;
                                            }

                                            if ($value['account_head'] == 'sales')
                                            {
                                                $trans_number   = 'INV - '.str_pad($value->invoice->invoice_number, 6, "0", STR_PAD_LEFT);
                                            }
                                            elseif ($value['account_head'] == 'previoue-due-collection')
                                            {
                                                $trans_number   = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                            }
                                            elseif ($value['account_head'] == 'sales-return')
                                            {
                                                $trans_number   = 'SR - '.str_pad($value->salesReturn->sales_return_number, 6, "0", STR_PAD_LEFT);
                                            }
                                            elseif ($value['account_head'] == 'purchase')
                                            {
                                                $trans_number   = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                            }
                                            elseif ($value['account_head'] == 'previoue-due-paid')
                                            {
                                                $trans_number   = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                            }
                                            elseif ($value['account_head'] == 'purchase-return')
                                            {
                                                $trans_number   = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                            }
                                            elseif ($value['account_head'] == 'payment')
                                            {
                                                $trans_number   = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                            }
                                            elseif ($value['account_head'] == 'customer-opening-balance')
                                            {
                                                $trans_number   = '';
                                            }
                                            elseif ($value['account_head'] == 'supplier-opening-balance')
                                            {
                                                $trans_number   = '';
                                            }
                                            elseif ($value['account_head'] == 'supplier-advance-adjustment')
                                            {
                                                $trans_number   = '';
                                            }
                                        ?>

                                        <td style="text-align: center;"><?php echo e(date('d-m-Y', strtotime($value['date']))); ?></a>
                                        </td>

                                        <td style="text-align: center;"><?php echo e($value['note']); ?></td>
                                        <td style="text-align: center;"><?php echo e($trans_number); ?></td>
                                        <td style="text-align: right;"><?php echo e(number_format($credit,2,'.',',')); ?></td>
                                        <td style="text-align: right;"><?php echo e(number_format($debit,2,'.',',')); ?></td>
                                        <td style="text-align: right;"><?php echo e(number_format($sub_total,2,'.',',')); ?></td>
                                    </tr>
                                 
                                <?php $i++; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="5" style="text-align: right;">TOTAL</th>
                                    <th colspan="1" style="text-align: right;"><?php echo e(number_format($sub_total,2,'.',',')); ?></th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/32/Modules/Reports/Resources/views/supplier_due_report_details.blade.php ENDPATH**/ ?>