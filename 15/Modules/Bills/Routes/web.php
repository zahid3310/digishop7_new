<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('bills')->group(function() {
    Route::get('/', 'BillsController@index')->name('bills_index');
    Route::get('/all-bills', 'BillsController@AllBills')->name('bills_all_bills');
    Route::post('/store', 'BillsController@store')->name('bills_store');
    Route::get('/edit/{id}', 'BillsController@edit')->name('bills_edit');
    Route::post('/update/{id}', 'BillsController@update')->name('bills_update');
    Route::get('/show/{id}', 'BillsController@show')->name('bills_show');
    Route::get('/product/entries/list/{id}', 'BillsController@ProductEntriesList')->name('bills_product_entries_list');
    Route::get('/product/entries/list/invoice', 'BillsController@ProductEntriesListInvoice')->name('bills_product_entries_list_invoice');
    Route::get('/bill/list/load', 'BillsController@billListLoad')->name('bills_list_load');
    Route::get('/bill/search/list/{id}', 'BillsController@billListSearch')->name('bills_list_search');
    Route::get('/from-bill/store/product', 'BillsController@storeProduct');
    Route::get('/bill-product-list', 'BillsController@billProductList');
    Route::get('/bill-product-entries-list/{id}', 'BillsController@posSearchProductBill');
    Route::get('/product/entries/list/bill/{id}', 'BillsController@ProductEntriesListBill');
    Route::get('/get-lot-details/{id}', 'BillsController@getLotDetails');
    Route::post('/update-expire-date', 'BillsController@updateExpireDate')->name('bills_update_expire_date');

    Route::get('/product-list-load-bill', 'BillsController@productListLoadBill')->name('bills_product_list_load');
});