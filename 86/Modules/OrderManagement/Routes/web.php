<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('ordermanagement/services')->group(function() {
    Route::get('/', 'OrderManagementController@serviceIndex')->name('community_service_index');
    Route::post('/store', 'OrderManagementController@serviceStore')->name('community_service_store');
    Route::get('/edit/{id}', 'OrderManagementController@serviceEdit')->name('community_service_edit');
    Route::post('/update/{id}', 'OrderManagementController@serviceUpdate')->name('community_service_update');
    Route::get('/sms-list', 'OrderManagementController@smsList')->name('sms_list');
});

Route::prefix('ordermanagement')->group(function() {
    Route::get('/quick-order', 'OrderManagementController@index')->name('quick_order');
    Route::get('/order-list', 'OrderManagementController@orderList')->name('order_list');
    Route::post('/store', 'OrderManagementController@store')->name('order_store');
    Route::get('/order/list/load', 'OrderManagementController@orderListLoad')->name('order_list_load');
    Route::get('/cancel-order/{id}', 'OrderManagementController@orderCancel')->name('order_cancel');
    Route::post('/change-order-status', 'OrderManagementController@orderStatusChange')->name('change_order_status');
    Route::get('/edit/{id}', 'OrderManagementController@edit')->name('order_edit');
    Route::post('/update/{id}', 'OrderManagementController@update')->name('order_update');
    Route::get('/customer-make-payment/{id}', 'OrderManagementController@makePayment');
    Route::post('/pay-customer-bill/payments', 'OrderManagementController@storePayment')->name('pay_customer_bill');
    Route::get('/show/{id}', 'OrderManagementController@show')->name('order_show');
    Route::get('/order/search/list/{from_date}/{to_date}/{program_from_date}/{program_to_date}/{customer}/{status}', 'OrderManagementController@orderListSearch')->name('order_list_search');
    Route::get('/customer-pay-slip/{invoice_id}', 'OrderManagementController@customerPaySlip')->name('customer_pay_slip');
    Route::post('/order-cancel-confirm/{id}', 'OrderManagementController@orderCancelConfirm')->name('order_cancel_confirm');
});
