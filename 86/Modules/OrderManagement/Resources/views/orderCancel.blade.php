@extends('layouts.app')

@section('title', 'Delete Order')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Delete Order</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Order Management</a></li>
                                    <li class="breadcrumb-item active">Delete Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif


                               <div class="row">
                                    <div style="font-size: 16px" class="col-sm-6 col-6">
                                        <address>
                                             
                                            <strong>Billed To:</strong> {{ $data['customer_name'] }}<br>
                                            <strong>Customer Contact No:</strong> {{ $data['phone'] }}<br>
                                            <strong>Program Start-Time : </strong> 
                                            <?php $start_time = new DateTime($data['start_time'], new DateTimezone('Asia/Dhaka')); echo $start_time->format('g:i a'); ?> <strong> End-Time : </strong><?php $end_time = new DateTime($data['end_time'], new DateTimezone('Asia/Dhaka')); echo $end_time->format('g:i a'); ?><br>
                                        </address>
                                    </div>


                                    <div style="font-size: 16px" class="col-sm-6 col-6 text-sm-right">
                                        <address>
                                            <strong>Booking Date:</strong> {{ date('d-m-Y', strtotime($data['order_date'])) }}<br>

                                            <strong>Program From Date:</strong> {{ date('d-m-Y', strtotime($data['program_from_data'])) }}<br>

                                            <strong>Program To Date:</strong> {{ date('d-m-Y', strtotime($data['program_to_data'])) }}<br>
                                        </address>
                                    </div>
                                </div>

                                <hr>
                                <form id="FormSubmit" action="{{ route('order_cancel_confirm', $data['id']) }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Order Number *</label>
                                        <input type="text" name="order_number" class="inner form-control" id="order_number" value="{{ 'ORD - ' . str_pad($data['order_number'], 6, "0", STR_PAD_LEFT) }}" readonly />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Total Amount</label>
                                        <input type="number" name="order_amount" class="inner form-control" id="order_amount" value="{{$data['order_amount']}}">
                                    </div>


                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Paid Amount</label>
                                        <input type="text" oninput="checkOverPayment()" name="paid_amount" class="inner form-control" id="paid_amount_0" value="{{$data['cash_given']}}" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Return Amount</label>
                                        <input type="text" oninput="checkOverPayment()" name="return_amount" id="return_amount_0" class="inner form-control amountPaid" value="{{$data['return_amount']}}" />
                                    </div>

                                </div>

                                <div class="form-group row" style="text-align: right;">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button onclick="return confirm('আপনি কি নিশ্চিত আপনি এই অর্ডার বাতিল করতে চান? আপনি যদি এই অর্ডার বাতিল করেন তবে আপনি এই অর্ডার পুনরায় নিশ্চিত করতে পারবেন না ।');" type="submit" class="btn btn-primary waves-effect waves-light">Confirm</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('order_list') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')


<script type="text/javascript">
            function checkOverPayment(x)
        {
            var dAmount           = $("#paid_amount_0").val();
            var pAmount           = $("#return_amount_0").val();

            var amount_left       = parseFloat(dAmount);
            var paid              = 0;

            if (Math.sign(parseFloat(amount_left)) == -1)
            {
                $("#return_amount_0").val(parseFloat(pAmount));

                $('.amountPaid').each(function()
                {  
                    amount_left     -= parseFloat($(this).val());
                    paid            += parseFloat($(this).val());
                });
            }

            if (Math.sign(parseFloat(amount_left)) == 1)
            {
                $("#return_amount_0").val(parseFloat(pAmount));

                $('.amountPaid').each(function()
                {  
                    amount_left     -= parseFloat($(this).val());
                    paid            += parseFloat($(this).val());
                });
            }

            if (parseFloat(amount_left) < 0)
            {
                $("#return_amount_0").val(0);
            }
        }
</script>
@endsection