<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\PaidThroughAccounts;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\UnitConversions;
use App\Models\Users;
use App\Models\AccountTransactions;
use App\Models\CommunityServices;
use App\Models\Department;
use App\Models\Inventory;
use App\Models\InventoryEntries;
use App\Models\Branches;
use Response;
use DB;
use View;
use Auth;
use Validator;
use Carbon\Carbon;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $user_id    = Auth::user()->id;
        $branches = Branches::all();
        $user_find  = Users::find($user_id);

        return view('inventory::index',compact('branches','user_find'));
    }

    public function getUseList()
    {
        return view('inventory::list');
    }



    public function inventoryShow($id)
    {


        $invoice    = Inventory::find($id);

        $entries    = InventoryEntries::leftjoin('products', 'products.id', 'inventory_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'inventory_entries.product_entry_id')
                                    ->leftjoin('branches', 'branches.id', 'inventory_entries.branch_id')
                                    ->where('inventory_entries.inventory_id', $id)
                                    ->select('inventory_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.name as product_entry_name',
                                             'products.name as product_name','branches.name as branche_name')
                                    ->get();  
                     
        $user_info  = Users::find(1);



        return view('inventory::show', compact('entries', 'invoice', 'user_info'));
    }


    public function store(Request $request)
    {


        $rules = array(
            'selling_date'          => 'required',
            'department_id'           => 'required',
            'product_entries.*'     => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            $data_find                          = Inventory::orderBy('created_at', 'DESC')->first();
            $inventory_number                     = $data_find != null ? $data_find['inventory_number'] + 1 : 1;

            $invoice                            = new Inventory;
            $invoice->inventory_number          = $inventory_number;
            $invoice->branch_id                 = $data['branch_id'];
            $invoice->department_id             = $data['department_id'];
            $invoice->inventory_date            = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->inventory_note            = $data['invoice_note'];
            $invoice->total_amount              = $data['total_amount'];
            $invoice->created_by                = $user_id;

            if ($invoice->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $inventory_entries[] = [
                        'inventory_id'        => $invoice['id'],
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value,
                        'main_unit_id'      => $data['main_unit_id'][$key],
                        'branch_id'         => $data['branch_id'][$key],
                        'conversion_unit_id'=> $data['unit_id'][$key],
                        'department_id'     => $invoice['department_id'],
                        'quantity'          => $data['quantity'][$key],
                        'rate'              => $data['rate'][$key],
                        'amount'            => $data['amount'][$key],
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('inventory_entries')->insert($inventory_entries);

                stockOut($data, $item_id=null);


                DB::commit();

                return back();

            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){

            dd($exception);
            DB::rollback();
            
            return back()->with("unsuccess","Not Added");
        }
    }
    
    public function inventoryListLoad()
    {
        $data           = Inventory::leftjoin('inventory_entries', 'inventory_entries.inventory_id', 'inventory.id')
                                    ->orderBy('inventory.created_at', 'DESC')
                                    ->select('inventory.*')
                                    ->distinct('inventory.id')
                                    ->take(20)
                                    ->get();

        return Response::json($data);
    }

    public function inventoryStatement()
    {
         $branches = Branches::all();
         return view('inventory::inventory_statement',compact('branches'));
    }

    public function inventoryStatementPrint()
    {
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $branch_id            = isset($_GET['branch_id']) ? $_GET['branch_id'] : 0;

        $category_id            = isset($_GET['category_id']) ? $_GET['category_id'] : 0;
        $product_code           = isset($_GET['product_code']) ? $_GET['product_code'] : 0;
        $product_id             = isset($_GET['product_id']) ? $_GET['product_id'] : 0;

        $data                  = Inventory::when($branch_id != 0, function ($query) use ($branch_id) {
                                                    return $query->where('inventory.branch_id', $branch_id);
                                                })
                                          
                                                
                                              
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('inventory.inventory_date', [$from_date, $to_date]);
                                                })
                                      
                                                ->select('inventory.*')


                                                 ->with(['inventoryEntries' => function($query) use($category_id, $product_code, $product_id) {
                                                        $query->when($category_id != 0, function ($query) use ($category_id) {
                                                        return $query->where('inventory_entries.product_id', $category_id);
                                                        });

                                                        $query->when($product_code != 0, function ($query) use ($product_code) {
                                                        return $query->where('inventory_entries.product_entry_id', $product_code);
                                                        });

                                                        $query->when($product_id != 0, function ($query) use ($product_id) {
                                                        return $query->where('inventory_entries.product_entry_id', $product_id);
                                                        });
                                                }])

                                                ->get();

        $item_category_name     = Products::find($category_id);
        $branch_id_name         = Branches::find($branch_id);
        // $sales_id               = Invoices::find($invoice_number);
        // $customer_name          = Customers::find($customer_id);
        // $reference              = Customers::find($reference_id);

        $user_info              = Users::find(1);

        return view('inventory::inventory_statement_print', compact('user_info','from_date', 'to_date', 'data','item_category_name','branch_id_name'));

    }

    public function inventorySummary()
    {
        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $branch_id            = isset($_GET['branch_id']) ? $_GET['branch_id'] : 0;

        $inventory               = Inventory::leftjoin('branches', 'branches.id', 'inventory.branch_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('inventory.inventory_date', [$from_date, $to_date]);
                                                })
                                                ->when($branch_id != 0, function ($query) use ($branch_id) {
                                                    return $query->where('inventory.branch_id', $branch_id);
                                                })
                                                ->select('inventory.*','branches.name as branch_name')
                                                ->get();

    

        $inventory_entries        = InventoryEntries::leftjoin('inventory', 'inventory.id', 'inventory_entries.inventory_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('inventory.inventory_date', [$from_date, $to_date]);
                                                })
                                        ->select('inventory_entries.*')
                                        ->get();

        $total_inventory_amount   = 0;

        if ($inventory->count() > 0)
        {
            foreach ($inventory as $key => $value)
            {   
               


                $data[$value['id']]['inventory_number']           = 'INV - ' . str_pad($value['inventory_number'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['inventory_date']             = date('d-m-Y', strtotime($value['inventory_date']));
                $data[$value['id']]['inventory_id']               = $value['id'];
                $data[$value['id']]['branch_name']               = $value['branch_name'];

                $data[$value['id']]['total_amount']           = $value['total_amount'];

                $total_inventory_amount   = $total_inventory_amount + $value['total_amount'];
            }
        }
        else
        {
            $data = [];
        }

       

        $branches   = Branches::all();
        $user_info  = Users::find(1);

        return view('inventory::inventory_summary', compact('data', 'total_inventory_amount', 'from_date', 'to_date', 'user_info', 'branches'));

    }


    public function inventoryProductWiseStatement()
    {
         $branches = Branches::all();
         return view('inventory::inventory_product_wise_summary',compact('branches'));
    }

    public function inventoryProductWiseStatementReport()
    {
         $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $product_id             = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $branch_id             = isset($_GET['branch_id']) ? $_GET['branch_id'] : 0;

        $invoice_entries        = InventoryEntries::leftjoin('inventory', 'inventory.id', 'inventory_entries.inventory_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('inventory.inventory_date', [$from_date, $to_date]);
                                                })

                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('inventory_entries.product_entry_id', $product_id);
                                                })

                                                ->when($branch_id != 0, function ($query) use ($branch_id) {
                                                    return $query->where('inventory_entries.branch_id', $branch_id);
                                                })
                                                ->select('inventory_entries.*')
                                                ->get();

        $products               = ProductEntries::select('id', 'name', 'product_code', 'unit_id','buy_price')
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('product_entries.id', $product_id);
                                                })
                                                ->get();

        if ($invoice_entries->count() > 0)
        {
           
            
            foreach ($products as $key => $value)
            {
            if ($invoice_entries->where('product_entry_id', $value['id'])->sum('quantity') > 0) {
                $data[$value['id']]['product_name']       = $value['name'];
                $data[$value['id']]['rate']               = $value['buy_price'];
                $data[$value['id']]['product_code']       = str_pad($value['product_code'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['quantity']           = $invoice_entries->where('product_entry_id', $value['id'])->sum('quantity');
                $data[$value['id']]['unit']               = $value->unit->name;
                $data[$value['id']]['amount']             = $invoice_entries->where('product_entry_id', $value['id'])->sum('amount');
            }
          }
        }
        else
        {
            $data = [];
        }

        $item_name     = ProductEntries::find($product_id);
        $branch_name     = Branches::find($branch_id);

        return view('inventory::inventory_product_wise_summary_print', compact('user_info', 'from_date', 'to_date', 'data', 'item_name','branch_name'));
    }



}
