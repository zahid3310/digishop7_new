<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = "inventory";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function inventoryEntries()
    {
        return $this->hasMany(InventoryEntries::class, "inventory_id");
    }

    public function branch()
    {
        return $this->belongsTo('App\Models\Branches','branch_id');
    }
}
