@extends('layouts.app')

@section('title', 'Show')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Issues</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Issues</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if($user_info['header_image'] == null)
                                        <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-4 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">{{ $user_info['organization_name'] }}</h2>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_number'] }}</p>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    @else
                                        <img class="float-left" src="{{ url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-4 col-6">
                                        <address>
                                            <strong>Issued To:</strong><br>
                                            {{ $issue['sr_name'] }}
                                            @if($issue['address'] != null)
                                               <br> <?php echo $issue['address']; ?> <br>
                                            @endif
                                            @if($issue['address'] == null)
                                                <br>
                                            @endif
                                            {{ $issue['phone'] }}
                                        </address>
                                    </div>

                                    <div class="col-sm-4 hidden-xs">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div style="font-size: 16px" class="col-sm-4 col-6 text-sm-right">
                                        <address>
                                            <strong>Issue Date:</strong><br>
                                            {{ date('d-m-Y', strtotime($issue['issue_date'])) }}<br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6 hidden-xs">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Issue summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Issue # {{ 'SIN - ' . str_pad($issue['issue_number'], 6, "0", STR_PAD_LEFT) }}</h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-nowrap">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th style="width: 70px;">No.</th>
                                                <th>Item</th>
                                                <th class="text-right">Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">

                                            @if(!empty($entries) && ($entries->count() > 0))

                                            <?php $sub_total = 0; ?>

                                            @foreach($entries as $key => $value)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>
                                                    @if($value['product_type'] == 1)
                                                        <?php echo $value['product_entry_name']; ?>
                                                    @else
                                                        <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                                    @endif
                                                </td>
                                                <td class="text-right">{{ number_format($value['quantity'],2,'.',',') . ' ' .$value['unit_name'] }}</td>
                                            </tr>

                                            <?php $sub_total = $sub_total + $value['quantity']; ?>

                                            @endforeach
                                            @endif
                                            
                                            <tr>
                                                <td style="font-size: 16px" colspan="2" class="text-right">Total</td>
                                                <td style="font-size: 16px" class="text-right">{{ number_format($sub_total,2,'.',',') }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                @if($issue['issue_note'] != null)
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 style="font-size: 16px"><strong>Note :</strong> {{ $issue['issue_note'] }}</h6>
                                    </div>
                                </div>
                                @endif

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="position: fixed;bottom: 0">
                                    <!-- <h4 class="float-right font-size-16">Order # 12345</h4> -->
                                    <!-- <div class="col-md-4">
                                        <img class="float-left" src="{{ url('public/az-ai.png') }}" alt="logo" height="20"/>
                                    </div>

                                    <div class="col-md-4">
                                        <h2 style="text-align: center">{{ $user_info['organization_name'] }}</h2>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['address'] }}</p>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['contact_number'] }}</p>
                                    </div>

                                    <div class="col-md-4">
                                        <p style="line-height: 18px;text-align: right;padding: 0px">Phone - 01718937082<br>01711418731<br>01711418731</p>
                                    </div> -->
                                    @if($user_info['footer_image'] != null)
                                        <img class="float-left" src="{{ url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

<script type="text/javascript">
    $( document ).ready(function() {
        javascript:window.print();
    });

    window.onafterprint = function(e){
        var site_url        = $('.site_url').val();
        window.location.replace(site_url + '/srissues');
    };
</script>

@endsection