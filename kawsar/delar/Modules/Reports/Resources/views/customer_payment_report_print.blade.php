<!DOCTYPE html>
<html>

<head>
    <title>Customer Payment</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">List of Customer Payment</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                    <th style="text-align: center">CUSTOMER</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>

                                    <td style="text-align: center">
                                        @if($customer_name != null)
                                            {{ $customer_name['name'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 10%">PAYMENT DATE</th>
                                    <th style="text-align: center;width: 10%">PAYMENT NUMBER</th>
                                    <th style="text-align: center;width: 15%">CUSTOMER</th>
                                    <th style="text-align: center;width: 10%">PAID THROUGH</th>
                                    <th style="text-align: center;width: 12%">ACCOUNT INFORMATION</th>
                                    <th style="text-align: center;width: 10%">INVOICE NUMBER</th>
                                    <th style="text-align: center;width: 10%">AMOUNT</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i                  = 1;
                                    $total_amount       = 0;
                                ?>
                                @foreach($data as $key => $value)
                                <tr>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ collect($data[$key]['payment_entries'])->count() != 0 ? collect($data[$key]['payment_entries'])->count() : 1 }}">{{ $i }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ collect($data[$key]['payment_entries'])->count() != 0 ? collect($data[$key]['payment_entries'])->count() : 1 }}">{{ date('d-m-Y', strtotime($value['payments']['payment_date'])) }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ collect($data[$key]['payment_entries'])->count() != 0 ? collect($data[$key]['payment_entries'])->count() : 1 }}">{{ str_pad($value['payments']['payment_number'], 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: left;vertical-align: middle" rowspan="{{ collect($data[$key]['payment_entries'])->count() != 0 ? collect($data[$key]['payment_entries'])->count() : 1 }}">{{ $value['payments']['customer_name'] }}</td>
                                    <td style="text-align: left;vertical-align: middle" rowspan="{{ collect($data[$key]['payment_entries'])->count() != 0 ? collect($data[$key]['payment_entries'])->count() : 1 }}">{{ $value['payments']['paid_through_account_name'] }}</td>
                                    <td style="text-align: left;vertical-align: middle" rowspan="{{ collect($data[$key]['payment_entries'])->count() != 0 ? collect($data[$key]['payment_entries'])->count() : 1 }}">{{ $value['payments']['account_information'] }}</td>
                                    
                                    <td style="text-align: center;">{{ isset($data[$key]['payment_entries'][0]) ? str_pad($data[$key]['payment_entries'][0]['invoice_number'], 6, "0", STR_PAD_LEFT) : '' }}</td>
                                    <td style="text-align: right;">{{ isset($data[$key]['payment_entries'][0]) ? $data[$key]['payment_entries'][0]['amount'] : 0 }}</td>
                                    
                                </tr>
                                
                                @foreach($data[$key]['payment_entries'] as $key1 => $value1)
                                @if($key1 != 0)
                                <tr>
                                    <td style="text-align: center;">{{ str_pad($value1['invoice_number'], 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: right;">{{ $value1['amount'] }}</td>
                                </tr>
                                @endif
                                @endforeach

                                <?php
                                    $i++;
                                    $total_amount      = $total_amount + $value['payments']['amount'];
                                ?>

                                @endforeach
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="7" style="text-align: right;">TOTAL</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_amount }}</th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>