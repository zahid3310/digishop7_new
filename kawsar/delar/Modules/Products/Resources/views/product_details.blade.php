@extends('layouts.app')

@section('title', 'Products Details')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Product Details</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Products</a></li>
                                    <li class="breadcrumb-item active">Details</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<h4 class="card-title">Product Name : {{ $products['name'] }}</h4>
                                <br>

                                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Product Code</th>
                                            <th>Name</th>
                                            <th style="text-align: center">Stock In Hand</th>
                                            <th>Unit</th>
                                            <th style="text-align: center">Total Sold</th>
                                            <th style="text-align: center">Alert Quantity</th>
                                            <th style="text-align: center">Buy Price</th>
                                            <th style="text-align: center">Sell Price</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                    	@if(!empty($product_entries) && ($product_entries->count() > 0))
                                    	@foreach($product_entries as $key => $product_entry)
	                                        <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ str_pad($product_entry['product_code'], 6, "0", STR_PAD_LEFT) }}</td>
                                                <td>{{ $product_entry['name'] }}</td>
                                                <td style="text-align: center">{{ $product_entry['stock_in_hand'] }}</td>
                                                <td>{{ $product_entry['unit_name'] }}</td>
                                                <td style="text-align: center">{{ $product_entry['total_sold'] }}</td>
                                                <td style="text-align: center">{{ $product_entry['alert_quantity'] }}</td>
                                                <td style="text-align: center">{{ number_format($product_entry['buy_price'],2,'.',',') }}</td>
                                                <td style="text-align: center">{{ number_format($product_entry['sell_price'],2,'.',',') }}</td>
	                                        </tr>
	                                    @endforeach
	                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection