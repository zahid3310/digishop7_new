

<?php $__env->startSection('title', 'Print Barcodes'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Print Barcodes</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Products</a></li>
                                    <li class="breadcrumb-item active">Print Barcodes</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('products_barcode_print_print')); ?>" method="post" files="true" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>


                                <?php if(!empty($bill_entries) && ($bill_entries->count() > 0)): ?>
                                <div style="margin-bottom: 0px !important" class="inner-repeater mb-4">
                                    <div data-repeater-list="inner-group" class="inner form-group mb-0 row">
                                        <div class="inner col-lg-12 ml-md-auto input_fields_wrap getMultipleRow">
                                        <?php $__currentLoopData = $bill_entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_<?php echo e($key1); ?>">
                                                <div class="col-md-6 form-group">
                                                    <?php if($key1 == 0): ?>
                                                    <label for="productname">Product Name *</label>
                                                    <?php endif; ?>
                                                    <select style="width: 100%;cursor: pointer" name="product_entry_id[]" class="inner form-control select2" id="product_entry_id_<?php echo e($key1); ?>" required>
                                                        <option value="">--Select Product--</option>
                                                        <?php if(!empty($product_entries) && ($product_entries->count() > 0)): ?>
                                                        <?php $__currentLoopData = $product_entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option <?php echo e($product['id'] == $value1['product_entry_id'] ? 'selected' : ''); ?> value="<?php echo e($product['id']); ?>">
                                                                <?php echo e($product['name']); ?>

                                                                <?php
                                                                    if ($product['product_type'] == 2)
                                                                    {
                                                                        echo ' - ' . $product['variations'];
                                                                    } 
                                                                ?>
                                                            </option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <?php endif; ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-5 form-group">
                                                    <?php if($key1 == 0): ?>
                                                    <label for="productname">Quantity*</label>
                                                    <?php endif; ?>
                                                    <input type="number" name="quantity[]" class="inner form-control" id="quantity_<?php echo e($key1); ?>"  value="<?php echo e($value1['quantity']); ?>" />
                                                </div>
                                                
                                                <div class="col-md-1 form-group remove_field" data-val="<?php echo e($key1); ?>">
                                                    <?php if($key1 == 0): ?>
                                                    <label for="productname">Action</label>
                                                    <?php endif; ?>
                                                    <input type="button" class="btn btn-primary btn-block inner" value="Delete"/>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php else: ?>
                                <div style="margin-bottom: 0px !important" class="inner-repeater mb-4">
                                    <div data-repeater-list="inner-group" class="inner form-group mb-0 row">
                                        <div class="inner col-lg-12 ml-md-auto input_fields_wrap getMultipleRow">
                                            <div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_0">
                                                <div class="col-md-6 form-group">
                                                    <label for="productname">Product Name *</label>
                                                    <select style="width: 100%;cursor: pointer" name="product_entry_id[]" class="inner form-control select2" id="product_entry_id_0" required>
                                                        <option value="">--Select Product--</option>
                                                        <?php if(!empty($product_entries) && ($product_entries->count() > 0)): ?>
                                                        <?php $__currentLoopData = $product_entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($product['id']); ?>">
                                                                <?php echo e($product['name']); ?>

                                                                <?php
                                                                    if ($product['product_type'] == 2)
                                                                    {
                                                                        echo ' - ' . $product['variations'];
                                                                    } 
                                                                ?>
                                                            </option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <?php endif; ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-5 form-group">
                                                    <label for="productname">Quantity*</label>
                                                    <input type="number" name="quantity[]" class="inner form-control" id="quantity" value="1" placeholder="Print Quantity"/>
                                                </div>
                                                
                                                <div class="col-md-1 form-group" data-val="0">
                                                    <label for="productname">Action</label>
                                                    <input type="button" class="btn btn-primary btn-block inner remove_field" value="Delete"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>

                                <div class="row justify-content-end">
                                    <div class="col-md-1 form-group">
                                        <input type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row mb-12">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                                        <div class="form-group">
                                            <label for="productname">Print Type</label>
                                            <select id="type_id" style="width: 100%;cursor: pointer" class="form-control" name="type_id">
                                                <option value="0" selected>A4 Size Printer</option>
                                                <option value="1">Label Printer</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                                        <div class="form-group">
                                            <label for="productname">Barcode Label Size</label>
                                            <select id="label_size" style="width: 100%;cursor: pointer" class="form-control" name="label_size">
                                                <option value="0" selected>40 Labels Per Sheet</option>
                                                <option value="1">36 Labels Per Sheet</option>
                                                <option value="2">22 Labels Per Sheet</option>
                                                <!-- <option value="3">60 Labels Per Sheet</option> -->
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                                        <div class="form-group">
                                            <label for="productname">Page Margin</label>
                                            <input class="form-control" type="number" name="margin">
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <h5>Information to show in Labels </h5>
                                        <br>
                                    </div>
                  
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                        <div class="custom-control custom-checkbox mb-3">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1" name="organization_name" checked>
                                            <label class="custom-control-label" for="customCheck1">Organization Name</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                        <div class="custom-control custom-checkbox mb-3">
                                            <input type="checkbox" class="custom-control-input" id="customCheck2" name="product_name" checked>
                                            <label class="custom-control-label" for="customCheck2">Product Name</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                        <div class="custom-control custom-checkbox mb-3">
                                            <input type="checkbox" class="custom-control-input" id="customCheck3" name="product_price" checked>
                                            <label class="custom-control-label" for="customCheck3">product Price</label>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Print Preview</button>
                                    </div>
                                </div>

                                </form>

                                <br>

                                <div class="col-sm-12">
                                    <div id="default_printing" style="text-align: center" class="row">
                                    </div>
                                </div>
  
                                <div id="label_printing" style="text-align: center;" class="col-sm-12">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        function ProductEntriesList(x) 
        {
            //For getting item commission information from items table start
            var site_url = $(".site_url").val();

            $.get(site_url + '/products/product-list-ajax', function(data){

                var list5 = '';
                var list7 = '';

                $.each(data, function(i, data)
                {
                    list5 += '<option value = "' +  data.id + '">' + data.name + '</option>';

                });

                list7 += '<option value = "">' + '--Select Product--' +'</option>';

                $("#product_entry_id_"+x).empty();
                $("#product_entry_id_"+x).append(list7);
                $("#product_entry_id_"+x).append(list5);
            });     
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x           = <?php echo e($entry_count); ?>;
        $(add_button).click(function(e)
        {   
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {   
                x++;

                var serial = x + 1;

                ProductEntriesList(x);

                $('.getMultipleRow').append(' ' + '<div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_'+x+'">' +
                                                    '<div class="col-md-6 form-group">\n' +
                                                        '<select style="width: 100%" name="product_entry_id[]" class="inner form-control single_select2" id="product_entry_id_'+x+'" required>\n' +
                                                        '</select>\n' + 
                                                    '</div>\n' +
                                                    '<div class="col-md-5 form-group">\n' + 
                                                        '<input type="number" name="quantity[]" class="inner form-control" id="quantity_'+x+'" value="1" placeholder="Print Quantity"/>\n' + 
                                                    '</div>\n' + 
                                                    '<div class="col-md-1 form-group remove_field" data-val="'+x+'">\n' + 
                                                        '<div class="mt-2 mt-md-0">\n' + 
                                                            '<input type="button" class="btn btn-primary btn-block inner" value="Delete"/>\n' + 
                                                        '</div>\n' + 
                                                    '</div>\n' + 
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();    
            }
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/kawsar/delar/Modules/Products/Resources/views/barcodes.blade.php ENDPATH**/ ?>