<?php

namespace Modules\Categories\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Categories::orderBy('created_at', 'DESC')->get();

        return view('categories::index', compact('categories'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'  => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $category               = new categories;
            $category->name         = $data['name'];
            $category->discount     = $data['discount'];
            $category->status       = $data['status'];
            $category->created_by   = $user_id;

            if ($category->save())
            {   
                return back()->with("success","Brand Created Successfully !!");
            }else
            {
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){

            return back()->with("unsuccess","Not Added");
        }
    }

    public function edit($id)
    {
        $categories     = Categories::orderBy('created_at', 'DESC')->get();
        $find_category  = Categories::find($id);

        return view('categories::edit', compact('categories', 'find_category'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'name'  => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $category               = Categories::find($id);
            $category->name         = $data['name'];
            $category->discount     = $data['discount'];
            $category->status       = $data['status'];
            $category->updated_by   = $user_id;

            if ($category->save())
            {   
                return redirect()->route('categories_index')->with("success","Brand updated Successfully !!");
            }else
            {
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){

            return back()->with("unsuccess","Not Added");
        }
    }

    public function listAjax()
    {
        $tables     = TableNameByUsers();
        $categories = $tables['categories']->orderBy('created_at', 'DESC')->get();

        return Response::json($categories);
    }
}