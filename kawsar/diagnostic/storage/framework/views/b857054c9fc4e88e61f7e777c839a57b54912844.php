<?php $__env->startSection('title', 'Create Doctor Appointment'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Create Doctor Appointment</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Doctor Appointment</a></li>
                                    <li class="breadcrumb-item active">Create Doctor Appointment</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('products_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                <?php echo e(csrf_field()); ?>


                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label>Select Doctor * </label>
                                        <select  id="doctor_id" name="doctor_id" class="form-control select2" required>
                                           <option value="">Select Doctor</option>
                                           <option value="1">--Walk In Doctor--</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label>Schedule Date*</label>
                                        <select id="schedule_id" name="schedule_id" class="form-control select2" required>
                                        </select>
                                    </div>


                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label>Patient Name *</label>
                                        <input type="text" name="buying_price" class="inner form-control" id="buying_price_0" placeholder="Purchase Price"  required />
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label>Patient Number *</label>
                                        <input type="text" name="buying_price" class="inner form-control" id="buying_price_0" placeholder="Purchase Price"  required />
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('products_index')); ?>">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script type="text/javascript">
    $(document).on("change", "#doctor_id" , function() {

            var site_url        = $('.site_url').val();
            var doctor_id      = $("#doctor_id").val();

            $.get(site_url + '/doctorappointment/get-doctor-schedule/' + doctor_id, function(data){

                
                 $("#schedule_id").empty();

                 $.each(data, function(i, schedule){
                    $('#schedule_id').append('<option value="'+ schedule.id +'" selected>' + schedule.schedule_date + ' Time ' + schedule.from_time + 'To' + schedule.to_time + '</option>');
                });
            });
        });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/kawsar/diagnostic/Modules/DoctorAppointment/Resources/views/index.blade.php ENDPATH**/ ?>