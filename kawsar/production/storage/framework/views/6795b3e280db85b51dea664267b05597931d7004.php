

<?php $__env->startSection('title', 'Customer Due List'); ?>

<?php $__env->startSection('styles'); ?>
    <style type="text/css">
        @media  print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page  {
                margin: 0cm ! important;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.Customer_due_list')); ?></h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.reports')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.Customer_due_list')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['address']); ?></p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['contact_number']); ?></p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e(__('messages.Customer_due_list')); ?></h4>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><strong><?php echo e(__('messages.from_date')); ?></strong> <?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?> <strong><?php echo e(__('messages.to_date')); ?></strong> <?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="<?php echo e(route('due_report_customer_print_due')); ?>" enctype="multipart/form-data">
                                    <div class="form-group row">
                                        <div style="margin-bottom: 10px;display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="<?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="<?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        <?php if(Auth()->user()->branch_id == 1): ?>
                                        <div style="display: none" class="col-lg-2 col-md-2 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select style="width: 100%" id="branch_id" name="branch_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" onchange="getBranchCustomers()" required>
                                                   <?php if($branches->count() > 0): ?>
                                                   <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                   <option value="<?php echo e($value['id']); ?>" <?php echo e($value->id == $branch_id ? 'selected' : ''); ?>><?php echo e($value['name']); ?></option>
                                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                   <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php endif; ?>

                                        <div class="col-lg-3 col-md-3 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="customer_id" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="<?php echo e($customer_name != null ? $customer_name['id'] : 0); ?>" selected><?php echo e($customer_name != null ? $customer_name['name'] : '--All Customers--'); ?></option>
                                                </select>
                                            </div>
                                        </div>

                                        <div style="display: none" class="col-lg-2 col-md-2 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="ledger_type" style="width: 100" class="form-control select2" name="ledger_type">
                                                    <option value="0" selected>--<?php echo e(__('messages.show_all')); ?>--</option>
                                                    <option <?php echo e($ledger_type == 1 ? 'selected' : ''); ?> value="1">Dues Available</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="<?php echo e(__('messages.search')); ?>"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center"><?php echo e(__('messages.sl')); ?></th>
                                            <th style="text-align: center"><?php echo e(__('messages.customer')); ?></th>
                                            <th style="text-align: center"><?php echo e(__('messages.advance')); ?></th>
                                            <th style="text-align: center"><?php echo e(__('messages.phone')); ?></th>
                                            <th style="text-align: center"><?php echo e(__('messages.balance')); ?></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                            $i = 1;
                                        ?>
                                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td style="text-align: center;"><?php echo e($i); ?></td>
                                            <td style="text-align: left;"><?php echo e($value['customer_name']); ?> </td>
                                            <td style="text-align: left;"><?php echo e($value['address']); ?></td>
                                            <td style="text-align: center;"><?php echo e($value['phone']); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($value['balance'],0,'.',',')); ?></td>
                                        </tr>
                                         
                                        <?php $i++; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                        <tr>
                                            <th colspan="4" style="text-align: right;"><?php echo e(__('messages.total')); ?></th>
                                            <th colspan="1" style="text-align: right;"><?php echo e(number_format($total_balance,0,'.',',')); ?></th>
                                        </tr>

                                    </tbody>
                                    
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    $( document ).ready(function() {
        
        var site_url    = $('.site_url').val();

        $("#customer_id").select2({
            ajax: { 
            url:  site_url + '/invoices/customer/list/invoices',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['contact_type'] == 0 || result['id'] == 0)
                {
                    return result['text'];
                }
            },
        });
    });

    function pad (str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }
    
    function getBranchCustomers()
    {
        var site_url    = $('.site_url').val();
        var branch_id  = $("#branch_id").val();
         
        $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/reports/branch-contacts/get-contacts/' + branch_id,
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },
    
                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
        });
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/kawsar/production/Modules/Reports/Resources/views/due_report_customer_due.blade.php ENDPATH**/ ?>