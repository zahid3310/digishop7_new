

<?php $__env->startSection('title', 'Sales Summary'); ?>

<style type="text/css">
    @media  print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page  {
            margin: 0cm ! important;
        }
    }

    .table td, .table th {
        font-size: 12px !important;
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.sales_summary')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.reports')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.sales_summary')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['address']); ?></p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['contact_number']); ?></p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e(__('messages.sales_summary')); ?> <?php echo e(__('messages.reports')); ?></h4>
                                        <p style="text-align: center"><strong><?php echo e(__('messages.from_date')); ?></strong> <?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?> <strong><?php echo e(__('messages.to')); ?></strong> <?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="<?php echo e(route('sales_summary_index')); ?>" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div style="margin-bottom: 10px" class="col-lg-4 col-md-4 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="<?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="<?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        <?php if(Auth()->user()->branch_id == 1): ?>
                                        <div class="col-lg-2 col-md-2 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select style="width: 100%" id="branch_id" name="branch_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" required>
                                                   <?php if($branches->count() > 0): ?>
                                                   <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                   <option value="<?php echo e($value['id']); ?>"><?php echo e($value['name']); ?></option>
                                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                   <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php endif; ?>

                                        <div class="col-lg-4 col-md-4 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="0" selected>-- <?php echo e(__('messages.all_customer')); ?> --</option>
                                                    <?php if(!empty($customers) && ($customers->count() > 0)): ?>
                                                    <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option <?php echo e(isset($_GET['customer_id']) && ($_GET['customer_id'] == $value['id']) ? 'selected' : ''); ?> value="<?php echo e($value['id']); ?>"><?php echo e($value['name']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="<?php echo e(__('messages.search')); ?>"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th><?php echo e(__('messages.sl')); ?></th>
                                            <th><?php echo e(__('messages.invoice')); ?>#</th>
                                            <th><?php echo e(__('messages.date')); ?></th>
                                            <th><?php echo e(__('messages.customer')); ?></th>
                                            <th style="text-align: right"><?php echo e(__('messages.invoice_amount')); ?></th>
                                            <th style="text-align: right"><?php echo e(__('messages.discount')); ?></th>
                                            <th style="text-align: right"><?php echo e(__('messages.receivable')); ?></th>
                                            <th style="text-align: right"><?php echo e(__('messages.received')); ?></th>
                                        </tr>
                                    </thead>


                                    <tbody>

                                        <?php if(!empty($data)): ?>

                                        <?php $serial = 1; ?>

                                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        
                                            <tr>
                                                <td><?php echo e($serial); ?></td>
                                                <td>
                                                    <a href="<?php echo e(route('invoices_show', $value['invoice_id'])); ?>" target="_blank">
                                                        <?php echo e($value['invoice_number']); ?>

                                                    </a>
                                                </td>
                                                <td><?php echo e($value['invoice_date']); ?></td>
                                                <td><?php echo e($value['customer_name']); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($value['invoice_amount'],0,'.',',')); ?></td>
                                                <td style="text-align: right">
                                                    <?php echo e(number_format($value['total_discount'] + $value['discount'],0,'.',',')); ?>


                                                    <?php if($value['total_discount_note'] != null): ?> 
                                                        <br>
                                                    <?php endif; ?>

                                                    <?php echo e($value['total_discount_note'] != null ? $value['total_discount_note'] : ''); ?>

                                                </td>
                                                <td style="text-align: right"><?php echo e(number_format($value['invoice_amount'],2,'.',',')); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($value['paid_amount'],0,'.',',')); ?></td>
                                            </tr>

                                            <?php $serial++; ?>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>

                                    </tbody>
                                    <tr>
                                        <th style="text-align: right" colspan="4"><?php echo e(__('messages.total')); ?></th>
                                        <th style="text-align: right"><?php echo e(number_format($total_invoice_amount,0,'.',',')); ?></th>
                                        <th style="text-align: right"><?php echo e(number_format($total_discount_amount,0,'.',',')); ?></th>
                                        <th style="text-align: right"><?php echo e(number_format($total_receivable,0,'.',',')); ?></th>
                                        <th style="text-align: right"><?php echo e(number_format($total_paid_amount,0,'.',',')); ?></th>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/kawsar/production/Modules/Reports/Resources/views/sales_summary.blade.php ENDPATH**/ ?>