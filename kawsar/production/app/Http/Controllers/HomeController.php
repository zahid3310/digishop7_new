<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Models
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\SalesReturn;
use App\Models\PurchaseReturn;
use App\Models\PurchaseReturnEntries;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\Expenses;
use App\Models\ExpenseEntries;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\Branches;
use App\Models\Users;
use App\Models\Incomes;
use App\Models\Settlements;
use App\Models\Transactions;
use App\Models\BranchInventories;
use App\Models\StockTransfers;
use App\Models\Assets;
use App\Models\Investment;
use App\Models\ReturnInvestment;
use Carbon\Carbon;
use Response;
use Artisan;
use Auth;
use DB;
use Session;
use Cache;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth()->user()->status == 1)
        {
            return view('home');
        }
        else
        {
            return back();
        }
    }

    public function dashboardItems()
    {
        $branch_id                  = Auth::user()->branch_id;
        $total_customers            = Customers::where('contact_type', 0)->where('branch_id', $branch_id)->count();
        $total_suppliers            = Customers::where('contact_type', 1)->where('branch_id', $branch_id)->count();
        $todays_sells               = JournalEntries::where('date', date('Y-m-d'))->where('transaction_head', 'sales')->where('debit_credit', 0)->where('branch_id', $branch_id)->sum('amount');
        $todays_sells_return        = JournalEntries::where('date', date('Y-m-d'))->where('transaction_head', 'sales-return')->where('debit_credit', 0)->where('branch_id', $branch_id)->sum('amount');
        $todays_purchase            = JournalEntries::where('date', date('Y-m-d'))->where('transaction_head', 'purchase')->where('debit_credit', 0)->where('branch_id', $branch_id)->sum('amount');
        $todays_purchase_return     = JournalEntries::where('date', date('Y-m-d'))->where('transaction_head', 'purchase-return')->where('debit_credit', 0)->where('branch_id', $branch_id)->sum('amount');
        $todays_expense             = JournalEntries::where('date', date('Y-m-d'))->where('transaction_head', 'expense')->where('debit_credit', 0)->where('branch_id', $branch_id)->sum('amount');
        $total_expense              = Expenses::sum('amount');
        $total_income               = Incomes::sum('amount');

        
        $date = Carbon::now()->subDays(30);
  
        $egg_production_rate = DB::table("productions")->leftjoin('production_entries','production_entries.production_id','productions.id')->where('date','>=', $date)->sum('production_entries.quantity');
        
    
        $egg_total_stock = ProductEntries::select('stock_in_hand')->Where('id',6)->first();
        


        // $sales_summary              = Invoices::whereYear('invoice_date', date('Y'))
        //                                         ->select(DB::raw('MONTH(invoice_date) month'),
        //                                               DB::raw('SUM(invoice_amount) total'))
        //                                         ->groupBy(DB::raw('MONTH(invoice_date)'))
        //                                         ->get();

        // $purchase_summary           = Bills::whereYear('bill_date', date('Y'))
        //                                         ->select(DB::raw('MONTH(bill_date) month'),
        //                                               DB::raw('SUM(bill_amount) total'))
        //                                         ->groupBy(DB::raw('MONTH(bill_date)'))
        //                                         ->get();
        
        
        $sales_summary              = Incomes::whereYear('income_date', date('Y'))
                                                ->select(DB::raw('MONTH(income_date) month'),
                                                       DB::raw('SUM(amount) total'))
                                                ->groupBy(DB::raw('MONTH(income_date)'))
                                                ->get();

        $purchase_summary           = Expenses::whereYear('expense_date', date('Y'))
                                                ->select(DB::raw('MONTH(expense_date) month'),
                                                       DB::raw('SUM(amount) total'))
                                                ->groupBy(DB::raw('MONTH(expense_date)'))
                                                ->get();
                                                
        $year_sales_summary              = Incomes::select(DB::raw('YEAR(income_date) year'),
                                                       DB::raw('SUM(amount) total'))
                                                ->groupBy(DB::raw('YEAR(income_date)'))
                                                ->get();

        $year_purchase_summary           = Expenses::select(DB::raw('YEAR(expense_date) year'),
                                                       DB::raw('SUM(amount) total'))
                                                ->groupBy(DB::raw('YEAR(expense_date)'))
                                                ->get(); 
                                                
                                                
        // Current Blance Start
        
            $user_info  = Users::find(1);
            $branch_id  = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;
            $date       = date('Y-m-d');
            $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
            $to_date    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
    
            $result                 = JournalEntries::where('branch_id', $branch_id)
                                                ->select('journal_entries.*')
                                                ->orderBy('id', 'ASC')
                                                ->get();
    
            $paid_through_accounts  = Accounts::where('account_type_id', 4)
                                                ->whereNotIn('id', [2,3])
                                                ->where('status', 1)
                                                ->get();
    
            foreach ($paid_through_accounts as $key => $value)
            {   
                $debit      = $result->where('debit_credit', 0)->where('account_id', $value->id)->sum('amount');
                $credit     = $result->where('debit_credit', 1)->where('account_id', $value->id)->sum('amount');
    
                $balance    = $credit - $debit;
    
                $currentdata[$value->id]['account_name']     = $value->account_name;
                $currentdata[$value->id]['balance']          = $balance;
            }
            
            $total_balance = 0;
            
            foreach ($currentdata as $key => $value)
            {   
                $total_balance = $total_balance + $value['balance'];
            }
            
        // Current Blance End
        
        $assets = Assets::all();
        
        $invest = Investment::all();
        $return_invest = ReturnInvestment::all();


        $data['sales_summary']              = $sales_summary;
        $data['purchase_summary']           = $purchase_summary;
        $data['year_sales_summary']         = $year_sales_summary;
        $data['year_purchase_summary']      = $year_purchase_summary;
        $data['total_customers']            = $total_customers;
        $data['total_suppliers']            = $total_suppliers;
        $data['todays_sells']               = $todays_sells;
        $data['todays_sells_return']        = $todays_sells_return;
        $data['todays_purchase']            = $todays_purchase;
        $data['todays_purchase_return']     = $todays_purchase_return;
        $data['todays_expenses']            = $todays_expense;
        $data['total_expense']              = $total_expense;
        $data['total_income']               = $total_income;
        $data['total_sells']                = JournalEntries::where('transaction_head', 'sales')->where('debit_credit', 0)->where('branch_id', $branch_id)->sum('amount');
        $data['total_purchase']             = JournalEntries::where('transaction_head', 'purchase')->where('debit_credit', 0)->where('branch_id', $branch_id)->sum('amount');;
        $data['customer_dues']              = Customers::where('contact_type', 0)->where('branch_id', $branch_id)->where('balance', '>', 0)->sum('balance');
        $data['supplier_dues']              = Customers::where('contact_type', 1)->where('branch_id', $branch_id)->where('balance', '>', 0)->sum('balance');

        $data['customer_advance']              = Customers::where('contact_type', 0)->where('branch_id', $branch_id)->where('balance', '<', 0)->sum('balance');

        $data['current_blance']             = $total_balance;
        $data['total_assets']               = $assets->sum('asset_value') + $total_balance + $data['customer_dues'] + $data['customer_advance'] ;

        $data['current_invest']             = $invest->sum('amount') - $return_invest->sum('amount');

        $data['business_balance']           = $data['total_assets'] - $data['current_invest'];
        $data['egg_production_rate']        = $egg_production_rate/30;
        $data['egg_total_stock']            = $egg_total_stock->stock_in_hand;
        
        
        
        

        return Response::json($data);
    }

    public function stockOutItems()
    {
        $products   = ProductEntries::whereRaw('product_entries.stock_in_hand <= product_entries.alert_quantity')
                                ->count();

        return Response::json($products);
    }

    public function oldDataTransfer()
    {   
        ini_set('max_execution_time', 1200); //300 seconds = 20 minutes
        DB::beginTransaction();
        try{
            // $incomes = Incomes::get();

            // foreach ($incomes as $key => $value)
            // {
            //     //account_id
            //         if ($value->income_category_id == 8) 
            //         {
            //             $account_id  = 27;
            //         }
            //         elseif ($value->income_category_id == 9)
            //         {
            //             $account_id  = 28;
            //         }
            //         elseif ($value->income_category_id == 10)
            //         {
            //             $account_id  = 29;
            //         }
            //         elseif ($value->income_category_id == 11)
            //         {
            //             $account_id  = 30;
            //         }

            //     //paid_through_id
            //         if ($value->paid_through_id  == 1) 
            //         {
            //             $paid_through_id   = 1;
            //         }
            //         elseif ($value->paid_through_id  == 13)
            //         {
            //             $paid_through_id   = 18;
            //         }
            //         elseif ($value->paid_through_id  == 14)
            //         {
            //             $paid_through_id   = 19;
            //         }
            //         elseif ($value->paid_through_id  == 15)
            //         {
            //             $paid_through_id   = 20;
            //         }
            //         elseif ($value->paid_through_id  == 16)
            //         {
            //             $paid_through_id   = 21;
            //         }
            //         elseif ($value->paid_through_id  == 17)
            //         {
            //             $paid_through_id   = 22;
            //         }
            //         elseif ($value->paid_through_id  == 18)
            //         {
            //             $paid_through_id   = 23;
            //         }
            //         elseif ($value->paid_through_id  == 19)
            //         {
            //             $paid_through_id   = 24;
            //         }
            //         elseif ($value->paid_through_id  == 20)
            //         {
            //             $paid_through_id   = 25;
            //         }
            //         elseif ($value->paid_through_id  == 21)
            //         {
            //             $paid_through_id   = 26;
            //         }

            //     \DB::disconnect('mysql'); 
            //     \Config::set('database.connections.mysql.database', 'clients_2_accounts');
            //     \DB::reconnect();

            //     $insert_income                      = new Incomes;
            //     $insert_income->income_number       = $value->income_number;
            //     $insert_income->income_date         = date('Y-m-d', strtotime($value->income_date));
            //     $insert_income->customer_id         = $value->customer_id;
            //     $insert_income->amount              = $value->amount;
            //     $insert_income->paid_through_id     = $paid_through_id;
            //     $insert_income->account_id          = $account_id;
            //     $insert_income->account_information = $value->account_information;
            //     $insert_income->note                = $value->note;
            //     $insert_income->branch_id           = $value->branch_id;
            //     $insert_income->created_by          = $value->created_by;
            //     $insert_income->updated_by          = $value->updated_by;
            //     $insert_income->save();
            // }

            // $expenses = Expenses::get();

            // foreach ($expenses as $key => $value)
            // {
            //     //account_id
            //         if ($value->expense_category_id == 10) 
            //         {
            //             $account_id  = 31;
            //         }
            //         elseif ($value->expense_category_id == 25)
            //         {
            //             $account_id  = 32;
            //         }
            //         elseif ($value->expense_category_id == 26)
            //         {
            //             $account_id  = 33;
            //         }
            //         elseif ($value->expense_category_id == 27)
            //         {
            //             $account_id  = 34;
            //         }
            //         elseif ($value->expense_category_id == 28)
            //         {
            //             $account_id  = 35;
            //         }
            //         elseif ($value->expense_category_id == 29)
            //         {
            //             $account_id  = 36;
            //         }
            //         elseif ($value->expense_category_id == 30)
            //         {
            //             $account_id  = 37;
            //         }
            //         elseif ($value->expense_category_id == 31)
            //         {
            //             $account_id  = 38;
            //         }
            //         elseif ($value->expense_category_id == 32)
            //         {
            //             $account_id  = 39;
            //         }
            //         elseif ($value->expense_category_id == 33)
            //         {
            //             $account_id  = 40;
            //         }
            //         elseif ($value->expense_category_id == 34)
            //         {
            //             $account_id  = 41;
            //         }
            //         elseif ($value->expense_category_id == 35)
            //         {
            //             $account_id  = 42;
            //         }
            //         elseif ($value->expense_category_id == 36)
            //         {
            //             $account_id  = 43;
            //         }
            //         elseif ($value->expense_category_id == 37)
            //         {
            //             $account_id  = 44;
            //         }
            //         elseif ($value->expense_category_id == 38)
            //         {
            //             $account_id  = 45;
            //         }


            //     //paid_through_id
            //         if ($value->paid_through_id  == 1) 
            //         {
            //             $paid_through_id   = 1;
            //         }
            //         elseif ($value->paid_through_id  == 13)
            //         {
            //             $paid_through_id   = 18;
            //         }
            //         elseif ($value->paid_through_id  == 14)
            //         {
            //             $paid_through_id   = 19;
            //         }
            //         elseif ($value->paid_through_id  == 15)
            //         {
            //             $paid_through_id   = 20;
            //         }
            //         elseif ($value->paid_through_id  == 16)
            //         {
            //             $paid_through_id   = 21;
            //         }
            //         elseif ($value->paid_through_id  == 17)
            //         {
            //             $paid_through_id   = 22;
            //         }
            //         elseif ($value->paid_through_id  == 18)
            //         {
            //             $paid_through_id   = 23;
            //         }
            //         elseif ($value->paid_through_id  == 19)
            //         {
            //             $paid_through_id   = 24;
            //         }
            //         elseif ($value->paid_through_id  == 20)
            //         {
            //             $paid_through_id   = 25;
            //         }
            //         elseif ($value->paid_through_id  == 21)
            //         {
            //             $paid_through_id   = 26;
            //         }

            //     \DB::disconnect('mysql'); 
            //     \Config::set('database.connections.mysql.database', 'clients_2_accounts');
            //     \DB::reconnect();

            //     $insert_expense                      = new Expenses;
            //     $insert_expense->expense_number      = $value->expense_number;
            //     $insert_expense->expense_date        = date('Y-m-d', strtotime($value->expense_date));
            //     $insert_expense->customer_id         = $value->customer_id;
            //     $insert_expense->amount              = $value->amount;
            //     $insert_expense->paid_through_id     = $paid_through_id;
            //     $insert_expense->account_id          = $account_id;
            //     $insert_expense->account_information = $value->account_information;
            //     $insert_expense->note                = $value->note;
            //     $insert_expense->branch_id           = $value->branch_id;
            //     $insert_expense->created_by          = $value->created_by;
            //     $insert_expense->updated_by          = $value->updated_by;
            //     $insert_expense->save();
            // }

            // $Settlements = Settlements::get();

            // foreach ($Settlements as $key => $value)
            // {
            //     //paid_through_id
            //         if ($value->paid_through_id  == 1) 
            //         {
            //             $paid_through_id   = 1;
            //         }
            //         elseif ($value->paid_through_id  == 13)
            //         {
            //             $paid_through_id   = 18;
            //         }
            //         elseif ($value->paid_through_id  == 14)
            //         {
            //             $paid_through_id   = 19;
            //         }
            //         elseif ($value->paid_through_id  == 15)
            //         {
            //             $paid_through_id   = 20;
            //         }
            //         elseif ($value->paid_through_id  == 16)
            //         {
            //             $paid_through_id   = 21;
            //         }
            //         elseif ($value->paid_through_id  == 17)
            //         {
            //             $paid_through_id   = 22;
            //         }
            //         elseif ($value->paid_through_id  == 18)
            //         {
            //             $paid_through_id   = 23;
            //         }
            //         elseif ($value->paid_through_id  == 19)
            //         {
            //             $paid_through_id   = 24;
            //         }
            //         elseif ($value->paid_through_id  == 20)
            //         {
            //             $paid_through_id   = 25;
            //         }
            //         elseif ($value->paid_through_id  == 21)
            //         {
            //             $paid_through_id   = 26;
            //         }

            //     \DB::disconnect('mysql'); 
            //     \Config::set('database.connections.mysql.database', 'clients_2_accounts');
            //     \DB::reconnect();

            //     $settlement                    = new Settlements;
            //     $settlement->customer_id       = $value->customer_id;
            //     $settlement->date              = date('Y-m-d', strtotime($value->settlement_date));
            //     $settlement->amount            = $value['amount'];
            //     $settlement->paid_through_id   = $paid_through_id;
            //     $settlement->note              = $value['note'];
            //     $settlement->type              = $value['type'];
            //     $settlement->branch_id         = $value->branch_id;
            //     $settlement->created_by        = $value->created_by;
            //     $settlement->updated_by        = $value->updated_by;
            //     $settlement->save();
            // }


            // $transactions = Transactions::get();

            // foreach ($transactions as $key => $value)
            // {
            //     //paid_through_id
            //         if ($value->paid_through  == 1) 
            //         {
            //             $paid_through   = 1;
            //         }
            //         elseif ($value->paid_through  == 13)
            //         {
            //             $paid_through   = 18;
            //         }
            //         elseif ($value->paid_through  == 14)
            //         {
            //             $paid_through   = 19;
            //         }
            //         elseif ($value->paid_through  == 15)
            //         {
            //             $paid_through   = 20;
            //         }
            //         elseif ($value->paid_through  == 16)
            //         {
            //             $paid_through   = 21;
            //         }
            //         elseif ($value->paid_through  == 17)
            //         {
            //             $paid_through   = 22;
            //         }
            //         elseif ($value->paid_through  == 18)
            //         {
            //             $paid_through   = 23;
            //         }
            //         elseif ($value->paid_through  == 19)
            //         {
            //             $paid_through   = 24;
            //         }
            //         elseif ($value->paid_through  == 20)
            //         {
            //             $paid_through   = 25;
            //         }
            //         elseif ($value->paid_through  == 21)
            //         {
            //             $paid_through   = 26;
            //         }


            //     if ($value->account_head == 'customer-opening-balance')
            //     {
            //         debit($customer_id=$value->customer_id, $date=$value->date, $account_id=8, $amount=$value->amount, $note='Customer Opening Balance', $transaction_head='customer-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$value->invoice_id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

            //         credit($customer_id=$value->customer_id, $date=$value->date, $account_id=2, $amount=$value->amount, $note='Customer Opening Balance', $transaction_head='customer-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$value->invoice_id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            //     }
            //     elseif ($value->account_head == 'supplier-opening-balance')
            //     {
            //         debit($customer_id=$value->customer_id, $date=$value->date, $account_id=3, $amount=$value->amount, $note='Supplier Opening Balance', $transaction_head='supplier-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$value->bill_id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

            //         credit($customer_id=$value->customer_id, $date=$value->date, $account_id=9, $amount=$value->amount, $note='Supplier Opening Balance', $transaction_head='supplier-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$value->bill_id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            //     }
            //     elseif ($value->account_head == 'sales')
            //     {   
            //         if ($value->account_head != 'customer-advance-adjustment') 
            //         {
            //             if ($value->note == 'বিক্রয় বাবদ গ্রহণযোগ্য')
            //             {
            //                 debit($customer_id=$value->customer_id, $date=$value->date, $account_id=8, $amount=$value->amount, $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$value->invoice_id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

            //                 credit($customer_id=$value->customer_id, $date=$value->date, $account_id=4, $amount=$value->amount, $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$value->invoice_id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            //             }

            //             if ($value->note == 'বিক্রয় বাবদ আদায়')
            //             {
            //                 $curr_balance = CurrentBalance::where('transaction_date', $value->date)
            //                                             ->where('customer_id', $value->customer_id)
            //                                             ->where('amount', $value->amount)
            //                                             ->where('transaction_head', 'sales')
            //                                             ->where('associated_id', $value->invoice_id)
            //                                             ->first();
                                                        
            //                 if($transactions[$key - 1]['transaction_head'] == 'customer-advance-adjustment')
            //                 {  
            //                   $amo  =  $value->amount - ($transactions[$key - 1]['amount']);
            //                 }
            //                 else
            //                 {
            //                     $amo  =  $value->amount;
            //                 }
                            
            //                 //paid_through_id
            //                 if ($curr_balance['paid_through_id']  == 1) 
            //                 {
            //                     $paid_through   = 1;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 13)
            //                 {
            //                     $paid_through   = 18;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 14)
            //                 {
            //                     $paid_through   = 19;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 15)
            //                 {
            //                     $paid_through   = 20;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 16)
            //                 {
            //                     $paid_through   = 21;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 17)
            //                 {
            //                     $paid_through   = 22;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 18)
            //                 {
            //                     $paid_through   = 23;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 19)
            //                 {
            //                     $paid_through   = 24;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 20)
            //                 {
            //                     $paid_through   = 25;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 21)
            //                 {
            //                     $paid_through   = 26;
            //                 }

            //                 debit($customer_id=$value->customer_id, $date=$value->date, $account_id=$paid_through, $amount=$amo, $note=null, $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$value->invoice_id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

            //                 credit($customer_id=$value->customer_id, $date=$value->date, $account_id=8, $amount=$amo, $note=null, $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$value->invoice_id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            //             }
            //         }
            //     }
            //     elseif ($value->account_head == 'purchase')
            //     {   
            //         if ($value->account_head != 'supplier-advance-adjustment') 
            //         {
            //             if ($value->note == 'ক্রয় বাবদ প্রদেয়')
            //             {
            //                 debit($customer_id=$value->customer_id, $date=$value->date, $account_id=6, $amount=$value->amount, $note=null, $transaction_head='purchase', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$value->bill_id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

            //                 credit($customer_id=$value->customer_id, $date=$value->date, $account_id=9, $amount=$value->amount, $note=null, $transaction_head='purchase', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$value->bill_id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            //             }

            //             if ($value->note == 'ক্রয় বাবদ ব্যায়')
            //             { 
            //                 $curr_balance = CurrentBalance::where('transaction_date', $value->date)
            //                                             ->where('customer_id', $value->customer_id)
            //                                             ->where('amount', $value->amount)
            //                                             ->where('transaction_head', 'purchase')
            //                                             ->where('associated_id', $value->bill_id)
            //                                             ->first();
                                                        
            //                 if($transactions[$key - 1]['transaction_head'] == 'supplier-advance-adjustment')
            //                 {  
            //                   $amo  =  $value->amount - ($transactions[$key - 1]['amount']);
            //                 }
            //                 else
            //                 {
            //                     $amo  =  $value->amount;
            //                 }
                            
            //                 //paid_through_id
            //                 if ($curr_balance['paid_through_id']  == 1) 
            //                 {
            //                     $paid_through   = 1;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 13)
            //                 {
            //                     $paid_through   = 18;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 14)
            //                 {
            //                     $paid_through   = 19;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 15)
            //                 {
            //                     $paid_through   = 20;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 16)
            //                 {
            //                     $paid_through   = 21;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 17)
            //                 {
            //                     $paid_through   = 22;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 18)
            //                 {
            //                     $paid_through   = 23;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 19)
            //                 {
            //                     $paid_through   = 24;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 20)
            //                 {
            //                     $paid_through   = 25;
            //                 }
            //                 elseif ($curr_balance['paid_through_id']  == 21)
            //                 {
            //                     $paid_through   = 26;
            //                 }

            //                 debit($customer_id=$value->customer_id, $date=$value->date, $account_id=9, $amount=$amo, $note=null, $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$value->bill_id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

            //                 credit($customer_id=$value->customer_id, $date=$value->date, $account_id=$paid_through, $amount=$amo, $note=null, $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$value->bill_id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            //             }
            //         }
            //     }
            //     elseif ($value->account_head == 'previoue-due-collection')
            //     {
            //         debit($customer_id=$value->customer_id, $date=$value->date, $account_id=$paid_through, $amount=$value->amount, $note=null, $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

            //         credit($customer_id=$value->customer_id, $date=$value->date, $account_id=8, $amount=$value->amount, $note=null, $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            //     }
            //     elseif ($value->account_head == 'previoue-due-paid')
            //     {
            //         debit($customer_id=$value->customer_id, $date=$value->date, $account_id=9, $amount=$value->amount, $note=null, $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

            //         credit($customer_id=$value->customer_id, $date=$value->date, $account_id=$paid_through, $amount=$value->amount, $note=null, $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            //     }
            //     elseif ($value->account_head == 'sales-return')
            //     {
            //         debit($customer_id=$value->customer_id, $date=$value->date, $account_id=4, $amount=$value->amount, $note=null, $transaction_head='sales-return', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=$value->sales_return_id, $purchase_return_id=null);

            //         credit($customer_id=$value->customer_id, $date=$value->date, $account_id=9, $amount=$value->amount, $note=null, $transaction_head='sales-return', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=$value->sales_return_id, $purchase_return_id=null);

            //         debit($customer_id=$value->customer_id, $date=$value->date, $account_id=9, $amount=$value->amount, $note=null, $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=$value->sales_return_id, $purchase_return_id=null);

            //         credit($customer_id=$value->customer_id, $date=$value->date, $account_id=$paid_through, $amount=$value->amount, $note=null, $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=$value->sales_return_id, $purchase_return_id=null);
            //     }
            //     elseif ($value->account_head == 'purchase-return')
            //     {
            //         debit($customer_id=$value->customer_id, $date=$value->date, $account_id=6, $amount=$value->amount, $note=null, $transaction_head='purchase-return', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=$value->purchase_return_id);

            //         credit($customer_id=$value->customer_id, $date=$value->date, $account_id=8, $amount=$value->amount, $note=null, $transaction_head='purchase-return', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=$value->purchase_return_id);

            //         debit($customer_id=$value->customer_id, $date=$value->date, $account_id=$paid_through, $amount=$value->amount, $note=null, $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=$value->purchase_return_id);

            //         credit($customer_id=$value->customer_id, $date=$value->date, $account_id=9, $amount=$value->amount, $note=null, $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=$value->purchase_return_id);
            //     }
            //     elseif ($value->account_head == 'income')
            //     {
            //         $income = Incomes::find($value->income_id);

            //         debit($customer_id=null, $date=$value->date, $account_id=$paid_through, $amount=$value->amount, $note=null, $transaction_head='income', $income_id=$value->income_id, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

            //         credit($customer_id=null, $date=$value->date, $account_id=$income != null ? $income->account_id : 27, $amount=$value->amount, $note=null, $transaction_head='income', $income_id=$value->income_id, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            //     }
            //     elseif ($value->account_head == 'expense')
            //     {
            //         $expense = Expenses::find($value->expense_id);

            //         debit($customer_id=null, $date=$value->date, $account_id=$expense != null ? $expense->account_id : 32, $amount=$value->amount, $note=null, $transaction_head='expense', $income_id=null, $expense_id=$value->expense_id, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

            //         credit($customer_id=null, $date=$value->date, $account_id=$paid_through, $amount=$value->amount, $note=null, $transaction_head='expense', $income_id=null, $expense_id=$value->expense_id, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            //     }
            //     elseif ($value->account_head == 'customer-settlement')
            //     {
            //         debit($customer_id=$value->customer_id, $date=$value->date, $account_id=9, $amount=$value->amount, $note=null, $transaction_head='customer-settlement', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=$value->settlement_id, $sales_return_id=null, $purchase_return_id=null);

            //         credit($customer_id=$value->customer_id, $date=$value->date, $account_id=$paid_through, $amount=$value->amount, $note=null, $transaction_head='customer-settlement', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=$value->settlement_id, $sales_return_id=null, $purchase_return_id=null);
            //     }
            //     elseif ($value->account_head == 'sales-return-payment')
            //     {
            //         debit($customer_id=$value->customer_id, $date=$value->date, $account_id=9, $amount=$value->amount, $note=null, $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=$value->sales_return_id, $purchase_return_id=null);

            //         credit($customer_id=$value->customer_id, $date=$value->date, $account_id=$paid_through, $amount=$value->amount, $note=null, $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=$value->sales_return_id, $purchase_return_id=null);
            //     }
            // }
            
            
            // //Stock Correction
            // $product_entries  = ProductEntries::get();
            
            // foreach($product_entries as $val)
            // {   
            //     $find_branch_inventory    = BranchInventories::where('product_entry_id', $val->id)->sum('stock_in_hand');
                
            //     $product_entry_update                   = ProductEntries::find($val->id);
            //     $product_entry_update->total_sold       = 0;
            //     $product_entry_update->opening_stock    = $product_entry_update['stock_in_hand'] + $find_branch_inventory;
            //     $product_entry_update->save();
                
                
            //     $transfers    = BranchInventories::where('product_entry_id', $val->id)->get();
                
            //     foreach($transfers as $val1)
            //     {
            //         $transfer                       = new StockTransfers;
            //         $transfer->date                 = date('Y-m-d');
            //         $transfer->product_entry_id     = $val->id;
            //         $transfer->transfer_from        = 1;
            //         $transfer->transfer_to          = $val1->branch_id;
            //         $transfer->quantity             = $val1->stock_in_hand;
            //         $transfer->note                 = 'Opening Balance when New Software Start';
            //         $transfer->main_unit_id         = $val->unit_id;
            //         $transfer->conversion_unit_id   = $val->unit_id;
            //         $transfer->created_by           = Auth::user()->id;
            //         $transfer->save();
            //     }
            // }
            
            DB::commit();
            dd('Done');
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }


    public function changeLocale(Request $request)
    {
        Cache::put('locale', $request->locale);
        // $data = Cache::get('locale');
        // dd($data);

        // \Session::put('locale', $request->locale);
        return redirect()->back();
    }
}
