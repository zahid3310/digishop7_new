@extends('layouts.app')

@section('title', 'Area Add')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.add_area')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.registers')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.add_area')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('area_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">{{ __('messages.name')}} *</label>
                                        <input name="area_name" type="text" class="form-control" placeholder="{{ __('messages.name')}}" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">{{ __('messages.save')}}</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('customers_index') }}">{{ __('messages.close')}}</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->

                    <div class="col-8">
                        <div class="card">
                            <div class="card-body table-responsive">

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th width="5%">{{ __('messages.sl')}}</th>
                                            <th width="85%">{{ __('messages.name')}}</th>
                                            <th width="10%">Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        @foreach($lists as $list)
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td>{{$list->name}}</td>
                                             <td>
                                                <a data-toggle="modal"  data-target="#exampleModalLong" data-id="{{$list->id}}" data-area="{{$list->name}}" class="btn btn-info areaModel">Edit</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
    
    
<!-- Area Edit Modal  -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Area Name Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="{{route('area_update')}}" method="post">
            @csrf
            <div class="form-group">
                <label>Area Name</label>
                <input type="hidden" class="form-control" name="area_id" id="area_id">
                <input type="text" class="form-control" name="area_name" id="area_name">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
    $(".areaModel").click(function(){
        var area_id   = $(this).data('id');
        var area_name = $(this).data('area');

        $('#area_id').val(area_id);
        $('#area_name').val(area_name);
    });
</script>
@endsection
