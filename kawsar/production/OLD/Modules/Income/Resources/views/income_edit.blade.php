@extends('layouts.app')

@section('title', 'Edit Income')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.edit')}} {{ __('messages.incomes')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.incomes')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.edit')}} {{ __('messages.incomes')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<div class="col-md-12">
                                    <div class="row">
                                        <div style="padding-left: 0px" class="col-lg-10 col-md-10 col-sm-6 col-6">
                                            <h4 class="card-title">{{ __('messages.category')}}</h4>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-6 col-6">
                                            @if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3)
                                            <button style="padding: 3px;border-radius: 4px" type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#myModal">{{ __('messages.add')}}</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            	
                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.sl')}}</th>
                                            <th>{{ __('messages.name')}}</th>
                                            <th>{{ __('messages.action')}}</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($income_categories) && ($income_categories->count() > 0))
                                    	@foreach($income_categories as $key => $income_category)
	                                        <tr>
	                                            <td>{{ $key + 1 }}</td>
	                                            <td>{{ $income_category['name'] }}</td>
	                                            <td>
	                                            	<div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            @if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3)
                                                            <a class="dropdown-item" href="{{ route('incomes_categories_edit', $income_category['id']) }}">{{ __('messages.edit')}}</a>
                                                            @endif
                                                        </div>
                                                    </div>
	                                            </td>
	                                        </tr>
	                                    @endforeach
	                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->

                    <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('incomes_update', $income_find['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

            						<div class="row">
					                	<div class="col-sm-4">
					                        <div class="form-group">
					                            <label for="income_date">{{ __('messages.income_date')}} *</label>
					                            <input id="income_date" name="income_date" type="text" value="{{ date('d-m-Y', strtotime($income_find['income_date'])) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
					                        </div> 
					                    </div>

					                    <div class="col-sm-4">
	                                    	<div id="contact_id_reload" class="form-group">
		                                        <label class="control-label">{{ __('messages.income_category')}} *</label>
		                                        <select style="width: 100" class="form-control select2" name="income_category_id" required>
		                                        	<option value="">--{{ __('messages.select_category')}}--</option>
													@if(!empty($income_categories))
														@foreach($income_categories as $key => $income_category)
														<option @if(isset($income_find)) {{ $income_find['category_id'] == $income_category['id'] ? 'selected' : '' }} @endif value="{{ $income_category->id }}">{{ $income_category->name }}</option>
														@endforeach
													@endif
		                                        </select>
		                                    </div>
	                                    </div>

					                    <div class="col-sm-4">
					                        <div class="form-group">
					                            <label for="amount">{{ __('messages.amount')}} *</label>
					                            <input id="amount" name="amount" type="text" value="{{ $income_find['amount'] }}" class="form-control" required>
					                        </div>
					                    </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="note">{{ __('messages.paid_through')}} *</label>
                                                <select id="paid_through_id" style="cursor: pointer" name="paid_through" class="form-control select2">
                                                @if(!empty($paid_accounts) && ($paid_accounts->count() > 0))
                                                @foreach($paid_accounts as $key => $paid_account)
                                                    <option {{ $income_find['paid_through_id'] == $paid_account['id'] ? 'selected' : '' }} value="{{ $paid_account['id'] }}">{{ $paid_account['name'] }}</option>
                                                @endforeach
                                                @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="note">{{ __('messages.account_information')}}</label>
                                                <input type="text" name="account_information" class="form-control" id="account_information" value="{{ $income_find['account_information'] }}"/>
                                            </div>
                                        </div>

				                        <div class="col-sm-4">
					                        <div class="form-group">
					                            <label for="note">{{ __('messages.note')}}</label>
					                            <input id="note" name="note" type="text" value="{{ $income_find['note'] }}" class="form-control">
					                        </div>
					                    </div>
				                	</div>

                                    <hr style="margin-top: 0px">

                                    <div class="form-group row">
                                        <div class="button-items col-md-12">
                                            <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light">{{ __('messages.update')}}</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('incomes_index') }}">{{ __('messages.close')}}</a></button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <hr>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">{{ __('messages.income_list')}}</h4>
                                
                                <div style="margin-right: 0px" class="row">
                                    <div class="col-lg-8 col-md-6 col-sm-4 col-4"></div>
                                    <div class="col-lg-1 col-md-2 col-sm-4 col-4">{{ __('messages.search')}}: </div>
                                    <div class="col-lg-3 col-md-4 col-sm-4 col-4">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.sl')}}</th>
                                            <th>{{ __('messages.income_date')}}</th>
                                            <th>{{ __('messages.income_number')}}</th>
                                            <th>{{ __('messages.category')}}</th>
                                            <th>{{ __('messages.amount')}}</th>
                                            <th>{{ __('messages.action')}}</th>
                                        </tr>
                                    </thead>

                                    <tbody id="income_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

    		</div>
		</div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">{{ __('messages.create_income_category')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form id="FormSubmit1" action="{{ route('incomes_categories_store') }}" method="post" files="true" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div style="padding-top: 10px;padding-bottom: 0px" class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="income_category_name">{{ __('messages.category_name')}} *</label>
                                    <input id="income_category_name" name="income_category_name" type="text" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">{{ __('messages.save')}}</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">{{ __('messages.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url        = $('.site_url').val();

            $.get(site_url + '/incomes/income/list/load', function(data){

                var income_list = '';
                $.each(data, function(i, income_data)
                {
                    var serial      = parseFloat(i) + 1;

                    if (income_data.income_category_id != 1)
                    {
                        var edit_url    = site_url + '/incomes/edit/' + income_data.id;
                    }
                    else
                    {
                        var edit_url    = site_url + '/incomes/';
                    }

                    income_list += '<tr>' +
                                        '<td>' +
                                            serial +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(income_data.income_date) +
                                        '</td>' +
                                        '<td>' +
                                           'EXP - ' + income_data.income_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            income_data.category_name +
                                        '</td>' +
                                        '<td>' +
                                           income_data.amount +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '@if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3)' +
                                                    '<a class="dropdown-item" href="' + edit_url +'">' + '{{ __('messages.edit')}}' + '</a>' +
                                                    '@endif' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                });

                $("#income_list").empty();
                $("#income_list").append(income_list);
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var search_text     = $('#searchPayment').val();
            var site_url        = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/incomes/income/search/list/' + search_text, function(data){

                var income_list = '';
                $.each(data, function(i, income_data)
                {
                    var serial      = parseFloat(i) + 1;

                    if (income_data.income_category_id != 1)
                    {
                        var edit_url    = site_url + '/incomes/edit/' + income_data.id;
                    }
                    else
                    {
                        var edit_url    = site_url + '/incomes/';
                    }

                    income_list += '<tr>' +
                                        '<td>' +
                                            serial +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(income_data.income_date) +
                                        '</td>' +
                                        '<td>' +
                                           'EXP - ' + income_data.income_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            income_data.category_name +
                                        '</td>' +
                                        '<td>' +
                                           income_data.amount +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '@if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3)' +
                                                    '<a class="dropdown-item" href="' + edit_url +'">' + '{{ __('messages.edit')}}' + '</a>' +
                                                    '@endif' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                });

                $("#income_list").empty();
                $("#income_list").append(income_list);
            });
        }
    </script>
@endsection