@extends('layouts.app')

@section('title', 'All Purchases')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.all_purchases')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.purchase')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.all_purchases')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {!! Session::get('success') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('unsuccess'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! Session::get('unsuccess') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('errors'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! 'Some required fields are missing..!! Please try again..' !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif
                                
                            <div class="card-body table-responsive">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label class="col-md-5 col-form-label">{{ __('messages.from_date')}}</label>
                                            <div class="col-md-7">
                                                <input style="cursor: pointer" id="search_from_date" type="date" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label class="col-md-5 col-form-label">{{ __('messages.to_date')}}</label>
                                            <div class="col-md-7">
                                                <input style="cursor: pointer" id="search_to_date" type="date" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label style="text-align: right" class="col-md-4 col-form-label">{{ __('messages.customer')}} </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="biller_customer_id" class="form-control select2">
                                                    <option value="0">{{ __('messages.all')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                            <label style="text-align: right" class="col-md-4 col-form-label">{{ __('messages.bill')}} </label>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <select style="width: 73%" id="biller_number" name="biller_number" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                       <option value="0">{{ __('messages.all')}}</option>
                                                    </select>
                                                    <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="searchBillList()">
                                                        <i class="bx bx-search font-size-24"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                
                                <div style="margin-right: 0px;display:none;" class="row">
                                    <div class="col-md-8"></div>
                                    <div class="col-md-1">{{ __('messages.search')}}: </div>
                                    <div class="col-md-3">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.sl')}}</th>
                                            <th>{{ __('messages.date')}}</th>
                                            <th>{{ __('messages.purchase')}}#</th>
                                            <th>{{ __('messages.supplier')}}</th>
                                            <th>{{ __('messages.amount')}}</th>
                                            <th>{{ __('messages.paid')}}</th>
                                            <th>{{ __('messages.due')}}/{{ __('messages.change')}}</th>
                                            <th>{{ __('messages.action')}}</th>
                                        </tr>
                                    </thead>

                                    <tbody id="bill_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();
            
            $("#biller_customer_id").select2({
                ajax: { 
                url:  site_url + '/bills/bill/customerList',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0 || result['contact_type'] == 1 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

             $("#biller_number").select2({
                ajax: { 
                url:  site_url + '/bills/bill/search-biller-number',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });


            $.get(site_url + '/bills/bill/list/load', function(data){

                billList(data);
                
            });
        });
        
        function searchBillList()
        {
            var site_url                = $('.site_url').val();
            var search_from_date        = $('#search_from_date').val();
            var search_to_date          = $('#search_to_date').val();
            var search_customer         = $('#payment_customer_id').val();
            var search_biller_number   = $('#biller_number').val();

            if (search_from_date != '')
            {
                var from_date = $('#search_from_date').val();
            }
            else
            {
                var from_date = 0;
            }

            if (search_to_date != '')
            {
                var to_date = $('#search_to_date').val();
            }
            else
            {
                var to_date = 0;
            }

            if (search_customer != '')
            {
                var customer = $('#payment_customer_id').val();
            }
            else
            {
                var customer = 0;
            }

            if (search_biller_number != '')
            {
                var bill_number = $('#biller_number').val();
            }
            else
            {
                var bill_number = 0;
            }

            // console.log(bill_number);

            $.get(site_url + '/bills/bill/search/list/' + from_date + '/' + to_date + '/' + customer + '/' + bill_number, function(data){

                billList(data);
            });
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var search_text     = $('#searchPayment').val();
            var site_url        = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/bills/bill/search/list/' + search_text, function(data){

                billList(data);

            });
        }

        function billList(data)
        {
            var bill_list = '';
            var site_url  = $('.site_url').val();
            $.each(data, function(i, bill_data)
            {   
                var serial              = parseFloat(i) + 1;
                var edit_url            = site_url + '/bills/edit/' + bill_data.id;
                var print_url           = site_url + '/bills/show/' + bill_data.id;
                var purchase_return     = site_url + '/purchasereturn?supplier_id=' + bill_data.vendor_id + '&bill_id=' + bill_data.id;
                var barcode_print       = site_url + '/products/barcode-print?bill_id=' + bill_data.id;

                if (bill_data.return_id != null)
                {
                    var return_sign  = '&nbsp;' + '&nbsp;' + '<i class="bx bx-repost font-size-15">' + '</i>';
                }
                else
                {
                    var return_sign  = '';
                }

                if (bill_data.contact_type == 0)
                {
                    var type = 'Customer';
                }

                if (bill_data.contact_type == 1)
                {
                    var type = 'Supplier';
                }

                if (bill_data.contact_type == 2)
                {
                    var type = 'Employee';
                }

                bill_list += '<tr>' +
                                    '<td>' +
                                        serial +
                                    '</td>' +
                                    '<td>' +
                                       formatDate(bill_data.bill_date) +
                                    '</td>' +
                                    '<td>' +
                                       'BILL - ' + bill_data.bill_number.padStart(6, '0') + return_sign +
                                    '</td>' +
                                    '<td>' +
                                        bill_data.customer_name +
                                    '</td>' +
                                    '<td>' +
                                       bill_data.bill_amount +
                                    '</td>' +
                                    '<td>' +
                                       parseFloat(bill_data.cash_given) +
                                    '</td>' +
                                    '<td>' +
                                       (parseFloat(bill_data.bill_amount) - parseFloat(bill_data.cash_given)) +
                                    '</td>' +
                                    '<td>' +
                                        '<div class="dropdown">' +
                                            '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                            '</a>' +
                                            '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                '@if(Auth::user()->role == 1)' +
                                                '<a class="dropdown-item" href="' + edit_url +'">' + '{{ __('messages.edit')}}' + '</a>' +
                                                '@endif' +
                                                '<a class="dropdown-item" href="' + print_url +'" target="_blank">' + '{{ __('messages.show')}}' + '</a>' +
                                                '<a class="dropdown-item" href="' + barcode_print +'" target="_blank">' + '{{ __('messages.print_barcode')}}' + '</a>' +
                                                '<a class="dropdown-item" href="' + purchase_return +'" target="_blank">' + '{{ __('messages.purchase_return')}}' + '</a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</td>' +
                                '</tr>';
            });

            $("#bill_list").empty();
            $("#bill_list").append(bill_list);
        }
    </script>
@endsection