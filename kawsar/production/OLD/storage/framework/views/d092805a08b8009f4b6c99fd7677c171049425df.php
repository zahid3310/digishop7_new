<!DOCTYPE html>
<html>

<head>
    <title><?php echo e(__('messages.daily_report')); ?></title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p><?php echo e($user_info['address']); ?></p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold"><?php echo e(__('messages.daily_report')); ?></h6>
                    </div>

                    <div class="ibox-content">
                        <!-- Header Start -->
                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center"><?php echo e(__('messages.from_date')); ?></th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center"><?php echo e($from_date); ?> <strong><?php echo e(__('messages.to')); ?></strong> <?php echo e($to_date); ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Header End -->

                        <!-- Sales Statement Start -->
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right: 5px">
                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead class="theight">
                                        <tr style="background:#ddd;">
                                            <th style="text-align: center;width: 20%"><?php echo e(__('messages.description_deposit')); ?></th>
                                            <th style="text-align: center;width: 10%"><?php echo e(__('messages.taka')); ?></th>
                                        </tr>
                                    </thead>

                                    <tbody class="theight">

                                        <?php
                                            $total_income = 0;
                                        ?>
                                        <?php if($data['incomes']->count() > 0): ?>
                                        <?php $__currentLoopData = $data['incomes']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                            if ($value['account_id'] != null)
                                            {
                                                $account   = ' ('.$value->paidThrough->name.')';
                                            }
                                            else
                                            {
                                                $account   = '';
                                            }

                                            if ($value['transaction_head'] == 'sales')
                                            {
                                                $trans_number   = 'INV - '.str_pad($value->invoice->invoice_number, 6, "0", STR_PAD_LEFT);
                                                $details        = 'বিক্রয় বাবদ আদায়';
                                            }
                                            elseif ($value['transaction_head'] == 'receivable')
                                            {
                                                $trans_number   = 'INV - '.str_pad($value->invoice->invoice_number, 6, "0", STR_PAD_LEFT);
                                                $details        = 'বিক্রয় বাবদ গ্রহণযোগ্য';
                                            }
                                            elseif ($value['transaction_head'] == 'payment-receive')
                                            {
                                                $trans_number   = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                                $details        = 'পূর্বের বকেয়া আদায়';
                                            }
                                            elseif ($value['transaction_head'] == 'payable')
                                            {
                                                $trans_number   = 'SR - '.str_pad($value->salesReturn->sales_return_number, 6, "0", STR_PAD_LEFT);
                                                $details        = 'রিটার্ন বাবদ প্রদেয়';
                                            }
                                            elseif ($value['transaction_head'] == 'sales-return')
                                            {
                                                $trans_number   = 'SR - '.str_pad($value->salesReturn->sales_return_number, 6, "0", STR_PAD_LEFT);
                                                $details        = 'রিটার্ন বাবদ প্রদান';
                                            }
                                            elseif ($value['transaction_head'] == 'income')
                                            {
                                                $trans_number   = 'INC - '.str_pad($value->income->income_number, 6, "0", STR_PAD_LEFT);
                                                $details        = 'Income';
                                            }
                                            
                                        ?>
                                        <tr>
                                            <td style="text-align: left;"><?php echo e($value->customer_id != null ? $value->customer->name : ''); ?> <?php echo e($details); ?> </td>
                                            <td style="text-align: right;"><?php echo e($value['amount']); ?></td>
                                        </tr>

                                        <?php
                                            $total_income = $total_income + $value['amount'];
                                        ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>

                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="1" style="text-align: right;"><?php echo e(__('messages.total')); ?></th>
                                            <th style="text-align: right;"><?php echo e($total_income); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left: 5px">
                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead class="theight">
                                        <tr style="background:#ddd;">
                                            <th style="text-align: center;width: 20%"><?php echo e(__('messages.description_withdraw')); ?></th>
                                            <th style="text-align: center;width: 10%"><?php echo e(__('messages.taka')); ?></th>
                                        </tr>
                                    </thead>

                                    <tbody class="theight">

                                        <?php
                                            $total_expense = 0;
                                        ?>

                                        <?php if($data['expenses']->count() > 0): ?>
                                        <?php $__currentLoopData = $data['expenses']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                            if ($value1['account_id'] != null)
                                            {
                                                $account1   = ' ('.$value1->paidThrough->name.')';
                                            }
                                            else
                                            {
                                                $account1   = '';
                                            }

                                            if ($value1['transaction_head'] == 'purchase')
                                            {
                                                $trans_number1   = 'BILL - '.str_pad($value1->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                                $details1        = 'বিক্রয় বাবদ প্রদান';
                                            }
                                            elseif ($value1['transaction_head'] == 'payable')
                                            {
                                                $trans_number1   = 'BILL - '.str_pad($value1->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                                $details1        = 'বিক্রয় বাবদ প্রদেয়';
                                            }
                                            elseif ($value1['transaction_head'] == 'payment-made')
                                            {
                                                $trans_number1   = 'PM - '.str_pad($value1->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                                $details1        = 'পূর্বের বকেয়া প্রদান';
                                            }
                                            elseif ($value1['transaction_head'] == 'receivable')
                                            {
                                                $trans_number1   = 'PR - '.str_pad($value1->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                                $details1        = 'রিটার্ন বাবদ গ্রহণযোগ্য';
                                            }
                                            elseif ($value1['transaction_head'] == 'purchase-return')
                                            {
                                                $trans_number1   = 'PR - '.str_pad($value1->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                                $details1        = 'রিটার্ন বাবদ প্রদান';
                                            }
                                            elseif ($value1['transaction_head'] == 'expense')
                                            {
                                                $trans_number1   = '';
                                                $details1        = 'Expense';
                                            }
                                        ?>
                                        <tr>
                                            <td style="text-align: left;"><?php echo e($value1->customer_id != null ? $value1->customer->name : ''); ?> <?php echo e($details1); ?> </td>
                                            <td style="text-align: right;"><?php echo e($value1['amount']); ?></td>
                                        </tr>

                                        <?php
                                            $total_expense = $total_expense + $value1['amount'];
                                        ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                    
                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="1" style="text-align: right;"><?php echo e(__('messages.total')); ?></th>
                                            <th style="text-align: right;"><?php echo e($total_expense); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- Sales Statement End -->

                        <!-- Footer Start -->
                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/70/Modules/Reports/Resources/views/daily_report_print.blade.php ENDPATH**/ ?>