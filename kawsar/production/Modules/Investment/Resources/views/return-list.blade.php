@extends('layouts.app')

@section('title', 'Return List')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Investment</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Investment</a></li>
                                    <li class="breadcrumb-item active">Return List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">Return List</h4>

                                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.sl')}}</th>
                                            <th>{{ __('messages.date')}}</th>
                                            <th>{{ __('messages.number')}}</th>
                                            <th>{{ __('messages.paid_through')}}</th>
                                            <th>{{ __('messages.account_head')}}</th>
                                            <th>{{ __('messages.amount')}}</th>
                                            <th>{{ __('messages.action')}}</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if($returnList->count() > 0)
                                        @foreach($returnList as $key => $value)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ date('d-m-Y', strtotime($value['investment_date'])) }}</td>
                                            <td>{{ 'RINV - ' . str_pad($value['investment_number'], 6, "0", STR_PAD_LEFT) }}</td>
                                            <td>{{ $value->paidThroughAccount->account_name }}</td>
                                            <td>{{ $value->account->account_name }}</td>
                                            <td>{{ $value->amount }}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                        <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" style="">
                                                        <a class="dropdown-item" href="{{ route('incomes_edit', $value->id) }}">{{ __('messages.edit')}} </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url        = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['id'] != 0 && result['id'] != 1 && result['id'] != 2)
                    {
                        return result['text'];
                    }
                },
            });
            
            $.get(site_url + '/incomes/income/list/load', function(data){

                var income_list = '';
                $.each(data, function(i, income_data)
                {   
                    var serial      = parseFloat(i) + 1;

                    if (income_data.income_category_id != 1)
                    {
                        var edit_url    = site_url + '/incomes/edit/' + income_data.id;
                    }
                    else
                    {
                        var edit_url    = site_url + '/incomes/';
                    }

                    income_list += '<tr>' +
                                        '<td>' +
                                            serial +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(income_data.income_date) +
                                        '</td>' +
                                        '<td>' +
                                           'INC - ' + income_data.income_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            income_data.category_name +
                                        '</td>' +
                                        '<td>' +
                                           income_data.amount +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '@if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3)' +
                                                    '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                    '@endif' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                });

                $("#income_list").empty();
                $("#income_list").append(income_list);
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var search_text     = $('#searchPayment').val();
            var site_url        = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/incomes/income/search/list/' + search_text, function(data){

                var income_list = '';
                $.each(data, function(i, income_data)
                {   
                    var serial      = parseFloat(i) + 1;
                    
                    if (income_data.income_category_id != 1)
                    {
                        var edit_url    = site_url + '/incomes/edit/' + income_data.id;
                    }
                    else
                    {
                        var edit_url    = site_url + '/incomes/';
                    }

                    income_list += '<tr>' +
                                        '<td>' +
                                            serial +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(income_data.income_date) +
                                        '</td>' +
                                        '<td>' +
                                           'INC - ' + income_data.income_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            income_data.category_name +
                                        '</td>' +
                                        '<td>' +
                                           income_data.amount +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '@if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3)' +
                                                    '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                    '@endif' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                });

                $("#income_list").empty();
                $("#income_list").append(income_list);
            });
        }
    </script>
@endsection