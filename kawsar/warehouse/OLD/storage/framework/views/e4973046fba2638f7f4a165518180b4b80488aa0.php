

<?php $__env->startSection('title', 'Show'); ?>

<?php $__env->startPush('styles'); ?>
<style type="text/css">
    .pt-2, .py-2 {
         padding-top: 0rem!important; 
    }

    .mt-3, .my-3 {
         margin-top: 0rem!important; 
    }  

    address {
         margin-bottom: 0rem; 
    }
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Issues</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Issues</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['address']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['contact_number']); ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-4 col-6">
                                        <address>
                                            <strong>Issued To:</strong><br>
                                            <?php echo e($issue->srName->name); ?>

                                            <?php if($issue->srName->address != null): ?>
                                               <br> <?php echo $issue->srName->address; ?> <br>
                                            <?php endif; ?>
                                            <?php if($issue->srName->address == null): ?>
                                                <br>
                                            <?php endif; ?>
                                            <?php echo e($issue->srName->phone); ?>

                                        </address>
                                    </div>

                                    <div class="col-sm-4 hidden-xs">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div style="font-size: 16px" class="col-sm-4 col-6 text-sm-right">
                                        <address>
                                            <strong>Issue Date:</strong><br>
                                            <?php echo e(date('d-m-Y', strtotime($issue['issue_date']))); ?><br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6 hidden-xs">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Issue summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Issue # <?php echo e('SIN - ' . str_pad($issue['issue_number'], 6, "0", STR_PAD_LEFT)); ?></h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-nowrap">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th style="width: 70px;">No.</th>
                                                <th>Item</th>
                                                <th class="text-right">Quantity</th>
                                                <th class="text-right">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">
                                            <?php if(!empty($entries) && ($entries->count() > 0)): ?>
                                            <?php $i = 0; $total_amount = 0; ?>
                                            <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php 
                                                $i++; 
                                                $total_qty = 0;
                                                $total_amo = 0;
                                                foreach ($value as $key1 => $value1)
                                                {
                                                    $total_qty = $total_qty + $value1['quantity'];
                                                    $total_amo = $total_amo + $value1['amount'];
                                                }

                                                $total_amount = $total_amount + $total_amo;
                                            ?>
                                            <tr>
                                                <td><?php echo e($i); ?></td>
                                                <td style="text-align:left;">
                                                    <?php echo productNameShow($key); ?>
                                                </td>
                                                <td class="text-right"><?php echo e($total_qty); ?> <?php echo productUnitNameShow($key); ?></td>
                                                <td class="text-right"><?php echo e($total_amo); ?></td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th style="font-size: 18px" class="text-right" colspan="3">Total</th>
                                                <th style="font-size: 18px" class="text-right"><?php echo e($total_amount); ?></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <?php if($issue['issue_note'] != null): ?>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 style="font-size: 16px"><strong>Note :</strong> <?php echo e($issue['issue_note']); ?></h6>
                                    </div>
                                </div>
                                <?php endif; ?>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script type="text/javascript">
    $( document ).ready(function() {
        // javascript:window.print();
    });

    window.onafterprint = function(e){
        var site_url        = $('.site_url').val();
        window.location.replace(site_url + '/srissues');
    };
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/85/Modules/SrIssues/Resources/views/show.blade.php ENDPATH**/ ?>