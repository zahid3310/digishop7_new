

<?php $__env->startSection('title', 'Purchase'); ?>

<style type="text/css">
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Purchase</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Purchases</a></li>
                                    <li class="breadcrumb-item active">New Purchase</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

            					<form id="FormSubmit" action="<?php echo e(route('bills_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					<?php echo e(csrf_field()); ?>


                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Supplier *</label>
                                            <div class="col-md-8">
                                               <select style="width: 100%" id="customer_id" name="vendor_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" required>
                                                   <option value="2">Walk-In Supplier</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label id="bbShow" for="productname" class="col-md-4 col-form-label">Payable </label>
                                            <div class="col-md-8">
                                                <input id="balance" name="previous_due" type="text" value="" class="form-control" value="0" readonly>
                                                <input id="bbBalance" type="hidden" name="balance_type" class="form-control" value="1">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-2 col-form-label">Note </label>
                                            <div class="col-md-10">
                                                <input id="bill_note" name="bill_note" type="text" value="" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Date * </label>
                                            <div class="col-md-8">
                                                <input id="selling_date" name="selling_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2" style="display:none">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                                                    Add New Product
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Barcode </label>
                                            <div class="col-md-8">
                                                <input type="text" name="product_code_pos" class="inner form-control" id="product_code_pos" placeholder="Scan Product Code" oninput="PosScannerSearch()" autofocus />
                                                <p id="alertMessage" style="display: none">No Product Found !</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <i id="add_field_button" style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button col-md-3"></i>
                                    </div>
                                </div>

                                <div style="display: none">
                                    <select style="padding: 6px;border-radius: 4px;cursor: pointer" class="taxType" id="tax_type_0" name="tax_type" oninput="calculateActualAmount(0)">
                                        <option style="padding: 10px" value="1" selected>BDT</option>
                                        <option style="padding: 10px" value="0">%</option>
                                    </select>
                                    <input id="tax_amount_0" type="text" class="form-control width-xs taxAmount" name="tax_amount" value="0" oninput="calculateActualAmount(0)">
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 485px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        <div class="row di_0">
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 130px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Sub Total</label>
                                            <div class="col-md-7">
                                                <input type="text" id="subTotalBdt" class="form-control">
                                                <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">VAT</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control" id="vat_type_0" name="vat_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="0" <?php echo e(Auth::user()->vat_type == 0 ? 'selected' : ''); ?>>%</option>
                                                    <option style="padding: 10px" value="1" <?php echo e(Auth::user()->vat_type == 1 ? 'selected' : ''); ?>>BDT</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="vat_amount_0" type="text" class="form-control width-xs" name="vat_amount" value="<?php echo e(Auth::user()->vat_amount); ?>" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Discount</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control totalDiscountType" id="total_discount_type_0" name="total_discount_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="1" selected>BDT</option>
                                                    <option style="padding: 10px" value="0">%</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="total_discount_0" type="text" class="form-control totalDiscount" name="total_discount_amount" value="0" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 130px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Dis. Note</label>
                                            <div class="col-md-7">
                                                <input id="total_discount_note" type="text" class="form-control width-xs" name="total_discount_note" value="<?php echo e(old('total_discount_note')); ?>" placeholder="Discount Note">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Coupon</label>
                                            <div class="col-md-7">
                                                <input id="coupon_code" type="number" class="form-control width-xs couponCode" name="coupon_code" onchange="couponMembership()" placeholder="Coupon/Membership">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Send SMS</label>
                                            <div class="col-md-12">
                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Non Masking
                                                    </label>
                                                </div>

                                                <div style="padding-left: 10px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Masking
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 130px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">BILL Amount</label>
                                            <div class="col-md-7">
                                                <input type="text" id="totalBdt" class="form-control">
                                                <input style="display: none" type="text" id="totalBdtShow" name="total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Net Payable</label>
                                            <div class="col-md-7">
                                                <input type="text" id="netPa" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 130px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Acc. Head</label>
                                            <div class="col-md-7">
                                                <select style="width: 100%;cursor: pointer;" id="account_id" name="account_id" class="form-control" required>
                                                    <option value="">--Select Account Head--</option>
                                                    <?php if($accounts->count() > 0): ?>
                                                    <?php $__currentLoopData = $accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($account->id); ?>" <?php echo e($account->id == 6 ? 'selected' : ''); ?>><?php echo e($account->account_name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Cash Given</label>
                                            <div class="col-md-7">
                                                <input id="cash_given" type="text" class="form-control" name="cash_given" value="0" oninput="calculateChangeAmount()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Adjustment</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="adjustment" name="adjustment" placeholder="Adjustment" value="0" oninput="calculateChangeAmount()">
                                            </div>
                                        </div>

                                        <input type="hidden" name="supplier_advance_amount" id="supplier_advance_amount" value="0">

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <div class="col-md-12">
                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="adjustAdvancePayment" name="adjust_cash_given">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="adjustAdvancePayment" onclick="adjustAdvancePayment()">
                                                        Adjust Advance Payment
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Advance</label>
                                            <div class="col-md-7">
                                                <input type="text" value="0" class="form-control" id="change_amount" name="change_amount" placeholder="Advance Amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Due</label>
                                            <div class="col-md-7">
                                                <input type="text" value="0" class="form-control" id="due_amount" name="due_amount" placeholder="Due Amount" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 150px;padding-top: 13px;padding-right: 0px;padding-left: 0px" class="col-md-12">
                                        <div style="background-color: #D2D2D2;height: 130px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap_current_balance getMultipleRowCurrentBalance">
                                            <div class="row row di_current_balance_0">
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>

                                    <div style="display: none;" class="row justify-content-end">
                                        <div class="col-lg-1 col-md-2 form-group">
                                            <input id="pos_add_button" type="button" class="btn btn-success btn-block inner add_field_button_pos" value="Add"/>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button_current_balance" type="button" class="btn btn-success btn-block inner add_field_button_current_balance" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px !important;padding-bottom: 0px !important" class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Save & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('bills_index')); ?>">Close</a></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Supplier</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control commonSupplierClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control commonSupplierClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control commonSupplierClass">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">

                    <div class="row">
                        <input type="hidden" name="product_id" id="product_id" />
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Category *</label>
                            <select style="width: 100%" id="product_category_id" name="product_category_id" class="form-control select2 col-lg-12 col-md-12 col-sm-12 col-12" required>
                               <option value="">--Select Product Category--</option>
                           </select>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Sub Category *</label>
                            <select style="width: 100%" id="product_sub_category_id" name="product_sub_category_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10">
                               <option value="">--Select Sub Category--</option>
                            </select>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label for="productname">Product Code</label>
                            <input type="text" name="code" class="inner form-control" id="product_code" value="<?php echo e($product_id != null ? str_pad($product_id[0]['product_code'] + 1, 6, "0", STR_PAD_LEFT) : str_pad(1, 6, "0", STR_PAD_LEFT)); ?>" readonly />
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label for="productname">Product Name *</label>
                            <input type="text" name="product_name" class="inner form-control" id="product_name" placeholder="Product Name" required />
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label for="productname">Unit</label>
                            <select id="unit_id" style="width: 100%" class="form-control select2 unit" name="unit_id">
                                <option value="">--Select Unit--</option>
                                <?php if(!empty($units)): ?>
                                    <?php $__currentLoopData = $units; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $unit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($unit->id); ?>"><?php echo e($unit->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </select>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label for="productname">Product Type</label>
                            <select style="cursor: pointer" id="product_type" class="form-control" name="product_type">
                                <option value="1">Single</option>
                                <!-- <option value="2">Variable</option> -->
                            </select>
                        </div>

                        <div style="display: none" class="col-md-12 variationClass">
                            <hr style="margin-top: 0px !important">

                            <div data-repeater-list="inner-group" class="inner form-group mb-0 row">
                                <div class="inner col-lg-12 ml-md-auto input_fields_wrap_variation getMultipleRowVariation">

                                    <h5>Add Variations</h5>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 col-sm-6"></div>
                                <div class="col-lg-1 col-md-2 col-sm-6 col-12 form-group">
                                    <div id="add_field_button_variation" class="add_field_button_variation">
                                        <i class="btn btn-success btn-block bx bx-plus font-size-20"></i>
                                    </div>
                                </div>
                            </div>

                            <hr style="margin-top: 0px !important">
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label for="productname">Purchase Price *</label>
                            <input type="text" name="buying_price" class="inner form-control" id="buying_price" placeholder="Purchase Price" required />
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label for="productname">Sell Price *</label>
                            <input type="text" name="selling_price" class="inner form-control" id="selling_price" placeholder="Sell Price"required />
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label style="padding-left: 0px" for="productname" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Alert Quantity</label>
                            <input type="text" name="alert_quantity" class="inner form-control" id="alert_quantity" placeholder="Alert Quantity" />
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Status</label>
                            <select id="status" style="width: 100%" class="form-control select2" name="status">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                        <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript" src="<?php echo e(url('public/common_javascripts/common.js')); ?>"></script>

    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/bills/customer/list/bills',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 1 && result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });

            $('#add_field_button').click();
            $('#add_field_button_current_balance').click();

            var type = $("#bbBalance").val();

            if (type == 1)
            {
                $("#bbShow").html('Payable');
                $("#bbBalance").val(type);
            }
            else
            {
                $("#bbShow").html('Advance');
                $("#bbBalance").val(type);
            }

            var customerId      = $("#customer_id").val();

            $.get(site_url + '/bills/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data.balance);

                if (data.type == 1)
                {
                    $("#bbShow").html('Payable');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', false);
                }
                else
                {
                    $("#bbShow").html('Advance');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', true);
                }     
            });

            $("#group_id_0").select2({
                ajax: { 
                url:  site_url + '/groups/group-list-ajax',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $("#brand_id_0").select2({
                ajax: { 
                    url:  site_url + '/groups/brand-list-ajax',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#product_category_id_0").select2({
                ajax: { 
                    url:  site_url + '/groups/category-list-ajax',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = 1;
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '<?php echo e(csrf_token()); ?>' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                            $('.commonSupplierClass').val('');
                        }
                        
                        $("#customer_id").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        }); 

        $('#submitBtn1').click(function() {
            
            var product_category_id             = $("#product_category_id").val();
            var product_sub_category_id         = $("#product_sub_category_id").val();
            var status                          = $("#status").val();
            var product_name                    = $("#product_name").val();
            var product_code                    = $("#product_code").val();
            var unit_id                         = $("#unit_id").val();
            var product_type                    = $("#product_type").val();
            var buying_price                    = $("#buying_price").val();
            var selling_price                   = $("#selling_price").val();
            var alert_quantity                  = $("#alert_quantity").val();
            var site_url                        = $('.site_url').val();

            if (product_name == '' || product_category_id == '' || buying_price == '' || selling_price == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }

            $.ajax({
    
                type:   'get',
                url:    site_url + '/bills/from-bill/store/product/',
                data:   { product_category_id : product_category_id, product_sub_category_id : product_sub_category_id, status : status, product_name : product_name, product_code : product_code, unit_id : unit_id, product_type : product_type, buying_price : buying_price, selling_price : selling_price, alert_quantity : alert_quantity,_token: '<?php echo e(csrf_token()); ?>' },
    
                success: function (data) {
                    console.log(data);
                    if(data != 0)
                    {
                        $('#CloseButton1').click();
                        $('#add_field_button').click();
                    }

                    $("#product_id").val(data.id);
                }
    
            });
        });    
    </script>

    <script type="text/javascript">
        function PosScannerSearch()
        {
            $("#alertMessage").hide();

            var site_url      = $(".site_url").val();
            var read_code     = $("#product_code").val();

            if (read_code.length == 6)
            {
                var product_code  = read_code.replace(/^0+/, '');
            }
            else
            {
                var product_code  = '0';
            }

            if(product_code != 0)
            {
                $.get(site_url + '/bills/bill-product-entries-list/'+ product_code, function(data_pos){

                    if ($.isEmptyObject(data_pos))
                    {   
                        $('input[name=product_code').val('');
                        $("#alertMessage").show();
                    }
                    else
                    {
                        var state = 0;

                        $('.productEntries').each(function()
                        {
                            var entry_id    = $(this).val();
                            var value_x     = $(this).prop("id");

                            if (data_pos.id == entry_id)
                            {
                                var explode          = value_x.split('_');
                                var quantity_id      = explode[2];
                                var quantity         = $('#quantity_'+quantity_id).val();
                                var inc_quantity     = parseFloat(quantity) + 1;

                                $('#quantity_'+quantity_id).val(inc_quantity);
                                $('#quantity_'+quantity_id).change();
                                $('input[name=product_code').val('');
                                calculateActualAmount(quantity_id);

                                state++;
                            }

                            if (entry_id == '')
                            {   
                                var explode          = value_x.split('_');
                                var quantity_id      = explode[2];
                                
                                ProductEntriesList(quantity_id);

                                state++;
                            }
                        });

                        if (state == 0)
                        {   
                            $("#pos_add_button").click();
                        }
                        else
                        {
                            return false;
                        }
                    }

                });
            } 
        }

        function ProductEntriesList(x,entry_id) 
        {
            //For getting item commission information from items table start
            var site_url  = $(".site_url").val();
            
            $.get(site_url + '/bills/product/entries/category-list/invoice/'+ entry_id, function(data){

                var list5           = '';
                var list7           = '';
                var free_item_list  = '';
                var free_list       = '';
                var checkProduct    = [];

                $.each(data, function(i, data)
                {
                    if (data.stock_in_hand != null)
                    {
                        var stock = data.stock_in_hand;
                    }
                    else
                    {
                        var stock = 0;
                    }

                    if (data.product_type == 2)
                    {
                        var productName = data.name + ' - ' + data.variations;
                    }
                    else
                    {
                        var productName = data.name;
                    }

                    // $('.productEntries').each(function()
                    // {
                    //     var entry_id    = $(this).val();

                    //     if(checkValue(entry_id, checkProduct) == 'Not exist')
                    //     {
                    //         checkProduct.push(entry_id);
                    //     }       
                    // });

                    // if(checkValue(data.id, checkProduct) == 'Not exist')
                    // {

                        list5 += '<option value = "' +  data.id + '">' + productName + '</option>';
                    // }

                    free_item_list += '<option value = "' +  data.id + '">' + productName + '</option>';
                });

                list7           += '<option value = "">' + '--Select Product--' +'</option>';
                free_list       += '<option value = "">' + '--Select Product--' +'</option>';

                $("#product_entries_"+x).empty();
                $("#product_entries_"+x).append(list7);
                $("#product_entries_"+x).append(list5);

                $("#free_items_"+x).empty();
                $("#free_items_"+x).append(free_list);
                $("#free_items_"+x).append(free_item_list);

                var read_code     = $("#product_code").val();

                if (read_code)
                {
                    var product_code  = read_code.replace(/^0+/, '');
                }
                else
                {
                    var product_code  = '';
                }

                if(product_code)
                {
                    $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code, function(data_pos){

                        $('#product_entries_'+x).val(data_pos.id).change();
                    });

                    $('input[name=product_code').val('');
                } 
            });
        }

        function freeProductCheck(x)
        {
            var total       = '';
            var stock_qty           = $('.stockQuantity').map((_,el) => el.value).get();
            var main_products       = $('.productEntries').map((_,el) => el.value).get();
            var main_products_qty   = $('.mainProductQuantity').map((_,el) => el.value).get();
            var free_products       = $('.freeItems').map((_,el) => el.value).get();
            var free_products_qty   = $('.freeProductQuantity').map((_,el) => el.value).get();

            var free_product_id     = $('#free_items_'+x).val();
            var sr_id               = $('#sr_id').val();

            var total_main_quantity     = 0;
            var total_free_quantity     = 0;

            for (var i = 0; i < main_products.length; i++)
            {
                if (free_product_id == main_products[i])
                {  
                    total_main_quantity += parseFloat(main_products_qty[i]);

                    var stockQty    = stock_qty[i];
                }

                if (free_product_id == free_products[i])
                {  
                    total_free_quantity += parseFloat(free_products_qty[i]);
                }
            }

            var total       = parseFloat(total_main_quantity) + parseFloat(total_free_quantity);

            var site_url    = $('.site_url').val();




            // if (free_product_id != undefined)
            if (free_product_id != '')
            {
                $.get(site_url+'/invoices/return-sr-product-stock/' + sr_id + '/' + free_product_id, function(data){

                    var stockQtys   = parseFloat(data.receive_quantity) - parseFloat(data.sold_quantity) - parseFloat(data.return_quantity) - parseFloat(total);

                    if (parseFloat(stockQtys) < 0)
                    {   
                        $("#free_quantity_"+x).val(0);
                        $('#free_item_stock_'+x).val('');
                        $('#free_item_stock_show_'+x).html('Stock : ' + '');
                        $('#free_item_stock_'+x).val(parseFloat(data.receive_quantity) - parseFloat(data.sold_quantity));
                        $('#free_item_stock_show_'+x).html('Stock : ' + (parseFloat(data.receive_quantity) - parseFloat(data.sold_quantity)));
                    }
                    else
                    {
                        if (parseFloat(stockQtys) != NaN)
                        {
                            $('#free_item_stock_show_'+x).html(0);
                            $('#free_item_stock_show_'+x).html('Stock : ' + stockQtys);
                            $('#free_item_stock_'+x).val(0);
                            $('#free_item_stock_'+x).val(stockQtys);
                        }
                        else
                        {
                            $('#free_item_stock_show_'+x).html(0);
                            $('#free_item_stock_show_'+x).html('Stock : 0');
                            $('#free_item_stock_'+x).val(0);
                            $('#free_item_stock_'+x).val(0);
                        }
                    }
                    
                });
            }
        }

        function checkValue(value,arr)
        {
            var status = 'Not exist';
             
            for(var i=0; i<arr.length; i++)
            {
                var name = arr[i];
                if(name == value)
                {
                    status = 'Exist';
                    break;
                }
            }

            return status;
        }

        function getItemPrice(x)
        {
            //For getting item commission information from items table start
            var entry_id  = $("#product_entries_"+x).val();
            var site_url  = $(".site_url").val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    var list    = '';
                    $.each(data.unit_conversions, function(i, data_list)
                    {   
                        list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                    });

                    $("#unit_id_"+x).empty();
                    $("#unit_id_"+x).append(list);

                    if (data.product_entries.stock_in_hand == '' || data.product_entries.stock_in_hand == null)
                    {
                        var stockInHand  = 0;
                    }
                    else
                    {
                        var stockInHand  = data.product_entries.stock_in_hand;
                    }

                    if (data.product_entries.unit_id == null)
                    {
                        var unit  = '';
                    }
                    else
                    {
                        var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                    }

                    $("#rate_"+x).val(parseFloat(data.product_entries.buy_price).toFixed(2));
                    $("#quantity_"+x).val(1);
                    $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
                    $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' ' + unit);
                    $("#main_unit_id_"+x).val(data.product_entries.unit_id);
                    $("#main_unit_name_"+x).val(data.product_entries.unit_name);
                    $("#discount_"+x).val(0);
                    // $("#unit_id_"+x).val(data.product_entries.unit_id).trigger('change');
      
                    calculateActualAmount(x);
                });
            }
            // calculateActualAmount(x);
            //For getting item commission information from items table end
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                    $("#stock_show_"+x).html('Stock : ' + convertedStock + ' ( ' + convertedUnitName + ' )');
                    $("#rate_"+x).val(parseFloat(data.buy_price).toFixed(2));

                    calculateActualAmount(x);
                }

            });
        }

        function getFreeConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#free_items_"+x).val();
            var unit_id             = $("#free_unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    var site_url  = $(".site_url").val();
                    var entry_id  = $("#free_items_"+x).val();
                    var sr_id     = $("#sr_id").val();

                    if(entry_id)
                    {
                        $.get(site_url + '/invoices/products-price-list/'+ entry_id + '/' + sr_id, function(data){

                            if (jQuery.isEmptyObject(data) == false)
                            {   
                                if (data.product_entries != null)
                                {
                                    if (data.product_entries.stock_in_hand == 0 || data.product_entries.stock_in_hand == '')
                                    {
                                        var stockInHand  = 0;
                                    }
                                    else
                                    {
                                        var stockInHand  = data.product_entries.stock_in_hand;
                                    }

                                    if (data.product_entries.unit_id == null)
                                    {
                                        var unit  = '';
                                    }
                                    else
                                    {
                                        var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                                    }
                                }
                                else
                                {
                                     var stockInHand    = 0;
                                     var unit           = '';
                                }
                                
                                $("#free_item_stock_"+x).val(stockInHand);
                                $("#free_item_stock_show_"+x).html('Stock : ' + stockInHand + unit);
                  
                                // calculateActualAmount(x);
                            }
                            else
                            {
                                $("#free_item_stock_"+x).val(0);
                                $("#free_item_stock_show_"+x).html('Stock : 0');
                  
                                // calculateActualAmount(x);
                            }
                            
                        });
                    }
                    // calculateActualAmount(x);
                }
                else
                {   
                    var convertedUnitName = $('#free_unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#free_item_stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                    $("#free_item_stock_"+x).val(convertedStock);
                    $("#free_item_stock_show_"+x).html('Stock : ' + convertedStock + ' ( ' + convertedUnitName + ' )');
                    // calculateActualAmount(x);
                }

            });
        }

        function getFreeItemUnit(x)
        {
            //For getting item commission information from items table start
            var entry_id  = $("#free_items_"+x).val();
            var site_url  = $(".site_url").val();
            var sr_id     = $("#sr_id").val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products-price-list/'+ entry_id + '/' + sr_id, function(data){

                    var list    = '';
                    $.each(data.unit_conversions, function(i, data_list)
                    {   
                        list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                    });

                    if (data.product_entries.stock_in_hand == 0)
                    {
                        var stockInHand  = 0;
                    }
                    else
                    {
                        var stockInHand  = data.product_entries.stock_in_hand;
                    }

                    if (data.product_entries.unit_id == null)
                    {
                        var unit  = '';
                    }
                    else
                    {
                        var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                    }

                    $("#free_item_stock_"+x).val(stockInHand);
                    $("#free_item_stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' ' + unit);
                    $("#free_unit_id_"+x).empty();
                    $("#free_unit_id_"+x).append(list);
                    $("#free_item_main_unit_id_"+x).val(data.product_entries.unit_id);
                });
            }
            //For getting item commission information from items table end
        }

        function pad (str, max)
        {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        function getCategoryList(x)
        {
            var site_url  = $('.site_url').val();
            var group_id  = $('#group_id_'+x).val();

            $("#product_category_id_"+x).select2({
                ajax: { 
                    url:  site_url + '/groups/category-list-ajax/' + group_id,
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });
        }
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }

        function addButtonCurrentBalance()
        {
            $('.add_field_button_current_balance').click();
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 500;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = -1;
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var group_label         = '<label class="hidden-xs" for="productname">Group *</label>\n';
                    var brand_label         = '<label class="hidden-xs" for="productname">Brand *</label>\n';
                    var category_label      = '<label class="hidden-xs" for="productname">Category *</label>\n';
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var free_product_label  = '<label class="hidden-xs" for="productname">Free Item</label>\n';
                    var unit_label          = '<label class="hidden-xs" for="productname">Unit</label>\n';
                    var stock_label         = '<label class="hidden-xs" for="productname">Stock</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                    var free_item_label     = '<label class="hidden-xs" for="productname">Free Qty</label>\n';
                    var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                    var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">Discount</label>\n';
                    var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 19px" for="productname"></label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }
                else
                {
                    var group_label         = '';
                    var brand_label         = '';
                    var category_label      = '';
                    var product_label       = '';
                    var free_product_label  = '';
                    var unit_label          = '';
                    var stock_label         = '';
                    var rate_label          = '';
                    var quantity_label      = '';
                    var free_item_label     = '';
                    var discount_label      = '';
                    var type_label          = '';
                    var amount_label        = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Group *</label>\n' +
                                                        group_label +
                                                        '<select style="width: 100%" class="inner form-control single_select2 groupId" id="group_id_'+x+'" required>\n' +
                                                            '<option value="">' + '--Select Group--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Brand *</label>\n' +
                                                        brand_label +
                                                        '<select style="width: 100%" class="inner form-control single_select2" id="brand_id_'+x+'" onchange="getCategoryList('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Brand--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Category *</label>\n' +
                                                        category_label +
                                                        '<select style="width: 100%" class="inner form-control single_select2" id="product_category_id_'+x+'" onchange="getProductList('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Category--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +
                                                    
                                                    '<div style="margin-bottom: 5px" class="col-lg-4 col-md-4 col-sm-6 col-12">\n' +
                                                        '<label style="display: none;width: 100%" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                        '<option value="">' + '--Select Product--' + '</option>' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +

                                                    '<div style="padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                        unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control" id="unit_id_'+x+'" required onchange="getConversionParam('+x+')">\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 10px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control mainProductQuantity" id="quantity_'+x+'" placeholder="Quantity" onchange="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-12 col-12">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 19px" for="productname"></label>\n' +
                                                                type_label +
                                                                '<select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_'+x+'" oninput="calculateActualAmount('+x+')">\n' +
                                                                    '<option value="1">BDT</option>' +
                                                                    '<option value="0" selected>%</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" name="discount[]" class="inner form-control" id="discount_'+x+'" placeholder="Discount" oninput="calculateActualAmount('+x+')"/>\n' +
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                        amount_label +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display:none!important;" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Free Item</label>\n' +
                                                        free_product_label +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 1px" class="col-md-7">' +
                                                                '<select style="width: 100%" name="free_items[]" class="inner form-control single_select2 freeItems" id="free_items_'+x+'" onchange="getFreeItemUnit('+x+')">\n' +
                                                                '<option value="">' + '--Select Product--' + '</option>' +
                                                                '</select>\n' +
                                                                '<p id="free_item_stock_show_'+x+'" style="padding: 0px;margin: 0px">' + 'Stock : 0' + '</p>' +
                                                            '</div>' +

                                                            '<div style="padding-left: 1px" class="col-md-5">' +
                                                                '<select style="width: 100%;cursor: pointer" name="free_unit_id[]" class="inner form-control single_select2" id="free_unit_id_'+x+'" onchange="getFreeConversionParam('+x+')">\n' +
                                                                '</select>\n' +
                                                            '</div>' + 
                                                        '</div>' +
                                                    '</div>\n' + 

                                                    '<input type="hidden" id="free_item_stock_'+x+'">' +
                                                    '<input type="hidden" name="free_item_main_unit_id[]" class="inner form-control" id="free_item_main_unit_id_'+x+'" />\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display:none!important;" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Free Qty *</label>\n' +
                                                        free_item_label  +
                                                        '<input type="text" name="free_quantity[]" class="inner form-control freeProductQuantity" id="free_quantity_'+x+'" value="0" />\n' +
                                                    '</div>\n' +   
                                                    
                                                    add_btn +

                                                '</div>\n' +

                                                '<hr style="margin-top: 5px !important;margin-bottom: 5px !important">'
                                            );

                                            $('.single_select2').select2();

                var site_url    = $(".site_url").val();
                $("#group_id_"+x).select2({
                    ajax: { 
                    url:  site_url + '/groups/group-list-ajax',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                    },
                });

                $("#brand_id_"+x).select2({
                    ajax: { 
                        url:  site_url + '/groups/brand-list-ajax',
                        type: "get",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                searchTerm: params.term // search term
                            };
                        },
                        processResults: function (response) {
                            return {
                                results: response
                            };
                        },
                        cache: true
                    }
                });
            }                                    
        });
        //For apending another rows end

        //For apending another rows start
        $(add_button_pos).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var group_label         = '<label class="hidden-xs" for="productname">Group *</label>\n';
                    var brand_label         = '<label class="hidden-xs" for="productname">Brand *</label>\n';
                    var category_label      = '<label class="hidden-xs" for="productname">Category *</label>\n';
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var free_product_label  = '<label class="hidden-xs" for="productname">Free Item</label>\n';
                    var unit_label          = '<label class="hidden-xs" for="productname">Unit</label>\n';
                    var stock_label         = '<label class="hidden-xs" for="productname">Stock</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                    var free_item_label     = '<label class="hidden-xs" for="productname">Free Qty</label>\n';
                    var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                    var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">Discount</label>\n';
                    var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 19px" for="productname"></label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }
                else
                {
                    var group_label         = '';
                    var brand_label         = '';
                    var category_label      = '';
                    var product_label       = '';
                    var free_product_label  = '';
                    var unit_label          = '';
                    var stock_label         = '';
                    var rate_label          = '';
                    var quantity_label      = '';
                    var free_item_label     = '';
                    var discount_label      = '';
                    var type_label          = '';
                    var amount_label        = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Group *</label>\n' +
                                                        group_label +
                                                        '<select style="width: 100%" class="inner form-control single_select2 groupId" id="group_id_'+x+'" required>\n' +
                                                            '<option value="">' + '--Select Group--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Brand *</label>\n' +
                                                        brand_label +
                                                        '<select style="width: 100%" class="inner form-control single_select2" id="brand_id_'+x+'" onchange="getCategoryList('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Brand--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Category *</label>\n' +
                                                        category_label +
                                                        '<select style="width: 100%" class="inner form-control single_select2" id="product_category_id_'+x+'" onchange="getProductList('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Category--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +
                                                    
                                                    '<div style="margin-bottom: 5px" class="col-lg-4 col-md-4 col-sm-6 col-12">\n' +
                                                        '<label style="display: none;width: 100%" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                        '<option value="">' + '--Select Product--' + '</option>' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +

                                                    '<div style="padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                        unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control" id="unit_id_'+x+'" required onchange="getConversionParam('+x+')">\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 10px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control mainProductQuantity" id="quantity_'+x+'" placeholder="Quantity" onchange="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-12 col-12">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 19px" for="productname"></label>\n' +
                                                                type_label +
                                                                '<select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_'+x+'" oninput="calculateActualAmount('+x+')">\n' +
                                                                    '<option value="1">BDT</option>' +
                                                                    '<option value="0" selected>%</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" name="discount[]" class="inner form-control" id="discount_'+x+'" placeholder="Discount" oninput="calculateActualAmount('+x+')"/>\n' +
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                        amount_label +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display:none!important;" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Free Item</label>\n' +
                                                        free_product_label +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 1px" class="col-md-7">' +
                                                                '<select style="width: 100%" name="free_items[]" class="inner form-control single_select2 freeItems" id="free_items_'+x+'" onchange="getFreeItemUnit('+x+')">\n' +
                                                                '<option value="">' + '--Select Product--' + '</option>' +
                                                                '</select>\n' +
                                                                '<p id="free_item_stock_show_'+x+'" style="padding: 0px;margin: 0px">' + 'Stock : 0' + '</p>' +
                                                            '</div>' +

                                                            '<div style="padding-left: 1px" class="col-md-5">' +
                                                                '<select style="width: 100%;cursor: pointer" name="free_unit_id[]" class="inner form-control single_select2" id="free_unit_id_'+x+'" onchange="getFreeConversionParam('+x+')">\n' +
                                                                '</select>\n' +
                                                            '</div>' + 
                                                        '</div>' +
                                                    '</div>\n' + 

                                                    '<input type="hidden" id="free_item_stock_'+x+'">' +
                                                    '<input type="hidden" name="free_item_main_unit_id[]" class="inner form-control" id="free_item_main_unit_id_'+x+'" />\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display:none!important;" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Free Qty *</label>\n' +
                                                        free_item_label  +
                                                        '<input type="text" name="free_quantity[]" class="inner form-control freeProductQuantity" id="free_quantity_'+x+'" value="0" />\n' +
                                                    '</div>\n' +   
                                                    
                                                    add_btn +

                                                '</div>\n' +

                                                '<hr style="margin-top: 5px !important;margin-bottom: 5px !important">'
                                            );

                                            $('.single_select2').select2();

                var site_url    = $(".site_url").val();
                $("#group_id_"+x).select2({
                    ajax: { 
                    url:  site_url + '/groups/group-list-ajax',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                    },
                });

                $("#brand_id_"+x).select2({
                    ajax: { 
                        url:  site_url + '/groups/brand-list-ajax',
                        type: "get",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                searchTerm: params.term // search term
                            };
                        },
                        processResults: function (response) {
                            return {
                                results: response
                            };
                        },
                        cache: true
                    }
                });
            }                                    
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            var rate                    = $("#rate_"+x).val();
            var quantity                = $("#quantity_"+x).val();
            var stock                   = $("#stock_"+x).val();
            var discount                = $("#discount_"+x).val();
            var discountType            = $("#discount_type_"+x).val();
            var vatType                 = $("#vat_type_0").val();
            var vatAmount               = $("#vat_amount_0").val();
            var taxType                 = $("#tax_type_0").val();
            var taxAmount               = $("#tax_amount_0").val();
            var totalDiscount           = $("#total_discount_0").val();
            var totalDiscountType       = $("#total_discount_type_0").val();

            if (rate == '')
            {
                var rateCal             = 0;
            }
            else
            {
                var rateCal             = $("#rate_"+x).val();
            }

            if (quantity == '')
            {
                var quantityCal         = 1;
            }
            else
            {
                var quantityCal         = $("#quantity_"+x).val();
            }

            if (discount == '')
            {
                var discountCal         = 1;
            }
            else
            {
                var discountCal         = $("#discount_"+x).val();
            }

            if (discount == '')
            {
                var discountTypeCal     = 0;
            }
            else
            {
                if (discountType == 0)
                {
                    var discountTypeCal     = (parseFloat(discountCal)*parseFloat(rateCal)*parseFloat(quantityCal))/100;
                }
                else
                {
                    var discountTypeCal     = $("#discount_"+x).val();
                }
            }

            var AmountIn              =  (parseFloat(rateCal)*parseFloat(quantityCal)) - parseFloat(discountTypeCal);
     
            $("#amount_"+x).val(AmountIn.toFixed(2));

            //Calculating Subtotal Amount
            var total       = 0;

            $('.amount').each(function()
            {
                total       += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(total.toFixed());
            $("#subTotalBdtShow").val(total.toFixed());

            if (vatAmount == '')
            {   
                $("#vat_amount_0").val(0);
                var vatCal         = 1;
            }
            else
            {
                var vatCal         = $("#vat_amount_0").val();
            }

            if (vatType == 0)
            {
                var vatTypeCal     = (parseFloat(vatCal)*(parseFloat(total)))/100;
            }
            else
            {
                var vatTypeCal     = $("#vat_amount_0").val();
            }

            if (taxAmount == '')
            {   
                $("#tax_amount_0").val(0);
                var taxCal         = 1;
            }
            else
            {
                var taxCal         = $("#tax_amount_0").val();
            }

            if (taxType == 0)
            {
                var taxTypeCal     = (parseFloat(taxCal)*(parseFloat(total)))/100;
            }
            else
            {
                var taxTypeCal     = $("#tax_amount_0").val();
            }

            if (totalDiscount > 0)
            {   
                if (totalDiscountType == 0)
                {
                    var totalDiscountTypeCal     = (parseFloat(totalDiscount)*(parseFloat(total) + parseFloat(vatTypeCal)))/100;
                }
                else
                {
                    var totalDiscountTypeCal     = $("#total_discount_0").val();
                }
            }
            else
            {
                var totalDiscountTypeCal     = 0;
            }

            var totalShow = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountTypeCal);

            $("#totalBdtShow").val(totalShow.toFixed());
            $("#totalBdt").val(parseFloat(totalShow).toFixed());

            var preBal      = $("#balance").val();
            var preBalType  = $("#bbBalance").val();

            if (preBalType == 1)
            {
                var netPaShowVal   = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountTypeCal) + parseFloat(preBal);
            }

            if (preBalType == 2)
            {
                var netPaShowVal   = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountTypeCal) - parseFloat(preBal);
            }

            $("#netPa").val(parseFloat(netPaShowVal).toFixed());

            calculateChangeAmount();

            var type = $("#bbBalance").val();

            if (type == 2)
            {
                var site_url        = $('.site_url').val();
                var customerId      = $("#customer_id").val();

                $.get(site_url + '/bills/adjust-advance-payment/' + customerId, function(data){

                    var payable  = $("#totalBdtShow").val();

                    if (parseFloat(payable) <= parseFloat(data))
                    {
                        $("#adjustment").val(parseFloat(payable).toFixed());
                    }
                    else
                    {
                        $("#adjustment").val(parseFloat(data).toFixed());
                    }

                    $("#customer_advance_amount").val(parseFloat(data).toFixed());
                    calculateChangeAmount();
                });
            }
        }
    </script>

    <script type="text/javascript">
        var max_fields_current_balance = 50;                           //maximum input boxes allowed
        var wrapper_current_balance    = $(".input_fields_wrap_current_balance");      //Fields wrapper
        var add_button_current_balance = $(".add_field_button_current_balance");       //Add button ID
        var index_no_current_balance   = 1;

        //For apending another rows start
        var c = -1;
        $(add_button_current_balance).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(c < max_fields_current_balance)
            {   
                c++;

                var serial = c + 1;

                if (serial == 1)
                {
                    var amount_paid_label   = '<label class="hidden-xs" for="productname"> Paid *</label>\n';
                    var paid_through_label  = '<label class="hidden-xs" for="productname">Paid Through *</label>\n';
                    var account_info_label  = '<label class="hidden-xs" for="productname">Acc/Info</label>\n';
                    var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button_current_balance">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonCurrentBalance()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var amount_paid_label   = '';
                    var paid_through_label  = '';
                    var account_info_label  = '';
                    var note_label          = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field_current_balance" data-val="'+c+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRowCurrentBalance').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_current_balance_'+c+'">' +
                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid *</label>\n' +
                                                        amount_paid_label +
                                                        '<input type="text" name="current_balance_amount_paid[]" class="form-control currentBalancePaidAmount" id="current_balance_amount_paid_'+c+'" value="0" oninput="checkCurrentBalance('+c+')" required />\n' + 
                                                    '</div>\n' +

                                                    '<div style="padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid Through *</label>\n' +
                                                        paid_through_label  +
                                                        '<select id="current_balance_paid_through_'+c+'" style="cursor: pointer;width: 100%" name="current_balance_paid_through[]" class="form-control single_select2">\n' +
                                                            '<?php if(!empty($paid_accounts) && ($paid_accounts->count() > 0)): ?>\n' +
                                                            '<?php $__currentLoopData = $paid_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $paid_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>\n' +
                                                                '<option value="<?php echo e($paid_account['id']); ?>"><?php echo e($paid_account['account_name']); ?></option>\n' +
                                                            '<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>\n' +
                                                            '<?php endif; ?>\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Acc/Info</label>\n' +
                                                        account_info_label +
                                                        '<input type="text" name="current_balance_account_information[]" class="form-control" id="current_balance_account_information_'+c+'" placeholder="Account Information"/>\n' + 
                                                    '</div>\n' + 

                                                    '<div style="padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                        note_label +
                                                        '<input type="text" name="current_balance_note[]" class="form-control" id="current_balance_note_'+c+'" placeholder="note"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +

                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
                }                                    
        });
        //For apending another rows end

        $(wrapper_current_balance).on("click",".remove_field_current_balance", function(e)
        {
            e.preventDefault();

            var c = $(this).attr("data-val");

            $('.di_current_balance_'+c).remove(); c--;

            calculateActualAmount(c);
        });
    </script>

    <script type="text/javascript">
        function sellingPriceTaxType()
        {
            var tax_type  = $("#tax_type").val();

            if (tax_type == 2)
            {
                $("#serviceCharge").hide();
                $("#vatPercentage").hide();
                $("#service_charge").val('');
                $("#vat_percentage").val('');

                CalculateTotalSellPrice();
            }
            else
            {
                $("#serviceCharge").show();
                $("#vatPercentage").show();
                $("#service_charge").val('');
                $("#vat_percentage").val('');

                CalculateTotalSellPrice();
            }
        }

        $(document).on("change", "#defaultCheck2" , function() 
        {

            var checkbox_value    = $("#defaultCheck2")[0].checked;

            if (checkbox_value == true)
            {
                $("#vatShow").show();
                $("#taxShow").show();
            }
            else
            {
                $("#vatShow").hide();
                $("#taxShow").hide();
            }
        });

        function CalculateTotalSellPrice()
        {
            var sell_price_call                 = $("#selling_price").val();
            var vat_percentage_call             = $("#vat_percentage").val();
            var service_charge_call             = $("#service_charge").val();

            if (sell_price_call == '')
            {
                var sell_price                  = 0;
            }
            else
            {
                var sell_price                  = $("#selling_price").val();
            }

            if (vat_percentage_call == '')
            {
                var vat_percentage              = 0;
            }
            else
            {
                var vat_percentage              = $("#vat_percentage").val();
            }

            if (service_charge_call == '')
            {
                var service_charge              = 0;
            }
            else
            {
                var service_charge              = $("#service_charge").val();
            }

            var vat_amount                      = (parseFloat(sell_price)*parseFloat(vat_percentage))/100;
            var service_charge_amount           = (parseFloat(sell_price)*parseFloat(service_charge))/100;
            var selling_price_exclusive_tax     = parseFloat(sell_price) - parseFloat(vat_amount) - parseFloat(service_charge_amount);

            $("#selling_price_exclusive_tax").val(selling_price_exclusive_tax);
        }

        function calculateChangeAmount()
        {
            var tAmount             = $("#totalBdtShow").val();
            var balance             = $("#balance").val();
            var cGiven              = $("#cash_given").val();
            var adjustment          = $("#adjustment").val();
            var type                = $("#bbBalance").val();
            var netPay              = $("#netPa").val();

            if (tAmount != '')
            {
                var totalAmount     = $("#totalBdtShow").val();
            }
            else
            {
                var totalAmount     = 0;
            }

            if (adjustment != '')
            {
                var adjustmentAmount     = $("#adjustment").val();
            }
            else
            {
                var adjustmentAmount     = 0;
            }

            if (cGiven != '')
            {
                var cashGiven       = $("#cash_given").val();
            }
            else
            {
                var cashGiven       = 0;
            }

            if (balance != '')
            {
                var balanceAmount   = $("#balance").val();
            }
            else
            {
                var balanceAmount   = 0;
            }

            var paidAmount  = parseFloat(cashGiven) + parseFloat(adjustmentAmount);

            if (type == 1)
            {   
                var payaAmount  = parseFloat(balanceAmount) + parseFloat(totalAmount);
            }
            else
            {
                var payaAmount  = parseFloat(totalAmount);
            }

            var changeAmount            = parseFloat(paidAmount) - parseFloat(payaAmount);
            var changeAmountOriginal    = parseFloat(paidAmount) - parseFloat(totalAmount);

            if (parseFloat(paidAmount) <= parseFloat(totalAmount))
            { 
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(paidAmount));
                $("#current_balance_amount_paid_0").val(parseFloat(cashGiven).toFixed());
            }

            if ((parseFloat(paidAmount) > parseFloat(totalAmount)) && (parseFloat(paidAmount) <= parseFloat(payaAmount)))
            {   
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(totalAmount).toFixed());
                $("#current_balance_amount_paid_0").val(parseFloat(cashGiven).toFixed());
            }

            if (parseFloat(paidAmount) > parseFloat(payaAmount))
            {   
                $("#change_amount").val(parseFloat(changeAmount).toFixed());
                $("#amount_paid_0").val(parseFloat(totalAmount).toFixed());
                $("#current_balance_amount_paid_0").val(parseFloat(cashGiven).toFixed());
            }

            if(parseFloat(cashGiven) < parseFloat(netPay))
            {
                var due_amount = parseFloat(netPay) - parseFloat(cashGiven);
                $("#due_amount").val(parseFloat(due_amount).toFixed());
            }
            else
            {
                $("#due_amount").val(0);
            }
        }

        $(document).on("change", "#major_category_id" , function() {
            var site_url            = $('.site_url').val();
            var major_category_id   = $('#major_category_id').val();

            $("#product_entries_0").select2({
                ajax: { 
                url:  site_url + '/bills/product-list-load-bill/' + major_category_id,
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        }); 

        $(document).on("change", "#customer_id" , function() {

            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            $.get(site_url + '/bills/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data.balance);

                if (data.type == 1)
                {
                    $("#bbShow").html('Payable');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', false);
                }
                else
                {
                    $("#bbShow").html('Advance');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', true);
                }
                
            });
        });

        function adjustAdvancePayment()
        {
            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            if ($('#adjustAdvancePayment').is(":checked"))
            {
                $("#adjustment").val(0);
                $("#customer_advance_amount").val(0);
                calculateChangeAmount();
            }
            else
            {
                $.get(site_url + '/bills/adjust-advance-payment/' + customerId, function(data){

                    var payable  = $("#totalBdtShow").val();

                    if (parseFloat(payable) <= parseFloat(data))
                    {
                        $("#adjustment").val(parseFloat(payable).toFixed());
                    }
                    else
                    {
                        $("#adjustment").val(parseFloat(data).toFixed());
                    }

                    $("#customer_advance_amount").val(parseFloat(data).toFixed());
                    calculateChangeAmount();
                });
            }
        }

        function checkBalance(id)
        {
            var total  = 0;
            $('.paidAmount').each(function()
            {
                total  += parseFloat($(this).val());
            });

            var payable_amount  = $("#totalBdt").val();
            var amount_paid     = $("#amount_paid_"+id).val();

            if (parseFloat(total) > parseFloat(payable_amount))
            {
                $("#amount_paid_"+id).val(0);
            }
        }

        function checkCurrentBalance(id)
        {
            var total  = 0;
            $('.currentBalancePaidAmount').each(function()
            {
                total  += parseFloat($(this).val());
            });

            var cash_given      = $("#cash_given").val();
            var amount_paid     = $("#current_balance_amount_paid_"+id).val();

            if (parseFloat(total) > parseFloat(cash_given))
            {
                $("#current_balance_amount_paid_"+id).val(0);
            }
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/85/Modules/Bills/Resources/views/index.blade.php ENDPATH**/ ?>