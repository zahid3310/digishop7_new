

<?php $__env->startSection('title', 'Show'); ?>

<style type="text/css">
    @media  print {
        /*.card {
            width: 145mm;
            padding-right: 0px;
            padding-left: 15px;
            margin-top: 20px;
            color: black;
        }*/
    }
    
    .table td, .table th {
        padding: 0px !important;
    }
    
    .pb-2, .py-2 {
        padding-bottom: 0px !important;
    }
    
    .pt-2, .py-2 {
        padding-top: 0px !important;
    }
    .mt-3, .my-3 {
        margin-top: 0px !important;
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Issue To DSM</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">List of Issue</a></li>
                                    <li class="breadcrumb-item active">Print</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-6 col-6">
                                        <address style="margin-bottom: 0px;padding-bottom: 0px;margin-top: 0px;padding-top: 0px">
                                            <strong>Billed To:</strong><br>
                                            <?php echo e($value['invoice']->customer->name); ?>

                                            <?php if($value['invoice']->customer->address != null): ?>
                                               <br> <?php echo $value['invoice']->customer->address; ?> <br>
                                            <?php endif; ?>
                                            <?php if($value['invoice']->customer->address == null): ?>
                                                <br>
                                            <?php endif; ?>
                                            <?php echo e($value['invoice']->customer->phone); ?>

                                        </address>
                                    </div>

                                    <div style="font-size: 16px;text-align: right;" class="col-sm-6 col-6">
                                        <address style="margin-bottom: 0px;padding-bottom: 0px;margin-top: 0px;padding-top: 0px">
                                            <strong>Order Date:</strong>
                                            <?php echo e(date('d-m-Y', strtotime($value['invoice']['invoice_date']))); ?><br>

                                            <strong>Delivery Date:</strong>
                                            <?php echo e(date('d-m-Y', strtotime(date('Y-m-d')))); ?><br>

                                             <strong>SM</strong> : <?php echo e($value['invoice']->sr_id > 0 ? $value['invoice']->sr->name : ''); ?>

                                            <br>
                                            <strong>Area</strong> : <?php echo e($value['invoice']->customer->area_id != null ? $value['invoice']->customer->area->name : ''); ?>

                                            
                                        </address>
                                    </div>
                                </div>

                                <div style="margin-top: 0px !important" class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Order summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Order # <?php echo e('ORD - ' . str_pad($value['invoice']['invoice_number'], 6, "0", STR_PAD_LEFT)); ?></h3>
                                        </div>
                                    </div>
                                </div>

                                <table class="table">
                                    <thead style="font-size: 18px">
                                        <tr>
                                            <th style="text-align:left;width: 5%">No.</th>
                                            <th style="text-align:left;width: 40%">Item</th>
                                            <th style="text-align:left;width: 15%">Rate</th>
                                            <th style="text-align:left;width: 15%">Qty</th>
                                            <th style="text-align:left;width: 15%">Discount</th>
                                            <th style="text-align:right;width: 10%">Price</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: 16px">

                                        <?php if(!empty($value['entries']) && ($value['entries']->count() > 0)): ?>

                                        <?php $sub_total = 0; ?>

                                        <?php $__currentLoopData = $value['entries']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td class="text-left"><?php echo e($key1 + 1); ?></td>
                                            <td style="text-align:left;">
                                                <?php echo $value1->productEntries->group->name . ' - ' . $value1->productEntries->brand->name . ' - ' . $value1->productEntries->product->name . ' - ' . $value1->productEntries->name; ?>
                                            </td>
                                            <td class="text-left"><?php echo e($value1['rate']); ?></td>
                                            <td class="text-left"><?php echo e($value1['quantity'] . ' ' .$value1->convertedUnit->name); ?></td>
                                            <td style="text-align:left;">
                                                <?php $total_dis = $value1['discount_type'] == 0 ? (($value1['rate']*$value1['quantity']*$value1['discount_amount'])/100) : $value1['discount_amount']; ?>
                                                <?php if($value1['discount_type'] == 0): ?>
                                                    <?php echo number_format($total_dis,2,'.',','); ?>
                                                    <?php echo '('.$value1['discount_amount'].'%'.')'; ?>
                                                <?php else: ?>
                                                    <?php echo number_format($value1['discount_amount'],2,'.',','); ?>
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-right"><?php echo e($value1['total_amount']); ?></td>
                                        </tr>

                                        <?php $sub_total = $sub_total + $value1['total_amount']; ?>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                        
                                        <tr>
                                            <td style="font-size: 16px" colspan="5" class="text-right">Order Total</td>
                                            <td style="font-size: 16px" class="text-right"><?php echo e(number_format($value['invoice']['invoice_amount'],2,'.',',')); ?></td>
                                        </tr>
                                        
                                        <tr>
                                            <td style="font-size: 16px" colspan="5" class="text-right">Previous Due</td>
                                            <td style="font-size: 16px" class="text-right"><?php echo e(number_format($value['invoice']['previous_due'],2,'.',',')); ?></td>
                                        </tr>

                                        <tr style="display:none;">
                                            <td style="font-size: 16px" colspan="5" class="text-right">VAT (<?php echo e($value['invoice']['vat_type'] == 0 ? $value['invoice']['total_vat'].'%' : 'BDT'); ?>)</td>
                                            <td style="font-size: 16px"><?php echo e($value['invoice']['vat_type'] == 0 ? (($sub_total*$value['invoice']['total_vat'])/100) : number_format($value['invoice']['total_vat'],2,'.',',')); ?></td>
                                        </tr>

                                        <!-- <tr style="line-height: 0px">
                                            <td style="font-size: 16px" colspan="5" class="border-0 text-right">
                                                Discount (<?php echo e($value['invoice']['total_discount_type'] == 0 ? $value['invoice']['total_discount_amount'].'%' : 'BDT'); ?>)</td>
                                            <td style="font-size: 16px" class="border-0 text-right"><?php echo e($value['invoice']['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$value['invoice']['total_vat'])/100))*$value['invoice']['total_discount_amount'])/100) : number_format($value['invoice']['total_discount_amount'],2,'.',',')); ?></td>
                                        </tr> -->

                                        <tr style="line-height: 16px">
                                            <td style="font-size: 16px;font-weight: bold" colspan="5" class="border-0 text-right">Net Total</td>
                                            <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                <?php echo e(number_format($value['invoice']['invoice_amount']+ $value['invoice']['previous_due'],2,'.',',')); ?>

                                            </td>
                                        </tr>

                                        <tr style="line-height: 16px">
                                            <td style="font-size: 16px" colspan="5" class="border-0 text-right">Paid</td>
                                            <td style="font-size: 16px" class="border-0 text-right">
                                                <?php echo e(number_format($value['invoice']['cash_given'],2,'.',',')); ?>

                                            </td>
                                        </tr>
                                        
                                        <tr style="line-height: 16px">
                                            <td style="font-size: 16px" colspan="5" class="border-0 text-right">Net Dues</td>
                                            <td style="font-size: 16px" class="border-0 text-right">
                                                <?php echo e(number_format($value['invoice']['invoice_amount'] + $value['invoice']['previous_due'] - $value['invoice']['cash_given'],2,'.',',')); ?>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            
                                <?php if($value['invoice']['invoice_note'] != null): ?>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 style="font-size: 16px"><strong>Note :</strong> <?php echo e($value['invoice']['invoice_note']); ?></h6>
                                    </div>
                                </div>
                                <?php endif; ?>
                                
                                <br>

                                <div class="row">
                                    <div class="col-4">
                                    <p style="text-decoration: overline dotted;">SM Signature</p>
                                    </div>

                                    <div class="col-4">
                                    <p style="text-decoration: overline dotted;text-align: center;">DSM Signature</p>
                                    </div>

                                    <div class="col-4">
                                    <p style="text-decoration: overline dotted;text-align: right;">Customer Signature</p>
                                    </div>

                                </div>

                                <hr style="margin: 0px !important;margin-bottom: 15px !important;">
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/85/Modules/SrIssues/Resources/views/print_order_list.blade.php ENDPATH**/ ?>