<?php $__env->startSection('title', 'Show'); ?>
<style type="text/css">
    @media  print {
        /*.card {
            width: 145mm;
            padding-right: 0px;
            padding-left: 15px;
            margin-top: 20px;
            color: black;
        }*/
    }
    
    .table td, .table th {
        padding: 0px !important;
    }
    
    .pb-2, .py-2 {
        padding-bottom: 0px !important;
    }
    
    .pt-2, .py-2 {
        padding-top: 0px !important;
    }
    .mt-3, .my-3 {
        margin-top: 0px !important;
    }
</style>
<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">

                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">Order/Memo</h2>

                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight:bold;"><?php echo e(isset($user_info['organization_name']) ? $user_info['organization_name'] : $user_info['name']); ?></h2>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['address']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e(isset($user_info['contact_number']) ? $user_info['contact_number'] : $user_info['phone']); ?></p>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12"></div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-6 col-6">
                                        <address style="margin-bottom: 0px;padding-bottom: 0px;margin-top: 0px;padding-top: 0px">
                                            <strong>Billed To:</strong><br>
                                            <?php echo e($invoice['customer_name']); ?>

                                            <?php if($invoice['address'] != null): ?>
                                               <br> <?php echo $invoice['address']; ?> <br>
                                            <?php endif; ?>
                                            <?php if($invoice['address'] == null): ?>
                                                <br>
                                            <?php endif; ?>
                                            <?php echo e($invoice['phone']); ?>

                                        </address>
                                    </div>

                                    <div style="font-size: 16px;text-align: right;" class="col-sm-6 col-6">
                                        <address style="margin-bottom: 0px;padding-bottom: 0px;margin-top: 0px;padding-top: 0px">
                                            <strong>Invoice Date:</strong>
                                            <?php echo e(date('d-m-Y', strtotime($invoice['invoice_date']))); ?><br>

                                            <strong>Delivery Date:</strong>
                                            <?php echo e(date('d-m-Y', strtotime(date('Y-m-d')))); ?><br>

                                             <strong>SM</strong> : <?php echo e($customer_sr_name->sr_name); ?>

                                            <br>
                                            <strong>Area</strong> : <?php echo e($invoice->customer->area_id != null ? $invoice->customer->area->name : ''); ?>

                                            
                                        </address>
                                    </div>
                                </div>

                                <div style="margin-top: 0px !important" class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Sales summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Sales # <?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?></h3>
                                        </div>
                                    </div>
                                </div>

                                <table class="table">
                                    <thead style="font-size: 18px">
                                        <tr>
                                            <th style="text-align:left;width: 10%">No.</th>
                                            <th style="text-align:left;width: 30%">Item</th>
                                            <th style="text-align:right;width: 15%">Rate</th>
                                            <th style="text-align:right;width: 15%">Qty</th>
                                            <th style="text-align:center;width: 15%">Dis</th>
                                            <th style="text-align:right;width: 10%">Price</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: 16px">

                                        <?php if(!empty($entries) && ($entries->count() > 0)): ?>

                                        <?php $sub_total = 0; ?>

                                        <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td class="text-left"><?php echo e($key + 1); ?></td>
                                            <td style="text-align:left;">
                                                <?php echo $value->productEntries->group->name . ' - ' . $value->productEntries->brand->name . ' - ' . $value->productEntries->product->name . ' - ' . $value->productEntries->name; ?>
                                            </td>
                                            <td class="text-right"><?php echo e($value['rate']); ?></td>
                                            <td class="text-right"><?php echo e($value['quantity'] . ' ' . $value->convertedUnit->name); ?></td>
                                            <td class="text-center" style="">
                                                <?php $total_dis = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; ?>
                                                <?php if($value['discount_type'] == 0): ?>
                                                    <?php echo number_format($total_dis,2,'.',','); ?><br>
                                                    <?php echo '('.$value['discount_amount'].'%'.')'; ?>
                                                <?php else: ?>
                                                    <?php echo number_format($value['discount_amount'],2,'.',','); ?>
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-right"><?php echo e($value['total_amount']); ?></td>
                                        </tr>

                                        <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                        
                                        <tr>
                                            <td style="font-size: 16px" colspan="5" class="text-right">Invoice Total</td>
                                            <td style="font-size: 16px" class="text-right"><?php echo e(number_format($invoice['invoice_amount'],2,'.',',')); ?></td>
                                        </tr>
                                        
                                        <tr>
                                            <td style="font-size: 16px" colspan="5" class="text-right">Previous Due</td>
                                            <td style="font-size: 16px" class="text-right"><?php echo e(number_format($invoice['previous_due'],2,'.',',')); ?></td>
                                        </tr>

                                        <tr style="display:none;">
                                            <td style="font-size: 16px" colspan="5" class="text-right">VAT (<?php echo e($invoice['vat_type'] == 0 ? $invoice['total_vat'].'%' : 'BDT'); ?>)</td>
                                            <td style="font-size: 16px"><?php echo e($invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : number_format($invoice['total_vat'],2,'.',',')); ?></td>
                                        </tr>

                                        <!--<tr style="line-height: 0px">-->
                                        <!--    <td style="font-size: 16px" colspan="4" class="border-0 text-right">-->
                                        <!--        Discount (<?php echo e($invoice['total_discount_type'] == 0 ? $invoice['total_discount_amount'].'%' : 'BDT'); ?>)</td>-->
                                        <!--    <td style="font-size: 16px" class="border-0 text-right"><?php echo e($invoice['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$invoice['total_vat'])/100))*$invoice['total_discount_amount'])/100) : number_format($invoice['total_discount_amount'],2,'.',',')); ?></td>-->
                                        <!--</tr>-->

                                        <tr style="line-height: 16px">
                                            <td style="font-size: 16px;font-weight: bold" colspan="5" class="border-0 text-right">Net Total</td>
                                            <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                <?php echo e(number_format($invoice['invoice_amount'] + $invoice['previous_due'],2,'.',',')); ?>

                                            </td>
                                        </tr>

                                        <tr style="line-height: 16px">
                                            <td style="font-size: 16px" colspan="5" class="border-0 text-right">Paid</td>
                                            <td style="font-size: 16px" class="border-0 text-right">
                                                <?php echo e(number_format($invoice['cash_given'],2,'.',',')); ?>

                                            </td>
                                        </tr>
                                        
                                        <tr style="line-height: 16px;">
                                            <td style="font-size: 16px" colspan="5" class="border-0 text-right">Net Dues</td>
                                            <td style="font-size: 16px" class="border-0 text-right">
                                                <?php echo e(number_format($invoice['invoice_amount'] + $invoice['previous_due'] - $invoice['cash_given'],2,'.',',')); ?>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            
                                <div class="row">
                                    <div class="col-6">
                                    <p style="text-decoration: overline dotted;">Owner Signature</p>
                                    </div>

                                    <div class="col-6">
                                    <p style="text-decoration: overline dotted;text-align: right;">Customer Signature</p>
                                    </div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/85/Modules/Invoices/Resources/views/show.blade.php ENDPATH**/ ?>