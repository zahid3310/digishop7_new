

<?php $__env->startSection('title', 'Delivery List'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Delivery List</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">Delivery List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                            Payment Successfull !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                            Payment Not Added !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div style="min-height: 300px" class="card">
                            <?php if(Session::has('success')): ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <?php echo Session::get('success'); ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            <?php if(Session::has('unsuccess')): ?>
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                <?php echo Session::get('unsuccess'); ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            <?php if(Session::has('errors')): ?>
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            <div class="card-body table-responsive">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">From Date</label>
                                            <div class="col-md-8">
                                                <input style="cursor: pointer" id="search_from_date" type="date" class="form-control" value="<?= date("Y-m-d") ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">To Date</label>
                                            <div class="col-md-8">
                                                <input style="cursor: pointer" id="search_to_date" type="date" class="form-control" value="<?= date("Y-m-d") ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Customer </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="customer_id" class="form-control select2">
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">SM </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="sm_id" class="form-control select2">
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Order </label>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <select style="width: 73%" id="invoice_number" name="invoice_number" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                       <option value="0">All</option>
                                                    </select>
                                                    <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="searchPayment()">
                                                        <i class="bx bx-search font-size-24"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <hr style="margin-top: 5px !important;margin-bottom: 15px">

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th>Order#</th>
                                            <th>SM</th>
                                            <th>Customer</th>
                                            <th>Total Payable</th>
                                            <th>Given</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="invoice_list">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url    = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#sm_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 4 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#invoice_number").select2({
                ajax: { 
                url:  site_url + '/reports/sales-statement/invoices-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['type'] == 0 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $.get(site_url + '/orders/invoice/delivery/list/load', function(data){

                invoiceList(data);
                
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var site_url                = $('.site_url').val();
            var search_from_date        = $('#search_from_date').val();
            var search_to_date          = $('#search_to_date').val();
            var search_customer         = $('#customer_id').val();
            var search_sm               = $('#sm_id').val();
            var search_invoice_number   = $('#invoice_number').val();

            if (search_from_date != '')
            {
                var from_date = $('#search_from_date').val();
            }
            else
            {
                var from_date = 0;
            }

            if (search_to_date != '')
            {
                var to_date = $('#search_to_date').val();
            }
            else
            {
                var to_date = 0;
            }

            if (search_customer != '')
            {
                var customer = $('#customer_id').val();
            }
            else
            {
                var customer = 0;
            }

            if (search_sm != '')
            {
                var sm = $('#sm_id').val();
            }
            else
            {
                var sm = 0;
            }

            if (search_invoice_number != '')
            {
                var invoice = $('#invoice_number').val();
            }
            else
            {
                var invoice = 0;
            }

            $.get(site_url + '/orders/delivery/search/list/' + from_date + '/' + to_date + '/' + customer + '/' + sm + '/' +invoice, function(data){

                invoiceList(data);

            });
        }
    </script>

    <script type="text/javascript">
        function findInvoice(invoice_id)
        {
            var site_url        = $('.site_url').val();
            $('.remove_field_payment').click();
            $('#add_field_button_payment').click();

            $.get(site_url + '/orders/customer-make-payment/' + invoice_id, function(data){

                $("#receivable").empty();
                $("#receivable").val(data.invoice_amount);

                $("#received").empty();
                $("#received").val(parseFloat(data.invoice_amount) - parseFloat(data.due_amount));

                $("#dues").empty();
                $("#dues").val(data.due_amount);

                $("#customer_id").empty();
                $("#customer_id").val(data.customer_id);

                $("#find_invoice_id").empty();
                $("#find_invoice_id").val(invoice_id);

            });
        }

        function calculateDues()
        {
            var due_amount  = $("#dues").val();
            var amount      = $("#amount").val();

            if (parseFloat(amount) > parseFloat(due_amount))
            {
                $("#amount").val(due_amount);
            }
        }
    </script>

    <script type="text/javascript">
        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>

    <script type="text/javascript">
        function invoiceList(data)
        {   
            console.log(data);
            var invoice_list = '';
            var sl_no        = 1;
            $.each(data, function(i, invoice_data)
            {
                var serial          = parseFloat(i) + 1;
                var site_url        = $('.site_url').val();

                var edit_url        = site_url + '/orders/edit/' + invoice_data.id;
                var print_url       = site_url + '/orders/show/' + invoice_data.id;
                var print_url_pos   = site_url + '/orders/show-pos/' + invoice_data.id;
                var order_edit      = site_url + '/orders/order-edit/' + invoice_data.id;

                if (invoice_data.customer_name != null)
                {
                    var customer  = invoice_data.customer_name;
                }
                else
                {
                    var customer  = invoice_data.contact_name;
                }

                if (invoice_data.return_id != null)
                {
                    var return_sign  = '&nbsp;' + '&nbsp;' + '<i class="bx bx-repost font-size-15">' + '</i>';
                }
                else
                {
                    var return_sign  = '';
                }

                if (invoice_data.cash_given != null) 
                {
                    cash_given = invoice_data.cash_given;
                }
                else
                {
                    cash_given = 0;
                }

                if((parseFloat(invoice_data.invoice_amount) - parseFloat(cash_given)) > 0)
                {
                    var due = parseFloat(invoice_data.invoice_amount) - parseFloat(cash_given);
                }
                else
                {
                    var due = 0;
                }
                
                if((parseFloat(due) > 0) || (parseFloat(due) != null) )
                {
                    var adv = 0;
                }
                else
                {
                    var adv =  Math.abs(parseFloat(cash_given) - parseFloat(invoice_data.invoice_amount));
                }

                if (invoice_data.type == 3)
                {
                    invoice_list += '<tr>' +
                                '<td>' +
                                    sl_no +
                                '</td>' +
                                '<td>' +
                                  formatDate(invoice_data.invoice_date) +
                                '</td>' +
                                '<td>' +
                                    'ORD - ' + invoice_data.invoice_number.padStart(6, '0') +
                                '</td>' +
                                '<td>' +
                                    invoice_data.sm_name + 
                                '</td>' +
                                '<td>' +
                                    customer + 
                                '</td>' +
                                '<td>' +
                                  invoice_data.invoice_amount +
                                '</td>' +
                                '<td>' +
                                  cash_given +
                                '</td>' +
                                '<td>' +
                                    '<div class="dropdown">' +
                                        '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                            '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                        '</a>' +
                                        '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                            '<a class="dropdown-item" href="' + edit_url +'">' + 'Confirm' + '</a>' +
                                            '<a class="dropdown-item" href="' + order_edit +'">' + 'Edit' + '</a>' +
                                            '<a class="dropdown-item" href="' + print_url +'" target="_blank">' + 'Show' + '</a>' +
                                            '<a class="dropdown-item" href="' + print_url_pos +'" target="_blank">' + 'Print' + '</a>' +
                                        '</div>' +
                                    '</div>' +
                                '</td>' +
                            '</tr>';

                            sl_no++;
                }
            });

            $("#invoice_list").empty();
            $("#invoice_list").append(invoice_list);
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/85/Modules/Orders/Resources/views/all_delivery_list.blade.php ENDPATH**/ ?>