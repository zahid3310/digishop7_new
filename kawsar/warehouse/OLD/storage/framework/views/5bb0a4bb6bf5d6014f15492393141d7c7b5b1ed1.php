<!DOCTYPE html>
<html>

<head>
    <title>Bank Book</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p><?php echo e($user_info['address']); ?></p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Bank Book</h6>
                    </div>

                    <div class="ibox-content">
                        <!-- Header Start -->
                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center"><?php echo e($from_date); ?> <strong>To</strong> <?php echo e($to_date); ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Header End -->

                        <!-- Sales Statement Start -->
                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 8%">DATE</th>
                                    <th style="text-align: center;width: 10%">V.ID</th>
                                    <th style="text-align: center;width: 10%">PARTICULARS</th>
                                    <th style="text-align: center;width: 15%">DESCRIPTION</th>
                                    <th style="text-align: center;width: 8%">DEBIT</th>
                                    <th style="text-align: center;width: 8%">CREDIT</th>
                                    <th style="text-align: center;width: 10%">BALANCE</th>
                                    <th style="text-align: center;width: 15%">ENTRY BY</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php
                                    $total_opening_balance  = 0;
                                    $total_debit            = 0;
                                    $total_credit           = 0;
                                    $total_balance          = 0;
                                ?>

                                <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(count($value1['data']) > 0 || $value1['opening_balance'] > 0): ?>

                                <?php
                                    $i                          = 1;
                                    $sub_total_opening_balance  = $value1['opening_balance'];
                                    $sub_total_debit            = 0;
                                    $sub_total_credit           = 0;
                                    $sub_total_balance          = $value1['opening_balance'];
                                ?>

                                <tr>
                                    <th colspan="7" style="text-align: left;"><?php echo e($value1['account_name']); ?></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                </tr>

                                <tr>
                                    <th colspan="7" style="text-align: right;">Opening Balance</th>
                                    <th style="text-align: right;"><?php echo e(number_format($sub_total_opening_balance,0,'.',',')); ?></th>
                                    <th style="text-align: right;"></th>
                                </tr>

                                <?php $__currentLoopData = $value1['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php
                                    if ($value['debit_credit'] == 0)
                                    {
                                        $debit          = $value['amount'];
                                        $total_debit    = $total_debit + $value['amount'];
                                        $credit         = 0;
                                    }

                                    if ($value['debit_credit'] == 1)
                                    {
                                        $credit         = $value['amount'];
                                        $total_credit   = $total_credit + $value['amount'];
                                        $debit          = 0;
                                    }

                                    $sub_total_opening_balance  = $sub_total_opening_balance + $value1['opening_balance'];
                                    $sub_total_balance          = $sub_total_balance + $credit - $debit;
                                    $sub_total_debit            = $sub_total_debit + $debit;
                                    $sub_total_credit           = $sub_total_credit + $credit;

                                    if ($value['transaction_head'] == 'payment-receive')
                                    {
                                        if ($value->invoice_id != null)
                                        {
                                            $trans_number     = 'INV - '.str_pad($value->invoice->invoice_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head = 'Sales of Items';
                                            $transaction_note = $value->note;
                                            $register         = $value->customer->name;
                                            $payment_methode  = $value->account->account_name;
                                            $text             = 'Received from ';
                                            $entries          = collect();
                                            $rowspan          = 1;
                                        }
                                        elseif ($value->purchase_return_id != null)
                                        {
                                            $trans_number     = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head = 'Return of Items';
                                            $transaction_note = $value->note;
                                            $register         = $value->customer->name;
                                            $payment_methode  = $value->account->account_name;
                                            $text             = 'Received from ';
                                            $entries          = collect();
                                            $rowspan          = 1;
                                        }
                                        else
                                        {
                                            $trans_number     = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head = $value->purchase_return_id != null ? 'Purchase Return of Items' : 'Previous Due Collection';
                                            $transaction_note = $value->note;
                                            $register         = $value->customer->name;
                                            $payment_methode  = $value->account->account_name;
                                            $text             = 'Received from ';
                                            $entries          = collect();
                                            $rowspan          = 1;
                                        }
                                    }
                                    elseif ($value['transaction_head'] == 'sales-return')
                                    {
                                        $trans_number       = 'SR - '.str_pad($value->salesReturn->sales_return_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = 'Sales Return of items';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = $value->account->name;
                                        $text               = 'Return Back to ';
                                    }
                                    elseif ($value['transaction_head'] == 'payment-made')
                                    {
                                        if ($value->bill_id != null)
                                        {
                                            $trans_number       = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head   = 'Purchase of Items';
                                            $transaction_note   = $value->note;
                                            $register           = $value->customer->name;
                                            $payment_methode    = $value->account->account_name;
                                            $text               = 'Paid to ';
                                            $entries            = collect();
                                            $rowspan            = 1;

                                        }
                                        elseif ($value->sales_return_id != null)
                                        {
                                            $trans_number       = 'SR - '.str_pad($value->salesReturn->sales_return_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head   = 'Return of Items';
                                            $transaction_note   = $value->note;
                                            $register           = $value->customer->name;
                                            $payment_methode    = $value->account->account_name;
                                            $text               = 'Paid to ';
                                            $entries            = collect();
                                            $rowspan            = 1;

                                        }
                                        else
                                        {
                                            $trans_number       = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head   = $value->sales_return_id != null ? 'Sales Return of items' : 'Previous Due Paid';
                                            $transaction_note   = $value->note;
                                            $register           = $value->customer->name;
                                            $payment_methode    = $value->account->account_name;
                                            $text               = 'Paid to ';
                                            $entries            = collect();
                                            $rowspan            = 1;
                                        }
                                    }
                                    elseif ($value['transaction_head'] == 'purchase-return')
                                    {
                                        $trans_number       = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = $value->purchase_return_id != null ? 'Purchase Return of items' : 'Previous Due Paid';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = $value->account->name;
                                        $text               = 'Received Back from ';
                                    }
                                    elseif ($value['transaction_head'] == 'income')
                                    {
                                        $trans_number       = 'INC - '.str_pad($value->income->income_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = 'Income';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer_id != null ? $value->customer->name : $value->income->account->account_name;
                                        $payment_methode    = $value->account->name;
                                        $text               = $value->customer_id != null ? 'Income From' : 'Received from ';
                                    }
                                    elseif ($value['transaction_head'] == 'expense')
                                    {
                                        $trans_number       = 'EXP - '.str_pad($value->expense->expense_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = 'Expense';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer_id != null ? $value->customer->name : $value->expense->account->account_name;;
                                        $payment_methode    = $value->account->account_name;
                                        $text               = $value->customer_id != null ? 'To ' : 'Against ';
                                    }
                                    elseif ($value['transaction_head'] == 'customer-settlement')
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = 'Customer Settlement';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = $value->account->name;
                                        $text               = 'Return Customer Advance To';
                                    }
                                    elseif ($value['transaction_head'] == 'supplier-settlement')
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = 'Supplier Settlement';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = $value->account->name;
                                        $text               = 'Return Back Advance From ';
                                    }
                                    elseif ($value['transaction_head'] == 'balance-transfer')
                                    {
                                        if ($value['debit_credit'] == 0)
                                        {
                                            $trans_number       = '';
                                            $transaction_head   = 'Balance Transfer';
                                            $transaction_note   = $value->note;
                                            $register           = $value->balanceTransfer->transferTo->account_name;
                                            $payment_methode    = $value->account->account_name;
                                            $text               = 'From Cash To ';
                                        }
                                        else
                                        {
                                            $trans_number       = '';
                                            $transaction_head   = 'Balance Transfer';
                                            $transaction_note   = $value->note;
                                            $register           = $value->balanceTransfer->transferTo->account_name;
                                            $payment_methode    = $value->account->account_name;
                                            $text               = 'From ' . $value->balanceTransfer->transferFrom->account_name. ' To ';
                                        }
                                    }
                                    else
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = '';
                                        $register           = '';
                                        $transaction_note   = '';
                                        $payment_methode    = '';
                                        $text               = '';
                                    }
                                ?>

                                <tr>
                                    <td style="text-align: center;font-size: 12px"><?php echo e($i); ?></td>
                                    <td style="text-align: center;font-size: 12px"><?php echo e(date('d-m-Y', strtotime($value['date']))); ?></td>
                                    <td style="text-align: center;font-size: 12px"><?php echo e($trans_number); ?></td>
                                    <td style="text-align: center;font-size: 12px"><strong><?php echo e($transaction_head); ?> </strong> <br> <?php echo e($transaction_note); ?></td>
                                    <td style="text-align: left;font-size: 12px"><?php echo e($text . ' '); ?> <strong><?php echo e($register); ?></strong></td>
                                    <td style="text-align: right;font-size: 12px"><?php echo e(number_format($debit,0,'.',',')); ?></td>
                                    <td style="text-align: right;font-size: 12px"><?php echo e(number_format($credit,0,'.',',')); ?></td>
                                    <td style="text-align: right;font-size: 12px"><?php echo e($sub_total_balance >= 0 ? number_format($sub_total_balance,0,'.',',') : '('.number_format(abs($sub_total_balance),0,'.',',').')'); ?></td>
                                    <td style="text-align: left;font-size: 12px"><?php echo e(isset($value->createdBy->name) ? $value->createdBy->name : ''); ?></td>
                                </tr>

                                <?php
                                    $i++;
                                ?>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                <tr>
                                    <th colspan="5" style="text-align: right;">SUB TOTAL</th>
                                    <th style="text-align: right;"><?php echo e(number_format($sub_total_debit,0,'.',',')); ?></th>
                                    <th style="text-align: right;"><?php echo e(number_format($sub_total_credit,0,'.',',')); ?></th>
                                    <th style="text-align: right;"><?php echo e($sub_total_balance >= 0 ? number_format($sub_total_balance,0,'.',',') : '('.number_format(abs($sub_total_balance),0,'.',',').')'); ?></th>
                                    <th style="text-align: right;"></th>
                                </tr>

                                <?php
                                    $total_opening_balance  = $total_opening_balance + $sub_total_opening_balance;
                                    $total_debit            = $total_debit + $sub_total_debit;
                                    $total_credit           = $total_credit + $sub_total_credit;
                                    $total_balance          = $total_balance + $sub_total_balance;
                                ?>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="7" style="text-align: right;">GRAND TOTAL</th>
                                    <th style="text-align: right;"><?php echo e($total_balance >= 0 ? number_format($total_balance,0,'.',',') : '('.number_format(abs($total_balance),0,'.',',').')'); ?></th>
                                    <th style="text-align: right;"></th>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- Sales Statement End -->

                        <!-- Footer Start -->
                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/85/Modules/Reports/Resources/views/income_statement_print.blade.php ENDPATH**/ ?>