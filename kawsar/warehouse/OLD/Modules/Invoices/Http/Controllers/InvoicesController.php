<?php

namespace Modules\Invoices\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Users;
use App\Models\SrItems;
use App\Models\FreeItems;
use App\Models\UnitConversions;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\Stores;
use Carbon\Carbon;
use Response;
use DB;
use View;

class InvoicesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoices           = Invoices::orderBy('id', 'DESC')->first();
        $paid_accounts      = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();
        $accounts           = Accounts::where('account_type_id',9)->where('status', 1)->get();

        return view('invoices::index', compact('paid_accounts', 'invoices', 'accounts'));
    }

    public function AllSales()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        
        $paid_accounts      = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();

        return view('invoices::all_sales', compact('paid_accounts'));
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('invoices::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'          => 'required',
            'customer_id'           => 'required',
            'product_entries.*'     => 'required',
            'amount.*'              => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id        = Auth::user()->id;
        $data           = $request->all();
        $cus_cre_limit  = Customers::find($data['customer_id']);

        if (($cus_cre_limit['balance'] - $data['cash_given']) > $cus_cre_limit['credit_limit'])
        {
            return back()->with("unsuccess","This Customer do not have enough credit !!");
        }

        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];
             
            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    if($data['main_unit_id'][$key] == $data['unit_id'][$key])
                    {
                        $product    = ProductEntries::find($value);
                        $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                    }
                    else
                    {
                        $product1   = UnitConversions::where('product_entry_id', $value)
                                                        ->where('main_unit_id', $data['main_unit_id'][$key])
                                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                                        ->first();
                       
                        $buy_price  = $buy_price + ($product1['purchase_price']*$data['quantity'][$key]);
                    }
                }

            $data_find                          = Invoices::orderBy('created_at', 'DESC')->first();
            $invoice_number                     = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

            $invoice                            = new Invoices;;
            $invoice->invoice_number            = $invoice_number;
            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->sr_id                     = $data['sr_id'] != 0 ? $data['sr_id'] : Null;
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $data['total_amount'];
            $invoice->total_buy_price           = round($buy_price, 2);
            $invoice->total_discount            = $discount;
            $invoice->invoice_note              = $data['invoice_note'];
            $invoice->total_vat                 = $vat;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->previous_due              = $data['previous_due'];
            $invoice->previous_due_type         = $data['balance_type'];
            $invoice->adjusted_amount           = $data['adjustment'];
            $invoice->account_id                = $data['account_id'];
            $invoice->created_by                = $user_id;

            if (isset($data['type']))
            {
                $invoice->type = $data['type'];
            }

            if ($invoice->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price      = ProductEntries::find($value);
                    $conversion_rate_find   = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key])
                                                            ->where('converted_unit_id', $data['unit_id'][$key])
                                                            ->where('product_entry_id', $value)
                                                            ->first();
                                                            
                    $invoice_entries[] = [
                        'invoice_id'              => $invoice['id'],
                        'product_id'              => $product_buy_price['product_id'],
                        'product_entry_id'        => $value,
                        'free_product_entry_id'   => isset($data['free_items'][$key]) ? $data['free_items'][$key] : Null,
                        'customer_id'             => $invoice['customer_id'],
                        'main_unit_id'            => $data['main_unit_id'][$key],
                        'conversion_unit_id'      => $data['unit_id'][$key],
                        'free_main_unit_id'       => isset($data['free_item_main_unit_id'][$key]) ? $data['free_item_main_unit_id'][$key] : null,
                        'free_conversion_unit_id' => isset($data['free_unit_id'][$key]) ? $data['free_unit_id'][$key] : null,
                        'reference_id'            => $data['reference_id'],
                        'buy_price'               => ($data['main_unit_id'][$key] == $data['unit_id'][$key]) ? round($product_buy_price['buy_price'], 2) : $conversion_rate_find['purchase_price'],
                        'rate'                    => $data['rate'][$key],
                        'quantity'                => $data['quantity'][$key],
                        'free_quantity'           => $data['free_quantity'][$key],
                        'total_amount'            => $data['amount'][$key],
                        'discount_type'           => $data['discount_type'][$key],
                        'discount_amount'         => $data['discount'][$key],
                        'created_by'              => $user_id,
                        'created_at'              => date('Y-m-d H:i:s'),
                    ];

                    $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $data['quantity'][$key]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key];

                    if (isset($data['free_unit_id'][$key]))
                    {
                        $free_conversion_rate_find    = UnitConversions::where('main_unit_id', $data['free_item_main_unit_id'][$key])
                                        ->where('converted_unit_id', $data['free_unit_id'][$key])
                                        ->where('product_entry_id', $value)
                                        ->first();

                        $free_converted_quantity_to_main_unit  = $free_conversion_rate_find != null ? $data['free_quantity'][$key]/$free_conversion_rate_find['conversion_rate'] : $data['free_quantity'][$key];
                    }
                    else
                    {
                        $free_converted_quantity_to_main_unit  = 0;
                    }

                    // if ($data['sr_id'] != 0)
                    // {
                    //     $sr_items[] = [
                    //         'sales_id'          => $invoice['id'],
                    //         'sr_id'             => $data['sr_id'],
                    //         'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                    //         'product_id'        => $product['product_id'],
                    //         'product_entry_id'  => $value,
                    //         'main_unit_id'      => $data['main_unit_id'][$key],
                    //         'conversion_unit_id'=> $data['unit_id'][$key],
                    //         'type'              => 2,
                    //         'quantity'          => $converted_quantity_to_main_unit,
                    //         'created_by'        => $user_id,
                    //         'created_at'        => date('Y-m-d H:i:s'),
                    //     ];
                    // }

                    if (isset($data['free_items'][$key]) && ($data['free_items'][$key] != 0))
                    {
                        $sr_items[] = [
                            'sales_id'          => $invoice['id'],
                            'sr_id'             => $data['sr_id'],
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'product_id'        => $product['product_id'],
                            'product_entry_id'  => $data['free_items'][$key],
                            'type'              => 2,
                            'quantity'          => $free_converted_quantity_to_main_unit,
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }

                    if ($data['free_quantity'][$key] != 0)
                    {
                        $free_items[] = [
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'invoice_id'        => $invoice['id'],
                            'customer_id'       => $data['customer_id'],
                            'sr_id'             => $data['sr_id'],
                            'product_entry_id'  => $value,
                            'total_quantity'    => $free_converted_quantity_to_main_unit,
                            'free_quantity'     => $data['free_quantity'][$key],
                            'purchase_price'    => round($product_buy_price['buy_price'], 2),
                            'sell_price'        => $data['rate'][$key],
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('invoice_entries')->insert($invoice_entries);

                // if ($data['sr_id'] != 0)
                // {
                //     DB::table('sr_items')->insert($sr_items);
                // }

                if (isset($free_items))
                {
                    DB::table('free_items')->insert($free_items);
                }

                stockOut($data, $item_id=null);

                //Financial Accounting Start
                    debit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=8, $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    credit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=$data['account_id'], $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

                    //Insert into journal_entries 
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {
                                debit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                credit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=8, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                            }
                        }
                    }

                    customerBalanceUpdate($data['customer_id']);
                //Financial Accounting End

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Sales Created Successfully !!");
                }
                else
                {
                    $user_info  = userDetails();

                    if ($user_info['pos_printer'] == 0)
                    {
                        return redirect()->route('invoices_show_pos', $invoice->id);
                    }
                    else
                    {
                        return redirect()->route('invoices_show', $invoice->id);
                    }
                }
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.address as address',
                                             'customers.phone as phone')
                                    ->find($id);

        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->where('invoice_entries.invoice_id', $id)
                                    ->orderBy('invoice_entries.id', 'DESC')
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.name as product_entry_name',
                                             'products.name as product_name')
                                    ->get();  
        
        $find_customer      = Customers::find($invoice['customer_id']);
                                    
        $sr_name_id         = Customers::select('customers.sr_id as sr_id')
                                    ->where('customers.id',$invoice['customer_id'])
                                    ->first();
                            
        $customer_sr_name   = Customers::select('customers.name as sr_name')
                                        ->where('customers.id',$sr_name_id->sr_id)
                                        ->first();

        if ($find_customer['store_id'] != null)
        {
            $user_info  = Stores::find($find_customer['store_id']);
        }  
        else
        {
            $user_info  = userDetails();
        }    

        return view('invoices::show', compact('entries', 'invoice', 'user_info', 'sr_name_id', 'customer_sr_name'));
    }

    public function showPos($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.address as address',
                                             'customers.phone as phone')
                                    ->find($id);

        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->where('invoice_entries.invoice_id', $id)
                                    ->orderBy('invoice_entries.id', 'DESC')
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.name as product_entry_name',
                                             'products.name as product_name')
                                    ->get();   
        
        $user_infos         = userDetails();
        $sr_name_id         = Customers::select('customers.sr_id as sr_id')
                                        ->where('customers.id',$invoice['customer_id'])
                                        ->first();              
        $customer_sr_name   = Customers::select('customers.name as sr_name')
                                        ->where('customers.id',$sr_name_id->sr_id)
                                        ->first();             
        $find_customer  = Customers::find($invoice['customer_id']);

        if ($find_customer['store_id'] != null)
        {
            $user_info  = Stores::find($find_customer['store_id']);
        }  
        else
        {
            $user_info  = userDetails();
        }

        return view('invoices::show_pos', compact('entries', 'invoice', 'user_info', 'user_infos', 'sr_name_id', 'customer_sr_name'));
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End


        $find_invoice           = Invoices::leftjoin('customers as customers', 'customers.id', '=', 'invoices.customer_id')
                                            ->leftjoin('customers as srs', 'srs.id', '=', 'invoices.sr_id')
                                            ->select('invoices.*',
                                                 'customers.id as customer_id',
                                                 'customers.name as contact_name',
                                                 'srs.id as sr_id',
                                                 'srs.name as sr_name')
                                            ->find($id);

        $find_invoice_entries   = InvoiceEntries::leftjoin('customers', 'customers.id', 'invoice_entries.customer_id')  
                                            ->leftjoin('product_entries AS main_product', 'main_product.id', '=', 'invoice_entries.product_entry_id')
                                            ->leftjoin('product_entries AS free_product', 'free_product.id', '=', 'invoice_entries.free_product_entry_id')
                                            ->where('invoice_entries.invoice_id', $id)
                                            ->select('invoice_entries.*',
                                                    'customers.id as customer_id',
                                                    'customers.name as customer_name',
                                                    'main_product.id as item_id',
                                                    'main_product.stock_in_hand as stock_in_hand',
                                                    'main_product.name as item_name',
                                                    'free_product.id as free_item_id',
                                                    'free_product.name as free_item_name')
                                            ->get();

        $entries_count          = $find_invoice_entries->count();

        $current_balance        = JournalEntries::whereIn('transaction_head', ['payment-receive'])
                                                ->where('invoice_id', $id)
                                                ->where('debit_credit', 1)
                                                ->selectRaw('journal_entries.*')
                                                ->get();

        $current_balance_count  = $current_balance->count();

        $paid_accounts          = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();
        $accounts               = Accounts::where('account_type_id',9)->where('status', 1)->get();

        return view('invoices::edit', compact('find_invoice', 'find_invoice_entries', 'entries_count', 'paid_accounts', 'current_balance', 'current_balance_count', 'accounts'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'      => 'required',
            'customer_id'       => 'required',
            'product_entries.*' => 'required',
            'amount.*'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat           = $data['vat_amount'];
            $old_invoice   = Invoices::find($id);
            $invoice       = Invoices::find($id);

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    if($data['main_unit_id'][$key] == $data['unit_id'][$key])
                    {
                        $product    = ProductEntries::find($value);
                        $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                    }
                    else
                    {
                        $product1   = UnitConversions::where('product_entry_id', $value)
                                                        ->where('main_unit_id', $data['main_unit_id'][$key])
                                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                                        ->first();
                       
                        $buy_price  = $buy_price + ($product1['purchase_price']*$data['quantity'][$key]);
                    }
                }

            //Calculate Due Amount

                if ($data['total_amount'] > $invoice['invoice_amount']) 
                {
                    $invoice_dues = $invoice['due_amount'] + ($data['total_amount'] - $invoice['invoice_amount']);

                }
                
                if ($data['total_amount'] < $invoice['invoice_amount'])
                {
                    $invoice_dues = $invoice['due_amount'] - ($invoice['invoice_amount'] - $data['total_amount']);
                }

                if ($data['total_amount'] == $invoice['invoice_amount'])
                {
                    $invoice_dues = $invoice['due_amount'];
                }
            
            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->sr_id                     = $data['sr_id'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $invoice_dues;
            $invoice->total_buy_price           = round($buy_price, 2);
            $invoice->total_discount            = $discount;
            $invoice->invoice_note              = $data['invoice_note'];
            $invoice->total_vat                 = $vat;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->discount_code             = $data['coupon_code'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->updated_by                = $user_id;

            if ($invoice->save())
            {
                $item_id                = InvoiceEntries::where('invoice_id', $invoice['id'])->get();
                $item_delete            = InvoiceEntries::where('invoice_id', $invoice['id'])->delete();

                if ($old_invoice['order_id'] != null)
                {
                    $delete_sr_items        = SrItems::where('sales_id', $invoice['id'])->delete();
                }

                $delete_free_items      = FreeItems::where('invoice_id', $invoice['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'              => $invoice['id'],
                        'product_id'              => $product_buy_price['product_id'],
                        'product_entry_id'        => $value,
                        'free_product_entry_id'   => $data['free_items'][$key],
                        'customer_id'             => $invoice['customer_id'],
                        'main_unit_id'            => $data['main_unit_id'][$key],
                        'conversion_unit_id'      => $data['unit_id'][$key],
                        'free_main_unit_id'       => isset($data['free_item_main_unit_id'][$key]) ? $data['free_item_main_unit_id'][$key] : null,
                        'free_conversion_unit_id' => isset($data['free_unit_id'][$key]) ? $data['free_unit_id'][$key] : null,
                        'reference_id'            => $data['reference_id'],
                        'buy_price'               => round($product_buy_price['buy_price'], 2),
                        'rate'                    => $data['rate'][$key],
                        'quantity'                => $data['quantity'][$key],
                        'free_quantity'           => $data['free_quantity'][$key],
                        'total_amount'            => $data['amount'][$key],
                        'discount_type'           => $data['discount_type'][$key],
                        'discount_amount'         => $data['discount'][$key],
                        'created_by'              => $user_id,
                        'created_at'              => date('Y-m-d H:i:s'),
                    ];

                    $conversion_rate_find    = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key])
                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                        ->where('product_entry_id', $value)
                                        ->first();

                    $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $data['quantity'][$key]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key];

                    if (isset($data['free_unit_id'][$key]))
                    {
                        $free_conversion_rate_find    = UnitConversions::where('main_unit_id', $data['free_item_main_unit_id'][$key])
                                        ->where('converted_unit_id', $data['free_unit_id'][$key])
                                        ->where('product_entry_id', $value)
                                        ->first();

                        $free_converted_quantity_to_main_unit  = $free_conversion_rate_find != null ? $data['free_quantity'][$key]/$free_conversion_rate_find['conversion_rate'] : $data['free_quantity'][$key];
                    }
                    else
                    {
                        $free_converted_quantity_to_main_unit  = 0;
                    }

                    if ($old_invoice['order_id'] != null)
                    {
                        if ($data['sr_id'] != 0)
                        {
                            $sr_items[] = [
                                'sales_id'          => $invoice['id'],
                                'sr_id'             => $data['sr_id'],
                                'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                                'product_id'        => $product['product_id'],
                                'product_entry_id'  => $value,
                                'type'              => 2,
                                'quantity'          => $converted_quantity_to_main_unit,
                                'created_by'        => $user_id,
                                'created_at'        => date('Y-m-d H:i:s'),
                            ];
                        }
                    }

                    if ($data['free_items'][$key] != 0)
                    {
                        $sr_items[] = [
                            'sales_id'          => $invoice['id'],
                            'sr_id'             => $data['sr_id'],
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'product_id'        => $product['product_id'],
                            'product_entry_id'  => $data['free_items'][$key],
                            'type'              => 2,
                            'quantity'          => $free_converted_quantity_to_main_unit,
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }

                    if ($data['free_quantity'][$key] != 0)
                    {
                        $free_items[] = [
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'invoice_id'        => $invoice['id'],
                            'customer_id'       => $data['customer_id'],
                            'sr_id'             => $data['sr_id'],
                            'product_entry_id'  => $value,
                            'total_quantity'    => $data['quantity'][$key],
                            'free_quantity'     => $free_converted_quantity_to_main_unit,
                            'purchase_price'    => round($product_buy_price['buy_price'], 2),
                            'sell_price'        => $data['rate'][$key],
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('invoice_entries')->insert($invoice_entries);

                if ($old_invoice['order_id'] != null)
                {
                    DB::table('sr_items')->insert($sr_items);
                }

                if (isset($free_items))
                {
                    DB::table('free_items')->insert($free_items);
                }

                if ($old_invoice['order_id'] == null)
                {
                    stockOut($data, $item_id);
                }

                $jour_ent_debit     = JournalEntries::where('invoice_id', $invoice->id)
                                        ->where('transaction_head', 'sales')
                                        ->where('debit_credit', 1)
                                        ->first();

                $jour_ent_credit    = JournalEntries::where('invoice_id', $invoice->id)
                                        ->where('transaction_head', 'sales')
                                        ->where('debit_credit', 0)
                                        ->first();
                //Financial Accounting Start
                    debitUpdate($jour_ent_debit['id'], $customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=8, $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    creditUpdate($jour_ent_credit['id'], $customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=$data['account_id'], $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //Financial Accounting End

                //Insert into journal_entries Start
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {   
                                $pay_debit  = JournalEntries::find($data['current_balance_id'][$i]);
                                $pay_credit = JournalEntries::find($data['current_balance_id'][$i] + 1);

                                if ($pay_debit != null)
                                { 
                                    debitUpdate($pay_debit['id'], $customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                                else
                                {
                                    debit($customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }

                                if ($pay_credit != null)
                                { 
                                    creditUpdate($pay_credit['id'], $customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=8, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                                else
                                {
                                    credit($customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=8, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                            }
                            else
                            {
                                $pay_debit  = JournalEntries::where('id', $data['current_balance_id'][$i])->delete();
                                $pay_credit = JournalEntries::where('id', $data['current_balance_id'][$i] + 1)->delete();
                            }
                        }
                    }

                    customerBalanceUpdate($data['customer_id']);
                //Insert into journal_entries End

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return redirect()->route('invoices_all_sales')->with("success","Sales Updated Successfully !!");
                }
                else
                {
                    $user_info  = userDetails();

                    if ($user_info['pos_printer'] == 0)
                    {
                        return redirect()->route('invoices_show_pos', $invoice->id);
                    }
                    else
                    {
                        return redirect()->route('invoices_show', $invoice->id);
                    }
                }
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function productList()
    {
        $data       = ProductEntries::where('product_entries.stock_in_hand', '>', 0)
                                    ->Where('product_entries.stock_in_hand', '!=', null)
                                    ->orderBy('product_entries.total_sold', 'DESC')
                                    ->select('product_entries.*')
                                    ->get();

        return Response::json($data);
    }

    public function makePayment($id)
    {
        $data       = Invoices::find($id);

        return Response::json($data);
    }

    public function storePayment(Request $request)
    {
        $user_id                    = Auth::user()->id;
        $data                       = $request->all();

        DB::beginTransaction();

        try{

            if (isset($data['amount_paid']))
            {
                $data_find        = Payments::orderBy('id', 'DESC')->first();
                $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                for($i = 0; $i < count($data['amount_paid']); $i++)
                {   
                    if ($data['amount_paid'][$i] > 0)
                    {
                        $payments = [
                            'payment_number'        => $payment_number,
                            'customer_id'           => $data['customer_id'],
                            'payment_date'          => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $data['amount_paid'][$i],
                            'paid_through'          => $data['paid_through'][$i],
                            'account_information'   => $data['account_information'][$i],
                            'note'                  => $data['note'][$i],
                            'type'                  => 0,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $payment_id = DB::table('payments')->insertGetId($payments);      

                        if ($payment_id)
                        {   
                            $payment_entries = [
                                    'payment_id'        => $payment_id,
                                    'invoice_id'        => $data['invoice_id'],
                                    'amount'            => $data['amount_paid'][$i],
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            DB::table('payment_entries')->insert($payment_entries);  
                        }

                        $update_invoice_dues                = Invoices::find($data['invoice_id']);
                        $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                        $update_invoice_dues->save();

                        $payment_number++;
                    }
                }
            }
            
            DB::commit();
            return Response::json(1);
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            dd($exception);
            return Response::json(0);
        }
    }

    public function productPriceList($id)
    {
        $data['product_entries']        = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                    // ->where('product_entries.product_id', '!=', 1)
                                                    ->selectRaw('product_entries.*, units.name as unit_name')
                                                    ->find($id);

        $data1                          = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                        ->where('product_entries.id', $id)
                                                        ->selectRaw('units.id as unit_id, units.name as unit_name')
                                                        ->get()
                                                        ->toArray();

        $data2                          = UnitConversions::leftjoin('units', 'units.id', 'unit_conversions.converted_unit_id')
                                                    ->where('unit_conversions.product_entry_id', $id)
                                                    ->selectRaw('unit_conversions.converted_unit_id as unit_id, units.name as unit_name')
                                                    ->get()
                                                    ->toArray();

        $data['unit_conversions']       = collect(array_merge($data1, $data2));

        return Response::json($data);
    }

    public function productPriceListBySr($product_entry_id, $sr_id)
    {
        $data['product_entries']        = SrItems::leftjoin('product_entries', 'product_entries.id', 'sr_items.product_entry_id')
                                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                    ->where('sr_items.sr_id', $sr_id)
                                                    ->where('sr_items.product_entry_id', $product_entry_id)
                                                    ->groupBy('sr_items.product_entry_id')
                                                    ->select(DB::raw("GROUP_CONCAT(DISTINCT sr_items.product_entry_id) as product_entry_id"),
                                                             DB::raw("GROUP_CONCAT(DISTINCT sr_items.product_id) as product_id"),
                                                             DB::raw("GROUP_CONCAT(DISTINCT product_entries.sell_price) as sell_price"),
                                                             DB::raw("GROUP_CONCAT(DISTINCT units.id) as unit_id"),
                                                             DB::raw("GROUP_CONCAT(DISTINCT units.name) as unit_name"),
                                                             DB::raw("SUM(IF(sr_items.type='1',sr_items.quantity,0)) -  SUM(IF(sr_items.type='2',sr_items.quantity,0)) - SUM(IF( sr_items.type='3',sr_items.quantity,0)) as stock_in_hand")
                                                            )
                                                    ->first();

        $data1                          = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                        ->where('product_entries.id', $product_entry_id)
                                                        ->selectRaw('units.id as unit_id, units.name as unit_name')
                                                        ->get()
                                                        ->toArray();

        $data2                          = UnitConversions::leftjoin('units', 'units.id', 'unit_conversions.converted_unit_id')
                                                    ->where('unit_conversions.product_entry_id', $product_entry_id)
                                                    ->selectRaw('unit_conversions.converted_unit_id as unit_id, units.name as unit_name')
                                                    ->get()
                                                    ->toArray();

        $data['unit_conversions']       = collect(array_merge($data1, $data2));
        
       

        return Response::json($data);
    }

    public function invoiceListLoad()
    {
        $data           = Invoices::leftjoin('sales_return', 'sales_return.invoice_id', 'invoices.id')
                                    ->leftjoin('customers AS customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('customers AS sm', 'sm.id', 'invoices.sr_id')
                                    ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->where('invoices.type', 1)
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                            'sales_return.id as return_id',
                                            'customers.name as customer_name',
                                            'sm.name as sm_name',
                                            'customers.phone as phone')
                                    ->distinct('invoices.id')
                                    ->take(500)
                                    ->get();

        return Response::json($data);
    }

    public function invoiceListSearch($from_date, $to_date, $customer, $invoice)
    {
        $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
        $search_by_customer     = $customer != 0 ? $customer : 0;
        $search_by_invoice      = $invoice != 0 ? $invoice : 0;

        $data           = Invoices::leftjoin('sales_return', 'sales_return.invoice_id', 'invoices.id')
                                        ->leftjoin('customers AS customers', 'customers.id', 'invoices.customer_id')
                                        ->leftjoin('customers AS sm', 'sm.id', 'invoices.sr_id')
                                        ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                        ->where('invoices.type', 1)
                                        ->whereBetween('invoices.invoice_date', [$search_by_from_date, $search_by_to_date])
                                        ->when($search_by_customer != 0, function ($query) use ($search_by_customer) {
                                            return $query->where('customers.id', $search_by_customer);
                                        })
                                        ->when($search_by_invoice != 0, function ($query) use ($search_by_invoice) {
                                            return $query->where('invoices.id', $search_by_invoice);
                                        })
                                        ->orderBy('invoices.created_at', 'DESC')
                                        ->select('invoices.*',
                                                 'sales_return.id as return_id',
                                                 'customers.name as customer_name',
                                                 'sm.name as sm_name',
                                                 'customers.phone as phone')
                                        ->distinct('invoices.id')
                                        // ->take(20)
                                        ->get();

        return Response::json($data);
    }

    public function posSearchProduct($id)
    {
        $user_info      = userDetails();
        $data           = ProductEntries::where('product_entries.product_code', $id)
                                        ->where('product_entries.stock_in_hand', '>', 0)
                                        ->first();

        return Response::json($data);
    }

    public function customerStore(Request $request)
    {
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        $customers                              = new Customers;
        $customers->name                        = $data['customer_name'];
        $customers->address                     = $data['address'];
        $customers->phone                       = $data['mobile_number'];
        $customers->contact_type                = $data['contact_type'];
        $customers->created_by                  = $user_id;

        if ($customers->save())
        {   
            return Response::json($customers);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function customersListInvoice()
    {
        
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Customers::whereIn('customers.contact_type',[0,1,2,4,6])
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Customers::where('customers.name', 'LIKE', "%$search%")
                                    ->orWhere('customers.phone', 'LIKE', "%$search%")
                                    ->orWhere('customers.code', 'LIKE', "%$search%")
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->whereIn('customers.contact_type',[0,1,2,4,6])
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "phone" =>'', "code" =>'', "contact_type" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name'], "phone" =>$value['phone'], "code" =>$value['code'], "contact_type" =>$value['contact_type']);

            $i++;
        }

   
        return Response::json($data);
    }

    public function dsrCustomersListInvoice()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Customers::whereIn('customers.contact_type',[4])
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Customers::where('customers.name', 'LIKE', "%$search%")
                                    ->orWhere('customers.phone', 'LIKE', "%$search%")
                                    ->orWhere('customers.code', 'LIKE', "%$search%")
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->whereIn('customers.contact_type',[4])
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "phone" =>'', "code" =>'', "contact_type" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name'], "phone" =>$value['phone'], "code" =>$value['code'], "contact_type" =>$value['contact_type']);

            $i++;
        }

   
        return Response::json($data);
    }

    public function customersSm($customer_id)
    {
        $sr_id      = Customers::select('customers.*','customers.sr_id')->where('id',$customer_id)->first();
        $sr_name    = Customers::select('customers.*')->where('id',$sr_id->sr_id)->first();
        
        return Response::json($sr_name);
    }
    
    public function customersAddress($customer_id)
    {
        $address    = Customers::select('customers.address', 'customers.code')
                               ->where('id', $customer_id)
                               ->first();
        return Response::json($address);
    }

    public function couponCode($id)
    {
        $today          = Carbon::today();

        $data           = Discounts::leftjoin('discount_products', 'discount_products.discount_id', 'discounts.id')
                                ->where('discounts.coupon_code', $id)
                                ->orWhere('discounts.card_number', $id)
                                ->whereDate('discounts.expire_date', '>=', $today->format('Y-m-d'))
                                ->where('discounts.status', 1)
                                ->select('discount_products.product_id as product_id', 
                                         'discounts.discount_type as discount_type',
                                         'discounts.discount_amount as discount_amount',
                                         'discounts.expire_date as expire_date'
                                        )
                                ->get();

        return Response::json($data);
    }

    public function printInvoicesList()
    {
        $user_id        = Auth::user()->id;
        $data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->where('invoices.type', 1)
                                    ->where('invoices.created_by', $user_id)
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                            'customers.name as customer_name',
                                            'customers.phone as phone',
                                            'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function printInvoicesSearch($date,$customer,$invoice_number)
    {
        $search_by_date              = $date != 0 ? date('Y-m-d', strtotime($date)) : 0;
        $search_by_invoice_number    = $invoice_number != 0 ? $invoice_number : 0;
        $search_by_invoice_customer  = $customer != 0 ? $customer : 0;
        $user_info                   = userDetails();

        $data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->where('invoices.type', 1)
                                    ->when($search_by_date != 0, function ($query) use ($search_by_date) {
                                        return $query->where('invoices.invoice_date', 'LIKE', "%$search_by_date%");
                                    })
                                    ->when($search_by_invoice_number != 0, function ($query) use ($search_by_invoice_number) {
                                        return $query->where('invoices.invoice_number', 'LIKE', "%$search_by_invoice_number%");
                                    })
                                    ->when($search_by_invoice_customer != 0, function ($query) use ($search_by_invoice_customer) {
                                        return $query->where('customers.name', 'LIKE', "%$search_by_invoice_customer%");
                                    })
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.phone as phone',
                                             'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function calculateOpeningBalance($customer_id)
    {
        $data  = calculateOpeningBalance($customer_id);

        return Response::json($data);
    }

    public function srProductStock($sr_id, $product_entry_id)
    {
        $data       = SrItems::leftjoin('issues', 'issues.id', 'sr_items.issue_id')
                                    ->leftjoin('customers', 'customers.id', 'sr_items.sr_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'sr_items.product_entry_id')
                                    ->where('sr_items.sr_id', $sr_id)
                                    ->where('sr_items.product_entry_id', $product_entry_id)
                                    ->groupBy('sr_items.product_entry_id')
                                    ->select(DB::raw('group_concat(distinct sr_items.product_entry_id) as product_entry_id'),
                                             DB::raw('group_concat(distinct product_entries.name) as product_name'),
                                             DB::raw("SUM(IF(sr_items.type='1',sr_items.quantity,0)) AS receive_quantity"),
                                             DB::raw("SUM(IF(sr_items.type='2',sr_items.quantity,0)) AS sold_quantity"),
                                             DB::raw("SUM(IF( sr_items.type='3',sr_items.quantity,0)) AS return_quantity")
                                            )
                                    ->first();
        
      
        return Response::json($data);
    }

    public function adjustAdvancePayment($customer_id)
    {
        $data  = Customers::find($customer_id);

        if ($data['balance'] < 0)
        {
            $result = abs($data['balance']);
        }
        else
        {
            $result = 0;
        }

        return Response::json($result);
    }
}