@extends('layouts.app')

@section('title', 'Edit Return Issues')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('issues_return_update', $find_return['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

                                <div class="row">

                                    <div style="background-color: #D2D2D2;height: 620px;padding-top: 10px;" class="col-md-7">
                                        <div style="display: none" class="inner-repeater mb-4 issueDetails">
                                            <div data-repeater-list="inner-group" class="inner form-group row">
                                                <div id="return_product_list" class="inner col-lg-12 ml-md-auto">

                                                    @foreach($find_ireturn_entries as $key => $value)
                                                    <div class="row di_{{$key}}">
                                                        <div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-4 col-md-4 col-sm-6 col-12">
                                                            <label class="show-xs" for="productname">Group *</label>
                                                            <select style="width: 100%" class="inner form-control select2" id="group_id_{{$key}}">
                            
                                                                <option value="{{ $value->productEntries->group_id }}" selected>{{ $value->productEntries->group->name }}</option>
                                                                
                                                            </select>
                                                        </div>

                                                        <div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-4 col-md-4 col-sm-6 col-12">
                                                            <label style="" class="show-xs" for="productname">Brand *</label>
                                                            <select style="width: 100%" class="inner form-control select2" id="brand_id_{{$key}}">
                            
                                                                <option value="{{ $value->productEntries->brand_id }}" selected>{{ $value->productEntries->brand->name }}</option>
                                                                
                                                            </select>
                                                        </div>

                                                        <div style="margin-bottom: 5px;padding-right: 10px" class="col-lg-4 col-md-4 col-sm-6 col-12">
                                                            <label style="" class="show-xs" for="productname">Category *</label>
                                                            <select style="width: 100%" class="inner form-control select2 productCategory" id="product_category_id_{{$key}}" onchange="getProductList({{$key}})">
                            
                                                                <option value="{{ $value->productEntries->product_id }}" selected>{{ $value->productEntries->product->name }}</option>
                                                                
                                                            </select>
                                                        </div>

                                                        <div style="margin-bottom: 5px;padding-left: 10px" class="col-lg-6 col-md-6 col-sm-6 col-12">
                                                            <label  class="show-xs" for="productname">Product *</label>
                                                            <select style="width: 100%" name="product_entries[]" class="inner form-control select2 productEntries" id="product_entries_{{$key}}" onchange="getItemPrice({{$key}})" required>
                                                                <option value="{{ $value['product_entry_id'] }}" selected>
                                                                    {{ $value->productEntries->group->name . ' - ' . $value->productEntries->brand->name . ' - ' . $value->productEntries->product->name . ' - ' . $value->productEntries->name }}
                                                                </option>
                                                            </select>
                                                        </div>

                                                        <input type="hidden" name="stock[]" class="inner form-control" id="stock_{{$key}}" placeholder="Stock" oninput="calculateActualAmount({{$key}})" readonly />
                                                        <input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_{{$key}}" value="{{ $value->unit_id }}" />
                                                        <input type="hidden" class="inner form-control" id="main_unit_name_{{$key}}" value="{{$value->productEntries->unit->name}}" />

                                                        <div style="padding-left: 0px" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                                            <label class="show-xs" for="productname">Unit</label>
                                                            <select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_{{$key}}" required onchange="getConversionParam({{$key}})">
                                                                <option value="{{ $value->conversion_unit_id }}" selected>{{ $value->convertedUnit->name }}</option>
                                                            </select>
                                                        </div>

                                                        <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                            <label class="show-xs" for="productname">Return Quantity</label>
                                                            <input type="text" name="return_quantity[]" class="inner form-control returnQuantity" id="quantity_{{$key}}" placeholder="Quantity" oninput="calculate({{$key}})" value="{{ $value->quantity }}" required />
                                                        </div>
                                                    </div>

                                                    <hr style="margin-top: 5px !important;margin-bottom: 5px !important">
                                                    @endforeach
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 620px;padding-top: 10px" class="col-md-5">

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Select DSM * </label>
                                                    <select id="sr_id" name="sr_id" class="form-control col-md-12" required>
                                                       <option value="{{ $value['sr_id'] }}">{{ $value->srName->name }}</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="return_date">Return Date *</label>
                                                    <input id="return_date" name="return_date" type="text" value="{{ date('d-m-Y', strtotime($find_return['date'])) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="return_note">Note</label>
                                                    <input id="return_note" name="return_note" type="text" value="{{ $find_return['note'] }}" class="form-control">
                                                </div>
                                            </div>

                                            <div style="display: none;margin-top: 20px" class="button-items col-lg-12 issueDetails">
                                                <button name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                                <!-- <button name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Update & Print</button> -->
                                                <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('all_return_issues') }}">Close</a></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            $('.issueDetails').show();

        });
    </script>
@endsection