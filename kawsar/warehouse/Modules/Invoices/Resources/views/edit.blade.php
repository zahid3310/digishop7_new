@extends('layouts.app')

@section('title', 'Edit Sales')

@push('styles')
<style type="text/css">
    body[data-sidebar=dark].vertical-collpsed {
        min-height: 0px !important;
    }

    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>
@endpush

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Order</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">Edit Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                            Payment Successfull !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                            Payment Not Added !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('invoices_update', $find_invoice['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                {{ csrf_field() }}

                                <input type="hidden" id="default_stock" value="0">

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Customer *</label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="customer_id" name="customer_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9" required>
                                                   <option value="{{ $find_invoice['customer_id'] }}" selected>{{ $find_invoice['contact_name'] }}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Reference</label>
                                            <div class="col-md-8">
                                                <select style="width: 75%" id="reference_id" name="reference_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                   <option value="{{ customersTableDetails($find_invoice['reference_id'])['id'] }}" selected>{{ customersTableDetails($find_invoice['reference_id'])['name'] }}</option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal1">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">SM </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="sr_id" name="sr_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9" required>
                                                    <option value="{{ $find_invoice['sr_id'] }}">{{ $find_invoice['sr_name'] }}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label id="bbShow" for="productname" class="col-md-4 col-form-label">{{ $find_invoice['previous_due_type'] == 1 ? 'Receivable' : 'Advance' }}</label>
                                            <div class="col-md-8">
                                                <input id="balance" name="previous_due" type="text" class="form-control" value="{{ $find_invoice['previous_due'] }}" readonly>
                                                <input id="bbBalance" type="hidden" name="balance_type" class="form-control" value="{{ $find_invoice['previous_due_type'] }}">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Barcode </label>
                                            <div class="col-md-8">
                                                <input type="text" name="product_code" class="inner form-control" id="product_code" placeholder="Scan Product Code" oninput="PosScannerSearch()" autofocus />
                                                <p id="alertMessage" style="display: none">No Product Found !</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Date *</label>
                                            <div class="col-md-8">
                                                <input id="selling_date" name="selling_date" type="text" value="{{ date('d-m-Y', strtotime($find_invoice['invoice_date'])) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;margin-left: 1px;" class="form-group row">
                                            <i id="add_field_button" style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button col-md-3"></i>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-3 col-form-label">Note </label>
                                            <div class="col-md-9">
                                                <input id="invoice_note" name="invoice_note" type="text" value="" class="form-control" value="{{ $find_invoice['invoice_note'] }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 260px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        @foreach($find_invoice_entries as $key => $value)
                                            <div style="margin-bottom: 0px !important" class="row di_{{$key}}">

                                                <div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">
                                                    <label class="show-xs" for="productname">Group *</label>
                                                    <select style="width: 100%" class="inner form-control select2" id="group_id_{{$key}}">
                    
                                                        <option value="{{ $value->productEntries->group_id }}" selected>{{ $value->productEntries->group->name }}</option>
                                                        
                                                    </select>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">
                                                    <label style="" class="show-xs" for="productname">Brand *</label>
                                                    <select style="width: 100%" class="inner form-control select2" id="brand_id_{{$key}}">
                    
                                                        <option value="{{ $value->productEntries->brand_id }}" selected>{{ $value->productEntries->brand->name }}</option>
                                                        
                                                    </select>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">
                                                    <label style="" class="show-xs" for="productname">Category *</label>
                                                    <select style="width: 100%" class="inner form-control select2 productCategory" id="product_category_id_{{$key}}" onchange="getCategoryList({{$key}})">
                    
                                                        <option value="{{ $value->productEntries->product_id }}" selected>{{ $value->productEntries->product->name }}</option>
                                                        
                                                    </select>
                                                </div>

                                                <div style="margin-bottom: 5px" class="col-lg-4 col-md-4 col-sm-6 col-12">
                                                    <label  class="show-xs" for="productname">Product *</label>
                                                    <select style="width: 100%" name="product_entries[]" class="inner form-control select2 productEntries" id="product_entries_{{$key}}" onchange="getItemPrice({{$key}})" required>
                                                        <option value="{{ $value['product_entry_id'] }}" selected>
                                                            {{ $value->productEntries->group->name . ' - ' . $value->productEntries->brand->name . ' - ' . $value->productEntries->product->name . ' - ' . $value->productEntries->name }}
                                                        </option>
                                                    </select>
                                                </div>

                                                <input type="hidden" name="stock[]" class="inner form-control" id="stock_{{$key}}" placeholder="Stock" value="{{ $value['stock_in_hand'] }}" />
                                                <input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_{{$key}}" value="{{ $value['main_unit_id'] }}" />
                                                <input type="hidden" class="inner form-control" id="main_unit_name_{{$key}}" />

                                                <div style="padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                                    <label  class="show-xs" for="productname">Unit</label>
                                                    <select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control" id="unit_id_{{$key}}" required onchange="getConversionParam({{$key}})">
                                                    <option value="{{ $value['conversion_unit_id'] }}">{{ $value->convertedUnit->name }}</option>
                                                    </select>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 10px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <label class="show-xs" for="productname">Qty *</label>
                                                    <input type="text" name="quantity[]" class="inner form-control mainProductQuantity" id="quantity_{{$key}}" value="{{$value['quantity']}}" placeholder="Quantity" onchange="calculateActualAmount({{$key}})" required/>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <label  class="show-xs" for="productname">Rate *</label>
                                                    <input type="text" name="rate[]" class="inner form-control" id="rate_{{$key}}" value="{{round($value['rate'], 2)}}" placeholder="Rate" oninput="calculateActualAmount({{$key}})" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-12 col-12">
                                                    
                                                    <div class="row">
                                                        <div  style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">
                                                            <label class="show-xs" style="padding-top: 15px" for="productname"></label>
                                                            <select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_{{$key}}" oninput="calculateActualAmount({{$key}})">
                                                                <option value="1" {{ $value['discount_type'] == 1 ? 'selected' : '' }}>BDT</option>
                                                                <option value="0" {{ $value['discount_type'] == 0 ? 'selected' : '' }}>%</option>
                                                            </select>
                                                        </div>

                                                        <div  style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">
                                                            <label class="show-xs" style="padding-bottom: 0px;" for="productname">Discount</label>
                                                            <input type="text" name="discount[]" class="inner form-control" id="discount_{{$key}}" value="{{$value['discount_amount']}}" placeholder="Discount" oninput="calculateActualAmount({{$key}})"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <label style="" class="show-xs" for="productname">Total</label>
                                                    <input type="text" name="amount[]" class="inner form-control amount" id="amount_{{$key}}" value="{{round($value['total_amount'], 2)}}" placeholder="Total"/>
                                                </div>

                                                <div style="margin-bottom: 5px;;display:none!important;" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <label style="display: none" class="show-xs" for="productname">Free Item</label>

                                                    <div class="row">
                                                        <div style="padding-right: 0px" class="col-md-7">
                                                            <select style="width: 100%" name="free_items[]" class="inner form-control select2 freeItems" id="free_items_{{$key}}" onchange="getFreeItemUnit({{$key}})">
                                                                <option value="">--Select Product--</option>
                                                                @if(!empty($product_entries) && ($product_entries->count() > 0))
                                                                @foreach($product_entries as $key1 => $product_entry)
                                                                    <option value="{{ $product_entry['id'] }}" {{ $value['free_product_entry_id'] == $product_entry['id'] ? 'selected' : '' }}>
                                                                        <?php
                                                                            if ($product_entry['product_type'] == 2)
                                                                            {
                                                                                $variants = ' - ' . $product_entry['variations'];
                                                                            }
                                                                            else
                                                                            {
                                                                                $variants = '';
                                                                            } 
                                                                        ?>
                                                                        {{ $product_entry['name'] . $variants . ' ( ' .str_pad($product_entry['product_code'], 6, "0", STR_PAD_LEFT). ' )' }}
                                                                    </option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                        </div>

                                                        <input type="hidden" name="free_item_main_unit_id[]" class="inner form-control" id="free_item_main_unit_id_{{$key}}" value="{{ $value['free_main_unit_id'] }}" />

                                                        <div style="padding-left: 0px" class="col-md-5">
                                                            <select style="width: 100%;cursor: pointer" name="free_unit_id[]" class="inner form-control single_select2" id="free_unit_id_{{$key}}">
                                                            <option value="{{ $value['free_conversion_unit_id'] }}">{{  $value->free_conversion_unit_id != null ? $value->freeConvertedUnit->name : '' }}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px;display:none!important;" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <label style="display: none" class="show-xs" for="productname">Free Qty </label>
                                                    <input type="text" name="free_quantity[]" class="inner form-control freeProductQuantity" id="free_quantity_{{$key}}" value="{{$value['free_quantity']}}" placeholder="Quantity" onchange="freeProductCheckEdit({{$key}})"/>
                                                </div>
                                                
                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <label style="" class="show-xs" for="productname">Action</label>
                                                    <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field" data-val="{{$key}}"></i>
                                                </div>
                                            </div>

                                            <hr style="margin-top: 5px !important;margin-bottom: 5px !important">
                                        @endforeach
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 150px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Sub Total</label>
                                            <div class="col-md-7">
                                                <input type="text" id="subTotalBdt" class="form-control">
                                                <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">VAT</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control" id="vat_type_0" name="vat_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="0" {{ Auth::user()->vat_type == 0 ? 'selected' : '' }}>%</option>
                                                    <option style="padding: 10px" value="1" {{ Auth::user()->vat_type == 1 ? 'selected' : '' }}>BDT</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="vat_amount_0" type="text" class="form-control width-xs" name="vat_amount" value="{{ Auth::user()->vat_amount }}" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Discount</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control totalDiscountType" id="total_discount_type_0" name="total_discount_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="1" selected>BDT</option>
                                                    <option style="padding: 10px" value="0">%</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="total_discount_0" type="text" class="form-control totalDiscount" name="total_discount_amount" value="0" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 150px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Dis. Note</label>
                                            <div class="col-md-7">
                                                <input id="total_discount_note" type="text" class="form-control width-xs" name="total_discount_note" value="{{ old('total_discount_note') }}" placeholder="Discount Note">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Coupon</label>
                                            <div class="col-md-7">
                                                <input id="coupon_code" type="number" class="form-control width-xs couponCode" name="coupon_code" onchange="couponMembership()" placeholder="Coupon/Membership">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Send SMS</label>
                                            <div class="col-md-12">
                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Non Masking
                                                    </label>
                                                </div>

                                                <div style="padding-left: 10px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Masking
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 150px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">INV Amount</label>
                                            <div class="col-md-7">
                                                <input type="text" id="totalBdt" class="form-control">
                                                <input style="display: none" type="text" id="totalBdtShow" name="total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Net Payable</label>
                                            <div class="col-md-7">
                                                <input type="text" id="netPa" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 150px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Cash Given</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="cash_given" name="cash_given" placeholder="Cash Given" value="{{ $find_invoice['cash_given'] }}" oninput="calculateChangeAmount()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Adjustment</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="adjustment" name="adjustment" placeholder="Adjustment" value="0" oninput="calculateChangeAmount()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <div class="col-md-12">
                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="adjustAdvancePayment" name="adjust_cash_given">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="adjustAdvancePayment" onclick="adjustAdvancePayment()">
                                                        Adjust Advance Payment
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <input type="hidden" name="customer_advance_amount" id="customer_advance_amount" value="0">

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Advance</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="change_amount" name="change_amount" placeholder="Advance Amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Acc. Head</label>
                                            <div class="col-md-7">
                                                <select style="width: 100%;cursor: pointer;" id="account_id" name="account_id" class="form-control" required>
                                                    <option value="">--Select Account Head--</option>
                                                    @if($accounts->count() > 0)
                                                    @foreach($accounts as $key=> $account)
                                                    <option value="{{ $account->id }}" {{ $account->id == 4 ? 'selected' : '' }}>{{ $account->account_name }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 140px;padding-top: 13px;padding-right: 0px;padding-left: 0px" class="col-md-12">
                                        <div style="background-color: #D2D2D2;height: 120px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap_current_balance getMultipleRowCurrentBalance">
                                            @if($current_balance->count() > 0)
                                            @foreach($current_balance as $key_current_balance => $current_balance_value)
                                            <div class="row di_current_balance_{{$key_current_balance}}">
                                                <div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    @if($key_current_balance == 0)
                                                        <label class="hidden-xs" for="productname">Date *</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">Date *</label>
                                                    <input id="current_balance_payment_date" name="current_balance_payment_date[]" type="text" value="{{ date('d-m-Y', strtotime($current_balance_value['date'])) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                                </div>

                                                <div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    @if($key_current_balance == 0)
                                                        <label class="hidden-xs" for="productname"> Paid *</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname"> Paid *</label>
                                                    <input type="text" class="form-control" value="{{$current_balance_value['amount']}}" name="current_balance_amount_paid[]" />
                                                </div>

                                                <div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                    @if($key_current_balance == 0)
                                                        <label class="hidden-xs" for="productname">Paid Through *</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">Paid Through *</label>
                                                    <select id="current_balance_paid_through_{{$key_current_balance}}" style="cursor: pointer" name="current_balance_paid_through[]" class="form-control single_select2">
                                                        @if(!empty($paid_accounts) && ($paid_accounts->count() > 0))
                                                        @foreach($paid_accounts as $key => $paid_account)
                                                            <option {{ $paid_account['id'] == $current_balance_value['account_id'] ? 'selected' : '' }} value="{{ $paid_account['id'] }}">{{ $paid_account['account_name'] }}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </div>

                                                <div style="" class="col-lg-2 col-md-3 col-sm-6 col-6">
                                                    @if($key_current_balance == 0)
                                                        <label class="hidden-xs" for="productname">AC Info.</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">Account Info.</label>
                                                    <input type="text" name="current_balance_account_information[]" class="form-control" id="current_balance_account_information_{{$key_current_balance}}" value="{{$current_balance_value['account_information']}}" placeholder="Account Information" />
                                                </div>

                                                <div style="" class="col-lg-2 col-md-3 col-sm-6 col-6">
                                                    @if($key_current_balance == 0)
                                                        <label class="hidden-xs" for="productname">Note</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">Note</label>
                                                    <input type="text" name="current_balance_note[]" class="form-control" id="current_balance_note_{{$key_current_balance}}" value="{{$current_balance_value['note']}}" placeholder="Note" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group">
                                                    @if($key_current_balance == 0)
                                                        <label class="hidden-xs" for="productname">Action</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">Action</label>
                                                    @if($key_current_balance == 0)
                                                    <i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonCurrentBalance()"></i>
                                                    @else
                                                    <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field_current_balance" data-val="{{$key_current_balance}}"></i>
                                                    @endif
                                                </div>
                                            </div>
                                            <input type="hidden" name="current_balance_id[]" value="{{ $current_balance_value['id'] }}">
                                            <input type="hidden" name="current_balance_account_id[]" value="{{ $current_balance_value['account_id'] }}">
                                            @endforeach
                                            @endif

                                            <div style="display: none" class="row justify-content-end">
                                                <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                                    <input id="add_field_button_current_balance" type="button" class="btn btn-success btn-block inner add_field_button_current_balance" value="Add"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>

                                    <div style="display: none;" class="row justify-content-end">
                                        <div class="col-lg-1 col-md-2 form-group">
                                            <input id="pos_add_button" type="button" class="btn btn-success btn-block inner add_field_button_pos" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px !important;padding-bottom: 0px !important" class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Update & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('bills_index') }}">Close</a></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control commonCustomerClass">
                        </div>
                    </div>

                    <?php
                        if (isset($_GET['sales_type']) && ($_GET['sales_type'] == 4))
                        {
                            $c_type = $_GET['sales_type'];
                        }

                        if (isset($_GET['sales_type']) && ($_GET['sales_type'] == 1))
                        {
                            $c_type = 0;
                        }

                        if (!isset($_GET['sales_type']))
                        {
                            $c_type = 0;
                        }
                    ?>

                    <input id="contact_type" name="contact_type" type="hidden" value="{{ $c_type }}" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Reference</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name1" name="customer_name" type="text" class="form-control commonReferenceClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number1" name="mobile_number" type="text" class="form-control commonReferenceClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address1" name="address" type="text" class="form-control commonReferenceClass">
                        </div>
                    </div>

                    <input id="contact_type1" name="contact_type" type="hidden" value="3" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            $('#vertical-menu-btn').click();
            
            var site_url    = $('.site_url').val();
            var type        = (new URL(location.href)).searchParams.get('edit_type');

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (type != null)
                    {   
                        if(type == 1)
                        {
                            var type_show = 0;
                        }
                        else
                        {
                            var type_show = 4;
                        }
                        
                        if (result['contact_type'] == type_show && result['id'] != 0)
                        {
                            return result['text'];
                        }
                    }
                    else
                    {
                        if (result['contact_type'] == 0 && result['id'] != 0)
                        {
                            return result['text'];
                        }
                    }
                },
            });
            
            $("#reference_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 3)
                    {
                        return result['text'];
                    }
                },
            });

            $("#sr_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 4 && result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });

            var customerId      = $("#customer_id").val();

            var sr_id     = $("#sr_id").val();

            if (sr_id == 0)
            {
                $(".productEntries").prop('disabled', true);
                $(".freeItems").prop('disabled', true);
            }
            else
            {
                $(".productEntries").prop('disabled', false);
                $(".freeItems").prop('disabled', false);
            }

            $('#add_field_button_current_balance').click();
            
            setTimeout(
              function() 
              {
                calculateActualAmount();
              }, 3000);
        });
    </script>

    <script type="text/javascript">
        function PosScannerSearch()
        {
            $("#alertMessage").hide();

            var site_url      = $(".site_url").val();
            var read_code     = $("#product_code").val();

            if (read_code.length == 6)
            {
                var product_code  = read_code.replace(/^0+/, '');
            }
            else
            {
                var product_code  = '0';
            }

            if(product_code != 0)
            {
                $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code, function(data_pos){

                    if ($.isEmptyObject(data_pos))
                    {   
                        $('input[name=product_code').val('');
                        $("#alertMessage").show();
                    }
                    else
                    {
                        var state = 0;

                        $('.productEntries').each(function()
                        {
                            var entry_id    = $(this).val();
                            var value_x     = $(this).prop("id");

                            if (data_pos.id == entry_id)
                            {
                                var explode          = value_x.split('_');
                                var quantity_id      = explode[2];
                                var quantity         = $('#quantity_'+quantity_id).val();
                                var inc_quantity     = parseFloat(quantity) + 1;

                                $('#quantity_'+quantity_id).val(inc_quantity);
                                $('#quantity_'+quantity_id).change();
                                $('input[name=product_code').val('');
                                calculateActualAmount(quantity_id);

                                state++;
                            }

                            if (entry_id == '')
                            {   
                                var explode          = value_x.split('_');
                                var quantity_id      = explode[2];
                                
                                ProductEntriesList(quantity_id);

                                state++;
                            }
                        });

                        if (state == 0)
                        {   
                            $("#pos_add_button").click();
                        }
                        else
                        {
                            return false;
                        }
                    }

                });
            } 
        }

        function ProductEntriesList(x,entry_id) 
        {
            //For getting item commission information from items table start
            var site_url  = $(".site_url").val();
            
            $.get(site_url + '/bills/product/entries/category-list/invoice/'+ entry_id, function(data){

                var list5           = '';
                var list7           = '';
                var free_item_list  = '';
                var free_list       = '';
                var checkProduct    = [];

                $.each(data, function(i, data)
                {
                    if (data.stock_in_hand != null)
                    {
                        var stock = data.stock_in_hand;
                    }
                    else
                    {
                        var stock = 0;
                    }

                    if (data.product_type == 2)
                    {
                        var productName = data.name + ' - ' + data.variations;
                    }
                    else
                    {
                        var productName = data.name;
                    }

                    // $('.productEntries').each(function()
                    // {
                    //     var entry_id    = $(this).val();

                    //     if(checkValue(entry_id, checkProduct) == 'Not exist')
                    //     {
                    //         checkProduct.push(entry_id);
                    //     }       
                    // });

                    // if(checkValue(data.id, checkProduct) == 'Not exist')
                    // {

                        list5 += '<option value = "' +  data.id + '">' + productName + '</option>';
                    // }

                    free_item_list += '<option value = "' +  data.id + '">' + productName + '</option>';
                });

                list7           += '<option value = "">' + '--Select Product--' +'</option>';
                free_list       += '<option value = "">' + '--Select Product--' +'</option>';

                $("#product_entries_"+x).empty();
                $("#product_entries_"+x).append(list7);
                $("#product_entries_"+x).append(list5);

                $("#free_items_"+x).empty();
                $("#free_items_"+x).append(free_list);
                $("#free_items_"+x).append(free_item_list);

                var read_code     = $("#product_code").val();

                if (read_code)
                {
                    var product_code  = read_code.replace(/^0+/, '');
                }
                else
                {
                    var product_code  = '';
                }

                if(product_code)
                {
                    $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code, function(data_pos){

                        $('#product_entries_'+x).val(data_pos.id).change();
                    });

                    $('input[name=product_code').val('');
                } 
            });
        }

        function freeProductCheck(x)
        {
            var total       = '';
            var stock_qty           = $('.stockQuantity').map((_,el) => el.value).get();
            var main_products       = $('.productEntries').map((_,el) => el.value).get();
            var main_products_qty   = $('.mainProductQuantity').map((_,el) => el.value).get();
            var free_products       = $('.freeItems').map((_,el) => el.value).get();
            var free_products_qty   = $('.freeProductQuantity').map((_,el) => el.value).get();

            var free_product_id     = $('#free_items_'+x).val();
            var sr_id               = $('#sr_id').val();

            var total_main_quantity     = 0;
            var total_free_quantity     = 0;

            for (var i = 0; i < main_products.length; i++)
            {
                if (free_product_id == main_products[i])
                {  
                    total_main_quantity += parseFloat(main_products_qty[i]);

                    var stockQty    = stock_qty[i];
                }

                if (free_product_id == free_products[i])
                {  
                    total_free_quantity += parseFloat(free_products_qty[i]);
                }
            }

            var total       = parseFloat(total_main_quantity) + parseFloat(total_free_quantity);

            var site_url    = $('.site_url').val();




            // if (free_product_id != undefined)
            if (free_product_id != '')
            {
                $.get(site_url+'/invoices/return-sr-product-stock/' + sr_id + '/' + free_product_id, function(data){

                    var stockQtys   = parseFloat(data.receive_quantity) - parseFloat(data.sold_quantity) - parseFloat(data.return_quantity) - parseFloat(total);

                    if (parseFloat(stockQtys) < 0)
                    {   
                        $("#free_quantity_"+x).val(0);
                        $('#free_item_stock_'+x).val('');
                        $('#free_item_stock_show_'+x).html('Stock : ' + '');
                        $('#free_item_stock_'+x).val(parseFloat(data.receive_quantity) - parseFloat(data.sold_quantity));
                        $('#free_item_stock_show_'+x).html('Stock : ' + (parseFloat(data.receive_quantity) - parseFloat(data.sold_quantity)));
                    }
                    else
                    {
                        if (parseFloat(stockQtys) != NaN)
                        {
                            $('#free_item_stock_show_'+x).html(0);
                            $('#free_item_stock_show_'+x).html('Stock : ' + stockQtys);
                            $('#free_item_stock_'+x).val(0);
                            $('#free_item_stock_'+x).val(stockQtys);
                        }
                        else
                        {
                            $('#free_item_stock_show_'+x).html(0);
                            $('#free_item_stock_show_'+x).html('Stock : 0');
                            $('#free_item_stock_'+x).val(0);
                            $('#free_item_stock_'+x).val(0);
                        }
                    }
                    
                });
            }
        }

        function checkValue(value,arr)
        {
            var status = 'Not exist';
             
            for(var i=0; i<arr.length; i++)
            {
                var name = arr[i];
                if(name == value)
                {
                    status = 'Exist';
                    break;
                }
            }

            return status;
        }

        function getItemPrice(x)
        {
            //For getting item commission information from items table start
            var entry_id  = $("#product_entries_"+x).val();
            var site_url  = $(".site_url").val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    var list    = '';
                    $.each(data.unit_conversions, function(i, data_list)
                    {   
                        list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                    });

                    $("#unit_id_"+x).empty();
                    $("#unit_id_"+x).append(list);

                    if (data.product_entries.stock_in_hand == '' || data.product_entries.stock_in_hand == null)
                    {
                        var stockInHand  = 0;
                    }
                    else
                    {
                        var stockInHand  = data.product_entries.stock_in_hand;
                    }

                    if (data.product_entries.unit_id == null)
                    {
                        var unit  = '';
                    }
                    else
                    {
                        var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                    }

                    $("#rate_"+x).val(parseFloat(data.product_entries.buy_price).toFixed(2));
                    $("#quantity_"+x).val(1);
                    $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
                    $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' ' + unit);
                    $("#main_unit_id_"+x).val(data.product_entries.unit_id);
                    $("#main_unit_name_"+x).val(data.product_entries.unit_name);
                    $("#discount_"+x).val(0);
                    // $("#unit_id_"+x).val(data.product_entries.unit_id).trigger('change');
      
                    calculateActualAmount(x);
                });
            }
            // calculateActualAmount(x);
            //For getting item commission information from items table end
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                    $("#stock_show_"+x).html('Stock : ' + convertedStock + ' ( ' + convertedUnitName + ' )');
                    $("#rate_"+x).val(parseFloat(data.sell_price).toFixed(2));

                    calculateActualAmount(x);
                }

            });
        }

        function getFreeConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#free_items_"+x).val();
            var unit_id             = $("#free_unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    var site_url  = $(".site_url").val();
                    var entry_id  = $("#free_items_"+x).val();
                    var sr_id     = $("#sr_id").val();

                    if(entry_id)
                    {
                        $.get(site_url + '/invoices/products-price-list/'+ entry_id + '/' + sr_id, function(data){

                            if (jQuery.isEmptyObject(data) == false)
                            {   
                                if (data.product_entries != null)
                                {
                                    if (data.product_entries.stock_in_hand == 0 || data.product_entries.stock_in_hand == '')
                                    {
                                        var stockInHand  = 0;
                                    }
                                    else
                                    {
                                        var stockInHand  = data.product_entries.stock_in_hand;
                                    }

                                    if (data.product_entries.unit_id == null)
                                    {
                                        var unit  = '';
                                    }
                                    else
                                    {
                                        var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                                    }
                                }
                                else
                                {
                                     var stockInHand    = 0;
                                     var unit           = '';
                                }
                                
                                $("#free_item_stock_"+x).val(stockInHand);
                                $("#free_item_stock_show_"+x).html('Stock : ' + stockInHand + unit);
                  
                                // calculateActualAmount(x);
                            }
                            else
                            {
                                $("#free_item_stock_"+x).val(0);
                                $("#free_item_stock_show_"+x).html('Stock : 0');
                  
                                // calculateActualAmount(x);
                            }
                            
                        });
                    }
                    // calculateActualAmount(x);
                }
                else
                {   
                    var convertedUnitName = $('#free_unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#free_item_stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                    $("#free_item_stock_"+x).val(convertedStock);
                    $("#free_item_stock_show_"+x).html('Stock : ' + convertedStock + ' ( ' + convertedUnitName + ' )');
                    // calculateActualAmount(x);
                }

            });
        }

        function getFreeItemUnit(x)
        {
            //For getting item commission information from items table start
            var entry_id  = $("#free_items_"+x).val();
            var site_url  = $(".site_url").val();
            var sr_id     = $("#sr_id").val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products-price-list/'+ entry_id + '/' + sr_id, function(data){

                    var list    = '';
                    $.each(data.unit_conversions, function(i, data_list)
                    {   
                        list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                    });

                    if (data.product_entries.stock_in_hand == 0)
                    {
                        var stockInHand  = 0;
                    }
                    else
                    {
                        var stockInHand  = data.product_entries.stock_in_hand;
                    }

                    if (data.product_entries.unit_id == null)
                    {
                        var unit  = '';
                    }
                    else
                    {
                        var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                    }

                    $("#free_item_stock_"+x).val(stockInHand);
                    $("#free_item_stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' ' + unit);
                    $("#free_unit_id_"+x).empty();
                    $("#free_unit_id_"+x).append(list);
                    $("#free_item_main_unit_id_"+x).val(data.product_entries.unit_id);
                });
            }
            //For getting item commission information from items table end
        }

        function pad (str, max)
        {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        function getCategoryList(x)
        {
            var site_url  = $('.site_url').val();
            var group_id  = $('#group_id_'+x).val();

            $("#product_category_id_"+x).select2({
                ajax: { 
                    url:  site_url + '/groups/category-list-ajax/' + group_id,
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });
        }
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = $("#contact_type").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                            $('.commonCustomerClass').val('');
                        }
                        
                        $("#customer_id").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });

        $('#submitBtn1').click(function() {
            
            var customer_name               = $("#customer_name1").val();
            var address                     = $("#address1").val();
            var mobile_number               = $("#mobile_number1").val();
            var contact_type                = $("#contact_type1").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton1').click();
                            $('.commonReferenceClass').val('');
                        }
                        
                        $("#reference_id").empty();
                        $('#reference_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });    
    </script>

    <script type="text/javascript">
        var max_fields       = 500;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = {{$entries_count}};
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var group_label         = '<label class="hidden-xs" for="productname">Group *</label>\n';
                    var brand_label         = '<label class="hidden-xs" for="productname">Brand *</label>\n';
                    var category_label      = '<label class="hidden-xs" for="productname">Category *</label>\n';
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var free_product_label  = '<label class="hidden-xs" for="productname">Free Item</label>\n';
                    var unit_label          = '<label class="hidden-xs" for="productname">Unit</label>\n';
                    var stock_label         = '<label class="hidden-xs" for="productname">Stock</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                    var free_item_label     = '<label class="hidden-xs" for="productname">Free Qty</label>\n';
                    var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                    var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">Discount</label>\n';
                    var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }
                else
                {
                    var group_label         = '';
                    var brand_label         = '';
                    var category_label      = '';
                    var product_label       = '';
                    var free_product_label  = '';
                    var unit_label          = '';
                    var stock_label         = '';
                    var rate_label          = '';
                    var quantity_label      = '';
                    var free_item_label     = '';
                    var discount_label      = '';
                    var type_label          = '';
                    var amount_label        = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Group *</label>\n' +
                                                        group_label +
                                                        '<select style="width: 100%" class="inner form-control single_select2 groupId" id="group_id_'+x+'" required>\n' +
                                                            '<option value="">' + '--Select Group--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Brand *</label>\n' +
                                                        brand_label +
                                                        '<select style="width: 100%" class="inner form-control single_select2" id="brand_id_'+x+'" onchange="getCategoryList('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Brand--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Category *</label>\n' +
                                                        category_label +
                                                        '<select style="width: 100%" class="inner form-control single_select2" id="product_category_id_'+x+'" onchange="getProductList('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Category--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +
                                                    
                                                    '<div style="margin-bottom: 5px" class="col-lg-4 col-md-4 col-sm-6 col-12">\n' +
                                                        '<label style="display: none;width: 100%" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                        '<option value="">' + '--Select Product--' + '</option>' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +

                                                    '<div style="padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                        unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control" id="unit_id_'+x+'" required onchange="getConversionParam('+x+')">\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 10px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control mainProductQuantity" id="quantity_'+x+'" placeholder="Quantity" onchange="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-12 col-12">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n' +
                                                                type_label +
                                                                '<select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_'+x+'" oninput="calculateActualAmount('+x+')">\n' +
                                                                    '<option value="1">BDT</option>' +
                                                                    '<option value="0" selected>%</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" name="discount[]" class="inner form-control" id="discount_'+x+'" placeholder="Discount" oninput="calculateActualAmount('+x+')"/>\n' +
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                        amount_label +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display:none!important;" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Free Item</label>\n' +
                                                        free_product_label +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 1px" class="col-md-7">' +
                                                                '<select style="width: 100%" name="free_items[]" class="inner form-control single_select2 freeItems" id="free_items_'+x+'" onchange="getFreeItemUnit('+x+')">\n' +
                                                                '<option value="">' + '--Select Product--' + '</option>' +
                                                                '</select>\n' +
                                                                '<p id="free_item_stock_show_'+x+'" style="padding: 0px;margin: 0px">' + 'Stock : 0' + '</p>' +
                                                            '</div>' +

                                                            '<div style="padding-left: 1px" class="col-md-5">' +
                                                                '<select style="width: 100%;cursor: pointer" name="free_unit_id[]" class="inner form-control single_select2" id="free_unit_id_'+x+'" onchange="getFreeConversionParam('+x+')">\n' +
                                                                '</select>\n' +
                                                            '</div>' + 
                                                        '</div>' +
                                                    '</div>\n' + 

                                                    '<input type="hidden" id="free_item_stock_'+x+'">' +
                                                    '<input type="hidden" name="free_item_main_unit_id[]" class="inner form-control" id="free_item_main_unit_id_'+x+'" />\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display:none!important;" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Free Qty *</label>\n' +
                                                        free_item_label  +
                                                        '<input type="text" name="free_quantity[]" class="inner form-control freeProductQuantity" id="free_quantity_'+x+'" value="0" />\n' +
                                                    '</div>\n' +   
                                                    
                                                    add_btn +

                                                '</div>\n' +

                                                '<hr style="margin-top: 5px !important;margin-bottom: 5px !important">'
                                            );

                                            $('.single_select2').select2();

                var site_url    = $(".site_url").val();
                $("#group_id_"+x).select2({
                    ajax: { 
                    url:  site_url + '/groups/group-list-ajax',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                    },
                });

                $("#brand_id_"+x).select2({
                    ajax: { 
                        url:  site_url + '/groups/brand-list-ajax',
                        type: "get",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                searchTerm: params.term // search term
                            };
                        },
                        processResults: function (response) {
                            return {
                                results: response
                            };
                        },
                        cache: true
                    }
                });
            }                                    
        });
        //For apending another rows end

        //For apending another rows start
        $(add_button_pos).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var group_label         = '<label class="hidden-xs" for="productname">Group *</label>\n';
                    var brand_label         = '<label class="hidden-xs" for="productname">Brand *</label>\n';
                    var category_label      = '<label class="hidden-xs" for="productname">Category *</label>\n';
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var free_product_label  = '<label class="hidden-xs" for="productname">Free Item</label>\n';
                    var unit_label          = '<label class="hidden-xs" for="productname">Unit</label>\n';
                    var stock_label         = '<label class="hidden-xs" for="productname">Stock</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                    var free_item_label     = '<label class="hidden-xs" for="productname">Free Qty</label>\n';
                    var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                    var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">Discount</label>\n';
                    var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }
                else
                {
                    var group_label         = '';
                    var brand_label         = '';
                    var category_label      = '';
                    var product_label       = '';
                    var free_product_label  = '';
                    var unit_label          = '';
                    var stock_label         = '';
                    var rate_label          = '';
                    var quantity_label      = '';
                    var free_item_label     = '';
                    var discount_label      = '';
                    var type_label          = '';
                    var amount_label        = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Group *</label>\n' +
                                                        group_label +
                                                        '<select style="width: 100%" class="inner form-control single_select2 groupId" id="group_id_'+x+'" required>\n' +
                                                            '<option value="">' + '--Select Group--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Brand *</label>\n' +
                                                        brand_label +
                                                        '<select style="width: 100%" class="inner form-control single_select2" id="brand_id_'+x+'" onchange="getCategoryList('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Brand--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Category *</label>\n' +
                                                        category_label +
                                                        '<select style="width: 100%" class="inner form-control single_select2" id="product_category_id_'+x+'" onchange="getProductList('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Category--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +
                                                    
                                                    '<div style="margin-bottom: 5px" class="col-lg-4 col-md-4 col-sm-6 col-12">\n' +
                                                        '<label style="display: none;width: 100%" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                        '<option value="">' + '--Select Product--' + '</option>' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +

                                                    '<div style="padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                        unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control" id="unit_id_'+x+'" required onchange="getConversionParam('+x+')">\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 10px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control mainProductQuantity" id="quantity_'+x+'" placeholder="Quantity" onchange="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-12 col-12">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n' +
                                                                type_label +
                                                                '<select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_'+x+'" oninput="calculateActualAmount('+x+')">\n' +
                                                                    '<option value="1">BDT</option>' +
                                                                    '<option value="0" selected>%</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" name="discount[]" class="inner form-control" id="discount_'+x+'" placeholder="Discount" oninput="calculateActualAmount('+x+')"/>\n' +
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                        amount_label +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display:none!important;" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Free Item</label>\n' +
                                                        free_product_label +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 1px" class="col-md-7">' +
                                                                '<select style="width: 100%" name="free_items[]" class="inner form-control single_select2 freeItems" id="free_items_'+x+'" onchange="getFreeItemUnit('+x+')">\n' +
                                                                '<option value="">' + '--Select Product--' + '</option>' +
                                                                '</select>\n' +
                                                                '<p id="free_item_stock_show_'+x+'" style="padding: 0px;margin: 0px">' + 'Stock : 0' + '</p>' +
                                                            '</div>' +

                                                            '<div style="padding-left: 1px" class="col-md-5">' +
                                                                '<select style="width: 100%;cursor: pointer" name="free_unit_id[]" class="inner form-control single_select2" id="free_unit_id_'+x+'" onchange="getFreeConversionParam('+x+')">\n' +
                                                                '</select>\n' +
                                                            '</div>' + 
                                                        '</div>' +
                                                    '</div>\n' + 

                                                    '<input type="hidden" id="free_item_stock_'+x+'">' +
                                                    '<input type="hidden" name="free_item_main_unit_id[]" class="inner form-control" id="free_item_main_unit_id_'+x+'" />\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display:none!important;" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Free Qty *</label>\n' +
                                                        free_item_label  +
                                                        '<input type="text" name="free_quantity[]" class="inner form-control freeProductQuantity" id="free_quantity_'+x+'" value="0" />\n' +
                                                    '</div>\n' +   
                                                    
                                                    add_btn +

                                                '</div>\n' +

                                                '<hr style="margin-top: 5px !important;margin-bottom: 5px !important">'
                                            );

                                            $('.single_select2').select2();

                var site_url    = $(".site_url").val();
                $("#group_id_"+x).select2({
                    ajax: { 
                    url:  site_url + '/groups/group-list-ajax',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                    },
                });

                $("#brand_id_"+x).select2({
                    ajax: { 
                        url:  site_url + '/groups/brand-list-ajax',
                        type: "get",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                searchTerm: params.term // search term
                            };
                        },
                        processResults: function (response) {
                            return {
                                results: response
                            };
                        },
                        cache: true
                    }
                });
            }                                    
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            var rate                    = $("#rate_"+x).val();
            var quantity                = $("#quantity_"+x).val();
            var stock                   = $("#stock_"+x).val();
            var discount                = $("#discount_"+x).val();
            var discountType            = $("#discount_type_"+x).val();
            var vatType                 = $("#vat_type_0").val();
            var vatAmount               = $("#vat_amount_0").val();
            var taxType                 = $("#tax_type_0").val();
            var taxAmount               = $("#tax_amount_0").val();
            var totalDiscount           = $("#total_discount_0").val();
            var totalDiscountType       = $("#total_discount_type_0").val();

            //Checking Overselling Start
            // var stock_qty           = $('.stockQuantity').map((_,el) => el.value).get();
            // var main_products       = $('.productEntries').map((_,el) => el.value).get();
            // var main_products_qty   = $('.mainProductQuantity').map((_,el) => el.value).get();
            // var free_products       = $('.freeItems').map((_,el) => el.value).get();
            // var free_products_qty   = $('.freeProductQuantity').map((_,el) => el.value).get();

            // var main_product_id     = $('#product_entries_'+x).val();
            // var total_main_quantity = 0;
            // var total_free_quantity = 0;

            // for (var i = 0; i < main_products.length; i++)
            // {
            //     if (main_product_id == main_products[i])
            //     {  
            //         total_main_quantity += parseFloat(main_products_qty[i]);
            //     }

            //     if (main_product_id == free_products[i])
            //     {  
            //         total_free_quantity += parseFloat(free_products_qty[i]);
            //     }
            // }

            // var total = parseFloat(stock) - parseFloat(total_main_quantity) - parseFloat(total_free_quantity);

            // if (parseFloat(total) >= 0)
            // {   
            //     $("#quantity_"+x).val(quantity);
            // }
            // else
            // {
            //     $("#quantity_"+x).val('');
            // }
            //Checking Overselling End

            if (rate == '')
            {
                var rateCal             = 0;
            }
            else
            {
                var rateCal             = $("#rate_"+x).val();
            }

            if (quantity == '')
            {
                var quantityCal         = 1;
            }
            else
            {
                var quantityCal         = $("#quantity_"+x).val();
            }

            if (discount == '')
            {
                var discountCal         = 1;
            }
            else
            {
                var discountCal         = $("#discount_"+x).val();
            }

            if (discount == '')
            {
                var discountTypeCal     = 0;
            }
            else
            {
                if (discountType == 0)
                {
                    var discountTypeCal     = (parseFloat(discountCal)*parseFloat(rateCal)*parseFloat(quantityCal))/100;
                }
                else
                {
                    var discountTypeCal     = $("#discount_"+x).val();
                }
            }

            var AmountIn              =  (parseFloat(rateCal)*parseFloat(quantityCal)) - parseFloat(discountTypeCal);
     
            $("#amount_"+x).val(AmountIn);

            //Calculating Subtotal Amount
            var total       = 0;

            $('.amount').each(function()
            {
                total       += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(total.toFixed());
            $("#subTotalBdtShow").val(total.toFixed());

            if (vatAmount == '')
            {   
                $("#vat_amount_0").val(0);
                var vatCal         = 0;
            }
            else
            {
                var vatCal         = $("#vat_amount_0").val();
            }

            if (vatType == 0)
            {
                var vatTypeCal     = (parseFloat(vatCal)*(parseFloat(total)))/100;
            }
            else
            {
                var vatTypeCal     = $("#vat_amount_0").val();
            }

            if (taxAmount == '')
            {   
                $("#tax_amount_0").val(0);
                var taxCal         = 0;
            }
            else
            {
                var taxCal         = $("#tax_amount_0").val();
            }

            if (taxType == 0)
            {
                var taxTypeCal     = (parseFloat(taxCal)*(parseFloat(total)))/100;
            }
            else
            {
                var taxTypeCal     = $("#tax_amount_0").val();
            }

            if (totalDiscount > 0)
            {
                if (totalDiscountType == 0)
                {
                    var totalDiscountTypeCal     = (parseFloat(totalDiscount)*(parseFloat(total) + parseFloat(vatTypeCal)))/100;
                }
                else
                {
                    var totalDiscountTypeCal     = $("#total_discount_0").val();
                }
            }
            else
            {
                var totalDiscountTypeCal     = 0;
            }

            var totalShow = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountTypeCal);

            $("#totalBdtShow").val(totalShow.toFixed());
            $("#totalBdt").val(parseFloat(totalShow).toFixed());

            var preBal      = $("#balance").val();
            var preBalType  = $("#bbBalance").val();

            if (preBalType == 1)
            {
                var netPaShowVal   = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountTypeCal) + parseFloat(preBal);
            }

            if (preBalType == 2)
            {
                var netPaShowVal   = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountTypeCal) - parseFloat(preBal);
            }

            $("#netPa").val(parseFloat(netPaShowVal).toFixed());

            calculateChangeAmount();
            freeProductCheck(x)

            var type = $("#bbBalance").val();

            if (type == 2)
            {
                var site_url        = $('.site_url').val();
                var customerId      = $("#customer_id").val();

                $.get(site_url + '/invoices/adjust-advance-payment/' + customerId, function(data){

                    var payable  = $("#totalBdtShow").val();

                    if (parseFloat(payable) <= parseFloat(data))
                    {
                        $("#adjustment").val(parseFloat(payable).toFixed());
                    }
                    else
                    {
                        $("#adjustment").val(parseFloat(data).toFixed());
                    }

                    $("#customer_advance_amount").val(parseFloat(data).toFixed());
                    calculateChangeAmount();
                });
            }
        }
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }

        function addButtonCurrentBalance()
        {
            $('.add_field_button_current_balance').click();
        }
    </script>

    <script type="text/javascript">
        var max_fields_current_balance = 50;                           //maximum input boxes allowed
        var wrapper_current_balance    = $(".input_fields_wrap_current_balance");      //Fields wrapper
        var add_button_current_balance = $(".add_field_button_current_balance");       //Add button ID
        var index_no_current_balance   = 1;

        //For apending another rows start
        var c = {{$current_balance_count > 0 ? $current_balance_count : -1}};
        $(add_button_current_balance).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(c < max_fields_current_balance)
            {   
                c++;

                var serial = c + 1;

                if (serial == 1)
                {
                    var amount_paid_label   = '<label class="hidden-xs" for="productname">Amount Paid *</label>\n';
                    var date_label          = '<label class="hidden-xs" for="productname">Date *</label>\n';
                    var paid_through_label  = '<label class="hidden-xs" for="productname">Paid Through *</label>\n';
                    var account_info_label  = '<label class="hidden-xs" for="productname">Account Info.</label>\n';
                    var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button_current_balance">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonCurrentBalance()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var amount_paid_label   = '';
                    var date_label          = '';
                    var paid_through_label  = '';
                    var account_info_label  = '';
                    var note_label          = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field_current_balance" data-val="'+c+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRowCurrentBalance').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_current_balance_'+c+'">' +
                                                    '<input type="hidden" name="current_balance_id[]" value="-1">' +
                                                    '<input type="hidden" name="current_balance_account_id[]" value="-1">' +
                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Date *</label>\n' +
                                                        date_label +
                                                        '<input id="payment_date" name="current_balance_payment_date[]" type="text" value="{{ date("d-m-Y") }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Amount Paid *</label>\n' +
                                                        amount_paid_label +
                                                        '<input type="text" name="current_balance_amount_paid[]" class="form-control currentBalancePaidAmount" id="current_balance_amount_paid_'+c+'" value="0" oninput="checkCurrentBalance('+c+')" required />\n' + 
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid Through *</label>\n' +
                                                        paid_through_label  +
                                                        '<select id="current_balance_paid_through_'+c+'" style="cursor: pointer" name="current_balance_paid_through[]" class="form-control single_select2">\n' +
                                                            '@if(!empty($paid_accounts) && ($paid_accounts->count() > 0))\n' +
                                                            '@foreach($paid_accounts as $key => $paid_account)\n' +
                                                                '<option value="{{ $paid_account['id'] }}">{{ $paid_account['account_name'] }}</option>\n' +
                                                            '@endforeach\n' +
                                                            '@endif\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Account Info.</label>\n' +
                                                        account_info_label +
                                                        '<input type="text" name="current_balance_account_information[]" class="form-control" id="current_balance_account_information_'+c+'" placeholder="Account Information"/>\n' + 
                                                    '</div>\n' + 

                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                        note_label +
                                                        '<input type="text" name="current_balance_note[]" class="form-control" id="current_balance_note_'+c+'" placeholder="note"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +

                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }                                    
        });
        //For apending another rows end

        $(wrapper_current_balance).on("click",".remove_field_current_balance", function(e)
        {
            e.preventDefault();

            var c = $(this).attr("data-val");

            $('.di_current_balance_'+c).remove(); c--;

            calculateActualAmount(c);
        });
    </script>

    <script type="text/javascript">
        $(document).on("change", "#defaultCheck2" , function() {

            var checkbox_value    = $("#defaultCheck2")[0].checked;

            if (checkbox_value == true)
            {
                $("#vatShow").show();
                $("#taxShow").show();
            }
            else
            {
                $("#vatShow").hide();
                $("#taxShow").hide();
            }
        });

        function couponMembership()
        {
            var site_url      = $(".site_url").val();
            var coupon_code   = $("#coupon_code").val();
    
            $('.DiscountType').val(1);
            $('.DiscountAmount').val(0);

            $.get(site_url + '/invoices/search/coupon-code/'+ coupon_code, function(data_coupon){

                if (data_coupon == '')
                {
                    alert('Invalid Coupon Code or Membership Card !!');
                    $('#coupon_code').val('');
                }
                else
                {
                    var state = 0;

                    $('.productEntries').each(function()
                    {
                        var entry_id    = $(this).val();
                        var value_x     = $(this).prop("id");
                        
                        if (data_coupon[0].product_id != null)
                        {
                            for (var i = data_coupon.length - 1; i >= 0; i--)
                            {   
                                if (data_coupon[i].product_id == entry_id)
                                {
                                    var explode    = value_x.split('_');
                                    var di_id      = explode[2];

                                    $('#discount_type_'+di_id).val(data_coupon[i].discount_type);
                                    $('#discount_'+di_id).val(data_coupon[i].discount_amount);
                                    
                                    calculateActualAmount(di_id);

                                    state++;
                                }
                            }
                        }
                        else
                        {
                            var explode    = value_x.split('_');
                            var di_id      = explode[2];

                            $('#discount_type_'+di_id).val(data_coupon[0].discount_type);
                            $('#discount_'+di_id).val(data_coupon[0].discount_amount);
                            
                            calculateActualAmount(di_id);
                        }    
                    });
                }
            });
        }

        function calculateChangeAmount()
        {
            var tAmount             = $("#totalBdtShow").val();
            var cGiven              = $("#cash_given").val();

            if (tAmount != '')
            {
                var totalAmount     = $("#totalBdtShow").val();
            }
            else
            {
                var totalAmount     = 0;
            }

            if (cGiven != '')
            {
                var cashGiven          = $("#cash_given").val();
            }
            else
            {
                var cashGiven          = 0;
            }

            var changeAmount    = parseFloat(cashGiven) - parseFloat(totalAmount);

            if (changeAmount > 0)
            {   
                $("#change_amount").val(0);
                $("#change_amount").val(parseFloat(changeAmount));
                $("#amount_paid_0").val(parseFloat(cashGiven) - parseFloat(changeAmount));
            }

            if (parseFloat(changeAmount) < 0)
            {   
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(cashGiven));
            }

            if (parseFloat(changeAmount) == 0)
            {   
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(cashGiven));
            }
        }

        $(document).on("change", "#customer_id" , function() {

            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            $.get(site_url + '/invoices/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data.toFixed());
                
            });
        });

        $(document).on("change", "#sr_id" , function() {

            var sr_id     = $("#sr_id").val();

            if (sr_id == 0)
            {
                $(".productEntries").prop('disabled', true);
                $(".freeItems").prop('disabled', true);
            }
            else
            {
                $(".productEntries").prop('disabled', false);
                $(".freeItems").prop('disabled', false);
            }
        });
    </script>
@endsection