<?php $__env->startSection('title', 'Transaction History'); ?>

<?php $__env->startSection('styles'); ?>
    <style type="text/css">
        @media  print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page  {
                margin: 0cm ! important;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Software Billing</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Pay Bill</a></li>
                                    <li class="breadcrumb-item active">Transaction History</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;width: 5%">SL</th>
                                            <th style="text-align: center;width: 10%">Date</th>
                                            <th style="text-align: center;width: 30%">Particular</th>
                                            <th style="text-align: center;width: 10%">Paid Through</th>
                                            <th style="text-align: center;width: 15%">Transaction#</th>
                                            <th style="text-align: center;width: 10%">Payable</th>
                                            <th style="text-align: center;width: 10%">Paid</th>
                                            <th style="text-align: center;width: 10%">Balance</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $balance = 0; ?>
                                        <?php if(count($data) > 0): ?>
                                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                            if($value->transaction_head == 'payment-receive')
                                            {
                                                $particular = $value->note;
                                                $payable    = 0;
                                                $paid       = $value->amount;
                                                $balance    = $balance - $paid;
                                            }
                                            elseif($value->transaction_head == 'one-time-payment' || $value->transaction_head == 'service-charge' || $value->transaction_head == 'sms-purchase')
                                            {
                                                $particular = $value->note;
                                                $payable    = $value->amount;
                                                $paid       = 0;
                                                $balance    = $balance + $payable;
                                            }
                                        ?>
                                        <tr>
                                            <td><?php echo e($key + 1); ?></td>
                                            <td><?php echo e(date('d-m-Y', strtotime($value->date))); ?></td>
                                            <td><?php echo e($particular); ?></td>
                                            <td><?php echo e($value->account_name); ?></td>
                                            <td><?php echo e($value->transaction_number); ?></td>
                                            <td style="text-align: right"><?php echo e(number_format($payable,0,'.',',')); ?></td>
                                            <td style="text-align: right"><?php echo e(number_format($paid,0,'.',',')); ?></td>
                                            <td style="text-align: right"><?php echo e(number_format($balance,0,'.',',')); ?></td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                        
                                        <tr>
                                            <td style="text-align: right" colspan="7"><strong>Total <?php if($balance > 0): ?> Due <?php elseif($balance < 0): ?> Advance <?php else: ?> Balance <?php endif; ?></strong></td>
                                            <td style="text-align: right"><strong><?php echo e(number_format($balance,0,'.',',')); ?></strong></td>
                                        </tr>
                                    </tbody>
                                    
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/kawsar/restaurant/Modules/SoftwareBilling/Resources/views/index.blade.php ENDPATH**/ ?>