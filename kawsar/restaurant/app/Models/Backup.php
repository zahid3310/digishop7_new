<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Backup extends Model
{  
    protected $table = "backup";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

}
