

<?php $__env->startSection('title', 'Collection Report'); ?>

<?php $__env->startSection('styles'); ?>
    <style type="text/css">
        @media  print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page  {
                margin: 0cm ! important;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Collection Report</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Collection Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['address']); ?></p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['contact_number']); ?></p>
                                        <h4 style="line-height: 10px;text-align: center">Collection Report</h4>
                                        <p style="line-height: 20px;text-align: center"><strong>From</strong> <?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?> <strong>To</strong> <?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="<?php echo e(route('collection_report_index')); ?>" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div style="margin-bottom: 10px" class="col-lg-5 col-md-5 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="<?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="<?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        <div style="display: none" class="col-lg-5 col-md-5 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100%;cursor: pointer" class="form-control" name="type_id">
                                                    <option <?php echo e(($type_id == 1 || $type_id == 0) ? 'selected' : ''); ?> value="1">Date Wise</option>
                                                    <option <?php echo e(($type_id == 2) ? 'selected' : ''); ?> value="2">Month Wise</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%">SL</th>
                                            <th style="width: 13%">Date</th>
                                            <th style="text-align: center;width: 7%">Sales</th>
                                            <th style="text-align: center;width: 7%">Return</th>
                                            <th style="text-align: center;width: 7%">Net Sales</th>
                                            <th style="text-align: center;width: 7%">Income</th>
                                            <th style="text-align: center;width: 7%">Net Income</th>
                                            <th style="text-align: center;width: 7%">Purchase Cost</th>
                                            <th style="text-align: center;width: 7%">Return Purchase Cost</th>
                                            <th style="text-align: center;width: 7%">Net Purchase Cost</th>
                                            <th style="text-align: center;width: 7%">Expense</th>
                                            <th style="text-align: center;width: 7%">Net Expense</th>
                                            <th style="text-align: center;width: 12%">Profit/Loss</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        <?php if(!empty($data)): ?>

                                        <?php $serial = 1; ?>

                                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($serial); ?></td>
                                                <td><?php echo e(($type_id == 0 || $type_id == 1) ? date('d-m-Y', strtotime($key)) : date("F, Y", strtotime($key))); ?></td>
                                                <td style="text-align: center"><?php echo e(number_format($value['invoice_amount'],2,'.',',')); ?></td>
                                                <td style="text-align: center"><?php echo e(number_format($value['return_amount'],2,'.',',')); ?></td>
                                                <td style="text-align: center"><?php echo e(number_format($value['invoice_amount'] - $value['return_amount'],2,'.',',')); ?></td>
                                                <td style="text-align: center"><?php echo e(number_format($value['income'],2,'.',',')); ?></td>
                                                <td style="text-align: center"><?php echo e(number_format($value['invoice_amount'] - $value['return_amount'] + $value['income'],2,'.',',')); ?></td>
                                                <td style="text-align: center"><?php echo e(number_format($value['purchase_cost'],2,'.',',')); ?></td>
                                                <td style="text-align: center"><?php echo e(number_format($value['return_purchase_cost'],2,'.',',')); ?></td>
                                                <td style="text-align: center"><?php echo e(number_format($value['purchase_cost'] - $value['return_purchase_cost'],2,'.',',')); ?></td>
                                                <td style="text-align: center"><?php echo e(number_format($value['expense'],2,'.',',')); ?></td>
                                                <td style="text-align: center"><?php echo e(number_format($value['purchase_cost'] - $value['return_purchase_cost'] + $value['expense'],2,'.',',')); ?></td>

                                                <td style="text-align: center"><?php echo e(number_format($value['invoice_amount'] - $value['return_amount'] + $value['income'] - $value['purchase_cost'] - $value['return_purchase_cost'] + $value['expense'],2,'.',',')); ?></td>
                                            </tr>

                                            <?php $serial++; ?>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/66/Modules/Reports/Resources/views/collection_report.blade.php ENDPATH**/ ?>