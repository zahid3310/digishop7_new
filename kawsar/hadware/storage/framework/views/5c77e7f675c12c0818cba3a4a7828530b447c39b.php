<!DOCTYPE html>
<html>

<head>
    <title>Statement of Sales</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">
</head>

<style type="text/css">        
    @page  {
        size: auto;   
        margin: 15mm 5mm 5mm 10mm;
    }

    #print-footer {
        display: none;
        position: fixed;
        bottom: 0;
        left:0;
    }

    #hide-print {
        display: none;
    }

    #print-footer {
        display: none;
    }

    body {
        margin-bottom:10mm;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #ddd;
    }

    .table td, .table th {
        font-size: 12px !important;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p><?php echo e($user_info['address']); ?></p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold"><?php echo e(__('messages.sales_statement')); ?></h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center"><?php echo e(__('messages.from_date')); ?></th>
                                    <th style="text-align: center"><?php echo e(__('messages.item_category')); ?></th>
                                    <th style="text-align: center"><?php echo e(__('messages.sales_type')); ?></th>
                                    <th style="text-align: center"><?php echo e(__('messages.sales_by')); ?></th>
                                    <th style="text-align: center"><?php echo e(__('messages.sales_id')); ?></th>
                                    <th style="text-align: center"><?php echo e(__('messages.customer_name')); ?></th>
                                    <th style="text-align: center"><?php echo e(__('messages.reference')); ?></th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center"><?php echo e($from_date); ?> <strong><?php echo e(__('messages.to')); ?></strong> <?php echo e($to_date); ?></td>
                                    <td style="text-align: center">
                                        <?php if($item_category_name != null): ?>
                                            <?php echo e($item_category_name['name']); ?>

                                        <?php else: ?>
                                            <?php echo e(__('messages.all')); ?>

                                        <?php endif; ?>
                                    </td>

                                    <td style="text-align: center">
                                        <?php if($sales_return == 0): ?>
                                            <?php echo e(__('messages.all')); ?>

                                        <?php endif; ?>

                                        <?php if($sales_return == 1): ?>
                                            Sales
                                        <?php endif; ?>

                                        <?php if($sales_return == 2): ?>
                                            Sales Return
                                        <?php endif; ?>
                                    </td>

                                    <td style="text-align: center">
                                        <?php if($sales_by_name != null): ?>
                                            <?php echo e($sales_by_name['name']); ?>

                                        <?php else: ?>
                                            <?php echo e(__('messages.all')); ?>

                                        <?php endif; ?>
                                    </td>

                                    <td style="text-align: center">
                                        <?php if($sales_id != null): ?>
                                            <?php echo e('INV - ' . str_pad($sales_id['invoice_number'], 6, "0", STR_PAD_LEFT)); ?>

                                        <?php else: ?>
                                            <?php echo e(__('messages.all')); ?>

                                        <?php endif; ?>
                                    </td>

                                    <td style="text-align: center">
                                        <?php if($customer_name != null): ?>
                                            <?php echo e($customer_name['name']); ?>

                                        <?php else: ?>
                                            <?php echo e(__('messages.all')); ?>

                                        <?php endif; ?>
                                    </td>

                                    <td style="text-align: center">
                                        <?php if($reference != null): ?>
                                            <?php echo e($reference['name']); ?>

                                        <?php else: ?>
                                            <?php echo e(__('messages.all')); ?>

                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%"><?php echo e(__('messages.sl')); ?></th>
                                    <th style="text-align: center;width: 6%"><?php echo e(__('messages.T/date')); ?></th>
                                    <th style="text-align: center;width: 7%"><?php echo e(__('messages.sales_id')); ?></th>
                                    <th style="text-align: center;width: 6%"><?php echo e(__('messages.item_code')); ?></th>
                                    <th style="text-align: center;width: 11%"><?php echo e(__('messages.name')); ?></th>
                                    <th style="text-align: center;width: 4%"><?php echo e(__('messages.u/m')); ?></th>
                                    <th style="text-align: center;width: 4%"><?php echo e(__('messages.qty')); ?></th>
                                    <th style="text-align: center;width: 4%"><?php echo e(__('messages.unit_price')); ?></th>
                                    <th style="text-align: center;width: 5%"><?php echo e(__('messages.T/price')); ?></th>
                                    <th style="text-align: center;width: 4%"><?php echo e(__('messages.discount')); ?></th>
                                    <th style="text-align: center;width: 6%"><?php echo e(__('messages.T/payable')); ?></th>
                                    <th style="text-align: center;width: 4%"><?php echo e(__('messages.vat')); ?></th>
                                    <th style="text-align: center;width: 4%"><?php echo e(__('messages.T/discount')); ?></th>
                                    <th style="text-align: center;width: 4%"><?php echo e(__('messages.N/payable')); ?></th>
                                    <th style="text-align: center;width: 4%"><?php echo e(__('messages.paid')); ?></th>
                                    <th style="text-align: center;width: 10%"><?php echo e(__('messages.customer_name')); ?></th>
                                    <th style="text-align: center;width: 2%"><?php echo e(__('messages.type')); ?></th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i                  = 1;
                                    $total_t_price      = 0;
                                    $total_discount     = 0;
                                    $total_payable      = 0;
                                    $total_vat          = 0;
                                    $total_discount_t   = 0;
                                    $total_net_payable  = 0;
                                    $total_paid         = 0;
                                    $total_due          = 0;
                                ?>
                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(isset($value->invoiceEntries[0]) || isset($value->salesReturnEntries[0])): ?>
                                <?php
                                    if (isset($value->invoiceEntries[0]['discount_type']))
                                    {
                                        if ($value->invoiceEntries[0]['discount_type'] == 0)
                                        {
                                            $discountAmount0   = ($value->invoiceEntries[0]['rate'] * $value->invoiceEntries[0]['quantity'] * $value->invoiceEntries[0]['discount_amount'])/100;
                                        }
                                        else
                                        {
                                            $discountAmount0   = $value->invoiceEntries[0]['discount_amount'];
                                        }
                                    }
                                    else
                                    {
                                        $discountAmount0  = 0;
                                    }

                                    if (isset($value->invoiceEntries[0]))
                                    {
                                        $rowspan                = $value->invoiceEntries->count();
                                        $vat                    = $value['vat_type'] == 0 ? $value['total_vat'] : $value['vat'];
                                        $net_payable            = $value['invoice_amount'] + $vat - $discountAmount0;
                                        $total_buy_price        = $value['total_buy_price'];
                                        $profit_loss            = $net_payable - $value['total_buy_price'];
                                    }
                                    else
                                    {
                                        $rowspan                = $value->salesReturnEntries->count();
                                        $vat                    = $value['vat_type'] == 0 ? $value['total_vat'] : $value['vat'];
                                        $net_payable            = $value['return_amount'] + $vat - $discountAmount0;
                                        $total_buy_price        = 0;
                                        $profit_loss            = 0;
                                    }
                                ?>

                                <tr>
                                    <td style="text-align: center;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e($i); ?></td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e(isset($value['invoice_date']) ? $value['invoice_date'] : $value['sales_return_date']); ?></td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e(isset($value['invoice_number']) ? $value['invoice_number'] : $value['sales_return_number']); ?></td>

                                    <td style="text-align: center;"><?php echo e(isset($value->invoiceEntries[0]) ? str_pad($value->invoiceEntries[0]->productEntries->product_code, 6, "0", STR_PAD_LEFT) : str_pad($value->salesReturnEntries[0]->productEntries->product_code, 6, "0", STR_PAD_LEFT)); ?></td>
                                    <td style="text-align: left;"><?php echo e(isset($value->invoiceEntries[0]) ? $value->invoiceEntries[0]->productEntries->name : $value->salesReturnEntries[0]->productEntries->name); ?></td>
                                    <td style="text-align: center;"><?php echo e(isset($value->invoiceEntries[0]) ? $value->invoiceEntries[0]->convertedUnit->name : $value->salesReturnEntries[0]->convertedUnit->name); ?></td>
                                    <td style="text-align: center;"><?php echo e(isset($value->invoiceEntries[0]) ? $value->invoiceEntries[0]['quantity'] : $value->salesReturnEntries[0]['quantity']); ?></td>
                                    <td style="text-align: right;"><?php echo e(isset($value->invoiceEntries[0]) ? $value->invoiceEntries[0]['rate'] : $value->salesReturnEntries[0]['rate']); ?></td>
                                    <td style="text-align: right;"><?php echo e(isset($value->invoiceEntries[0]) ? round($value->invoiceEntries[0]['rate'] * $value->invoiceEntries[0]['quantity'], 2) : round($value->salesReturnEntries[0]['rate'] * $value->salesReturnEntries[0]['quantity'], 2)); ?></td>
                                    <td style="text-align: right;"><?php echo e($discountAmount0); ?></td>
                                    <td style="text-align: right;"><?php echo e(isset($value->invoiceEntries[0]) ? ($value->invoiceEntries[0]['rate'] * $value->invoiceEntries[0]['quantity']) - $discountAmount0 : ($value->salesReturnEntries[0]['rate'] * $value->salesReturnEntries[0]['quantity']) - $discountAmount0); ?></td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e($vat); ?></td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>">
                                        <?php if(isset($value['total_discount_type'])): ?>
                                            <?php echo e($value['total_discount_type'] == 0 ? $value['total_discount_perc'] : $value['total_discount']); ?>

                                        <?php else: ?>
                                            0
                                        <?php endif; ?>
                                    </td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e(round($net_payable, 2)); ?></td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e(isset($value->invoiceEntries[0]) ? round($value['cash_given'], 2) : round($value['return_amount'], 2)); ?></td>

                                    <td style="text-align: center;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e($value->customer->name); ?></td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e(isset($value['type']) ? 'S' : 'R'); ?></td>
                                </tr>
                                 
                                <?php
                                    $sub_total_t_price      = 0;
                                    $sub_total_discount     = 0;
                                    $sub_total_payable      = 0;
                                ?>

                                <?php if(isset($value->invoiceEntries)): ?>
                                    <?php $__currentLoopData = $value->invoiceEntries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <?php
                                        if (isset($value1['discount_type']))
                                        {
                                            if ($value1['discount_type'] == 0)
                                            {
                                                $discountAmount   = ($value1['rate'] * $value1['quantity'] * $value1['discount_amount'])/100;
                                            }
                                            else
                                            {
                                                $discountAmount   = $value1['discount_amount'];
                                            }
                                        }
                                        else
                                        {
                                            $discountAmount   = 0;
                                        }

                                        $sub_total_t_price  = $sub_total_t_price + ($value1['rate'] * $value1['quantity']);
                                        $sub_total_discount = $sub_total_discount + $discountAmount;
                                        $sub_total_payable  = $sub_total_payable + (($value1['rate'] * $value1['quantity']) - $discountAmount);
                                    ?>

                                    <?php if($key1 != 0): ?>

                                    <tr>
                                        <td style="text-align: center;"><?php echo e(str_pad($value1->productEntries->product_code, 6, "0", STR_PAD_LEFT)); ?></td>
                                        <td style="text-align: left;"><?php echo e($value1->productEntries->name); ?></td>
                                        <td style="text-align: center;"><?php echo e($value1['unit_name']); ?></td>
                                        <td style="text-align: center;"><?php echo e($value1['quantity']); ?></td>
                                        <td style="text-align: right;"><?php echo e($value1['rate']); ?></td>
                                        <td style="text-align: right;"><?php echo e(round($value1['rate'] * $value1['quantity'], 2)); ?></td>
                                        <td style="text-align: right;">
                                            <?php
                                                if ($value1['discount_type'] == 0)
                                                {
                                                    $discountAmount   = ($value1['rate'] * $value1['quantity'] * $value1['discount_amount'])/100;
                                                }
                                                else
                                                {
                                                    $discountAmount   = $value1['discount_amount'];
                                                }
                                            ?>

                                            <?php echo e($discountAmount); ?>

                                        </td>
                                        <td style="text-align: right;"><?php echo e(($value1['rate'] * $value1['quantity']) - $discountAmount); ?></td>
                                    </tr>

                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    <?php
                                        $i++;

                                        $total_t_price      = $total_t_price + $sub_total_t_price;
                                        $total_discount     = $total_discount + $sub_total_discount;
                                        $total_payable      = $total_payable + $sub_total_payable;
                                        $total_vat          = $total_vat +  $vat;
                                        $total_discount_t   = $total_discount_t + $discountAmount0;
                                        $total_net_payable  = $total_net_payable + $net_payable;
                                        $total_paid         = $total_paid + $value['cash_given'];
                                    ?>
                                <?php else: ?>
                                    <?php $__currentLoopData = $value->salesReturnEntries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2 => $value2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <?php
                                        if (isset($value2['discount_type']))
                                        {
                                            if ($value2['discount_type'] == 0)
                                            {
                                                $discountAmount   = ($value2['rate'] * $value2['quantity'] * $value2['discount_amount'])/100;
                                            }
                                            else
                                            {
                                                $discountAmount   = $value2['discount_amount'];
                                            }
                                        }
                                        else
                                        {
                                            $discountAmount   = 0;
                                        }

                                        $sub_total_t_price  = $sub_total_t_price + ($value2['rate'] * $value2['quantity']);
                                        $sub_total_discount = $sub_total_discount + $discountAmount;
                                        $sub_total_payable  = $sub_total_payable + (($value2['rate'] * $value2['quantity']) - $discountAmount);
                                    ?>

                                    <?php if($key2 != 0): ?>

                                    <tr>
                                        <td style="text-align: center;"><?php echo e(str_pad($value2->productEntries->product_code, 6, "0", STR_PAD_LEFT)); ?></td>
                                        <td style="text-align: left;"><?php echo e($value2->productEntries->name); ?></td>
                                        <td style="text-align: center;"><?php echo e($value2['unit_name']); ?></td>
                                        <td style="text-align: center;"><?php echo e($value2['quantity']); ?></td>
                                        <td style="text-align: right;"><?php echo e($value2['rate']); ?></td>
                                        <td style="text-align: right;"><?php echo e(round($value2['rate'] * $value2['quantity'], 2)); ?></td>
                                        <td style="text-align: right;">
                                            <?php
                                                if ($value2['discount_type'] == 0)
                                                {
                                                    $discountAmount   = ($value2['rate'] * $value2['quantity'] * $value2['discount_amount'])/100;
                                                }
                                                else
                                                {
                                                    $discountAmount   = $value2['discount_amount'];
                                                }
                                            ?>

                                            <?php echo e($discountAmount); ?>

                                        </td>
                                        <td style="text-align: right;"><?php echo e(($value2['rate'] * $value2['quantity']) - $discountAmount); ?></td>
                                    </tr>

                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    <?php
                                        $i++;

                                        $total_t_price      = $total_t_price + $sub_total_t_price;
                                        $total_discount     = $total_discount + $sub_total_discount;
                                        $total_payable      = $total_payable + $sub_total_payable;
                                        $total_vat          = $total_vat +  $vat;
                                        $total_discount_t   = $total_discount_t + $discountAmount0;
                                        $total_net_payable  = $total_net_payable + $net_payable;
                                        $total_paid         = $total_paid + $value2['return_amount'];
                                    ?>
                                <?php endif; ?>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="8" style="text-align: right;"><?php echo e(__('messages.total')); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e(round($total_t_price)); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e(round($total_discount)); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e(round($total_payable)); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e(round($total_vat)); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e(round($total_discount_t)); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e(round($total_net_payable)); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e(round($total_paid)); ?></th>
                                    <th colspan="1" style="text-align: right;"></th>
                                    <th colspan="2" style="text-align: right;"></th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script> -->
<!-- <script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script> -->
<!-- <script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script> -->

</body>
</html><?php /**PATH /home/digishop7/public_html/kawsar/hadware/Modules/Reports/Resources/views/sales_statement_print.blade.php ENDPATH**/ ?>