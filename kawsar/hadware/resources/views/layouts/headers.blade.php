<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">{{__('messages.menu')}}</li>

                <li>
                    <a href="{{ route('home') }}" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span>{{ __('messages.dashboard')}}</span>
                    </a>
                </li>

                @if(Auth::user()->branch_id == 1)
                <li class="{{ 
                Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-suitcase"></i><span>{{ __('messages.receive_or_purchase_report')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('bills_index') }}">{{ __('messages.receive_purchase_item')}}</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' }}" href="{{ route('bills_all_bills') }}">{{ __('messages.list_of_receive/purchase')}}</a> </li>
                        <li> <a class="{{ Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' }}" href="{{ route('purchase_return_index') }}">{{ __('messages.receive/purchase_return')}}</a> </li>
                    </ul>
                </li>
                @endif

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-suitcase"></i><span>{{ __('messages.stock_transfer')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('stock_transfer_index') }}">{{ __('messages.new_transfer')}}</a> </li>
                    </ul>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-shopping-basket"></i><span>{{ __('messages.sales_to_sr/customer')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('invoices_index') }}">{{ __('messages.daily_sales')}}</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' }}" href="{{ route('invoices_all_sales') }}">{{ __('messages.list_of_sales')}}</a> </li>
                        <li> <a class="{{ Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' }}"  href="{{ route('sales_return_index') }}">{{ __('messages.sales_return')}}</a> </li>
                    </ul>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'settlement_type=0' ? 'mm-active' : '' ||  Request::getQueryString() == 'settlement_type=1' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'settlement_type=0' ? 'mm-active' : '' ||  Request::getQueryString() == 'settlement_type=1' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fab fa-adn"></i><span>{{ __('messages.accounts')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' }}" href="{{ route('expenses_index') }}">{{ __('messages.expenses')}}</a> </li>

                        <li> <a class="{{ 
                            Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' }}" href="{{ route('incomes_index') }}">{{ __('messages.incomes')}}</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : '' }}" href="{{ route('balance_transfer_index') }}">{{ __('messages.balance_transfer')}}</a>
                        </li>
                        <li> <a class="{{ Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' }}" href="{{ route('payments_create').'?payment_type=0' }}">{{ __('messages.receive_from_customer')}}</a> </li>
                        @if(Auth::user()->branch_id == 1)
                        <li> <a class="{{ Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' }}" href="{{ route('payments_create').'?payment_type=1' }}">{{__('messages.pay_to_supplier')}}</a> </li>
                        @endif
                        <li> <a class="{{ Request::getQueryString() == 'settlement_type=0' ? 'mm-active' : '' }}" href="{{ route('settlements_create').'?settlement_type=0' }}">{{__('messages.customer_settlement')}}</a> </li>
                        @if(Auth::user()->branch_id == 1)
                        <li> <a class="{{ Request::getQueryString() == 'settlement_type=1' ? 'mm-active' : '' }}" href="{{ route('settlements_create').'?settlement_type=1' }}">{{__('messages.supplier_settlement')}}</a> </li>
                        @endif
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="far fa-envelope"></i><span>{{ __('messages.messaging')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('messages_send_index') }}">{{ __('messages.send_meg')}}</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>{{ __('messages.reports')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.sales')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('sales_statement_index') }}">{{ __('messages.sales_statement')}}</a> </li>
                                <li> <a href="{{ route('sales_summary_index') }}">{{ __('messages.sales_summary')}}</a> </li>
                                <li> <a href="{{ route('product_wise_sales_report_index') }}">{{ __('messages.product_wise_sales')}}</a> </li>
                            </ul>
                        </li>

                        @if(Auth()->user()->branch_id == 1)
                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.receive_or_purchase_report')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('purchase_statement_index') }}">{{ __('messages.purchase_statement')}}</a> </li>
                                <li> <a href="{{ route('purchase_summary_index') }}">{{ __('messages.receive/purchase_summary')}}</a> </li>
                            </ul>
                        </li>
                        @endif

                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.accounts')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('expense_report_index') }}">{{ __('messages.list_of_expense')}}</a> </li>
                                <li> <a href="{{ route('income_report_index') }}">{{ __('messages.list_of_income')}}</a> </li>
                                <!-- <li> <a href="{{ route('general_ledger_index') }}">{{ __('messages.ledger_book')}}</a> </li> -->
                                <li> <a href="{{ route('income_expense_ledger_index') }}">{{ __('messages.cash_book')}}</a> </li>
                                <li> <a href="{{ route('income_statement_index') }}">{{ __('messages.bank_book')}}</a> </li>
                                <!-- <li> <a href="{{ route('daily_collection_report_index') }}">{{ __('messages.daily_report')}}</a> </li> -->
                            </ul>
                        </li>
                        
                        <!-- <li class="">
                            <a class="has-arrow waves-effect">
                                Payments
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('customer_payment_report_index') }}">SR/Customer Payments</a> </li>
                                <li> <a href="{{ route('supplier_payment_report_index') }}">Supplier Payments</a> </li>
                            </ul>
                        </li> -->

                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.mis')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('stock_report_index') }}">{{ __('messages.stock_status')}}</a> </li>
                                <!--<li> <a href="{{ route('stock_status_details_index') }}">Stock Status Details</a> </li>-->
                                <li> <a href="{{ route('stock_transfer_report_index') }}">{{ __('messages.stock_transfer_report')}}</a> </li>
                                <li> <a href="{{ route('current_balance_print') }}" target="_blank">{{ __('messages.current_balance')}}</a> </li>
                                @if(Auth::user()->branch_id == 1)
                                <li> <a href="{{ route('due_report_supplier_index') }}">{{ __('messages.supplier_Ledger')}}</a> </li>
                                @endif
                                <li> <a href="{{ route('due_report_customer_index') }}">{{ __('messages.SR/Customer_Ledger')}}</a> </li>
                                <li> <a href="{{ route('due_report_customer_print_due') }}">{{ __('messages.Customer_due_list')}}</a> </li>
                                @if(Auth::user()->branch_id == 1)
                                <li> <a href="{{ route('due_report_supplier_index_due') }}">{{ __('messages.Supplier_due_list')}}</a> </li>
                                @endif
                                <!-- <li> <a href="{{ route('due_ledger_index') }}">Due Ledger</a> </li> -->
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.basic_report')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                @if(Auth()->user()->branch_id == 1)
                                <li> <a href="{{ route('item_list_index') }}">{{ __('messages.list_items')}}</a> </li>
                                <li> <a href="{{ route('emergency_item_list_index') }}">{{ __('messages.emergency_purchase')}}</a> </li>
                                <li> <a href="{{ route('product_suppliers_index') }}">{{ __('messages.item_wise_supplier')}}</a> </li>
                                <li> <a href="{{ route('product_customers_index') }}">{{ __('messages.item_wise_SR/Customer')}}</a> </li>
                                @endif
                                <li> <a href="{{ route('register_list_index').'?type=0' }}" target="_blank">{{ __('messages.SR/Customer_list')}}</a> </li>
                                <li> <a href="{{ route('register_list_index').'?type=1' }}" target="_blank">{{ __('messages.supplier_list')}}</a> </li>
                            </ul>
                        </li>
                        
                        <!-- <li> <a href="#">List of Sending SMS</a> </li>
                        <li> <a href="{{ route('salary_report_index') }}">Salary Report</a> </li> -->

                    </ul>
                </li>

                <li class="{{ 
                Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'users_index' ? 'mm-active' : '' || Route::currentRouteName() == 'users_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'users_index' ? 'mm-active' : '' || Route::currentRouteName() == 'users_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>{{ __('messages.basic_settings')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        @if(Auth()->user()->branch_id == 1)
                        <li class="{{ Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                 {{ __('messages.product')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' }}" href="{{ route('products_index') }}">{{ __('messages.add_product')}}</a> </li>
                                <li> <a class="" href="{{ route('products_index_all') }}">{{ __('messages.list_product')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }}" href="{{ route('products_category_index') }}">{{ __('messages.add_categories')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' }}" href="{{ route('categories_index') }}" href="{{ route('categories_index') }}">Add Brand</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_group_edit' ? 'mm-active' : '' }}" href="{{ route('products_group_index') }}">Add Model</a> </li>
                                
                                 
                                <li> <a class="{{ Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' }}" href="{{ route('products_units_index') }}">{{ __('messages.add_unit_measure')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' }}" href="{{ route('products_variations_index') }}">{{ __('messages.product_variations')}}</a> </li>
                                <li> <a href="{{ route('products_barcode_print') }}">{{ __('messages.print_barcode')}}</a> </li>
                                <li> <a href="{{ route('products_opening_stock') }}">{{ __('messages.bulk_opening_stock')}}</a> </li>
                                <li> <a href="{{ route('products_bulk_product_list_update') }}">{{ __('messages.bulk_product_update')}}</a> </li>
                            </ul>
                        </li>
                        @endif

                        <li class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' }}">
                            <a class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' }} has-arrow waves-effect">
                                 {{ __('messages.registers')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=0' }}">{{ __('messages.add_sr/customer')}}</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=1' }}">{{ __('messages.add_supplier')}}</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=2' }}">{{ __('messages.add_employee')}}</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=3' }}">{{ __('messages.add_reference')}}</a> </li>
                            </ul>
                        </li>

                        <li class="{{ Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || 
                        Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' ||
                        Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || 
                            Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                {{ __('messages.accounts')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <!-- <li> <a class="{{ Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' }}" href="{{ route('account_types_index') }}">Account Types</a> </li> -->
                                <li> <a class="{{ Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' }}" href="{{ route('chart_of_accounts_index') }}">{{ __('messages.chart_of_accounts')}}</a> </li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow waves-effect">
                                {{ __('messages.messaging')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('messages_index') }}">{{ __('messages.create_message')}}</a> </li>
                                <li> <a href="{{ route('messages_phone_book_index') }}">{{ __('messages.pb')}}</a> </li>
                            </ul>
                        </li>

                        @if(Auth()->user()->branch_id == 1)
                        <li>
                            <a class="has-arrow waves-effect">
                                {{ __('messages.security_system')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('branch_index') }}">{{ __('messages.add_branch')}}</a> </li>
                                <li> <a href="{{ route('users_index') }}">{{ __('messages.add_user')}}</a> </li>
                                <li> <a href="{{ route('users_index_all') }}">{{ __('messages.list_user')}}</a> </li>
                                <li> <a href="{{ route('set_access_index') }}">{{ __('messages.permission')}}</a> </li>
                            </ul>
                        </li>
                         @endif
                        
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>