<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Issues extends Model
{  
    protected $table = "issues";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function srName()
    {
        return $this->belongsTo('App\Models\Customers','sr_id');
    }
}
