<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class DealerDamages extends Model
{  
    protected $table = "dealer_damages";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Models\Customers','supplier_id');
    }

    public function issues()
    {
        return $this->belongsTo('App\Models\Issues','issue_id');
    }

    public function orders()
    {
        return $this->belongsTo('App\Models\Orders','order_id');
    }
    
    public function dealerDamageEntries()
    {
        return $this->hasMany(DealerDamageEntries::class, "dealer_damage_id");
    }
}
