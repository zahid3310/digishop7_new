<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class SalaryStatements extends Model
{  
    protected $table = "salary_statements";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function employee()
    {
        return $this->belongsTo('App\Models\Customers','employee_id');
    }

    public function grade()
    {
        return $this->belongsTo('App\Models\SalaryGrades','grade_id');
    }
}
