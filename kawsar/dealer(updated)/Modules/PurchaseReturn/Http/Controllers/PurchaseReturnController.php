<?php

namespace Modules\PurchaseReturn\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\BranchInventories;
use App\Models\Customers;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\PurchaseReturn;
use App\Models\PurchaseReturnEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Users;
use App\Models\UnitConversions;
use App\Models\Accounts;
use App\Models\JournalEntries;
use Validator;
use Auth;
use Response;
use DB;

class PurchaseReturnController extends Controller
{
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $paid_accounts  = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();

        return view('purchasereturn::index', compact('paid_accounts'));
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('purchasereturn::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'return_date'   => 'required',
            'customer_id'   => 'required',
            'bill_id'       => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat                                        = $data['vat_amount'];
            $data_find                                  = PurchaseReturn::orderBy('created_at', 'DESC')->first();
            $purchase_return_number                     = $data_find != null ? $data_find['purchase_return_number'] + 1 : 1;

            $purchase_return                            = new PurchaseReturn;
            $purchase_return->purchase_return_number    = $purchase_return_number;
            $purchase_return->customer_id               = $data['customer_id'];
            $purchase_return->bill_id                   = $data['bill_id'];
            $purchase_return->purchase_return_date      = date('Y-m-d', strtotime($data['return_date']));
            $purchase_return->return_note               = $data['return_note'];
            $purchase_return->sub_total_amount          = $data['sub_total_amount'];
            $purchase_return->return_amount             = $data['total_return_amount'];
            $purchase_return->due_amount                = $data['total_return_amount'] - $data['total_return_amount_paid'];
            $purchase_return->total_vat                 = $vat;
            $purchase_return->vat_type                  = $data['vat_type'];
            $purchase_return->total_discount_type       = $data['total_discount_type'];
            $purchase_return->total_discount_amount     = $data['total_discount_amount'];
            $purchase_return->created_by                = $user_id;

            if ($purchase_return->save())
            {   
                $update_bill                 =  Bills::find($data['bill_id']);
                $update_bill->return_amount  =  $update_bill['return_amount'] + $data['total_return_amount'];
                $update_bill->save();

                foreach ($data['product_id'] as $key => $value)
                {   
                    if ($data['return_quantity'][$key] != 0)
                    {   
                        $stock    = ProductEntries::find($data['product_entries'][$key]);

                        $purchase_return_entries[] = [
                            'bill_id'               => $purchase_return['bill_id'],
                            'purchase_return_id'    => $purchase_return['id'],
                            'product_id'            => $value,
                            'product_entry_id'      => $data['product_entries'][$key],
                            'customer_id'           => $purchase_return['customer_id'],
                            'main_unit_id'          => $data['main_unit_id'][$key],
                            'conversion_unit_id'    => $data['unit_id'][$key],
                            'rate'                  => $data['amount'][$key]/$data['quantity'][$key],
                            'quantity'              => $data['return_quantity'][$key],
                            'total_amount'          => ($data['amount'][$key]/$data['quantity'][$key])*$data['return_quantity'][$key],
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('purchase_return_entries')->insert($purchase_return_entries);

                stockOutReturn($data, $item_id=null);

                //Financial Accounting Start
                    debit($customer_id=$data['customer_id'], $date=$data['return_date'], $account_id=6, $amount=$data['total_return_amount'], $note=null, $transaction_head='purchase-return', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=$purchase_return->id);
                    credit($customer_id=$data['customer_id'], $date=$data['return_date'], $account_id=8, $amount=$data['total_return_amount'], $note=null, $transaction_head='purchase-return', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=$purchase_return->id);

                    //Insert into journal_entries 
                    if ((isset($data['total_return_amount_paid'])) && ($data['total_return_amount_paid'] > 0))
                    {   
                        debit($customer_id=$data['customer_id'], $date=$data['return_date'], $account_id=$data['paid_through'], $amount=$data['total_return_amount_paid'], $note=null, $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=$purchase_return->id);
                        credit($customer_id=$data['customer_id'], $date=$data['return_date'], $account_id=9, $amount=$data['total_return_amount_paid'], $note=null, $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=$purchase_return->id);
                    }

                    supplierBalanceUpdate($data['customer_id']);
                //Financial Accounting End

                DB::commit();
                return back()->with("success","Purchase Return Created Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $purchase_return    = PurchaseReturn::leftjoin('customers', 'customers.id', 'purchase_return.customer_id')
                                        ->select('purchase_return.*',
                                                 'customers.name as customer_name',
                                                 'customers.address as address',
                                                 'customers.phone as phone')
                                        ->find($id);

        $entries            = PurchaseReturnEntries::leftjoin('products', 'products.id', 'purchase_return_entries.product_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'purchase_return_entries.product_entry_id')
                                        ->where('purchase_return_entries.purchase_return_id', $id)
                                        ->select('purchase_return_entries.*',
                                                 'product_entries.name as product_entry_name',
                                                 'products.name as product_name')
                                        ->get();  
                     
        $user_info  = userDetails();

        return view('purchasereturn::show', compact('entries', 'purchase_return', 'user_info'));
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('purchasereturn::edit');
    }

    public function update(Request $request, $id)
    {
    }

    public function delete($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        DB::beginTransaction();

        try{
            $return     = PurchaseReturn::find($id);
            $entries    = PurchaseReturnEntries::where('purchase_return_id', $id)->get();

            //stock Update
            foreach ($entries as $key => $value)
            {
                $conversion_rate_find   = UnitConversions::where('main_unit_id', $value['main_unit_id'])
                                                        ->where('converted_unit_id', $value['conversion_unit_id'])
                                                        ->where('product_entry_id', $value['product_entry_id'])
                                                        ->first();

                $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $value['quantity']/$conversion_rate_find['conversion_rate'] : $value['quantity'];

                //product entries table update
                $product_entry                    = ProductEntries::find($value['product_entry_id']);
                $product_entry->stock_in_hand     = $product_entry['stock_in_hand'] + round($converted_quantity_to_main_unit, 2);
                $product_entry->save();
            }

            $return_delete  = PurchaseReturn::where('id', $id)->delete();
            $journal_delete = JournalEntries::where('purchase_return_id', $id)->delete();

            supplierBalanceUpdate($return['customer_id']);

            DB::commit();
            return back()->with("success","Purchase Return Deleted Successfully !!");

        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return back()->with("unsuccess","Not Deleted");
        }
    }

    public function billList($id)
    {
        $data           = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                ->where('bills.vendor_id', $id)
                                ->orderBy('bills.created_at', 'DESC')
                                ->select('bills.*',
                                         'customers.name as customer_name')
                                ->get();
   
        return Response::json($data);
    }

    public function billEntriesList($id)
    {
        $bill_entry             = BillEntries::leftjoin('customers', 'customers.id', 'bill_entries.vendor_id')
                                        ->leftjoin('products', 'products.id', 'bill_entries.product_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                        ->leftjoin('units', 'units.id', 'bill_entries.conversion_unit_id')
                                        ->where('bill_entries.bill_id', $id)
                                        ->orderBy('bill_entries.created_at', 'DESC')
                                        ->select('bill_entries.*',
                                                 'products.name as product_name',
                                                 'product_entries.name as entry_name',
                                                 'product_entries.product_code as product_code',
                                                 'units.name as conversion_unit_name',
                                                 'customers.name as customer_name')
                                        ->get();

        $purchase_return_entry  = PurchaseReturnEntries::where('purchase_return_entries.bill_id', $id)->get();
        $bill                   = Bills::where('bills.id', $id)->select('bills.*')->first();

        if ($bill_entry->count() > 0)
        {
            foreach ($bill_entry as $key => $value)
            {
                $purchase_return_quantity_sum                       = $purchase_return_entry->where('product_entry_id', $value['product_entry_id']) ->sum('quantity');

                $entry_data[$value['id']]['id']                     =  $value['id'];
                $entry_data[$value['id']]['bill_id']                =  $value['bill_id'];
                $entry_data[$value['id']]['product_id']             =  $value['product_id'];
                $entry_data[$value['id']]['product_entry_id']       =  $value['product_entry_id'];
                $entry_data[$value['id']]['product_code']           =  $value['product_code'];
                $entry_data[$value['id']]['customer_id']            =  $value['customer_id'];
                $entry_data[$value['id']]['main_unit_id']           =  $value['main_unit_id'];
                $entry_data[$value['id']]['conversion_unit_id']     =  $value['conversion_unit_id'];
                $entry_data[$value['id']]['conversion_unit_name']   =  $value['conversion_unit_name'];
                $entry_data[$value['id']]['buy_price']              =  $value['buy_price'];
                $entry_data[$value['id']]['rate']                   =  $value['rate'];
                $entry_data[$value['id']]['original_quantity']      =  $value['quantity'];
                $entry_data[$value['id']]['quantity']               =  $value['quantity'] - $purchase_return_quantity_sum;
                $entry_data[$value['id']]['total_amount']           =  $value['total_amount'];
                $entry_data[$value['id']]['discount_type']          =  $value['discount_type'];
                $entry_data[$value['id']]['discount_amount']        =  $value['discount_amount'];
                $entry_data[$value['id']]['product_name']           =  $value['product_name'];
                $entry_data[$value['id']]['entry_name']             =  $value['entry_name'];
                $entry_data[$value['id']]['group']                  =  $value->productEntries->group->name;
                $entry_data[$value['id']]['brand']                  =  $value->productEntries->brand->name;
                $entry_data[$value['id']]['category']               =  $value->productEntries->product->name;
                $entry_data[$value['id']]['customer_name']          =  $value['customer_name'];

                $data_1                                 = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                                            ->where('product_entries.id', $value['product_entry_id'])
                                                                            ->selectRaw('units.id as unit_id, units.name as unit_name')
                                                                            ->get()
                                                                            ->toArray();

                $data_2                                 = UnitConversions::leftjoin('units', 'units.id', 'unit_conversions.converted_unit_id')
                                                                            ->where('unit_conversions.product_entry_id', $value['product_entry_id'])
                                                                            ->selectRaw('unit_conversions.converted_unit_id as unit_id, units.name as unit_name')
                                                                            ->get()
                                                                            ->toArray();

                $entry_data[$value['id']]['unit_conversions']    = collect(array_merge($data_1, $data_2));
            }
        }
        else
        {
            $entry_data = [];
        }

        $data['bill']               = $bill;
        $data['bill_entries']       = $entry_data;
   
        return Response::json($data);
    }

    public function purchaseReturnListLoad()
    {
        $data           = PurchaseReturn::leftjoin('bills', 'bills.id', 'purchase_return.bill_id')
                                ->leftjoin('customers', 'customers.id', 'purchase_return.customer_id')
                                ->orderBy('purchase_return.created_at', 'DESC')
                                ->select('purchase_return.*',
                                         'customers.name as customer_name',
                                         'customers.phone as phone',
                                         'bills.bill_number as bill_number')
                                ->get();

        return Response::json($data);
    }

    public function purchaseReturnListSearch($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data           = PurchaseReturn::leftjoin('bills', 'bills.id', 'purchase_return.bill_id')
                                ->leftjoin('customers', 'customers.id', 'purchase_return.customer_id')
                                ->where('customers.name', 'LIKE', "%$id%")
                                ->orWhere('purchase_return.purchase_return_date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                    return $query->orWhere('purchase_return.purchase_return_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('purchase_return.created_at', 'DESC')
                                ->select('purchase_return.*',
                                         'customers.name as customer_name',
                                         'customers.phone as phone',
                                         'bills.bill_number as bill_number')
                                ->get();
        }
        else
        {
            $data           = PurchaseReturn::leftjoin('bills', 'bills.id', 'purchase_return.bill_id')
                                ->leftjoin('customers', 'customers.id', 'purchase_return.customer_id')
                                ->orderBy('purchase_return.created_at', 'DESC')
                                ->select('purchase_return.*',
                                         'customers.name as customer_name',
                                         'customers.phone as phone',
                                         'bills.bill_number as bill_number')
                                ->get();
        }

        return Response::json($data);
    }

    public function findBillDetails($id)
    {
        $data           = Bills::find($id);

        return Response::json($data);
    }

    public function calculateOpeningBalance($customer_id)
    {
        $data  = calculateOpeningBalance($customer_id);

        return Response::json($data);
    }
}
