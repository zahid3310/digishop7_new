@extends('layouts.app')

@section('title', 'Damages')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Damage From SM</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Damages</a></li>
                                    <li class="breadcrumb-item active">New Damage</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('damages_receive_from_customer_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                {{ csrf_field() }}

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Date *</label>
                                            <div class="col-md-8">
                                                <input id="damage_date" name="damage_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">DSM</label>
                                            <select id="customer_id" name="customer_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" onchange="getIssueList()" required>
                                                <option>--Select DSM--</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Issue#</label>
                                            <select id="issue_number" name="issue_number" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" onchange="getOrderList()" required>
                                                <option>--Select Issue--</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Order#</label>
                                            <select id="order_number" name="order_number" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" onchange="findIssueList()" required>
                                                <option>--Select Order--</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-2 col-form-label">Note</label>
                                            <div class="col-md-10">
                                                <input id="note" name="note" type="text" class="form-control" placeholder="Damage Note" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 620px;padding-top: 10px;overflow-y: auto;overflow-x: hidden;" class="col-md-12">
                                        <div style="display: none" class="inner-repeater mb-4 issueDetails">
                                            <div data-repeater-list="inner-group" class="inner form-group row">
                                                <div id="return_product_list" class="inner col-lg-12 ml-md-auto">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="display: none;padding: 10px" class="row issueDetails">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-5">
                                                <button name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('all_return_issues') }}">Close</a></button>
                                            </div>
                                            <div class="button-items col-lg-4"></div>
                                            <div class="button-items col-lg-3">
                                                 <div style="margin-bottom: 5px" class="form-group row">
                                                    <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.sub_total')}}</label>
                                                    <div class="col-md-7">
                                                        <input type="text" id="total_amount" name="total_amount" class="form-control" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ url('public/common_javascripts/common.js') }}"></script>

    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 6)
                    {
                        return result['text'];
                    }
                },
            });
        });

        function getIssueList()
        {
            var site_url       = $('.site_url').val();
            var customer_id    = $('#customer_id').val();

            $("#issue_number").select2({
                ajax: { 
                url:  site_url + '/receive-from-customer/list-of-issues/' + customer_id,
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        }

        function getOrderList()
        {
            var site_url       = $('.site_url').val();
            var issue_number   = $('#issue_number').val();

            $("#order_number").select2({
                ajax: { 
                url:  site_url + '/receive-from-customer/list-of-orders/' + issue_number,
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        }
    </script>

    <script type="text/javascript">
        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>

    <script type="text/javascript">
        function getItemPrice(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var main_unit_id        = $("#main_unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param-main-unit/'+ product_entry_id + '/' + main_unit_id, function(data){
  
                var convertedUnitName = $('#converted_unit_id_'+x).find(":selected").text();
                var mainStock         = $("#main_stock_"+x).val();
                var convertedStock    = (parseFloat(mainStock)/parseFloat(data.conversion_rate)).toFixed(2);
                var mainStock         = $("#main_stock_"+x).val(parseFloat(convertedStock).toFixed(2));

                $("#quantity_"+x).val(convertedStock);
                $("#rate_"+x).val(parseFloat(data.sell_price_pr).toFixed(2));
            });
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#converted_unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#converted_unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#main_stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed();
                    var mainStock         = $("#main_stock_"+x).val(parseFloat(convertedStock).toFixed(2));

                    $("#quantity_"+x).val(convertedStock);
                    $("#rate_"+x).val(parseFloat(data.sell_price).toFixed(2));
                    $("#main_unit_show_"+x).html(convertedUnitName);
                }

            });
        }

        function getCategoryList(x)
        {
            var site_url  = $('.site_url').val();
            var group_id  = $('#group_id_'+x).val();

            $("#product_category_id_"+x).select2({
                ajax: { 
                    url:  site_url + '/groups/category-list-ajax/' + group_id,
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });
        }
    </script>

    <script type="text/javascript">
        function findIssueList()
        {
            var site_url        = $(".site_url").val();
            var sr_id           = $("#customer_id").val();
            var issue_id        = $("#issue_number").val();
            var order_id        = $("#order_number").val();

            $.get(site_url + '/receive-from-customer/order-details/'+ sr_id + '/' + issue_id + '/' + order_id, function(data){

                if (data != '')
                {
                    $('.issueDetails').show();
                }
                else
                {
                    $('.issueDetails').hide();
                }

                var return_product_list = '';
                var serial  = 1;
                $.each(data, function(j, data_list_j)
                {
                    var header  = data_list_j.order_details;
                    $.each(data_list_j, function(i, data_list)
                    {
                        if($.isNumeric(i))
                        {
                            if (serial == serial)
                            {
                                var group_label         = '<label class="hidden-xs" for="productname">Group *</label>\n';
                                var brand_label         = '<label class="hidden-xs" for="productname">Brand *</label>\n';
                                var category_label      = '<label class="hidden-xs" for="productname">Category *</label>\n';
                                var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                                var unit_label          = '<label class="hidden-xs" for="productname">Unit</label>\n';
                                var stock_label         = '<label class="hidden-xs" for="productname">Stock</label>\n';
                                var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                                var rate_label          = '<label class="hidden-xs" for="productname">Rate</label>\n';
                                var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                                var return_label        = '<label class="hidden-xs" for="productname">Damage Qty</label>\n';
                            }
                            else
                            {
                                var group_label         = '';
                                var brand_label         = '';
                                var category_label      = '';
                                var product_label       = '';
                                var unit_label          = '';
                                var stock_label         = '';
                                var rate_label          = '';
                                var quantity_label      = '';
                                var rate_label          = '';
                                var amount_label        = '';
                                var return_label        = '';
                            }

                            var quantity        = parseFloat(data_list.quantity);
                            var return_quantity = '<input id="return_quantity_'+ serial +'" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="" oninput="calculate('+ serial +')" />\n' 
                            

                            var unit_list    = '';
                            $.each(data_list.unit_data, function(i, data_list_unit)
                            {   
                                if (data_list_unit.unit_id == data_list.converter_unit_id)
                                {

                                    unit_list    += '<option value = "' + data_list_unit.unit_id + '" selected>' + data_list_unit.unit_name + '</option>';
                                }
                                else
                                {
                                    unit_list    += '<option value = "' + data_list_unit.unit_id + '">' + data_list_unit.unit_name + '</option>';

                                }  
                            });

                            if (quantity != 0)
                            {
                                return_product_list += ' ' + '<div class="row di_'+serial+'">' +
                                                            
                                                            
                                                            
                                                            '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12">\n' +
                                                                '<h4>' + header + '</h4>' +
                                                            '</div>\n' +
                                                            '<span style="margin-top: 35px;padding-left: 10px;font-weight:bold;font-size:14px">'+serial +'.</span>'+
                                                            '<input id="order_id" type="hidden" class="inner form-control" value="'+ data_list.order_id +'" name="order_id[]" />\n' +
                                                                
                                                            '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-4 col-md-4 col-sm-6 col-12">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Group *</label>\n' +
                                                                group_label +
                                                                '<select style="width: 100%" class="inner form-control single_select2 groupId" id="group_id_'+serial+'" required>\n' +
                                                                    '<option value="'+data_list.group_id+'">' + data_list.group_name + '</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-4 col-md-4 col-sm-6 col-12">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Brand *</label>\n' +
                                                                brand_label +
                                                                '<select style="width: 100%" class="inner form-control single_select2" id="brand_id_'+serial+'" required>\n' +
                                                                    '<option value="'+data_list.brand_id+'">' + data_list.brand_name + '</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="margin-bottom: 5px;padding-right: 10px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Category *</label>\n' +
                                                                category_label +
                                                                '<select style="width: 125%" class="inner form-control single_select2" id="product_category_id_'+serial+'" onchange="getProductList('+serial+')"required>\n' +
                                                                    '<option value="'+data_list.category_id+'">' + data_list.category_name + '</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +
                                                            
                                                            '<div style="margin-bottom: 5px;padding-left: 10px" class="col-lg-6 col-md-6 col-sm-6 col-12">\n' +
                                                                '<label style="display: none;width: 100%" class="show-xs" for="productname">Product *</label>\n' +
                                                                product_label +
                                                                '<input type="text" class="inner form-control" value="'+ data_list.product_name +'" readonly />\n' +
                                                                '<input id="product_entries_'+serial+'" name="product_entries[]" type="hidden" value="'+ data_list.product_entry_id +'" readonly />\n' +
                                                            '</div>\n' +

                                                            '<input id="main_unit_id_'+ serial +'" type="hidden" class="inner form-control" value="'+ data_list.unit_id +'" name="main_unit_id[]" />\n' +
                                                            '<input id="main_stock_'+ serial +'" type="hidden" class="inner form-control" value="'+ quantity +'" name="main_stock[]" />\n' +

                                                            '<div style="padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                                unit_label +
                                                                '<select style="width: 100%;cursor: pointer" name="converted_unit_id[]" class="inner form-control CUID" id="converted_unit_id_'+serial+'" required onchange="getConversionParam('+serial+')">\n' +
                                                                unit_list +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                                quantity_label  +
                                                                '<input id="quantity_'+ serial +'" type="text" class="inner form-control" value="'+ quantity +'" readonly />\n' +
                                                                '<span id="main_unit_show_'+serial+'" style="color: black">' + data_list.converter_unit + '</span>' +
                                                            '</div>\n' +

                                                            '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Damage Qty</label>\n' +
                                                                return_label +
                                                                return_quantity +
                                                            '</div>\n' +

                                                            '<div style="margin-bottom: 5px;padding-left: 10px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs" for="productname">Rate</label>\n' +
                                                                rate_label  +
                                                                '<input type="text" name="rate[]" class="inner form-control" id="rate_'+ serial +'" placeholder="Rate" value="'+ data_list.rate +'" required  readonly/>\n' +
                                                            '</div>\n' +

                                                            '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                                '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                                amount_label +
                                                                '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+ serial +'" placeholder="Total" value="0" readonly/>\n' + 
                                                            '</div>\n' +

                                                        '</div>\n' +

                                                        '<hr style="margin-top: 5px !important;margin-bottom: 5px !important;background: #000!important;">';

                                                    serial++;
                                                    
                                 header = '';
                            }  
                        }
                        else
                        {

                        }                    
                    });
                });

                $("#return_product_list").empty();
                $("#return_product_list").append(return_product_list);

                calculate(1);
            });
        }

        function calculate(x)
        {
            var quantity                = $("#quantity_"+x).val();
            var return_quantity         = $("#return_quantity_"+x).val();
            var rate                    = $("#rate_"+x).val();

            if (quantity == '')
            {
                var quantityCal         = 0;
            }
            else
            {
                var quantityCal         = $("#quantity_"+x).val();
            }

            if (return_quantity == '')
            {
                var returnQuantityCal   = 0;
            }
            else
            {
                var returnQuantityCal   = $("#return_quantity_"+x).val();
            }

            if (rate == '')
            {
                var rateCal             = 0;
            }
            else
            {
                var rateCal             = $("#rate_"+x).val();
            }

            //Calculating Subtotal Amount

            var AmountIn  =  (parseFloat(rateCal)*parseFloat(returnQuantityCal));

            $("#amount_"+x).val(parseFloat(AmountIn).toFixed(2));

            var totalAmount       = 0;
            $('.amount').each(function()
            {
                totalAmount       += parseFloat($(this).val());
            });

            $("#total_amount").val(0);
            $("#total_amount").val(totalAmount.toFixed());
        }
    </script>
@endsection