@extends('layouts.app')

@section('title', 'Show')

@push('styles')
<style type="text/css">
    .pt-2, .py-2 {
         padding-top: 0rem!important; 
    }

    .mt-3, .my-3 {
         margin-top: 0rem!important; 
    }  

    address {
         margin-bottom: 0rem; 
    }
</style>
@endpush

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Issues</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Issues</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">{{ $user_info['organization_name'] }}</h2>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_number'] }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-4 col-6">
                                        <address>
                                            <strong>Issued To:</strong><br>
                                            {{ $issue->srName->name }}
                                            @if($issue->srName->address != null)
                                               <br> <?php echo $issue->srName->address; ?> <br>
                                            @endif
                                            @if($issue->srName->address == null)
                                                <br>
                                            @endif
                                            {{ $issue->srName->phone }}
                                        </address>
                                    </div>

                                    <div class="col-sm-4 hidden-xs">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div style="font-size: 16px" class="col-sm-4 col-6 text-sm-right">
                                        <address>
                                            <strong>Issue Date:</strong><br>
                                            {{ date('d-m-Y', strtotime($issue['issue_date'])) }}<br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6 hidden-xs">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Issue summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Issue # {{ 'SIN - ' . str_pad($issue['issue_number'], 6, "0", STR_PAD_LEFT) }}</h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-nowrap">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th style="width: 70px;">No.</th>
                                                <th>Item</th>
                                                <th class="text-right">Quantity</th>
                                                <th class="text-right">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">
                                            @if(!empty($entries) && ($entries->count() > 0))
                                            <?php $i = 0; $total_amount = 0; ?>
                                            @foreach($entries as $key => $value)
                                            <?php 
                                                $i++; 
                                                $total_qty = 0;
                                                $total_amo = 0;
                                                foreach ($value as $key1 => $value1)
                                                {
                                                    $total_qty = $total_qty + $value1['quantity'];
                                                    $total_amo = $total_amo + $value1['amount'];
                                                }

                                                $total_amount = $total_amount + $total_amo;
                                            ?>
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td style="text-align:left;">
                                                    <?php echo productNameShow($key); ?>
                                                </td>
                                                <td class="text-right">{{ $total_qty }} <?php echo productUnitNameShow($key); ?></td>
                                                <td class="text-right">{{ $total_amo }}</td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th style="font-size: 18px" class="text-right" colspan="3">Total</th>
                                                <th style="font-size: 18px" class="text-right">{{ $total_amount }}</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                @if($issue['issue_note'] != null)
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 style="font-size: 16px"><strong>Note :</strong> {{ $issue['issue_note'] }}</h6>
                                    </div>
                                </div>
                                @endif

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

<script type="text/javascript">
    $( document ).ready(function() {
        // javascript:window.print();
    });

    window.onafterprint = function(e){
        var site_url        = $('.site_url').val();
        window.location.replace(site_url + '/srissues');
    };
</script>

@endsection