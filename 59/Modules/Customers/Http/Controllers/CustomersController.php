<?php

namespace Modules\Customers\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Customers;
use App\Models\Users;
use App\Models\CustomerTypes;
use Response;
use DB;

class CustomersController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $customers  = Customers::orderBy('created_at', 'DESC')->get();
        $users      = Users::orderBy('created_at', 'DESC')->get();
        $types      = CustomerTypes::orderBy('created_at', 'DESC')->get();

        return view('customers::index', compact('customers', 'users', 'types'));
    }

    public function create()
    {
        return view('customers::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                              = new Customers;
            $customers->name                        = $data['customer_name'];
            $customers->address                     = $data['address'];
            $customers->phone                       = $data['mobile_number'];
            $customers->nid_number                  = $data['nid_number'];
            $customers->alternative_contact         = $data['alternative_mobile_number'];
            $customers->contact_type                = $data['contact_type'];
            $customers->joining_date                = date('Y-m-d', strtotime($data['joining_date']));
            $customers->designation                 = $data['designation'];
            $customers->salary                      = $data['salary'];
            $customers->customer_type               = 1;
            $customers->branch_id                   = $branch_id;
            $customers->user_id                     = $data['user_id'];

            if($request->hasFile('image'))
            {
                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->image       = $logoUrl;
            }

            if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
            {
                $customers->opening_balance         = $data['opening_balance'];
            }

            $customers->created_by                  = $user_id;

            if ($customers->save())
            {   
                if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
                {   
                    openingBalanceStore($data['opening_balance'], $customers['id'], $data['contact_type']);
                }

                DB::commit();
                
                if ((isset($data['contact_type_reference'])))
                {
                    return redirect()->route('customers_index','contact_type='.$data['contact_type_reference'])->with("success","Contact Added Successfully !!");
                }
                else
                {
                    return back()->with("success","Contact Added Successfully !!");
                }
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('customers::show');
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $find_customer  = Customers::find($id);
        $customers      = Customers::orderBy('created_at', 'DESC')->get();
        $users          = Users::orderBy('created_at', 'DESC')->get();
        $types          = CustomerTypes::orderBy('created_at', 'DESC')->get();
        $find_type      = CustomerTypes::get();

        return view('customers::edit', compact('customers', 'find_customer', 'users', 'types', 'find_type'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth()->user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                              = Customers::find($id);
            $customers->name                        = $data['customer_name'];
            $customers->address                     = $data['address'];
            $customers->phone                       = $data['mobile_number'];
            $customers->nid_number                  = $data['nid_number'];
            $customers->alternative_contact         = $data['alternative_mobile_number'];
            $customers->contact_type                = $data['contact_type'];
            $customers->joining_date                = date('Y-m-d', strtotime($data['joining_date']));
            $customers->designation                 = $data['designation'];
            $customers->salary                      = $data['salary'];
            $customers->customer_type               = 1;
            $customers->branch_id                   = $branch_id;
            $customers->user_id                     = $data['user_id'];

            if($request->hasFile('image'))
            {   
                if ($customers->image != null)
                {
                    unlink('public/'.$customers->image);
                }

                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->image       = $logoUrl;
            }

            if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
            {
                $customers->opening_balance         = $data['opening_balance'];
            }

            $customers->updated_by                  = $user_id;

            if ($customers->save())
            {   
                if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
                {   
                    openingBalanceUpdate($data['opening_balance'], $customers['id'], $data['contact_type']);
                }

                DB::commit();

                if ((isset($data['contact_type_reference'])))
                {
                    return redirect()->route('customers_index','contact_type='.$data['contact_type_reference'])->with("success","Contact Updated Successfully !!");
                }
                else
                {
                    return redirect()->route('customers_index')->with("success","Contact Updated Successfully !!");
                }
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function customerListLoad()
    {
        $branch_id      = Auth()->user()->branch_id;
        $data           = Customers::leftjoin('users', 'users.id', 'customers.user_id')
                                ->orderBy('customers.created_at', 'DESC')
                                ->when($branch_id != 1, function ($query) use ($branch_id) {
                                    return $query->where('customers.branch_id', $branch_id);
                                })
                                ->whereNotIn('customers.id', [1,2])
                                ->select('customers.*',
                                         'users.name as user_name')
                                // ->take(100)
                                ->get();

        return Response::json($data);
    }

    public function customerListSearch($id)
    {
        $branch_id      = Auth()->user()->branch_id;
        if ($id != 'No_Text')
        {
            $data           = Customers::leftjoin('users', 'users.id', 'customers.user_id')
                                        ->where('customers.name', 'LIKE', "%$id%")
                                        ->orWhere('customers.phone', 'LIKE', "%$id%")
                                        ->orWhere('customers.alternative_contact', 'LIKE', "%$id%")
                                        ->orWhere('customers.address', 'LIKE', "%$id%")
                                        ->orWhere('customers.nid_number', 'LIKE', "%$id%")
                                        ->orderBy('customers.created_at', 'DESC')
                                        ->when($branch_id != 1, function ($query) use ($branch_id) {
                                            return $query->where('customers.branch_id', $branch_id);
                                        })
                                        ->select('customers.*',
                                                 'users.name as user_name')
                                        // ->take(100)
                                        ->get();
        }
        else
        {
            $data           = Customers::leftjoin('users', 'users.id', 'customers.user_id')
                                        ->orderBy('customers.created_at', 'DESC')
                                        ->when($branch_id != 1, function ($query) use ($branch_id) {
                                            return $query->where('customers.branch_id', $branch_id);
                                        })
                                        ->select('customers.*',
                                                 'users.name as user_name')
                                        // ->take(100)
                                        ->get();
        }

        return Response::json($data);
    }

    //Customer Types
    public function indexType()
    {
        $types  = CustomerTypes::orderBy('created_at', 'DESC')->get();

        return view('customers::types.index', compact('types'));
    }

    public function storeType(Request $request)
    {
        $rules = array(
            'name'     => 'required',
            'status'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                              = new CustomerTypes;
            $customers->name                        = $data['name'];
            $customers->status                      = $data['status'];
            $customers->created_by                  = $user_id;

            if ($customers->save())
            {   
                DB::commit();
                return back()->with("success","Customer Type Added Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function editType($id)
    {
        $find_type  = CustomerTypes::find($id);
        $types      = CustomerTypes::orderBy('created_at', 'DESC')->get();

        return view('customers::types.edit', compact('types', 'find_type'));
    }

    public function updateType(Request $request, $id)
    {
        $rules = array(
            'name'     => 'required',
            'status'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth()->user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                              = CustomerTypes::find($id);
            $customers->name                        = $data['name'];
            $customers->status                      = $data['status'];
            $customers->updated_by                  = Auth::user()->id;

            if ($customers->save())
            {   
                 DB::commit();
                return redirect()->route('customers_type_index')->with("success","Customer Type Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }
}
