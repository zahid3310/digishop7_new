@extends('layouts.app')

@section('title', 'Add Trade License')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Add Tax Holder</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Registers</a></li>
                                    <li class="breadcrumb-item active">Add Trade License</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('update_trade_license',$data['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div class="row">
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">License No *</label>
                                        <input name="license_no" type="text" class="form-control" placeholder="লাইসেন্স নং" value="{{$data->license_no}}">
                                    </div>                                    

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">License ID *</label>
                                        <input name="license_id" type="text" class="form-control" placeholder="লাইসেন্স আইডি" value="{{$data->license_id}}">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Name of business organization *</label>
                                        <input name="business_name" type="text" class="form-control" placeholder="ব্যবসা প্রতিষ্ঠানের নাম" value="{{$data->business_name}}">
                                    </div>                                    

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Owner name *</label>
                                        <input name="owner_name" type="text" class="form-control" placeholder="মালিকের নাম" value="{{$data->owner_name}}">
                                    </div>                                    

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Type of business *</label>
                                        <input name="type_of_business" type="text" class="form-control" placeholder="ব্যবসার ধরন" value="{{$data->type_of_business}}">
                                    </div>                                    

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Ward no *</label>
                                        <input name="ward_no" type="text" class="form-control" placeholder="ওয়ার্ড নং" value="{{$data->ward_no}}">
                                    </div>


                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Road / Mouza / Area / Mahalla *</label>
                                        <input name="area_name" type="text" class="form-control" placeholder="রাস্তা/মৌজা/এলাকা/মহল্লা" value="{{$data->area_name}}">
                                    </div>
                                    

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Address of business organization</label>
                                        <input name="business_address" type="text" class="form-control" placeholder="ব্যবসা প্রতিষ্ঠানের ঠিকানা" value="{{$data->business_address}}">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Mobile No</label>
                                        <input name="mobile_number" type="number" class="form-control" placeholder="মোবাইল নং" value="{{$data->mobile_number}}">
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('customers_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Contacts</h4>

                                <div style="margin-right: 10px" class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-1">Search : </div>
                                    <div class="col-md-2">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Sl</th>
                                            <th>License No</th>
                                            <th>License ID</th>
                                            <th>Name of business organization</th>
                                            <th>Owner name</th>
                                            <th>Type of business</th>
                                            <th>Ward no</th>
                                            <th>Road / Mouza / Area / Mahalla</th>
                                            <th>Address of business organization</th>
                                            <th>Mobile No</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($lists as $key=> $list)
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td>{{$list->license_no}}</td>
                                            <td>{{$list->license_id}}</td>
                                            <td>{{$list->business_name}}</td>
                                            <td>{{$list->owner_name}}</td>
                                            <td>{{$list->type_of_business}}</td>
                                            <td>{{$list->ward_no}}</td>
                                            <td>{{$list->area_name}}</td>
                                            <td>{{$list->business_address}}</td>
                                            <td>{{$list->mobile_number}}</td>
                                            <td>
  
                                                <div class="dropdown">
                                                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                        <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" style="">
                                                        <a class="dropdown-item" href="{{ route('edit_trade_license', $list['id']) }}">Edit</a>
                                                    </div>
                                                </div>
        
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
