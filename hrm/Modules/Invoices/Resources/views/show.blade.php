<!DOCTYPE html >
<html>

<head>
    <meta charset="utf-8">
    <title>Sales Invoice</title>
    <link href="https://fonts.maateen.me/bangla/font.css" rel="stylesheet">

    <style>
        *{margin:0;padding:0;outline:0}

        body {
            font-size:14px;
            line-height:18px;
            color:#000;
            height:292mm;
            width: 203mm;/*297*/
            margin:0 auto;
            font-family: 'Bangla', Arial, sans-serif !important;  
        }

        table,th {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 1px;
        }

        td {
            border-left: 1px solid black;
            border-right: 1px solid black;
            padding: 6px;
        }

        .bg_templete{
            background:url(images/bg3.png) no-repeat;
            background-size: 100%;
            -webkit-print-color-adjust: exact;
        }

        .templete{
            width:90%;
            background:none;
            margin:0 auto;
        }
        
        .clear{
            overflow:hidden;
        }

        .text-center{
            text-align: center;
        }

        .text-end{
            text-align: end;
        }

        .item2 {
            grid-area: com_logo;
        }

        .item3 {
            grid-area: com_info;
        }

        .item4 {
            grid-area: other_info;
        }

        .grid-container {
            display: grid;
            grid-template-areas: "com_logo com_info other_info ";
            /*padding-top: 10px;*/
        }

        .grid-container div {
            text-align: center;
        }

        .grid-container-aline-end {
            text-align: end !important;
        }

        .grid-container-aline-start {
            text-align: start !important;
        }

        .com_logo_img {
            height: 50px;
            width: 50px;
        }

        .tr-height{
            padding: 10px;
        }

        .item11 { grid-area: sig_text2; }
        .item22 { grid-area: some_info; }
        .item33 { grid-area: sig1; }
        .item44 { grid-area: sig2; }
        .item55 { grid-area: sig_text1; }

        .grid-container-1 {
            display: grid;
            grid-template-areas:
            ' sig1 sig1 some_info some_info sig2 sig2'
            ' sig_text1 sig_text1 some_info some_info sig_text2 sig_text2';
        }

        .signaturesection1 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 10px;
        }

        .signaturesection2 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 10px;
        }

        @page {
            size: A4;
            page-break-after: always;
        }
    </style>
</head>

<body>
    <div class="bg_templete clear">

        <div class="templete clear">

            <br>
            <br>
            <p style="line-height: 2;text-align: center;padding-left: 115px"><span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 13px;margin-right: 130px;background-color: #0000ff1a">Cash Memo</span></p>
            <div class="grid-container">
                <div class="item2 grid-container-aline-start">
                    <img class="com_logo_img" src="{{ url('public/'.userDetails()->logo) }}"alt="qr sample"
                    />
                </div>

                <div class="item3">
                    <h2 style="line-height: 1;font-size: 25px;color: black;padding-left: 63px">{{ userDetails()->organization_name }}</h2>
                    <p style="line-height: 2;text-align: center;padding-left: 60px"><span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 10px;color: black;background-color: #0000ff1a">Pro. Md. Khairul Hossain</span></p>
                </div>

                <div class="item4 grid-container-aline-end">

                    <p style="padding-right: 0px;line-height: 1;font-size: 14px"><span style="font-weight: bold;color: black">Cell: </span>
                        <span style="font-weight: bold;color: black">01311-692841</span> <br>
                        <span style="font-weight: bold;color: black">01311-692850</span> <br>
                        <span style="font-weight: bold;color: black">01796-209939</span>
                    </p>  
                </div>
            </div>

            <div class="row">
                <h4 style="line-height: 1.1;font-size: 13px;text-align: center;color: black;font-family: italic">Ceramics, Tiles, Marble, Stone, Sanitary Ware Supplier & Agency</h4>
                <p style="line-height: 1.1;font-size: 13px;text-align: center;color: black">Dealer : Mir, Star, Gratewall, D.B.L & B.H.L Ceramic Ltd.</p>
                <p style="line-height: 1.1;font-size: 13px;text-align: center;color: black">{{ userDetails()->address }}</p>
                <p style="line-height: 1.1;font-size: 13px;text-align: center;color: black"><span>Email: {{ userDetails()->email }}</span></p>
            </div>

            <hr style="margin-top: 10px">
            
            <!-- <div style="padding-top: 5px;text-align: center" class="col-md-12"><input type="checkbox"> Customer Copy &nbsp;&nbsp;&nbsp;<input type="checkbox"> Office Copy</div> -->

            <div style="font-size: 12px;padding-top: 5px;line-height: 1.5">
                <span style="padding: 3px;color: black;border: 1px solid black;float: left;background-color: #0000ff1a">Invoice No.  &nbsp;</span>  
                <span style="padding: 2px;float: left;width: 150px">{{ $invoice['invoice_number'] }}</span>  

                <span style="padding: 2px;float: right;width: 135px">{{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}</span>  
                <span style="padding: 3px;color: black;border: 1px solid black;float: right;background-color: #0000ff1a">Date  &nbsp;&nbsp;&nbsp;</span>
            </div>

            <br>
            <br>

            <div style="font-size: 12px;line-height: 1.5">
                <span style="padding: 3px;color: black;border: 1px solid black;float: left;background-color: #0000ff1a">Name &nbsp;&nbsp;</span>  
                <span style="padding: 2px;float: left;width: 435px">{{ $invoice['customer_name'] != null ? $invoice['customer_name'] : $invoice['contact_name'] }}</span>  

                <span style="padding: 2px;float: right;width: 135px">{{ $invoice['contact_phone'] != null ? '.'.$invoice['contact_phone'] : '.'.$invoice['contact_phone'] }}</span>  
                <span style="padding: 3px;color: black;border: 1px solid black;float: right;background-color: #0000ff1a">Phone  &nbsp;</span>
            </div>

            <br>
            <br>

            <div style="font-size: 12px;line-height: 1.5">
                <span style="padding: 3px;color: black;border: 1px solid black;float: left;background-color: #0000ff1a">Address </span>
                <span style="padding: 2px;float: left;width: 634px">{{ $invoice['customer_address'] != null ? $invoice['customer_address'] : $invoice['contact_address'] . ' .' }}</span>
            </div>

            <br>
            <br>

            <div style="padding-top: 0px;padding-bottom: 0px">
                <table style="width: 100%">
                    <tr style="background-color: #0000ff1a">
                        <th style="font-size: 12px;width: 5%;color: black;font-weight: normal">No.</th>
                        <th style="font-size: 12px;width: 34%;color: black;font-weight: normal">Description</th>
                        <th style="font-size: 12px;width: 16%;color: black;font-weight: normal">Code</th>
                        <th style="font-size: 12px;width: 10%;color: black;font-weight: normal">Grade</th>
                        <th style="font-size: 12px;width: 10%;color: black;font-weight: normal">Quantity</th>
                        <th style="font-size: 12px;width: 10%;color: black;font-weight: normal">Rate</th>
                        <th style="font-size: 12px;width: 15%;color: black;font-weight: normal">Taka</th>
                    </tr>

                    @if($entries->count() > 0)

                    <?php
                        $total_amount  = 0;
                        $total_sft     = 0;
                    ?>

                    @foreach($entries as $key => $value)
                    @if($value->type == 1)
                    <?php
                        $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                        $variation_name = ProductVariationName($value['product_entry_id']);

                        if ($value['product_code'] != null)
                        {
                            $productCode  = $value['product_code'];
                        }
                        else
                        {
                            $productCode  = '';
                        }

                        if ($value['product_name'] != null)
                        {
                            $category  = ' - '.$value['product_name'];
                        }
                        else
                        {
                            $category  = '';
                        }

                        if ($value['brand_name'] != null)
                        {
                            $brandName  = $value['brand_name'];
                        }
                        else
                        {
                            $brandName  = '';
                        }

                        if (($value['height'] != null) && ($value['width'] != null))
                        {
                            $dimension  = ' - '.$value['height'] . ' X ' . $value['width'];
                        }
                        else
                        {
                            $dimension  = '';
                        }

                        if ($value['unit_name'] != null)
                        {
                            $unit  = ' '.$value['unit_name'];
                        }
                        else
                        {
                            $unit  = ' SFT';
                        }

                        if ($variation_name != null)
                        {
                            $variation  = ' '.$variation_name;
                        }
                        else
                        {
                            $variation  = '';
                        }

                        $pre_dues = $invoice['previous_due'];
                        $net_paya = round($total_amount, 2);
                        $paid     = round($invoice['cash_given'], 2);
                        $dues     = round($net_paya - $paid, 2);
                    ?>

                    <tr class="tr-height">
                        <td style="text-align: center;font-size: 12px;line-height: 0.5">{{ $key + 1 }}</td>
                        <td style="padding-left: 10px;font-size: 12px;line-height: 0.5">{{  $value->productEntries->name . $dimension }}</td>
                        <td style="padding-left: 10px;font-size: 12px;line-height: 0.5">{{ $productCode }}</td>
                        <td style="padding-left: 10px;font-size: 12px;line-height: 0.5;text-align: center">{{ $value->productEntries->grade_id != null ? $value->productEntries->grade->name : '' }}</td>
                        <td style="padding-left: 10px;text-align: center;font-size: 12px;line-height: 0.5">{{ $value['quantity'] }}</td>
                        <td style="text-align: center;font-size: 12px;line-height: 0.5">{{ $value['contact_type'] != 4 ? $value['rate'] : '' }}</td>
                        <td style="text-align: center;font-size: 12px;line-height: 0.5">{{ $value['contact_type'] != 4 ? round($value['total_amount'], 2) : '' }}</td>

                        <?php
                            $total_sft     = $total_sft + $value['quantity'];
                        ?>
                    </tr>
                    @else
                    <?php
                        $variation_name = ProductVariationName($value['product_entry_id']);

                        if ($value['product_code'] != null)
                        {
                            $productCode  = $value['product_code'];
                        }
                        else
                        {
                            $productCode  = '';
                        }

                        if ($value['product_name'] != null)
                        {
                            $category  = ' - '.$value['product_name'];
                        }
                        else
                        {
                            $category  = '';
                        }

                        if ($value['brand_name'] != null)
                        {
                            $brandName  = $value['brand_name'];
                        }
                        else
                        {
                            $brandName  = '';
                        }

                        if ($variation_name != null)
                        {
                            $variation  = ' '.$variation_name;
                        }
                        else
                        {
                            $variation  = '';
                        }

                        $total_amount   = $total_amount + ($value->rate*$value->quantity);
                        $pre_dues       = round($invoice['previous_due'], 2);
                        $net_paya       = round($total_amount, 2);
                        $paid           = round($invoice['cash_given'], 2);
                        $dues           = round($net_paya - $paid, 2);
                    ?>
                    <tr class="tr-height">
                        <td style="text-align: center;font-size: 12px;line-height: 1.5">{{ $key + 1 }}</td>
                        <td style="padding-left: 10px;font-size: 12px;line-height: 1.5">{{ $value->productEntries->name }}</td>
                        <td style="padding-left: 10px;font-size: 12px;line-height: 1.5">{{ $productCode }}</td>
                        <td style="padding-left: 10px;font-size: 12px;line-height: 1.5;border-top: 1px solid black;border-bottom: 1px solid black;text-align: center" colspan="2">{{ $value->quantity . ' ' . $value->convertedUnit->name }}</td>
                        <td style="text-align: center;font-size: 12px;line-height: 1.5">{{ round($value->rate, 2) }}</td>
                        <td style="text-align: center;font-size: 12px;line-height: 1.5">{{ round($value->quantity*$value->rate, 2) }}</td>
                    </tr>
                    @endif
                    @endforeach
                    @endif

                    <?php
                        if ($invoice['vat_type'] == 0)
                        {
                            $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                        }
                        else
                        {
                            $vat_amount  = $invoice['total_vat'];
                        }

                        if ($invoice['total_discount_type'] == 0)
                        {
                            $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                        }
                        else
                        {
                            $discount_on_total_amount  = $invoice['total_discount_amount'];
                        }
                    ?>

                    <tr>
                        <th style="text-align: right" colspan="4"><strong>Total</strong></th>
                        <th style="text-align: center" colspan="1"><strong>{{ round($total_sft, 2) }}</strong></th>
                        <th style="text-align: right;font-size: 12px" colspan="1"><strong></strong></th>
                        <th style="text-align: center">{{ $net_paya != 0 ? round($net_paya - $invoice['total_discount'], 2) : '' }}</th>
                    </tr>

                    <tr>
                        <th style="text-align: right;border-bottom-color: white;border-left-color: white" colspan="6"><strong>Vat</strong></th>
                        <th style="text-align: center">{{ round($vat_amount, 2) }}</th>
                    </tr>

                    <tr>
                        <th style="text-align: right;border-bottom-color: white;border-left-color: white" colspan="6"><strong>Total Discount</strong></th>
                        <th style="text-align: center">{{ round($discount_on_total_amount + $invoice['total_discount'], 2) }}</th>
                    </tr>
                    
                    <tr>
                        <th style="text-align: right;border-bottom-color: white;border-left-color: white" colspan="6"><strong>Shipping Cost</strong></th>
                        <th style="text-align: center">{{ round($invoice['shipping_cost'], 2) }}</th>
                    </tr>

                    <tr>
                        <th style="text-align: right;border-bottom-color: white;border-left-color: white" colspan="6"><strong>Net Payable</strong></th>
                        <th style="text-align: center">{{ $net_paya - $invoice['total_discount'] - $discount_on_total_amount + $vat_amount + $invoice['shipping_cost'] != 0 ? round($net_paya - $invoice['total_discount'] - $discount_on_total_amount + $vat_amount + $invoice['shipping_cost'], 2) : '' }}</th>
                    </tr>

                    <tr>
                        <th style="text-align: right;border-bottom-color: white;border-left-color: white" colspan="6"><strong>Paid</strong></th>
                        <th style="text-align: center">{{ $paid != 0 ? round($paid, 2) : '' }}</th>
                    </tr>

                    <tr>
                        <th style="text-align: right;border-bottom-color: white;border-left-color: white" colspan="6"><strong>Current Due</strong></th>
                        <th style="text-align: center">{{ $dues - $discount_on_total_amount - $invoice['total_discount'] + $invoice['shipping_cost'] != 0 ? round($dues - $discount_on_total_amount - $invoice['total_discount'] + $invoice['shipping_cost'], 2) : '' }}</th>
                    </tr>

                    <tr>
                        <th style="text-align: right;border-bottom-color: white;border-left-color: white" colspan="6"><strong>Previous Due</strong></th>
                        <th style="text-align: center">{{ $pre_dues != 0 ? round($pre_dues, 2) : 0 }}</th>
                    </tr>

                    <tr>
                        <th style="text-align: right;border-bottom-color: white;border-left-color: white" colspan="6"><strong>Total Due</strong></th>
                        <th style="text-align: center">{{ $pre_dues + $invoice['invoice_amount'] - $invoice['cash_given'] != 0 ? round($pre_dues + $invoice['invoice_amount'] - $invoice['cash_given'], 2) : 0 }}</th>
                    </tr>
                </table>
            </div>

            <div style="margin-top: 20px" class="grid-container-1">
                <div class="item33">
                    <span style="font-weight: normal;font-size: 12px">In Words :  {{ $value['contact_type'] != 4 ? numberTowords(round($net_paya - $invoice['total_discount'] - $discount_on_total_amount + $vat_amount), 2) : '' }}</span>
                </div> 
            </div>

            <div style="margin-top: 50px" class="grid-container">

                <div class="item2 grid-container-aline-start">
                    <p class="signaturesection1" style="font-size: 12px;text-align: left;color: black;padding: 5px;font-weight: normal">Receiver's Signature</p>
                </div>

                <div class="item3">
                    <p style="font-size: 12px;text-align: center;color: black;padding: 5px;margin-top: 10px;border-top: 1px solid black">Accounts Dept.</p>
                </div>

                <div class="item4 grid-container-aline-end">
                    <p class="signaturesection2" style="font-size: 12px;text-align: right;color: black;padding: 5px;font-weight: normal">Sales Dept.</p>
                </div>
            </div>

            <div class="grid-container">

                <div class="item2 grid-container-aline-start">
                </div>

                <div class="item3">
                    <p style="font-size: 12px;text-align: center;padding: 0px;margin-top: 0px;font-weight: bold">Goods once sold will not be taken back</p>
                </div>

                <div class="item4 grid-container-aline-end">
                </div>
            </div>
        </div>
    </div>
</body>

</html>