<!DOCTYPE html>
<html>

<head>
    <title>Cash Book</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-top:5mm;
            margin-bottom:5mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 50px;font-size: 16px">
                        <p style="line-height: 10px"><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p style="line-height: 10px">{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p style="line-height: 10px">{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Cash Book</h6>
                    </div>

                    <div class="ibox-content">
                        <!-- Header Start -->
                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Header End -->

                        <!-- Sales Statement Start -->
                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 8%">DATE</th>
                                    <th style="text-align: center;width: 9%">V.ID</th>
                                    <th style="text-align: center;width: 12%">PARTICULARS</th>
                                    <th style="text-align: center;width: 15%">DESCRIPTION</th>
                                    <th style="text-align: center;width: 8%">DEBIT</th>
                                    <th style="text-align: center;width: 8%">CREDIT</th>
                                    <th style="text-align: center;width: 10%">BALANCE</th>
                                    <th style="text-align: center;width: 15%">ENTRY BY</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php
                                    $i                = 1;
                                    $total_debit      = 0;
                                    $total_credit     = 0;
                                    $total_balance    = $opening_balance;
                                ?>

                                <tr>
                                    <th colspan="7" style="text-align: right;">Opening Balance</th>
                                    <th style="text-align: right;">{{ number_format($opening_balance,0,'.',',') }}</th>
                                    <th style="text-align: right;"></th>
                                </tr>

                                @foreach($data as $key => $value)

                                <?php
                                    if ($value['debit_credit'] == 0)
                                    {
                                        $credit         = $value['amount'];
                                        $total_credit   = $total_credit + $value['amount'];
                                        $debit          = 0;
                                    }

                                    if ($value['debit_credit'] == 1)
                                    {
                                        $debit          = $value['amount'];
                                        $total_debit    = $total_debit + $value['amount'];
                                        $credit         = 0;
                                    }

                                    $total_balance      = $total_balance + $debit - $credit;

                                    if ($value['transaction_head'] == 'payment-receive')
                                    {
                                        if ($value->invoice_id != null)
                                        {
                                            $trans_number     = 'INV - '.str_pad($value->invoice->invoice_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head = 'Sales of Items';
                                            $transaction_note = $value->note;
                                            $register         = $value->customer->name;
                                            $payment_methode  = $value->account->name;
                                            $text             = 'Received from ';
                                        }
                                        else
                                        {
                                            $trans_number     = '';
                                            $transaction_head = $value->purchase_return_id != null ? 'Purchase Return of Items' : 'Previous Due Collection';
                                            $transaction_note = $value->note;
                                            $register         = $value->customer->name;
                                            $payment_methode  = $value->account->name;
                                            $text             = 'Received from ';
                                        }
                                    }
                                    elseif ($value['transaction_head'] == 'sales-return')
                                    {
                                        $trans_number       = 'SR - '.str_pad($value->salesReturn->sales_return_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = 'Sales Return of items';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = $value->account->name;
                                        $text               = 'Return Back to ';
                                    }
                                    elseif ($value['transaction_head'] == 'payment-made')
                                    {
                                        if ($value->bill_id != null)
                                        {
                                            $trans_number       = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head   = 'Purchase of Items';
                                            $transaction_note   = $value->note;
                                            $register           = $value->customer->name;
                                            $payment_methode    = $value->account->name;
                                            $text               = 'Paid to ';
                                        }
                                        else
                                        {
                                        $trans_number       = '';
                                        $transaction_head   = $value->sales_return_id != null ? 'Sales Return of items' : 'Previous Due Paid';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = $value->account->name;
                                        $text               = 'Paid to ';
                                        }
                                    }
                                    elseif ($value['transaction_head'] == 'purchase-return')
                                    {
                                        $trans_number       = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = $value->purchase_return_id != null ? 'Purchase Return of items' : 'Previous Due Paid';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = $value->account->name;
                                        $text               = 'Received Back from ';
                                    }
                                    elseif ($value['transaction_head'] == 'income')
                                    {
                                        $trans_number       = 'INC - '.str_pad($value->income->income_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = 'Income';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer_id != null ? $value->customer->name : $value->income->account->account_name;
                                        $payment_methode    = $value->account->name;
                                        $text               = $value->customer_id != null ? 'Income From' : 'Received from ';
                                    }
                                    elseif ($value['transaction_head'] == 'expense')
                                    {
                                        $trans_number       = 'EXP - '.str_pad($value->expense->expense_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = 'Expense';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer_id != null ? $value->customer->name : $value->expense->account->account_name;;
                                        $payment_methode    = $value->account->account_name;
                                        $text               = $value->customer_id != null ? 'To ' : 'Against ';
                                    }
                                    elseif ($value['transaction_head'] == 'customer-settlement')
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = 'Customer Settlement';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = $value->account->name;
                                        $text               = 'Return Customer Advance To';
                                    }
                                    elseif ($value['transaction_head'] == 'supplier-settlement')
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = 'Supplier Settlement';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = $value->account->name;
                                        $text               = 'Return Back Advance From ';
                                    }
                                    elseif ($value['transaction_head'] == 'balance-transfer')
                                    {
                                        if ($value['debit_credit'] == 0)
                                        {
                                            $trans_number       = '';
                                            $transaction_head   = 'Balance Transfer';
                                            $transaction_note   = $value->note;
                                            $register           = $value->balanceTransfer->transferTo->account_name;
                                            $payment_methode    = $value->account->account_name;
                                            $text               = 'From Cash To ';
                                        }
                                        else
                                        {
                                            $trans_number       = '';
                                            $transaction_head   = 'Balance Transfer';
                                            $transaction_note   = $value->note;
                                            $register           = ' Cash';
                                            $payment_methode    = $value->account->account_name;
                                            $text               = 'From ' . $value->balanceTransfer->transferFrom->account_name;;
                                        }
                                    }
                                    else
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = '';
                                        $register           = '';
                                        $transaction_note   = '';
                                        $payment_methode    = '';
                                        $text               = '';
                                    }
                                ?>

                                <tr>
                                    <td style="text-align: center;font-size: 12px">{{ $i }}</td>
                                    <td style="text-align: center;font-size: 12px">{{ date('d-m-Y', strtotime($value['date'])) }}</td>
                                    <td style="text-align: center;font-size: 12px">{{ $trans_number }}</td>
                                    <td style="text-align: center;font-size: 12px"><strong>{{ $transaction_head }}</strong> <br> {{ $transaction_note }}</td>
                                    <td style="text-align: left;font-size: 12px">{{ $text . ' ' }} <strong>{{ $register }}</strong></td>
                                    <td style="text-align: right;font-size: 12px">{{ number_format($credit,0,'.',',') }}</td>
                                    <td style="text-align: right;font-size: 12px">{{ number_format($debit,0,'.',',') }}</td>
                                    <td style="text-align: right;font-size: 12px">{{ $total_balance >= 0 ? number_format($total_balance,0,'.',',') : '('.number_format(abs($total_balance),0,'.',',').')'  }}</td>
                                    <td style="text-align: left;font-size: 12px">{{ isset($value->createdBy->name) ? $value->createdBy->name : '' }}</td>
                                </tr>

                                <?php
                                    $i++;
                                ?>

                                @endforeach
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="5" style="text-align: right;">TOTAL</th>
                                    <th style="text-align: right;">{{ number_format($total_credit,0,'.',',') }}</th>
                                    <th style="text-align: right;">{{ number_format($total_debit,0,'.',',') }}</th>
                                    <th style="text-align: right;">{{ $total_balance >= 0 ? number_format($total_balance,0,'.',',') : '('.number_format(abs($total_balance),0,'.',',').')'  }}</th>
                                    <th style="text-align: right;"></th>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- Sales Statement End -->

                        <!-- Footer Start -->
                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>