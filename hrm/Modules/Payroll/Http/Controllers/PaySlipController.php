<?php

namespace Modules\Payroll\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\PaySlips;
use App\Models\Customers;
use App\Models\MonthlySalarySheets;
use App\Models\Payments;
use DB;
use Response;

class PaySlipController extends Controller
{
    public function index()
    {
        $employee_id    = isset($_GET['employee_id']) ? $_GET['employee_id'] : 0;
        $month          = isset($_GET['month']) ? $_GET['month'] : 0;
        $year           = isset($_GET['year']) ? $_GET['year'] : 0;

        if ($employee_id == 0 && $month == 0 && $year == 0)
        {
            $salary_statements = collect();
        }
        else
        {
            $salary_statements = MonthlySalarySheets::when($employee_id != 0, function ($query) use ($employee_id) {
                                                    return $query->where('employee_id', $employee_id);
                                                })
                                                ->get();
        }

        $customer_name  = Customers::find($employee_id);

        return view('payroll::PaySlips.index', compact('salary_statements', 'customer_name'));
    }

    public function create()
    {
        $employees  = Customers::where('contact_type', 2)->get();

        return view('payroll::increaments.create', compact('employees'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'          => 'required|string',
            'position'      => 'nullable|integer',
            'basic_salary'  => 'nullable|numeric',
            'house_rent'    => 'nullable|numeric',
            'medical'       => 'nullable|numeric',
            'convence'      => 'nullable|numeric',
            'food'          => 'nullable|numeric',
            'mobile_bill'   => 'nullable|numeric',
            'others'        => 'nullable|numeric',
            'gross'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $grades                = new SalaryGrades;
            $grades->name          = $data['name'];
            $grades->position      = $data['position'];
            $grades->basic         = $data['basic_salary'];
            $grades->house_rent    = $data['house_rent'];
            $grades->medical       = $data['medical'];
            $grades->convence      = $data['convence'];
            $grades->food          = $data['food'];
            $grades->mobile        = $data['mobile_bill'];
            $grades->others        = $data['others'];
            $grades->gross         = $data['gross'];
            $grades->created_by    = $user_id;

            if ($grades->save())
            {   
                DB::commit();
                return back()->with("success","Grade Added Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        return view('payroll::show');
    }

    public function edit($id)
    {
        $find_grade = SalaryGrades::find($id);

        return view('payroll::SalaryGrades.edit', compact('find_grade'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required|string',
            'position'      => 'nullable|integer',
            'basic_salary'  => 'nullable|numeric',
            'house_rent'    => 'nullable|numeric',
            'medical'       => 'nullable|numeric',
            'convence'      => 'nullable|numeric',
            'food'          => 'nullable|numeric',
            'mobile_bill'   => 'nullable|numeric',
            'others'        => 'nullable|numeric',
            'gross'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $grades                = SalaryGrades::find($id);
            $grades->name          = $data['name'];
            $grades->position      = $data['position'];
            $grades->basic         = $data['basic_salary'];
            $grades->house_rent    = $data['house_rent'];
            $grades->medical       = $data['medical'];
            $grades->convence      = $data['convence'];
            $grades->food          = $data['food'];
            $grades->mobile        = $data['mobile_bill'];
            $grades->others        = $data['others'];
            $grades->gross         = $data['gross'];
            $grades->updated_by    = $user_id;

            if ($grades->save())
            {   
                DB::commit();
                return redirect()->route('salary_grades_index')->with("success","Grade Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function getEmployeeData($monthly_salary_statement_id)
    {
        $data    = MonthlySalarySheets::find($monthly_salary_statement_id);

        return Response::json($data);
    }

    public function allPaySlips()
    {
        $employee_id    = isset($_GET['employee_id']) ? $_GET['employee_id'] : 0;
        $month          = isset($_GET['month']) ? $_GET['month'] : 0;
        $year           = isset($_GET['year']) ? $_GET['year'] : 0;

        if ($employee_id == 0 && $month == 0 && $year == 0)
        {
            $pay_slips = collect();
        }
        else
        {
            $pay_slips = PaySlips::when($employee_id != 0, function ($query) use ($employee_id) {
                                                    return $query->where('employee_id', $employee_id);
                                                })
                                                ->get();
        }

        $customer_name  = Customers::find($employee_id);

        return view('payroll::PaySlips.all_slips', compact('pay_slips', 'customer_name'));
    }

    public function paySlipPaymentStore(Request $request)
    {
        $rules = array(
            'employee_id_payment'       => 'required|integer',
            'monthly_salary_sheet_id'   => 'required|integer',
            'payment_date'              => 'required|date',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $pay_slips                          = new PaySlips;
            $pay_slips->date                    = date('Y-m-d', strtotime($data['payment_date']));
            $pay_slips->monthly_salary_sheet_id = $data['monthly_salary_sheet_id'];
            $pay_slips->employee_id             = $data['employee_id_payment'];
            $pay_slips->amount                  = $data['paid_amount'];
            $pay_slips->created_by              = $user_id;

            if ($pay_slips->save())
            {   
                $update_stetement           = MonthlySalarySheets::find($data['monthly_salary_sheet_id']);
                $update_stetement->paid     = $update_stetement['paid'] + $data['paid_amount'];
                $update_stetement->save();

                DB::commit();
                return back()->with("success","Payment Created Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function paySlipShow($id)
    {
        $pay_slips = PaySlips::find($id);

        return view('payroll::PaySlips.show', compact('pay_slips'));
    }

    public function paySlipAllShow($id)
    {
        $pay_slip = PaySlips::where('monthly_salary_sheet_id', $id)->get();

        return view('payroll::PaySlips.show_all', compact('pay_slip'));
    }
}
