<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class SalaryStatementTrns extends Model
{  
    protected $table = "salary_statement_trns";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }
}
