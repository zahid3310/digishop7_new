

<?php
    if (isset($_GET['contact_type']) && ($_GET['contact_type'] == 0))
    {
        $title = 'DSR/Customers';
    }

    if (isset($_GET['contact_type']) && ($_GET['contact_type'] == 1))
    {
        $title = 'Suppliers';
    }

    if (isset($_GET['contact_type']) && ($_GET['contact_type'] == 2))
    {
        $title = 'Employees';
    }

    if (isset($_GET['contact_type']) && ($_GET['contact_type'] == 3))
    {
        $title = 'Reference';
    }

    if (!isset($_GET['contact_type']))
    {
        $title = 'Contacts';
    }
?>

<?php $__env->startSection('title', $title); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e($title); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Registers</a></li>
                                    <li class="breadcrumb-item active">Add <?php echo e($title); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('customers_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                <?php echo e(csrf_field()); ?>


                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Name *</label>
                                        <input name="customer_name" type="text" class="form-control" placeholder="Enter Name" required>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Address</label>
                                        <input name="address" type="text" class="form-control" placeholder="Enter Address">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Mobile Number</label>
                                        <input name="mobile_number" type="number" class="form-control" placeholder="Enter Phone Number">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Alternative Phone</label>
                                        <input name="alternative_mobile_number" type="number" class="form-control" placeholder="Enter Aternative Phone">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">NID Number</label>
                                        <input name="nid_number" type="text" class="form-control" placeholder="Enter NID Number">
                                    </div>

                                    <?php if((isset($_GET['contact_type'])) && ($_GET['contact_type'] == 0) || (isset($_GET['contact_type'])) && ($_GET['contact_type'] == 1) || (isset($_GET['contact_type'])) && ($_GET['contact_type'] == 3)): ?>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Opening Balance</label>
                                        <input name="opening_balance" type="number" class="form-control" placeholder="Enter Opening Balance">
                                    </div>
                                    <?php endif; ?>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Contact Type *</label>
                                        <select style="cursor: pointer" name="contact_type" class="form-control" id="contact_type" onchange="additionalFields()" required>
                                            <?php if(isset($_GET['contact_type']) && ($_GET['contact_type'] == 0)): ?>
                                            <option value="0">DSR/Customers</option>
                                            <?php endif; ?>

                                            <?php if(isset($_GET['contact_type']) && ($_GET['contact_type'] == 1)): ?>
                                            <option value="1">Supplier</option>
                                            <?php endif; ?>

                                            <?php if(isset($_GET['contact_type']) && ($_GET['contact_type'] == 2)): ?>
                                            <option value="2">Employee</option>
                                            <?php endif; ?>

                                            <?php if(isset($_GET['contact_type']) && ($_GET['contact_type'] == 3)): ?>
                                            <option value="3">Reference</option>
                                            <?php endif; ?>

                                            <?php if(!isset($_GET['contact_type'])): ?>
                                            <option value="0">DSR/Customers</option>
                                            <option value="1">Supplier</option>
                                            <option value="2">Employee</option>
                                            <option value="3">Reference</option>
                                            <?php endif; ?>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Customer Type *</label>
                                        <select style="cursor: pointer" name="customer_type" class="form-control" id="customer_type" required>
                                            <?php if($types->count() > 0): ?>
                                            <?php $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($type->id); ?>"><?php echo e($type->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>

                                    <?php if(isset($_GET['contact_type'])): ?>
                                        <input type="hidden" name="contact_type_reference" value="<?php echo e($_GET['contact_type']); ?>">
                                    <?php endif; ?>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group additionalFields">
                                        <label for="example-text-input" class="col-form-label">Joining Date</label>
                                        <input id="joining_date" name="joining_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                    </div>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group additionalFields">
                                        <label for="example-text-input" class="col-form-label">Designation</label>
                                        <input name="designation" type="text" class="form-control" placeholder="Enter Designation">
                                    </div>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group additionalFields">
                                        <label for="example-text-input" class="col-form-label">Salary</label>
                                        <input name="salary" type="number" class="form-control" placeholder="Enter Salary">
                                    </div>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group AssociativeContactId">
                                        <label for="example-text-input" class="col-form-label">Associative User ID</label>
                                        <select style="cursor: pointer" name="user_id" class="form-control" id="user_id">
                                            <option value="">--Select User--</option>
                                            <?php if(!empty($users) && ($users->count() > 0)): ?>
                                                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($user->id); ?>"><?php echo e($user->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Image</label>
                                        <input name="image" type="file">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('customers_index')); ?>">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Contacts</h4>

                                <div style="margin-right: 10px" class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-1">Search : </div>
                                    <div class="col-md-2">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Address</th>
                                            <th>Phone</th>
                                            <th>Alternative Phone</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody id="customer_list">
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url       = $('.site_url').val();
            var contact_type   = $('#contact_type').val();

            if (contact_type == 0)
            {
                $('.AssociativeContactId').hide();
                $('.additionalFields').hide();
            }

            if (contact_type == 2)
            {
                $('.additionalFields').show();
                $('.AssociativeContactId').show();
            }
            
            if (contact_type == 1)
            {
                $('.additionalFields').hide();
                $('.AssociativeContactId').hide();
            }

            if (contact_type == 3)
            {
                $('.additionalFields').hide();
                $('.AssociativeContactId').hide();
            }

            $.get(site_url + '/customers/customer/list/load', function(data){

                var customer_list   = '';
                var serial          = 1;
                $.each(data, function(i, customer_data)
                {   
                    var locationValue   = (new URL(location.href)).searchParams.get('contact_type');
                    if (locationValue   != null)
                    {
                        var edit_url = site_url + '/customers/edit/' + customer_data.id + '?contact_type=' + locationValue;
                    }
                    else
                    {
                        var edit_url     = site_url + '/customers/edit/' + customer_data.id;
                    }

                    if (customer_data.phone == null)
                    {
                        var customer_phone   = '';
                    }
                    else
                    {
                        var customer_phone   = customer_data.phone;
                    }

                    if (customer_data.address == null)
                    {
                        var address         = '';
                    }
                    else
                    {
                        var address         = customer_data.address;
                    }

                    if (customer_data.alternative_contact == null)
                    {
                        var alter_contact   = '';
                    }
                    else
                    {
                        var alter_contact   = customer_data.alternative_contact;
                    }

                    if (customer_data.contact_type == 0)
                    {
                        var contact_type    = 'DSR/Customers';
                        var customer_name   = customer_data.name;
                    }
                    
                    if (customer_data.contact_type == 1)
                    {
                        var contact_type    = 'Supplier';
                        var customer_name   = customer_data.name;
                    }

                    if (customer_data.contact_type == 3)
                    {
                        var contact_type    = 'Reference';
                        var customer_name   = customer_data.name;
                    }

                    if (customer_data.contact_type == 2)
                    {
                        var contact_type   = 'Employee';

                        if (customer_data.designation != null)
                        {
                            var designation   = customer_data.designation;
                        }
                        else
                        {
                            var designation   = '';
                        }

                        if (customer_data.salary != null)
                        {
                            var salary = customer_data.salary;
                        }
                        else
                        {
                            var salary = '';
                        } 

                        if (customer_data.user_name != null)
                        {
                            var user_name = customer_data.user_name;
                        }
                        else
                        {
                            var user_name = '';
                        }                      

                        var customer_name  = customer_data.name + "<br>" + "<strong>Designation : </strong>" + designation + "<br>" + "<strong>Salary : </strong>" + salary+ "<br>" + "<strong>User ID : </strong>" + user_name;
                    }

                    if (customer_data.image != null)
                    {
                        var img  = site_url + '/public/' + customer_data.image;
                    }
                    else
                    {
                        var img  = site_url + '/public/' + 'default.png';
                    }

                    if (locationValue != null)
                    {
                        if (customer_data.contact_type == locationValue)
                        {
                            customer_list += '<tr>' +
                                            '<td style="text-align: left">' +
                                                serial +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                '<img style="height: 30px;width: 30px" src="'+ img +'">' +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               customer_name +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               contact_type +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               address +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                customer_phone +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               alter_contact +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                '<div class="dropdown">' +
                                                    '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                        '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                    '</a>' +
                                                    '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                        '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</td>' +
                                        '</tr>';

                            serial++;
                        }
                    }
                    else
                    {
                        customer_list += '<tr>' +
                                        '<td style="text-align: left">' +
                                            serial +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            '<img style="height: 30px;width: 30px" src="'+ img +'">' +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           customer_name +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           contact_type +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           address +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            customer_phone +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           alter_contact +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                        serial++; 
                    }
                });

                $("#customer_list").empty();
                $("#customer_list").append(customer_list);
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var search_text = $('#searchPayment').val();
            var site_url    = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/customers/customer/search/list/' + search_text, function(data){

                var customer_list   = '';
                var serial          = 1;
                $.each(data, function(i, customer_data)
                {          
                    var locationValue   = (new URL(location.href)).searchParams.get('contact_type');
                    if (locationValue   != null)
                    {
                        var edit_url = site_url + '/customers/edit/' + customer_data.id + '?contact_type=' + locationValue;
                    }
                    else
                    {
                        var edit_url     = site_url + '/customers/edit/' + customer_data.id;
                    }

                    if (customer_data.phone == null)
                    {
                        var customer_phone   = '';
                    }
                    else
                    {
                        var customer_phone   = customer_data.phone;
                    }

                    if (customer_data.address == null)
                    {
                        var address         = '';
                    }
                    else
                    {
                        var address         = customer_data.address;
                    }

                    if (customer_data.alternative_contact == null)
                    {
                        var alter_contact   = '';
                    }
                    else
                    {
                        var alter_contact   = customer_data.alternative_contact;
                    }

                    if (customer_data.contact_type == 0)
                    {
                        var contact_type    = 'DSR/Customers';
                        var customer_name   = customer_data.name;
                    }
                    
                    if (customer_data.contact_type == 1)
                    {
                        var contact_type    = 'Supplier';
                        var customer_name   = customer_data.name;
                    }

                    if (customer_data.contact_type == 3)
                    {
                        var contact_type    = 'Reference';
                        var customer_name   = customer_data.name;
                    }

                    if (customer_data.contact_type == 2)
                    {
                        var contact_type   = 'Employee';

                        if (customer_data.designation != null)
                        {
                            var designation   = customer_data.designation;
                        }
                        else
                        {
                            var designation   = '';
                        }

                        if (customer_data.salary != null)
                        {
                            var salary = customer_data.salary;
                        }
                        else
                        {
                            var salary = '';
                        }                       

                        if (customer_data.user_name != null)
                        {
                            var user_name = customer_data.user_name;
                        }
                        else
                        {
                            var user_name = '';
                        }                      

                        var customer_name  = customer_data.name + "<br>" + "<strong>Designation : </strong>" + designation + "<br>" + "<strong>Salary : </strong>" + salary+ "<br>" + "<strong>User ID : </strong>" + user_name;
                    }

                    if (customer_data.image != null)
                    {
                        var img  = site_url + '/public/' + customer_data.image;
                    }
                    else
                    {
                        var img  = site_url + '/public/' + 'default.png';
                    }

                    if (locationValue != null)
                    {
                        if (customer_data.contact_type == locationValue)
                        {
                            customer_list += '<tr>' +
                                            '<td style="text-align: left">' +
                                                serial +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                '<img style="height: 30px;width: 30px" src="'+ img +'">' +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               customer_name +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               contact_type +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               address +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                customer_phone +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               alter_contact +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                '<div class="dropdown">' +
                                                    '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                        '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                    '</a>' +
                                                    '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                        '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</td>' +
                                        '</tr>';
                            serial++;
                        }
                    }
                    else
                    {
                        customer_list += '<tr>' +
                                        '<td style="text-align: left">' +
                                            serial +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            '<img style="height: 30px;width: 30px" src="'+ img +'">' +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           customer_name +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           contact_type +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           address +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            customer_phone +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           alter_contact +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                        serial++; 
                    }
                });

                $("#customer_list").empty();
                $("#customer_list").append(customer_list);
            });
        }

        function additionalFields()
        {
            var contact_type   = $('#contact_type').val();

            if (contact_type == 0)
            {
                $('.AssociativeContactId').hide();
                $('.additionalFields').hide();
            }

            if (contact_type == 2)
            {
                $('.additionalFields').show();
                $('.AssociativeContactId').show();
            }
            
            if (contact_type == 1)
            {
                $('.additionalFields').hide();
                $('.AssociativeContactId').hide();
            }

            if (contact_type == 3)
            {
                $('.AssociativeContactId').hide();
                $('.additionalFields').hide();
            }
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/82/Modules/Customers/Resources/views/index.blade.php ENDPATH**/ ?>