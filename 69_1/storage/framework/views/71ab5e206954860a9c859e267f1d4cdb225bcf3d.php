

<?php
    if (isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 1))
    {
        $title = 'Supplier Settlement';
    }

    if (isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 0))
    {
        $title = 'Customer Settlement';
    }
?>

<?php $__env->startSection('title', $title); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Settlements</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Settlement</a></li>
                                    <li class="breadcrumb-item active">Settlement</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <form id="FormSubmit" action="<?php echo e(route('settlements_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                <?php echo e(csrf_field()); ?>


                <div class="row">
                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
                        <div class="card">
                            <div style="height: 250px;padding-bottom: 0px" class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
                                            <label class="control-label">Contact Type *</label>
                                            <select id="type" style="width: 100%;cursor: pointer" class="form-control" name="type">
                                                <?php if(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 1)): ?>
                                                <option value="1">Supplier</option>
                                                <?php endif; ?>
                                                <?php if(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 0)): ?>
                                                <option value="0">Customer</option>
                                                <?php endif; ?>
                                                
                                                <?php if(!isset($_GET['settlement_type'])): ?>
                                                <option value="0">Customers</option>
                                                <option value="1">Supplier</option>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                    	<div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
	                                        <label class="control-label">Search Contact *</label>
	                                        <select id="contact_id" name="customer_id" style="width: 100%;cursor: pointer" class="form-control select2 contact_id" onchange="searchContact()">
	                                        	<option value="">--Select Contact--</option>
												<?php if(!empty($customers)): ?>
                                                    <?php 
                                                        if (isset($_GET['settlement_type']))
                                                        {
                                                            $customers = $customers->where('contact_type', $_GET['settlement_type']);
                                                        }
                                                        else
                                                        {
                                                            $customers = $customers;
                                                        }
                                                    ?>
													<?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<option <?php if(isset($find_customer)): ?> <?php echo e($find_customer['id'] == $customer['id'] ? 'selected' : ''); ?> <?php endif; ?> value="<?php echo e($customer->id); ?>"><?php echo e($customer->name); ?> <?php echo e($customer->phone != null ? ' | ' . $customer->phone : ''); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												<?php endif; ?>
	                                        </select>
	                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
                        <div class="card">
                            <div style="padding-bottom: 0px" class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

        						<div class="row">
    			                	<div class="col-sm-6">
    			                        <div class="form-group">
    			                            <label for="settlement_date">Settlement Date *</label>
    			                            <input id="settlement_date" name="settlement_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
    			                        </div>
    			                    </div>

    			                    <div class="col-sm-6">
    			                        <div class="form-group">
    			                            <label for="amount">Amount *</label>
    			                            <input id="amount" name="amount" type="text" class="form-control">
    			                        </div>
    			                    </div>

    			                    <div class="col-sm-6 form-group">
    		                            <label class="control-label">Paid Through</label>
                                        <select style="cursor: pointer" name="paid_through" class="form-control select2">
                                            <?php if(!empty($paid_accounts) && ($paid_accounts->count() > 0)): ?>
                                            <?php $__currentLoopData = $paid_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $paid_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($paid_account['id']); ?>"><?php echo e($paid_account['account_name']); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
    		                        </div>

    		                        <div class="col-sm-6">
    			                        <div class="form-group">
    			                            <label for="note">Note</label>
    			                            <input id="note" name="note" type="text" class="form-control">
    			                        </div>
    			                    </div>
    		                	</div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('settlements_create')); ?>">Close</a></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </form>

                <hr style="margin-top: 0px">

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">Settlement List</h4>

                                <div style="margin-right: 10px" class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-1">Search : </div>
                                    <div class="col-md-2">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th>Contact</th>
                                            <th>Paid Through</th>
                                            <th>Note</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="settlement_list"></tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <input type="hidden" id="settlement_id_put">
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Are you sure want to delete the entry ?</label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="delete_btn" class="btn btn-primary waves-effect waves-light delete_btn">Delete</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url    = $('.site_url').val();
            var contact_id  = $('#contact_id').val();

            if (contact_id == '')
            {
                var contact_id_find = 0;
            }
            else
            {
                var contact_id_find = $('#contact_id').val();
            }

            $.get(site_url + '/payments/settlements/get-settlement-data/' + contact_id_find, function(data){

                settlementList(data);
                
            });
        });

        function searchContact()
        {
            var site_url    = $('.site_url').val();
            var contact_id  = $('#contact_id').val();

            $.get(site_url + '/payments/settlements/get-balance/' + contact_id, function(data){

                $("#amount").val(data.balance);
            });

            $.get(site_url + '/payments/settlements/get-settlement-data/' + contact_id, function(data){

                settlementList(data);
            });
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }
    </script>

    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }
    </script>

    <script type="text/javascript">
        function settlementList(data)
        {
            var settlement_list     = '';
            var site_url            = $('.site_url').val();
            var serial              = 0;
            $.each(data, function(i, settlement_data)
            {
                var edit_url    = site_url + '/payments/settlements/edit/' + settlement_data.id + '?settlement_type=' + settlement_data.type;
                var delete_url  = site_url + '/payments/settlements/delete/' + settlement_data.id + '?settlement_type=' + settlement_data.type;

                if (settlement_data.note == null)
                {
                    var note = '';
                }
                else
                {
                    var note = settlement_data.note;
                }

                var locationValue   = (new URL(location.href)).searchParams.get('settlement_type');

                if (settlement_data.type == locationValue)
                {
                    serial++;

                    settlement_list += '<tr>' +
                                        '<input class="form-control paymentId" type="hidden" name="settlement_id[]" value="' +  settlement_data.id + '">' +
                                        '<td style="text-align: left">' +
                                            serial +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            formatDate(settlement_data.date) +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           settlement_data.customer_name +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           settlement_data.paid_through_account_name +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           note +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           settlement_data.amount +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                '<a class="dropdown-item" href="' + delete_url +'" data-toggle="modal" data-target="#myModal" onclick="putId('+settlement_data.id+')">' + 'Delete' + '</a>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                }
            });

            $("#settlement_list").empty();
            $("#settlement_list").append(settlement_list);
        }
    </script>

    <script type="text/javascript">
        function putId(x)
        {
            $('#settlement_id_put').val(x);
        }

        $('.delete_btn').click(function () 
        {
            var site_url            = $('.site_url').val();
            var id                  = $('#settlement_id_put').val();
            window.location.href    = site_url + "/payments/settlements/delete/"+id;
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/69_1/Modules/Payments/Resources/views/settlement_index.blade.php ENDPATH**/ ?>