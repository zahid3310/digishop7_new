

<?php $__env->startSection('title', 'Product Report'); ?>

<?php $__env->startSection('styles'); ?>
    <style type="text/css">
        @media  print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page  {
                margin: 0cm ! important;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Product Report</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Product Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['address']); ?></p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['contact_number']); ?></p>
                                        <h4 style="line-height: 10px;text-align: center">Stock Report</h4>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="<?php echo e(route('stock_report_index')); ?>" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="major_category_id" style="width: 100" class="form-control select2" name="major_category_id">
                                                    <option value="<?php echo e($major_category != null ? $major_category->id : 0); ?>" selected><?php echo e($major_category != null ? $major_category->name : '-- All Major Categories --'); ?></option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-6 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="product_id">
                                                    <option value="0" selected>-- All Categories --</option>
                                                    <?php if(!empty($products) && ($products->count() > 0)): ?>
                                                    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option <?php echo e(isset($_GET['product_id']) && ($_GET['product_id'] == $value['id']) ? 'selected' : ''); ?> value="<?php echo e($value['id']); ?>"><?php echo e($value['name']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-6 col-12 d-print-none margin-top-10-xs">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>

                                    </div>
                                </form>

                                <table class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th style="text-align: center">P/Price</th>
                                            <th style="text-align: center">S/Price</th>
                                            <?php if($branches->count() > 0): ?>
                                            <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <th style="text-align: center"><?php echo e($branch->name); ?></th>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                            <?php if($branch_id == 1): ?>
                                            <th style="text-align: center">T/Stock</th>
                                            <?php endif; ?>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php if(!empty($data) && ($data->count() > 0)): ?>
                                        <?php
                                            $total_stock_in_hand    = 0;
                                            $total_sold             = 0;
                                            $total_stock_value      = 0;
                                            $total_sell_value       = 0;
                                        ?>
                                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e(str_pad($product['product_code'], 6, "0", STR_PAD_LEFT)); ?></td>
                                                <td><?php echo e($product['name']); ?></td>
                                                <td><?php echo e($product['category_name']); ?></td>
                                                <td style="text-align: center"><?php echo e(number_format($product['buy_price'],2,'.',',')); ?></td>
                                                <td style="text-align: center"><?php echo e(number_format($product['sell_price'],2,'.',',')); ?></td>
                                                
                                                <?php if($branches->count() > 0): ?>
                                                <?php $total_stock = 0; ?>
                                                <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($branch['id'] == 1): ?>
                                                <td style="text-align: center">
                                                    <?php echo e(number_format($product['stock_in_hand'],3,'.',',')); ?> 
                                                    <?php if($product['stock_in_hand'] != null): ?>
                                                       <?php echo $product['unit_name'] ?> 
                                                    <?php endif; ?>
                                                </td>
                                                <?php $total_stock = $total_stock + $product['stock_in_hand']; ?>
                                                <?php else: ?>
                                                <td style="text-align: center">
                                                    <?php
                                                        $stock = findStock($product['id'], $branch['id']);
                                                    ?>

                                                    <?php echo e(number_format($stock['stock_in_hand'],3,'.',',')); ?> 
                                                    <?php if($product['stock_in_hand'] != null): ?>
                                                       <?php echo $product['unit_name'] ?> 
                                                    <?php endif; ?>
                                                </td>
                                                <?php $total_stock = $total_stock + $stock['stock_in_hand']; ?>
                                                <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>

                                                <?php if($branch_id == 1): ?>
                                                <td style="text-align: center">
                                                    <?php echo e(number_format($total_stock,3,'.',',')); ?> 
                                                    <?php if($product['stock_in_hand'] != null): ?>
                                                       <?php echo $product['unit_name'] ?> 
                                                    <?php endif; ?>
                                                </td>
                                                <?php endif; ?>
                                            </tr>

                                            <?php
                                                $total_stock_in_hand    = $total_stock_in_hand + $product['stock_in_hand'];
                                                $total_sold             = $total_sold + $product['total_sold'];
                                                $total_stock_value      = $total_stock_value + ($product['buy_price']*$product['stock_in_hand']);
                                                $total_sell_value       = $total_sell_value + ($product['sell_price']*$product['stock_in_hand']);
                                            ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>

                                        <!-- <tr>
                                            <td style="text-align: right" colspan="6"><strong>Total</strong></td>
                                            <?php if($branches->count() > 0): ?>
                                            <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <th style="text-align: center"></th>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                            <td style="text-align: center"><?php echo e(isset($total_stock_in_hand) ? number_format($total_stock_in_hand,2,'.',',') : 0); ?></td>
                                        </tr> -->
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $("#major_category_id").select2({
                ajax: { 
                    url:  site_url + '/products/brand-brand-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/69_1/Modules/Reports/Resources/views/stock_report.blade.php ENDPATH**/ ?>