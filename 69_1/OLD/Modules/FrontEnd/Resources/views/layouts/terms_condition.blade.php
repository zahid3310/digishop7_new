@extends('frontend::layouts.app')

@section('main_body')
<!-- End Header -->
<main class="main">
            <div class="page-content mt-10 pt-4">
                <section class="contact-section">
                    <div class="container">
                        <div class="row">
                          
            
                            <div class="col-lg-12 col-md-12 col-xs-12">
                               <h3>Terms Condition</h3>
                               {!! $data[0]->terms_condition !!}
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End About Section-->

            </div>
        </main>
<!-- End Main -->
@endsection