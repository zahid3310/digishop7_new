<?php

namespace Modules\Sliders\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Models
use App\Models\Sliders;

class SlidersController extends Controller
{
    public function index()
    {
        $sliders = Sliders::selectRaw("sliders.*")
                            ->where('status', 1)
                            ->get();
     
        return view('sliders::index', compact('sliders'));
    }

    public function create()
    {
        return view('sliders::create');
    }

    public function store(Request $request)
    {
        $request->validate([
            "title"        => ["required", "string"],
            "status"       => ["required", "in:0,1"]
        ]);

        $type       = "success";
        $message    = "Slider Successfully Added !!";
        $data       = $request->all();

        try{
            $slider                  = new Sliders;
            $slider->title           = $data['title'];
            $slider->status          = $data['status'];
            $slider->created_by      = Auth()->user()->id;

            if($request->hasFile('image'))
            {   
                $companyLogo         = $request->file('image');
                $logoName            = time().".".$companyLogo->getClientOriginalExtension();
                $directory           = 'slider-images/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl             = $directory.$logoName;
                $slider->image       = $logoUrl;
            }

            if ($slider->save())
            {
                return redirect()->route("sliders_index")->with($type, $message);
            }
        }catch (\Exception $exception) {

            $type       = "unsuccess";
            $message    = "Something Went Wrong.Please Try Again.!";
            return back()->with($type, $message);
        }
    }

    public function edit($id)
    {
        $slider  = Sliders::find($id);
        return view('sliders::edit', compact('slider'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "title"   => ["required", "string"],
            "status"  => ["required", "in:0,1"]

        ]);

        $type       = "success";
        $message    = "Slider Successfully Updated !!";
        $data       = $request->all();

        try{
            $slider                  = Sliders::find($id);
            $slider->title           = $data['title'];
            $slider->status          = $data['status'];
            $slider->updated_by      = Auth()->user()->id;

            if($request->hasFile('image'))
            {   
                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'slider-images/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $slider->image          = $logoUrl;
            }

            if ($slider->save())
            {
                return redirect()->route("sliders_index")->with($type, $message);
            }
        }catch (\Exception $exception) {

            $type       = "unsuccess";
            $message    = "Something Went Wrong.Please Try Again.!";
            return back()->with($type, $message);
        }
    }

    public function destroy($id)
    {
        $slider = Sliders::where('id', $id)->first();

        if($slider->delete())
        {
            return back()->with("success", "Slider Successfully Deleted !!");
        }
        else
        {
            return back()->with("unsuccess", "Something Went Wrong.Please Try Again.!");
        }   
    }
}
