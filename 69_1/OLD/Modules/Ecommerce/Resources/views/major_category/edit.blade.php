@extends('layouts.app')

@section('title', 'Edit Major Category')

@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Major Category</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Ecommerce Content</a></li>
                                <li class="breadcrumb-item active">Major Category</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div>
                                <a href="{{route('sliders_index')}}" class="btn btn-success float-right">List of Major Category</a>
                                <h4 class="card-title">Edit Major Category</h4>
                            </div>
                            <br>
                            <hr>

                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {!! Session::get('success') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('unsuccess'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! Session::get('unsuccess') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('errors'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! 'Some required fields are missing..!! Please try again..' !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            <form id="FormSubmit" action="{{ route('ecommerce_major_category_update',$major_categories->id) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Name *</label>
                                        <input type="text" class="form-control" value="{{ $major_categories['name'] }}" placeholder="Enter Name" name="name" />
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Status</label>
                                        <select style="cursor: pointer" class="form-control" value="{{old('status')}}" name="status">
                                            <option value="1" {{ $major_categories['status'] == 1 ? 'selected' : '' }}>Active</option>
                                            <option value="0" {{ $major_categories['status'] == 0 ? 'selected' : '' }}>Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-3">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('home') }}">Close</a></button>
                                    </div>
                                    <div class="col-md-8"></div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    function preventDoubleClick() {
        $('.enableOnInput').prop('disabled', true)
        $('#FormSubmit').submit();
    }
</script>
@endsection