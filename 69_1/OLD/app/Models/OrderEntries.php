<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderEntries extends Model
{
    protected $table = "order_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoices','invoice_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Products','product_id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Models\Units','main_unit_id');
    }

    public function convertedUnit()
    {
        return $this->belongsTo('App\Models\Units','conversion_unit_id');
    }
}
