@extends('layouts.app')

@section('title', 'Bulk Product Update')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Bulk Product Update</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Products</a></li>
                                    <li class="breadcrumb-item active">Bulk Product Update</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif
                                
                                 <form action="{{ route('products_bulk_product_list_update') }}" method="get">


                                    <div class="row">

                                         <div class="col-sm-3">
                                            <div id="contact_id_reload" class="form-group">
                                                <label class="control-label">Brand</label>
                                                <select  style="width: 100" class="form-control select2" name="brand_id">
                                                    <option value="0" selected>-- All Brand --</option>
                                                    @if(!empty($brand) && ($brand->count() > 0))
                                                    @foreach($brand as $key => $value)
                                                        <option {{ isset($_GET['brand_id']) && ($_GET['brand_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                                    @endforeach
                                                    @endif 
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div id="contact_id_reload" class="form-group">
                                                <label class="control-label">Category</label>
                                                <select id="type" style="width: 100" class="form-control select2" name="product_id">
                                                    <option value="0" selected>-- All Categories --</option>
                                                    @if(!empty($products) && ($products->count() > 0))
                                                    @foreach($products as $key => $value)
                                                        <option {{ isset($_GET['product_id']) && ($_GET['product_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div id="contact_id_reload" class="form-group">
                                                <label class="control-label">Type</label>
                                                <select id="type_id" style="width: 100" class="form-control select2" name="type_id">
                                                    <option value="">--Select Type--</option>
                                                    <option value="1" {{ isset($_GET['type_id']) && ($_GET['type_id'] == 1) ? 'selected' : '' }}>New Product</option>
                                                    <option value="2" {{ isset($_GET['type_id']) && ($_GET['type_id'] == 2) ? 'selected' : '' }}>Old Product</option>
                                                    
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-3" style="top:25px;">
                                            <button type="submit" class="btn btn-primary">Search</button>
                                        </div>

                                        
                                    </div>

                                </form>
                                
                                <hr>

                                <form id="FormSubmit" action="{{ route('products_bulk_product_list_update_save') }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                @if($product_entries->count() > 0)
                                @foreach($product_entries as $key => $value)
                                <div class="row">
                                    <input type="hidden" name="product_id[]" class="inner form-control" id="product_id" value="{{ $value['id'] }}" />
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        @if($key == 0)
                                        <label for="productname" class="col-form-label">Product Code</label>
                                        @endif
                                        <input type="text" name="code[]" class="inner form-control barCode" id="bar_code_0" value="{{ $value['product_code'] }}" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        @if($key == 0)
                                        <label for="productname" class="col-form-label">Product Name *</label>
                                        @endif
                                        <input type="text" name="product_name[]" class="inner form-control" id="product_name" value="{{ $value['name'] }}" required />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        @if($key == 0)
                                        <label for="productname" class="col-form-label">Purchase Price *</label>
                                        @endif
                                        <input type="text" name="buying_price[]" class="inner form-control" id="buying_price_0" value="{{ $value['buy_price'] }}" required />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        @if($key == 0)
                                        <label for="productname" class="col-form-label">Sell Price *</label>
                                        @endif
                                        <input type="text" name="selling_price[]" class="inner form-control" id="selling_price_0" value="{{ $value['sell_price'] }}" required />
                                    </div>
                                </div>
                                @endforeach
                                @endif

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('products_bulk_product_list_update') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection