@extends('layouts.app')

@section('title', 'Customers List')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }

        .mb-5 {
            margin: 0rem 0rem 0rem 0rem !important;
            font-size: 10px !important; 
            border-radius: 0px !important;
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Digishop Customers</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Digishop Customers</a></li>
                                    <li class="breadcrumb-item active">Customers List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <form method="get" action="{{ route('dashboard_get_customer_list') }}" enctype="multipart/form-data">
                                    <div class="form-group row">
                                        <div class="col-lg-3 col-md-3 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="customer_id" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="" selected>--All Customers--</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>
                                
                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;width: 5%;font-size: 12px">SL</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Name</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Address</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Phone</th>
                                        </tr>
                                    </thead>
                                    <tbody id="list">
                                        <tr id="spin">
                                            <td style="text-align: center" colspan="4"><div class="spinner-border"></div></td>
                                        </tr>
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
 	<script type="text/javascript">
        $( document ).ready(function() {

            var site_url = $('.site_url').val();
            
            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/reports/branch-contacts/get-contacts',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },
    
                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';
    
                    if (result['contact_type'] == 0 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $.get(site_url + '/get-customer-list-ajax/', function(data){
   
                var list = [];
                var sl   = 1;
                $.each(data, function(i, data_list)
                { 
                    list += '<tr>' +
                                '<td style="text-align: left;font-weight: bold;font-size: 16px" colspan="4">' +
                                   i + " (Total Customer : " + data_list.length + ')'
                                '</td>' +
                            '</tr>';
                            
                    $.each(data_list, function(j, data_list_customer)
                    {   
                        if(data_list_customer.address != null)
                        {
                            var address = data_list_customer.address;
                        }
                        else
                        {
                             var address = 'N/A';
                        }
                        
                        if(data_list_customer.phone != null)
                        {
                            var phone = data_list_customer.phone;
                        }
                        else
                        {
                             var phone = 'N/A';
                        }
                        
                        list += '<tr>' +
                                '<td style="text-align: center">' +
                                   sl +
                                '</td>' +
                                '<td style="text-align: left">' +
                                   data_list_customer.name +
                                '</td>' +
                                '<td style="text-align: left">' +
                                   address +
                                '</td>' +
                                '<td style="text-align: center">' +
                                   phone +
                                '</td>' +
                            '</tr>';
                            
                            sl++;
                    });
                });
                
                $("#spin").hide();
                $("#list").empty();
                $("#list").html(list);
            });
        });
 	</script>
@endsection