<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class SupportTokens extends Model
{  
    protected $table = "support_tokens";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }
    
    public function requestBy()
    {
        return $this->belongsTo('App\Models\Customers','request_by');
    }
}
