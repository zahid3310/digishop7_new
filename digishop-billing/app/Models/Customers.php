<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table = "customers";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function partner()
    {
        return $this->belongsTo('App\Models\Customers','partner_id');
    }

    public function area()
    {
        return $this->belongsTo('App\Models\Areas','area_id');
    }
    
    public function rmp()
    {
        return $this->belongsTo('App\Models\Customers','rmp_id');
    }
    
    public function dmp()
    {
        return $this->belongsTo('App\Models\Customers','dmp_id');
    }
    
    public function amp()
    {
        return $this->belongsTo('App\Models\Customers','amp_id');
    }
}
