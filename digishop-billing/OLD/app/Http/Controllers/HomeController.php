<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

//Models
use App\Models\Customers;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\Users;
use Carbon\Carbon;
use Response;
use Artisan;
use Auth;
use DB;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth()->user()->status == 1)
        {
            return view('home');
        }
        else
        {
            return back();
        }
    }

    public function dashboardItems()
    {
        $branch_id                                  = Auth::user()->branch_id;
        $data['todays_received_digishop']           = JournalEntries::where('journal_entries.transaction_head', 'payment-receive', 'sms-purchase')
                                                            ->where('journal_entries.date', date('Y-m-d'))
                                                            ->where('journal_entries.debit_credit', 0)
                                                            ->where('journal_entries.type', 0)
                                                            ->sum('journal_entries.amount');
        $total_receivable_digishop_debit            = JournalEntries::whereIn('journal_entries.transaction_head', ['one-time-payment', 'service-charge', 'sms-purchase'])
                                                            ->where('journal_entries.debit_credit', 1)
                                                            ->where('journal_entries.type', 0)
                                                            ->sum('journal_entries.amount');
        $total_receivable_digishop_credit           = JournalEntries::where('journal_entries.transaction_head', 'payment-receive')
                                                            ->where('journal_entries.debit_credit', 0)
                                                            ->where('journal_entries.type', 0)
                                                            ->sum('journal_entries.amount');
        $data['total_receivable_digishop']          = $total_receivable_digishop_debit - $total_receivable_digishop_credit;
        $data['total_customers_digishop']           = Customers::where('contact_type', 0)->count();


        //Digiedu
        $data['todays_received_digiedu']           = JournalEntries::where('journal_entries.transaction_head', 'payment-receive', 'sms-purchase')
                                                            ->where('journal_entries.date', date('Y-m-d'))
                                                            ->where('journal_entries.debit_credit', 0)
                                                            ->where('journal_entries.type', 2)
                                                            ->sum('journal_entries.amount');
        $total_receivable_digiedu_debit            = JournalEntries::whereIn('journal_entries.transaction_head', ['one-time-payment', 'service-charge', 'sms-purchase'])
                                                            ->where('journal_entries.debit_credit', 1)
                                                            ->where('journal_entries.type', 2)
                                                            ->sum('journal_entries.amount');
        $total_receivable_digiedu_credit           = JournalEntries::where('journal_entries.transaction_head', 'payment-receive')
                                                            ->where('journal_entries.debit_credit', 0)
                                                            ->where('journal_entries.type', 2)
                                                            ->sum('journal_entries.amount');
                                                            
        $data['current_month_received_digiedu']     = JournalEntries::where('journal_entries.transaction_head', 'payment-receive', 'sms-purchase')
                                                            ->whereYear('journal_entries.date', date('Y'))
                                                            ->whereMonth('journal_entries.date', date('m'))
                                                            ->where('journal_entries.debit_credit', 0)
                                                            ->where('journal_entries.type', 2)
                                                            ->sum('journal_entries.amount');
                                                            
        $data['current_month_received_digishop']    = JournalEntries::where('journal_entries.transaction_head', 'payment-receive', 'sms-purchase')
                                                            ->whereYear('journal_entries.date', date('Y'))
                                                            ->whereMonth('journal_entries.date', date('m'))
                                                            ->where('journal_entries.debit_credit', 0)
                                                            ->where('journal_entries.type', 0)
                                                            ->sum('journal_entries.amount');
                                                            
        $data['current_month_received']            = JournalEntries::where('journal_entries.transaction_head', 'payment-receive', 'sms-purchase')
                                                            ->whereYear('journal_entries.date', date('Y'))
                                                            ->whereMonth('journal_entries.date', date('m'))
                                                            ->where('journal_entries.debit_credit', 0)
                                                            ->sum('journal_entries.amount');
        $data['total_receivable_digiedu']           = $total_receivable_digiedu_debit - $total_receivable_digiedu_credit;
        $data['total_customers_digiedu']            = Customers::where('contact_type', 2)->count();

        return Response::json($data);
    }
    
    public function getCustomerList()
    {   
        return view('customer_list');
    }
    
    public function getCustomerListAjax()
    {   
        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $customers      = Customers::where('contact_type', 0)
                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                return $query->where('customers.id', $customer_id);
                            })
                            ->get();
        
        foreach($customers as $key => $value)
        {
            $response               = Http::get('https://digishop7.com/digishop-third-party-software/get-active-customers/'.$value->digishop_id); 
            $data[$value->name]     = json_decode($response);
        }
        
        return Response::json($data);
    }
}
