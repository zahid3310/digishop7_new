<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('reports/due-customer/digishop')->group(function() {
    Route::get('/', 'ReportsController@dueReportCustomer')->name('due_report_customer_index');
    Route::get('/details/{customer_id}', 'ReportsController@dueReportCustomerDetails')->name('due_report_customer_details');
});

Route::prefix('reports/due-customer/digiedu')->group(function() {
    Route::get('/', 'ReportsController@dueReportCustomerDigiedu')->name('due_report_customer_index_digiedu');
    Route::get('/details/{customer_id}', 'ReportsController@dueReportCustomerDetailsDigiedu')->name('due_report_customer_details_digiedu');
});

Route::prefix('reports/branch-contacts')->group(function() {
    Route::get('/get-contacts/', 'ReportsController@branchContacts');
});
