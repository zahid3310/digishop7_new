<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('payments')->group(function() {
    Route::get('/create', 'PaymentsController@create')->name('payments_create');
    Route::get('/edit/{id}', 'PaymentsController@edit')->name('payments_edit');
    Route::get('/contact/list/{id}', 'PaymentsController@contactList')->name('contact_list');
    Route::post('/store', 'PaymentsController@store')->name('payments_store');
    Route::post('/update/{id}', 'PaymentsController@update')->name('payments_update');
    Route::get('/delete/{id}', 'PaymentsController@destroy')->name('payments_delete');
    Route::get('/show/{id}', 'PaymentsController@show')->name('payments_show');
});

Route::prefix('generate-bill')->group(function() {
    Route::get('/', 'PaymentsController@billCreate')->name('generate_bill_create');
    Route::get('/edit/{id}', 'PaymentsController@billEdit')->name('generate_bill_edit');
    Route::post('/store', 'PaymentsController@billStore')->name('generate_bill_store');
    Route::post('/update/{id}', 'PaymentsController@billUpdate')->name('generate_bill_update');
    Route::get('/delete/{id}', 'PaymentsController@billDestroy')->name('generate_bill_delete');
    Route::get('/show/{id}', 'PaymentsController@billShow')->name('generate_bill_print');
});

Route::prefix('payments/digiedu')->group(function() {
    Route::get('/create', 'PaymentsController@createDigiedu')->name('payments_create_digiedu');
    Route::get('/edit/{id}', 'PaymentsController@editDigiedu')->name('payments_edit_digiedu');
    Route::get('/contact/list/{id}', 'PaymentsController@contactListDigiedu')->name('contact_list_digiedu');
    Route::post('/store', 'PaymentsController@storeDigiedu')->name('payments_store_digiedu');
    Route::post('/update/{id}', 'PaymentsController@updateDigiedu')->name('payments_update_digiedu');
    Route::get('/delete/{id}', 'PaymentsController@destroyDigiedu')->name('payments_delete_digiedu');
    Route::get('/show/{id}', 'PaymentsController@showDigiedu')->name('payments_show_digiedu');
});

Route::prefix('generate-bill/digiedu')->group(function() {
    Route::get('/', 'PaymentsController@billCreateDigiedu')->name('generate_bill_create_digiedu');
    Route::get('/edit/{id}', 'PaymentsController@billEditDigiedu')->name('generate_bill_edit_digiedu');
    Route::post('/store', 'PaymentsController@billStoreDigiedu')->name('generate_bill_store_digiedu');
    Route::post('/update/{id}', 'PaymentsController@billUpdateDigiedu')->name('generate_bill_update_digiedu');
    Route::get('/delete/{id}', 'PaymentsController@billDestroyDigiedu')->name('generate_bill_delete_digiedu');
    Route::get('/show/{id}', 'PaymentsController@billShowDigiedu')->name('generate_bill_print_digiedu');
});

Route::prefix('payments/pending-bills-sms')->group(function() {
    Route::get('/', 'PaymentsController@pendingBillSms')->name('payments_pending_bill_sms');
    Route::get('/approve/{id}', 'PaymentsController@pendingBillSmsApprove')->name('payments_pending_bill_sms_approve');
});