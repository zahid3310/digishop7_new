@extends('layouts.app')

<?php
    $title = 'Partners';
?>

@section('title', $title)

@push('styles')

@endpush

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ $title }}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Registers</a></li>
                                    <li class="breadcrumb-item active">Add {{ $title }}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('partners_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Position</label>
                                        <input name="position" type="text" class="form-control" placeholder="Enter Position">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Name *</label>
                                        <input name="customer_name" type="text" class="form-control" placeholder="Enter Name" required>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Address</label>
                                        <input name="address" type="text" class="form-control" placeholder="Enter Address">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Mobile Number</label>
                                        <input name="mobile_number" type="number" class="form-control" placeholder="Enter Phone Number">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Alternative Phone</label>
                                        <input name="alternative_mobile_number" type="number" class="form-control" placeholder="Enter Aternative Phone">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Email</label>
                                        <input name="email" type="text" class="form-control" placeholder="Enter Email">
                                    </div>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Contact Type *</label>
                                        <select style="cursor: pointer" name="contact_type" class="form-control" id="contact_type" required>
                                            <option value="1" selected>Partner</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Area</label>
                                        <select name="area_id" class="form-control" id="area_id">
                                            <option value="" selected>--Select Area--</option>
                                            @if($areas->count() > 0)
                                            @foreach($areas as $area)
                                            <option value="{{ $area->id }}">{{ $area->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Designation</label>
                                        <select style="cursor: pointer" name="designation" class="form-control" id="designation">
                                            <option value="" selected>--Select Designation--</option>
                                            <option value="1">RMP</option>
                                            <option value="2">DMP</option>
                                            <option value="3">AMP</option>
                                        </select>
                                    </div> 

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Agreement Paper</label>
                                        <input name="image" type="file">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('partners_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">All Partners</h4>
                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;text-align: center">SL</th>
                                            <th style="width: 15%;text-align: center">Name</th>
                                            <th style="width: 10%;text-align: center">Designation</th>
                                            <th style="width: 10%;text-align: center">Area</th>
                                            <th style="width: 10%;text-align: center">Address</th>
                                            <th style="width: 10%;text-align: center">Phone</th>
                                            <th style="width: 10%;text-align: center">Email</th>
                                            <th style="width: 10%;text-align: center">Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        @if(!empty($customers) && ($customers->count() > 0))
                                        @foreach($customers as $key => $customer)
                                            <tr>
                                                <td style="text-align: center">{{ $key + 1 }}</td>
                                                <td style="text-align: left">{{ $customer['name'] }}</td>
                                                <td style="text-align: center">
                                                    @if($customer['designation'] == 1)
                                                    RMP
                                                    @elseif($customer['designation'] == 2)
                                                    DMP
                                                    @elseif($customer['designation'] == 3)
                                                    AMP
                                                    @endif
                                                </td>
                                                <td style="text-align: center">{{ $customer->area_id != null ? $customer->area->name : '' }}</td>
                                                <td style="text-align: left">{{ $customer['address'] }}</td>
                                                <td style="text-align: center">{{ $customer['phone'] }}</td>
                                                <td style="text-align: center">{{ $customer['email'] }}</td>
                                                <td style="text-align: center">
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('partners_show', $customer['id']) }}" target="_blank">Details</a>
                                                            <a class="dropdown-item" href="{{ route('partners_edit', $customer['id']) }}">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        
        // $('#vertical-menu-btn').click();
    });
</script>
@endsection