<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('customers')->group(function() {
    Route::get('/', 'CustomersController@index')->name('customers_index');
    Route::post('/store', 'CustomersController@store')->name('customers_store');
    Route::get('/edit/{id}', 'CustomersController@edit')->name('customers_edit');
    Route::post('/update/{id}', 'CustomersController@update')->name('customers_update');
    Route::get('/show/{id}', 'CustomersController@show')->name('customers_show');
});

Route::prefix('customers/digiedu')->group(function() {
    Route::get('/', 'CustomersController@indexDigiedu')->name('customers_index_digiedu');
    Route::post('/store', 'CustomersController@storeDigiedu')->name('customers_store_digiedu');
    Route::get('/edit/{id}', 'CustomersController@editDigiedu')->name('customers_edit_digiedu');
    Route::post('/update/{id}', 'CustomersController@updateDigiedu')->name('customers_update_digiedue');
    Route::get('/show/{id}', 'CustomersController@showDigiedu')->name('customers_show_digiedu');
});

Route::prefix('customers/digishop-database-backup')->group(function() {
    Route::get('/', 'CustomersController@databaseBackupDigishop')->name('customers_database_backup_digishop');
    Route::get('/customer-list-all-download', 'CustomersController@databaseBackupDigishopAll')->name('customers_database_backup_digishop_all');
});

Route::prefix('customers/digiedu-database-backup')->group(function() {
    Route::get('/', 'CustomersController@databaseBackupDigiedu')->name('customers_database_backup_digiedu');
    Route::get('/customer-list-all-download', 'CustomersController@databaseBackupDigieduAll')->name('customers_database_backup_digiedu_all');
});

Route::prefix('customers/digishop-sms')->group(function() {
    Route::get('/', 'CustomersController@digishopSmsIndex')->name('customers_digishop_sms_index');
    Route::post('/update-sms-info/{customer_id}', 'CustomersController@digishopSmsUpdate')->name('customers_digishop_update_sms_info');
});