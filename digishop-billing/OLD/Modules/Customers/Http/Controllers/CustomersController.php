<?php

namespace Modules\Customers\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use Validator;
use Auth;

//Models
use App\Models\Customers;
use App\Models\Users;
use Response;
use Hash;
use DB;

class CustomersController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $customers  = Customers::where('contact_type', 0)->orderBy('position', 'ASC')->get();
        $partners   = Customers::where('contact_type', 1)->orderBy('position', 'ASC')->get();

        return view('customers::index', compact('customers', 'partners'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                  = new Customers;
            $customers->position        = $data['position'];
            $customers->name            = $data['customer_name'];
            $customers->contact_person  = $data['contact_person'];
            $customers->address         = $data['address'];
            $customers->phone           = $data['mobile_number'];
            $customers->email           = $data['email'];
            $customers->digishop_url    = $data['digishop_url'];
            $customers->digishop_id     = $data['digishop_id'];
            $customers->partner_id      = $data['partner_id'];
            $customers->bill_type       = $data['bill_type'];
            $customers->bill_amount     = $data['bill_amount'];
            $customers->contact_type    = $data['contact_type'];
            $customers->sms_type        = $data['sms_type'];
            $customers->api_key         = $data['api_key'];
            $customers->sender_id       = $data['sender_id'];
            $customers->sms_rate        = $data['sms_rate'];
            $customers->branch_id       = $branch_id;
            $customers->created_by      = $user_id;

            if($request->hasFile('image'))
            {
                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->image       = $logoUrl;
            }

            if ($customers->save())
            { 
                DB::commit();
                
                return back()->with("success","Contact Added Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        $customer = Customers::find($id);

        return view('customers::show', compact('customer'));
    }

    public function edit($id)
    {
        $find_customer  = Customers::find($id);
        $customers      = Customers::where('contact_type', 0)->orderBy('position', 'ASC')->get();
        $partners       = Customers::where('contact_type', 1)->orderBy('position', 'ASC')->get();

        return view('customers::edit', compact('customers', 'find_customer', 'partners'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth()->user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                  = Customers::find($id);
            $customers->position        = $data['position'];
            $customers->name            = $data['customer_name'];
            $customers->contact_person  = $data['contact_person'];
            $customers->address         = $data['address'];
            $customers->phone           = $data['mobile_number'];
            $customers->email           = $data['email'];
            $customers->digishop_url    = $data['digishop_url'];
            $customers->digishop_id     = $data['digishop_id'];
            $customers->partner_id      = $data['partner_id'];
            $customers->bill_type       = $data['bill_type'];
            $customers->bill_amount     = $data['bill_amount'];
            $customers->contact_type    = $data['contact_type'];
            $customers->sms_type        = $data['sms_type'];
            $customers->api_key         = $data['api_key'];
            $customers->sender_id       = $data['sender_id'];
            $customers->sms_rate        = $data['sms_rate'];
            $customers->updated_by      = $user_id;

            if($request->hasFile('image'))
            {
                if ($customers->image != null)
                {
                    unlink('public/'.$customers->image);
                }

                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->image       = $logoUrl;
            }

            if ($customers->save())
            {   
                DB::commit();

                return redirect()->route('customers_index')->with("success","Contact Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Updated");
        }
    }

    //Digiedu
    public function indexDigiedu()
    {
        $customers  = Customers::where('contact_type', 2)->orderBy('position', 'ASC')->get();
        $partners   = Customers::where('contact_type', 1)->orderBy('position', 'ASC')->get();

        return view('customers::digiedu.index', compact('customers', 'partners'));
    }

    public function storeDigiedu(Request $request)
    {
        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                  = new Customers;
            $customers->position        = $data['position'];
            $customers->name            = $data['customer_name'];
            $customers->contact_person  = $data['contact_person'];
            $customers->address         = $data['address'];
            $customers->phone           = $data['mobile_number'];
            $customers->email           = $data['email'];
            $customers->digiedu_url     = $data['digiedu_url'];
            $customers->digiedu_id      = $data['digiedu_id'];
            $customers->partner_id      = $data['partner_id'];
            $customers->bill_type       = $data['bill_type'];
            $customers->per_student_rate= $data['per_student_rate'];
            $customers->total_student   = $data['total_student'];
            $customers->bill_amount     = $data['bill_amount'];
            $customers->contact_type    = $data['contact_type'];
            $customers->branch_id       = $branch_id;
            $customers->created_by      = $user_id;

            if($request->hasFile('image'))
            {
                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->image       = $logoUrl;
            }

            if ($customers->save())
            { 
                DB::commit();
                
                return back()->with("success","Contact Added Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function showDigiedu($id)
    {
        $customer = Customers::find($id);

        return view('customers::digiedu.show', compact('customer'));
    }

    public function editDigiedu($id)
    {
        $find_customer  = Customers::find($id);
        $customers      = Customers::where('contact_type', 2)->orderBy('position', 'ASC')->get();
        $partners       = Customers::where('contact_type', 1)->orderBy('position', 'ASC')->get();

        return view('customers::digiedu.edit', compact('customers', 'find_customer', 'partners'));
    }

    public function updateDigiedu(Request $request, $id)
    {
        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth()->user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                  = Customers::find($id);
            $customers->position        = $data['position'];
            $customers->name            = $data['customer_name'];
            $customers->contact_person  = $data['contact_person'];
            $customers->address         = $data['address'];
            $customers->phone           = $data['mobile_number'];
            $customers->email           = $data['email'];
            $customers->digiedu_url     = $data['digiedu_url'];
            $customers->digiedu_id      = $data['digiedu_id'];
            $customers->partner_id      = $data['partner_id'];
            $customers->bill_type       = $data['bill_type'];
            $customers->per_student_rate= $data['per_student_rate'];
            $customers->total_student   = $data['total_student'];
            $customers->bill_amount     = $data['bill_amount'];
            $customers->contact_type    = $data['contact_type'];
             $customers->updated_by     = $user_id;

            if($request->hasFile('image'))
            {
                if ($customers->image != null)
                {
                    unlink('public/'.$customers->image);
                }

                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->image       = $logoUrl;
            }

            if ($customers->save())
            {   
                DB::commit();

                return redirect()->route('customers_index_digiedu')->with("success","Contact Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Updated");
        }
    }

    //Database Backup Digishop
    public function databaseBackupDigishop()
    {
        $customers      = Customers::where('contact_type', 0)->orderBy('position', 'ASC')->get();
        
        return view('customers::database_backup.digishop_database_backup', compact('customers'));
    }
    
    public function databaseBackupDigishopAll()
    {
        $customers      = Customers::where('contact_type', 0)->orderBy('position', 'ASC')->get();
        
        $url  = 'https://digishop7.com/2/backup/store';
        foreach($customers as $key=> $val)
        {   
            if($val['digishop_url'] != null)
            {
                $url  = $url . ','. $val['digishop_url'].'/backup/store';
            }
        }

        return Response::json($url);
    }
    
    //Database Backup Digiedu
    public function databaseBackupDigiedu()
    {
        $customers      = Customers::where('contact_type', 2)->orderBy('position', 'ASC')->get();
        
        return view('customers::database_backup.digiedu_database_backup', compact('customers'));
    }
    
    public function databaseBackupDigieduAll()
    {
        $customers      = Customers::where('contact_type', 2)->orderBy('position', 'ASC')->get();
        
        $url  = 'https://digiedubd.com/2/BackupController/store';
        foreach($customers as $key=> $val)
        {   
            if($val['digiedu_url'] != null)
            {
                $url  = $url . ','. $val['digiedu_url'].'/BackupController/store';
            }
        }

        return Response::json($url);
    }
    
    //Digishop SMS
    public function digishopSmsIndex()
    {
        $customers  = Customers::where('contact_type', 0)->orderBy('position', 'ASC')->get();

        return view('customers::sms.digishop.index', compact('customers'));
    }
    
    public function digishopSmsUpdate(Request $request, $id)
    {
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                  = Customers::find($id);
            $customers->sms_type        = $data['sms_type'];
            $customers->api_key         = $data['api_key'];
            $customers->sender_id       = $data['sender_id'];
            $customers->sms_rate        = $data['sms_rate'];
            $customers->updated_by      = $user_id;

            if ($customers->save())
            {   
                DB::commit();

                return back()->with("success","Information Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Updated");
        }
    }
}
