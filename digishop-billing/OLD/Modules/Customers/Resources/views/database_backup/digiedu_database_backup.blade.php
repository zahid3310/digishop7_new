@extends('layouts.app')

<?php
    $title = 'Digiedu Database Backup';
?>

@section('title', $title)

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ $title }}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Basic Settings</a></li>
                                    <li class="breadcrumb-item active">{{ $title }}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <h4 class="card-title">All Client <a style="float: right" onclick="downloadDatabase()" href="javascript: void(0);"><i class="fa fa-download font-size-15"></i>&nbsp;Download All</a></h4>
                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;text-align: center">SL</th>
                                            <th style="width: 5%;text-align: center">Digiedu ID</th>
                                            <th style="width: 25%;text-align: center">Name</th>
                                            <th style="width: 35%;text-align: center">Address</th>
                                            <th style="width: 10%;text-align: center">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($customers) && ($customers->count() > 0))
                                        @foreach($customers as $key => $customer)
                                            <tr>
                                                <td style="text-align: center">{{ $key + 1 }}</td>
                                                <td style="text-align: center">{{ $customer['digiedu_id'] }}</td>
                                                <td style="text-align: left">
                                                    <a href="{{ $customer['digishop_url'] }}" target="_blank">
                                                        {{ $customer['name'] }}
                                                    </a>
                                                </td>
                                                <td style="text-align: left">{{ $customer['address'] }}</td>
                                                <td style="text-align: center">
                                                    <a id="{{ $customer['digishop_id'] }}" href="{{ $customer['digiedu_url'].'/BackupController/store' }}"><i class="fa fa-download font-size-15"></i>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    function downloadDatabase()
    {   
        //Call ajax
        // $.ajax({
        //     type : "get",
        //     url : 'https://digishop7.com/2/backup/store',
        //     success:function(response){
        //         console.log(response);
        //     }
        // });
        
        var site_url = $('.site_url').val();
        $.get(site_url + '/customers/digiedu-database-backup/customer-list-all-download/', function(data){
            
            $.each( data.split( "," ), function( index, item ) {
                window.open( item, "_blank" );
            });
  
        });
    }
</script>

@endsection