@extends('layouts.app')

<?php
    $title = 'All Order';
?>

@section('title', $title)

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Orders</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">{{ $title }}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif
                                
                                <h4 class="card-title">{{ $title }}</h4>
                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;text-align: center">SL</th>
                                            <th style="width: 5%;text-align: center">Type</th>
                                            <th style="width: 15%;text-align: center">Partner</th>
                                            <th style="width: 8%;text-align: center">Request Date</th>
                                            <th style="width: 10%;text-align: center">Received By</th>
                                            <th style="width: 10%;text-align: center">Developer</th>
                                            <th style="width: 7%;text-align: center">Delivery Date</th>
                                            <th style="width: 10%;text-align: center">Status</th>
                                            <th style="width: 10%;text-align: center">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $i = 0; ?>
                                        @if(!empty($data) && (count($data) > 0))
                                        @foreach($data as $key => $value)
                                        @if(($value->developer_id == Auth()->user()->id) && ($value->status != 4))
                                            <?php
                                                $i++;
                                                
                                                if($value->partner_designation == 1)
                                                {
                                                    $designation = 'RMP';
                                                }
                                                elseif($value->partner_designation == 2)
                                                {
                                                    $designation = 'DMP';
                                                }
                                                elseif($value->partner_designation == 3)
                                                {
                                                    $designation = 'AMP';
                                                }
                                                
                                                if($value->status == 0)
                                                {
                                                    $status = 'Pending';
                                                }
                                                elseif($value->status == 1)
                                                {
                                                    $status = 'Received';
                                                }
                                                elseif($value->status == 2)
                                                {
                                                    $status = 'Working';
                                                }
                                                elseif($value->status == 3)
                                                {
                                                    $status = 'Done';
                                                }
                                            ?>
                                            
                                            <tr>
                                                <td style="text-align: center">{{ $i }}</td>
                                                <td style="text-align: center">
                                                    @if($value->request_type == 1)
                                                    <?php echo 'Digishop'; ?>
                                                    @elseif($value->request_type == 2)
                                                    <?php echo 'Digiedu'; ?>
                                                    @elseif($value->request_type == 3)
                                                    <?php echo 'Website'; ?>
                                                    @endif
                                                </td>
                                                <td style="text-align: left">{{ $value->partner_name. '('.$designation.')' }}
                                                </td>
                                                <td style="text-align: center">
                                                    {{ $value->request_date != null ? date('d-m-Y', strtotime($value->request_date)) : '' }} <br>
                                                    {{ $value->request_date != null ? date('h:i:s A', strtotime($value->request_date)) : '' }}
                                                </td>
                                                <td style="text-align: center">
                                                    {{ $value->support_ex }}<br>
                                                    {{ $value->ex_receive_date != null ? date('d-m-Y', strtotime($value->ex_receive_date)) : '' }} <br>
                                                    {{ $value->ex_receive_date != null ? date('h:i:s A', strtotime($value->ex_receive_date)) : '' }}
                                                </td>
                                                <td style="text-align: center">
                                                    {{ $value->developer_name }}<br>
                                                    {{ $value->developer_receive_date != null ? date('d-m-Y', strtotime($value->developer_receive_date)) : '' }} <br>
                                                    {{ $value->developer_receive_date != null ? date('h:i:s A', strtotime($value->developer_receive_date)) : '' }}
                                                </td>
                                                <td style="text-align: center">
                                                    {{ $value->delivery_date != null ? date('d-m-Y', strtotime($value->delivery_date)) : '' }} <br>
                                                    {{ $value->delivery_date != null ? date('h:i:s A', strtotime($value->delivery_date)) : '' }}
                                                </td>
                                                <td style="text-align: center">{{ $status }}</td>
                                                <td style="text-align: center">
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#myModal_{{$value->id}}">Details</a>
                                                            <!--<a class="dropdown-item" href="{{ route('id_request_edit', $value->id).'?request_type='.$value->request_type }}">Edit</a>-->
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            
                                            <div id="myModal_{{$value->id}}" class="modal fade fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-xl">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0" id="myModalLabel">Request Details</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
    
                                                        <div style="padding-top: 20px !important" class="modal-body">
                                                            <div style="margin-bottom: 0px !important" class="form-group row">
                                                                @if($value->request_type == 1)
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Request Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->request_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->request_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Support Receive Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->ex_receive_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->ex_receive_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Developer Receive Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->developer_receive_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->developer_receive_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Delivery Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->delivery_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->delivery_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Delivery Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->delivery_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->delivery_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Partner : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->partner_name. '('.$designation.')' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Software Type : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    @if($value->request_type == 1)
                                                                    <?php echo 'Digishop'; ?>
                                                                    @elseif($value->request_type == 2)
                                                                    <?php echo 'Digiedu'; ?>
                                                                    @elseif($value->request_type == 3)
                                                                    <?php echo 'Website'; ?>
                                                                    @endif
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Shop Name : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->shop_name }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Bussiness Type : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    @if($value->bussiness_type == 1)
                                                                    <?php echo 'General POS'; ?>
                                                                    @elseif($value->bussiness_type == 2)
                                                                    <?php echo 'Community Center'; ?>
                                                                    @elseif($value->bussiness_type == 3)
                                                                    <?php echo 'Tailor'; ?>
                                                                    @elseif($value->bussiness_type == 4)
                                                                    <?php echo 'Dealer'; ?>
                                                                    @elseif($value->bussiness_type == 5)
                                                                    <?php echo 'Electronics'; ?>
                                                                    @elseif($value->bussiness_type == 6)
                                                                    <?php echo 'Pharmacy'; ?>
                                                                    @elseif($value->bussiness_type == 7)
                                                                    <?php echo 'Tiles'; ?>
                                                                    @elseif($value->bussiness_type == 8)
                                                                    <?php echo 'Auto Rice Mill'; ?>
                                                                    @endif
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Shop Address : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->shop_address }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Shop Phone : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->shop_phone_number }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Shop Email : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->shop_email }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Shop Website : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->shop_website }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Shop Admin : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->shop_admin_name }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Shop Admin Phone : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->shop_admin_phone_number }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Package : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    @if($value->package == 1)
                                                                    <?php echo 'Pre-Paid'; ?>
                                                                    @elseif($value->package == 2)
                                                                    <?php echo 'Post-Paid'; ?>
                                                                    @endif
                                                                    
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Billing Type : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    @if($value->billing_type == 1)
                                                                    <?php echo 'Monthly'; ?>
                                                                    @elseif($value->billing_type == 2)
                                                                    <?php echo 'Yearly'; ?>
                                                                    @endif
                                                                    
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Service Charge : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->service_charge }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Note : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->note }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Status : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    @if($value->status == 0)
                                                                    <?php echo 'Pending'; ?>
                                                                    @elseif($value->status == 1)
                                                                    <?php echo 'Received'; ?>
                                                                    @elseif($value->status == 2)
                                                                    <?php echo 'Working'; ?>
                                                                    @elseif($value->status == 3)
                                                                    <?php echo 'Done'; ?>
                                                                    @elseif($value->status == 4)
                                                                    <?php echo 'Delivered'; ?>
                                                                    @endif
                                                                </div>
                                                                @endif
                                                                
                                                                @if($value->request_type == 2)
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Request Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->request_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->request_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Support Receive Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->ex_receive_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->ex_receive_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Developer Receive Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->developer_receive_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->developer_receive_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Delivery Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->delivery_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->delivery_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Delivery Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->delivery_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->delivery_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Partner : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->partner_name. '('.$designation.')' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Software Type : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    @if($value->request_type == 1)
                                                                    <?php echo 'Digishop'; ?>
                                                                    @elseif($value->request_type == 2)
                                                                    <?php echo 'Digiedu'; ?>
                                                                    @elseif($value->request_type == 3)
                                                                    <?php echo 'Website'; ?>
                                                                    @endif
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Institute Name : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->institute_name }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Institute Address 1 : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->institute_address_1 }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Institute Address 2 : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->institute_address_2 }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Institute Phone : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->institute_phone_number }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Institute Email : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->institute_email }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Institute Website : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->institute_website }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Institute Admin : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->institute_admin_name }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Institute Admin Phone : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->institute_admin_phone_number }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Package : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    @if($value->package == 1)
                                                                    <?php echo 'Pre-Paid'; ?>
                                                                    @elseif($value->package == 2)
                                                                    <?php echo 'Post-Paid'; ?>
                                                                    @endif
                                                                    
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Billing Type : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    @if($value->billing_type == 1)
                                                                    <?php echo 'Monthly'; ?>
                                                                    @elseif($value->billing_type == 2)
                                                                    <?php echo 'Yearly'; ?>
                                                                    @endif
                                                                    
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Total Student : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->student_quantity }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Per Student Rate : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->per_student_rate }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Note : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->note }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Status : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    @if($value->status == 0)
                                                                    <?php echo 'Pending'; ?>
                                                                    @elseif($value->status == 1)
                                                                    <?php echo 'Received'; ?>
                                                                    @elseif($value->status == 2)
                                                                    <?php echo 'Working'; ?>
                                                                    @elseif($value->status == 3)
                                                                    <?php echo 'Done'; ?>
                                                                    @elseif($value->status == 4)
                                                                    <?php echo 'Delivered'; ?>
                                                                    @endif
                                                                </div>
                                                                @endif
                                                                
                                                                @if($value->request_type == 3)
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Request Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->request_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->request_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Support Receive Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->ex_receive_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->ex_receive_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Developer Receive Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->developer_receive_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->developer_receive_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Delivery Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->delivery_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->delivery_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Delivery Date : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->delivery_date != null ? date('D || M d, Y || h:i:s A', strtotime($value->delivery_date)) : '' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Partner : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->partner_name. '('.$designation.')' }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Software Type : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    @if($value->request_type == 1)
                                                                    <?php echo 'Digishop'; ?>
                                                                    @elseif($value->request_type == 2)
                                                                    <?php echo 'Digiedu'; ?>
                                                                    @elseif($value->request_type == 3)
                                                                    <?php echo 'Website'; ?>
                                                                    @endif
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Website Type : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    @if($value->website_type == 1)
                                                                    <?php echo 'School Website'; ?>
                                                                    @elseif($value->website_type == 2)
                                                                    <?php echo 'Shop Website'; ?>
                                                                    @elseif($value->website_type == 3)
                                                                    <?php echo 'E-Commerce'; ?>
                                                                    @elseif($value->website_type == 4)
                                                                    <?php echo 'Others'; ?>
                                                                    @endif
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Website Name : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->website_name }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Website Address : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->website_address }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Website Admin : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->website_admin_name }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Package : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    @if($value->package == 1)
                                                                    <?php echo 'Pre-Paid'; ?>
                                                                    @elseif($value->package == 2)
                                                                    <?php echo 'Post-Paid'; ?>
                                                                    @endif
                                                                    
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Billing Type : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    @if($value->billing_type == 1)
                                                                    <?php echo 'Monthly'; ?>
                                                                    @elseif($value->billing_type == 2)
                                                                    <?php echo 'Yearly'; ?>
                                                                    @endif
                                                                    
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Service Charge : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->service_charge }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Note : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    {{ $value->note }}
                                                                </div>
                                                                
                                                                <div style="text-align: right" class="col-md-2">
                                                                    <strong>Status : </strong>
                                                                </div>
                                                                
                                                                <div style="text-align: left" class="col-md-4">
                                                                    @if($value->status == 0)
                                                                    <?php echo 'Pending'; ?>
                                                                    @elseif($value->status == 1)
                                                                    <?php echo 'Received'; ?>
                                                                    @elseif($value->status == 2)
                                                                    <?php echo 'Working'; ?>
                                                                    @elseif($value->status == 3)
                                                                    <?php echo 'Done'; ?>
                                                                    @elseif($value->status == 4)
                                                                    <?php echo 'Delivered'; ?>
                                                                    @endif
                                                                </div>
                                                                @endif
                                                            </div>
                                                            
                                                            <hr>
                                                            
                                                            <form method="get" action="{{ route('id_request_update_status', $value->id) }}" enctype="multipart/form-data">
                                                                <div style="margin-bottom: 0px !important" class="form-group row">
                                                                    @if(Auth()->user()->role == 1)
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                                                        <label for="example-text-input" class="col-form-label">Support Executive</label>
                                                                        <select style="cursor: pointer;width: 100%" name="exe_id" class="form-control select2" id="exe_id">
                                                                            <option value="" selected>--Select Support Executive--</option>
                                                                            @if($employes->count() > 0)
                                                                            @foreach($employes as $supp)
                                                                            @if($supp->role == 3)
                                                                            <option value="{{ $supp->id }}" {{ $supp->id == $value->customer_support_ex_id ? 'selected' : '' }}>{{ $supp->name }}</option>
                                                                            @endif
                                                                            @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                    @else
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                                                        <label for="example-text-input" class="col-form-label">Support Executive</label>
                                                                        <input class="form-control select2" value="{{ Auth()->user()->name }}" readonly>
                                                                        <input style="display: none" name="exe_id" class="form-control select2" id="exe_id" value="{{ Auth()->user()->id }}">
                                                                    </div>
                                                                    @endif
                                                                    
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                                                        <label for="example-text-input" class="col-form-label">Developer</label>
                                                                        <select style="cursor: pointer;width: 100%" name="developer_id" class="form-control select2" id="developer_id">
                                                                            <option value="" selected>--Select Developer--</option>
                                                                            @if($employes->count() > 0)
                                                                            @foreach($employes as $dev)
                                                                            @if($dev->role == 4)
                                                                            <option value="{{ $dev->id }}" {{ $dev->id == $value->developer_id ? 'selected' : '' }}>{{ $dev->name }}</option>
                                                                            @endif
                                                                            @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Update</button>
                                                            </form>
                                                            <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    function getDropdownField()
    {
        var designation = $("#designation").val();

        if (parseFloat(designation) == 1)
        {
            $(".rmp").hide();
            $(".dmp").hide();
            $(".amp").hide();
        }

        if (parseFloat(designation) == 2)
        {
            $(".rmp").show();
            $(".dmp").hide();
            $(".amp").hide();
        }

        if (parseFloat(designation) == 3)
        {
            $(".rmp").show();
            $(".dmp").show();
            $(".amp").hide();
        }

        if (!designation)
        {
            $(".rmp").hide();
            $(".dmp").hide();
            $(".amp").hide();
        }
    }
</script>
@endsection