@extends('layouts.app')

@section('title', 'My Tokens')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">My Tokens</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Support Token</a></li>
                                    <li class="breadcrumb-item active">My Tokens</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if(count($token_response) > 0)
                                    @foreach($token_response as $token_response_data_val)
                                    @if(($token_response_data_val->receive_by == Auth()->user()->id) && ($token_response_data_val->status != 4))
                                    <?php
                                        if($token_response_data_val->software_type == 1)
                                        {
                                           $softwareType = 'Digishop'; 
                                        }
                                        elseif($token_response_data_val->software_type == 2)
                                        {
                                           $softwareType = 'Digiedu'; 
                                        }
                                        elseif($token_response_data_val->software_type == 3)
                                        {
                                           $softwareType = 'Digishop Website'; 
                                        }
                                        elseif($token_response_data_val->software_type == 4)
                                        {
                                           $softwareType = 'Digiedu Website'; 
                                        }
                                        elseif($token_response_data_val->software_type == 5)
                                        {
                                           $softwareType = 'Ecommerce'; 
                                        }
                                        elseif($token_response_data_val->software_type == 6)
                                        {
                                           $softwareType = 'Digimunicipality'; 
                                        }
                                        elseif($token_response_data_val->software_type == 7)
                                        {
                                           $softwareType = 'Digihospital'; 
                                        }
                                        elseif($token_response_data_val->software_type == 8)
                                        {
                                           $softwareType = 'Others'; 
                                        }
                                        else
                                        {
                                           $softwareType = ''; 
                                        }
                                    ?>
                                    <div class="col-md-4">
                                        <div style="background-color: #F4F4F7;margin: 5px" class="col-md-12">
                                            <li style="text-decoration: none;list-style: none;padding: 15px">
                                                <span style="font-size: 15px">
                                                    Token Id: {{ $token_response_data_val->token_number }}
                                                </span>
                                                <br>
                                                <span style="font-size: 15px;color: blue">
                                                    {{ $token_response_data_val->requestBy->name }}
                                                </span>
                                                <br>
                                                <span style="color: black;font-size: 15px">
                                                    Issue Date: {{ date('F j, Y, g:i a', strtotime($token_response_data_val->request_date)) }}
                                                </span>
                                                <span style="float: right;font-size: 15px">
                                                    <button style="line-height: 0.75 !important;border-radius: 0 !important" class="btn btn-success" data-toggle="modal" data-target="#myModal_{{$token_response_data_val->id}}">View</button>
                                                </span>
                                                <br>
                                                @if($token_response_data_val->delivery_date != null)
                                                <span style="color: black;font-size: 15px">
                                                    Solve Date: {{ date('F j, Y, g:i a', strtotime($token_response_data_val->delivery_date)) }}
                                                </span>
                                                @endif
                                                
                                                <span style="color: blue;font-size: 15px">
                                                    <br>
                                                    {{ 'Software Type : ' . $softwareType . ' | ' . 'Software ID : ' . $token_response_data_val->software_id }}
                                                </span>
                                            </li>
                                        </div>
                                    </div>
                                    
                                    <div id="myModal_{{$token_response_data_val->id}}" class="modal fade fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-xl">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title mt-0" id="myModalLabel">Token # 
                                                        <span style="color: blue">{{ $token_response_data_val->token_number . ' | '  }}</span>
                                                        Request From : 
                                                        <span style="color: blue">{{ $token_response_data_val->requestBy->name . ' | ' }}</span>
                                                        Request Date : 
                                                        <span style="color: blue">{{ date('F j, Y, g:i a', strtotime($token_response_data_val->request_date)) . ' | ' }}</span>
                                                        Softwate Type : 
                                                        <span style="color: blue">{{ $softwareType . ' | '  }}</span>
                                                        Software ID : 
                                                        <span style="color: blue">{{ $token_response_data_val->software_id }}</span>
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>

                                                <div style="padding-top: 10px !important" class="modal-body">
                                                    <div style="margin-bottom: 0px !important" class="form-group row">
                                                        <div class="col-md-12">
                                                            <form method="get" action="{{ route('support_token_update_status', $token_response_data_val->id) }}" enctype="multipart/form-data">
                                                                <div style="margin-bottom: 0px !important" class="form-group row">
                                                                    
                                                                    @if(Auth()->user()->role == 1)
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                                                        <label for="example-text-input" class="col-form-label">Support Executive</label>
                                                                        <select style="cursor: pointer;width: 100%" name="exe_id" class="form-control select2" id="exe_id">
                                                                            <option value="" selected>--Select Support Executive--</option>
                                                                            @if($employes->count() > 0)
                                                                            @foreach($employes as $supp)
                                                                            @if($supp->role == 3)
                                                                            <option value="{{ $supp->id }}" {{ $supp->id == $token_response_data_val->customer_support_ex_id ? 'selected' : '' }}>{{ $supp->name }}</option>
                                                                            @endif
                                                                            @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                    @else
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                                                        <label for="example-text-input" class="col-form-label">Support Executive</label>
                                                                        <input class="form-control select2" value="{{ Auth()->user()->name }}" readonly>
                                                                        <input style="display: none" name="exe_id" class="form-control select2" id="exe_id" value="{{ Auth()->user()->id }}">
                                                                    </div>
                                                                    @endif
                                                                    
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                                                        <label for="example-text-input" class="col-form-label">Developer</label>
                                                                        <select style="cursor: pointer;width: 100%" name="developer_id" class="form-control select2" id="developer_id">
                                                                            <option value="" selected>--Select Developer--</option>
                                                                            @if($employes->count() > 0)
                                                                            @foreach($employes as $dev)
                                                                            @if($dev->role == 4)
                                                                            <option value="{{ $dev->id }}" {{ $dev->id == $token_response_data_val->developer_id ? 'selected' : '' }}>{{ $dev->name }}</option>
                                                                            @endif
                                                                            @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        
                                                        <hr>
                                                        
                                                        <div class="col-md-12">
                                                            <h4 style="color: blue;">Token Details : </h4>
                                                            <br>
                                                            <span style="color: black;font-size: 15px">
                                                                <?php echo $token_response_data_val->details; ?>
                                                            </span>
                                                            <br>
                                                            <h4 style="color: blue;">Attachments : </h4>
                                                            <br>
                                                            
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <img width="370" height="220" src="{{ $token_response_data_val->image }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="modal-footer">
                                                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Update</button>
                                                    </form>
                                                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

    		</div>
		</div>
    </div>
@endsection

@section('scripts')
<script src="{{ url('public/admin_panel_assets/editor/ckeditor.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/bower_components/ckeditor/ckeditor.js') }}"></script>
@endsection