@extends('layouts.app')

<?php
    $title = 'Due Collection';
?>

@push('scripts')
<style type="text/css">
    body[data-sidebar=dark].vertical-collpsed {
        min-height: 0px !important;
    }
    
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>
@endpush

@section('title', $title)

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.payment')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.payment')}}</a></li>
                                    <li class="breadcrumb-item active">{{ $title }}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <form id="FormSubmit" action="{{ route('payments_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
                            <div style="height: 271px" class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
                                                <label class="control-label">{{ __('messages.payment_type')}} *</label>
                                                <select id="type" style="width: 100%;cursor: pointer" class="form-control" name="type">
                                                    <option value="0" selected>Digishop Payment</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                        	<div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
    	                                        <label class="control-label">{{ __('messages.search_contact')}} *</label>
    	                                        <select id="contact_id" style="width: 100%;cursor: pointer" class="form-control select2 contact_id" name="customer_id" onchange="searchContact()">
    	                                        	<option value="">--{{ __('messages.select_contact')}}--</option>
    												@if(!empty($customers))
    													@foreach($customers as $key => $customer)
    													<option value="{{ $customer->id }}">{{ $customer->name }}</option>
    													@endforeach
    												@endif
    	                                        </select>
    	                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
                            <div class="card">
                                <div class="card-body">

                                    @if(Session::has('success'))
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        {!! Session::get('success') !!}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                        			@endif

                        			@if(Session::has('unsuccess'))
                        			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                        {!! Session::get('unsuccess') !!}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    @endif

            						<div class="row">
    				                	<div class="col-sm-4">
    				                        <div class="form-group">
    				                            <label for="payment_date">{{ __('messages.payment_date')}} *</label>
    				                            <input id="payment_date" name="payment_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
    				                        </div>
    				                    </div>

    				                    <div class="col-sm-4">
    				                        <div class="form-group">
    				                            <label for="amount">{{ __('messages.amount')}} *</label>
    				                            <input id="amount" name="amount" type="text" class="form-control" value="0">
    				                        </div>
    				                    </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="discount_amount">Discount</label>
                                                <input id="discount_amount" name="discount_amount" type="text" class="form-control" value="0">
                                            </div>
                                        </div>

    				                    <div class="col-sm-6 form-group">
    			                            <label class="control-label">{{ __('messages.paid_through')}}</label>
                                            <select style="cursor: pointer" name="paid_through" class="form-control select2">
                                                @if(!empty($paid_accounts) && ($paid_accounts->count() > 0))
                                                @foreach($paid_accounts as $key => $paid_account)
                                                    <option value="{{ $paid_account['id'] }}">{{ $paid_account['account_name'] }}</option>
                                                @endforeach
                                                @endif
                                            </select>
    			                        </div>

    			                        <div class="col-sm-6">
    				                        <div class="form-group">
    				                            <label for="note">{{ __('messages.note')}}</label>
    				                            <input id="note" name="note" type="text" class="form-control">
    				                        </div>
    				                    </div>
    			                	</div>

                                    <hr style="margin-top: 0px !important">

                                    <div class="form-group row">
                                        <div class="button-items col-md-12">
                                            <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">{{ __('messages.make_payment')}}</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('payments_create') }}">{{ __('messages.close')}}</a></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->

                        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                            <div class="row">

                                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
                                    <div style="height: 100px" class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                        <div class="card-body">
                                            
                                            <div class="media">
                                                <div class="media-body">
                                                    <h4 style="font-size: 14px" id="customerName" class="mb-0"></h4>
                                                    <p style="font-size: 14px" id="customerAddress" class="text-muted font-weight-medium"></p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
                                    <div class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                        <div class="card-body">
                                            
                                            <div class="media">
                                                <div class="media-body">
                                                    <p id="receivablePShow" class="text-muted font-weight-medium">{{ __('messages.total_receivables')}}</p>
                                                    <h4 id="totalReceivable" class="mb-0">0.00</h4>
                                                </div>

                                                <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                                    <span class="avatar-title">
                                                        <i class="bx bx-credit-card font-size-24"></i>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
                                    <div class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                        <div class="card-body">
                                            
                                            <div class="media">
                                                <div class="media-body">
                                                    <p id="receivedPShow" class="text-muted font-weight-medium">{{ __('messages.total_received')}}</p>
                                                    <h4 id="totalReceived" class="mb-0">0.00</h4>
                                                </div>

                                                <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                                    <span class="avatar-title">
                                                        <i class="bx bx-credit-card font-size-24"></i>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
                                    <div class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                        <div class="card-body">
                                            
                                            <div class="media">
                                                <div class="media-body">
                                                    <p class="text-muted font-weight-medium">{{ __('messages.balance')}}</p>
                                                    <h4 id="totalDues" class="mb-0">0.00</h4>
                                                </div>

                                                <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                                    <span class="avatar-title">
                                                        <i class="bx bx-credit-card font-size-24"></i>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <hr style="margin-top: 0px">

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">All Digishop Payments</h4>

                                <br> 

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;text-align: center">SL</th>
                                            <th style="width: 10%;text-align: center">Date</th>
                                            <th style="width: 10%;text-align: center">Payment#</th>
                                            <th style="width: 5%;text-align: center">ID</th>
                                            <th style="width: 20%;text-align: center">Client</th>
                                            <th style="width: 10%;text-align: center">Methode</th>
                                            <th style="width: 15%;text-align: center">Note</th>
                                            <th style="width: 10%;text-align: center">Amount</th>
                                            <th style="width: 10%;text-align: center">Discount</th>
                                            <th style="width: 10%;text-align: center">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($payments) && ($payments->count() > 0))
                                        @foreach($payments as $key => $payment)
                                            <tr>
                                                <td style="text-align: center">{{ $key + 1 }}</td>
                                                <td style="text-align: center">{{ date('d-m-Y', strtotime($payment['payment_date'])) }}</td>
                                                <td style="text-align: center">{{ 'PM - ' . str_pad($payment['payment_number'], 6, "0", STR_PAD_LEFT) }}</td>
                                                <td style="text-align: center">{{ $payment->customer->digishop_id }}</td>
                                                <td style="text-align: left">{{ $payment['customer_id'] != null ? $payment->customer->name : '' }}</td>
                                                <td style="text-align: center">{{ $payment['paid_through'] != null ? $payment->paidThrough->account_name : '' }}</td>
                                                <td style="text-align: left">{{ $payment['note'] != null ? $payment->note : '' }}</td>
                                                <td style="text-align: center">{{ $payment->amount }}</td>
                                                <td style="text-align: center">{{ $payment->discount_amount }}</td>
                                                <td style="text-align: center">
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('payments_show', $payment['id']) }}">Show</a>
                                                            <a class="dropdown-item" href="{{ route('payments_edit', $payment['id']) }}">Edit</a>
                                                            <a class="dropdown-item" href="{{ route('payments_delete', $payment['id']) }}" onclick="return confirm('Are you sure want to delete?');">Delete</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> 
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        function searchContact()
        {
            var contact_id  = $('#contact_id').val();
            var site_url    = $('.site_url').val();
            var amounts     = $('#amount').val();

            if (amounts == '')
            {
                var amount = 0;
            }
            else
            {
                var amount = amounts;
            }

            $.get(site_url + '/payments/contact/list/' + contact_id, function(data){

                if (data.phone != null)
                {
                    var phone = data.phone;
                }
                else
                {
                    var phone = '';
                }

                if (data.address != null)
                {
                    var address = data.address  + "<br>" + phone;
                }
                else
                {
                    var address = '';
                }

                if (data.receivable != null)
                {
                    var receivable = data.receivable;
                }
                else
                {
                    var receivable = 0;
                }

                if (data.received != null)
                {
                    var received = data.received;
                }
                else
                {
                    var received = 0;
                }

                var dues = parseFloat(data.dues);

                $("#customerName").empty();
                $("#customerName").html(data.name);
                $("#customerAddress").empty();
                $("#customerAddress").html(address);
                $("#totalReceivable").empty();
                $("#totalReceivable").html(receivable);
                $("#totalReceived").empty();
                $("#totalReceived").html(parseFloat(received));
                $("#totalDues").empty();
                $("#totalDues").html(dues);    
            });
        }
    </script>

    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }
    </script>
@endsection