@extends('layouts.app')

@push('scripts')
<style type="text/css">
    body[data-sidebar=dark].vertical-collpsed {
        min-height: 0px !important;
    }
    
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>
@endpush

@section('title', 'Generate Bill')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Accounts</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Generate Bill</a></li>
                                    <li class="breadcrumb-item active">Create Bill</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                
                <form id="FormSubmit" action="{{ route('generate_bill_store_digiedu') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            	{{ csrf_field() }}
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
                        <div style="height: 271px" class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
                                            <label class="control-label">Bill Type *</label>
                                            <select id="bill_type" name="bill_type" style="width: 100%;cursor: pointer" class="form-control" required>
                                                <option>--Select Type--</option>
                                                <option value="1">One Time Payment</option>
                                                <option value="2">Service Charge</option>
                                                <option value="3">SMS Purchase</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                    	<div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
	                                        <label class="control-label">{{ __('messages.search_contact')}} *</label>
	                                        <select style="width: 100%;cursor: pointer" name="customer_id" class="form-control select2">
	                                        	<option value="">--{{ __('messages.select_contact')}}--</option>
												@if(!empty($customers))
													@foreach($customers as $key => $customer)
													<option @if(isset($find_customer)) {{ $find_customer['id'] == $customer['id'] ? 'selected' : '' }} @endif value="{{ $customer->id }}">{{ $customer->name }}</option>
													@endforeach
												@endif
	                                        </select>
	                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

        						<div class="row">
				                	<div class="col-sm-6">
				                        <div class="form-group">
				                            <label for="payment_date">Bill Date *</label>
				                            <input id="payment_date" name="payment_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
				                        </div>
				                    </div>

				                    <div class="col-sm-6">
				                        <div class="form-group">
				                            <label for="amount">{{ __('messages.amount')}} *</label>
				                            <input id="amount" name="amount" type="text" class="form-control">
				                        </div>
				                    </div>

			                        <div class="col-sm-12">
				                        <div class="form-group">
				                            <label for="note">{{ __('messages.note')}}</label>
				                            <input id="note" name="note" type="text" class="form-control">
				                        </div>
				                    </div>
			                	</div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('generate_bill_create') }}">{{ __('messages.close')}}</a></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                
                <hr>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center">SL</th>
                                            <th style="text-align: center">Date</th>
                                            <th style="text-align: center">Digiedu ID</th>
                                            <th style="text-align: center">Client</th>
                                            <th style="text-align: center">Particular</th>
                                            <th style="text-align: center">Amount</th>
                                            <th style="text-align: center">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(count($data) > 0)
                                        @foreach($data as $key => $value)
                                        <tr>
                                            <td style="text-align: center">{{ $key + 1 }}</td>
                                            <td style="text-align: center">{{ date('d-m-Y', strtotime($value->date)) }}</td>
                                            <td style="text-align: center">{{ $value->customer->digiedu_id }}</td>
                                            <td style="text-align: left">{{ $value->customer->name }}</td>
                                            <td style="text-align: left">{{ $value->note }}</td>
                                            <td style="text-align: right">{{ $value->amount }}</td>
                                            <td style="text-align: center">
                                                <div class="dropdown">
                                                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                        <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" style="">
                                                        <a class="dropdown-item" href="{{ route('generate_bill_edit_digiedu', $value['id']) }}">Edit</a>
                                                        <a class="dropdown-item" href="{{ route('generate_bill_delete_digiedu', $value['id']) }}" onclick="return confirm('Are you sure want to delete?');">Delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                    
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }
    </script>
@endsection