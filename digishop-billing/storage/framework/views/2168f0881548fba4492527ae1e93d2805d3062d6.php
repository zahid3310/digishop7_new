

<?php
    $title = 'Digishop Database Backup';
?>

<?php $__env->startSection('title', $title); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e($title); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Basic Settings</a></li>
                                    <li class="breadcrumb-item active"><?php echo e($title); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <h4 class="card-title">All Client <a style="float: right" onclick="downloadDatabase()" href="javascript: void(0);"><i class="fa fa-download font-size-15"></i>&nbsp;Download All</a></h4>
                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;text-align: center">SL</th>
                                            <th style="width: 5%;text-align: center">Digishop ID</th>
                                            <th style="width: 25%;text-align: center">Name</th>
                                            <th style="width: 35%;text-align: center">Address</th>
                                            <th style="width: 10%;text-align: center">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php if(!empty($customers) && ($customers->count() > 0)): ?>
                                        <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td style="text-align: center"><?php echo e($key + 1); ?></td>
                                                <td style="text-align: center"><?php echo e($customer['digishop_id']); ?></td>
                                                <td style="text-align: left">
                                                    <a href="<?php echo e($customer['digishop_url']); ?>" target="_blank">
                                                        <?php echo e($customer['name']); ?>

                                                    </a>
                                                </td>
                                                <td style="text-align: left"><?php echo e($customer['address']); ?></td>
                                                <td style="text-align: center">
                                                    <?php if(Auth::user()->id == 1): ?>
                                                    <a id="<?php echo e($customer['digishop_id']); ?>" href="<?php echo e($customer['digishop_url'].'/backup/source-code/download/'. $customer['digishop_id']); ?>" title="Source Code Download"><i class="fa fa-file font-size-15"></i> &nbsp;&nbsp;</a>
                                                    <?php endif; ?>
                                                    <a id="<?php echo e($customer['digishop_id']); ?>" href="<?php echo e($customer['digishop_url'].'/backup/store'); ?>" title="Database Download"><i class="fa fa-download font-size-15"></i></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    function downloadDatabase()
    {   
        //Call ajax
        // $.ajax({
        //     type : "get",
        //     url : 'https://digishop7.com/2/backup/store',
        //     success:function(response){
        //         console.log(response);
        //     }
        // });
        
        var site_url = $('.site_url').val();
        $.get(site_url + '/customers/digishop-database-backup/customer-list-all-download/', function(data){
            
            $.each( data.split( "," ), function( index, item ) {
                window.open( item, "_blank" );
            });
  
        });
    }
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/digishop-billing/Modules/Customers/Resources/views/database_backup/digishop_database_backup.blade.php ENDPATH**/ ?>