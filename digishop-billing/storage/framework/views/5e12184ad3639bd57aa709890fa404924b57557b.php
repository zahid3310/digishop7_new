

<?php
    $title = 'Edit Digiedu Clients';
?>

<?php $__env->startSection('title', $title); ?>

<?php $__env->startPush('styles'); ?>
<style type="text/css">
    body[data-sidebar=dark].vertical-collpsed {
        min-height: 0px !important;
    }
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit <?php echo e($title); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Edit <?php echo e($title); ?></a></li>
                                    <li class="breadcrumb-item active">Edit <?php echo e($title); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('customers_update_digiedue', $find_customer['id'])); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                <?php echo e(csrf_field()); ?>


                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Position</label>
                                        <input name="position" type="text" class="form-control" placeholder="Enter Position" value="<?php echo e($find_customer['position']); ?>">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Organization Name *</label>
                                        <input name="customer_name" type="text" class="form-control" placeholder="Enter Name" value="<?php echo e($find_customer['name']); ?>" required>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Admin Name</label>
                                        <input name="contact_person" type="text" class="form-control" placeholder="Enter Name" value="<?php echo e($find_customer['contact_person']); ?>">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Admin Mobile Number</label>
                                        <input name="mobile_number" type="number" class="form-control" placeholder="Enter Phone Number" value="<?php echo e($find_customer['phone']); ?>">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Admin Email</label>
                                        <input name="email" type="text" class="form-control" placeholder="Enter Email" value="<?php echo e($find_customer['email']); ?>">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Address</label>
                                        <input name="address" type="text" class="form-control" placeholder="Enter Address" value="<?php echo e($find_customer['address']); ?>">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Partner</label>
                                        <select style="cursor: pointer;width: 100%" name="partner_id" class="form-control select2" id="partner_id">
                                            <option value="" selected>--Select Partner--</option>
                                            <?php if($partners->count() > 0): ?>
                                            <?php $__currentLoopData = $partners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $partner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($partner->id); ?>" <?php echo e($partner->id == $find_customer->partner_id ? 'selected' : ''); ?>><?php echo e($partner->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Digiedu ID</label>
                                        <input name="digiedu_id" type="number" class="form-control" placeholder="Enter Digiedu ID" value="<?php echo e($find_customer['digiedu_id']); ?>">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Digiedu Link</label>
                                        <input name="digiedu_url" type="text" class="form-control" placeholder="Enter Digiedu Link" value="<?php echo e($find_customer['digiedu_url']); ?>">
                                    </div>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Contact Type *</label>
                                        <select style="cursor: pointer" name="contact_type" class="form-control" id="contact_type" required>
                                            <option value="2" selected>Client</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Bill Type</label>
                                        <select style="cursor: pointer" name="bill_type" class="form-control" id="bill_type">
                                            <option value="" selected>--Select Bill Type--</option>
                                            <option value="1" <?php echo e($find_customer['bill_type'] == 1 ? 'selected' : ''); ?>>Pre paid</option>
                                            <option value="2" <?php echo e($find_customer['bill_type'] == 2 ? 'selected' : ''); ?>>Post Paid</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Total Student</label>
                                        <input name="total_student" type="text" class="form-control" placeholder="Enter Total Student" value="<?php echo e($find_customer['total_student']); ?>">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Per Student Rate</label>
                                        <input name="per_student_rate" type="text" class="form-control" placeholder="Enter Per Student Rate" value="<?php echo e($find_customer['per_student_rate']); ?>">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Bill Amount</label>
                                        <input name="bill_amount" type="text" class="form-control" placeholder="Enter Bill Amount" value="<?php echo e($find_customer['bill_amount']); ?>"> 
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Agreement Paper</label>
                                        <input name="image" type="file">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Previous Image</label>
                                    <div class="col-md-10">
                                        <?php if($find_customer['image'] != null): ?>
                                        <img style="height: 80px;width: 80px;padding: 0px" src="<?php echo e(url('public/'.$find_customer['image'])); ?>" class="form-control">
                                        <?php else: ?>
                                        <img style="height: 80px;width: 80px;padding: 0px" src="<?php echo e(url('public/default.png')); ?>" class="form-control">
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('customers_index_digiedu')); ?>">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">All Client</h4>
                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;text-align: center">SL</th>
                                            <th style="width: 5%;text-align: center">ID</th>
                                            <th style="width: 25%;text-align: center">Name</th>
                                            <th style="width: 20%;text-align: center">Address</th>
                                            <th style="width: 10%;text-align: center">Phone</th>
                                            <th style="width: 10%;text-align: center">Bill Type</th>
                                            <th style="width: 5%;text-align: center">Rate</th>
                                            <th style="width: 15%;text-align: center">Partner</th>
                                            <th style="width: 5%;text-align: center">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php if(!empty($customers) && ($customers->count() > 0)): ?>
                                        <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td style="text-align: center"><?php echo e($key + 1); ?></td>
                                                <td style="text-align: center"><?php echo e($customer['digiedu_id']); ?></td>
                                                <td style="text-align: left">
                                                    <a href="<?php echo e($customer['digiedu_url']); ?>" target="_blank">
                                                        <?php echo e($customer['name']); ?>

                                                    </a>
                                                </td>
                                                <td style="text-align: left"><?php echo e($customer['address']); ?></td>
                                                <td style="text-align: center"><?php echo e($customer['phone']); ?></td>
                                                <td style="text-align: center"><?php echo e($customer['bill_type'] == 1 ? 'Pre Paid' : 'Post Paid'); ?></td>
                                                <td style="text-align: center"><?php echo e($customer['per_student_rate']); ?></td>
                                                <td style="text-align: left"><?php echo e($customer->partner_id != null ? $customer->partner->name : ''); ?></td>
                                                <td style="text-align: center">
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="<?php echo e(route('customers_show_digiedu', $customer['id'])); ?>" target="_blank">Details</a>
                                                            <a class="dropdown-item" href="<?php echo e(route('customers_edit_digiedu', $customer['id'])); ?>">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            
            // $('#vertical-menu-btn').click();
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/digishop-billing/Modules/Customers/Resources/views/digiedu/edit.blade.php ENDPATH**/ ?>