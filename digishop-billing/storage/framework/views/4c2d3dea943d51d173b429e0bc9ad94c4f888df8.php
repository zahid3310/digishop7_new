

<?php $__env->startSection('title', 'Show Payment'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.payment')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.payment')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.print')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['address']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['contact_number']); ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <address>
                                            <strong><?php if($payment['type'] == 0): ?> <?php echo e(__('messages.paid_by')); ?> : <?php else: ?> <?php echo e(__('messages.paid_to')); ?> : <?php endif; ?></strong><br>
                                            <?php echo e($payment['customer_name']); ?>

                                            <?php if($payment['address'] != null): ?>
                                               <br> <?php echo $payment['address']; ?> <br>
                                            <?php endif; ?>
                                            <?php if($payment['address'] == null): ?>
                                                <br>
                                            <?php endif; ?>
                                            <?php echo e($payment['phone']); ?>

                                        </address>
                                    </div>

                                    <div class="col-sm-4">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div class="col-sm-4 text-sm-right">
                                        <address class="mt-2 mt-sm-0">
                                            <strong><?php echo e(__('messages.payment_date')); ?>:</strong><br>
                                            <?php echo e(date('d-m-Y', strtotime($payment['payment_date']))); ?><br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <h3 class="font-size-15 font-weight-bold"><?php echo e(__('messages.payment_details')); ?></h3>
                                </div>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                   
                                    <thead>
                                        <tr>
                                            <th><?php echo e(__('messages.payment_date')); ?></th>
                                            <th><?php echo e(__('messages.payment_number')); ?></th>
                                            <th><?php echo e(__('messages.paid_through')); ?></th>
                                            <th style="text-align: right"><?php echo e(__('messages.amount')); ?></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td><?php echo e(date('d-m-Y', strtotime($payment['payment_date']))); ?></td>
                                            <td><?php echo e('PM - ' . str_pad($payment['payment_number'], 6, "0", STR_PAD_LEFT)); ?></td>
                                            <td><?php echo e($payment->paidThrough->account_name); ?></td>
                                            <td style="text-align: right"><?php echo e(number_format($payment['amount'],0,'.',',')); ?></td>
                                        </tr>        
                                    </tbody>
                                        
                                </table>

                                <?php if($payment['note'] != null): ?>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6><strong>Note :</strong> <?php echo e($payment['note']); ?></h6>
                                    </div>
                                </div>
                                <?php endif; ?>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="position: fixed;bottom: 0">
                                    <!-- <h4 class="float-right font-size-16">Order # 12345</h4> -->
                                    <!-- <div class="col-md-4">
                                        <img class="float-left" src="<?php echo e(url('public/az-ai.png')); ?>" alt="logo" height="20"/>
                                    </div>

                                    <div class="col-md-4">
                                        <h2 style="text-align: center"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="line-height: 0px;text-align: center"><?php echo e($user_info['address']); ?></p>
                                        <p style="line-height: 0px;text-align: center"><?php echo e($user_info['contact_number']); ?></p>
                                    </div>

                                    <div class="col-md-4">
                                        <p style="line-height: 18px;text-align: right;padding: 0px">Phone - 01718937082<br>01711418731<br>01711418731</p>
                                    </div> -->

                                    <?php if($user_info['footer_image'] != null): ?>
                                        <img class="float-left" src="<?php echo e(url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image'])); ?>" alt="logo" style="width: 100%" />
                                    <?php endif; ?>
                                </div>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/digishop-billing/Modules/Payments/Resources/views/digiedu/show.blade.php ENDPATH**/ ?>