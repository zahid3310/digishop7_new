@extends('layouts.app')

@section('title', 'Print Check')

<style>
    input[type="text" i] {
        padding: 1px 2px;
        border: none; 
    }
</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Accounts</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Print Checks</a></li>
                                    <li class="breadcrumb-item active">Print</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="padding: 10px;padding-top: 0px" class="row">
                    <div style="padding-bottom: 10px" class="d-print-none col-md-12">
                        <div class="float-right">
                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <div style="margin-top: 20px" class="row checkSize">
                                    <div style="font-size: 15px;margin-bottom: 10px" class="col-md-7">
                                        <address style="margin-bottom: 0px !important">
                                            <strong></strong>
                                        </address>
                                    </div>

                                    <div style="font-size: 17px;margin-top: 1.3cm;margin-right: -5cm" class="col-md-5 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <input type="text" style="width: .65cm;height: .6cm;padding-left: 12px;padding-top:8px;" value="{{ $check['d1'] }}">
                                            <input type="text" style="width: .65cm;height: .6cm;padding-left: 6px;padding-top:8px;" value="{{ $check['d2'] }}">
                                            <input type="text" style="width: .65cm;height: .6cm;padding-left: 6px;padding-top:8px;" value="{{ $check['m1'] }}">
                                            <input type="text" style="width: .65cm;height: .6cm;padding-left: 6px;padding-top:8px;" value="{{ $check['m2'] }}">
                                            <input type="text" style="width: .65cm;height: .6cm;padding-left: 6px;padding-top:8px;" value="{{ $check['y1'] }}">
                                            <input type="text" style="width: .65cm;height: .6cm;padding-left: 6px;padding-top:8px;" value="{{ $check['y2'] }}">
                                            <input type="text" style="width: .65cm;height: .6cm;padding-left: 6px;padding-top:8px;" value="{{ $check['y3'] }}">
                                            <input type="text" style="width: .65cm;height: .6cm;padding-left: 6px;padding-top:8px;" value="{{ $check['y4'] }}">
                                        </address>
                                    </div>

                                    <div style="font-size: 22px;margin-top: .5cm" class="col-md-10">
                                        <address style="margin-bottom: 0px !important;padding-left: 6cm">
                                            {{ $check['pay_to'] }}
                                        </address>
                                    </div>

                                    <div style="font-size: 20px;margin-top: 1cm" class="col-md-2 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <strong></strong>
                                        </address>
                                    </div>

                                    <div style="font-size: 22px;margin-top: -20px;margin-left: 10px;" class="col-8">
                                        <address style="margin-bottom: 0px !important;padding-left: 7.5cm">
                                            {{ numberTowords($check['amount']) }}
                                        </address>
                                    </div>

                                    <div style="font-size: 20px" class="col-3">
                                        <address style="margin-bottom: 0px !important;padding-left: 3.5cm;margin-top: 15px;">
                                            <span style="height: 1.2cm;width: 5.3cm"> {{ number_format($check['amount'],0,'.',',') }}</span>
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
</script>
@endsection
