<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\SalesReturn;
use App\Models\SalesReturnEntries;
use App\Models\PurchaseReturn;
use App\Models\PurchaseReturnEntries;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\Expenses;
use App\Models\ExpenseEntries;
use App\Models\Users;
use Carbon\Carbon;
use DB;
use Auth;
use Response;
use Artisan;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth()->user()->status == 1)
        {
            return view('home');
        }
        else
        {
            return back();
        }
    }

    public function dashboardItems()
    {
        $branch_id                  = Auth::user()->branch_id;
        $sells                      = Invoices::where('type', 1)->where('branch_id', $branch_id)->get();
        $sells_return               = SalesReturn::where('branch_id', $branch_id)->get();
        $purchases                  = Bills::where('type', 1)->where('branch_id', $branch_id)->get();
        $purchases_return           = PurchaseReturn::where('branch_id', $branch_id)->get();
        $expenses                   = Expenses::where('branch_id', $branch_id)->get();
        $customers                  = Customers::where('branch_id', $branch_id)->get();

        $total_customers            = $customers->where('contact_type', 0)->count();
        $total_suppliers            = $customers->where('contact_type', 1)->count();
        $todays_sells               = $sells->where('invoice_date', date('Y-m-d'))->sum('invoice_amount');
        $todays_sells_return        = $sells_return->where('sales_return_date', date('Y-m-d'))->sum('return_amount');
        $todays_expense             = $expenses->where('expense_date', date('Y-m-d'))->sum('amount');

        $sales_summary              = Invoices::whereYear('invoice_date', date('Y'))
                                                ->where('branch_id', $branch_id)
                                                ->where('type', 1)
                                                ->select(DB::raw('MONTH(invoice_date) month'),
                                                       DB::raw('SUM(invoice_amount) total'))
                                                ->groupBy(DB::raw('MONTH(invoice_date)'))
                                                ->get();

        $purchase_summary           = Bills::whereYear('bill_date', date('Y'))
                                                ->where('branch_id', $branch_id)
                                                ->where('type', 1)
                                                ->select(DB::raw('MONTH(bill_date) month'),
                                                       DB::raw('SUM(bill_amount) total'))
                                                ->groupBy(DB::raw('MONTH(bill_date)'))
                                                ->get();


        $data['sales_summary']              = $sales_summary;
        $data['purchase_summary']           = $purchase_summary;
        $data['total_customers']            = $total_customers;
        $data['total_suppliers']            = $total_suppliers;
        $data['todays_sells']               = $todays_sells;
        $data['todays_sells_return']        = $todays_sells_return;
        $data['todays_expenses']            = $todays_expense;
        $data['total_invoices']             = $sells->count();
        $data['total_bills']                = $purchases->count();
        $data['total_sells']                = $sells->sum('invoice_amount');
        $data['total_purchase']             = $purchases->sum('bill_amount');
        $data['customer_dues']              = Customers::where('contact_type', 0)->sum('balance');
        $data['supplier_dues']              = Customers::where('contact_type', 1)->sum('balance');;

        return Response::json($data);
    }

    public function stockOutItems()
    {
        $products   = ProductEntries::whereRaw('product_entries.stock_in_hand <= product_entries.alert_quantity')
                                ->count();

        return Response::json($products);
    }
}
