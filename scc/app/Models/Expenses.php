<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expenses extends Model
{
    protected $table = "expenses";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function expenseCategory()
    {
        return $this->belongsTo('App\Models\ExpenseCategories','expense_category_id');
    }

    public function paidThrough()
    {
        return $this->belongsTo('App\Models\PaidThroughAccounts','paid_through_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function paidThroughAccount()
    {
        return $this->belongsTo('App\Models\Accounts','paid_through_id');
    }

    public function account()
    {
        return $this->belongsTo('App\Models\Accounts','account_id');
    }
}
