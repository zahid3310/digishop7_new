

<?php $__env->startSection('title', 'Edit Salary Grades'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Salary Grades</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Basic Settings</a></li>
                                    <li class="breadcrumb-item active">Edit Salary Grades</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('salary_grades_update', $find_grade->id)); ?>" method="post" files="true" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>


                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Grade Name</label>
                                        <input type="text" class="inner form-control" id="name" name="name" placeholder="Enter Grade Name" value="<?php echo e($find_grade->name); ?>" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Position</label>
                                        <input type="number" class="inner form-control" id="position" name="position" placeholder="Enter Grade Position" value="<?php echo e($find_grade->position); ?>" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Basic Salary</label>
                                        <input type="text" class="inner form-control" id="basic_salary" name="basic_salary" placeholder="Enter Basic Salary" oninput="calculateGrossAmount()" value="<?php echo e($find_grade->basic); ?>" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">House Rent</label>
                                        <input type="text" class="inner form-control" id="house_rent" name="house_rent" placeholder="Enter House Rent" oninput="calculateGrossAmount()" value="<?php echo e($find_grade->house_rent); ?>" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Medical Allowance</label>
                                        <input type="text" class="inner form-control" id="medical" name="medical" placeholder="Enter Medical Allowance" oninput="calculateGrossAmount()" value="<?php echo e($find_grade->medical); ?>" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Convence Bill</label>
                                        <input type="text" class="inner form-control" id="convence" name="convence" placeholder="Enter Convence Bill" oninput="calculateGrossAmount()" value="<?php echo e($find_grade->convence); ?>" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Food</label>
                                        <input type="text" class="inner form-control" id="food" name="food" placeholder="Enter Food Bill" oninput="calculateGrossAmount()" value="<?php echo e($find_grade->food); ?>" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Mobile Bill</label>
                                        <input type="text" class="inner form-control" id="mobile_bill" name="mobile_bill" placeholder="Enter Mobile Bill" oninput="calculateGrossAmount()" value="<?php echo e($find_grade->mobile); ?>" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Others</label>
                                        <input type="text" class="inner form-control" id="others" name="others" placeholder="Enter Other Allowance" oninput="calculateGrossAmount()" value="<?php echo e($find_grade->others); ?>" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Gross</label>
                                        <input type="text" class="inner form-control" id="gross" name="gross"   value="<?php echo e($find_grade->gross); ?>" readonly/>
                                    </div>
                                </div>  

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('salary_grades_index')); ?>">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    function calculateGrossAmount()
    {
        var getBasic        = $("#basic_salary").val();
        var getHouseRent    = $("#house_rent").val();
        var getMedical      = $("#medical").val();
        var getConvence     = $("#convence").val();
        var getFood         = $("#food").val();
        var getMobileBill   = $("#mobile_bill").val();
        var getOthers       = $("#others").val();

        if (getBasic == '')
        {
            var basicSalary = 0;
        }
        else
        {
            var basicSalary = $("#basic_salary").val();
        }

        if (getHouseRent == '')
        {
            var houseRent = 0;
        }
        else
        {
            var houseRent = $("#house_rent").val();
        }

        if (getMedical == '')
        {
            var medical = 0;
        }
        else
        {
            var medical = $("#medical").val();
        }

        if (getConvence == '')
        {
            var convence = 0;
        }
        else
        {
            var convence = $("#convence").val();
        }

        if (getFood == '')
        {
            var food = 0;
        }
        else
        {
            var food = $("#food").val();
        }

        if (getMobileBill == '')
        {
            var mobileBill = 0;
        }
        else
        {
            var mobileBill = $("#mobile_bill").val();
        }

        if (getOthers == '')
        {
            var others = 0;
        }
        else
        {
            var others = $("#others").val();
        }

        var grossSalary = parseFloat(basicSalary) + parseFloat(houseRent) + parseFloat(medical) + parseFloat(convence) + parseFloat(food) + parseFloat(mobileBill) + parseFloat(others);

        $("#gross").val(grossSalary);
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/scc/Modules/Payroll/Resources/views/SalaryGrades/edit.blade.php ENDPATH**/ ?>