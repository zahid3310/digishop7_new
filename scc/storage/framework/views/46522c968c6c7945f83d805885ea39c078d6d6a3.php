

<?php $__env->startSection('title', 'Add Increament'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Add Increament</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Payroll</a></li>
                                    <li class="breadcrumb-item active">Add Increament</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('salary_increaments_store')); ?>" method="post" files="true" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>


                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Employee * </label>
                                        <select style="width: 100%" id="employee_id" name="employee_id" class="form-control select2" required onchange="employeeStatements()">
                                           <option value="">--Select Employee--</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                    </div>
                                </div>

                                <hr style="margin: 0px"> 

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Basic Salary</label>
                                        <input type="text" class="inner form-control" id="basic_salary" name="basic_salary" placeholder="Enter Basic Salary" oninput="calculateGrossAmount()" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">House Rent</label>
                                        <input type="text" class="inner form-control" id="house_rent" name="house_rent" placeholder="Enter House Rent" oninput="calculateGrossAmount()" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Medical Allowance</label>
                                        <input type="text" class="inner form-control" id="medical" name="medical" placeholder="Enter Medical Allowance" oninput="calculateGrossAmount()" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Convence Bill</label>
                                        <input type="text" class="inner form-control" id="convence" name="convence" placeholder="Enter Convence Bill" oninput="calculateGrossAmount()" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Food</label>
                                        <input type="text" class="inner form-control" id="food" name="food" placeholder="Enter Food Bill" oninput="calculateGrossAmount()" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Mobile Bill</label>
                                        <input type="text" class="inner form-control" id="mobile_bill" name="mobile_bill" placeholder="Enter Mobile Bill" oninput="calculateGrossAmount()" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Others</label>
                                        <input type="text" class="inner form-control" id="others" name="others" placeholder="Enter Other Allowance" oninput="calculateGrossAmount()" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Gross</label>
                                        <input type="text" class="inner form-control" id="gross" name="gross" readonly/>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Provident Fund</label>
                                        <input type="text" class="inner form-control" id="pf_amount" name="pf_amount" placeholder="Enter Provident Fund Amount" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Tax</label>
                                        <input type="text" class="inner form-control" id="tax" name="tax" placeholder="Enter Tax Amount" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Bonus</label>
                                        <input type="text" class="inner form-control" id="bonus" name="bonus" placeholder="Enter Bonus Amount" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Increament Date *</label>
                                        <input id="increament_date" name="increament_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Increament Amount</label>
                                        <input type="text" class="inner form-control" id="increament_amount" name="increament_amount" placeholder="Enter Increament Amount" />
                                    </div>
                                </div>  

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('salary_grades_index')); ?>">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    $( document ).ready(function() {
        
        var site_url  = $('.site_url').val();

        $("#employee_id").select2({
            ajax: { 
            url:  site_url + '/invoices/customer/list/invoices',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['contact_type'] == 2 && result['id'] != 0)
                {
                    return result['text'];
                }
            },
        });
    });
</script>

<script type="text/javascript">
    function calculateGrossAmount()
    {
        var getBasic        = $("#basic_salary").val();
        var getHouseRent    = $("#house_rent").val();
        var getMedical      = $("#medical").val();
        var getConvence     = $("#convence").val();
        var getFood         = $("#food").val();
        var getMobileBill   = $("#mobile_bill").val();
        var getOthers       = $("#others").val();

        if (getBasic == '')
        {
            var basicSalary = 0;
        }
        else
        {
            var basicSalary = $("#basic_salary").val();
        }

        if (getHouseRent == '')
        {
            var houseRent = 0;
        }
        else
        {
            var houseRent = $("#house_rent").val();
        }

        if (getMedical == '')
        {
            var medical = 0;
        }
        else
        {
            var medical = $("#medical").val();
        }

        if (getConvence == '')
        {
            var convence = 0;
        }
        else
        {
            var convence = $("#convence").val();
        }

        if (getFood == '')
        {
            var food = 0;
        }
        else
        {
            var food = $("#food").val();
        }

        if (getMobileBill == '')
        {
            var mobileBill = 0;
        }
        else
        {
            var mobileBill = $("#mobile_bill").val();
        }

        if (getOthers == '')
        {
            var others = 0;
        }
        else
        {
            var others = $("#others").val();
        }

        var grossSalary = parseFloat(basicSalary) + parseFloat(houseRent) + parseFloat(medical) + parseFloat(convence) + parseFloat(food) + parseFloat(mobileBill) + parseFloat(others);

        $("#gross").val(grossSalary);
    }

    function employeeStatements()
    {
        var employee_id  = $("#employee_id").val();
        var site_url     = $('.site_url').val();

        $.get(site_url + '/payroll/salary-increaments/employee-details/'+ employee_id, function(data){

            var getBasic        = data.basic;
            var getHouseRent    = data.house_rent;
            var getMedical      = data.medical;
            var getConvence     = data.convence;
            var getFood         = data.food;
            var getMobileBill   = data.mobile;
            var getOthers       = data.others;
            var getGross        = data.gross;

            if (getBasic == null)
            {
                var basicSalary = 0;
            }
            else
            {
                var basicSalary = data.basic;
            }

            if (getHouseRent == null)
            {
                var houseRent = 0;
            }
            else
            {
                var houseRent = data.house_rent;
            }

            if (getMedical == null)
            {
                var medical = 0;
            }
            else
            {
                var medical = data.medical;
            }

            if (getConvence == null)
            {
                var convence = 0;
            }
            else
            {
                var convence = data.convence;
            }

            if (getFood == null)
            {
                var food = 0;
            }
            else
            {
                var food = data.food;
            }

            if (getMobileBill == null)
            {
                var mobileBill = 0;
            }
            else
            {
                var mobileBill = data.mobile;
            }

            if (getOthers == null)
            {
                var others = 0;
            }
            else
            {
                var others = data.others;
            }

            if (getGross == null)
            {
                var gross = 0;
            }
            else
            {
                var gross = data.gross;
            }

            $("#basic_salary").val(parseFloat(basicSalary));
            $("#house_rent").val(parseFloat(houseRent));
            $("#medical").val(parseFloat(medical));
            $("#convence").val(parseFloat(convence));
            $("#food").val(parseFloat(food));
            $("#mobile_bill").val(parseFloat(mobileBill));
            $("#others").val(parseFloat(others));
            $("#others").val(parseFloat(others));
            $("#gross").val(parseFloat(gross));

        });
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/scc/Modules/Payroll/Resources/views/Increaments/create.blade.php ENDPATH**/ ?>