

<?php $__env->startSection('title', 'Chart of Accounts'); ?>

<link rel="stylesheet" href="<?php echo e(url('public/tree-view-asset/dist/listree.min.css')); ?>"/>
<script src="<?php echo e(url('public/tree-view-asset/dist/listree.umd.min.js')); ?>"></script>

<style type="text/css">
    .nonTranLi{
        color: red;
        font-weight: bold;
    }

    .tranLi{
        color: black;
        font-weight: normal;
    }

    .custom-menu {
        display: none;
        z-index: 1000;
        position: absolute;
        overflow: hidden;
        border: 1px solid #CCC;
        white-space: nowrap;
        font-family: sans-serif;
        background: #FFF;
        color: #333;
        border-radius: 5px;
        padding: 0;
    }

    .custom-menu li {
        padding: 8px 12px;
        cursor: pointer;
        list-style-type: none;
    }

    .custom-menu li:hover {
        background-color: #DEF;
    }

    .customT-menu li {
        padding: 8px 53px 8px 17px;
        cursor: pointer;
        list-style-type: none;
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Chart of Accounts</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Accounts</a></li>
                                    <li class="breadcrumb-item active">Chart of Accounts</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <div class="row">
                                    <ul class="listree">
                                        <li>
                                            <div class="listree-submenu-heading nonTranLi">Cyberdyne Technology Ltd.</div>
                                            <ul class="listree-submenu-items">
                                                <li>
                                                    <div id="ds" class="listree-submenu-heading nonTranLi" onmousedown="mousDown()">Equity & Liabilities</div>
                                                    
                                                    <ul class="listree-submenu-items">
                                                        <li>
                                                            <div class="listree-submenu-heading nonTranLi">Equity</div>
                                                            <ul class="listree-submenu-items">
                                                                <li>
                                                                    <div class="listree-submenu-heading">Item 1-1-1-1</div>
                                                                    <ul class="listree-submenu-items">
                                                                        <li>
                                                                            <a class="tranLi">Item 1-1-1-1-1</a>
                                                                        </li>
                                                                        <li>
                                                                            <a class="tranLi">Item 1-1-1-1-2</a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>

                                                        <li>
                                                            <div class="listree-submenu-heading nonTranLi">Liabilities</div>
                                                            <ul class="listree-submenu-items">
                                                                <li>
                                                                    <div class="listree-submenu-heading">Item 1-1-1-1</div>
                                                                    <ul class="listree-submenu-items">
                                                                        <li>
                                                                            <a class="tranLi">Item 1-1-1-1-1</a>
                                                                        </li>
                                                                        <li>
                                                                            <a class="tranLi">Item 1-1-1-1-2</a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>

                                                        <li>
                                                            <a class="tranLi">Opening Balance</a>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <div class="listree-submenu-heading nonTranLi">Asset & Property</div>
                                                    <ul class="listree-submenu-items">
                                                        <li>
                                                            <div class="listree-submenu-heading">Item 1-1-1</div>
                                                            <ul class="listree-submenu-items">
                                                                <li>
                                                                    <div class="listree-submenu-heading">Item 1-1-1-1</div>
                                                                    <ul class="listree-submenu-items">
                                                                        <li>
                                                                            <a href="">Item 1-1-1-1-1</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="">Item 1-1-1-1-2</a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <div class="listree-submenu-heading nonTranLi">Revenue</div>
                                                    <ul class="listree-submenu-items">
                                                        <li>
                                                            <div class="listree-submenu-heading">Item 1-1-1</div>
                                                            <ul class="listree-submenu-items">
                                                                <li>
                                                                    <div class="listree-submenu-heading">Item 1-1-1-1</div>
                                                                    <ul class="listree-submenu-items">
                                                                        <li>
                                                                            <a href="">Item 1-1-1-1-1</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="">Item 1-1-1-1-2</a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <div class="listree-submenu-heading nonTranLi">Expense</div>
                                                    <ul class="listree-submenu-items">
                                                        <li>
                                                            <div class="listree-submenu-heading">Item 1-1-1</div>
                                                            <ul class="listree-submenu-items">
                                                                <li>
                                                                    <div class="listree-submenu-heading">Item 1-1-1-1</div>
                                                                    <ul class="listree-submenu-items">
                                                                        <li>
                                                                            <a href="">Item 1-1-1-1-1</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="">Item 1-1-1-1-2</a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>

    <ul class='custom-menu'>
      <!--<li data-toggle="modal"  class="open-myModal4" href="#myModal4"  >New Transactable Item</li>-->
      <li data-toggle="modal"  class="open-nonTransactable" href="#nonTransactable" >New Parent/Control Head</li>
      <li data-toggle="modal"  class="open-myModal4" href="#myModal4"  >New Transactable Head</li>
      <hr style="margin:0px;border-color:#ddd">
      <li data-toggle="modal"  class="open-EditNonTransactable" href="#EditNonTransactable" >Edit Parent/Control Head</li> 
      <!-- <li data-toggle="modal"  class="open-Edit_Transactable" href="#Edit_Transactable" >Edit Transactable Head</li> -->
      <hr style="margin:0px;border-color:#ddd">
      <li data-toggle="modal"  class="open-printList" href="#printList">Print Chart of Accounts Head</li>
      <li data-toggle="modal"  class="open-printList" href="#printList1">Print Transactable Accounts Head</li>
    </ul>

    <button type="hidden" class="btn btn-success" id="on_mouse_down_btn" data-toggle="modal" data-target="#myModal"></button>

    <div id="myModal" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Print Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label for="productname" class="col-md-4 col-form-label">Date</label>
                                <div class="col-md-8">
                                    <input style="cursor: pointer" id="search_date" type="date" value="<?= date("Y-m-d") ?>" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Customer </label>
                                <div class="col-md-8">
                                    <input id="customer" type="text" class="form-control"  placeholder="Enter Customer Name">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Search Invoice </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <input style="width: 95%" type="text" class="form-control col-lg-9 col-md-9 col-sm-9 col-9" id="invoiceNumber" placeholder="Enter Invoice ID">
                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="printInvoicesSearch()">
                                            <i class="bx bx-search font-size-24"></i>
                                        </span>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Order#</th>
                                <th>Customer</th>
                                <th>Amount</th>
                                <th>Paid</th>
                                <th>Due</th>
                                <th>Creator</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody id="print_invoice_list">
                        </tbody>
                    </table>
                </div>
                
                <div class="modal-footer">
                    <button id="CloseButton2" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            listree();
        });

        function mousDown()
        {
            // $("#on_mouse_down_btn").click();
            $('#ds').bind("contextmenu", function (event) {
    
                // Avoid the real one
                event.preventDefault();
                
                // Show contextmenu
                $(".custom-menu").finish().toggle(100).
                
                // In the right position (the mouse)
                css({
                    top: event.pageY-25 + "px",
                    left: event.pageX+30 + "px"
                });
                
            });
        }

        $(document).bind("mousedown", function (e) {
    
            // If the clicked element is not the menu
            if (!$(e.target).parents(".custom-menu").length > 0) {
                
                // Hide it
                $(".custom-menu").hide(100);
            }
            //$(document).click(function(){$(".custom-menu").hide(100);});
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/scc/Modules/Accounts/Resources/views/chart_of_accounts/bc.blade.php ENDPATH**/ ?>