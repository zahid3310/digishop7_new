<?php

namespace Modules\Attendance\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Model
use Rats\Zkteco\Lib\ZKTeco;
use App\Models\Devices;
use App\Models\MachineDataLog;
use App\Models\DailyAttendance;
use App\Models\LeaveCategories;
use App\Models\LeaveTransactions;
use App\Models\Customers;
use App\Models\GovernmentHolidays;
use App\Models\Shifts;
use App\Models\DutyRosters;
use App\Models\Sections;
use App\Models\Departments;
use App\Models\GeneralRoster;
use Validator;
use Auth;
use Response;
use DB;
use DateTime;

class AttendanceController extends Controller
{
    public function index()
    {
        $zk = new ZKTeco('192.168.0.205');
        $zk->connect();
        $employees = $zk->getUser();

        return view('attendance::all_device_users', compact('employees'));
    }

    public function importUser()
    {
        $zk = new ZKTeco('192.168.0.201');
        $zk->connect();

        $uid = array_key_last($zk->getUser());

        return view('attendance::import_employee_to_device', compact('uid'));
    }

    public function importUserStore(Request $request)
    {
        $data = $request->all();

        $zk = new ZKTeco('192.168.0.201');
        $zk->connect();
 
        $zk->setUser($data['uid'],$data['userid'],$data['name'],$password='',$role=0);

        return back();
    }

    public function deleteUser($uid)
    {
        $zk = new ZKTeco('192.168.0.201');
        $zk->connect();
        $zk->removeUser($uid); 

        return redirect()->route('attendance_import_user_index');
    }

    public function deviceList()
    {
        // $zk = new ZKTeco('192.168.0.201');
        // $zk->connect();
        // dd('sdasd');
        $devices = Devices::get();

        return view('attendance::add_device', compact('devices'));
    }

    public function deviceStore(Request $request)
    {
        $rules = array(
            'device_ip'  => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $device               = new Devices;
            $device->device_name  = $data['device_name'];
            // $device->device_id    = ;
            // $device->device_ip    = ;
            $device->created_by   = $user_id;

            if ($device->save())
            {   
                return back()->with("success","Device Added Successfully !!");
            }
        }
        catch (\Exception $exception){
            return back()->with("unsuccess","Device Not Added");
        }
    }

    public function pullData()
    {
        $zk = new ZKTeco('192.168.0.205');
        $zk->connect();
        $serial     = explode('=', $zk->serialNumber());
        $machine_id = $serial[1];

        $tas_json_decode   = $zk->getAttendance();

        if($tas_json_decode)
        {
            $tas_machine_data = array();
            foreach($tas_json_decode as $data)
            {
                $machine_data = array(
                    'UserID'        => trim($data['uid']),
                    'DateTime'      => trim($data['timestamp']),
                    'MachineID'     => trim($machine_id),
                    'InOutStatus'   => trim($data['state'])
                );

                $tas_machine_data[] = $machine_data;
            }

            DB::table('tbl_tas_machine_data')->insert($tas_machine_data);

            $insert_data = DB::table('tbl_tas_machine_data_log')->insert($tas_machine_data);

            if($insert_data)
            {
                return back()->with('success', 'Data inserted successfully !! !!');
            }
            else
            {
                return back()->with('unsuccess', 'Something wrong.Try again !!');
            }
        }
        else
        {
            return back()->with('unsuccess', 'Something wrong.Try again !!');
        }
    }

    public function attendanceList()
    {
        $data = MachineDataLog::orderBy('timestamp', 'DESC')->get();

        return view('attendance::attendance_list', compact('data'));
    }

    public function importUserUpdate(Request $request)
    {
        $data   = $request->all();
        $zk     = new ZKTeco('192.168.0.205');
        $zk->connect();
    
        DB::beginTransaction();

        try{
            $zk->clearAdmin();

            foreach ($data['employee_id'] as $key => $value)
            {
                $zk->setUser($data['uid'][$key],$value,$data['employee_name'][$key],$password='',$role=0);
            }

            DB::commit();
            return back()->with("success","Successfully Updated");
        }catch (\Exception $exception){
            DB::rollback($exception);
            return back()->with("unsuccess","Not Added");
        }
    }
    
    function createDateRangeArray($strDateFrom,$strDateTo)
    {
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.
    
        // could test validity of dates here but I'm already doing
        // that in the main script
    
        $aryRange = [];
    
        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));
    
        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom<$iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }
        return $aryRange;
    }

    public function manualAttendanceIndex()
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End
        
        // $get_da_delete  = DailyAttendance::where('date', '2022-04-27')->delete();
        
        $date           = isset($_GET['date']) ? date('Y-m-d', strtotime($_GET['date'])) : date('Y-m-d');
        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;

        $emploees       = Customers::where('contact_type', 2)
                                    ->where('status', 1)
                                    ->when($customer_id != 0, function ($query) use ($customer_id) {
                                        return $query->where('id', $customer_id);
                                    })
                                    ->get();
                                    
        
        // $dates  = $this->createDateRangeArray('2022-06-06', '2022-06-06');
    
        // foreach($dates as $date)
        // {   
        //     foreach($emploees as $emp)
        //     {
        //         $in_out = MachineDataLog::where('date', date('Y-m-d', strtotime($date)))->where('UserID', $emp->id)->count();
        //         $in     = MachineDataLog::where('date', date('Y-m-d', strtotime($date)))->where('UserID', $emp->id)->orderBy('AID', 'ASC')->first();
        //         $out    = MachineDataLog::where('date', date('Y-m-d', strtotime($date)))->where('UserID', $emp->id)->orderBy('AID', 'DESC')->first();
                
        //         $get_data_check             = DailyAttendance::where('date', $date)->where('user_id', $emp->id)->first();
                
        //         if($in_out == 1)
        //         {
        //             $get_data_check->out_time    = $in != null ? date('H:i:s', strtotime($in->DateTime)) : null;
        //             $get_data_check->status     = 'Present';
        //             $get_data_check->save();
        //         }
                
        //         if($in_out > 1)
        //         {
        //             $get_data_check->in_time    = $in != null ? date('H:i:s', strtotime($in->DateTime)) : null;
        //             $get_data_check->out_time   = $out != null ? date('H:i:s', strtotime($out->DateTime)) : null;
        //             $get_data_check->status     = 'Present';
        //             $get_data_check->save();
        //         }
                
        //     }
            
            
            
            
        //     // foreach($emploees as $emp)
        //     // {
        //     //     $get_data_check                  = new DailyAttendance;
        //     //     $get_data_check->user_id         = $emp->id;
        //     //     $get_data_check->date            = '2022-05-28';
        //     //     $get_data_check->shift_in_date   = '2022-05-28';
        //     //     $get_data_check->shift_out_date  = '2022-05-28';
        //     //     $get_data_check->shift_in_time   = date('H:i:s', strtotime('09:00:00'));
        //     //     $get_data_check->shift_out_time  = date('H:i:s', strtotime('18:00:00'));
        //     //     $get_data_check->late_in_time    = date('H:i:s', strtotime('09:20:00'));
        //     //     $get_data_check->duty_time       = 0;
        //     //     $get_data_check->status          = 'W/H';
        //     //     $get_data_check->save();
                
        //     // }
        // }
        
        // foreach($emploees as $emp)
        // {
        //     $dds                        = DailyAttendance::where('date', '2022-05-10')->where('user_id', $emp->id)->first();
            
        //     if($dds != null)
        //     {
        //         $get_data_check             = DailyAttendance::where('date', '2022-05-24')->where('user_id', $emp->id)->first();
        //         $get_data_check->in_time    = $dds->in_time != null ? date('H:i:s', strtotime($dds->in_time)) : null;
        //         $get_data_check->out_time   = $dds->out_time != null ? date('H:i:s', strtotime($dds->out_time)) : null;
        //         $get_data_check->status     = $dds->status;
        //         $get_data_check->save();
        //     }
        // }
        
                                    
        $get_data_check   = DailyAttendance::where('date', $date)->get();
                             
        foreach ($get_data_check as $key1 => $value1)
        {   
            // $up_daily_aatn                  = DailyAttendance::find($value1->id);
            // $up_daily_aatn->shift_in_date   = date('Y-m-d', strtotime($up_daily_aatn['date']));
            // $up_daily_aatn->shift_out_date  = date('Y-m-d', strtotime($up_daily_aatn['date']));;
            // $up_daily_aatn->shift_in_time   = date('H:i:s', strtotime('09:00:00'));
            // $up_daily_aatn->shift_out_time  = date('H:i:s', strtotime('18:00:00'));
            // $up_daily_aatn->late_in_time    = date('H:i:s', strtotime('09:20:00'));
            // $up_daily_aatn->duty_time       = 0;
            // $up_daily_aatn->save();
            
            $result[$value1->customer->id]['name']        = $value1->customer->name;
            $result[$value1->customer->id]['id']          = $value1->customer->id;
            $result[$value1->customer->id]['employee_id'] = $value1->customer->employee_id;
            $result[$value1->customer->id]['designation'] = $value1->customer->designation;
            $result[$value1->customer->id]['status']      = $value1['status'];
            $result[$value1->customer->id]['shift_id']    = $value1->shift_id;
            $result[$value1->customer->id]['in_time']     = $value1['in_time'] != null ? date('H:i', strtotime($value1['in_time'])) : 0;
            $result[$value1->customer->id]['out_time']    = $value1['out_time'] != null ? date('H:i', strtotime($value1['out_time'])) : 0;
        }

        if (isset($result))
        {
            $result = $result;
        }
        else
        {
            $result = [];
        }

        $customer_name  = Customers::find($customer_id);
        $shifts         = Shifts::get();

        return view('attendance::manual_attendance', compact('date', 'result', 'customer_name', 'shifts'));
    }

    public function manualAttendanceStore(Request $request)
    {
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            foreach ($data['id'] as $key => $value)
            {   
                $in_time    = $data['in_time'][$key] != null ? date('H:i:s', strtotime($data['in_time'][$key])) : null;
                $out_time   = $data['out_time'][$key] != null ? date('H:i:s', strtotime($data['out_time'][$key])) : null;
                $log_data   = DailyAttendance::where('date', date('Y-m-d', strtotime($data['date'])))->where('user_id', $value)->first();

                if ($log_data != null)
                {
                    if(($in_time != $log_data['in_time']) || ($out_time != $log_data['out_time']) || ($data['status'][$key] != $log_data['status']) || ($data['shift_id'][$key] != $log_data['shift_id']))
                    {
                        $manual_att_data[] = [
                            'user_id'       => $value,
                            'date'          => date('Y-m-d', strtotime($data['date'])),
                            'in_time'       => $data['in_time'][$key] != null ? $in_time : null,
                            'out_time'      => $data['out_time'][$key] != null ? $out_time : null,
                            'status'        => $data['status'][$key],
                            'created_by'    => Auth::user()->id,
                            'created_at'    => date('Y-m-d H:i:s'),
                        ];
                    }
                    
                    if(($data['shift_id'][$key] == 0) || ($data['status'][$key] == 'W/H'))
                    {
			            $log_data->in_time          = null;
                        $log_data->out_time         = null;
                        $log_data->status           = 'W/H';
                        $log_data->shift_id         = 0;
                        $log_data->shift_in_date    = null;
                        $log_data->shift_out_date   = null;
                        $log_data->shift_in_time    = null;
                        $log_data->shift_out_time   = null;
                        $log_data->late_in_time     = null;
                        $log_data->save();
                    }
                    else
                    {   
                        $find_shift             = Shifts::find($data['shift_id'][$key]);
                        
                        $shift_start_date       = date('Y-m-d', strtotime($data['date']));
                        $shift_start_time       = date('H:i:s', strtotime($find_shift['start']));
                        $shift_end_time         = date('H:i:s', strtotime($find_shift['end']));

                        $add_shift_hour         = '+'.$find_shift['duty_hour'].' hours';
                        $start_date_time        = date('Y-m-d', strtotime($data['date'])) . ' ' . $find_shift['start'];
                        $shift_end_date_time    = date('Y-m-d H:i:s',strtotime($add_shift_hour,strtotime($start_date_time)));
                        $shift_end_date         = date('Y-m-d', strtotime($shift_end_date_time));
                        
                        $log_data->in_time          = $data['in_time'][$key] != null ? $in_time : null;
                        $log_data->out_time         = $data['out_time'][$key] != null ? $out_time : null;
                        $log_data->status           = $data['status'][$key];
                        $log_data->shift_id         = $data['shift_id'][$key];
                        $log_data->shift_in_date    = $shift_start_date == null ? null : date('Y-m-d', strtotime($shift_start_date));
                        $log_data->shift_out_date   = $shift_end_date == null ? null : date('Y-m-d', strtotime($shift_end_date));
                        $log_data->shift_in_time    = $shift_start_time == null ? null : date('H:i:s', strtotime($shift_start_time));
                        $log_data->shift_out_time   = $shift_end_time == null ? null : date('H:i:s', strtotime($shift_end_time));
                        $log_data->late_in_time     = $find_shift == null ? null : date('H:i:s', strtotime($find_shift['late_in']));
                        $log_data->save();
                    }
                }
            }
            
            if(isset($manual_att_data))
            {
                DB::table('manual_attendance')->insert($manual_att_data);
            }

            DB::commit();
            return back()->with("success","Attendance Updated Successfully.");

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    //Leave Category 
    public function leaveCategoryIndex()
    {
        $leave_categories  = LeaveCategories::orderBy('id', 'ASC')->get();

        return view('attendance::leave_category.index', compact('leave_categories'));
    }

    public function leaveCategoryCreate()
    {
        return view('attendance::leave_category.create');
    }

    public function leaveCategoryStore(Request $request)
    {
        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $category                   = new LeaveCategories;
            $category->name             = $data['name'];
            $category->short_name       = $data['short_name'];
            $category->total_leave      = $data['total_leave'];
            $category->created_by       = $user_id;
            
            if ($category->save())
            {
                return redirect()->route('leave_category_index')->with("success","Leave Category Created Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function leaveCategoryEdit($id)
    {
        $find_leave_category    = LeaveCategories::find($id);

        return view('attendance::leave_category.edit', compact('find_leave_category'));
    }

    public function leaveCategoryUpdate(Request $request, $id)
    {
        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $category                   = LeaveCategories::find($id);
            $category->name             = $data['name'];
            $category->short_name       = $data['short_name'];
            $category->total_leave      = $data['total_leave'];
            $category->updated_by       = $user_id;
            
            if ($category->save())
            {
                return redirect()->route('leave_category_index')->with("success","Leave Category Updated Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    //Leave Application 
    public function leaveApplicationIndex()
    {
        $leave_applications  = LeaveTransactions::orderBy('id', 'ASC')->get();

        return view('attendance::leave_application.index', compact('leave_applications'));
    }

    public function leaveApplicationCreate()
    {
        $leave_categories  = LeaveCategories::orderBy('id', 'ASC')->get();
        
        return view('attendance::leave_application.create', compact('leave_categories'));
    }

    public function leaveApplicationStore(Request $request)
    {
        $rules = array(
            'customer_id'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $leave                      = new LeaveTransactions;
            $leave->employee_id         = $data['customer_id'];
            $leave->year                = date('Y', strtotime($data['start_date']));
            $leave->application_date    = date('Y-m-d');
            $leave->leave_category      = $data['leave_type'];
            $leave->start_date          = date('Y-m-d', strtotime($data['start_date']));
            $leave->end_date            = date('Y-m-d', strtotime($data['end_date']));
            $leave->total_days          = $this->dateDiff(date('Y-m-d', strtotime($data['start_date'])), date('Y-m-d', strtotime($data['end_date'])));
            $leave->note                = $data['note'];
            $leave->reliever_id         = $data['reliever_id'];
            $leave->reliever_contact    = $data['reliever_contact'];
            $leave->status              = 1;
            $leave->created_by          = $user_id;
            
            if ($leave->save())
            {   
                DB::commit();
                return redirect()->route('leave_application_index')->with("success","Leave Application Submitted Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function leaveApplicationEdit($id)
    {
        $find_leave_category    = LeaveCategories::find($id);

        return view('attendance::leave_category.edit', compact('find_leave_category'));
    }

    public function leaveApplicationUpdate(Request $request, $id)
    {
        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $category                   = LeaveCategories::find($id);
            $category->name             = $data['name'];
            $category->short_name       = $data['short_name'];
            $category->total_leave      = $data['total_leave'];
            $category->updated_by       = $user_id;
            
            if ($category->save())
            {
                return redirect()->route('leave_category_index')->with("success","Leave Category Updated Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    function dateDiff($date1, $date2)
    {
        $date1_ts   = strtotime($date1);
        $date2_ts   = strtotime($date2);
        $diff       = $date2_ts - $date1_ts;

        return round($diff / 86400) + 1;
    }

    function createRange($start, $end, $format = 'Y-m-d') 
    {
        $start  = new DateTime($start);
        $end    = new DateTime($end);
        $invert = $start > $end;

        $dates      = array();
        $dates[]    = $start->format($format);
        while ($start != $end) 
        {
            $start->modify(($invert ? '-' : '+') . '1 day');
            $dates[] = $start->format($format);
        }

        return $dates;
    }

    public function getDeData($employee_id)
    {
        $data['employee_info']  = Customers::leftjoin('sections', 'sections.id', 'customers.section_id')
                                        ->leftjoin('departments', 'departments.id', 'sections.department_id')
                                        ->where('customers.id', $employee_id)
                                        ->selectRaw('customers.*, departments.name as department_name, sections.name as section_name')
                                        ->first();

        $leave_data             = LeaveTransactions::where('employee_id', $employee_id)->get();
        $leave_category_data    = LeaveCategories::get();

        foreach($leave_category_data as $key => $value)
        {
            $result['short_name'][]         = $value->short_name;
            $result['authorized_leave'][]   = $value->total_leave;
            $result['availed_leave'][]      = $leave_data->where('leave_category', $value->id)->sum('total_days');
        }



        $data['other_info']      = $result;

        return Response::json($data);
    }
    
    //Attendance Configuration 
    public function attendanceConfigurationEdit()
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End
        
        // $get_da_delete  = DailyAttendance::where('date', '2022-04-27')->delete();
        
        $date           = isset($_GET['date']) ? date('Y-m-d', strtotime($_GET['date'])) : date('Y-m-d');
        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;

        $emploees       = Customers::where('contact_type', 2)
                                    ->where('status', 1)
                                    ->when($customer_id != 0, function ($query) use ($customer_id) {
                                        return $query->where('id', $customer_id);
                                    })
                                    ->get();
                                    
        $get_data_check   = DailyAttendance::where('date', $date)->get();
                             
        foreach ($get_data_check as $key1 => $value1)
        { 
            $result[$value1->customer->id]['name']        = $value1->customer->name;
            $result[$value1->customer->id]['id']          = $value1->customer->id;
            $result[$value1->customer->id]['employee_id'] = $value1->customer->employee_id;
            $result[$value1->customer->id]['designation'] = $value1->customer->designation;
            $result[$value1->customer->id]['status']      = $value1['status'];
            $result[$value1->customer->id]['in_time']     = $value1['in_time'] != null ? date('H:i', strtotime($value1['in_time'])) : 0;
            $result[$value1->customer->id]['out_time']    = $value1['out_time'] != null ? date('H:i', strtotime($value1['out_time'])) : 0;
        }

        if (isset($result))
        {
            $result = $result;
        }
        else
        {
            $result = [];
        }

        $customer_name  = Customers::find($customer_id);

        return view('attendance::manual_attendance', compact('date', 'result', 'customer_name'));
    }

    public function attendanceConfigurationUpdate(Request $request)
    {
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            foreach ($data['id'] as $key => $value)
            {   
                $in_time    = $data['in_time'][$key] != null ? date('H:i:s', strtotime($data['in_time'][$key])) : null;
                $out_time   = $data['out_time'][$key] != null ? date('H:i:s', strtotime($data['out_time'][$key])) : null;
                $log_data   = DailyAttendance::where('date', date('Y-m-d', strtotime($data['date'])))
                                            ->where('user_id', $value)
                                            ->first();

                if ($log_data != null)
                {
                    if(($in_time != $log_data['in_time']) || ($out_time != $log_data['out_time']))
                    {
                        $manual_att_data[] = [
                            'user_id'       => $value,
                            'date'          => date('Y-m-d', strtotime($data['date'])),
                            'in_time'       => $data['in_time'][$key] != null ? $in_time : null,
                            'out_time'      => $data['out_time'][$key] != null ? $out_time : null,
                            'status'        => $data['status'][$key],
                            'created_by'    => Auth::user()->id,
                            'created_at'    => date('Y-m-d H:i:s'),
                        ];
                    }
                    
                    $log_data->in_time  = $data['in_time'][$key] != null ? $in_time : null;
                    $log_data->out_time = $data['out_time'][$key] != null ? $out_time : null;

                    if ($log_data['in_time'] || $log_data['out_time'])
                    {
                        $log_data->status   = $data['status'][$key];
                    }

                    $log_data->save();
                }
            }
            
            if(isset($manual_att_data))
            {
                DB::table('manual_attendance')->insert($manual_att_data);
            }
            
            DB::table('manual_attendance')->whereNull('in_time')->whereNull('out_time')->delete();

            DB::commit();
            return back()->with("success","Attendance Updated Successfully.");

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }
    
    //Holiday Calender 
   public function holidayCalenderIndex()
    {
        $holidays  = GovernmentHolidays::orderBy('id', 'ASC')->orderBy('Year', 'DESC')->get();
        
        return view('attendance::holiday_calender.index', compact('holidays'));
    }

    public function holidayCalenderCreate()
    {
        return view('attendance::holiday_calender.create');
    }

    public function holidayCalenderStore(Request $request)
    {
        $rules = array(
            'name'          => 'required',
            'start_date'    => 'required',
            'end_date'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $leave                      = new GovernmentHolidays;
            $leave->title               = $data['name'];
            $leave->description         = $data['description'];
            $leave->year                = date('Y', strtotime($data['year']));
            $leave->start_date          = date('Y-m-d', strtotime($data['start_date']));
            $leave->end_date            = date('Y-m-d', strtotime($data['end_date']));
            $leave->created_by          = $user_id;
            
            if ($leave->save())
            {   
                DB::commit();
                return redirect()->route('holiday_calender_index')->with("success","Calender Added Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function holidayCalenderEdit($id)
    {
        $find_holidays  = GovernmentHolidays::find($id);

        return view('attendance::holiday_calender.edit', compact('find_holidays'));
    }

    public function holidayCalenderUpdate(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required',
            'start_date'    => 'required',
            'end_date'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $leave                      = GovernmentHolidays::find($id);
            $leave->title               = $data['name'];
            $leave->description         = $data['description'];
            $leave->year                = date('Y', strtotime($data['year']));
            $leave->start_date          = date('Y-m-d', strtotime($data['start_date']));
            $leave->end_date            = date('Y-m-d', strtotime($data['end_date']));
            $leave->updated_by          = $user_id;
            
            if ($leave->save())
            {   
                DB::commit();
                return redirect()->route('holiday_calender_index')->with("success","Calender Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }
    
    //Shifts 
    public function shiftsIndex()
    {
        $shifts  = Shifts::orderBy('id', 'ASC')->get();
        
        return view('attendance::shifts.index', compact('shifts'));
    }

    public function shiftsCreate()
    {
        return view('attendance::shifts.create');
    }

    public function shiftsStore(Request $request)
    {
        $rules = array(
            'name'          => 'required',
            'start_time'    => 'required',
            'end_time'      => 'required',
            'late_in_time'  => 'required',
            'duty_hour'     => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $shifts                     = new Shifts;
            $shifts->name               = $data['name'];
            $shifts->start              = date('H:i:s', strtotime($data['start_time']));
            $shifts->end                = date('H:i:s', strtotime($data['end_time']));
            $shifts->late_in            = date('H:i:s', strtotime($data['late_in_time']));
            $shifts->duty_hour          = $data['duty_hour'];
            $shifts->over_time_start    = $data['overtime_start'] != null ? date('H:i:s', strtotime($data['overtime_start'])) : null;
            $shifts->over_time_end      = $data['overtime_end'] != null ? date('H:i:s', strtotime($data['overtime_end'])) : null;
            $shifts->created_by         = $user_id;
            
            if ($shifts->save())
            {  
                DB::commit();
                return redirect()->route('shifts_index')->with("success","Shift Added Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function shiftsEdit($id)
    {
        $find_shift  = Shifts::find($id);
        
        return view('attendance::shifts.edit', compact('find_shift'));
    }

    public function shiftsUpdate(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required',
            'start_time'    => 'required',
            'end_time'      => 'required',
            'late_in_time'  => 'required',
            'duty_hour'     => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $shifts                     = Shifts::find($id);
            $shifts->name               = $data['name'];
            $shifts->start              = date('H:i:s', strtotime($data['start_time']));
            $shifts->end                = date('H:i:s', strtotime($data['end_time']));
            $shifts->late_in            = date('H:i:s', strtotime($data['late_in_time']));
            $shifts->duty_hour          = $data['duty_hour'];
            $shifts->over_time_start    = $data['overtime_start'] != null ? date('H:i:s', strtotime($data['overtime_start'])) : null;
            $shifts->over_time_end      = $data['overtime_end'] != null ? date('H:i:s', strtotime($data['overtime_end'])) : null;
            $shifts->updated_by         = $user_id;
            
            if ($shifts->save())
            {  
                DB::commit();
                return redirect()->route('shifts_index')->with("success","Shift Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }
    
    //Duty Roster 
    public function dutyRosterIndex()
    {
        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $section_id     = isset($_GET['section_id']) ? $_GET['section_id'] : 0;
        $department_id  = isset($_GET['department_id']) ? $_GET['department_id'] : 0;

        $emploees       = Customers::where('contact_type', 2)
                                    ->where('status', 1)
                                    ->when($customer_id != 0, function ($query) use ($customer_id) {
                                        return $query->where('id', $customer_id);
                                    })
                                    ->when($section_id != 0, function ($query) use ($section_id) {
                                        return $query->where('section_id', $section_id);
                                    })
                                    ->when($department_id != 0, function ($query) use ($department_id) {
                                        return $query->where('department_id', $department_id);
                                    })
                                    ->get();
                                    
        
        foreach ($emploees as $key1 => $value1)
        {   
            $get_data_check   = DutyRosters::where('employee_id', $value1->id)->first();
            
            $result[$value1->id]['name']        = $value1->name;
            $result[$value1->id]['id']          = $value1->id;
            $result[$value1->id]['employee_id'] = $value1->employee_id;
            $result[$value1->id]['designation'] = $value1->designation;
            $result[$value1->id]['department']  = $value1->department_id != null ? $value1->department->name : '';
            $result[$value1->id]['section']     = $value1->section_id != null ? $value1->section->name : '';
            $result[$value1->id]['sat']         = $get_data_check != null ? $get_data_check['sat'] : null;
            $result[$value1->id]['sun']         = $get_data_check != null ? $get_data_check['sun'] : null;
            $result[$value1->id]['mon']         = $get_data_check != null ? $get_data_check['mon'] : null;
            $result[$value1->id]['tue']         = $get_data_check != null ? $get_data_check['tue'] : null;
            $result[$value1->id]['wed']         = $get_data_check != null ? $get_data_check['wed'] : null;
            $result[$value1->id]['thu']         = $get_data_check != null ? $get_data_check['thu'] : null;
            $result[$value1->id]['fri']         = $get_data_check != null ? $get_data_check['fri'] : null;
        }

        if (isset($result))
        {
            $result = $result;
        }
        else
        {
            $result = [];
        }

        $customer_name      = Customers::find($customer_id);
        $section_name       = Sections::find($section_id);
        $department_name    = Departments::find($department_id);
        $shifts             = Shifts::orderBy('id', 'ASC')->get();
    
        return view('attendance::duty_roster.index', compact('result', 'customer_name', 'shifts', 'section_name', 'department_name'));
    }

    public function dutyRosterStore(Request $request)
    {
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            foreach ($data['id'] as $key => $value)
            {   
                $log_data   = DutyRosters::where('employee_id', $value)->first();

                if ($log_data != null)
                {
                    $log_data->fri    = $data['fri'][$key];
                    $log_data->sat    = $data['sat'][$key];
                    $log_data->sun    = $data['sun'][$key];
                    $log_data->mon    = $data['mon'][$key];
                    $log_data->tue    = $data['tue'][$key];
                    $log_data->wed    = $data['wed'][$key];
                    $log_data->thu    = $data['thu'][$key];
                    $log_data->save();
                }
                else
                {  
                    $add_log_data               = new DutyRosters;
                    $add_log_data->employee_id  = $data['id'][$key];
                    $add_log_data->fri          = $data['fri'][$key];
                    $add_log_data->sat          = $data['sat'][$key];
                    $add_log_data->sun          = $data['sun'][$key];
                    $add_log_data->mon          = $data['mon'][$key];
                    $add_log_data->tue          = $data['tue'][$key];
                    $add_log_data->wed          = $data['wed'][$key];
                    $add_log_data->thu          = $data['thu'][$key];
                    $add_log_data->save();
                }
            }
            
            DB::commit();
            return back()->with("success","Roster Updated Successfully.");

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }
    
    public function getAllSection()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Sections::orderBy('id', 'ASC')->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Sections::where('name', 'LIKE', "%$search%")
                                    ->orderBy('id', 'ASC')
                                    ->get();
        }

        $data       = array();
        $i          = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name']);

            $i++;
        }
   
        return Response::json($data);
    }
    
    public function getAllDepartment()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Departments::orderBy('id', 'ASC')->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Departments::where('name', 'LIKE', "%$search%")
                                    ->orderBy('id', 'ASC')
                                    ->get();
        }

        $data       = array();
        $i          = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name']);

            $i++;
        }
   
        return Response::json($data);
    }
}
