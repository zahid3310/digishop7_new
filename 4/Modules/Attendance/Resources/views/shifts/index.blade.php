@extends('layouts.app')

@section('title', 'Shifts')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Shifts</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Basic Settings</a></li>
                                    <li class="breadcrumb-item active">Shift</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {!! Session::get('success') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('unsuccess'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! Session::get('unsuccess') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('errors'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! 'Some required fields are missing..!! Please try again..' !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif
                        
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<h4 class="card-title">All Shift</h4>

                                <br>

                                <table id="datatable" class="table table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Title</th>
                                            <th>Shift Time</th>
                                            <th>Late In</th>
                                            <th>Duty Hour</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                    	@if(!empty($shifts) && ($shifts->count() > 0))
                                    	@foreach($shifts as $key => $shift)
	                                        <tr>
	                                            <td>{{ $key + 1 }}</td>
                                                <td>{{ $shift->name }}</td>
                                                <td>{{ date('g:i A', strtotime($shift['start'])) . ' To ' . date('g:i A', strtotime($shift['end'])) }}</td>
                                                <td>{{ date('g:i A', strtotime($shift['late_in'])) }}</td>
                                                <td>{{ $shift['duty_hour'] . ' hours' }}</td>
	                                            <td>
	                                            	<div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                             <a class="dropdown-item" href="{{ route('shifts_edit', $shift['id']) }}">Edit</a> 
                                                        </div>
                                                    </div>
	                                            </td>
	                                        </tr>
	                                    @endforeach
	                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection