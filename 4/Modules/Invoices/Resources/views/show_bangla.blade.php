<!DOCTYPE html >
<html>

<head>
    <meta charset="utf-8">
    <title>Cash Memo</title>
    <link href="https://fonts.maateen.me/bangla/font.css" rel="stylesheet">

    <style>
        *{margin:0;padding:0;outline:0}

        body {
            font-size:14px;
            line-height:18px;
            color:#000;
            height:292mm;
            width: 203mm;/*297*/
            margin:0 auto;
            font-family: 'Bangla', Arial, sans-serif !important;  
        }

        table,th {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 10px;
        }

        td {
            border-left: 1px solid black;
            border-right: 1px solid black;
            padding: 10px;
        }

        .bg_templete{
            background:url(images/bg3.png) no-repeat;
            background-size: 100%;
            -webkit-print-color-adjust: exact;
        }

        .templete{
            width:90%;
            background:none;
            margin:0 auto;
        }
        
        .clear{
            overflow:hidden;
        }

        .text-center{
            text-align: center;
        }

        .text-end{
            text-align: end;
        }

        .item2 {
            grid-area: com_logo;
        }

        .item3 {
            grid-area: com_info;
        }

        .item4 {
            grid-area: other_info;
        }

        .grid-container {
            display: grid;
            grid-template-areas: "com_logo com_info other_info ";
            /*padding-top: 10px;*/
        }

        .grid-container div {
            text-align: center;
        }

        .grid-container-aline-end {
            text-align: end !important;
        }

        .grid-container-aline-start {
            text-align: start !important;
        }

        .com_logo_img {
            height: 80px;
            width: 80px;
        }

        .tr-height{
            padding: 10px;
        }

        .item11 { grid-area: sig_text2; }
        .item22 { grid-area: some_info; }
        .item33 { grid-area: sig1; }
        .item44 { grid-area: sig2; }
        .item55 { grid-area: sig_text1; }

        .grid-container-1 {
            display: grid;
            grid-template-areas:
            ' sig1 sig1 some_info some_info sig2 sig2'
            ' sig_text1 sig_text1 some_info some_info sig_text2 sig_text2';
        }

        .signaturesection1 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 45px;
        }

        .signaturesection2 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 45px;
        }

        @page {
            size: A4;
            page-break-after: always;
        }
    </style>
</head>

<body>
    <div class="bg_templete clear">

        <div class="templete clear">

            <br>
            <br>
            <p style="line-height: 2;text-align: center;padding-left: 53px"><span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 20px;background-color: #174833;color: white;margin-right: 130px;">ক্যাশ মেমো</span></p>
            <div class="grid-container">
                <div class="item2 grid-container-aline-start">
                    <img class="com_logo_img" src="{{ url('public/'.userDetails()->logo) }}"alt="qr sample"
                    />
                </div>

                <div class="item3">
                    <h2 style="line-height: 1;font-size: 60px;color: #800000">মেসার্স টাইলস ফ্যাশান</h2>
                    <p style="line-height: 2;text-align: center"><span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 20px;background-color: #174833;color: white">প্রোঃ মোঃ খাইরুল হোসেন</span></p>
                </div>

                <div class="item4 grid-container-aline-end">

                    <p style="padding-right: 0px;line-height: 1;font-size: 20px"><span style="font-weight: bold;color: #2E2B3F">মোবাঃ </span>
                        <span style="font-weight: bold;color: #2E2B3F">০১৩১১-৬৯২৮৪১</span> <br>
                        <span style="font-weight: bold;color: #174833">০১৩১১-৬৯২৮৫০</span> <br>
                        <span style="font-weight: bold;color: #2E2B3F">০১৭২১-৭৬১৫১৩</span>
                    </p>  
                </div>
            </div>

            <div class="row">
                <h4 style="line-height: 1;font-size: 20px;text-align: center;color: blue">এখানে দেশী-বিদেশী উন্নতমানের সকল প্রকার টাইলস ও স্যানিটারী সামগ্রী খুচরা ও পাইকারী বিক্রয় করা হয় ।</h4>
                <p style="line-height: 1;font-size: 20px;text-align: center;color: #800000">ডিলারঃ মীর, স্টার, গ্রেটওয়াল, ডি.বি.এল ও বি.এইচ.এল সিরামিকস লিঃ</p>
                <p style="line-height: 1;font-size: 20px;text-align: center;color: #174833">নওদাপাড়া, আমচত্বর, রাজশাহী</p>
                <p style="line-height: 1;font-size: 18px;text-align: center;color: blue"><span>Email: tilesfashion06@gmail.com</span></p>
            </div>

            <hr style="margin-top: 10px">
            
            <!-- <div style="padding-top: 5px;text-align: center" class="col-md-12"><input type="checkbox"> Customer Copy &nbsp;&nbsp;&nbsp;<input type="checkbox"> Office Copy</div> -->

            <div style="font-size: 18px;padding-top: 5px;line-height: 1.5">
                <span style="background-color: #800000;padding: 3px;color: white;float: left">নং  &nbsp;</span>  
                <span style="border: 1px solid black;padding: 2px;float: left;width: 150px">{{ $invoice['invoice_number'] }}</span>  

                <span style="border: 1px solid black;padding: 2px;float: right;width: 135px">{{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}</span>  
                <span style="background-color: #652328;padding: 3px;color: white;float: right">তারিখ  &nbsp;</span>
            </div>

            <br>
            <br>

            <div style="font-size: 18px;line-height: 1.5">
                <span style="background-color: #00008B;padding: 3px;color: white;float: left">নাম &nbsp;&nbsp; </span>
                <span style="border: 1px solid black;padding: 2px;float: left;width: 639px">{{ $invoice['customer_name'] != null ? $invoice['customer_name'] : $invoice['contact_name'] }}</span>
            </div>

            <br>
            <br>

            <div style="font-size: 18px;line-height: 1.5">
                <span style="background-color: #800080;padding: 3px;color: white;float: left">ঠিকানা </span>
                <span style="border: 1px solid black;padding: 2px;float: left;width: 641px">{{ $invoice['customer_address'] != null ? $invoice['customer_address'] : $invoice['contact_address'] . ' .' }}</span>
            </div>

            <br>
            <br>

            <div style="padding-top: 10px;padding-bottom: 20px">
                <table style="width: 100%">
                    <tr style="background-color: #043871">
                        <th style="font-size: 18px;width: 5%;color: white;font-weight: normal">সংখ্যা</th>
                        <th style="font-size: 18px;width: 40%;color: white;font-weight: normal">কোম্পানির নাম</th>
                        <th style="font-size: 18px;width: 10%;color: white;font-weight: normal">কোড</th>
                        <th style="font-size: 18px;width: 10%;color: white;font-weight: normal">গ্রেড</th>
                        <th style="font-size: 18px;width: 15%;color: white;font-weight: normal">পরিমাণ</th>
                        <th style="font-size: 18px;width: 1%;color: white;font-weight: normal">দর</th>
                        <th style="font-size: 18px;width: 10%;color: white;font-weight: normal">টাকা</th>
                    </tr>

                    @if($entries->count() > 0)

                    <?php
                        $total_amount                   = 0;
                    ?>

                    @foreach($entries as $key => $value)
                    <?php
                        $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                        $variation_name = ProductVariationName($value['product_entry_id']);

                        if ($value['product_code'] != null)
                        {
                            $productCode  = $value['product_code'];
                        }
                        else
                        {
                            $productCode  = '';
                        }

                        if ($value['product_name'] != null)
                        {
                            $category  = ' - '.$value['product_name'];
                        }
                        else
                        {
                            $category  = '';
                        }

                        if ($value['brand_name'] != null)
                        {
                            $brandName  = $value['brand_name'];
                        }
                        else
                        {
                            $brandName  = '';
                        }

                        if (($value['height'] != null) && ($value['width'] != null))
                        {
                            $dimension  = ' - '.$value['height'] . ' X ' . $value['width'];
                        }
                        else
                        {
                            $dimension  = '';
                        }

                        if ($value['unit_name'] != null)
                        {
                            $unit  = ' '.$value['unit_name'];
                        }
                        else
                        {
                            $unit  = ' SFT';
                        }

                        if ($variation_name != null)
                        {
                            $variation  = ' '.$variation_name;
                        }
                        else
                        {
                            $variation  = '';
                        }

                        $pre_dues = $value['contact_type'] != 4 ? previousDues($invoice['customer_id']) : 0;
                        $net_paya = $value['contact_type'] != 4 ? round($total_amount, 2) : 0;
                        $paid     = $value['contact_type'] != 4 ? round($invoice['invoice_amount'] - $invoice['due_amount'], 2) : 0;
                        $dues     = $value['contact_type'] != 4 ? round($net_paya - $paid, 2) : 0;
                    ?>

                    <tr class="tr-height">
                        <td style="text-align: center">{{ $key + 1 }}</td>
                        <td style="padding-left: 30px">{{ $brandName . $dimension }}</td>
                        <td style="padding-left: 10px">{{ $productCode }}</td>
                        <td style="padding-left: 10px">{{ $value->grade_id != null ? $value->grade->name : '' }}</td>
                        <td style="text-align: center">{{ $value['quantity'] . $unit }}</td>
                        <td style="text-align: center">{{ $value['contact_type'] != 4 ? $value['rate'] : '' }}</td>
                        <td style="text-align: center">{{ $value['contact_type'] != 4 ? round($value['total_amount'], 2) : '' }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <?php
                        if ($invoice['vat_type'] == 0)
                        {
                            $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                        }
                        else
                        {
                            $vat_amount  = $invoice['total_vat'];
                        }

                        if ($invoice['total_discount_type'] == 0)
                        {
                            $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                        }
                        else
                        {
                            $discount_on_total_amount  = $invoice['total_discount_amount'];
                        }
                    ?>

                    <tr>
                        <th style="text-align: left" colspan="5"></th>
                        <th style="text-align: right"><strong>সর্বমোট</strong></th>
                        <th style="text-align: center">{{ $net_paya != 0 ? round($net_paya - $invoice['total_discount'], 2) : '' }}</th>
                    </tr>
                </table>
            </div>

            <div class="grid-container-1">
                <div class="item33">
                    <span style="font-weight: normal;">In Words :  {{ $value['contact_type'] != 4 ? numberTowords(round($net_paya - $invoice['total_discount'] - $discount_on_total_amount + $vat_amount)) : '' }}</span>
                </div> 
            </div>

            <div class="grid-container">

                <div class="item2 grid-container-aline-start">
                    <p class="signaturesection1" style="font-size: 18px;text-align: center;background-color: #043871;color: white;padding: 8px;font-weight: normal">ক্রেতার স্বাক্ষর</p>
                </div>

                <div class="item3">
                    <p style="font-size: 18px;text-align: center;background-color: #043871;color: white;padding: 8px;margin-top: 45px;border-top: 1px solid black">বিঃ দ্রঃ বিক্রিত মাল ফেরত নেওয়া হয় না ।</p>
                </div>

                <div class="item4 grid-container-aline-end">
                    <p class="signaturesection2" style="font-size: 18px;text-align: center;background-color: #043871;color: white;padding: 8px;font-weight: normal">বিক্রেতার স্বাক্ষর</p>
                </div>
            </div>
        </div>
    </div>
</body>

</html>