<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Shifts extends Model
{  
    protected $table = "shifts";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

}
