<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class AccountTypes extends Model
{  
    protected $table = "account_types";

    public function parentAccountType()
    {
        return $this->belongsTo('App\Models\ParentAccountTypes','parent_account_type_id');
    }
}
