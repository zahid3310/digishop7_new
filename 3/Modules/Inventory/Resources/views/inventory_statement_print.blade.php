<!DOCTYPE html>
<html>

<head>
    <title>Statement of Sales</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>   
</head>

<body id="print-container-body" class="printwindow">

    <div style="display: none;">
        <button id="btnExport">Export to excel</button>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="" style="">

                        <div style="width:50%;">
                            <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                        </div>

                        <div class="company-head" style="text-align: right; min-height: 82px;">
                            <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                            <p>{{ $user_info['address'] }}</p>
                            <!-- <p>phone</p> -->
                            <!-- <p>Email</p> -->
                            <!-- <p>Website</p> -->
                            <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                        </div>

                        <br>

                        <div class="ibox-title" style="padding: 14px 15px 0px;">
                            <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Statement of Inventory</h6>
                        </div>

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                    <th style="text-align: center">Branch</th>
                                    <th style="text-align: center">ITEM CATEGORY</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>

                                    <td style="text-align: center">
                                        @if($branch_id_name != null)
                                        {{ 'Branch - ' . $branch_id_name['name'] }}
                                        @else
                                        ALL
                                        @endif
                                    </td>
                                    <td style="text-align: center">
                                        @if($item_category_name != null)
                                        {{ $item_category_name['name'] }}
                                        @else
                                        ALL
                                        @endif
                                    </td>

                                </tr>
                            </tbody>
                        </table>

                        <div class="ibox-content">


                            <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead class="theight">
                                    <tr style="background:#ddd;">
                                        <th style="text-align: center;width: 3%">SL</th>
                                        <th style="text-align: center;width: 6%">T/DATE</th>
                                        <th style="text-align: center;width: 7%">Branch</th>
                                        <th style="text-align: center;width: 7%">Invoice No</th>
                                        <th style="text-align: center;width: 6%">ITEM CODE</th>
                                        <th style="text-align: center;width: 11%">NAME</th>
                                        <th style="text-align: center;width: 4%">U/M</th>
                                        <th style="text-align: center;width: 4%">Qty</th>
                                        <th style="text-align: center;width: 4%">Purchase PRICE</th>

                                    </tr>
                                </thead>

                                <tbody class="theight">

                                    <?php 
                                    $i                  = 1;
                                    ?>

                                    @foreach($data as $key => $value)

                                    @if(isset($value->inventoryEntries[0]))
                                    <?php

                                    if (isset($value->inventoryEntries[0]))
                                    {
                                        $rowspan                = $value->inventoryEntries->count();
                                    }

                                    ?>

                                    <tr>
                                        <td style="text-align: center;vertical-align: middle" rowspan="{{ $rowspan }}">{{ $i }}</td>

                                        <td style="text-align: center;vertical-align: middle" rowspan="{{ $rowspan }}">{{ isset($value['inventory_date']) ? $value['inventory_date'] : $value['inventory_date'] }}</td>

                                        <td style="text-align: center;vertical-align: middle" rowspan="{{ $rowspan }}">{{ isset($value->branch['name']) ? $value->branch['name'] : $value->branch['name'] }}</td>

                                        <td style="text-align: center;vertical-align: middle" rowspan="{{ $rowspan }}">

                                            {{ isset($value['inventory_number']) ? 'INV - ' . str_pad($value['inventory_number'], 6, "0", STR_PAD_LEFT) : 'INV - ' . str_pad($value['inventory_number'], 6, "0", STR_PAD_LEFT) }}

                                        </td>

                                        <td style="text-align: center;">{{ isset($value->inventoryEntries[0]->productEntries->product_code) ? str_pad($value->inventoryEntries[0]->productEntries->product_code, 6, "0", STR_PAD_LEFT) : '' }}</td>

                                        <td style="text-align: left;">{{ isset($value->inventoryEntries[0]) ? $value->inventoryEntries[0]->productEntries->name : $value->salesReturnEntries[0]->productEntries->name }}</td>

                                        <td style="text-align: center;">{{ isset($value->inventoryEntries[0]) ? $value->inventoryEntries[0]->convertedUnit->name : $value->salesReturnEntries[0]->convertedUnit->name }}</td>

                                        <td style="text-align: center;">{{ isset($value->inventoryEntries[0]) ? $value->inventoryEntries[0]['quantity'] : $value->salesReturnEntries[0]['quantity'] }}</td>

                                        <td style="text-align: right;">{{ isset($value->inventoryEntries[0]) ? $value->inventoryEntries[0]['rate'] : $value->salesReturnEntries[0]['rate'] }}</td>


                                    </tr>


                                    @if(isset($value->inventoryEntries))

                                    @foreach($value->inventoryEntries as $key1 => $value1)

                                    @if($key1 != 0)

                                    <tr>
                                        <td style="text-align: center;">{{ str_pad($value1->productEntries->product_code, 6, "0", STR_PAD_LEFT) }}</td>
                                        <td style="text-align: left;">{{ $value1->productEntries->name }}</td>
                                        <td style="text-align: center;">{{ $value1['unit_name'] }}</td>
                                        <td style="text-align: center;">{{ $value1['quantity'] }}</td>
                                        <td style="text-align: right;">{{ $value1['rate'] }}</td>
                                    </tr>

                                    @endif
                                    @endforeach

                                    @else

                                    @if($key2 != 0)
                                    <tr>
                                        <td style="text-align: center;">{{ str_pad($value2->productEntries->product_code, 6, "0", STR_PAD_LEFT) }}</td>
                                        <td style="text-align: left;">{{ $value2->productEntries->name }}</td>
                                        <td style="text-align: center;">{{ $value2['unit_name'] }}</td>
                                        <td style="text-align: center;">{{ $value2['quantity'] }}</td>
                                        <td style="text-align: right;">{{ $value2['rate'] }}</td>

                                    </tr>
                                    @endif

                                    @endif

                                    @endif
                                    <?php $i++; ?>
                                    @endforeach
                                </tbody>

                            </table>

                            <div>
                                <table class="table table-striped table-hover dataTables-example">
                                    <thead>
                                        <tr>
                                            <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
    <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>