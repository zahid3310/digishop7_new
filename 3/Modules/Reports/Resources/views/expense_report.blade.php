@extends('layouts.app')

@section('title', 'Expense Report')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Expense Report</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Expense Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['organization_name'] }}</h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['address'] }}</p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['contact_number'] }}</p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">Expense Report</h4>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><strong>From</strong> {{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }} <strong>To</strong> {{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="{{ route('expense_report_index') }}" enctype="multipart/form-data">
                                    <div @if(Auth()->user()->branch_id != 1) style="display: none" @endif class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Select Branch</label>
                                        <div class="col-md-5">
                                            <select style="width: 100%" id="branch_id" name="branch_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" required>
                                                <option value="0" {{ $branch_id == 0 ? 'selected' : '' }}>--All Branch--</option>
                                                @if($branches->count() > 0)
                                                @foreach($branches as $key => $value)
                                                <option value="{{ $value['id'] }}" {{ $branch_id == $value['id'] ? 'selected' : '' }}>{{ $value['name'] }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-12">
                                        <div style="margin-bottom: 10px" class="col-lg-5 col-md-5 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="{{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="{{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        <div class="col-lg-5 col-md-5 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="account_head" style="cursor: pointer" name="account_id" class="form-control select2" required>
                                                <option value="0">--All--</option>
                                                @if(!empty($accounts) && ($accounts->count() > 0))
                                                @foreach($accounts as $key => $account)
                                                    <option value="{{ $account['id'] }}" {{ $account_id == $account['id'] ? 'selected' : '' }}>{{ $account['account_name'] }}</option>
                                                @endforeach
                                                @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Branch</th>
                                            <th>Date</th>
                                            <th>Expense#</th>
                                            <th>Paid Through</th>
                                            <th>Account Head</th>
                                            <th>Note</th>
                                            <th style="text-align: right">Amount</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        <?php
                                            $serial         = 1; 
                                            $total_amount   = 0; 
                                        ?>
                                        
                                        @if(!empty($expenses) && ($expenses->count() > 0))
                                        @foreach($expenses as $key => $value)
                                            <tr>
                                                <td>{{ $serial }}</td>
                                                <td>{{ $value->branch->name }}</td>
                                                <td>{{ date('d-m-Y', strtotime($value['expense_date'])) }}</td>
                                                <td>
                                                    <a href="{{ route('expenses_show', $value->id) }}" target="_blank">
                                                        {{ 'EXP - ' . str_pad($value['expense_number'], 6, "0", STR_PAD_LEFT) }}
                                                    </a>
                                                </td>
                                                <td>{{ $value->paidThroughAccount->account_name }}</td>
                                                <td>{{ $value->account->account_name }}</td>
                                                <td>{{ $value['note'] }}</td>
                                                <td style="text-align: right">{{ number_format($value['amount'],0,'.',',') }}</td>
                                            </tr>

                                            <?php 
                                                $serial++;
                                                $total_amount   = $total_amount + $value['amount'];
                                            ?>

                                        @endforeach
                                        @endif

                                    </tbody>
                                    <tr>
                                        <th style="text-align: right" colspan="7">Total</th>
                                        <th style="text-align: right">{{ number_format($total_amount,0,'.',',') }}</th>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection