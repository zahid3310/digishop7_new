@extends('layouts.app')

@section('title', 'List Of Products')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">List Of Products</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Products</a></li>
                                    <li class="breadcrumb-item active">List Of Products</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {!! Session::get('success') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('unsuccess'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! Session::get('unsuccess') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('errors'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! 'Some required fields are missing..!! Please try again..' !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">List Of Products</h4>

                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th style="text-align: center">Major Category</th>
                                            <th style="text-align: center">Purchase Price</th>
                                            <th style="text-align: center">R/Price</th>
                                            <th style="text-align: center">W/Price</th>
                                            <th style="text-align: center">Stock</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($products) && ($products->count() > 0))
                                        @foreach($products as $key => $product)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $product['product_code'] }}</td>
                                                <td>
                                                    {{ $product['name'] }}
                                                    <?php
                                                        if ($product['product_type'] == 2)
                                                        {
                                                            echo ' - ' . $product['variations'];
                                                        } 
                                                    ?>
                                                </td>
                                                <td>{{ $product['category_name'] }}</td>
                                                <td>{{ $product['brand_name'] }}</td>
                                                <!-- <td>{{ $product['supplier_name'] }}</td> -->
                                                <td style="text-align: center">{{ number_format($product['buy_price'],2,'.',',') }}</td>
                                                <td style="text-align: center">{{ number_format($product['sell_price'],2,'.',',') }}</td>
                                                <td style="text-align: center">{{ number_format($product['wholesale_price'],2,'.',',') }}</td>
                                                <td style="text-align: center">
                                                    {{ number_format($product['stock_in_hand'],2,'.',',') }} 
                                                    @if($product['stock_in_hand'] != null)
                                                       <?php echo $product['unit_name'] ?> 
                                                    @endif
                                                </td>
                                                <td>{{ $product['status'] == 1 ? 'Active' : 'Inactive' }}</td>
                                                <td>
                                                    @if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3)
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('products_edit', $product['id']) }}">Edit</a>
                                                            <a style="cursor: pointer" class="dropdown-item" data-toggle="modal" data-target="#myModal3" onclick="findSuppliers({{$product['id']}})">Add Suppliers</a>
                                                            <a style="cursor: pointer" class="dropdown-item" data-toggle="modal" data-target="#myModal4" onclick="findCustomers({{$product['id']}})">Add Customers</a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal3" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Suppliers List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">

                    <div style="padding-top: 0px !important" class="modal-body">
                        <form action="{{ route('products_supplier_list_update') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                        {{ csrf_field() }}

                        <input type="hidden" name="product_id" id="product_id">

                        <div class="row">
                            <div class="col-md-3">
                                <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                    <label for="productname" class="col-md-4 col-form-label"></label>
                                    <div class="col-md-8">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                    <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Add Supplier </label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <select style="width: 75%" id="select_supplier_id" name="select_supplier_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                               <option value="0">--Select Supplier--</option>
                                            </select>
                                            <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="AddSupplier()">
                                                <i class="bx bx-plus font-size-24"></i>
                                            </span>

                                            <div class="col-md-12">
                                                <p style="color: red;display: none" class="alert">Already added</p>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                            <div class="col-md-4">
                                <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                    <label style="text-align: right" for="productname" class="col-md-4 col-form-label"></label>
                                    <div class="col-md-8">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Supplier Name</th>
                                    <th>Whole Sale Rate</th>
                                    <th>Retail Rate</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody id="supplier_list" class="getMultipleRowSuppliers input_fields_wrap">
                            </tbody>
                        </table>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="submitBtn3" class="btn btn-primary waves-effect waves-light">Save</button>
                        <button id="CloseButton3" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal4" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Customers List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">

                    <div style="padding-top: 0px !important" class="modal-body">
                        <form action="{{ route('products_customer_list_update') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                        {{ csrf_field() }}

                        <input type="hidden" name="product_id_customer" id="product_id_customer">

                        <div class="row">
                            <div class="col-md-3">
                                <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                    <label for="productname" class="col-md-4 col-form-label"></label>
                                    <div class="col-md-8">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                    <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Add Customer </label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <select style="width: 75%" id="select_customer_id" name="select_customer_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                               <option value="0">--Select Customer--</option>
                                            </select>
                                            <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="AddCustomer()">
                                                <i class="bx bx-plus font-size-24"></i>
                                            </span>

                                            <div class="col-md-12">
                                                <p style="color: red;display: none" class="alert">Already added</p>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                            <div class="col-md-4">
                                <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                    <label style="text-align: right" for="productname" class="col-md-4 col-form-label"></label>
                                    <div class="col-md-8">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Customer Name</th>
                                    <th>Whole Sale Rate</th>
                                    <th>Retail Rate</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody id="customer_list" class="getMultipleRowCustomers input_fields_wrap_customer">
                            </tbody>
                        </table>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="submitBtn4" class="btn btn-primary waves-effect waves-light">Save</button>
                        <button id="CloseButton4" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#select_supplier_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 1 && result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#select_customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0 && result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn2').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = 1;
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton2').click();
                        }
                        
                        $("#supplier_id").empty();
                        $('#supplier_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });   
    </script>

    <script type="text/javascript">
        function findSuppliers(product_id)
        {
            var site_url = $(".site_url").val();

            $("#product_id").val(product_id);

            $.get(site_url + '/products/get-supplier-list/'+ product_id, function(data){

                var supplier_list  = '';
                var serial         = 1;
                $.each(data, function(i, supplier_data)
                {
                    if (serial == 1)
                    {
                        var sl_label                    = '<label class="hidden-xs" for="productname">SL</label>\n';
                        var supplier_label              = '<label class="hidden-xs" for="productname">Supplier Name</label>\n';
                        var whole_sale_rate_label       = '<label class="hidden-xs" for="productname">Whole Sale Rate</label>\n';
                        var retail_sale_rate_label      = '<label class="hidden-xs" for="productname">Retail Rate</label>\n';
                        var action_label                = '<label class="hidden-xs" for="productname">Action</label>\n';
                    }
                    else
                    {
                        var sl_label                    = '';
                        var supplier_label              = '';
                        var whole_sale_rate_label       = '';
                        var retail_sale_rate_label      = '';
                        var action_label                = '';
                    }

                    supplier_list += ' ' + '<tr class="di_'+serial+'">' +
                                                '<td>' + serial + '</td>' +

                                                '<input type="hidden" name="sup_id[]" value="'+ supplier_data.supplier_id +'" class="SupplierID">' +
                                                '<input type="hidden" id="serial" class="serial" value="'+ serial +'">' +
                                                
                                                '<td>' + 
                                                    supplier_data.supplier_name + 
                                                '</td>' +

                                                '<td>' +
                                                    '<input type="text" class="form-control" name="whole_rate[]" value="'+ supplier_data.whole_sale_price +'">' + 
                                                '</td>' +

                                                '<td>' + 
                                                    '<input type="text" class="form-control" name="retail_rate[]" value="'+ supplier_data.retail_price +'">' + 
                                                '</td>' +

                                                '<td>' + 
                                                    '<i id="remove_'+serial+'" style="padding: 0.48rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field" data-val="'+serial+'" onclick="RemoveSupplier()"></i>' +
                                                '</td>' +
                                            '</tr>';

                                            serial++;                             
                });

                $("#supplier_list").empty();
                $("#supplier_list").append(supplier_list);
            });
        }

        function AddSupplier()
        {
            $('.alert').hide();  

            var supplierId      = $('#select_supplier_id').val();
            var supplierName    = $("#select_supplier_id option:selected").text();
            var serials         = parseInt($(".serial:last").val());

            if (isNaN(serials))
            {   
                var serial      = 0;
            }
            else
            {
                var serial      = parseInt($(".serial:last").val());
            }

            var checkSupplier   = [];
            $('.suppliseID').each(function()
            {
                var supplier_id    = $(this).val();

                if(checkValue(supplier_id, checkSupplier) == 'Not exist')
                {
                    checkSupplier.push(supplier_id);
                }
                
            });

            if((checkValue(supplierId, checkSupplier) == 'Not exist') && (supplierId != 0))
            {
                serial++;
                $('.getMultipleRowSuppliers').append(' ' + '<tr class="di_'+serial+'">' +
                                                    '<td>' + serial + '</td>' +

                                                    '<input type="hidden" name="sup_id[]" value="'+ supplierId +'" class="suppliseID">' +
                                                    '<input type="hidden" id="serial" class="serial" value="'+ serial +'">' +
                                                    
                                                    '<td>' + 
                                                        supplierName + 
                                                    '</td>' +

                                                    '<td>' +
                                                        '<input type="text" class="form-control" name="whole_rate[]" value="0">' + 
                                                    '</td>' +

                                                    '<td>' + 
                                                        '<input type="text" class="form-control" name="retail_rate[]" value="0">' + 
                                                    '</td>' +

                                                    '<td>' + 
                                                        '<i id="remove_'+serial+'" style="padding: 0.48rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field" data-val="'+serial+'" onclick="RemoveSupplier()"></i>' +
                                                    '</td>' +
                                                '</tr>' 
                                                );
            }
            else
            {
              $('.alert').show();  
            }
        }

        function checkValue(value,arr)
        {
            var status = 'Not exist';
             
            for(var i=0; i<arr.length; i++)
            {
                var name = arr[i];
                if(name == value)
                {
                    status = 'Exist';
                    break;
                }
            }

            return status;
        }

        function RemoveSupplier()
        {
            var wrapper          = $(".input_fields_wrap"); 

            $(wrapper).on("click",".remove_field", function(e)
            {
                e.preventDefault();

                var serial = $(this).attr("data-val");

                $('.di_'+serial).remove(); serial--;
            });
        }
    </script>

    <script type="text/javascript">
        function findCustomers(product_id)
        {
            var site_url = $(".site_url").val();

            $("#product_id_customer").val(product_id);

            $.get(site_url + '/products/get-customer-list/'+ product_id, function(data){

                var customer_list  = '';
                var serials         = 1;
                $.each(data, function(i, customer_data)
                {
                    if (serials == 1)
                    {
                        var sl_label                    = '<label class="hidden-xs" for="productname">SL</label>\n';
                        var supplier_label              = '<label class="hidden-xs" for="productname">Customer Name</label>\n';
                        var whole_sale_rate_label       = '<label class="hidden-xs" for="productname">Whole Sale Rate</label>\n';
                        var retail_sale_rate_label      = '<label class="hidden-xs" for="productname">Retail Rate</label>\n';
                        var action_label                = '<label class="hidden-xs" for="productname">Action</label>\n';
                    }
                    else
                    {
                        var sl_label                    = '';
                        var supplier_label              = '';
                        var whole_sale_rate_label       = '';
                        var retail_sale_rate_label      = '';
                        var action_label                = '';
                    }

                    customer_list += ' ' + '<tr class="di_customer_'+serials+'">' +
                                                '<td>' + serials + '</td>' +

                                                '<input type="hidden" name="cus_id[]" value="'+ customer_data.customer_id +'" class="customerID">' +
                                                '<input type="hidden" id="serial_customer" class="serialCustomer" value="'+ serials +'">' +
                                                
                                                '<td>' + 
                                                    customer_data.supplier_name + 
                                                '</td>' +

                                                '<td>' +
                                                    '<input type="text" class="form-control" name="whole_rate[]" value="'+ customer_data.whole_sale_price +'">' + 
                                                '</td>' +

                                                '<td>' + 
                                                    '<input type="text" class="form-control" name="retail_rate[]" value="'+ customer_data.retail_price +'">' + 
                                                '</td>' +

                                                '<td>' + 
                                                    '<i id="customer_remove_'+serials+'" style="padding: 0.48rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field_customer" data-val="'+serials+'" onclick="RemoveCustomer()"></i>' +
                                                '</td>' +
                                            '</tr>';

                                            serials++;                             
                });

                $("#customer_list").empty();
                $("#customer_list").append(customer_list);
            });
        }

        function AddCustomer()
        {
            $('.alert').hide();  

            var customerId      = $('#select_customer_id').val();
            var customerName    = $("#select_customer_id option:selected").text();
            var serialss        = parseInt($(".serialCustomer:last").val());

            if (isNaN(serialss))
            {   
                var serials      = 0;
            }
            else
            {
                var serials      = parseInt($(".serialCustomer:last").val());
            }

            var checkCustomer   = [];
            $('.customerID').each(function()
            {
                var customer_id    = $(this).val();

                if(checkValue(customer_id, checkCustomer) == 'Not exist')
                {
                    checkCustomer.push(customer_id);
                }
                
            });

            if((checkValue(customerId, checkCustomer) == 'Not exist') && (customerId != 0))
            {
                serials++;
                $('.getMultipleRowCustomers').append(' ' + '<tr class="di_customer_'+serials+'">' +
                                                    '<td>' + serials + '</td>' +

                                                    '<input type="hidden" name="cus_id[]" value="'+ customerId +'" class="customerID">' +
                                                    '<input type="hidden" id="serial_customer" class="serialCustomer" value="'+ serials +'">' +
                                                    
                                                    '<td>' + 
                                                        customerName + 
                                                    '</td>' +

                                                    '<td>' +
                                                        '<input type="text" class="form-control" name="whole_rate[]" value="0">' + 
                                                    '</td>' +

                                                    '<td>' + 
                                                        '<input type="text" class="form-control" name="retail_rate[]" value="0">' + 
                                                    '</td>' +

                                                    '<td>' + 
                                                        '<i id="customer_remove_'+serials+'" style="padding: 0.48rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field_customer" data-val="'+serials+'" onclick="RemoveCustomer()"></i>' +
                                                    '</td>' +
                                                '</tr>' 
                                                );
            }
            else
            {
              $('.alert').show();  
            }
        }

        function RemoveCustomer()
        {
            var wrapper          = $(".input_fields_wrap_customer"); 

            $(wrapper).on("click",".remove_field_customer", function(e)
            {
                e.preventDefault();

                var serials = $(this).attr("data-val");

                $('.di_customer_'+serials).remove(); serials--;
            });
        }
    </script>
@endsection