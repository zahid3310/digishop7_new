@extends('layouts.app')

@section('title', 'Print Pos')

@if($user_info['pos_printer'] == 1)
<style type="text/css">
    /*@media print {*/
    /*    a[href]:after {*/
    /*        content:"" !important;*/
    /*    }*/

    /*    header nav, footer {*/
    /*        display: none;*/
    /*    }*/

    /*    @page {*/
    /*        margin: 0cm ! important;*/
    /*        margin-top: 0cm ! important;*/
    /*        color: black;*/
    /*    }*/

    /*    .card {*/
    /*        width: 90mm;*/
    /*        padding: 0px;*/
    /*        color: black;*/
    /*    }*/

    /*    .card-body{*/
    /*        padding: 0rem !important;*/
    /*    }*/

    /*    ::-webkit-scrollbar {*/
    /*        display: none;*/
    /*    }*/

    /*    .marginTopPrint{*/
    /*        margin-top: -0px !important;*/
    /*    }*/
    /*}*/
</style>
@endif

@if($user_info['pos_printer'] == 0)
<style type="text/css">
    @media print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 60mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -80px !important;
        }
    }
</style>
@endif

@section('content')
    <div class="main-content marginTopPrint">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row d-print-none">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Orders</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">Print Pos</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                @if($user_info['pos_printer'] == 1)
                 <center>
                    <div class="row" style="width:7in!important">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row" style="display:none;">
                                    @if($user_info['header_image'] == null)
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">{{ $user_info['organization_name'] }}</h2>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_number'] }}</p>
                                        </div>
                                    @else
                                        <img class="float-left" src="{{ url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>

                                <!-- <hr> -->

                                <div class="row" style="padding-top: 1.4in!important;">
                                    <div style="font-size: 16px;" class="col-sm-4 col-6">
                                        <address style="text-align: left;">
                                            <strong>Billed To:</strong><br>
                                            {{ $invoice['customer_name'] }}
                                            @if($invoice['address'] != null)
                                               <br> <?php echo $invoice['address']; ?> <br>
                                            @endif
                                            @if($invoice['address'] == null)
                                                <br>
                                            @endif
                                            {{ $invoice['phone'] }}
                                        </address>
                                    </div>

                                    <div class="col-sm-4 hidden-xs">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div style="font-size: 16px" class="col-sm-4 col-6 text-sm-right">
                                        <address>
                                            <strong>Order Date:</strong><br>
                                            {{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}<br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6 hidden-xs" style="text-align: left;">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Order summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Order # {{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }}</h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th>No.</th>
                                                <th>Item</th>
                                                <th class="text-center">Rate</th>
                                                <th class="text-center">Qty</th>
                                                <th class="text-center">Discount</th>
                                                <th class="text-center">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">

                                            @if(!empty($entries) && ($entries->count() > 0))

                                            <?php $sub_total = 0; ?>

                                            @foreach($entries as $key => $value)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>
                                                    @if($value['product_variations_id'] == null)
                                                        <?php echo $value['product_entry_name']; ?>
                                                    @else
                                                        <?php echo $value['product_variations_name']; ?>
                                                    @endif
                                                </td>
                                                <td class="text-right">{{ number_format($value['rate'],2,'.',',') }}</td>
                                                <td class="text-right">{{ number_format($value['quantity'],2,'.',',') . ' ' .$value['unit_name'] }}</td>
                                                <td class="text-right">
                                                    <?php $total_dis = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; ?>
                                                    @if($value['discount_type'] == 0)
                                                        <?php echo number_format($total_dis,2,'.',','); ?><br>
                                                        <?php echo '('.$value['discount_amount'].'%'.')'; ?>
                                                    @else
                                                        <?php echo number_format($value['discount_amount'],2,'.',','); ?>
                                                    @endif
                                                </td>
                                                <td class="text-right">{{ number_format($value['total_amount'],2,'.',',') }}</td>
                                            </tr>

                                            <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                            @endforeach
                                            @endif
                                            
                                            <tr>
                                                <td style="font-size: 16px" colspan="5" class="text-right">Sub Total</td>
                                                <td style="font-size: 16px" class="text-right">{{ number_format($sub_total,2,'.',',') }}</td>
                                            </tr>
                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px" colspan="5" class="border-0 text-right">
                                                    Discount On Total Amount ({{ $invoice['total_discount_type'] == 0 ? $invoice['total_discount_amount'].'%' : 'BDT' }})</td>
                                                <td style="font-size: 16px" class="border-0 text-right">{{ $invoice['total_discount_type'] == 0 ? (($sub_total*$invoice['total_discount_amount'])/100) : number_format($invoice['total_discount_amount'],2,'.',',') }}</td>
                                            </tr>
                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="5" class="border-0 text-right">Total</td>
                                                <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                    {{ number_format($invoice['invoice_amount'],2,'.',',') }}
                                                </td>
                                            </tr>
                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px" colspan="5" class="border-0 text-right">Paid</td>
                                                <td style="font-size: 16px" class="border-0 text-right">
                                                    {{ number_format($invoice['invoice_amount'] - $invoice['due_amount'],2,'.',',') }}
                                                </td>
                                            </tr>
                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px" colspan="5" class="border-0 text-right">Dues</td>
                                                <td style="font-size: 16px" class="border-0 text-right">
                                                    {{ number_format($invoice['due_amount'],2,'.',',') }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                @if($invoice['invoice_note'] != null)
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 style="font-size: 16px"><strong>Note :</strong> {{ $invoice['invoice_note'] }}</h6>
                                    </div>
                                </div>
                                @endif

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="position: fixed;bottom: 0">
                                    <!-- <h4 class="float-right font-size-16">Order # 12345</h4> -->
                                    <!-- <div class="col-md-4">
                                        <img class="float-left" src="{{ url('public/az-ai.png') }}" alt="logo" height="20"/>
                                    </div>

                                    <div class="col-md-4">
                                        <h2 style="text-align: center">{{ $user_info['organization_name'] }}</h2>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['address'] }}</p>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['contact_number'] }}</p>
                                    </div>

                                    <div class="col-md-4">
                                        <p style="line-height: 18px;text-align: right;padding: 0px">Phone - 01718937082<br>01711418731<br>01711418731</p>
                                    </div> -->
                                    @if($user_info['footer_image'] != null)
                                        <img class="float-left" src="{{ url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
                </center>
                @endif

                @if($user_info['pos_printer'] == 0)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div id="hedColor" style="margin-top: 0px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold">{{ $user_info['organization_name'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold">{{ $user_info['address'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 18px;font-weight: bold">{{ $user_info['contact_number'] }}</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 18px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">Invoice</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 14px">Date : {{ date('d/m/Y', strtotime($invoice['invoice_date'])) }}</span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 14px">{{ date('h:i a', strtotime(now())) }}</span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 14px">Invoice No : {{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }}</span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">
                                    
                                </div>

                                <div style="border-top: 2px dashed black;border-bottom: 2px dashed black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px;width: 5%" class="col-2"><strong>Qt</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-4"><strong>Item</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Price</strong></div>
                                    <div style="font-size: 12px;text-align: right;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Total</strong></div>
                                </div>

                                @if(!empty($entries) && ($entries->count() > 0))

                                    <?php 
                                        $sub_total              = 0; 
                                        $total_discount         = 0;
                                    ?>

                                    @foreach($entries as $key => $value)

                                    <div style="padding-bottom: 10px;" id="DetailsFontSize" class="row">

                                        <?php 
                                            $discount           = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; 
                                            $total_discount     = $total_discount + $discount;
                                        ?>

                                        <div style="font-size: 12px" class="col-2">
                                            {{ $value['quantity'] }}
                                        </div>

                                        <div style="font-size: 12px" class="col-4">
                                            {{ $value['product_variations_name'] != null ? $value['product_variations_name'] : $value['product_entry_name'] }}
                                        </div>
                                        <div style="font-size: 12px" class="col-3">
                                            {{ number_format($value['rate'],2,'.',',') }}
                                        </div>
                                        <div style="font-size: 12px;text-align: right" class="col-3">{{ number_format($value['total_amount'],2,'.',',') }}</div>
                                    </div>

                                    <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                    @endforeach
                                @endif

                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>GROSS Total :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong>{{ number_format($sub_total,2,'.',',') }}</strong></div>
                                </div>

                                <!-- <div style="padding-bottom: 10px" class="row"></div> -->

                                <?php
                                    $total_discount_amount       = $invoice['total_discount_type'] == 0 ? (($sub_total*$invoice['total_discount_amount'])/100) : $invoice['total_discount_amount']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>Total Discount :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong>{{ number_format($total_discount + $total_discount_amount,2,'.',',') }}</strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;" class="row">
                                    <div style="font-size: 14px" class="col-6"><strong>NET Total :</strong></div>
                                    <div style="font-size: 14px;text-align: right" class="col-6"><strong>{{ number_format($sub_total - $total_discount_amount,2,'.',',') }}</strong></div>
                                </div>

                                <div style="border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 14px" class="col-12"><strong>VAT Included</strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px;" class="row"></div> -->
                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px;" class="row"></div> -->

                               <!--  <div class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-12">Payments</div>
                                </div> -->

                                <div style="padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 14px;font-weight: bold" class="col-6">Total Paid</div>
                                    <div style="font-size: 14px;text-align: right;font-weight: bold" class="col-6">{{ number_format($invoice['invoice_amount'] - $invoice['due_amount'],2,'.',',') }}</div>
                                </div>

                                <!-- <div style="padding-left: 15px;border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 16px" class="col-6">-TOTAL DUES</div>
                                    <div style="font-size: 16px;text-align: right" class="col-6">{{ number_format($invoice['due_amount'],2,'.',',') }}</div>
                                </div> -->

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 12px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        javascript:window.print();
    });

    window.onafterprint = function(e){
        var site_url  = $('.site_url').val();
        window.location.replace(site_url + '/invoices');
    };
</script>
@endsection