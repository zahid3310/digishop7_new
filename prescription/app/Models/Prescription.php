<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    protected $table = "prescription";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function majorCategory()
    {
        return $this->belongsTo('App\Models\Categories','major_category_id');
    }

    public function prescriptionEntries()
    {
        return $this->hasMany(PrescriptionEntries::class, "prescription_id");
    }
}
