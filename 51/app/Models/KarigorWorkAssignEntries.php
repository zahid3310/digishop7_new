<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KarigorWorkAssignEntries extends Model
{
    protected $table = "karigor_work_assign_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function productName()
    {
        return $this->belongsTo('App\Models\ProductEntries','product_id');
    }

    public function karigorName()
    {
        return $this->belongsTo('App\Models\karigorInfo','karigor_id');
    }
}
