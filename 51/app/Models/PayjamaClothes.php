<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayjamaClothes extends Model
{
    protected $table = "payjama_clothes";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }
}
