<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sliders extends Model
{
    protected $table = "sliders";
    
    public function createdBy()
    {
        return $this->belongsTo('App\User', "created_by");
    }

    public function updateddBy()
    {
        return $this->belongsTo('App\User', "updated_by");
    }
}
