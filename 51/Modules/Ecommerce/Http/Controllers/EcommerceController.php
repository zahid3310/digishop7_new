<?php

namespace Modules\Ecommerce\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Models
use App\Models\MajorCategories;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\ProductImages;
use DB;
use Auth;
use Response;

class EcommerceController extends Controller
{
    public function majorCategoryIndex()
    {
        $major_categories = MajorCategories::selectRaw("major_categories.*")
                            ->where('status', 1)
                            ->get();
                            // dd($major_categories);
     
        return view('ecommerce::major_category.index', compact('major_categories'));
    }

    public function majorCategoryCreate()
    {
        return view('ecommerce::major_category.create');
    }

    public function majorCategoryStore(Request $request)
    {
        $request->validate([
            "name"        => ["required", "string"],
            "status"      => ["required", "in:0,1"]
        ]);

        $type       = "success";
        $message    = "Major Category Successfully Added !!";
        $data       = $request->all();

        try{
            $major_category                  = new MajorCategories;
            $major_category->name            = $data['name'];
            $major_category->status          = $data['status'];
            $major_category->created_by      = Auth()->user()->id;

            if ($major_category->save())
            {
                return redirect()->route("ecommerce_major_category_index")->with($type, $message);
            }
        }catch (\Exception $exception) {

            $type       = "unsuccess";
            $message    = "Something Went Wrong.Please Try Again.!";
            return back()->with($type, $message);
        }
    }

    public function majorCategoryEdit($id)
    {
        $major_categories  = MajorCategories::selectRaw("major_categories.*")->find($id);

        return view('ecommerce::major_category.edit', compact('major_categories'));
    }

    public function majorCategoryUpdate(Request $request, $id)
    {
        $request->validate([
            "name"    => ["required", "string"],
            "status"  => ["required", "in:0,1"]

        ]);

        $type       = "success";
        $message    = "Major Category Successfully Updated !!";
        $data       = $request->all();

        try{
            $major_category                  = MajorCategories::find($id);
            $major_category->name            = $data['name'];
            $major_category->status          = $data['status'];
            $major_category->updated_by      = Auth()->user()->id;

            if ($major_category->save())
            {
                return redirect()->route("ecommerce_major_category_index")->with($type, $message);
            }
        }catch (\Exception $exception) {

            $type       = "unsuccess";
            $message    = "Something Went Wrong.Please Try Again.!";
            return back()->with($type, $message);
        }
    }

    public function asignMajorCategoryEdit()
    {
        $major_categories  = MajorCategories::selectRaw("major_categories.*")
                                            ->with('product')
                                            ->get();

        return view('ecommerce::asign_major_category.edit', compact('major_categories'));
    }

    public function asignMajorCategoryUpdate(Request $request)
    {
        $type       = "success";
        $message    = "Product Successfully Updated !!";
        $data       = $request->all();

        DB::beginTransaction();

        try{
            Products::query()->update(['major_category_id' => null]);
            ProductEntries::query()->update(['major_category_id' => null]);

            foreach ($data['major_category_id'] as $key => $value)
            {   
                if (isset($data['category_id'][$value]))
                {
                    foreach ($data['category_id'][$value] as $key1 => $value1)
                    {
                        $product                       = Products::find($value1);
                        $product->major_category_id    = $value;
                        $product->updated_by           = Auth()->user()->id;
                        
                        if ($product->save())
                        {
                            $product_entries = ProductEntries::where('product_id', $value1)->get();

                            if ($product_entries->count() > 0)
                            {
                                foreach ($product_entries as $key2 => $value2)
                                {
                                    $product_entry                       = ProductEntries::find($value2['id']);
                                    $product_entry->major_category_id    = $value;
                                    $product_entry->updated_by           = Auth()->user()->id;
                                    $product_entry->save();
                                }
                            }
                        }
                    }
                }
            }

            DB::commit();
            return redirect()->route("ecommerce_asign_major_category_edit")->with($type, $message);
            
        }catch (\Exception $exception) {
            DB::rollback();
            dd($exception);
            $type       = "unsuccess";
            $message    = "Something Went Wrong.Please Try Again.!";
            return back()->with($type, $message);
        }
    }

    public function productImagesEdit()
    {
        $product_entry_id   = isset($_GET['product_entry_id']) ? $_GET['product_entry_id'] : 0;
        $product_images     = ProductImages::where('product_entry_id', $product_entry_id)->get();
        $images_count       = $product_images->count();
        $product_name       = ProductEntries::find($product_entry_id);

        return view('ecommerce::upload_product_image.edit', compact('product_images', 'images_count', 'product_name'));
    }

    public function productImagesUpdate(Request $request)
    {
        $type               = "success";
        $message            = "Images Successfully Updated !!";
        $user_id            = Auth::user()->id;
        $data               = $request->all();
        $find_product_entry = ProductEntries::find($data['product_entries_id']);

        DB::beginTransaction();

        try{
            $images     = $request->file('images');
            if ($request->hasFile('images')) :
                foreach ($images as $key => $item):
                    $image              = $item;
                    $image_name         = time(). $key .'.'.$image->getClientOriginalExtension();
                    $file_path          = 'public/images/products/';
                    $rowImage           = $image;
                    $savingPath         = $file_path;
                    $imageName          = $image_name;
                    $resizeImage        = $this->compressAndResize($rowImage, $savingPath, $imageName, 15, 600, 750);

                    $arr[]      = [
                        'product_entry_id'  => $data['product_entries_id'],
                        'major_category_id' => $find_product_entry['major_category_id'],
                        'image'             => $imageName,
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];
                endforeach;
            endif;



            if (isset($arr))
            {
                DB::table('product_images')->insert($arr);
                
            }

            
            $product_description = ProductEntries::where('id',$data['product_entries_id'])->first();
            $product_description->description = $data['description'];
            $product_description->save();

            DB::commit();

            return back()->with($type, $message);

            

            
        }catch (\Exception $exception) {
            DB::rollback();
            dd($exception);
            $type       = "unsuccess";
            $message    = "Something Went Wrong.Please Try Again.!";
            return back()->with($type, $message);
        }
    }

    public function deleteProductImages($product_image_id)
    {
        $delete_row  = ProductImages::where('id', $product_image_id)->delete();

        if ($delete_row)
        {
            return Response::json(1);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function compressAndResize($source_url, $destination_url, $imageName , $quality, $desired_width, $desired_height)
    {
        $info = getimagesize($source_url);
        if ($info['mime'] == 'image/jpeg')
        {
            list($width, $height)   = getimagesize($source_url);
            $src                    = imagecreatefromjpeg($source_url);
            $dst                    = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        elseif ($info['mime'] == 'image/png'){
            list($width, $height)   = getimagesize($source_url);
            $src                    = imagecreatefrompng($source_url);
            $dst                    = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        elseif ($info['mime'] == 'image/gif'){
            list($width, $height)   = getimagesize($source_url);
            $src                    = imagecreatefromgif($source_url);
            $dst                    = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        return $destination_url.$imageName;
    }
}
