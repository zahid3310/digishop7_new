<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('discounts')->group(function() {
    Route::get('/', 'DiscountsController@index')->name('discounts_index');
    Route::post('/store', 'DiscountsController@store')->name('discounts_store');
    Route::get('/edit/{id}', 'DiscountsController@edit')->name('discounts_edit');
    Route::post('/update/{id}', 'DiscountsController@update')->name('discounts_update');
    Route::get('/discount/list/load', 'DiscountsController@ListLoad')->name('discounts_list_load');
    Route::get('/discount/search/list/{id}', 'DiscountsController@ListSearch')->name('discounts_list_search');
});