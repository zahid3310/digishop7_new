@extends('layouts.app')

@section('title', 'Orders')

@section('styles')
<style>
table, td, th {  
  border: 1px solid #ddd;
  text-align: center;
}

table {
  border-collapse: collapse;
  width: 100%;
}
.myWidth{
    width: 95%!important;
}

.number-center { 
    text-align: center; 
}

.amount-bold { 
    font-weight: bold;
}

.list-group{
      max-height: 400px;
      margin-bottom: 10px;
     overflow-y: scroll;
      -webkit-overflow-scrolling: touch;
    }

</style>

@endsection

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                 <form id="FormSubmit" action="{{ route('tailors_order_update', $find_order['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                {{ csrf_field() }}

                                <div style="background-color: #F4F4F7;padding-top: 10px;border-top: 2px solid #563d7c;border-radius: 6px;" class="row">
                                   
                                    <div class="col-md-6">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-2 col-form-label"><strong>নাম *</strong></label>
                                            <div class="col-md-10">
                                                <select id="customer_id" name="customer_id" class="customerID form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                  <option value="{{ $find_order['customer_id'] }}" selected>{{ $find_order['contact_name'] }}({{ $find_order['mobile_number'] }})</option>
                                                </select>
 
                                            </div>
                                        </div>

                                      
                                    </div>

                                   

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-6 col-form-label"><strong>অর্ডার তারিখ:</strong></label>
                                            <div class="col-md-6">
                                                <input id="order_date" name="order_date" type="text" value="{{ date('d-m-Y', strtotime($find_order['order_date'])) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                            </div>
                                        </div>
                                        
                                    </div>

                                      <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-5 col-form-label"><strong>অগ্রাধিকার:</strong> </label>
                                            <div class="col-md-7">
                                                <select id="priority_id" name="priority_id" class="priority_id form-control select2">
                                                    <option {{ $find_order['priority_id'] == 1 ? 'selected' : '' }} value="1">Normal</option>
                                                   <option {{ $find_order['priority_id'] == 2 ? 'selected' : '' }} value="2">Urgent</option>
                                                   <option {{ $find_order['priority_id'] == 3 ? 'selected' : '' }} value="3">Very Urgent</option>
                                                </select>
                                            </div>
                                        </div>
     
                                    </div>

                                    <div class="col-md-3">

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label"><strong>ঘুন্ডি :</strong> </label>
                                            <div class="col-md-8 mt-2">
                                                <!-- <input  type="text" name="knob" class="form-control" width="100%"> -->
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="knob" id="knob1" value="1" {{ $find_order['knob'] == 1 ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="knob1">Yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="knob" id="knob2" value="2" {{ $find_order['knob'] == 2 ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="knob2">No</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                      
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-6 col-form-label"><strong>ডেলিভারি তারিখ:</strong></label>
                                            <div class="col-md-6">
                                                <input id="delivery_date" name="delivery_date" type="text" value="{{ date('d-m-Y', strtotime($find_order['delivery_date'])) }}" class="deliveryDate form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>

                                <hr>

                                <div class="row" style="background-color: #F4F4F7;padding-bottom: 5px; padding-top: 5px;">

                                    <div class="col-md-12" style="padding-top:5px;padding-bottom:5px;text-align: center;font-size: 17px;background-color: #9e9e9e36;border-top: 2px solid #563d7c;border-radius: 6px;color: #000;"><strong>পাঞ্জাবীর  মাপ:</strong></div>


                                    <div class="col-md-11">
                                        <label class="mt-2" style="font-weight: 500;"><strong>পাঞ্জাবীর বিবরণ:</strong></label>
                                        <textarea class="punjabi-description form-control" name="punjabi_description" style="height:auto!important;" >{{$find_order['punjabi_description']}}</textarea>
                                    </div>

                                    <div class="col-md-1" style="margin-top: 35px;">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#punjabiModal"><i class="bx bx-plus"></i></button>
                                    </div>

                                    <!--পাঞ্জাবীর বিবরণ নিৰ্বাচন  Modal -->
                                    <div class="modal fade" id="punjabiModal" tabindex="-1" role="dialog" aria-labelledby="punjabiModalLabel" aria-hidden="true">
                                      <div class="modal-dialog modal-xl" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h5 class="modal-title" id="punjabiModalLabel">পাঞ্জাবীর বিবরণ তালিকা</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </div>
                                          <div class="modal-body">
                                            <table id="punjabiTable" class="list-group table table-striped table-bordered table-hover">
                                                <tbody>
                                                    @foreach($PunjabiDescription as $item)
                                                    <tr>
                                                        <!-- <td width="5%"><button type="button" class="punjabi_description btn btn-sm btn-primary">Select</button></td> -->
                                                        <td width="100%" class="punjabi_description" style="text-align: justify;cursor: pointer;">{{$item->punjabi_description}}</td>
                                                    </tr>
                                                     @endforeach
                                                </tbody>
                                            </table>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                
                                    <!--পাঞ্জাবীর বিবরণ নিৰ্বাচন  Modal End -->


                                    <div class="col-md-11">
                                        <label class="mt-2" style="font-weight: 500;"><strong>এমব্রয়ডারি বিবরণ:</strong></label>
                                        <textarea class="em-description form-control" name="embroidery" style="height:auto!important;">{{$find_order['embroidery']}}</textarea>
                                    </div>

                                    <div class="col-md-1" style="margin-top: 35px;">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#embroideryModal"><i class="bx bx-plus"></i></button>
                                    </div>

                                    <!--এমব্রয়ডারি  বিবরণ নিৰ্বাচন  Modal -->
                                    <div class="modal fade" id="embroideryModal" tabindex="-1" role="dialog" aria-labelledby="embroideryModalLabel" aria-hidden="true">
                                      <div class="modal-dialog modal-xl" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h5 class="modal-title" id="embroideryModalLabel">এমব্রয়ডারি বিবরণ তালিকা</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </div>
                                          <div class="modal-body">
                                            <table id="embroideryTable" class="list-group table table-striped table-bordered table-hover">
                                                <tbody>
                                                    @foreach($EmbroideryDescription as $item)
                                                    <tr>
                                                        <td width="100%" class="embroidery_description" style="text-align: justify;cursor: pointer;">{{$item->embroidery_description}}</td>
                                                    </tr>
                                                     @endforeach
                                                </tbody>
                                            </table>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                
                                    <!--এমব্রয়ডারি বিবরণ নিৰ্বাচন  Modal End -->


                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>লম্বা</strong></label>
                                        <input type="text" class="form-control" name="punjabi_hight" value="{{$find_order['punjabi_hight']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>বডি</strong></label>
                                        <input type="text" class="form-control" name="punjabi_body" value="{{$find_order['punjabi_body']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>কাঁধ</strong></label>
                                        <input type="text" class="form-control" name="punjabi_shoulders" value="{{$find_order['punjabi_shoulders']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>হাতা</strong></label>
                                        <input type="text" class="form-control" name="punjabi_hand" value="{{$find_order['punjabi_hand']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>হাতের মুহুরী</strong></label>
                                        <input type="text" class="form-control" name="punjabi_hand_muhuri" value="{{$find_order['punjabi_hand_muhuri']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>গলা</strong></label>
                                        <input type="text" class="form-control" name="punjabi_Throat" value="{{$find_order['punjabi_Throat']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>ঘের</strong></label>
                                        <input type="text" class="form-control" name="punjabi_gher" value="{{$find_order['punjabi_gher']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>আর্মহোল</strong></label>
                                        <input type="text" class="form-control" name="punjabi_armhole" value="{{$find_order['punjabi_armhole']}}">
                                    </div>

                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>লুজ</strong></label>
                                        <input type="text" class="form-control" name="punjabi_loose" value="{{$find_order['punjabi_loose']}}">
                                    </div>

                                    <div class="col-md-4">
                                        <label class="mt-2"><strong>পাঞ্জাবীর স্টাইল :</strong></label>
                                        <select name="services_id" id="tessrter" class="ServiceId form-control select2">
                                            <option value="0" hidden="">স্টাইল নির্বাচন করুন</option>
                                            @foreach($services as $serv)
                                             <option @if($find_order_entries[0]['service_id'] == $serv->id) selected  @endif value="{{$serv->id}}">{{$serv->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-2 PunjabiStyleOtherData">
                                        <label class="mt-2"><strong>মজুরি :</strong></label>
                                        <input type="number" id="panjabirMujuri" class="servicePrice number-center form-control" name="panjabi_rate"  value="{{$find_order_entries[0]['panjabi_rate']}}" readonly="">
                                    </div>


                                    <div class="col-md-2 PunjabiStyleOtherData">
                                        <label class="mt-2"><strong>কলার প্লেট</strong></label>
                                        <input type="text" class="form-control" name="punjabi_collar_plate" value="{{$find_order['punjabi_collar_plate']}}">
                                    </div>
                                    <div class="col-md-2 PunjabiStyleOtherData">
                                        <label class="mt-2"><strong>বুক ফারা</strong></label>
                                        <input type="text" class="form-control" name="punjabi_buk_fara" value="{{$find_order['punjabi_buk_fara']}}">
                                    </div>
                                    <div class="col-md-2 PunjabiStyleOtherData">
                                        <label class="mt-2"><strong>বোতাম</strong></label>
                                        <input type="number" class="form-control" name="punjabi_button" value="{{$find_order['punjabi_button']}}">
                                    </div>
                                    <div class="col-md-2 PunjabiStyleOtherData">
                                        <label class="mt-2"><strong>কলার</strong></label>
                                        <select class="form-control" name="punjabi_kolar">
                                            <option {{ $find_order['punjabi_kolar'] == 1 ? 'selected' : '' }} value="1">গোল গলা</option>
                                            <option {{ $find_order['punjabi_kolar'] == 2 ? 'selected' : '' }} value="2">শেরওয়ানি</option>
                                            <option {{ $find_order['punjabi_kolar'] == 3 ? 'selected' : '' }} value="3">হাফ বেল্ট</option>
                                            <option {{ $find_order['punjabi_kolar'] == 4 ? 'selected' : '' }} value="4">ফুল বেল্ট</option>
                                        </select>
                                    </div>
                                </div>


                                <hr>
                                <div class="row" style="background-color: #F4F4F7;padding-top: 5px;padding-bottom: 10px;">

                                   

                                    <div class="col-md-12" style="padding-top:5px;padding-bottom:5px;text-align: center;font-size: 17px;background-color: #9e9e9e36;border-top: 2px solid #563d7c;border-radius: 6px;color: #000;"><strong>পাজামার মাপ:</strong></div>

                                    <div class="col-md-11">
                                        <label class="mt-2" style="font-weight: 500;"><strong>পাজামার বিবরণ:</strong></label>
                                        <textarea class="payjama-description form-control" name="payjama_description" style="height: auto!important;">{{$find_order['payjama_description']}}</textarea>
                                    </div>

                                    <div class="col-md-1" style="margin-top: 35px;">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#payjamaModal"><i class="bx bx-plus"></i></button>
                                    </div>

                                    <!--পাঞ্জাবীর বিবরণ নিৰ্বাচন  Modal -->
                                    <div class="modal fade" id="payjamaModal" tabindex="-1" role="dialog" aria-labelledby="payjamaModalLabel" aria-hidden="true">
                                      <div class="modal-dialog modal-xl" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h5 class="modal-title" id="payjamaModalLabel">পাজামার বিবরণ তালিকা</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </div>
                                          <div class="modal-body">
                                            <table id="payjamaTable" class="list-group table table-striped table-bordered table-hover">
                                                <tbody>
                                                    @foreach($PayjamaDescription as $item)
                                                    <tr>
                                                        <!-- <td width="5%"><button type="button" class="payjama_description btn btn-sm btn-primary">Select</button></td> -->
                                                        <td width="100%" class="payjama_description" style="text-align: justify;cursor: pointer;">{{$item->payjama_description}}</td>
                                                    </tr>
                                                     @endforeach
                                                </tbody>
                                            </table>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                
                                
                                    <!--পাজামার বিবরণ নিৰ্বাচন  Modal End -->
                                 

                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>লম্বা</strong></label>
                                        <input type="text" class="form-control" name="payjama_hight" value="{{$find_order['payjama_hight']}}">
                                    </div>

                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>মুহুরী</strong></label>
                                        <input type="text" class="form-control" name="payjama_muhuri" value="{{$find_order['payjama_muhuri']}}">
                                    </div>

                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>লুস</strong></label>
                                        <input type="text" class="form-control" name="payjama_lus" value="{{$find_order['payjama_lus']}}">
                                    </div>

                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>হাই</strong></label>
                                        <input type="text" class="form-control" name="payjama_high" value="{{$find_order['payjama_high']}}">
                                    </div>

                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>কোমর</strong></label>
                                        <input type="text" class="form-control" name="payjama_komor" value="{{$find_order['payjama_komor']}}">
                                    </div>

                                </div>

                                <hr>

                                <div class="row" style="background-color: #F4F4F7;padding-top: 5px;padding-bottom: 5px;">

                                    <div class="col-md-6" style="border-right: 2px dotted #000;">
                                        <center><strong>পাঞ্জাবীর হিসাব :</strong></center>
                                        <hr>
                                        <div class="row">

                                           
                                            <div class="col-md-12 ">

                                                <div class="font-size-20 form-check form-check-inline">
                                                    <input  class="coupon_question1 form-check-input" type="checkbox" id="lf" name="lf_cf" checked="" value="1">
                                                    <label class="form-check-label" for="lf">LF</label>
                                                </div>
                                                
                                                <div class="font-size-20 form-check form-check-inline">
                                                    <input class="coupon_question2 form-check-input" type="checkbox" id="cf" name="lf_cf" value="0">
                                                    <label class="form-check-label" for="cf">CF</label>
                                                </div>
                                                
                                                
                                            </div>

                                             <div class="col-md-2">
                                                <label class="mt-2"><strong>সংখ্যা:</strong></label>
                                                <input type="number" name="number_of_punjabi" id="numberPunjabi" class="number-center form-control" value="{{$find_order_entries[0]['panjabi_quantity']}}">
                                            </div> 

                                        
                                            <div class="answer col-md-10">
                                                <label class="mt-2"><strong>কাপড়ের নাম</strong></label>
                                                 <select class="form-control punjabiPriceID" name="punjabi_cloth_name">
                                                    <option value="0">নির্বাচন করুন</option>
                                                     @foreach($PunjabiClothes as $list)
                                                     <option value="{{$serv->id}}" @if($find_order_entries[0]['punjabi_cloth_name'] == $list->id) selected  @endif>{{$list->clothes_name}}</option>
                                                     @endforeach
                                                 </select>
                                            </div>


                                            <div class="answer col-md-4">
                                                <label class="mt-2"><strong>কাপড়ের দর</strong></label>
                                                <input type="number" id="panjabirKaporerDor" class="punjabiPrice number-center form-control" name="price_panjabi_cloth"  value="{{$find_order_entries[0]['price_panjabi_cloth']}}" readonly="">
                                            </div>

                                   

                                            <div class="answer col-md-4">
                                                <label class="mt-2"><strong>কাপড়ের পরিমান</strong></label>
                                                <input type="number" step="0.01" id="panjabirKaporerPorimap" class="number-center form-control" name="amount_panjabi_cloth" value="{{$find_order_entries[0]['amount_panjabi_cloth']}}">
                                            </div>


                                             <div class="col-md-4">
                                                <label class="mt-2"><strong>এমব্রয়ডারি</strong></label>
                                                <input type="number" class="number-center form-control" name="ambortari_price" id="panjabirAmbortari" value="{{$find_order_entries[0]['ambortari_price']}}">
                                            </div>

                                        </div>
                                    </div>

                                    

                                    <div class="col-md-6">

                                        <center><strong>পাজামার হিসাব :</strong></center>
                                        <hr>
                                        <div class="row">

                                    

                                     <div class="col-md-6">
                                        <label class="mt-2"><strong>পাজামার সংখ্যা:</strong></label>
                                        <input type="number" name="number_of_payjama" id="numberPayjama" class="number-center form-control" value="{{$find_order_entries[0]['payjama_quantity']}}">
                                    </div>

                                             

                                              <div class="col-md-6">
                                        <label class="mt-2"><strong> মজুরি :</strong></label>
                                        <input type="number" id="payjamarMujuri" class="number-center form-control" name="payjama_rate"  value="{{$find_order_entries[0]['payjama_rate']}}" readonly="">
                                    </div>

                                    <div class="answer col-md-12">
                                                <label class="mt-2"><strong>কাপড়ের নাম</strong></label>

                                                <select class="form-control payjamaPriceId" name="payjama_cloth_name">
                                                     <option value="0">নির্বাচন করুন</option>
                                                     @foreach($PayjamaClothes as $list)
                                                     <option @if($find_order_entries[0]['payjama_cloth_name'] == $list->id) selected  @endif >{{$list->clothes_name}}</option>
                                                     @endforeach
                                                 </select>
                                            </div>

                                            <div class="answer col-md-6">
                                                <label class="mt-2"><strong> দর</strong></label>
                                                <input type="number" id="payjamaKaporerDor" class="payjamaPrice number-center form-control" name="price_payjama_cloth"  value="{{$find_order_entries[0]['price_payjama_cloth']}}" readonly="">
                                            </div>

                                            <div class="answer col-md-6">
                                                <label class="mt-2"><strong>পরিমান</strong></label>
                                                <input type="number" step="0.01"  class="number-center form-control" name="amount_payjama_cloth" id="payjamaKaporerPorimap" value="{{$find_order_entries[0]['amount_payjama_cloth']}}">
                                            </div>


                                           

                                        </div>
                                    </div>



                                </div>

                                <div class="row" style="background-color: #F4F4F7;padding-top: 5px;border-top: 2px dotted; #000;">
                                   

                                    <div class="col-md-4">
                                        <label class="mt-2"><strong>মোট টাকা :</strong></label>
                                        <input type="number" id="sum"  class="amount-bold number-center form-control" name="total_amount" value="{{$find_order_entries[0]['total_amount']}}" readonly="">


                                    </div>

                                    <div class="col-md-4">
                                        <label class="mt-2"><strong>জমা টাকা :</strong></label>
                                        <input type="number" id="Joma" class="amount-bold number-center form-control" name="submission_amount" value="{{$find_order_entries[0]['submission_amount']}}">

                                        
                                    </div>

                                     <div class="col-md-4">
                                        <label class="mt-2"><strong>বাকি টাকা :</strong></label>
                                        <input type="number" id="payable" class="servicePrice amount-bold number-center form-control" name="due_amount" value="{{$find_order_entries[0]['due_amount']}}" readonly="">
                                    </div>
                                </div>

                                <hr>

                                
                                <div style="padding-top: 5px !important;padding-bottom: 5px !important;background-color: #F4F4F7;" class="row">
                                   

                                    <div class="col-md-6">
                                        <strong style="font-size:18px;">Send SMS</strong><br>

                                         <div class="form-check form-check-inline">
                                          <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="2">
                                          <label class="form-check-label" for="inlineCheckbox2" style="font-size: 18px;">Masking</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="1" checked="">
                                          <label class="form-check-label" for="inlineCheckbox1" style="font-size: 18px;">Non Masking</label>
                                        </div>
                                       

                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="3">
                                          <label class="form-check-label" for="inlineCheckbox2" style="font-size: 18px;">Voice</label>
                                        </div>

                                       

                                        
                                    </div>
                                    <div class="col-md-6 text-right mt-2">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Update & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('tailors_order_index') }}">Close</a></button>
                                              
                                              
                                            </div>
                                        </div>
                                    </div>

                                  
                                </div>

                                
                                </form>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number*</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Email</label>
                        <div class="col-md-12">
                            <input id="email" name="email" type="text" class="form-control">
                        </div>
                    </div>

                     <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Date of birth</label>
                        <div class="col-md-12">
                            <input type="date" id="db" name="db" class="form-control">
                        </div>
                    </div>

                     <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Marriage date</label>
                        <div class="col-md-12">
                            <input id="md" name="md" type="date" class="form-control">
                        </div>
                    </div>


                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control">
                        </div>
                    </div>

                    <input id="contact_type" name="contact_type" type="hidden" value="0" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

  
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/tailors/orders/local-customer-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };

                    console.log(response);
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

       
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var email                       = $("#email").val();
            var date_of_birth               = $("#db").val();
            var marriage_date               = $("#md").val();
            var contact_type                = $("#contact_type").val();
            var site_url                    = $('.site_url').val();


            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/tailors/orders/local-customer-add',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number,email : email ,date_of_birth : date_of_birth,marriage_date : marriage_date, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        console.log(data.id)
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                        }
                        
                        $("#customer_id").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });
    
    </script>

 


    <script type="text/javascript">
      
        $(document).ready(function(){
            $("select.customerID").change(function(){
                var customerID = $(this).children("option:selected").val();
                var site_url                    = $('.site_url').val();
                $.ajax({
                    type:   'get',
                    url:    site_url + '/tailors/orders/localCustomerList1',
                    data:   {customerID : customerID, _token: '{{csrf_token()}}' },
        
                    success: function (Serviceprice) {
                       console.log(Serviceprice)
                    }
                });
            });


        });


        $(document).ready(function(){
            $("select.ServiceId").change(function(){
                var ServiceId = $(this).children("option:selected").val();
                var site_url                    = $('.site_url').val();
                $.ajax({
                    type:   'get',
                    url:    site_url + '/tailors/orders/get-service-price',
                    data:   {ServiceId : ServiceId, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        $(".servicePrice").val(data[0]['price']);


                    }
                });
            });


        });

         $(document).ready(function(){
            $("select.punjabiPriceID").change(function(){
                var punjabiPrice = $(this).children("option:selected").val();
                var site_url                    = $('.site_url').val();
                $.ajax({
                    type:   'get',
                    url:    site_url + '/tailors/orders/get-punjabi-price',
                    data:   {punjabiPrice : punjabiPrice, _token: '{{csrf_token()}}' },
        
                    success: function (punjabidata) {
                        $(".punjabiPrice").val(punjabidata[0]['price']);


                    }
                });
            });

        }); 


         $(document).ready(function(){
            $("select.payjamaPriceId").change(function(){
                var payjamaPrice = $(this).children("option:selected").val();
                var site_url                    = $('.site_url').val();
                $.ajax({
                    type:   'get',
                    url:    site_url + '/tailors/orders/get-payjama-price',
                    data:   {payjamaPrice : payjamaPrice, _token: '{{csrf_token()}}' },
        
                    success: function (payjamadata) {
                        $(".payjamaPrice").val(payjamadata[0]['price']);


                    }
                });
            });

        });


        $(document).ready(function(){
            $("select.priority_id").change(function(){
                var priority_id = $(this).children("option:selected").val();

                if (priority_id == 1) {
                    var priorityDate = <?php echo json_encode(date('d-m-Y',strtotime("10 days"))); ?>;
                     $(".deliveryDate").val(priorityDate);
                }
                if (priority_id == 2) {
                    var priorityDate = <?php echo json_encode(date('d-m-Y',strtotime("5 days"))); ?>;
                    $(".deliveryDate").val(priorityDate);
                }

                if (priority_id == 3) {
                    var priorityDate = <?php echo json_encode(date('d-m-Y',strtotime("3 days"))); ?>;
                    $(".deliveryDate").val(priorityDate);
                }
            });

 


        });


        $("#punjabiTable").on('dblclick','.punjabi_description',function(){
        // get the current row
        var currentRow=$(this).closest("tr"); 
        var col2=currentRow.find("td:eq(0)").text();
        $(".punjabi-description").val(col2);
        $('#punjabiModal').modal('hide');
        });


        $("#payjamaTable").on('dblclick','.payjama_description',function(){
        // get the current row
        var currentRow=$(this).closest("tr"); 
        var col2=currentRow.find("td:eq(0)").text();
        $(".payjama-description").val(col2);
        $('#payjamaModal').modal('hide');
        });


        $("#embroideryTable").on('dblclick','.embroidery_description',function(){
        // get the current row
        var emEurrentRow = $(this).closest("tr"); 
        var td = emEurrentRow.find("td:eq(0)").text();
        $(".em-description").val(td);
        $('#embroideryModal').modal('hide');
        });






        // function myAmmount() {
        //     var panjabirMujuri              = document.getElementById("panjabirMujuri").value;
        //     var kaporerDor                  = document.getElementById("kaporerDor").value;
        //     var payjamarMujuri              = document.getElementById("payjamarMujuri").value;

        //     var Surviceprice                = $(".servicePrice").val();
           

        //     var numberPunjabi                = document.getElementById("numberPunjabi").value;
        //     var numberPayjama                = document.getElementById("numberPayjama").value;

        //     var sum = parseInt(panjabirMujuri)+parseInt(kaporerDor)+parseInt(payjamarMujuri)+parseInt(Surviceprice);



        //     $("#total").val(sum);
        // }

        // function jomataka() {
        //     var jomataka                = document.getElementById("Joma").value;
        //     var totaltaka                = document.getElementById("total").value;
        //     var Subtraction = parseInt(totaltaka)-parseInt(jomataka);
        //     $("#payable").val(Subtraction);
        // }


        
    </script>

    <script type="text/javascript">
/*
        $(function(){
            $('#panjabirMujuri,#panjabirKaporerDor,#panjabirKaporerPorimap,#panjabirAmbortari,#numberPunjabi,#numberPayjama,#payjamarMujuri,#payjamaKaporerPorimap,#payjamaKaporerDor,#Joma,#sum').keyup(function(){

            

                var numberPunjabi          =   parseFloat($('#numberPunjabi').val()) || 1;
                var panjabirMujuri         =   parseFloat($('#panjabirMujuri').val()) || 0;
                var panjabirKaporerPorimap =   parseFloat($('#panjabirKaporerPorimap').val()) || 0;
                var panjabirKaporerDor     =   parseFloat($('#panjabirKaporerDor').val()) || 0;
                var panjabirAmbortari      =   parseFloat($('#panjabirAmbortari').val()) || 0;
               

                var panjabikaporerTotalAmount = panjabirKaporerPorimap * panjabirKaporerDor;

                var panjabirTotalCost = (numberPunjabi * panjabirMujuri) + (panjabikaporerTotalAmount + panjabirAmbortari);
             


                var numberPayjama          =   parseFloat($('#numberPayjama').val()) || 1;
                var payjamarMujuri         =   parseFloat($('#payjamarMujuri').val()) || 0;
                var payjamaKaporerPorimap  =   parseFloat($('#payjamaKaporerPorimap').val()) || 0;
                var payjamaKaporerDor      =   parseFloat($('#payjamaKaporerDor').val()) || 0;


                var payjamakaporerTotalAmount = payjamaKaporerPorimap * payjamaKaporerDor;

                var payjamarTotalCost = (numberPayjama * payjamarMujuri) + payjamakaporerTotalAmount;

                $('#sum').val(panjabirTotalCost+payjamarTotalCost);


                var Joma                   =   parseFloat($('#Joma').val()) || 0;
                var sum                    =   parseFloat($('#sum').val()) || 0;

                $('#payable').val(sum-Joma);



            });
         });
    */

        var myFunction = function() {

                var numberPunjabi          =   parseFloat($('#numberPunjabi').val()) || 1;
                var panjabirMujuri         =   parseFloat($('#panjabirMujuri').val()) || 0;
                var panjabirKaporerPorimap =   parseFloat($('#panjabirKaporerPorimap').val()) || 0;
                var panjabirKaporerDor     =   parseFloat($('#panjabirKaporerDor').val()) || 0;
                var panjabirAmbortari      =   parseFloat($('#panjabirAmbortari').val()) || 0;
               

                var panjabikaporerTotalAmount = panjabirKaporerPorimap * panjabirKaporerDor;

                var panjabirTotalCost = (numberPunjabi * panjabirMujuri) + (panjabikaporerTotalAmount + panjabirAmbortari);
             

                // /////////////////////////////////////Payjama Hisab///////////////////////////////////////////////////////

                var numberPayjama          =   parseFloat($('#numberPayjama').val()) || 1;
                var payjamarMujuri         =   parseFloat($('#payjamarMujuri').val()) || 0;
                var payjamaKaporerPorimap  =   parseFloat($('#payjamaKaporerPorimap').val()) || 0;
                var payjamaKaporerDor      =   parseFloat($('#payjamaKaporerDor').val()) || 0;


                var payjamakaporerTotalAmount = payjamaKaporerPorimap * payjamaKaporerDor;

                var payjamarTotalCost = (numberPayjama * payjamarMujuri) + payjamakaporerTotalAmount;


                // var value1 = parseFloat($('#panjabirMujuri').val()) || 0;
                // var value2 = parseFloat($('#kaporerDor').val()) || 0;
                $('#sum').val(panjabirTotalCost+payjamarTotalCost);


                var Joma                   =   parseFloat($('#Joma').val()) || 0;
                var sum                    =   parseFloat($('#sum').val()) || 0;

                $('#payable').val(sum-Joma);


        };
        setInterval(myFunction, 300);

    </script>

    <script type="text/javascript">
        $(".answer").show();
        $(".coupon_question1").click(function() {
            if($(this).is(":checked")) {
                $(".answer").show();
                $("#cf").prop("checked", false);
            } else {
                $(".answer").hide();
                $("#cf").prop("checked", true);
            }
        });

         $(".coupon_question2").click(function() {
            if($(this).is(":checked")) {
                $(".answer").hide();
                $("#lf").prop("checked", false);

            } else {
                 $(".answer").show();
                $("#lf").prop("checked", true);
            }
        });

    </script>

@endsection