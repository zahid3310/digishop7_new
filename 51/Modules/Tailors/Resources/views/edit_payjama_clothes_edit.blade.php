@extends('layouts.app')

@section('title', 'Update Payjama Clothes')
@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Update Punjabi Description</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Order Management</a></li>
                                    <li class="breadcrumb-item active">Update Punjabi Description</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-5">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{route('payjama_clothes_update',$content->id)}}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div class="form-group row">
                                    <label for="example-text-input" class="form-label">Clothes Name *</label>
                                    <textarea class="form-control" rows="3" cols="3" name="clothes_name">{{$content->clothes_name}}</textarea>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="form-label">Price *</label>
                                    <input type="number" class="form-control" name="price" value="{{$content->price}}">
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="form-label">Status *</label>
                                    <select class="form-control" name="status">
                                        <option value="1" @if($content->status == 1) selected  @endif>Active</option>
                                        <option value="0" @if($content->status == 0) selected  @endif>Inactive</option>
                                    </select>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('add_payjama_clothes') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->

                    <div class="col-7">

                        <div class="card">
                            <div class="card-header" style="background-color: #ffffff!important;border-bottom: 1px solid #2a304169!important;">
                                 <h4 class="card-title float-left">Update Payjama Clothes</h4>
                                 <a href="{{route('add_payjama_clothes')}}" class="btn btn-primary float-right">Add Payjama Clothes</a>
                            </div>
                            <div class="card-body table-responsive">
                                
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Clothes Name</th>
                                            <th>Price</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $item)
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td>{{$item->clothes_name}}</td>
                                            <td>{{ number_format($item->price, 2) }}</td>
                                            <td>
                                                @if($item->status == 1)
                                                <span style="color: #34c38f;">Active</span>
                                                @else
                                                <span style="color: #f46a6a;">Inactive</span>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="dropdown">
                                                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                        <i class="mdi mdi-dots-horizontal"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" style="border: 1px solid #009688;">
                                                        <a style="color: #34c38f !important;border-bottom: 1px solid #34c38f;"class="dropdown-item" href="{{route('payjama_clothes_edit',$item->id)}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>

                                                        <a style="color: #f46a6a!important;" class="dropdown-item" href="{{route('payjama_clothes_remove',$item->id)}}"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    
@endsection