@extends('layouts.app')

@section('title', 'Corporate Orders Update')

@section('styles')
<style>
table, td, th {  
  border: 1px solid #ddd;
  text-align: center;
}

table {
  border-collapse: collapse;
  width: 100%;
}
.myWidth{
    width: 95%!important;
}

.number-center { 
    text-align: center; 
}

.amount-bold { 
    font-weight: bold;
}

.list-group{
      max-height: 400px;
      margin-bottom: 10px;
     overflow-y: scroll;
      -webkit-overflow-scrolling: touch;
    }

</style>

@endsection

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                               <form id="FormSubmit" action="{{ route('tailors_corporate_order_update', $find_order['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                {{ csrf_field() }}

                                <div style="background-color: #F4F4F7;padding-top: 10px;border-top: 2px solid #563d7c;border-radius: 6px;" class="row">

                                    <div class="col-md-6">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-2 col-form-label"><strong>প্রতিষ্ঠানের গ্রাহক *</strong></label>
                                            <div class="col-md-10">
                                                <select name="lebas_corporate_employee" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                   <option value="{{ $find_order['user_id'] }}">{{ $find_order['user_name'] }}({{ $find_order['contact_number'] }})</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-2 col-form-label"><strong>কর্পোরেট গ্রাহক *</strong></label>
                                            <div class="col-md-10">
                                                <select id="customer_id" name="customer_id" class="customerID form-control select2 col-lg-9 col-md-9 col-sm-9 col-9" readonly>
                                                   <option value="{{ $find_order['customer_id'] }}">{{ $find_order['contact_name'] }}({{ $find_order['mobile_number'] }})</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-6 col-form-label"><strong>অর্ডার তারিখ:</strong></label>
                                            <div class="col-md-6">
                                                <input id="order_date" name="order_date" type="text" value="{{ date('d-m-Y', strtotime($find_order['order_date'])) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>
                                        
                                    </div>

                                      <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-5 col-form-label"><strong>অগ্রাধিকার:</strong> </label>
                                            <div class="col-md-7">
                                                <select id="priority_id" name="priority_id" class="priority_id form-control select2">
                                                   <option value="1">Normal</option>
                                                   <option value="2">Urgent</option>
                                                   <option value="3">Very Urgent</option>
                                                </select>
                                            </div>
                                        </div>
     
                                    </div>

                        
                                     <div class="col-md-3">

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label"><strong>ঘুন্ডি :</strong> </label>
                                            <div class="col-md-8 mt-2">
                                                <!-- <input  type="text" name="knob" class="form-control" width="100%"> -->
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="knob" id="knob1" value="1" checked="">
                                                    <label class="form-check-label" for="knob1">Yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="knob" id="knob2" value="2">
                                                    <label class="form-check-label" for="knob2">No</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                      
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-6 col-form-label"><strong>ডেলিভারি তারিখ:</strong></label>
                                            <div class="col-md-6">
                                                <input id="delivery_date" name="delivery_date" type="text" value="{{ date('d-m-Y', strtotime($find_order['delivery_date'])) }}" class="deliveryDate form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="mt-2"><strong>পাঞ্জাবীর স্টাইল :</strong></label>
                                        <select name="services_id" id="tessrter" class="ServiceId form-control select2 PunjabiStyleOtherShow" required>
                                            <option value="0" hidden="">স্টাইল নির্বাচন করুন</option>
                                            @foreach($services as $serv)
                                            <option @if($find_order['services_id'] == $serv->id) selected  @endif value="{{$serv->id}}">{{$serv->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-2" style="display: none;">
                                        <label class="mt-2"><strong>মজুরি :</strong></label>
                                        <input type="number" id="panjabirMujuri" class="servicePrice number-center form-control" name="panjabi_rate"  value="{{$find_order['panjabi_rate']}}">
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <label class="mt-2"><strong>পাজামার স্টাইল :</strong></label>
                                        <select name="payjama_services_id"  class="PayjamaServiceId form-control select2 PayjamasStyleOtherShow" required>
                                            <option value="0" hidden="">স্টাইল নির্বাচন করুন</option>
                                             @foreach($services2 as $serv)
                                            <option @if($find_order['payjama_services_id'] == $serv->id) selected  @endif value="{{$serv->id}}">{{$serv->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-2" style="display: none;">
                                        <label class="mt-2"><strong>মজুরি </strong></label>
                                        <input type="text" id="payjamaMujuri" class="payjamaServicePrice form-control" name="payjama_rate" value="{{$find_order['payjama_rate']}}">
                                    </div>
                                </div>

                                <hr>

                                <div class="row PunjabiStyleOtherData" style="background-color: #F4F4F7;padding-bottom: 5px; padding-top: 5px;">

                                    <div class="col-md-12" style="padding-top:5px;padding-bottom:5px;text-align: center;font-size: 17px;background-color: #9e9e9e36;border-top: 2px solid #563d7c;border-radius: 6px;color: #000;"><strong>পাঞ্জাবীর  মাপ:</strong></div>

                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>লম্বা</strong></label>
                                        <input type="text" class="form-control" name="punjabi_hight" value="{{$find_order['punjabi_hight']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>বডি</strong></label>
                                        <input type="text" class="form-control" name="punjabi_body" value="{{$find_order['punjabi_body']}}">
                                    </div>

                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>লুজ</strong></label>
                                        <input type="text" class="form-control" name="punjabi_loose" value="{{$find_order['punjabi_loose']}}">
                                    </div>                                    
                                    
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>কল্লি </strong></label>
                                        <input type="text" class="form-control" name="punjabi_kolli" value="{{$find_order['punjabi_kolli']}}">
                                    </div>
                                    
                                     <div class="col-md-2">
                                        <label class="mt-2"><strong>পেট</strong></label>
                                        <input type="text" class="form-control" name="punjabi_belly" value="{{$find_order['punjabi_belly']}}">
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>কাঁধ</strong></label>
                                        <input type="text" class="form-control" name="punjabi_shoulders" value="{{$find_order['punjabi_shoulders']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>হাতা</strong></label>
                                        <input type="text" class="form-control" name="punjabi_hand" value="{{$find_order['punjabi_hand']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>হাতের মুহুরী</strong></label>
                                        <input type="text" class="form-control" name="punjabi_hand_muhuri" value="{{$find_order['punjabi_hand_muhuri']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>গলা</strong></label>
                                        <input type="text" class="form-control" name="punjabi_Throat" value="{{$find_order['punjabi_Throat']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>ঘের</strong></label>
                                        <input type="text" class="form-control" name="punjabi_gher" value="{{$find_order['punjabi_gher']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>আর্মহোল</strong></label>
                                        <input type="text" class="form-control" name="punjabi_armhole" value="{{$find_order['punjabi_armhole']}}">
                                    </div>


                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>কলার প্লেট</strong></label>
                                        <input type="text" class="form-control" name="punjabi_collar_plate" value="{{$find_order['punjabi_collar_plate']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>বুক ফারা</strong></label>
                                        <input type="text" class="form-control" name="punjabi_buk_fara" value="{{$find_order['punjabi_buk_fara']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>বোতাম</strong></label>
                                        <input type="number" class="form-control" name="punjabi_button" value="{{$find_order['punjabi_button']}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>কলার</strong></label>
                                        <select class="form-control" name="punjabi_kolar">
                                            <option {{ $find_order['punjabi_kolar'] == 1 ? 'selected' : '' }} value="1">গোল গলা</option>
                                            <option {{ $find_order['punjabi_kolar'] == 2 ? 'selected' : '' }} value="2">শেরওয়ানি</option>
                                            <option {{ $find_order['punjabi_kolar'] == 3 ? 'selected' : '' }} value="3">হাফ বেল্ট</option>
                                            <option {{ $find_order['punjabi_kolar'] == 4 ? 'selected' : '' }} value="4">ফুল বেল্ট</option>
                                        </select>
                                    </div>
                                    
                                    
                                    <div class="col-md-4">
                                        @if($find_order['have_panjabi_ambodari'] == 1)
                                        <div style="margin-top: 20px" class="form-group row">
                                            <label class="col-md-6 col-form-label"><strong>পাঞ্জাবীর এমব্রয়ডারি  {{$find_order['have_panjabi_ambodari']}}:</strong> </label>
                                            <div class="col-md-6 mt-2">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input ShowPanjabiAmbodari1" type="radio" name="have_panjabi_ambodari" id="have_panjabiAmbodari1" value="1" checked="">
                                                    <label class="form-check-label" for="have_panjabiAmbodari1">Yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input ShowPanjabiAmbodari2" type="radio" name="have_panjabi_ambodari" id="have_panjabiAmbodari2" value="2">
                                                    <label class="form-check-label" for="have_panjabiAmbodari2">No</label>
                                                </div>
                                            </div>
                                        </div>
                                         @else

                                         <div style="margin-top: 20px" class="form-group row">
                                            <label class="col-md-6 col-form-label"><strong>পাঞ্জাবীর এমব্রয়ডারি :</strong> </label>
                                            <div class="col-md-6 mt-2">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input ShowPanjabiAmbodari1" type="radio" name="have_panjabi_ambodari" id="have_panjabiAmbodari1" value="1" >
                                                    <label class="form-check-label" for="have_panjabiAmbodari1">Yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input ShowPanjabiAmbodari2" type="radio" name="have_panjabi_ambodari" id="have_panjabiAmbodari2" value="2" checked="">
                                                    <label class="form-check-label" for="have_panjabiAmbodari2">No</label>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    
                                    
                                    
                                    <div class="col-md-2 panjabiAmbodariStyle">
                                        <label class="mt-2"><strong>পাঞ্জাবীর এমব্রয়ডারি সংখ্যা</strong></label>
                                        <input type="number" class="form-control" name="number_punjabi_embroidery" value="$find_order['number_punjabi_embroidery']">
                                    </div>
                                   
                                    
                                    <div class="col-md-11">
                                        <label class="mt-2" style="font-weight: 500;"><strong>পাঞ্জাবীর বিবরণ:</strong></label>
                                        <textarea class="punjabi-description form-control" name="punjabi_description" rows="2">{{$find_order['punjabi_description']}}</textarea>
                                    </div>



                                    <div class="col-md-1" style="margin-top: 35px;">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#punjabiModal"><i class="bx bx-plus"></i></button>
                                    </div>

                                    <!--পাঞ্জাবীর বিবরণ নিৰ্বাচন  Modal -->
                                    <div class="modal fade" id="punjabiModal" tabindex="-1" role="dialog" aria-labelledby="punjabiModalLabel" aria-hidden="true">
                                      <div class="modal-dialog modal-xl" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h5 class="modal-title" id="punjabiModalLabel">পাঞ্জাবীর বিবরণ তালিকা</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </div>
                                          <div class="modal-body">
                                            <table id="punjabiTable" class="list-group table table-striped table-bordered table-hover">
                                                <tbody>
                                                    @foreach($PunjabiDescription as $item)
                                                    <tr>
                                                        <!-- <td width="5%"><button type="button" class="punjabi_description btn btn-sm btn-primary">Select</button></td> -->
                                                        <td width="100%" class="punjabi_description" style="text-align: justify;cursor: pointer;">{{$item->punjabi_description}}</td>
                                                    </tr>
                                                     @endforeach
                                                </tbody>
                                            </table>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                
                                    <!--পাঞ্জাবীর বিবরণ নিৰ্বাচন  Modal End -->
                                   
                                    <div class="col-md-11 panjabiAmbodariStyle">
                                        <label class="mt-2" style="font-weight: 500;"><strong>এমব্রয়ডারি বিবরণ:</strong></label>
                                        <textarea class="em-description form-control" name="embroidery" rows="2">{{$find_order['embroidery']}}</textarea>
                                    </div>



                                    <div class="col-md-1 panjabiAmbodariStyle" style="margin-top: 35px;">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#embroideryModal"><i class="bx bx-plus"></i></button>
                                    </div>

                                    <!--এমব্রয়ডারি  বিবরণ নিৰ্বাচন  Modal -->
                                    <div class="modal fade" id="embroideryModal" tabindex="-1" role="dialog" aria-labelledby="embroideryModalLabel" aria-hidden="true">
                                      <div class="modal-dialog modal-xl" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h5 class="modal-title" id="embroideryModalLabel">এমব্রয়ডারি বিবরণ তালিকা</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </div>
                                          <div class="modal-body">
                                            <table id="embroideryTable" class="list-group table table-striped table-bordered table-hover">
                                                <tbody>
                                                    @foreach($EmbroideryDescription as $item)
                                                    <tr>
                                                        <td width="100%" class="embroidery_description" style="text-align: justify;cursor: pointer;">{{$item->embroidery_description}}</td>
                                                    </tr>
                                                     @endforeach
                                                </tbody>
                                            </table>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  
                                
                                    <!--এমব্রয়ডারি বিবরণ নিৰ্বাচন  Modal End -->
                                </div>


                                
                                <div class="row PayjamasStyleOtherData" style="background-color: #F4F4F7;padding-top: 5px;padding-bottom: 10px;">

                                   

                                    <div class="col-md-12" style="padding-top:5px;padding-bottom:5px;text-align: center;font-size: 17px;background-color: #9e9e9e36;border-top: 2px solid #563d7c;border-radius: 6px;color: #000;"><strong>পাজামার মাপ:</strong></div>
                                    
                                    
                                    
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>লম্বা</strong></label>
                                        <input type="text" class="form-control" name="payjama_hight" value="{{$find_order['payjama_hight']}}">
                                    </div>

                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>মুহুরী</strong></label>
                                        <input type="text" class="form-control" name="payjama_muhuri" value="{{$find_order['payjama_muhuri']}}">
                                    </div>

                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>লুস</strong></label>
                                        <input type="text" class="form-control" name="payjama_lus" value="{{$find_order['payjama_lus']}}">
                                    </div>

                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>হাই</strong></label>
                                        <input type="text" class="form-control" name="payjama_high" value="{{$find_order['payjama_high']}}">
                                    </div>

                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>কোমর</strong></label>
                                        <input type="text" class="form-control" name="payjama_komor" value="{{$find_order['payjama_komor']}}">
                                    </div>
                                    
                                    
                                     <div class="col-md-4">
                                        @if($find_order['have_panjabi_ambodari'] == 1)
                                        <div style="margin-top: 20px" class="form-group row">
                                            <label class="col-md-6 col-form-label"><strong>পাজামার এমব্রয়ডারি :</strong> </label>
                                            <div class="col-md-6 mt-2">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input ShowPayjamaAmbodari1" type="radio" name="have_payjama_ambodari" id="have_payjamaAmbodari1" value="1" checked="">
                                                    <label class="form-check-label" for="have_payjamaAmbodari1">Yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input ShowPayjamaAmbodari2" type="radio" name="have_payjama_ambodari" id="have_payjamaAmbodari2" value="2">
                                                    <label class="form-check-label" for="have_payjamaAmbodari2">No</label>
                                                </div>
                                            </div>
                                        </div>
                                         @else

                                         <div style="margin-top: 20px" class="form-group row">
                                            <label class="col-md-6 col-form-label"><strong>পাজামার এমব্রয়ডারি :</strong> </label>
                                            <div class="col-md-6 mt-2">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input ShowPayjamaAmbodari1" type="radio" name="have_payjama_ambodari" id="have_payjamaAmbodari1" value="1">
                                                    <label class="form-check-label" for="have_payjamaAmbodari1">Yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input ShowPayjamaAmbodari2" type="radio" name="have_payjama_ambodari" id="have_payjamaAmbodari2" value="2" checked="">
                                                    <label class="form-check-label" for="have_payjamaAmbodari2">No</label>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>

                   
                                    <div class="col-md-2 payjamaAmbodariStyle">
                                        <label class="mt-2"><strong>পাজামার এমব্রয়ডারি সংখ্যা</strong></label>
                                        <input type="number" class="form-control" name="number_payjama_embroidery" value="$find_order['number_payjama_embroidery']">
                                    </div>
                               


                                    
                                    <div class="col-md-11">
                                        <label class="mt-2" style="font-weight: 500;"><strong>পাজামার বিবরণ:</strong></label>
                                        <textarea class="payjama-description form-control" name="payjama_description" rows="2">{{$find_order['payjama_description']}}</textarea>
                                    </div>



                                    <div class="col-md-1" style="margin-top: 35px;">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#payjamaModal"><i class="bx bx-plus"></i></button>
                                    </div>

                                    <!--পাঞ্জাবীর বিবরণ নিৰ্বাচন  Modal -->
                                    <div class="modal fade" id="payjamaModal" tabindex="-1" role="dialog" aria-labelledby="payjamaModalLabel" aria-hidden="true">
                                      <div class="modal-dialog modal-xl" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h5 class="modal-title" id="payjamaModalLabel">পাজামার বিবরণ তালিকা</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </div>
                                          <div class="modal-body">
                                            <table id="payjamaTable" class="list-group table table-striped table-bordered table-hover">
                                                <tbody>
                                                    @foreach($PayjamaDescription as $item)
                                                    <tr>
                                                        <!-- <td width="5%"><button type="button" class="payjama_description btn btn-sm btn-primary">Select</button></td> -->
                                                        <td width="100%" class="payjama_description" style="text-align: justify;cursor: pointer;">{{$item->payjama_description}}</td>
                                                    </tr>
                                                     @endforeach
                                                </tbody>
                                            </table>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <!--পাজামার বিবরণ নিৰ্বাচন  Modal End -->
                                   
                                    <div class="col-md-11 payjamaAmbodariStyle">
                                        <label class="mt-2" style="font-weight: 500;"><strong>পাজামার এমব্রয়ডারি বিবরণ:</strong></label>
                                        <textarea class="payjama-em-description form-control" name="payjama_embroidery" rows="2">{{$find_order['payjama_embroidery']}}</textarea>
                                    </div>



                                    <div class="col-md-1 payjamaAmbodariStyle" style="margin-top: 35px;">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#payjamaEmbroideryModal"><i class="bx bx-plus"></i></button>
                                    </div>

                                    <!--পাজামার এমব্রয়ডারি  বিবরণ নিৰ্বাচন  Modal -->
                                    <div class="modal fade" id="payjamaEmbroideryModal" tabindex="-1" role="dialog" aria-labelledby="payjamaEmbroideryModalLabel" aria-hidden="true">
                                      <div class="modal-dialog modal-xl" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h5 class="modal-title" id="payjamaEmbroideryModalLabel">পাজামার এমব্রয়ডারি বিবরণ তালিকা</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </div>
                                          <div class="modal-body">
                                            <table id="payjamaEmbroideryTable" class="list-group table table-striped table-bordered table-hover">
                                                <tbody>
                                                    @foreach($PayjamaEmbroideryDescription as $item)
                                                    <tr>
                                                        <td width="100%" class="payjama_embroidery_description" style="text-align: justify;cursor: pointer;">{{$item->payjama_embroidery_description}}</td>
                                                    </tr>
                                                     @endforeach
                                                </tbody>
                                            </table>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                   
                                
                                    <!--পাজামার এমব্রয়ডারি বিবরণ নিৰ্বাচন  Modal End -->
                                     
                                </div>

                                
                               
                                <div class="row" style="background-color: #F4F4F7;padding-top: 5px;padding-bottom: 10px;">

                                    <div class="col-md-6 PunjabiStyleOtherData" style="border-right: 2px dotted #000;padding-top: 10px!important;">
                                        <center><strong style="font-size: 16px;">পাঞ্জাবীর হিসাব :</strong></center>
                                        <hr>
                                        <div class="row">

                                            <div class="col-md-4">
                                                <label class="mt-2"><strong>পাঞ্জাবীর সংখ্যা:</strong></label>
                                                <input type="number" name="number_of_punjabi" id="numberPunjabi" class="number-center form-control" value="{{$find_order['number_of_punjabi']}}" readonly="">
                                            </div> 

                                            @if($find_order['lf_cf'] == 1)
                                            <div class="col-md-6 mt-4">

                                                <div class="font-size-20 form-check form-check-inline">
                                                    <input  class="coupon_question1 form-check-input" type="checkbox" id="lf" name="lf_cf" checked="" value="1">
                                                    <label class="form-check-label" for="lf">LF</label>
                                                </div>
                                                
                                                <div class="font-size-20 form-check form-check-inline">
                                                    <input class="coupon_question2 form-check-input" type="checkbox" id="cf" name="lf_cf" value="0">
                                                    <label class="form-check-label" for="cf">CF</label>
                                                </div>
                                                
                                                
                                            </div>

                                            @else

                                            <div class="col-md-6 mt-4">

                                                <div class="font-size-20 form-check form-check-inline">
                                                    <input  class="coupon_question1 form-check-input" type="checkbox" id="lf" name="lf_cf"  value="1">
                                                    <label class="form-check-label" for="lf">LF</label>
                                                </div>
                                                
                                                <div class="font-size-20 form-check form-check-inline">
                                                    <input class="coupon_question2 form-check-input" type="checkbox" id="cf" name="lf_cf" checked="" value="0">
                                                    <label class="form-check-label" for="cf">CF</label>
                                                </div>
                                                
                                                
                                            </div>
                                            @endif
 
                                            <div class="col-md-2" style="display:none;">
                                                <span class="btn btn-primary" onclick="panjabiInfoAdd()">ADD</span>
                                            </div>

                                            <div class="col-md-12" style="display:none;">
                                                <label class="mt-2"><strong>স্যাম্পল  :</strong></label>
                                               

                                                <input type="file" name="panjabi_photo" class="form-control">
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 input_fields_wrap getMultipleRow">
                                                @foreach($find_order_entries as $key => $item)
                                                
                                            
                                                <div class="row di_{{$key}}">
                                                    <div class="answer col-md-10">
                                                        <label class="mt-2"><strong>কাপড়ের নাম</strong></label>
                                                        <input type="text" class="form-control"value="{{$item->name}} - {{$item->product_code}}" readonly="">
                                                        <input type="hidden" class="form-control" name="punjabi_cloth_name[]" value="{{$item->punjabi_cloth_name}}">
                                                    </div>

                                                    <div class="col-md-2" style="margin-top: 35px;display: none;">
                                                    <span  data-val="{{$key}}" class="btn btn-danger remove_field"><i class="fas fa-trash"></i></span>
                                                    </div>

                                                    <div class="answer col-md-3">
                                                        <label class="mt-2"><strong>কাপড়ের পরিমান</strong></label>
                                                        <input type="number" step="0.01" id="panjabirKaporerPorimap_{{$key}}" class="number-center form-control" name="amount_panjabi_cloth[]" value="{{$item->amount_panjabi_cloth}}">

                                                        <input type="hidden" class="number-center form-control" name="old_amount_panjabi_cloth[]" value="{{$item->amount_panjabi_cloth}}">

                                                    </div>

                                                    <div class="answer col-md-3">
                                                        <label class="mt-2"><strong>কাপড়ের দর</strong></label>
                                                        <input type="number" id="panjabirKaporerDor_{{$key}}" class="punjabiPrice_{{$key}} number-center form-control" name="price_panjabi_cloth[]"  value="{{$item->price_panjabi_cloth}}">
                                                    </div>

                                                    <div class="col-md-3 panjabiAmbodariStyle">
                                                        <label class="mt-2"><strong>এমব্রয়ডারি দর</strong></label>

                                                        <input type="number" class="number-center form-control" name="ambortari_price[]" id="panjabirAmbortari_{{$key}}" value="{{$item->ambortari_price}}">
                                                    </div>

                                                    <div class="col-md-3 panjabiAmbodariStyle">
                                                        <label class="mt-2"><strong>এমব্রয়ডারি নম্বর </strong></label>
                                                        <input type="text" class="number-center form-control" name="punjabi_ambortari_no[]" value="{{$item->punjabi_ambortari_no}}">
                                                    </div>

                                                    <div class="col-md-3">
                                                        <label class="mt-2"><strong>অন্যান্য </strong></label>
                                                        <input type="text" class="number-center form-control" name="panjabi_others_price[]" id="panjabi_others_price_{{$key}}" value="{{$item->panjabi_others_price}}">
                                                    </div>

                                                </div>
                                                @endforeach
                                            </div>
                                        </div>



                                    </div>

                                    

                                    <div class="col-md-6 PayjamasStyleOtherData" style="padding-top: 10px!important;">

                                        <center><strong style="font-size: 16px;">পাজামার হিসাব :</strong></center>
                                        <hr>
                                        <div class="row">

                                            <div class="col-md-4">
                                                <label class="mt-2"><strong>পাজামার সংখ্যা:</strong></label>
                                                <input type="number" name="number_of_payjama" id="numberPayjama" class="number-center form-control" value="{{$find_order['number_of_payjama']}}" readonly="">
                                            </div>
                                             @if($find_order['payjama_lf_cf'] == 1)
                                             <div class="col-md-6 mt-4">

                                               
                                                <div class="font-size-20 form-check form-check-inline">
                                                    <input  class="coupon_question3 form-check-input" type="checkbox" id="payjama_lf" name="payjama_lf_cf" checked="" value="1">
                                                    <label class="form-check-label" for="payjama_lf">LF</label>
                                                </div>
                                                
                                                <div class="font-size-20 form-check form-check-inline">
                                                    <input class="coupon_question4 form-check-input" type="checkbox" id="payjama_cf" name="payjama_lf_cf" value="0">
                                                    <label class="form-check-label" for="payjama_cf">CF</label>
                                                </div>
                                                
                                            </div>

                                            @else

                                            <div class="col-md-6 mt-4">
                                                <div class="font-size-20 form-check form-check-inline">
                                                    <input  class="coupon_question3 form-check-input" type="checkbox" id="payjama_lf" name="payjama_lf_cf" value="1">
                                                    <label class="form-check-label" for="payjama_lf">LF</label>
                                                </div>
                                                
                                                <div class="font-size-20 form-check form-check-inline">
                                                    <input class="coupon_question4 form-check-input" type="checkbox" id="payjama_cf" name="payjama_lf_cf" checked="" value="0">
                                                    <label class="form-check-label" for="payjama_cf">CF</label>
                                                </div>
                                            </div>
                                            @endif

                                            <div class="col-md-2" style="display:none;">
                                                <span class="btn btn-primary" onclick="payjamaInfoAdd()">ADD</span>
                                            </div>

                                            <div class="col-md-12" style="display:none;">
                                                <label class="mt-2"><strong>স্যাম্পল :</strong></label>
                                                <input type="file" name="payjama_photo" class="form-control">
                                            </div>
                                        </div>

                                            


                                            <div class="row">
                                                <div class="col-md-12 input_fields_wrap2 pGetMultipleRow">
                                                        @foreach($find_payjama_order_entries as $key => $item)
                                                    <div class="row dii_{{$key}}" style="padding-left: 18px;">
                                                        <div class="answer1 col-md-10">
                                                            <label class="mt-2"><strong>কাপড়ের নাম</strong></label>
                                                            <input type="text" class="form-control"value="{{$item->name}} - {{$item->product_code}}" readonly="">
                                                            <input type="hidden" class="form-control" name="payjama_cloth_name[]" value="{{$item->payjama_cloth_name}}">
                                                        </div>

                                                        <div class="col-md-2" style="margin-top: 35px;display: none;">
                                                            <span  data-val="{{$key}}" class="btn btn-danger remove_field2"><i class="fas fa-trash"></i></span>
                                                        </div>

                                                        <div class="answer1 col-md-3">
                                                            <label class="mt-2"><strong>পরিমান</strong></label>
                                                            <input type="number" step="0.01"  class="number-center form-control" name="amount_payjama_cloth[]" id="payjamaKaporerPorimap_{{$key}}" value="{{$item->amount_payjama_cloth}}">

                                                            <input type="hidden" class="number-center form-control" name="old_amount_payjama_cloth[]" value="{{$item->amount_payjama_cloth}}">

                                                        </div>
                                                        <div class="answer1 col-md-3">
                                                            <label class="mt-2"><strong> দর</strong></label>
                                                            <input type="number" id="payjamaKaporerDor_{{$key}}" class="payjamaPrice_{{$key}} number-center form-control" name="price_payjama_cloth[]"  value="{{$item->price_payjama_cloth}}">
                                                        </div>
                                                       
                                                        <div class="col-md-3 panjabiAmbodariStyle">
                                                            <label class="mt-2"><strong>এমব্রয়ডারি দর</strong></label>
                                                            <input type="number" class="number-center form-control" name="payjama_ambortari_price[]" id="payjamaAmbortari_{{$key}}" value="{{$item->payjama_ambortari_price}}">
                                                        </div>

                                                        <div class="col-md-3 panjabiAmbodariStyle">
                                                            <label class="mt-2"><strong>এমব্রয়ডারি নম্বর </strong></label>
                                                            <input type="text" class="number-center form-control" name="payjama_ambortari_no[]" value="{{$item->payjama_ambortari_no}}">
                                                        </div>
                                                       

                                                        <div class="col-md-3">
                                                            <label class="mt-2"><strong>অন্যান্য</strong></label>
                                                            <input type="text" class="number-center form-control" name="payjama_others_price[]" id="payjama_others_price_{{$key}}" value="{{$item->payjama_others_price}}">
                                                        </div>

                                                    </div>
                                                        @endforeach
                                                </div>
                                            </div>

                                            

                                        </div>
                                    </div>


                                </div>

                                <div class="row" style="background-color: #F4F4F7;padding-top: 5px;border-top: 2px dotted #000;">
                                   

                                    <div class="col-md-3">
                                        <label class="mt-2"><strong>মোট টাকা :</strong></label>
                                        <input type="number" id="sum"  class="amount-bold number-center form-control" name="total_amount" readonly="">
                                        <input type="hidden" id="row_number" value="{{$find_order_entries->count()}}">
                                        <input type="hidden" id="row_numberr" value="{{$find_payjama_order_entries->count()}}">
                                    </div>

                                    <div class="col-md-3">
                                        <label class="mt-2"><strong>ডিসকাউন্ট :</strong></label>
                                        <input type="number" step="0.01" id="discount" class="amount-bold number-center form-control" name="discount" value="{{$find_order['discount']}}">
                                    </div>

                                    <div class="col-md-3">
                                        <label class="mt-2"><strong>জমা টাকা :</strong></label>
                                        <input type="number" id="Joma" class="amount-bold number-center form-control" name="submission_amount" value="{{$find_order['submission_amount']}}">
                                    </div>

                                     <div class="col-md-3">
                                        <label class="mt-2"><strong>বাকি টাকা :</strong></label>
                                        <input type="number" id="payable" class="servicePrice amount-bold number-center form-control" name="due_amount" value="{{$find_order['due_amount']}}" readonly="">
                                    </div>
                                </div>

                                <hr>

                                
                            


                                <div style="padding-top: 5px !important;padding-bottom: 5px !important;background-color: #F4F4F7;" class="row">
                                   
                                    <div class="col-md-12 text-right mt-2">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Update & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('tailors_order_index') }}">Close</a></button>
                                            </div>
                                        </div>
                                    </div>

                                  
                                </div>


                                </form>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>



  
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();
            $("#payjama_product_entries_0").select2({
                ajax: { 
                url:  site_url + '/bills/product-list-load-bill',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
            
            $("#product_entries_0").select2({
                ajax: { 
                url:  site_url + '/bills/product-list-load-bill',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/tailors/orders/local-customer-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };

                    console.log(response);
                
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

       
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var email                       = $("#email").val();
            var date_of_birth               = $("#db").val();
            var marriage_date               = $("#md").val();
            var contact_type                = $("#contact_type").val();
            var site_url                    = $('.site_url').val();


            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/tailors/orders/local-customer-add',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number,email : email ,date_of_birth : date_of_birth,marriage_date : marriage_date, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        console.log(data.id)
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                        }
                        
                        $("#customer_id").empty();
                        $("#mobileNumber").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                        $("#mobileNumber").val(data.mobile_number);
                    }
                });
            }
        });
    
    </script>

 


    <script type="text/javascript">
      
        $(document).ready(function(){
            $("select.customerID").change(function(){
                var customerID = $(this).children("option:selected").val();
                var site_url                    = $('.site_url').val();
                $.ajax({
                    type:   'get',
                    url:    site_url + '/tailors/orders/localCustomerList1',
                    data:   {customerID : customerID, _token: '{{csrf_token()}}' },
        
                    success: function (data) {

                        console.log(data);
                        $("#mobileNumber").val(data['mobile_number']);
                        
                    }
                });
            });


        });


        $(document).ready(function(){
            $("select.ServiceId").change(function(){
                var ServiceId = $(this).children("option:selected").val();
                var site_url                    = $('.site_url').val();
                $.ajax({
                    type:   'get',
                    url:    site_url + '/tailors/orders/get-service-price',
                    data:   {ServiceId : ServiceId, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        $(".servicePrice").val(data[0]['price']);


                    }
                });
            });


        });
        
        $(document).ready(function(){
                    $("select.PayjamaServiceId").change(function(){
                        var PayjamaServiceId = $(this).children("option:selected").val();
                        var site_url                    = $('.site_url').val();
                        $.ajax({
                            type:   'get',
                            url:    site_url + '/tailors/orders/get-payjama-service-price',
                            data:   {PayjamaServiceId : PayjamaServiceId, _token: '{{csrf_token()}}' },
                
                            success: function (data) {
                                $(".payjamaServicePrice").val(data[0]['price']);


                            }
                        });
                    });


                });

              $(document).ready(function(){
                   var wrapper          = $(".input_fields_wrap");

                $(wrapper).on("click",".remove_field", function(e)
                {
                    e.preventDefault();

                    var x = $(this).attr("data-val");

                    $('.di_'+x).remove(); x--;
                });


                });

                $(document).ready(function(){
                    var wrapper2          = $(".input_fields_wrap2");
                    $(wrapper2).on("click",".remove_field2", function(e)
                    {
                        e.preventDefault();

                        var y = $(this).attr("data-val");

                        $('.dii_'+y).remove(); y--;
                    });


                });

        //  $(document).ready(function(){
        //     $("select.punjabiPriceID").change(function(){

        //         var punjabiPrice = $(this).children("option:selected").val();
        //         var site_url                    = $('.site_url').val();
        //         $.ajax({
        //             type:   'get',
        //             url:    site_url + '/tailors/orders/get-punjabi-price',
        //             data:   {punjabiPrice : punjabiPrice, _token: '{{csrf_token()}}' },
        
        //             success: function (punjabidata) {
        //                 $(".punjabiPrice").val(punjabidata[0]['sell_price']);


        //             }
        //         });
        //     });

        // }); 


         $(document).ready(function(){
            $("select.payjamaPriceId").change(function(){
                var payjamaPrice = $(this).children("option:selected").val();
                var site_url                    = $('.site_url').val();
                $.ajax({
                    type:   'get',
                    url:    site_url + '/tailors/orders/get-payjama-price',
                    data:   {payjamaPrice : payjamaPrice, _token: '{{csrf_token()}}' },
        
                    success: function (payjamadata) {
                        $(".payjamaPrice").val(payjamadata[0]['sell_price']);


                    }
                });
            });

        });


        $(document).ready(function(){
            $("select.priority_id").change(function(){
                var priority_id = $(this).children("option:selected").val();

                if (priority_id == 1) {
                    var priorityDate = <?php echo json_encode(date('d-m-Y',strtotime("10 days"))); ?>;
                     $(".deliveryDate").val(priorityDate);
                }
                if (priority_id == 2) {
                    var priorityDate = <?php echo json_encode(date('d-m-Y',strtotime("5 days"))); ?>;
                    $(".deliveryDate").val(priorityDate);
                }

                if (priority_id == 3) {
                    var priorityDate = <?php echo json_encode(date('d-m-Y',strtotime("3 days"))); ?>;
                    $(".deliveryDate").val(priorityDate);
                }
            });

 


        });


        $("#punjabiTable").on('dblclick','.punjabi_description',function(){
        // get the current row
        var currentRow=$(this).closest("tr"); 
        var col2=currentRow.find("td:eq(0)").text();
        $(".punjabi-description").val(col2);
        $('#punjabiModal').modal('hide');
        });


        $("#payjamaTable").on('dblclick','.payjama_description',function(){
        // get the current row
        var currentRow=$(this).closest("tr"); 
        var col2=currentRow.find("td:eq(0)").text();
        $(".payjama-description").val(col2);
        $('#payjamaModal').modal('hide');
        });


        $("#embroideryTable").on('dblclick','.embroidery_description',function(){
        // get the current row
        var emEurrentRow = $(this).closest("tr"); 
        var td = emEurrentRow.find("td:eq(0)").text();
        $(".em-description").val(td);
        $('#embroideryModal').modal('hide');
        });
        
    </script>

    <script type="text/javascript">


        var myFunction = function() {

                var row_number          =   parseFloat($('#row_number').val());
                var row_numberr          =   parseFloat($('#row_numberr').val());



                var panjabikaporerTotalAmount = 0;

                for (var i = 0; i < row_number; i++) {
                    var panjabirKaporerPorimap =   parseFloat($('#panjabirKaporerPorimap_'+i).val()) || 0;
                    var panjabirKaporerDor     =   parseFloat($('#panjabirKaporerDor_'+i).val()) || 0;
                    var panjabirAmbortari      =   parseFloat($('#panjabirAmbortari_'+i).val()) || 0;
                    var panjabiOthersPrice      =   parseFloat($('#panjabi_others_price_'+i).val()) || 0;

                    var panjabikaporerTotalAmount = (panjabirKaporerPorimap * panjabirKaporerDor) + panjabirAmbortari + panjabiOthersPrice + panjabikaporerTotalAmount;

                    
                }
                var numberPunjabi          =   parseFloat($('#numberPunjabi').val()) || 1;
                var panjabirMujuri         =   parseFloat($('#panjabirMujuri').val()) || 0;
                var panjabirTotalCost = (numberPunjabi * panjabirMujuri) + panjabikaporerTotalAmount;
               

                
             

                // /////////////////////////////////////Payjama Hisab///////////////////////////////////////////////////////

                var payjamakaporerTotalAmount = 0;

                for (var u = 0; u < row_numberr; u++) {
                    var payjamaKaporerPorimap  =   parseFloat($('#payjamaKaporerPorimap_'+u).val()) || 0;
                    var payjamaKaporerDor      =   parseFloat($('#payjamaKaporerDor_'+u).val()) || 0;
                    var payjamaAmbortari      =   parseFloat($('#payjamaAmbortari_'+u).val()) || 0;
                    var payjamaOthersPrice  =   parseFloat($('#payjama_others_price_'+u).val()) || 0;

                    var payjamakaporerTotalAmount = (payjamaKaporerPorimap * payjamaKaporerDor) + payjamaAmbortari + payjamaOthersPrice + payjamakaporerTotalAmount;

                    
                }


                var numberPayjama          =   parseFloat($('#numberPayjama').val()) || 1;
                var payjamaMujuri         =   parseFloat($('#payjamaMujuri').val()) || 0;
                var payjamarTotalCost = (numberPayjama * payjamaMujuri) + payjamakaporerTotalAmount;


                // var value1 = parseFloat($('#panjabirMujuri').val()) || 0;
                // var value2 = parseFloat($('#kaporerDor').val()) || 0;
                $('#sum').val(panjabirTotalCost+payjamarTotalCost);


                var Joma                   =   parseFloat($('#Joma').val()) || 0;
                var sum                    =   parseFloat($('#sum').val()) || 0;
                var discount         =   parseFloat($('#discount').val()) || 0;

                $('#payable').val(sum-Joma-discount);


        };
        setInterval(myFunction, 300);

    </script>

    <script type="text/javascript">
        $(".answer").show();
        $(".coupon_question1").click(function() {
            if($(this).is(":checked")) {
                $(".answer").show();
                $("#cf").prop("checked", false);
            } else {
                $(".answer").hide();
                $("#cf").prop("checked", true);
            }
        });

         $(".coupon_question2").click(function() {
            if($(this).is(":checked")) {
                $(".answer").hide();
                $("#lf").prop("checked", false);

            } else {
                 $(".answer").show();
                $("#lf").prop("checked", true);
            }
        });

    </script>


    <script type="text/javascript">
        $(".answer1").show();
        $(".coupon_question3").click(function() {
            if($(this).is(":checked")) {
                $(".answer1").show();
                $("#payjama_cf").prop("checked", false);
            } else {
                $(".answer1").hide();
                $("#payjama_cf").prop("checked", true);
            }
        });

         $(".coupon_question4").click(function() {
            if($(this).is(":checked")) {
                $(".answer1").hide();
                $("#payjama_lf").prop("checked", false);

            } else {
                 $(".answer1").show();
                $("#payjama_lf").prop("checked", true);
            }
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){

            if ($("#payjama_lf").prop("checked") == true) {
                $(".answer1").show();
            }else
            {
               $(".answer1").hide(); 
            }

            if ($("#lf").prop("checked") == true) {
                $(".answer").show();
            }else
            {
              $(".answer").hide();
            }
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){

            if ($("#have_panjabiAmbodari1").prop("checked") == true) {
                $(".panjabiAmbodariStyle").show();
            }else
            {
               $(".panjabiAmbodariStyle").hide(); 
            }

            if ($("#have_payjamaAmbodari1").prop("checked") == true) {
                $(".payjamaAmbodariStyle").show();
            }else
            {
              $(".payjamaAmbodariStyle").hide();
            }
        });
    </script>


    <!-- <script type="text/javascript">
        $(".PunjabiStyleOtherData").hide();
        $(".PayjamasStyleOtherData").hide();

         $("select.PunjabiStyleOtherShow").change(function(){
                var PunjabiStyleOtherShow = $(this).children("option:selected").val();
                if(PunjabiStyleOtherShow == 0) {
                    $(".PunjabiStyleOtherData").hide();
                    $('.servicePrice').val('');
                } else {
                    $(".PunjabiStyleOtherData").show();
                }
            });
            
        $("select.PayjamasStyleOtherShow").change(function(){
                var PayjamasStyleOtherShow = $(this).children("option:selected").val();
                if(PayjamasStyleOtherShow == 0) {
                    $(".PayjamasStyleOtherData").hide();
                    $('.payjamaServicePrice').val('');
                } else {
                    $(".PayjamasStyleOtherData").show();
                }
            });

    </script> -->


    <script type="text/javascript">
        var wrapper          = $(".input_fields_wrap");
        var max_fields       = 500;
        var x = $('#row_number').val()-1;
        var j = $('#row_number').val();
        function panjabiInfoAdd() 
        {



            $('#row_number').val(++j);

             if(x < max_fields)
            {
                x++;

                 $('.getMultipleRow').prepend(`<div class="row di_`+x+`">
                                                    <div class="answer col-md-10">
                                                        <label class="mt-2"><strong>কাপড়ের নাম</strong></label>
                                                        <select class="form-control single_select2" id="product_entries_`+x+`" onchange="punjabiGetItemPrice(`+x+`)" name="punjabi_cloth_name[]">
                                                            <option value="0">নির্বাচন করুন</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2" style="margin-top: 35px;">
                                                    <span  data-val="`+x+`" class="btn btn-danger remove_field"><i class="fas fa-trash"></i></span>
                                                    </div>
                                                    <div class="answer col-md-3">
                                                        <label class="mt-2"><strong>কাপড়ের পরিমান</strong></label>
                                                        <input type="number" step="0.01" id="panjabirKaporerPorimap_`+x+`" class="number-center form-control" name="amount_panjabi_cloth[]">
                                                    </div>

                                                    <div class="answer col-md-3">
                                                        <label class="mt-2"><strong>কাপড়ের দর</strong></label>
                                                        <input type="number" id="panjabirKaporerDor_`+x+`" class="punjabiPrice_`+x+` number-center form-control" name="price_panjabi_cloth[]"  value="">
                                                    </div>

                                                    <div class="col-md-3">
                                                        <label class="mt-2"><strong>এমব্রয়ডারি দর</strong></label>

                                                        <input type="number" class="number-center form-control" name="ambortari_price[]" id="panjabirAmbortari_`+x+`" value="">
                                                    </div>

                                                    <div class="col-md-3">
                                                        <label class="mt-2"><strong>এমব্রয়ডারি নম্বর </strong></label>
                                                        <input type="text" class="number-center form-control" name="punjabi_ambortari_no[]">
                                                    </div>
                                                </div><hr>`);
                 $('.single_select2').select2();

                  var site_url = $(".site_url").val();
                $("#product_entries_"+x).select2({
                    ajax: { 
                    url:  site_url + '/bills/product-list-load-bill',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                     
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                    },
                });


                $(wrapper).on("click",".remove_field", function(e)
                {
                    e.preventDefault();

                    var x = $(this).attr("data-val");

                    $('.di_'+x).remove(); x--;
                });

                punjabiGetItemPrice(x);



            }
           
        }
    </script>


     <script type="text/javascript">
        var wrapper2          = $(".input_fields_wrap2");
        var max_fields       = 500;
        var y = $('#row_numberr').val()-1;;
        var k = $('#row_numberr').val();
        function payjamaInfoAdd() 
        {

            $('#row_numberr').val(++k);

             if(y < max_fields)
            {
                y++;

                 $('.pGetMultipleRow').prepend(`<div class="row dii_`+y+`" style="padding-left: 18px;">
                                                        <div class="answer1 col-md-10">
                                                            <label class="mt-2"><strong>কাপড়ের নাম</strong></label>

                                                            <select class="form-control single_select22" id="payjama_product_entries_`+y+`" onchange="payjamaGetItemPrice(`+y+`)"name="payjama_cloth_name[]">
                                                                <option value="0">নির্বাচন করুন</option>
                                                            </select>
                                                        </div>

                                                        <div class="col-md-2" style="margin-top: 35px;">
                                                        <span  data-val="`+y+`" class="btn btn-danger remove_field2"><i class="fas fa-trash"></i></span>
                                                        </div>

                                                        <div class="answer1 col-md-3">
                                                            <label class="mt-2"><strong>পরিমান</strong></label>
                                                            <input type="number" step="0.01"  class="number-center form-control" name="amount_payjama_cloth[]" id="payjamaKaporerPorimap_`+y+`">
                                                        </div>
                                                        <div class="answer1 col-md-3">
                                                            <label class="mt-2"><strong> দর</strong></label>
                                                            <input type="number" id="payjamaKaporerDor_`+y+`" class="payjamaPrice_`+y+` number-center form-control" name="price_payjama_cloth[]"  value="">
                                                        </div>

                                                        <div class="col-md-3">
                                                            <label class="mt-2"><strong>এমব্রয়ডারি দর</strong></label>
                                                            <input type="number" class="number-center form-control" name="payjama_ambortari_price[]" id="payjamaAmbortari_`+y+`" value="">
                                                        </div>

                                                        <div class="col-md-3">
                                                            <label class="mt-2"><strong>এমব্রয়ডারি নম্বর </strong></label>
                                                            <input type="text" class="number-center form-control" name="payjama_ambortari_no[]">
                                                        </div>
                                                    </div><hr>`);
                 $('.single_select22').select2();

                  var site_url = $(".site_url").val();
                $("#payjama_product_entries_"+y).select2({
                ajax: { 
                url:  site_url + '/bills/product-list-load-bill',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

                $(wrapper2).on("click",".remove_field2", function(e)
                {
                    e.preventDefault();

                    var y = $(this).attr("data-val");

                    $('.dii_'+y).remove(); y--;
                });

                payjamaGetItemPrice(y);
            }
           
        }
    </script>




    <script type="text/javascript">
        function punjabiGetItemPrice(x)
        {
            

            var entry_id  = $("#product_entries_"+x).val();
            var site_url  = $(".site_url").val();
            if(entry_id)
            {

                $.ajax({
                    type:   'get',
                    url:    site_url + '/tailors/orders/get-punjabi-price',
                    data:   {entry_id : entry_id, _token: '{{csrf_token()}}' },
        
                    success: function (punjabidata) {
                        $(".punjabiPrice_"+x).val(punjabidata[0]['sell_price']);
                        // $(".punjabiPrice"+x).val(parseFloat(punjabidata.product_entries.sell_price).toFixed(2));


                    }
                });
            }
            
            //For getting item commission information from items table end
        }
    </script>



    <script type="text/javascript">
        function payjamaGetItemPrice(y)
        {
            var entry_id  = $("#payjama_product_entries_"+y).val();
            var site_url  = $(".site_url").val();
            if(entry_id)
            {

                $.ajax({
                    type:   'get',
                    url:    site_url + '/tailors/orders/get-payjama-price',
                    data:   {entry_id : entry_id, _token: '{{csrf_token()}}' },
        
                    success: function (payjamadata) {
                        $(".payjamaPrice_"+y).val(payjamadata[0]['sell_price']);
                    }
                });
            }
            
            //For getting item commission information from items table end
        }
    </script>

@endsection