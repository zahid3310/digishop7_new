<?php

namespace Modules\Messages\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use Validator;
use Auth;

//Models
use App\Models\Messages;
use App\Models\MessageDeliveries;
use App\Models\Customers;
use App\Models\LocalCustomer;
use App\Models\PhoneBook;
use App\Models\CustomerSMS;
use DB;
use Response;
use GuzzleHttp\Client;

class MessagesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $messages  = Messages::get();

        return view('messages::index', compact('messages'));
    }

    public function create()
    {
        return view('messages::create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'title'    => 'required',
            'body'     => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $messages                      = new Messages;
            $messages->title               = $data['title'];
            $messages->body                = $data['body'];
            $messages->created_by          = $user_id;

            if ($messages->save())
            {   
                DB::commit();
                return back()->with("success","Message Created Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        return view('messages::show');
    }

    public function edit($id)
    {
        $messages       = Messages::get();
        $find_message   = Messages::find($id);

        return view('messages::edit', compact('messages', 'find_message'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'title'    => 'required',
            'body'     => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $messages                      = Messages::find($id);
            $messages->title               = $data['title'];
            $messages->body                = $data['body'];
            $messages->updated_by          = $user_id;

            if ($messages->save())
            {   
                DB::commit();
                return redirect()->route('messages_index')->with("success","Message Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function messageSendIndex()
    {
        $messages  = Messages::get();

        return view('messages::message_send_index', compact('messages'));
    }

    public function contactListByCategory($message_id, $contact_category_id)
    {
        $user_id                = Auth::user()->id;
        $message_id             = $message_id;
        $contact_category_id    = $contact_category_id;

        if ($contact_category_id == 0 || $contact_category_id == 1 || $contact_category_id == 2)
        {

            $all_contacts       = Customers::where('contact_type', $contact_category_id )
                                            ->where('phone', '!=' , null )
                                            ->get();
        }elseif($contact_category_id == 5)
        {
             $all_contacts       = LocalCustomer::select('local_customer.*','local_customer.mobile_number as phone')
                                            ->where('mobile_number', '!=' , null )
                                            ->get();
        }
        else
        {
            $all_contacts       = PhoneBook::where('phone', '!=' , null )
                                            ->get();
        }
        
        $message_deliveries = MessageDeliveries::where('message_id', $message_id)->get();

        foreach ($all_contacts as $key => $value)
        {
            if ($contact_category_id == 0 || $contact_category_id == 1 || $contact_category_id == 2)
            {
                $send_status   = $message_deliveries->where('customer_id', $value['id'])->where('message_id', $message_id);

                if ($send_status->count() == 0)
                {
                    $contacts[]      = $value;
                }
            }
            else
            {
                $send_status   = $message_deliveries->where('phone_book_id', $value['id'])->where('message_id', $message_id);

                if ($send_status->count() == 0)
                {
                    $contacts[]      = $value;
                }
            }
        }

        $message  = Messages::find($message_id);

        $data['contacts']   = $all_contacts;
        $data['message']    = $message;

        return Response::json($data);
    }

    public function sendSMS(Request $request)
    {   
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        foreach ($data['check_box_id'] as $key => $value)
        {   
            if ($value != 0)
            {
                $mobile     = $data['phone_number'][$key];
                $message    = urlencode($data['message_body']);

                // $user       = Users::find(1);
                // $response   = Http::get('https://digishop7.com/digishop-billing/send-mim-sms/'.$user['billing_id'].'?mobile='.$mobile.'&message='.$message);
                
                $client              = new Client();
                $sms                 = $client->request("GET", "https://isms.mimsms.com/smsapi?api_key=C2001576630de671bae396.84295482&type=text&contacts=$mobile&senderid=8809601003555&msg=$message");
                

                $message_deliveries[] = [
                    'message_id'        => $data['message_id'],
                    'customer_id'       => $data['contact_category_id'] == 0 || $data['contact_category_id'] == 1 || $data['contact_category_id'] == 2 ? $data['contact_id'][$key] : null,
                    'phone_book_id'     => $data['contact_category_id'] == 3 ? $data['contact_id'][$key] : null,
                    'created_by'        => $user_id,
                    'created_at'        => date('Y-m-d H:i:s'),
                ];
            }
        }

        if (isset($message_deliveries))
        {
            DB::table('message_deliveries')->insert($message_deliveries);
        }

        return redirect()->route('messages_send_index')->with("success","SMS Send Successfully !!");
    }

    public function phoneBookIndex()
    {
        $phone_books  = PhoneBook::get();

        return view('messages::phone_book_index', compact('phone_books'));
    }

    public function phoneBookStore(Request $request)
    {
        $rules = array(
            'name'    => 'required',
            'phone'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $phone_books                   = new PhoneBook;
            $phone_books->name             = $data['name'];
            $phone_books->phone            = $data['phone'];
            $phone_books->address          = $data['address'];
            $phone_books->created_by       = $user_id;

            if ($phone_books->save())
            {   
                DB::commit();
                return back()->with("success","Contact Added Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function phoneBookEdit($id)
    {
        $phone_books        = PhoneBook::get();
        $find_phone_books   = PhoneBook::find($id);

        return view('messages::phone_book_edit', compact('phone_books', 'find_phone_books'));
    }

    public function phoneBookUpdate(Request $request, $id)
    {
        $rules = array(
            'name'    => 'required',
            'phone'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $phone_books                   = PhoneBook::find($id);
            $phone_books->name             = $data['name'];
            $phone_books->phone            = $data['phone'];
            $phone_books->address          = $data['address'];
            $phone_books->updated_by       = $user_id;

            if ($phone_books->save())
            {   
                DB::commit();
                return redirect()->route('messages_phone_book_index')->with("success","Contact Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function messageSendList()
    {
        $messages  = MessageDeliveries::leftjoin('customers', 'customers.id', 'message_deliveries.customer_id')
                                        ->leftjoin('phone_book', 'phone_book.id', 'message_deliveries.phone_book_id')
                                        ->selectRaw('message_deliveries.*, 
                                                     customers.name as customer_name, 
                                                     customers.phone as customer_phone, 
                                                     phone_book.phone as phone_phone,
                                                     phone_book.name as phone_name')
                                        ->get();

        return view('messages::delivery_message_list', compact('messages'));
    }

    public function deleteSendList()
    {
        $messages  = MessageDeliveries::where('id', '!=', 0)->delete();

        return back()->with("success","Records Deleted Successfully !!");
    }

    public function getMessage($message_id)
    {
        $user_id        = Auth::user()->id;
        $message_id     = $message_id;
        $message        = Messages::find($message_id);

        return Response::json($message);
    }
    
    public function customerSms()
    {
        $lists = CustomerSMS::all();
         return view('messages::customer_sms',compact('lists'));
    }

    public function customerSmsStore(Request $request)
    {
        $sms = new CustomerSMS;
        $sms->name = $request->name;
        $sms->phone = $request->phone;
        $sms->address = $request->address;
        $sms->entry_date = date('Y-m-d');
        $sms->created_by = Auth::user()->id;
        
        if($sms->save())
        {
            $mobile     = $request->phone;
            $message    = 'Welcom to LEBAS

                           পাঞ্জাবী তৈরির নির্ভরযোগ্য প্রতিষ্ঠান 
                           01309008300';

            $client     = new Client();
            $sms        = $client->request("GET", "esms.mimsms.com/smsapi?api_key=C2008576610f84f49764e4.05026045 &type=text&contacts=". $mobile ."&senderid=8809612436500&msg=".$message);
        }

        return back();
    }
    
     public function customerSmsList()
    {
         $lists = CustomerSMS::all();
         return view('messages::customer_sms_list',compact('lists'));
    }
}
