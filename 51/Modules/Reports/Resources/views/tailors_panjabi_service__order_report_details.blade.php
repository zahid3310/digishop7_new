@extends('layouts.app')

@section('title', 'Service Report')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Service Report</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Report</a></li>
                                    <li class="breadcrumb-item active">Service Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>



                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body table-responsive">
                                                <h4 class="card-title">Details</h4>

                                                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align:center;">SL</th>
                                                            <th style="text-align:center;">Order#</th>
                                                            <th style="text-align:center;">Order Date</th>
                                                            <th style="text-align:center;">Delivery Date</th>
                                                            <th style="text-align:center;">Customer</th>
                                                            <th style="text-align:center;">Phone</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach($order as $key=> $value)
                                                        <tr>
                                                            <td style="text-align: center;">{{ $loop->index+1 }}</td>
                                                            <td style="text-align: center;">{{ 'OR - ' . str_pad($value['order_number'], 6, "0", STR_PAD_LEFT) }}</td>
                                                            <td style="text-align: center;">{{ $value['order_date'] }}</td>
                                                            <td style="text-align: center;">{{ $value['delivery_date'] }}</td>
                                                            <td style="text-align: center;">{{ $value['customer_name'] }}</td>
                                                            <td style="text-align: center;">{{ $value['phone'] }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>

    		</div>
		</div>
    </div>

@endsection

@section('scripts')

@endsection