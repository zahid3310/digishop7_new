<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Cobardia (firebrick)</title>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="Invoicebus Invoice Template">
    <meta name="author" content="Invoicebus">

    <meta name="template-hash" content="baadb45704803c2d1a1ac3e569b757d5">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>

    <style type="text/css">
      body {
    background: #f8f9fa;
    margin-top: 20px;
    margin-bottom: 20px;
}
.my_row{
    margin-right: 0px!important;
    margin-left: 0px!important;
}
.print-button
{
    color: #26b;
    border-color: #26b;
    background-color: transparent;
    border: 2px solid #26b;
    font-weight: bold;
    cursor: pointer;
}

.print-button:hover
{
    color: white;
    border-color: #26b;
    background-color: #26b;
    border: 2px solid #26b;
    font-weight: bold;
    cursor: pointer;
}

    </style>
  </head>
  <body>
    
<div class="container">
    
    <div class="row my_row">

        <div class="col-12">
            <div class="card">

                <div class="card-body p-0">

                    <div class="row p-2 my_row">
                        <div class="col-md-6" style="padding-top: 15px">
                            <img src="{{ url('public/'.userDetails()->logo) }}" width="200" height="50" >
                        </div>

                        <div class="col-md-6 text-right">
                            <button class="print-button" id="togglee"><i class="fas fa-print"></i></button>
                            <p class="font-weight-bold mb-1">Order Number #{{$orderDetails[0]->order_number}}</p>
                            <p class="text-muted">Order Date: {{$orderDetails[0]->order_date}}</p>
                        </div>
                    </div>

                    <hr class="my-1">

                    <div class="row pb-2 p-2 my_row">
                        <div class="col-md-6">
                            <p class="font-weight-bold mb-2">Customer Information</p>
                            <p class="mb-1">{{$orderDetails[0]->name}}</p>
                            <span>Phone: {{$orderDetails[0]->phone}}</span><br>
                            <span>Email: {{$orderDetails[0]->email}}</span>
                            <p class="mb-1">Address: {{$orderDetails[0]->address}}</p>

                        </div>

                        <!-- <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-4">Payment Details</p>
                            <p class="mb-1"><span class="text-muted">VAT: </span> 1425782</p>
                            <p class="mb-1"><span class="text-muted">VAT ID: </span> 10253642</p>
                            <p class="mb-1"><span class="text-muted">Payment Type: </span> Root</p>
                            <p class="mb-1"><span class="text-muted">Name: </span> John Doe</p>
                        </div> -->
                    </div>

                    <div class="row p-2 my_row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="border-0 text-uppercase small font-weight-bold">SL</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Product Name</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Price</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($productDetails as $item)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->quantity}}</td>
                                        <td>{{number_format((float)$item->rate, 2, '.', '')}}</td>
                                        <?php $price = number_format((float)$item->rate, 2, '.', '') * $item->quantity; ?>
                                        <td>{{number_format((float)$price, 2, '.', '')}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="d-flex flex-row-reverse bg-dark text-white p-2">
                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Grand Total</div>
                            <div class="h2 font-weight-light">

                                <?php $grandTotal = $orderDetails[0]->order_amount - (($orderDetails[0]->order_amount * $orderDetails[0]->total_discount)/100);
                                    echo number_format((float)$grandTotal, 2, '.', '');
                                 ?>
                                
                            </div>
                        </div>

                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Discount</div>
                            <div class="h2 font-weight-light">
                                <?php 

                                if ($orderDetails[0]->total_discount == null) {
                                   echo number_format((float)0, 2, '.', '');
                                }else{
                                    
                                    echo number_format((float)$orderDetails[0]->total_discount, 2, '.', '');
                                } 
                                ?>
                            </div>
                        </div>

                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Sub Total amount</div>
                            <div class="h2 font-weight-light">{{number_format((float)$orderDetails[0]->order_amount, 2, '.', '')}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script type="text/javascript">
    $('.print-button').click(function() {
        document.getElementById('togglee').style.visibility = 'hidden';
        window.print();
        location.reload();
});
</script>

  </body>
</html>
