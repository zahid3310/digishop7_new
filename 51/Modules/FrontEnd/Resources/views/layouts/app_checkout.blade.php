<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> @yield('title')</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png">

    <!-- All CSS is here
    ============================================ -->

    <script>
        WebFontConfig = {
            google: { families: ['Open+Sans:400,600,700', 'Poppins:400,600,700'] }
        };
        (function (d) {
            var wf = d.createElement('script'), s = d.scripts[0];
            wf.src = 'js/webfont.js';
            wf.async = true;
            s.parentNode.insertBefore(wf, s);
        })(document);
    </script>

    <link rel="stylesheet" type="text/css" href="{{ url('public/front_end_asset/vendor/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/front_end_asset/vendor/animate/animate.min.css') }}">

    <!-- Plugins CSS File -->
    <!-- <link rel="stylesheet" type="text/css" href="vendor/magnific-popup/magnific-popup.min.css"> -->
    <link rel="stylesheet" type="text/css" href="{{ url('public/front_end_asset/vendor/owl-carousel/owl.carousel.min.css') }}">

    <!-- Main CSS File -->
    <link rel="stylesheet" type="text/css" href="{{ url('public/front_end_asset/css/demo1.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/front_end_asset/css/demo3.min.css') }}">
</head>

<body class="home">
    <div class="page-wrapper">
        @include('frontend::layouts.header')
        @yield('main_body')
        @include('frontend::layouts.footer')
    </div>
    <!-- Sticky Footer -->
    <div class="sticky-footer sticky-content fix-bottom">
        <a href="demo3.html" class="sticky-link active">
            <i class="d-icon-home"></i>
            <span>Home</span>
        </a>
        <a href="demo3-shop.html" class="sticky-link">
            <i class="d-icon-volume"></i>
            <span>Categories</span>
        </a>
        <a href="wishlist.html" class="sticky-link">
            <i class="d-icon-heart"></i>
            <span>Wishlist</span>
        </a>
        <a href="account.html" class="sticky-link">
            <i class="d-icon-user"></i>
            <span>Account</span>
        </a>
        <div class="dropdown cart-dropdown dir-up">
            <a href="cart.html" class="sticky-link cart-toggle">
                <i class="d-icon-bag"></i>
                <span>Cart</span>
            </a>
            <!-- End of Cart Toggle -->
            <div class="dropdown-box">
                <div class="product product-cart-header">
                    <span class="product-cart-counts">2 items</span>
                    <span><a href="cart.html">View cart</a></span>
                </div>
                <div class="products scrollable">
                    <div class="product product-cart">
                        <div class="product-detail">
                            <a href="product.html" class="product-name">Solid Pattern In Fashion Summer Dress</a>
                            <div class="price-box">
                                <span class="product-quantity">1</span>
                                <span class="product-price">$129.00</span>
                            </div>
                        </div>
                        <figure class="product-media">
                            <a href="#">
                                <img src="images/cart/product-1.jpg" alt="product" width="90"
                                    height="90" />
                            </a>
                            <button class="btn btn-link btn-close">
                                <i class="fas fa-times"></i>
                            </button>
                        </figure>
                    </div>
                    <!-- End of Cart Product -->
                    <div class="product product-cart">
                        <div class="product-detail">
                            <a href="product.html" class="product-name">Mackintosh Poket Backpack</a>
                            <div class="price-box">
                                <span class="product-quantity">1</span>
                                <span class="product-price">$98.00</span>
                            </div>
                        </div>
                        <figure class="product-media">
                            <a href="#">
                                <img src="images/cart/product-2.jpg" alt="product" width="90"
                                    height="90" />
                            </a>
                            <button class="btn btn-link btn-close">
                                <i class="fas fa-times"></i>
                            </button>
                        </figure>
                    </div>
                    <!-- End of Cart Product -->
                </div>
                <!-- End of Products  -->
                <div class="cart-total">
                    <label>Subtotal:</label>
                    <span class="price">$42.00</span>
                </div>
                <!-- End of Cart Total -->
                <div class="cart-action">
                    <a href="checkout.html" class="btn btn-dark"><span>Checkout</span></a>
                </div>
                <!-- End of Cart Action -->
            </div>
            <!-- End of Dropdown Box -->
        </div>
    </div>
    <!-- Scroll Top -->
    <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="fas fa-chevron-up"></i></a>

    @include('frontend::layouts.mobile_menu')

    <!-- Plugins JS File -->
    <script src="{{ url('public/front_end_asset/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('public/front_end_asset/vendor/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ url('public/front_end_asset/vendor/elevatezoom/jquery.elevatezoom.min.js') }}"></script>
    <!-- <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script> -->

    <script src="{{ url('public/front_end_asset/vendor/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ url('public/front_end_asset/vendor/sticky/sticky.min.js') }}"></script>
    <!-- Main JS File -->
    <script src="{{ url('public/front_end_asset/js/main.js') }}"></script>

    @yield('scripts')

    @stack('scripts')
    <input type="hidden" class="site_url" value="{{ url('/') }}" />
    
</body>

</html>