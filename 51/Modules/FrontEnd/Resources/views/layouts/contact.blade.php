@extends('frontend::layouts.app')

@section('main_body')
<main class="main">
            <div class="page-content mt-6 pt-2 pb-6">
                <section class="contact-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-xs-5 ls-m pt-3">
                                <h2 class="font-weight-bold text-uppercase ls-m mb-2">Contact us</h2>
                                <p>Looking for help? Fill the form and start a new adventure.</p>

                                <h4 class="mb-1 text-uppercase">Address</h4>
                                <p>35,Chayanir,Lamabazar point,Sylhet</p>

                                <h4 class="mb-1 text-uppercase">Phone</h4>
                                <p><a href="tel:#">01715262667</a></p>

                                <h4 class="mb-1 text-uppercase">Support</h4>
                                <p>
                                    <a href="#">sylhetibazarsb@gmail.com</a>
                                </p>
                            </div>
                            <div class="col-lg-9 col-md-8 col-xs-7">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3619.0796094901234!2d91.85803461432079!3d24.895265999994237!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3750552d4857cc8f%3A0xed3b16a36acb4322!2sLamabazar%2C%20Sylhet!5e0!3m2!1sen!2sbd!4v1623239852207!5m2!1sen!2sbd" width="100%" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End About Section-->
            </div>
        </main>
@endsection