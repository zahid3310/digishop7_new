<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>





             @if(Auth::user()->role != 6 )
                <li>
                    <a href="{{ route('home') }}" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                
                @endif
                
                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-shopping-basket"></i><span>Corporate Order</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('tailors_corporate_order_create') }}">Take Order</a> </li>
                        <li> <a href="{{ route('tailors_corporate_order_index') }}">List of Orders</a> </li>
                        <li> <a href="{{ route('tailors_corporate_due_list') }}">List of Dues</a> </li>
                        <li> <a href="{{ route('tailors_corporate_trnsFactory_lists') }}">List of Cutting</a> </li>
                        <li> <a href="{{ route('tailors_corporate_sewing_list') }}">List of Sewing</a> </li>
                        <li> <a href="{{ route('tailors_corporate_receFactory_lists') }}">List of Recv. From Sewing</a> </li>
                        <li> <a href="{{ route('tailors_corporate_delivered_list') }}">List of Delivered</a> </li>
                    </ul>
                </li>

                 
                 @if(Auth::user()->collect_number == 0)
                 <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-phone"></i><span>Collect Number </span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('customer_sms') }}">Add Number</a> </li>
                        <li> <a href="{{ route('customer_sms_list') }}">List Number</a> </li>
                    </ul>
                </li>
                @endif
                
                @if(Auth::user()->role != 6 )
                @if(Auth::user()->role != 5 )
                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-shopping-basket"></i><span>Order Management</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('tailors_order_create') }}">Take Order</a> </li>
                        <li> <a href="{{ route('tailors_order_index') }}">List of Orders</a> </li>
                        <li> <a href="{{ route('tailors_due_list') }}">List of Dues</a> </li>
                        <li> <a href="{{ route('tailors_trnsFactory_lists') }}">List of Cutting</a> </li>
                        <li> <a href="{{ route('tailors_sewing_list') }}">List of Sewing</a> </li>
                        <li> <a href="{{ route('tailors_receFactory_lists') }}">List of Recv. From Sewing</a> </li>
                        <li> <a href="{{ route('tailors_delivered_list') }}">List of Delivered</a> </li>
                        <li> <a href="{{ route('tailors_service_index') }}">List of Punjabi Services</a> </li>
                        <li> <a href="{{ route('tailors_pajamas_service_index') }}">List of Pajamas Services</a> </li>
                        <li> <a href="{{ route('tailors_order_cancel') }}">Order Cancel</a> </li>
                    </ul>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-shopping-basket"></i><span>Sales</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('invoices_index') }}">Daily Sales</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' }}" href="{{ route('invoices_all_sales') }}">List of Sales</a> </li>
                        <li> <a class="{{ Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' }}"  href="{{ route('sales_return_index') }}">Sales Return</a> </li>
                        <li> <a class="{{ Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' }}" href="{{ route('payments_create').'?payment_type=0' }}">Invoice Wise Collection</a> </li>
                    </ul>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-suitcase"></i><span>Purchase</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('bills_index') }}">Purchase Item</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' }}" href="{{ route('bills_all_bills') }}">List of Purchase</a> </li>
                        <li> <a class="{{ Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' }}" href="{{ route('purchase_return_index') }}">Purchase Return</a> </li>
                        <li> <a class="{{ Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' }}" href="{{ route('payments_create').'?payment_type=1' }}">Bill Wise Payment</a> </li>
                    </ul>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fab fa-adn"></i><span>Accounts</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' }}" href="{{ route('expenses_index') }}">Expenses</a> </li>

                        <li> <a class="{{ 
                            Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' }}" href="{{ route('incomes_index') }}">Incomes</a> </li>

                        <li> <a class="{{ 
                            Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : '' }}" href="{{ route('balance_transfer_index') }}">Balance Transfer</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="far fa-envelope"></i><span>Messaging</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('messages_send_index') }}">Send Message</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>Reports</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        
                        <li class="">
                            <a class="has-arrow waves-effect">
                                Orders
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('tailors_work_history') }}">Work Histroy</a> </li>
                                <li> <a href="{{ route('tailors_order_report_index') }}">Order Report</a> </li>
                                <li> <a href="{{ route('tailors_payment_history') }}">Payment History</a> </li>
                                <li> <a href="{{ route('tailors_service_order_report') }}">Service Order Reports</a> </li>
                            </ul>
                        </li> 
                        
                        <li class="">
                            <a class="has-arrow waves-effect">
                                Sales
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('sales_statement_index') }}">Statement of Sales</a> </li>
                                <li> <a href="{{ route('sales_summary_index') }}">Sales Summary</a> </li>
                                <li> <a href="{{ route('product_wise_sales_report_index') }}">Product Wise Sales</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Purchase
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('purchase_statement_index') }}">Statement of Pur.</a> </li>
                                <li> <a href="{{ route('purchase_summary_index') }}">Purchase Summary</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Accounts
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('expense_report_index') }}">List of Expense</a> </li>
                                <li> <a href="{{ route('income_report_index') }}">List of Income</a> </li>
                                <li> <a href="{{ route('income_statement_index') }}">Income Statement</a> </li>
                                <!-- <li> <a href="{{ route('collection_report_index') }}">Daily Report</a> </li> -->
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Payments
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('customer_payment_report_index') }}">Customer Payments</a> </li>
                                <li> <a href="{{ route('supplier_payment_report_index') }}">Supplier Payments</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                MIS
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('current_balance_index') }}">Current Balance</a> </li>
                                <li> <a href="{{ route('stock_report_index') }}">Stock Status</a> </li>
                                <li> <a href="{{ route('due_report_customer_index') }}">Buyer Ledger</a> </li>
                                <li> <a href="{{ route('due_report_supplier_index') }}">Supplier Ledger</a> </li>
                                <li> <a href="{{ route('due_ledger_index') }}">Due Ledger</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Basic Report
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('item_list_index') }}">List of Items</a> </li>
                                <li> <a href="{{ route('emergency_item_list_index') }}">Emergency Purchase</a> </li>
                                <li> <a href="{{ route('product_suppliers_index') }}">Item Wise Supplier</a> </li>
                                <li> <a href="{{ route('product_customers_index') }}">Item Wise Customer</a> </li>
                                <li> <a href="{{ route('register_list_index').'?type=0' }}" target="_blank">Buyer List</a> </li>
                                <li> <a href="{{ route('register_list_index').'?type=1' }}" target="_blank">Supplier List</a> </li>
                            </ul>
                        </li>
                        
                        <!-- <li> <a href="#">List of Sending SMS</a> </li>
                        <li> <a href="{{ route('salary_report_index') }}">Salary Report</a> </li> -->

                    </ul>
                </li>

                <li class="{{ 
                Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>Basic Settings</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">

                        <li class="{{ Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Product
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="" href="{{ route('products_index_all') }}">List Of Products</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' }}" href="{{ route('products_index') }}">Add Product</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }}" href="{{ route('products_category_index') }}">Add Categories</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' }}" href="{{ route('categories_index') }}" href="{{ route('categories_index') }}">Brand/Manufacturer</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' }}" href="{{ route('products_units_index') }}">Add Unit Measure</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' }}" href="{{ route('products_variations_index') }}">Product Variations</a> </li>
                                <li> <a href="{{ route('products_barcode_print') }}">Print Barcode</a> </li>
                                <li> <a href="{{ route('products_opening_stock') }}">Bulk Opening Stock</a> </li>
                                <li> <a href="{{ route('products_bulk_product_list_update') }}">Bulk Product Update</a> </li>
                            </ul>
                        </li>
                        
                         <li class="{{ Route::currentRouteName() == 'add_punjabi_desc' ? 'mm-active' : '' ||  Route::currentRouteName() == 'punjabi_description_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'add_karigor' ? 'mm-active' : '' || Route::currentRouteName() == 'karigor_info_edit' ? 'mm-active' : ''}}">
                            <a class="{{ Route::currentRouteName() == 'add_punjabi_desc' ? 'mm-active' : ''}} has-arrow waves-effect">
                                Order Management
                            </a>
                            <ul class="sub-menu" aria-expanded="false">

                                <li> <a class="{{ Route::currentRouteName() == 'add_karigor' ? 'mm-active' : '' || Route::currentRouteName() == 'karigor_info_edit' ? 'mm-active' : ''}}" href="{{route('add_karigor')}}">Add Karigor</a> </li>

                                <li> <a class="{{ Route::currentRouteName() == 'add_embroidery' ? 'mm-active' : '' || Route::currentRouteName() == 'karigor_info_edit' ? 'mm-active' : ''}}" href="{{route('add_embroidery')}}">Add Punjabi Embroidery</a> </li>

                                 <li> <a class="{{ Route::currentRouteName() == 'add_payjama_embroidery' ? 'mm-active' : '' || Route::currentRouteName() == 'karigor_info_edit' ? 'mm-active' : ''}}" href="{{route('add_payjama_embroidery')}}">Add Payjama Embroidery</a> </li>

                                <li> <a class="{{ Route::currentRouteName() == 'add_punjabi_desc' ? 'mm-active' : '' ||  Route::currentRouteName() == 'punjabi_description_edit' ? 'mm-active' : ''}}" href="{{route('add_punjabi_desc')}}">Add Punjabi Descc.</a> </li>


                                 <li> <a class="{{ Route::currentRouteName() == 'add_payjama_desc' ? 'mm-active' : '' ||  Route::currentRouteName() == 'payjama_description_edit' ? 'mm-active' : ''}}" href="{{route('add_payjama_desc')}}">Add Payjama Desc.</a> </li>

                       

                                <li> <a class="{{ Route::currentRouteName() == 'add_punjabi_clothes' ? 'mm-active' : ''}}" href="{{route('add_punjabi_clothes')}}">Add Punjabi Clothes</a> </li>

                                <li> <a class="{{ Route::currentRouteName() == 'add_payjama_clothes' ? 'mm-active' : ''}}" href="{{route('add_payjama_clothes')}}">Add Payjama Clothes</a> </li>

                            </ul>
                        </li>

                        <li class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' }}">
                            <a class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Registers
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=0' }}">Add Buyer/Customer</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=1' }}">Add Supplier</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=2' }}">Add Employee</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=3' }}">Add Reference</a> </li>
                            </ul>
                        </li>

                        <li class="{{ Route::currentRouteName() == 'paid_through_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'paid_through_accounts_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'paid_through_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'paid_through_accounts_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Accounts
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Route::currentRouteName() == 'paid_through_accounts_edit' ? 'mm-active' : '' }}" href="{{ route('paid_through_accounts_index') }}">Paid Through</a> </li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow waves-effect">
                                Messaging
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('messages_index') }}">Create Text Message</a> </li>
                                <li> <a href="{{ route('messages_phone_book_index') }}">Phone Book</a> </li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow waves-effect">
                                Security System
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('users_index') }}">Add User</a> </li>
                                <li> <a href="{{ route('users_index_all') }}">List of User</a> </li>
                                <li> <a href="{{ route('set_access_index') }}">Set Permission</a> </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                
               @endif

               @endif
            </ul>
        </div>
    </div>
</div>