@extends('layouts.app')

@section('title', 'Transaction History')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Software Billing</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Pay Bill</a></li>
                                    <li class="breadcrumb-item active">Transaction History</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;width: 5%">SL</th>
                                            <th style="text-align: center;width: 10%">Date</th>
                                            <th style="text-align: center;width: 30%">Particular</th>
                                            <th style="text-align: center;width: 10%">Paid Through</th>
                                            <th style="text-align: center;width: 15%">Transaction#</th>
                                            <th style="text-align: center;width: 10%">Payable</th>
                                            <th style="text-align: center;width: 10%">Paid</th>
                                            <th style="text-align: center;width: 10%">Balance</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $balance = 0; ?>
                                        @if(count($data) > 0)
                                        @foreach($data as $key => $value)
                                        <?php
                                            if($value->transaction_head == 'payment-receive')
                                            {
                                                $particular = $value->note;
                                                $payable    = 0;
                                                $paid       = $value->amount;
                                                $balance    = $balance - $paid;
                                            }
                                            elseif($value->transaction_head == 'one-time-payment' || $value->transaction_head == 'service-charge' || $value->transaction_head == 'sms-purchase')
                                            {
                                                $particular = $value->note;
                                                $payable    = $value->amount;
                                                $paid       = 0;
                                                $balance    = $balance + $payable;
                                            }
                                        ?>
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ date('d-m-Y', strtotime($value->date)) }}</td>
                                            <td>{{ $particular }}</td>
                                            <td>{{ $value->account_name }}</td>
                                            <td>{{ $value->transaction_number }}</td>
                                            <td style="text-align: right">{{ number_format($payable,0,'.',',') }}</td>
                                            <td style="text-align: right">{{ number_format($paid,0,'.',',') }}</td>
                                            <td style="text-align: right">{{ number_format($balance,0,'.',',') }}</td>
                                        </tr>
                                        @endforeach
                                        @endif
                                        
                                        <tr>
                                            <td style="text-align: right" colspan="7"><strong>Total @if($balance > 0) Due @elseif($balance < 0) Advance @else Balance @endif</strong></td>
                                            <td style="text-align: right"><strong>{{ number_format($balance,0,'.',',') }}</strong></td>
                                        </tr>
                                    </tbody>
                                    
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection