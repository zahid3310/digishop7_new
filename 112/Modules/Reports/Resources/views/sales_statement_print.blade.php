<!DOCTYPE html>
<html>

<head>
    <title>Statement of Sales</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">
</head>

<style type="text/css">        
    @page {
        size: auto;   
        margin: 15mm 5mm 5mm 10mm;
    }

    #print-footer {
        display: none;
        position: fixed;
        bottom: 0;
        left:0;
    }

    #hide-print {
        display: none;
    }

    #print-footer {
        display: none;
    }

    body {
        margin-bottom:10mm;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #ddd;
    }

    .table td, .table th {
        font-size: 12px !important;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">{{__('messages.sales_statement')}}</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">{{__('messages.from_date')}}</th>
                                    <th style="text-align: center">{{__('messages.item_category')}}</th>
                                    <th style="text-align: center">{{__('messages.sales_type')}}</th>
                                    <th style="text-align: center">{{__('messages.sales_by')}}</th>
                                    <th style="text-align: center">{{__('messages.sales_id')}}</th>
                                    <th style="text-align: center">{{__('messages.customer_name')}}</th>
                                    <th style="text-align: center">{{__('messages.reference')}}</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>{{__('messages.to')}}</strong> {{ $to_date }}</td>
                                    <td style="text-align: center">
                                        @if($item_category_name != null)
                                            {{ $item_category_name['name'] }}
                                        @else
                                            {{__('messages.all')}}
                                        @endif
                                    </td>

                                    <td style="text-align: center">
                                        @if($sales_return == 0)
                                            {{__('messages.all')}}
                                        @endif

                                        @if($sales_return == 1)
                                            Sales
                                        @endif

                                        @if($sales_return == 2)
                                            Sales Return
                                        @endif
                                    </td>

                                    <td style="text-align: center">
                                        @if($sales_by_name != null)
                                            {{ $sales_by_name['name'] }}
                                        @else
                                            {{__('messages.all')}}
                                        @endif
                                    </td>

                                    <td style="text-align: center">
                                        @if($sales_id != null)
                                            {{ 'INV - ' . str_pad($sales_id['invoice_number'], 6, "0", STR_PAD_LEFT) }}
                                        @else
                                            {{__('messages.all')}}
                                        @endif
                                    </td>

                                    <td style="text-align: center">
                                        @if($customer_name != null)
                                            {{ $customer_name['name'] }}
                                        @else
                                            {{__('messages.all')}}
                                        @endif
                                    </td>

                                    <td style="text-align: center">
                                        @if($reference != null)
                                            {{ $reference['name'] }}
                                        @else
                                            {{__('messages.all')}}
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">{{__('messages.sl')}}</th>
                                    <th style="text-align: center;width: 6%">{{__('messages.T/date')}}</th>
                                    <th style="text-align: center;width: 7%">{{__('messages.sales_id')}}</th>
                                    <th style="text-align: center;width: 6%">{{__('messages.item_code')}}</th>
                                    <th style="text-align: center;width: 11%">{{__('messages.name')}}</th>
                                    <th style="text-align: center;width: 4%">{{__('messages.u/m')}}</th>
                                    <th style="text-align: center;width: 4%">{{__('messages.qty')}}</th>
                                    <th style="text-align: center;width: 4%">{{__('messages.unit_price')}}</th>
                                    <th style="text-align: center;width: 5%">{{__('messages.T/price')}}</th>
                                    <th style="text-align: center;width: 4%">{{__('messages.discount')}}</th>
                                    <th style="text-align: center;width: 6%">{{__('messages.T/payable')}}</th>
                                    <th style="text-align: center;width: 4%">{{__('messages.vat')}}</th>
                                    <th style="text-align: center;width: 4%">{{__('messages.T/discount')}}</th>
                                    <th style="text-align: center;width: 4%">{{__('messages.N/payable')}}</th>
                                    <th style="text-align: center;width: 4%">{{__('messages.paid')}}</th>
                                    <th style="text-align: center;width: 4%">{{__('messages.due')}}</th>
                                    <th style="text-align: center;width: 4%">Profit</th>
                                    <th style="text-align: center;width: 4%">Loss</th>
                                    <th style="text-align: center;width: 10%">{{__('messages.customer_name')}}</th>
                                    <th style="text-align: center;width: 2%">{{__('messages.type')}}</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
        
                                <?php
                                    $i                  = 1;
                                    $total_t_price      = 0;
                                    $total_discount     = 0;
                                    $total_payable      = 0;
                                    $total_vat          = 0;
                                    $total_discount_t   = 0;
                                    $total_net_payable  = 0;
                                    $total_paid         = 0;
                                    $total_due          = 0;
                                    $total_profit       = 0;
                                    $total_loss         = 0;
                                ?>
                                @foreach($data as $key => $value)
                                @if(isset($value->invoiceEntries[0]) || isset($value->salesReturnEntries[0]))
                                <?php
                                    if (isset($value->invoiceEntries[0]['discount_type']))
                                    {
                                        if ($value->invoiceEntries[0]['discount_type'] == 0)
                                        {
                                            $discountAmount0   = ($value->invoiceEntries[0]['rate'] * $value->invoiceEntries[0]['quantity'] * $value->invoiceEntries[0]['total_discount'])/100;
                                        }
                                        else
                                        {
                                            $discountAmount0   = $value->invoiceEntries[0]['total_discount'];
                                        }
                                    }
                                    else
                                    {
                                        $discountAmount0  = 0;
                                    }

                                    if (isset($value->invoiceEntries[0]))
                                    {
                                        $rowspan                = $value->invoiceEntries->count();
                                        $vat                    = $value['vat_type'] == 0 ? $value['total_vat'] : $value['vat'];
                                        $net_payable            = $value['invoice_amount'] + $vat ;
                                        $total_buy_price        = $value['total_buy_price'];
                                        $profit_loss            = $net_payable - $value['total_buy_price'];
                                    }
                                    else
                                    {
                                        $rowspan                = $value->salesReturnEntries->count();
                                        $vat                    = $value['vat_type'] == 0 ? $value['total_vat'] : $value['vat'];
                                        $net_payable            = $value['return_amount'] + $vat;
                                        $total_buy_price        = 0;
                                        $profit_loss            = 0;
                                    }
                                ?>
                                
                                

                                <tr>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $rowspan }}">{{ $i }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $rowspan }}">{{ isset($value['invoice_date']) ? $value['invoice_date'] : $value['sales_return_date'] }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $rowspan }}">
                                        @if($value['type'] ==1)
                                        <a href="{{ route('invoices_show', $value['id']) }}" target="_blank">
                                        {{ isset($value['invoice_number']) ? $value['invoice_number'] : $value['sales_return_number'] }}
                                        </a>
                                        @else
                                        <a href="{{ route('sales_return_show', $value['id']) }}" target="_blank">
                                        {{ isset($value['invoice_number']) ? $value['invoice_number'] : $value['sales_return_number'] }}
                                        </a>
                                        @endif

                                    <td style="text-align: center;">{{ isset($value->invoiceEntries[0]) ? str_pad($value->invoiceEntries[0]->productEntries->product_code, 6, "0", STR_PAD_LEFT) : str_pad($value->salesReturnEntries[0]->productEntries->product_code, 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: left;">{{ isset($value->invoiceEntries[0]) ? $value->invoiceEntries[0]->productEntries->name : $value->salesReturnEntries[0]->productEntries->name }}</td>
                                    <td style="text-align: center;">{{ isset($value->invoiceEntries[0]) ? $value->invoiceEntries[0]->convertedUnit->name : $value->salesReturnEntries[0]->convertedUnit->name }}</td>
                                    <td style="text-align: center;">{{ isset($value->invoiceEntries[0]) ? $value->invoiceEntries[0]['quantity'] : $value->salesReturnEntries[0]['quantity'] }}</td>
                                    <td style="text-align: right;">{{ isset($value->invoiceEntries[0]) ? $value->invoiceEntries[0]['rate'] : $value->salesReturnEntries[0]['rate'] }}</td>
                                    <td style="text-align: right;">{{ isset($value->invoiceEntries[0]) ? round($value->invoiceEntries[0]['rate'] * $value->invoiceEntries[0]['quantity'], 2) : round($value->salesReturnEntries[0]['rate'] * $value->salesReturnEntries[0]['quantity'], 2) }}</td>
                                    <td style="text-align: right;">{{ $discountAmount0 }}</td>
                                    <td style="text-align: right;">{{  isset($value->invoiceEntries[0]) ? ($value->invoiceEntries[0]['rate'] * $value->invoiceEntries[0]['quantity']) - $discountAmount0 : ($value->salesReturnEntries[0]['rate'] * $value->salesReturnEntries[0]['quantity']) - $discountAmount0 }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $rowspan }}">{{ $vat }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $rowspan }}" class="TDiscount">
                                        @if(isset($value['total_discount_type']))
                                            {{ $value['total_discount_type'] == 0 ? $value['total_discount_perc'] : $value['total_discount_amount']  }}
                                        @else
                                            0
                                        @endif
                                    </td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $rowspan }}">{{ round($net_payable, 2) }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $rowspan }}">{{ isset($value->invoiceEntries[0]) ? round($value['cash_given'], 2) : round($value['return_amount'], 2) }}</td>
                                    <?php
                                        $profitLoss = profitLoss($value->id);
                                        
                                        if($profitLoss['profit_loss'] > 0)
                                        {
                                            // $total_profit = $total_profit + $profitLoss;
                                            $profit       = $profitLoss['profit_loss'];
                                            $per_profit   = $profitLoss['per_profit_loss'];
                                            $loss         = 0;
                                            $per_loss     = 0;
                                        }
                                        else
                                        {
                                            // $total_loss = $total_loss + abs($profitLoss);
                                            $loss       = abs($profitLoss['profit_loss']);
                                            $per_loss   = abs($profitLoss['per_profit_loss']);
                                            $profit     = 0;
                                            $per_profit = 0;
                                        }
                                    ?>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $rowspan }}" class="due">{{ ($net_payable -  $value['cash_given'])}}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $rowspan }}" class="pr">{{ round($profit)}} <span>(<span class="profitP">{{ round($per_profit) }}</span>%)</span></td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $rowspan }}" class="lss">{{ round($loss).'('.round($per_loss).'%'.')' }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $rowspan }}">{{ $value->customer->name }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $rowspan }}">{{ isset($value['type']) ? 'S' : 'R' }}</td>
                                </tr>
                                 
                                <?php
                                    $sub_total_t_price      = 0;
                                    $sub_total_discount     = 0;
                                    $sub_total_payable      = 0;
                                ?>

                                @if(isset($value->invoiceEntries))
                                    @foreach($value->invoiceEntries as $key1 => $value1)

                                    <?php
                                        if (isset($value1['discount_type']))
                                        {
                                            if ($value1['discount_type'] == 0)
                                            {
                                                $discountAmount   = ($value1['rate'] * $value1['quantity'] * $value1['discount_amount'])/100;
                                            }
                                            else
                                            {
                                                $discountAmount   = $value1['discount_amount'];
                                            }
                                        }
                                        else
                                        {
                                            $discountAmount   = 0;
                                        }

                                        $sub_total_t_price  = $sub_total_t_price + ($value1['rate'] * $value1['quantity']);
                                        $sub_total_discount = $sub_total_discount + $discountAmount;
                                        $sub_total_payable  = $sub_total_payable + (($value1['rate'] * $value1['quantity']) - $discountAmount);
                                    ?>

                                    @if($key1 != 0)

                                    <tr>
                                        <td style="text-align: center;">{{ str_pad($value1->productEntries->product_code, 6, "0", STR_PAD_LEFT) }}</td>
                                        <td style="text-align: left;">{{ $value1->productEntries->name }}</td>
                                        <td style="text-align: center;">{{ $value1['unit_name'] }}</td>
                                        <td style="text-align: center;">{{ $value1['quantity'] }}</td>
                                        <td style="text-align: right;">{{ $value1['rate'] }}</td>
                                        <td style="text-align: right;">{{ round($value1['rate'] * $value1['quantity'], 2) }}</td>
                                        <td style="text-align: right;">
                                            <?php
                                                if ($value1['discount_type'] == 0)
                                                {
                                                    $discountAmount   = ($value1['rate'] * $value1['quantity'] * $value1['discount_amount'])/100;
                                                }
                                                else
                                                {
                                                    $discountAmount   = $value1['discount_amount'];
                                                }
                                            ?>

                                            {{ $discountAmount }}
                                        </td>
                                        <td style="text-align: right;">{{ ($value1['rate'] * $value1['quantity']) - $discountAmount }}</td>
                                    </tr>

                                    @endif
                                    @endforeach

                                    <?php
                                        $i++;

                                        $total_t_price      = $total_t_price + $sub_total_t_price;
                                        $total_discount     = $total_discount + $sub_total_discount;
                                        $total_payable      = $total_payable + $sub_total_payable;
                                        $total_vat          = $total_vat +  $vat;
                                        $total_discount_t   = $total_discount_t + $discountAmount0;
                                        $total_net_payable  = $total_net_payable + $net_payable;
                                        $total_paid         = $total_paid + $value['cash_given'];
                                    ?>
                                @else
                                    @foreach($value->salesReturnEntries as $key2 => $value2)

                                    <?php
                                        if (isset($value2['discount_type']))
                                        {
                                            if ($value2['discount_type'] == 0)
                                            {
                                                $discountAmount   = ($value2['rate'] * $value2['quantity'] * $value2['discount_amount'])/100;
                                            }
                                            else
                                            {
                                                $discountAmount   = $value2['discount_amount'];
                                            }
                                        }
                                        else
                                        {
                                            $discountAmount   = 0;
                                        }

                                        $sub_total_t_price  = $sub_total_t_price + ($value2['rate'] * $value2['quantity']);
                                        $sub_total_discount = $sub_total_discount + $discountAmount;
                                        $sub_total_payable  = $sub_total_payable + (($value2['rate'] * $value2['quantity']) - $discountAmount);
                                    ?>

                                    @if($key2 != 0)

                                    <tr>
                                        <td style="text-align: center;">{{ str_pad($value2->productEntries->product_code, 6, "0", STR_PAD_LEFT) }}</td>
                                        <td style="text-align: left;">{{ $value2->productEntries->name }}</td>
                                        <td style="text-align: center;">{{ $value2['unit_name'] }}</td>
                                        <td style="text-align: center;">{{ $value2['quantity'] }}</td>
                                        <td style="text-align: right;">{{ $value2['rate'] }}</td>
                                        <td style="text-align: right;">{{ round($value2['rate'] * $value2['quantity'], 2) }}</td>
                                        <td style="text-align: right;">
                                            <?php
                                                if ($value2['discount_type'] == 0)
                                                {
                                                    $discountAmount   = ($value2['rate'] * $value2['quantity'] * $value2['discount_amount'])/100;
                                                }
                                                else
                                                {
                                                    $discountAmount   = $value2['discount_amount'];
                                                }
                                            ?>

                                            {{ $discountAmount }}
                                        </td>
                                        <td style="text-align: right;">{{ ($value2['rate'] * $value2['quantity']) - $discountAmount }}</td>
                                    </tr>

                                    @endif
                                    @endforeach

                                    <?php
                                        $i++;

                                        $total_t_price      = $total_t_price + $sub_total_t_price;
                                        $total_discount     = $total_discount + $sub_total_discount;
                                        $total_payable      = $total_payable + $sub_total_payable;
                                        $total_vat          = $total_vat +  $vat;
                                        $total_discount_t   = $total_discount_t + $discountAmount0;
                                        $total_net_payable  = $total_net_payable + $net_payable;
                                        $total_paid         = $total_paid + $value2['return_amount'];
                                    ?>
                                @endif
                                @endif
                                @endforeach
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="8" style="text-align: right;">{{__('messages.total')}}</th>
                                    <th colspan="1" style="text-align: right;">{{ round($total_t_price - $total_sales_retuen * 2) }}</th>
                                    <th colspan="1" style="text-align: right;">{{ round($total_discount) }}</th>
                                    <th colspan="1" style="text-align: right;">{{ round($total_payable - $total_sales_retuen * 2) }}</th>
                                    <th colspan="1" style="text-align: right;">{{ round($total_vat) }}</th>
                                    <th colspan="1" style="text-align: right;" id ="TDiscount"></th>
                                    <th colspan="1" style="text-align: right;">{{ round($total_net_payable - $total_sales_retuen * 2) }}</th>
                                    <th colspan="1" style="text-align: right;">{{ round($total_paid - $total_sales_retuen) }}</th>
                                    <th colspan="1" style="text-align: right;" id="Tdue"></th>
                                    <th colspan="1" style="text-align: right;" id="Tprof"></th>
                                    <th colspan="1" style="text-align: right;" id="Tloss"></th>
                                    <th colspan="2" style="text-align: right;"></th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input value="{{count($data)}}" id="total_count_number">
 <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script> 
<!-- <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script> -->
<!-- <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script> -->
<script type="text/javascript">
    $( document ).ready(function() {
        var total_count_number = $("#total_count_number").val();
        
        var sum1 = 0;
        $('.pr').each(function(){
            sum1 += parseFloat($(this).text());  // Or this.innerHTML, this.innerText
        });
        
        var sum2 = 0;
        $('.lss').each(function(){
            sum2 += parseFloat($(this).text());  // Or this.innerHTML, this.innerText
        });         
        
        
        var sum3 = 0;
        $('.due').each(function(){
            sum3 += parseFloat($(this).text());  // Or this.innerHTML, this.innerText
        });        
        
        var sum4 = 0;
        $('.profitP').each(function(){
            sum4 += parseFloat($(this).text());  // Or this.innerHTML, this.innerText
        });
        
        
        var sum5 = 0;
        $('.TDiscount').each(function(){
            sum5 += parseFloat($(this).text());  // Or this.innerHTML, this.innerText
        }); 
        
      
        var asas = parseFloat(sum1) + '(' + (parseFloat(sum4) / total_count_number) + '%)' ; 
        
        
        
        console.log(sum4);
        
        $("#Tprof").html(asas);
        $('#Tloss').html(parseFloat(sum2));
        $('#Tdue').html(parseFloat(sum3));
        $('#TDiscount').html(parseFloat(sum5));
        
        // console.log(sum1);
        // console.log(sum2);
    });
</script>

</body>
</html>