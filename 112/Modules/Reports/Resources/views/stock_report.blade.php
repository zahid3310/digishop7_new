@extends('layouts.app')

@section('title', 'Product Report')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.product_report')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.reports')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.product_report')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['organization_name'] }}</h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['address'] }}</p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['contact_number'] }}</p>
                                        <h4 style="line-height: 10px;text-align: center">{{ __('messages.stocK_report')}}</h4>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="{{ route('stock_report_index') }}" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="major_category_id" style="width: 100" class="form-control select2" name="major_category_id">
                                                    <option value="{{ $major_category != null ? $major_category->id : 0 }}" selected>{{ $major_category != null ? $major_category->name : '-- All Major Categories --' }}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-6 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="product_id">
                                                    <option value="0" selected>-- {{ __('messages.all_category')}} --</option>
                                                    @if(!empty($products) && ($products->count() > 0))
                                                    @foreach($products as $key => $value)
                                                        <option {{ isset($_GET['product_id']) && ($_GET['product_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-6 col-12 d-print-none margin-top-10-xs">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="{{ __('messages.search')}}"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>

                                    </div>
                                </form>

                                <table class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.sl')}}</th>
                                            <th>{{ __('messages.code')}}</th>
                                            <th>{{ __('messages.name')}}</th>
                                            <th>{{ __('messages.category')}}</th>
                                            <th style="text-align: center">{{ __('messages.purchase_price')}}</th>
                                            <th style="text-align: center">{{ __('messages.sell_price')}}</th>
                                            @if($branches->count() > 0)
                                            @foreach($branches as $branch)
                                            <th style="text-align: center">{{ $branch->name }}</th>
                                            @endforeach
                                            @endif
                                            @if($branch_id == 1)
                                            <th style="text-align: center">{{ __('messages.total_stock')}}</th>
                                            @endif
                                            <th style="text-align: center">{{ __('messages.purchase_price')}}</th>
                                            <th style="text-align: center">{{ __('messages.sell_price')}}</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($data) && ($data->count() > 0))
                                        <?php
                                            $total_stock_in_hand    = 0;
                                            $total_sold             = 0;
                                            $total_stock_value      = 0;
                                            $total_sell_value       = 0;
                                        ?>
                                        @foreach($data as $key => $product)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ str_pad($product['product_code'], 6, "0", STR_PAD_LEFT) }}</td>
                                                <td>{{ $product['name'] }}</td>
                                                <td>{{ $product['category_name'] }}</td>
                                                <td style="text-align: center">{{ number_format($product['buy_price'],0,'.',',') }}</td>
                                                <td style="text-align: center">{{ number_format($product['sell_price'],0,'.',',') }}</td>
                                                
                                                @if($branches->count() > 0)
                                                <?php $total_stock = 0; ?>
                                                @foreach($branches as $branch)
                                                @if($branch['id'] == 1)
                                                <td style="text-align: center">
                                                    {{ number_format($product['stock_in_hand'],0,'.',',') }} 
                                                    @if($product['stock_in_hand'] != null)
                                                       <?php echo $product['unit_name'] ?> 
                                                    @endif
                                                </td>
                                                <?php $total_stock = $total_stock + $product['stock_in_hand']; ?>
                                                @else
                                                <td style="text-align: center">
                                                    <?php
                                                        $stock = findStock($product['id'], $branch['id']);
                                                    ?>

                                                    {{ number_format($stock['stock_in_hand'],3,'.',',') }} 
                                                    @if($product['stock_in_hand'] != null)
                                                       <?php echo $product['unit_name'] ?> 
                                                    @endif
                                                </td>
                                                <?php $total_stock = $total_stock + $stock['stock_in_hand']; ?>
                                                @endif
                                                @endforeach
                                                @endif

                                                @if($branch_id == 1)
                                                <td style="text-align: center">
                                                    {{ number_format($total_stock,0,'.',',') }} 
                                                    @if($product['stock_in_hand'] != null)
                                                       <?php echo $product['unit_name'] ?> 
                                                    @endif
                                                </td>
                                                @endif
                                                
                                                <td style="text-align: center">{{ number_format($product['buy_price'] *$product['stock_in_hand'],0,'.',',') }}</td>
                                                <td style="text-align: center">{{ number_format($product['sell_price'] *$product['stock_in_hand'],0,'.',',') }}</td>
                                            </tr>

                                            <?php
                                                $total_stock_in_hand    = $total_stock_in_hand + $product['stock_in_hand'];
                                                $total_sold             = $total_sold + $product['total_sold'];
                                                $total_stock_value      = $total_stock_value + ($product['buy_price']*$product['stock_in_hand']);
                                                $total_sell_value       = $total_sell_value + ($product['sell_price']*$product['stock_in_hand']);
                                            ?>
                                        @endforeach
                                        @endif
                                        
                                        <?php 
                                        
                                        $profit = round((($total_sell_value - $total_stock_value) * 100) / $total_sell_value) ;
                                        
                                        ?>

                                         <tr>
                                            <td style="text-align: right" colspan="8"><strong>Total</strong></td>
                                            <!--@if($branches->count() > 0)-->
                                            <!--@foreach($branches as $branch)-->
                                            <!--<th style="text-align: center"></th>-->
                                            <!--@endforeach-->
                                            <!--@endif-->
                                            <!--<td style="text-align: center">{{ isset($total_stock_in_hand) ? number_format($total_stock_in_hand,2,'.',',') : 0 }}</td>-->
                                            <td style="text-align: center">{{ isset($total_stock_in_hand) ? number_format($total_stock_value,0,'.',',') : 0 }}</td>
                                            <td style="text-align: center">{{ isset($total_stock_in_hand) ? number_format($total_sell_value,0,'.',',') : 0 }} ({{$profit}}%)</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $("#major_category_id").select2({
                ajax: { 
                    url:  site_url + '/products/brand-brand-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

        });
    </script>
@endsection