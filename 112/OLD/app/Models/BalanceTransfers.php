<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class BalanceTransfers extends Model
{  
    protected $table = "balance_transfers";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function transferFrom()
    {
        return $this->belongsTo('App\Models\Accounts','transfer_from');
    }

    public function transferTo()
    {
        return $this->belongsTo('App\Models\Accounts','transfer_to');
    }

    public function branchFrom()
    {
        return $this->belongsTo('App\Models\Branches','branch_from');
    }

    public function branchTo()
    {
        return $this->belongsTo('App\Models\Branches','branch_to');
    }
}
