@extends('layouts.app')

@section('title', 'Return Issues')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('issues_return_issue_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

                                <div class="row">

                                    <div style="background-color: #D2D2D2;height: 620px;padding-top: 10px;" class="col-md-7">
                                        <div style="display: none" class="inner-repeater mb-4 issueDetails">
                                            <div data-repeater-list="inner-group" class="inner form-group row">
                                                <div id="return_product_list" class="inner col-lg-12 ml-md-auto">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 620px;padding-top: 10px" class="col-md-5">

                                        <div class="row">
                                            <div style="display: none" class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="from_date">From Date *</label>
                                                    <input id="from_date" type="text" value="{{ date('01-01-1970') }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                                </div>
                                            </div>

                                            <div style="display: none" class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="to_date">To Date *</label>
                                                    <input id="to_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Select SR * </label>
                                                    <select style="width: 73%" id="sr_id" name="sr_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                           <option value="0">All</option>
                                                        </select>
                                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="findIssueList()">
                                                            <i class="bx bx-search font-size-24"></i>
                                                        </span>
                                                </div>
                                            </div>

                                            <div style="display: none" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Issue Number * </label>
                                                    <div style="padding-left: 10px" class="row">
                                                        <select style="width: 73%" id="issue_id" name="issue_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                           <option value="0">All</option>
                                                        </select>
                                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3">
                                                            <i class="bx bx-search font-size-24"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="return_date">Return Date *</label>
                                                    <input id="return_date" name="return_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="return_note">Note</label>
                                                    <input id="return_note" name="return_note" type="text" placeholder="Enter Return Note" class="form-control">
                                                </div>
                                            </div>

                                            <div style="display: none;margin-top: 20px" class="button-items col-lg-12 issueDetails">
                                                <button name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                <!-- <button name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Save & Print</button> -->
                                                <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('all_return_issues') }}">Close</a></button>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();
            var type      = (new URL(location.href)).searchParams.get('return_type');

            $("#sr_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 4 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#issue_id").select2({
                ajax: { 
                url:  site_url + '/srissues/issues-list/',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });
    </script>

    <script type="text/javascript">
        function findIssueList()
        {
            var site_url        = $(".site_url").val();
            var from_date       = $("#from_date").val();
            var to_date         = $("#to_date").val();
            var sr_id           = $("#sr_id").val();
            var issue_id        = $("#issue_id").val();

            $.get(site_url + '/srissues/return-issues-product-list/' + from_date + '/' + to_date + '/' + sr_id + '/' + issue_id, function(data){

                if (data != '')
                {
                    $('.issueDetails').show();
                }
                else
                {
                    $('.issueDetails').hide();
                }

                var return_product_list = '';
                var serial              = 1;
                $.each(data, function(i, data_list)
                {
                    if (serial == 1)
                    {
                        var product_label    = '<label class="hidden-xs" for="productname">Product *</label>\n';
                        var quantity_label   = '<label class="hidden-xs" for="productname">Qty. *</label>\n';
                        var return_label     = '<label class="hidden-xs" for="productname">Return Qty.</label>\n';
                    }
                    else
                    {
                        var product_label    = '';
                        var quantity_label   = '';
                        var return_label     = '';
                    }

                    var quantity = parseFloat(data_list.receive_quantity) - parseFloat(data_list.sold_quantity) - parseFloat(data_list.return_quantity);

                    if (quantity == 0)
                    {
                        var return_quantity = '<input id="return_quantity_'+ serial +'" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="'+ quantity +'" oninput="calculate('+ serial +')" readonly />\n' 
                    }
                    else
                    {
                        var return_quantity = '<input id="return_quantity_'+ serial +'" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="'+ quantity +'" oninput="calculate('+ serial +')" />\n' 
                    }

                    if (quantity != 0)
                    {
                        return_product_list += ' ' + '<div style="margin-bottom: 0px !important" class="mb-3 row align-items-center">' +
                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-6 col-md-6 col-sm-6 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<input type="text" class="inner form-control" value="'+ data_list.product_name +'" readonly />\n' +
                                                        '<input name="product_entries[]" type="hidden" value="'+ data_list.product_entry_id +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty. *</label>\n' +
                                                        quantity_label +
                                                        '<input id="quantity_'+ serial +'" type="text" class="inner form-control" value="'+ quantity +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Return Qty</label>\n' +
                                                        return_label +
                                                        return_quantity +
                                                    '</div>\n' +
                                                '</div>\n';

                                                serial++;
                    }                         
                });

                $("#return_product_list").empty();
                $("#return_product_list").append(return_product_list);

            });
        }
    </script>

    <script type="text/javascript">
        function calculate(x)
        {
        	var return_quantity     = $('.returnQuantity').map((_,el) => el.value).get();
         	var amount 				= $('.amount').map((_,el) => el.value).get();
         	var quantity			= $('.quantity').map((_,el) => el.value).get();
         	var rate				= $('.rate').map((_,el) => el.value).get();

         	var vatType             = $("#vat_type_0").val();
            var vatAmount           = $("#vat_amount_0").val();
            var taxType             = $("#tax_type_0").val();
            var taxAmount           = $("#tax_amount_0").val();
            var totalDiscount       = $("#total_discount_0").val();
            var totalDiscountType   = $("#total_discount_type_0").val();
            var totalBdt            = $("#totalBdt").val();
            var subTotal            = $("#subTotalBdt").val();

            var total                   = 0;
            var total_quantity          = 0;
            var total_return_quantity   = 0;

         	for (var i = 0; i < return_quantity.length; i++)
         	{
         		if (return_quantity[i] > 0)
         		{	
         			var result   = (parseFloat(amount[i])/parseFloat(quantity[i]))*parseFloat(return_quantity[i]);

         			total   	+= parseFloat(result);
         		}

                total_quantity          += parseFloat(quantity[i]);
                total_return_quantity   += parseFloat(return_quantity[i]);
         	}

            if (vatType == 0)
            {
                var vatTypeCal     = (parseFloat(total)*parseFloat(vatAmount))/100;
            }
            else
            {
                var vatTypeCal     = (parseFloat(vatAmount)*parseFloat(total))/parseFloat(subTotal);
            }

            if (totalDiscountType == 0)
            {
                var totalDiscountCal     = ((parseFloat(total) + parseFloat(vatTypeCal))*parseFloat(totalDiscount))/100;
            }
            else
            {
                var totalDiscountCal     = (parseFloat(total)*parseFloat(totalDiscount))/(parseFloat(subTotal) + parseFloat(vatAmount));
            }

            if (taxType == 0)
            {
                var taxTypeCal     = (parseFloat(total)*parseFloat(taxAmount))/100;
            }
            else
            {
                var taxTypeCal     = $("#tax_amount_0").val();
            }

            var total_result       = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountCal);

         	$('#totalReturnedBdt').val(total_result.toFixed(2));
            $("#amount_paid").val(parseFloat(total_result.toFixed(2)));
        }
    </script>
@endsection