<?php

namespace Modules\Income\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\IncomeCategories;
use App\Models\Incomes;
use App\Models\PaidThroughAccounts;
use App\Models\AccountTransactions;
use DB;
use Response;

class IncomeController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $income_categories      = IncomeCategories::where('income_categories.id', '!=', 1)
                                                    ->get();

        $incomes                = Incomes::leftjoin('income_categories', 'income_categories.id', 'incomes.income_category_id')
                                        ->orderBy('incomes.created_at', 'DESC')
                                        ->select('incomes.*',
                                         'income_categories.name as category_name')
                                        ->get();

        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();

        return view('income::index', compact('income_categories', 'incomes', 'paid_accounts'));
    }

    public function create()
    {
        return view('income::create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'income_date'          => 'required',
            'income_category_id'   => 'required',
            'amount'               => 'required',
            'amount'               => 'required',
            'paid_through'         => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $data_find                      = Incomes::orderBy('created_at', 'DESC')->first();
            $income_number                  = $data_find != null ? $data_find['income_number'] + 1 : 1;
   
            $income                         = new Incomes;
            $income->income_number          = $income_number;
            $income->income_date            = date('Y-m-d', strtotime($data['income_date']));
            $income->income_category_id     = $data['income_category_id'];
            $income->amount                 = $data['amount'];
            $income->paid_through_id        = $data['paid_through'];
            $income->account_information    = $data['account_information'];
            $income->note                   = $data['note'];
            $income->created_by             = $user_id;

            if ($income->save())
            {   
                $account_transaction                      = new AccountTransactions;
                $account_transaction->transaction_date    = date('Y-m-d', strtotime($data['income_date']));
                $account_transaction->amount              = $data['amount'];
                $account_transaction->paid_through_id     = $data['paid_through'];
                $account_transaction->account_information = $data['account_information'];
                $account_transaction->note                = $data['note'];
                $account_transaction->type                = 0;
                $account_transaction->transaction_head    = 'income';
                $account_transaction->associated_id       = $income->id;
                $account_transaction->created_by          = $user_id;
                $account_transaction->save();

                DB::commit();
                return back()->with("success","Income Created Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function categoryStore(Request $request)
    {
        $rules = array(
            'income_category_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $income_categories              = new IncomeCategories;
            $income_categories->name        = $data['income_category_name'];
            $income_categories->created_by  = $user_id;

            if ($income_categories->save())
            {   
                DB::commit();
                return back()->with("success","Category Created Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        return view('income::show');
    }

    public function edit($id)
    {
        $income_categories     = IncomeCategories::where('income_categories.id', '!=', 1)
                                                    ->get();

        $incomes               = Incomes::leftjoin('income_categories', 'income_categories.id', 'incomes.income_category_id')
                                                    ->orderBy('incomes.created_at', 'DESC')
                                                    ->select('incomes.*',
                                                             'income_categories.name as category_name')
                                                    ->get();

        $income_find           = Incomes::leftjoin('income_categories', 'income_categories.id', 'incomes.income_category_id')
                                                    ->orderBy('incomes.created_at', 'DESC')
                                                    ->select('incomes.*',
                                                             'income_categories.name as category_name',
                                                             'income_categories.id as category_id')
                                                    ->find($id);

        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();

        return view('income::income_edit', compact('income_categories', 'incomes', 'income_find', 'paid_accounts'));
    }

    public function categoryEdit($id)
    {
        $income_categories      = IncomeCategories::where('income_categories.id', '!=', 1)
                                                    ->where('income_categories.id', '!=', 2)
                                                    ->get();
                                                    
        $incomes                = Incomes::leftjoin('income_categories', 'income_categories.id', 'incomes.income_category_id')
                                                    ->orderBy('incomes.created_at', 'DESC')
                                                    ->select('incomes.*',
                                                     'income_categories.name as category_name')
                                                    ->get();

        $income_category_find   = IncomeCategories::find($id);

        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();

        return view('income::income_category_edit', compact('income_categories', 'incomes', 'income_category_find', 'paid_accounts'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'income_date'          => 'required',
            'income_category_id'   => 'required',
            'amount'               => 'required',
            'amount'               => 'required',
            'paid_through'         => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $income                        = Incomes::find($id);
            $income->income_date           = date('Y-m-d', strtotime($data['income_date']));
            $income->income_category_id    = $data['income_category_id'];
            $income->amount                = $data['amount'];
            $income->paid_through_id       = $data['paid_through'];
            $income->account_information   = $data['account_information'];
            $income->note                  = $data['note'];
            $income->updated_by            = $user_id;

            if ($income->save())
            {  
                $delete_transaction                       = AccountTransactions::where('transaction_head', 'income')->where('associated_id', $income->id)->delete();
                $account_transaction                      = new AccountTransactions;
                $account_transaction->transaction_date    = date('Y-m-d', strtotime($data['income_date']));
                $account_transaction->amount              = $data['amount'];
                $account_transaction->paid_through_id     = $data['paid_through'];
                $account_transaction->account_information = $data['account_information'];
                $account_transaction->note                = $data['note'];
                $account_transaction->type                = 0;
                $account_transaction->transaction_head    = 'income';
                $account_transaction->associated_id       = $income->id;
                $account_transaction->updated_by          = $user_id;
                $account_transaction->save();
                
                DB::commit();
                return redirect()->route('incomes_index')->with("success","Income Updated Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function categoryUpdate(Request $request, $id)
    {
        $rules = array(
            'income_category_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $income_categories              = IncomeCategories::find($id);
            $income_categories->name        = $data['income_category_name'];
            $income_categories->updated_by  = $user_id;

            if ($income_categories->save())
            {   
                DB::commit();
                return redirect()->route('incomes_index')->with("success","Category Updated Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function destroy($id)
    {
    }

    public function incomeListLoad()
    {
        $data           = Incomes::leftjoin('income_categories', 'income_categories.id', 'incomes.income_category_id')
                                ->orderBy('incomes.created_at', 'DESC')
                                ->select('incomes.*',
                                         'income_categories.name as category_name')
                                ->take(100)
                                ->get();

        return Response::json($data);
    }

    public function incomeListSearch($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data           = Incomes::leftjoin('income_categories', 'income_categories.id', 'incomes.income_category_id')
                                ->where('income_categories.name', 'LIKE', "%$id%")
                                ->orWhere('incomes.income_date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number, $table_id) {
                                    return $query->orWhere('incomes.income_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('incomes.created_at', 'DESC')
                                ->select('incomes.*',
                                         'income_categories.name as category_name')
                                ->take(100)
                                ->get();
        }
        else
        {
            $data           = Incomes::leftjoin('income_categories', 'income_categories.id', 'incomes.income_category_id')
                                ->orderBy('incomes.created_at', 'DESC')
                                ->select('incomes.*',
                                         'income_categories.name as category_name')
                                ->take(100)
                                ->get();
        }
        
        return Response::json($data);
    }
}
