<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('invoices')->group(function() {
    Route::get('/', 'InvoicesController@index')->name('invoices_index');
    Route::get('/all-sales', 'InvoicesController@AllSales')->name('invoices_all_sales');
    Route::post('/store', 'InvoicesController@store')->name('invoices_store');
    Route::get('/edit/{id}', 'InvoicesController@edit')->name('invoices_edit');
    Route::post('/update/{id}', 'InvoicesController@update')->name('invoices_update');
    Route::get('/show/{id}', 'InvoicesController@show')->name('invoices_show');
    Route::get('/show-pos/{id}', 'InvoicesController@showPos')->name('invoices_show_pos');
    Route::get('/products/list', 'InvoicesController@productList')->name('product_list');
    Route::get('/products/price/list/{id}', 'InvoicesController@productPriceList')->name('product_price_list');
    Route::get('/invoice/list/load', 'InvoicesController@invoiceListLoad')->name('invoices_list_load');
    Route::get('/invoice/search/list/{from_date}/{to_date}/{customer_name}/{invoice_number}', 'InvoicesController@invoiceListSearch')->name('invoices_list_search');
    Route::get('/invoice/pos/search/product/{id}', 'InvoicesController@posSearchProduct')->name('invoices_pos_search_product');
    Route::post('/customer/add/invoices', 'InvoicesController@customerStore');
    Route::get('/customer/list/invoices', 'InvoicesController@customersListInvoice');

    Route::get('/customer-make-payment/{id}', 'InvoicesController@makePayment');
    Route::post('/pay-customer-bill/payments', 'InvoicesController@storePayment')->name('pay_customer_bill');
    Route::get('/search/coupon-code/{id}', 'InvoicesController@couponCode')->name('invoices_coupon_code');

    Route::get('/print-invoices-list', 'InvoicesController@printInvoicesList')->name('invoices_print_invoices_list');
    Route::get('/print-invoices-search/{date}/{customer}/{invoice_number}', 'InvoicesController@printInvoicesSearch')->name('invoices_print_invoices_search');
    Route::get('/calculate-opening-balance/{customer_id}', 'InvoicesController@calculateOpeningBalance')->name('invoices_calculate_opening_balance');
    Route::get('/product-list-load-invoice', 'InvoicesController@productListLoadInvoice')->name('invoices_product_list_load');
    Route::get('/invoice-expense-categories', 'InvoicesController@expenseCategories')->name('invoices_expense_categories');
    Route::get('/adjust-advance-payment/{customer_id}', 'InvoicesController@adjustAdvancePayment')->name('invoices_adjust_advance_payment');
});

Route::prefix('cashPaymentVoucher')->group(function(){
    Route::get('/voucherList', 'InvoicesController@listOfVouchers')->name('list_of_voucher');
    Route::get('/', 'InvoicesController@cashPaymentVoucherIndex')->name('cash_payment_voucher_index');
    Route::post('/store', 'InvoicesController@cashPaymentVoucherStore')->name('cash_payment_voucher_store');
    Route::get('/print/{voucher_id}', 'InvoicesController@voucherprint')->name('voucher_print');
});

Route::prefix('cashReceiptVoucher')->group(function(){
    Route::get('/', 'InvoicesController@cashReceiptVoucherIndex')->name('cash_receipt_voucher_index');
    Route::post('/store', 'InvoicesController@cashReceiptVoucherStore')->name('cash_receipt_voucher_store');
});

Route::prefix('bankPaymentVoucher')->group(function(){
    Route::get('/', 'InvoicesController@bankPaymentVoucherIndex')->name('bank_payment_voucher_index');
    Route::post('/store', 'InvoicesController@bankPaymentVoucherStore')->name('bank_payment_voucher_store');
});

Route::prefix('bankReceiptVoucher')->group(function(){
    Route::get('/', 'InvoicesController@bankReceiptVoucherIndex')->name('bank_receipt_voucher_index');
    Route::post('/store', 'InvoicesController@bankReceiptVoucherStore')->name('bank_receipt_voucher_store');
});

Route::prefix('journalVoucher')->group(function(){
    Route::get('/', 'InvoicesController@journalVoucherIndex')->name('journal_voucher_index');
    Route::post('/store', 'InvoicesController@journalVoucherStore')->name('journal_voucher_store');
});

Route::prefix('contraVoucher')->group(function(){
    Route::get('/', 'InvoicesController@contraVoucherIndex')->name('contra_voucher_index');
    Route::post('/store', 'InvoicesController@contraVoucherStore')->name('contra_voucher_store');
});

Route::prefix('voucherPosting')->group(function(){
    Route::get('/', 'InvoicesController@voucherPostingIndex')->name('voucher_posting_index');
    Route::post('/store', 'InvoicesController@voucherPostingStore')->name('voucher_posting_store');
    Route::get('/voucher-details/{voucher_number}', 'InvoicesController@getVoucherDetails')->name('voucher_posting_get_voucher_details');
    Route::post('/posting-approve', 'InvoicesController@voucherPostingApprove')->name('voucher_posting_approve');
    Route::get('/posting-unapprove/{id}', 'InvoicesController@voucherPostingUnapprove')->name('voucher_posting_unapprove');
    Route::get('/voucher-list/{voucher_type}/{entry_by}/{type}/{date_from}/{date_to}', 'InvoicesController@voucherList')->name('voucher_list');
    Route::get('/load-vouchers-datatable', 'InvoicesController@loadVouchersDatatable');

    ##Cash Payment Voucher Edit Start##
    Route::get('/cashPaymentVoucherEdit/{id}', 'InvoicesController@cashPaymentVoucherEdit')->name('cash_payment_voucher_edit');
    Route::post('/cashPaymentVoucherUpdate/{id}', 'InvoicesController@cashPaymentVoucherUpdate')->name('cash_payment_voucher_update');
    ##Cash Payment Voucher Edit End##

    ##Cash Receipt Voucher Edit Start##
    Route::get('/cashReceiptVoucherEdit/{id}', 'InvoicesController@cashReceiptVoucherEdit')->name('cash_receipt_voucher_edit');
    Route::post('/cashReceiptVoucherUpdate/{id}', 'InvoicesController@cashReceiptVoucherUpdate')->name('cash_receipt_voucher_update');
    Route::get('/getBankBalance/{id}', 'InvoicesController@getBankBalance');
    Route::get('/getCashBalance', 'InvoicesController@getCashBalance');
    ##Cash Receipt Voucher Edit End##

    ##Bank Payment Voucher Edit Start##
    Route::get('/bankPaymentVoucherEdit/{id}', 'InvoicesController@bankPaymentVoucherEdit')->name('bank_payment_voucher_edit');
    Route::post('/bankPaymentVoucherUpdate/{id}', 'InvoicesController@bankPaymentVoucherUpdate')->name('bank_payment_voucher_update');
    ##Bank Payment Voucher Edit End##

    ##Bank Receipt Voucher Edit Start##
    Route::get('/bankReceiptVoucherEdit/{id}', 'InvoicesController@bankReceiptVoucherEdit')->name('bank_receipt_voucher_edit');
    Route::post('/bankReceiptVoucherUpdate/{id}', 'InvoicesController@bankReceiptVoucherUpdate')->name('bank_receipt_voucher_update');
    ##Bank Receipt Voucher Edit End##

    ##Journal Voucher Edit Start##
    Route::get('/journalVoucherEdit/{id}', 'InvoicesController@journalVoucherEdit')->name('journal_voucher_edit');
    Route::post('/journalVoucherUpdate/{id}', 'InvoicesController@journalVoucherUpdate')->name('journal_voucher_update');
    ##Journal Voucher Edit End##

    ##Contra Voucher Edit Start##
    Route::get('/contraVoucherEdit/{id}', 'InvoicesController@contraVoucherEdit')->name('contra_voucher_edit');
    Route::post('/contraVoucherUpdate/{id}', 'InvoicesController@contraVoucherUpdate')->name('contra_voucher_update');
    ##Contra Voucher Edit End##
});