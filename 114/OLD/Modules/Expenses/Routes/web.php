<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('expenses')->group(function() {
    Route::get('/', 'ExpensesController@index')->name('expenses_index');
    Route::post('/store', 'ExpensesController@store')->name('expenses_store');
    Route::get('/edit/{id}', 'ExpensesController@edit')->name('expenses_edit');
    Route::get('/show/{id}', 'ExpensesController@show')->name('expenses_show');
    Route::post('/update/{id}', 'ExpensesController@update')->name('expenses_update');
    Route::get('/expense/list/load', 'ExpensesController@expenseListLoad')->name('expenses_list_load');
    Route::get('/expense/search/list/{id}', 'ExpensesController@expenseListSearch')->name('expenses_list_search');
});

Route::prefix('expenses-category')->group(function() {
    Route::post('/store', 'ExpensesController@categoryStore')->name('expenses_categories_store');
    Route::get('/edit/{id}', 'ExpensesController@categoryEdit')->name('expenses_categories_edit');
    Route::post('/update/{id}', 'ExpensesController@categoryUpdate')->name('expenses_categories_update');
});

Route::prefix('expenses-employee-salary')->group(function() {
    Route::get('/', 'ExpensesController@employeeSalaryIndex')->name('expenses_employee_salary_index');
    Route::post('/store', 'ExpensesController@employeeSalaryStore')->name('expenses_employee_salary_store');
    Route::get('/edit/{id}', 'ExpensesController@employeeSalaryEdit')->name('expenses_employee_salary_edit');
    Route::post('/update/{id}', 'ExpensesController@employeeSalaryUpdate')->name('expenses_employee_salary_update');
    Route::get('/expense/employee-salary/list/load', 'ExpensesController@employeeSalaryListLoad')->name('expenses_employee_salary_list_load');
    Route::get('/expense/employee-salary/search/list/{id}', 'ExpensesController@employeeSalaryListSearch')->name('expenses_employee_salary_list_search');
});
