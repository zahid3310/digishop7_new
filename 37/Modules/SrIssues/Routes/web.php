<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('srissues')->group(function() {
    Route::get('/', 'SrIssuesController@index')->name('issues_index');
    Route::get('/all-issues', 'SrIssuesController@AllIssues')->name('all_issues');
    Route::post('/store', 'SrIssuesController@store')->name('issues_store');
    Route::get('/edit/{id}', 'SrIssuesController@edit')->name('issues_edit');
    Route::post('/update/{id}', 'SrIssuesController@update')->name('issues_update');
    Route::get('/show/{id}', 'SrIssuesController@show')->name('issues_show');
    Route::get('/calculate-opening-balance/{sr_id}', 'SrIssuesController@calculateOpeningBalance')->name('issues_calculate_opening_balance');

    //Js Codes Start
    Route::get('/issues/list/load', 'SrIssuesController@issuesListLoad')->name('issues_list_load');
    Route::get('/issue/search/list/{from_date}/{to_date}/{sr_name}/{issue_number}', 'SrIssuesController@issueListSearch')->name('issues_list_search');
    Route::get('/print-issues-list', 'SrIssuesController@printIssuesList')->name('issues_print_issues_list');
    Route::get('/print-issues-search/{date}/{customer}/{issue_number}', 'SrIssuesController@printIssuesSearch')->name('issues_print_issues_search');
    Route::get('/issues-list', 'SrIssuesController@issuesList')->name('issues_list');
    //Js Codes Start

    //Return Routes
    Route::get('/all-returns', 'SrIssuesController@AllReturns')->name('all_return_issues');
    Route::get('/return-issues', 'SrIssuesController@returnIssues')->name('issues_return_issues');
    Route::post('/return-issues-store', 'SrIssuesController@returnIssueStore')->name('issues_return_issue_store');
    Route::get('/return-issues-edit/{id}', 'SrIssuesController@editReturn')->name('issues_return_edit');
    Route::post('/return-issues-update/{id}', 'SrIssuesController@updateReturn')->name('issues_return_update');
    Route::get('/return-issues-show/{id}', 'SrIssuesController@showReturn')->name('issues_return_show');
    Route::get('/return-issues-product-list/{from_date}/{to_date}/{sr_id}/{issue_id}', 'SrIssuesController@returnIssueProductList')->name('issues_return_issue_product_list');
    Route::get('/returns/list/load', 'SrIssuesController@returnsListLoad')->name('issues_return_list_load');
    Route::get('/issue-return-list', 'SrIssuesController@issueReturnList')->name('issues_return_list');
    Route::get('/return/search/list/{from_date}/{to_date}/{sr_name}/{issue_number}', 'SrIssuesController@allReturnsListSearch')->name('issues_return_list_search');
});
