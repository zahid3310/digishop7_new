

<?php $__env->startSection('title', 'List of Grades'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">List of Grades</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Basic Settings</a></li>
                                    <li class="breadcrumb-item active">List of Grades</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <?php if(Session::has('success')): ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <?php echo Session::get('success'); ?>

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <?php endif; ?>

                        <?php if(Session::has('unsuccess')): ?>
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            <?php echo Session::get('unsuccess'); ?>

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <?php endif; ?>

                        <?php if(Session::has('errors')): ?>
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <?php endif; ?>

                        <div class="card">
                            <div class="card-body table-responsive">
                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Title</th>
                                            <th>Basic</th>
                                            <th>House Rent</th>
                                            <th style="text-align: right">Medical</th>
                                            <th style="text-align: right">Convence</th>
                                            <th style="text-align: right">Food</th>
                                            <th style="text-align: right">Mobile</th>
                                            <th style="text-align: right">Others</th>
                                            <th style="text-align: right">Gross</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php if(!empty($salary_grades) && ($salary_grades->count() > 0)): ?>
                                        <?php $__currentLoopData = $salary_grades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $salary_grade): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e($salary_grade->name); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($salary_grade->basic,2,'.',',')); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($salary_grade->house_rent,2,'.',',')); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($salary_grade->medical,2,'.',',')); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($salary_grade->convence,2,'.',',')); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($salary_grade->food,2,'.',',')); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($salary_grade->mobile,2,'.',',')); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($salary_grade->others,2,'.',',')); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($salary_grade->gross,2,'.',',')); ?></td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="<?php echo e(route('salary_grades_edit', $salary_grade->id)); ?>">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/zsrm/Modules/Payroll/Resources/views/SalaryGrades/index.blade.php ENDPATH**/ ?>