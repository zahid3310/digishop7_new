

<?php $__env->startSection('title', 'Edit Transfer'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.edit_transfer')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.production')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.edit_transfer')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                            Payment Successfull !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                            Payment Not Added !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('productions_transfer_to_production_update', $find_production['id'])); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                <?php echo e(csrf_field()); ?>

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label"><?php echo e(__('messages.date')); ?> *</label>
                                            <div class="col-md-8">
                                                <input id="production_date" name="production_date" type="text" value="<?php echo e(date('d-m-Y', strtotime($find_production['date']))); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label"><?php echo e(__('messages.production')); ?># *</label>
                                            <div class="col-md-7">
                                                <input id="production_number" type="text" class="form-control" placeholder="Production Number" value="<?php echo e($find_production['production_number']); ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-3 col-form-label"><?php echo e(__('messages.note')); ?></label>
                                            <div class="col-md-9">
                                                <input id="note" name="note" type="text" class="form-control" placeholder="<?php echo e(__('messages.note')); ?>" value="<?php echo e($find_production['note']); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 530px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        <?php $__currentLoopData = $find_production_entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div style="margin-bottom: 0px !important" class="row di_<?php echo e($key); ?>">
                                                <div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname"><?php echo e(__('messages.product')); ?> *</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname"><?php echo e(__('messages.product')); ?> *</label>
                                                    <select style="width: 100%" name="product_entries[]" class="inner form-control select2 productEntries" id="product_entries_<?php echo e($key); ?>" onchange="getItemPrice(<?php echo e($key); ?>)" required>
                                                        <option value="<?php echo e($value['item_id']); ?>">
                                                        <?php
                                                            $vari       = ProductVariationName($value['item_id']);
                                                            $variation  = $vari != null ? ' - ' . $vari : '';
                                                        ?>

                                                        <?php echo e($value['item_name'] . $variation . '( ' . str_pad($value['product_code'], 6, "0", STR_PAD_LEFT) . ' )'); ?>

                                                        </option>
                                                    </select>
                                                    <span id="stock_show_<?php echo e($key); ?>" style="color: black"><?php echo e('Stock : '.round($value['stock_in_hand'], 2)); ?></span>
                                                </div>

                                                <input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_<?php echo e($key); ?>" value="<?php echo e($value['main_unit_id']); ?>" />

                                                <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname"><?php echo e(__('messages.unit')); ?></label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname"><?php echo e(__('messages.unit')); ?></label>
                                                    <select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_<?php echo e($key); ?>" required onchange="getConversionParam(<?php echo e($key); ?>)">
                                                        <option value="<?php echo e($value['conversion_unit_id']); ?>"><?php echo e($value['conversion_unit_id'] != null ? $value->convertedUnit->name : ''); ?></option>
                                                    </select>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname"><?php echo e(__('messages.qty')); ?> *</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname"><?php echo e(__('messages.qty')); ?> *</label>
                                                    <input type="text" name="quantity[]" class="inner form-control quantityCheck" id="quantity_<?php echo e($key); ?>" value="<?php echo e(round($value['quantity'], 2)); ?>" placeholder="<?php echo e(__('messages.qty')); ?>" oninput="calculateActualAmount(<?php echo e($key); ?>)" required/>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname"><?php echo e(__('messages.rate')); ?> *</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname"><?php echo e(__('messages.rate')); ?> *</label>
                                                    <input type="text" name="rate[]" class="inner form-control rateCheck" id="rate_<?php echo e($key); ?>" value="<?php echo e(round($value['rate'], 2)); ?>" placeholder="<?php echo e(__('messages.rate')); ?>" oninput="calculateActualAmount(<?php echo e($key); ?>)" required/>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname"><?php echo e(__('messages.amount')); ?></label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname"><?php echo e(__('messages.amount')); ?></label>
                                                    <input type="text" name="amount[]" class="inner form-control amount" id="amount_<?php echo e($key); ?>" value="<?php echo e(round($value['amount'], 2)); ?>" placeholder="<?php echo e(__('messages.amount')); ?>" oninput="calculateActualAmount(<?php echo e($key); ?>)" required/>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname"><?php echo e(__('messages.action')); ?></label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname"><?php echo e(__('messages.action')); ?> *</label>
                                                    <?php if($key == 0): ?>
                                                        <i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button"></i>
                                                    <?php else: ?>
                                                        <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field" data-val="<?php echo e($key); ?>"></i>
                                                    <?php endif; ?> 
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px !important;padding-bottom: 0px !important;background-color: #F4F4F7" class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-9">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light"><?php echo e(__('messages.update')); ?></button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('productions_transfer_to_production_index')); ?>"><?php echo e(__('messages.close')); ?></a></button>
                                            </div>
                                            <div class="button-items col-lg-3">
                                                 <div style="margin-bottom: 5px" class="form-group row">
                                                    <label style="text-align: right" class="col-md-5 col-form-label"><?php echo e(__('messages.sub_total')); ?></label>
                                                    <div class="col-md-7">
                                                        <input type="text" id="subTotalBdt" class="form-control">
                                                        <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $(".productEntries").select2({
                ajax: { 
                url:  site_url + '/productions/transfer-to-production/get-raw-materials',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            calculateActualAmount();
        });
    </script>

    <script type="text/javascript">
        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }
    </script>

    <script type="text/javascript">
        function getItemPrice(x)
        {
            //For getting item commission information from items table start
            var site_url  = $(".site_url").val();
            var entry_id  = $("#product_entries_"+x).val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    if (jQuery.isEmptyObject(data) == false)
                    {   
                        var list    = '';
                        $.each(data.unit_conversions, function(i, data_list)
                        {   
                            list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                        });

                        $("#unit_id_"+x).empty();
                        $("#unit_id_"+x).append(list);

                        if (data.product_entries != null)
                        {
                            if (data.product_entries.stock_in_hand == 0 || data.product_entries.stock_in_hand == '')
                            {
                                var stockInHand  = 0;
                            }
                            else
                            {
                                var stockInHand  = data.product_entries.stock_in_hand;
                            }

                            if (data.product_entries.unit_id == null)
                            {
                                var unit  = '';
                            }
                            else
                            {
                                var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                            }

                            $("#rate_"+x).val(parseFloat(data.product_entries.buy_price).toFixed(2));
                        }
                        else
                        {
                             var stockInHand    = 0;
                             var unit           = '';

                             $("#rate_"+x).val(0);
                        }
                        
                        $("#stock_"+x).val(stockInHand);
                        $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' ' + unit);
                        $("#quantity_"+x).val(1);
                        $("#main_unit_id_"+x).val(data.product_entries.unit_id);
          
                        calculateActualAmount(x);
                    }
                    else
                    {
                        $("#rate_"+x).val(0);
                        $("#stock_"+x).val(0);
                        $("#stock_show_"+x).html('Stock : 0');
                        $("#quantity_"+x).val(0);
                        $("#main_unit_id_"+x).val(data.product_entries.unit_id);
          
                        calculateActualAmount(x);
                    }
                    
                });
            }
            // calculateActualAmount(x);
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                    $("#stock_show_"+x).html('Stock : ' + convertedStock + ' ( ' + convertedUnitName + ' )');
                    $("#rate_"+x).val(parseFloat(data.buy_price).toFixed(2));

                    // calculateActualAmount(x);
                }

            });
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = <?php echo e($entries_count); ?>;
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {   
                x++;

                var serial = x + 1;

                if (serial == 1)
                {
                    var product_label   = '<label class="hidden-xs" for="productname"><?php echo e(__('messages.product')); ?> *</label>\n';
                    var unit_label      = '<label class="hidden-xs" for="productname"><?php echo e(__('messages.unit')); ?></label>\n';
                    var quantity_label  = '<label class="hidden-xs" for="productname"><?php echo e(__('messages.qty')); ?> *</label>\n';
                    var rate_label      = '<label class="hidden-xs" for="productname"><?php echo e(__('messages.rate')); ?></label>\n';
                    var amount_label    = '<label class="hidden-xs" for="productname"><?php echo e(__('messages.amount')); ?></label>\n';
                    var action_label    = '<label class="hidden-xs" for="productname"><?php echo e(__('messages.action')); ?></label>\n';

                    var add_btn         = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button">\n' + 
                                                '<label style="display: none" class="show-xs" for="productname"><?php echo e(__('messages.action')); ?></label>\n' +
                                                    action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButton()"></i>' +
                                            '</div>\n';
                }
                else
                {
                    var product_label   = '';
                    var unit_label      = '';
                    var quantity_label  = '';
                    var rate_label      = '';
                    var amount_label    = '';
                    var action_label    = '';

                    var add_btn         = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname"><?php echo e(__('messages.action')); ?></label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRow').append(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-12 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname"><?php echo e(__('messages.product')); ?> *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                            '<option value="">' + '--<?php echo e(__('messages.select_product')); ?>--' + '</option>' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="<?php echo e(__('messages.stock')); ?>" oninput="calculateActualAmount('+x+')" />\n' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +

                                                    '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname"><?php echo e(__('messages.unit')); ?></label>\n' +
                                                        unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_'+x+'" required onchange="getConversionParam('+x+')">\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control" id="quantity_'+x+'" placeholder="<?php echo e(__('messages.qty')); ?>" oninput="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname"><?php echo e(__('messages.qty')); ?> *</label>\n' +
                                                        rate_label  +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="<?php echo e(__('messages.rate')); ?>" oninput="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname"><?php echo e(__('messages.amount')); ?></label>\n' +
                                                        amount_label  +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="<?php echo e(__('messages.amount')); ?>" required />\n' +
                                                    '</div>\n' +
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

                var site_url = $(".site_url").val();
                $(".productEntries").select2({
                    ajax: { 
                    url:  site_url + '/productions/transfer-to-production/get-raw-materials',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                    },
                });
            }                                    
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            var rate                    = $("#rate_"+x).val();
            var quantity                = $("#quantity_"+x).val();
        
            if (rate == '')
            {
                var rateCal             = 0;
            }
            else
            {
                var rateCal             = $("#rate_"+x).val();
            }

            if (quantity == '')
            {
                var quantityCal         = 1;
            }
            else
            {
                var quantityCal         = $("#quantity_"+x).val();
            }

            var AmountIn              =  (parseFloat(rateCal)*parseFloat(quantityCal));
     
            $("#amount_"+x).val(AmountIn);

            //Calculating Subtotal Amount
            var total       = 0;

            $('.amount').each(function()
            {
                total       += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(total.toFixed());
            $("#subTotalBdtShow").val(total.toFixed());
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/zsrm/Modules/Productions/Resources/views/send_to_transfer/edit.blade.php ENDPATH**/ ?>