

<?php $__env->startSection('title', 'Bulk Product Update'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.bulk_product_update')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.product')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.bulk_product_update')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('products_bulk_product_list_update_save')); ?>" method="post" files="true" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>


                                <?php if($product_entries->count() > 0): ?>
                                <?php $__currentLoopData = $product_entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <div class="row">
                                    
                            		
	                                
                                    <input type="hidden" name="product_id[]" class="inner form-control" id="product_id" value="<?php echo e($value['id']); ?>" />
                                    
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        <?php if($key == 0): ?>
                                        <label for="productname" class="col-form-label"><?php echo e(__('messages.product_code')); ?></label>
                                        <?php endif; ?>
                                        <input type="text" name="code[]" class="inner form-control barCode" id="bar_code_0" value="<?php echo e($value['product_code']); ?>" />
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        <?php if($key == 0): ?>
                                        <label for="productname" class="col-form-label"><?php echo e(__('messages.product_name')); ?> *</label>
                                        <?php endif; ?>
                                        <input type="text" name="product_name[]" class="inner form-control" id="product_name" value="<?php echo e($value['name']); ?>" required />
                                    </div>
                                    
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        <?php if($key == 0): ?>
                                        <label for="productname" class="col-form-label"><?php echo e(__('messages.stock_in_hand')); ?> *</label>
                                        <?php endif; ?>
                                        <input type="text" name="stock_in_hand[]" class="inner form-control" id="stock_in_hand_0" value="<?php echo e($value['stock_in_hand']); ?>" required />
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        <?php if($key == 0): ?>
                                        <label for="productname" class="col-form-label"><?php echo e(__('messages.purchase_price')); ?> *</label>
                                        <?php endif; ?>
                                        <input type="text" name="buying_price[]" class="inner form-control" id="buying_price_0" value="<?php echo e($value['buy_price']); ?>" required />
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        <?php if($key == 0): ?>
                                        <label for="productname" class="col-form-label"><?php echo e(__('messages.sell_price')); ?> *</label>
                                        <?php endif; ?>
                                        <input type="text" name="selling_price[]" class="inner form-control" id="selling_price_0" value="<?php echo e($value['sell_price']); ?>" required />
                                    </div>
                                    
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        <?php if($key == 0): ?>
                                        <label for="productname" class="col-form-label"><?php echo e(__('messages.wholesale_price')); ?> *</label>
                                        <?php endif; ?>
                                        <input type="text" name="wholesale_price[]" class="inner form-control" id="wholesale_price_0" value="<?php echo e($value['wholesale_price']); ?>" required />
                                    </div>
                                    
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light"><?php echo e(__('messages.update')); ?></button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('products_bulk_product_list_update')); ?>"><?php echo e(__('messages.close')); ?></a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/zsrm/Modules/Products/Resources/views/bulk_product_update.blade.php ENDPATH**/ ?>