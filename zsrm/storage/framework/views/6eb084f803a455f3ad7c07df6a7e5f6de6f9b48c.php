

<?php $__env->startSection('title', 'Income Report'); ?>

<?php $__env->startSection('styles'); ?>
    <style type="text/css">
        @media  print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page  {
                margin: 0cm ! important;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.income_report')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.reports')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.income_report')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['address']); ?></p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['contact_number']); ?></p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e(__('messages.income_report')); ?></h4>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><strong><?php echo e(__('messages.from_date')); ?></strong> <?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?> <strong><?php echo e(__('messages.to')); ?></strong> <?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="<?php echo e(route('income_report_index')); ?>" enctype="multipart/form-data">
                                    <?php if(Auth()->user()->branch_id == 1): ?>
                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label"><?php echo e(__('messages.select_branch')); ?></label>
                                        <div class="col-md-5">
                                            <select style="width: 100%" id="branch_id" name="branch_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" required>
                                               <?php if($branches->count() > 0): ?>
                                               <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                               <option value="<?php echo e($value['id']); ?>" <?php echo e($branch_id == $value['id'] ? 'selected' : ''); ?>><?php echo e($value['name']); ?></option>
                                               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                               <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <div class="form-group row mb-12">
                                        <div style="margin-bottom: 10px" class="col-lg-5 col-md-5 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="<?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="<?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        <div class="col-lg-5 col-md-5 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="account_head" style="cursor: pointer" name="account_id" class="form-control select2" required>
                                                <option value="0">--<?php echo e(__('messages.all')); ?>--</option>
                                                <?php if(!empty($accounts) && ($accounts->count() > 0)): ?>
                                                <?php $__currentLoopData = $accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($account['id']); ?>" <?php echo e($account_id == $account['id'] ? 'selected' : ''); ?>><?php echo e($account['account_name']); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="<?php echo e(__('messages.search')); ?>"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th><?php echo e(__('messages.sl')); ?></th>
                                            <th><?php echo e(__('messages.date')); ?></th>
                                            <th><?php echo e(__('messages.incomes')); ?>#</th>
                                            <th><?php echo e(__('messages.paid_through')); ?></th>
                                            <th><?php echo e(__('messages.account_head')); ?></th>
                                            <th><?php echo e(__('messages.note')); ?></th>
                                            <th style="text-align: right"><?php echo e(__('messages.amount')); ?></th>
                                        </tr>
                                    </thead>


                                    <tbody>

                                        <?php
                                            $serial         = 1; 
                                            $total_amount   = 0; 
                                        ?>
                                        
                                        <?php if(!empty($incomes) && ($incomes->count() > 0)): ?>
                                        <?php $__currentLoopData = $incomes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($serial); ?></td>
                                                <td><?php echo e('INC - ' . str_pad($value['income_number'], 6, "0", STR_PAD_LEFT)); ?></td>
                                                <td><?php echo e(date('d-m-Y', strtotime($value['income_date']))); ?></td>
                                                <td><?php echo e($value->paidThroughAccount->account_name); ?></td>
                                                <td><?php echo e($value->account->account_name); ?></td>
                                                <td><?php echo e($value['note']); ?></td>
                                                <td style="text-align: right"><?php echo e(number_format($value['amount'],0,'.',',')); ?></td>
                                            </tr>

                                            <?php 
                                                $serial++;
                                                $total_amount   = $total_amount + $value['amount'];
                                            ?>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>

                                    </tbody>
                                    <tr>
                                        <th style="text-align: right" colspan="6"><?php echo e(__('messages.total')); ?></th>
                                        <th style="text-align: right"><?php echo e(number_format($total_amount,0,'.',',')); ?></th>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/zsrm/Modules/Reports/Resources/views/income_report.blade.php ENDPATH**/ ?>