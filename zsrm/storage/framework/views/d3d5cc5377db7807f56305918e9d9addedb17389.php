

<?php $__env->startSection('title', 'Incomes'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.incomes')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.incomes')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.incomes')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

            					<form id="FormSubmit" action="<?php echo e(route('incomes_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					<?php echo e(csrf_field()); ?>


            						<div class="row">
					                	<div class="col-sm-3">
					                        <div class="form-group">
					                            <label for="income_date"><?php echo e(__('messages.income_date')); ?> *</label>
					                            <input id="income_date" name="income_date" type="text" value="<?php echo e(Session::has('income_date') ? date('d-m-Y', strtotime(Session::get('income_date'))) : date('d-m-Y')); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
					                        </div>
					                    </div>

                                        <!-- <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="income_number">Income# *</label>
                                                <input id="income_number" name="income_number" type="text" class="form-control" required>
                                            </div>
                                        </div> -->

                                        <div class="col-sm-3">
                                            <div id="contact_id_reload" class="form-group">
                                                <label class="control-label"><?php echo e(__('messages.receive/income_from')); ?></label>
                                                <select id="customer_id" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="">--<?php echo e(__('messages.select_contact')); ?>--</option>
                                                    
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="note"><?php echo e(__('messages.account_head')); ?> *</label>
                                                <select id="account_head" style="cursor: pointer" name="account_id" class="form-control select2" required>
                                                <option value="">--<?php echo e(__('messages.select_account_head')); ?>--</option>
                                                <?php if(!empty($accounts) && ($accounts->count() > 0)): ?>
                                                <?php $__currentLoopData = $accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($account['id']); ?>"><?php echo e($account['account_name']); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

					                    <div class="col-sm-3">
					                        <div class="form-group">
					                            <label for="amount"><?php echo e(__('messages.amount')); ?> *</label>
					                            <input id="amount" name="amount" type="text" class="form-control" oninput="searchContact()" required>
					                        </div>
					                    </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="note"><?php echo e(__('messages.receive_through')); ?> *</label>
                                                <select id="paid_through_id" style="cursor: pointer" name="paid_through" class="form-control select2" required>
                                                <option value="">--<?php echo e(__('messages.select_paid_through')); ?>--</option>
                                                <?php if(!empty($paid_accounts) && ($paid_accounts->count() > 0)): ?>
                                                <?php $__currentLoopData = $paid_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $paid_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($paid_account['id']); ?>"><?php echo e($paid_account['account_name']); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="note"><?php echo e(__('messages.account_information')); ?></label>
                                                <input type="text" name="account_information" class="form-control" id="account_information" placeholder="<?php echo e(__('messages.account_information')); ?>"/>
                                            </div>
                                        </div>

				                        <div class="col-sm-6">
					                        <div class="form-group">
					                            <label for="note"><?php echo e(__('messages.note')); ?></label>
					                            <input id="note" name="note" type="text" class="form-control">
					                        </div>
					                    </div>
				                	</div>

                                    <hr style="margin-top: 0px">

                                    <div class="form-group row">
                                        <div class="button-items col-md-12">
                                            <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light"><?php echo e(__('messages.save')); ?></button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('incomes_index')); ?>"><?php echo e(__('messages.close')); ?></a></button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <hr>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title"><?php echo e(__('messages.income_list')); ?></h4>

                                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th><?php echo e(__('messages.sl')); ?></th>
                                            <th><?php echo e(__('messages.income_date')); ?></th>
                                            <th><?php echo e(__('messages.income_number')); ?></th>
                                            <th><?php echo e(__('messages.paid_through')); ?></th>
                                            <th><?php echo e(__('messages.account_head')); ?></th>
                                            <th><?php echo e(__('messages.amount')); ?></th>
                                            <th><?php echo e(__('messages.action')); ?></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                    	<?php if($incomes->count() > 0): ?>
                                    	<?php $__currentLoopData = $incomes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    	<tr>
                                    		<td><?php echo e($key + 1); ?></td>
                                    		<td><?php echo e(date('d-m-Y', strtotime($value['income_date']))); ?></td>
                                    		<td><?php echo e('INC - ' . str_pad($value['income_number'], 6, "0", STR_PAD_LEFT)); ?></td>
                                    		<td><?php echo e($value->paidThroughAccount->account_name); ?></td>
                                    		<td><?php echo e($value->account->account_name); ?></td>
                                    		<td><?php echo e($value->amount); ?></td>
                                    		<td>
                                    			<div class="dropdown">
	                                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
	                                                    <i class="mdi mdi-dots-horizontal font-size-18"></i>
	                                                </a>
	                                                <div class="dropdown-menu dropdown-menu-right" style="">
		                                                <a class="dropdown-item" href="<?php echo e(route('incomes_edit', $value->id)); ?>"><?php echo e(__('messages.edit')); ?> </a>
		                                            </div>
	                                            </div>
                                    		</td>
                                    	</tr>
                                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    	<?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

    		</div>
		</div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Create New Income Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form id="FormSubmit1" action="<?php echo e(route('incomes_categories_store')); ?>" method="post" files="true" enctype="multipart/form-data">
                    <?php echo e(csrf_field()); ?>

                    <div style="padding-top: 10px;padding-bottom: 0px" class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="income_category_name">Category Name *</label>
                                    <input id="income_category_name" name="income_category_name" type="text" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url        = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['id'] != 0 && result['id'] != 1 && result['id'] != 2)
                    {
                        return result['text'];
                    }
                },
            });
            
            $.get(site_url + '/incomes/income/list/load', function(data){

                var income_list = '';
                $.each(data, function(i, income_data)
                {   
                    var serial      = parseFloat(i) + 1;

                    if (income_data.income_category_id != 1)
                    {
                        var edit_url    = site_url + '/incomes/edit/' + income_data.id;
                    }
                    else
                    {
                        var edit_url    = site_url + '/incomes/';
                    }

                    income_list += '<tr>' +
                                        '<td>' +
                                            serial +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(income_data.income_date) +
                                        '</td>' +
                                        '<td>' +
                                           'INC - ' + income_data.income_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            income_data.category_name +
                                        '</td>' +
                                        '<td>' +
                                           income_data.amount +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '<?php if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3): ?>' +
                                                    '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                    '<?php endif; ?>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                });

                $("#income_list").empty();
                $("#income_list").append(income_list);
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var search_text     = $('#searchPayment').val();
            var site_url        = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/incomes/income/search/list/' + search_text, function(data){

                var income_list = '';
                $.each(data, function(i, income_data)
                {   
                    var serial      = parseFloat(i) + 1;
                    
                    if (income_data.income_category_id != 1)
                    {
                        var edit_url    = site_url + '/incomes/edit/' + income_data.id;
                    }
                    else
                    {
                        var edit_url    = site_url + '/incomes/';
                    }

                    income_list += '<tr>' +
                                        '<td>' +
                                            serial +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(income_data.income_date) +
                                        '</td>' +
                                        '<td>' +
                                           'INC - ' + income_data.income_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            income_data.category_name +
                                        '</td>' +
                                        '<td>' +
                                           income_data.amount +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '<?php if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3): ?>' +
                                                    '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                    '<?php endif; ?>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                });

                $("#income_list").empty();
                $("#income_list").append(income_list);
            });
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/zsrm/Modules/Income/Resources/views/index.blade.php ENDPATH**/ ?>