<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="{{ route('home') }}" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                @if(Auth::user()->branch_id == 1)
                <li class="{{ 
                Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-suitcase"></i><span>Receive/Purchase</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('bills_index') }}">Receive/Purchase Item</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' }}" href="{{ route('bills_all_bills') }}">List of Receive/Purchase</a> </li>
                        <li> <a class="{{ Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' }}" href="{{ route('purchase_return_index') }}">Receive/Purchase Return</a> </li>
                    </ul>
                </li>
                @endif

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-suitcase"></i><span>Stock Transfer</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('stock_transfer_index') }}">New Transfer</a> </li>
                    </ul>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-shopping-basket"></i><span>Sales To SR/Customer</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('invoices_index') }}">Daily Sales</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' }}" href="{{ route('invoices_all_sales') }}">List of Sales</a> </li>
                        <li> <a class="{{ Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' }}"  href="{{ route('sales_return_index') }}">Sales Return</a> </li>
                    </ul>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'settlement_type=0' ? 'mm-active' : '' ||  Request::getQueryString() == 'settlement_type=1' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'settlement_type=0' ? 'mm-active' : '' ||  Request::getQueryString() == 'settlement_type=1' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fab fa-adn"></i><span>Accounts</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' }}" href="{{ route('expenses_index') }}">Expenses</a> </li>

                        <li> <a class="{{ 
                            Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' }}" href="{{ route('incomes_index') }}">Incomes</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : '' }}" href="{{ route('balance_transfer_index') }}">Balance Transfer</a>
                        </li>
                        <li> <a class="{{ Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' }}" href="{{ route('payments_create').'?payment_type=0' }}">Receive Form Customer</a> </li>
                        @if(Auth::user()->branch_id == 1)
                        <li> <a class="{{ Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' }}" href="{{ route('payments_create').'?payment_type=1' }}">Pay To Supplier</a> </li>
                        @endif
                        <li> <a class="{{ Request::getQueryString() == 'settlement_type=0' ? 'mm-active' : '' }}" href="{{ route('settlements_create').'?settlement_type=0' }}">Customer Settlement</a> </li>
                        @if(Auth::user()->branch_id == 1)
                        <li> <a class="{{ Request::getQueryString() == 'settlement_type=1' ? 'mm-active' : '' }}" href="{{ route('settlements_create').'?settlement_type=1' }}">Supplier Settlement</a> </li>
                        @endif
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="far fa-envelope"></i><span>Messaging</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('messages_send_index') }}">Send Message</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>Reports</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li class="">
                            <a class="has-arrow waves-effect">
                                Sales
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('sales_statement_index') }}">Statement of Sales</a> </li>
                                <li> <a href="{{ route('sales_summary_index') }}">Sales Summary</a> </li>
                                <li> <a href="{{ route('product_wise_sales_report_index') }}">Product Wise Sales</a> </li>
                            </ul>
                        </li>

                        @if(Auth()->user()->branch_id == 1)
                        <li class="">
                            <a class="has-arrow waves-effect">
                                Receive/Purchase
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('purchase_statement_index') }}">Statement of Pur.</a> </li>
                                <li> <a href="{{ route('purchase_summary_index') }}">Receive/Purchase Summary</a> </li>
                            </ul>
                        </li>
                        @endif

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Accounts
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('expense_report_index') }}">List of Expense</a> </li>
                                <li> <a href="{{ route('income_report_index') }}">List of Income</a> </li>
                                <!-- <li> <a href="{{ route('general_ledger_index') }}">Ledger Book</a> </li> -->
                                <li> <a href="{{ route('income_expense_ledger_index') }}">Cash Book</a> </li>
                                <li> <a href="{{ route('income_statement_index') }}">Bank Book</a> </li>
                                <!-- <li> <a href="{{ route('daily_collection_report_index') }}">Daily Report</a> </li> -->
                            </ul>
                        </li>
                        
                        <!-- <li class="">
                            <a class="has-arrow waves-effect">
                                Payments
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('customer_payment_report_index') }}">SR/Customer Payments</a> </li>
                                <li> <a href="{{ route('supplier_payment_report_index') }}">Supplier Payments</a> </li>
                            </ul>
                        </li> -->

                        <li class="">
                            <a class="has-arrow waves-effect">
                                MIS
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('stock_report_index') }}">Stock Status</a> </li>
                                <li> <a href="{{ route('stock_status_details_index') }}">Stock Status Details</a> </li>
                                <li> <a href="{{ route('stock_transfer_report_index') }}">Stock Transfer Report</a> </li>
                                <li> <a href="{{ route('current_balance_print') }}" target="_blank">Current Balance</a> </li>
                                @if(Auth::user()->branch_id == 1)
                                <li> <a href="{{ route('due_report_supplier_index') }}">Supplier Ledger</a> </li>
                                @endif
                                <li> <a href="{{ route('due_report_customer_index') }}">SR/Customer Ledger</a> </li>
                                <li> <a href="{{ route('due_report_customer_print_due') }}">Due Customer List</a> </li>
                                @if(Auth::user()->branch_id == 1)
                                <li> <a href="{{ route('due_report_supplier_index_due') }}">Due Supplier List</a> </li>
                                @endif
                                <!-- <li> <a href="{{ route('due_ledger_index') }}">Due Ledger</a> </li> -->
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Basic Report
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                @if(Auth()->user()->branch_id == 1)
                                <li> <a href="{{ route('item_list_index') }}">List of Items</a> </li>
                                <li> <a href="{{ route('emergency_item_list_index') }}">Emergency Purchase</a> </li>
                                <li> <a href="{{ route('product_suppliers_index') }}">Item Wise Supplier</a> </li>
                                <li> <a href="{{ route('product_customers_index') }}">Item Wise SR/Customer</a> </li>
                                @endif
                                <li> <a href="{{ route('register_list_index').'?type=0' }}" target="_blank">SR/Customer List</a> </li>
                                <li> <a href="{{ route('register_list_index').'?type=1' }}" target="_blank">Supplier List</a> </li>
                            </ul>
                        </li>
                        
                        <!-- <li> <a href="#">List of Sending SMS</a> </li>
                        <li> <a href="{{ route('salary_report_index') }}">Salary Report</a> </li> -->

                    </ul>
                </li>

                <li class="{{ 
                Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'users_index' ? 'mm-active' : '' || Route::currentRouteName() == 'users_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'users_index' ? 'mm-active' : '' || Route::currentRouteName() == 'users_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>Basic Settings</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        @if(Auth()->user()->branch_id == 1)
                        <li class="{{ Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Product
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' }}" href="{{ route('products_index') }}">Add Product</a> </li>
                                <li> <a class="" href="{{ route('products_index_all') }}">List Of Products</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }}" href="{{ route('products_category_index') }}">Add Categories</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' }}" href="{{ route('categories_index') }}" href="{{ route('categories_index') }}">Add Major Categories</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' }}" href="{{ route('products_units_index') }}">Add Unit Measure</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' }}" href="{{ route('products_variations_index') }}">Product Variations</a> </li>
                                <li> <a href="{{ route('products_barcode_print') }}">Print Barcode</a> </li>
                                <li> <a href="{{ route('products_opening_stock') }}">Bulk Opening Stock</a> </li>
                                <li> <a href="{{ route('products_bulk_product_list_update') }}">Bulk Product Update</a> </li>
                            </ul>
                        </li>
                        @endif

                        <li class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' }}">
                            <a class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Registers
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=0' }}">Add SR/Customer</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=1' }}">Add Supplier</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=2' }}">Add Employee</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=3' }}">Add Reference</a> </li>
                            </ul>
                        </li>

                        <li class="{{ Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || 
                        Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' ||
                        Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || 
                            Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Accounts
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <!-- <li> <a class="{{ Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' }}" href="{{ route('account_types_index') }}">Account Types</a> </li> -->
                                <li> <a class="{{ Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' }}" href="{{ route('chart_of_accounts_index') }}">Chart of Accounts</a> </li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow waves-effect">
                                Messaging
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('messages_index') }}">Create Text Message</a> </li>
                                <li> <a href="{{ route('messages_phone_book_index') }}">Phone Book</a> </li>
                            </ul>
                        </li>

                        @if(Auth()->user()->branch_id == 1)
                        <li>
                            <a class="has-arrow waves-effect">
                                Security System
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('branch_index') }}">Add Branch</a> </li>
                                <li> <a href="{{ route('users_index') }}">Add User</a> </li>
                                <li> <a href="{{ route('users_index_all') }}">List of User</a> </li>
                                <li> <a href="{{ route('set_access_index') }}">Set Permission</a> </li>
                            </ul>
                        </li>
                         @endif
                         
                        <li> <a href="{{ route('assets_index') }}">Fixed Assets</a></li>
                        
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>