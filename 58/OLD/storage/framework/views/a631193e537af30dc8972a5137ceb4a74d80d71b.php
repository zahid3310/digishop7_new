

<?php $__env->startSection('title', 'Emergency Item List'); ?>

<?php $__env->startSection('styles'); ?>
    <style type="text/css">
        @media  print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page  {
                margin: 0cm ! important;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Emergency Item List</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Emergency Item List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="margin-left: 150px;margin-right: 150px" class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <form method="get" action="<?php echo e(route('emergency_item_list_print')); ?>" enctype="multipart/form-data" target="_blank">

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Brand </label>
                                        <div class="col-md-9">
                                            <select style="width: 100%" id="brand_id" name="brand_id" class="form-control select2">
                                               <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Category </label>
                                        <div class="col-md-9">
                                            <select style="width: 100%" id="category_id" name="category_id" class="form-control select2">
                                               <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Product</label>
                                        <div class="col-md-9">
                                            <select style="width: 100%" id="product_id" name="product_id" class="form-control select2">
                                               <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label"></label>
                                        <div class="col-md-9">
                                            <button style="border-radius: 0px;background-color: #297049;color: white" class="btn" type="submit" target="_blank">
                                                Print
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>


    <script type="text/javascript">

    $(document).on("change", "#brand_id" , function() {
        var site_url  = $('.site_url').val();
        var brandId   = $('#brand_id').val();

        $("#product_id").select2({
                ajax: { 
                url:  site_url + '/reports/emergency-item-list/emergency-product-list/'+brandId,
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    console.log(response);
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
    });

        $( document ).ready(function() {

            var site_url  = $('.site_url').val();
            var brandId   = $('#brand_id').val();

            $("#brand_id").select2({
                ajax: { 
                url:  site_url + '/reports/emergency-item-list/emergency-brand-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $("#category_id").select2({
                ajax: { 
                url:  site_url + '/reports/emergency-item-list/emergency-category-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $("#product_id").select2({
                ajax: { 
                url:  site_url + '/reports/emergency-item-list/emergency-product-list/'+brandId,
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });


        });

        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/58/Modules/Reports/Resources/views/emergency_item_list.blade.php ENDPATH**/ ?>