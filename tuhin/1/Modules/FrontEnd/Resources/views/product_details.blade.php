@extends('frontend::layouts.app')

@section('main_body')
<main class="main mt-lg-4">
    <div class="page-content">
        <div class="container">
            <div class="row main-content-wrap gutter-lg">
                <aside class="col-lg-3 sidebar sidebar-fixed shop-sidebar sticky-sidebar-wrapper">
                    <div class="sidebar-overlay">
                        <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
                    </div>
                    <div class="sidebar-content">
                        <div class="sticky-sidebar">
                            <div class="widget widget-collapsible">
                                <h3 class="widget-title">All Top Categories</h3>
                                <ul class="widget-body filter-items search-ul">
                                    @if($majorCategories->count() > 0)
                                    @foreach($majorCategories as $key => $major_category)
                                    <li class="with-ul">
                                        <a href="{{route('product_list')}}">{{ $major_category['name'] }}</a>
                                        <ul>
                                            @if($major_category->product->count() > 0)
                                            @foreach($major_category->product as $key1 => $category)
                                            <li><a href="{{route('product_list')}}">{{ $category['name'] }}</a></li>
                                            @endforeach
                                            @endif
                                        </ul>
                                    </li>
                                    @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </aside>

                <div class="col-xl-9 col-lg-8">
                    <main class="main mt-4">
                        <div class="page-content mb-10">
                            <div class="container">
                                <div class="product product-single row mb-4">
                                    <div class="col-md-6">
                                        <div class="product-gallery pg-vertical">
                                            <div class="product-single-carousel owl-carousel owl-theme owl-nav-inner row cols-1">
                                                @if($product_images->count() > 0)
                                                @foreach($product_images as $key => $product_image)
                                                <figure class="product-image">
                                                    <img src="{{ url('public/images/products/'.$product_image->image) }}"
                                                        alt="Product Image" width="800" height="900">
                                                </figure>
                                                @endforeach
                                                @endif
                                            </div>

                                            <div class="product-thumbs-wrap">
                                                <div class="product-thumbs">
                                                    @if($product_images->count() > 0)
                                                    @foreach($product_images as $key1 => $product_image1)
                                                    <div class="product-thumb active">
                                                        <img src="{{ url('public/images/products/'.$product_image1->image) }}" alt="Product Image"
                                                            width="109" height="122">
                                                    </div>
                                                    @endforeach
                                                    @endif
                                                </div>
                                                <button class="thumb-up disabled"><i class="fas fa-chevron-left"></i></button>
                                                <button class="thumb-down disabled"><i class="fas fa-chevron-right"></i></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="product-details">
                                            <h1 class="product-name">{{ $find_product->name != null ? $find_product->name : '' }}</h1>
                                            <div class="product-meta">
                                                @if($find_product->product_code != null)
                                                SKU: <span class="product-sku">{{ $find_product->product_code }}</span>
                                                @endif
                                                @if($find_product->brand_id != null)
                                                BRAND: <span class="product-brand">{{ $find_product->brand->name }} </span>
                                                @endif
                                            </div>
                                            <div class="product-price">{{ 'BDT ' . $find_product->sell_price }}</div>
                                            
                                            <p class="product-short-desc">{{$find_product->description}}</p>

                                            <hr class="product-divider">

                                            <div class="product-form product-qty">
                                                <label>QTY:</label>
                                                <div class="product-form-group">
                                                    <div class="input-group">
                                                        <button class="quantity-minus d-icon-minus"></button>
                                                        <input class="quantity form-control" type="number" min="1" max="1000000">
                                                        <button class="quantity-plus d-icon-plus"></button>
                                                    </div>
                                                    <button class="btn-product btn-cart addItemToCart" data-id="{{$find_product->id}}" data-thumb="{{ !empty(productImage($find_product['id'])) ? url('public/images/products/'.productImage($find_product['id'])->image) : url('public/default-product-image.jpeg') }}" data-title="{{$find_product->name}}" data-price="{{$find_product->sell_price}}" data-url="{{ route('product_details', $find_product->id) }}" data-quantity="1" data-toggle="modal" href="javascript:void(0)" title="Add to cart">Add To Cart</button>
                                                </div>
                                            </div>

                                            <hr class="product-divider mb-3">

                                            <div class="product-footer">
                                                <div class="social-links">
                                                    <a href="#" class="social-link social-facebook fab fa-facebook-f"></a>
                                                    <!-- <a href="#" class="social-link social-twitter fab fa-twitter"></a>
                                                    <a href="#" class="social-link social-vimeo fab fa-vimeo-v"></a> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
            </div>

            <section>
                <h2 class="title">Our Featured</h2>

                <div class="owl-carousel owl-theme owl-nav-full row cols-2 cols-md-3 cols-lg-4"
                    data-owl-options="{
                    'items': 5,
                    'nav': false,
                    'loop': false,
                    'dots': true,
                    'margin': 20,
                    'responsive': {
                        '0': {
                            'items': 2
                        },
                        '768': {
                            'items': 3
                        },
                        '992': {
                            'items': 4,
                            'dots': false,
                            'nav': true
                        }
                    }
                }">
                    @if($products->count() > 0)
                    @foreach($products as $key => $product)
                    <div class="product shadow-media">
                        <figure class="product-media">
                            <a href="{{ route('product_details', $product->id) }}">
                                @if(!empty(productImage($product['id'])))
                                <img src="{{ url('public/images/products/'.productImage($product['id'])->image) }}" alt="product"
                                    width="280" height="315">
                                @else
                                <img src="{{ url('public/front_end_asset/images/demos/demo3/products/1.jpg') }}" alt="product"
                                    width="280" height="315">
                                @endif
                            </a>
                            <!-- <div class="product-label-group">
                                <label class="product-label label-new">new</label>
                            </div>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                    data-target="#addCartModal" title="Add to cart"><i
                                        class="d-icon-bag"></i></a>
                            </div> -->
                            <div class="product-action">
                                <a class="btn-product btn-cart addItemToCart"
                                        data-id="{{$product->id}}" data-thumb="{{ !empty(productImage($product['id'])) ? url('public/images/products/'.productImage($product['id'])->image) : url('public/default-product-image.jpeg') }}" data-title="{{$product->name}}" data-price="{{$product->sell_price}}" data-url="{{ route('product_details', $product->id) }}" data-quantity="1" data-toggle="modal" href="javascript:void(0)" title="Add to cart">ADD TO CART</a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <div class="product-cat">
                                <a href="">{{ $product->major_category_id != null ? $product->product->name : '' }}</a>
                            </div>
                            <h3 class="product-name">
                                <a href="product.html">{{ $product->name }}</a>
                            </h3>
                            <div class="product-price">
                                <ins class="new-price">{{ 'BDT '. $product->sell_price }}</ins><del class="old-price">{{ $product->old_price != null ? 'BDT ' .$product->old_price : '' }}</del>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </section>

            <br>
            <br>
            <br>
        </div>
    </div>
</main>
@endsection