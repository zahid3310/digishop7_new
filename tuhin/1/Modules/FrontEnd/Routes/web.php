<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'FrontEndController@index')->name('home');
Route::get('/major-category-list', 'FrontEndController@majorCategoryList');
Route::get('/product-list', 'FrontEndController@productList')->name('product_list');
Route::get('/details/{id}', 'FrontEndController@productDetails')->name('product_details');
Route::get('/about', 'FrontEndController@about')->name('about');
Route::get('/contact', 'FrontEndController@contact')->name('contact');
Route::post('/search', 'FrontEndController@search')->name('search');
Route::get('/terms_condition', 'FrontEndController@view_terms_condition')->name('view_terms_condition');
/*
|--------------------------------------------------------------------------
| Web Customer Login, Registration and Logout Routes
|--------------------------------------------------------------------------
*/
Route::get('/customer-login', 'FrontEndController@customerLogin')->name('customer_login');
Route::post('/login-submit', 'FrontEndController@loginSubmit')->name('login_submit');
Route::get('/logout-customer', 'FrontEndController@customerLogout')->name('customer_logout');
Route::post('/customer-registration', 'FrontEndController@customerRegistration')->name('customer_registration');


/*
|--------------------------------------------------------------------------
| Web Customer Dashboard Routes
|--------------------------------------------------------------------------
*/
Route::get('/customerDashboard', 'FrontEndController@customerDashboard')->name('customer_dashboard');
Route::get('/customer-order-list', 'FrontEndController@customerOrderList')->name('customer_order_list');
Route::get('/order-information/{id}', 'FrontEndController@orderInformation')->name('order_information');


/*
|--------------------------------------------------------------------------
| Web Customer Cart Routes
|--------------------------------------------------------------------------
*/
Route::get('/cart', 'FrontEndController@cartPage')->name('cart_page');
Route::get('/checkout', 'FrontEndController@checkout')->name('checkout');
Route::post('/place-order', 'FrontEndController@placeOrder')->name('place_order');