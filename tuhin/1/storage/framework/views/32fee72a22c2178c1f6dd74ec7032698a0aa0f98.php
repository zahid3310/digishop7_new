

<?php $__env->startSection('title', 'Edit Major Category'); ?>

<?php $__env->startSection('content'); ?>
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Major Category</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Ecommerce Content</a></li>
                                <li class="breadcrumb-item active">Major Category</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div>
                                <a href="<?php echo e(route('sliders_index')); ?>" class="btn btn-success float-right">List of Major Category</a>
                                <h4 class="card-title">Edit Major Category</h4>
                            </div>
                            <br>
                            <hr>

                            <?php if(Session::has('success')): ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <?php echo Session::get('success'); ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            <?php if(Session::has('unsuccess')): ?>
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                <?php echo Session::get('unsuccess'); ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            <?php if(Session::has('errors')): ?>
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            <form id="FormSubmit" action="<?php echo e(route('ecommerce_major_category_update',$major_categories->id)); ?>" method="post" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>


                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Name *</label>
                                        <input type="text" class="form-control" value="<?php echo e($major_categories['name']); ?>" placeholder="Enter Name" name="name" />
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Status</label>
                                        <select style="cursor: pointer" class="form-control" value="<?php echo e(old('status')); ?>" name="status">
                                            <option value="1" <?php echo e($major_categories['status'] == 1 ? 'selected' : ''); ?>>Active</option>
                                            <option value="0" <?php echo e($major_categories['status'] == 0 ? 'selected' : ''); ?>>Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-3">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('home')); ?>">Close</a></button>
                                    </div>
                                    <div class="col-md-8"></div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    function preventDoubleClick() {
        $('.enableOnInput').prop('disabled', true)
        $('#FormSubmit').submit();
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/35/Modules/Ecommerce/Resources/views/major_category/edit.blade.php ENDPATH**/ ?>