<div class="widget widget-categories">
	<h3 class="widget-title">Categories</h3>
	<ul class="widget-body filter-items search-ul">

		<li><a href="<?php echo e(route('customer_order_list')); ?>" class="<?php echo e((last(request()->segments()) == 'customer-order-list') ? 'dashboard-menu-active' : ''); ?>"><i class="fas fa-list"></i> Order List</a></li>

		<li><a href="<?php echo e(route('customer_dashboard')); ?>" class="<?php echo e((last(request()->segments()) == 'customerDashboard') ? 'dashboard-menu-active' : ''); ?>"><i class="fas fa-user-circle"></i> Profile</a></li>

		<li><a href="<?php echo e(route('customer_logout')); ?>"><i class="fas fa-sign-in-alt"></i>  Logout</a></li>
	</ul>
</div>

<?php /**PATH /home/digishop7/public_html/35/Modules/FrontEnd/Resources/views/layouts/customer_dashboard_menu.blade.php ENDPATH**/ ?>