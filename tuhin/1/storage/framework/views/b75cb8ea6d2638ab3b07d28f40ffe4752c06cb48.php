
<?php $__env->startSection('styles'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main_body'); ?>
<!-- End Header -->
<main class="main">
            <div class="page-content mt-10 pt-4 pb-4">
                <section class="contact-section">
                    <div class="container">
                        <div class="row">
                          
                                <aside class="col-lg-3 col-md-3 col-xs-3 blog-sidebar sidebar-fixed sticky-sidebar-wrapper">
                            <div class="sidebar-overlay">
                                <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
                            </div>
                            <a href="#" class="sidebar-toggle"><i class="fas fa-chevron-left"></i></a>
                            <div class="sidebar-content">
                                <div class="sticky-sidebar" data-sticky-options="{'top': 89, 'bottom': 70}">
                                   
                                    <?php echo $__env->make('frontend::layouts.customer_dashboard_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                   
                                </div>
                            </div>
                        </aside>
                                
                            <div class="col-lg-9 col-md-9 col-xs-9 grey-section" style="padding: 10px;">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-message alert-light alert-primary alert-link mb-4">
                                    <h4 class="alert-title"><?php echo e(Session::get('success')); ?>.</h4>
                                    <button type="button" class="btn btn-link btn-close">
                                        <i class="d-icon-times"></i>
                                    </button>
                                </div>
                                <script type="text/javascript">
                                    window.localStorage.clear();
                                </script>
                                <?php endif; ?>

                                <table id="example" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Sl</th>
                                            <th>Order Date</th>
                                            <th>Order Number</th>
                                            <th>Order Ammount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody style="text-align: center;">
                                        <?php $__currentLoopData = $orderList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($loop->index+1); ?></td>
                                            <td><?php echo e($item->order_date); ?></td>
                                            <td><?php echo e($item->order_number); ?></td>
                                            <td><?php echo e($item->order_amount); ?></td>
                                            <td>
                                                <a href="<?php echo e(route('order_information', encrypt($item->id))); ?>" target="_blank" class="btn btn-outline btn-primary btn-sm">Order Details</a>
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>

                                </table>
                                
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End About Section-->

            </div>
        </main>
<!-- End Main -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>


<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend::layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/35/Modules/FrontEnd/Resources/views/layouts/customer_order_list.blade.php ENDPATH**/ ?>