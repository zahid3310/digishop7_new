  <!-- End of Main -->
<footer class="footer appear-animate" data-animation-options="{
    'delay': '.3s'
}">
    <div class="container">
        <div class="footer-top">
            <div class="row">
                <div class="col-lg-3">
                    <a href="demo3.html" class="logo-footer">
                        <img src="<?php echo e(url('public/'.userDetails()->logo)); ?>" height="39" />
                    </a>
                    <!-- End of FooterLogo -->
                </div>
                <!-- <div class="col-lg-9">
                    <div class="widget widget-newsletter form-wrapper form-wrapper-inline">
                        <div class="newsletter-info mx-auto mr-lg-2 ml-lg-4">
                            <h4 class="widget-title">Subscribe to our Newsletter</h4>
                            <p>Get all the latest information on Events, Sales and Offers.</p>
                        </div>
                        <form action="#" class="input-wrapper input-wrapper-inline">
                            <input type="email" class="form-control" name="newsletter_email"
                                id="newsletter_email" placeholder="Email address here..." required />
                            <button class="btn btn-grey btn-md ml-2" type="submit">subscribe<i
                                    class="d-icon-arrow-right"></i></button>
                        </form>
                    </div>
                </div> -->
            </div>
        </div>
        <!-- End of FooterTop -->
        <div class="footer-middle">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="widget">
                        <h4 class="widget-title">Contact Info</h4>
                        <ul class="widget-body">
                            <li>
                                <label>Phone:</label>
                                <a href="#"> <?php echo e(userDetails()->contact_number); ?></a>
                            </li>
                            <li>
                                <label>Email:</label>
                                <a href="#"><?php echo e(userDetails()->contact_email); ?>

                                </a>
                            </li>
                            <li>
                                <label>Address:</label>
                                <a href="#"><?php echo e(userDetails()->address); ?></a>
                            </li>
                            <li>
                                <label>WORKING DAYS/HOURS</label>
                            </li>
                            <li>
                                <a href="#">Mon - Sun / 9:00 AM - 8:00 PM</a>
                            </li>
                        </ul>
                    </div>
                    <!-- End of Widget -->
                </div>
                <div class="col-lg-6 col-md-6" style="text-align: center;">
                    <div class="widget ml-lg-4">
                        <h4 class="widget-title">My Account</h4>
                        <ul class="widget-body">
                            <li>
                                <a href="<?php echo e(route('about')); ?>">About Us</a>
                            </li>
                            <li>
                                <a href="#">Order History</a>
                            </li>
                            <!-- <li>
                                <a href="#">Returns</a>
                            </li> -->
                            <li>
                                <a href="#">Custom Service</a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('view_terms_condition')); ?>">Terms &amp; Condition</a>
                            </li>
                        </ul>
                    </div>
                    <!-- End of Widget -->
                </div>
                <!-- <div class="col-lg-3 col-md-6">
                    <div class="widget ml-lg-4">
                        <h4 class="widget-title">Contact Info</h4>
                        <ul class="widget-body">
                            <li>
                                <a href="#">About Us</a>
                            </li>
                            <li>
                                <a href="#">Order History</a>
                            </li>
                            <li>
                                <a href="#">Returns</a>
                            </li>
                            <li>
                                <a href="#">Custom Service</a>
                            </li>
                            <li>
                                <a href="#">Terms &amp; Condition</a>
                            </li>
                        </ul>
                    </div>
            
                </div> -->
                <div class="col-lg-3 col-md-6">
                    <div class="widget widget-instagram">
                        <h4 class="widget-title">Payment Methode</h4>
                        <figure class="widget-body row">
                            <div class="col-4">
                                <img src="<?php echo e(url('public/bkash.png')); ?>" alt="instagram 1" width="64" height="64" />
                            </div>

                            <div class="col-4">
                                <img src="<?php echo e(url('public/nogot.png')); ?>" alt="instagram 1" width="64" height="64" />
                            </div>

                            <div class="col-4">
                                <img src="<?php echo e(url('public/rocket.png')); ?>" alt="instagram 1" width="64" height="64" />
                            </div>
                        </figure>
                    </div>
                    <!-- End of Instagram -->
                </div>
            </div>
        </div>
        <!-- End of FooterMiddle -->
        <div class="footer-bottom">
            <div class="footer-left">
                <figure class="payment">
                    <!-- <img src="images/payment.png" alt="payment" width="159" height="29" /> -->
                </figure>
            </div>
            <div class="footer-center">
                <p class="copyright">
                    ©<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | <a href="https://cyberdynetechnologyltd.com/" target="_blank">Cyberdyne Technology Ltd.</a>
                </p>
            </div>
            <div class="footer-right">
                <div class="social-links">
                    <a href="#" class="social-link social-facebook fab fa-facebook-f"></a>
<!--                     <a href="#" class="social-link social-twitter fab fa-twitter"></a>
                    <a href="#" class="social-link social-linkedin fab fa-linkedin-in"></a> -->
                </div>
            </div>
        </div>
        <!-- End of FooterBottom -->
    </div>
</footer>
<!-- End of Footer --><?php /**PATH /home/digishop7/public_html/35/Modules/FrontEnd/Resources/views/layouts/footer.blade.php ENDPATH**/ ?>