

<?php $__env->startSection('main_body'); ?>
<main class="main mt-lg-4">
    <div class="page-content">
        <div class="container">
            <div class="row main-content-wrap gutter-lg">
                <aside class="col-lg-3 sidebar sidebar-fixed shop-sidebar sticky-sidebar-wrapper">
                    <div class="sidebar-overlay">
                        <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
                    </div>
                    <div class="sidebar-content">
                        <div class="sticky-sidebar">
                            <div class="widget widget-collapsible">
                                <h3 class="widget-title">All Top Categories</h3>
                                <ul class="widget-body filter-items search-ul">
                                    <?php if($majorCategories->count() > 0): ?>
                                    <?php $__currentLoopData = $majorCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $major_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li class="with-ul">
                                        <a href="<?php echo e(route('product_list')); ?>"><?php echo e($major_category['name']); ?></a>
                                        <ul>
                                            <?php if($major_category->product->count() > 0): ?>
                                            <?php $__currentLoopData = $major_category->product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><a href="<?php echo e(route('product_list')); ?>"><?php echo e($category['name']); ?></a></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </ul>
                                    </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </aside>

                <div class="col-xl-9 col-lg-8">
                    <main class="main mt-4">
                        <div class="page-content mb-10">
                            <div class="container">
                                <div class="product product-single row mb-4">
                                    <div class="col-md-6">
                                        <div class="product-gallery pg-vertical">
                                            <div class="product-single-carousel owl-carousel owl-theme owl-nav-inner row cols-1">
                                                <?php if($product_images->count() > 0): ?>
                                                <?php $__currentLoopData = $product_images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product_image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <figure class="product-image">
                                                    <img src="<?php echo e(url('public/images/products/'.$product_image->image)); ?>"
                                                        alt="Product Image" width="800" height="900">
                                                </figure>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </div>

                                            <div class="product-thumbs-wrap">
                                                <div class="product-thumbs">
                                                    <?php if($product_images->count() > 0): ?>
                                                    <?php $__currentLoopData = $product_images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $product_image1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="product-thumb active">
                                                        <img src="<?php echo e(url('public/images/products/'.$product_image1->image)); ?>" alt="Product Image"
                                                            width="109" height="122">
                                                    </div>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </div>
                                                <button class="thumb-up disabled"><i class="fas fa-chevron-left"></i></button>
                                                <button class="thumb-down disabled"><i class="fas fa-chevron-right"></i></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="product-details">
                                            <h1 class="product-name"><?php echo e($find_product->name != null ? $find_product->name : ''); ?></h1>
                                            <div class="product-meta">
                                                <?php if($find_product->product_code != null): ?>
                                                SKU: <span class="product-sku"><?php echo e($find_product->product_code); ?></span>
                                                <?php endif; ?>
                                                <?php if($find_product->brand_id != null): ?>
                                                BRAND: <span class="product-brand"><?php echo e($find_product->brand->name); ?> </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="product-price"><?php echo e('BDT ' . $find_product->sell_price); ?></div>
                                            
                                            <p class="product-short-desc"><?php echo e($find_product->description); ?></p>

                                            <hr class="product-divider">

                                            <div class="product-form product-qty">
                                                <label>QTY:</label>
                                                <div class="product-form-group">
                                                    <div class="input-group">
                                                        <button class="quantity-minus d-icon-minus"></button>
                                                        <input class="quantity form-control" type="number" min="1" max="1000000">
                                                        <button class="quantity-plus d-icon-plus"></button>
                                                    </div>
                                                    <button class="btn-product btn-cart addItemToCart" data-id="<?php echo e($find_product->id); ?>" data-thumb="<?php echo e(!empty(productImage($find_product['id'])) ? url('public/images/products/'.productImage($find_product['id'])->image) : url('public/default-product-image.jpeg')); ?>" data-title="<?php echo e($find_product->name); ?>" data-price="<?php echo e($find_product->sell_price); ?>" data-url="<?php echo e(route('product_details', $find_product->id)); ?>" data-quantity="1" data-toggle="modal" href="javascript:void(0)" title="Add to cart">Add To Cart</button>
                                                </div>
                                            </div>

                                            <hr class="product-divider mb-3">

                                            <div class="product-footer">
                                                <div class="social-links">
                                                    <a href="#" class="social-link social-facebook fab fa-facebook-f"></a>
                                                    <!-- <a href="#" class="social-link social-twitter fab fa-twitter"></a>
                                                    <a href="#" class="social-link social-vimeo fab fa-vimeo-v"></a> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
            </div>

            <section>
                <h2 class="title">Our Featured</h2>

                <div class="owl-carousel owl-theme owl-nav-full row cols-2 cols-md-3 cols-lg-4"
                    data-owl-options="{
                    'items': 5,
                    'nav': false,
                    'loop': false,
                    'dots': true,
                    'margin': 20,
                    'responsive': {
                        '0': {
                            'items': 2
                        },
                        '768': {
                            'items': 3
                        },
                        '992': {
                            'items': 4,
                            'dots': false,
                            'nav': true
                        }
                    }
                }">
                    <?php if($products->count() > 0): ?>
                    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="product shadow-media">
                        <figure class="product-media">
                            <a href="<?php echo e(route('product_details', $product->id)); ?>">
                                <?php if(!empty(productImage($product['id']))): ?>
                                <img src="<?php echo e(url('public/images/products/'.productImage($product['id'])->image)); ?>" alt="product"
                                    width="280" height="315">
                                <?php else: ?>
                                <img src="<?php echo e(url('public/front_end_asset/images/demos/demo3/products/1.jpg')); ?>" alt="product"
                                    width="280" height="315">
                                <?php endif; ?>
                            </a>
                            <!-- <div class="product-label-group">
                                <label class="product-label label-new">new</label>
                            </div>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                    data-target="#addCartModal" title="Add to cart"><i
                                        class="d-icon-bag"></i></a>
                            </div> -->
                            <div class="product-action">
                                <a class="btn-product btn-cart addItemToCart"
                                        data-id="<?php echo e($product->id); ?>" data-thumb="<?php echo e(!empty(productImage($product['id'])) ? url('public/images/products/'.productImage($product['id'])->image) : url('public/default-product-image.jpeg')); ?>" data-title="<?php echo e($product->name); ?>" data-price="<?php echo e($product->sell_price); ?>" data-url="<?php echo e(route('product_details', $product->id)); ?>" data-quantity="1" data-toggle="modal" href="javascript:void(0)" title="Add to cart">ADD TO CART</a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <div class="product-cat">
                                <a href=""><?php echo e($product->major_category_id != null ? $product->product->name : ''); ?></a>
                            </div>
                            <h3 class="product-name">
                                <a href="product.html"><?php echo e($product->name); ?></a>
                            </h3>
                            <div class="product-price">
                                <ins class="new-price"><?php echo e('BDT '. $product->sell_price); ?></ins><del class="old-price"><?php echo e($product->old_price != null ? 'BDT ' .$product->old_price : ''); ?></del>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </section>

            <br>
            <br>
            <br>
        </div>
    </div>
</main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend::layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/35/Modules/FrontEnd/Resources/views/product_details.blade.php ENDPATH**/ ?>