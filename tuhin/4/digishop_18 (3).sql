-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 10, 2021 at 10:08 PM
-- Server version: 5.7.33
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digishop_18`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_transactions`
--

CREATE TABLE `account_transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transaction_date` date NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `paid_through_id` bigint(20) UNSIGNED DEFAULT NULL,
  `account_information` text COLLATE utf8_unicode_ci,
  `note` text COLLATE utf8_unicode_ci,
  `amount` double NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '0 = In 1 = Out 2 = transfer',
  `transfer_from` bigint(20) UNSIGNED DEFAULT NULL,
  `transfer_to` bigint(20) UNSIGNED DEFAULT NULL,
  `transaction_head` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `associated_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backup`
--

CREATE TABLE `backup` (
  `id` bigint(20) NOT NULL,
  `file_url` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `bill_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bill_date` date NOT NULL,
  `vendor_id` bigint(20) UNSIGNED NOT NULL,
  `bill_amount` double NOT NULL,
  `due_amount` double NOT NULL,
  `return_amount` double DEFAULT NULL,
  `total_discount` double NOT NULL,
  `adjustment_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `total_adjustment` double DEFAULT NULL,
  `total_tax` double DEFAULT NULL,
  `tax_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `vat_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `total_vat` double DEFAULT NULL,
  `adjustment_note` text COLLATE utf8mb4_unicode_ci,
  `bill_note` text COLLATE utf8mb4_unicode_ci,
  `total_discount_type` tinyint(4) DEFAULT NULL,
  `total_discount_amount` double DEFAULT NULL,
  `total_discount_note` text COLLATE utf8mb4_unicode_ci,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cash_given` double DEFAULT NULL,
  `change_amount` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bill_entries`
--

CREATE TABLE `bill_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bill_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` bigint(20) UNSIGNED NOT NULL,
  `main_unit_id` bigint(20) UNSIGNED DEFAULT NULL,
  `conversion_unit_id` bigint(20) UNSIGNED DEFAULT NULL,
  `rate` double NOT NULL,
  `quantity` double NOT NULL DEFAULT '1',
  `total_amount` double NOT NULL,
  `discount_type` tinyint(4) NOT NULL COMMENT '0= % 1= BDT',
  `discount_amount` double NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=active 0=inactive',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `status`, `branch_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(17, 'MIR CERAMIC', 1, NULL, 5, NULL, '2021-03-01 01:51:27', '2021-03-01 01:51:27');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci,
  `email` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `nid_number` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `nid` text COLLATE utf8mb4_unicode_ci,
  `certificate` text COLLATE utf8mb4_unicode_ci,
  `alternative_contact` text COLLATE utf8mb4_unicode_ci,
  `contact_type` tinyint(4) NOT NULL COMMENT '0= customer 1= Supplier 2= Employee 3= Reference',
  `joining_date` date DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary` double DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `opening_balance` double DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `organization_name`, `phone`, `email`, `address`, `nid_number`, `image`, `nid`, `certificate`, `alternative_contact`, `contact_type`, `joining_date`, `designation`, `salary`, `user_id`, `branch_id`, `opening_balance`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Walk-In Customer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-01-03', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2021-01-03 07:38:36', '2021-01-03 07:38:36'),
(2, 'Walk-In Supplier', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-03', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2021-01-03 07:38:49', '2021-01-03 07:38:49'),
(42, 'Zahid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2021-03-10 20:42:26', '2021-03-10 20:42:26');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '0=coupons 1=membership',
  `coupon_code` bigint(20) DEFAULT NULL,
  `card_number` double DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_date` date NOT NULL,
  `expire_date` date NOT NULL,
  `discount_type` tinyint(4) NOT NULL COMMENT '0= % 1= BDT',
  `discount_amount` double NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL COMMENT '0=inactive 1=active',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `discount_products`
--

CREATE TABLE `discount_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `discount_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `expense_category_id` bigint(20) UNSIGNED NOT NULL,
  `expense_date` date NOT NULL,
  `expense_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `salary_type` tinyint(4) DEFAULT NULL COMMENT '0=Salary 1=Service Charge',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `paid_through_id` bigint(20) UNSIGNED DEFAULT NULL,
  `account_information` text COLLATE utf8mb4_unicode_ci,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expense_categories`
--

CREATE TABLE `expense_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expense_categories`
--

INSERT INTO `expense_categories` (`id`, `name`, `branch_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Employee Salary', NULL, 1, NULL, '2021-01-03 07:43:18', '2021-01-03 07:43:18'),
(2, 'Product Purchase', NULL, 1, NULL, '2021-01-03 07:43:30', '2021-01-03 07:43:30');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `incomes`
--

CREATE TABLE `incomes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `income_category_id` bigint(20) UNSIGNED NOT NULL,
  `income_date` date NOT NULL,
  `income_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `paid_through_id` bigint(20) UNSIGNED DEFAULT NULL,
  `account_information` text COLLATE utf8_unicode_ci,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `income_categories`
--

CREATE TABLE `income_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `invoice_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_date` date NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `reference_id` bigint(20) UNSIGNED DEFAULT NULL,
  `customer_name` text COLLATE utf8mb4_unicode_ci,
  `customer_phone` text COLLATE utf8mb4_unicode_ci,
  `customer_address` text COLLATE utf8mb4_unicode_ci,
  `invoice_amount` double NOT NULL,
  `due_amount` double NOT NULL,
  `return_amount` double DEFAULT NULL,
  `total_buy_price` double NOT NULL,
  `total_discount` double NOT NULL,
  `total_adjustment` double DEFAULT NULL,
  `total_tax` double DEFAULT NULL,
  `tax_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `vat_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `total_vat` double DEFAULT NULL,
  `adjustment_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `adjustment_note` text COLLATE utf8mb4_unicode_ci,
  `invoice_note` text COLLATE utf8mb4_unicode_ci,
  `discount_code` double DEFAULT NULL,
  `total_discount_type` tinyint(4) DEFAULT NULL,
  `total_discount_amount` double DEFAULT NULL,
  `total_discount_note` text COLLATE utf8mb4_unicode_ci,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `cash_given` double DEFAULT NULL,
  `change_amount` double DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `type`, `invoice_number`, `invoice_date`, `customer_id`, `reference_id`, `customer_name`, `customer_phone`, `customer_address`, `invoice_amount`, `due_amount`, `return_amount`, `total_buy_price`, `total_discount`, `total_adjustment`, `total_tax`, `tax_type`, `vat_type`, `total_vat`, `adjustment_type`, `adjustment_note`, `invoice_note`, `discount_code`, `total_discount_type`, `total_discount_amount`, `total_discount_note`, `branch_id`, `cash_given`, `change_amount`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(72, 1, '1', '2021-02-28', 1, NULL, NULL, NULL, NULL, 7360, 7360, NULL, 117760, 0, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 5000, 0, 5, 1, '2021-03-01 02:22:29', '2021-03-07 02:25:51'),
(73, 1, '2', '2021-03-09', 1, NULL, NULL, NULL, NULL, 552, 552, NULL, 1472, 0, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, 0, 5, NULL, '2021-03-10 02:36:14', '2021-03-10 02:36:14');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_entries`
--

CREATE TABLE `invoice_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `main_unit_id` bigint(20) UNSIGNED NOT NULL,
  `conversion_unit_id` bigint(20) UNSIGNED NOT NULL,
  `reference_id` bigint(20) UNSIGNED DEFAULT NULL,
  `buy_price` double NOT NULL,
  `rate` double NOT NULL,
  `quantity` double NOT NULL DEFAULT '1',
  `total_amount` double NOT NULL,
  `discount_type` tinyint(4) NOT NULL COMMENT '0= % 1= BDT',
  `discount_amount` double NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_entries`
--

INSERT INTO `invoice_entries` (`id`, `invoice_id`, `product_id`, `product_entry_id`, `customer_id`, `main_unit_id`, `conversion_unit_id`, `reference_id`, `buy_price`, `rate`, `quantity`, `total_amount`, `discount_type`, `discount_amount`, `branch_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(186, 72, 27, 321, 1, 1, 8, NULL, 1472, 92, 80, 7360, 0, 0, NULL, 1, NULL, NULL, NULL),
(187, 73, 27, 321, 1, 11, 10, NULL, 1472, 92, 1, 92, 0, 0, NULL, 5, NULL, '2021-03-10 02:36:14', NULL),
(188, 73, 27, 322, 1, 11, 10, NULL, 1472, 92, 0, 460, 0, 0, NULL, 5, NULL, '2021-03-10 02:36:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sub_category_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=active 0=inactive',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `message_deliveries`
--

CREATE TABLE `message_deliveries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `message_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `phone_book_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `message_lists`
--

CREATE TABLE `message_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Product Categories', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Products', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Orders', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Purchases', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Sales Return', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Purchase Return', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Expenses', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Payments', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Contacts', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Paid Through Accounts', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Units', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Employee Salary', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Bar Code Print', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Discounts', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Reports', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Users', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Access Level', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `modules_access`
--

CREATE TABLE `modules_access` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `module_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `display` tinyint(4) NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules_access`
--

INSERT INTO `modules_access` (`id`, `module_id`, `user_id`, `display`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(2, 2, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(3, 3, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(4, 4, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(5, 5, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(6, 6, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(7, 7, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(8, 8, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(9, 9, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(10, 10, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(11, 11, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(12, 12, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(13, 13, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(14, 14, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(15, 15, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(16, 16, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(17, 17, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(18, 1, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(19, 2, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(20, 3, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(21, 4, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(22, 5, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(23, 6, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(24, 7, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(25, 8, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(26, 9, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(27, 10, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(28, 11, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(29, 12, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(30, 13, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(31, 14, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(32, 15, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(33, 16, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(34, 17, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(52, 1, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(53, 2, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(54, 3, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(55, 4, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(56, 5, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(57, 6, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(58, 7, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(59, 8, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(60, 9, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(61, 10, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(62, 11, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(63, 12, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(64, 13, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(65, 14, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(66, 15, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(67, 16, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(68, 17, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `paid_through_accounts`
--

CREATE TABLE `paid_through_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `paid_through_accounts`
--

INSERT INTO `paid_through_accounts` (`id`, `name`, `description`, `branch_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'CASH', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `payment_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_date` date NOT NULL,
  `amount` double NOT NULL,
  `paid_through` bigint(20) UNSIGNED NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `account_information` text COLLATE utf8mb4_unicode_ci,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '0= invoice 1= bill 2= sales return 3= purchase return',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_entries`
--

CREATE TABLE `payment_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `payment_id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) UNSIGNED DEFAULT NULL,
  `bill_id` bigint(20) UNSIGNED DEFAULT NULL,
  `sales_return_id` bigint(20) UNSIGNED DEFAULT NULL,
  `purchase_return_id` bigint(20) UNSIGNED DEFAULT NULL,
  `amount` double NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `url_id` bigint(20) UNSIGNED NOT NULL,
  `access_level` tinyint(4) NOT NULL COMMENT '0= Not Allowed 1= Allowed',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `user_id`, `url_id`, `access_level`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 85, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(2, 1, 86, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(3, 1, 87, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(4, 1, 88, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(5, 1, 89, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(6, 1, 90, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(7, 1, 91, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(8, 1, 92, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(9, 1, 93, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(10, 1, 94, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(11, 1, 95, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(12, 1, 96, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(13, 1, 97, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(14, 1, 98, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(15, 1, 99, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(16, 1, 100, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(17, 1, 101, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(18, 1, 102, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(19, 1, 103, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(20, 1, 104, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(21, 1, 105, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(22, 1, 106, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(23, 1, 107, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(24, 1, 108, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(25, 1, 109, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(26, 1, 110, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(27, 1, 111, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(28, 1, 112, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(29, 1, 113, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(30, 1, 114, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(31, 1, 115, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(32, 1, 116, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(33, 1, 117, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(34, 1, 118, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(35, 1, 119, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(36, 1, 120, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(37, 1, 121, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(38, 1, 122, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(39, 1, 123, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(40, 1, 124, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(41, 1, 125, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(42, 1, 126, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(43, 1, 127, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(44, 1, 128, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(45, 1, 129, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(46, 1, 130, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(47, 1, 131, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(48, 1, 132, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(49, 1, 133, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(50, 1, 134, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(51, 1, 135, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(52, 1, 136, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(53, 1, 137, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(54, 1, 138, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(55, 1, 139, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(56, 1, 140, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(57, 1, 141, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(58, 1, 142, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(59, 1, 143, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(60, 1, 144, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(61, 1, 145, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(62, 1, 146, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(63, 1, 147, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(64, 1, 148, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(65, 1, 149, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(66, 1, 150, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(67, 1, 151, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(68, 1, 152, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(69, 1, 153, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(70, 1, 154, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(71, 1, 155, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(72, 1, 156, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(73, 1, 157, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(74, 1, 158, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(75, 1, 159, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(76, 1, 160, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(77, 1, 161, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(78, 1, 162, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(79, 1, 163, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(80, 1, 164, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(81, 1, 165, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(82, 1, 166, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(83, 1, 167, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(84, 1, 168, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(85, 1, 169, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(86, 2, 85, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(87, 2, 86, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(88, 2, 87, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(89, 2, 88, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(90, 2, 89, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(91, 2, 90, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(92, 2, 91, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(93, 2, 92, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(94, 2, 93, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(95, 2, 94, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(96, 2, 95, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(97, 2, 96, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(98, 2, 97, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(99, 2, 98, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(100, 2, 99, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(101, 2, 100, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(102, 2, 101, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(103, 2, 102, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(104, 2, 103, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(105, 2, 104, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(106, 2, 105, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(107, 2, 106, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(108, 2, 107, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(109, 2, 108, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(110, 2, 109, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(111, 2, 110, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(112, 2, 111, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(113, 2, 112, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(114, 2, 113, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(115, 2, 114, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(116, 2, 115, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(117, 2, 116, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(118, 2, 117, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(119, 2, 118, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(120, 2, 119, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(121, 2, 120, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(122, 2, 121, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(123, 2, 122, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(124, 2, 123, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(125, 2, 124, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(126, 2, 125, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(127, 2, 126, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(128, 2, 127, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(129, 2, 128, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(130, 2, 129, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(131, 2, 130, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(132, 2, 131, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(133, 2, 132, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(134, 2, 133, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(135, 2, 134, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(136, 2, 135, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(137, 2, 136, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(138, 2, 137, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(139, 2, 138, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(140, 2, 139, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(141, 2, 140, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(142, 2, 141, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(143, 2, 142, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(144, 2, 143, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(145, 2, 144, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(146, 2, 145, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(147, 2, 146, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(148, 2, 147, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(149, 2, 148, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(150, 2, 149, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(151, 2, 150, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(152, 2, 151, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(153, 2, 152, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(154, 2, 153, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(155, 2, 154, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(156, 2, 155, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(157, 2, 156, 0, 1, NULL, '2021-01-20 10:59:35', '2021-01-20 11:03:20'),
(158, 2, 157, 0, 1, NULL, '2021-01-20 10:59:35', '2021-01-20 11:03:20'),
(159, 2, 158, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(160, 2, 159, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(161, 2, 160, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(162, 2, 161, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(163, 2, 162, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(164, 2, 163, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(165, 2, 164, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(166, 2, 165, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(167, 2, 166, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(168, 2, 167, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(169, 2, 168, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(170, 2, 169, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(256, 5, 85, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(257, 5, 86, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(258, 5, 87, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(259, 5, 88, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(260, 5, 89, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(261, 5, 90, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(262, 5, 91, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(263, 5, 92, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(264, 5, 93, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(265, 5, 94, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(266, 5, 95, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(267, 5, 96, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(268, 5, 97, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(269, 5, 98, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(270, 5, 99, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(271, 5, 100, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(272, 5, 101, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(273, 5, 102, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(274, 5, 103, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(275, 5, 104, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(276, 5, 105, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(277, 5, 106, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(278, 5, 107, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(279, 5, 108, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(280, 5, 109, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(281, 5, 110, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(282, 5, 111, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(283, 5, 112, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(284, 5, 113, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(285, 5, 114, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(286, 5, 115, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(287, 5, 116, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(288, 5, 117, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(289, 5, 118, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(290, 5, 119, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(291, 5, 120, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(292, 5, 121, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(293, 5, 122, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(294, 5, 123, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(295, 5, 124, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(296, 5, 125, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(297, 5, 126, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(298, 5, 127, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(299, 5, 128, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(300, 5, 129, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(301, 5, 130, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(302, 5, 131, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(303, 5, 132, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(304, 5, 133, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(305, 5, 134, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(306, 5, 135, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(307, 5, 136, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(308, 5, 137, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(309, 5, 138, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(310, 5, 139, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(311, 5, 140, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(312, 5, 141, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(313, 5, 142, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(314, 5, 143, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(315, 5, 144, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(316, 5, 145, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(317, 5, 146, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(318, 5, 147, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(319, 5, 148, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(320, 5, 149, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(321, 5, 150, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(322, 5, 151, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(323, 5, 152, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(324, 5, 153, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(325, 5, 154, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(326, 5, 155, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(327, 5, 156, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(328, 5, 157, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(329, 5, 158, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(330, 5, 159, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(331, 5, 160, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(332, 5, 161, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(333, 5, 162, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(334, 5, 163, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(335, 5, 164, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(336, 5, 165, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(337, 5, 166, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(338, 5, 167, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(339, 5, 168, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(340, 5, 169, 1, 1, NULL, '2021-01-31 10:39:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `phone_book`
--

CREATE TABLE `phone_book` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `sub_category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `item_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_code` text COLLATE utf8mb4_unicode_ci,
  `stock_in_hand` double DEFAULT NULL,
  `total_sold` double DEFAULT NULL,
  `unit` bigint(20) UNSIGNED DEFAULT NULL,
  `buy_price` double DEFAULT NULL,
  `sell_price` double DEFAULT NULL,
  `total_purchase_return` double DEFAULT NULL,
  `total_sales_return` double DEFAULT NULL,
  `total_damage` double DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=active 0=inactive',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `sub_category_id`, `item_id`, `name`, `product_code`, `stock_in_hand`, `total_sold`, `unit`, `buy_price`, `sell_price`, `total_purchase_return`, `total_sales_return`, `total_damage`, `image`, `status`, `branch_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(27, NULL, NULL, NULL, 'Floor Tiles', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, 5, '2021-03-01 00:58:15', '2021-03-01 22:35:32'),
(28, NULL, NULL, NULL, 'Wall Tiles', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, NULL, '2021-03-01 22:02:50', '2021-03-01 22:02:50'),
(29, NULL, NULL, NULL, 'Stairs Tiles', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, NULL, '2021-03-01 22:03:18', '2021-03-01 22:03:18'),
(30, NULL, NULL, NULL, 'Cladding Tiles', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, NULL, '2021-03-01 22:04:26', '2021-03-01 22:04:26'),
(31, NULL, NULL, NULL, 'Parking Tiles', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, NULL, '2021-03-01 22:06:37', '2021-03-01 22:06:37'),
(32, NULL, NULL, NULL, 'Putting', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, 5, '2021-03-01 22:06:52', '2021-03-01 22:35:03'),
(33, NULL, NULL, NULL, 'VIXAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, NULL, '2021-03-01 22:07:03', '2021-03-01 22:07:03'),
(34, NULL, NULL, NULL, 'Scenery Tiles', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, NULL, '2021-03-01 22:07:43', '2021-03-01 22:07:43'),
(35, NULL, NULL, NULL, 'Marking Cloths (কাপড়)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, 5, '2021-03-01 22:08:50', '2021-03-01 22:34:39');

-- --------------------------------------------------------

--
-- Table structure for table `product_customers`
--

CREATE TABLE `product_customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `whole_sale_price` double DEFAULT NULL,
  `retail_price` double DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_entries`
--

CREATE TABLE `product_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `sub_category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `supplier_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_code` text COLLATE utf8mb4_unicode_ci,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock_in_hand` double DEFAULT NULL,
  `opening_stock` double DEFAULT NULL,
  `total_sold` double DEFAULT '0',
  `buy_price` double DEFAULT NULL,
  `sell_price` double DEFAULT NULL,
  `total_purchase_return` double DEFAULT NULL,
  `total_sales_return` double DEFAULT NULL,
  `total_damage` double DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_id` bigint(20) UNSIGNED DEFAULT NULL,
  `alert_quantity` double DEFAULT NULL,
  `tax_type` tinyint(4) DEFAULT NULL COMMENT '1=Inclusive 2=Exclusive',
  `selling_price_exclusive_tax` double DEFAULT NULL,
  `vat_percentage` double DEFAULT NULL,
  `service_charge` double DEFAULT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=active 0=inactive',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_type` tinyint(4) NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_entries`
--

INSERT INTO `product_entries` (`id`, `product_id`, `sub_category_id`, `brand_id`, `supplier_id`, `product_code`, `name`, `stock_in_hand`, `opening_stock`, `total_sold`, `buy_price`, `sell_price`, `total_purchase_return`, `total_sales_return`, `total_damage`, `image`, `unit_id`, `alert_quantity`, `tax_type`, `selling_price_exclusive_tax`, `vat_percentage`, `service_charge`, `status`, `branch_id`, `product_type`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(321, 27, NULL, 17, NULL, 'GMP 6600', 'Glazed Mirror Polish', 99.9375, NULL, 81, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-01 01:42:38', '2021-03-10 02:36:14'),
(322, 27, NULL, 17, NULL, 'GMP 6603', 'Glazed Mirror Polish', 0, 100, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-01 03:50:39', '2021-03-01 22:00:45'),
(323, 27, NULL, 17, NULL, 'GMP 6604', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 16:54:11', '2021-03-02 17:06:14'),
(324, 27, NULL, 17, NULL, 'GMP 6605', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 17:30:28', '2021-03-02 17:31:09'),
(325, 27, NULL, 17, NULL, 'GMP 6608', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 17:32:29', '2021-03-02 17:32:57'),
(326, 27, NULL, 17, NULL, 'GMP 6608BR', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 17:42:52', '2021-03-02 17:43:20'),
(327, 27, NULL, 17, NULL, 'GMP 6609', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 17:44:38', '2021-03-02 17:45:27'),
(328, 27, NULL, 17, NULL, 'GMP 6612', 'Glazed Mirror Polish', NULL, NULL, 0, 1680, 1680, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 18:31:54', '2021-03-02 19:17:04'),
(329, 27, NULL, 17, NULL, 'GMP 6613', 'Glazed Mirror Polish', NULL, NULL, 0, 1680, 1680, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 18:34:20', '2021-03-02 19:19:21'),
(330, 27, NULL, 17, NULL, 'GMP 6614', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 18:38:01', '2021-03-02 18:43:23'),
(331, 27, NULL, 17, NULL, 'GMP 6614 BR', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 18:44:43', '2021-03-02 18:44:43'),
(332, 27, NULL, 17, NULL, 'GMP 6617', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 18:46:22', '2021-03-02 18:46:58'),
(333, 27, NULL, 17, NULL, 'GMP 6619', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 18:48:45', '2021-03-02 18:49:08'),
(334, 27, NULL, 17, NULL, 'GMP 6621', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 18:49:47', '2021-03-02 18:50:51'),
(335, 27, NULL, 17, NULL, 'GMP 6623', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 19:03:53', '2021-03-02 19:32:38'),
(336, 27, NULL, 17, NULL, 'GMP 6622', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 19:36:01', '2021-03-02 19:37:49'),
(337, 27, NULL, 17, NULL, 'GMP 6624', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 19:45:10', '2021-03-02 19:47:54'),
(338, 27, NULL, 17, NULL, 'GMP 6628', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 19:50:02', '2021-03-02 19:50:20'),
(339, 27, NULL, 17, NULL, 'GMP 6629', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 19:51:44', '2021-03-02 19:52:12'),
(340, 27, NULL, 17, NULL, 'GMP 6630', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 19:53:09', '2021-03-02 19:53:44'),
(341, 27, NULL, 17, NULL, 'GMP 6631', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 19:57:57', '2021-03-02 19:58:17'),
(342, 27, NULL, 17, NULL, 'GMP 6632', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:03:55', '2021-03-02 20:04:19'),
(343, 27, NULL, 17, NULL, 'GMP 6633', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:07:15', '2021-03-02 20:08:08'),
(344, 27, NULL, 17, NULL, 'GMP 6634', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:09:02', '2021-03-02 20:09:28'),
(345, 27, NULL, 17, NULL, 'GMP 6636', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:11:05', '2021-03-02 20:13:41'),
(346, 27, NULL, 17, NULL, 'GMP 6638', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:14:35', '2021-03-02 20:15:12'),
(347, 27, NULL, 17, NULL, 'GMP 6643', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:20:25', '2021-03-02 20:20:51'),
(348, 27, NULL, 17, NULL, 'GMP 6645', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:28:39', '2021-03-02 20:29:10'),
(349, 27, NULL, 17, NULL, 'GMP 6646', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:30:32', '2021-03-02 20:31:00'),
(350, 27, NULL, 17, NULL, 'GMP 6647JU', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:31:58', '2021-03-02 20:32:35'),
(351, 27, NULL, 17, NULL, 'GMP 6647JD', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:33:39', '2021-03-02 20:34:18'),
(352, 27, NULL, 17, NULL, 'GMP 6648', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:36:39', '2021-03-02 20:37:05'),
(353, 27, NULL, 17, NULL, 'GMP 6652', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:37:55', '2021-03-02 20:38:22'),
(354, 27, NULL, 17, NULL, 'GMP 6653', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:39:31', '2021-03-02 20:40:03'),
(355, 27, NULL, 17, NULL, 'GMP 6654', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:41:13', '2021-03-02 20:41:43'),
(356, 27, NULL, 17, NULL, 'GMP 6655', 'Glazed Mirror Polish', NULL, NULL, 0, 1472, 1472, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:43:08', '2021-03-02 20:43:37'),
(357, 27, NULL, 17, NULL, 'CMP 66101', 'Crystal Mirror Polish', NULL, NULL, 0, 1520, 1520, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 20:50:01', '2021-03-02 20:52:00'),
(358, 27, NULL, 17, NULL, 'CMP 66102', 'Crystal Mirror Polish', NULL, NULL, 0, 1520, 1520, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 21:11:37', '2021-03-02 21:12:29'),
(359, 27, NULL, 17, NULL, 'CMP 66103', 'Crystal Mirror Polish', NULL, NULL, 0, 1520, 1520, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-02 21:15:26', '2021-03-02 21:15:58'),
(360, 27, NULL, 17, NULL, 'RCM 6601', 'Rock Carving Matt', NULL, NULL, 0, 1312, 1440, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 01:24:40', '2021-03-03 01:26:19'),
(361, 27, NULL, 17, NULL, 'RCM 6602', 'Rock Carving Matt', NULL, NULL, 0, 1312, 1440, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 01:27:56', '2021-03-03 01:28:29'),
(362, 27, NULL, 17, NULL, 'RCM 6603', 'Rock Carving Matt', NULL, NULL, 0, 1312, 1440, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 01:29:41', '2021-03-03 01:30:15'),
(363, 27, NULL, 17, NULL, 'RCM 6604', 'Rock Carving Matt', NULL, NULL, 0, 1312, 1440, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 01:33:03', '2021-03-03 01:35:18'),
(364, 27, NULL, 17, NULL, 'RCM 6605', 'Rock Carving Matt', NULL, NULL, 0, 1312, 1440, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 01:41:28', '2021-03-03 01:42:48'),
(365, 27, NULL, 17, NULL, 'GRCM 6651', 'Rock Carving Matt', NULL, NULL, 0, 1312, 1440, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 01:46:35', '2021-03-03 01:48:03'),
(366, 27, NULL, 17, NULL, 'PDG 6601', 'Porcelain Digital Print', NULL, NULL, 0, 1280, 1520, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 02:31:39', '2021-03-03 02:32:29'),
(367, 27, NULL, 17, NULL, 'PDG 6602', 'Porcelain Digital Print', NULL, NULL, 0, 1280, 1520, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 02:34:19', '2021-03-03 02:35:05'),
(368, 27, NULL, 17, NULL, 'PDG 6603', 'Porcelain Digital Print', NULL, NULL, 0, 1280, 1520, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 02:37:07', '2021-03-03 02:38:01'),
(369, 27, NULL, 17, NULL, 'PDG 6604', 'Porcelain Digital Print', NULL, NULL, 0, 1280, 1520, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 02:39:44', '2021-03-03 02:42:39'),
(370, 27, NULL, 17, NULL, 'PDG 6605', 'Porcelain Digital Print', NULL, NULL, 0, 1280, 1520, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 02:46:39', '2021-03-03 02:47:11'),
(371, 27, NULL, 17, NULL, 'PDG 6606', 'Porcelain Digital Print', NULL, NULL, 0, 1280, 1520, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 02:48:54', '2021-03-03 02:49:49'),
(372, 27, NULL, 17, NULL, 'PDG 6607', 'Porcelain Digital Print', NULL, NULL, 0, 1280, 1520, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 02:51:02', '2021-03-07 20:42:00'),
(373, 27, NULL, 17, NULL, 'G 6600', 'Homogeneous', NULL, NULL, 0, 1248, 1280, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 02:52:41', '2021-03-03 03:01:49'),
(374, 27, NULL, 17, NULL, 'G 6604', 'Homogeneous', NULL, NULL, 0, 1248, 1280, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 03:04:03', '2021-03-03 03:05:23'),
(375, 27, NULL, 17, NULL, 'G 6607', 'Homogeneous', NULL, NULL, 0, 1248, 1820, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 03:18:33', '2021-03-03 03:19:54'),
(376, 27, NULL, 17, NULL, 'G 6608', 'Homogeneous', NULL, NULL, 0, 1248, 1280, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 03:21:50', '2021-03-03 03:22:35'),
(377, 27, NULL, 17, NULL, 'G 6609', 'Homogeneous', NULL, NULL, 0, 1248, 1280, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 03:24:52', '2021-03-03 03:26:28'),
(378, 27, NULL, 17, NULL, 'MP 6600', 'Mirror Polish', NULL, NULL, 0, 1440, 1440, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-03 22:02:14', '2021-03-03 22:05:45'),
(379, 27, NULL, 17, NULL, 'RCM 4401', 'Rock Carving Matt', NULL, NULL, 0, 1008, 1104, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 01:27:21', '2021-03-04 01:27:56'),
(380, 27, NULL, 17, NULL, 'RCM 4402', 'Rock Carving Matt', NULL, NULL, 0, 1008, 1104, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 01:29:27', '2021-03-04 01:30:53'),
(381, 27, NULL, 17, NULL, 'RCM 4403', 'Rock Carving Matt', NULL, NULL, 0, 1008, 1104, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 01:40:57', '2021-03-04 01:41:48'),
(382, 27, NULL, 17, NULL, 'RCM 4404', 'Rock Carving Matt', NULL, NULL, 0, 1008, 1104, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 01:46:25', '2021-03-04 01:47:41'),
(383, 27, NULL, 17, NULL, 'RCM 4405', 'Rock Carving Matt', NULL, NULL, 0, 1008, 1104, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 01:48:55', '2021-03-04 01:49:45'),
(384, 27, NULL, 17, NULL, 'RCM 4406', 'Rock Carving Matt', NULL, NULL, 0, 1008, 1104, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 01:51:29', '2021-03-04 01:52:49'),
(385, 27, NULL, 17, NULL, 'PDG 4459', 'Porcelain Digital Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:11:06', '2021-03-04 02:11:37'),
(386, 27, NULL, 17, NULL, 'PDG 4461', 'Porcelain Digital Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:13:05', '2021-03-04 02:13:45'),
(387, 27, NULL, 17, NULL, 'PDG 4462', 'Porcelain Digital Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:15:26', '2021-03-04 02:16:15'),
(388, 27, NULL, 17, NULL, 'PDG 4463', 'Porcelain Digital Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:20:31', '2021-03-04 02:21:16'),
(389, 27, NULL, 17, NULL, 'PDG 4464', 'Porcelain Digital Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:22:34', '2021-03-04 02:24:26'),
(390, 27, NULL, 17, NULL, 'PDG 4465', 'PDG 4465', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:26:21', '2021-03-04 02:26:43'),
(391, 27, NULL, 17, NULL, 'PDG 4466', 'Porcelain Digital Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:27:41', '2021-03-04 02:28:26'),
(392, 27, NULL, 17, NULL, 'PDG 4472', 'Porcelain Digital Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:29:08', '2021-03-04 02:29:58'),
(393, 27, NULL, 17, NULL, 'PP 4400', 'Porcelain  Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:31:26', '2021-03-04 02:32:03'),
(394, 27, NULL, 17, NULL, 'PP 4401', 'Porcelain  Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:33:19', '2021-03-04 02:34:33'),
(395, 27, NULL, 17, NULL, 'PP 4403', 'Porcelain  Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:37:07', '2021-03-04 02:38:03'),
(396, 27, NULL, 17, NULL, 'PP 4404', 'Porcelain  Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:42:42', '2021-03-04 02:43:09'),
(397, 27, NULL, 17, NULL, 'PP 4405', 'Porcelain  Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:44:20', '2021-03-04 02:44:47'),
(398, 27, NULL, 17, NULL, 'PP 4406', 'Porcelain  Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:47:02', '2021-03-04 02:47:56'),
(399, 27, NULL, 17, NULL, 'PP 4409 SG', 'Porcelain  Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:49:12', '2021-03-04 02:49:40'),
(400, 27, NULL, 17, NULL, 'PP 4410 SG', 'Porcelain  Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:50:31', '2021-03-04 02:50:53'),
(401, 27, NULL, 17, NULL, 'PP 4414', 'Porcelain  Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:52:34', '2021-03-04 02:53:06'),
(402, 27, NULL, 17, NULL, 'PP 4415', 'Porcelain  Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:54:23', '2021-03-04 02:54:58'),
(403, 27, NULL, 17, NULL, 'PP 4416', 'Porcelain  Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:56:01', '2021-03-04 02:56:29'),
(404, 27, NULL, 17, NULL, 'PP 4418 SG', 'Porcelain  Print', NULL, NULL, 0, 992, 1040, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-04 02:57:37', '2021-03-04 02:58:03'),
(405, 28, NULL, 17, NULL, 'WDG 3510 D', 'Ceramic Digital Print', NULL, NULL, 0, 966.66628, 966.66628, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 00:10:03', '2021-03-09 00:01:53'),
(406, 27, NULL, 17, NULL, 'G 4400', 'Homogeneous', NULL, NULL, 0, 976, 976, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 17:31:02', '2021-03-07 17:31:28'),
(407, 27, NULL, 17, NULL, 'G 4402', 'Homogeneous', NULL, NULL, 0, 976, 976, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 17:33:11', '2021-03-07 17:34:00'),
(408, 27, NULL, 17, NULL, 'G 4403', 'Homogeneous', NULL, NULL, 0, 976, 976, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 17:35:10', '2021-03-07 17:35:34'),
(409, 27, NULL, 17, NULL, 'G 4404', 'Homogeneous', NULL, NULL, 0, 976, 976, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 17:38:54', '2021-03-07 17:40:15'),
(410, 27, NULL, 17, NULL, 'G 4405', 'Homogeneous', NULL, NULL, 0, 976, 976, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 17:41:19', '2021-03-07 17:42:05'),
(411, 27, NULL, 17, NULL, 'G 4406', 'Homogeneous', NULL, NULL, 0, 976, 976, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 17:43:02', '2021-03-07 17:43:19'),
(412, 27, NULL, 17, NULL, 'G 4407', 'Homogeneous', NULL, NULL, 0, 976, 976, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 17:44:14', '2021-03-07 17:44:38'),
(413, 27, NULL, 17, NULL, 'G 4408', 'Homogeneous', NULL, NULL, 0, 976, 976, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 17:46:00', '2021-03-07 17:46:28'),
(414, 27, NULL, 17, NULL, 'G 4409', 'Homogeneous', NULL, NULL, 0, 976, 976, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 17:48:10', '2021-03-07 17:48:32'),
(415, 27, NULL, 17, NULL, 'PDG 33304', 'Porcelain Digital Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:19:46', '2021-03-07 19:20:17'),
(416, 27, NULL, 17, NULL, 'PDG 33313', 'Porcelain Digital Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:21:46', '2021-03-07 19:22:31'),
(417, 27, NULL, 17, NULL, 'PDG 33422', 'Porcelain Digital Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:23:41', '2021-03-07 19:24:04'),
(418, 27, NULL, 17, NULL, 'PDG 33357', 'Porcelain Digital Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:25:28', '2021-03-07 20:45:05'),
(419, 27, NULL, 17, NULL, 'PDG 33363', 'Porcelain Digital Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:26:50', '2021-03-07 19:27:09'),
(420, 27, NULL, 17, NULL, 'PDG 33364', 'Porcelain Digital Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:28:11', '2021-03-07 19:28:28'),
(421, 27, NULL, 17, NULL, 'PDG 33365', 'Porcelain Digital Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:29:33', '2021-03-07 19:30:08'),
(422, 27, NULL, 17, NULL, 'PDG 33370', 'Porcelain Digital Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:31:14', '2021-03-07 19:31:47'),
(423, 27, NULL, 17, NULL, 'PDG 33394', 'Porcelain Digital Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:32:40', '2021-03-07 19:33:09'),
(424, 27, NULL, 17, NULL, 'PP 3300', 'Porcelain  Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:35:53', '2021-03-07 19:36:45'),
(425, 27, NULL, 17, NULL, 'PP 3300 A', 'Porcelain  Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:37:43', '2021-03-07 19:38:24'),
(426, 27, NULL, 17, NULL, 'PP 3300 G', 'Porcelain  Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:39:29', '2021-03-07 19:39:59'),
(427, 27, NULL, 17, NULL, 'PP 3301', 'Porcelain  Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:40:56', '2021-03-07 19:41:29'),
(428, 27, NULL, 17, NULL, 'PP 3302', 'Porcelain  Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:42:53', '2021-03-07 19:43:32'),
(429, 27, NULL, 17, NULL, 'PP 3308', 'Porcelain  Print', NULL, NULL, 0, 912, 960, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:44:51', '2021-03-07 19:45:13'),
(430, 27, NULL, 17, NULL, 'G 3300', 'Homogeneous', NULL, NULL, 0, 880, 880, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:48:49', '2021-03-07 19:49:51'),
(431, 27, NULL, 17, NULL, 'G 3303', 'Homogeneous', NULL, NULL, 0, 896, 896, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:51:24', '2021-03-07 19:52:11'),
(432, 27, NULL, 17, NULL, 'G 3304', 'Homogeneous', NULL, NULL, 0, 896, 896, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:53:42', '2021-03-07 19:54:32'),
(433, 27, NULL, 17, NULL, 'G 3305', 'Homogeneous', NULL, NULL, 0, 896, 896, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:55:39', '2021-03-07 19:56:09'),
(434, 27, NULL, 17, NULL, 'G 3306', 'Homogeneous', NULL, NULL, 0, 896, 896, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 19:57:15', '2021-03-07 19:58:16'),
(435, 27, NULL, 17, NULL, 'G 3308', 'Homogeneous', NULL, NULL, 0, 896, 896, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 20:00:17', '2021-03-07 20:00:54'),
(436, 27, NULL, 17, NULL, 'G 3309', 'Homogeneous', NULL, NULL, 0, 896, 896, NULL, NULL, NULL, NULL, 11, 31.25, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 20:02:01', '2021-03-07 20:02:46'),
(437, 28, NULL, 17, NULL, 'WH 36208 L', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 21:30:14', '2021-03-07 21:31:02'),
(438, 28, NULL, 17, NULL, 'WH 36208 D', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 21:34:21', '2021-03-07 21:35:04'),
(439, 28, NULL, 17, NULL, 'WH 36208 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1184, 1184, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 21:36:54', '2021-03-07 21:37:27'),
(440, 28, NULL, 17, NULL, 'WH 36209L', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 21:39:22', '2021-03-07 21:40:08'),
(441, 28, NULL, 17, NULL, 'WH 36209 D', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 21:42:05', '2021-03-07 21:42:43'),
(442, 28, NULL, 17, NULL, 'WH 36209 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1184, 1184, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 21:44:52', '2021-03-07 21:45:23'),
(443, 28, NULL, 17, NULL, 'WH 36210 L', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 21:46:36', '2021-03-07 21:47:38'),
(444, 28, NULL, 17, NULL, 'WH 36210 D', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 21:58:22', '2021-03-07 21:58:46'),
(445, 28, NULL, 17, NULL, 'WH 36210 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1184, 1184, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 22:03:56', '2021-03-07 22:04:37'),
(446, 28, NULL, 17, NULL, 'WH 36211 L', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-07 22:06:13', '2021-03-07 22:06:44'),
(447, 28, NULL, 17, NULL, 'WH 36211 D', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 00:20:01', '2021-03-08 00:21:10'),
(448, 28, NULL, 17, NULL, 'WH 36211 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1184, 1184, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:01:02', '2021-03-08 17:01:39'),
(449, 28, NULL, 17, NULL, 'WH 36212 L', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:05:15', '2021-03-08 17:06:36'),
(450, 28, NULL, 17, NULL, 'WH 36212 D', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:07:38', '2021-03-08 17:08:10'),
(451, 28, NULL, 17, NULL, 'WH 36212 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1184, 1184, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:09:20', '2021-03-08 17:09:55'),
(452, 28, NULL, 17, NULL, 'WH 36214 L', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:11:23', '2021-03-08 17:12:41'),
(453, 28, NULL, 17, NULL, 'WH 36214 D', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:13:47', '2021-03-08 17:13:47'),
(454, 28, NULL, 17, NULL, 'WH 36214 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1184, 1184, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:15:14', '2021-03-08 17:18:08'),
(455, 28, NULL, 17, NULL, 'WH 3621 9 L', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:19:31', '2021-03-08 17:20:12'),
(456, 28, NULL, 17, NULL, 'WH 36219 D', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:23:43', '2021-03-08 17:24:02'),
(457, 28, NULL, 17, NULL, 'WH 36219 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1184, 1184, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:25:33', '2021-03-08 17:25:57'),
(458, 28, NULL, 17, NULL, 'WH 36219 KHL', 'Ceramic Digital Print', NULL, NULL, 0, 1184, 1184, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:27:22', '2021-03-08 17:28:22'),
(459, 28, NULL, 17, NULL, 'WH 36216 L', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:29:55', '2021-03-08 17:30:35'),
(460, 28, NULL, 17, NULL, 'WH 36216 D', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:31:41', '2021-03-08 17:32:12'),
(461, 28, NULL, 17, NULL, 'WH 36216 KHL', 'Ceramic Digital Print', NULL, NULL, 0, 1184, 1184, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:41:38', '2021-03-08 17:42:46'),
(462, 28, NULL, 17, NULL, 'WH 36218 L', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:44:33', '2021-03-08 17:44:58'),
(463, 28, NULL, 17, NULL, 'WH 36218 D', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:47:20', '2021-03-08 17:47:43'),
(464, 28, NULL, 17, NULL, 'WH 36218 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1184, 1184, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:49:49', '2021-03-08 17:50:19'),
(465, 28, NULL, 17, NULL, 'WH 36220 L', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 17:51:10', '2021-03-08 17:52:02'),
(466, 28, NULL, 17, NULL, 'WH 36220 D', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:04:05', '2021-03-08 18:04:37'),
(467, 28, NULL, 17, NULL, 'WH 36220 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1184, 1184, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:05:56', '2021-03-08 18:06:36'),
(468, 28, NULL, 17, NULL, 'WDG 3600', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:08:25', '2021-03-08 18:10:49'),
(469, 28, NULL, 17, NULL, 'WDG 3602', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:12:43', '2021-03-08 18:13:31'),
(470, 28, NULL, 17, NULL, 'WDG 3605', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:14:20', '2021-03-08 18:15:24'),
(471, 29, NULL, 17, NULL, 'WDG 3605 BR', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:20:26', '2021-03-08 18:21:17'),
(472, 28, NULL, 17, NULL, 'WDG 3609 L', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:23:34', '2021-03-08 18:23:54'),
(473, 28, NULL, 17, NULL, 'WDG 3609 D', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:25:01', '2021-03-08 18:25:47'),
(474, 28, NULL, 17, NULL, 'WDG 3609 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:27:01', '2021-03-08 18:28:21'),
(475, 28, NULL, 17, NULL, 'WDG 3616 L', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:29:27', '2021-03-08 18:30:31'),
(476, 28, NULL, 17, NULL, 'WDG 3616 D', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:32:06', '2021-03-08 18:33:21'),
(477, 28, NULL, 17, NULL, 'WDG 3616 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:38:43', '2021-03-08 18:39:32'),
(478, 28, NULL, 17, NULL, 'WDG 3617 L', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:40:39', '2021-03-08 18:41:38'),
(479, 28, NULL, 17, NULL, 'WDG 3617 D', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:42:39', '2021-03-08 18:43:20'),
(480, 28, NULL, 17, NULL, 'WDG 3617 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:44:27', '2021-03-08 18:45:00'),
(481, 28, NULL, 17, NULL, 'WDG 3621 L', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:46:14', '2021-03-08 18:46:44'),
(482, 28, NULL, 17, NULL, 'WDG 3621 D', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:47:40', '2021-03-08 18:48:10'),
(483, 28, NULL, 17, NULL, 'WDG 3621 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:49:12', '2021-03-08 18:50:35'),
(484, 28, NULL, 17, NULL, 'WDG 3622 L', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:54:14', '2021-03-08 18:54:39'),
(485, 28, NULL, 17, NULL, 'WDG 3622 D', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:57:02', '2021-03-08 18:57:31'),
(486, 28, NULL, 17, NULL, 'WDG 3622 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 18:59:09', '2021-03-08 18:59:35'),
(487, 28, NULL, 17, NULL, 'WDG 3623 L', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 19:03:02', '2021-03-08 19:04:10'),
(488, 28, NULL, 17, NULL, 'WDG 3623 D', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 19:06:53', '2021-03-08 19:08:35'),
(489, 28, NULL, 17, NULL, 'WDG 3623 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 19:09:31', '2021-03-08 19:10:19'),
(490, 28, NULL, 17, NULL, 'WDG 3623 BHL', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 19:11:28', '2021-03-08 19:12:12'),
(491, 28, NULL, 17, NULL, 'WDG 3624 L', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 19:13:22', '2021-03-08 19:14:05'),
(492, 28, NULL, 17, NULL, 'WDG 3624 D', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 19:15:12', '2021-03-08 19:15:39'),
(493, 28, NULL, 17, NULL, 'WDG 3624 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 19:16:32', '2021-03-08 19:17:13'),
(494, 28, NULL, 17, NULL, 'WDG 3626 L', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 20:12:40', '2021-03-08 20:13:58'),
(495, 28, NULL, 17, NULL, 'WDG 3626 D', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 20:16:50', '2021-03-08 20:17:17'),
(496, 28, NULL, 17, NULL, 'WDG 3626 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 20:18:32', '2021-03-08 20:19:10'),
(497, 28, NULL, 17, NULL, 'WDG 3626 BHL', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 20:21:49', '2021-03-08 20:22:10'),
(498, 28, NULL, 17, NULL, 'WDG 3630 L', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 21:38:09', '2021-03-08 21:39:42'),
(499, 28, NULL, 17, NULL, 'WDG 3630 D', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 21:43:27', '2021-03-08 21:44:01'),
(500, 28, NULL, 17, NULL, 'WDG 3630 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 21:45:20', '2021-03-08 21:46:26'),
(501, 28, NULL, 17, NULL, 'WDG 3632 L', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 21:48:22', '2021-03-08 21:49:14'),
(502, 28, NULL, 17, NULL, 'WDG 3632 D', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 21:50:37', '2021-03-08 21:51:57'),
(503, 28, NULL, 17, NULL, 'WDG 3632 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 22:01:33', '2021-03-08 22:02:03'),
(504, 28, NULL, 17, NULL, 'WDG 3636 L', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 22:04:33', '2021-03-08 22:19:46'),
(505, 28, NULL, 17, NULL, 'WDG 3636 D', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 22:20:57', '2021-03-08 22:21:25'),
(506, 28, NULL, 17, NULL, 'WDG 3636 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 22:22:42', '2021-03-08 22:23:42'),
(507, 28, NULL, 17, NULL, 'WDG 3642 L', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 22:25:01', '2021-03-08 22:26:24'),
(508, 28, NULL, 17, NULL, 'WDG 3642 D', 'Ceramic Digital Print', NULL, NULL, 0, 1088, 1088, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 22:27:33', '2021-03-08 22:28:03'),
(509, 28, NULL, 17, NULL, 'WDG 3642 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1152, 1152, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 22:29:15', '2021-03-08 22:29:50'),
(510, 28, NULL, 17, NULL, 'WDG 3510 L', 'Ceramic Digital Print', NULL, NULL, 0, 966.66628, 966.66628, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-08 23:51:21', '2021-03-09 00:00:03'),
(511, 28, NULL, 17, NULL, 'WDG 3510 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1033.33292, 1033.33292, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-09 00:09:26', '2021-03-09 00:10:04'),
(512, 28, NULL, 17, NULL, 'WDG 3513 L', 'Ceramic Digital Print', NULL, NULL, 0, 966.66628, 966.66628, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-09 00:13:02', '2021-03-09 00:13:48'),
(513, 28, NULL, 17, NULL, 'WDG 3513 D', 'Ceramic Digital Print', NULL, NULL, 0, 966.66628, 966.66628, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-09 00:18:01', '2021-03-09 00:20:50'),
(514, 28, NULL, 17, NULL, 'WDG 3513 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1033.33292, 1033.33292, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-09 00:22:28', '2021-03-09 00:23:29'),
(515, 28, NULL, 17, NULL, 'WDG 3514 L', 'Ceramic Digital Print', NULL, NULL, 0, 966.66628, 966.66628, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-09 00:26:29', '2021-03-09 00:26:47'),
(516, 28, NULL, 17, NULL, 'WDG 3514 D', 'Ceramic Digital Print', NULL, NULL, 0, 966.66628, 966.66628, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-09 00:33:44', '2021-03-09 00:34:38'),
(517, 28, NULL, 17, NULL, 'WDG 3514 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1033.33292, 1033.33292, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-09 00:36:28', '2021-03-09 00:36:50'),
(518, 28, NULL, 17, NULL, 'WDG 3516 L', 'Ceramic Digital Print', NULL, NULL, 0, 966.66628, 966.66628, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-09 03:42:39', '2021-03-09 03:43:10'),
(519, 28, NULL, 17, NULL, 'WDG 3516 D', 'Ceramic Digital Print', NULL, NULL, 0, 966.66628, 966.66628, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-09 03:45:18', '2021-03-09 03:46:18'),
(520, 28, NULL, 17, NULL, 'WDG 3516 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1033.33292, 1033.33292, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-09 03:47:53', '2021-03-09 03:49:42'),
(521, 28, NULL, 17, NULL, 'WDG 3529 L', 'Ceramic Digital Print', NULL, NULL, 0, 966.66628, 966.66628, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-09 03:51:52', '2021-03-09 03:52:32'),
(522, 28, NULL, 17, NULL, 'WDG 3529 D', 'Ceramic Digital Print', NULL, NULL, 0, 966.66628, 966.66628, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-09 03:54:40', '2021-03-09 03:55:40'),
(523, 28, NULL, 17, NULL, 'WDG 3529 HL', 'Ceramic Digital Print', NULL, NULL, 0, 1033.33292, 1033.33292, NULL, NULL, NULL, NULL, 11, 18.75, NULL, NULL, NULL, NULL, 1, NULL, 2, 5, NULL, '2021-03-09 04:01:15', '2021-03-09 04:02:44');

-- --------------------------------------------------------

--
-- Table structure for table `product_suppliers`
--

CREATE TABLE `product_suppliers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL,
  `whole_sale_price` double DEFAULT NULL,
  `retail_price` double DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_variations`
--

CREATE TABLE `product_variations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_variations`
--

INSERT INTO `product_variations` (`id`, `name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(10, 'Color', NULL, 1, 5, '2021-01-21 04:40:47', '2021-03-02 19:26:42'),
(11, 'Size', NULL, 5, 5, '2021-03-01 18:38:15', '2021-03-07 00:05:22');

-- --------------------------------------------------------

--
-- Table structure for table `product_variation_entries`
--

CREATE TABLE `product_variation_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `variation_value_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_variation_entries`
--

INSERT INTO `product_variation_entries` (`id`, `product_entry_id`, `variation_id`, `variation_value_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(55, 322, 11, 53, 5, NULL, '2021-03-01 22:00:45', NULL),
(56, 321, 11, 53, 5, NULL, '2021-03-01 22:20:06', NULL),
(58, 323, 11, 53, 5, NULL, '2021-03-02 17:06:14', NULL),
(60, 324, 11, 53, 5, NULL, '2021-03-02 17:31:09', NULL),
(62, 325, 11, 53, 5, NULL, '2021-03-02 17:32:57', NULL),
(64, 326, 11, 53, 5, NULL, '2021-03-02 17:43:20', NULL),
(66, 327, 11, 53, 5, NULL, '2021-03-02 17:45:27', NULL),
(72, 330, 11, 53, 5, NULL, '2021-03-02 18:43:23', NULL),
(73, 331, 11, 53, 5, NULL, '2021-03-02 18:44:43', NULL),
(75, 332, 11, 53, 5, NULL, '2021-03-02 18:46:58', NULL),
(77, 333, 11, 53, 5, NULL, '2021-03-02 18:49:08', NULL),
(79, 334, 11, 53, 5, NULL, '2021-03-02 18:50:51', NULL),
(85, 328, 11, 53, 5, NULL, '2021-03-02 19:28:38', NULL),
(86, 328, 10, 62, 5, NULL, '2021-03-02 19:28:38', NULL),
(87, 329, 11, 53, 5, NULL, '2021-03-02 19:29:18', NULL),
(88, 329, 10, 52, 5, NULL, '2021-03-02 19:29:18', NULL),
(89, 335, 11, 53, 5, NULL, '2021-03-02 19:32:38', NULL),
(91, 336, 11, 53, 5, NULL, '2021-03-02 19:37:49', NULL),
(93, 337, 11, 53, 5, NULL, '2021-03-02 19:47:54', NULL),
(95, 338, 11, 53, 5, NULL, '2021-03-02 19:50:20', NULL),
(97, 339, 11, 53, 5, NULL, '2021-03-02 19:52:12', NULL),
(99, 340, 11, 53, 5, NULL, '2021-03-02 19:53:44', NULL),
(101, 341, 11, 53, 5, NULL, '2021-03-02 19:58:17', NULL),
(103, 342, 11, 53, 5, NULL, '2021-03-02 20:04:19', NULL),
(105, 343, 11, 53, 5, NULL, '2021-03-02 20:08:08', NULL),
(107, 344, 11, 53, 5, NULL, '2021-03-02 20:09:28', NULL),
(108, 345, 11, 53, 5, NULL, '2021-03-02 20:13:41', NULL),
(110, 346, 11, 53, 5, NULL, '2021-03-02 20:15:12', NULL),
(112, 347, 11, 53, 5, NULL, '2021-03-02 20:20:51', NULL),
(114, 348, 11, 53, 5, NULL, '2021-03-02 20:29:10', NULL),
(116, 349, 11, 53, 5, NULL, '2021-03-02 20:31:00', NULL),
(118, 350, 11, 53, 5, NULL, '2021-03-02 20:32:35', NULL),
(120, 351, 11, 53, 5, NULL, '2021-03-02 20:34:18', NULL),
(122, 352, 11, 53, 5, NULL, '2021-03-02 20:37:05', NULL),
(124, 353, 11, 53, 5, NULL, '2021-03-02 20:38:22', NULL),
(126, 354, 11, 53, 5, NULL, '2021-03-02 20:40:03', NULL),
(128, 355, 11, 53, 5, NULL, '2021-03-02 20:41:43', NULL),
(130, 356, 11, 53, 5, NULL, '2021-03-02 20:43:37', NULL),
(132, 357, 11, 53, 5, NULL, '2021-03-02 20:52:00', NULL),
(134, 358, 11, 53, 5, NULL, '2021-03-02 21:12:29', NULL),
(136, 359, 11, 53, 5, NULL, '2021-03-02 21:15:58', NULL),
(138, 360, 11, 53, 5, NULL, '2021-03-03 01:26:19', NULL),
(140, 361, 11, 53, 5, NULL, '2021-03-03 01:28:29', NULL),
(142, 362, 11, 53, 5, NULL, '2021-03-03 01:30:15', NULL),
(144, 363, 11, 53, 5, NULL, '2021-03-03 01:35:18', NULL),
(147, 364, 11, 53, 5, NULL, '2021-03-03 01:43:23', NULL),
(150, 365, 11, 53, 5, NULL, '2021-03-03 01:48:41', NULL),
(152, 366, 11, 53, 5, NULL, '2021-03-03 02:32:29', NULL),
(154, 367, 11, 53, 5, NULL, '2021-03-03 02:35:05', NULL),
(156, 368, 11, 53, 5, NULL, '2021-03-03 02:38:01', NULL),
(159, 369, 11, 53, 5, NULL, '2021-03-03 02:44:52', NULL),
(161, 370, 11, 53, 5, NULL, '2021-03-03 02:47:11', NULL),
(163, 371, 11, 53, 5, NULL, '2021-03-03 02:49:49', NULL),
(171, 373, 11, 53, 5, NULL, '2021-03-03 03:09:49', NULL),
(172, 373, 10, 61, 5, NULL, '2021-03-03 03:09:49', NULL),
(173, 374, 11, 53, 5, NULL, '2021-03-03 03:12:28', NULL),
(174, 374, 10, 63, 5, NULL, '2021-03-03 03:12:28', NULL),
(177, 375, 11, 53, 5, NULL, '2021-03-03 03:19:54', NULL),
(178, 375, 10, 64, 5, NULL, '2021-03-03 03:19:54', NULL),
(181, 376, 11, 53, 5, NULL, '2021-03-03 03:22:35', NULL),
(182, 376, 10, 66, 5, NULL, '2021-03-03 03:22:35', NULL),
(186, 377, 11, 53, 5, NULL, '2021-03-03 03:27:23', NULL),
(187, 377, 10, 52, 5, NULL, '2021-03-03 03:27:23', NULL),
(192, 378, 11, 53, 5, NULL, '2021-03-03 22:05:45', NULL),
(193, 378, 10, 61, 5, NULL, '2021-03-03 22:05:45', NULL),
(254, 379, 11, 55, 5, NULL, '2021-03-07 17:07:54', NULL),
(255, 380, 11, 55, 5, NULL, '2021-03-07 17:08:43', NULL),
(256, 381, 11, 55, 5, NULL, '2021-03-07 17:10:05', NULL),
(257, 382, 11, 55, 5, NULL, '2021-03-07 17:10:38', NULL),
(258, 383, 11, 55, 5, NULL, '2021-03-07 17:11:02', NULL),
(259, 384, 11, 55, 5, NULL, '2021-03-07 17:11:35', NULL),
(260, 385, 11, 55, 5, NULL, '2021-03-07 17:13:01', NULL),
(261, 386, 11, 55, 5, NULL, '2021-03-07 17:13:46', NULL),
(262, 387, 11, 55, 5, NULL, '2021-03-07 17:14:19', NULL),
(263, 388, 11, 55, 5, NULL, '2021-03-07 17:14:44', NULL),
(264, 389, 11, 55, 5, NULL, '2021-03-07 17:15:23', NULL),
(266, 391, 11, 55, 5, NULL, '2021-03-07 17:16:28', NULL),
(267, 392, 11, 55, 5, NULL, '2021-03-07 17:17:38', NULL),
(268, 390, 11, 55, 5, NULL, '2021-03-07 17:17:57', NULL),
(269, 393, 11, 55, 5, NULL, '2021-03-07 17:18:39', NULL),
(270, 394, 11, 55, 5, NULL, '2021-03-07 17:19:10', NULL),
(271, 395, 11, 55, 5, NULL, '2021-03-07 17:19:43', NULL),
(272, 396, 11, 55, 5, NULL, '2021-03-07 17:20:17', NULL),
(273, 397, 11, 55, 5, NULL, '2021-03-07 17:20:51', NULL),
(274, 398, 11, 55, 5, NULL, '2021-03-07 17:21:15', NULL),
(275, 399, 11, 55, 5, NULL, '2021-03-07 17:21:50', NULL),
(276, 400, 11, 55, 5, NULL, '2021-03-07 17:23:20', NULL),
(277, 401, 11, 55, 5, NULL, '2021-03-07 17:24:10', NULL),
(278, 402, 11, 55, 5, NULL, '2021-03-07 17:24:42', NULL),
(279, 403, 11, 55, 5, NULL, '2021-03-07 17:25:08', NULL),
(280, 404, 11, 55, 5, NULL, '2021-03-07 17:25:50', NULL),
(282, 406, 11, 55, 5, NULL, '2021-03-07 17:31:28', NULL),
(284, 407, 11, 55, 5, NULL, '2021-03-07 17:34:00', NULL),
(287, 408, 11, 55, 5, NULL, '2021-03-07 17:35:58', NULL),
(289, 409, 11, 55, 5, NULL, '2021-03-07 17:40:15', NULL),
(291, 410, 11, 55, 5, NULL, '2021-03-07 17:42:05', NULL),
(293, 411, 11, 55, 5, NULL, '2021-03-07 17:43:19', NULL),
(296, 412, 11, 55, 5, NULL, '2021-03-07 17:45:09', NULL),
(298, 413, 11, 55, 5, NULL, '2021-03-07 17:46:28', NULL),
(301, 414, 11, 55, 5, NULL, '2021-03-07 17:49:17', NULL),
(303, 415, 11, 56, 5, NULL, '2021-03-07 19:20:17', NULL),
(305, 416, 11, 56, 5, NULL, '2021-03-07 19:22:31', NULL),
(307, 417, 11, 56, 5, NULL, '2021-03-07 19:24:04', NULL),
(311, 419, 11, 56, 5, NULL, '2021-03-07 19:27:09', NULL),
(313, 420, 11, 56, 5, NULL, '2021-03-07 19:28:28', NULL),
(315, 421, 11, 56, 5, NULL, '2021-03-07 19:30:08', NULL),
(317, 422, 11, 56, 5, NULL, '2021-03-07 19:31:47', NULL),
(319, 423, 11, 56, 5, NULL, '2021-03-07 19:33:09', NULL),
(322, 424, 11, 56, 5, NULL, '2021-03-07 19:36:45', NULL),
(323, 424, 10, 60, 5, NULL, '2021-03-07 19:36:45', NULL),
(325, 425, 11, 56, 5, NULL, '2021-03-07 19:38:24', NULL),
(327, 426, 11, 56, 5, NULL, '2021-03-07 19:39:59', NULL),
(329, 427, 11, 56, 5, NULL, '2021-03-07 19:41:29', NULL),
(331, 428, 11, 56, 5, NULL, '2021-03-07 19:43:32', NULL),
(333, 429, 11, 56, 5, NULL, '2021-03-07 19:45:13', NULL),
(334, 430, 11, 56, 5, NULL, '2021-03-07 19:49:51', NULL),
(336, 431, 11, 56, 5, NULL, '2021-03-07 19:52:11', NULL),
(337, 432, 11, 56, 5, NULL, '2021-03-07 19:54:32', NULL),
(339, 433, 11, 56, 5, NULL, '2021-03-07 19:56:09', NULL),
(341, 434, 11, 56, 5, NULL, '2021-03-07 19:58:16', NULL),
(343, 435, 11, 56, 5, NULL, '2021-03-07 20:00:54', NULL),
(345, 436, 11, 56, 5, NULL, '2021-03-07 20:02:46', NULL),
(346, 372, 11, 53, 5, NULL, '2021-03-07 20:42:00', NULL),
(347, 418, 11, 56, 5, NULL, '2021-03-07 20:45:05', NULL),
(349, 437, 11, 57, 5, NULL, '2021-03-07 21:31:02', NULL),
(352, 438, 11, 57, 5, NULL, '2021-03-07 21:35:29', NULL),
(354, 439, 11, 57, 5, NULL, '2021-03-07 21:37:27', NULL),
(356, 440, 11, 57, 5, NULL, '2021-03-07 21:40:08', NULL),
(358, 441, 11, 57, 5, NULL, '2021-03-07 21:42:43', NULL),
(360, 442, 11, 57, 5, NULL, '2021-03-07 21:45:23', NULL),
(362, 443, 11, 57, 5, NULL, '2021-03-07 21:47:38', NULL),
(365, 444, 11, 57, 5, NULL, '2021-03-07 22:02:57', NULL),
(367, 445, 11, 57, 5, NULL, '2021-03-07 22:04:37', NULL),
(370, 446, 11, 57, 5, NULL, '2021-03-08 00:18:39', NULL),
(372, 447, 11, 57, 5, NULL, '2021-03-08 00:21:10', NULL),
(374, 448, 11, 57, 5, NULL, '2021-03-08 17:01:39', NULL),
(376, 449, 11, 57, 5, NULL, '2021-03-08 17:06:36', NULL),
(378, 450, 11, 57, 5, NULL, '2021-03-08 17:08:10', NULL),
(380, 451, 11, 57, 5, NULL, '2021-03-08 17:09:55', NULL),
(382, 452, 11, 57, 5, NULL, '2021-03-08 17:12:41', NULL),
(384, 453, 11, 57, 5, NULL, '2021-03-08 17:14:14', NULL),
(386, 454, 11, 57, 5, NULL, '2021-03-08 17:18:08', NULL),
(388, 455, 11, 57, 5, NULL, '2021-03-08 17:20:12', NULL),
(390, 456, 11, 57, 5, NULL, '2021-03-08 17:24:02', NULL),
(392, 457, 11, 57, 5, NULL, '2021-03-08 17:25:57', NULL),
(394, 458, 11, 57, 5, NULL, '2021-03-08 17:28:22', NULL),
(396, 459, 11, 57, 5, NULL, '2021-03-08 17:30:35', NULL),
(398, 460, 11, 57, 5, NULL, '2021-03-08 17:32:12', NULL),
(400, 461, 11, 57, 5, NULL, '2021-03-08 17:42:46', NULL),
(403, 462, 11, 57, 5, NULL, '2021-03-08 17:46:28', NULL),
(405, 463, 11, 57, 5, NULL, '2021-03-08 17:47:43', NULL),
(407, 464, 11, 57, 5, NULL, '2021-03-08 17:50:19', NULL),
(410, 465, 11, 57, 5, NULL, '2021-03-08 18:01:48', NULL),
(412, 466, 11, 57, 5, NULL, '2021-03-08 18:04:37', NULL),
(415, 467, 11, 57, 5, NULL, '2021-03-08 18:06:58', NULL),
(417, 468, 11, 57, 5, NULL, '2021-03-08 18:10:49', NULL),
(419, 469, 11, 57, 5, NULL, '2021-03-08 18:13:31', NULL),
(421, 470, 11, 57, 5, NULL, '2021-03-08 18:15:24', NULL),
(423, 471, 11, 57, 5, NULL, '2021-03-08 18:21:17', NULL),
(425, 472, 11, 57, 5, NULL, '2021-03-08 18:23:54', NULL),
(427, 473, 11, 57, 5, NULL, '2021-03-08 18:25:47', NULL),
(429, 474, 11, 57, 5, NULL, '2021-03-08 18:28:21', NULL),
(431, 475, 11, 57, 5, NULL, '2021-03-08 18:30:31', NULL),
(434, 476, 11, 57, 5, NULL, '2021-03-08 18:37:30', NULL),
(436, 477, 11, 57, 5, NULL, '2021-03-08 18:39:32', NULL),
(438, 478, 11, 57, 5, NULL, '2021-03-08 18:41:38', NULL),
(440, 479, 11, 57, 5, NULL, '2021-03-08 18:43:20', NULL),
(442, 480, 11, 57, 5, NULL, '2021-03-08 18:45:00', NULL),
(444, 481, 11, 57, 5, NULL, '2021-03-08 18:46:44', NULL),
(446, 482, 11, 57, 5, NULL, '2021-03-08 18:48:10', NULL),
(449, 483, 11, 57, 5, NULL, '2021-03-08 18:51:09', NULL),
(451, 484, 11, 57, 5, NULL, '2021-03-08 18:54:39', NULL),
(453, 485, 11, 57, 5, NULL, '2021-03-08 18:57:31', NULL),
(456, 486, 11, 57, 5, NULL, '2021-03-08 19:01:16', NULL),
(458, 487, 11, 57, 5, NULL, '2021-03-08 19:04:10', NULL),
(460, 488, 11, 57, 5, NULL, '2021-03-08 19:08:35', NULL),
(462, 489, 11, 57, 5, NULL, '2021-03-08 19:10:20', NULL),
(464, 490, 11, 57, 5, NULL, '2021-03-08 19:12:12', NULL),
(466, 491, 11, 57, 5, NULL, '2021-03-08 19:14:05', NULL),
(468, 492, 11, 57, 5, NULL, '2021-03-08 19:15:39', NULL),
(471, 493, 11, 57, 5, NULL, '2021-03-08 20:11:35', NULL),
(476, 494, 11, 57, 5, NULL, '2021-03-08 20:15:52', NULL),
(478, 495, 11, 57, 5, NULL, '2021-03-08 20:17:17', NULL),
(480, 496, 11, 57, 5, NULL, '2021-03-08 20:19:10', NULL),
(482, 497, 11, 57, 5, NULL, '2021-03-08 20:22:10', NULL),
(485, 498, 11, 57, 5, NULL, '2021-03-08 21:40:56', NULL),
(487, 499, 11, 57, 5, NULL, '2021-03-08 21:44:01', NULL),
(490, 500, 11, 57, 5, NULL, '2021-03-08 21:47:01', NULL),
(495, 502, 11, 57, 5, NULL, '2021-03-08 21:57:26', NULL),
(498, 503, 11, 57, 5, NULL, '2021-03-08 22:03:32', NULL),
(500, 501, 11, 57, 5, NULL, '2021-03-08 22:17:32', NULL),
(501, 504, 11, 57, 5, NULL, '2021-03-08 22:19:46', NULL),
(503, 505, 11, 57, 5, NULL, '2021-03-08 22:21:25', NULL),
(506, 506, 11, 57, 5, NULL, '2021-03-08 22:24:08', NULL),
(508, 507, 11, 57, 5, NULL, '2021-03-08 22:26:24', NULL),
(510, 508, 11, 57, 5, NULL, '2021-03-08 22:28:03', NULL),
(512, 509, 11, 57, 5, NULL, '2021-03-08 22:29:50', NULL),
(519, 511, 11, 67, 5, NULL, '2021-03-09 00:10:04', NULL),
(522, 405, 11, 67, 5, NULL, '2021-03-09 00:14:42', NULL),
(523, 512, 11, 67, 5, NULL, '2021-03-09 00:16:34', NULL),
(525, 513, 11, 67, 5, NULL, '2021-03-09 00:20:50', NULL),
(530, 514, 11, 67, 5, NULL, '2021-03-09 00:27:19', NULL),
(531, 510, 11, 67, 5, NULL, '2021-03-09 00:30:08', NULL),
(532, 515, 11, 67, 5, NULL, '2021-03-09 00:32:26', NULL),
(535, 516, 11, 67, 5, NULL, '2021-03-09 00:35:14', NULL),
(537, 517, 11, 67, 5, NULL, '2021-03-09 00:36:50', NULL),
(539, 518, 11, 67, 5, NULL, '2021-03-09 03:43:10', NULL),
(541, 519, 11, 67, 5, NULL, '2021-03-09 03:46:18', NULL),
(543, 520, 11, 67, 5, NULL, '2021-03-09 03:49:42', NULL),
(545, 521, 11, 67, 5, NULL, '2021-03-09 03:52:32', NULL),
(547, 522, 11, 67, 5, NULL, '2021-03-09 03:55:40', NULL),
(550, 523, 11, 67, 5, NULL, '2021-03-09 04:51:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_variation_values`
--

CREATE TABLE `product_variation_values` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_variation_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_variation_values`
--

INSERT INTO `product_variation_values` (`id`, `product_variation_id`, `name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(51, 10, 'লাল', NULL, 1, 5, '2021-01-21 04:40:47', '2021-03-02 19:26:42'),
(52, 10, 'কালো', NULL, 1, 5, '2021-01-21 04:40:47', '2021-03-02 19:26:42'),
(53, 11, '24*24', NULL, 5, 5, '2021-03-01 18:38:15', '2021-03-07 00:05:22'),
(54, 11, '20*20', NULL, 5, 5, '2021-03-01 18:38:15', '2021-03-07 00:05:22'),
(55, 11, '16*16', NULL, 5, 5, '2021-03-01 18:38:15', '2021-03-07 00:05:22'),
(56, 11, '12*12', NULL, 5, 5, '2021-03-01 18:38:15', '2021-03-07 00:05:22'),
(57, 11, '12*24', NULL, 5, 5, '2021-03-01 18:38:15', '2021-03-07 00:05:22'),
(60, 10, 'সাদা', NULL, 5, NULL, '2021-03-02 19:26:42', NULL),
(61, 10, 'অফ হোয়াইট', NULL, 5, NULL, '2021-03-02 19:26:42', NULL),
(62, 10, 'মেরুন', NULL, 5, NULL, '2021-03-02 19:26:42', NULL),
(63, 10, 'পিংক', NULL, 5, NULL, '2021-03-02 19:26:42', NULL),
(64, 10, 'সবুজ', NULL, 5, NULL, '2021-03-02 19:26:42', NULL),
(65, 10, 'হলুদ', NULL, 5, NULL, '2021-03-02 19:26:42', NULL),
(66, 10, 'ব্লু', NULL, 5, NULL, '2021-03-02 19:26:42', NULL),
(67, 11, '12*20', NULL, 5, NULL, '2021-03-07 00:05:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return`
--

CREATE TABLE `purchase_return` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bill_id` bigint(20) UNSIGNED NOT NULL,
  `purchase_return_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchase_return_date` date NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `sub_total_amount` double NOT NULL,
  `return_amount` double NOT NULL,
  `due_amount` double NOT NULL,
  `total_tax` double DEFAULT NULL,
  `tax_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `vat_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `total_vat` double DEFAULT NULL,
  `return_note` text COLLATE utf8mb4_unicode_ci,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `total_discount_type` tinyint(4) DEFAULT NULL,
  `total_discount_amount` double DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return_entries`
--

CREATE TABLE `purchase_return_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `purchase_return_id` bigint(20) UNSIGNED NOT NULL,
  `bill_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `rate` double NOT NULL,
  `quantity` double NOT NULL DEFAULT '1',
  `total_amount` double NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales_return`
--

CREATE TABLE `sales_return` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) UNSIGNED NOT NULL,
  `sales_return_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sales_return_date` date NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `sub_total_amount` double NOT NULL,
  `return_amount` double NOT NULL,
  `due_amount` double NOT NULL,
  `total_tax` double DEFAULT NULL,
  `tax_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `vat_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `total_vat` double DEFAULT NULL,
  `return_note` text COLLATE utf8mb4_unicode_ci,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `total_discount_type` tinyint(4) DEFAULT NULL,
  `total_discount_amount` double DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales_return_entries`
--

CREATE TABLE `sales_return_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sales_return_id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `main_unit_id` bigint(20) UNSIGNED NOT NULL,
  `conversion_unit_id` bigint(20) UNSIGNED NOT NULL,
  `buy_price` double NOT NULL,
  `rate` double NOT NULL,
  `quantity` double NOT NULL DEFAULT '1',
  `total_amount` double NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=active 0=inactive',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transaction_date` date NOT NULL,
  `invoice_id` bigint(20) UNSIGNED DEFAULT NULL,
  `bill_id` bigint(20) UNSIGNED DEFAULT NULL,
  `sales_return_id` bigint(20) UNSIGNED DEFAULT NULL,
  `purchase_return_id` bigint(20) UNSIGNED DEFAULT NULL,
  `expense_id` bigint(20) UNSIGNED DEFAULT NULL,
  `income_id` bigint(20) UNSIGNED DEFAULT NULL,
  `payment_id` bigint(20) UNSIGNED DEFAULT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `amount` double NOT NULL,
  `paid_through` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `branch_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Pcs', NULL, 1, 1, '2021-01-03 07:40:36', '2021-02-18 09:10:00'),
(6, 'Kg', NULL, 1, NULL, '2021-02-18 09:10:08', '2021-02-18 09:10:08'),
(7, 'Ton', NULL, 1, NULL, '2021-02-18 09:10:14', '2021-02-18 09:10:14'),
(9, 'Feet', NULL, 1, NULL, '2021-02-18 09:10:27', '2021-02-18 09:10:27'),
(10, 'SFT', NULL, 1, NULL, '2021-02-18 09:10:36', '2021-02-18 09:10:36'),
(11, 'Cartoon', NULL, 1, NULL, '2021-02-18 09:11:26', '2021-02-18 09:11:26'),
(12, 'Set', NULL, 5, NULL, '2021-03-01 22:13:13', '2021-03-01 22:13:13');

-- --------------------------------------------------------

--
-- Table structure for table `unit_conversions`
--

CREATE TABLE `unit_conversions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `main_unit_id` bigint(20) UNSIGNED NOT NULL,
  `converted_unit_id` bigint(20) UNSIGNED NOT NULL,
  `conversion_rate` double NOT NULL,
  `purchase_price` double DEFAULT NULL,
  `sell_price` double DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated-at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `unit_conversions`
--

INSERT INTO `unit_conversions` (`id`, `product_entry_id`, `main_unit_id`, `converted_unit_id`, `conversion_rate`, `purchase_price`, `sell_price`, `created_by`, `updated_by`, `created_at`, `updated-at`) VALUES
(36, 322, 11, 1, 4, 368, 368, 5, NULL, '2021-03-01 22:00:45', NULL),
(37, 322, 11, 10, 16, 92, 92, 5, NULL, '2021-03-01 22:00:45', NULL),
(38, 321, 11, 1, 4, 368, 368, 5, NULL, '2021-03-01 22:20:06', NULL),
(39, 321, 11, 10, 16, 92, 92, 5, NULL, '2021-03-01 22:20:06', NULL),
(42, 323, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 17:06:14', NULL),
(43, 323, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 17:06:14', NULL),
(46, 324, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 17:31:09', NULL),
(47, 324, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 17:31:09', NULL),
(50, 325, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 17:32:57', NULL),
(51, 325, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 17:32:57', NULL),
(54, 326, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 17:43:20', NULL),
(55, 326, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 17:43:20', NULL),
(58, 327, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 17:45:27', NULL),
(59, 327, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 17:45:27', NULL),
(68, 330, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 18:43:23', NULL),
(69, 330, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 18:43:23', NULL),
(70, 331, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 18:44:43', NULL),
(71, 331, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 18:44:43', NULL),
(74, 332, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 18:46:58', NULL),
(75, 332, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 18:46:58', NULL),
(78, 333, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 18:49:08', NULL),
(79, 333, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 18:49:08', NULL),
(80, 334, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 18:50:51', NULL),
(81, 334, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 18:50:51', NULL),
(88, 328, 11, 1, 4, 420, 420, 5, NULL, '2021-03-02 19:28:38', NULL),
(89, 328, 11, 10, 16, 105, 105, 5, NULL, '2021-03-02 19:28:38', NULL),
(90, 329, 11, 1, 4, 420, 420, 5, NULL, '2021-03-02 19:29:18', NULL),
(91, 329, 11, 11, 16, 105, 105, 5, NULL, '2021-03-02 19:29:18', NULL),
(92, 335, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 19:32:38', NULL),
(93, 335, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 19:32:38', NULL),
(96, 336, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 19:37:49', NULL),
(97, 336, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 19:37:49', NULL),
(100, 337, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 19:47:54', NULL),
(101, 337, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 19:47:54', NULL),
(104, 338, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 19:50:20', NULL),
(105, 338, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 19:50:20', NULL),
(108, 339, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 19:52:12', NULL),
(109, 339, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 19:52:12', NULL),
(112, 340, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 19:53:44', NULL),
(113, 340, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 19:53:44', NULL),
(116, 341, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 19:58:17', NULL),
(117, 341, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 19:58:17', NULL),
(120, 342, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:04:19', NULL),
(121, 342, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:04:19', NULL),
(124, 343, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:08:08', NULL),
(125, 343, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:08:08', NULL),
(128, 344, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:09:28', NULL),
(129, 344, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:09:28', NULL),
(130, 345, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:13:41', NULL),
(131, 345, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:13:41', NULL),
(134, 346, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:15:12', NULL),
(135, 346, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:15:12', NULL),
(138, 347, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:20:51', NULL),
(139, 347, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:20:51', NULL),
(142, 348, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:29:10', NULL),
(143, 348, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:29:10', NULL),
(146, 349, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:31:00', NULL),
(147, 349, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:31:00', NULL),
(150, 350, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:32:35', NULL),
(151, 350, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:32:35', NULL),
(154, 351, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:34:18', NULL),
(155, 351, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:34:18', NULL),
(158, 352, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:37:05', NULL),
(159, 352, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:37:05', NULL),
(162, 353, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:38:22', NULL),
(163, 353, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:38:22', NULL),
(166, 354, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:40:03', NULL),
(167, 354, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:40:03', NULL),
(170, 355, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:41:43', NULL),
(171, 355, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:41:43', NULL),
(174, 356, 11, 1, 4, 368, 368, 5, NULL, '2021-03-02 20:43:37', NULL),
(175, 356, 11, 10, 16, 92, 92, 5, NULL, '2021-03-02 20:43:37', NULL),
(178, 357, 11, 1, 4, 380, 380, 5, NULL, '2021-03-02 20:52:00', NULL),
(179, 357, 11, 10, 16, 95, 95, 5, NULL, '2021-03-02 20:52:00', NULL),
(182, 358, 11, 1, 4, 380, 380, 5, NULL, '2021-03-02 21:12:29', NULL),
(183, 358, 11, 10, 16, 95, 95, 5, NULL, '2021-03-02 21:12:29', NULL),
(186, 359, 11, 1, 4, 380, 380, 5, NULL, '2021-03-02 21:15:58', NULL),
(187, 359, 11, 10, 16, 95, 95, 5, NULL, '2021-03-02 21:15:58', NULL),
(190, 360, 11, 1, 4, 328, 360, 5, NULL, '2021-03-03 01:26:19', NULL),
(191, 360, 11, 10, 16, 82, 90, 5, NULL, '2021-03-03 01:26:19', NULL),
(194, 361, 11, 1, 4, 328, 360, 5, NULL, '2021-03-03 01:28:29', NULL),
(195, 361, 11, 10, 16, 82, 90, 5, NULL, '2021-03-03 01:28:29', NULL),
(198, 362, 11, 1, 4, 328, 360, 5, NULL, '2021-03-03 01:30:15', NULL),
(199, 362, 11, 10, 16, 82, 90, 5, NULL, '2021-03-03 01:30:15', NULL),
(202, 363, 11, 1, 4, 328, 360, 5, NULL, '2021-03-03 01:35:18', NULL),
(203, 363, 11, 10, 16, 82, 90, 5, NULL, '2021-03-03 01:35:18', NULL),
(208, 364, 11, 1, 4, 328, 360, 5, NULL, '2021-03-03 01:43:23', NULL),
(209, 364, 11, 10, 16, 82, 90, 5, NULL, '2021-03-03 01:43:23', NULL),
(214, 365, 11, 1, 4, 328, 360, 5, NULL, '2021-03-03 01:48:41', NULL),
(215, 365, 11, 10, 16, 82, 90, 5, NULL, '2021-03-03 01:48:41', NULL),
(218, 366, 11, 1, 4, 320, 380, 5, NULL, '2021-03-03 02:32:29', NULL),
(219, 366, 11, 10, 16, 80, 95, 5, NULL, '2021-03-03 02:32:29', NULL),
(222, 367, 11, 1, 4, 320, 380, 5, NULL, '2021-03-03 02:35:05', NULL),
(223, 367, 11, 10, 16, 80, 95, 5, NULL, '2021-03-03 02:35:05', NULL),
(226, 368, 11, 1, 4, 320, 380, 5, NULL, '2021-03-03 02:38:01', NULL),
(227, 368, 11, 10, 16, 80, 95, 5, NULL, '2021-03-03 02:38:01', NULL),
(232, 369, 11, 1, 4, 320, 380, 5, NULL, '2021-03-03 02:44:52', NULL),
(233, 369, 11, 10, 16, 80, 95, 5, NULL, '2021-03-03 02:44:52', NULL),
(236, 370, 11, 1, 4, 320, 380, 5, NULL, '2021-03-03 02:47:11', NULL),
(237, 370, 11, 10, 16, 80, 95, 5, NULL, '2021-03-03 02:47:11', NULL),
(240, 371, 11, 1, 4, 320, 380, 5, NULL, '2021-03-03 02:49:49', NULL),
(241, 371, 11, 10, 16, 80, 95, 5, NULL, '2021-03-03 02:49:49', NULL),
(256, 373, 11, 1, 4, 312, 320, 5, NULL, '2021-03-03 03:09:49', NULL),
(257, 373, 11, 10, 16, 78, 80, 5, NULL, '2021-03-03 03:09:49', NULL),
(258, 374, 11, 1, 4, 312, 320, 5, NULL, '2021-03-03 03:12:28', NULL),
(259, 374, 11, 10, 16, 78, 80, 5, NULL, '2021-03-03 03:12:28', NULL),
(262, 375, 11, 1, 4, 312, 320, 5, NULL, '2021-03-03 03:19:54', NULL),
(263, 375, 11, 10, 16, 78, 80, 5, NULL, '2021-03-03 03:19:54', NULL),
(266, 376, 11, 1, 4, 312, 320, 5, NULL, '2021-03-03 03:22:35', NULL),
(267, 376, 11, 11, 16, 78, 80, 5, NULL, '2021-03-03 03:22:35', NULL),
(272, 377, 11, 1, 4, 312, 320, 5, NULL, '2021-03-03 03:27:23', NULL),
(273, 377, 11, 10, 16, 78, 80, 5, NULL, '2021-03-03 03:27:23', NULL),
(278, 378, 11, 1, 4, 360, 360, 5, NULL, '2021-03-03 22:05:45', NULL),
(279, 378, 11, 10, 16, 90, 90, 5, NULL, '2021-03-03 22:05:45', NULL),
(399, 379, 11, 1, 9, 112, 122.66, 5, NULL, '2021-03-07 17:07:54', NULL),
(400, 379, 11, 10, 16, 63, 69, 5, NULL, '2021-03-07 17:07:54', NULL),
(401, 380, 11, 1, 9, 112, 122.66, 5, NULL, '2021-03-07 17:08:43', NULL),
(402, 380, 11, 10, 16, 63, 69, 5, NULL, '2021-03-07 17:08:43', NULL),
(403, 381, 11, 1, 9, 112, 122.66, 5, NULL, '2021-03-07 17:10:05', NULL),
(404, 381, 11, 10, 16, 63, 69, 5, NULL, '2021-03-07 17:10:05', NULL),
(405, 382, 11, 1, 9, 112, 122.66, 5, NULL, '2021-03-07 17:10:38', NULL),
(406, 382, 11, 10, 16, 63, 69, 5, NULL, '2021-03-07 17:10:38', NULL),
(407, 383, 11, 1, 9, 112, 122.66, 5, NULL, '2021-03-07 17:11:02', NULL),
(408, 383, 11, 10, 16, 63, 69, 5, NULL, '2021-03-07 17:11:02', NULL),
(409, 384, 11, 1, 9, 112, 122.66, 5, NULL, '2021-03-07 17:11:35', NULL),
(410, 384, 11, 10, 16, 63, 69, 5, NULL, '2021-03-07 17:11:35', NULL),
(411, 385, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:13:01', NULL),
(412, 385, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:13:01', NULL),
(413, 386, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:13:46', NULL),
(414, 386, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:13:46', NULL),
(415, 387, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:14:19', NULL),
(416, 387, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:14:19', NULL),
(417, 388, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:14:44', NULL),
(418, 388, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:14:44', NULL),
(419, 389, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:15:23', NULL),
(420, 389, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:15:23', NULL),
(423, 391, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:16:28', NULL),
(424, 391, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:16:28', NULL),
(425, 392, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:17:38', NULL),
(426, 392, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:17:38', NULL),
(427, 390, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:17:57', NULL),
(428, 390, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:17:57', NULL),
(429, 393, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:18:39', NULL),
(430, 393, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:18:39', NULL),
(431, 394, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:19:10', NULL),
(432, 394, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:19:10', NULL),
(433, 395, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:19:43', NULL),
(434, 395, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:19:43', NULL),
(435, 396, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:20:17', NULL),
(436, 396, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:20:17', NULL),
(437, 397, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:20:51', NULL),
(438, 397, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:20:51', NULL),
(439, 398, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:21:15', NULL),
(440, 398, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:21:15', NULL),
(441, 399, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:21:50', NULL),
(442, 399, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:21:50', NULL),
(443, 400, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:23:20', NULL),
(444, 400, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:23:20', NULL),
(445, 401, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:24:10', NULL),
(446, 401, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:24:10', NULL),
(447, 402, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:24:42', NULL),
(448, 402, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:24:42', NULL),
(449, 403, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:25:08', NULL),
(450, 403, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:25:08', NULL),
(451, 404, 11, 1, 9, 110.22, 115.55, 5, NULL, '2021-03-07 17:25:50', NULL),
(452, 404, 11, 10, 16, 62, 65, 5, NULL, '2021-03-07 17:25:50', NULL),
(455, 406, 11, 1, 9, 108.44, 108.44, 5, NULL, '2021-03-07 17:31:28', NULL),
(456, 406, 11, 10, 16, 61, 61, 5, NULL, '2021-03-07 17:31:28', NULL),
(458, 407, 11, 1, 9, 108.44, 108.44, 5, NULL, '2021-03-07 17:34:00', NULL),
(459, 407, 11, 10, 16, 61, 61, 5, NULL, '2021-03-07 17:34:00', NULL),
(464, 408, 11, 1, 9, 108.44, 108.44, 5, NULL, '2021-03-07 17:35:58', NULL),
(465, 408, 11, 10, 16, 61, 61, 5, NULL, '2021-03-07 17:35:58', NULL),
(468, 409, 11, 1, 9, 108.44, 108.44, 5, NULL, '2021-03-07 17:40:15', NULL),
(469, 409, 11, 11, 16, 61, 61, 5, NULL, '2021-03-07 17:40:15', NULL),
(472, 410, 11, 1, 9, 108.44, 108.44, 5, NULL, '2021-03-07 17:42:05', NULL),
(473, 410, 11, 10, 16, 61, 61, 5, NULL, '2021-03-07 17:42:05', NULL),
(476, 411, 11, 1, 9, 108.44, 108.44, 5, NULL, '2021-03-07 17:43:19', NULL),
(477, 411, 11, 10, 16, 61, 61, 5, NULL, '2021-03-07 17:43:19', NULL),
(482, 412, 11, 1, 9, 108.44, 108.44, 5, NULL, '2021-03-07 17:45:09', NULL),
(483, 412, 11, 10, 16, 61, 61, 5, NULL, '2021-03-07 17:45:09', NULL),
(486, 413, 11, 1, 9, 108.44, 108.44, 5, NULL, '2021-03-07 17:46:28', NULL),
(487, 413, 11, 10, 16, 61, 61, 5, NULL, '2021-03-07 17:46:28', NULL),
(492, 414, 11, 1, 9, 108.44, 108.44, 5, NULL, '2021-03-07 17:49:17', NULL),
(493, 414, 11, 10, 16, 61, 61, 5, NULL, '2021-03-07 17:49:17', NULL),
(496, 415, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 19:20:17', NULL),
(497, 415, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 19:20:17', NULL),
(500, 416, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 19:22:31', NULL),
(501, 416, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 19:22:31', NULL),
(504, 417, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 19:24:04', NULL),
(505, 417, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 19:24:04', NULL),
(512, 419, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 19:27:09', NULL),
(513, 419, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 19:27:09', NULL),
(516, 420, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 19:28:28', NULL),
(517, 420, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 19:28:28', NULL),
(520, 421, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 19:30:08', NULL),
(521, 421, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 19:30:08', NULL),
(524, 422, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 19:31:47', NULL),
(525, 422, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 19:31:47', NULL),
(528, 423, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 19:33:09', NULL),
(529, 423, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 19:33:09', NULL),
(532, 424, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 19:36:45', NULL),
(533, 424, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 19:36:45', NULL),
(536, 425, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 19:38:24', NULL),
(537, 425, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 19:38:24', NULL),
(540, 426, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 19:39:59', NULL),
(541, 426, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 19:39:59', NULL),
(544, 427, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 19:41:29', NULL),
(545, 427, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 19:41:29', NULL),
(548, 428, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 19:43:32', NULL),
(549, 428, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 19:43:32', NULL),
(552, 429, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 19:45:13', NULL),
(553, 429, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 19:45:13', NULL),
(556, 430, 11, 1, 16, 55, 55, 5, NULL, '2021-03-07 19:49:51', NULL),
(557, 430, 11, 10, 16, 55, 55, 5, NULL, '2021-03-07 19:49:51', NULL),
(560, 431, 11, 1, 16, 56, 56, 5, NULL, '2021-03-07 19:52:11', NULL),
(561, 431, 11, 10, 16, 56, 56, 5, NULL, '2021-03-07 19:52:11', NULL),
(564, 432, 11, 1, 16, 56, 56, 5, NULL, '2021-03-07 19:54:32', NULL),
(565, 432, 11, 10, 16, 56, 56, 5, NULL, '2021-03-07 19:54:32', NULL),
(568, 433, 11, 1, 16, 56, 56, 5, NULL, '2021-03-07 19:56:09', NULL),
(569, 433, 11, 10, 16, 56, 56, 5, NULL, '2021-03-07 19:56:09', NULL),
(572, 434, 11, 1, 16, 56, 56, 5, NULL, '2021-03-07 19:58:16', NULL),
(573, 434, 11, 10, 16, 56, 56, 5, NULL, '2021-03-07 19:58:16', NULL),
(576, 435, 11, 1, 16, 56, 56, 5, NULL, '2021-03-07 20:00:54', NULL),
(577, 435, 11, 10, 16, 56, 56, 5, NULL, '2021-03-07 20:00:54', NULL),
(580, 436, 11, 1, 16, 56, 56, 5, NULL, '2021-03-07 20:02:46', NULL),
(581, 436, 11, 10, 16, 56, 56, 5, NULL, '2021-03-07 20:02:46', NULL),
(582, 372, 11, 1, 4, 320, 380, 5, NULL, '2021-03-07 20:42:00', NULL),
(583, 372, 11, 10, 16, 80, 95, 5, NULL, '2021-03-07 20:42:00', NULL),
(584, 418, 11, 1, 16, 57, 60, 5, NULL, '2021-03-07 20:45:05', NULL),
(585, 418, 11, 10, 16, 57, 60, 5, NULL, '2021-03-07 20:45:05', NULL),
(588, 437, 11, 1, 8, 144, 144, 5, NULL, '2021-03-07 21:31:02', NULL),
(589, 437, 11, 10, 16, 72, 72, 5, NULL, '2021-03-07 21:31:02', NULL),
(594, 438, 11, 1, 8, 144, 144, 5, NULL, '2021-03-07 21:35:29', NULL),
(595, 438, 11, 10, 16, 72, 72, 5, NULL, '2021-03-07 21:35:29', NULL),
(598, 439, 11, 1, 8, 148, 148, 5, NULL, '2021-03-07 21:37:27', NULL),
(599, 439, 11, 10, 16, 74, 74, 5, NULL, '2021-03-07 21:37:27', NULL),
(602, 440, 11, 1, 8, 144, 144, 5, NULL, '2021-03-07 21:40:08', NULL),
(603, 440, 11, 10, 16, 72, 72, 5, NULL, '2021-03-07 21:40:08', NULL),
(606, 441, 11, 1, 8, 144, 144, 5, NULL, '2021-03-07 21:42:43', NULL),
(607, 441, 11, 10, 16, 72, 72, 5, NULL, '2021-03-07 21:42:43', NULL),
(610, 442, 11, 1, 8, 148, 148, 5, NULL, '2021-03-07 21:45:23', NULL),
(611, 442, 11, 10, 16, 74, 74, 5, NULL, '2021-03-07 21:45:23', NULL),
(614, 443, 11, 1, 8, 144, 144, 5, NULL, '2021-03-07 21:47:38', NULL),
(615, 443, 11, 10, 16, 72, 72, 5, NULL, '2021-03-07 21:47:38', NULL),
(620, 444, 11, 1, 8, 144, 144, 5, NULL, '2021-03-07 22:02:57', NULL),
(621, 444, 11, 10, 16, 72, 72, 5, NULL, '2021-03-07 22:02:57', NULL),
(624, 445, 11, 1, 8, 148, 148, 5, NULL, '2021-03-07 22:04:37', NULL),
(625, 445, 11, 10, 16, 74, 74, 5, NULL, '2021-03-07 22:04:37', NULL),
(630, 446, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 00:18:39', NULL),
(631, 446, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 00:18:39', NULL),
(634, 447, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 00:21:10', NULL),
(635, 447, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 00:21:10', NULL),
(638, 448, 11, 1, 8, 148, 148, 5, NULL, '2021-03-08 17:01:39', NULL),
(639, 448, 11, 10, 16, 74, 74, 5, NULL, '2021-03-08 17:01:39', NULL),
(642, 449, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 17:06:36', NULL),
(643, 449, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 17:06:36', NULL),
(646, 450, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 17:08:10', NULL),
(647, 450, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 17:08:10', NULL),
(650, 451, 11, 1, 8, 148, 148, 5, NULL, '2021-03-08 17:09:55', NULL),
(651, 451, 11, 10, 16, 74, 74, 5, NULL, '2021-03-08 17:09:55', NULL),
(654, 452, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 17:12:41', NULL),
(655, 452, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 17:12:41', NULL),
(658, 453, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 17:14:14', NULL),
(659, 453, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 17:14:14', NULL),
(662, 454, 11, 1, 8, 148, 148, 5, NULL, '2021-03-08 17:18:08', NULL),
(663, 454, 11, 10, 16, 74, 74, 5, NULL, '2021-03-08 17:18:08', NULL),
(666, 455, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 17:20:12', NULL),
(667, 455, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 17:20:12', NULL),
(670, 456, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 17:24:02', NULL),
(671, 456, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 17:24:02', NULL),
(674, 457, 11, 1, 8, 148, 148, 5, NULL, '2021-03-08 17:25:57', NULL),
(675, 457, 11, 10, 16, 74, 74, 5, NULL, '2021-03-08 17:25:57', NULL),
(678, 458, 11, 1, 8, 148, 148, 5, NULL, '2021-03-08 17:28:22', NULL),
(679, 458, 11, 10, 16, 74, 74, 5, NULL, '2021-03-08 17:28:22', NULL),
(682, 459, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 17:30:35', NULL),
(683, 459, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 17:30:35', NULL),
(686, 460, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 17:32:12', NULL),
(687, 460, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 17:32:12', NULL),
(690, 461, 11, 1, 8, 148, 148, 5, NULL, '2021-03-08 17:42:46', NULL),
(691, 461, 11, 10, 16, 74, 74, 5, NULL, '2021-03-08 17:42:46', NULL),
(696, 462, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 17:46:28', NULL),
(697, 462, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 17:46:28', NULL),
(700, 463, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 17:47:43', NULL),
(701, 463, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 17:47:43', NULL),
(704, 464, 11, 1, 8, 148, 148, 5, NULL, '2021-03-08 17:50:19', NULL),
(705, 464, 11, 10, 16, 74, 74, 5, NULL, '2021-03-08 17:50:19', NULL),
(710, 465, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 18:01:48', NULL),
(711, 465, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 18:01:48', NULL),
(714, 466, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 18:04:37', NULL),
(715, 466, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 18:04:37', NULL),
(720, 467, 11, 1, 8, 148, 148, 5, NULL, '2021-03-08 18:06:58', NULL),
(721, 467, 11, 10, 16, 74, 74, 5, NULL, '2021-03-08 18:06:58', NULL),
(724, 468, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 18:10:49', NULL),
(725, 468, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 18:10:49', NULL),
(728, 469, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 18:13:31', NULL),
(729, 469, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 18:13:31', NULL),
(731, 470, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 18:15:24', NULL),
(732, 470, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 18:15:24', NULL),
(735, 471, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 18:21:17', NULL),
(736, 471, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 18:21:17', NULL),
(739, 472, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 18:23:54', NULL),
(740, 472, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 18:23:54', NULL),
(743, 473, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 18:25:47', NULL),
(744, 473, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 18:25:47', NULL),
(747, 474, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 18:28:21', NULL),
(748, 474, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 18:28:21', NULL),
(751, 475, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 18:30:31', NULL),
(752, 475, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 18:30:31', NULL),
(757, 476, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 18:37:30', NULL),
(758, 476, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 18:37:30', NULL),
(761, 477, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 18:39:32', NULL),
(762, 477, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 18:39:32', NULL),
(765, 478, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 18:41:38', NULL),
(766, 478, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 18:41:38', NULL),
(769, 479, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 18:43:20', NULL),
(770, 479, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 18:43:20', NULL),
(773, 480, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 18:45:00', NULL),
(774, 480, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 18:45:00', NULL),
(777, 481, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 18:46:44', NULL),
(778, 481, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 18:46:44', NULL),
(781, 482, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 18:48:10', NULL),
(782, 482, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 18:48:10', NULL),
(787, 483, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 18:51:09', NULL),
(788, 483, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 18:51:09', NULL),
(791, 484, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 18:54:39', NULL),
(792, 484, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 18:54:39', NULL),
(795, 485, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 18:57:31', NULL),
(796, 485, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 18:57:31', NULL),
(801, 486, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 19:01:16', NULL),
(802, 486, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 19:01:16', NULL),
(805, 487, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 19:04:10', NULL),
(806, 487, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 19:04:10', NULL),
(809, 488, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 19:08:35', NULL),
(810, 488, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 19:08:35', NULL),
(813, 489, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 19:10:20', NULL),
(814, 489, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 19:10:20', NULL),
(817, 490, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 19:12:12', NULL),
(818, 490, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 19:12:12', NULL),
(821, 491, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 19:14:05', NULL),
(822, 491, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 19:14:05', NULL),
(825, 492, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 19:15:39', NULL),
(826, 492, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 19:15:39', NULL),
(831, 493, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 20:11:35', NULL),
(832, 493, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 20:11:35', NULL),
(841, 494, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 20:15:52', NULL),
(842, 494, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 20:15:52', NULL),
(845, 495, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 20:17:17', NULL),
(846, 495, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 20:17:17', NULL),
(849, 496, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 20:19:10', NULL),
(850, 496, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 20:19:10', NULL),
(853, 497, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 20:22:10', NULL),
(854, 497, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 20:22:10', NULL),
(859, 498, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 21:40:56', NULL),
(860, 498, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 21:40:56', NULL),
(863, 499, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 21:44:01', NULL),
(864, 499, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 21:44:01', NULL),
(869, 500, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 21:47:01', NULL),
(870, 500, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 21:47:01', NULL),
(879, 502, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 21:57:26', NULL),
(880, 502, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 21:57:26', NULL),
(885, 503, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 22:03:32', NULL),
(886, 503, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 22:03:32', NULL),
(889, 501, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 22:17:32', NULL),
(890, 501, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 22:17:32', NULL),
(891, 504, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 22:19:46', NULL),
(892, 504, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 22:19:46', NULL),
(895, 505, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 22:21:25', NULL),
(896, 505, 11, 11, 16, 68, 68, 5, NULL, '2021-03-08 22:21:25', NULL),
(901, 506, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 22:24:08', NULL),
(902, 506, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 22:24:08', NULL),
(905, 507, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 22:26:24', NULL),
(906, 507, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 22:26:24', NULL),
(909, 508, 11, 1, 8, 136, 136, 5, NULL, '2021-03-08 22:28:03', NULL),
(910, 508, 11, 10, 16, 68, 68, 5, NULL, '2021-03-08 22:28:03', NULL),
(913, 509, 11, 1, 8, 144, 144, 5, NULL, '2021-03-08 22:29:50', NULL),
(914, 509, 11, 10, 16, 72, 72, 5, NULL, '2021-03-08 22:29:50', NULL),
(927, 511, 11, 1, 10, 103.333292, 103.333292, 5, NULL, '2021-03-09 00:10:04', NULL),
(928, 511, 11, 10, 16.66666, 62, 62, 5, NULL, '2021-03-09 00:10:04', NULL),
(933, 405, 11, 1, 10, 96.666628, 96.666628, 5, NULL, '2021-03-09 00:14:42', NULL),
(934, 405, 11, 10, 16.66666, 58, 58, 5, NULL, '2021-03-09 00:14:42', NULL),
(935, 512, 11, 1, 10, 96.666628, 96.666628, 5, NULL, '2021-03-09 00:16:34', NULL),
(936, 512, 11, 10, 16.66666, 58, 58, 5, NULL, '2021-03-09 00:16:34', NULL),
(939, 513, 11, 1, 10, 96.666628, 96.666628, 5, NULL, '2021-03-09 00:20:50', NULL),
(940, 513, 11, 10, 16.66666, 58, 58, 5, NULL, '2021-03-09 00:20:50', NULL),
(949, 514, 11, 1, 10, 103.333292, 103.333292, 5, NULL, '2021-03-09 00:27:19', NULL),
(950, 514, 11, 10, 16.66666, 62, 62, 5, NULL, '2021-03-09 00:27:19', NULL),
(951, 510, 11, 1, 10, 96.666628, 96.666628, 5, NULL, '2021-03-09 00:30:08', NULL),
(952, 510, 11, 10, 16.66666, 58, 58, 5, NULL, '2021-03-09 00:30:08', NULL),
(953, 515, 11, 1, 10, 96.666628, 96.666628, 5, NULL, '2021-03-09 00:32:26', NULL),
(954, 515, 11, 10, 16.66666, 58, 58, 5, NULL, '2021-03-09 00:32:26', NULL),
(959, 516, 11, 1, 10, 96.666628, 96.666628, 5, NULL, '2021-03-09 00:35:14', NULL),
(960, 516, 11, 10, 16.66666, 58, 58, 5, NULL, '2021-03-09 00:35:14', NULL),
(963, 517, 11, 1, 10, 103.333292, 103.333292, 5, NULL, '2021-03-09 00:36:50', NULL),
(964, 517, 11, 10, 16.66666, 62, 62, 5, NULL, '2021-03-09 00:36:50', NULL),
(967, 518, 11, 1, 10, 96.666628, 96.666628, 5, NULL, '2021-03-09 03:43:10', NULL),
(968, 518, 11, 10, 16.66666, 58, 58, 5, NULL, '2021-03-09 03:43:10', NULL),
(971, 519, 11, 1, 10, 96.666628, 96.666628, 5, NULL, '2021-03-09 03:46:18', NULL),
(972, 519, 11, 10, 16.66666, 58, 58, 5, NULL, '2021-03-09 03:46:18', NULL),
(975, 520, 11, 1, 10, 103.333292, 103.333292, 5, NULL, '2021-03-09 03:49:42', NULL),
(976, 520, 11, 10, 16.66666, 62, 62, 5, NULL, '2021-03-09 03:49:42', NULL),
(979, 521, 11, 1, 10, 96.666628, 96.666628, 5, NULL, '2021-03-09 03:52:32', NULL),
(980, 521, 11, 10, 16.66666, 58, 58, 5, NULL, '2021-03-09 03:52:32', NULL),
(983, 522, 11, 1, 10, 96.666628, 96.666628, 5, NULL, '2021-03-09 03:55:40', NULL),
(984, 522, 11, 10, 16.66666, 58, 58, 5, NULL, '2021-03-09 03:55:40', NULL),
(989, 523, 11, 1, 10, 103.333292, 103.333292, 5, NULL, '2021-03-09 04:51:03', NULL),
(990, 523, 11, 10, 16.66666, 62, 62, 5, NULL, '2021-03-09 04:51:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `urls`
--

CREATE TABLE `urls` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `urls`
--

INSERT INTO `urls` (`id`, `name`, `url`, `module_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(85, 'Product Categories Create', 'products_category_index', 1, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(86, 'Product Categories Store', 'products_category_store', 1, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(87, 'Product Categories Edit', 'products_category_edit', 1, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(88, 'Product Categories Update', 'products_category_update', 1, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(89, 'Product Create', 'products_index', 2, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(90, 'Product Store', 'products_store', 2, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(91, 'Product Edit', 'products_edit', 2, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(92, 'Product Update', 'products_update', 2, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(93, 'All Orders', 'invoices_all_sales', 3, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(94, 'Order Create', 'invoices_index', 3, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(95, 'Order Store', 'invoices_store', 3, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(96, 'Order Edit', 'invoices_edit', 3, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(97, 'Order Update', 'invoices_update', 3, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(98, 'Order Show A4 Size', 'invoices_show', 3, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(99, 'Order Show Pos Printer', 'invoices_show_pos', 3, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(100, 'All Purchases', 'bills_all_bills', 4, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(101, 'Purchase Create', 'bills_index', 4, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(102, 'Purchase Store', 'bills_edit', 4, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(103, 'Purchase Edit', 'bills_store', 4, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(104, 'Purchase Update', 'bills_update', 4, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(105, 'Purchase Show', 'bills_show', 4, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(106, 'Sales Return Create', 'sales_return_index', 5, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(107, 'Sales Return Store', 'sales_return_store', 5, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(108, 'Sales Return Show', 'sales_return_show', 5, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(109, 'Sales Return Delete', 'sales_return_delete', 5, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(110, 'Purchase Return Create', 'purchase_return_index', 6, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(111, 'Purchase Return Store', 'purchase_return_store', 6, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(112, 'Purchase Return Show', 'purchase_return_show', 6, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(113, 'Purchase Return Delete', 'purchase_return_delete', 6, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(114, 'Expense Categories Store', 'expenses_categories_store', 7, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(115, 'Expense Categories Edit', 'expenses_categories_edit', 7, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(116, 'Expense Categories Update', 'expenses_categories_update', 7, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(117, 'Expenses Create', 'expenses_index', 7, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(118, 'Expenses Store', 'expenses_store', 7, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(119, 'Expenses Edit', 'expenses_edit', 7, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(120, 'Expenses Update', 'expenses_update', 7, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(121, 'Payments Create', 'payments_create', 8, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(122, 'Payments Store', 'payments_store', 8, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(123, 'Payments Show', 'payments_print', 8, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(124, 'Payments Delete', 'payments_delete', 8, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(125, 'Contacts Create', 'customers_index', 9, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(126, 'Contacts Store', 'customers_store', 9, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(127, 'Contacts Edit', 'customers_edit', 9, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(128, 'Contacts Update', 'customers_update', 9, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(129, 'Paid Through Accounts Create', 'paid_through_accounts_index', 10, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(130, 'Paid Through Accounts Store', 'paid_through_accounts_store', 10, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(131, 'Paid Through Accounts Edit', 'paid_through_accounts_edit', 10, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(132, 'Paid Through Accounts Update', 'paid_through_accounts_update', 10, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(133, 'Units Create', 'products_units_index', 11, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(134, 'Units Store', 'products_units_store', 11, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(135, 'Units Edit', 'products_units_edit', 11, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(136, 'Units Update', 'products_units_update', 11, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(137, 'Employee Salary Create', 'expenses_employee_salary_index', 12, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(138, 'Employee Salary Store', 'expenses_employee_salary_store', 12, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(139, 'Employee Salary Edit', 'expenses_employee_salary_edit', 12, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(140, 'Employee Salary Update', 'expenses_employee_salary_update', 12, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(141, 'Bar Code Generate', 'products_barcode_print', 13, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(142, 'Bar Code Print', 'products_barcode_print_print', 13, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(143, 'Discounts Create', 'discounts_index', 14, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(144, 'Discounts Store', 'discounts_store', 14, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(145, 'Discounts Edit', 'discounts_edit', 14, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(146, 'Discounts Update', 'discounts_update', 14, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(147, 'Users Create', 'users_index', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(148, 'Users Store', 'users_store', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(149, 'Users Edit', 'users_edit', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(150, 'Users Update', 'users_update', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(151, 'Users Delete', 'users_destroy', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(152, 'Users Profile Edit', 'users_edit_profile', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(153, 'Users Profile Update', 'users_update_profile', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(154, 'Users Settings Edit', 'users_edit_settings', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(155, 'Users Settings Update', 'users_update_settings', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(156, 'Access Level Create', 'set_access_index', 17, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(157, 'Access Level Update', 'set_access_update', 17, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(158, 'Stock Report', 'stock_report_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(159, 'Profit Loss Report', 'profit_loss_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(160, 'Sales Report', 'sales_report_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(161, 'Sales Summary Report', 'sales_summary_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(162, 'Purchase Report', 'purchase_report_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(163, 'Purchase Summary Report', 'purchase_summary_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(164, 'Customer Due Report', 'due_report_customer_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(165, 'Supplier Due Report', 'due_report_supplier_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(166, 'Payment Report', 'payment_report_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(167, 'Expense Rerport', 'expense_report_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(168, 'Collection Report', 'collection_report_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(169, 'SalaryReport', 'salary_report_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` tinyint(4) NOT NULL COMMENT '1=Super Admin 2=Admin 3=Employee 4=cutomer',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=Active\r\n0=Inactive',
  `organization_name` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `contact_number` text COLLATE utf8mb4_unicode_ci,
  `contact_email` text COLLATE utf8mb4_unicode_ci,
  `website` text COLLATE utf8mb4_unicode_ci,
  `header_image` text COLLATE utf8mb4_unicode_ci,
  `footer_image` text COLLATE utf8mb4_unicode_ci,
  `sales_show` double DEFAULT NULL,
  `vat_reg_number` text COLLATE utf8mb4_unicode_ci,
  `mushak` text COLLATE utf8mb4_unicode_ci,
  `vat_type` tinyint(4) DEFAULT NULL,
  `vat_amount` double DEFAULT NULL,
  `tax_type` tinyint(4) DEFAULT NULL,
  `tax_amount` double DEFAULT NULL,
  `pos_printer` tinyint(4) DEFAULT '1' COMMENT '0= 58 mm Label Size 1 = 80 mm Label Size 2 = A4 3 = letter',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `photo`, `logo`, `remember_token`, `role`, `branch_id`, `status`, `organization_name`, `address`, `contact_number`, `contact_email`, `website`, `header_image`, `footer_image`, `sales_show`, `vat_reg_number`, `mushak`, `vat_type`, `vat_amount`, `tax_type`, `tax_amount`, `pos_printer`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Cyberdyne Technology Ltd.', 'superadmin', '0000-00-00 00:00:00', '$2y$10$e4t28BJ6.DSK1gmQ/IO3qOB0WqszhEzUvr0HGEypWGuHLuBY9zC3S', 'company-profile-images/1611226529.png', 'company-profile-images/1614513131.jpg', '', 1, 1, 1, 'RONGDHONO TRADERS', 'KANKORD TAWER, ZELKHANAR MORE, VALANAGOR, NARSINGDI.', '01729164879', 'info@cyberdynetechnologyltd.com', 'www.cyberdynetechnologyltd.com', '', '', 0, '12-23-34-21', '1001', 0, 0, 0, 0, 3, 1, 1, '2020-04-23 15:06:17', '2021-03-01 00:52:46'),
(5, 'Super Admin', '01729164879', NULL, '$2y$10$YgGKHREzkx7WXHzc5oxAROzLNSTA82Xsdv3QPAlGKB1EqSVWzcFJy', NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 5, '2021-01-31 10:39:24', '2021-03-01 00:11:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_transactions`
--
ALTER TABLE `account_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transfer_from` (`transfer_from`),
  ADD KEY `transfer_to` (`transfer_to`);

--
-- Indexes for table `backup`
--
ALTER TABLE `backup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bills_78_vendor_id_foreign` (`vendor_id`),
  ADD KEY `bills_78_created_by_foreign` (`created_by`),
  ADD KEY `bills_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `bill_entries`
--
ALTER TABLE `bill_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_entries_78_bill_id_foreign` (`bill_id`),
  ADD KEY `bill_entries_78_product_id_foreign` (`product_id`),
  ADD KEY `bill_entries_78_product_entry_id_foreign` (`product_entry_id`),
  ADD KEY `bill_entries_78_vendor_id_foreign` (`vendor_id`),
  ADD KEY `bill_entries_78_created_by_foreign` (`created_by`),
  ADD KEY `bill_entries_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `categories_78_created_by_foreign` (`created_by`),
  ADD KEY `categories_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discounts_78_created_by_foreign` (`created_by`),
  ADD KEY `discounts_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `discount_products`
--
ALTER TABLE `discount_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discount_products_78_product_id_foreign` (`product_id`),
  ADD KEY `discount_products_78_discount_id_foreign` (`discount_id`),
  ADD KEY `discount_products_78_created_by_foreign` (`created_by`),
  ADD KEY `discount_products_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_78_expense_category_id_foreign` (`expense_category_id`),
  ADD KEY `expenses_78_user_id_foreign` (`user_id`),
  ADD KEY `expenses_78_created_by_foreign` (`created_by`),
  ADD KEY `expenses_78_updated_by_foreign` (`updated_by`),
  ADD KEY `paid_through_id` (`paid_through_id`);

--
-- Indexes for table `expense_categories`
--
ALTER TABLE `expense_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expense_categories_78_created_by_foreign` (`created_by`),
  ADD KEY `expense_categories_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incomes`
--
ALTER TABLE `incomes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `income_category_id` (`income_category_id`),
  ADD KEY `paid_through_id` (`paid_through_id`);

--
-- Indexes for table `income_categories`
--
ALTER TABLE `income_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_78_customer_id_foreign` (`customer_id`),
  ADD KEY `invoices_78_created_by_foreign` (`created_by`),
  ADD KEY `invoices_78_updated_by_foreign` (`updated_by`),
  ADD KEY `branch_id` (`branch_id`);

--
-- Indexes for table `invoice_entries`
--
ALTER TABLE `invoice_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_entries_78_invoice_id_foreign` (`invoice_id`),
  ADD KEY `invoice_entries_78_product_id_foreign` (`product_id`),
  ADD KEY `invoice_entries_78_product_entry_id_foreign` (`product_entry_id`),
  ADD KEY `invoice_entries_78_customer_id_foreign` (`customer_id`),
  ADD KEY `invoice_entries_78_created_by_foreign` (`created_by`),
  ADD KEY `invoice_entries_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `items_78_sub_category_id_foreign` (`sub_category_id`),
  ADD KEY `items_78_created_by_foreign` (`created_by`),
  ADD KEY `items_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `message_deliveries`
--
ALTER TABLE `message_deliveries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `phone_book_id` (`phone_book_id`),
  ADD KEY `message_id` (`message_id`);

--
-- Indexes for table `message_lists`
--
ALTER TABLE `message_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `modules_78_created_by_foreign` (`created_by`),
  ADD KEY `modules_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `modules_access`
--
ALTER TABLE `modules_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `modules_access_78_module_id_foreign` (`module_id`),
  ADD KEY `modules_access_78_user_id_foreign` (`user_id`),
  ADD KEY `modules_access_78_created_by_foreign` (`created_by`),
  ADD KEY `modules_access_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `paid_through_accounts`
--
ALTER TABLE `paid_through_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `paid_through_accounts_78_created_by_foreign` (`created_by`),
  ADD KEY `paid_through_accounts_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_78_customer_id_foreign` (`customer_id`),
  ADD KEY `payments_78_paid_through_foreign` (`paid_through`),
  ADD KEY `payments_78_created_by_foreign` (`created_by`),
  ADD KEY `payments_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `payment_entries`
--
ALTER TABLE `payment_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_entries_78_payment_id_foreign` (`payment_id`),
  ADD KEY `payment_entries_78_invoice_id_foreign` (`invoice_id`),
  ADD KEY `payment_entries_78_bill_id_foreign` (`bill_id`),
  ADD KEY `payment_entries_78_sales_return_id_foreign` (`sales_return_id`),
  ADD KEY `payment_entries_78_purchase_return_id_foreign` (`purchase_return_id`),
  ADD KEY `payment_entries_78_created_by_foreign` (`created_by`),
  ADD KEY `payment_entries_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_78_url_id_foreign` (`url_id`),
  ADD KEY `permissions_78_user_id_foreign` (`user_id`),
  ADD KEY `permissions_78_created_by_foreign` (`created_by`),
  ADD KEY `permissions_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `phone_book`
--
ALTER TABLE `phone_book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_78_category_id_foreign` (`category_id`),
  ADD KEY `products_78_sub_category_id_foreign` (`sub_category_id`),
  ADD KEY `products_78_item_id_foreign` (`item_id`),
  ADD KEY `products_78_unit_foreign` (`unit`),
  ADD KEY `products_78_created_by_foreign` (`created_by`),
  ADD KEY `products_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `product_customers`
--
ALTER TABLE `product_customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `supplier_id` (`customer_id`),
  ADD KEY `product_entry_id` (`product_entry_id`);

--
-- Indexes for table `product_entries`
--
ALTER TABLE `product_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_entries_78_product_id_foreign` (`product_id`),
  ADD KEY `product_entries_78_unit_id_foreign` (`unit_id`),
  ADD KEY `product_entries_78_created_by_foreign` (`created_by`),
  ADD KEY `product_entries_78_updated_by_foreign` (`updated_by`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `product_suppliers`
--
ALTER TABLE `product_suppliers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `product_entry_id` (`product_entry_id`);

--
-- Indexes for table `product_variations`
--
ALTER TABLE `product_variations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `product_variation_entries`
--
ALTER TABLE `product_variation_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `product_entry_id` (`product_entry_id`),
  ADD KEY `variation_id` (`variation_id`),
  ADD KEY `variation_value_id` (`variation_value_id`);

--
-- Indexes for table `product_variation_values`
--
ALTER TABLE `product_variation_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `product_variation_id` (`product_variation_id`);

--
-- Indexes for table `purchase_return`
--
ALTER TABLE `purchase_return`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_return_78_bill_id_foreign` (`bill_id`),
  ADD KEY `purchase_return_78_customer_id_foreign` (`customer_id`),
  ADD KEY `purchase_return_78_created_by_foreign` (`created_by`),
  ADD KEY `purchase_return_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `purchase_return_entries`
--
ALTER TABLE `purchase_return_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_return_entries_78_purchase_return_id_foreign` (`purchase_return_id`),
  ADD KEY `purchase_return_entries_78_bill_id_foreign` (`bill_id`),
  ADD KEY `purchase_return_entries_78_product_id_foreign` (`product_id`),
  ADD KEY `purchase_return_entries_78_product_entry_id_foreign` (`product_entry_id`),
  ADD KEY `purchase_return_entries_78_customer_id_foreign` (`customer_id`),
  ADD KEY `purchase_return_entries_78_created_by_foreign` (`created_by`),
  ADD KEY `purchase_return_entries_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `sales_return`
--
ALTER TABLE `sales_return`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sales_return_78_invoice_id_foreign` (`invoice_id`),
  ADD KEY `sales_return_78_customer_id_foreign` (`customer_id`),
  ADD KEY `sales_return_78_created_by_foreign` (`created_by`),
  ADD KEY `sales_return_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `sales_return_entries`
--
ALTER TABLE `sales_return_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sales_return_entries_78_sales_return_id_foreign` (`sales_return_id`),
  ADD KEY `sales_return_entries_78_invoice_id_foreign` (`invoice_id`),
  ADD KEY `sales_return_entries_78_product_id_foreign` (`product_id`),
  ADD KEY `sales_return_entries_78_product_entry_id_foreign` (`product_entry_id`),
  ADD KEY `sales_return_entries_78_customer_id_foreign` (`customer_id`),
  ADD KEY `sales_return_entries_78_created_by_foreign` (`created_by`),
  ADD KEY `sales_return_entries_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_categories_78_category_id_foreign` (`category_id`),
  ADD KEY `sub_categories_78_created_by_foreign` (`created_by`),
  ADD KEY `sub_categories_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_78_invoice_id_foreign` (`invoice_id`),
  ADD KEY `transactions_78_bill_id_foreign` (`bill_id`),
  ADD KEY `transactions_78_sales_return_id_foreign` (`sales_return_id`),
  ADD KEY `transactions_78_purchase_return_id_foreign` (`purchase_return_id`),
  ADD KEY `transactions_78_expense_id_foreign` (`expense_id`),
  ADD KEY `transactions_78_payment_id_foreign` (`payment_id`),
  ADD KEY `transactions_78_customer_id_foreign` (`customer_id`),
  ADD KEY `transactions_78_paid_through_foreign` (`paid_through`),
  ADD KEY `transactions_78_created_by_foreign` (`created_by`),
  ADD KEY `transactions_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`),
  ADD KEY `units_78_created_by_foreign` (`created_by`),
  ADD KEY `units_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `unit_conversions`
--
ALTER TABLE `unit_conversions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `main_unit_id` (`main_unit_id`),
  ADD KEY `converted_unit_id` (`converted_unit_id`),
  ADD KEY `product_entry_id` (`product_entry_id`);

--
-- Indexes for table `urls`
--
ALTER TABLE `urls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `urls_78_module_id_foreign` (`module_id`),
  ADD KEY `urls_78_created_by_foreign` (`created_by`),
  ADD KEY `urls_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `branch_id` (`branch_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_transactions`
--
ALTER TABLE `account_transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `backup`
--
ALTER TABLE `backup`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `bills`
--
ALTER TABLE `bills`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `bill_entries`
--
ALTER TABLE `bill_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `discount_products`
--
ALTER TABLE `discount_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `expense_categories`
--
ALTER TABLE `expense_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `incomes`
--
ALTER TABLE `incomes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `income_categories`
--
ALTER TABLE `income_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `invoice_entries`
--
ALTER TABLE `invoice_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message_deliveries`
--
ALTER TABLE `message_deliveries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `message_lists`
--
ALTER TABLE `message_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `modules_access`
--
ALTER TABLE `modules_access`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `paid_through_accounts`
--
ALTER TABLE `paid_through_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;

--
-- AUTO_INCREMENT for table `payment_entries`
--
ALTER TABLE `payment_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=341;

--
-- AUTO_INCREMENT for table `phone_book`
--
ALTER TABLE `phone_book`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `product_customers`
--
ALTER TABLE `product_customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product_entries`
--
ALTER TABLE `product_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=524;

--
-- AUTO_INCREMENT for table `product_suppliers`
--
ALTER TABLE `product_suppliers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `product_variations`
--
ALTER TABLE `product_variations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `product_variation_entries`
--
ALTER TABLE `product_variation_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=551;

--
-- AUTO_INCREMENT for table `product_variation_values`
--
ALTER TABLE `product_variation_values`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `purchase_return`
--
ALTER TABLE `purchase_return`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `purchase_return_entries`
--
ALTER TABLE `purchase_return_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `sales_return`
--
ALTER TABLE `sales_return`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `sales_return_entries`
--
ALTER TABLE `sales_return_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `unit_conversions`
--
ALTER TABLE `unit_conversions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=991;

--
-- AUTO_INCREMENT for table `urls`
--
ALTER TABLE `urls`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account_transactions`
--
ALTER TABLE `account_transactions`
  ADD CONSTRAINT `account_transactions_ibfk_1` FOREIGN KEY (`transfer_from`) REFERENCES `paid_through_accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `account_transactions_ibfk_2` FOREIGN KEY (`transfer_to`) REFERENCES `paid_through_accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `backup`
--
ALTER TABLE `backup`
  ADD CONSTRAINT `backup_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `backup_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bills`
--
ALTER TABLE `bills`
  ADD CONSTRAINT `bills_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bills_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bills_78_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bill_entries`
--
ALTER TABLE `bill_entries`
  ADD CONSTRAINT `bill_entries_78_bill_id_foreign` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bill_entries_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bill_entries_78_product_entry_id_foreign` FOREIGN KEY (`product_entry_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bill_entries_78_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bill_entries_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bill_entries_78_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `branches`
--
ALTER TABLE `branches`
  ADD CONSTRAINT `branches_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `branches_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `categories_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customers_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customers_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `discounts`
--
ALTER TABLE `discounts`
  ADD CONSTRAINT `discounts_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `discounts_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `discount_products`
--
ALTER TABLE `discount_products`
  ADD CONSTRAINT `discount_products_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `discount_products_78_discount_id_foreign` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `discount_products_78_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `discount_products_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `expenses`
--
ALTER TABLE `expenses`
  ADD CONSTRAINT `expenses_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `expenses_78_expense_category_id_foreign` FOREIGN KEY (`expense_category_id`) REFERENCES `expense_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `expenses_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `expenses_78_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `expenses_ibfk_1` FOREIGN KEY (`paid_through_id`) REFERENCES `paid_through_accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `expense_categories`
--
ALTER TABLE `expense_categories`
  ADD CONSTRAINT `expense_categories_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `expense_categories_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `incomes`
--
ALTER TABLE `incomes`
  ADD CONSTRAINT `incomes_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `incomes_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `incomes_ibfk_3` FOREIGN KEY (`income_category_id`) REFERENCES `income_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `incomes_ibfk_4` FOREIGN KEY (`paid_through_id`) REFERENCES `paid_through_accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `income_categories`
--
ALTER TABLE `income_categories`
  ADD CONSTRAINT `income_categories_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `income_categories_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoices_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoices_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoices_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `invoice_entries`
--
ALTER TABLE `invoice_entries`
  ADD CONSTRAINT `invoice_entries_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoice_entries_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoice_entries_78_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoice_entries_78_product_entry_id_foreign` FOREIGN KEY (`product_entry_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoice_entries_78_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoice_entries_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `items_78_sub_category_id_foreign` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `items_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `message_deliveries`
--
ALTER TABLE `message_deliveries`
  ADD CONSTRAINT `message_deliveries_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_deliveries_ibfk_2` FOREIGN KEY (`phone_book_id`) REFERENCES `phone_book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_deliveries_ibfk_3` FOREIGN KEY (`message_id`) REFERENCES `message_lists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `message_lists`
--
ALTER TABLE `message_lists`
  ADD CONSTRAINT `message_lists_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_lists_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `modules`
--
ALTER TABLE `modules`
  ADD CONSTRAINT `modules_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `modules_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `modules_access`
--
ALTER TABLE `modules_access`
  ADD CONSTRAINT `modules_access_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `modules_access_78_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `modules_access_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `modules_access_78_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `paid_through_accounts`
--
ALTER TABLE `paid_through_accounts`
  ADD CONSTRAINT `paid_through_accounts_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `paid_through_accounts_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payments_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payments_78_paid_through_foreign` FOREIGN KEY (`paid_through`) REFERENCES `paid_through_accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payments_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payment_entries`
--
ALTER TABLE `payment_entries`
  ADD CONSTRAINT `payment_entries_78_bill_id_foreign` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_entries_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_entries_78_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_entries_78_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_entries_78_purchase_return_id_foreign` FOREIGN KEY (`purchase_return_id`) REFERENCES `purchase_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_entries_78_sales_return_id_foreign` FOREIGN KEY (`sales_return_id`) REFERENCES `sales_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_entries_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permissions_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permissions_78_url_id_foreign` FOREIGN KEY (`url_id`) REFERENCES `urls` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permissions_78_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_78_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_78_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_78_sub_category_id_foreign` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_78_unit_foreign` FOREIGN KEY (`unit`) REFERENCES `units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_entries`
--
ALTER TABLE `product_entries`
  ADD CONSTRAINT `product_entries_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_entries_78_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_entries_78_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_entries_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_entries_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_suppliers`
--
ALTER TABLE `product_suppliers`
  ADD CONSTRAINT `product_suppliers_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_suppliers_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_suppliers_ibfk_3` FOREIGN KEY (`supplier_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_suppliers_ibfk_4` FOREIGN KEY (`product_entry_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_variations`
--
ALTER TABLE `product_variations`
  ADD CONSTRAINT `product_variations_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_variations_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_variation_entries`
--
ALTER TABLE `product_variation_entries`
  ADD CONSTRAINT `product_variation_entries_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_variation_entries_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_variation_entries_ibfk_3` FOREIGN KEY (`product_entry_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_variation_entries_ibfk_4` FOREIGN KEY (`variation_id`) REFERENCES `product_variations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_variation_entries_ibfk_5` FOREIGN KEY (`variation_value_id`) REFERENCES `product_variation_values` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_variation_values`
--
ALTER TABLE `product_variation_values`
  ADD CONSTRAINT `product_variation_values_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_variation_values_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_variation_values_ibfk_3` FOREIGN KEY (`product_variation_id`) REFERENCES `product_variations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `purchase_return`
--
ALTER TABLE `purchase_return`
  ADD CONSTRAINT `purchase_return_78_bill_id_foreign` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `purchase_return_entries`
--
ALTER TABLE `purchase_return_entries`
  ADD CONSTRAINT `purchase_return_entries_78_bill_id_foreign` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_entries_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_entries_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_entries_78_product_entry_id_foreign` FOREIGN KEY (`product_entry_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_entries_78_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_entries_78_purchase_return_id_foreign` FOREIGN KEY (`purchase_return_id`) REFERENCES `purchase_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_entries_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sales_return`
--
ALTER TABLE `sales_return`
  ADD CONSTRAINT `sales_return_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_78_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sales_return_entries`
--
ALTER TABLE `sales_return_entries`
  ADD CONSTRAINT `sales_return_entries_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_entries_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_entries_78_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_entries_78_product_entry_id_foreign` FOREIGN KEY (`product_entry_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_entries_78_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_entries_78_sales_return_id_foreign` FOREIGN KEY (`sales_return_id`) REFERENCES `sales_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_entries_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_78_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_categories_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_categories_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_78_bill_id_foreign` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_expense_id_foreign` FOREIGN KEY (`expense_id`) REFERENCES `expenses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_paid_through_foreign` FOREIGN KEY (`paid_through`) REFERENCES `paid_through_accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_purchase_return_id_foreign` FOREIGN KEY (`purchase_return_id`) REFERENCES `purchase_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_sales_return_id_foreign` FOREIGN KEY (`sales_return_id`) REFERENCES `sales_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `units`
--
ALTER TABLE `units`
  ADD CONSTRAINT `units_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `units_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `unit_conversions`
--
ALTER TABLE `unit_conversions`
  ADD CONSTRAINT `unit_conversions_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `unit_conversions_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `unit_conversions_ibfk_3` FOREIGN KEY (`main_unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `unit_conversions_ibfk_4` FOREIGN KEY (`converted_unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `unit_conversions_ibfk_5` FOREIGN KEY (`product_entry_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `urls`
--
ALTER TABLE `urls`
  ADD CONSTRAINT `urls_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `urls_78_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `urls_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
