

<?php $__env->startSection('title', 'Delivery List'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Delivery List</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Message</a></li>
                                    <li class="breadcrumb-item active">Delivery List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button style="border-radius: 0px !important" type="button" class="btn btn-success waves-effect waves-light">
                                            <a style="color: white" href="<?php echo e(route('messages_delete_send_list')); ?>"> Empty Record </a>
                                        </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body table-responsive">
                                                <h4 class="card-title">All Messages</h4>

                                                <br>

                                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                                    <thead>
                                                        <tr>
                                                            <th>SL</th>
                                                            <th>Name</th>
                                                            <th>Phone</th>
                                                            <th>Message</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <?php if(!empty($messages) && ($messages->count() > 0)): ?>
                                                        <?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <tr>
                                                                <td><?php echo e($key + 1); ?></td>
                                                                <td><?php echo $message['customer_name'] != null ? $message['customer_name'] : $message['phone_name']; ?></td>
                                                                <td><?php echo $message['customer_phone'] != null ? $message['phone_name'] : $message['phone_phone']; ?></td>
                                                                <td><?php echo $message->message->title; ?></td>
                                                            </tr>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <?php endif; ?>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script src="<?php echo e(url('public/admin_panel_assets/editor/ckeditor.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/tuhin/5/Modules/Messages/Resources/views/delivery_message_list.blade.php ENDPATH**/ ?>