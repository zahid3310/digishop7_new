

<?php $__env->startSection('title', 'Edit Discounts'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Discounts</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Others</a></li>
                                    <li class="breadcrumb-item active">Edit Discounts</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('discounts_update', $find_discount['id'])); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                <?php echo e(csrf_field()); ?>


                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div id="contact_id_reload" class="form-group">
                                                <label class="control-label">Type *</label>
                                                <select style="width: 100%;cursor: pointer;" class="form-control" name="type" id="type" onchange="typeSelect()" required>
                                                    <?php if($find_discount['type'] == 0): ?>
                                                    <option value="0">Coupon Code</option>
                                                    <?php else: ?>
                                                    <option value="1">Membership Card</option>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div style="display: none" class="col-sm-6 coupon">
                                            <div class="form-group">
                                                <label for="coupon_code">Coupon Code</label>
                                                <input id="coupon_code" name="coupon_code" type="number" class="form-control" value="<?php echo e($find_discount['coupon_code']); ?>" readonly>
                                            </div>
                                        </div>

                                        <div style="display: none" class="col-sm-6 memberShip">
                                            <div class="form-group">
                                                <label for="card_number">Card Number *</label>
                                                <input id="card_number" name="card_number" type="number" class="form-control" value="<?php echo e($find_discount['card_number']); ?>">
                                            </div>
                                        </div>

                                        <div style="display: none" class="col-sm-6 memberShip">
                                            <div class="form-group">
                                                <label for="customer_name">Customer Name</label>
                                                <input id="customer_name" name="customer_name" type="text" class="form-control" value="<?php echo e($find_discount['customer_name']); ?>">
                                            </div>
                                        </div>

                                        <div style="display: none" class="col-sm-6 memberShip">
                                            <div class="form-group">
                                                <label for="customer_phone">Customer Phone</label>
                                                <input id="customer_phone" name="customer_phone" type="number" class="form-control" value="<?php echo e($find_discount['customer_phone']); ?>">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div id="contact_id_reload" class="form-group">
                                                <label class="control-label">Discount Type *</label>
                                                <select style="width: 100%" class="form-control" name="discount_type" required>
                                                    <option <?php echo e($find_discount['discount_type'] == 0 ? 'selected' : ''); ?> value="0">%</option>
                                                    <option <?php echo e($find_discount['discount_type'] == 1 ? 'selected' : ''); ?> value="1">BDT</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="amount">Amount *</label>
                                                <input id="amount" name="amount" type="text" class="form-control" value="<?php echo e($find_discount['discount_amount']); ?>" required>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="issue_date">Issue Date *</label>
                                                <input id="issue_date" name="issue_date" type="text" value="<?php echo e(date('d-m-Y', strtotime($find_discount['issue_date']))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="expire_date">Expire Date *</label>
                                                <input id="expire_date" name="expire_date" type="text" value="<?php echo e(date('d-m-Y', strtotime($find_discount['expire_date']))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="note">Note</label>
                                                <input id="note" name="note" type="text" class="form-control" value="<?php echo e($find_discount['note']); ?>">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div id="contact_id_reload" class="form-group">
                                                <label class="control-label">Status</label>
                                                <select style="width: 100%" class="form-control" name="status" required>
                                                    <option <?php echo e($find_discount['status'] == 1 ? 'selected' : ''); ?> value="1">Active</option>
                                                    <option <?php echo e($find_discount['status'] == 0 ? 'selected' : ''); ?> value="0">Inactive</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div id="contact_id_reload" class="form-group">
                                                <label class="control-label">Select Product</label>
                                                <select style="width: 100%" class="form-control select2" name="product_id[]" multiple>
                                                    <?php if((!empty($discount_products)) && ($discount_products->count() > 0)): ?>
                                                    <?php $__currentLoopData = $discount_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $discount_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($discount_product['product_id']); ?>" selected><?php echo e($discount_product['product_name']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>

                                                    <?php if((!empty($products)) && ($products->count() > 0)): ?>
                                                    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($product['id']); ?>"><?php echo e($product['name']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <hr style="margin-top: 0px">

                                    <div class="form-group row">
                                        <div class="button-items col-md-12">
                                            <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('expenses_index')); ?>">Close</a></button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <hr>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">Discount List</h4>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Coupon#</th>
                                            <th>Card#</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Issue Date</th>
                                            <th>Expire Date</th>
                                            <th>Amount</th>
                                            <th>Products</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php if(!empty($discounts) && ($discounts->count() > 0)): ?>
                                        <?php $__currentLoopData = $discounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $discount): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php
                                                $discount_type      = $discount['discount_type'] == 0 ? '%' : 'BDT';
                                                $discount_products  = discountProducts($discount['id']); 
                                            ?>
                                            <tr>
                                                <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e($discount['coupon_code']); ?></td>
                                                <td><?php echo e($discount['card_number']); ?></td>
                                                <td><?php echo e($discount['name']); ?></td>
                                                <td><?php echo e($discount['phone_number']); ?></td>
                                                <td><?php echo e(date('d-m-Y', strtotime($discount['issue_date']))); ?></td>
                                                <td><?php echo e(date('d-m-Y', strtotime($discount['expire_date']))); ?></td>
                                                <td><?php echo e($discount['discount_amount'] . ' ' . $discount_type); ?></td>
                                                <td>
                                                    <?php if($discount_products->count() > 0): ?>
                                                        <?php $__currentLoopData = $discount_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php echo $value['product_name']."<br>"; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>     
                                                    <?php endif; ?>
                                                </td>
                                                <td><?php echo e($discount['status'] == 1 ? 'Active' : 'Inactive'); ?></td>
                                                <td>
                                                    <?php if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3): ?>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="<?php echo e(route('discounts_edit', $discount['id'])); ?>">Edit</a>
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var type  = $('#type').val();

            if (type == 1)
            {
                $(".memberShip").show();
                $(".coupon").hide();
            }
            if (type == 0)
            {
                $(".memberShip").hide();
                $(".coupon").show();
            }
        });
    </script>

    <script type="text/javascript">
        function typeSelect()
        {   
            var type  = $('#type').val();

            if (type == 1)
            {
                $(".memberShip").show();
                $(".coupon").hide();
            }
            if (type == 0)
            {
                $(".memberShip").hide();
                $(".coupon").show();
            }
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/tuhin/5/Modules/Discounts/Resources/views/edit.blade.php ENDPATH**/ ?>