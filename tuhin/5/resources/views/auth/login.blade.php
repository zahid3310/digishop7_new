<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login | Cyberdyne Technology Ltd.</title>
    <meta name="keywords" content="pos software,inventory software,showroom software,Best software company in bangladesh">
    <meta name="Developed By" content="Cyberdyne Technology Ltd." />
    <meta name="Developer" content="Cyberdyne Technology Team | 01715317133" />
    <meta name="description" content="This is one of the largest online platform for Point Of Sales Software in Bangladesh." />
    <meta property="og:url" content="https://cyberdynetechnologyltd.com" />
    <meta property="og:title" content="Cyberdyne | Point Of Sales Software" />
    <meta property="og:description" content="This is one of the largest online platform for Point Of Sales Software in Bangladesh." />
    <meta property="og:site_name" content="Cyberdyne | Point Of Sales Software" />
    <link rel="canonical" href="https://Cyberdynetechnologyltd.com">
    <link rel="icon" type="image/png" href="{{ url('/public/favicon.png') }}" />
    <link rel="stylesheet" href="{{ url('public/login_form_assets/css/vendor/flaticon.css') }}">
    <link rel="stylesheet" href="{{ url('public/login_form_assets/css/custom/all.min.css') }}">
    <link rel="stylesheet" href="{{ url('public/login_form_assets/css/vendor/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('public/login_form_assets/css/custom/main.css') }}">
    <link rel="stylesheet" href="{{ url('public/login_form_assets/css/custom/signin-up.css') }}">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <style type="text/css">
        .bg-img {
            position: relative;
            border-bottom: none;
            -webkit-background-size: cover;
            background-size: cover;
            background-repeat: no-repeat;
            z-index: 0;
        }
    </style>
</head>

<body class="bg-img" style="background-image:url('public/bg-1.png');overflow-x: hidden;overflow-y: hidden">
  <div class="row" style="margin-top: 5%;">
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <div class="card">
    <div class="card-body">
      <center>
        <h2 id="organizationName"></h2>
      <p>Use credentials to access your account.</p>
      </center>
       <form method="POST" action="{{ route('login') }}">
                @csrf
        <div class="form-group">
          <label for="exampleInputEmail1">Phone Number</label>
          <input type="text" class="form-control"  name="email" placeholder="Phone Number" value="{{ old('email') }}">
          @error('email')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input type="password" class="form-control" name="password" placeholder="Password" required autocomplete="current-password">
          @error('password')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <button type="submit" class="btn btn-primary">Sign in now</button><br><br>
        <center> <span>Don't have an account? Please contact your super admin.</span></center>
      </form>
     
    </div>
    <div class="card-footer">
      
      <center> <span> {{ '@'.date('Y') }} Cyberdyne Technology Ltd. | +88 01748 715 715 | <a href="https://cyberdynetechnologyltd.com/" target="_blank">Cyberdyne Technology Ltd.</a></span></center>
    </div>
  </div>
    </div>
    <div class="col-md-4"></div>
  </div>
  
   <script src="{{ url('public/login_form_assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
   <script src="{{ url('public/login_form_assets/js/vendor/popper.min.js') }}"></script>
   <script src="{{ url('public/login_form_assets/js/vendor/bootstrap.min.js') }}"></script>
   <script src="{{ url('public/login_form_assets/js/custom/main.js') }}"></script>
  
  <script type="text/javascript">
       $( document ).ready(function() {

            var site_url  = $('.site_url').val();
console.log(site_url);
            $.get('https://digishop7.com/tuhin/5/login-page-info', function(data){

                $("#organizationName").html(data.organization_name);
            });
        });
   </script>

</body>

</html>