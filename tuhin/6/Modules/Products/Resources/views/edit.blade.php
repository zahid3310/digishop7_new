@extends('layouts.app')

@section('title', 'Edit Product')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Product</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Products</a></li>
                                    <li class="breadcrumb-item active">Edit Product</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('products_update', $product_entries['id']) }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-form-label">Category *</label>
                                        <select style="width: 83%" id="product_category_id" name="product_category_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10" required>
                                           <option value="{{ $product_entries['product_id'] }}" selected>{{ $product_entries['category_name'] }}</option>
                                        </select>

                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center;" class="form-control col-lg-2 col-md-2 col-sm-2 col-2" data-toggle="modal" data-target="#myModal">
                                            <i class="bx bx-plus font-size-24"></i>
                                        </span>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Product Code</label>
                                        <input type="text" name="code" class="inner form-control barCode" id="bar_code_0" value="{{ $product_entries['product_code'] }}" readonly />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Product Name *</label>
                                        <input type="text" name="product_name" class="inner form-control" id="product_name" value="{{ $product_entries['name'] }}" required />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Generic </label>
                                        <input type="text" name="generic_name" class="inner form-control" id="generic_name" value="{{ $product_entries['generic_name'] }}" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Manufacturer </label>
                                        <select style="width: 83%" id="brand_id" name="brand_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10">
                                            @if($product_entries['brand_id'] != null)
                                            <option value="{{ $product_entries['brand_id'] }}" selected>{{ $product_entries['brand_name'] }}</option>
                                            @else
                                            <option value="">--Select Manufacturer--</option>
                                            @endif
                                        </select>

                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center;" class="form-control col-lg-2 col-md-2 col-sm-2 col-2" data-toggle="modal" data-target="#myModal1">
                                            <i class="bx bx-plus font-size-24"></i>
                                        </span>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Unit</label>
                                        <select id="unit_id" style="width: 100%" class="form-control select2 unit" name="unit_id">
                                            <option value="">--Select Unit--</option>
                                            @if(!empty($units))
                                                @foreach($units as $key => $unit)
                                                <option {{ $product_entries['unit_id'] == $unit['id'] ? 'selected' : '' }} value="{{ $unit->id }}">{{ $unit->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Purchase Price *</label>
                                        <input type="text" name="buying_price" class="inner form-control" id="buying_price_0" value="{{ $product_entries['buy_price'] }}" required />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Sell Price *</label>
                                        <input type="text" name="selling_price" class="inner form-control" id="selling_price_0" value="{{ $product_entries['sell_price'] }}" required />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Wholesale Price</label>
                                        <input type="text" name="whole_sale_price" class="inner form-control" id="whole_sale_price" placeholder="Wholesale Price" value="{{ $product_entries['wholesale_price'] }}" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Discount Limit (%)</label>
                                        <input type="text" name="discount_limit" class="inner form-control" id="discount_limit" placeholder="% Discount Limit" value="{{ $product_entries['discount_limit'] }}" />
                                    </div>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-8 col-md-8 col-sm-8 col-8 col-form-label">Product Type</label>
                                        <div style="padding-right: 0px;padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12">
                                            <select style="cursor: pointer" id="product_type" class="form-control" name="product_type">
                                                <option {{ $product_entries['product_type'] == 1 ? 'selected' : '' }} value="1">Single</option>
                                                <option {{ $product_entries['product_type'] == 2 ? 'selected' : '' }} value="2">Variable</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div style="display: none" class="col-md-12 variationClass">
                                        <hr style="margin-top: 0px !important">

                                        <div data-repeater-list="inner-group" class="inner form-group mb-0 row">
                                            <div class="inner col-lg-12 ml-md-auto input_fields_wrap getMultipleRow">

                                                <h5>Add Variations</h5>
                                                @if($variation_entries->count() > 0)
                                                @foreach($variation_entries as $key => $value)
                                                    <div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_{{$key}}">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                                            <select style="width: 100%;cursor: pointer" name="variation_id[]" class="inner form-control single_select2" id="variation_id_{{$key}}" required onchange="variationValue({{$key}})">
                                                                <option>--Select Variations--</option>
                                                                @if(!empty($variations) && ($variations->count() > 0))
                                                                @foreach($variations as $key1 => $variation)
                                                                    <option {{ $value['variation_id'] == $variation['id'] ? 'selected' : '' }} value="{{ $variation['id'] }}">{{ $variation['name'] }}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>

                                                        </div>

                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                                            <select style="width: 100%;cursor: pointer" name="variation_value[]" class="inner form-control single_select2" id="variation_value_{{$key}}" required>
                                                                <option value="{{ $value['variation_value_id'] }}">{{ $value->variationValue->name }}</option>
                                                            </select>

                                                        </div>

                                                        <div class="col-lg-1 col-md-2 col-sm-6 col-6 form-group remove_field" data-val="{{$key}}">
                                                            <i class="btn btn-success btn-block bx bx-trash-alt font-size-20"></i>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                @endif

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6 col-sm-6"></div>
                                            <div class="col-lg-1 col-md-2 col-sm-6 col-12 form-group">
                                                <div id="add_field_button" class="add_field_button">
                                                    <i class="btn btn-success btn-block bx bx-plus font-size-20"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <hr style="margin-top: 0px !important">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Stock Quantity</label>
                                        <input type="number" name="stock_quantity" class="inner form-control" id="stock_quantity" value="{{ $product_entries['opening_stock'] }}" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Op. Stock Expire Date</label>
                                        <input id="expire_date" name="expire_date" type="text" value="{{ $find_lot['expire_date'] != null ? date('d-m-Y', strtotime($find_lot['expire_date'])) : '' }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Alert Quantity</label>
                                        <input type="text" name="alert_quantity" class="inner form-control" id="alert_quantity" value="{{ $product_entries['alert_quantity'] }}" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Location</label>
                                        <input type="text" name="product_location" class="inner form-control" id="product_location" value="{{ $product_entries['product_location'] }}" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-8 col-md-8 col-sm-8 col-8 col-form-label padding-left-0-xs">Status</label>
                                        <div style="padding-right: 0px;padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12">
                                            <select style="cursor: pointer" class="form-control" name="status">
                                                <option value="1" {{ $product_entries['status'] == 1 ? 'selected' : '' }}>Active</option>
                                                <option value="0" {{ $product_entries['status'] == 0 ? 'selected' : '' }}>Inactive</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div style="display: none" class="col-md-12 unitConversionClass">
                                        <hr style="margin-top: 0px !important">

                                        <div data-repeater-list="inner-group" class="inner form-group mb-0 row">
                                            <div class="inner col-lg-12 ml-md-auto input_fields_wrap_unit_conversion getMultipleRowUnitConversion">

                                                <h5>Set Unit Conversion</h5>
                                                @if($unit_conversions->count() > 0)
                                                @foreach($unit_conversions as $key1 => $value1)
                                                <div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_unit_{{$key1}}">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                                        <span class="unit_name_show" style="font-size: 24px; float: right;padding-bottom: 0px">
                                                            {{ '1 '.$value1->mainUnitId->name. ' = ' }}
                                                        </span>
                                                    </div>

                                                    <div class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">
                                                        <input class="form-control" type="text" value="{{ $value1['conversion_rate'] }}" name="conversion_rate[]">
                                                    </div>

                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                                        <select style="width: 100%;cursor: pointer" name="converted_unit_id[]" class="inner form-control single_select2" id="converted_unit_id_{{$key1}}" required>
                                                            <option>--Select Conversion Unit--</option>
                                                            @foreach($units as $key_unit => $unit_value)
                                                            <option {{ $unit_value['id'] == $value1['converted_unit_id'] ? 'selected' : '' }} value="{{ $unit_value->id }}">{{ $unit_value->name }}</option>
                                                            @endforeach
                                                        </select>

                                                    </div>

                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                                        <input class="form-control" type="text" value="{{ $value1['purchase_price'] }}" name="purchase_price[]">
                                                    </div>

                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                                        <input class="form-control" type="text" value="{{ $value1['sell_price'] }}" name="sell_price[]">
                                                    </div>

                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                                        <input class="form-control" type="text" value="{{ $value1['wholesale_price'] }}" name="wholesale_price[]">
                                                    </div>

                                                    <div class="col-lg-1 col-md-2 col-sm-6 col-6 form-group remove_field_unit_conversion" data-val="{{$key1}}">
                                                        <i class="btn btn-success btn-block bx bx-trash-alt font-size-20"></i>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @endif

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-11 col-sm-11"></div>
                                            <div class="col-lg-1 col-md-1 col-sm-6 col-12 form-group">
                                                <div id="add_field_button_unit_conversion" class="add_field_button_unit_conversion">
                                                    <i class="btn btn-success btn-block bx bx-plus font-size-20"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <hr style="margin-top: 0px !important">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('products_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Name</label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="search_name" class="form-control select2">
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Code</label>
                                            <div class="col-md-8">
                                                <input id="search_code" type="text" class="form-control" placeholder="Code">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Manufac. </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="manufacturer_id" class="form-control select2">
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Category </label>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <select style="width: 73%" id="category_id" name="category_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                       <option value="0">All</option>
                                                    </select>
                                                    <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="searchPayment()">
                                                        <i class="bx bx-search font-size-24"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <table class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Manufacturer</th>
                                            <th style="text-align: center">P/Price</th>
                                            <th style="text-align: center">S/Price</th>
                                            <th style="text-align: center">Stock</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="product_list">
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Product Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-bottom: 0px !important;padding-top: 0px !important" class="modal-body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Category Name *</label>
                        <div class="col-md-12">
                            <input id="category_name" name="category_name" type="text" class="form-control" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Manufacturer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-bottom: 0px !important;padding-top: 0px !important" class="modal-body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Manufacturer Name *</label>
                        <div class="col-md-12">
                            <input id="brand_name" name="brand_name" type="text" class="form-control" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Supplier</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submitBtn2" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton2" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal3" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Suppliers List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">

                    <div style="padding-top: 0px !important" class="modal-body">
                        <form action="{{ route('products_supplier_list_update') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                        {{ csrf_field() }}

                        <input type="hidden" name="product_id" id="product_id">

                        <div class="row">
                            <div class="col-md-3">
                                <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                    <label for="productname" class="col-md-4 col-form-label"></label>
                                    <div class="col-md-8">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                    <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Add Supplier </label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <select style="width: 75%" id="select_supplier_id" name="select_supplier_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                               <option value="0">--Select Supplier--</option>
                                            </select>
                                            <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="AddSupplier()">
                                                <i class="bx bx-plus font-size-24"></i>
                                            </span>

                                            <div class="col-md-12">
                                                <p style="color: red;display: none" class="alert">Already added</p>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                            <div class="col-md-4">
                                <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                    <label style="text-align: right" for="productname" class="col-md-4 col-form-label"></label>
                                    <div class="col-md-8">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Supplier Name</th>
                                    <th>Whole Sale Rate</th>
                                    <th>Retail Rate</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody id="supplier_list" class="getMultipleRowSuppliers input_fields_wrap">
                            </tbody>
                        </table>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="submitBtn3" class="btn btn-primary waves-effect waves-light">Save</button>
                        <button id="CloseButton3" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal4" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Customers List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">

                    <div style="padding-top: 0px !important" class="modal-body">
                        <form action="{{ route('products_customer_list_update') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                        {{ csrf_field() }}

                        <input type="hidden" name="product_id_customer" id="product_id_customer">

                        <div class="row">
                            <div class="col-md-3">
                                <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                    <label for="productname" class="col-md-4 col-form-label"></label>
                                    <div class="col-md-8">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                    <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Add Customer </label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <select style="width: 75%" id="select_customer_id" name="select_customer_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                               <option value="0">--Select Customer--</option>
                                            </select>
                                            <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="AddCustomer()">
                                                <i class="bx bx-plus font-size-24"></i>
                                            </span>

                                            <div class="col-md-12">
                                                <p style="color: red;display: none" class="alert">Already added</p>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                            <div class="col-md-4">
                                <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                    <label style="text-align: right" for="productname" class="col-md-4 col-form-label"></label>
                                    <div class="col-md-8">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Customer Name</th>
                                    <th>Whole Sale Rate</th>
                                    <th>Retail Rate</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody id="customer_list" class="getMultipleRowCustomers input_fields_wrap_customer">
                            </tbody>
                        </table>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="submitBtn4" class="btn btn-primary waves-effect waves-light">Save</button>
                        <button id="CloseButton4" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#product_category_id").select2({
                ajax: { 
                    url:  site_url + '/products/product-category-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#search_name").select2({
                ajax: { 
                    url:  site_url + '/products/product-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                        
                    },
            });

            $("#select_supplier_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 1)
                    {
                        return result['text'];
                    }
                },
            });

            $("#brand_id").select2({
                ajax: { 
                    url:  site_url + '/products/brand-brand-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#category_id").select2({
                ajax: { 
                    url:  site_url + '/products/product-category-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#manufacturer_id").select2({
                ajax: { 
                    url:  site_url + '/products/brand-brand-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#supplier_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0)
                    {
                        var type = 'Customer';
                    }

                    if (result['contact_type'] == 1)
                    {
                        var type = 'Supplier';
                    }

                    if (result['contact_type'] == 1)
                    {
                        return result['text'] + ' | ' + type;
                    }
                },
            });

            $("#select_customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0 && result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });

            var tax_type  = $("#tax_type").val();

            if (tax_type == 2)
            {
                $("#serviceCharge").hide();
                $("#vatPercentage").hide();
            }
            else
            {
                $("#serviceCharge").show();
                $("#vatPercentage").show();
            }

            var productType = $('#product_type').val();

            if (productType == 2)
            {
                $(".variationClass").show();
            }

            if (productType == 1)
            {
                $(".variationClass").hide();
            }

            var unitId   = $("#unit_id").val();

            if (unitId != '')
            {
                $('.unitConversionClass').show();
            }
            else
            {
                $('.unitConversionClass').hide();
            }

            $.get(site_url + '/products/product-list-load-ajax', function(data){

                productList(data);
                
            });
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var product_name  = $("#category_name").val();
            var site_url      = $('.site_url').val();

            if (product_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }

            $.ajax({
    
                type:   'get',
                url:    site_url + '/products/add-new-category/',
                data:   {  product_name : product_name, _token: '{{csrf_token()}}' },
    
                success: function (data) {
                    if(data != 0)
                    {
                        $('#CloseButton').click();
                    }
                    
                    $("#product_category_id").empty();
                    $('#product_category_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    $('#category_name').val('');
                }
    
            });
        });

        $('#submitBtn1').click(function() {
            
            var brand_name      = $("#brand_name").val();
            var site_url        = $('.site_url').val();

            if (brand_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }

            $.ajax({
                type:   'get',
                url:    site_url + '/products/add-new-brand/',
                data:   {  brand_name : brand_name, _token: '{{csrf_token()}}' },
    
                success: function (data) {
                    if(data != 0)
                    {
                        $('#CloseButton1').click();
                    }
                    
                    $("#brand_id").empty();
                    $('#brand_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    $('#brand_name').val('');
                }
            });
        });

        $('#submitBtn2').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = 1;
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton2').click();
                        }
                        
                        $("#supplier_id").empty();
                        $('#supplier_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });    
    </script>

    <script type="text/javascript">
        function sellingPriceTaxType()
        {
            var tax_type  = $("#tax_type").val();

            if (tax_type == 2)
            {
                $("#serviceCharge").hide();
                $("#vatPercentage").hide();
                $("#service_charge_0").val('');
                $("#vat_percentage_0").val('');

                CalculateTotalSellPrice();
            }
            else
            {
                $("#serviceCharge").show();
                $("#vatPercentage").show();
                $("#service_charge_0").val('');
                $("#vat_percentage_0").val('');

                CalculateTotalSellPrice();
            }
        }

        function CalculateTotalSellPrice()
        {
            var sell_price_call                 = $("#selling_price_0").val();
            var vat_percentage_call             = $("#vat_percentage_0").val();
            var service_charge_call             = $("#service_charge_0").val();

            if (sell_price_call == '')
            {
                var sell_price                  = 0;
            }
            else
            {
                var sell_price                  = $("#selling_price_0").val();
            }

            if (vat_percentage_call == '')
            {
                var vat_percentage          = 0;
            }
            else
            {
                var vat_percentage          = $("#vat_percentage_0").val();
            }

            if (service_charge_call == '')
            {
                var service_charge          = 0;
            }
            else
            {
                var service_charge          = $("#service_charge_0").val();
            }

            var vat_amount                  = (parseFloat(sell_price)*parseFloat(vat_percentage))/100;
            var service_charge_amount       = (parseFloat(sell_price)*parseFloat(service_charge))/100;
            var selling_price_exclusive_tax = parseFloat(sell_price) - parseFloat(vat_amount) - parseFloat(service_charge_amount);

            $("#selling_price_exclusive_tax_0").val(selling_price_exclusive_tax);
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x       = {{$varia_entry_count}};
        $(add_button).click(function(e)
        {   
            e.preventDefault();

            if(x < max_fields)
            {   
                x++;

                $('.getMultipleRow').append(' ' + '<div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_'+x+'">' +
                                                    '<div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">\n' +
                                                        '<select style="width: 100%" name="variation_id[]" class="inner form-control single_select2" id="variation_id_'+x+'" required onchange="variationValue('+x+')">\n' +
                                                            '<option>--Select Variations--</option>\n' +
                                                            '@if(!empty($variations) && ($variations->count() > 0))' +
                                                            '@foreach($variations as $key => $variation)' +
                                                                '<option value="{{ $variation['id'] }}">{{ $variation['name'] }}</option>\n' +
                                                            '@endforeach' +
                                                            '@endif' +
                                                        '</select>\n' +

                                                    '</div>\n' +

                                                    '<div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">\n' +
                                                        '<select style="width: 100%" name="variation_value[]" class="inner form-control single_select2" id="variation_value_'+x+'" required>\n' +
                                                            '<option>--Select Variation Value--</option>\n' +
                                                        '</select>\n' +

                                                    '</div>\n' +

                                                    '<div class="col-lg-1 col-md-2 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                        '<i class="btn btn-success btn-block bx bx-trash-alt font-size-20"></i>\n' + 
                                                    '</div>\n' + 
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;
        });
    </script>

    <script type="text/javascript">
        var max_fields_unit_conversion = 50; //maximum input boxes allowed
        var wrapper_unit_conversion    = $(".input_fields_wrap_unit_conversion"); //Fields wrapper
        var add_button_unit_conversion = $(".add_field_button_unit_conversion");  //Add button ID
        var index_no_unit_conversion   = 1;

        //For apending another rows start
        var y       = {{$conversion_count}};
        $(add_button_unit_conversion).click(function(e)
        {
            e.preventDefault();

            if(y < max_fields_unit_conversion)
            {   
                y++;

                $('.getMultipleRowUnitConversion').append(' ' + '<div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_unit_'+y+'">' +
                                                    '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<span class="unit_name_show" style="font-size: 24px; float: right;padding-bottom: 0px">' + '</span>' +
                                                    '</div>\n' +

                                                    '<div class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        '<input class="form-control" type="text" name="conversion_rate[]">' +

                                                    '</div>\n' +

                                                    '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<select style="width: 100%;cursor: pointer" name="converted_unit_id[]" class="inner form-control single_select2" id="converted_unit_id_'+y+'" required>\n' +
                                                            '<option>--Select Conversion Unit--</option>\n' +
                                                            '@foreach($units as $key_unit => $unit_value)' +
                                                            '<option value="{{ $unit_value->id }}">{{ $unit_value->name }}</option>' +
                                                            '@endforeach' +
                                                        '</select>\n' +

                                                    '</div>\n' +

                                                    '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<input id="purchase_price" class="form-control" type="text" name="purchase_price[]" placeholder="Purchase Price">' +

                                                    '</div>\n' +

                                                    '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<input id="sell_price" class="form-control" type="text" name="sell_price[]" placeholder="Retail Price">' +

                                                    '</div>\n' +

                                                    '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<input id="wholesale_price" class="form-control" type="text" name="wholesale_price[]" placeholder="Wholesale Price">' +

                                                    '</div>\n' +

                                                    '<div class="col-lg-1 col-md-2 col-sm-6 col-6 form-group remove_field_unit_conversion" data-val="'+y+'">\n' + 
                                                        '<i class="btn btn-success btn-block bx bx-trash-alt font-size-20"></i>\n' + 
                                                    '</div>\n' + 
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

                                            var unitName = $("#unit_id option:selected").text();
                                            $('.unit_name_show').html('1 ' + unitName + ' = ');
            }
        });
        //For apending another rows end

        $(wrapper_unit_conversion).on("click",".remove_field_unit_conversion", function(e)
        {
            e.preventDefault();

            var y = $(this).attr("data-val");

            $('.di_unit_'+y).remove(); y--;
        });
    </script>

    <script type="text/javascript">
        function variationValue(x)
        {
            var site_url        = $(".site_url").val();
            var variationId     = $("#variation_id_"+x).val();

            $.get(site_url + '/products/get-variation-value-list/'+ variationId, function(data){
                
                var list5 = '';
                var list7 = '';

                $.each(data, function(i, data_list)
                {   
                    list5 += '<option value = "' +  data_list.id + '">' + data_list.name + '</option>';
                });

                list7 += '<option value = "">' + '--Select Variation Value--' +'</option>';

                $("#variation_value_"+x).empty();
                $("#variation_value_"+x).append(list7);
                $("#variation_value_"+x).append(list5);

            });
        }

        $(document).on('change', '#product_type', function() {
            
            var productType = $('#product_type').val();

            if (productType == 2)
            {
                $(".variationClass").show();
            }

            if (productType == 1)
            {
                $(".variationClass").hide();
            }
        });

        function searchPayment()
        {
            var site_url        = $('.site_url').val();
            var search_name     = $('#search_name').val();
            var search_code     = $('#search_code').val();
            var manufacturer_id = $('#manufacturer_id').val();
            var category_id     = $('#category_id').val();

            if (search_name != '')
            {
                var product_name = $('#search_name').val();
            }
            else
            {
                var product_name = 0;
            }

            if (search_code != '')
            {
                var product_code = $('#search_code').val();
            }
            else
            {
                var product_code = 0;
            }

            if (manufacturer_id != '')
            {
                var manufturer = $('#manufacturer_id').val();
            }
            else
            {
                var manufturer = 0;
            }

            if (category_id != '')
            {
                var category = $('#category_id').val();
            }
            else
            {
                var category = 0;
            }

            $.get(site_url + '/products/product-list-search-ajax/' + product_name + '/' + product_code + '/' + manufturer + '/' +category, function(data){

                productList(data);

            });
        }
    </script>

    <script type="text/javascript">
        function findSuppliers(product_id)
        {
            var site_url = $(".site_url").val();

            $("#product_id").val(product_id);

            $.get(site_url + '/products/get-supplier-list/'+ product_id, function(data){

                var supplier_list  = '';
                var serial         = 1;
                $.each(data, function(i, supplier_data)
                {
                    if (serial == 1)
                    {
                        var sl_label                    = '<label class="hidden-xs" for="productname">SL</label>\n';
                        var supplier_label              = '<label class="hidden-xs" for="productname">Supplier Name</label>\n';
                        var whole_sale_rate_label       = '<label class="hidden-xs" for="productname">Whole Sale Rate</label>\n';
                        var retail_sale_rate_label      = '<label class="hidden-xs" for="productname">Retail Rate</label>\n';
                        var action_label                = '<label class="hidden-xs" for="productname">Action</label>\n';
                    }
                    else
                    {
                        var sl_label                    = '';
                        var supplier_label              = '';
                        var whole_sale_rate_label       = '';
                        var retail_sale_rate_label      = '';
                        var action_label                = '';
                    }

                    supplier_list += ' ' + '<tr class="di_'+serial+'">' +
                                                '<td>' + serial + '</td>' +

                                                '<input type="hidden" name="sup_id[]" value="'+ supplier_data.supplier_id +'" class="suppliseID">' +
                                                '<input type="hidden" id="serial" class="serial" value="'+ serial +'">' +
                                                
                                                '<td>' + 
                                                    supplier_data.supplier_name + 
                                                '</td>' +

                                                '<td>' +
                                                    '<input type="text" class="form-control" name="whole_rate[]" value="'+ supplier_data.whole_sale_price +'">' + 
                                                '</td>' +

                                                '<td>' + 
                                                    '<input type="text" class="form-control" name="retail_rate[]" value="'+ supplier_data.retail_price +'">' + 
                                                '</td>' +

                                                '<td>' + 
                                                    '<i id="remove_'+serial+'" style="padding: 0.48rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field" data-val="'+serial+'"></i>' +
                                                '</td>' +
                                            '</tr>';

                                            serial++;                             
                });

                $("#supplier_list").empty();
                $("#supplier_list").append(supplier_list);
            });
        }

        function AddSupplier()
        {
            $('.alert').hide();  

            var supplierId      = $('#select_supplier_id').val();
            var supplierName    = $("#select_supplier_id option:selected").text();
            var serials         = parseInt($(".serial:last").val());

            if (isNaN(serials))
            {   
                var serial      = 0;
            }
            else
            {
                var serial      = parseInt($(".serial:last").val());
            }

            var checkSupplier   = [];
            $('.suppliseID').each(function()
            {
                var supplier_id    = $(this).val();

                if(checkValue(supplier_id, checkSupplier) == 'Not exist')
                {
                    checkSupplier.push(supplier_id);
                }
                
            });

            if((checkValue(supplierId, checkSupplier) == 'Not exist') && (supplierId != 0))
            {
                serial++;
                $('.getMultipleRowSuppliers').append(' ' + '<tr class="di_'+serial+'">' +
                                                    '<td>' + serial + '</td>' +

                                                    '<input type="hidden" name="sup_id[]" value="'+ supplierId +'" class="suppliseID">' +
                                                    '<input type="hidden" id="serial" class="serial" value="'+ serial +'">' +
                                                    
                                                    '<td>' + 
                                                        supplierName + 
                                                    '</td>' +

                                                    '<td>' +
                                                        '<input type="text" class="form-control" name="whole_rate[]" value="0">' + 
                                                    '</td>' +

                                                    '<td>' + 
                                                        '<input type="text" class="form-control" name="retail_rate[]" value="0">' + 
                                                    '</td>' +

                                                    '<td>' + 
                                                        '<i id="remove_'+serial+'" style="padding: 0.48rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field" data-val="'+serial+'"></i>' +
                                                    '</td>' +
                                                '</tr>' 
                                                );
            }
            else
            {
              $('.alert').show();  
            }
        }

        function checkValue(value,arr)
        {
            var status = 'Not exist';
             
            for(var i=0; i<arr.length; i++)
            {
                var name = arr[i];
                if(name == value)
                {
                    status = 'Exist';
                    break;
                }
            }

            return status;
        }

        function RemoveSupplier()
        {
            var wrapper          = $(".input_fields_wrap"); 

            $(wrapper).on("click",".remove_field", function(e)
            {
                e.preventDefault();

                var serial = $(this).attr("data-val");

                $('.di_'+serial).remove(); serial--;
            });
        }
    </script>

    <script type="text/javascript">
        function findCustomers(product_id)
        {
            var site_url = $(".site_url").val();

            $("#product_id_customer").val(product_id);

            $.get(site_url + '/products/get-customer-list/'+ product_id, function(data){

                var customer_list  = '';
                var serials         = 1;
                $.each(data, function(i, customer_data)
                {
                    if (serials == 1)
                    {
                        var sl_label                    = '<label class="hidden-xs" for="productname">SL</label>\n';
                        var supplier_label              = '<label class="hidden-xs" for="productname">Customer Name</label>\n';
                        var whole_sale_rate_label       = '<label class="hidden-xs" for="productname">Whole Sale Rate</label>\n';
                        var retail_sale_rate_label      = '<label class="hidden-xs" for="productname">Retail Rate</label>\n';
                        var action_label                = '<label class="hidden-xs" for="productname">Action</label>\n';
                    }
                    else
                    {
                        var sl_label                    = '';
                        var supplier_label              = '';
                        var whole_sale_rate_label       = '';
                        var retail_sale_rate_label      = '';
                        var action_label                = '';
                    }

                    customer_list += ' ' + '<tr class="di_customer_'+serials+'">' +
                                                '<td>' + serials + '</td>' +

                                                '<input type="hidden" name="cus_id[]" value="'+ customer_data.customer_id +'" class="customerID">' +
                                                '<input type="hidden" id="serial_customer" class="serialCustomer" value="'+ serials +'">' +
                                                
                                                '<td>' + 
                                                    customer_data.supplier_name + 
                                                '</td>' +

                                                '<td>' +
                                                    '<input type="text" class="form-control" name="whole_rate[]" value="'+ customer_data.whole_sale_price +'">' + 
                                                '</td>' +

                                                '<td>' + 
                                                    '<input type="text" class="form-control" name="retail_rate[]" value="'+ customer_data.retail_price +'">' + 
                                                '</td>' +

                                                '<td>' + 
                                                    '<i id="customer_remove_'+serials+'" style="padding: 0.48rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field_customer" data-val="'+serials+'" onclick="RemoveCustomer()"></i>' +
                                                '</td>' +
                                            '</tr>';

                                            serials++;                             
                });

                $("#customer_list").empty();
                $("#customer_list").append(customer_list);
            });
        }

        function AddCustomer()
        {
            $('.alert').hide();  

            var customerId      = $('#select_customer_id').val();
            var customerName    = $("#select_customer_id option:selected").text();
            var serialss        = parseInt($(".serialCustomer:last").val());

            if (isNaN(serialss))
            {   
                var serials      = 0;
            }
            else
            {
                var serials      = parseInt($(".serialCustomer:last").val());
            }

            var checkCustomer   = [];
            $('.customerID').each(function()
            {
                var customer_id    = $(this).val();

                if(checkValue(customer_id, checkCustomer) == 'Not exist')
                {
                    checkCustomer.push(customer_id);
                }
                
            });

            if((checkValue(customerId, checkCustomer) == 'Not exist') && (customerId != 0))
            {
                serials++;
                $('.getMultipleRowCustomers').append(' ' + '<tr class="di_customer_'+serials+'">' +
                                                    '<td>' + serials + '</td>' +

                                                    '<input type="hidden" name="cus_id[]" value="'+ customerId +'" class="customerID">' +
                                                    '<input type="hidden" id="serial_customer" class="serialCustomer" value="'+ serials +'">' +
                                                    
                                                    '<td>' + 
                                                        customerName + 
                                                    '</td>' +

                                                    '<td>' +
                                                        '<input type="text" class="form-control" name="whole_rate[]" value="0">' + 
                                                    '</td>' +

                                                    '<td>' + 
                                                        '<input type="text" class="form-control" name="retail_rate[]" value="0">' + 
                                                    '</td>' +

                                                    '<td>' + 
                                                        '<i id="customer_remove_'+serials+'" style="padding: 0.48rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field_customer" data-val="'+serials+'" onclick="RemoveCustomer()"></i>' +
                                                    '</td>' +
                                                '</tr>' 
                                                );
            }
            else
            {
              $('.alert').show();  
            }
        }

        function RemoveCustomer()
        {
            var wrapper          = $(".input_fields_wrap_customer"); 

            $(wrapper).on("click",".remove_field_customer", function(e)
            {
                e.preventDefault();

                var serials = $(this).attr("data-val");

                $('.di_customer_'+serials).remove(); serials--;
            });
        }
    </script>

    <script type="text/javascript">
        function productList(data)
        {
            var product_list = '';
            var sl_no        = 1;
            $.each(data, function(i, product_data)
            {
                var serial              = parseFloat(i) + 1;
                var site_url            = $('.site_url').val();
                var edit_url            = site_url + '/products/edit/' + product_data.id;
                var supplier_url        = site_url + '/products/get-supplier-list/' + product_data.id;
                var customer_url        = site_url + '/products/get-supplier-list/' + product_data.id;

                if (product_data.status == 1)
                {
                    var stat = 'Active';
                }
                else
                {
                    var stat = 'Inactive';
                }

                if (product_data.stock_in_hand == null)
                {
                    var stock = 0;
                }
                else
                {
                    var stock = product_data.stock_in_hand;
                }

                if (product_data.brand_name == null)
                {
                    var brand = '';
                }
                else
                {
                    var brand = product_data.brand_name;
                }

                product_list += '<tr>' +
                                    '<td>' +
                                        sl_no +
                                    '</td>' +
                                    '<td>' +
                                        pad(product_data.product_code, 6) +
                                    '</td>' +
                                    '<td>' +
                                        product_data.name +
                                    '</td>' +
                                    '<td>' +
                                        product_data.category_name +
                                    '</td>' +
                                    '<td>' +
                                        brand +
                                    '</td>' +
                                    '<td>' +
                                        product_data.buy_price +
                                    '</td>' +
                                    '<td>' +
                                        product_data.sell_price +
                                    '</td>' +
                                    '<td>' +
                                        stock +
                                    '</td>' +
                                    '<td>' +
                                        stat +
                                    '</td>' +
                                    '<td>' +
                                        '<div class="dropdown">' +
                                            '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                            '</a>' +
                                            '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                '<a class="dropdown-item" href="'+ edit_url +'">' + 'Edit' + '</a>' +
                                                '<a style="cursor: pointer" class="dropdown-item" data-toggle="modal" data-target="#myModal3" onclick="findSuppliers('+ product_data.id +')">' + 'Add Suppliers' + '</a>' +
                                                '<a style="cursor: pointer" class="dropdown-item" data-toggle="modal" data-target="#myModal4" onclick="findCustomers('+ product_data.id +')">' + 'Add Customers' + '</a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</td>' +
                                '</tr>';

                                sl_no++;
            });

            $("#product_list").empty();
            $("#product_list").append(product_list);
        }

        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }

        $(document).on("change", "#unit_id" , function() {

            var site_url = $('.site_url').val();
            var unitId   = $("#unit_id").val();
            var unitName = $("#unit_id option:selected").text();

            if (unitId != '')
            {   
                $('.remove_field_unit_conversion').click();
                $('#add_field_button_unit_conversion').click();
                $('.unitConversionClass').show();
                $('.unit_name_show').html('1 ' + unitName + ' = ');
            }
            else
            {
                $('.remove_field_unit_conversion').click();
                $('.unitConversionClass').hide();
            }
        });
    </script>
@endsection