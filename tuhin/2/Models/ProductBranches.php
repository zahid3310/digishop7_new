<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ProductBranches extends Model
{  
    protected $table = "product_branches";

    public function branch()
    {
        return $this->belongsTo('App\Models\Branches','branch_id');
    }
}
