

<?php $__env->startSection('title', 'Generate Bill'); ?>

<style type="text/css">
    .table td, .table th {
        padding: 0px !important;
    }

    .button-items .btn {
        margin-bottom: 5px;
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="overlay"></div>

    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Billing</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Billing</a></li>
                                    <li class="breadcrumb-item active">Generate Bill</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div style="padding: 0.5rem !important" class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Select Month *</label>
                                        <select id="customer_id" class="form-control select2" name="customer_id">
                                            <option value="">All Customer</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Select Month *</label>
                                        <select style="cursor: pointer" id="month" class="form-control select2" name="month">
                                            <option value="Jan" <?php echo e(date('m') == 'Jan' ? 'selected' : ''); ?>>January</option>
                                            <option value="Feb" <?php echo e(date('m') == 'Feb' ? 'selected' : ''); ?>>February</option>
                                            <option value="Mar" <?php echo e(date('m') == 'Mar' ? 'selected' : ''); ?>>March</option>
                                            <option value="Apr" <?php echo e(date('m') == 'Apr' ? 'selected' : ''); ?>>Appril</option>
                                            <option value="May" <?php echo e(date('m') == 'May' ? 'selected' : ''); ?>>May</option>
                                            <option value="Jun" <?php echo e(date('m') == 'Jun' ? 'selected' : ''); ?>>Jun</option>
                                            <option value="Jul" <?php echo e(date('m') == 'Jul' ? 'selected' : ''); ?>>July</option>
                                            <option value="Aug" <?php echo e(date('m') == 'Aug' ? 'selected' : ''); ?>>August</option>
                                            <option value="Sep" <?php echo e(date('m') == 'Sep' ? 'selected' : ''); ?>>September</option>
                                            <option value="Oct" <?php echo e(date('m') == 'Oct' ? 'selected' : ''); ?>>October</option>
                                            <option value="Nov" <?php echo e(date('m') == 'Nov' ? 'selected' : ''); ?>>November</option>
                                            <option value="Dec" <?php echo e(date('m') == 'Dec' ? 'selected' : ''); ?>>December</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Select Year *</label>
                                        <select style="cursor: pointer" id="year" class="form-control select2" name="year">
                                            <option value="2021" <?php echo e(date('Y') == '2021' ? 'selected' : ''); ?>>2021</option>
                                            <option value="2022" <?php echo e(date('Y') == '2022' ? 'selected' : ''); ?>>2022</option>
                                            <option value="2023" <?php echo e(date('Y') == '2023' ? 'selected' : ''); ?>>2023</option>
                                            <option value="2024" <?php echo e(date('Y') == '2024' ? 'selected' : ''); ?>>2024</option>
                                            <option value="2025" <?php echo e(date('Y') == '2025' ? 'selected' : ''); ?>>2025</option>
                                            <option value="2026" <?php echo e(date('Y') == '2026' ? 'selected' : ''); ?>>2026</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        <div style="padding-left: 0px" class="form-check form-check-right">
                                            <button style="border-radius: 0px;margin-top: 36px;" type="button" class="btn btn-primary waves-effect waves-light col-md-12" onclick="searchItems()">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <form id="FormSubmit" action="<?php echo e(route('generate_bill_store')); ?>" method="post" files="true" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>


                <div style="display: none;" class="row listClass">
                    <div style="padding-left: 15px" class="form-check form-check-right">
                        <button style="border-radius: 0px;padding: 0.3rem .5rem !important" type="submit" class="btn btn-primary waves-effect waves-light col-md-12">Generate Bill</button>
                    </div>

                    <div style="padding-top: 10px" class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <table id="contactList" class="table table-striped"></table>
                            </div>
                        </div>
                    </div>
                </div>
                
                </form>
            </div>
        </div>
    </div>

    <!-- <div class="loader"></div> -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/accountsreport/select2-api/get-registers-customers-api/',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });

        function searchItems()
        {   
            $("body").addClass("loading");

            $(".listClass").show();

            var site_url    = $(".site_url").val();
            var customerId  = $("#customer_id").val();

            $.get(site_url + '/generate-bill/all-get-data?customer_id=' + customerId, function(data) {

                var list    = '';
                    list    += '<tr>' +
                                '<th id="select-all" style="width: 4%" class="active">' +
                                    '<input id="selectAll" type="checkbox" class="select-all" onclick="checkVal()" />' +
                                '</th>' +
                                '<th style="width: 6%" class="success">' + 'SL' + '</th>' +
                                '<th style="width: 15%" class="success">' + 'Customer' + '</th>' +
                                '<th style="width: 15%" class="success">' + 'Address' + '</th>' +

                                '<th style="width: 15%;text-align: left" class="success">' + 'Phone' + '</th>' + 
                                '<th style="width: 15%" class="success">' + 'Bill Amount' + '</th>' +  
                            '</tr>';

                var month   = $('#month').val();
                var year    = $('#year').val();
                var serial  = 1;
                $.each(data, function(i, data_list)
                {   
                    if (data_list.ClientAddress != null) 
                    {
                        var ClientAddress = data_list.ClientAddress;
                    }
                    else
                    {
                        var ClientAddress = '';
                    }

                    if (data_list.ClientContactNumber != null) 
                    {
                        var ClientContactNumber = data_list.ClientContactNumber;
                    }
                    else
                    {
                        var ClientContactNumber = '';
                    }

                    list += '<tr>' +
                                '<td class="active">' +
                                    '<input id="checkbox_'+i+'" type="checkbox" class="select-item checkbox" onclick="checkValUnik('+i+')" />' +
                                    '<input id="check_status_'+i+'" type="hidden" name="check_box_id[]" class="checkboxId" value="0" />' +
                                    '<input type="hidden" name="customer_id[]" value="'+ data_list.id +'" />' +
                                    '<input type="hidden" name="month[]" value="'+ month +'" />' +
                                    '<input type="hidden" name="year[]" value="'+ year +'" />' +
                                    '<input type="hidden" name="amount[]" value="'+ data_list.BillAmount +'" />' +
                                '</td>' +
                                '<td class="success">' + serial + '</td>' +
                                '<td class="success">' + data_list.ClientName + '</td>' +
                                '<td class="success">' + ClientAddress + '</td>' +
                                '<td style="text-align: left" class="success">' + ClientContactNumber + '</td>' +
                                '<td class="success">' + '<input value="'+ data_list.BillAmount +'" class="form-controll" />' + '</td>' +
                            '</tr>';
                    serial++;
                });

                $("#contactList").empty();
                $("#contactList").append(list);

                $("body").removeClass("loading");
            });
        }
    </script>

    <script type="text/javascript">
        function checkVal()
        {
            if ($('#selectAll').is(":checked"))
            {
                $('.checkbox').prop('checked', true);
                $('.checkboxId').val(1);
            }
            else
            {
                $('.checkbox').prop('checked', false);
                $('.checkboxId').val(0);
            }
        }

        function checkValUnik(i)
        {
            if ($('#checkbox_'+i).is(":checked") == true)
            {
                $('#check_status_'+i).val(1);
            }
            else
            {
                $('#check_status_'+i).val(0);
            }
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/water-billing/Modules/Payments/Resources/views/generate_bill/create.blade.php ENDPATH**/ ?>