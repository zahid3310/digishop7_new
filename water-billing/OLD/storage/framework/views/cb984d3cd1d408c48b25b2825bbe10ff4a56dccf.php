

<?php $__env->startSection('title', 'Summary Report'); ?>

<?php $__env->startSection('styles'); ?>
    <style type="text/css">
        @media  print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page  {
                margin: 0cm ! important;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Summary Report</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Billing</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Summary Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="margin-left: 150px;margin-right: 150px" class="row d-print-none">
                    <div class="col-12">
                        <div class="card">
                            <div style="padding-bottom: 4px" class="card-body table-responsive">
                                <form method="get" action="<?php echo e(route('billing_summary_report_index_print')); ?>" enctype="multipart/form-data" target="_blank">
	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"> Date </label>
	                                    <div class="col-md-9">
	                                    	<div class="row">
	                                    		<div class="input-group-append col-md-5">
		                                    		<select style="cursor: pointer" id="from_month" class="form-control select2" name="from_month">
                                                        <option value="Jan" <?php echo e(date('m') == 'Jan' ? 'selected' : ''); ?>>January</option>
                                                        <option value="Feb" <?php echo e(date('m') == 'Feb' ? 'selected' : ''); ?>>February</option>
                                                        <option value="Mar" <?php echo e(date('m') == 'Mar' ? 'selected' : ''); ?>>March</option>
                                                        <option value="Apr" <?php echo e(date('m') == 'Apr' ? 'selected' : ''); ?>>Appril</option>
                                                        <option value="May" <?php echo e(date('m') == 'May' ? 'selected' : ''); ?>>May</option>
                                                        <option value="Jun" <?php echo e(date('m') == 'Jun' ? 'selected' : ''); ?>>Jun</option>
                                                        <option value="Jul" <?php echo e(date('m') == 'Jul' ? 'selected' : ''); ?>>July</option>
                                                        <option value="Aug" <?php echo e(date('m') == 'Aug' ? 'selected' : ''); ?>>August</option>
                                                        <option value="Sep" <?php echo e(date('m') == 'Sep' ? 'selected' : ''); ?>>September</option>
                                                        <option value="Oct" <?php echo e(date('m') == 'Oct' ? 'selected' : ''); ?>>October</option>
                                                        <option value="Nov" <?php echo e(date('m') == 'Nov' ? 'selected' : ''); ?>>November</option>
                                                        <option value="Dec" <?php echo e(date('m') == 'Dec' ? 'selected' : ''); ?>>December</option>
                                                    </select>
		                                    	</div>

		                                    	<div class="input-group-append col-md-7">
		                                    		<select style="cursor: pointer" id="from_year" class="form-control select2" name="from_year">
                                                        <option value="2021" <?php echo e(date('Y') == '2021' ? 'selected' : ''); ?>>2021</option>
                                                        <option value="2022" <?php echo e(date('Y') == '2022' ? 'selected' : ''); ?>>2022</option>
                                                        <option value="2023" <?php echo e(date('Y') == '2023' ? 'selected' : ''); ?>>2023</option>
                                                        <option value="2024" <?php echo e(date('Y') == '2024' ? 'selected' : ''); ?>>2024</option>
                                                        <option value="2025" <?php echo e(date('Y') == '2025' ? 'selected' : ''); ?>>2025</option>
                                                        <option value="2026" <?php echo e(date('Y') == '2026' ? 'selected' : ''); ?>>2026</option>
                                                    </select>
		                                    	</div>
	                                    	</div>
	                                    </div>
	                                </div>

                                    <!-- <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label"> To Date </label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="input-group-append col-md-5">
                                                    <select style="cursor: pointer" id="to_month" class="form-control select2" name="to_month">
                                                        <option value="Jan" <?php echo e(date('m') == 'Jan' ? 'selected' : ''); ?>>January</option>
                                                        <option value="Feb" <?php echo e(date('m') == 'Feb' ? 'selected' : ''); ?>>February</option>
                                                        <option value="Mar" <?php echo e(date('m') == 'Mar' ? 'selected' : ''); ?>>March</option>
                                                        <option value="Apr" <?php echo e(date('m') == 'Apr' ? 'selected' : ''); ?>>Appril</option>
                                                        <option value="May" <?php echo e(date('m') == 'May' ? 'selected' : ''); ?>>May</option>
                                                        <option value="Jun" <?php echo e(date('m') == 'Jun' ? 'selected' : ''); ?>>Jun</option>
                                                        <option value="Jul" <?php echo e(date('m') == 'Jul' ? 'selected' : ''); ?>>July</option>
                                                        <option value="Aug" <?php echo e(date('m') == 'Aug' ? 'selected' : ''); ?>>August</option>
                                                        <option value="Sep" <?php echo e(date('m') == 'Sep' ? 'selected' : ''); ?>>September</option>
                                                        <option value="Oct" <?php echo e(date('m') == 'Oct' ? 'selected' : ''); ?>>October</option>
                                                        <option value="Nov" <?php echo e(date('m') == 'Nov' ? 'selected' : ''); ?>>November</option>
                                                        <option value="Dec" <?php echo e(date('m') == 'Dec' ? 'selected' : ''); ?>>December</option>
                                                    </select>
                                                </div>

                                                <div class="input-group-append col-md-7">
                                                    <select style="cursor: pointer" id="to_year" class="form-control select2" name="to_year">
                                                        <option value="2021" <?php echo e(date('Y') == '2021' ? 'selected' : ''); ?>>2021</option>
                                                        <option value="2022" <?php echo e(date('Y') == '2022' ? 'selected' : ''); ?>>2022</option>
                                                        <option value="2023" <?php echo e(date('Y') == '2023' ? 'selected' : ''); ?>>2023</option>
                                                        <option value="2024" <?php echo e(date('Y') == '2024' ? 'selected' : ''); ?>>2024</option>
                                                        <option value="2025" <?php echo e(date('Y') == '2025' ? 'selected' : ''); ?>>2025</option>
                                                        <option value="2026" <?php echo e(date('Y') == '2026' ? 'selected' : ''); ?>>2026</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Name/Company Name</label>
                                        <div class="col-md-9">
                                            <select id="customer_id" class="form-control select2" name="customer_id">
                                                <option value="0">All Customer</option>
                                            </select>
                                        </div>
                                    </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"></label>
	                                    <div class="col-md-9">
	                                        <button style="border-radius: 0px;background-color: #297049;color: white" class="btn" type="submit" target="_blank">
	                                        	Print
	                                    	</button>
	                                    </div>
	                                </div>
                            	</form>   
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
        $( document ).ready(function() {
            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/accountsreport/select2-api/get-registers-customers-api/',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/water-billing/Modules/Payments/Resources/views/reports/summaryReport/index.blade.php ENDPATH**/ ?>