<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>
                
                <li>
                    <a href="{{ route('home') }}" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
    
                <li>
                    <a href="{{ route('branch_edit', 1) }}" class="waves-effect">
                        <i class="fas fa-list"></i>
                        <span>Company Information</span>
                    </a>
                </li>

                <li class="">
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>Admin</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <!-- <li> <a class="" href="{{ route('chart_of_accounts_index') }}">Chart of Accounts Head</a> </li> -->
                        <!-- <li> <a class="" href="{{ route('chart_of_projects_index') }}">Chart of Projects</a> </li> -->
                        <li> <a class="" href="{{ route('chart_of_registers_index') }}">Chart of Registers</a> </li>
                    </ul>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'payments_create' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'payments_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'payments_show' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'generate_bill_create' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'generate_bill_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'payments_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'payments_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'payments_create' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'payments_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'payments_show' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'generate_bill_create' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'generate_bill_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'payments_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'payments_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>Billing</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li>    
                            <a class="{{ Route::currentRouteName() == 'payments_create' ? 'mm-active' : '' }}" href="{{ route('payments_create') }}">
                                Bill Collection
                            </a> 
                        </li>

                        <li> 
                            <a class="{{ Route::currentRouteName() == 'generate_bill_create' ? 'mm-active' : '' }}" href="{{ route('generate_bill_create') }}">
                                Generate Bill
                            </a> 
                        </li>

                        <li> 
                            <a class="{{ Route::currentRouteName() == 'generate_bill_index' ? 'mm-active' : '' || 
                                          Route::currentRouteName() == 'generate_bill_edit' ? 'mm-active' : '' }}" href="{{ route('generate_bill_index') }}">
                                List of Generate Bill
                            </a> 
                        </li>

                        <li> 
                            <a class="{{ Route::currentRouteName() == 'payments_index' ? 'mm-active' : '' ||
                                          Route::currentRouteName() == 'payments_show' ? 'mm-active' : '' ||  
                                          Route::currentRouteName() == 'payments_edit' ? 'mm-active' : '' }}" href="{{ route('payments_index') }}">
                                List of Collection
                            </a> 
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                <span>Reports</span>
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="" href="{{ route('billing_collection_report_index') }}">Collection Report</a> </li>
                                <li> <a class="" href="{{ route('billing_summary_report_index') }}">Monthly Summary</a> </li>
                                <li> <a class="" href="{{ route('billing_due_report_index') }}">Ledger Report</a> </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li style="display: none;" class="{{ 
                Route::currentRouteName() == 'cash_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'cash_receipt_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'bank_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'contra_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'journal_voucher_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                Route::currentRouteName() == 'cash_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'cash_receipt_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'bank_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'contra_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'journal_voucher_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fab fa-adn"></i><span>Accounts</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="" href="{{ route('cash_payment_voucher_index') }}">Cash Payment Voucher</a> </li>
                        <li> <a class="" href="{{ route('cash_receipt_voucher_index') }}">Cash Receipt Voucher</a> </li>
                        <li> <a class="" href="{{ route('bank_payment_voucher_index') }}">Bank Payment Voucher</a> </li>
                        <li> <a class="" href="{{ route('bank_receipt_voucher_index') }}">Bank Receipt Voucher</a> </li>
                        <li> <a class="" href="{{ route('journal_voucher_index') }}">Journal Voucher</a> </li>
                        <li> <a class="" href="{{ route('contra_voucher_index') }}">Contra Voucher</a> </li>
                        <!-- <li> <a class="" href="#">Cheque State Change</a> </li> -->
                        <!-- <li> <a class="" href="#">Bank Reconcilation</a> </li> -->
                        <li class=""> <a class="{{ 
                Route::currentRouteName() == 'cash_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'cash_receipt_voucher_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'bank_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'contra_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'journal_voucher_edit' ? 'mm-active' : '' }}" href="{{ route('voucher_posting_index') }}">Voucher Posting</a> </li>
                        <li> <a class="" href="{{ route('list_of_voucher') }}">List of Voucher</a> </li>
                    </ul>
                </li>

                <li style="display: none;" class="">
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>Account Reports</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="" href="{{ route('accounts_report_current_balance_index') }}" target="_blank">Current Balance</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_accounts_voucher_index') }}">Accounts Voucher</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_cash_book_index') }}">Cash Book</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_bank_book_index') }}">Bank Book</a> </li>
                        <li> <a class="" href="#">Journal Book</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_ledger_book_index') }}">Ledger Book</a> </li>
                        <li> <a class="" href="#">Detail Annual Phase Cost</a> </li>
                        <li> <a class="" href="#">Daily Transaction</a> </li>
                        <li> <a class="" href="#">Transaction of Cheque</a> </li>
                        <li> <a class="" href="#">Bank Reconcilation</a> </li>
                        <li> <a class="" href="#">Project Wise Summary</a> </li>
                        <li> <a class="" href="#">Register Wise Summary</a> </li>
                        <li> <a class="" href="#">Accounts Wise Summary</a> </li>
                        <li> <a class="" href="#">Transactions of Voucher</a> </li>
                        <li> <a class="" href="#">Trial Balance</a> </li>
                        <li> <a class="" href="#">Trial Balance With Trns</a> </li>
                        <li> <a class="" href="#">Balance Sheet</a> </li>
                        <li> <a class="" href="#">Income Statement</a> </li>
                    </ul>
                </li>
                
                <li>
                    <a class="has-arrow waves-effect">
                        <i class="far fa-envelope"></i><span>Messaging</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('messages_send_index') }}">Send Message</a> </li>
                    </ul>
                </li>
                
                <li class="{{ 
                    Route::currentRouteName() == 'users_index' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'partners_areas_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'users_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'users_index' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'partners_areas_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'users_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>Basic Settings</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li>
                            <a class="has-arrow waves-effect">
                                Messaging
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('messages_index') }}">Create Text Message</a> </li>
                                <li> <a href="{{ route('messages_phone_book_index') }}">Phone Book</a> </li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow waves-effect">
                                Security System
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('users_index') }}">Add User</a> </li>
                                <li> <a href="{{ route('users_index_all') }}">List of User</a> </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>