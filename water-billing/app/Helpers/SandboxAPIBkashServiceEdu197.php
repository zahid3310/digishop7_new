<?php
/**
 * Created by PhpStorm
 * User: ProgrammerHasan
 * Date: 31-10-2020
 * Time: 9:16 PM
 */
class SandboxAPIBkashServiceEdu197
{   
    // bKash config ***secret
    public static function config(): array
    {
        return [
            // CHECKOUT (IFRAME BASED)
            'createURL'             => 'https://checkout.pay.bka.sh/v1.2.0-beta/checkout/payment/create',
            'executeURL'            => 'https://checkout.pay.bka.sh/v1.2.0-beta/checkout/payment/execute/', // must be (/{paymentID)
            'tokenURL'              => 'https://checkout.pay.bka.sh/v1.2.0-beta/checkout/token/grant',
            'refresh_tokenURL'      => 'https://checkout.pay.bka.sh/v1.2.0-beta/checkout/token/refresh',
            'query_paymentURL'      => 'https://checkout.pay.bka.sh/v1.2.0-beta/checkout/payment/query/', // must be (/{paymentID)
            'search_transactionURL' => 'https://checkout.pay.bka.sh/v1.2.0-beta/checkout/payment/search/', // must be (/{trxID})
            'refund_URL'            => 'https://checkout.pay.bka.sh/v1.2.0-beta/checkout/payment/refund', 
            // CHECKOUT (URL BASED)
            'tokenized_tokenURL'            => 'https://checkout.pay.bka.sh/v1.2.0-beta/tokenized/checkout/token/grant',
            'tokenized_refresh_tokenURL'    => 'https://checkout.pay.bka.sh/v1.2.0-beta/tokenized/checkout/token/refresh',
            'tokenized_createURL'           => 'https://checkout.pay.bka.sh/v1.2.0-beta/tokenized/checkout/create',
            'tokenized_executeURL'          => 'https://checkout.pay.bka.sh/v1.2.0-beta/tokenized/checkout/execute',
            // bKash Script
            'script'        => 'https://scripts.pay.bka.sh/versions/1.2.0-beta/checkout/bKash-checkout.js',
            // sandbox credential :: formal Sandbox
            'app_key'       => 'fQMY2YLMJoTn6fpUhGytHSnNch',
            'app_secret'    => 'mjKM1nL3l982mNnAff3Dho0GzDlVivVUjqMEmjkVVRZ4tyY50tbQ',
            'proxy'         => '',
            'username'      => '01793601278',
            'password'      => 'ZG!n*mGHbP5',
            'token'         => 'eyJraWQiOiJmalhJQmwxclFUXC9hM215MG9ScXpEdVZZWk5KXC9qRTNJOFBaeGZUY3hlamc9IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI4ZGU4ZjBlMC1mY2RjLTQyNzMtYjY4YS1iNDAwOWNjZjc3ZDEiLCJhdWQiOiI1bmVqNWtlZ3VvcGo5Mjhla2NqM2RuZThwIiwiZXZlbnRfaWQiOiJiODcwYzg5OS0yMDQyLTQ3NzYtOWVkYy1mYWRjNThlY2NkNzMiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTU2OTIyMjA5MiwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLmFwLXNvdXRoZWFzdC0xLmFtYXpvbmF3cy5jb21cL2FwLXNvdXRoZWFzdC0xX2tmNUJTTm9QZSIsImNvZ25pdG86dXNlcm5hbWUiOiJ0ZXN0ZGVtbyIsImV4cCI6MTU2OTIyNTY5MiwiaWF0IjoxNTY5MjIyMDkyfQ.CspZCwHCD5j4OY7_WuxyKJh_oDP4JEKOAtZDDcLjJ1v0aBxhWouox9wAHselmoCoIrP_j7T0zR7aPvHnFfGw1ApmndqTKK3CXKSQTkHtAb4NK_dAsHIt-W1518DYF5JrZyn7P4VwlsD8xg_hLd0o49TN351wWfju83viHjEEMbr0r4LSefP3IaXxhwveZOrzlBcdhPhis6F3enJPx_72EESZK0JDOabZ5Bhdk1Rb1hNzKuq3k_t1II5gFhGjr-_udqR5LaSmR80LkEI_cjEXqJcBm12ZCy3_4GwolF9W-3haE0jXpdiAjT6QtzYCTKsX1DUrcnv-OAEZVyGtiGk-pA',
        ];
    }

    // get Token (static function)
    public static function bkashGetToken()
    {
        $post_token = array(
            'app_key'       => self::config()['app_key'],
            'app_secret'    => self::config()['app_secret'],
        );

        $url        = curl_init(self::config()['tokenURL']);
        $proxy      = self::config()['proxy'];
        $posttoken  = json_encode($post_token);

        $header = array(
            'Content-Type:application/json',
            'password:' . self::config()['password'],
            'username:' . self::config()['username']
        );

        curl_setopt($url, CURLOPT_HTTPHEADER, $header);
        curl_setopt($url, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($url, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($url, CURLOPT_POSTFIELDS, $posttoken);
        curl_setopt($url, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($url, CURLOPT_PROXY, $proxy);
        $resultdata = curl_exec($url);
        curl_close($url);
        return json_decode($resultdata, true);
    }

    // refresh Token (static function)
    // ***$grantRefreshToken => Refresh token value found in the Grant Token API against the original id_token.
    public static function bkashRefreshToken($grantRefreshToken)
    {
        $post_token = array(
            'app_key'       => self::config()['app_key'],
            'app_secret'    => self::config()['app_secret'],
            'refresh_token' => $grantRefreshToken,
        );
        
        $url        = curl_init(self::config()['refresh_tokenURL']);
        $proxy      = self::config()['proxy'];
        $posttoken  = json_encode($post_token);

        $header = array(
            'Content-Type:application/json',
            'password:' . self::config()['password'],
            'username:' . self::config()['username']
        );

        curl_setopt($url, CURLOPT_HTTPHEADER, $header);
        curl_setopt($url, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($url, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($url, CURLOPT_POSTFIELDS, $posttoken);
        curl_setopt($url, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($url, CURLOPT_PROXY, $proxy);
        $resultdata = curl_exec($url);
        curl_close($url);
        return json_decode($resultdata, true);
    }

    // createPayment
    public static function createPayment($request_data)
    {
        $accessToken    = $request_data['accessToken'];
        $amount         = $request_data['total_price'];
        $intent         = $request_data['intent'];
        $invoice        = "Inv".uniqid(); // must be unique
        $proxy          = self::config()["proxy"];
        $createpaybody  = array('amount'=>$amount, 'currency'=>'BDT', 'merchantInvoiceNumber'=>$invoice,'intent'=>$intent);
        $url            = curl_init(self::config()["createURL"]);
        $createpaybodyx = json_encode($createpaybody);

        $header = array(
            'Content-Type:application/json',
            'authorization:'.$accessToken,
            'x-app-key:'. self::config()["app_key"]
        );

        curl_setopt($url,CURLOPT_HTTPHEADER, $header);
        curl_setopt($url,CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($url,CURLOPT_POSTFIELDS, $createpaybodyx);
        curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($url, CURLOPT_PROXY, $proxy);
        $resultdata = curl_exec($url);
        curl_close($url);
        return $resultdata;
    }

    // executePayment
    public static function executePayment($request_data)
    {
        $accessToken    = $request_data['accessToken'];
        $paymentID      = $request_data['paymentID'];
        $url            = curl_init(self::config()["executeURL"].$paymentID);

        $header=array(
            'Content-Type:application/json',
            'authorization:'.$accessToken,
            'x-app-key:'. self::config()["app_key"]
        );

        curl_setopt($url,CURLOPT_HTTPHEADER, $header);
        curl_setopt($url,CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($url, CURLOPT_PROXY, $proxy);
        $resultdatax=curl_exec($url);
        curl_close($url);

        //Code for insert data to database start
        $data  = json_decode($resultdatax);
        if(isset($data->paymentID))
        {
            $bkash_payments_data = [
                'paymentID'             => $data->paymentID,
                'trxID'                 => $data->trxID,
                'customerMsisdn'        => $data->customerMsisdn,
                'merchantInvoiceNumber' => $data->merchantInvoiceNumber,
                'currency'              => $data->currency,
                'intent'                => $data->intent,
                'amount'                => $data->amount,
                'transactionStatus'     => $data->transactionStatus,
            ];
            \DB::table('bkash_payments_edu_197')->insert($bkash_payments_data);
        }
        //Code for insert data to database end

        return $resultdatax;
    }

    // query payment Request Parameters(paymentId,$accessToken)
    public static function queryPayment($request_data)
    {
        $paymentID      = $request_data['paymentID'];
        $accessToken    = $request_data['accessToken'];
        $url            = curl_init(self::config()['query_paymentURL'].$paymentID);

        $header = array(
            'Content-Type:application/json',
            'authorization:'.$accessToken,
            'x-app-key:'.self::config()['app_key']
        );

        curl_setopt($url,CURLOPT_HTTPHEADER, $header);
        curl_setopt($url,CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
        $resultdatax=curl_exec($url);
        curl_close($url);
        return $resultdatax;
    }

    // search transaction Request Parameters(trxID,$accessToken)
    public static function searchTransaction($request_data)
    {
        $trxID          = $request_data['trxID'];
        $accessToken    = $request_data['accessToken'];
        $url            = curl_init(self::config()['search_transactionURL'].$trxID);
        $header = array(
            'Content-Type:application/json',
            'authorization:'.$accessToken,
            'x-app-key:'.self::config()['app_key']
        );

        curl_setopt($url,CURLOPT_HTTPHEADER, $header);
        curl_setopt($url,CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
        $resultdatax=curl_exec($url);
        curl_close($url);
        return $resultdatax;
    }

    ##  ---Refund---  ##
    public static function refund($request_data)
    {
        $accessToken    = $request_data['accessToken'];
        $url            = curl_init(self::config()['refund_URL']);
        $header         = array(
            'Content-Type:application/json',
            'authorization:'.$accessToken,
            'x-app-key:'.self::config()['app_key']
        );

        $post_fields = [
            'paymentID' => $request_data['paymentId'],
            'amount'    => $request_data['amount'],
            'trxID'     => $request_data['trxId'], 
            'sku'       => $request_data['sku'], 
            'reason'    => $request_data['reason'],
        ];

        curl_setopt($url, CURLOPT_HTTPHEADER, $header);
        curl_setopt($url, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($url, CURLOPT_POSTFIELDS, json_encode($post_fields));
        curl_setopt($url, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($url, CURLOPT_FOLLOWLOCATION, 1);
        $resultdatax = curl_exec($url);
        curl_close($url);

        //Code for insert data to database start
        if(isset($data->refundTrxID))
        {
            $data  = json_decode($resultdatax);
            $bkash_payments_data = [
                'refund_amount' => $request_data['amount'],
                'refundTrxID'   => $data->refundTrxID,
            ];
            \DB::table('bkash_payments')->where('trxID', $request_data['trxId'])->update($bkash_payments_data);
        }
        //Code for insert data to database end

        return $resultdatax;
    }
    ##  ---Refund---  ##
}