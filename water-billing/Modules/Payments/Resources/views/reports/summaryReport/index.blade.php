@extends('layouts.app')

@section('title', 'Summary Report')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Summary Report</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Billing</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Summary Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="margin-left: 150px;margin-right: 150px" class="row d-print-none">
                    <div class="col-12">
                        <div class="card">
                            <div style="padding-bottom: 4px" class="card-body table-responsive">
                                <form method="get" action="{{ route('billing_summary_report_index_print') }}" enctype="multipart/form-data" target="_blank">
	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"> Date </label>
	                                    <div class="col-md-9">
	                                    	<div class="row">
	                                    		<div class="input-group-append col-md-5">
		                                    		<select style="cursor: pointer" id="from_month" class="form-control select2" name="from_month">
                                                        <option value="Jan" {{ date('m') == 'Jan' ? 'selected' : '' }}>January</option>
                                                        <option value="Feb" {{ date('m') == 'Feb' ? 'selected' : '' }}>February</option>
                                                        <option value="Mar" {{ date('m') == 'Mar' ? 'selected' : '' }}>March</option>
                                                        <option value="Apr" {{ date('m') == 'Apr' ? 'selected' : '' }}>Appril</option>
                                                        <option value="May" {{ date('m') == 'May' ? 'selected' : '' }}>May</option>
                                                        <option value="Jun" {{ date('m') == 'Jun' ? 'selected' : '' }}>Jun</option>
                                                        <option value="Jul" {{ date('m') == 'Jul' ? 'selected' : '' }}>July</option>
                                                        <option value="Aug" {{ date('m') == 'Aug' ? 'selected' : '' }}>August</option>
                                                        <option value="Sep" {{ date('m') == 'Sep' ? 'selected' : '' }}>September</option>
                                                        <option value="Oct" {{ date('m') == 'Oct' ? 'selected' : '' }}>October</option>
                                                        <option value="Nov" {{ date('m') == 'Nov' ? 'selected' : '' }}>November</option>
                                                        <option value="Dec" {{ date('m') == 'Dec' ? 'selected' : '' }}>December</option>
                                                    </select>
		                                    	</div>

		                                    	<div class="input-group-append col-md-7">
		                                    		<select style="cursor: pointer" id="from_year" class="form-control select2" name="from_year">
                                                        <option value="2021" {{ date('Y') == '2021' ? 'selected' : '' }}>2021</option>
                                                        <option value="2022" {{ date('Y') == '2022' ? 'selected' : '' }}>2022</option>
                                                        <option value="2023" {{ date('Y') == '2023' ? 'selected' : '' }}>2023</option>
                                                        <option value="2024" {{ date('Y') == '2024' ? 'selected' : '' }}>2024</option>
                                                        <option value="2025" {{ date('Y') == '2025' ? 'selected' : '' }}>2025</option>
                                                        <option value="2026" {{ date('Y') == '2026' ? 'selected' : '' }}>2026</option>
                                                    </select>
		                                    	</div>
	                                    	</div>
	                                    </div>
	                                </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Name/Company Name</label>
                                        <div class="col-md-9">
                                            <select id="customer_id" class="form-control select2" name="customer_id">
                                                <option value="0">All Customer</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Type</label>
                                        <div class="col-md-9">
                                            <select id="type_id" class="form-control select2" name="type_id">
                                                <option value="1">Show All</option>
                                                <option value="2">Due Available</option>
                                            </select>
                                        </div>
                                    </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"></label>
	                                    <div class="col-md-9">
	                                        <button style="border-radius: 0px;background-color: #297049;color: white" class="btn" type="submit" target="_blank">
	                                        	Print
	                                    	</button>
	                                    </div>
	                                </div>
                            	</form>   
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
        $( document ).ready(function() {
            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/accountsreport/select2-api/get-registers-customers-api/',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });
    </script>
@endsection