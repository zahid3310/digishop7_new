@extends('layouts.app')

@section('title', 'Pending Payment Request')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Pending Payment Request</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Digishop</a></li>
                                    <li class="breadcrumb-item active">Pending Payment Request</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif
                            <div class="card-body table-responsive">
                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;width: 5%;font-size: 12px">SL</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Digishop ID</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Date</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Particular</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">SMS Type</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Quantity</th>
                                            <th style="text-align: center;width: 15%;font-size: 12px">Transaction#</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Payable</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Paid</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Paid Through</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Status</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(count($data) > 0)
                                        @foreach($data as $key => $value)
                                        <tr>
                                            <td style="font-size: 12px;text-align: center">{{ $key + 1 }}</td>
                                            <td style="font-size: 12px;text-align: center">{{ $value->customer->digishop_id }}</td>
                                            <td style="font-size: 12px">{{ date('d-m-Y', strtotime($value->date)) }}</td>
                                            <td style="text-align: center;font-size: 12px">SMS Purchase</td>
                                            <td style="text-align: center;font-size: 12px">
                                                @if($value->sms_type == 1)
                                                Non Masking
                                                @elseif($value->sms_type == 2)
                                                Masking
                                                @else
                                                Voice
                                                @endif
                                            </td>
                                            <td style="font-size: 12px;text-align: center">{{ $value->quantity }}</td>
                                            <td style="font-size: 12px">{{ $value->transaction_id }}</td>
                                            <td style="text-align: right;font-size: 12px">{{ number_format($value->total_payable,0,'.',',') }}</td>
                                            <td style="text-align: right;font-size: 12px">{{ number_format($value->paid_amount,0,'.',',') }}</td>
                                            <td style="font-size: 12px;text-align: center">Bkash</td>
                                            <td style="text-align: center;font-size: 12px;font-weight: bold;">
                                                @if($value->status == 0)
                                                <span style="color: red">Pending</span>
                                                @else
                                                <span style="color: green">Approved</span>
                                                @endif
                                            </td>
                                            <td style="font-size: 12px;text-align: center">
                                                <a style="custor: pointer" href="{{ route('payments_pending_bill_sms_approve',$value->id) }}" onclick="return confirm('you want to approve?');"><i class="fa fa-plus"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    function getRate()
    {
        var typeId      = $("#sms_type").val();
        var site_url    = $('.site_url').val();

        $.get(site_url + '/softwarebilling/sms-rate/' + typeId, function(data){
            
            if (typeId == 1)
            {
                $("#sms_rate").val(parseFloat(data.non_masking_sms_rate));
            }

            if(typeId == 2)
            {
                $("#sms_rate").val(parseFloat(data.masking_sms_rate));
            }

            if(typeId == 3)
            {
                $("#sms_rate").val(parseFloat(data.voice_sms_rate));
            }
        });
    }

    function calculate()
    {
        var sms_rate = $('#sms_rate').val();
        var sms_qty  = $('#quantity').val();

        if (sms_rate == '')
        {
            var rateCal     = 0;
        }
        else
        {
            var rateCal     = $("#sms_rate").val();
        }

        if (sms_qty == '')
        {
            var quantityCal = 0;
        }
        else
        {
            var quantityCal = $("#quantity").val();
        }

        var total = parseFloat(rateCal)*parseFloat(quantityCal);

        $("#total_payable").val(total.toFixed());
    }
</script>
@endsection