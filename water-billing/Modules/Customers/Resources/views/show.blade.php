@extends('layouts.app')

@section('title', 'Customer Details')

<style>
    table,th {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 2px;
    }

    td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        padding: 2px;
    }

    @page {
        size: A4;
        page-break-after: always;
    }
</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-8">
                        <div class="card">
                            <div class="card-body">
                                <div style="padding-top: 10px;padding-bottom: 20px">
                                    <table style="width: 100%">
                                        <tr style="background:#ddd;">
                                            <th style="text-align: center;width: 20%">Title</th>
                                            <th style="text-align: center;width: 30%">Details</th>
                                        </tr>

                                        <tr>
                                            <td style="text-align: right;padding-right: 20px"><strong>Position</strong></td>
                                            <td style="text-align: left;padding-left: 20px">{{ $customer->position != null ? $customer->position : 'N/A' }}</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;padding-right: 20px"><strong>Organization Name</strong></td>
                                            <td style="text-align: left;padding-left: 20px">{{ $customer->name }}</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;padding-right: 20px"><strong>Admin Name</strong></td>
                                            <td style="text-align: left;padding-left: 20px">{{ $customer->contact_person != null ? $customer->contact_person : 'N/A' }}</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;padding-right: 20px"><strong>Admin Mobile Number</strong></td>
                                            <td style="text-align: left;padding-left: 20px">{{ $customer->phone != null ? $customer->phone : 'N/A' }}</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;padding-right: 20px"><strong>Admin Email</strong></td>
                                            <td style="text-align: left;padding-left: 20px">{{ $customer->email != null ? $customer->email : 'N/A' }}</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;padding-right: 20px"><strong>Address</strong></td>
                                            <td style="text-align: left;padding-left: 20px">{{ $customer->address != null ? $customer->address : 'N/A' }}</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;padding-right: 20px"><strong>Bill Type</strong></td>
                                            <td style="text-align: left;padding-left: 20px">
                                                @if($customer->bill_type != null)
                                                @if($customer->bill_type == 1)
                                                Pre Paid
                                                @else
                                                Post Paid
                                                @endif
                                                @else
                                                N/A
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;padding-right: 20px"><strong>Bill Amount</strong></td>
                                            <td style="text-align: left;padding-left: 20px">{{ $customer->bill_amount != null ? $customer->bill_amount : 'N/A' }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-2"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection
