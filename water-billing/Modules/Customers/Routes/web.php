<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('customers')->group(function() {
    Route::get('/', 'CustomersController@index')->name('customers_index');
    Route::post('/store', 'CustomersController@store')->name('customers_store');
    Route::get('/edit/{id}', 'CustomersController@edit')->name('customers_edit');
    Route::post('/update/{id}', 'CustomersController@update')->name('customers_update');
    Route::get('/show/{id}', 'CustomersController@show')->name('customers_show');
});