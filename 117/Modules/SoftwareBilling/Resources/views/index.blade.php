@extends('layouts.app')

@section('title', 'Transaction History')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }

        .mb-5 {
            margin: 0rem 0rem 0rem 0rem !important;
            font-size: 10px !important; 
            border-radius: 0px !important;
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Software Billing</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Pay Bill</a></li>
                                    <li class="breadcrumb-item active">Transaction History</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <?php
                    $total_payable = collect($data)->where('debit_credit', 1)->sum('amount');
                    $total_paid    = collect($data)->where('debit_credit', 0)->sum('amount');
                    $total_due     = $total_payable - $total_paid;
                ?>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div style="text-align: center;background-color: #D2D2D2" class="card-body">
                                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-lg-4 col-xs-4">
                                            <div class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                                <div class="card-body">
                                                    
                                                    <div class="media">
                                                        <div class="media-body">
                                                            <p class="text-muted font-weight-medium">Total Payable</p>
                                                            <h4 class="mb-0">{{ number_format($total_payable ,0,'.',',') }}</h4>
                                                        </div>

                                                        <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                                            <span class="avatar-title">
                                                                <i class="bx bx-credit-card font-size-24"></i>
                                                            </span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4 col-lg-4 col-xs-4">
                                            <div class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                                <div class="card-body">
                                                    
                                                    <div class="media">
                                                        <div class="media-body">
                                                            <p class="text-muted font-weight-medium">Total Paid</p>
                                                            <h4 id="totalReceived" class="mb-0">{{ number_format($total_paid,0,'.',',') }}</h4>
                                                        </div>

                                                        <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                                            <span class="avatar-title">
                                                                <i class="bx bx-credit-card font-size-24"></i>
                                                            </span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4 col-lg-4 col-xs-4">
                                            <div class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                                <div class="card-body">
                                                    
                                                    <div class="media">
                                                        <div class="media-body">
                                                            <p class="text-muted font-weight-medium">Total Due</p>
                                                            <h4 id="totalDues" class="mb-0">{{ number_format($total_due,0,'.',',') }}</h4>
                                                        </div>

                                                        <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                                            <span class="avatar-title">
                                                                <i class="bx bx-credit-card font-size-24"></i>
                                                            </span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <button style="margin-bottom: 0rem!important;border-radius: 0px !important" type="button" title="SMS Purchase" class="btn btn-primary mb-5">
                                    <a style="color: white;display: block !important" href="{{ route('software_billing_sms_purchase') }}"><strong>SMS Purchase</strong></a>
                                </button>

                                <button style="margin-bottom: 0rem!important;border-radius: 0px !important" type="button" title="Bill Payment" class="btn btn-danger mb-5">
                                    <a style="color: white;display: block !important" href=""><strong>Pay Bill</strong></a>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <hr style="margin-top: 0px">

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;width: 5%;font-size: 12px">SL</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Date</th>
                                            <th style="text-align: center;width: 25%;font-size: 12px">Particular</th>
                                            <th style="text-align: center;width: 15%;font-size: 12px">Paid Through</th>
                                            <th style="text-align: center;width: 15%;font-size: 12px">Transaction#</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Payable</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Paid</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Balance</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $balance = 0; ?>
                                        @if(count($data) > 0)
                                        @foreach($data as $key => $value)
                                        <?php
                                            if($value->transaction_head == 'payment-receive')
                                            {
                                                $particular = $value->note;
                                                $payable    = 0;
                                                $paid       = $value->amount;
                                                $balance    = $balance - $paid;
                                            }
                                            elseif($value->transaction_head == 'one-time-payment' || $value->transaction_head == 'service-charge' || $value->transaction_head == 'sms-purchase')
                                            {
                                                $particular = $value->note;
                                                $payable    = $value->amount;
                                                $paid       = 0;
                                                $balance    = $balance + $payable;
                                            }
                                        ?>
                                        <tr>
                                            <td style="font-size: 12px;text-align: center">{{ $key + 1 }}</td>
                                            <td style="font-size: 12px;text-align: center">{{ date('d-m-Y', strtotime($value->date)) }}</td>
                                            <td style="text-align: center;font-size: 12px;text-align: left">{{ $particular }}</td>
                                            <td style="font-size: 12px;text-align: center">{{ $value->account_name }}</td>
                                            <td style="font-size: 12px">{{ $value->transaction_number }}</td>
                                            <td style="text-align: right;font-size: 12px">{{ number_format($payable,0,'.',',') }}</td>
                                            <td style="text-align: right;font-size: 12px">{{ number_format($paid,0,'.',',') }}</td>
                                            <td style="text-align: right;font-size: 12px">{{ number_format($balance,0,'.',',') }}</td>
                                        </tr>
                                        @endforeach
                                        @endif
                                        
                                        <tr>
                                            <td style="text-align: right" colspan="7"><strong>Total @if($balance > 0) Due @elseif($balance < 0) Advance @else Balance @endif</strong></td>
                                            <td style="text-align: right"><strong>{{ number_format($balance,0,'.',',') }}</strong></td>
                                        </tr>
                                    </tbody>    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection