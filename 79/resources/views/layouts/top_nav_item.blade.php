<style type="text/css">
    .m-5 {
        margin: 0.7rem 0.1rem !important;
        font-size: 1.4rem !important; 
    }
    
    blink {
      -webkit-animation: 2s linear infinite condemned_blink_effect; /* for Safari 4.0 - 8.0 */
      animation: 2s linear infinite condemned_blink_effect;
    }

    /* for Safari 4.0 - 8.0 */
    @-webkit-keyframes condemned_blink_effect {
      0% {
        visibility: hidden;
      }
      50% {
        visibility: hidden;
      }
      100% {
        visibility: visible;
      }
    }

    @keyframes condemned_blink_effect {
      0% {
        visibility: hidden;
      }
      50% {
        visibility: hidden;
      }
      100% {
        visibility: visible;
      }
    }
</style>

<header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <div style="padding: 0rem !important" class="navbar-brand-box">
                <a href="{{ route('home') }}" class="logo logo-light">
                    <span class="logo-sm">
                        <img style="width: 40;height: 30px;margin-top: 15px" src="{{ url('/public/favicon.png') }}" alt="">
                    </span>
                    <span class="logo-lg">
                        @if(userDetails()->logo != null)
                        <img style="width: 50px;height: 50px;margin-top: 15px" src="{{ url('public/'.userDetails()->logo) }}" alt="">
                        @else
                        @endif
                    </span>
                </a>
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
                <i class="fa fa-fw fa-bars"></i>
            </button>

            <button type="button" title="Dashboard" class="btn btn-danger btn-flat m-6 btn-xs m-5 btn-modal pull-right hidden-xs">
                <a style="color: white;display: block !important" href="{{ route('home') }}"><strong><i class="fa fa-home"></i></strong></a>
            </button>

            <button type="button" title="Sells List" class="btn btn-info btn-flat m-6 btn-xs m-5 btn-modal pull-right hidden-xs">
                <a style="color: white;display: block !important" href="{{ route('invoices_all_sales') }}"><strong><i class="fas fa-list"></i></strong></a>
            </button>

            <button type="button" title="Sells" class="btn btn-primary btn-flat m-6 btn-xs m-5 btn-modal pull-right hidden-xs" data-href="">
                <a style="color: white;display: block !important" href="{{ route('invoices_index') }}"><strong><i class="fa fa-cart-plus"></i></strong></a>
            </button>

            <button type="button" title="Sales Return" class="btn btn-info btn-flat m-6 btn-xs m-5 btn-modal pull-right hidden-xs" data-href="">
                <a style="color: white;display: block !important" href="{{ route('sales_return_index') }}"><strong><i class="fas fa-history"></i></strong></a>
            </button>

            <button type="button" title="Purchase" class="btn btn-warning btn-flat m-6 btn-xs m-5 btn-modal pull-right hidden-xs" data-href="">
                <a style="color: white;display: block !important" href="{{ route('bills_index') }}"><strong> <i class="fas fa-suitcase"></i></strong></a>
            </button>

            <button type="button" title="Purchase Return" class="btn btn-info btn-flat m-6 btn-xs m-5 btn-modal pull-right hidden-xs">
                <a style="color: white;display: block !important" href="{{ route('purchase_return_index') }}"><strong><i class="fas fa-undo-alt"></i></strong></a>
            </button>

            <button type="button" title="Payments" class="btn btn-success btn-flat m-6 btn-xs m-5 btn-modal pull-right hidden-xs">
                <a style="color: white;display: block !important" href="{{ route('payments_create') }}"><strong><i class="fas fa-coins"></i></strong></a>
            </button>

            <button type="button" title="Membership/Cupon" class="btn btn-info btn-flat m-6 btn-xs m-5 btn-modal pull-right hidden-xs">
                <a style="color: white;display: block !important" href="{{ route('discounts_index') }}"><strong><i class="fas fa-address-card"></i></strong></a>
            </button>

            <button type="button" title="Contacts" class="btn btn-info btn-flat m-6 btn-xs m-5 btn-modal pull-right hidden-xs" data-href="">
                <a style="color: white;display: block !important" href="{{ route('customers_index') }}"><strong><i class="fas fa-users"></i></strong></a>
            </button>

            <button type="button" title="Print Bar Code" class="btn btn-primary btn-flat m-6 btn-xs m-5 btn-modal pull-right hidden-xs" data-href="">
                <a style="color: white;display: block !important" href="{{ route('products_barcode_print') }}"><strong><i class="fa fa-barcode"></i></strong></a>
            </button>
        </div>

        <div class="d-flex">
            <div class="dropdown d-inline-block">
                <button type="button" title="Bill Pay" class="btn btn-info btn-flat m-6 btn-xs m-5 btn-modal pull-right hidden-xs">
                    <a style="color: white;display: block !important;font-size: 14px;border-radius: 0px !important" href="{{ route('software_billing_index') }}"><blink>Bill Pay</blink></strong></a>
                </button>
                
                <button type="button" title="Dashboard" class="btn btn-danger btn-flat m-6 btn-xs m-5 btn-modal pull-right hidden-xs">
                    <a style="color: white;display: block !important;font-size: 14px;border-radius: 0px !important" href="{{ route('emergency_item_list_index') }}"><strong>Stock Out : <span id="stockOutProduct"></span></strong></a>
                </button>

                <button style="font-size: 0.8125rem !important" type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(Auth::user()->photo != null)
                    <img class="rounded-circle header-profile-user" src="{{ url('public/'. Auth::user()->photo) }}"
                        alt="Header Avatar">
                    @else
                    <img class="rounded-circle header-profile-user" src="{{ url('public/default.png') }}"
                        alt="Header Avatar">
                    @endif
                    <span class="d-none d-xl-inline-block ml-1">{{ Auth::user()->name }}</span>
                    <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{ route('users_edit_profile') }}"><i class="bx bx-user-circle font-size-16 align-middle mr-1"></i> Profile</a>
                    @if(Auth::user()->role == 1)
                    <a class="dropdown-item" href="{{ route('users_edit_settings') }}"><i class="bx bx-wrench font-size-16 align-middle mr-1"></i> Settings</a>
                    @endif
                    <a class="dropdown-item text-danger" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> Logout</a> 
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>