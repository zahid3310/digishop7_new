<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class BranchInventories extends Model
{  
    protected $table = "branch_inventories";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

}
