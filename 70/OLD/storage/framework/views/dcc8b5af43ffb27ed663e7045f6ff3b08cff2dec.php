

<?php $__env->startSection('title', 'Print Pos'); ?>

<?php if($user_info['pos_printer'] == 0): ?>
<style type="text/css">
    @media  print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page  {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 60mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }
    }
</style>
<?php endif; ?>

<?php if($user_info['pos_printer'] == 1): ?>
<style type="text/css">
    @media  print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page  {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 90mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }
    }
</style>
<?php endif; ?>

<?php if($user_info['pos_printer'] == 2 || $user_info['pos_printer'] == 3): ?>
<style type="text/css">
    @media  print {
        #footerInvoice {
            position: fixed;
            bottom: 60;
        }
        
    .border-none{
        border-bottom: 1px solid #fff!important;
        border-left: 1px solid #fff!important;
        border-right: 1px solid #fff!important;
        border-top: 1px solid #fff!important;
    }
    
    .border-none-2{
        border-bottom: 1px solid #fff!important;
        border-left: 1px solid #fff!important;
        border-top: 1px solid #fff!important;
    }
        
        
    }

    .textstyle {
        background-color: white; /* Changing background color */
        font-weight: bold; /* Making font bold */
        border-radius: 20px; /* Making border radius */
        border: 3px solid black; /* Making border radius */
        width: auto; /* Making auto-sizable width */
        height: auto; /* Making auto-sizable height */
        padding: 5px 10px 5px 10px; /* Making space around letters */
        font-size: 18px; /* Changing font size */
    }

    .column-bordered-table thead td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table tfoot tr {
        border-top: 1px solid #c3c3c3;
        border-bottom: 1px solid #c3c3c3;
    }

    table,th {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 2px;
        font-weight: bold;
        font-size: 18px
    }

   /* td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        padding: 2px;
        font-weight: bold;
        font-size: 18px
    }*/

    td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        /*padding: 2px;*/
        font-weight: bold;
        font-size: 16px
    }

     table, th, td {
  border: 1px solid black!important;
}

    @page  {
        size: A4;
        page-break-after: always;
    }
</style>
<?php endif; ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content marginTopPrint">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row d-print-none">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.sales')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.sales')); ?></a></li>
                                    <li class="breadcrumb-item active">Print Pos</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <?php if($user_info['pos_printer'] == 0): ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div id="hedColor" style="margin-top: 0px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold"><?php echo e($user_info['address']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 18px;font-weight: bold"><?php echo e($user_info['contact_number']); ?></p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 18px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">Invoice</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 14px">Date : <?php echo e(date('d/m/Y', strtotime($invoice['invoice_date']))); ?></span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 14px"><?php echo e(date('h:i a', strtotime(now()))); ?></span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 14px">Invoice# : <?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?></span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">
                                    
                                </div>

                                <div style="border-top: 2px dashed black;border-bottom: 2px dashed black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px;" class="col-2"><strong>Qt</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-4"><strong>Item</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Price</strong></div>
                                    <div style="font-size: 12px;text-align: right;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Total</strong></div>
                                </div>

                                <?php if(!empty($entries) && ($entries->count() > 0)): ?>

                                    <?php 
                                        $sub_total              = 0; 
                                        $total_discount         = 0;
                                    ?>

                                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <div style="padding-bottom: 10px;" id="DetailsFontSize" class="row">

                                        <?php 
                                            $discount           = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; 
                                            $total_discount     = $total_discount + $discount;
                                        ?>

                                        <div style="font-size: 12px" class="col-2">
                                            <?php echo e($value['quantity']); ?> <br> <?php echo e($value->convertedUnit->name); ?>

                                        </div>

                                        <div style="font-size: 12px" class="col-4">
                                            <?php if($value['product_type'] == 1): ?>
                                                <?php echo $value['product_entry_name']; ?>
                                            <?php else: ?>
                                                <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                            <?php endif; ?>
                                        </div>
                                        <div style="font-size: 12px" class="col-3">
                                            <?php echo e(number_format($value['rate'])); ?>

                                        </div>
                                        <div style="font-size: 12px;text-align: right" class="col-3"><?php echo e(number_format($value['total_amount'])); ?></div>
                                    </div>

                                    <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>GROSS Total :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong><?php echo e(number_format($sub_total)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-bottom: 10px" class="row"></div> -->

                                <?php
                                    $total_vat_amount       = $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : $invoice['total_vat']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>Total VAT :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong><?php echo e(number_format($total_vat_amount)); ?></strong></div>
                                </div>

                                <?php
                                    $total_discount_amount       = $invoice['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$invoice['total_vat'])/100))*$invoice['total_discount_amount'])/100) : $invoice['total_discount_amount']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>Total Discount :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong><?php echo e(number_format($total_discount + $total_discount_amount)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;" class="row">
                                    <div style="font-size: 14px" class="col-6"><strong>NET Total :</strong></div>
                                    <div style="font-size: 14px;text-align: right" class="col-6"><strong><?php echo e(number_format($sub_total + $total_vat_amount - $total_discount_amount)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px;" class="row"></div> -->
                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px;" class="row"></div> -->

                               <!--  <div class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-12">Payments</div>
                                </div> -->

                                <div style="padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 14px;font-weight: bold" class="col-6">Total Paid</div>
                                    <div style="font-size: 14px;text-align: right;font-weight: bold" class="col-6"><?php echo e(number_format($invoice['invoice_amount'] - $invoice['due_amount'])); ?></div>
                                </div>

                                <!-- <div style="padding-left: 15px;border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 16px" class="col-6">-TOTAL DUES</div>
                                    <div style="font-size: 16px;text-align: right" class="col-6"><?php echo e(number_format($invoice['due_amount'],2,'.',',')); ?></div>
                                </div> -->

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 12px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if($user_info['pos_printer'] == 1): ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div id="hedColor" style="margin-top: 0px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 26px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight: bold"><?php echo e($user_info['address']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 20px;font-weight: bold"><?php echo e($user_info['contact_number']); ?></p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 20px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">Invoice</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 16px">Date : <?php echo e(date('d/m/Y', strtotime($invoice['invoice_date']))); ?></span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 16px">Time : <?php echo e(date('h:i a', strtotime(now()))); ?></span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 16px">Invoice# : <?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?></span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">
                                    
                                </div>

                                <div style="border-top: 2px dashed black;border-bottom: 2px dashed black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-2"><strong>Qty</strong></div>
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-4"><strong>Item Name</strong></div>
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Price</strong></div>
                                    <div style="font-size: 16px;text-align: right;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>T.Price</strong></div>
                                </div>

                                <?php if(!empty($entries) && ($entries->count() > 0)): ?>

                                    <?php 
                                        $sub_total              = 0; 
                                        $total_discount         = 0;
                                    ?>

                                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <div style="padding-bottom: 10px;" id="DetailsFontSize" class="row">

                                        <?php 
                                            $discount           = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; 
                                            $total_discount     = $total_discount + $discount;
                                        ?>

                                        <div style="font-size: 16px" class="col-2">
                                            <?php echo e($value['quantity']); ?> <br> <?php echo e($value->convertedUnit->name); ?>

                                        </div>

                                        <div style="font-size: 16px" class="col-4">
                                            <?php if($value['product_type'] == 1): ?>
                                                <?php echo $value['product_entry_name']; ?>
                                            <?php else: ?>
                                                <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                            <?php endif; ?>
                                        </div>
                                        <div style="font-size: 16px" class="col-3">
                                            <?php echo e(number_format($value['rate'])); ?>

                                        </div>
                                        <div style="font-size: 16px;text-align: right" class="col-3"><?php echo e(number_format($value['total_amount'])); ?></div>
                                    </div>

                                    <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>GROSS Total :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong><?php echo e(number_format($sub_total)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-bottom: 10px" class="row"></div> -->

                                <?php
                                    $total_vat_amount       =  $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : $invoice['total_vat']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>Total VAT :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong><?php echo e(number_format($total_vat_amount)); ?></strong></div>
                                </div>

                                <?php
                                    $total_discount_amount       = $invoice['total_discount_type'] == 0 ? ((($sub_total + $total_vat_amount)*$invoice['total_discount_amount'])/100) : $invoice['total_discount_amount']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>Total Discount :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong><?php echo e(number_format($total_discount + $total_discount_amount)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;" class="row">
                                    <div style="font-size: 20px" class="col-6"><strong>NET Total :</strong></div>
                                    <div style="font-size: 20px;text-align: right" class="col-6"><strong><?php echo e(number_format($sub_total + $total_vat_amount - $total_discount_amount)); ?></strong></div>
                                </div>

                                <!-- <div style="border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 20px" class="col-12"><strong>VAT Included</strong></div>
                                </div> -->

                                <!-- <div style="padding-top: 10px;" class="row"></div> -->
                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px;" class="row"></div> -->

                               <!--  <div class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-12">Payments</div>
                                </div> -->

                                <div style="padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-6">Total Paid</div>
                                    <div style="font-size: 18px;text-align: right;font-weight: bold" class="col-6"><?php echo e(number_format($invoice['invoice_amount'] - $invoice['due_amount'])); ?></div>
                                </div>

                                <!-- <div style="padding-left: 15px;border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 16px" class="col-6">-TOTAL DUES</div>
                                    <div style="font-size: 16px;text-align: right" class="col-6"><?php echo e(number_format($invoice['due_amount'],2,'.',',')); ?></div>
                                </div> -->

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 16px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if($user_info['pos_printer'] == 2 || $user_info['pos_printer'] == 3): ?>
                    <div style="padding: 10px;padding-top: 25px" class="row">
                        <div style="padding-bottom: 30px" class="d-print-none col-md-12">
                            <div class="float-right">
                                <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                            </div>
                        </div>

                        <div style="border-right: 1px dotted black" class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                    <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                                </div>
                                <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                    <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 26px;padding-top: 10px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></h2>
                                   <!--  <p style="line-height: 1;font-size: 20px;font-weight: bold" class="text-center">প্রোঃ মোঃ মাসুদ রানা</p> -->
                                </div>
                                <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                    <?php echo e(QrCode::size(60)->generate("string")); ?>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px"> <?php echo e($user_info['address']); ?></p>
                                    <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px"><span style="font-weight:bold;">Phone</span> <?php echo e($user_info['contact_number']); ?> <span style="font-weight:bold;">Email:</span> <?php echo e($user_info['contact_email']); ?></p>
                                 <!--    <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 13px">সর্বউৎকৃষ্টমানের বিস্কুট, চানাচুর, লজেন্স, স্পেশাল লাচ্ছা ও খিল সেমাই বিক্রেতা ।</p> -->
                                </div>
                            </div>

                            <hr style="margin: 5px !important">

                            <div class="row" style="line-height: 25px;">
                                <div style="font-size: 18px" class="col-md-7">
                                   
                                        <strong>Invoice No - </strong>

                                        <?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?>

                                        <!-- <?php echo e(eng2bangMonth(date('m', strtotime($invoice['invoice_date'])))); ?>/<?php echo e(eng2bang(date('y', strtotime($invoice['invoice_date'])))); ?>/<?php echo e(eng2bang($invoice['invoice_number'])); ?>  -->   
                                </div>

                                <div style="font-size: 18px" class="col-md-5 text-sm-right">
                                   
                                        <strong>Date: </strong>
                                        <span style="font-weight: bold; color: green;">
                                            <?php echo e(date('d-m-Y', strtotime($invoice['invoice_date']))); ?>

                                        </span>    
                                </div>

                                <div style="font-size: 18px" class="col-md-12">
                                   
                                        <span style="font-weight: bold">SR/Customer Name:  </span><span style="font-weight: bold; color: red;"><?php echo e($invoice['customer_name']); ?> </span><br>
                                        <span style="font-weight: bold">Phone: </span><strong><?php echo e($invoice['customer_phone'] != null ? $invoice['customer_phone'] : $invoice->customer->phone); ?></strong>     
                                </div>
                            </div>

                            <div style="padding-top: 0px;padding-bottom: 0px">
                                <table style="width: 100%;">
                                    <tr>
                                        <th style="font-size: 18px;width: 5%;text-align: center">No</th>
                                        <th colspan="2" style="font-size: 18px;width: 50%;text-align: center">Description</th>
                                        <th style="font-size: 18px;width: 15%;text-align: center">Rate</th>
                                        <th style="font-size: 18px;width: 10%;text-align: center">Quantity</th>
                                        <th style="font-size: 18px;width: 20%;text-align: center">Price</th>
                                    </tr>

                                    <?php if($entries->count() > 0): ?>

                                    <?php
                                    $total_amount                   = 0;
                                    ?>

                                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php
                                        $total_amount   = $total_amount + ($value['total_amount']);
                                        $variation_name = ProductVariationName($value['product_entry_id']);

                                        if ($value['product_code'] != null)
                                        {
                                            $productCode  = ' - '.$value['product_code'];
                                        }
                                        else
                                        {
                                            $productCode  = '';
                                        }

                                        if ($value['product_name'] != null)
                                        {
                                            $category  = ' - '.$value['product_name'];
                                        }
                                        else
                                        {
                                            $category  = '';
                                        }

                                        if ($value['brand_name'] != null)
                                        {
                                            $brandName  = $value['brand_name'];
                                        }
                                        else
                                        {
                                            $brandName  = '';
                                        }

                                        if ($value['unit_name'] != null)
                                        {
                                            $unit  = ' '.$value['unit_name'];
                                        }
                                        else
                                        {
                                            $unit  = '';
                                        }

                                        if ($variation_name != null)
                                        {
                                            $variation  = ' '.$variation_name;
                                        }
                                        else
                                        {
                                            $variation  = '';
                                        }

                                        $pre_dues = $invoice['previous_due'];
                                        $net_paya = round($total_amount, 2);
                                        $paid     = round($invoice['invoice_amount'] - $invoice['due_amount'], 2);
                                        $dues     = round($net_paya - $paid, 2);
                                    ?>



                                    <tr class="tr-height">
                                        <td <?php if($value['product_entry_id'] == 94 || $value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6): ?> style="text-align: center;color: red" <?php endif; ?> style="text-align: center"><?php echo e($key + 1); ?></td>
                                        <td <?php if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6): ?> style="padding-left: 30px;color: red" <?php endif; ?> colspan="2" style="padding-left: 30px"><?php echo e($value['product_entry_name'] . $variation); ?></td>
                                        <td <?php if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6): ?> style="text-align: center;color: red" <?php endif; ?> style="text-align: center"><?php echo e($value['rate']); ?></td>
                                        <td <?php if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6): ?> style="text-align: center;color: red" <?php endif; ?> style="text-align: center"><?php echo e($value['quantity'] . $unit); ?></td>
                                        <td <?php if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6): ?> style="text-align: right;color: red" <?php endif; ?> style="text-align: right"><?php echo e(round($value['total_amount'], 2)); ?></td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>

                                    <?php
                                        if ($invoice['vat_type'] == 0)
                                        {
                                            $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                        }
                                        else
                                        {
                                            $vat_amount  = $invoice['total_vat'];
                                        }

                                        if ($invoice['total_discount_type'] == 0)
                                        {
                                            $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                        }
                                        else
                                        {
                                            $discount_on_total_amount  = $invoice['total_discount_amount'];
                                        }
                                    ?>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2" style="width: 115px!important;"></th>
                                        <th style="text-align: left;text-align: right"><strong style="color: green;">Sub Total</strong></th>
                                        <th colspan="3" style="text-align: right;font-weight: bold; color: green;"><?php echo e($net_paya != 0 ? round($net_paya - $invoice['total_discount']) : ''); ?></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>Discount</strong></th>
                                        <th colspan="3" style="text-align: right"><?php echo e(round($discount_on_total_amount)); ?></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>Previous Due</strong></th>
                                        <th colspan="3" style="text-align: right"><?php echo e($pre_dues != 0 ? round($pre_dues) : ''); ?></th>
                                    </tr>

                                    

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong style="color: red;">Total </strong></th>
                                        <th colspan="3" style="text-align: right;font-weight: bold; color: red;"><?php echo e($net_paya - $invoice['total_discount'] + $pre_dues - $discount_on_total_amount  != 0 ? round($net_paya - $invoice['total_discount'] + $pre_dues - $discount_on_total_amount) : ''); ?></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>Paid</strong></th>
                                        <th colspan="3" style="text-align: right"><?php echo e($invoice['cash_given']); ?></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>Current Balance</strong></th>
                                        <th colspan="3" style="text-align: right"><?php echo e(($net_paya - $invoice['total_discount'] + $pre_dues != 0 ? round($net_paya - $invoice['total_discount'] - $discount_on_total_amount + $pre_dues) : '') -$invoice['cash_given']); ?> </th>
                                    </tr>

                                   <!--  <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>Other Paid</strong></th>
                                        <th colspan="3" style="text-align: right"></th>
                                    </tr>

                                    <tr>
                                        <th class="border-none"></th>
                                        <th class="border-none-2"></th>
                                        <th style="text-align: right"><strong>Current Balance</strong></th>
                                        <th colspan="3" style="text-align: right"></th>
                                    </tr> -->
                                </table>
                            </div>

                            <br>

                            <div class="row" style="padding-top:50px!important">
                                <div class="col-md-6">
                                    <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px;font-size: 18px">Buyer's signature </span> </h6>
                                </div>
                                <div class="col-md-6">
                                    <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px;font-size: 18px">Seller's signature</span> </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                        <div class="col-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                            <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 26px;padding-top: 10px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></h2>
                                            <p style="line-height: 1;font-size: 20px;font-weight: bold" class="text-center">প্রোঃ মোঃ মাসুদ রানা</p>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                            <?php echo e(QrCode::size(60)->generate("string")); ?>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px"> এমরান মার্কেট, গোয়ালপাড়া, চাঁপাইনবাবগঞ্জ । <span style="font-weight: bold"> ফোনঃ </span> ০৭৮১৫২৩০৯</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">মোবাঃ ০১৭১৮৬২৯৪৫৩ Email : sazzadenterprise24@gmail.com</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 13px">সর্বউৎকৃষ্টমানের বিস্কুট, চানাচুর, লজেন্স, স্পেশাল লাচ্ছা ও খিল সেমাই বিক্রেতা ।</p>
                                        </div>
                                    </div>

                                    <hr style="margin: 5px !important">

                                    <div class="row" style="line-height: 25px;">
                                        <div style="font-size: 18px" class="col-md-7">
                                           
                                                <strong>ইনভয়েস/চালান নং - </strong><?php echo e(eng2bangMonth(date('m', strtotime($invoice['invoice_date'])))); ?>/<?php echo e(eng2bang(date('y', strtotime($invoice['invoice_date'])))); ?>/<?php echo e(eng2bang($invoice['invoice_number'])); ?>    
                                        </div>

                                        <div style="font-size: 18px" class="col-md-5 text-sm-right">
                                           
                                                <strong>তারিখঃ </strong>
                                                <span style="font-weight: bold; color: #556ee6;">
                                                    <?php echo e(eng2bang(date('d-m-Y', strtotime($invoice['invoice_date'])))); ?>

                                                </span>    
                                        </div>

                                        <div style="font-size: 18px" class="col-md-12">
                                           
                                                <span style="font-weight: bold">এসআর/কাস্টমারের নামঃ  </span><span style="font-weight: bold; color: red;"><?php echo e($invoice['customer_name']); ?> </span><br>
                                                <span style="font-weight: bold">ফোনঃ </span><strong><?php echo e($invoice['customer_phone'] != null ? $invoice['customer_phone'] : $invoice->customer->phone); ?></strong>     
                                        </div>
                                    </div>

                                    <div style="padding-top: 0px;padding-bottom: 0px">
                                        <table style="width: 100%;">
                                            <tr>
                                                <th style="font-size: 18px;width: 5%;text-align: center">নং</th>
                                                <th colspan="2" style="font-size: 18px;width: 50%;text-align: center">বিবরণ</th>
                                                <th style="font-size: 18px;width: 15%;text-align: center">দর</th>
                                                <th style="font-size: 18px;width: 10%;text-align: center">পরিমান</th>
                                                <th style="font-size: 18px;width: 20%;text-align: center">মূল্য</th>
                                            </tr>

                                            <?php if($entries->count() > 0): ?>

                                            <?php
                                            $total_amount                   = 0;
                                            ?>

                                            <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $variation_name = ProductVariationName($value['product_entry_id']);

                                            if ($value['product_code'] != null)
                                            {
                                                $productCode  = ' - '.$value['product_code'];
                                            }
                                            else
                                            {
                                                $productCode  = '';
                                            }

                                            if ($value['product_name'] != null)
                                            {
                                                $category  = ' - '.$value['product_name'];
                                            }
                                            else
                                            {
                                                $category  = '';
                                            }

                                            if ($value['brand_name'] != null)
                                            {
                                                $brandName  = $value['brand_name'];
                                            }
                                            else
                                            {
                                                $brandName  = '';
                                            }

                                            if ($value['unit_name'] != null)
                                            {
                                                $unit  = ' '.$value['unit_name'];
                                            }
                                            else
                                            {
                                                $unit  = '';
                                            }

                                            if ($variation_name != null)
                                            {
                                                $variation  = ' '.$variation_name;
                                            }
                                            else
                                            {
                                                $variation  = '';
                                            }

                                            $pre_dues = $invoice['previous_due'];
                                            $net_paya = round($total_amount, 2);
                                            $paid     = round($invoice['invoice_amount'] - $invoice['due_amount'], 2);
                                            $dues     = round($net_paya - $paid, 2);
                                            ?>

                                            <tr class="tr-height">
                                                <td style="text-align: center"><?php echo e(eng2bang($key + 1)); ?></td>
                                                <td colspan="2" style="padding-left: 30px"><?php echo e($value['product_entry_name'] . $variation); ?></td>
                                                <td style="text-align: center"><?php echo e(eng2bang($value['rate'])); ?></td>
                                                <td style="text-align: center"><?php echo e(eng2bang($value['quantity']) . $unit); ?></td>
                                                <td style="text-align: right"><?php echo e(eng2bang(round($value['total_amount'], 2))); ?></td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>

                                            <?php
                                            if ($invoice['vat_type'] == 0)
                                            {
                                                $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $vat_amount  = $invoice['total_vat'];
                                            }

                                            if ($invoice['total_discount_type'] == 0)
                                            {
                                                $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $discount_on_total_amount  = $invoice['total_discount_amount'];
                                            }
                                            ?>

                                            <tr>
                                                <th class="border-none"></th>
                                                <th class="border-none-2" style="width: 115px!important;"></th>
                                                <th style="text-align: left;text-align: right"><strong style="color: #556ee6;">মোট</strong></th>
                                                <th colspan="3" style="text-align: right;font-weight: bold; color: #556ee6;"><?php echo e($net_paya != 0 ? eng2bang(round($net_paya - $invoice['total_discount'])) : ''); ?></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none"></th>
                                                <th class="border-none-2"></th>
                                                <th style="text-align: right"><strong>পূর্বের জের</strong></th>
                                                <th colspan="3" style="text-align: right"><?php echo e($pre_dues != 0 ? eng2bang(round($pre_dues)) : ''); ?></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none"></th>
                                                <th class="border-none-2"></th>
                                                <th style="text-align: right"><strong style="color: red;">সর্বমোট </strong></th>
                                                <th colspan="3" style="text-align: right;font-weight: bold; color: red;"><?php echo e($net_paya - $invoice['total_discount'] + $pre_dues != 0 ? eng2bang(round($net_paya - $invoice['total_discount'] + $pre_dues)) : ''); ?></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none"></th>
                                                <th class="border-none-2"></th>
                                                <th style="text-align: right"><strong>জমা</strong></th>
                                                <th colspan="3" style="text-align: right"></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none"></th>
                                                <th class="border-none-2"></th>
                                                <th style="text-align: right"><strong>জের</strong></th>
                                                <th colspan="3" style="text-align: right"></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none"></th>
                                                <th class="border-none-2"></th>
                                                <th style="text-align: right"><strong>অন্যান্য জমা</strong></th>
                                                <th colspan="3" style="text-align: right"></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none"></th>
                                                <th class="border-none-2"></th>
                                                <th style="text-align: right"><strong>বর্তমান জের</strong></th>
                                                <th colspan="3" style="text-align: right"></th>
                                            </tr>
                                        </table>
                                    </div>

                                    <br>

                                    <div class="row" style="padding-top:50px!important">
                                        <div class="col-md-6">
                                            <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px;font-size: 18px">ক্রেতার স্বাক্ষর </span> </h6>
                                        </div>
                                        <div class="col-md-6">
                                            <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px;font-size: 18px">বিক্রেতার স্বাক্ষর</span> </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    $( document ).ready(function() {
        javascript:window.print();
    });

    window.onafterprint = function(e){
        var site_url  = $('.site_url').val();
        window.location.replace(site_url + '/invoices/all-sales');
    };
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/70/Modules/Invoices/Resources/views/show_pos.blade.php ENDPATH**/ ?>