

<?php $__env->startSection('title', 'Accounts'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.paid_through_account')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.setting')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.paid_through_account')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

            					<form id="FormSubmit" action="<?php echo e(route('paid_through_accounts_store')); ?>" method="post" files="true" enctype="multipart/form-data">
            					<?php echo e(csrf_field()); ?>


                                <div style="margin-bottom: 0px" class="form-group row">
                                    <div class="col-md-6 form-group">
                                        <label for="productname"><?php echo e(__('messages.name')); ?> *</label>
                                        <input type="text" name="name" class="inner form-control" id="name" placeholder="Enter Name" required />
                                    </div>

                                    <div class="col-md-6 form-group">
                                        <label for="productname"><?php echo e(__('messages.description')); ?> </label>
                                        <input type="text" name="description" class="inner form-control" id="description" placeholder="<?php echo e(__('messages.description')); ?>" />
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                    	<button type="submit" class="btn btn-primary waves-effect waves-light"><?php echo e(__('messages.save')); ?></button>
                                    	<button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('paid_through_accounts_index')); ?>"><?php echo e(__('messages.close')); ?></a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<h4 class="card-title"><?php echo e(__('messages.all_accounts')); ?></h4>

                                <br>

                                <table id="datatable" class="table table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th><?php echo e(__('messages.sl')); ?></th>
                                            <th><?php echo e(__('messages.name')); ?></th>
                                            <th><?php echo e(__('messages.description')); ?></th>
                                            <th><?php echo e(__('messages.action')); ?></th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                    	<?php if(!empty($accounts) && ($accounts->count() > 0)): ?>
                                    	<?php $__currentLoopData = $accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                                        <tr>
	                                            <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e($account['name']); ?></td>
                                                <td><?php echo e($account['description']); ?></td>
	                                            <td>
                                                    <?php if(Auth::user()->role == 1): ?>
	                                            	<div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="<?php echo e(route('paid_through_accounts_edit', $account['id'])); ?>"><?php echo e(__('messages.edit')); ?></a>
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
	                                            </td>
	                                        </tr>
	                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                                    <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/70/Modules/PaidThroughAccounts/Resources/views/index.blade.php ENDPATH**/ ?>