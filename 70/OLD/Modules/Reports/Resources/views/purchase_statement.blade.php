@extends('layouts.app')

@section('title', 'Statement of Purchase')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.purchase_statement')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.reports')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.purchase_statement')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="margin-left: 150px;margin-right: 150px" class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <form method="get" action="{{ route('purchase_statement_print') }}" enctype="multipart/form-data" target="_blank">
	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"> {{ __('messages.from_date')}} </label>
	                                    <div class="col-md-9">
	                                    	<div class="row">
	                                    		<div class="input-group-append col-md-5">
	                                    			<span style="border-radius: 0px" class="input-group-text"><i class="mdi mdi-calendar"></i></span>
		                                    		<input style="border-radius: 0px;height: 42px;" type="text" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" value="{{ date('d-m-Y') }}" name="from_date">
		                                    	</div>

		                                    	<div class="input-group-append col-md-7">
		                                    		<label style="text-align: left;" for="productname" class="col-md-5 col-form-label"> {{ __('messages.to')}} </label>
		                                    		<span style="border-radius: 0px" class="input-group-text"><i class="mdi mdi-calendar"></i></span>
		                                    		<input style="border-radius: 0px;height: 42px;" type="text" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" value="{{ date('d-m-Y') }}" name="to_date">
		                                    	</div>
	                                    	</div>
	                                    </div>
	                                </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label">{{ __('messages.supplier_company')}} </label>
	                                    <div class="col-md-9">
	                                        <select style="width: 100%" id="supplier_id" name="supplier_id" class="form-control select2">
	                                           <option value="0">{{ __('messages.all')}}</option>
	                                        </select>
	                                    </div>
	                                </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label">{{ __('messages.purchase_id')}} </label>
	                                    <div class="col-md-9">
	                                        <select style="width: 100%" id="bill_number" name="bill_number" class="form-control select2">
	                                           <option value="0">{{ __('messages.all')}}</option>
	                                        </select>
	                                    </div>
	                                </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label">{{ __('messages.category_name')}} </label>
	                                    <div class="col-md-9">
	                                        <select style="width: 100%" id="category_id" name="category_id" class="form-control select2">
	                                           <option value="0">{{ __('messages.all')}}</option>
	                                        </select>
	                                    </div>
	                                </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"> {{ __('messages.item_name')}} </label>
	                                    <div class="col-md-9">
	                                    	<div class="row">
	                                    		<div class="input-group-append col-md-6">
	                                    			<select style="width: 100%" id="product_id" name="product_id" class="form-control select2">
			                                           <option value="0">{{ __('messages.all')}}</option>
			                                        </select>
		                                    	</div>

		                                    	<div class="input-group-append col-md-6">
		                                    		<label style="text-align: right" for="productname" class="col-md-3 col-form-label"> {{ __('messages.code')}} </label>
		                                    		<select style="width: 100%" id="product_code" name="product_code" class="form-control select2">
			                                           <option value="0">{{ __('messages.all')}}</option>
			                                        </select>
		                                    	</div>
	                                    	</div>
	                                    </div>
	                                </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label">{{ __('messages.purchase_by')}} </label>
	                                    <div class="col-md-9">
	                                        <select style="width: 100%" id="user_id" name="user_id" class="form-control select2">
	                                           <option value="0">{{ __('messages.all')}}</option>
	                                        </select>
	                                    </div>
	                                </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label">{{ __('messages.purchase_return')}} </label>
	                                    <div class="col-md-9">
	                                        <select style="width: 100%;cursor: pointer" id="sales_return" name="purchase_return" class="form-control">
	                                           <option value="0">{{ __('messages.select_option')}}</option>
	                                           <option value="1">{{ __('messages.purchase')}}</option>
	                                           <option value="2">{{ __('messages.return')}}</option>
	                                        </select>
	                                    </div>
	                                </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"></label>
	                                    <div class="col-md-9">
	                                        <button style="border-radius: 0px;background-color: #297049;color: white" class="btn" type="submit">
	                                        	{{ __('messages.print')}}
	                                    	</button>
	                                    </div>
	                                </div>
                            	</form>
                                
                            </div>

                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#supplier_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 1 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#bill_number").select2({
                ajax: { 
                url:  site_url + '/reports/purchase-statement/bill-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $("#user_id").select2({
                ajax: { 
                url:  site_url + '/reports/sales-statement/users-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $("#category_id").select2({
                ajax: { 
                url:  site_url + '/reports/sales-statement/category-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $("#product_id").select2({
                ajax: { 
                url:  site_url + '/reports/sales-statement/product-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $("#product_code").select2({
                ajax: { 
                url:  site_url + '/reports/sales-statement/product-code-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });

        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>
@endsection