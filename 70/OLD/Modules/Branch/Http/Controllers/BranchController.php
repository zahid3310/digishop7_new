<?php

namespace Modules\Branch\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\ProductEntries;
use App\Models\Branches;
use App\Models\ProductBranches;
use DB;
use Response;

class BranchController extends Controller
{
    public function index()
    {
        $branches  = Branches::orderBy('id', 'ASC')->get();

        return view('branch::index', compact('branches'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'branch_name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $branch                    = new Branches;
            $branch->name              = $data['branch_name'];
            $branch->description       = $data['description'];
            $branch->created_by        = $user_id;
            
            if ($branch->save())
            {
                return redirect()->route('branch_index')->with("success","Branch Created Successfully !!");
            }

        }catch (\Exception $exception){
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function edit($id)
    {
        $branches       = Branches::orderBy('id', 'ASC')->get();
        $find_branch    = Branches::find($id);

        return view('branch::edit', compact('branches', 'find_branch'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'branch_name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $branch                    = Branches::find($id);
            $branch->name              = $data['branch_name'];
            $branch->description       = $data['description'];
            $branch->updated_by        = $user_id;
            
            if ($branch->save())
            {
                return redirect()->route('branch_index')->with("success","Branch Updated Successfully !!");
            }

        }catch (\Exception $exception){
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function assignBranch()
    {
        $assign_branches    = ProductEntries::selectRaw("product_entries.id, product_entries.name")->with('productBranches')->get();
        $branches           = Branches::selectRaw("branches.id, branches.name")->get();

        return view('branch::assign_branch.edit', compact('assign_branches', 'branches'));
    }

    public function assignBranchUpdate(Request $request)
    {
        $type       = "success";
        $message    = "Branch Added Successfully Updated !!";
        $data       = $request->all();

        DB::beginTransaction();

        try{
            foreach ($data['product_entry_id'] as $key => $value)
            {   
                $delete_entry  = ProductBranches::where('product_entry_id', $value)->delete();

                if (isset($data['branch_id'][$value]))
                {
                    foreach ($data['branch_id'][$value] as $key1 => $value1)
                    {
                        $branch_product                      = new ProductBranches;
                        $branch_product->product_entry_id    = $value;
                        $branch_product->branch_id           = $value1;
                        $branch_product->created_by          = Auth()->user()->id;
                        $branch_product->save();
                    }
                }
            }

            DB::commit();
            return back()->with($type, $message);
            
        }catch (\Exception $exception) {
            DB::rollback();
            dd($exception);
            $type       = "unsuccess";
            $message    = "Something Went Wrong.Please Try Again.!";
            return back()->with($type, $message);
        }
    }

    public function getBranchList()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Branches::select('branches.*')->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];  
            $fetchData  = Branches::where('branches.name', 'LIKE', "%$search%")->select('branches.*')->get(); 
        }

        $data = array();
        foreach ($fetchData as $key => $value)
        {   
            $data[] = array("id"=>$value['id'], "text"=>$value['name']);
        }
   
        return Response::json($data);
    }
}
