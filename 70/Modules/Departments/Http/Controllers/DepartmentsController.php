<?php

namespace Modules\Departments\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Departments;
use App\Models\Sections;
use DB;
use Response;

class DepartmentsController extends Controller
{
    public function index()
    {
        $departments  = Departments::orderBy('id', 'ASC')->get();

        return view('departments::index', compact('departments'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $department                    = new Departments;
            $department->name              = $data['name'];
            $department->description       = $data['description'];
            $department->created_by        = $user_id;
            
            if ($department->save())
            {
                return redirect()->route('department_index')->with("success","Department Created Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function edit($id)
    {
        $departments        = Departments::orderBy('id', 'ASC')->get();
        $find_department    = Departments::find($id);

        return view('departments::edit', compact('departments', 'find_department'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $department                    = Departments::find($id);
            $department->name              = $data['name'];
            $department->description       = $data['description'];
            $department->updated_by        = $user_id;
            
            if ($department->save())
            {
                return redirect()->route('department_index')->with("success","Department Updated Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }
    
    //Section
    public function sectionIndex()
    {
        $sections       = Sections::orderBy('id', 'ASC')->get();
        $departments    = Departments::orderBy('id', 'ASC')->get();

        return view('departments::section.index', compact('sections', 'departments'));
    }

    public function sectionStore(Request $request)
    {
        $rules = array(
            'name'          => 'required',
            'department_id' => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $department                     = new Sections;
            $department->name               = $data['name'];
            $department->department_id      = $data['department_id'];
            $department->created_by         = $user_id;
            
            if ($department->save())
            {
                return redirect()->route('section_index')->with("success","Section Created Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function sectionEdit($id)
    {
        $sections       = Sections::orderBy('id', 'ASC')->get();
        $find_section   = Sections::find($id);
        $departments    = Departments::orderBy('id', 'ASC')->get();

        return view('departments::section.edit', compact('sections', 'find_section', 'departments'));
    }

    public function sectionUpdate(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required',
            'department_id' => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $department                     = Sections::find($id);
            $department->name               = $data['name'];
            $department->department_id      = $data['department_id'];
            $department->updated_by         = $user_id;
            
            if ($department->save())
            {
                return redirect()->route('section_index')->with("success","Section Updated Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }
}
