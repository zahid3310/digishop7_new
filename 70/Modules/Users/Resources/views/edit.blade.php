@extends('layouts.app')

@section('title', 'Edit Users')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.edit')}} {{ __('messages.user')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.setting')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.edit')}} {{ __('messages.user')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('users_update',$user_find['id']) }}" method="post" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">{{ __('messages.name')}} *</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user_find['name'] }}" name="name" id="name" placeholder="{{ __('messages.name')}}" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">{{ __('messages.username')}} *</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user_find['email'] }}" name="user_name" id="user_name" readonly>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">{{ __('messages.password')}}</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="" name="password" id="password" placeholder="{{ __('messages.password')}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">{{ __('messages.role')}}</label>
                                    <div class="col-md-10">
                                        <select style="cursor: pointer" class="form-control" name="role">
                                            @if(Auth::user()->role == 1 || Auth::user()->id == 1)
                                            <option {{ $user_find['role'] == 1 ? 'selected' : '' }} value="1">Super Admin</option>
                                            @endif
                                            <option {{ $user_find['role'] == 2 ? 'selected' : '' }} value="2">Admin</option>
                                            <option {{ $user_find['role'] == 3 ? 'selected' : '' }} value="3">Employee</option>
                                        </select>
                                    </div>
                                </div>

                                @if(Auth::user()->role == 1)
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">{{ __('messages.branch')}}</label>
                                    <div class="col-md-10">
                                        <select style="cursor: pointer" id="branch_id" class="form-control" name="branch_id">
                                            @if($branches->count() > 0)
                                            @foreach($branches as $key => $value)
                                            <option value="{{ $value['id'] }}" {{ $user_find['branch_id'] == $value['id'] ? 'selected' : '' }}>{{ $value['name'] }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @endif
                                
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">{{ __('messages.status')}}</label>
                                    <div class="col-md-10">
                                        <select style="cursor: pointer" class="form-control" name="status">
                                            <option {{ $user_find['status'] == 1 ? 'selected' : '' }} value="1">{{ __('messages.active')}}</option>
                                            <option {{ $user_find['status'] == 0 ? 'selected' : '' }} value="0">{{ __('messages.inactive')}}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                    	<button type="submit" class="btn btn-primary waves-effect waves-light">{{ __('messages.update')}}</button>
                                    	<button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('users_index') }}">{{ __('messages.close')}}</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection