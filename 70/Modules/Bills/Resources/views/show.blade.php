@extends('layouts.app')

@section('title', 'Show')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.purchase')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.purchase')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.show')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">{{ $user_info['organization_name'] }}</h2>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_number'] }}</p>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-4 col-6">
                                        <address>
                                            <strong>{{ __('messages.payable_to')}}:</strong><br>
                                            {{ $bill['customer_name'] }}
                                            @if($bill['address'] != null)
                                               <br> <?php echo $bill['address']; ?> <br>
                                            @endif
                                            @if($bill['address'] == null)
                                                <br>
                                            @endif
                                            {{ $bill['phone'] }}
                                        </address>
                                    </div>

                                    <div class="col-sm-4 hidden-xs">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div style="font-size: 16px" class="col-sm-4 col-6 text-sm-right">
                                        <address>
                                            <strong>{{ __('messages.purchase_date')}}:</strong><br>
                                            {{ date('d-m-Y', strtotime($bill['bill_date'])) }}<br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6 hidden-xs">
                                            <h3 style="font-size: 16px" class="font-weight-bold">{{ __('messages.purchase_summary')}}</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">{{ __('messages.purchase')}} # {{ 'BILL - ' . str_pad($bill['bill_number'], 6, "0", STR_PAD_LEFT) }}</h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-nowrap">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th style="width: 70px;">{{ __('messages.no')}}.</th>
                                                <th>{{ __('messages.items')}}</th>
                                                <th class="text-right">{{ __('messages.rate')}}</th>
                                                <th class="text-right">{{ __('messages.qty')}}</th>
                                                <th class="text-center">{{ __('messages.unit')}}</th>
                                                <th class="text-right">{{ __('messages.discount')}}</th>
                                                <th class="text-right">{{ __('messages.price')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">

                                            @if(!empty($entries) && ($entries->count() > 0))

                                            <?php $sub_total = 0; ?>

                                            @foreach($entries as $key => $value)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>
                                                    @if($value['product_variations_id'] == null)
                                                        <?php echo $value['product_entry_name']; ?>
                                                    @else
                                                        <?php echo $value['product_variations_name']; ?>
                                                    @endif
                                                </td>
                                                <td class="text-right">{{ number_format($value['rate'],2,'.',',') }}</td>
                                                <td class="text-right">{{ number_format($value['quantity'],2,'.',',') . ' ' .$value['unit_name'] }}</td>
                                                <td class="text-center">{{ $value->convertedUnit->name }}</td>
                                                <td class="text-right">
                                                    <?php $total_dis = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; ?>
                                                    @if($value['discount_type'] == 0)
                                                        <?php echo number_format($total_dis,2,'.',','); ?><br>
                                                        <?php echo '('.$value['discount_amount'].'%'.')'; ?>
                                                    @else
                                                        <?php echo number_format($value['discount_amount'],2,'.',','); ?>
                                                    @endif
                                                </td>
                                                <td class="text-right">{{ number_format($value['total_amount'],2,'.',',') }}</td>
                                            </tr>

                                            <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                            @endforeach
                                            @endif
                                            
                                            <tr>
                                                <td style="font-size: 16px" colspan="6" class="text-right">Sub Total</td>
                                                <td style="font-size: 16px" class="text-right">{{ number_format($sub_total,2,'.',',') }}</td>
                                            </tr>

                                            <tr>
                                                <td style="font-size: 16px" colspan="6" class="text-right">VAT ({{ $bill['vat_type'] == 0 ? $bill['total_vat'].'%' : 'BDT' }})</td>
                                                <td style="font-size: 16px" class="text-right">{{ $bill['vat_type'] == 0 ? (($sub_total*$bill['total_vat'])/100) : number_format($bill['total_vat'],2,'.',',') }}</td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px" colspan="6" class="border-0 text-right">
                                                    Discount On Total Amount ({{ $bill['total_discount_type'] == 0 ? $bill['total_discount_amount'].'%' : 'BDT' }})</td>
                                                <td style="font-size: 16px" class="border-0 text-right">{{ $bill['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$bill['total_vat'])/100))*$bill['total_discount_amount'])/100) : number_format($bill['total_discount_amount'],2,'.',',') }}</td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="6" class="border-0 text-right">Total</td>
                                                <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                    {{ number_format($bill['bill_amount'],2,'.',',') }}
                                                </td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px" colspan="6" class="border-0 text-right">Paid</td>
                                                <td style="font-size: 16px" class="border-0 text-right">
                                                    {{ number_format($bill['cash_given'],2,'.',',') }}
                                                </td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px" colspan="6" class="border-0 text-right">Dues</td>
                                                <td style="font-size: 16px" class="border-0 text-right">
                                                    {{ number_format($bill['bill_amount'] - $bill['cash_given'],2,'.',',') }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                @if($bill['bill_note'] != null)
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 style="font-size: 16px"><strong>Note :</strong> {{ $bill['bill_note'] }}</h6>
                                    </div>
                                </div>
                                @endif

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="position: fixed;bottom: 0">
                                    <!-- <h4 class="float-right font-size-16">Order # 12345</h4> -->
                                    <!-- <div class="col-md-4">
                                        <img class="float-left" src="{{ url('public/az-ai.png') }}" alt="logo" height="20"/>
                                    </div>

                                    <div class="col-md-4">
                                        <h2 style="text-align: center">{{ $user_info['organization_name'] }}</h2>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['address'] }}</p>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['contact_number'] }}</p>
                                    </div>

                                    <div class="col-md-4">
                                        <p style="line-height: 18px;text-align: right;padding: 0px">Phone - 01718937082<br>01711418731<br>01711418731</p>
                                    </div> -->
                                    @if($user_info['footer_image'] != null)
                                        <img class="float-left" src="{{ url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection