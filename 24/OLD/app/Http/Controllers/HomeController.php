<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\SalesReturn;
use App\Models\SalesReturnEntries;
use App\Models\PurchaseReturn;
use App\Models\PurchaseReturnEntries;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\Transactions;
use App\Models\Expenses;
use App\Models\ExpenseEntries;
use App\Models\Users;
use App\Models\PaidThroughAccounts;
use DB;
use Auth;
use Response;
use Carbon\Carbon;
use Artisan;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth()->user()->status == 1)
        {   
            $invoices           = Invoices::orderBy('id', 'DESC')->first();
            $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                            ->get();
            $products           = Products::select('id', 'name')->orderBy('name', 'ASC')->get();
            $product_entries    = ProductEntries::where('product_entries.maintain_stock', 0)
                                            ->orWhere(function($query)
                                            {
                                                $query->where('product_entries.maintain_stock', 1)
                                                      ->where('product_entries.stock_in_hand', '>', 0);

                                            })
                                            ->select('id', 'name', 'product_id', 'image')
                                            ->orderBy('name', 'ASC')
                                            ->get();

            return view('invoices::quick_sales', compact('paid_accounts', 'invoices', 'products', 'product_entries'));
        }
        else
        {
            return back();
        }
    }

    public function dashboard()
    {
        if (Auth()->user()->status == 1)
        {   
            return view('home');
        }
        else
        {
            return back();
        }
    }

    public function dashboardItems()
    {
        $products                   = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                                    ->where('product_entries.product_id', '!=', 1)
                                                    ->groupBy('product_entries.id')
                                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                                ')
                                                    ->orderBy('product_entries.total_sold', 'DESC')
                                                    ->get();

        $sells                      = Invoices::get();
        $sells_return               = SalesReturn::get();
        $purchases                  = Bills::get();
        $purchases_return           = PurchaseReturn::get();
        $expenses                   = Expenses::get();
        $customers                  = Customers::get();

        $top_products               = $products->take(5);
        $total_products             = $products->count();
        $total_customers            = $customers->where('contact_type', 0)->count();
        $total_suppliers            = $customers->where('contact_type', 1)->count();
        $todays_sells               = $sells->where('invoice_date', date('Y-m-d'))->sum('invoice_amount');
        $todays_sells_return        = $sells_return->where('sales_return_date', date('Y-m-d'))->sum('return_amount');
        $todays_expense             = $expenses->where('expense_date', date('Y-m-d'))->sum('amount');

        $sales_summary              = Invoices::whereYear('invoice_date', date('Y'))
                                                ->select(DB::raw('MONTH(invoice_date) month'),
                                                       DB::raw('SUM(invoice_amount) total'))
                                                ->groupBy(DB::raw('MONTH(invoice_date)'))
                                                ->get();

        $purchase_summary           = Bills::whereYear('bill_date', date('Y'))
                                                ->select(DB::raw('MONTH(bill_date) month'),
                                                       DB::raw('SUM(bill_amount) total'))
                                                ->groupBy(DB::raw('MONTH(bill_date)'))
                                                ->get();

        // $expense_summary            = Expenses::whereYear('expense_date', date('Y'))
        //                                         ->select(DB::raw('MONTH(expense_date) month'),
        //                                                DB::raw('SUM(amount) total'))
        //                                         ->groupBy(DB::raw('MONTH(expense_date)'))
        //                                         ->get();

        $top_product_data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                                ->whereRaw('year(`invoice_date`) = ?', array(date('Y')))
                                                ->select(DB::raw('DISTINCT(invoice_entries.product_entry_id) id'),
                                                         DB::raw('GROUP_CONCAT(DISTINCT product_entries.name) name'),
                                                         DB::raw('SUM(quantity) total_sold'))
                                                ->groupBy('product_entry_id')
                                                ->get();

        //Pie Chart is not working without 5 array index . This code make the array length 5.
        for($i=0; $i<5; $i++)
        {
            $dummy_array[$i]['id']              = 0;
            $dummy_array[$i]['product_code']    = '';
            $dummy_array[$i]['name']            = '';
            $dummy_array[$i]['stock_in_hand']   = 0;
            $dummy_array[$i]['alert_quantity']  = 0;
            $dummy_array[$i]['total_sold']      = 0;
            $dummy_array[$i]['variations']      = '';
        }

        $top_product_data_5                     = array_merge($top_products->toArray(), $dummy_array);
        //End of making array

        //Cash Flow is not working without 5 array index . This code make the array length 12.
        // for($i=0; $i<12; $i++)
        // {
        //     $dummy_array_cash_flow[$i]['id']         = 0;
        //     $dummy_array_cash_flow[$i]['month']      = '';
        //     $dummy_array_cash_flow[$i]['total']      = 0;
        // }

        // $sales_cash_flow       = array_merge($sales_summary->toArray(), $dummy_array_cash_flow);
        // $expense_cash_flow     = array_merge($expense_summary->toArray(), $dummy_array_cash_flow);
        //End of making array

        $data['sales_summary']              = $sales_summary;
        $data['purchase_summary']           = $purchase_summary;
        // $data['sales_summary_cash_flow']    = collect($sales_cash_flow)->take(12);
        // $data['expense_summary_cash_flow']  = collect($expense_cash_flow)->take(12);
        $data['products']                   = $products;
        $data['top_products']               = collect($top_product_data_5)->take(5);
        $data['top_products_total_sold']    = $products->sum('total_sold');
        $data['total_customers']            = $total_customers;
        $data['total_suppliers']            = $total_suppliers;
        $data['total_products']             = $total_products;
        $data['todays_sells']               = $todays_sells;
        $data['todays_sells_return']        = $todays_sells_return;
        $data['todays_expenses']            = $todays_expense;
        $data['due_invoices']               = $sells->where('due_amount', '>', 0);
        $data['total_invoices']             = $sells->count();
        $data['total_bills']                = $purchases->count();
        $data['total_sells']                = $sells->sum('invoice_amount');
        $data['total_purchase']             = $purchases->sum('bill_amount');
        $data['customer_dues']              = $sells->sum('due_amount');
        $data['supplier_dues']              = $purchases->sum('due_amount');

        return Response::json($data);
    }

    public function stockOutItems()
    {
        $products   = ProductEntries::where('product_entries.product_id', '!=', 1)
                                ->whereRaw('product_entries.stock_in_hand <= product_entries.alert_quantity')
                                ->count();

        return Response::json($products);
    }
}
