

<?php
    $title = 'Installment Collection';
?>

<?php $__env->startPush('scripts'); ?>
<style type="text/css">
    body[data-sidebar=dark].vertical-collpsed {
        min-height: 0px !important;
    }
    
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('title', $title); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.payment')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.payment')); ?></a></li>
                                    <li class="breadcrumb-item active">Installment Collection</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <form id="FormSubmit" action="<?php echo e(route('installment_collection_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                <?php echo e(csrf_field()); ?>


                <div class="row">
                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
                        <div style="height: 271px" class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group ajax-select mt-3 mt-lg-0">
                                            <label class="control-label"><?php echo e(__('messages.payment_type')); ?> *</label>
                                            <select style="width: 100%;cursor: pointer" class="form-control">
                                                <option selected>Installment Collection</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                    	<div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
	                                        <label class="control-label"><?php echo e(__('messages.search_contact')); ?> *</label>
	                                        <select name="customer_id" id="contact_id" style="width: 100%;cursor: pointer" class="form-control select2 contact_id" onchange="searchContact()">
	                                        	<option value="">--<?php echo e(__('messages.select_contact')); ?>--</option>
												<?php if(!empty($customers)): ?>
													<?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<option <?php if(isset($find_customer)): ?> <?php echo e($find_customer['id'] == $customer['id'] ? 'selected' : ''); ?> <?php endif; ?> value="<?php echo e($customer->id); ?>"><?php echo e($customer->name); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												<?php endif; ?>
	                                        </select>
	                                    </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group ajax-select mt-3 mt-lg-0">
                                            <label class="control-label">Select Invoice *</label>
                                            <select name="invoice_id" id="invoice_id" style="width: 100%;cursor: pointer" class="form-control select2" onchange="getInstallments()">
                                                <option value="">--Select Invocie--</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

        						<div class="row">
				                	<div class="col-sm-4">
				                        <div class="form-group">
				                            <label for="payment_date"><?php echo e(__('messages.payment_date')); ?> *</label>
				                            <input id="payment_date" name="payment_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
				                        </div>
				                    </div>

				                    <div class="col-sm-4">
				                        <div class="form-group">
				                            <label for="amount"><?php echo e(__('messages.amount')); ?> *</label>
				                            <input id="amount" name="amount" type="text" class="form-control">
				                        </div>
				                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="discount_amount">Discount</label>
                                            <input id="discount_amount" name="discount_amount" type="text" class="form-control">
                                        </div>
                                    </div>

				                    <div class="col-sm-6 form-group">
			                            <label class="control-label"><?php echo e(__('messages.paid_through')); ?></label>
                                        <select style="cursor: pointer" name="paid_through" class="form-control select2">
                                            <?php if(!empty($paid_accounts) && ($paid_accounts->count() > 0)): ?>
                                            <?php $__currentLoopData = $paid_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $paid_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($paid_account['id']); ?>"><?php echo e($paid_account['account_name']); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
			                        </div>

			                        <div class="col-sm-6">
				                        <div class="form-group">
				                            <label for="note"><?php echo e(__('messages.note')); ?></label>
				                            <input id="note" name="note" type="text" class="form-control">
				                        </div>
				                    </div>

                                    <div id="type0" style="display: none" class="table-responsive">
                                        <table class="table table-centered table-nowrap">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Installment#</th>
                                                    <th>Date</th>
                                                    <th>Receivable</th>
                                                    <th>Received</th>
                                                    <th>Dues</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody id="installment_list">
                                                
                                            </tbody>
                                        </table>
                                    </div>
			                	</div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()"><?php echo e(__('messages.make_payment')); ?></button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('payments_create')); ?>"><?php echo e(__('messages.close')); ?></a></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
                                <div style="height: 100px" class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <div class="card-body">
                                        
                                        <div class="media">
                                            <div class="media-body">
                                                <h4 style="font-size: 14px" id="customerName" class="mb-0"></h4>
                                                <p style="font-size: 14px" id="customerAddress" class="text-muted font-weight-medium"></p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
                                <div class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body">
                                                <p id="receivablePShow" class="text-muted font-weight-medium"><?php echo e(__('messages.total_receivables')); ?></p>
                                                <h4 id="totalReceivable" class="mb-0">0.00</h4>
                                            </div>

                                            <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                                <span class="avatar-title">
                                                    <i class="bx bx-credit-card font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
                                <div class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <div class="card-body">
                                        <div class="media">
                                            <div class="media-body">
                                                <p id="receivedPShow" class="text-muted font-weight-medium"><?php echo e(__('messages.total_received')); ?></p>
                                                <h4 id="totalReceived" class="mb-0">0.00</h4>
                                            </div>

                                            <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                                <span class="avatar-title">
                                                    <i class="bx bx-credit-card font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
                                <div class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <div class="card-body">
                                        
                                        <div class="media">
                                            <div class="media-body">
                                                <p class="text-muted font-weight-medium" id="label"><?php echo e(__('messages.balance')); ?></p>
                                                <h4 id="totalDues" class="mb-0">0.00</h4>
                                            </div>

                                            <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                                <span class="avatar-title">
                                                    <i class="bx bx-credit-card font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </form>

                <hr style="margin-top: 0px">

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">All Installment Payments</h4>

                                <br> 

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;text-align: center">SL</th>
                                            <th style="width: 10%;text-align: center">Date</th>
                                            <th style="width: 10%;text-align: center">Payment#</th>
                                            <th style="width: 10%;text-align: center">Invoice#</th>
                                            <th style="width: 20%;text-align: center">Customer</th>
                                            <th style="width: 10%;text-align: center">Methode</th>
                                            <th style="width: 15%;text-align: center">Note</th>
                                            <th style="width: 10%;text-align: center">Amount</th>
                                            <th style="width: 10%;text-align: center">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php if(!empty($payments) && ($payments->count() > 0)): ?>
                                        <?php $__currentLoopData = $payments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td style="text-align: center"><?php echo e($key + 1); ?></td>
                                                <td style="text-align: center"><?php echo e(date('d-m-Y', strtotime($payment['payment_date']))); ?></td>
                                                <td style="text-align: center"><?php echo e('PM - ' . str_pad($payment['payment_number'], 6, "0", STR_PAD_LEFT)); ?></td>
                                                <td style="text-align: center"><?php echo e('INV - ' . str_pad($payment->invoice->invoice_number, 6, "0", STR_PAD_LEFT)); ?></td>
                                                <td style="text-align: left"><?php echo e($payment['customer_id'] != null ? $payment->customer->name : ''); ?></td>
                                                <td style="text-align: center"><?php echo e($payment['paid_through'] != null ? $payment->paidThrough->account_name : ''); ?></td>
                                                <td style="text-align: left"><?php echo e($payment['note'] != null ? $payment->note : ''); ?></td>
                                                <td style="text-align: center"><?php echo e($payment->amount); ?></td>
                                                <td style="text-align: center">
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <!-- <a class="dropdown-item" href="<?php echo e(route('installment_collection_print', $payment['id'])); ?>">Show</a> -->
                                                            <!-- <a class="dropdown-item" href="<?php echo e(route('installment_collection_edit', $payment['id'])); ?>">Edit</a> -->
                                                            <!-- <a class="dropdown-item" href="<?php echo e(route('installment_collection_delete', $payment['id'])); ?>" onclick="return confirm('Are you sure want to delete?');">Delete</a> -->
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> 
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
        });

        function searchContact()
        {
            var contact_id  = $('#contact_id').val();
            var site_url    = $('.site_url').val();

            $("#invoice_id").select2({
                ajax: { 
                url:  site_url + '/installmentcollection/invoice-list-inst/' + contact_id,
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';
                    return result['text'];
                },
            });

            $('#receivablePShow').show();
            $('#receivedPShow').show();

            $.get(site_url + '/payments/contact/list/' + contact_id, function(data){

                if (data.phone != null)
                {
                    var phone = data.phone;
                }
                else
                {
                    var phone = '';
                }

                if (data.address != null)
                {
                    var address = data.address  + "<br>" + phone;
                }
                else
                {
                    var address = '';
                }

                if (data.receivable != null)
                {
                    var receivable = data.receivable;
                }
                else
                {
                    var receivable = 0;
                }

                if (data.received != null)
                {
                    var received = data.received;
                }
                else
                {
                    var received = 0;
                }

                var dues = parseFloat(data.dues);
                
                if (dues > 0) 
                {
                  $("#label").html('Due');  
                }else if(dues < 0)
                {
                  $("#label").html('Advance');
                }else
                {
                  $("#label").html('Balance');
                }

                $("#customerName").empty();
                $("#customerName").html(data.name);
                $("#customerAddress").empty();
                $("#customerAddress").html(address);
                $("#totalReceivable").empty();
                $("#totalReceivable").html(receivable);
                $("#totalReceived").empty();
                $("#totalReceived").html(parseFloat(received));
                $("#totalDues").empty();
                $("#totalDues").html(Math.abs(dues));    
            });
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function getInstallments()
        {
            $('#type0').show();

            var site_url  = $('.site_url').val();
            var invoiceId = $('#invoice_id').val();
            $.get(site_url + '/installmentcollection/installments-list-inst/' + invoiceId, function(data){

                var installment_list = '';
                $.each(data, function(i, data_list)
                {   
                    if (parseFloat(data_list.amount) > parseFloat(data_list.paid))
                    {
                        installment_list += '<tr>' +
                                        '<input class="form-control" type="hidden" name="installment_id[]" value="' +  data_list.id + '">' +
                                        '<td style="text-align: left">' +
                                           data_list.installment_number +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            formatDate(data_list.date) +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           (data_list.amount) +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           parseFloat(data_list.paid) +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           (parseFloat(data_list.amount) - parseFloat(data_list.paid)) +
                                        '</td>' +

                                        '<td style="text-align: left">' +
                                            '<input style="width: 150px" class="form-control" type="text" name="paid[]" value="">' +
                                        '</td>' +
                                    '</tr>';
                    }
                });

                $("#installment_list").empty();
                $("#installment_list").append(installment_list); 
            });
        }
    </script>

    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/61_1/Modules/InstallmentCollection/Resources/views/create.blade.php ENDPATH**/ ?>