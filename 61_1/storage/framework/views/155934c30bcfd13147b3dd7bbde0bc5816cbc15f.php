<!DOCTYPE html>
<html>

<head>
    <title>Summary Report</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">
</head>

<style type="text/css" media="print">        
    @page  {
        size: auto;   
        margin: 15mm 5mm 5mm 10mm;
    }

    #print-footer {
        display: none;
        position: fixed;
        bottom: 0;
        left:0;
    }

    #hide-print {
        display: none;
    }

    #print-footer {
        display: none;
    }

    body {
        margin-bottom:10mm;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #ddd;
    }
    
    fontSizeC {
        font-size: 10px !important;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p><?php echo e($user_info['address']); ?></p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Daily Report</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date : <?php echo e(date('d-m-Y', strtotime($from_date))); ?></th>
                                </tr>
                            </thead>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="font-size: 10px;text-align: center;font-size: 15px" colspan="3">Income/Receive</th>

                                    <th style="font-size: 10px;text-align: center;font-size: 15px" colspan="4">Expense/Paid</th>
                                </tr>

                                <tr>
                                    <th style="font-size: 10px;text-align: center;width: 5%">SL</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">Description</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">Note</th>
                                    <th style="font-size: 10px;text-align: center;width: 5%">Total</th>


                                    <th style="font-size: 10px;text-align: center;width: 5%">SL</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">Description</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">Note</th>
                                    <th style="font-size: 10px;text-align: center;width: 5%">Total</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php if(isset($result)): ?>
                                <?php for($i =0; $i < $count_sum; $i++): ?>
                                <tr>
                                    <?php if(isset($result['credit'][$i])): ?>
                                        <td style="font-size: 10px;text-align: center;"><?php echo e($i + 1); ?></td>
                                        <td style="font-size: 10px;text-align: left;"><?php echo e($result['credit'][$i]['customer'] != null ? $result['credit'][$i]['customer'] : ''); ?><strong><?php echo e($result['credit'][$i]['description']); ?></strong>
                                        
                                            <?php if($result['credit'][$i]['sales_id'] != null): ?>
                                            <a class="dropdown-item" href="<?php echo e(route('invoices_show', $result['credit'][$i]['sales_id'])); ?>"><?php echo e('INV - ' . str_pad($result['credit'][$i]['sales_id'], 6, "0", STR_PAD_LEFT)); ?></a>
                                            <?php endif; ?>
                                            
                                            <?php if($result['credit'][$i]['payemnt_id'] != null): ?>
                                            <a class="dropdown-item" href="<?php echo e(route('payments_print', $result['credit'][$i]['payemnt_id'])); ?>"><?php echo e('PM - ' . str_pad($result['credit'][$i]['payemnt_id'], 6, "0", STR_PAD_LEFT)); ?></a>
                                            <?php endif; ?>
                                        
                                        </td>
                                        <td style="font-size: 10px;text-align: left;"> <strong><?php echo e($result['credit'][$i]['note']); ?></strong></td>
                                        <td style="font-size: 10px;text-align: right;"><?php echo e($result['credit'][$i]['amount'] != 0 ? number_format($result['credit'][$i]['amount'],0,'.',',') : ''); ?></td>
                                    <?php else: ?>
                                        <td style="font-size: 10px;text-align: center;"><?php echo e($i + 1); ?></td>
                                        <td style="font-size: 10px;text-align: left;"></td>
                                        <td style="font-size: 10px;text-align: right;"></td>
                                        <td style="font-size: 10px;text-align: right;"></td>
                                    <?php endif; ?>



                                    <?php if(isset($result['debit'][$i])): ?>
                                    <td style="font-size: 10px;text-align: center;"><?php echo e($i + 1); ?></td>
                                    <td style="font-size: 10px;text-align: left;"><?php echo e($result['debit'][$i]['customer'] != null ? $result['debit'][$i]['customer'] : ''); ?> <strong><?php echo e($result['debit'][$i]['description']); ?></strong></td>
                                    <td style="font-size: 10px;text-align: left;"> <strong><?php echo e($result['debit'][$i]['note']); ?></strong></td>
                                    <td style="font-size: 10px;text-align: right;"><?php echo e(number_format($result['debit'][$i]['amount'],0,'.',',')); ?></td>
                                    <?php else: ?>
                                    <td style="font-size: 10px;text-align: center;"><?php echo e($i + 1); ?></td>
                                    <td style="font-size: 10px;text-align: left;"></td>
                                    <td style="font-size: 10px;text-align: right;"></td>
                                    <td style="font-size: 10px;text-align: right;"></td>
                                    <?php endif; ?>
                                </tr>
                                <?php endfor; ?>
                                <?php endif; ?>
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="3" style="text-align: right;">Total </th>
                                    <th colspan="1" style="text-align: right;"><?php echo e(number_format($credit_sum,0,'.',',')); ?></th>
                                    
                                    <th colspan="3" style="text-align: right;">Total</th>
                                    <th colspan="1" style="text-align: right;"><?php echo e(number_format($debit_sum,0,'.',',')); ?></th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/61_1/Modules/Reports/Resources/views/custom_collection_report_print.blade.php ENDPATH**/ ?>