<!DOCTYPE html>
<html>

<head>
    <title>Monthly Summary Report</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">
</head>

<style type="text/css" media="print">        
    @page {
        size: auto;   
        margin: 15mm 5mm 5mm 10mm;
    }

    #print-footer {
        display: none;
        position: fixed;
        bottom: 0;
        left:0;
    }

    #hide-print {
        display: none;
    }

    #print-footer {
        display: none;
    }

    body {
        margin-bottom:10mm;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #ddd;
    }
    
    fontSizeC {
        font-size: 10px !important;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Monthly Report</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">From Date : {{ date('d-m-Y', strtotime($from_date)) }}  To Date : {{ date('d-m-Y', strtotime($to_date)) }}</th>
                                </tr>
                            </thead>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center;width: 2%">SL</th>
                                    <th style="text-align: center;width: 5%">Date</th>
                                    <th style="text-align: center;width: 10%">Cutomer</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom" rowspan="2">Trans#</th>
                                    <th style="text-align: center;width: 5%">Debit</th>
                                    <th style="text-align: center;width: 5%">Credit</th>
                                    <th style="text-align: center;width: 5%">Balance</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <tr>
                                    <td style="text-align: right;font-size: 12px !important" colspan="4"><strong>Opening Balance</strong></a>
                                    </td>
                                    <td style="text-align: right;font-size: 12px !important;" colspan="3"><strong>{{  number_format($opening_balance,0,'.',',') }}</strong></td>
                                </tr>

                                <?php 
                                $balance = 0; 
                                $sub_total   = $opening_balance;
                                ?>
                                @if(isset($data))
                                @foreach($data as $key=> $value)

                                <?php 

                                    if ($value['debit_credit'] == 0)
                                    {   
                                        $credit     = $value['amount'];
                                        $debit      = 0;
                                        $balance    = $balance + $value['amount'];
                                        
                                    }
                                    
                                    if ($value['debit_credit'] == 1)
                                    {
                                        $credit     = 0;
                                        $debit      =  $value['amount'];
                                        $balance    = $balance - $value['amount'];
                                    }

                                    $sub_total = $sub_total + $credit - $debit;


                                    if ($value['transaction_head'] == 'supplier-opening-balance')
                                    {
                                        $trans_number     = '';
                                    }
                                    elseif ($value['transaction_head'] == 'purchase')
                                    {
                                        $trans_number       = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['transaction_head'] == 'payment-receive')
                                    {
                                        if ($value->invoice_id != null)
                                        {
                                            $trans_number     = 'INV - '.str_pad($value->invoice->invoice_number, 6, "0", STR_PAD_LEFT);
                                        }
                                        elseif ($value->purchase_return_id != null)
                                        {
                                            $trans_number     = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                        }
                                        else
                                        {
                                            $trans_number     = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                        }
                                    }
                                    elseif ($value['transaction_head'] == 'payment-made')
                                    {
                                        if ($value->bill_id != null)
                                        {
                                            $trans_number       = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                        }
                                        elseif ($value->sales_return_id != null)
                                        {
                                            $trans_number       = 'SR - '.str_pad($value->salesReturn->sales_return_number, 6, "0", STR_PAD_LEFT);

                                        }
                                        else
                                        {
                                            $trans_number       = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                        }
                                    }
                                    elseif ($value['transaction_head'] == 'purchase-return')
                                    {
                                        $trans_number       = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['transaction_head'] == 'supplier-settlement')
                                    {
                                        $trans_number       = '';
                                    }
                                    else
                                    {
                                        $trans_number       = '';
                                    } 

                                ?>
                                <tr>
                                    <td style="text-align: center;">{{$loop->index+1}}</td>
                                    <td style="text-align: center;">{{ date('d-m-Y', strtotime($value['date'])) }}</td>
                                    <td style="text-align: left;">{{ $value->customer->name }} </td>
                                    <td style="text-align: left;">{{ $trans_number  }}</td>
                                    <td style="text-align: right;">{{ number_format($debit,0,'.',',') }}</td>
                                    <td style="text-align: right;">{{ number_format($credit,0,'.',',') }}</td>
                                    <td style="text-align: right;">{{ number_format($sub_total,0,'.',',') }}</td>
    
                                </tr>
                                @endforeach
                                @endif
                            </tbody>


                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>