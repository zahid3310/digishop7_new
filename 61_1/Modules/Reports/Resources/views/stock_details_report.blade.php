@extends('layouts.app')

@section('title', 'Stock Details Report')

<style type="text/css">
    @media print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page {
            margin: 0cm ! important;
        }

        ::-webkit-scrollbar {
            display: none;
        }
    }
</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Stock Details Report</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Stock Details Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center">{{ $user_info['organization_name'] }}</h2>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['address'] }}</p>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['contact_number'] }}</p>
                                        <h4 style="line-height: 10px;text-align: center">Stock Details Report</h4>
                                        <h3 style="line-height: 15px;text-align: center">{{ $products['name'] }}</h3>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="{{ route('stock_report_details', $products['id']) }}" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="{{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="{{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-6 col-12 d-print-none margin-top-10-xs">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a style="padding: 11px !important;" href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center">SL</th>
                                            <th style="text-align: center">Date</th>
                                            <th style="text-align: center">Reference</th>
                                            <th style="text-align: center">Item Name</th>
                                            <th style="text-align: center">Quantity</th>
                                            <th style="text-align: center">Type</th>
                                            <th style="text-align: right">Price</th>
                                            <th style="text-align: right">Actual Price</th>
                                            <th style="text-align: right">Amount</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        @if(!empty($data) && (count($data) > 0))
                                        <?php $i = 1; ?>
                                        @foreach($data as $key => $value)
                                            <tr>
                                                <td style="text-align: center">{{ $i }}</td>
                                                <td>{{ $value['date'] }}</td>
                                                <td>{{ $value['reference'] }}</td>
                                                <td>{{ $value['item_name'] }}</td>
                                                <td style="text-align: center">{{ number_format($value['quantity'],2,'.',',') }}</td>
                                                <td style="text-align: center">{{ $value['type'] }}</td>
                                                <td style="text-align: right">{{ number_format($value['sale_price'],2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format($value['actual_price'],2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format($value['amount'],2,'.',',') }}</td>
                                            </tr>
                                            <?php $i++; ?>
                                         @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection