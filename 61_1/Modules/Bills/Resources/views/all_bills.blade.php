@extends('layouts.app')

@section('title', 'All Purchases')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">All Purchases</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Purchases</a></li>
                                    <li class="breadcrumb-item active">All Purchases</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {!! Session::get('success') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('unsuccess'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! Session::get('unsuccess') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('errors'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! 'Some required fields are missing..!! Please try again..' !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif
                                
                            <div class="card-body table-responsive">
                                
                                <form method="get" action="{{ route('bills_all_bills') }}" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div style="margin-bottom: 0px;margin-top: 15px" class="form-group row">
                                                <label for="productname" class="col-md-4 col-form-label">From Date</label>
                                                <div class="col-md-8">
                                                    <input style="cursor: pointer" id="search_from_date" type="date" class="form-control" name="from_date" @if($search_by_from_date != 0) value="<?= date("Y-m-d", strtotime($search_by_from_date)) ?>" @endif>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div style="margin-bottom: 0px;margin-top: 15px" class="form-group row">
                                                <label for="productname" class="col-md-4 col-form-label">To Date</label>
                                                <div class="col-md-8">
                                                    <input style="cursor: pointer" id="search_to_date" type="date" class="form-control" name="to_date" @if($search_by_to_date != 0) value="<?= date("Y-m-d", strtotime($search_by_to_date)) ?>" @endif>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div style="margin-bottom: 0px;margin-top: 15px" class="form-group row">
                                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Supplier </label>
                                                <div class="col-md-8">
                                                    <select style="width: 100%" id="supplier_id" class="form-control select2" name="supplier_id">
                                                        <option value="{{ $find_supplier != null ? $find_supplier['id'] : 0 }}" selected>{{ $find_supplier != null ? $find_supplier['name'] : 'All' }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div style="margin-bottom: 0px;margin-top: 15px" class="form-group row">
                                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Product </label>
                                                <div class="col-md-8">
                                                    <select style="width: 100%" id="product_id" class="form-control select2" name="product_id">
                                                        <option value="{{ $find_product != null ? $find_product['id'] : 0 }}" selected>{{ $find_product != null ? $find_product['name'] . " ( ".$find_product['product_code']. " - ".$find_product['brand_name']. " )" : 'All' }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div style="margin-bottom: 0px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Bill </label>
                                                <div class="col-md-8">
                                                    <div class="row">
                                                        <select style="width: 73%" id="bill_number" name="bill_number" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                           <option value="{{ $find_bill != null ? $find_bill['id'] : 0 }}" selected>{{ $find_bill != null ? 'BILL - ' . str_pad($find_bill['bill_number'], 6, "0", STR_PAD_LEFT) : 'All' }}</option>
                                                        </select>
                                                        <button style="border-radius: 0px;padding: 0px;padding-left: 15px;padding-right: 15px;padding-top: 6px;" type="submit" class="btn btn-success">
                                                            <i class="bx bx-search font-size-24"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </form>

                                <hr>

                                <table id="der" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%">SL</th>
                                            <th style="width: 10%">Date</th>
                                            <th style="width: 10%">Bill#</th>
                                            <th style="width: 10%">Supplier</th>
                                            <th style="width: 20%">Products</th>
                                            <th style="width: 10%">Amount</th>
                                            <th style="width: 10%">Paid</th>
                                            <th style="width: 10%">Due</th>
                                            <th style="width: 5%">Action</th>
                                        </tr>
                                    </thead>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $("#supplier_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 1 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#bill_number").select2({
                ajax: { 
                url:  site_url + '/reports/purchase-statement/bill-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $("#product_id").select2({
                ajax: { 
                url:  site_url + '/invoices/product-list-load-invoice-search',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            if (location.search != "")
            {
                var from_date       = (new URL(location.href)).searchParams.get('from_date');
                var to_date         = (new URL(location.href)).searchParams.get('to_date');
                var customer_id     = (new URL(location.href)).searchParams.get('supplier_id');
                var product_id      = (new URL(location.href)).searchParams.get('product_id');
                var bill_number     = (new URL(location.href)).searchParams.get('bill_number');

                $('#der').DataTable({
                    processing: true,
                    serverSide: true,
                    pageLength: 100,
                    ajax: {
                        url: site_url + '/bills/bill/list/load/' + '?from_date=' + from_date +'&to_date=' + to_date + '&supplier_id=' + supplier_id + '&product_id=' + product_id + '&bill_number=' + bill_number,
                        type: 'GET',
                    },
                    columns: [
                        {data: 'sl'},
                        {data: 'date'},
                        {data: 'bill_number'},
                        {data: 'supplier_name'},
                        {data: 'products'},
                        {data: 'total_payable'},
                        {data: 'given'},
                        {data: 'due'},
                        {data: 'action'}
                    ],
                });
            }
            else
            {
                $('#der').DataTable({
                    processing: true,
                    serverSide: true,
                    pageLength: 100,
                    ajax: {
                        url: site_url + '/bills/bill/list/load/',
                        type: 'GET',
                    },
                    columns: [
                        {data: 'sl'},
                        {data: 'date'},
                        {data: 'bill_number'},
                        {data: 'supplier_name'},
                        {data: 'products'},
                        {data: 'total_payable'},
                        {data: 'given'},
                        {data: 'due'},
                        {data: 'action'}
                    ],
                });
            }

            $.get(site_url + '/bills/bill/list/load', function(data){

                billList(data);
                
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var search_text     = $('#searchPayment').val();
            var site_url        = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/bills/bill/search/list/' + search_text, function(data){

                billList(data);

            });
        }

        function billList(data)
        {
            var bill_list = '';
            var site_url  = $('.site_url').val();
            $.each(data, function(i, bill_data)
            {   
                var serial              = parseFloat(i) + 1;
                var edit_url            = site_url + '/bills/edit/' + bill_data.id;
                var print_url           = site_url + '/bills/show/' + bill_data.id;
                var purchase_return     = site_url + '/purchasereturn?supplier_id=' + bill_data.vendor_id + '&bill_id=' + bill_data.id;
                var barcode_print       = site_url + '/products/barcode-print?bill_id=' + bill_data.id;

                if (bill_data.return_id != null)
                {
                    var return_sign  = '&nbsp;' + '&nbsp;' + '<i class="bx bx-repost font-size-15">' + '</i>';
                }
                else
                {
                    var return_sign  = '';
                }

                if (bill_data.contact_type == 0)
                {
                    var type = 'Customer';
                }

                if (bill_data.contact_type == 1)
                {
                    var type = 'Supplier';
                }

                if (bill_data.contact_type == 2)
                {
                    var type = 'Employee';
                }

                bill_list += '<tr>' +
                                    '<td>' +
                                        serial +
                                    '</td>' +
                                    '<td>' +
                                       formatDate(bill_data.bill_date) +
                                    '</td>' +
                                    '<td>' +
                                       'BILL - ' + bill_data.bill_number.padStart(6, '0') + return_sign +
                                    '</td>' +
                                    '<td>' +
                                        bill_data.customer_name + ' | ' + type +
                                    '</td>' +
                                    '<td>' +
                                       bill_data.bill_amount +
                                    '</td>' +
                                    '<td>' +
                                       (parseFloat(bill_data.bill_amount) - parseFloat(bill_data.due_amount)) +
                                    '</td>' +
                                    '<td>' +
                                       bill_data.due_amount +
                                    '</td>' +
                                    '<td>' +
                                        '<div class="dropdown">' +
                                            '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                            '</a>' +
                                            '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                '@if(Auth::user()->role == 1)' +
                                                '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                '@endif' +
                                                '<a class="dropdown-item" href="' + purchase_return +'" target="_blank">' + 'Purchase Return' + '</a>' +
                                                '<a class="dropdown-item" href="' + print_url +'" target="_blank">' + 'Show' + '</a>' +
                                                '<a class="dropdown-item" href="' + barcode_print +'" target="_blank">' + 'Print Bar Code' + '</a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</td>' +
                                '</tr>';
            });

            $("#bill_list").empty();
            $("#bill_list").append(bill_list);
        }
    </script>
@endsection