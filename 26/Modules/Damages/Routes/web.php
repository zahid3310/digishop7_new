<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('damages')->group(function() {
    Route::get('/', 'DamagesController@index')->name('damages_index');
    Route::get('/create', 'DamagesController@create')->name('damages_create');
    Route::post('/store', 'DamagesController@store')->name('damages_store');
    Route::get('/edit/{id}', 'DamagesController@edit')->name('damages_edit');
    Route::post('/update/{id}', 'DamagesController@update')->name('damages_update');
});
