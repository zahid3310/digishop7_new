<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('invoices')->group(function() {
    Route::get('/', 'InvoicesController@index')->name('invoices_index');
    Route::get('/all-sales', 'InvoicesController@AllSales')->name('invoices_all_sales');
    Route::post('/store', 'InvoicesController@store')->name('invoices_store');
    Route::get('/edit/{id}', 'InvoicesController@edit')->name('invoices_edit');
    Route::post('/update/{id}', 'InvoicesController@update')->name('invoices_update');
    Route::get('/show/{id}', 'InvoicesController@show')->name('invoices_show');
    Route::get('/show-pos/{id}', 'InvoicesController@showPos')->name('invoices_show_pos');
    Route::get('/show-invoice/{id}', 'InvoicesController@showInvoice')->name('invoices_show_invoice');
    Route::get('/products/list', 'InvoicesController@productList')->name('product_list');
    Route::get('/products/price/list/{id}', 'InvoicesController@productPriceList')->name('product_price_list');
    Route::get('/warrenty-price-list/{product_entry_id}/{warrenty_id}', 'InvoicesController@warrentyProductPriceList')->name('warrenty_product_price_list');
    Route::get('/invoice/list/load', 'InvoicesController@invoiceListLoad')->name('invoices_list_load');
    Route::get('/invoice/search/list/{from_date}/{to_date}/{customer_name}/{invoice_number}', 'InvoicesController@invoiceListSearch')->name('invoices_list_search');
    Route::get('/invoice/pos/search/product/{id}', 'InvoicesController@posSearchProduct')->name('invoices_pos_search_product');
    Route::post('/customer/add/invoices', 'InvoicesController@customerStore');
    Route::get('/customer/list/invoices', 'InvoicesController@customersListInvoice');

    Route::get('/customer-make-payment/{id}', 'InvoicesController@makePayment');
    Route::post('/pay-customer-bill/payments', 'InvoicesController@storePayment')->name('pay_customer_bill');
    Route::get('/search/coupon-code/{id}', 'InvoicesController@couponCode')->name('invoices_coupon_code');

    Route::get('/print-invoices-list', 'InvoicesController@printInvoicesList')->name('invoices_print_invoices_list');
    Route::get('/print-invoices-search/{date}/{customer}/{invoice_number}', 'InvoicesController@printInvoicesSearch')->name('invoices_print_invoices_search');
    Route::get('/calculate-opening-balance/{customer_id}', 'InvoicesController@calculateOpeningBalance')->name('invoices_calculate_opening_balance');
});