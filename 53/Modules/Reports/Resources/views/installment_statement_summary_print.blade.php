<!DOCTYPE html>
<html>

<head>
    <title>Statement of Installment</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
    
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Statement of Installment</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Customer Name</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">
                                        @if($customer_name != null)
                                            {{ $customer_name['name'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 11%">CUSTOMER NAME</th>
                                    <th style="text-align: center;width: 4%">INVOICE#</th>
                                    <th style="text-align: center;width: 4%">INVOICE DATE</th>
                                    <th style="text-align: center;width: 4%">RECEIVABLE</th>
                                    <th style="text-align: center;width: 5%">RECEIVED</th>
                                    <th style="text-align: center;width: 4%">DUES</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i                = 1;
                                    $total_receivable = 0;
                                    $total_received   = 0;
                                    $total_dues       = 0;
                                ?>
                                @foreach($data as $key => $value)
                                <tr>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value->count() }}">{{ $i }}</td>
                                    <td style="text-align: left;vertical-align: middle" rowspan="{{ $value->count() }}">{{ $key }}</td>
                                    
                                    <td style="text-align: center;">{{ str_pad($value[0]['invoice_number'], 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: center;">{{ date('d-m-Y', strtotime($value[0]['invoice_date'])) }}</td>
                                    <td style="text-align: right;">{{ $value[0]['total_amount'] }}</td>
                                    <td style="text-align: right;">{{ $value[0]['total_paid'] }}</td>
                                    <td style="text-align: right;">{{ $value[0]['total_amount'] - $value[0]['total_paid'] }}</td>
                                </tr>

                                <?php
                                    $sub_total_receivable = 0;
                                    $sub_total_received   = 0;
                                    $sub_total_dues       = 0;
                                ?>

                                @foreach($value as $key1 => $value1)

                                <?php

                                    $sub_total_receivable   = $sub_total_receivable + $value1['total_amount'];
                                    $sub_total_received     = $sub_total_received + $value1['total_paid'];
                                    $sub_total_dues         = $sub_total_dues + ($value1['total_amount'] - $value1['total_paid']);
                                ?>

                                @if($key1 != 0)
                                <tr>
                                    <td style="text-align: center;">{{ str_pad($value1['invoice_number'], 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: center;">{{ date('d-m-Y', strtotime($value1['invoice_date'])) }}</td>
                                    <td style="text-align: right;">{{ $value1['total_amount'] }}</td>
                                    <td style="text-align: right;">{{ $value1['total_paid'] }}</td>
                                    <td style="text-align: right;">{{ $value1['total_amount'] - $value1['total_paid'] }}</td>
                                </tr>

                                @endif
                                @endforeach
                                 
                                <?php
                                    $i++;
                                    $total_receivable = $total_receivable + $sub_total_receivable;
                                    $total_received   = $total_received + $sub_total_received;
                                    $total_dues       = $total_dues + $sub_total_dues;
                                ?>
                                @endforeach
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="4" style="text-align: right;">TOTAL</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_receivable }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_received }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_dues }}</th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>