

<?php $__env->startSection('title', 'Installment Collection'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Payments</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Payments</a></li>
                                    <li class="breadcrumb-item active">Payments</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
                                            <label class="control-label">Payment Type *</label>
                                            <select id="type" style="width: 100%;cursor: pointer" class="form-control" onchange="searchContact()">
                                                <option value="0">Installment Payment</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                    	<div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
	                                        <label class="control-label">Search Contact *</label>
	                                        <select id="contact_id" style="width: 100%;cursor: pointer" class="form-control select2 contact_id" onchange="searchContact()">
	                                        	<option value="">--Select Contact--</option>
												<?php if(!empty($customers)): ?>
                                                    <?php 
                                                        if (isset($_GET['payment_type']))
                                                        {
                                                            $customers = $customers->where('contact_type', $_GET['payment_type']);
                                                        }
                                                        else
                                                        {
                                                            $customers = $customers;
                                                        }
                                                    ?>
													<?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<option <?php if(isset($find_customer)): ?> <?php echo e($find_customer['id'] == $customer['id'] ? 'selected' : ''); ?> <?php endif; ?> value="<?php echo e($customer->id); ?>"><?php echo e($customer->name . ' | '); ?> <?php echo e($customer->contact_type == 0 ? 'Customer' : 'Supplier'); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												<?php endif; ?>
	                                        </select>
	                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->

                    <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

            					<form id="FormSubmit" action="<?php echo e(route('payments_installment_collection_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					<?php echo e(csrf_field()); ?>


                                    <input id="customerIdHidden" name="customer_id" type="hidden" class="form-control">
                                    <input id="customerIdHiddenReload" name="" type="hidden" class="form-control" value="<?php echo e(Session('find_customer')); ?>">
                                    <input id="typeHiddenReload" name="" type="hidden" class="form-control" value="<?php echo e(Session('find_type')); ?>">
                                    <input id="typeInput" name="type" type="hidden" class="form-control">

            						<div class="row">
					                	<div class="col-sm-6">
					                        <div class="form-group">
					                            <label for="payment_date">Payment Date *</label>
					                            <input id="payment_date" name="payment_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
					                        </div>
					                    </div>

					                    <div class="col-sm-6">
					                        <div class="form-group">
					                            <label for="amount">Amount *</label>
					                            <input id="amount" name="amount" type="text" class="form-control" onchange="searchContact()">
					                        </div>
					                    </div>

					                    <div class="col-sm-6 form-group">
				                            <label class="control-label">Paid Through</label>
                                            <select style="cursor: pointer" name="paid_through" class="form-control select2">
                                                <?php if(!empty($paid_accounts) && ($paid_accounts->count() > 0)): ?>
                                                <?php $__currentLoopData = $paid_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $paid_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($paid_account['id']); ?>"><?php echo e($paid_account['name']); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </select>
				                        </div>

				                        <div class="col-sm-6">
					                        <div class="form-group">
					                            <label for="note">Note</label>
					                            <input id="note" name="note" type="text" class="form-control">
					                        </div>
					                    </div>
				                	</div>

                                    <div id="type0" style="display: none" class="table-responsive">
                                        <table class="table table-centered table-nowrap">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Installment#</th>
                                                    <th>Date</th>
                                                    <th>Receivable</th>
                                                    <th>Received</th>
                                                    <th>Dues</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody id="invoice_list">
												
                                            </tbody>
                                        </table>
                                    </div>

                                    <hr style="margin-top: 0px !important">

                                    <div class="form-group row">
                                        <div class="button-items col-md-12">
                                            <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Make Payment</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('payments_installment_collection')); ?>">Close</a></button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <hr>

                <div class="row">

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">Payment List</h4>

                                <div style="margin-right: 10px" class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-1">Search : </div>
                                    <div class="col-md-2">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th>Payment#</th>
                                            <th>Contact</th>
                                            <th>Type</th>
                                            <th>Paid Through</th>
                                            <th>Note</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="payment_list">
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Are you sure want to delete the payment ?</label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="delete_btn" class="btn btn-primary waves-effect waves-light delete_btn">Delete</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

 	<script type="text/javascript">
        $( document ).ready(function() {

            $('.enableOnInput').prop('disabled', true);

            var customerId      = $('#customerIdHiddenReload').val();
            var typeId          = $('#typeHiddenReload').val();
            var type_input      = $('#type').val();

            $('#contact_id').val(customerId).change();
            $('#typeInput').val(type_input);

            if(typeId != '')
            {    
                $('#type').val(typeId).change();
            }

            var site_url        = $('.site_url').val();
            var contact_id      = $('#contact_id').val();

            if (contact_id == '')
            {
                var contact_id_find = 0;
            }
            else
            {
                var contact_id_find = $('#contact_id').val();
            }

            $.get(site_url + '/payments/payment/list/' + contact_id_find, function(data){

                paymentList(data);
                
            });

            searchContact();
        });

 		function searchContact()
 		{
 			var contact_id 	= $('#contact_id').val();
            var site_url    = $('.site_url').val();
 			var amounts	    = $('#amount').val();
            var type_input  = $('#type').val();

            $('#typeInput').val(type_input);

            if (amounts == '')
            {
                var amount = 0;
            }
            else
            {
                var amount = amounts;
            }

            if (type_input == 0)
            {
                $('#type0').show();

                $.get(site_url + '/payments/installment-list/' + contact_id, function(data){

                    var invoice_list = '';
                    var amount_left  = parseFloat(amount);
                    var paid         = 0;

                    $.each(data.installments, function(i, data)
                    {   
                        if (amount_left > (parseFloat(data.amount) - parseFloat(data.paid)))
                        {
                            var amount_paid  = (parseFloat(data.amount) - parseFloat(data.paid));
                            amount_left     -= parseFloat(amount_paid);
                            paid            += parseFloat(amount_paid);
                        }
                        else
                        {
                            var amount_paid  = amount_left;
                            amount_left     -= parseFloat(amount_paid);
                            paid            += parseFloat(amount_paid);
                        }

                        if (paid != 0)
                        {
                            $('#amount').val(paid);
                        }

                        invoice_list += '<tr>' +
                                            '<input class="form-control" type="hidden" name="invoice_id[]" value="' +  data.invoice_id + '">' +
                                            '<input class="form-control" type="hidden" name="installment_id[]" value="' +  data.id + '">' +
                                            '<td style="text-align: left">' +
                                               'INS - ' + data.installment_number.padStart(3, '0') +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                formatDate(data.date) +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               (data.amount) +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               (parseFloat(data.paid)) +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               (parseFloat(data.amount) - parseFloat(data.paid)) +
                                            '</td>' +

                                            '<td style="text-align: left">' +
                                                '<input style="width: 150px" class="form-control" type="text" name="paid[]" value="' + amount_paid + '">' +
                                            '</td>' +
                                        '</tr>';
                    });

                    if (invoice_list != '')
                    {
                        $('.enableOnInput').prop('disabled', false);
                    }
                    else
                    {
                        $('.enableOnInput').prop('disabled', true);
                    }

                    $("#invoice_list").empty();
                    $("#invoice_list").append(invoice_list);   
                });
            }
            
            $("#customerIdHidden").val(contact_id);

            //Payment List Show In The Index Section For A Specific Contact ID
            $.get(site_url + '/payments/payment/list/' + contact_id, function(data){

                paymentList(data);

            });
 		}

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var search_text = $('#searchPayment').val();
            var site_url    = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }
            else
            {
                var search_text = $('#searchPayment').val();
            }

            $.get(site_url + '/payments/payment/list/search/' + search_text, function(data){

                paymentList(data);

            });
        }
 	</script>

    <script type="text/javascript">
        $('.delete_btn').click(function () {
            var site_url            = $('.site_url').val();
            var id                  = $('.paymentId').val();
            window.location.href    = site_url + "/payments/delete/"+id;
        })
    </script>

    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }
    </script>

    <script type="text/javascript">
        function paymentList(data)
        {
            var payment_list    = '';
            var site_url        = $('.site_url').val();
            $.each(data, function(i, payment_data)
            {
                var serial      = parseFloat(i) + 1;
                var show_url    = site_url + '/payments/show/' + payment_data.id;
                var delete_url  = site_url + '/payments/installment-delete/' + payment_data.id;
   
                if (payment_data.type == 0)
                {
                    var p_type = "Customer Payment";
                }

                if (payment_data.type == 1)
                {
                    var p_type = "Supplier Payment";
                }

                if (payment_data.type == 2)
                {
                    var p_type = "Sales Return Payment";
                }

                if (payment_data.type == 3)
                {
                    var p_type = "Purchase Return Payment";
                }

                if(payment_data.note != null)
                {
                    var p_note = payment_data.note;
                }
                else
                {
                    var p_note = "";
                }
                
                payment_list += '<tr>' +
                                    '<input class="form-control paymentId" type="hidden" name="payment_id[]" value="' +  payment_data.id + '">' +
                                    '<td style="text-align: left">' +
                                        serial +
                                    '</td>' +
                                    '<td style="text-align: left">' +
                                        formatDate(payment_data.payment_date) +
                                    '</td>' +
                                    '<td style="text-align: left">' +
                                       'PM - ' + payment_data.payment_number.padStart(5, '0') +
                                    '</td>' +
                                    '<td style="text-align: left">' +
                                       payment_data.customer_name +
                                    '</td>' +
                                    '<td style="text-align: left">' +
                                       p_type +
                                    '</td>' +
                                    '<td style="text-align: left">' +
                                       payment_data.paid_through_accounts_name +
                                    '</td>' +
                                    '<td style="text-align: left">' +
                                       p_note +
                                    '</td>' +
                                    '<td style="text-align: left">' +
                                       (payment_data.amount) +
                                    '</td>' +
                                    '<td style="text-align: left">' +
                                        '<div class="dropdown">' +
                                            '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                            '</a>' +
                                            '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                '<?php if(Auth::user()->role == 1): ?>' + 
                                                '<a class="dropdown-item" href="' + delete_url +'">' + 'Delete' + '</a>' +
                                                '<?php endif; ?>' +
                                                '<a class="dropdown-item" href="' + show_url +'" target="_blank">' + 'Show' + '</a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</td>' +
                                '</tr>';
            });

            $("#payment_list").empty();
            $("#payment_list").append(payment_list);
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/53/Modules/Payments/Resources/views/installment_collection.blade.php ENDPATH**/ ?>