<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesReturnEntries extends Model
{
    protected $table = "sales_return_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Products','product_id');
    }
}
