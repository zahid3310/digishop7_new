<?php

namespace Modules\Bills\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Products;
use App\Models\Customers;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\PaidThroughAccounts;
use App\Models\ProductEntries;
use App\Models\Payments;
use App\Models\Expenses;
use App\Models\Users;
use App\Models\ProductVariations;
use App\Models\PaymentEntries;
use App\Models\AccountTransactions;
use Response;
use DB;

class BillsController extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $products           = ProductEntries::orderBy('product_code', 'DESC')
                                            ->get();

        $product_id         = array_values($products->sortByDesc('product_code')->take(1)->toArray());
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();
        $variations         = ProductVariations::orderBy('id', 'ASC')->get();

        return view('bills::index', compact('products', 'product_id', 'paid_accounts', 'variations'));
    }

    public function allBills()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('bills::all_bills');
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('bills::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'          => 'required|date',
            'vendor_id'             => 'required|integer',
            'product_entries.*'     => 'required|integer',
            'amount.*'              => 'required|numeric',
            'vendor_id'             => 'required|integer',
            'bill_note'             => 'nullable|string',
            'selling_date'          => 'required|date',
            'product_entries.*'     => 'required|integer',
            'rate.*'                => 'required|numeric',
            'quantity.*'            => 'required|numeric',
            'discount_type.*'       => 'required|integer',
            'discount_amount.*'     => 'nullable|numeric',
            'amount.*'              => 'required|numeric',
            'total_amount'          => 'required|numeric',
            'total_discount_type'   => 'nullable|integer',
            'vat_type'              => 'nullable|numeric',
            'vat_amount'            => 'nullable|numeric',
            'total_discount_amount' => 'nullable|numeric',
            'total_discount_note'   => 'nullable|string',
            'cash_given'            => 'nullable|numeric',
            'change_amount'         => 'nullable|numeric',
            'coupon_code'           => 'nullable|numeric',
            'account_information.*' => 'nullable|string',
            'paid_through.*'        => 'nullable|integer',
            'note.*'                => 'nullable|string',
            'amount_paid.*'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            // $adjustment     = $data['adjustment_amount'];
            $vat            = $data['vat_amount'];
            // $tax            = $data['tax_amount'];

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            $data_find                      = Bills::orderBy('created_at', 'DESC')->first();
            $bill_number                    = $data_find != null ? $data_find['bill_number'] + 1 : 1;

            $bill                           = new Bills;
            $bill->bill_number              = $bill_number;
            $bill->vendor_id                = $data['vendor_id'];
            $bill->bill_date                = date('Y-m-d', strtotime($data['selling_date']));
            $bill->bill_amount              = $data['total_amount'];
            $bill->due_amount               = $data['total_amount'];
            $bill->total_discount           = $discount;
            $bill->bill_note                = $data['bill_note'];
            $bill->total_vat                = $vat;
            $bill->vat_type                 = $data['vat_type'];
            $bill->total_discount_type      = $data['total_discount_type'];
            $bill->total_discount_amount    = $data['total_discount_amount'];
            $bill->total_discount_note      = $data['total_discount_note'];
            $bill->cash_given               = $data['cash_given'];
            $bill->change_amount            = $data['change_amount'];
            $bill->product_type             = $data['product_type'];
            $bill->created_by               = $user_id;

            if ($bill->save())
            {
                foreach ($data['product_entries'] as $key1 => $value1)
                {   
                    $product_buy_price       = ProductEntries::find($value1);

                    $bill_entries[] = [
                        'bill_id'            => $bill['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value1,
                        'vendor_id'          => $bill['vendor_id'],
                        'pcs'                => $data['pcs'][$key1] != null ? $data['pcs'][$key1] : 0,
                        'cartoon'            => $data['cartoon'][$key1] != null ? $data['cartoon'][$key1] : 0,
                        'rate'               => $data['rate'][$key1],
                        'quantity'           => $data['quantity'][$key1],
                        'total_amount'       => $data['amount'][$key1],
                        'discount_type'      => $data['discount_type'][$key1],
                        'discount_amount'    => $data['discount'][$key1],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('bill_entries')->insert($bill_entries);

                stockIn($data, $item_id=null);

                if (isset($data['amount_paid']))
                {   
                    $data_find_payment  = Payments::orderBy('id', 'DESC')->first();
                    $payment_number     = $data_find_payment != null ? $data_find_payment['payment_number'] + 1 : 1;

                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {   
                        if ($data['amount_paid'][$i] > 0)
                        {   
                            $account_transactions[] = [
                                'customer_id'           => $data['vendor_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through_id'       => $data['paid_through'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 1,  // 0 = In , 1 = Out
                                'transaction_head'      => 'purchase',
                                'associated_id'         => $bill->id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payments = [
                                'payment_number'        => $payment_number,
                                'customer_id'           => $data['vendor_id'],
                                'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through'          => $data['paid_through'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 1,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payment_id = DB::table('payments')->insertGetId($payments);      

                            if ($payment_id)
                            {   
                                $payment_entries = [
                                        'payment_id'        => $payment_id,
                                        'bill_id'           => $bill['id'],
                                        'amount'            => $data['amount_paid'][$i],
                                        'created_by'        => $user_id,
                                        'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('payment_entries')->insert($payment_entries);  
                            }

                            $update_bill_dues                = Bills::find($bill['id']);
                            $update_bill_dues->due_amount    = $update_bill_dues['due_amount'] - $data['amount_paid'][$i];
                            $update_bill_dues->save();

                            $payment_number++;
                        }
                    }

                    if (isset($account_transactions))
                    {
                        DB::table('account_transactions')->insert($account_transactions);
                    }
                }

                DB::commit();

                if ($data['print'] == 1)
                {
                    return redirect()->route('bills_all_bills')->with("success","Purchase Created Successfully !!");
                }
                else
                {
                    return redirect()->route('bills_show', $bill['id']);
                }
                
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

         $bill      = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')                       
                                    ->select('bills.*',
                                             'customers.name as customer_name',
                                             'customers.address as customer_address',
                                             'customers.phone as customer_phone')
                                    ->find($id);

        $entries    = BillEntries::leftjoin('products', 'products.id', 'bill_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('customers', 'customers.id', 'bill_entries.vendor_id')
                                    ->where('bill_entries.bill_id', $id)
                                    ->select('bill_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.type as type',
                                             'product_entries.name as product_entry_name',
                                             'product_entries.product_code as product_code',
                                             'customers.contact_type as contact_type',
                                             'units.name as unit_name',
                                             'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                             'sizes.height as height',
                                             'sizes.width as width',
                                             'categories.name as brand_name',
                                             'products.name as product_name')
                                    ->get(); 
                     
        $user_info  = Users::find(1);

        return view('bills::show', compact('entries', 'bill', 'user_info'));
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $find_bill              = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                        ->orderBy('bills.created_at', 'DESC')
                                        ->select('bills.*',
                                                 'customers.id as vendor_id',
                                                 'customers.name as vendor_name')
                                        ->find($id);

        $find_bill_entries      = BillEntries::leftjoin('customers', 'customers.id', 'bill_entries.vendor_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                        ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                        ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                        ->where('bill_entries.bill_id', $id)
                                        ->select('bill_entries.*',
                                                 'customers.id as vendor_id',
                                                 'customers.name as vendor_name',
                                                 'product_entries.id as item_id',
                                                 'product_entries.type as product_type',
                                                 'units.name as unit_name',
                                                 'product_entries.product_code as product_code',
                                                 'product_entries.stock_in_hand as stock_in_hand',
                                                 'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                                 'sizes.height as height',
                                                 'sizes.width as width',
                                                 'product_entries.name as item_name')
                                        ->get();

        $entries_count          = $find_bill_entries->count();
        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->where('payment_entries.bill_id', $id)
                                        ->selectRaw('payment_entries.*, 
                                                     payments.paid_through as paid_through_id,
                                                     payments.account_information as account_information,
                                                     payments.payment_date as payment_date,
                                                     payments.note as note')
                                        ->get();

        $payment_entries_count  = $payment_entries->count();
        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();

        return view('bills::edit', compact('find_bill', 'find_bill_entries', 'entries_count', 'payment_entries', 'payment_entries_count', 'paid_accounts'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'          => 'required|date',
            'vendor_id'             => 'required|integer',
            'product_entries.*'     => 'required|integer',
            'amount.*'              => 'required|numeric',
            'vendor_id'             => 'required|integer',
            'bill_note'             => 'nullable|string',
            'selling_date'          => 'required|date',
            'product_entries.*'     => 'required|integer',
            'rate.*'                => 'required|numeric',
            'quantity.*'            => 'required|numeric',
            'discount_type.*'       => 'required|integer',
            'discount_amount.*'     => 'nullable|numeric',
            'amount.*'              => 'required|numeric',
            'total_amount'          => 'required|numeric',
            'total_discount_type'   => 'nullable|integer',
            'vat_type'              => 'nullable|numeric',
            'vat_amount'            => 'nullable|numeric',
            'total_discount_amount' => 'nullable|numeric',
            'total_discount_note'   => 'nullable|string',
            'cash_given'            => 'nullable|numeric',
            'change_amount'         => 'nullable|numeric',
            'coupon_code'           => 'nullable|numeric',
            'account_information.*' => 'nullable|string',
            'paid_through.*'        => 'nullable|integer',
            'note.*'                => 'nullable|string',
            'amount_paid.*'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            $bill           = Bills::find($id);

            //Calculate Due Amount

                if ($data['total_amount'] > $bill['bill_amount']) 
                {
                    $bill_dues = $bill['due_amount'] + ($data['total_amount'] - $bill['bill_amount']);

                }
                
                if ($data['total_amount'] < $bill['bill_amount'])
                {
                    $bill_dues = $bill['due_amount'] - ($bill['bill_amount'] - $data['total_amount']);
                }

                if ($data['total_amount'] == $bill['bill_amount'])
                {
                    $bill_dues = $bill['due_amount'];
                }

            $bill->vendor_id                = $data['vendor_id'];
            $bill->bill_date                = date('Y-m-d', strtotime($data['selling_date']));
            $bill->bill_amount              = $data['total_amount'];
            $bill->due_amount               = $bill_dues;
            $bill->total_discount           = $discount;
            $bill->bill_note                = $data['bill_note'];
            $bill->total_vat                = $vat;
            $bill->vat_type                 = $data['vat_type'];
            $bill->total_discount_type      = $data['total_discount_type'];
            $bill->total_discount_amount    = $data['total_discount_amount'];
            $bill->total_discount_note      = $data['total_discount_note'];
            $bill->cash_given               = $data['cash_given'];
            $bill->change_amount            = $data['change_amount'];
            $bill->updated_by               = $user_id;

            if ($bill->save())
            {
                $item_id                = BillEntries::where('bill_id', $bill['id'])->get();
                $item_delete            = BillEntries::where('bill_id', $bill['id'])->delete();

                if (isset($data['payment_id']))
                {
                    foreach ($data['payment_id'] as $key_p => $value_p)
                    {
                        $payment_delete  = Payments::where('id', $value_p)->delete();
                    }

                    $update_bill                = Bills::find($bill->id);
                    $update_bill->due_amount    = $update_bill['bill_amount'];
                    $update_bill->updated_by    = $user_id;
                    $update_bill->save();
                }

                foreach ($data['product_entries'] as $key1 => $value1)
                {   
                    $product_buy_price      = ProductEntries::find($value1);

                    $bill_entries[] = [
                        'bill_id'            => $bill['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value1,
                        'vendor_id'          => $bill['vendor_id'],
                        'pcs'                => $data['pcs'][$key1] != null ? $data['pcs'][$key1] : 0,
                        'cartoon'            => $data['cartoon'][$key1] != null ? $data['cartoon'][$key1] : 0,
                        'rate'               => $data['rate'][$key1],
                        'quantity'           => $data['quantity'][$key1],
                        'total_amount'       => $data['amount'][$key1],
                        'discount_type'      => $data['discount_type'][$key1],
                        'discount_amount'    => $data['discount'][$key1],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('bill_entries')->insert($bill_entries);

                stockIn($data, $item_id);

                $transaction_delete = AccountTransactions::where('transaction_head', 'purchase')->where('associated_id', $bill['id'])->delete();

                if (isset($data['amount_paid']))
                {   
                    $data_find_payment  = Payments::orderBy('id', 'DESC')->first();
                    $payment_number     = $data_find_payment != null ? $data_find_payment['payment_number'] + 1 : 1;

                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {   
                        if ($data['amount_paid'][$i] > 0)
                        {   
                            $account_transactions[] = [
                                'customer_id'           => $data['vendor_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through_id'       => $data['paid_through'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 1,  // 0 = In , 1 = Out
                                'transaction_head'      => 'purchase',
                                'associated_id'         => $bill->id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payments = [
                                'payment_number'        => $payment_number,
                                'customer_id'           => $data['vendor_id'],
                                'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through'          => $data['paid_through'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 1,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payment_id = DB::table('payments')->insertGetId($payments);      
                            if ($payment_id)
                            {   
                                $payment_entries = [
                                        'payment_id'        => $payment_id,
                                        'bill_id'           => $bill['id'],
                                        'amount'            => $data['amount_paid'][$i],
                                        'created_by'        => $user_id,
                                        'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('payment_entries')->insert($payment_entries);  
                            }

                            $update_bill_dues                = Bills::find($bill['id']);
                            $update_bill_dues->due_amount    = $update_bill_dues['due_amount'] - $data['amount_paid'][$i];
                            $update_bill_dues->save();

                            $payment_number++;
                        }
                    }

                    if (isset($account_transactions))
                    {
                        DB::table('account_transactions')->insert($account_transactions);
                    }
                }

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return redirect()->route('bills_all_bills')->with("success","Purchase Updated Successfully !!");
                }
                else
                {
                    return redirect()->route('bills_show', $bill['id']);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function ProductEntriesList()
    {
        $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product    = $data->sortBy('name')->all();
        $products   = collect($product);

        return Response::json($products);
    }

    public function ProductEntriesListInvoice()
    {
        $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.stock_in_hand', '>', 0)
                                    ->Where('product_entries.stock_in_hand', '!=', null)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product    = $data->sortBy('name')->all();
        $products   = collect($product);

        return Response::json($products);
    }

    public function billListLoad()
    {
        $data           = Bills::leftjoin('purchase_return', 'purchase_return.bill_id', 'bills.id')
                                ->leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                ->leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                ->where('bills.type', 1)
                                ->orderBy('bills.created_at', 'DESC')
                                ->select('bills.*',
                                         'purchase_return.id as return_id',
                                         'customers.contact_type as contact_type',
                                         'customers.name as customer_name')
                                ->distinct('bills.id')
                                ->take(100)
                                ->get();

        return Response::json($data);
    }

    public function billListSearch($from_date, $to_date, $customer)
    {
        $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
        $search_by_customer     = $customer != 0 ? $customer : 0;

        $data           = bills::leftjoin('purchase_return', 'purchase_return.bill_id', 'bills.id')
                                        ->leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                        ->leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                        ->where('bills.type', 1)
                                        ->when($search_by_from_date != 0 && $search_by_to_date != 0, function ($query) use ($search_by_from_date, $search_by_to_date) {
                                            return $query->whereBetween('bills.bill_date', [$search_by_from_date, $search_by_to_date]);
                                        })
                                        ->when($search_by_customer != 0, function ($query) use ($search_by_customer) {
                                            return $query->where('customers.id', $search_by_customer);
                                        })
                                        ->orderBy('bills.created_at', 'DESC')
                                        ->select('bills.*',
                                                 'purchase_return.id as return_id',
                                                 'customers.name as customer_name',
                                                 'customers.phone as phone')
                                        ->distinct('bills.id')
                                        ->take(100)
                                        ->get();

        return Response::json($data);
    }

    public function storeProduct(Request $request)
    {
        $user_id                        = Auth::user()->id;
        $data                           = $request->all();

        DB::beginTransaction();

        try{
            $data_find                  = ProductEntries::orderBy('id', 'DESC')->first();
            $code                       = $data_find != null ? $data_find['product_code'] + 1 : 1;

            $product                    = new ProductEntries;
            $product->product_id        = $data['product_category_id'];
            $product->sub_category_id   = $data['product_sub_category_id'];
            $product->name              = $data['product_name'];
            $product->product_code      = $code;
            $product->sell_price        = $data['selling_price'];
            $product->buy_price         = $data['buying_price'];

            if ($data['unit_id'] != null)
            {
                $product->unit_id       = $data['unit_id'];
            }

            $product->status                        = $data['status'];
            $product->alert_quantity                = $data['alert_quantity'];
            $product->product_type                  = $data['product_type'];
            $product->created_by                    = $user_id;

            if ($product->save())
            {   
                DB::commit();
                return Response::json($product);
            }
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return Response::json(0);
        }
    }

    public function billProductList()
    {
        $data       = Products::orderBy('products.total_sold', 'DESC')
                                    ->select('products.*')
                                    ->get();

        return Response::json($data);
    }

    public function posSearchProductBill($id)
    {
        $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_code', $id)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->first();

        return Response::json($data);
    }

    public function ProductEntriesListBill($id)
    {
        $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product    = $data->sortBy('name')->all();
        $products   = collect($product);

        return Response::json($products);
    }

    public function productListLoadBill($product_type)
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData      = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->Where('product_entries.status', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.type) as type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                 GROUP_CONCAT(DISTINCT product_entries.pcs_per_cartoon) as pcs_per_cartoon,  
                                                 GROUP_CONCAT(DISTINCT sizes.height, " X " ,sizes.width) as size_name,  
                                                 GROUP_CONCAT(DISTINCT sizes.height) as height,  
                                                 GROUP_CONCAT(DISTINCT sizes.width) as width, 
                                                 GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();
        }
        else
        { 
            $search      = $_GET['searchTerm'];
            $explode     = explode('x', $search);

            if (isset($explode[0]))
            {
                $search_height  = $explode[0];
            }
            else
            {
                $search_height  = 0;
            }

            if (isset($explode[1]))
            {
                $search_width  = $explode[1];
            }
            else
            {
                $search_width  = 0;
            }

            $fetchData      = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->orWhere('product_entries.name', 'LIKE', '%' .$search. '%')
                                    ->orWhere('product_entries.product_code', 'LIKE', '%' .$search. '%')
                                    ->orWhere('categories.name', 'LIKE', '%' .$search. '%')
                                    ->orWhere('products.name', 'LIKE', '%' .$search. '%')
                                    ->when($search_height != 0, function ($query) use ($search_height) {
                                        return $query->orWhere('sizes.height', 'LIKE', '%'.$search_height. '%');
                                    })
                                    ->when($search_width != 0, function ($query) use ($search_width) {
                                        return $query->orWhere('sizes.width', 'LIKE', '%' .$search_width. '%');
                                    })
                                    ->orWhere('product_variation_values.name', 'LIKE', '%' .$search. '%')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.type) as type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                 GROUP_CONCAT(DISTINCT product_entries.pcs_per_cartoon) as pcs_per_cartoon,  
                                                 GROUP_CONCAT(DISTINCT sizes.height, " X " ,sizes.width) as size_name,  
                                                 GROUP_CONCAT(DISTINCT sizes.height) as height,  
                                                 GROUP_CONCAT(DISTINCT sizes.width) as width,
                                                 GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name 
                                                ')
                                    ->get();
                                    
                                    $fetchData  = $fetchData->where('status', 1);
        }


        foreach ($fetchData as $key => $value)
        {   
            if ($value['product_type'] == 2)
            {
                $variations  = ' - ' . $value['variations'];
            }
            else
            {
                $variations  = '';
            }

            if (($value['height'] != null) && ($value['width'] != null))
            {
                $dimension  = ' - ' . $value['height'].'X'.$value['width'];
            }
            else
            {
                $dimension  = '';
            }

            if ($value['brand_name'] != null)
            {
                $brand  = ' - ' . $value['brand_name'];
            }
            else
            {
                $brand  = '';
            }

            if ($value['product_code'] != null)
            {
                $code  = $value['product_code'] . ' - ';
            }
            else
            {
                $code  = '';
            }

            $name   = $code . $value['category_name'] . $dimension.$variations .$brand. ' - '.$value['name'];

            if ($product_type == 2)
            {
                $data[] = array("id"=>$value['id'], "text"=>$name);
            }
            else
            {   
                if ($value['type'] == $product_type)
                {
                    $data[] = array("id"=>$value['id'], "text"=>$name);
                }
            }
        }

        return Response::json($data);
    }

    public function getConversionParam($product_entry_id, $conversion_unit_id)
    {
        $data       = UnitConversions::where('unit_conversions.product_entry_id', $product_entry_id)
                                    ->where('unit_conversions.converted_unit_id', $conversion_unit_id)
                                    ->selectRaw('unit_conversions.*')
                                    ->first();

        return Response::json($data);
    }

    public function calculateOpeningBalance($supplier_id)
    {
        $bills                  = Bills::where('bills.vendor_id', $supplier_id)
                                            ->select('bills.*')
                                            ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->leftjoin('purchase_return', 'purchase_return.id', 'payment_entries.purchase_return_id')
                                                ->where('payments.customer_id', $supplier_id)
                                                ->where('payments.type', 3)
                                                ->select('payment_entries.*',
                                                         'payments.customer_id as customer_id')
                                                ->get();

        $due_amount             = $bills->sum('due_amount');
        $return_amount          = $bills->sum('return_amount');
        $return_paid_amount     = $payment_entries_return->sum('amount');
        $return_due_amount      = $return_amount - $return_paid_amount;

        $balance                = $due_amount + $return_due_amount; 

        return Response::json($balance);
    }
}
