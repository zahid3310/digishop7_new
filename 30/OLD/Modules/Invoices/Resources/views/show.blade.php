<!DOCTYPE html >
<html>

<head>
    <meta charset="utf-8">
    <title>Cash Memo</title>
    <link href="https://fonts.maateen.me/bangla/font.css" rel="stylesheet">

    <style>
        *{margin:0;padding:0;outline:0}

        body {
            font-size:14px;
            line-height:18px;
            color:#000;
            height:292mm;
            width: 203mm;/*297*/
            margin:0 auto;
            font-family: 'Bangla', Arial, sans-serif !important;  
        }

        table,th {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 10px;
        }

        td {
            border-left: 1px solid black;
            border-right: 1px solid black;
            padding: 10px;
        }

        .bg_templete{
            background:url(images/bg3.png) no-repeat;
            background-size: 100%;
            -webkit-print-color-adjust: exact;
        }

        .templete{
            width:90%;
            background:none;
            margin:0 auto;
        }
        
        .clear{
            overflow:hidden;
        }

        .text-center{
            text-align: center;
        }

        .text-end{
            text-align: end;
        }

        .item2 {
            grid-area: com_logo;
        }

        .item3 {
            grid-area: com_info;
        }

        .item4 {
            grid-area: other_info;
        }

        .grid-container {
            display: grid;
            grid-template-areas: "com_logo com_info other_info ";
            /*padding-top: 10px;*/
        }

        .grid-container div {
            text-align: center;
        }

        .grid-container-aline-end {
            text-align: end !important;
        }

        .grid-container-aline-start {
            text-align: start !important;
        }

        .com_logo_img {
            height: 80px;
            width: 80px;
        }

        .tr-height{
            padding: 10px;
        }

        .item11 { grid-area: sig_text2; }
        .item22 { grid-area: some_info; }
        .item33 { grid-area: sig1; }
        .item44 { grid-area: sig2; }
        .item55 { grid-area: sig_text1; }

        .grid-container-1 {
            display: grid;
            grid-template-areas:
            ' sig1 sig1 some_info some_info sig2 sig2'
            ' sig_text1 sig_text1 some_info some_info sig_text2 sig_text2';
        }

        .signaturesection1 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 45px;
        }

        .signaturesection2 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 45px;
        }

        @page {
            size: A4;
            page-break-after: always;
        }
    </style>
</head>

<body>
    <div class="bg_templete clear">

        <div class="templete clear">

            <br>
            <br>
            <p style="line-height: 2;text-align: center"><span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 20px">ক্যাশ মেমো</span></p>
            <div class="grid-container">
                <div class="item2 grid-container-aline-start">
                    <img class="com_logo_img" src="{{ url('public/'.userDetails()->logo) }}"alt="qr sample"
                    />
                </div>

                <div class="item3">
                    <h2 style="line-height: 1;font-size: 60px">মোসলেম এন্টারপ্রাইজ</h2>
                    <h3 style="line-height: 1;font-size: 21px; text-align: center;">প্রো: মোঃ মোসলেমুল হক</h3>
                    
                </div>

                <div class="item4 grid-container-aline-end">

                    <p style="padding-right: 25px;line-height: 1;font-size: 28px">ডিলার:</p>
                    <p style="line-height: 1;font-size: 11px;">গাজী পাইপ, ট্যাংক এন্ড মটর</p>
                    <p style="line-height: 1;font-size: 11px;">আর. এফ. এল.</p>
                    <p style="line-height: 1;font-size: 11px;">পেডরোলো মোটর</p>
                    <p style="line-height: 1;font-size: 11px;">মেঘনা P.V.C পাইপ</p>
                    <p style="line-height: 1;font-size: 11px;">স্টার টাইলস এন্ড সেনেটারী</p>
                    <p style="line-height: 1;font-size: 11px;">R.A.K টাইলস এন্ড সেনেটারী</p>
                    <p style="line-height: 1;font-size: 11px;">A.B.C টাইলস এন্ড সেনেটারী</p>
                    <p style="line-height: 1;font-size: 11px;">Huathai টাইলস</p>
                    
                </div>
            </div>

            
            <p style="line-height: 1;font-size: 20px; text-align: center;">অতি সুলভ মুল্যে প্লাস্টিক পাইপ, টিউবওয়েল এর পার্টস, গাজী ট্যাংক, প্লাস্টিক দ্রজা, বেসিন, কমেট, ইলেকট্রিক ইত্যাদি পাইকারি ও খুচরা  বিক্রেতা ও সরবরাহকারী</p>

            <div style="padding-bottom: 10px" class="text-center">
                <h4 style="line-height: 1;font-size: 18px">মোবারক চেয়ারম্যান সুপার মার্কেট, নবীনগর রোড, কোম্পানীগঞ্জ বাজার, মুরাদনগর, কুমিল্লা । ভ্যাট নং : ২১৪১১৬০৭৪৭</h4>
                <p style="line-height: 1;font-size: 20px">মোসলেমুল হক: ০১৭১৩-৬২৩৬৪৩, পারভেজুল হক: ০১৬২৭-১১১০০০</p>
                <p style="line-height: 1;font-size: 20px">শরিফ: ০১৮৩৮-২৪১৮২০, শাহিন: ০১৬২৬-৭১০৭৬৫, শফিক: ০১৮৭৪-২৩০৪৯২</p>
            </div>

            <hr>
            
            <div style="padding-top: 5px;text-align: center" class="col-md-12"><input type="checkbox"> Customer Copy &nbsp;&nbsp;&nbsp;<input type="checkbox"> Office Copy</div>

            <div style="font-size: 18px;padding-top: 5px;line-height: 1.5">
                <strong>Memo No &nbsp;:  </strong>{{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }} <strong style="float: right;">Date : <span>{{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}</span></strong>
            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong>{{ $invoice['customer_name'] != null ? $invoice['customer_name'] : $invoice['contact_name'] }}
            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Address &nbsp;&nbsp;: </strong>{{ $invoice['customer_address'] != null ? $invoice['customer_address'] : $invoice['contact_address'] }}
            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Phone &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong>{{ $invoice['customer_phone'] != null ? $invoice['customer_phone'] : $invoice['contact_phone'] }}
            </div>

            @if($invoice['product_type'] == 2 || $invoice['product_type'] == 1)
            <div style="padding-top: 10px;padding-bottom: 20px">
                <table style="width: 100%">
                    <tr>
                        <th style="font-size: 18px;width: 5%">নং</th>
                        <th style="font-size: 18px;width: 60%">মালের বিবরণ</th>
                        <th style="font-size: 18px">বর্গফুট</th>
                        <th style="font-size: 18px">কার্টুন</th>
                        <th style="font-size: 18px">পিছ</th>
                        <th style="font-size: 18px">দর</th>
                        <th style="font-size: 18px;width: 10%">টাকা</th>
                    </tr>

                    @if($entries->count() > 0)

                    <?php
                        $total_amount                           = 0;
                        $total_quentity                         = 0;
                        $total_cartoon                          = 0;
                        $total_pcs                              = 0;
                    ?>

                    @foreach($entries as $key => $value)
                    <?php
                        $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                        $variation_name = ProductVariationName($value['product_entry_id']);
                        $total_quentity     = $value['quantity'] + $total_quentity;
                        $total_cartoon      = $value['cartoon'] + $total_cartoon;
                        $total_pcs          = $value['pcs'] + $total_pcs;

                        if ($value['product_code'] != null)
                        {
                            $productCode  = ' - '.$value['product_code'];
                        }
                        else
                        {
                            $productCode  = '';
                        }

                        if ($value['product_name'] != null)
                        {
                            $category  = ' - '.$value['product_name'];
                        }
                        else
                        {
                            $category  = '';
                        }

                        if ($value['brand_name'] != null)
                        {
                            $brandName  = $value['brand_name'];
                        }
                        else
                        {
                            $brandName  = '';
                        }

                        if (($value['height'] != null) && ($value['width'] != null))
                        {
                            $dimension  = ' - '.$value['height'] . ' X ' . $value['width'];
                        }
                        else
                        {
                            $dimension  = '';
                        }

                        if ($value['unit_name'] != null)
                        {
                            $unit  = ' '.$value['unit_name'];
                        }
                        else
                        {
                            $unit  = ' SFT';
                        }

                        if ($variation_name != null)
                        {
                            $variation  = ' '.$variation_name;
                        }
                        else
                        {
                            $variation  = '';
                        }

                        $pre_dues = $value['contact_type'] != 4 ? previousDues($invoice['customer_id']) : 0;
                        $net_paya = $value['contact_type'] != 4 ? round($total_amount, 2) : 0;
                        $paid     = $value['contact_type'] != 4 ? round($invoice['invoice_amount'] - $invoice['due_amount'], 2) : 0;
                        $dues     = $value['contact_type'] != 4 ? round($net_paya - $paid, 2) : 0;
                    ?>

                    <tr class="tr-height">
                        <td style="text-align: center">{{ $key + 1 }}</td>
                        <!--<td style="padding-left: 30px">{{ $brandName . $productCode . $category . ' - ' . $value['product_entry_name'] . $dimension . $variation }}</td>-->
                        <td style="padding-left: 30px">{{ $brandName . $productCode . $category }}</td>
                        @if($value['type'] == 1)
                        <td style="text-align: center">{{ $value['quantity'] }}</td>
                        <td style="text-align: center">{{ $value['cartoon'] }}</td>
                        <td style="text-align: center">{{ $value['pcs'] }}</td>
                        @else
                        <td style="text-align: center"></td>
                        <td style="text-align: center"></td>
                        <td style="text-align: center">{{ $value['quantity'] . $unit }}</td>
                        @endif
                        <td style="text-align: center">{{ $value['contact_type'] != 4 ? $value['rate'] : '' }}</td>
                        <td style="text-align: center">{{ $value['contact_type'] != 4 ? round($value['total_amount'], 2) : '' }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <?php
                        if ($invoice['vat_type'] == 0)
                        {
                            $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                        }
                        else
                        {
                            $vat_amount  = $invoice['total_vat'];
                        }

                        if ($invoice['total_discount_type'] == 0)
                        {
                            $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                        }
                        else
                        {
                            $discount_on_total_amount  = $invoice['total_discount_amount'];
                        }
                    ?>

                    <tr>
                        <th style="text-align: left" colspan="2">In Words : <span style="font-weight: normal;">{{ $value['contact_type'] != 4 ? numberTowords(round($net_paya - $invoice['total_discount'] + $invoice['labour_cost'])) : '' }}</span></th>
                        <th>{{$total_quentity}}</th>
                        <th>{{$total_cartoon}}</th>
                        <th>{{$total_pcs}}</th>
                        <th style="text-align: right"><strong>Payable</strong></th>
                        <th style="text-align: center">{{ $net_paya != 0 ? round($net_paya - $invoice['total_discount'], 2) : '' }}</th>
                    </tr>

                    <tr>
                        <th style="text-align: right" colspan="6"><strong>Labor Cost</strong></th>
                        <th style="text-align: center">{{ round($invoice['labour_cost'], 2) }}</th>
                    </tr>

                    <tr>
                        <th style="text-align: right" colspan="6"><strong>Total</strong></th>
                        <th style="text-align: center">{{ round($net_paya - $invoice['total_discount'] + $invoice['labour_cost'], 2) }}</th>
                    </tr>

                    <tr style="display: none">
                        <th style="text-align: right" colspan="6"><strong>Vat</strong></th>
                        <th style="text-align: center">{{ round($vat_amount, 2) }}</th>
                    </tr>

                    <tr style="display: none">
                        <th style="text-align: right" colspan="6"><strong>Total Discount</strong></th>
                        <th style="text-align: center">{{ round($discount_on_total_amount + $invoice['total_discount'], 2) }}</th>
                    </tr>

                    <tr style="display: none">
                        <th style="text-align: right" colspan="6"><strong>Net Payable</strong></th>
                        <th style="text-align: center">{{ $invoice['total_discount'] - $discount_on_total_amount + $vat_amount != 0 ? round($net_paya - $invoice['total_discount'] - $discount_on_total_amount + $vat_amount) : '' }}</th>
                    </tr>

                    <tr style="display: none">
                        <th style="text-align: right" colspan="6"><strong>Paid</strong></th>
                        <th style="text-align: center">{{ $paid != 0 ? round($paid, 2) : '' }}</th>
                    </tr>

                    <tr style="display: none">
                        <th style="text-align: right" colspan="6"><strong>Current Due</strong></th>
                        <th style="text-align: center">{{ $dues - $discount_on_total_amount - $invoice['total_discount'] != 0 ? round($dues - $discount_on_total_amount - $invoice['total_discount'], 2) : '' }}</th>
                    </tr style="display: none">

                    <tr style="display: none">
                        <th style="text-align: right" colspan="6"><strong>Previous Due</strong></th>
                        <th style="text-align: center">{{ $pre_dues - $invoice['due_amount'] != 0 ? round($pre_dues - $invoice['due_amount'], 2) : '' }}</th>
                    </tr style="display: none">

                    <tr style="display: none">
                        <th style="text-align: right" colspan="6"><strong>Total Due</strong></th>
                        <th style="text-align: center">{{  $dues - $discount_on_total_amount - $invoice['total_discount'] +  $pre_dues - $invoice['due_amount'] != 0 ? $dues - $discount_on_total_amount - $invoice['total_discount'] +  $pre_dues - $invoice['due_amount'] : ''  }}</th>
                    </tr>
                </table>
            </div>
            @else
            <div style="padding-top: 10px;padding-bottom: 20px">
                <table style="width: 100%">
                    <tr>
                        <th style="font-size: 18px;width: 5%">নং</th>
                        <th style="font-size: 18px;width: 60%">মালের বিবরণ</th>
                        <th style="font-size: 18px">পরিমাণ</th>
                        <th style="font-size: 18px">দর</th>
                        <th style="font-size: 18px;width: 10%">টাকা</th>
                    </tr>

                    @if($entries->count() > 0)

                    <?php
                        $total_amount                           = 0;
                        $total_quentity                         = 0;
                        $total_cartoon                          = 0;
                        $total_pcs                              = 0;
                    ?>

                    @foreach($entries as $key => $value)
                    <?php
                        $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                        $variation_name = ProductVariationName($value['product_entry_id']);
                        $total_quentity     = $value['quantity'] + $total_quentity;
                        $total_cartoon      = $value['cartoon'] + $total_cartoon;
                        $total_pcs          = $value['pcs'] + $total_pcs;

                        if ($value['product_code'] != null)
                        {
                            $productCode  = ' - '.$value['product_code'];
                        }
                        else
                        {
                            $productCode  = '';
                        }

                        if ($value['product_name'] != null)
                        {
                            $category  = ' - '.$value['product_name'];
                        }
                        else
                        {
                            $category  = '';
                        }

                        if ($value['brand_name'] != null)
                        {
                            $brandName  = $value['brand_name'];
                        }
                        else
                        {
                            $brandName  = '';
                        }

                        if (($value['height'] != null) && ($value['width'] != null))
                        {
                            $dimension  = ' - '.$value['height'] . ' X ' . $value['width'];
                        }
                        else
                        {
                            $dimension  = '';
                        }

                        if ($value['unit_name'] != null)
                        {
                            $unit  = ' '.$value['unit_name'];
                        }
                        else
                        {
                            $unit  = ' SFT';
                        }

                        if ($variation_name != null)
                        {
                            $variation  = ' '.$variation_name;
                        }
                        else
                        {
                            $variation  = '';
                        }

                        $pre_dues = $value['contact_type'] != 4 ? previousDues($invoice['customer_id']) : 0;
                        $net_paya = $value['contact_type'] != 4 ? round($total_amount, 2) : 0;
                        $paid     = $value['contact_type'] != 4 ? round($invoice['invoice_amount'] - $invoice['due_amount'], 2) : 0;
                        $dues     = $value['contact_type'] != 4 ? round($net_paya - $paid, 2) : 0;
                    ?>

                    <tr class="tr-height">
                        <td style="text-align: center">{{ $key + 1 }}</td>
                        <!--<td style="padding-left: 30px">{{ $brandName . $productCode . $category . ' - ' . $value['product_entry_name'] . $dimension . $variation }}</td>-->
                        <td style="padding-left: 30px">{{ $brandName . $productCode . $category }}</td>
                        <td style="text-align: center">{{ $value['quantity'] . $unit }}</td>

                        <td style="text-align: center">{{ $value['rate'] }}</td>
                        <td style="text-align: center">{{ round($value['total_amount'], 2) }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <?php
                        if ($invoice['vat_type'] == 0)
                        {
                            $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                        }
                        else
                        {
                            $vat_amount  = $invoice['total_vat'];
                        }

                        if ($invoice['total_discount_type'] == 0)
                        {
                            $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                        }
                        else
                        {
                            $discount_on_total_amount  = $invoice['total_discount_amount'];
                        }
                    ?>

                    <tr>
                        <th style="text-align: left" colspan="2">In Words : <span style="font-weight: normal;">{{ numberTowords(round($net_paya - $invoice['total_discount'] + $invoice['labour_cost'])) }}</span></th>
                        <th style="text-align: right" colspan="2"><strong>Payable</strong></th>
                        <th style="text-align: center">{{ $net_paya != 0 ? round($net_paya - $invoice['total_discount'], 2) : '' }}</th>
                    </tr>

                    <tr>
                        <th style="text-align: right" colspan="4"><strong>Labor Cost</strong></th>
                        <th style="text-align: center">{{ round($invoice['labour_cost'], 2) }}</th>
                    </tr>

                    <tr>
                        <th style="text-align: right" colspan="4"><strong>Total</strong></th>
                        <th style="text-align: center">{{ round($net_paya - $invoice['total_discount'] + $invoice['labour_cost'], 2) }}</th>
                    </tr>

                    <tr style="display: none">
                        <th style="text-align: right" colspan="4"><strong>Vat</strong></th>
                        <th style="text-align: center">{{ round($vat_amount, 2) }}</th>
                    </tr>

                    <tr style="display: none">
                        <th style="text-align: right" colspan="4"><strong>Total Discount</strong></th>
                        <th style="text-align: center">{{ round($discount_on_total_amount + $invoice['total_discount'], 2) }}</th>
                    </tr>

                    <tr style="display: none">
                        <th style="text-align: right" colspan="4"><strong>Net Payable</strong></th>
                        <th style="text-align: center">{{ $invoice['total_discount'] - $discount_on_total_amount + $vat_amount != 0 ? round($net_paya - $invoice['total_discount'] - $discount_on_total_amount + $vat_amount) : '' }}</th>
                    </tr>

                    <tr style="display: none">
                        <th style="text-align: right" colspan="4"><strong>Paid</strong></th>
                        <th style="text-align: center">{{ $paid != 0 ? round($paid, 2) : '' }}</th>
                    </tr>

                    <tr style="display: none">
                        <th style="text-align: right" colspan="4"><strong>Current Due</strong></th>
                        <th style="text-align: center">{{ $dues - $discount_on_total_amount - $invoice['total_discount'] != 0 ? round($dues - $discount_on_total_amount - $invoice['total_discount'], 2) : '' }}</th>
                    </tr style="display: none">

                    <tr style="display: none">
                        <th style="text-align: right" colspan="4"><strong>Previous Due</strong></th>
                        <th style="text-align: center">{{ $pre_dues - $invoice['due_amount'] != 0 ? round($pre_dues - $invoice['due_amount'], 2) : '' }}</th>
                    </tr style="display: none">

                    <tr style="display: none">
                        <th style="text-align: right" colspan="4"><strong>Total Due</strong></th>
                        <th style="text-align: center">{{  $dues - $discount_on_total_amount - $invoice['total_discount'] +  $pre_dues - $invoice['due_amount'] != 0 ? $dues - $discount_on_total_amount - $invoice['total_discount'] +  $pre_dues - $invoice['due_amount'] : ''  }}</th>
                    </tr>
                </table>
            </div>
            @endif

            <div class="grid-container-1">

                <div class="item33"></div>  
                <div class="item55"><p class="signaturesection1" style="font-size: 18px;text-align: center">ক্রেতার স্বাক্ষর</p></div>

                <div class="item22" style="text-align: center;">
                    <h3 style="font-size: 18px">বিঃ দ্রঃ বিক্রিত মাল ফেরত যোগ্য নহে । </h3>
                    <div>
                        <p style="font-size: 25px;color: blue">ধন্যবাদ আবার আসবেন </p>
                    </div>
                </div>

                <div class="item44"></div>
                <div class="item1"><p class="signaturesection2" style="font-size: 18px;text-align: center">বিক্রেতার স্বাক্ষর</p></div>
            </div>
        </div>
    </div>
</body>

</html>