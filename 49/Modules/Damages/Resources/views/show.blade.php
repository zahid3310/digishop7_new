@extends('layouts.app')

@section('title', 'Show')

<style type="text/css">

.jol {
    position: absolute;
    left: 35%;
    height: 173px;
    top: 215px;
    opacity: 1.08;
}

address {
    margin-bottom: 0!important;
}



 @media print {
     
        address {
            margin-bottom: 0!important;
        }

        .table td, .table th {
            padding: 0.35rem!important;
 
        }
        
    }

</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Booking</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Booking</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-6 col-6">
                                        <address>
                                            <strong>Damage Date:</strong>{{ date('d-m-Y', strtotime($find_damage['damage_date'])) }}<br>

                                            <strong>Total Amount:</strong>{{ $find_damage['total_amount'] }}<br>
                                        </address>
                                    </div>
                                </div>

                                <br>
                                <br>

            
                                <div class="row">
                                    <div class="table-responsive test">
                                    <table class="table table-nowrap table-bordered" style="line-height: 13px;">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th class="text-center">Description</th>
                                                <th class="text-center">Buy Price</th>
                                                <th class="text-center">Rate</th>
                                                <th class="text-center">Quantity</th>
                                                <th class="text-center">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">

                                            @if(!empty($find_damage_entries) && ($find_damage_entries->count() > 0))

                                            <?php $sub_total = 0; ?>

                                            @foreach($find_damage_entries as $key => $value)
                                            <tr>
                                                <td class="text-center">{{ $value['item_name'] }}</td>
                                                <td class="text-right">{{ $value['buy_price'] }}</td>
                                                <td class="text-right">{{ $value['rate'] }}</td>
                                                <td class="text-center">{{ $value['quantity'] }} {{ $value['unit_name'] }}</td>
                                                <td class="text-right">{{ number_format($value['total_amount'],2,'.',',') }}</td>
                                            </tr>

                                            <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                            @endforeach
                                            @endif
                                            
                                        </tbody>
                                    </table>
                                </div>
                                    </div>
                                  

                                </div>
                                



                            </div>
                        </div>
                    </div> <!-- end col -->
                    
                   
    
                </div>


                
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection