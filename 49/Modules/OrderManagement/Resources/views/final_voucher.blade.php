@extends('layouts.app')

@section('title', 'Final Voucher')

<style type="text/css">

.jol {
    position: absolute;
    left: 28%;
    height: 270px;
    top: 110px;
    opacity: 0.30;
    display: none;
}

address {
    margin-bottom: 0!important;
}


.underline {
  text-decoration: none;
  background-image: linear-gradient(to right, #000 75%, transparent 75%);
  background-position: 0 1.04em;
  background-repeat: repeat-x;
  background-size: 8px 3px;
}

.table td, .table th {
    padding: 1.00rem!important;
}

.table-bordered thead td, .table-bordered thead th {
    border-bottom-width: 1px!important;
}

.table thead th {
    vertical-align: bottom;
    border-bottom: 1px solid #000!important;
}

.table-bordered td, .table-bordered th {
    border: 1px solid #000!important;
}


@media print {

    address {
        margin-bottom: 0!important;
    }

    .table td, .table th {
        padding: 1.00rem!important;
    }

    .table-bordered thead td, .table-bordered thead th {
        border-bottom-width: 1px!important;
    }

    .table thead th {
        vertical-align: bottom;
        border-bottom: 1px solid #000!important;
    }

    .table-bordered td, .table-bordered th {
        border: 1px solid #000!important;
    }
}

</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Booking</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Booking</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- Header Part Start-->
                                <div class="row">
                                    @if($user_info['header_image'] == null)
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="height: 100px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                    </div>
                                        <div class="col-md-8 col-xs-12 col-sm-12">
                                            <i><h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 40px;font-weight: bold;">{{ $user_info['organization_name'] }}</h2></i>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_number'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_email'] }}</p>
                                        </div>
                                        <!-- <div class="col-md-4 col-xs-12 col-sm-12"></div> -->
                                    @else
                                        <img class="float-left" src="{{ url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>
                                <!-- Header Part End-->

                                 <!--<hr> -->
                                
                                <div class="row" style="padding-top: 15px;">
                                    <div style="font-size: 16px;border: 1px solid;" class="col-sm-3 col-3">
                                        <address>
                                            <strong style="padding-right:5px; font-weight: 600!important;">Recipient's Name</strong><br>  {{ $invoice['customer_name'] }}
                                        </address>
                                    </div>

                                    <div style="font-size: 16px;border: 1px solid;" class="col-sm-4 col-4">
                                        <address>
                                            <strong style="padding-right:5px; font-weight: 600!important;">Recipient's Address</strong><br>
                                            @if($invoice['address'] != null)
                                            <?php echo $invoice['address']; ?>
                                            @endif
                                        </address>
                                    </div>

                                    <div style="font-size: 16px;border: 1px solid;" class="col-sm-2 col-2">
                                        <address>
                                            <strong style="padding-right:5px; font-weight: 600!important;">Date <br></strong>
                                            {{ date('D j M, Y', strtotime(date('Y-m-d H:i:s'))) }}
                                        </address>
                                    </div>

                                    <div style="font-size: 16px;border: 1px solid;" class="col-sm-3 col-3 text-sm-right">
                                        <address>
                                            <strong style="padding-right:5px; font-weight: 600!important;">Recipient's Contact</strong><br>
                                            @if($invoice['phone'] != null)
                                            <?php echo $invoice['phone']; ?>
                                            @endif
                                        </address>
                                    </div>

                                </div>
                                <center><p style="text-align: center;font-size: 24px;border: 1px solid;width: 125px;padding: 0px;margin-top: 5px;"><strong>FINAL BILL</strong></p></center>
                                <hr>

                                <div class="row">
                                    <table class="table table-bordered">
                                        <tbody style="line-height: 5px;">

                                            @if($invoice['set_menu_id'] == null)
                                            <tr>
                                                <th style="border: 1px solid #000!important;background: #8080803b!important;width: 50%;">Hall Rent</th>
                                                <td style="border: 1px solid #000!important;width: 50%;">{{$hall_rent }}</td>
                                            </tr>
                                            <tr>
                                                <th style="border: 1px solid #000!important;background: #8080803b!important;width: 50%;">Service Man</th>
                                                <td style="border: 1px solid #000!important;width: 50%;">{{$invoice['service_man_cost']}}</td>
                                            </tr>
                                            @else

                                            <?php 
                                                $menu_price         =  $invoice['set_menu_price'];
                                                $total_guest        =  $invoice['total_guest'];
                                                $food_total_price   = $menu_price * $total_guest;
                                            ?>
                                            <tr>
                                                <th style="border: 1px solid #000!important;background: #8080803b!important;width: 50%;">Food({{$menu_price}}x{{$total_guest}})</th>
                                                <td style="border: 1px solid #000!important;width: 50%;">{{$food_total_price}}</td>
                                            </tr>
                                            @endif

                                            <tr>
                                                <th style="border: 1px solid #000!important;background: #8080803b!important;width: 50%;">Additional Cost</th>
                                                <td style="border: 1px solid #000!important;width: 50%;">
                                                {{$invoice['additional_cost']}} - {{ $invoice['additional_cost_purpose'] }}
                                                </td>
                                            </tr>

                                            <tr>
                                                <th style="border: 1px solid #000!important;background: #8080803b!important;width: 50%;">Total</th>
                                                <td style="border: 1px solid #000!important;width: 50%;">{{$invoice['order_amount'] }}</td>
                                            </tr>


                                            <tr>
                                                <th style="border: 1px solid #000!important;background: #8080803b!important;width: 50%;">Discount</th>
                                                <td style="border: 1px solid #000!important;width: 50%;">
                                                   {{$invoice['discount']}}
                                                </td>
                                            </tr>

                                            <tr style="display:none">
                                                <th style="border: 1px solid #000!important;background: #8080803b!important;width: 50%;">Advance</th>
                                                <td style="border: 1px solid #000!important;width: 50%;">
                                                    @if($invoice['advance'] != null)
                                                        {{$invoice['advance']}}
                                                    @endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <th style="border: 1px solid #000!important;background: #8080803b!important;width: 50%;">Paid</th>
                                                <td style="border: 1px solid #000!important;width: 50%;">
                                                   {{$invoice['cash_given']}}
                                                </td>
                                            </tr>

                                            <tr>
                                                <th style="border: 1px solid #000!important;background: #8080803b!important;">Due</th>
                                                <td style="border: 1px solid #000!important;">
                                                    {{$invoice['order_amount'] - ($invoice['cash_given'] + $invoice['discount']) }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                <br><br><br>

                                <div class="row">
                                    <div class="col-md-6" style="display:none">
                                        <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">Signature of Service recipient </span> <br><br><span style="padding-left:5px;">Date: </span> </h6>
                                    </div>
                                    <div class="col-md-12">
                                        <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">Signature & Seal of the Authority</span> <br><br><span style="padding-left:5px;text-align: left;padding-right: 180px;">Date: </span> </h6>
                                    </div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="position: fixed;bottom: 0">
                                    @if($user_info['footer_image'] != null)
                                        <img class="float-left" src="{{ url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                    
                </div>


            <br><br>
                
            </div>
        </div>
    </div>
    
    <!--Make Payment Model Start-->
        <div id="myModal1" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0" id="myModalLabel">Make New Payment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
        
                    <div class="modal-body">
        
                        <div class="row">
                            <div style="padding: 0px" class="form-group col-md-3 col-3">
                                <label for="example-text-input" class="col-md-12 col-form-label">Receivable</label>
                                <div class="col-md-12">
                                    <input id="receivable" type="text" class="form-control" readonly>
                                </div>
                            </div>
        
                            <div style="padding: 0px" class="form-group col-md-3 col-3">
                                <label for="example-text-input" class="col-md-12 col-form-label">Received</label>
                                <div class="col-md-12">
                                    <input id="received" type="text" class="form-control" readonly>
                                </div>
                            </div>
        
                            <div style="padding: 0px" class="form-group col-md-3 col-3">
                                <label for="example-text-input" class="col-md-12 col-form-label">Dues</label>
                                <div class="col-md-12">
                                    <input id="dues" type="text" class="form-control" readonly>
                                </div>
                            </div>
                            
                             <div style="padding: 0px" class="form-group col-md-3 col-3">
                                <label for="example-text-input" class="col-md-12 col-form-label">Discount</label>
                                <div class="col-md-12">
                                    <input id="discount"  type="text" class="form-control">
                                </div>
                            </div>
                            
                        </div>
        
                        <hr>
        
                        <input id="customer_id_payment" name="customer_id" type="hidden">
                        <input id="find_invoice_id" name="invoice_id" type="hidden">
        
                        <div class="form-group row">
                            <label for="productname" class="col-md-2 col-form-label">Payment Date *</label>
                            <div class="col-md-10">
                                <input style="cursor: pointer" id="payment_date" name="payment_date" type="date" value="<?= date("Y-m-d") ?>" class="form-control" required>
                            </div>
                        </div>
        
                        <div class="inner-repeater">
                            <div style="margin-bottom: 0px !important" data-repeater-list="inner-group" class="inner form-group row">
                                <div class="inner col-lg-12 input_fields_wrap_payment getMultipleRowPayment">
                                    <div style="margin-bottom: 0px !important;" class="row di_payment_0">
                                    </div>
                                </div>
                            </div>
        
                            <div class="row justify-content-end">
                                <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                    <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                                </div>
                            </div>
                        </div>
        
                        <br>
        
                        <div class="modal-footer">
                            <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Make Payment</button>
                            <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Make Payment Model End-->
        
        
        <div id="payslipModal" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Payslip</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div id="printMe">


                      <div class="col-12">
                        <div class="first-section">
                            <div style="width: 20%; float:left">
                                <h1 style="    font-size: 40px; font-weight: 1000;">CASH RECEIPT</h1>
                            </div>
                            <div style="width: 30%; float:right">
                                <div>
                                    <h6>Payment Method:</h6>
                                    <input type="radio" id="cash" name="p-method" value="" checked>
                                    <label for="vehicle1"> Cash </label>
                                    <input type="radio" id="Cheque" name="p-method" value="">
                                    <label for="vehicle2"> Cheque </label>
                                    <input type="radio" id="Card" name="p-method" value="">
                                    <label for="vehicle3"> Card </label>
                                </div>
                                <div>
                                    <label for="lname">Cheque No :</label>
                                    <input type="text" id="#"><br><br>
                                </div>
                            </div>
                            <div style="width: 20%; float:left">
                                <div style="width: 50%; float:left">
                                    <label style="margin-bottom: 15px;" for="fname">Receipt No : </label> <br>
                                    <label style="margin-bottom: 12px;" for="lname">Date :</label> <br>
                                    <label style="margin-bottom: 9px;" for="lname">Event Date :</label> <br>
                                    <label style="margin-bottom: 5px;" for="lname">Shift :</label>
                                </div>
                            
                                <div style="width: 50%; float:left; margin-left:0px;">
                                    <form>
                                        <label style="margin-bottom: 7px;border-style:solid;border-width: 1px;min-width:150px;" for="fname" id="receipt_no"></label> <br>
                                        <label style="margin-bottom: 7px;border-style:solid;border-width: 1px;min-width:150px;" for="fname" id="pay_date"></label> <br>
                                        <label style="margin-bottom: 10px;border-style:solid;border-width: 1px;min-width:150px;" for="fname" id="event_date"></label> <br>                                        
                                        <label style="border-style:solid;border-width: 1px;min-width:150px;" for="fname" id="shift"></label> <br>
                                    </form>
                                </div>
                            </div>
                            
                        </div>
                        <div class="second-section">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td><strong>From: <span id="payslip_from_name"></span> </strong></td>
                                        <td style="width: 40%"> <strong>Amount: &nbsp;  <span id="pay_amount"></span> </strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"> <strong>Amount in word: &nbsp; <span style="text-transform: capitalize;" id="amount_in_words"></span> </strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"> <strong>Purpose: <span id="purpose"> </strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="third-section">
                            <div style="width: 40%; float:left">
                                <img style="height: 100px;padding-top: 10px;text-align: left" src="{{ url('public/logo-center.png') }}">
                            </div>
                            <div style="width: 40%; float:left">
                                <address>
                                    19 Doyel, Subidbazar, Sylhet <br>
                                    01730591391 <br>
                                    khanspalaceconventionhall@gmail.com <br>
                                    www.kpcbd.com
                                </address>
                    
                            </div>
                            <div style="width: 20%; float:left">
                                <div id="signature-block">
                                    ____________________
                                </div>
                                <div style="text-align: center; margin-top: 20px;">
                                    <p>Signature</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    </div>

                  <br><br>
                        <div class="d-print-none">
                            <div class="float-right">
                                <!-- <button id='btn' class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></button> -->
                                <button onclick="printDiv('printMe')" type="button" class="float-right btn btn-success">Print</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript">
        function findInvoice(invoice_id)
        {
            var site_url        = $('.site_url').val();
            $('.remove_field_payment').click();
            $('#add_field_button_payment').click();

            $.get(site_url + '/ordermanagement/customer-make-payment/' + invoice_id, function(data){

                $("#receivable").empty();
                $("#receivable").val(data.order_amount);
                
                $("#discount").empty();
                $("#discount").val(data.discount);

                $("#received").empty();
                $("#received").val(parseFloat(data.order_amount) - parseFloat(data.due_amount));

                $("#dues").empty();
                $("#dues").val(data.due_amount);

                $("#customer_id_payment").empty();
                $("#customer_id_payment").val(data.customer_id);

                $("#find_invoice_id").empty();
                $("#find_invoice_id").val(invoice_id);
            });
        }

        

        function calculateDues()
        {
            var due_amount  = $("#dues").val();
            var amount      = $("#amount").val();

            if (parseFloat(amount) > parseFloat(due_amount))
            {
                $("#amount").val(due_amount);
            }
        }
    </script>

       <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var invoice_id          = $("#find_invoice_id").val();
            var customer_id         = $("#customer_id_payment").val();
            var receivable          = $("#receivable").val();
            var received            = $("#received").val();
            var discount            = $("#discount").val();
            var payment_date        = $("#payment_date").val();
            var note                = $("#note").val();
            var account_information = $("#account_information").val();
            var site_url            = $('.site_url').val();

            console.log(received);

            var amount_paid = [];
            $('input[name^="amount_paid"]').each(function() {
                amount_paid.push(this.value);
            })

            var paid_through = [];
            $('.paidThrough').each(function() {
                paid_through.push(this.value);
            })

            var note = [];
            $('input[name^="note"]').each(function() {
                note.push(this.value);
            })

            var account_information = [];
            $('input[name^="account_information"]').each(function() {
                account_information.push(this.value);
            })

            if (customer_id == '' || invoice_id == '' || receivable == '' || payment_date == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
    
                type:   'post',
                url:    site_url + '/ordermanagement/pay-customer-bill/payments',
                data:   {  invoice_id : invoice_id,customer_id : customer_id, receivable : receivable,discount: discount, payment_date : payment_date, amount_paid : amount_paid, paid_through : paid_through, note : note, account_information : account_information, _token: '{{csrf_token()}}' },
    
                success: function (data) {
                    if(data != 0)
                    {

                        $("#payData").empty();
                        $('#CloseButton').click();
                        $('.remove_field_payment').click();

                        var site_url  = $('.site_url').val();

                        $.get(site_url + '/ordermanagement/order/list/load', function(data){
                            invoiceList(data);
                        });

                        $.get(site_url + '/ordermanagement/customer-pay-slip/' + invoice_id, function(result){

                                                var a = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];

                        var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

                        function inWords(num) {
                          if ((num = num.toString()).length > 9) return 'overflow';
                          n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
                          if (!n) return;
                          var str = '';
                          str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + a[n[1][1]]) + ' crore ' : '';
                          str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + a[n[2][1]]) + ' lakh ' : '';
                          str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + a[n[3][1]]) + ' thousand ' : '';
                          str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + a[n[4][1]]) + ' hundred ' : '';
                          str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + a[n[5][1]]) : '';
                          return str;
                        } //end of changing number to word

                        var numberWord = inWords(result.amount);


                        $("#payslipModal").modal("toggle");


                        function formatDate(date)
                        {
                            var d       = new Date(date),
                                month   = '' + (d.getMonth() + 1),
                                day     = '' + d.getDate(),
                                year    = d.getFullYear();

                            if (month.length < 2) month = '0' + month;
                            if (day.length < 2) day = '0' + day;

                            return [day, month, year].join('-');
                        }
                             var now = new Date();

                                if(result.data.shift_id == 0)
                                { 
                                    var shift  = 'Day';
                                } 
                                else if(result.data.shift_id == 1)
                                {
                                    var shift  = 'Night';
                                }
                                else
                                {
                                    var shift   = 'Both(Day and Night)';
                                }
                            var amount_in_words  = numberWord + 'Taka Only';

                            $("#payslip_from_name").html(result.data.customer_name);
                            $("#amount_in_words").html(amount_in_words);
                            $("#purpose").html(result.note);
                            $("#pay_amount").html(result.amount);
                            $('#pay_date').html( formatDate(now));
                            $('#event_date').html( formatDate(result.data.program_from_data));
                            $('#shift').html(shift);
                            $('#receipt_no').html(result.data.order_number.padStart(6, '0'));
                        });

                        $('#success_message').show();
                        
                        // location.reload();
                        
                    }
                    else
                    {
                        var site_url  = $('.site_url').val();
                        $.get(site_url + '/ordermanagement/order/list/load', function(data){
                            invoiceList(data);
                        });

                        $('#unsuccess_message').show();
                    }
                }
    
            });
            }
        });    
    </script>

    <script type="text/javascript">
        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>
    
    <script type="text/javascript">
        var max_fields_payment       = 50;                                   //maximum input boxes allowed
        var wrapper_payment          = $(".input_fields_wrap_payment");      //Fields wrapper
        var add_button_payment       = $(".add_field_button_payment");       //Add button ID
        var index_no_payment         = 1;

        //For apending another rows start
        var x = -1;
        $(add_button_payment).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields_payment)
            {   
                x++;

                var serial = x + 1;

                if (serial == 1)
                {
                    var amount_paid_label   = '<label class="hidden-xs" for="productname">Amount Paid *</label>\n';
                    var paid_through_label  = '<label class="hidden-xs" for="productname">Paid Through *</label>\n';
                    var account_info_label  = '<label class="hidden-xs" for="productname">Account Information</label>\n';
                    var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';
                }
                else
                {
                    var amount_paid_label   = '';
                    var paid_through_label  = '';
                    var account_info_label  = '';
                    var note_label          = '';
                    var action_label        = '';
                }

                $('.getMultipleRowPayment').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_payment_'+x+'">' +
                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Amount Paid *</label>\n' +
                                                        amount_paid_label +
                                                        '<input type="number" name="amount_paid[]" class="inner form-control amountPaid" id="amount_paid_'+x+'" value="0" required oninput="checkOverPayment('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid Through *</label>\n' +
                                                        paid_through_label  +
                                                        '<select id="paid_through_'+x+'" style="cursor: pointer" name="paid_through[]" class="form-control paidThrough">\n' +
                                                            '@if(!empty($paid_accounts) && ($paid_accounts->count() > 0))\n' +
                                                            '@foreach($paid_accounts as $key => $paid_account)\n' +
                                                                '<option value="{{ $paid_account['id'] }}">{{ $paid_account['name'] }}</option>\n' +
                                                            '@endforeach\n' +
                                                            '@endif\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Account Information</label>\n' +
                                                        account_info_label +
                                                        '<input type="text" name="account_information[]" class="form-control" id="account_information_'+x+'" placeholder="Account Information"/>\n' + 
                                                    '</div>\n' + 

                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6 form-group">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                        note_label +
                                                        '<input type="text" name="note[]" class="inner form-control" id="note_'+x+'" placeholder="note"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    '<div style="" class="col-lg-1 col-md-2 col-sm-6 col-6 form-group remove_field_payment" data-val="'+x+'">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<input type="button" class="btn btn-primary btn-block inner" value="Delete"/>\n' + 
                                                    '</div>\n' + 
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
                }                                    
        });
        //For apending another rows end

        $(wrapper_payment).on("click",".remove_field_payment", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_payment_'+x).remove(); x--;
        });
    </script>
    
    <script type="text/javascript">
      function printDiv(divName){
          var printContents = document.getElementById(divName).innerHTML;
          var originalContents = document.body.innerHTML;

          document.body.innerHTML = printContents;

          window.print();

          document.body.innerHTML = originalContents;

      }
  </script>
@endsection