@extends('layouts.app')

@section('title', 'Pay Slip')

@section('content')
<style>
    table {
        border-top: 2px solid black;
        border-collapse: collapse"
    }
    table td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    #signature-block {
        text-align: left;
        height: 30px;
        word-spacing: 1px;
        padding-top: 30px;
    }
</style>

    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                </div class="row">
                    <div class="col-12">
                        <div class="first-section">
                            <div style="width: 20%; float:left">
                                <h1 style="    font-size: 40px; font-weight: 1000;">CASH RECEIPT</h1>
                            </div>
                            <div style="width: 30%; float:right">
                                <div>
                                    <h6>Payment Method:</h6>
                                    <input type="radio" id="cash" name="p-method" value="Bike">
                                    <label for="vehicle1"> Cash </label>
                                    <input type="radio" id="Cheque" name="p-method" value="Car">
                                    <label for="vehicle2"> Cheque </label>
                                    <input type="radio" id="Card" name="p-method" value="Boat">
                                    <label for="vehicle3"> Card </label>
                                </div>
                                <div>
                                    <label for="lname">Cheque No :</label>
                                    <input type="text" id="#"><br><br>
                                </div>
                            </div>
                            <div style="width: 20%; float:left">
                                <div style="width: 50%; float:left">
                                    <label style="margin-bottom: 15px;" for="fname">Receipt No :</label> <br>
                                    <label style="margin-bottom: 12px;" for="lname">Date :</label> <br>
                                    <label style="margin-bottom: 9px;" for="lname">Event Date :</label> <br>
                                    <label style="margin-bottom: 5px;" for="lname">Shift :</label>
                                </div>
                            
                                <div style="width: 50%; float:left; margin-left:0px;">
                                    <form>
                                        <label style="margin-bottom: 7px;border-style:solid;border-width: 1px;min-width:150px;" for="fname">{{ str_pad($receipt_no, 6, "0", STR_PAD_LEFT) }}</label> <br>
                                        <label style="margin-bottom: 7px;border-style:solid;border-width: 1px;min-width:150px;" for="fname">{{ date('d-m-Y') }}</label> <br>
                                        <label style="margin-bottom: 10px;border-style:solid;border-width: 1px;min-width:150px;" for="fname">{{ date('d-m-Y', strtotime($data['program_from_data'])) }}</label> <br>
                                        <!--<input style="margin-bottom: 5px;" type="text" value="{{ str_pad($data['order_number'], 6, "0", STR_PAD_LEFT) }}" readonly>-->
                                        <!--<span style="border-style:solid;border-width: 1px;margin-bottom: 5px!important;padding-right:15px;">{{ str_pad($data['order_number'], 6, "0", STR_PAD_LEFT) }}</span><br><br>-->
                                        <!--<input style="margin-bottom: 5px;"  type="text" value="{{ date('d-m-Y') }}" readonly>-->
                                        <!--<span style="border-style:solid;border-width: 1px;margin-bottom: 5px!important;padding-right:15px;">{{ date('d-m-Y') }}</span><br><br>-->
                                        <!--<input style="margin-bottom: 5px;" type="text" value="{{ date('d-m-Y', strtotime($data['program_from_data'])) }}" readonly>-->
                                        <!--<span style="border-style:solid;border-width: 1px;margin-bottom: 5px!important;padding-right:15px;">{{ date('d-m-Y', strtotime($data['program_from_data'])) }}</span><br><br>-->
                                        <?php 
                                                if($data['shift_id'] == 0)
                                                { 
                                                    $shif = 'Day';
                                                } 
                                                elseif($data['shift_id'] == 1)
                                                {
                                                    $shif = 'Night';
                                                }
                                                else
                                                {
                                                    $shif = 'Both(Day and Night)';
                                                }
                                        ?>

                                        <!--<input style="margin-bottom: 5px;" type="text" value="{{$shif}}" readonly>-->
                                        <!--<span style="border-style:solid;border-width: 1px;margin-bottom: 5px!important;padding-right:15px;">{{$shif}}</span>-->
                                        <label style="border-style:solid;border-width: 1px;min-width:150px;" for="fname">{{$shif}}</label> <br>
                                    </form>
                                </div>
                            </div>
                            
                        </div>
                        <div class="second-section">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td><strong>From:</strong> {{$data->customer->name}} </td>
                                        <td style="width: 40%"> <strong>Amount: &nbsp; {{ $amount }} /= </strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"> <strong>Amount in word: &nbsp; {{ numberTowords($amount) }} only.</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"> <strong>Purpose: @if($note == null) @else {{$note}} @endif  </strong></td>
                                        
                                       
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="third-section">
                            <div style="width: 40%; float:left">
                                <img style="height: 100px;padding-top: 10px;text-align: left" src="{{ url('public/logo-center.png') }}">
                            </div>
                            <div style="width: 40%; float:left">
                                <address>
                                    19 Doyel, Subidbazar, Sylhet <br>
                                    01730591391 <br>
                                    khanspalaceconventionhall@gmail.com <br>
                                    www.kpcbd.com
                                </address>
                    
                            </div>
                            <div style="width: 20%; float:left">
                                <div id="signature-block">
                                    ____________________
                                </div>
                                <div style="text-align: center; margin-top: 20px;">
                                    <p>Signature</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="d-print-none">
                            <div class="float-right">
                                <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                            </div>
                        </div>
                                
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection