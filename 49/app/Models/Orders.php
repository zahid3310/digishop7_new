<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = "orders";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }    
    
    public function programType()
    {
        return $this->belongsTo('App\Models\ProgramTypes','order_note_id');
    }

    public function invoiceEntries()
    {
        return $this->hasMany(InvoiceEntries::class, "invoice_id");
    }
}
