@extends('layouts.app')

@section('title', 'Dashboard')

<style type="text/css">
    .card-counter{
        box-shadow: 2px 2px 10px #DADADA;
        margin: 5px;
        padding: 20px 10px;
        background-color: #fff;
        height: 100px;
        border-radius: 5px;
        transition: .3s linear all;
    }

    .card-counter:hover{
        box-shadow: 4px 4px 20px #DADADA;
        transition: .3s linear all;
    }

    .card-counter.primary{
        background-color: #007bff;
        color: #FFF;
    }

    .card-counter.danger{
        background-color: #ef5350;
        color: #FFF;
    }  

    .card-counter.success{
        background-color: #66bb6a;
        color: #FFF;
    }  

    .card-counter.info{
        background-color: #26c6da;
        color: #FFF;
    }  

    .card-counter i{
        font-size: 5em;
        opacity: 0.2;
    }

    .card-counter .count-numbers{
        position: absolute;
        right: 35px;
        top: 20px;
        font-size: 32px;
        display: block;
    }

    .card-counter .count-name{
        position: absolute;
        right: 35px;
        top: 65px;
        font-style: italic;
        text-transform: capitalize;
        display: block;
        font-size: 18px;
    }

    @media screen and (max-width: 48em) {
        .row-offcanvas {
            position: relative;
            -webkit-transition: all 0.25s ease-out;
            -moz-transition: all 0.25s ease-out;
            transition: all 0.25s ease-out;
        }

        .row-offcanvas-left .sidebar-offcanvas {
            left: -33%;
        }

        .row-offcanvas-left.active {
            left: 33%;
            margin-left: -6px;
        }

        .sidebar-offcanvas {
            position: absolute;
            top: 0;
            width: 33%;
            height: 100%;
        }
    }

    @media screen and (max-width: 34em) {
        .row-offcanvas-left .sidebar-offcanvas {
            left: -45%;
        }

        .row-offcanvas-left.active {
            left: 45%;
            margin-left: -6px;
        }

        .sidebar-offcanvas {
            width: 45%;
        }
    }
    
    .card {
        overflow: hidden;
    }
    
    .card-block .rotate {
        z-index: 8;
        float: right;
        height: 100%;
    }
    
    .card-block .rotate i {
        color: rgba(20, 20, 20, 0.15);
        position: absolute;
        left: 0;
        left: auto;
        right: -10px;
        bottom: 0;
        display: block;
        -webkit-transform: rotate(-44deg);
        -moz-transform: rotate(-44deg);
        -o-transform: rotate(-44deg);
        -ms-transform: rotate(-44deg);
        transform: rotate(-44deg);
    }

    .display-1 {
        font-size: 2rem !important;
        color: white !important;
    }

    a, button {
        outline: 0!important;
        /*display: none;*/
    }

    .canvasjs-chart-credit
    {
        display: none !important;
    }
</style>

 <link href="{{ url('public/admin_panel_assets/css/calendar.css') }}" id="app-style" rel="stylesheet" type="text/css" />
 <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" id="app-style" rel="stylesheet" type="text/css" />
 <link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css" id="app-style" rel="stylesheet" type="text/css" />


@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Dashboard</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->
            
                <div class="row">
                     <h1>Calednder</h1>
                     <div class="response"></div>
                     <div id='calendar'></div>  
                </div>
             
              <div class="row" style="display:none">
                <div class="col-md-3" style="display:none">
                    <div class="card-counter success">
                        <i class="fa fa-database"></i>
                        <span id="todaysSales" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Sales</span>
                    </div>
                </div>

                <div class="col-md-3" style="display:none">
                    <div class="card-counter info">
                        <i class="fa fa-users"></i>
                        <span id="todaysSalesReturn" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Sales Return</span>
                    </div>
                </div>

                <div class="col-md-3" style="display:none">
                    <div class="card-counter danger">
                        <i class="fa fa-database"></i>
                        <span id="todaysExpense" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Expense</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter primary">
                        <i class="fa fa-envelope"></i>

                        <?php
                            $url        = "https://esms.mimsms.com/miscapi/C2003959627906a7537345.07339736/getBalance";
                            $response   = file_get_contents($url);
                            $tmt        = substr($response, strrpos($response, ' ' ) + 1);
                            if($tmt == 1003)
                            {
                                $balance = 0;
                            }else
                            {
                                // $balance = floor($tmt/0.65);
                                $balance = 0;
                            }
                        ?>

                        <span class="count-numbers">
                            <h4 style="color: white;font-size: 30;" class="mb-0">{{ $balance }}</h4>
                        </span>
                        <span class="count-name">SMS Balance</span>
                    </div>
                </div>
            </div>

            <div class="row">
                 <!-- Booking Confirm List Start-->
            <div class="card-body table-responsive">
                <br><br>

                <table  class="table table-striped table-bordered" style="width:100%;display:none">
                    <thead>
                        <tr>
                            <th>SL</th>
                            <th>Booking Date</th>
                            <th>Program Date</th>
                            <th>Customer ID</th>
                            <th>Customer</th>
                            <th>Contact No.</th>
                            <th>Booking Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($bookingLists as $key=> $list)
                        <tr>
                            <td>{{$loop->index+1}}</td>
                            <td>{{$list->order_date}}</td>
                            <td>{{$list->program_from_data}}</td>
                            <td>{{$list->customer_id_number}}</td>
                            <td>{{$list->customer_name}}</td>
                            <td>{{$list->customer_phone}}</td>
                            <td>
                                @if($list->status == 1)
                                <span style="color:#00bcd4;;font-weight:bold">Confirm</span>
                                @endif
                            </td>
                            <td><a href="{{route('order_show',$list->id)}}" class="btn btn-primary">View</a></td>
        	                <td style="display:none">
                            	<div class="dropdown">
                                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                        <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" style="">
                                        <a class="dropdown-item" href="{{route('order_show',$list->id)}}">View</a>
                                        <a class="dropdown-item" href="" data-toggle="modal" data-target="#myModal1" onclick="findInvoice({{$list->id}})">Make Payment</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
                <table id="bookingSearch" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>SL</th>
                            <th>Booking Date</th>
                            <th>Program Date</th>
                            <th>Customer ID</th>
                            <th>Customer</th>
                            <th>Contact No.</th>
                            <th>Booking Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($bookingLists as $key=> $list)
                        <tr>
                            <td>{{$loop->index+1}}</td>
                            <td>{{$list->order_date}}</td>
                            <td>{{$list->program_from_data}}</td>
                            <td>{{$list->customer_id_number}}</td>
                            <td>{{$list->customer_name}}</td>
                            <td>{{$list->customer_phone}}</td>
                            <td>
                                @if($list->status == 1)
                                <span style="color:#00bcd4;;font-weight:bold">Confirm</span>
                                @endif
                            </td>
                            <td><a href="{{route('order_show',$list->id)}}" class="btn btn-primary">View</a></td>
                        </tr>
                        @endforeach
                         </tbody>
                    </table>

            </div>
            <!-- Booking Confirm List End -->
            </div>

            <div class="row" style="display:none;">
                <div class="col-md-3">
                    <div class="card-counter success">
                        <i class="fa fa-database"></i>
                        <span id="todaysSales" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Sales</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter info">
                        <i class="fa fa-users"></i>
                        <span id="todaysSalesReturn" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Sales Return</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter danger">
                        <i class="fa fa-database"></i>
                        <span id="todaysExpense" class="count-numbers">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name">Today's Expense</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter primary">
                        <i class="fa fa-envelope"></i>

                        <?php
                            // $url        = "https://esms.mimsms.com/miscapi/C20072945fa0e98b1d9f42.77965009/getBalance";
                            // $response   = file_get_contents($url);
                            // $tmt        = substr($response, strrpos($response, ' ' ) + 1); 
                        ?>

                        <span class="count-numbers">
                            <h4 style="color: white;font-size: 30;" class="mb-0">0</h4>
                        </span>
                        <span class="count-name">SMS Balance</span>
                    </div>
                </div>
            </div>

            <div class="row" style="display:none;">
                <div style="padding-top: 20px !important" class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4 float-sm-left">Top Selling Products</h4>
                            <!-- <div class="float-sm-right">
                                <ul class="nav nav-pills">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Week</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Month</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#">Year</a>
                                    </li>
                                </ul>
                            </div> -->
                            <div class="clearfix"></div>
                            <div id="chartContainer1" style="height: 395px; width: 100%;"></div>
                        </div>
                    </div>
                </div>

                <div style="padding-top: 16px !important" class="col-lg-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card-counter info">
                                <i class="fa fa-users"></i>
                                <span id="customerDues" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Customer Dues</span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card-counter info">
                                <i class="fa fa-users"></i>
                                <span id="supplierDues" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Supplier Dues</span>
                            </div>
                        </div>

                        <div style="padding-top: 15px !important" class="col-md-6">
                            <div class="card-counter success">
                                <i class="fa fa-database"></i>
                                <span id="totalSells" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Total Sales</span>
                            </div>
                        </div>

                        <div style="padding-top: 15px !important" class="col-md-6">
                            <div class="card-counter success">
                                <i class="fa fa-database"></i>
                                <span id="totalPurchase" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Total Purchase</span>
                            </div>
                        </div>

                        <div style="padding-top: 15px !important" class="col-md-6">
                            <div class="card-counter danger">
                                <i class="fa fa-database"></i>
                                <span id="totalCustomers" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Total Customers</span>
                            </div>
                        </div>

                        <div style="padding-top: 15px !important" class="col-md-6">
                            <div class="card-counter danger">
                                <i class="fa fa-database"></i>
                                <span id="totalSuppliers" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Total Suppliers</span>
                            </div>
                        </div>

                        <div style="padding-top: 15px !important" class="col-md-6">
                            <div class="card-counter primary">
                                <i class="fa fa-users"></i>
                                <span id="totalInvoices" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Total Invoices</span>
                            </div>
                        </div>

                        <div style="padding-top: 15px !important" class="col-md-6">
                            <div class="card-counter primary">
                                <i class="fa fa-users"></i>
                                <span id="totalBills" class="count-numbers">
                                    <h4 class="mb-0">0</h4>
                                </span>
                                <span class="count-name">Total Bills</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="display:none;">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4 float-sm-left">Sales Summary</h4>
                            <div class="clearfix"></div>
                            <div id="chartContainer3" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4 float-sm-left">Purchase Summary</h4>
                            <div class="clearfix"></div>
                            <div id="chartContainer4" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Stock Out Products</h4>
                            <div style="height: 334px" class="table-responsive">
                                <table class="table table-centered table-nowrap mb-0">
                                    <thead>
                                        <th>SL.</th>
                                        <th>Code</th>
                                        <th>Name</th>
                                    </thead>

                                    <tbody id="product_list">
                                    </tbody>
                                </table>
                            </div>
                            <!-- end table-responsive -->
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4 float-sm-left">Due Invoices</h4>
                            <div class="float-sm-right">
                                <ul class="nav nav-pills">
                                    <li style="cursor: pointer" class="nav-item">
                                        <a id="DueInvoiceWeek" class="nav-link" onclick="dueInvoiceClick()">Week</a>
                                    </li>
                                    <li style="cursor: pointer" class="nav-item">
                                        <a id="DueInvoiceMonth" class="nav-link" onclick="dueInvoiceClick()">Month</a>
                                    </li>
                                    <li style="cursor: pointer" class="nav-item">
                                        <a id="DueInvoiceYear" class="nav-link" onclick="dueInvoiceClick()">Year</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <div style="height: 334px" class="table-responsive">
                                <table class="table table-centered table-nowrap mb-0">
                                    <thead>
                                        <th>SL.</th>
                                        <th>Date</th>
                                        <th>Invoice#</th>
                                        <th style="text-align: right">Amount</th>
                                    </thead>

                                    <tbody id="invoice_list">
                                    </tbody>
                                </table>
                            </div>
                            <!-- end table-responsive -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--Make Payment Model Start-->
        <div id="myModal1" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0" id="myModalLabel">Make New Payment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
        
                    <div class="modal-body">
        
                        <div class="row">
                            <div style="padding: 0px" class="form-group col-md-3 col-3">
                                <label for="example-text-input" class="col-md-12 col-form-label">Receivable</label>
                                <div class="col-md-12">
                                    <input id="receivable" type="text" class="form-control" readonly>
                                </div>
                            </div>
        
                            <div style="padding: 0px" class="form-group col-md-3 col-3">
                                <label for="example-text-input" class="col-md-12 col-form-label">Received</label>
                                <div class="col-md-12">
                                    <input id="received" type="text" class="form-control" readonly>
                                </div>
                            </div>
        
                            <div style="padding: 0px" class="form-group col-md-3 col-3">
                                <label for="example-text-input" class="col-md-12 col-form-label">Dues</label>
                                <div class="col-md-12">
                                    <input id="dues" type="text" class="form-control" readonly>
                                </div>
                            </div>
                            
                             <div style="padding: 0px" class="form-group col-md-3 col-3">
                                <label for="example-text-input" class="col-md-12 col-form-label">Discount</label>
                                <div class="col-md-12">
                                    <input id="discount"  type="text" class="form-control">
                                </div>
                            </div>
                            
                        </div>
        
                        <hr>
        
                        <input id="customer_id_payment" name="customer_id" type="hidden">
                        <input id="find_invoice_id" name="invoice_id" type="hidden">
        
                        <div class="form-group row">
                            <label for="productname" class="col-md-2 col-form-label">Payment Date *</label>
                            <div class="col-md-10">
                                <input style="cursor: pointer" id="payment_date" name="payment_date" type="date" value="<?= date("Y-m-d") ?>" class="form-control" required>
                            </div>
                        </div>
        
                        <div class="inner-repeater">
                            <div style="margin-bottom: 0px !important" data-repeater-list="inner-group" class="inner form-group row">
                                <div class="inner col-lg-12 input_fields_wrap_payment getMultipleRowPayment">
                                    <div style="margin-bottom: 0px !important;" class="row di_payment_0">
                                    </div>
                                </div>
                            </div>
        
                            <div class="row justify-content-end">
                                <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                    <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                                </div>
                            </div>
                        </div>
        
                        <br>
        
                        <div class="modal-footer">
                            <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Make Payment</button>
                            <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Make Payment Model End-->
    </div>
</div>


@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            var site_url  = $('.site_url').val();

            $.get(site_url + '/dashboard/items', function(data){
    
                var product_list = '';
                var serial       = 0;
                $.each(data.products, function(i, product_data)
                {
                    if (product_data.stock_in_hand == null)
                    {
                        var stock = 0;
                    }
                    else
                    {
                        var stock = product_data.stock_in_hand;
                    }

                    if (product_data.stock_in_hand <= product_data.alert_quantity)
                    {   
                        serial++;

                        product_list += '<tr>' +
                                        '<td style="text-align: left">' +
                                            serial +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            pad(product_data.product_code, 6) +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           product_data.name +
                                        '</td>' +
                                    '</tr>';
                    }
                });

                $("#product_list").empty();
                $("#product_list").append(product_list);

                var top_product_list = '';
                $.each(data.top_products, function(j, top_product_data)
                {
                    var serial1      = parseFloat(j) + 1;

                    if (top_product_data.total_sold == null)
                    {
                        var top_sold = 0;
                    }
                    else
                    {
                        var top_sold = top_product_data.total_sold;
                    }

                    if(j == 0)
                    {
                        $("#top_sell_item_quantity").empty();
                        $("#top_sell_item_quantity").append(top_sold);
                        $("#top_sel_item_name").empty();
                        $("#top_sel_item_name").append(top_product_data.product_name + "<br>" + top_product_data.name);
                    }

                    top_product_list += '<tr>' +
                                            '<td style="width: 80%">' +
                                                '<p class="mb-0">' + top_product_data.product_name + "<br>" + top_product_data.name + '</p>' +
                                            '</td>' +
                                            '<td style="width: 20%">' +
                                               '<h5 class="mb-0">' + top_sold + '</h5>' +
                                            '</td>' +
                                        '</tr>';
                });
                
                $("#top_product_list").empty();
                $("#top_product_list").append(top_product_list);


                var invoice_list     = '';
                var sl               = 0;
                $.each(data.due_invoices, function(i, invoice_data)
                {
                    var show_invoice = site_url + '/invoices/show/' + invoice_data.id;

                    sl++;

                    invoice_list += '<tr>' +
                                        '<td style="text-align: left">' +
                                            sl +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            invoice_data.invoice_date +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            '<a href="'+ show_invoice +'" target="_blank">' +
                                                'INV - ' + pad(invoice_data.invoice_number, 6) +
                                            '</a>' +
                                        '</td>' +
                                        '<td style="text-align: right">' +
                                           invoice_data.due_amount.toFixed() +
                                        '</td>' +
                                    '</tr>';
                });

                $("#invoice_list").empty();
                $("#invoice_list").append(invoice_list);

                $("#totalCustomers").empty();
                $("#totalCustomers").append(data.total_customers);

                $("#totalSuppliers").empty();
                $("#totalSuppliers").append(data.total_suppliers);

                $("#totalProducts").empty();
                $("#totalProducts").append(data.total_products);

                $("#todaysSales").empty();
                $("#todaysSales").append(data.todays_sells.toFixed());

                $("#todaysSalesReturn").empty();
                $("#todaysSalesReturn").append(data.todays_sells_return.toFixed());

                $("#todaysExpense").empty();
                $("#todaysExpense").append(data.todays_expenses.toFixed());

                $("#totalInvoices").empty();
                $("#totalInvoices").append(data.total_invoices);

                $("#totalBills").empty();
                $("#totalBills").append(data.total_bills);

                $("#totalSells").empty();
                $("#totalSells").append(data.total_sells.toFixed());

                $("#totalPurchase").empty();
                $("#totalPurchase").append(data.total_purchase.toFixed());

                $("#customerDues").empty();
                $("#customerDues").append(data.customer_dues.toFixed());

                $("#supplierDues").empty();
                $("#supplierDues").append(data.supplier_dues.toFixed());


                //Top Selling Products Start

                    //Variations
                        if (data.top_products[0].variations != null)
                        {
                            var variation1  = ' - ' + data.top_products[0].variations;
                        }
                        else
                        {
                            var variation1  = '';
                        }

                        if (data.top_products[1].variations != null)
                        {
                            var variation2  = ' - ' + data.top_products[1].variations;
                        }
                        else
                        {
                            var variation2  = '';
                        }

                        if (data.top_products[2].variations != null)
                        {
                            var variation3  = ' - ' + data.top_products[2].variations;
                        }
                        else
                        {
                            var variation3  = '';
                        }

                        if (data.top_products[3].variations != null)
                        {
                            var variation4  = ' - ' + data.top_products[3].variations;
                        }
                        else
                        {
                            var variation4  = '';
                        }

                        if (data.top_products[4].variations != null)
                        {
                            var variation5  = ' - ' + data.top_products[4].variations;
                        }
                        else
                        {
                            var variation5  = '';
                        }

                    var arr_length   = data.top_products.length;
                
                    if (data.top_products[0].total_sold > 0)
                    {
                        var chart = new CanvasJS.Chart("chartContainer1", {
                            // theme: "light2", // "light1", "light2", "dark1", "dark2"
                            // exportEnabled: false,
                            animationEnabled: true,
                            // title: {
                            //   text: "Top Selling Products"
                            // },
                            data: [{
                              type: "pie",
                              startAngle: 25,
                              toolTipContent: "<b>{label}</b>: {y}%",
                              // showInLegend: "true",
                              legendText: "{}",
                              indexLabelFontSize: 14,
                              indexLabel: "{label} : {y}%",
                              dataPoints: [
                                
                                    { y: ((parseFloat(data.top_products[0].total_sold) * 100)/parseFloat(data.top_products_total_sold)).toFixed(2), label: '"'+ data.top_products[0].name + variation1 +'"' },
                                    { y: ((parseFloat(data.top_products[1].total_sold) * 100)/parseFloat(data.top_products_total_sold)).toFixed(2), label: '"'+ data.top_products[1].name + variation2 +'"' },
                                    { y: ((parseFloat(data.top_products[2].total_sold) * 100)/parseFloat(data.top_products_total_sold)).toFixed(2), label: '"'+ data.top_products[2].name + variation3 +'"' },
                                    { y: ((parseFloat(data.top_products[3].total_sold) * 100)/parseFloat(data.top_products_total_sold)).toFixed(2), label: '"'+ data.top_products[3].name + variation4 +'"' },
                                    { y: ((parseFloat(data.top_products[4].total_sold) * 100)/parseFloat(data.top_products_total_sold)).toFixed(2), label: '"'+ data.top_products[4].name +  variation5 +'"' }
                                ]
                            }]
                        });

                        setTimeout(function(){ 
                            chart.render(); }, 
                        3000);

                        function toggleDataSeries(e) {
                            if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                                e.dataSeries.visible = false;
                            }
                            else {
                                e.dataSeries.visible = true;
                            }

                            setTimeout(function(){ 
                                chart.render(); }, 
                            3000);
                        }
                    }
                //Top Selling Products End

                //Sales Summary Chart Start
                    var mounth               = [];
                    var amount               = [];

                    $.each(data.sales_summary,function (e, value) {

                        if (value.month == 1)
                        {
                            var SetMonth = "Jan";
                        }

                        if (value.month == 2)
                        {
                            var SetMonth = "Feb";
                        }

                        if (value.month == 3)
                        {
                            var SetMonth = "Mar";
                        }

                        if (value.month == 4)
                        {
                            var SetMonth = "Apr";
                        }

                        if (value.month == 5)
                        {
                            var SetMonth = "May";
                        }

                        if (value.month == 6)
                        {
                            var SetMonth = "Jun";
                        }

                        if (value.month == 7)
                        {
                            var SetMonth = "Jul";
                        }

                        if (value.month == 8)
                        {
                            var SetMonth = "Aug";
                        }

                        if (value.month == 9)
                        {
                            var SetMonth = "Sep";
                        }

                        if (value.month == 10)
                        {
                            var SetMonth = "Oct";
                        }

                        if (value.month == 11)
                        {
                            var SetMonth = "Nov";
                        }

                        if (value.month == 12)
                        {
                            var SetMonth = "Dec";
                        }

                        mounth.push(SetMonth);
                        amount.push(value.total);
                    });

                    var myChart = echarts.init(document.getElementById('chartContainer3'));

                    colors = ['#00C8C4'];
                    option = {
                        color:colors,
                        tooltip : {
                            trigger: 'axis',
                            
                        },
                        grid: {
                            left: '12%',
                            right: '1%',
                            bottom: '8%',
                            top:'10%',
                        },
                        xAxis : [
                            {
                                type : 'category',
                                data : mounth
                            }
                        ],
                        yAxis : [
                            {
                                type : 'value',
                            }
                        ],
                        series : [
                            {
                                name:'Sales',
                                type:'bar',
                                data:amount
                            }
                        ]
                    };

                    var salesY = option;

                    setTimeout(function(){ 
                        myChart.setOption(salesY); }, 
                    3000);
                //Sales Summary Chart End

                //Purchase Summary Chart Start
                    var mounth               = [];
                    var amount               = [];

                    $.each(data.purchase_summary,function (e, value) {

                        if (value.month == 1)
                        {
                            var SetMonth = "Jan";
                        }

                        if (value.month == 2)
                        {
                            var SetMonth = "Feb";
                        }

                        if (value.month == 3)
                        {
                            var SetMonth = "Mar";
                        }

                        if (value.month == 4)
                        {
                            var SetMonth = "Apr";
                        }

                        if (value.month == 5)
                        {
                            var SetMonth = "May";
                        }

                        if (value.month == 6)
                        {
                            var SetMonth = "Jun";
                        }

                        if (value.month == 7)
                        {
                            var SetMonth = "Jul";
                        }

                        if (value.month == 8)
                        {
                            var SetMonth = "Aug";
                        }

                        if (value.month == 9)
                        {
                            var SetMonth = "Sep";
                        }

                        if (value.month == 10)
                        {
                            var SetMonth = "Oct";
                        }

                        if (value.month == 11)
                        {
                            var SetMonth = "Nov";
                        }

                        if (value.month == 12)
                        {
                            var SetMonth = "Dec";
                        }

                        mounth.push(SetMonth);
                        amount.push(value.total);
                    });

                    var myChart1 = echarts.init(document.getElementById('chartContainer4'));

                    colors = ['#28a745'];
                    option = {
                        color:colors,
                        tooltip : {
                            trigger: 'axis',
                            
                        },
                        grid: {
                            left: '12%',
                            right: '1%',
                            bottom: '8%',
                            top:'10%',
                        },
                        xAxis : [
                            {
                                type : 'category',
                                data : mounth
                            }
                        ],
                        yAxis : [
                            {
                                type : 'value',
                            }
                        ],
                        series : [
                            {
                                name:'Purchase',
                                type:'bar',
                                data:amount
                            }
                        ]
                    };

                    var salesY1 = option;
                    setTimeout(function(){ 
                        myChart1.setOption(salesY1); }, 
                    3000);
                //Purchase Summary Chart End
            });

            $("#DueInvoiceYear").addClass("active"); 
        });
    
        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }

        function dueInvoiceClick()
        {
            var click_id  = $(this).id;

            console.log(click_id);
        }
    </script>
    
    <script type="text/javascript">
        function findInvoice(invoice_id)
        {
            var site_url        = $('.site_url').val();
            $('.remove_field_payment').click();
            $('#add_field_button_payment').click();

            $.get(site_url + '/ordermanagement/customer-make-payment/' + invoice_id, function(data){

                $("#receivable").empty();
                $("#receivable").val(data.order_amount);
                
                $("#discount").empty();
                $("#discount").val(data.discount);

                $("#received").empty();
                $("#received").val(parseFloat(data.order_amount) - parseFloat(data.due_amount));

                $("#dues").empty();
                $("#dues").val(data.due_amount);

                $("#customer_id_payment").empty();
                $("#customer_id_payment").val(data.customer_id);

                $("#find_invoice_id").empty();
                $("#find_invoice_id").val(invoice_id);
            });
        }

        

        function calculateDues()
        {
            var due_amount  = $("#dues").val();
            var amount      = $("#amount").val();

            if (parseFloat(amount) > parseFloat(due_amount))
            {
                $("#amount").val(due_amount);
            }
        }
    </script>
    
        <script type="text/javascript">
        $(document).ready(function () {
            $('#bookingSearch').DataTable();
        });
    </script>
@endsection

@push('scripts')
    <!-- echarts js -->
    <script src="{{ url('public/admin_panel_assets/libs/echarts/echarts.min.js') }}"></script>
    <!-- echarts init -->
    <script src="{{ url('public/admin_panel_assets/js/pages/echarts.init.js') }}"></script>
    <script src="{{ url('public/admin_panel_assets/js/canvasCharts.min.js') }}"></script>
    <script src="{{ url('public/admin_panel_assets/js/calendar.js') }}"></script> 
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script> 

    <script>
        $(document).ready(function () {

            var a = $('.fc-next-button').val();

            console.log()

            var SITEURL  = $('.site_url').val();

            console.log(SITEURL);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var calendar = $('#calendar').fullCalendar({
                editable: true,
                events: SITEURL + "/calendar",
                displayEventTime: true,
                editable: true,
                eventRender: function (event, element, view) {
                    if (event.allDay === 'true') {
                        event.allDay = 'asdasd';
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,

            });
        });
    </script>
    
       <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var invoice_id          = $("#find_invoice_id").val();
            var customer_id         = $("#customer_id_payment").val();
            var receivable          = $("#receivable").val();
            var received            = $("#received").val();
            var discount            = $("#discount").val();
            var payment_date        = $("#payment_date").val();
            var note                = $("#note").val();
            var account_information = $("#account_information").val();
            var site_url            = $('.site_url').val();

            console.log(received);

            var amount_paid = [];
            $('input[name^="amount_paid"]').each(function() {
                amount_paid.push(this.value);
            })

            var paid_through = [];
            $('.paidThrough').each(function() {
                paid_through.push(this.value);
            })

            var note = [];
            $('input[name^="note"]').each(function() {
                note.push(this.value);
            })

            var account_information = [];
            $('input[name^="account_information"]').each(function() {
                account_information.push(this.value);
            })

            if (customer_id == '' || invoice_id == '' || receivable == '' || payment_date == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
    
                type:   'post',
                url:    site_url + '/ordermanagement/pay-customer-bill/payments',
                data:   {  invoice_id : invoice_id,customer_id : customer_id, receivable : receivable,discount: discount, payment_date : payment_date, amount_paid : amount_paid, paid_through : paid_through, note : note, account_information : account_information, _token: '{{csrf_token()}}' },
    
                success: function (data) {
                    if(data != 0)
                    {

                        $("#payData").empty();
                        $('#CloseButton').click();
                        $('.remove_field_payment').click();

                        var site_url  = $('.site_url').val();

                        $.get(site_url + '/ordermanagement/order/list/load', function(data){
                            invoiceList(data);
                        });

                        $.get(site_url + '/ordermanagement/customer-pay-slip/' + invoice_id, function(result){

                        if (result.discount == null) 
                        {
                            var discountAmount = '0.00';
                        }else
                        {
                            var discountAmount = result.discount;
                        }

                        $("#payslipModal").modal("toggle");
                        $("#payData").append(`<tr>
                            <td>`+result.customer_name+`</td>
                            <td>`+result.order_amount+`</td>
                            <td>`+result.cash_given+`</td>
                            <td>`+data.amount+`</td>
                            <td>`+discountAmount+`</td>
                            <td>`+result.due_amount+`</td>
                            <td>`+data.payment_date+`</td>
                            <td>`+data.paid_through_name+`</td>
                            </tr>`);
                        });

                        $('#success_message').show();
                        
                    }
                    else
                    {
                        var site_url  = $('.site_url').val();
                        $.get(site_url + '/ordermanagement/order/list/load', function(data){
                            invoiceList(data);
                        });

                        $('#unsuccess_message').show();
                    }
                }
    
            });
            }
        });    
    </script>

    <script type="text/javascript">
        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>
    
        <script type="text/javascript">
        var max_fields_payment       = 50;                           //maximum input boxes allowed
        var wrapper_payment          = $(".input_fields_wrap_payment");      //Fields wrapper
        var add_button_payment       = $(".add_field_button_payment");       //Add button ID
        var index_no_payment         = 1;

        //For apending another rows start
        var x = -1;
        $(add_button_payment).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields_payment)
            {   
                x++;

                var serial = x + 1;

                if (serial == 1)
                {
                    var amount_paid_label   = '<label class="hidden-xs" for="productname">Amount Paid *</label>\n';
                    var paid_through_label  = '<label class="hidden-xs" for="productname">Paid Through *</label>\n';
                    var account_info_label  = '<label class="hidden-xs" for="productname">Account Information</label>\n';
                    var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';
                }
                else
                {
                    var amount_paid_label   = '';
                    var paid_through_label  = '';
                    var account_info_label  = '';
                    var note_label          = '';
                    var action_label        = '';
                }

                $('.getMultipleRowPayment').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_payment_'+x+'">' +
                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Amount Paid *</label>\n' +
                                                        amount_paid_label +
                                                        '<input type="number" name="amount_paid[]" class="inner form-control amountPaid" id="amount_paid_'+x+'" value="0" required oninput="checkOverPayment('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid Through *</label>\n' +
                                                        paid_through_label  +
                                                        '<select id="paid_through_'+x+'" style="cursor: pointer" name="paid_through[]" class="form-control paidThrough">\n' +
                                                            '@if(!empty($paid_accounts) && ($paid_accounts->count() > 0))\n' +
                                                            '@foreach($paid_accounts as $key => $paid_account)\n' +
                                                                '<option value="{{ $paid_account['id'] }}">{{ $paid_account['name'] }}</option>\n' +
                                                            '@endforeach\n' +
                                                            '@endif\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Account Information</label>\n' +
                                                        account_info_label +
                                                        '<input type="text" name="account_information[]" class="form-control" id="account_information_'+x+'" placeholder="Account Information"/>\n' + 
                                                    '</div>\n' + 

                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6 form-group">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                        note_label +
                                                        '<input type="text" name="note[]" class="inner form-control" id="note_'+x+'" placeholder="note"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    '<div style="" class="col-lg-1 col-md-2 col-sm-6 col-6 form-group remove_field_payment" data-val="'+x+'">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<input type="button" class="btn btn-primary btn-block inner" value="Delete"/>\n' + 
                                                    '</div>\n' + 
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
                }                                    
        });
        //For apending another rows end

        $(wrapper_payment).on("click",".remove_field_payment", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_payment_'+x).remove(); x--;
        });
    </script>
    
    
    

@endpush