<?php

use Illuminate\Support\Facades\Route;
use App\Models\Users;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if (Cache::has('key'))
	{
	   Cache::get('key');
	} 
	else 
	{
	   Cache::put('key', 30, 20);
	}
	
    return view('auth.login');
});

Route::get('/login-page-info', function () {

	$info = Users::find(1);
    $data['organization_name'] = $info['organization_name'];

    return Response::json($data);
});

// Route::get('/login-page-info', 'HomeController@loginPageInfo');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/dashboard/items', 'HomeController@dashboardItems')->name('dashboard_items');
Route::get('/stouck-out-items', 'HomeController@stockOutItems')->name('dashboard_stocK_out_items');
Route::get('/calendar', 'HomeController@calendar');
