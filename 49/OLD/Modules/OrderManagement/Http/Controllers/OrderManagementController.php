<?php

namespace Modules\OrderManagement\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\PaidThroughAccounts;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\UnitConversions;
use App\Models\Users;
use App\Models\AccountTransactions;
use App\Models\Orders;
use App\Models\OrderEntries;
use App\Models\CommunityServices;
use Response;
use DB;
use View;
use Carbon\Carbon;

class OrderManagementController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function serviceIndex()
    {
        $services = CommunityServices::orderBy('created_at', 'ASC')->get();

        return view('ordermanagement::service_index', compact('services'));
    }

    public function serviceStore(Request $request)
    {
        $rules = array(
            'name'      => 'required',
            'price'     => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $service                 = new CommunityServices;
            $service->name           = $data['name'];
            $service->price          = $data['price'];
            $service->description    = $data['description'];
            $service->status         = $data['status'];
            $service->created_by     = $user_id;

            if($request->hasFile('image'))
            {   
                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/products/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $service->image         = $logoUrl;
            }

            if ($service->save())
            {   
                DB::commit();
                return redirect()->route('community_service_index')->with("success","Service Created Successfully !!");
            }
        }catch (\Exception $exception){
            dd($exception);
            DB::rollback();
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function serviceEdit($id)
    {
        $services       = CommunityServices::orderBy('created_at', 'ASC')->get();
        $find_service   = CommunityServices::find($id);

        return view('ordermanagement::service_edit', compact('services', 'find_service'));
    }

    public function serviceUpdate(Request $request, $id)
    {
        $rules = array(
            'name'      => 'required',
            'price'     => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $service                 = CommunityServices::find($id);
            $service->name           = $data['name'];
            $service->price          = $data['price'];
            $service->description    = $data['description'];
            $service->status         = $data['status'];
            $service->updated_by     = $user_id;

            if($request->hasFile('image'))
            {   
                if ($service->image != null)
                {
                    unlink('public/'.$service->image);
                }

                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/products/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $service->image         = $logoUrl;
            }

            if ($service->save())
            {   
                DB::commit();
                return redirect()->route('community_service_index')->with("success","Service Updated Successfully !!");
            }
        }catch (\Exception $exception){

            dd($exception);
            DB::rollback();
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function index()
    {

        $invoices           = Invoices::orderBy('id', 'DESC')->first();
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();
        $products           = Products::select('id', 'name')->get();
        $services           = CommunityServices::select('id', 'name')->get();
        $product_entries    = ProductEntries::where('product_entries.maintain_stock', 0)
                                            ->orWhere(function($query)
                                            {
                                                $query->where('product_entries.maintain_stock', 1)
                                                      ->where('product_entries.stock_in_hand', '>', 0);

                                            })
                                            ->select('id', 'name', 'product_id', 'image')
                                            ->get();
        return view('ordermanagement::index', compact('paid_accounts', 'invoices', 'products', 'product_entries','services'));
    }


    public function orderList()
    {
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                                ->get();
        return view('ordermanagement::order_list', compact('paid_accounts'));
    }

    public function store(Request $request)
    {

        $rules = array(
            'selling_date'          => 'required',
            'customer_id'           => 'required',
            'product_entries.*'     => 'required',
            'amount.*'              => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();

      
        DB::beginTransaction();

        try{

             
            $data_find                        = Orders::orderBy('created_at', 'DESC')->first();
            $order_number                     = $data_find != null ? $data_find['id'] + 1 : 1;

            $order                            = new Orders;;
            $order->order_number              = $order_number;
            $order->customer_id               = $data['customer_id'];
            $order->reference_id              = $data['reference_id'];
            $order->customer_name             = $data['customer_name'];
            $order->customer_phone            = $data['customer_phone'];
            $order->order_date                = date('Y-m-d', strtotime($data['selling_date']));
            $order->program_from_data         = date('Y-m-d', strtotime($data['program_from_data']));
            $order->program_to_data           = date('Y-m-d', strtotime($data['program_to_data']));
            $order->start_time                = $data['start_time'];
            $order->end_time                  = $data['end_time'];
            $order->order_amount              = $data['total_amount'];
            $order->due_amount                = $data['total_amount'];
            $order->order_note                = $data['invoice_note'];
            $order->cash_given                = $data['cash_given'];
            $order->order_status              = 1;
            $order->status                    = 1;
            $order->created_by                = $user_id;

            if ($order->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'        => $order['id'],
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value,
                        // 'conversion_unit_id'=> $data['unit_id'][$key],
                        'customer_id'       => $order['customer_id'],
                        'reference_id'      => $data['reference_id'],
                        'rate'              => $data['rate'][$key],
                        'total_amount'      => $data['amount'][$key],

                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('order_entries')->insert($invoice_entries);

                if (isset($data['amount_paid']))
                {
                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {
                    $update_order_dues                = Orders::find($order['id']);
                    $update_order_dues->due_amount    = $update_order_dues['due_amount'] - $data['amount_paid'][$i];
                    $update_order_dues->save();
                }

                    // $data_find        = Payments::orderBy('id', 'DESC')->first();
                    // $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                    // for($i = 0; $i < count($data['amount_paid']); $i++)
                    // {   
                    //     if ($data['amount_paid'][$i] > 0)
                    //     {   
                    //         $account_transactions[] = [
                    //             'customer_id'           => $data['customer_id'],
                    //             'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                    //             'amount'                => $data['amount_paid'][$i],
                    //             'paid_through_id'       => $data['paid_through'][$i],
                    //             'account_information'   => $data['account_information'][$i],
                    //             'note'                  => $data['note'][$i],
                    //             'type'                  => 0,  // 0 = In , 1 = Out
                    //             'transaction_head'      => 'sales',
                    //             'associated_id'         => $invoice->id,
                    //             'created_by'            => $user_id,
                    //             'created_at'            => date('Y-m-d H:i:s'),
                    //         ];

                    //         $payments = [
                    //             'payment_number'        => $payment_number,
                    //             'customer_id'           => $data['customer_id'],
                    //             'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                    //             'amount'                => $data['amount_paid'][$i],
                    //             'account_information'   => $data['account_information'][$i],
                    //             'paid_through'          => $data['paid_through'][$i],
                    //             'note'                  => $data['note'][$i],
                    //             'type'                  => 0,
                    //             'created_by'            => $user_id,
                    //             'created_at'            => date('Y-m-d H:i:s'),
                    //         ];

                    //         $payment_id = DB::table('payments')->insertGetId($payments);      

                    //         if ($payment_id)
                    //         {   
                    //             $payment_entries = [
                    //                     'payment_id'        => $payment_id,
                    //                     'invoice_id'        => $invoice['id'],
                    //                     'amount'            => $data['amount_paid'][$i],
                    //                     'created_by'        => $user_id,
                    //                     'created_at'        => date('Y-m-d H:i:s'),
                    //             ];

                    //             DB::table('payment_entries')->insert($payment_entries);  
                    //         }

                    //         $update_invoice_dues                = Invoices::find($invoice['id']);
                    //         $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                    //         $update_invoice_dues->save();

                    //         $payment_number++;
                    //     }
                    // }

                    // if (isset($account_transactions))
                    // {
                    //     DB::table('account_transactions')->insert($account_transactions);
                    // }
                }

                DB::commit();

               



                if ($data['print'] == 1)
                {

                $invoice    = Orders::leftjoin('customers', 'customers.id', 'orders.customer_id')
                                    ->select('orders.*',
                                             'customers.name as customer_name',
                                             'customers.address as address',
                                             'customers.phone as phone')
                                    ->find($order['id']);

                $entries    = OrderEntries::leftjoin('community_service', 'community_service.id', 'order_entries.product_entry_id')
                                    ->where('order_entries.invoice_id',$order['id'])
                                    ->select('order_entries.*',
                                             'community_service.name as service_name')
                                            ->get();  
                $user_info  = Users::find(1);

                 return view('ordermanagement::order_show',compact('entries', 'invoice', 'user_info'));

                // return back()->with("success","Sales Created Successfully !!");
                }
                else
                {
                    $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                            ->select('invoices.*',
                                                     'customers.name as customer_name',
                                                     'customers.address as address',
                                                     'customers.phone as phone')
                                            ->find($invoice['id']);

                    $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->where('invoice_entries.invoice_id', $invoice['id'])
                                            ->select('invoice_entries.*',
                                                     'product_entries.product_type as product_type',
                                                     'product_entries.name as product_entry_name',
                                                     'units.name as unit_name',
                                                     'products.name as product_name')
                                            ->get();  
                                 
                    $user_info  = Users::find(1);

                    return View::make('invoices::show_pos')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        $invoice    = Orders::leftjoin('customers', 'customers.id', 'orders.customer_id')
                                    ->select('orders.*',
                                             'customers.name as customer_name',
                                             'customers.address as address',
                                             'customers.phone as phone')
                                    ->find($id);

        $entries    = OrderEntries::leftjoin('community_service', 'community_service.id', 'order_entries.product_entry_id')
                                    ->where('order_entries.invoice_id', $id)
                                    ->select('order_entries.*',
                                             'community_service.name as service_name')
                                    ->get();  
        $user_info  = Users::find(1);;

       return view('ordermanagement::order_show',compact('entries', 'invoice', 'user_info'));
    }

    public function orderListSearch($from_date, $to_date, $customer, $status)
    {
        $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
        $search_by_customer     = $customer != 0 ? $customer : 0;
        $search_by_status      = $status != 0 ? $status : 0;

        $data           = Orders::leftjoin('customers', 'customers.id', 'orders.customer_id')
                                        ->leftjoin('order_entries', 'order_entries.invoice_id', 'orders.id')
                                         ->leftjoin('community_service', 'community_service.id', 'order_entries.product_entry_id')
                                        ->when($search_by_from_date != 0 && $search_by_to_date != 0, function ($query) use ($search_by_from_date, $search_by_to_date) {
                                            return $query->whereBetween('orders.order_date', [$search_by_from_date, $search_by_to_date]);
                                        })
                                        ->when($search_by_customer != 0, function ($query) use ($search_by_customer) {
                                            return $query->where('orders.customer_id', $search_by_customer);
                                        })
                                        ->when($search_by_status != 0, function ($query) use ($search_by_status) {
                                            return $query->where('orders.order_status', $search_by_status);
                                        })
                                        ->where('orders.status', 1)
                                        ->orderBy('orders.created_at', 'DESC')
                                        ->select('orders.*',
                                            'customers.name as customer_name',
                                            'customers.phone as phone','community_service.name as service_name')
                                        ->get();

        return Response::json($data);
    }

    public function orderListLoad()
    {
        $data           = Orders::leftjoin('sales_return', 'sales_return.invoice_id', 'orders.id')
                                    ->leftjoin('order_entries', 'order_entries.invoice_id', 'orders.id')
                                    ->leftjoin('community_service', 'community_service.id', 'order_entries.product_entry_id')
                                    ->leftjoin('customers', 'customers.id', 'orders.customer_id')
                                    ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'orders.id')
                                    ->leftjoin('users', 'users.id', 'orders.created_by')
                                    ->where('orders.type', 1)
                                    ->where('orders.status', 1)
                                    ->orderBy('orders.created_at', 'DESC')
                                    ->select('orders.*',
                                            'sales_return.id as return_id',
                                            'customers.name as customer_name',
                                            'customers.phone as phone',
                                            'users.name as waiter_name','community_service.name as service_name')
                                    ->distinct('orders.id')
                                    ->take(20)
                                    ->get();

        return Response::json($data);
    }

    public function orderCancel($id)
    {
        $order = Orders::findorfail($id);
        $order->order_status = 3;
        $order->save();
        return back();

    }

    public function orderStatusChange(Request $request)
    {
        $order = Orders::findorfail($request->invoice_id);
        $order->order_status = $request->order_status;
        $order->save();
        return back();
    }


    public function edit($id)
    {

        $find_invoice           = Orders::leftjoin('customers', 'customers.id', 'orders.customer_id')
                                            ->select('orders.*',
                                                 'customers.id as customer_id',
                                                 'customers.name as contact_name')
                                            ->find($id);

        $find_invoice_entries   = OrderEntries::leftjoin('customers', 'customers.id', 'order_entries.customer_id')
                                            ->leftjoin('community_service', 'community_service.id', 'order_entries.product_entry_id')
                                            ->where('order_entries.invoice_id', $id)
                                            ->select('order_entries.*',
                                                    'customers.id as customer_id',
                                                    'customers.name as customer_name',
                                                    'community_service.name as item_name')
                                            ->get();


        $entries_count          = $find_invoice_entries->count();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->where('payment_entries.invoice_id', $id)
                                        ->selectRaw('payment_entries.*, 
                                                     payments.paid_through as paid_through_id,
                                                     payments.account_information as account_information,
                                                     payments.payment_date as payment_date,
                                                     payments.note as note')
                                        ->get();

        $payment_entries_count  = $payment_entries->count();

        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();
        
        return view('ordermanagement::edit', compact('find_invoice', 'find_invoice_entries', 'entries_count', 'payment_entries', 'payment_entries_count', 'paid_accounts'));

    }

    public function update(Request $request, $id)
    {
        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();



        DB::beginTransaction();

        try{
         
            
            $order        = Orders::find($id);

            
                 //Calculate Due Amount
            
                $order->customer_id               = $data['customer_id'];


                $order->order_date                = date('Y-m-d', strtotime($data['selling_date']));
                $order->program_from_data         = date('Y-m-d', strtotime($data['program_from_data']));
                $order->program_to_data           = date('Y-m-d', strtotime($data['program_to_data']));
                $order->start_time                = $data['start_time'];
                $order->end_time                  = $data['end_time'];
                $order->order_amount              = $data['total_amount'];
                $order->due_amount                = $data['total_amount'];
                $order->cash_given                = $data['cash_given'];
                $order->updated_by                = $user_id;


            if ($order->save())
            {
                $item_id                = OrderEntries::where('invoice_id', $order['id'])->get();
                $item_delete            = OrderEntries::where('invoice_id', $order['id'])->delete();

                if (isset($data['payment_id']))
                {
                    foreach ($data['payment_id'] as $key_p => $value_p)
                    {
                        $payment_delete  = Payments::where('id', $value_p)->delete();
                    }
                }

                $update_invoice                = Orders::find($order->id);
                $update_invoice->due_amount    = $update_invoice['due_amount'] - $data['cash_given'];
                $update_invoice->updated_by    = $user_id;
                $update_invoice->save();



                foreach ($data['product_entries'] as $key => $value)
                {   

                   
                    $product_buy_price = CommunityServices::find($value);

                    $invoice_entries[] = [
                        'invoice_id'        => $order['id'],
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value,
                        // 'conversion_unit_id'=> $data['unit_id'][$key],
                        'customer_id'       => $order['customer_id'],
                        'rate'              => $data['rate'][$key],
                        'total_amount'      => $data['amount'][$key],

                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),

                    ];
                }

                DB::table('order_entries')->insert($invoice_entries);



                $transaction_delete = AccountTransactions::where('transaction_head', 'sales')->where('associated_id', $order['id'])->delete();


                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return redirect()->route('order_list')->with("success","Sales Updated Successfully !!");
                }
                else
                {
                    if (Auth::user()->service_type == 1)
                    {
                        return redirect()->route('invoices_show', $invoice['id']);
                    }
                    else
                    {
                        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                            ->select('invoices.*',
                                                     'customers.name as customer_name',
                                                     'customers.address as address',
                                                     'customers.phone as phone')
                                            ->find($invoice['id']);

                        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                                ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                ->where('invoice_entries.invoice_id', $invoice['id'])
                                                ->select('invoice_entries.*',
                                                         'product_entries.product_type as product_type',
                                                         'product_entries.name as product_entry_name',
                                                         'units.name as unit_name',
                                                         'products.name as product_name')
                                                ->get();  
                                     
                        $user_info  = Users::find(1);

                        return View::make('invoices::show_pos')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info);
                    }
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }


    public function makePayment($id)
    {
        $data       = Orders::find($id);
        return Response::json($data);
    }

    public function storePayment(Request $request)
    {

        $user_id                    = Auth::user()->id;

        $data                       = $request->all();
        $order                      = Orders::find($data['invoice_id']);


        // $Order_entries              = OrderEntries::where('invoice_id',$data['invoice_id'])->get();
        
        DB::beginTransaction();

        try{


            if (isset($data['amount_paid']))
            {
                $data_find        = Payments::orderBy('id', 'DESC')->first();
                $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                for($i = 0; $i < count($data['amount_paid']); $i++)
                {   
                    if ($data['amount_paid'][$i] > 0)
                    {
                        $payments = [
                            'payment_number'        => $payment_number,
                            'customer_id'           => $data['customer_id'],
                            'payment_date'          => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $data['amount_paid'][$i],
                            'paid_through'          => $data['paid_through'][$i],
                            'account_information'   => $data['account_information'][$i],
                            'note'                  => $data['note'][$i],
                            'type'                  => 0,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $payment_id = DB::table('payments')->insertGetId($payments);      

                        if ($payment_id)
                        {   
                            $payment_entries = [
                                    'payment_id'        => $payment_id,
                                    'booking_id'        => $order['id'],
                                    'amount'            => $data['amount_paid'][$i],
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            DB::table('payment_entries')->insert($payment_entries);  
                        }

                        $update_booking_paid             = Orders::find($data['invoice_id']);
                        $update_booking_paid->due_amount = $update_booking_paid->due_amount - $data['amount_paid'][$i];
                        $update_booking_paid->cash_given = $update_booking_paid->cash_given + $data['amount_paid'][$i];
                        $update_booking_paid->save();

                        $payment_number++;
                    }
                }

            }
            
            DB::commit();
            $data = Payments::select('payments.*','customers.name as customer_name','customers.phone as customer_phone','paid_through_accounts.name as paid_through_name')
            ->leftjoin('customers','customers.id','payments.customer_id')
            ->leftjoin('paid_through_accounts','paid_through_accounts.id','payments.paid_through')
            ->where('payments.id',$payment_id)
            ->first();
            return Response::json($data);
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            dd($exception);
            return Response::json(0);
        }
    }


    
}
