<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="{{ route('home') }}" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                
                <li style="display: none" class="{{ Route::currentRouteName() == 'id_request_index' ? 'mm-active' : '' || 
                Request::getQueryString() == 'request_type=1' ? 'mm-active' : '' ||
                Request::getQueryString() == 'request_type=2' ? 'mm-active' : '' ||
                Request::getQueryString() == 'request_type=3' ? 'mm-active' : '' }}">
                    <a class="{{ Route::currentRouteName() == 'id_request_index' ? 'mm-active' : '' ||
                    Request::getQueryString() == 'request_type=1' ? 'mm-active' : '' ||
                    Request::getQueryString() == 'request_type=2' ? 'mm-active' : '' ||
                    Request::getQueryString() == 'request_type=3' ? 'mm-active' : '' }} waves-effect">
                        <i class="fa fa-list"></i>
                        <span>Orders</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('id_request_index') }}">All Orders</a> </li>
                        <li> <a class="{{ Request::getQueryString() == 'request_type=1' ? 'mm-active' : '' }}" href="{{ route('id_request_create').'?request_type=1' }}">Digishop</a> </li>
                        <li> <a class="{{ Request::getQueryString() == 'request_type=2' ? 'mm-active' : '' }}" href="{{ route('id_request_create').'?request_type=2' }}">Digiedu</a> </li>
                        <li> <a class="{{ Request::getQueryString() == 'request_type=3' ? 'mm-active' : '' }}" href="{{ route('id_request_create').'?request_type=3' }}">Website</a> </li>
                    </ul>
                </li>
                
                <li style="display: none">
                    <a href="{{ route('support_token_index') }}" class="waves-effect">
                        <i class="fa fa-list"></i>
                        <span>Support Token</span>
                    </a>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>Reports</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('reports_my_team_index') }}">My Team</a> </li>
                        <li> <a href="{{ route('due_report_customer_index_amp') }}">Due Ledger</a> </li>
                        <!--<li> <a href="{{ route('client_list_index') }}">Client List</a> </li>-->
                        <!--<li> <a href="{{ route('due_report_customer_index') }}">Digishop Client Ledger</a> </li>-->
                        <!--<li> <a href="{{ route('due_report_customer_index_digiedu') }}">Digiedu Client Ledger</a> </li>-->
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>