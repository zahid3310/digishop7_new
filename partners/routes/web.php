<?php

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use App\Models\Users;
use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('auth.login');
});

Route::get('create-user-from-billing-software', function() {
	
	$partnerId      = $_GET['partnerId'] != '' ? $_GET['partnerId'] : null;
	$pId            = $_GET['pId'] != '' ? $_GET['pId'] : null;
	$name           = $_GET['name'] != '' ? $_GET['name'] : null;
	$phone          = $_GET['phone'] != '' ? $_GET['phone'] : null;
	$email          = $_GET['email'] != '' ? $_GET['email'] : null;
	$user_name      = $_GET['user_name'] != '' ? $_GET['user_name'] : null;
	$password       = $_GET['password'] != '' ? $_GET['password'] : null;
	$designation    = $_GET['designation'] != '' ? $_GET['designation'] : 0;
	
	\DB::beginTransaction();
	
	try{
        $purc_req                   = new Users;
        $purc_req->name             = $name;
        $purc_req->partner_id       = $partnerId;
        $purc_req->pId              = $pId;
        $purc_req->email            = $user_name;
        $purc_req->contact_number   = $phone;
        $purc_req->contact_email    = $email;
        $purc_req->role             = $designation;
        $purc_req->password         = \Hash::make($password);
        $purc_req->status           = 1;
    
        if($purc_req->save())
        {   
            \DB::commit();
            return 1;
        }
    }catch (\Exception $exception){
        \DB::rollback();
        return json_encode($exception);
    }
    
    return $data;
});

Route::get('update-user-from-billing-software', function() {
	$pId            = $_GET['pId'] != '' ? $_GET['pId'] : null;
	$name           = $_GET['name'] != '' ? $_GET['name'] : null;
	$phone          = $_GET['phone'] != '' ? $_GET['phone'] : null;
	$email          = $_GET['email'] != '' ? $_GET['email'] : null;
	$user_name      = $_GET['user_name'] != '' ? $_GET['user_name'] : null;
	$password       = $_GET['password'] != '' ? $_GET['password'] : null;
	$user_status    = $_GET['user_status'] != '' ? $_GET['user_status'] : null;
	$designation    = $_GET['designation'] != '' ? $_GET['designation'] : 0;
	
	\DB::beginTransaction();
	
	try{
        $purc_req                   = Users::where('pId', $pId)->first();
        
        if($purc_req != null)
        {
            $purc_req->name             = $name;
            $purc_req->email            = $user_name;
            $purc_req->contact_number   = $phone;
            $purc_req->contact_email    = $email;
            $purc_req->role             = $designation;
            $purc_req->status           = $user_status;
            
            if($password != null)
            {
                $purc_req->password     = \Hash::make($password);
            }
        
            if($purc_req->save())
            {   
                \DB::commit();
                return 1;
            }
        }
        else
        {
            $purc_req_new                   = new Users;
            $purc_req_new->name             = $name;
            $purc_req_new->partner_id       = \Crypt::encrypt($pId);
            $purc_req_new->pId              = $pId;
            $purc_req_new->email            = $user_name;
            $purc_req_new->contact_number   = $phone;
            $purc_req_new->contact_email    = $email;
            $purc_req_new->role             = $designation;
            $purc_req_new->password         = \Hash::make($password);
            $purc_req_new->status           = 1;
        
            if($purc_req_new->save())
            {   
                \DB::commit();
            }
        }
    }catch (\Exception $exception){
        \DB::rollback();
        return json_encode($exception);
    }
    
    return $data;
});

Route::get('get-user-details/{id}', function($id) {
    
    $data           = Users::where('pId', $id)->first();
    
    return $data;
});

// artisan command
Route::get('/clear', function() {
	$exitCode = Artisan::call('view:clear');
	$exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:cache');
    // $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('config:cache');
    return 'Clear and Config All';
});

// Route::get('/login-page-info', 'HomeController@loginPageInfo');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');