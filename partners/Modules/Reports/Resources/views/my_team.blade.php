@extends('layouts.app')

@section('title', 'My Team')

<link href="{{ url('public/tree-plugin/jsTree/style.min.css') }}" rel="stylesheet">

<style type="text/css">
    #treePane ul {
        list-style: none !important;
    }

    #treePane .node {
        color: red !important;
        background-image: url("{{url('public/tree-plugin/jsTree/plus.gif')}}");
        background-position: left center !important;
        background-repeat: no-repeat !important;
        padding-left: 12px !important;
        cursor: pointer !important;
    }

    #treePane .open {
        background-image: url("{{url('public/tree-plugin/jsTree/minus.gif')}}");
    }

    #treePane ul li {
        background-image: url("{{url('public/tree-plugin/jsTree/treeview-default-line.gif')}}");
        background-repeat: no-repeat !important;
        padding-left: 20px !important;
        margin-left: -42px !important;
    }
</style>

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">My Team</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">My Team</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <div class="row">
                                    @if(Auth()->user()->role == 1)
                                    <!--<div class="col-md-12">-->
                                    <!--    <h4>-->
                                    <!--        <span style="color: green">Digishop :  {{$total_digishop_cliens}} || -->
                                    <!--        <span style="color: blue">Digiedu : {{$total_digiedu_cliens}} || -->
                                    <!--        <span style="color: red">Others : {{$total_other_cliens}}</span>-->
                                            
                                    <!--        <span style="color: green;float: right"> DMP :  {{$total_dmp}} || -->
                                    <!--        <span style="color: blue;float: right">&nbsp; AMP : {{$total_amp - $total_dmp}} ||-->
                                    <!--        <span style="color: red;float: right">&nbsp; Total Due : {{ number_format($total_due,0,'.',',')}}-->
                                    <!--    </h4>-->
                                        
                                    <!--    <hr>-->
                                    <!--</div>-->
                                    
                                    <!--DMP show start-->
                                    <?php function getparent($parents, $grand_parent_id, $childs, $law_childs){ ?>
                                        <ul id="uls_<?php echo $grand_parent_id; ?>">
                                        <?php foreach($parents as $key => $parent){?>
                                            <li>
                                                <label id="<?php echo $parent->id;?>" class="node">
                                                    <?php echo $parent->name;?>
                                                </label>
                                                
                                                <?php getchild($childs, $grand_parent_id, $parent->id, $law_childs); ?>
                                            </li> 
                                        <?php }?>
                                        </ul>
                                    <?php }?>
                                    <!--DMP show end-->
                                    
                                    <!--AMP show start-->
                                    <?php function getchild($childs, $grand_parent_id, $parent_id, $law_childs){ ?>
                                        <ul id="uls_<?php echo $parent_id; ?>">
                                        <?php foreach($childs as $key => $child){?>
                                        <?php if($child->dmp_id == $parent_id){?>
                                            <li>
                                                <label id="<?php echo $child->id;?>" style="color: green !important;" class="node">
                                                    <?php echo $child->name;?>
                                                </label>
                                                
                                                <?php getinstitute($law_childs, $grand_parent_id, $parent_id, $child->id); ?>
                                            </li>
                                        <?php }?>
                                        <?php }?>
                                        </ul>
                                    <?php }?>
                                    <!--AMP show end-->
                                    
                                    <!--Isntitute show start-->
                                    <?php function getinstitute($law_childs, $grand_parent_id, $parent_id, $child_id){ ?>
                                        <ul id="uls_<?php echo $child_id; ?>">
                                        <?php foreach($law_childs as $key => $law_child){?>
                                        <?php if($law_child->partner_id == $child_id){?>
                                            <li>
                                                <label id="<?php echo $law_child->id;?>">
                                                    <?php echo $law_child->name;?>
                                                </label>
                                            </li>
                                        <?php }?>
                                        <?php }?>
                                        </ul>
                                    <?php }?>
                                    <!--Isntitute show end-->
                                    
                                    <!--RMP show start-->
                                    <div id="treePane">
                                        <ul id="uls_0">
                                            <li>
                                                <label id="{{$grand_parents->id}}" class="node">{{$grand_parents->name}}</label>
                                                
                                                <?php getparent($parents, $grand_parents->id, $childs, $law_childs); ?>
                                            </li>
                                        </ul>
                                    </div>
                                    <!--RMP show end-->
                                    @endif
                                    
                                    @if(Auth()->user()->role == 2)
                                    <!--AMP show start-->
                                    <?php function getchild($childs, $grand_parent_id, $law_childs){ ?>
                                        <ul id="uls_<?php echo $grand_parent_id; ?>">
                                        <?php foreach($childs as $key => $child){?>
                                        <?php if($child->dmp_id == $grand_parent_id){?>
                                            <li>
                                                <label id="<?php echo $child->id;?>" style="color: green !important;" class="node">
                                                    <?php echo $child->name;?>
                                                </label>
                                                
                                                <?php getinstitute($law_childs, $grand_parent_id, $child->id); ?>
                                            </li>
                                        <?php }?>
                                        <?php }?>
                                        </ul>
                                    <?php }?>
                                    <!--AMP show end-->
                                    
                                    <!--Isntitute show start-->
                                    <?php function getinstitute($law_childs, $grand_parent_id, $child_id){ ?>
                                        <ul id="uls_<?php echo $child_id; ?>">
                                        <?php foreach($law_childs as $key => $law_child){?>
                                        <?php if($law_child->partner_id == $child_id){?>
                                            <li>
                                                <label id="<?php echo $law_child->id;?>">
                                                    <?php echo $law_child->name;?>
                                                </label>
                                            </li>
                                        <?php }?>
                                        <?php }?>
                                        </ul>
                                    <?php }?>
                                    <!--Isntitute show end-->
                                    
                                    <!--DMP show start-->
                                    <div id="treePane">
                                        <ul id="uls_0">
                                            <li>
                                                <label id="{{$grand_parents->id}}" class="node">{{$grand_parents->name}}</label>
                                                
                                                <?php getchild($childs, $grand_parents->id, $law_childs); ?>
                                            </li>
                                        </ul>
                                    </div>
                                    <!--DMP show end-->
                                    @endif
                                    
                                    @if(Auth()->user()->role == 3)
                                    <!--Isntitute show start-->
                                    <?php function getinstitute($law_childs, $grand_parent_id){ ?>
                                        <ul id="uls_<?php echo $grand_parent_id; ?>">
                                        <?php foreach($law_childs as $key => $law_child){?>
                                        <?php if($law_child->partner_id == $grand_parent_id){?>
                                            <li>
                                                <label id="<?php echo $law_child->id;?>">
                                                    <?php echo $law_child->name;?>
                                                </label>
                                            </li>
                                        <?php }?>
                                        <?php }?>
                                        </ul>
                                    <?php }?>
                                    <!--Isntitute show end-->
                                    
                                    <!--AMP show start-->
                                    <div id="treePane">
                                        <ul id="uls_0">
                                            <li>
                                                <label id="{{$grand_parents->id}}" class="node">{{$grand_parents->name}}</label>
                                                
                                                <?php getinstitute($law_childs, $grand_parents->id); ?>
                                            </li>
                                        </ul>
                                    </div>
                                    <!--AMP show end-->
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
<script src="{{ url('public/tree-plugin/jsTree/jstree.min.js') }}"></script>

<script type="text/javascript">
    (function() {
        var treePaneNodes = $('#treePane').find(".node");
        treePaneNodes.next("ul").hide();
        $('#treePane').find(".open").next("ul").show();

        treePaneNodes.on('click', function() 
        {
            $(this).next("ul").first().toggle();
            $(this).toggleClass("open");
        })
    })();
</script>
@endsection