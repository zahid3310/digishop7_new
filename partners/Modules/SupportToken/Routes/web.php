<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('supporttoken')->group(function() {
    Route::get('/', 'SupportTokenController@index')->name('support_token_index');
    Route::post('/store', 'SupportTokenController@store')->name('support_token_store');
    Route::get('/edit/{id}', 'SupportTokenController@edit')->name('support_token_edit');
    Route::post('/update/{id}', 'SupportTokenController@update')->name('support_token_update');
});
