<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('customers')->group(function() {
    Route::get('/', 'CustomersController@index')->name('customers_index');
    Route::post('/store', 'CustomersController@store')->name('customers_store');
    Route::get('/edit/{id}', 'CustomersController@edit')->name('customers_edit');
    Route::post('/update/{id}', 'CustomersController@update')->name('customers_update');
    Route::get('/customer/list/load', 'CustomersController@customerListLoad')->name('customers_list_load');
    Route::get('/customer/search/list/{id}', 'CustomersController@customerListSearch')->name('customers_list_search');
});

Route::prefix('customers/type')->group(function() {
    Route::get('/', 'CustomersController@indexType')->name('customers_type_index');
    Route::post('/store', 'CustomersController@storeType')->name('customers_type_store');
    Route::get('/edit/{id}', 'CustomersController@editType')->name('customers_type_edit');
    Route::post('/update/{id}', 'CustomersController@updateType')->name('customers_type_update');
});