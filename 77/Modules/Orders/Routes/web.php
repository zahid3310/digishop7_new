<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('orders')->group(function() {
    Route::get('/', 'OrdersController@index')->name('orders_index');
    Route::get('/all-orders', 'OrdersController@AllSales')->name('orders_all_order');
    Route::post('/store', 'OrdersController@store')->name('orders_store');
    Route::get('/edit/{id}', 'OrdersController@edit')->name('orders_edit');
    Route::post('/update', 'OrdersController@update')->name('orders_update');
    Route::get('/confirm', 'OrdersController@confirm')->name('orders_confirm');
    Route::get('/delete/{id}', 'OrdersController@delete')->name('orders_delete');
    Route::get('/show/{id}', 'OrdersController@show')->name('orders_show');
    Route::get('/show-pos/{id}', 'OrdersController@showPos')->name('orders_show_pos');
    Route::get('/order/list/load', 'OrdersController@orderListLoad')->name('orders_list_load');
    Route::get('/order/search/list/{from_date}/{to_date}/{customer_name}/{order_number}', 'OrdersController@orderListSearch')->name('orders_list_search');
    Route::get('/print-orders-list', 'OrdersController@printOrdersList')->name('orders_print_orders_list');
    Route::get('/print-orders-search/{date}/{customer}/{order_number}', 'OrdersController@printOrdersSearch')->name('orders_print_orders_search');
    Route::get('/order-list-load-search', 'OrdersController@orderListLoadSearch')->name('orders_order_list_load_search');
});
