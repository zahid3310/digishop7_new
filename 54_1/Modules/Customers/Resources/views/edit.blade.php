@extends('layouts.app')

<?php
    if (isset($_GET['contact_type']) && ($_GET['contact_type'] == 0))
    {
        $title = 'DSR/Customers';
    }

    if (isset($_GET['contact_type']) && ($_GET['contact_type'] == 1))
    {
        $title = 'Suppliers';
    }

    if (isset($_GET['contact_type']) && ($_GET['contact_type'] == 2))
    {
        $title = 'Employees';
    }

    if (isset($_GET['contact_type']) && ($_GET['contact_type'] == 3))
    {
        $title = 'Reference';
    }

    if (isset($_GET['contact_type']) && ($_GET['contact_type'] == 5))
    {
        $title = 'Loan Register';
    }

    if (!isset($_GET['contact_type']))
    {
        $title = 'Contacts';
    }
?>

@section('title', $title)

<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <style type="text/css">
        #results { padding:20px; border:1px solid; background:#ccc; }
    </style>
</head>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit {{ $title }}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Edit {{ $title }}</a></li>
                                    <li class="breadcrumb-item active">Edit {{ $title }}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('customers_update', $find_customer['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Code</label>
                                        <input name="code" type="text" class="form-control" placeholder="Enter Code" value="{{ $find_customer['code'] }}">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Name *</label>
                                        <input value="{{ $find_customer['name'] }}" name="customer_name" type="text" class="form-control" required>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Address</label>
                                        <input value="{{ $find_customer['address'] }}" name="address" type="text" class="form-control">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Mobile Number</label>
                                        <input value="{{ $find_customer['phone'] }}" name="mobile_number" type="text" class="form-control">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Alternative Phone</label>
                                        <input value="{{ $find_customer['alternative_contact'] }}" name="alternative_mobile_number" type="text" class="form-control">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">NID Number</label>
                                        <input value="{{ $find_customer['nid_number'] }}" name="nid_number" type="text" class="form-control">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Father's Name</label>
                                        <input name="father_name" type="text" class="form-control" placeholder="Enter Father's Name" value="{{ $find_customer['father_name'] }}">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Mother's Name</label>
                                        <input name="mother_name" type="text" class="form-control" placeholder="Enter Mother's Name" value="{{ $find_customer['mother_name'] }}">
                                    </div>

                                    @if((isset($_GET['contact_type'])) && ($_GET['contact_type'] == 0) || (isset($_GET['contact_type'])) && ($_GET['contact_type'] == 1) || (isset($_GET['contact_type'])) && ($_GET['contact_type'] == 3))
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Opening Balance</label>
                                        <input name="opening_balance" type="number" class="form-control" value="{{ $find_customer['opening_balance'] }}">
                                    </div>
                                    @endif

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Contact Type *</label>
                                        <select style="cursor: pointer" name="contact_type" class="form-control" id="contact_type" onchange="additionalFields()" required>
                                            @if(isset($_GET['contact_type']) && ($_GET['contact_type'] == 0))
                                            <option {{ $find_customer['contact_type'] == 0 ? 'selected' : '' }} value="0">DSR/Customers</option>
                                            @endif

                                            @if(isset($_GET['contact_type']) && ($_GET['contact_type'] == 1))
                                            <option {{ $find_customer['contact_type'] == 1 ? 'selected' : '' }} value="1">Supplier</option>
                                            @endif

                                            @if(isset($_GET['contact_type']) && ($_GET['contact_type'] == 2))
                                            <option {{ $find_customer['contact_type'] == 2 ? 'selected' : '' }} value="2">Employee</option>
                                            @endif

                                            @if(isset($_GET['contact_type']) && ($_GET['contact_type'] == 3))
                                            <option {{ $find_customer['contact_type'] == 3 ? 'selected' : '' }} value="3">Reference</option>
                                            @endif

                                            @if(isset($_GET['contact_type']) && ($_GET['contact_type'] == 5))
                                            <option value="5">Loan Register</option>
                                            @endif

                                            @if(!isset($_GET['contact_type']))
                                            <option {{ $find_customer['contact_type'] == 0 ? 'selected' : '' }} value="0">DSR/Customers</option>
                                            <option {{ $find_customer['contact_type'] == 1 ? 'selected' : '' }} value="1">Supplier</option>
                                            <option {{ $find_customer['contact_type'] == 2 ? 'selected' : '' }} value="2">Employee</option>
                                            <option {{ $find_customer['contact_type'] == 3 ? 'selected' : '' }} value="3">Reference</option>
                                            <option {{ $find_customer['contact_type'] == 5 ? 'selected' : '' }} value="5">Loan Register</option>
                                            @endif
                                        </select>
                                    </div>

                                    <div style="display: none;" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Customer Type</label>
                                        <select style="cursor: pointer" name="customer_type" class="form-control" id="customer_type">
                                            @if($types->count() > 0)
                                            @foreach($types as $key => $type)
                                            <option value="{{ $type->id }}" {{ $find_customer['customer_type'] == $type->id ? 'selected' : '' }}>{{ $type->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    @if(isset($_GET['contact_type']))
                                        <input type="hidden" name="contact_type_reference" value="{{ $_GET['contact_type'] }}">
                                    @endif

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group additionalFields">
                                        <label for="example-text-input" class="col-form-label">Joining Date</label>
                                        <input id="joining_date" name="joining_date" type="text" @if($find_customer['joining_date'] != null) value="{{ date('d-m-Y', strtotime($find_customer['joining_date'])) }}" @else value="{{ date('d-m-Y') }}" @endif class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                    </div>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group additionalFields">
                                        <label for="example-text-input" class="col-form-label">Designation</label>
                                        <input name="designation" type="text" class="form-control" value="{{ $find_customer['designation'] }}">
                                    </div>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group additionalFields">
                                        <label for="example-text-input" class="col-form-label">Salary</label>
                                        <input name="salary" type="text" class="form-control" value="{{ $find_customer['salary'] }}">
                                    </div>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group AssociativeContactId">
                                        <label for="example-text-input" class="col-form-label">Associative User ID</label>
                                        <select style="cursor: pointer" name="user_id" class="form-control" id="user_id">
                                            <option value="">--Select User--</option>
                                            @if(!empty($users) && ($users->count() > 0))
                                                @foreach($users as $key => $user)
                                                <option {{ $find_customer['user_id'] == $user['id'] ? 'selected' : '' }} value="{{ $user->id }}">{{ $user->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Image</label>
                                        <input name="image" type="file">
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <br>
                                        <label for="example-text-input" class="col-form-label"></label>
                                        <button type="button" class="btn btn-success" onClick="webCam()">Take Photo Via Webcam</button>
                                    </div>
                                    
                                    <div style="display: none" class="col-md-6 camImg">
                                        <div id="my_camera"></div>
                                        <input type=button value="Take Snapshot" onClick="take_snapshot()">
                                        <input type="hidden" name="image1" class="image-tag">
                                    </div>
                                    
                                    <div style="display: none" class="col-md-6 camImg">
                                        <div id="results">Your captured image will appear here...</div>
                                    </div>

                                    <div class="col-md-12">
                                        <label for="example-text-input" class="col-md-2 col-form-label">Previous Image</label>
                                        <div class="col-md-10">
                                            @if($find_customer['image'] != null)
                                            <img style="height: 80px;width: 80px;padding: 0px" src="{{ url('public/'.$find_customer['image']) }}" class="form-control">
                                            @else
                                            <img style="height: 80px;width: 80px;padding: 0px" src="{{ url('public/default.png') }}" class="form-control">
                                            @endif
                                        </div>
                                        <br>
                                    </div>

                                    <div class="col-md-12">
                                        <hr style="margin-top: 0px !important">
                                        <h4 style="text-align: center">Add Guarantor</h4>
                                        <div style="margin-bottom: 5px;" class="form-group row">
                                            <div class="col-md-3">
                                                <i id="add_field_button" style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button col-md-3"></i>
                                            </div>
                                        </div>

                                        <div style="background-color: #D2D2D2;height: 350px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                            <div class="row di_0">
                                                @if($guarantors->count() > 0)
                                                @foreach($guarantors as $key => $guarantor)
                                                <div style="margin-bottom: 5px;padding-left: 10px;padding-right: 0px" class="col-lg-3 col-md-3 col-sm-6 col-12">
                                                    <label class="show-xs" for="productname">Guarantor Name</label>
                                                    <input id="guarantor_name_{{$key}}" name="guarantor_name[]" type="text" class="form-control" value="{{ $guarantor->name }}">
                                                </div>
                                                
                                                <div style="margin-bottom: 5px;padding-left: 10px;padding-right: 0px" class="col-lg-3 col-md-3 col-sm-6 col-12">
                                                    <label class="show-xs" for="productname">Father Name</label>
                                                    <input id="guarantor_father_name_{{$key}}" name="guarantor_father_name[]" type="text" class="form-control" value="{{ $guarantor->father_name }}">
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 10px;padding-right: 0px" class="col-lg-3 col-md-3 col-sm-6 col-12">
                                                    <label class="show-xs" for="productname">Father Name</label>
                                                    <input id="guarantor_mother_name_{{$key}}" name="guarantor_mother_name[]" type="text" class="form-control" value="{{ $guarantor->mother_name }}">
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 10px;padding-right: 10px" class="col-lg-3 col-md-3 col-sm-6 col-12">
                                                    <label class="show-xs" for="productname">Address</label>
                                                    <input id="guarantor_address_{{$key}}" name="guarantor_address[]" type="text" class="form-control" value="{{ $guarantor->address }}">
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 10px;padding-right: 10px" class="col-lg-5 col-md-5 col-sm-6 col-12">
                                                    <label class="show-xs" for="productname">Mobile Number</label>
                                                    <input id="guarantor_phone_{{$key}}" name="guarantor_phone[]" type="text" class="form-control" value="{{ $guarantor->phone }}">
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 10px;padding-right: 10px" class="col-lg-6 col-md-6 col-sm-6 col-12">
                                                    <label class="show-xs" for="productname">NID</label>
                                                    <input id="guarantor_nid_{{$key}}" name="guarantor_nid[]" type="text" class="form-control" value="{{ $guarantor->nid_number }}">
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="{{$key}}"> 
                                                    <label class="show-xs" for="productname">Action</label>
                                                    <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>
                                                </div>
                                                <hr>
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>

                                        <hr style="margin-top: 0px !important">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('customers_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Contacts</h4>

                                <div style="margin-right: 10px" class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-1">Search : </div>
                                    <div class="col-md-2">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Address</th>
                                            <th>Phone</th>
                                            <th>Alternative Phone</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody id="customer_list">
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Configure a few settings and attach camera -->
    <script language="JavaScript">
        function webCam()
        {   
            $(".camImg").show();
            
            Webcam.set({
                width: 490,
                height: 390,
                image_format: 'jpeg',
                jpeg_quality: 90
            });
          
            Webcam.attach( '#my_camera' );
        } 
        
        function take_snapshot() {
            Webcam.snap( function(data_uri) {
                $(".image-tag").val(data_uri);
                document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
            } );
        }
    </script>
    
    <script type="text/javascript">
        $( document ).ready(function() {

            var contact_type   = $('#contact_type').val();

            if (contact_type == 0)
            {
                $('.AssociativeContactId').hide();
                $('.additionalFields').hide();
            }

            if (contact_type == 2)
            {
                $('.additionalFields').show();
                $('.AssociativeContactId').show();
            }
            
            if (contact_type == 1)
            {
                $('.additionalFields').hide();
                $('.AssociativeContactId').hide();
            }

            if (contact_type == 3)
            {
                $('.additionalFields').hide();
                $('.AssociativeContactId').hide();
            }

            if (contact_type == 5)
            {
                $('.AssociativeContactId').hide();
                $('.additionalFields').hide();
            }

            var site_url        = $('.site_url').val();

            $.get(site_url + '/customers/customer/list/load', function(data){

                var customer_list   = '';
                var serial          = 1;
                $.each(data, function(i, customer_data)
                {   
                    var locationValue   = (new URL(location.href)).searchParams.get('contact_type');
                    if (locationValue   != null)
                    {
                        var edit_url = site_url + '/customers/edit/' + customer_data.id + '?contact_type=' + locationValue;
                    }
                    else
                    {
                        var edit_url     = site_url + '/customers/edit/' + customer_data.id;
                    }
                    
                    if (customer_data.code == null)
                    {
                        var code   = '';
                    }
                    else
                    {
                        var code   = customer_data.code;
                    }

                    if (customer_data.phone == null)
                    {
                        var customer_phone   = '';
                    }
                    else
                    {
                        var customer_phone   = customer_data.phone;
                    }

                    if (customer_data.address == null)
                    {
                        var address         = '';
                    }
                    else
                    {
                        var address         = customer_data.address;
                    }

                    if (customer_data.alternative_contact == null)
                    {
                        var alter_contact   = '';
                    }
                    else
                    {
                        var alter_contact   = customer_data.alternative_contact;
                    }

                    if (customer_data.contact_type == 0)
                    {
                        var contact_type    = 'DSR/Customers';
                        var customer_name   = customer_data.name;
                    }
                    
                    if (customer_data.contact_type == 1)
                    {
                        var contact_type    = 'Supplier';
                        var customer_name   = customer_data.name;
                    }

                    if (customer_data.contact_type == 3)
                    {
                        var contact_type    = 'Reference';
                        var customer_name   = customer_data.name;
                    }

                    if (customer_data.contact_type == 5)
                    {
                        var contact_type    = 'Loan Register';
                        var customer_name   = customer_data.name;
                    }

                    if (customer_data.contact_type == 2)
                    {
                        var contact_type   = 'Employee';

                        if (customer_data.designation != null)
                        {
                            var designation   = customer_data.designation;
                        }
                        else
                        {
                            var designation   = '';
                        }

                        if (customer_data.salary != null)
                        {
                            var salary = customer_data.salary;
                        }
                        else
                        {
                            var salary = '';
                        } 

                        if (customer_data.user_name != null)
                        {
                            var user_name = customer_data.user_name;
                        }
                        else
                        {
                            var user_name = '';
                        }                      

                        var customer_name  = customer_data.name + "<br>" + "<strong>Designation : </strong>" + designation + "<br>" + "<strong>Salary : </strong>" + salary+ "<br>" + "<strong>User ID : </strong>" + user_name;
                    }

                    if (customer_data.image != null)
                    {
                        var img  = site_url + '/public/' + customer_data.image;
                    }
                    else
                    {
                        var img  = site_url + '/public/' + 'default.png';
                    }

                    if (locationValue != null)
                    {
                        if (customer_data.contact_type == locationValue)
                        {
                            customer_list += '<tr>' +
                                            '<td style="text-align: left">' +
                                                serial +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                '<img style="height: 30px;width: 30px" src="'+ img +'">' +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               code +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               customer_name +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               contact_type +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               address +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                customer_phone +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               alter_contact +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                '<div class="dropdown">' +
                                                    '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                        '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                    '</a>' +
                                                    '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                        '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</td>' +
                                        '</tr>';

                            serial++;
                        }
                    }
                    else
                    {
                        customer_list += '<tr>' +
                                        '<td style="text-align: left">' +
                                            serial +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            '<img style="height: 30px;width: 30px" src="'+ img +'">' +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           code +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           customer_name +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           contact_type +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           address +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            customer_phone +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           alter_contact +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                        serial++; 
                    }
                });

                $("#customer_list").empty();
                $("#customer_list").append(customer_list);
            });

            // $('#add_field_button').click();
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var search_text = $('#searchPayment').val();
            var site_url    = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/customers/customer/search/list/' + search_text, function(data){

                var customer_list   = '';
                var serial          = 1;
                $.each(data, function(i, customer_data)
                {          
                    var locationValue   = (new URL(location.href)).searchParams.get('contact_type');
                    if (locationValue   != null)
                    {
                        var edit_url = site_url + '/customers/edit/' + customer_data.id + '?contact_type=' + locationValue;
                    }
                    else
                    {
                        var edit_url     = site_url + '/customers/edit/' + customer_data.id;
                    }

                    if (customer_data.phone == null)
                    {
                        var customer_phone   = '';
                    }
                    else
                    {
                        var customer_phone   = customer_data.phone;
                    }
                    
                    if (customer_data.code == null)
                    {
                        var code   = '';
                    }
                    else
                    {
                        var code   = customer_data.code;
                    }

                    if (customer_data.address == null)
                    {
                        var address         = '';
                    }
                    else
                    {
                        var address         = customer_data.address;
                    }

                    if (customer_data.alternative_contact == null)
                    {
                        var alter_contact   = '';
                    }
                    else
                    {
                        var alter_contact   = customer_data.alternative_contact;
                    }

                    if (customer_data.contact_type == 0)
                    {
                        var contact_type    = 'Customer';
                        var customer_name   = customer_data.name;
                    }
                    
                    if (customer_data.contact_type == 1)
                    {
                        var contact_type    = 'Supplier';
                        var customer_name   = customer_data.name;
                    }

                    if (customer_data.contact_type == 3)
                    {
                        var contact_type    = 'Reference';
                        var customer_name   = customer_data.name;
                    }

                    if (customer_data.contact_type == 5)
                    {
                        var contact_type    = 'Loan Register';
                        var customer_name   = customer_data.name;
                    }

                    if (customer_data.contact_type == 2)
                    {
                        var contact_type   = 'Employee';

                        if (customer_data.designation != null)
                        {
                            var designation   = customer_data.designation;
                        }
                        else
                        {
                            var designation   = '';
                        }

                        if (customer_data.salary != null)
                        {
                            var salary = customer_data.salary;
                        }
                        else
                        {
                            var salary = '';
                        }                       

                        if (customer_data.user_name != null)
                        {
                            var user_name = customer_data.user_name;
                        }
                        else
                        {
                            var user_name = '';
                        }                      

                        var customer_name  = customer_data.name + "<br>" + "<strong>Designation : </strong>" + designation + "<br>" + "<strong>Salary : </strong>" + salary+ "<br>" + "<strong>User ID : </strong>" + user_name;
                    }

                    if (customer_data.image != null)
                    {
                        var img  = site_url + '/public/' + customer_data.image;
                    }
                    else
                    {
                        var img  = site_url + '/public/' + 'default.png';
                    }

                    if (locationValue != null)
                    {
                        if (customer_data.contact_type == locationValue)
                        {
                            customer_list += '<tr>' +
                                            '<td style="text-align: left">' +
                                                serial +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                '<img style="height: 30px;width: 30px" src="'+ img +'">' +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               code +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               customer_name +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               contact_type +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               address +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                customer_phone +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               alter_contact +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                '<div class="dropdown">' +
                                                    '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                        '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                    '</a>' +
                                                    '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                        '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</td>' +
                                        '</tr>';
                            serial++;
                        }
                    }
                    else
                    {
                        customer_list += '<tr>' +
                                        '<td style="text-align: left">' +
                                            serial +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            '<img style="height: 30px;width: 30px" src="'+ img +'">' +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           code +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           customer_name +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           contact_type +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           address +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            customer_phone +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           alter_contact +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                        serial++; 
                    }
                });

                $("#customer_list").empty();
                $("#customer_list").append(customer_list);
            });
        }

        function additionalFields()
        {
            var contact_type   = $('#contact_type').val();

            if (contact_type == 0)
            {
                $('.AssociativeContactId').hide();
                $('.additionalFields').hide();
            }

            if (contact_type == 2)
            {
                $('.additionalFields').show();
                $('.AssociativeContactId').show();
            }
            
            if (contact_type == 1)
            {
                $('.additionalFields').hide();
                $('.AssociativeContactId').hide();
            }

            if (contact_type == 3)
            {
                $('.AssociativeContactId').hide();
                $('.additionalFields').hide();
            }

            if (contact_type == 5)
            {
                $('.AssociativeContactId').hide();
                $('.additionalFields').hide();
            }
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = {{ $guar_count > 0 ? $guar_count : -1 }};
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var guaranter_name_label    = '<label class="hidden-xs" for="productname">Guarantor Name </label>\n';
                    var father_name_label       = '<label class="hidden-xs" for="productname">Father Name </label>\n';
                    var mother_name_label       = '<label class="hidden-xs" for="productname">Mother Name </label>\n';
                    var address_label           = '<label class="hidden-xs" for="productname">Address </label>\n';
                    var phone_label             = '<label class="hidden-xs" for="productname">Phone </label>\n';
                    var nid_label               = '<label class="hidden-xs" for="productname">NID </label>\n';
                    var action_label            = '<label class="hidden-xs" for="productname">Action</label>\n';
                    var add_btn                 =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }
                else
                {
                    var guaranter_name_label    = '';
                    var father_name_label       = '';
                    var mother_name_label       = '';
                    var address_label           = '';
                    var phone_label             = '';
                    var nid_label               = '';
                    var action_label            = '<label class="hidden-xs" for="productname">Action</label>\n';
                    var add_btn                 = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px;padding-left: 10px;padding-right: 0px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Guarantor Name</label>\n' +
                                                        guaranter_name_label +
                                                        '<input id="guarantor_name_'+x+'" name="guarantor_name[]" type="text" class="form-control" >\n' +
                                                    '</div>\n' +
                                                    
                                                    '<div style="margin-bottom: 5px;padding-left: 10px;padding-right: 0px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Father Name</label>\n' +
                                                        father_name_label +
                                                        '<input id="guarantor_father_name_'+x+'" name="guarantor_father_name[]" type="text" class="form-control" >\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 10px;padding-right: 0px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Father Name</label>\n' +
                                                        mother_name_label +
                                                        '<input id="guarantor_mother_name_'+x+'" name="guarantor_mother_name[]" type="text" class="form-control" >\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 10px;padding-right: 10px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Address</label>\n' +
                                                        address_label +
                                                        '<input id="guarantor_address_'+x+'" name="guarantor_address[]" type="text" class="form-control" >\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 10px;padding-right: 10px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Mobile Number</label>\n' +
                                                        phone_label +
                                                        '<input id="guarantor_phone_'+x+'" name="guarantor_phone[]" type="text" class="form-control" >\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 10px;padding-right: 10px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">NID</label>\n' +
                                                        nid_label +
                                                        '<input id="guarantor_nid_'+x+'" name="guarantor_nid[]" type="text" class="form-control" >\n' +
                                                    '</div>\n' +

                                                    add_btn +
                                                    
                                                '</div>\n' +
                                                '<hr>' 
                                            );
            }                                   
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;
        });
    </script>
@endsection