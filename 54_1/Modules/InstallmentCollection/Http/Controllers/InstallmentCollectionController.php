<?php

namespace Modules\InstallmentCollection\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\Bills;
use App\Models\InvoiceEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\JournalEntries;
use App\Models\Accounts;
use App\Models\Settlements;
use App\Models\Installments;
use Response;
use DB;

class InstallmentCollectionController extends Controller
{
    public function create()
    {
        $branch_id      = Auth::user()->branch_id;
        $customers      = Customers::where('contact_type', 0)
                                    ->orderBy('created_at', 'DESC')
                                    ->get();

        $customers      = $customers->where('branch_id', $branch_id);

        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $find_customer  = Customers::where('id', $customer_id)->first();
        $paid_accounts  = Accounts::where('account_type_id', 4)
                                    ->whereNotIn('id', [2,3])
                                    ->where('status', 1)
                                    ->get();

        $payments       = Payments::where('type', 4)->get();

        return view('installmentcollection::create', compact('customers', 'customer_id', 'find_customer', 'paid_accounts', 'payments'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'customer_id'   => 'required',
            'invoice_id'    => 'required',
            'payment_date'  => 'required',
            'amount'        => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id          = Auth::user()->branch_id;
        $user_id            = Auth::user()->id;
        $data               = $request->all();
        $discount_amount    = $data['discount_amount'] != null ? $data['discount_amount'] : 0;

        if ($data['amount'] == 0 || $data['amount'] == null || $data['payment_date'] == null)
        {
            return redirect()->back()->withInput();
        }

        DB::beginTransaction();

        try{
            $data_find                  = Payments::orderBy('id', 'DESC')->first();
            $payment_number             = $data_find != null ? $data_find['payment_number'] + 1 : 1;
            
            $payment                    = new Payments;
            $payment->payment_number    = $payment_number;
            $payment->customer_id       = $data['customer_id'];
            $payment->payment_date      = date('Y-m-d', strtotime($data['payment_date']));
            $payment->amount            = $data['amount'];
            $payment->discount_amount   = $discount_amount;
            $payment->paid_through      = $data['paid_through'];
            $payment->note              = $data['note'];
            $payment->invoice_id        = $data['invoice_id'];
            $payment->type              = 4;
            $payment->branch_id         = $branch_id;
            $payment->created_by        = $user_id;

            if ($payment->save())
            {   
                foreach ($data['paid'] as $key => $value)
                {
                    if ($value != 0)
                    {
                        $payment_entries[] = [
                            'invoice_id'        => $data['invoice_id'],
                            'installment_id'    => $data['installment_id'][$key],
                            'payment_id'        => $payment['id'],
                            'amount'            => $value,
                            'branch_id'         => $branch_id,
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];

                        $update_installment         = Installments::find($data['installment_id'][$key]);
                        $update_installment->paid   = $update_installment['paid'] + $value;
                        $update_installment->save();
                    }
                }

                if (isset($payment_entries))
                {
                    DB::table('payment_entries')->insert($payment_entries);
                }

                //Financial Accounting Start
                    debit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=$data['paid_through'], $amount=$data['amount'], $note=$data['note'], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    credit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=8, $amount=$data['amount'], $note=$data['note'], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

                    if ($discount_amount > 0) 
                    {
                        credit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=null, $amount=$discount_amount, $note=$data['note'], $transaction_head='discount', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    }
                //Financial Accounting End

                customerBalanceUpdate($data['customer_id']);

                DB::commit();
                return back()->with("success","Payment Created Successfully !!");
            }
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        $payment        = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                ->select('payments.*',
                                         'customers.name as customer_name',
                                         'customers.address as address',
                                         'customers.phone as phone')
                                ->find($id);

        if ($payment['type'] == 0)
        {
            $entries    = PaymentEntries::leftjoin('invoices', 'invoices.id', 'payment_entries.invoice_id')
                                ->where('payment_entries.payment_id', $id)
                                ->select('payment_entries.*', 'invoices.invoice_number as number')
                                ->get();
        }
        else
        {
            $entries    = PaymentEntries::leftjoin('bills', 'bills.id', 'payment_entries.bill_id')
                                ->where('payment_entries.payment_id', $id)
                                ->select('payment_entries.*', 'bills.bill_number as number')
                                ->get();
        }

        $user_info  = userDetails();

        return view('installmentcollection::show', compact('entries', 'payment', 'user_info'));
    }

    public function edit($id)
    {
        dd($entries);
        return view('installmentcollection::edit');
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        DB::beginTransaction();

        try{
            $payment  = Payments::find($id);

            if ($payment->type == 0)
            {
                $payment_delete         = Payments::where('id', $id)->delete();
                $delete_transactions    = JournalEntries::where('payment_id', $id)->whereIn('transaction_head', ['payment-receive', 'discount'])->delete();

                customerBalanceUpdate($payment['customer_id']);
            }

            if ($payment->type == 1)
            {
                $payment_delete         = Payments::where('id', $id)->delete();
                $delete_transactions    = JournalEntries::where('payment_id', $id)->whereIn('transaction_head', ['payment-made', 'discount'])->delete();
                
                supplierBalanceUpdate($payment['customer_id']);
            }

            DB::commit();
            return back()->with("success","Payment Deleted Successfully !!");

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Deleted");
        }      
    }

    public function invoiceListInst($customer_id)
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Invoices::leftjoin('installment_summary', 'installment_summary.invoice_id', 'invoices.id')
                                    ->whereIn('invoices.type', [1,3])
                                    ->where('invoices.customer_id', $customer_id)
                                    ->orderBy('invoices.invoice_date', 'DESC')
                                    ->orderBy('invoices.id', 'ASC')
                                    ->select('invoices.invoice_number',
                                             'invoices.id',
                                             'installment_summary.invoice_id as installment')
                                    ->distinct('invoices.id')
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Invoices::leftjoin('installment_summary', 'installment_summary.invoice_id', 'invoices.id')
                                    ->whereIn('invoices.type', [1,3])
                                    ->where('invoices.customer_id', $customer_id)
                                    ->where('invoices.invoice_number', 'LIKE', "%$search%")
                                    ->orderBy('invoices.invoice_date', 'DESC')
                                    ->orderBy('invoices.id', 'ASC')
                                    ->select('invoices.invoice_number',
                                             'invoices.id',
                                             'installment_summary.invoice_id as installment')
                                    ->distinct('invoices.id')
                                    ->get();
        }

        foreach ($fetchData as $key => $value)
        {   
            $name   = $value['invoice_number'];

            if ($value['installment'] != null)
            {
                $data[] = array("id"=>$value['id'], "text"=>$name);
            }
        }

        return Response::json($data);
    }

    public function installmentsListInst($invoice_id)
    {
        $data = Installments::leftjoin('invoices', 'invoices.id', 'installments.invoice_id')
                            ->where('installments.invoice_id', $invoice_id)
                            ->selectRaw('installments.*, invoices.invoice_number as invoice_number')
                            ->get();

        return Response::json($data);
    }
}
