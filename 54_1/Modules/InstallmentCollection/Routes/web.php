<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('installmentcollection')->group(function() {
    Route::get('/create', 'InstallmentCollectionController@create')->name('installment_collection_create');
    Route::post('/store', 'InstallmentCollectionController@store')->name('installment_collection_store');
    Route::get('/edit/{id}', 'InstallmentCollectionController@edit')->name('installment_collection_edit');
    Route::post('/update/{id}', 'InstallmentCollectionController@update')->name('installment_collection_update');
    Route::get('/delete/{id}', 'InstallmentCollectionController@destroy')->name('installment_collection_delete');
    Route::get('/show/{id}', 'InstallmentCollectionController@show')->name('installment_collection_print');
    Route::get('/invoice-list-inst/{customer_id}', 'InstallmentCollectionController@invoiceListInst');
    Route::get('/installments-list-inst/{invoice_id}', 'InstallmentCollectionController@installmentsListInst');
});
