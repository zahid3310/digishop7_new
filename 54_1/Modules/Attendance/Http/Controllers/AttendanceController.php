<?php

namespace Modules\Attendance\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Model
use Rats\Zkteco\Lib\ZKTeco;
use App\Models\Devices;
use App\Models\MachineDataLog;
use App\Models\Customers;
use Validator;
use Auth;
use Response;
use DB;

class AttendanceController extends Controller
{
    public function index()
    {
        $zk = new ZKTeco('192.168.0.205');
        $zk->connect();
        $employees = $zk->getUser();

        return view('attendance::all_device_users', compact('employees'));
    }

    public function importUser()
    {
        $zk = new ZKTeco('192.168.0.201');
        $zk->connect();

        $uid = array_key_last($zk->getUser());

        return view('attendance::import_employee_to_device', compact('uid'));
    }

    public function importUserStore(Request $request)
    {
        $data = $request->all();

        $zk = new ZKTeco('192.168.0.201');
        $zk->connect();
 
        $zk->setUser($data['uid'],$data['userid'],$data['name'],$password='',$role=0);

        return back();
    }

    public function deleteUser($uid)
    {
        $zk = new ZKTeco('192.168.0.201');
        $zk->connect();
        $zk->removeUser($uid); 

        return redirect()->route('attendance_import_user_index');
    }

    public function deviceList()
    {
        // $zk = new ZKTeco('192.168.0.201');
        // $zk->connect();
        // dd('sdasd');
        $devices = Devices::get();

        return view('attendance::add_device', compact('devices'));
    }

    public function deviceStore(Request $request)
    {
        $rules = array(
            'device_ip'  => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $device               = new Devices;
            $device->device_name  = $data['device_name'];
            // $device->device_id    = ;
            // $device->device_ip    = ;
            $device->created_by   = $user_id;

            if ($device->save())
            {   
                return back()->with("success","Device Added Successfully !!");
            }
        }
        catch (\Exception $exception){
            return back()->with("unsuccess","Device Not Added");
        }
    }

    public function pullData()
    {
        $zk = new ZKTeco('192.168.0.205');
        $zk->connect();
        $serial     = explode('=', $zk->serialNumber());
        $machine_id = $serial[1];

        $tas_json_decode   = $zk->getAttendance();

        if($tas_json_decode)
        {
            $tas_machine_data = array();
            foreach($tas_json_decode as $data)
            {
                $machine_data = array(
                    'UserID'        => trim($data['uid']),
                    'DateTime'      => trim($data['timestamp']),
                    'MachineID'     => trim($machine_id),
                    'InOutStatus'   => trim($data['state'])
                );

                $tas_machine_data[] = $machine_data;
            }

            DB::table('tbl_tas_machine_data')->insert($tas_machine_data);

            $insert_data = DB::table('tbl_tas_machine_data_log')->insert($tas_machine_data);

            if($insert_data)
            {
                return back()->with('success', 'Data inserted successfully !! !!');
            }
            else
            {
                return back()->with('unsuccess', 'Something wrong.Try again !!');
            }
        }
        else
        {
            return back()->with('unsuccess', 'Something wrong.Try again !!');
        }
    }

    public function attendanceList()
    {
        $data = MachineDataLog::orderBy('timestamp', 'DESC')->get();

        return view('attendance::attendance_list', compact('data'));
    }

    public function importUserUpdate(Request $request)
    {
        $data   = $request->all();
        $zk     = new ZKTeco('192.168.0.205');
        $zk->connect();
    
        DB::beginTransaction();

        try{
            $zk->clearAdmin();

            foreach ($data['employee_id'] as $key => $value)
            {
                $zk->setUser($data['uid'][$key],$value,$data['employee_name'][$key],$password='',$role=0);
            }

            DB::commit();
            return back()->with("success","Successfully Updated");
        }catch (\Exception $exception){
            DB::rollback($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function manualAttendanceIndex()
    {
        $date           = isset($_GET['date']) ? date('Y-m-d 00:00:00', strtotime($_GET['date'])) : date('Y-m-d 00:00:00');
        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;

        $data   = MachineDataLog::where(DB::Raw('DATE(DateTime)'), $date)
                                ->orderBy('DateTime', 'ASC')
                                ->select('AID as AID',
                                         'UserID as UserID',
                                          DB::Raw('DATE(DateTime) as DateTimeGr'),
                                          'DateTime as DateTime',
                                          'MachineID as MachineID',
                                          'InOutStatus as InOutStatus',
                                          'timestamp as timestamp'
                                        )
                                ->get();

        $emploees  = Customers::where('contact_type', 2)
                            ->where('status', 1)
                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                return $query->where('id', $customer_id);
                            })
                            ->get();
                             
        foreach ($emploees as $key1 => $value1)
        { 
            $get_data   = $data->where('UserID', $value1->id);
            
            if($get_data->count() == 1)
            {   
                $in_time    = collect();
                $out_time   = collect();
                $in_time    = $get_data->first();
            }
            else
            {   
                $in_time    = collect();
                $out_time   = collect();
                $in_time    = $get_data->first();
                $out_time   = $get_data->last();
            }                

            $result[$value1->id]['name']        = $value1->name;
            $result[$value1->id]['employee_id'] = $value1->id;
            $result[$value1->id]['designation'] = $value1->designation;
            $result[$value1->id]['in_time']     = isset($in_time['DateTime']) ? date('H:i', strtotime($in_time['DateTime'])) : 0;
            $result[$value1->id]['out_time']    = isset($out_time['DateTime']) ? date('H:i', strtotime($out_time['DateTime'])) : 0;
        }

        if (isset($result))
        {
            $result = $result;
        }
        else
        {
            $result = [];
        }

        $customer_name  = Customers::find($customer_id);

        return view('attendance::manual_attendance', compact('date', 'result', 'customer_name'));
    }

    public function manualAttendanceStore(Request $request)
    {
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            foreach ($data['employee_id'] as $key => $value)
            {
                $log_data   = MachineDataLog::where('date', date('Y-m-d', strtotime($data['date'])))
                                        ->where('UserID', $value)
                                        ->orderBy('DateTime', 'ASC')
                                        ->select('AID as AID',
                                                 'UserID as UserID',
                                                  DB::Raw('DATE(DateTime) as DateTimeGr'),
                                                  'DateTime as DateTime',
                                                  'MachineID as MachineID',
                                                  'InOutStatus as InOutStatus',
                                                  'timestamp as timestamp'
                                                )
                                        ->get();

                if ($log_data->count() == 0)
                {
                    $in_time     = date('Y-m-d', strtotime($data['date'])) . ' ' . $data['in_time'][$key] . ':' . '00';
                    $out_time    = date('Y-m-d', strtotime($data['date'])) . ' ' . $data['out_time'][$key] . ':' . '00';

                    if ($data['in_time'][$key] != null)
                    {
                        $add_attendance                 = new MachineDataLog;
                        $add_attendance->UserID         = $value;
                        $add_attendance->date           = date('Y-m-d', strtotime($data['date']));
                        $add_attendance->InOutStatus    = 0;
                        $add_attendance->DateTime       = date('Y-m-d H:i:s', strtotime($in_time));
                        $add_attendance->save();
                    }

                    if ($data['out_time'][$key] != null)
                    {
                        $add_attendance                 = new MachineDataLog;
                        $add_attendance->UserID         = $value;
                        $add_attendance->date           = date('Y-m-d', strtotime($data['date']));
                        $add_attendance->InOutStatus    = 0;
                        $add_attendance->DateTime       = date('Y-m-d H:i:s', strtotime($out_time));
                        $add_attendance->save();
                    }    
                }

                if ($log_data->count() > 0)
                {
                    $in_time     = date('Y-m-d', strtotime($data['date'])) . ' ' . $data['in_time'][$key] . ':' . '00';
                    $out_time    = date('Y-m-d', strtotime($data['date'])) . ' ' . $data['out_time'][$key] . ':' . '00';

                    if ($log_data->count() == 1)
                    {
                        $find_in_time   = MachineDataLog::where('date', date('Y-m-d', strtotime($data['date'])))
                                                    ->where('UserID', $value)
                                                    ->orderBy('DateTime', 'ASC')
                                                    ->select('AID as AID',
                                                             'UserID as UserID',
                                                              DB::Raw('DATE(DateTime) as DateTimeGr'),
                                                              'DateTime as DateTime',
                                                              'MachineID as MachineID',
                                                              'InOutStatus as InOutStatus',
                                                              'timestamp as timestamp'
                                                            )
                                                    ->first();

                        $up_in_time                 = MachineDataLog::find($find_in_time['AID']);
                        $up_in_time->UserID         = $value;
                        $up_in_time->date           = date('Y-m-d', strtotime($data['date']));
                        $up_in_time->DateTime       = date('Y-m-d H:i:s', strtotime($in_time));
                        $up_in_time->save();

                        if ($data['out_time'][$key] != null)
                        {
                            $add_attendance                 = new MachineDataLog;
                            $add_attendance->UserID         = $value;
                            $add_attendance->date           = date('Y-m-d', strtotime($data['date']));
                            $add_attendance->InOutStatus    = 0;
                            $add_attendance->DateTime       = date('Y-m-d H:i:s', strtotime($out_time));
                            $add_attendance->save();
                        } 
                    }  

                    if ($log_data->count() > 1)
                    {
                        $find_in_time   = MachineDataLog::where('date', date('Y-m-d', strtotime($data['date'])))
                                                    ->where('UserID', $value)
                                                    ->orderBy('DateTime', 'ASC')
                                                    ->select('AID as AID',
                                                             'UserID as UserID',
                                                              DB::Raw('DATE(DateTime) as DateTimeGr'),
                                                              'DateTime as DateTime',
                                                              'MachineID as MachineID',
                                                              'InOutStatus as InOutStatus',
                                                              'timestamp as timestamp'
                                                            )
                                                    ->first();

                        $find_out_time  = MachineDataLog::where('date', date('Y-m-d', strtotime($data['date'])))
                                                    ->where('UserID', $value)
                                                    ->orderBy('DateTime', 'DESC')
                                                    ->select('AID as AID',
                                                             'UserID as UserID',
                                                              DB::Raw('DATE(DateTime) as DateTimeGr'),
                                                              'DateTime as DateTime',
                                                              'MachineID as MachineID',
                                                              'InOutStatus as InOutStatus',
                                                              'timestamp as timestamp'
                                                            )
                                                    ->first();

                        $up_in_time                 = MachineDataLog::find($find_in_time['AID']);
                        $up_in_time->UserID         = $value;
                        $up_in_time->date           = date('Y-m-d', strtotime($data['date']));
                        $up_in_time->DateTime       = date('Y-m-d H:i:s', strtotime($in_time));
                        $up_in_time->save();

                        $up_out_time                = MachineDataLog::find($find_out_time['AID']);
                        $up_out_time->UserID        = $value;
                        $up_out_time->date          = date('Y-m-d', strtotime($data['date']));
                        $up_out_time->DateTime      = date('Y-m-d H:i:s', strtotime($out_time));
                        $up_out_time->save();
                    }
                }
            }

            DB::commit();
            return back()->with("success","Attendance Updated Successfully.");

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }
}
