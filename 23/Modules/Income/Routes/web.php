<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('incomes')->group(function() {
    Route::get('/', 'IncomeController@index')->name('incomes_index');
    Route::post('/store', 'IncomeController@store')->name('incomes_store');
    Route::get('/edit/{id}', 'IncomeController@edit')->name('incomes_edit');
    Route::post('/update/{id}', 'IncomeController@update')->name('incomes_update');
    Route::get('/income/list/load', 'IncomeController@incomeListLoad')->name('incomes_list_load');
    Route::get('/income/number/load', 'IncomeController@incomeNumberLoad')->name('incomes_number_load');
    Route::get('/income/search/list/{get_income_date}/{get_income_category}/{get_income_number}/{get_income_amount}', 'IncomeController@incomeListSearch')->name('incomes_list_search');
});

Route::prefix('incomes-category')->group(function() {
    Route::post('/store', 'IncomeController@categoryStore')->name('incomes_categories_store');
    Route::get('/edit/{id}', 'IncomeController@categoryEdit')->name('incomes_categories_edit');
    Route::post('/update/{id}', 'IncomeController@categoryUpdate')->name('incomes_categories_update');
});
