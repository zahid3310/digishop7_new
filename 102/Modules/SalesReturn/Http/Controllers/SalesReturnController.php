<?php

namespace Modules\SalesReturn\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\SalesReturn;
use App\Models\SalesReturnEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Users;
use App\Models\UnitConversions;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\Branches;
use Validator;
use Auth;
use Response;
use DB;

class SalesReturnController extends Controller
{
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $paid_accounts  = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();

        return view('salesreturn::index', compact('paid_accounts'));
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('salesreturn::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'return_date'     => 'required',
            'customer_id'     => 'required',
            'invoice_id'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat  = $data['vat_amount'];
            $data_find                          = SalesReturn::orderBy('created_at', 'DESC')->first();
            $sales_return_number                = $data_find != null ? $data_find['sales_return_number'] + 1 : 1;

            $sales_return                       = new SalesReturn;
            $sales_return->sales_return_number  = $sales_return_number;
            $sales_return->customer_id          = $data['customer_id'];
            $sales_return->invoice_id           = $data['invoice_id'];
            $sales_return->sales_return_date    = date('Y-m-d', strtotime($data['return_date']));
            $sales_return->return_note          = $data['return_note'];
            $sales_return->sub_total_amount     = $data['total_return_amount'];
            $sales_return->return_amount        = $data['total_return_amount'];
            $sales_return->due_amount           = $data['total_return_amount'] - $data['total_return_amount_paid'];
            $sales_return->total_vat            = $vat;
            $sales_return->vat_type             = $data['vat_type'];
            $sales_return->total_discount_type  = $data['total_discount_type'];
            $sales_return->total_discount_amount= $data['total_discount_amount'];
            $sales_return->less                 = $data['less'];
            $sales_return->branch_id            = $branch_id;
            $sales_return->created_by           = $user_id;

            if ($sales_return->save())
            {
                $update_invoice                 =  Invoices::find($data['invoice_id']);
                $update_invoice->return_amount  =  $update_invoice['return_amount'] + $data['total_return_amount'];
                $update_invoice->save();

                foreach ($data['product_id'] as $key => $value)
                {
                    if ($data['return_quantity'][$key] != 0)
                    {
                        $product    = ProductEntries::find($data['product_entries'][$key]);
                        $buy_price  = $product['buy_price'];

                        $sales_return_entries[] = [
                            'invoice_id'         => $sales_return['invoice_id'],
                            'sales_return_id'    => $sales_return['id'],
                            'product_id'         => $value,
                            'product_entry_id'   => $data['product_entries'][$key],
                            'customer_id'        => $sales_return['customer_id'],
                            'main_unit_id'       => $data['main_unit_id'][$key],
                            'conversion_unit_id' => $data['unit_id'][$key],
                            'pcs'                => isset($data['pcs']) ? $data['pcs'][$key] : 0,
                            'cartoon'            => isset($data['cartoon'][$key]) ? $data['cartoon'][$key] : 0,
                            'buy_price'          => $buy_price,
                            'rate'               => $data['amount'][$key]/$data['quantity'][$key],
                            'quantity'           => $data['return_quantity'][$key],
                            'total_amount'       => ($data['amount'][$key]/$data['quantity'][$key])*$data['return_quantity'][$key],
                            'branch_id'          => $branch_id,
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('sales_return_entries')->insert($sales_return_entries);

                stockInReturn($data, $item_id=null);

                //Financial Accounting Start
                    debit($customer_id=$data['customer_id'], $date=$data['return_date'], $account_id=4, $amount=$data['total_return_amount'], $note=null, $transaction_head='sales-return', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=$sales_return->id, $purchase_return_id=null);
                    credit($customer_id=$data['customer_id'], $date=$data['return_date'], $account_id=9, $amount=$data['total_return_amount'], $note=null, $transaction_head='sales-return', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=$sales_return->id, $purchase_return_id=null);

                    //Insert into journal_entries 
                    if ((isset($data['total_return_amount_paid'])) && ($data['total_return_amount_paid'] > 0))
                    {
                        debit($customer_id=$data['customer_id'], $date=$data['return_date'], $account_id=9, $amount=$data['total_return_amount_paid'], $note=null, $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=$sales_return->id, $purchase_return_id=null);
                        credit($customer_id=$data['customer_id'], $date=$data['return_date'], $account_id=$data['paid_through'], $amount=$data['total_return_amount_paid'], $note=null, $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=$sales_return->id, $purchase_return_id=null);
                    }

                    customerBalanceUpdate($data['customer_id']);
                //Financial Accounting End

                DB::commit();
                return back()->with("success","Sales Return Created Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $sales_return       = SalesReturn::leftjoin('customers', 'customers.id', 'sales_return.customer_id')
                                        ->select('sales_return.*',
                                                 'customers.name as customer_name',
                                                 'customers.address as address',
                                                 'customers.phone as phone')
                                        ->find($id);

        $entries            = SalesReturnEntries::leftjoin('products', 'products.id', 'sales_return_entries.product_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'sales_return_entries.product_entry_id')
                                        ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                        ->where('sales_return_entries.sales_return_id', $id)
                                        ->select('sales_return_entries.*',
                                                 'product_entries.name as product_entry_name',
                                                 'product_entries.product_code as product_code',
                                                 'product_entries.type as type',
                                                 'sizes.height as height',
                                                 'sizes.width as width',
                                                 'products.name as product_name')
                                        ->get();  
       
        $user_info  = userDetails();

        return view('salesreturn::show', compact('entries', 'sales_return', 'user_info'));
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('salesreturn::edit');
    }

    public function update(Request $request, $id)
    {
    }

    public function delete($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        DB::beginTransaction();

        try{
            $sales_return       = SalesReturn::find($id);
            $entries            = SalesReturnEntries::where('sales_return_id', $id)->get();
            $payment_entries    = PaymentEntries::where('payment_entries.sales_return_id', $id)->get();

            //Invoice Update
            $invoice                    = Invoices::find($sales_return['invoice_id']);
            $invoice->return_amount     = $invoice['return_amount'] - $sales_return['return_amount'];
            $invoice->save();

            //stock Update
            foreach ($entries as $key => $value)
            {   
                $conversion_rate_find             = UnitConversions::where('main_unit_id', $value['main_unit_id'])
                                                            ->where('converted_unit_id', $value['conversion_unit_id'])
                                                            ->where('product_entry_id', $value['product_entry_id'])
                                                            ->first();

                $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $value['quantity']/$conversion_rate_find['conversion_rate'] : $value['quantity'];


                //product entries table update
                $product_entry                    = ProductEntries::find($value['product_entry_id']);
                $product_entry->stock_in_hand     = $product_entry['stock_in_hand'] - $converted_quantity_to_main_unit;
                $product_entry->total_sold        = $product_entry['total_sold'] + $value['quantity'];
                $product_entry->save();
            }

            $sales_return->delete();
            
            $delete_current_balance     = CurrentBalance::where('customer_id', $sales_return['customer_id'])->where('transaction_head', 'sales-return')->where('associated_id', $id)->delete();
            $delete_transaction         = AccountTransactions::where('customer_id', $sales_return['customer_id'])->where('transaction_head', 'sales-return')->where('associated_id', $id)->delete();
            $delete_trans               = Transactions::where('customer_id', $sales_return['customer_id'])->where('account_head', 'sales-return')->where('sales_return_id', $id)->delete();

            foreach ($payment_entries as $key1 => $value1)
            {
                $find_payment    = Payments::find($value1['payment_id']);

                if ($find_payment)
                {
                   $find_payment ->delete();
                }
            }

            DB::commit();
            return back()->with("success","Sales Return Deleted Successfully !!");

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Deleted");
        }
    }

    public function invoiceList($id)
    {
        $data           = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                ->where('invoices.customer_id', $id)
                                ->orderBy('invoices.created_at', 'DESC')
                                ->select('invoices.*',
                                         'customers.name as customer_name')
                                ->get();
   
        return Response::json($data);
    }

    public function invoiceEntriesList($id)
    {
        $invoice_entry          = InvoiceEntries::leftjoin('customers', 'customers.id', 'invoice_entries.customer_id')
                                        ->leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                        ->where('invoice_entries.invoice_id', $id)
                                        ->orderBy('invoice_entries.created_at', 'DESC')
                                        ->select('invoice_entries.*',
                                                 'products.name as product_name',
                                                 'product_entries.name as entry_name',
                                                 'product_entries.product_code as product_code',
                                                 'product_entries.type as product_type',
                                                 'customers.name as customer_name')
                                        ->get();

        $sales_return_entry     = SalesReturnEntries::where('sales_return_entries.invoice_id', $id)
                                        ->get();

        $invoice                = Invoices::where('invoices.id', $id)
                                        ->select('invoices.*')
                                        ->first();

        if ($invoice_entry->count() > 0)
        {
            foreach ($invoice_entry as $key => $value)
            {   
                // $variation = ProductVariationName($value['product_entry_id']);

                if ($value->productEntries->size_id != null)
                {
                    $dimension  = $value->productEntries->size['height'].'X'.$value->productEntries->size['width'] . ' - ';
                }
                else
                {
                    $dimension  = '';
                }

                if ($value->productEntries['brand_id'] != null)
                {
                    $brand  = $value->productEntries->brand['name']. ' - ';
                }
                else
                {
                    $brand  = '';
                }

                if ($value->productEntries['product_code'] != null)
                {
                    $code  =  $value->productEntries['product_code']. ' - ';
                }
                else
                {
                    $code  = '';
                }
                
                if ($value->productEntries['grade'] != null)
                {
                    $grade  = $value->productEntries->grade['name'] . ' - ';
                }
                else
                {
                    $grade  = '';
                }

                if ($value->productEntries->product_id != null)
                {
                    $category = $value->productEntries->product['name'] . ' - ';
                }
                else
                {
                    $category = '';
                }

                $name   = $grade . $code. $category . $dimension .$brand.$value->productEntries['name'];

                $sales_return_quantity_sum                          = $sales_return_entry->where('product_entry_id', $value['product_entry_id']) ->sum('quantity');
                $entry_data[$value['id']]['id']                     =  $value['id'];
                $entry_data[$value['id']]['invoice_id']             =  $value['invoice_id'];
                $entry_data[$value['id']]['product_id']             =  $value['product_id'];
                $entry_data[$value['id']]['product_entry_id']       =  $value['product_entry_id'];
                $entry_data[$value['id']]['main_unit_id']           =  $value->productEntries->type != 1 ? $value->productEntries->unit_id : '';
                $entry_data[$value['id']]['converted_unit_id']      =  $value->productEntries->type != 1 ? $value->conversion_unit_id : '';
                $entry_data[$value['id']]['converted_unit_name']    =  $value->productEntries->type != 1 ? $value->convertedUnit->name : '';
                $entry_data[$value['id']]['type']                   =  $value->productEntries->type;
                $entry_data[$value['id']]['product_code']           =  $value['product_code'];
                $entry_data[$value['id']]['product_type']           =  $value['product_type'];
                $entry_data[$value['id']]['customer_id']            =  $value['customer_id'];
                $entry_data[$value['id']]['buy_price']              =  $value['buy_price'];
                $entry_data[$value['id']]['rate']                   =  $value['rate'];
                $entry_data[$value['id']]['original_quantity']      =  $value['quantity'];
                $entry_data[$value['id']]['quantity']               =  $value['quantity'] - $sales_return_quantity_sum;
                $entry_data[$value['id']]['total_amount']           =  $value['total_amount'];
                $entry_data[$value['id']]['discount_type']          =  $value['discount_type'];
                $entry_data[$value['id']]['discount_amount']        =  $value['discount_amount'];
                $entry_data[$value['id']]['product_name']           =  $name;
                $entry_data[$value['id']]['entry_name']             =  $value['entry_name'];
                $entry_data[$value['id']]['customer_name']          =  $value['customer_name'];
            }
        }
        else
        {
            $entry_data = [];
        }

        $data['invoice']            = $invoice;
        $data['invoice_entries']    = $entry_data;
   
        return Response::json($data);
    }

    public function salesReturnListLoad()
    {
        $branch_id      = Auth::user()->branch_id;
        $data           = SalesReturn::leftjoin('invoices', 'invoices.id', 'sales_return.invoice_id')
                                ->leftjoin('customers', 'customers.id', 'sales_return.customer_id')
                                ->where('sales_return.branch_id', $branch_id)
                                ->orderBy('sales_return.created_at', 'DESC')
                                ->select('sales_return.*',
                                         'customers.name as customer_name',
                                         'customers.phone as phone',
                                         'invoices.invoice_number as invoice_number')
                                ->get();

        return Response::json($data);
    }

    public function salesReturnListSearch($id)
    {
        $branch_id                   = Auth::user()->branch_id;
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data           = SalesReturn::leftjoin('invoices', 'invoices.id', 'sales_return.invoice_id')
                                ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                ->where('customers.name', 'LIKE', "%$id%")
                                ->orWhere('sales_return.sales_return_date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                    return $query->orWhere('sales_return.sales_return_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('sales_return.created_at', 'DESC')
                                ->select('sales_return.*',
                                         'customers.name as customer_name',
                                         'customers.phone as phone',
                                         'invoices.invoice_number as invoice_number')
                                ->take(100)
                                ->get();
        }
        else
        {
            $data           = SalesReturn::leftjoin('invoices', 'invoices.id', 'sales_return.invoice_id')
                                ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                ->orderBy('sales_return.created_at', 'DESC')
                                ->select('sales_return.*',
                                         'customers.name as customer_name',
                                         'customers.phone as phone',
                                         'invoices.invoice_number as invoice_number')
                                ->take(100)
                                ->get();
        }

        $data   = $data->where('branch_id', $branch_id);

        return Response::json($data);
    }

    public function findCustomerName($id)
    {
        $data           = Customers::find($id);

        return Response::json($data);
    }

    public function findInvoiceDetails($id)
    {
        $data           = Invoices::find($id);

        return Response::json($data);
    }

    public function calculateOpeningBalance($customer_id)
    {
        $data  = calculateOpeningBalance($customer_id);

        return Response::json($data);
    }
}
