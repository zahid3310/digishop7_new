@extends('layouts.app')

@section('title', 'Show')

<style type="text/css">
    @media print {
        #footerInvoice {
            position: fixed;
            bottom: 60;
        }
    }

    .textstyle {
        background-color: white; /* Changing background color */
        font-weight: bold; /* Making font bold */
        border-radius: 20px; /* Making border radius */
        border: 3px solid black; /* Making border radius */
        width: auto; /* Making auto-sizable width */
        height: auto; /* Making auto-sizable height */
        padding: 5px 10px 5px 10px; /* Making space around letters */
        font-size: 18px; /* Changing font size */
    }

    .column-bordered-table thead td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table tfoot tr {
        border-top: 1px solid #c3c3c3;
        border-bottom: 1px solid #c3c3c3;
    }
</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div class="row">
                                    @if($user_info['header_image'] == null)
                                        <div class="col-md-2 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-8 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 55px">{{ $user_info['organization_name'] }}</h2>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold">{{ $user_info['contact_email'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_number'] }}</p>
                                        </div>
                                        <div class="col-md-2 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-5 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-2 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 20px;margin-bottom: 0px;text-align: center;font-size: 26px;" class="textstyle">Invoice</h2>
                                        </div>
                                        <div class="col-md-5 col-xs-12 col-sm-12"></div>
                                    @else
                                        <img class="float-left" src="{{ url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>

                                <div style="margin-top: 20px" class="row">
                                    <div style="font-size: 16px" class="col-sm-4 col-6">
                                        <address>
                                            <strong>Customer Name &nbsp;&nbsp;: 
                                            {{ $invoice['customer_name'] != null ? $invoice['customer_name'] : $invoice['contact_name']  }} </strong><br>
                                            <strong>Address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong>
                                            @if($invoice['customer_address'] != null)
                                               <?php echo $invoice['customer_address']; ?> <br>
                                            @else
                                            <?php echo $invoice['address']; ?> <br>
                                            @endif
                                            <strong>Mobile &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                            {{ $invoice['contact_phone'] != null ? $invoice['contact_phone'] : $invoice['contact_phone']  }}</strong>
                                        </address>
                                    </div>

                                    <div class="col-sm-3 hidden-xs">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div style="font-size: 16px" class="col-sm-5 col-6 text-sm-left">
                                        <address style=";border-bottom: 2px solid black">
                                            <strong>Invoice Number &nbsp;&nbsp;: </strong>
                                            {{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }} <br>
                                            <strong>Ref No &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong>

                                            <br>
                                            <strong>Sold By &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong>
                                            {{ $invoice->createdBy->name }}
                                            <br>
                                        </address>
                                        <address>
                                            <strong>Date & Time : </strong>
                                            {{ date('d/m/Y', strtotime($invoice['invoice_date'])) }} &nbsp;&nbsp;&nbsp; Print Time : {{  date('h:i:s A', strtotime($invoice['created_at'])) }}
                                        </address>
                                    </div>
                                </div>

                                <div style="border: 2px solid black;height: 1180px" class="table-responsive">
                                    <table style="border-bottom: 1px solid gray" class="table column-bordered-table">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th style="width: 70px;">SL</th>
                                                <th>Description</th>
                                                <th class="text-right">Sold Qty</th>
                                                <th class="text-right">Unit Price</th>
                                                <th class="text-right">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">

                                            @if(!empty($entries) && ($entries->count() > 0))

                                            <?php $sub_total = 0; ?>

                                            @foreach($entries as $key => $value)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>
                                                    @if($value['product_type'] == 1)
                                                        <?php echo $value['product_entry_name']; ?>
                                                    @else
                                                        <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                                    @endif

                                                    <?php
                                                        $w_pr = WarrentyProducts($value['warrenty_product_id']);
                                                    ?>

                                                    @if($w_pr != null)
                                                    <br>
                                                    <br>
                                                    <strong>
                                                        <?php echo $w_pr; ?>
                                                    </strong>
                                                    @endif
                                                </td>
                                                <td class="text-right">{{ number_format($value['quantity'],2,'.',',') }}</td>
                                                <td class="text-right">{{ number_format($value['rate'],2,'.',',') }}</td>
                                                <td class="text-right">{{ number_format($value['total_amount'],2,'.',',') }}</td>
                                            </tr>

                                            <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                            @endforeach
                                            @endif
                                        
                                        </tbody>
                                    </table>

                                    <div style="margin-top: 20px" class="row">
                                        <div style="vertical-align: middle;text-align: center" class="col-sm-7 hidden-xs">
                                            @if($invoice['due_amount'] == 0)
                                            <h1>PAID</h1>
                                            @endif
                                        </div>

                                        <div style="font-size: 16px;border-bottom: 3px solid black" class="col-sm-5 col-6 text-sm-left">
                                            <address>
                                                Total Amount 
                                                <span style="float: right;padding-right: 10px">{{ number_format($sub_total,2,'.',',') }}</span> <br>

                                                Add Vat 
                                                <span style="float: right;padding-right: 10px">{{ $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : number_format($invoice['total_vat'],2,'.',',') }}</span> <br>

                                                Less Discount 
                                                <span style="float: right;padding-right: 10px">{{ $invoice['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$invoice['total_vat'])/100))*$invoice['total_discount_amount'])/100) : number_format($invoice['total_discount_amount'],2,'.',',') }}</span> <br>

                                                Add Installation / Service Charge 
                                                <span style="float: right;padding-right: 10px"></span> <br>
                                                
                                            </address>
                                        </div>
                                    </div>

                                    <div style="margin-top: 5px" class="row">
                                        <div style="vertical-align: middle;text-align: center" class="col-sm-7 hidden-xs">
                                        </div>

                                        <div style="font-size: 16px;" class="col-sm-5 col-6 text-sm-left">
                                            <address>
                                                Net Payable Amount
                                                <span style="float: right;padding-right: 10px">{{ number_format($invoice['invoice_amount'] - $invoice['due_amount'],2,'.',',') }}</span> 
                                            </address>
                                        </div>
                                    </div>

                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>

                                    <div class="row">

                                        <div style="text-align: left" class="col-md-6">
                                            
                                            <span style="border-top: 1px solid black"> Customers Signature  </span>
                                        </div>

                                        <div style="text-align: right" class="col-md-6">

                                            <span style="border-top: 1px solid black"> Authorised Signature </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection