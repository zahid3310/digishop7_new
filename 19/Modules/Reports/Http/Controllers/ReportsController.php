<?php

namespace Modules\Reports\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\ExpenseEntries;
use App\Models\SalesReturn;
use App\Models\SalesReturnEntries;
use App\Models\PurchaseReturn;
use App\Models\PurchaseReturnEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Expenses;
use App\Models\Incomes;
use App\Models\Users;
use App\Models\ProductVariations;
use App\Models\ProductVariationValues;
use App\Models\ProductSuppliers;
use App\Models\ProductCustomers;
use App\Models\StockTransfers;
use App\Models\BranchInventories;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\Branches;
use App\Models\Transactions;
use Carbon\Carbon;
use Response;
use Auth;
use DB;

class ReportsController extends Controller
{
    public function stockReport()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $product_id         = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $major_category_id  = isset($_GET['major_category_id']) ? $_GET['major_category_id'] : 0;
        $branch_id          = Auth()->user()->branch_id;

        $data           = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->when($product_id != 0, function ($query) use ($product_id) {
                                        return $query->where('products.id', $product_id);
                                    })
                                    ->when($major_category_id != 0, function ($query) use ($major_category_id) {
                                        return $query->where('product_entries.brand_id', $major_category_id);
                                    })
                                    ->select('product_entries.*', 
                                             'units.name as unit_name',
                                             'products.name as category_name')
                                    ->orderBy('product_entries.name', 'ASC')
                                    ->get();

        $products       = Products::orderBy('products.total_sold', 'DESC')->get();
        $major_category = Categories::find($major_category_id);
        $branches       = Branches::when($branch_id != 1, function ($query) use ($branch_id) {
                                        return $query->where('id', $branch_id);
                                    })
                                    ->orderBy('created_at', 'ASC')
                                    ->get();
        $user_info      = Users::find(1);

        return view('reports::stock_report', compact('data', 'user_info', 'products', 'branches', 'branch_id', 'major_category'));
    }

    public function stockDetailsReport($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $product_id     = isset($_GET['product_id']) ? $_GET['product_id'] : 0;

        $product        = Products::find($id);
        $data           = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                ->when($product_id != 0, function ($query) use ($product_id) {
                                    return $query->where('product_entries.id', $product_id);
                                })
                                ->where('product_entries.product_id', $id)
                                ->orderBy('product_entries.total_sold', 'DESC')
                                ->select('product_entries.*', 'units.name as unit_name')
                                ->get();

        $products       = ProductEntries::where('product_entries.product_id', $id)->orderBy('product_entries.total_sold', 'DESC')->get();
        $products->sortBy('name');
        $user_info      = Users::find(1);

        return view('reports::stock_details_report', compact('data', 'product', 'user_info', 'products'));
    }

    public function salesReport()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;

        $invoices               = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('invoices.customer_id', $customer_id);
                                                })
                                                ->where('invoices.type', 1)
                                                ->select('invoices.invoice_date as invoice_date',
                                                    'invoices.id as id',
                                                    'invoices.invoice_number as invoice_number',
                                                    'invoices.customer_id as customer_id',
                                                    'customers.name as customer_name',
                                                    'invoices.invoice_amount as invoice_amount',
                                                    'invoices.return_amount as return_amount',
                                                    'invoices.due_amount as due_amount',
                                                    'invoices.invoice_note as invoice_note',
                                                    'invoices.adjustment_type as adjustment_type',
                                                    'invoices.total_adjustment as total_adjustment',
                                                    'invoices.vat_type as vat_type',
                                                    'invoices.total_vat as total_vat',
                                                    'invoices.tax_type as tax_type',
                                                    'invoices.total_tax as total_tax')
                                                ->get();

        $invoice_entries        = InvoiceEntries::leftjoin('invoices', 'invoices.id', 'invoice_entries.invoice_id')
                                                ->leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->select('invoice_entries.*',
                                                         'product_entries.name as product_entry_name',
                                                         'products.name as product_name',
                                                         'product_entries.product_code as product_code')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('payments.customer_id', $customer_id);
                                                })
                                                ->select('payment_entries.*')
                                                ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->leftjoin('sales_return', 'sales_return.id', 'payment_entries.sales_return_id')
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('payments.customer_id', $customer_id);
                                                })
                                                ->where('payments.type', 2)
                                                ->select('payment_entries.*',
                                                         'sales_return.invoice_id as return_invoice_id')
                                                ->get();

        $total_invoice_amount       = 0;
        $total_sales_return         = 0;
        $total_paid_amount          = 0;
        $total_return_paid          = 0;
        $total_due_amount           = 0;
        $total_adjustment_amount    = 0;
        $total_tax_amount           = 0;
        $total_vat_amount           = 0;
        $total_return_due           = 0;

        if ($invoices->count() > 0)
        {
            foreach ($invoices as $key => $value)
            {
                $sub_total_value                            = $invoice_entries->where('invoice_id', $value['id'])->sum('total_amount');
                $perc_adjustment                            = ($sub_total_value * $value['total_adjustment'])/100;
                $perc_vat                                   = ($sub_total_value * $value['total_vat'])/100;
                $perc_tax                                   = ($sub_total_value * $value['total_tax'])/100;
                $sales_return_paid                          = $payment_entries_return->where('return_invoice_id', $value['id'])->sum('amount');

                $data[$value['id']]['invoice_number']       = 'INV - ' . str_pad($value['invoice_number'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['invoice_date']         = date('d-m-Y', strtotime($value['invoice_date']));
                $data[$value['id']]['invoice_id']           = $value['id'];
                $data[$value['id']]['customer_name']        = $value['customer_name'];
                $data[$value['id']]['invoice_amount']       = $value['invoice_amount'];
                $data[$value['id']]['return_amount']        = $value['return_amount'];
                $data[$value['id']]['paid_amount']          = $payment_entries->where('invoice_id', $value['id'])->sum('amount');
                $data[$value['id']]['return_paid']          = $sales_return_paid;
                $data[$value['id']]['due_amount']           = $value['due_amount'];
                $data[$value['id']]['return_due']           = $value['return_amount'] - $sales_return_paid;
                $data[$value['id']]['invoice_note']         = $value['invoice_note'];
                $data[$value['id']]['adjustment_type']      = $value['adjustment_type'];
                $data[$value['id']]['adjustment']           = $value['total_adjustment'];
                $data[$value['id']]['adjustment_perc']      = $perc_adjustment;
                $data[$value['id']]['vat_type']             = $value['vat_type'];
                $data[$value['id']]['vat']                  = $value['total_vat'];
                $data[$value['id']]['vat_perc']             = $perc_vat;
                $data[$value['id']]['tax_type']             = $value['tax_type'];
                $data[$value['id']]['tax']                  = $value['total_tax'];
                $data[$value['id']]['tax_perc']             = $perc_tax;
                $data[$value['id']]['sub_total']            = $sub_total_value;
                $data[$value['id']]['invoice_entries']      = $invoice_entries->where('invoice_id', $value['id']);

                $total_invoice_amount                       = $total_invoice_amount + $value['invoice_amount'];
                $total_sales_return                         = $total_sales_return + $value['return_amount'];
                $total_paid_amount                          = $total_paid_amount + ($payment_entries->where('invoice_id', $value['id'])->sum('amount'));
                $total_due_amount                           = $total_due_amount + $value['due_amount'];
                $total_adjustment_amount                    = $total_adjustment_amount + ($value['adjustment_type'] == 0 ? $perc_adjustment : $value['total_adjustment']);
                $total_tax_amount                           = $total_tax_amount + ($value['tax_type'] == 0 ? $perc_tax : $value['total_tax']);
                $total_vat_amount                           = $total_vat_amount + ($value['vat_type'] == 0 ? $perc_vat : $value['total_vat']);
                $total_return_due                           = $total_return_due + ($value['return_amount'] - $sales_return_paid); 
                $total_return_paid                          = $total_return_paid + $sales_return_paid;
            }
        }
        else
        {
            $data                                       = [];
            $total_invoice_amount                       = 0;
            $total_paid_amount                          = 0;
            $total_due_amount                           = 0;
            $total_adjustment_amount                    = 0;
            $total_tax_amount                           = 0;
            $total_vat_amount                           = 0;
        }

        $customers              = Customers::get();
        $user_info              = Users::find(1);

        return view('reports::sales_report', compact('data', 'total_invoice_amount', 'total_sales_return', 'total_return_paid', 'total_paid_amount', 'total_return_due', 'total_due_amount', 'total_adjustment_amount', 'total_tax_amount', 'total_vat_amount', 'from_date', 'to_date', 'user_info', 'customers'));
    }

    public function salesSummary()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $branch_id              = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;
        $branches               = Branches::orderBy('created_at', 'ASC')->get();

        $invoices               = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('invoices.customer_id', $customer_id);
                                                })
                                                ->when($branch_id != 1, function ($query) use ($branch_id) {
                                                    return $query->where('invoices.branch_id', $branch_id);
                                                })
                                                ->where('invoices.type', 1)
                                                ->select('invoices.invoice_date as invoice_date',
                                                    'invoices.id as id',
                                                    'invoices.invoice_number as invoice_number',
                                                    'invoices.total_discount_type as total_discount_type',
                                                    'invoices.total_discount_note as total_discount_note',
                                                    'invoices.total_discount_amount as total_discount_amount',
                                                    'invoices.total_discount as total_discount',
                                                    'invoices.invoice_amount as invoice_amount',
                                                    'invoices.return_amount as return_amount',
                                                    'invoices.due_amount as due_amount',
                                                    'invoices.cash_given as cash_given',
                                                    'invoices.customer_id as customer_id',
                                                    'customers.name as customer_name')
                                                ->get();

        $total_invoice_amount   = 0;
        $total_discount_amount  = 0;
        $total_receivable       = 0;
        $total_paid_amount      = 0;

        if ($invoices->count() > 0)
        {
            foreach ($invoices as $key => $value)
            {   
                if ($value['total_discount_type'] == 0)
                {
                    $sub_total      = $invoice_entries->where('invoice_id', $value['id'])->sum('total_amount');
                    $discount       = ($sub_total*$value['total_discount_amount'])/100;
                }
                else
                {
                    $discount       = $value['total_discount_amount'];
                }

                $data[$value['id']]['invoice_number']           = 'INV - ' . str_pad($value['invoice_number'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['invoice_date']             = date('d-m-Y', strtotime($value['invoice_date']));
                $data[$value['id']]['invoice_id']               = $value['id'];
                $data[$value['id']]['customer_name']            = $value['customer_name'];
                $data[$value['id']]['invoice_amount']           = $value['invoice_amount'];
                $data[$value['id']]['return_amount']            = $value['return_amount'];
                $data[$value['id']]['paid_amount']              = $value['cash_given'];
                $data[$value['id']]['due_amount']               = $value['due_amount'];
                $data[$value['id']]['discount']                 = $value['total_discount'];
                $data[$value['id']]['total_discount']           = $discount;
                $data[$value['id']]['total_discount_note']      = $value['total_discount_note'];
                $data[$value['id']]['total_discount_type']      = $value['total_discount_type'];
                $data[$value['id']]['total_discount_amount']    = $value['total_discount_amount'];

                $total_invoice_amount   = $total_invoice_amount + $value['invoice_amount'];
                $total_discount_amount  = $total_discount_amount + $discount;
                $total_receivable       = $total_receivable + ($value['invoice_amount']);
                $total_paid_amount      = $total_paid_amount + $value['cash_given'];
            }
        }
        else
        {
            $data = [];
        }

        $customers              = Customers::where('branch_id', $branch_id)->get();
        $user_info              = Users::find(1);

        return view('reports::sales_summary', compact('data', 'total_invoice_amount', 'total_receivable', 'total_paid_amount', 'from_date', 'to_date', 'user_info', 'customers', 'total_discount_amount', 'branches'));
    }

    public function profitLoss()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $invoices               = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->select('invoices.id as id',
                                                         'invoices.invoice_amount as invoice_amount',
                                                         'invoices.return_amount as return_amount')
                                                ->get();

        $total_expense          = Expenses::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                                })
                                                ->select('expenses.*')
                                                ->sum('amount');

        $invoice_entries        = InvoiceEntries::leftjoin('invoices', 'invoices.id', 'invoice_entries.invoice_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->select('invoice_entries.*')
                                                ->get();

        $return_entries         = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.invoice_id')
                                                ->select('sales_return_entries.*')
                                                ->get();

        $total_invoice_amount       = 0;
        $total_purchase_costs       = 0;
        $total_return_purchase_cost = 0;

        if ($invoices->count() > 0)
        {
            foreach ($invoices as $key => $value)
            {
                $find_purchase_cost               = $invoice_entries->where('invoice_id', $value['id']);
                $find_return_purchase_cost        = $return_entries->where('invoice_id', $value['id']);

                if (!empty($find_purchase_cost))
                {
                    $sub_total_purchase_cost      = 0;
                    foreach ($find_purchase_cost as $key1 => $value1)
                    {   
                        $sub_total_purchase_cost  = $sub_total_purchase_cost + ($value1['buy_price']*$value1['quantity']);
                    }

                    $total_purchase_costs         = $total_purchase_costs + $sub_total_purchase_cost;
                }

                if (!empty($find_return_purchase_cost))
                {
                    $sub_total_return_purchase_cost         = 0;
                    foreach ($find_return_purchase_cost as $key2 => $value2)
                    {   
                        $sub_total_return_purchase_cost     = $sub_total_return_purchase_cost + ($value2['buy_price']*$value2['quantity']);
                    }

                    $total_return_purchase_cost             = $total_return_purchase_cost + $sub_total_return_purchase_cost;
                }

                    $total_invoice_amount         = $total_invoice_amount + $value['invoice_amount'] - $value['return_amount'];
            }

                    $total_purchase_cost          = $total_purchase_costs - $total_return_purchase_cost;
        }
        else
        {
            $total_purchase_cost        = 0;   
            $total_invoice_amount       = 0;     
        }

        $user_info  = Users::find(1);

        return view('reports::profit_loss', compact('from_date', 'to_date', 'user_info', 'total_invoice_amount', 'total_purchase_cost', 'total_expense'));
    }

    public function purchaseReport()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $supplier_id            = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;

        $bills                  = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('bills.vendor_id', $supplier_id);
                                                })
                                                ->where('bills.type', 1)
                                                ->select('bills.bill_date as bill_date',
                                                    'bills.id as id',
                                                    'bills.bill_number as bill_number',
                                                    'bills.vendor_id as vendor_id',
                                                    'customers.name as vendor_name',
                                                    'bills.bill_amount as bill_amount',
                                                    'bills.due_amount as due_amount',
                                                    'bills.return_amount as return_amount',
                                                    'bills.bill_note as bill_note',
                                                    'bills.adjustment_type as adjustment_type',
                                                    'bills.total_adjustment as total_adjustment',
                                                    'bills.vat_type as vat_type',
                                                    'bills.total_vat as total_vat',
                                                    'bills.tax_type as tax_type',
                                                    'bills.total_tax as total_tax')
                                                ->get();

        $bill_entries           = BillEntries::leftjoin('bills', 'bills.id', 'bill_entries.bill_id')
                                                ->leftjoin('products', 'products.id', 'bill_entries.product_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->select('bill_entries.*',
                                                         'product_entries.name as product_entry_name',
                                                         'products.name as product_name',
                                                         'product_entries.product_code as product_code')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                        })
                                        ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('payments.customer_id', $supplier_id);
                                        })
                                        ->select('payment_entries.*')
                                        ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->leftjoin('purchase_return', 'purchase_return.id', 'payment_entries.purchase_return_id')
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('payments.customer_id', $supplier_id);
                                                })
                                                ->where('payments.type', 3)
                                                ->select('payment_entries.*',
                                                         'purchase_return.bill_id as return_bill_id')
                                                ->get();

        $total_bill_amount          = 0;
        $total_paid_amount          = 0;
        $total_due_amount           = 0;
        $total_adjustment_amount    = 0;
        $total_tax_amount           = 0;
        $total_vat_amount           = 0;
        $total_return_due           = 0;
        $total_return_paid          = 0;

        if ($bills->count() > 0)
        {
            foreach ($bills as $key => $value)
            {   
                $sub_total_value                            = $bill_entries->where('bill_id', $value['id'])->sum('total_amount');
                $perc_adjustment                            = ($sub_total_value * $value['total_adjustment'])/100;
                $perc_vat                                   = ($sub_total_value * $value['total_vat'])/100;
                $perc_tax                                   = ($sub_total_value * $value['total_tax'])/100;
                $purchase_return_paid                       = $payment_entries_return->where('return_bill_id', $value['id'])->sum('amount');

                $data[$value['id']]['bill_number']          = 'BILL - ' . str_pad($value['bill_number'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['bill_date']            = date('d-m-Y', strtotime($value['bill_date']));
                $data[$value['id']]['bill_id']              = $value['id'];
                $data[$value['id']]['vendor_name']          = $value['vendor_name'];
                $data[$value['id']]['bill_amount']          = $value['bill_amount'];
                $data[$value['id']]['paid_amount']          = $payment_entries->where('bill_id', $value['id'])->sum('amount');
                $data[$value['id']]['due_amount']           = $value['due_amount'];
                $data[$value['id']]['return_amount']        = $value['return_amount'];
                $data[$value['id']]['return_paid']          = $purchase_return_paid;
                $data[$value['id']]['return_due']           = $value['return_amount'] - $purchase_return_paid;
                $data[$value['id']]['bill_note']            = $value['bill_note'];
                $data[$value['id']]['adjustment_type']      = $value['adjustment_type'];
                $data[$value['id']]['adjustment']           = $value['total_adjustment'];
                $data[$value['id']]['adjustment_perc']      = $perc_adjustment;
                $data[$value['id']]['vat_type']             = $value['vat_type'];
                $data[$value['id']]['vat']                  = $value['total_vat'];
                $data[$value['id']]['vat_perc']             = $perc_vat;
                $data[$value['id']]['tax_type']             = $value['tax_type'];
                $data[$value['id']]['tax']                  = $value['total_tax'];
                $data[$value['id']]['tax_perc']             = $perc_tax;
                $data[$value['id']]['sub_total']            = $sub_total_value;
                $data[$value['id']]['bill_entries']         = $bill_entries->where('bill_id', $value['id']);

                $total_bill_amount                          = $total_bill_amount + $value['bill_amount'];
                $total_paid_amount                          = $total_paid_amount + ($payment_entries->where('bill_id', $value['id'])->sum('amount'));
                $total_due_amount                           = $total_due_amount + $value['due_amount'];
                $total_adjustment_amount                    = $total_adjustment_amount + ($value['adjustment_type'] == 0 ? $perc_adjustment : $value['total_adjustment']);
                $total_tax_amount                           = $total_tax_amount + ($value['tax_type'] == 0 ? $perc_tax : $value['total_tax']);
                $total_vat_amount                           = $total_vat_amount + ($value['vat_type'] == 0 ? $perc_vat : $value['total_vat']);
                $total_return_due                           = $total_return_due + ($value['return_amount'] - $purchase_return_paid);
                $total_return_paid                          = $total_return_paid + $purchase_return_paid;
            }
        }
        else
        {
            $data  =  [];   
        }

        $customers              = Customers::get();
        $user_info              = Users::find(1);

        return view('reports::purchase_report', compact('data', 'total_bill_amount', 'total_paid_amount', 'total_due_amount', 'total_adjustment_amount', 'total_return_due', 'total_return_paid' , 'total_vat_amount', 'total_tax_amount', 'from_date', 'to_date', 'user_info', 'customers'));
    }

    public function purchaseSummary()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $branch_id              = Auth::user()->branch_id;
        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $supplier_id            = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;

        $bills                  = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('bills.vendor_id', $supplier_id);
                                                })
                                                ->where('bills.type', 1)
                                                ->select('bills.bill_date as bill_date',
                                                    'bills.id as id',
                                                    'bills.bill_number as bill_number',
                                                    'bills.vendor_id as customer_id',
                                                    'customers.name as customer_name',
                                                    'bills.total_discount_type as total_discount_type',
                                                    'bills.total_discount_note as total_discount_note',
                                                    'bills.total_discount_amount as total_discount_amount',
                                                    'bills.bill_amount as bill_amount',
                                                    'bills.return_amount as return_amount',
                                                    'bills.due_amount as due_amount')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                        })
                                        ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('payments.customer_id', $supplier_id);
                                        })
                                        ->select('payment_entries.*')
                                        ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->leftjoin('purchase_return', 'purchase_return.id', 'payment_entries.purchase_return_id')
                                        ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('payments.customer_id', $supplier_id);
                                        })
                                        ->where('payments.type', 3)
                                        ->select('payment_entries.*',
                                                 'purchase_return.bill_id as return_bill_id')
                                        ->get();
                                                                        
        $bill_entries           = BillEntries::leftjoin('bills', 'bills.id', 'bill_entries.bill_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                        })
                                        ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('bill_entries.vendor_id', $supplier_id);
                                        })
                                        ->select('bill_entries.*')
                                        ->get();

        $total_bill_amount      = 0;
        $total_purchase_return  = 0;
        $total_payable          = 0;
        $total_paid_amount      = 0;
        $total_return_paid      = 0;
        $total_due_amount       = 0;
        $total_return_due       = 0;
        $total_discount_amount  = 0;

        if ($bills->count() > 0)
        {
            foreach ($bills as $key => $value)
            {   
                if ($value['total_discount_type'] == 0)
                {
                    $sub_total      = $bill_entries->where('bill_id', $value['id'])->sum('total_amount');
                    $discount       = ($sub_total*$value['total_discount_amount'])/100;
                }
                else
                {
                    $discount       = $value['total_discount_amount'];
                }

                $purchase_return_paid   = $payment_entries_return->where('return_bill_id', $value['id'])->sum('amount');

                $data[$value['id']]['bill_number']              = 'BILL - ' . str_pad($value['bill_number'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['bill_date']                = date('d-m-Y', strtotime($value['bill_date']));
                $data[$value['id']]['bill_id']                  = $value['id'];
                $data[$value['id']]['customer_name']            = $value['customer_name'];
                $data[$value['id']]['bill_amount']              = $value['bill_amount'];
                $data[$value['id']]['return_amount']            = $value['return_amount'];
                $data[$value['id']]['paid_amount']              = $payment_entries->where('bill_id', $value['id'])->sum('amount');
                $data[$value['id']]['return_paid']              = $purchase_return_paid;
                $data[$value['id']]['return_due']               = $value['return_amount'] - $purchase_return_paid;
                $data[$value['id']]['due_amount']               = $value['due_amount'];
                $data[$value['id']]['discount']                 = $value['total_discount'];
                $data[$value['id']]['total_discount']           = $discount;
                $data[$value['id']]['total_discount_note']      = $value['total_discount_note'];
                $data[$value['id']]['total_discount_type']      = $value['total_discount_type'];
                $data[$value['id']]['total_discount_amount']    = $value['total_discount_amount'];

                $total_bill_amount          = $total_bill_amount + $value['bill_amount'];
                $total_purchase_return      = $total_purchase_return + $value['return_amount'];
                $total_payable              = $total_payable + $value['bill_amount'];
                $total_paid_amount          = $total_paid_amount + $payment_entries->where('bill_id', $value['id'])->sum('amount');
                $total_return_paid          = $total_return_paid + $purchase_return_paid;
                $total_due_amount           = $total_due_amount + $value['due_amount'];
                $total_return_due           = $total_return_due + ($value['return_amount'] - $purchase_return_paid);
                $total_discount_amount      = $total_discount_amount + $discount;
            }
        }
        else
        {
            $data  =  [];   
        }

        $customers              = Customers::where('branch_id', $branch_id)->get();
        $user_info              = Users::find(1);

        return view('reports::purchase_summary', compact('data', 'total_bill_amount', 'total_payable', 'total_purchase_return', 'total_paid_amount', 'total_return_paid', 'total_due_amount', 'total_return_due', 'from_date', 'to_date', 'user_info', 'customers', 'total_discount_amount'));
    }

    public function dueReportSupplier()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $branches               = Branches::orderBy('created_at', 'ASC')->get();
        $user_info              = Users::find(1);
        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime('01-01-2020'));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $ledger_type            = isset($_GET['ledger_type']) ? $_GET['ledger_type'] : 0;
        $branch_id              = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;

        $customers              = Customers::where('contact_type', 1)
                                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('customers.id', $customer_id);
                                            })
                                            ->where('id', '!=', 2)
                                            ->where('customers.branch_id', $branch_id)
                                            ->orderBy('name', 'ASC')
                                            ->get();

        $total_balance          = 0;

        if ($customers->count() > 0)
        {
            foreach ($customers as $key => $value)
            {
                if ($ledger_type == 1)
                {
                    if ($value->balance != 0)
                    {
                        $data[$value['id']]['customer_id']      = $value['id'];
                        $data[$value['id']]['customer_name']    = $value['name'];
                        $data[$value['id']]['phone']            = $value['phone'];
                        $data[$value['id']]['address']          = $value['address'];
                        $data[$value['id']]['balance']          = $value->balance;

                        $total_balance           = $total_balance + $value->balance;
                    }
                }
                else
                {
                    $data[$value['id']]['customer_id']      = $value['id'];
                    $data[$value['id']]['customer_name']    = $value['name'];
                    $data[$value['id']]['phone']            = $value['phone'];
                    $data[$value['id']]['address']          = $value['address'];
                    $data[$value['id']]['balance']          = $value->balance;
                    
                    $total_balance           = $total_balance + $value->balance;
                }
            }
        }
        
        if(!empty($data))
        {
            $data = $data;
        }
        else
        {
            $data = [];
        }

        $customer_name    = Customers::find($customer_id);

        return view('reports::supplier_due_report', compact('data', 'total_balance', 'from_date', 'to_date', 'user_info', 'customers', 'customer_name', 'ledger_type', 'branches', 'branch_id'));
    }

    public function dueReportSupplierDetails($supplier_id)
    {
        $user_info          = Users::find(1);
        $customer_name      = Customers::find($supplier_id);
        $branch_id          = $customer_name['branch_id'];
        $date               = date('Y-m-d');
        $from_date          = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date            = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $opening_debit      = JournalEntries::where('date','<', $from_date)
                                        ->where('customer_id', $supplier_id)
                                        ->whereIn('transaction_head', ['production', 'purchase', 'supplier-opening-balance', 'payment-receive', 'supplier-settlement'])
                                        ->where('debit_credit', 1)
                                        ->where('branch_id', $branch_id)
                                        ->sum('amount');
        $opening_credit     = JournalEntries::where('date','<', $from_date)
                                        ->where('customer_id', $supplier_id)
                                        ->whereIn('transaction_head', ['payment-made', 'purchase-return'])
                                        ->where('debit_credit', 0)
                                        ->where('branch_id', $branch_id)
                                        ->sum('amount');
        $opening_balance    = $opening_debit - $opening_credit;

        $debit  = JournalEntries::whereBetween('journal_entries.date', [$from_date, $to_date])
                                ->where('customer_id', $supplier_id)
                                ->whereIn('transaction_head', ['production', 'purchase', 'supplier-opening-balance', 'payment-receive', 'supplier-settlement'])
                                ->where('debit_credit', 1)
                                ->where('branch_id', $branch_id)
                                ->get();
        $credit = JournalEntries::whereBetween('journal_entries.date', [$from_date, $to_date])
                                ->where('customer_id', $supplier_id)
                                ->whereIn('transaction_head', ['payment-made', 'purchase-return'])
                                ->where('debit_credit', 0)
                                ->where('branch_id', $branch_id)
                                ->get();
        $data   = $debit->merge($credit)->sortBy('id');

        return view('reports::supplier_due_report_details', compact('data', 'from_date', 'to_date', 'user_info', 'customer_name', 'opening_balance'));
    }

    public function dueReportSupplierDue()
    {
        $branches               = Branches::orderBy('created_at', 'ASC')->get();
        $user_info              = Users::find(1);
        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime('01-01-2020'));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $ledger_type            = 1;
        $branch_id              = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;

        $customers              = Customers::where('contact_type', 1)
                                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('customers.id', $customer_id);
                                            })
                                            ->where('id', '!=', 2)
                                            ->where('customers.branch_id', $branch_id)
                                            ->orderBy('name', 'ASC')
                                            ->get();

        $total_balance          = 0;

        if ($customers->count() > 0)
        {
            foreach ($customers as $key => $value)
            {
                if ($ledger_type == 1)
                {
                    if ($value->balance != 0)
                    {
                        $data[$value['id']]['customer_id']      = $value['id'];
                        $data[$value['id']]['customer_name']    = $value['name'];
                        $data[$value['id']]['phone']            = $value['phone'];
                        $data[$value['id']]['address']          = $value['address'];
                        $data[$value['id']]['balance']          = $value->balance;

                        $total_balance           = $total_balance + $value->balance;
                    }
                }
                else
                {
                    $data[$value['id']]['customer_id']      = $value['id'];
                    $data[$value['id']]['customer_name']    = $value['name'];
                    $data[$value['id']]['phone']            = $value['phone'];
                    $data[$value['id']]['address']          = $value['address'];
                    $data[$value['id']]['balance']          = $value->balance;
                    
                    $total_balance           = $total_balance + $value->balance;
                }
            }
        }
        
        if(!empty($data))
        {
            $data = $data;
        }
        else
        {
            $data = [];
        }

        $customer_name    = Customers::find($customer_id);

        return view('reports::supplier_due_report_due', compact('data', 'total_balance', 'from_date', 'to_date', 'user_info', 'customers', 'customer_name', 'ledger_type', 'branches', 'branch_id'));
    }

    public function expenseReport()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $account_id             = isset($_GET['account_id']) ? $_GET['account_id'] : 0;
        $branch_id              = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;

        $expenses               = Expenses::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                            })
                                            ->when($account_id != 0, function ($query) use ($account_id) {
                                                return $query->where('expenses.account_id', $account_id);
                                            })
                                            ->when($branch_id != 1, function ($query) use ($branch_id) {
                                                return $query->where('expenses.branch_id', $branch_id);
                                            })
                                            ->select('expenses.*')
                                            ->get();

        $accounts               = Accounts::where('parent_account_type_id',4)
                                            ->whereNotIn('id', [2,3,4,5,6,7,8,9,10,11])
                                            ->where('status', 1)
                                            ->get();

        $user_info  = Users::find(1);
        $branches   = Branches::orderBy('created_at', 'ASC')->get();

        return view('reports::expense_report', compact('expenses', 'from_date', 'to_date', 'user_info', 'accounts', 'account_id', 'branches', 'branch_id'));
    }

    public function incomeReport()
    {
        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $account_id             = isset($_GET['account_id']) ? $_GET['account_id'] : 0;
        $branch_id              = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;

        $incomes                = Incomes::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                return $query->whereBetween('incomes.income_date', [$from_date, $to_date]);
                                            })
                                            ->when($account_id != 0, function ($query) use ($account_id) {
                                                return $query->where('incomes.account_id', $account_id);
                                            })
                                            ->when($branch_id != 1, function ($query) use ($branch_id) {
                                                return $query->where('incomes.branch_id', $branch_id);
                                            })
                                            ->select('incomes.*')
                                            ->get();

        $accounts               = Accounts::where('parent_account_type_id',3)
                                            ->whereNotIn('id', [2,3,4,5,6,7,8,9,10,11])
                                            ->where('status', 1)
                                            ->get();

        $user_info  = Users::find(1);
        $branches   = Branches::orderBy('created_at', 'ASC')->get();

        return view('reports::income_report', compact('incomes', 'from_date', 'to_date', 'user_info', 'accounts', 'account_id', 'branches', 'branch_id'));
    }

    public function salaryReport()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $employee_id            = isset($_GET['employee_id']) ? $_GET['employee_id'] : 0;

        $expenses               = Expenses::leftjoin('customers', 'customers.id', 'expenses.user_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                                })
                                                ->when($employee_id != 0, function ($query) use ($employee_id) {
                                                    return $query->where('expenses.user_id', $employee_id);
                                                })
                                                ->select('expenses.*',
                                                    'customers.name as employee_name',
                                                    'customers.joining_date as joining_date',
                                                    'customers.designation as designation',
                                                    'customers.salary as salary')
                                                ->get();

        $employees              = Customers::where('customers.contact_type', 3)
                                                ->orderBy('created_at', 'ASC')
                                                ->get();

        $total_service_charge   = 0;
        $total_salary           = 0;
        $total_amount           = 0;

        if ($expenses->count() > 0)
        {
            foreach ($employees as $key => $value)
            {   
                $service_charge     = $expenses->where('user_id', $value['id'])
                                                ->where('salary_type', 1)
                                                ->sum('amount');

                $salary             = $expenses->where('user_id', $value['id'])
                                                ->where('salary_type', 0)
                                                ->sum('amount');

                $amount             = $service_charge + $salary;


                $data[$value['id']]['employee_name']    = $value['name'];
                $data[$value['id']]['joining_date']     = $value['joining_date'];
                $data[$value['id']]['designation']      = $value['designation'];

                $data[$value['id']]['service_charge']   = $service_charge;
                $data[$value['id']]['salary']           = $salary;
                $data[$value['id']]['amount']           = $amount;

                $total_service_charge                   = $total_service_charge + $service_charge;
                $total_salary                           = $total_salary + $salary;
                $total_amount                           = $total_amount + $amount;
            }
        }
        else
        {
            $data   =  [];   
        }

        $user_info  = Users::find(1);

        return view('reports::salary_report', compact('data', 'total_service_charge', 'total_salary', 'total_amount', 'from_date', 'to_date', 'user_info', 'employees'));
    }

    public function collectionReport()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $type_id                = isset($_GET['type_id']) ? $_GET['type_id'] : 0;

        $invoices               = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->select('invoices.id as id',
                                                    'invoices.invoice_date as invoice_date',
                                                    'invoices.invoice_amount as invoice_amount',
                                                    'invoices.return_amount as return_amount',
                                                    'invoices.total_buy_price as total_buy_price',
                                                    'invoices.due_amount as due_amount')
                                                ->get();


        $expenses               = Expenses::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                        })
                                        ->select('expenses.*')
                                        ->get();

        $incomes                = Incomes::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('incomes.income_date', [$from_date, $to_date]);
                                        })
                                        ->select('incomes.*')
                                        ->get();

        $return_entries         = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.sales_return_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('sales_return.sales_return_date', [$from_date, $to_date]);
                                        })
                                        ->select('sales_return_entries.*',
                                                 'sales_return.sales_return_date as sales_return_date'
                                                 )
                                        ->get();


        if ($type_id == 1 || $type_id == 0)
        {   
            $startDate          = new Carbon($from_date);
            $endDate            = new Carbon($to_date);
            $all_dates          = array();

            while ($startDate->lte($endDate))
            {
                $all_dates[] = $startDate->toDateString();
                $startDate->addDay();
            }

            if (!empty($all_dates))
            {  
                foreach ($all_dates as $key => $value)
                {   
                    $invoices_purchase_cost             = $invoices->where('invoice_date', $value);

                    $total_return_purchase_cost         = 0;
                    foreach ($invoices_purchase_cost as $key1 => $value1)
                    {
                        $find_return_purchase_cost        = $return_entries->where('invoice_id', $value1['id']);

                        if (!empty($find_return_purchase_cost))
                        {
                            $sub_total_return_purchase_cost      = 0;
                            foreach ($find_return_purchase_cost as $key2 => $value2)
                            {   
                                $sub_total_return_purchase_cost  = $sub_total_return_purchase_cost + ($value2['buy_price']*$value2['quantity']);
                            }

                            $total_return_purchase_cost          = $total_return_purchase_cost + $sub_total_return_purchase_cost;
                        }
                    }

                    $invoice_amount                     = $invoices->where('invoice_date', $value)->sum('invoice_amount');
                    $return_amount                      = $return_entries->where('sales_return_date', $value)->sum('total_amount');
                    $purchase_cost                      = $invoices->where('invoice_date', $value)->sum('total_buy_price');
                    $return_purchase_amount             = $return_entries->where('sales_return_date', $value)->sum('buy_price*quantity');
                    $expense                            = $expenses->where('expense_date', $value)->sum('amount');
                    $income                             = $incomes->where('income_date', $value)->sum('amount');

                    $data[$value]['invoice_amount']         = $invoice_amount;
                    $data[$value]['return_amount']          = $return_amount;
                    $data[$value]['purchase_cost']          = $purchase_cost;
                    $data[$value]['return_purchase_cost']   = $return_purchase_amount;
                    $data[$value]['expense']                = $expense;
                    $data[$value]['income']                 = $income;
                }
            }
            else
            {
                $data = [];
            }
        }
        else
        {
            $date                   = date('Y-m-d');
            $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
            $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

            $startDate              = new Carbon($from_date);
            $endDate                = new Carbon($to_date);
            $all_dates              = array();

            while ($startDate->lte($endDate))
            {
                $all_dates[] = $startDate->toDateString();
                $startDate->addDay();
            }

            if (!empty($all_dates))
            {   
                foreach ($all_dates as $key1 => $value1)
                {
                    $date_explode   = explode('-', $value1);
                    $date_format[]  = $date_explode[0].'-'.$date_explode[1];
                }

                $required_date      = array_unique($date_format);

                $total_invoice_amount                   = 0;
                $total_return_amount                    = 0;
                $total_due_amount                       = 0;
                $total_purchase_cost                    = 0;
                $total_expense                          = 0;
                $total_profit_loss                      = 0;

                foreach ($required_date as $key2 => $value2)
                {   
                    $invoice_amount                     = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                            })
                                                            ->where(DB::raw("(DATE_FORMAT(invoice_date,'%Y-%m'))"), $value2)
                                                            ->sum('invoices.invoice_amount');

                    $return_amount                      = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                            })
                                                            ->where(DB::raw("(DATE_FORMAT(invoice_date,'%Y-%m'))"), $value2)
                                                            ->sum('invoices.return_amount');

                    $due_amount                         = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                            })
                                                            ->where(DB::raw("(DATE_FORMAT(invoice_date,'%Y-%m'))"), $value2)
                                                            ->sum('invoices.due_amount');

                    $purchase_cost                      = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                            })
                                                            ->where(DB::raw("(DATE_FORMAT(invoice_date,'%Y-%m'))"), $value2)
                                                            ->sum('invoices.total_buy_price');

                    $sales_return                       = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.sales_return_id')
                                                            ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('sales_return.sales_return_date', [$from_date, $to_date]);
                                                            })
                                                            ->where(DB::raw("(DATE_FORMAT(sales_return_date,'%Y-%m'))"), $value2)
                                                            ->select('sales_return_entries.*')
                                                            ->get();

                    if (!empty($sales_return) && ($sales_return->count() > 0))
                    {   
                        $total_sales_return = 0;
                        foreach ($sales_return as $key => $value)
                        {
                            $total_sales_return = $total_sales_return + ($value['buy_price']*$value['quantity']);
                        }
                    }

                    $sales_return                       = $total_sales_return;
                    $expense                            = Expenses::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                                            })
                                                            ->where(DB::raw("(DATE_FORMAT(expense_date,'%Y-%m'))"), $value2)
                                                            ->sum('expenses.amount');

                    $return_entries                     = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.sales_return_id')
                                                            ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('sales_return.sales_return_date', [$from_date, $to_date]);
                                                            })
                                                            ->where(DB::raw("(DATE_FORMAT(sales_return_date,'%Y-%m'))"), $value2)
                                                            ->sum('sales_return.return_amount');

                    $invoices                           = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                                return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                            })
                                                            ->select('invoices.id as id',
                                                                'invoices.invoice_date as invoice_date',
                                                                'invoices.invoice_amount as invoice_amount',
                                                                'invoices.return_amount as return_amount',
                                                                'invoices.total_buy_price as total_buy_price',
                                                                'invoices.due_amount as due_amount')
                                                            ->get();


                    $invoice_amount_diff                = $invoice_amount - $return_amount;
                    $purchase_cost_diff                 = $purchase_cost - $sales_return;

                    $data[$value2]['invoice_amount']    = $invoice_amount_diff;
                    $data[$value2]['return_amount']     = $return_entries;
                    $data[$value2]['purchase_cost']     = $purchase_cost_diff;
                    $data[$value2]['due_amount']        = $due_amount;
                    $data[$value2]['paid_amount']       = $invoice_amount - $due_amount;
                    $data[$value2]['expense']           = $expense;
                    $data[$value2]['profit_loss']       = $invoice_amount_diff - $purchase_cost_diff - $expense;

                    $total_invoice_amount               = $total_invoice_amount + $invoice_amount_diff;
                    $total_return_amount                = $total_return_amount + $return_amount;
                    $total_purchase_cost                = $total_purchase_cost + $purchase_cost_diff;
                    $total_due_amount                   = $total_due_amount + $due_amount;
                    $total_expense                      = $total_expense + $expense;
                    $total_profit_loss                  = $total_profit_loss + ($invoice_amount_diff - $purchase_cost_diff - $expense);
                }
            }
            else
            {
                $datas = [];
            }  
        }                             

        $customers              = Customers::get();
        $user_info              = Users::find(1);

        return view('reports::collection_report', compact('type_id', 'data','from_date', 'to_date', 'user_info', 'customers'));
    }

    public function salesSummaryReduced()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;

        $invoices               = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('invoices.customer_id', $customer_id);
                                                })
                                                ->where('invoices.invoice_type', 1)
                                                ->select('invoices.invoice_date as invoice_date',
                                                    'invoices.id as id',
                                                    'invoices.invoice_number as invoice_number',
                                                    'invoices.customer_id as customer_id',
                                                    'invoices.adjustment_type as adjustment_type',
                                                    'invoices.adjustment_note as adjustment_note',
                                                    'customers.name as customer_name',
                                                    'invoices.invoice_amount as invoice_amount',
                                                    'invoices.return_amount as return_amount',
                                                    'invoices.due_amount as due_amount',
                                                    'invoices.total_adjustment as total_adjustment')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                        })
                                        ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('payments.customer_id', $customer_id);
                                        })
                                        ->select('payment_entries.*')
                                        ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->leftjoin('sales_return', 'sales_return.id', 'payment_entries.sales_return_id')
                                        ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('payments.customer_id', $customer_id);
                                        })
                                        ->where('payments.type', 2)
                                        ->select('payment_entries.*',
                                                 'sales_return.invoice_id as return_invoice_id')
                                        ->get();

        $invoice_entries        = InvoiceEntries::leftjoin('invoices', 'invoices.id', 'invoice_entries.invoice_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                        })
                                        ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('invoice_entries.customer_id', $customer_id);
                                        })
                                        ->where('invoices.invoice_type', 1)
                                        ->select('invoice_entries.*')
                                        ->get();

        $total_invoice_amount   = 0;
        $total_sales_return     = 0;
        $total_receivable       = 0;
        $total_paid_amount      = 0;
        $total_return_paid      = 0;
        $total_due_amount       = 0;
        $total_return_due       = 0;

        if ($invoices->count() > 0)
        {
            foreach ($invoices as $key => $value)
            {   
                if ($value['adjustment_type'] == 0)
                {
                    $sub_total   = $invoice_entries->where('invoice_id', $value['id'])->sum('total_amount');
                    $adjustment  = ($sub_total*$value['total_adjustment'])/100;
                }
                else
                {
                    $adjustment  = $value['total_adjustment'];
                }

                $sales_return_paid                      = $payment_entries_return->where('return_invoice_id', $value['id'])->sum('amount');

                $data[$value['id']]['invoice_number']   = 'INV - ' . str_pad($value['invoice_number'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['invoice_date']     = date('d-m-Y', strtotime($value['invoice_date']));
                $data[$value['id']]['invoice_id']       = $value['id'];
                $data[$value['id']]['customer_name']    = $value['customer_name'];
                $data[$value['id']]['invoice_amount']   = $value['invoice_amount'] - $adjustment;
                $data[$value['id']]['return_amount']    = $value['return_amount'];
                $data[$value['id']]['paid_amount']      = $payment_entries->where('invoice_id', $value['id'])->sum('amount');
                $data[$value['id']]['return_paid']      = $sales_return_paid;
                $data[$value['id']]['return_due']       = $value['return_amount'] - $sales_return_paid;
                $data[$value['id']]['due_amount']       = $value['due_amount'];
                $data[$value['id']]['adjustment']       = $adjustment;
                $data[$value['id']]['adjustment_note']  = $value['adjustment_note'];
                $data[$value['id']]['adjustment_type']  = $value['adjustment_type'];
                $data[$value['id']]['total_adjustment'] = $value['total_adjustment'];

                $total_invoice_amount                   = $total_invoice_amount + $value['invoice_amount'] - $adjustment;
                $total_sales_return                     = $total_sales_return + $value['return_amount'];
                $total_receivable                       = $total_receivable + ($value['invoice_amount'] -$value['return_amount']);
                $total_paid_amount                      = $total_paid_amount + $payment_entries->where('invoice_id', $value['id'])->sum('amount');
                $total_return_paid                      = $total_return_paid + $sales_return_paid;
                $total_due_amount                       = $total_due_amount + $value['due_amount'] - $adjustment;
                $total_return_due                       = $total_return_due + ($value['return_amount'] - $sales_return_paid);
            }

            $total_invoice_amount       = $total_invoice_amount*($user_info['sales_show']/100);
        }
        else
        {
            $data = [];
        }

        $customers                      = Customers::where('customers.contact_type', 0)
                                                ->orWhere('customers.contact_type', 1)
                                                ->orderBy('created_at', 'ASC')
                                                ->get();

        return view('reports::sales_summary_reduced', compact('data', 'total_invoice_amount', 'total_sales_return', 'total_receivable', 'total_paid_amount', 'total_return_paid', 'total_due_amount', 'total_return_due', 'from_date', 'to_date', 'user_info', 'customers'));
    }

    public function profitLossReduced()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $invoices               = Invoices::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->where('invoices.invoice_type', 1)
                                                ->select('invoices.id as id',
                                                         'invoices.invoice_amount as invoice_amount',
                                                         'invoices.return_amount as return_amount')
                                                ->get();

        $total_expense          = Expenses::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                                })
                                                ->select('expenses.*')
                                                ->sum('amount');

        $invoice_entries        = InvoiceEntries::leftjoin('invoices', 'invoices.id', 'invoice_entries.invoice_id')
                                        ->where('invoices.invoice_type', 1)
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                        })
                                        ->select('invoice_entries.*')
                                        ->get();

        $return_entries         = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.invoice_id')
                                        ->select('sales_return_entries.*')
                                        ->get();

        $total_purchase_cost    = Bills::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->select('bills.*')
                                                ->sum('bill_amount');

        $total_purchase_cost    = $total_purchase_cost*($user_info['sales_show']/100);

        $total_invoice_amount       = 0;

        if ($invoices->count() > 0)
        {
            foreach ($invoices as $key => $value)
            {
                $total_invoice_amount             = $total_invoice_amount + $value['invoice_amount'] - $value['return_amount'];
            }

            $total_invoice_amount       = $total_invoice_amount*($user_info['sales_show']/100);
        }
        else
        {     
            $total_invoice_amount       = 0;     
        }

        return view('reports::profit_loss_reduced', compact('from_date', 'to_date', 'user_info', 'total_invoice_amount', 'total_purchase_cost', 'total_expense'));
    }
    
    public function purchaseReportReduced()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $supplier_id            = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;

        $bills                  = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('bills.vendor_id', $supplier_id);
                                                })
                                                ->select('bills.bill_date as bill_date',
                                                    'bills.id as id',
                                                    'bills.bill_number as bill_number',
                                                    'bills.vendor_id as customer_id',
                                                    'customers.name as customer_name',
                                                    'bills.adjustment_type as adjustment_type',
                                                    'bills.total_adjustment as total_adjustment',
                                                    'bills.adjustment_note as adjustment_note',
                                                    'bills.bill_amount as bill_amount',
                                                    'bills.return_amount as return_amount',
                                                    'bills.due_amount as due_amount',
                                                    'bills.total_adjustment as total_adjustment')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                        })
                                        ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('payments.customer_id', $supplier_id);
                                        })
                                        ->select('payment_entries.*')
                                        ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->leftjoin('purchase_return', 'purchase_return.id', 'payment_entries.purchase_return_id')
                                        ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('payments.customer_id', $supplier_id);
                                        })
                                        ->where('payments.type', 3)
                                        ->select('payment_entries.*',
                                                 'purchase_return.bill_id as return_bill_id')
                                        ->get();
                                                                        
        $bill_entries           = BillEntries::leftjoin('bills', 'bills.id', 'bill_entries.bill_id')
                                        ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                        })
                                        ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                            return $query->where('bill_entries.vendor_id', $supplier_id);
                                        })
                                        ->select('bill_entries.*')
                                        ->get();

        $total_bill_amount      = 0;
        $total_purchase_return  = 0;
        $total_payable          = 0;
        $total_paid_amount      = 0;
        $total_return_paid      = 0;
        $total_due_amount       = 0;
        $total_return_due       = 0;

        if ($bills->count() > 0)
        {
            foreach ($bills as $key => $value)
            {   
                if ($value['adjustment_type'] == 0)
                {
                    $sub_total   = $bill_entries->where('bill_id', $value['id'])->sum('total_amount');
                    $adjustment  = ($sub_total*$value['total_adjustment'])/100;
                }
                else
                {
                    $adjustment  = $value['total_adjustment'];
                }

                $purchase_return_paid                   = $payment_entries_return->where('return_bill_id', $value['id'])->sum('amount');

                $data[$value['id']]['bill_number']      = 'BILL - ' . str_pad($value['bill_number'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['bill_date']        = date('d-m-Y', strtotime($value['bill_date']));
                $data[$value['id']]['bill_id']          = $value['id'];
                $data[$value['id']]['customer_name']    = $value['customer_name'];
                $data[$value['id']]['bill_amount']      = $value['bill_amount'] - $adjustment;
                $data[$value['id']]['return_amount']    = $value['return_amount'];
                $data[$value['id']]['paid_amount']      = $payment_entries->where('bill_id', $value['id'])->sum('amount');
                $data[$value['id']]['return_paid']      = $purchase_return_paid;
                $data[$value['id']]['return_due']       = $value['return_amount'] - $purchase_return_paid;
                $data[$value['id']]['due_amount']       = $value['due_amount'];
                $data[$value['id']]['adjustment']       = $adjustment;
                $data[$value['id']]['adjustment_note']  = $value['adjustment_note'];
                $data[$value['id']]['adjustment_type']  = $value['adjustment_type'];
                $data[$value['id']]['total_adjustment'] = $value['total_adjustment'];

                $total_bill_amount                      = $total_bill_amount + $value['bill_amount'] - $adjustment;
                $total_purchase_return                  = $total_purchase_return + $value['return_amount'];
                $total_payable                          = $total_payable + ($value['bill_amount'] -$value['return_amount']);
                $total_paid_amount                      = $total_paid_amount + $payment_entries->where('bill_id', $value['id'])->sum('amount');
                $total_return_paid                      = $total_return_paid + $purchase_return_paid;
                $total_due_amount                       = $total_due_amount + $value['due_amount'];
                $total_return_due                       = $total_return_due + ($value['return_amount'] - $purchase_return_paid);
            }

            $total_bill_amount   = $total_bill_amount*($user_info['sales_show']/100);
        }
        else
        {
            $data  =  [];   
        }

        $customers              = $tables['customers']->get();

        return view('reports::purchase_summary_reduced', compact('data', 'total_bill_amount', 'total_payable', 'total_purchase_return', 'total_paid_amount', 'total_return_paid', 'total_due_amount', 'total_return_due', 'from_date', 'to_date', 'user_info', 'customers'));
    }

    public function invoiceDueReport()
    {
        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $branch_id              = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;
        $branches               = Branches::orderBy('created_at', 'ASC')->get();

        $invoices               = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.due_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('invoices.customer_id', $customer_id);
                                                })
                                                ->when($branch_id != 0, function ($query) use ($branch_id) {
                                                    return $query->where('invoices.branch_id', $branch_id);
                                                })
                                                ->where('invoices.type', 1)
                                                ->select('invoices.invoice_date as invoice_date',
                                                    'invoices.due_date as due_date',
                                                    'invoices.id as id',
                                                    'invoices.invoice_number as invoice_number',
                                                    'invoices.total_discount_type as total_discount_type',
                                                    'invoices.total_discount_note as total_discount_note',
                                                    'invoices.total_discount_amount as total_discount_amount',
                                                    'invoices.total_discount as total_discount',
                                                    'invoices.invoice_amount as invoice_amount',
                                                    'invoices.return_amount as return_amount',
                                                    'invoices.due_amount as due_amount',
                                                    'invoices.customer_id as customer_id',
                                                    'customers.name as customer_name')
                                                ->get();

        $customers              = Customers::where('branch_id', $branch_id)->get();
        $user_info              = Users::find(1);

        return view('reports::invoice_due_report', compact('invoices', 'from_date', 'to_date', 'user_info', 'customers', 'branches', 'branch_id'));
    }

    //New Reports Developed At Cyberdyne Technology Ltd.
    public function salesStatement()
    {
        $branches  = Branches::orderBy('created_at', 'ASC')->get();
        return view('reports::sales_statement', compact('branches'));
    }

    public function salesStatementPrint()
    {
        $user_info      = Users::find(1);
        $date           = date('Y-m-d');
        $from_date      = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date        = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $invoice_number = isset($_GET['invoice_number']) ? $_GET['invoice_number'] : 0;
        $category_id    = isset($_GET['category_id']) ? $_GET['category_id'] : 0;
        $product_code   = isset($_GET['product_code']) ? $_GET['product_code'] : 0;
        $product_id     = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $reference_id   = isset($_GET['reference_id']) ? $_GET['reference_id'] : 0;
        $user_id        = isset($_GET['user_id']) ? $_GET['user_id'] : 0;
        $sales_return   = isset($_GET['sales_return']) ? $_GET['sales_return'] : 0;
        $branch_id      = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;
        
        $data1  = Invoices::when($customer_id != 0, function ($query) use ($customer_id) {
                                return $query->where('invoices.customer_id', $customer_id);
                            })
                            ->when($invoice_number != 0, function ($query) use ($invoice_number) {
                                return $query->where('invoices.id', $invoice_number);
                            })
                            ->when($reference_id != 0, function ($query) use ($reference_id) {
                                return $query->where('invoices.reference_id', $reference_id);
                            })
                            ->when($user_id != 0, function ($query) use ($user_id) {
                                return $query->where('invoices.created_by', $user_id);
                            })
                            ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                            })
                            ->when($branch_id != 1, function ($query) use ($branch_id) {
                                return $query->where('invoices.branch_id', $branch_id);
                            })
                            ->where('invoices.type', 1)
                            ->select('invoices.*')
                            ->with(['invoiceEntries' => function($query) use($category_id, $product_code, $product_id) {
                                    $query->when($category_id != 0, function ($query) use ($category_id) {
                                    return $query->where('invoice_entries.product_id', $category_id);
                                    });

                                    $query->when($product_code != 0, function ($query) use ($product_code) {
                                    return $query->where('invoice_entries.product_entry_id', $product_code);
                                    });

                                    $query->when($product_id != 0, function ($query) use ($product_id) {
                                    return $query->where('invoice_entries.product_entry_id', $product_id);
                                    });
                            }])
                            ->orderBy('invoices.created_at', 'ASC')
                            ->get();

        $data2  = SalesReturn::when($customer_id != 0, function ($query) use ($customer_id) {
                                return $query->where('sales_return.customer_id', $customer_id);
                            })
                            ->when($user_id != 0, function ($query) use ($user_id) {
                                return $query->where('sales_return.created_by', $user_id);
                            })
                            ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                return $query->whereBetween('sales_return.sales_return_date', [$from_date, $to_date]);
                            })
                            ->when($branch_id != 1, function ($query) use ($branch_id) {
                                return $query->where('sales_return.branch_id', $branch_id);
                            })
                            ->select('sales_return.*')
                            ->with(['salesReturnEntries' => function($query) use($category_id, $product_code, $product_id) {
                                    $query->when($category_id != 0, function ($query) use ($category_id) {
                                    return $query->where('sales_return_entries.product_id', $category_id);
                                    });

                                    $query->when($product_code != 0, function ($query) use ($product_code) {
                                    return $query->where('sales_return_entries.product_entry_id', $product_code);
                                    });

                                    $query->when($product_id != 0, function ($query) use ($product_id) {
                                    return $query->where('sales_return_entries.product_entry_id', $product_id);
                                    });
                            }])
                            ->orderBy('sales_return.created_at', 'ASC')
                            ->get();

        $data               = $data1->merge($data2);
        $item_category_name = Products::find($category_id);
        $sales_by_name      = Users::find($user_id);
        $sales_id           = Invoices::find($invoice_number);
        $customer_name      = Customers::find($customer_id);
        $reference          = Customers::find($reference_id);

        return view('reports::sales_statement_print', compact('user_info', 'from_date', 'to_date', 'data', 'item_category_name', 'sales_return', 'sales_by_name', 'sales_id', 'customer_name', 'reference', 'data'));
    }

    public function orderStatement()
    {
        $branches  = Branches::orderBy('created_at', 'ASC')->get();
        return view('reports::order_statement', compact('branches'));
    }

    public function orderStatementPrint()
    {
        $user_info      = Users::find(1);
        $date           = date('Y-m-d');
        $from_date      = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date        = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $invoice_number = isset($_GET['invoice_number']) ? $_GET['invoice_number'] : 0;
        $category_id    = isset($_GET['category_id']) ? $_GET['category_id'] : 0;
        $product_code   = isset($_GET['product_code']) ? $_GET['product_code'] : 0;
        $product_id     = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $reference_id   = isset($_GET['reference_id']) ? $_GET['reference_id'] : 0;
        $user_id        = isset($_GET['user_id']) ? $_GET['user_id'] : 0;
        $sales_return   = isset($_GET['sales_return']) ? $_GET['sales_return'] : 0;
        $branch_id      = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;
        
        $data   = Invoices::when($customer_id != 0, function ($query) use ($customer_id) {
                                return $query->where('invoices.customer_id', $customer_id);
                            })
                            ->when($invoice_number != 0, function ($query) use ($invoice_number) {
                                return $query->where('invoices.id', $invoice_number);
                            })
                            ->when($reference_id != 0, function ($query) use ($reference_id) {
                                return $query->where('invoices.reference_id', $reference_id);
                            })
                            ->when($user_id != 0, function ($query) use ($user_id) {
                                return $query->where('invoices.created_by', $user_id);
                            })
                            ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                            })
                            ->when($branch_id != 1, function ($query) use ($branch_id) {
                                return $query->where('invoices.branch_id', $branch_id);
                            })
                            ->where('invoices.type', 3)
                            ->select('invoices.*')
                            ->with(['invoiceEntries' => function($query) use($category_id, $product_code, $product_id) {
                                    $query->when($category_id != 0, function ($query) use ($category_id) {
                                    return $query->where('invoice_entries.product_id', $category_id);
                                    });

                                    $query->when($product_code != 0, function ($query) use ($product_code) {
                                    return $query->where('invoice_entries.product_entry_id', $product_code);
                                    });

                                    $query->when($product_id != 0, function ($query) use ($product_id) {
                                    return $query->where('invoice_entries.product_entry_id', $product_id);
                                    });
                            }])
                            ->orderBy('invoices.created_at', 'ASC')
                            ->get();

        $item_category_name = Products::find($category_id);
        $sales_by_name      = Users::find($user_id);
        $sales_id           = Invoices::find($invoice_number);
        $customer_name      = Customers::find($customer_id);
        $reference          = Customers::find($reference_id);

        return view('reports::order_statement_print', compact('user_info', 'from_date', 'to_date', 'data', 'item_category_name', 'sales_return', 'sales_by_name', 'sales_id', 'customer_name', 'reference', 'data'));
    }

    public function purchaseStatement()
    {
        return view('reports::purchase_statement');
    }

    public function purchaseStatementPrint()
    {
        $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $supplier_id            = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;
        $bill_number            = isset($_GET['bill_number']) ? $_GET['bill_number'] : 0;
        $category_id            = isset($_GET['category_id']) ? $_GET['category_id'] : 0;
        $product_code           = isset($_GET['product_code']) ? $_GET['product_code'] : 0;
        $product_id             = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $user_id                = isset($_GET['user_id']) ? $_GET['user_id'] : 0;
        $purchase_return        = isset($_GET['purchase_return']) ? $_GET['purchase_return'] : 0;

        $bills                  = Bills::leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                                ->leftjoin('customers', 'customers.id', 'bill_entries.vendor_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('bills.vendor_id', $supplier_id);
                                                })
                                                ->when($bill_number != 0, function ($query) use ($bill_number) {
                                                    return $query->where('bills.id', $bill_number);
                                                })
                                                ->when($category_id != 0, function ($query) use ($category_id) {
                                                    return $query->where('product_entries.product_id', $category_id);
                                                })
                                                ->when($product_code != 0, function ($query) use ($product_code) {
                                                    return $query->where('product_entries.id', $product_code);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('product_entries.id', $product_id);
                                                })
                                                ->when($user_id != 0, function ($query) use ($user_id) {
                                                    return $query->where('bills.created_by', $user_id);
                                                })
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->where('bills.type', 1)
                                                ->select('bills.bill_date as bill_date',
                                                    'bills.id as id',
                                                    'bills.bill_number as bill_number',
                                                    'bills.vendor_id as vendor_id',
                                                    'customers.name as customer_name',
                                                    'bills.bill_amount as bill_amount',
                                                    'bills.return_amount as return_amount',
                                                    'bills.due_amount as due_amount',
                                                    'bills.bill_note as bill_note',
                                                    'bills.total_discount_type as total_discount_type',
                                                    'bills.total_discount_amount as total_discount_amount',
                                                    'bills.vat_type as vat_type',
                                                    'bills.total_vat as total_vat',
                                                    'bills.tax_type as tax_type',
                                                    'bills.total_tax as total_tax')
                                                ->get();

        $bill_entries           = BillEntries::leftjoin('bills', 'bills.id', 'bill_entries.bill_id')
                                                ->leftjoin('products', 'products.id', 'bill_entries.product_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                                ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->select('bill_entries.*',
                                                         'product_entries.name as product_entry_name',
                                                         'products.name as product_name',
                                                         'product_entries.product_code as product_code',
                                                         'units.name as unit_name')
                                                ->get();

        $purchase_returns       = PurchaseReturn::leftjoin('purchase_return_entries', 'purchase_return_entries.purchase_return_id', 'purchase_return.id')
                                                ->leftjoin('customers', 'customers.id', 'purchase_return_entries.customer_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'purchase_return_entries.product_entry_id')
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('purchase_return.customer_id', $supplier_id);
                                                })
                                                ->when($category_id != 0, function ($query) use ($category_id) {
                                                    return $query->where('product_entries.product_id', $category_id);
                                                })
                                                ->when($product_code != 0, function ($query) use ($product_code) {
                                                    return $query->where('product_entries.id', $product_code);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('product_entries.id', $product_id);
                                                })
                                                ->when($user_id != 0, function ($query) use ($user_id) {
                                                    return $query->where('purchase_return.created_by', $user_id);
                                                })
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('purchase_return.purchase_return_date', [$from_date, $to_date]);
                                                })
                                                ->select('purchase_return.purchase_return_date as purchase_return_date',
                                                    'purchase_return.id as id',
                                                    'purchase_return.purchase_return_number as purchase_return_number',
                                                    'purchase_return.bill_id as bill_id',
                                                    'purchase_return.customer_id as customer_id',
                                                    'customers.name as customer_name',
                                                    'purchase_return.sub_total_amount as sub_total_amount',
                                                    'purchase_return.return_amount as return_amount',
                                                    'purchase_return.due_amount as due_amount',
                                                    'purchase_return.return_note as return_note',
                                                    'purchase_return.total_discount_type as total_discount_type',
                                                    'purchase_return.total_discount_amount as total_discount_amount',
                                                    'purchase_return.vat_type as vat_type',
                                                    'purchase_return.total_vat as total_vat')
                                                ->get();

        $purchase_return_entries= PurchaseReturnEntries::leftjoin('purchase_return', 'purchase_return.id', 'purchase_return_entries.purchase_return_id')
                                                ->leftjoin('products', 'products.id', 'purchase_return_entries.product_id')
                                                ->leftjoin('product_entries', 'product_entries.id', 'purchase_return_entries.product_entry_id')
                                                ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('purchase_return.purchase_return_date', [$from_date, $to_date]);
                                                })
                                                ->select('purchase_return_entries.*',
                                                         'product_entries.name as product_entry_name',
                                                         'products.name as product_name',
                                                         'product_entries.product_code as product_code',
                                                         'units.name as unit_name')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                                })
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('payments.customer_id', $supplier_id);
                                                })
                                                ->select('payment_entries.*')
                                                ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->leftjoin('purchase_return', 'purchase_return.id', 'payment_entries.purchase_return_id')
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('payments.customer_id', $supplier_id);
                                                })
                                                ->where('payments.type', 2)
                                                ->select('payment_entries.*',
                                                         'purchase_return.bill_id as return_bill_id')
                                                ->get();

        if ($bills->count() > 0)
        {
            if ((isset($_GET['purchase_return']) && $_GET['purchase_return'] == 1) || (isset($_GET['purchase_return']) && $_GET['purchase_return'] == 0))
            {
                foreach ($bills as $key => $value)
                {
                    $sub_total_value                             = $bill_entries->where('bill_id', $value['id'])->sum('total_amount');
                    $perc_vat                                    = ($sub_total_value * $value['total_vat'])/100;
                    $perc_total_discount                         = (($sub_total_value + $perc_vat)*$value['total_discount_amount'])/100;

                    $data1[$value['id']]['bill_number']          = 'BILL - ' . str_pad($value['bill_number'], 6, "0", STR_PAD_LEFT);
                    $data1[$value['id']]['bill_date']            = date('d-m-Y', strtotime($value['bill_date']));
                    $data1[$value['id']]['customer_name']        = $value['customer_name'];
                    $data1[$value['id']]['paid_amount']          = $payment_entries->where('bill_id', $value['id'])->sum('amount');
                    $data1[$value['id']]['vat_type']             = $value['vat_type'];
                    $data1[$value['id']]['vat']                  = $value['total_vat'];
                    $data1[$value['id']]['vat_perc']             = $perc_vat;
                    $data1[$value['id']]['total_discount_type']  = $value['total_discount_type'];
                    $data1[$value['id']]['total_discount']       = $value['total_discount_amount'];
                    $data1[$value['id']]['total_discount_perc']  = $perc_total_discount;
                    $data1[$value['id']]['sub_total']            = $sub_total_value;
                    $data1[$value['id']]['type']                 = 'P';

                    if ($value['vat_type'] == 0)
                    {
                        $net_payable_vat                        = $perc_vat;
                    }
                    else
                    {
                        $net_payable_vat                        = $value['total_vat'];
                    }

                    if ($value['total_discount_type'] == 0)
                    {
                        $net_payable_total_discount             = $perc_total_discount;
                    }
                    else
                    {
                        $net_payable_total_discount             = $value['total_discount_amount'];
                    }

                    $data1[$value['id']]['net_payable']          = $sub_total_value + $net_payable_vat - $net_payable_total_discount;
                    $data1[$value['id']]['total_buy_price']      = $value['total_buy_price'];
                    $data1[$value['id']]['profit_loss']          = ($sub_total_value + $net_payable_vat - $net_payable_total_discount) - $value['total_buy_price'];
                    $data1[$value['id']]['row_span']             = $bill_entries->where('bill_id', $value['id'])->count();
                    $data1[$value['id']]['bill_entries']      = array_values($bill_entries->where('bill_id', $value['id'])->toArray());
                }
            }
            else
            {
                $data1      = [];
            }
        }
        else
        {
            $data1                               = [];
        }

        if ($purchase_returns->count() > 0)
        {
            if ((isset($_GET['purchase_return']) && $_GET['purchase_return'] == 2) || (isset($_GET['purchase_return']) && $_GET['purchase_return'] == 0))
            {
                foreach ($purchase_returns as $key => $value)
                {   
                    $sub_total_value                             = $purchase_return_entries->where('purchase_return_id', $value['id'])->sum('total_amount');
                    $perc_vat                                    = ($sub_total_value * $value['total_vat'])/100;
                    $perc_total_discount                         = (($sub_total_value + $perc_vat)*$value['total_discount_amount'])/100;

                    $data2[$value['id']]['bill_number']          = 'PR - ' . str_pad($value['purchase_return_number'], 6, "0", STR_PAD_LEFT);
                    $data2[$value['id']]['bill_date']            = date('d-m-Y', strtotime($value['purchase_return_date']));
                    $data2[$value['id']]['customer_name']        = $value['customer_name'];
                    $data2[$value['id']]['paid_amount']          = $payment_entries->where('purchase_return_id', $value['id'])->sum('amount');
                    $data2[$value['id']]['vat_type']             = $value['vat_type'];
                    $data2[$value['id']]['vat']                  = ($value['total_vat']*$sub_total_value)/$value['sub_total_amount'];
                    $data2[$value['id']]['vat_perc']             = $perc_vat;
                    $data2[$value['id']]['total_discount_type']  = $value['total_discount_type'];
                    $data2[$value['id']]['total_discount']       = ($value['sub_total_amount'] + (($value['total_vat']*$sub_total_value)/$value['sub_total_amount']))*(($value['total_discount_amount']*$sub_total_value)/$value['sub_total_amount'])/($value['sub_total_amount'] + $value['total_vat']);
                    $data2[$value['id']]['total_discount_perc']  = $perc_total_discount;
                    $data2[$value['id']]['sub_total']            = $sub_total_value;
                    $data2[$value['id']]['type']                 = 'R';

                    if ($value['vat_type'] == 0)
                    {
                        $net_payable_vat                        = $perc_vat;
                    }
                    else
                    {
                        $net_payable_vat                        = ($value['total_vat']*$sub_total_value)/$value['sub_total_amount'];
                    }

                    if ($value['total_discount_type'] == 0)
                    {
                        $net_payable_total_discount             = $perc_total_discount;
                    }
                    else
                    {
                        $net_payable_total_discount             = ($value['sub_total_amount'] + (($value['total_vat']*$sub_total_value)/$value['sub_total_amount']))*(($value['total_discount_amount']*$sub_total_value)/$value['sub_total_amount'])/($value['sub_total_amount'] + $value['total_vat']);
                    }

                    if ($data2[$value['id']]['type'] == 'S')
                    {
                        $data2[$value['id']]['net_payable']          = $sub_total_value + $net_payable_vat - $net_payable_total_discount;
                    }
                    else
                    {
                        $data2[$value['id']]['net_payable']          = $sub_total_value + $net_payable_vat + $net_payable_total_discount;
                    }
                    
                    $data2[$value['id']]['total_buy_price']      = 0;
                    $data2[$value['id']]['profit_loss']          = 0;
                    $data2[$value['id']]['row_span']             = $purchase_return_entries->where('purchase_return_id', $value['id'])->count();
                    $data2[$value['id']]['bill_entries']      = array_values($purchase_return_entries->where('purchase_return_id', $value['id'])->toArray());
                }
            }
            else
            {
                $data2      = [];
            }
            
        }
        else
        {
            $data2                               = [];
        }

        $data  = array_merge($data1, $data2);

        $item_category_name     = Products::find($category_id);
        $purchase_by_name       = Users::find($user_id);
        $purchase_id            = Bills::find($bill_number);
        $supplier_name          = Customers::find($supplier_id);

        return view('reports::purchase_statement_print', compact('user_info', 'from_date', 'to_date', 'data', 'item_category_name', 'purchase_return', 'purchase_by_name', 'purchase_id', 'supplier_name', 'data'));
    }

    public function itemList()
    {
        return view('reports::item_list');
    }

    public function itemListPrint()
    {
        $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $category_id            = isset($_GET['category_id']) ? $_GET['category_id'] : 0;
        $product_id             = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $brand_id               = isset($_GET['brand_id']) ? $_GET['brand_id'] : 0;
        $variation_id           = isset($_GET['variation_id']) ? $_GET['variation_id'] : 0;

        $product            = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->when($category_id != 0, function ($query) use ($category_id) {
                                                return $query->where('product_entries.product_id', $category_id);
                                            })
                                            ->when($product_id != 0, function ($query) use ($product_id) {
                                                return $query->where('product_entries.id', $product_id);
                                            })
                                            ->when($brand_id != 0, function ($query) use ($brand_id) {
                                                return $query->where('product_entries.brand_id', $brand_id);
                                            })
                                            ->when($variation_id != 0, function ($query) use ($variation_id) {
                                                return $query->whereIn('product_variation_values.id', $variation_id);
                                            })
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                         GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT products.id) as product_id
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->get();
        
        $products               = $product->sortBy('name')->all();
        $products               = collect($product)->groupBy('category_name');

        $item_category_name     = Products::find($category_id);
        $product_name           = ProductEntries::find($product_id);
        $brand_name             = Categories::find($brand_id);

        if ($variation_id != 0)
        {
            $variations             = ProductVariationValues::whereIn('id', $variation_id)->selectRaw('name')->get();
            $variation_name         = $variations->implode('name', ',');
        }
        else
        {
            $variation_name         = '';
        }
        
        return view('reports::item_list_print', compact('user_info', 'item_category_name', 'product_name', 'brand_name', 'products', 'variation_name'));
    }

    public function registerList()
    {
        $user_info              = Users::find(1);
        $type                   = isset($_GET['type']) ? $_GET['type'] : 0;
        $branch_id              = Auth()->user()->branch_id;

        $customers              = Customers::Where('customers.contact_type', $type)
                                            ->when($branch_id != 1, function ($query) use ($branch_id) {
                                                    return $query->where('customers.branch_id', $branch_id);
                                                })
                                            ->get();
        
        return view('reports::register_list_print', compact('user_info', 'customers'));
    }

    public function incomeStatement()
    {
        $branches  = Branches::orderBy('created_at', 'ASC')->get();
        return view('reports::income_statement', compact('branches'));
    }

    public function incomeStatementPrint()
    {
        $user_info  = Users::find(1);
        $branch_id  = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;
        $date       = date('Y-m-d');
        $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $opening_data           = JournalEntries::where('journal_entries.date', '<', $from_date)
                                            ->where('journal_entries.account_id', '!=', 1)
                                            ->where('branch_id', $branch_id)
                                            ->select('journal_entries.debit_credit', 'journal_entries.amount', 'journal_entries.account_id')
                                            ->get();

        $data                   = JournalEntries::whereBetween('journal_entries.date',[$from_date,$to_date])
                                            ->where('journal_entries.account_id', '!=', 1)
                                            ->where('branch_id', $branch_id)
                                            ->select('journal_entries.*')
                                            ->orderBy('id', 'ASC')
                                            ->get();

        $paid_through_accounts  = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();;

        foreach ($paid_through_accounts as $key => $value)
        {   
            $opening_debit      = $opening_data->where('debit_credit', 0)->where('account_id', $value->id)->sum('amount');
            $opening_credit     = $opening_data->where('debit_credit', 1)->where('account_id', $value->id)->sum('amount');

            $opening_balance    = $opening_credit - $opening_debit;

            $d1 = $data->where('account_id', $value->id);

            $result[$value->id]['account_name']     = $value->account_name;
            $result[$value->id]['opening_balance']  = $opening_balance;
            $result[$value->id]['data']             = $d1;
        }

        return view('reports::income_statement_print', compact('user_info', 'from_date', 'to_date', 'result'));
    }

    public function currentBalance()
    {
        return view('reports::current_balance');
    }

    public function currentBalancePrint()
    {
        $user_info  = Users::find(1);
        $branch_id  = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;
        $date       = date('Y-m-d');
        $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $result                 = JournalEntries::whereBetween('journal_entries.date',[$from_date,$to_date])
                                            ->where('branch_id', $branch_id)
                                            ->select('journal_entries.*')
                                            ->orderBy('id', 'ASC')
                                            ->get();

        $paid_through_accounts  = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();;

        foreach ($paid_through_accounts as $key => $value)
        {   
            $debit      = $result->where('debit_credit', 0)->where('account_id', $value->id)->sum('amount');
            $credit     = $result->where('debit_credit', 1)->where('account_id', $value->id)->sum('amount');

            $balance    = $credit - $debit;

            $data[$value->id]['account_name']     = $value->account_name;
            $data[$value->id]['balance']          = $balance;
        }

        return view('reports::current_balance_print', compact('user_info', 'from_date', 'to_date', 'data'));
    }

    public function currentBalanceDetails($from_date, $to_date, $paid_through_id)
    {
        $user_info = Users::find(1);
        $date      = date('Y-m-d');
        $from_date = date('Y-m-d', strtotime($from_date));
        $to_date   = date('Y-m-d', strtotime($to_date));

        //type   0 = in, 1 = out 2 = out 3 = in
        $payments   = Payments::whereBetween('payments.payment_date', [$from_date, $to_date])
                                        ->selectRaw('payments.payment_date as date,
                                                     payments.paid_through as paid_through_id,
                                                     payments.amount as amount,
                                                     payments.amount as amount,
                                                     payments.type as type
                                                    ')
                                        ->get()->toArray();

        $incomes    = Incomes::whereBetween('incomes.income_date', [$from_date, $to_date])
                                ->selectRaw('incomes.income_date as date,
                                             incomes.paid_through_id as paid_through_id,
                                             incomes.amount as amount,
                                             "0" as type
                                            ')
                                ->get()->toArray();

        $expenses   = Expenses::whereBetween('expenses.expense_date', [$from_date, $to_date])
                                ->where('expense_category_id', '!=', 2)
                                ->selectRaw('expenses.expense_date as date,
                                             expenses.paid_through_id as paid_through_id,
                                             expenses.amount as amount,
                                             "1" as type
                                            ')
                                ->get()->toArray();

        $datas      = array_merge($payments, $incomes, $expenses);

        $paid_through_name      = PaidThroughAccounts::find($paid_through_id);
        $account_wise_payments  = collect($datas)->where('paid_through_id', $paid_through_id)->groupBy('date');
        $data['date_wise_data'] = $account_wise_payments;

        return view('reports::current_balance_details', compact('user_info', 'from_date', 'to_date', 'paid_through_name', 'data'));
    }

    public function productSuppliers()
    {
        return view('reports::product_suppliers');
    }

    public function productSuppliersPrint()
    {
        $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $supplier_id            = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;
        $product_id             = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $report_type            = isset($_GET['report_type']) ? $_GET['report_type'] : 0;

        if ($report_type == 1)
        {
            $product                = ProductSuppliers::leftjoin('product_entries', 'product_entries.id', 'product_suppliers.product_entry_id')
                                            ->leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_suppliers.supplier_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                return $query->where('product_suppliers.supplier_id', $supplier_id);
                                            })
                                            ->when($product_id != 0, function ($query) use ($product_id) {
                                                return $query->where('product_entries.id', $product_id);
                                            })
                                            ->groupBy('product_suppliers.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_suppliers.supplier_id) as supplier_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_suppliers.whole_sale_price) as whole_sale_price,
                                                         GROUP_CONCAT(DISTINCT product_suppliers.retail_price) as retail_price
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->get();
        
            $products               = $product->groupBy('supplier_name');
        }

        if ($report_type == 2)
        {
            $product                = ProductSuppliers::leftjoin('product_entries', 'product_entries.id', 'product_suppliers.product_entry_id')
                                            ->leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_suppliers.supplier_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                return $query->where('product_suppliers.supplier_id', $supplier_id);
                                            })
                                            ->when($product_id != 0, function ($query) use ($product_id) {
                                                return $query->where('product_entries.id', $product_id);
                                            })
                                            ->groupBy('product_suppliers.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_suppliers.supplier_id) as supplier_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT product_suppliers.whole_sale_price) as whole_sale_price,
                                                         GROUP_CONCAT(DISTINCT product_suppliers.retail_price) as retail_price
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->get();
        
            $products               = $product->groupBy('id');
        }
        
        $supplier_name              = Customers::find($supplier_id);
        $product_name               = ProductEntries::find($product_id);

        return view('reports::product_suppliers_print', compact('user_info', 'from_date', 'supplier_name', 'product_name' , 'to_date', 'products', 'report_type'));
    }

    public function productCustomers()
    {
        return view('reports::product_customers');
    }

    public function productCustomersPrint()
    {
        $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $product_id             = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $report_type            = isset($_GET['report_type']) ? $_GET['report_type'] : 0;

        if ($report_type == 1)
        {
            $product                = ProductCustomers::leftjoin('product_entries', 'product_entries.id', 'product_customers.product_entry_id')
                                            ->leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_customers.customer_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                return $query->where('product_customers.customer_id', $customer_id);
                                            })
                                            ->when($product_id != 0, function ($query) use ($product_id) {
                                                return $query->where('product_entries.id', $product_id);
                                            })
                                            ->groupBy('product_customers.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_customers.customer_id) as customer_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT customers.name) as customer_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_customers.whole_sale_price) as whole_sale_price,
                                                         GROUP_CONCAT(DISTINCT product_customers.retail_price) as retail_price
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->get();
        
            $products               = $product->groupBy('customer_name');
        }

        if ($report_type == 2)
        {
            $product                = ProductCustomers::leftjoin('product_entries', 'product_entries.id', 'product_customers.product_entry_id')
                                            ->leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_customers.customer_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                return $query->where('product_customers.customer_id', $customer_id);
                                            })
                                            ->when($product_id != 0, function ($query) use ($product_id) {
                                                return $query->where('product_entries.id', $product_id);
                                            })
                                            ->groupBy('product_customers.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_customers.customer_id) as customer_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT customers.name) as customer_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT product_customers.whole_sale_price) as whole_sale_price,
                                                         GROUP_CONCAT(DISTINCT product_customers.retail_price) as retail_price
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->get();
        
            $products               = $product->groupBy('id');
        }
        
        $customer_name              = Customers::find($customer_id);
        $product_name               = ProductEntries::find($product_id);

        return view('reports::product_customers_print', compact('user_info', 'from_date', 'customer_name', 'product_name' , 'to_date', 'products', 'report_type'));
    }

    public function customerPaymentReport()
    {
        return view('reports::customer_payment_report');
    }

    public function customerPaymentReportPrint()
    {
        $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;

        $payments               = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')               
                                                ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('payments.customer_id', $customer_id);
                                                })
                                                ->where('payments.type', 0)
                                                ->select('payments.*',
                                                         'paid_through_accounts.name as paid_through_account_name',
                                                         'customers.name as customer_name')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')         
                                                ->leftjoin('invoices', 'invoices.id', 'payment_entries.invoice_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                                })
                                                ->where('payment_entries.invoice_id', '!=', null)
                                                ->select('payment_entries.*', 'invoices.invoice_number as invoice_number')
                                                ->get();

        foreach ($payments as $key => $value)
        {
            $datas[$value['id']]['payments']           = $value;
            $datas[$value['id']]['payment_entries']    = array_values($payment_entries->where('payment_id', $value['id'])->toArray());
        }

        if (isset($datas))
        {
            $data  = array_values($datas);
        }
        else
        {
            $data  = [];
        }

        $customer_name          = Customers::find($customer_id);

        return view('reports::customer_payment_report_print', compact('data', 'from_date', 'to_date', 'user_info', 'customer_name'));
    }

    public function supplierPaymentReport()
    {
        return view('reports::supplier_payment_report');
    }

    public function supplierPaymentReportPrint()
    {
        $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $supplier_id            = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : 0;

        $payments               = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')               
                                                ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                                })
                                                ->when($supplier_id != 0, function ($query) use ($supplier_id) {
                                                    return $query->where('payments.customer_id', $supplier_id);
                                                })
                                                ->where('payments.type', 1)
                                                ->select('payments.*',
                                                         'paid_through_accounts.name as paid_through_account_name',
                                                         'customers.name as customer_name')
                                                ->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')         
                                                ->leftjoin('bills', 'bills.id', 'payment_entries.bill_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('payments.payment_date', [$from_date, $to_date]);
                                                })
                                                ->where('payment_entries.bill_id', '!=', null)
                                                ->select('payment_entries.*', 'bills.bill_number as bill_number')
                                                ->get();

        foreach ($payments as $key => $value)
        {
            $datas[$value['id']]['payments']           = $value;
            $datas[$value['id']]['payment_entries']    = array_values($payment_entries->where('payment_id', $value['id'])->toArray());
        }

        if (isset($datas))
        {
            $data  = array_values($datas);
        }
        else
        {
            $data  = [];
        }

        $supplier_name          = Customers::find($supplier_id);

        return view('reports::supplier_payment_report_print', compact('data', 'from_date', 'to_date', 'user_info', 'supplier_name'));
    }

    public function emergencyItemList()
    {
        return view('reports::emergency_item_list');
    }

    public function emergencyItemListPrint()
    {
        $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $category_id            = isset($_GET['category_id']) ? $_GET['category_id'] : 0;
        $product_id             = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $brand_id               = isset($_GET['brand_id']) ? $_GET['brand_id'] : 0;
        $variation_id           = isset($_GET['variation_id']) ? $_GET['variation_id'] : 0;

        $product            = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->when($category_id != 0, function ($query) use ($category_id) {
                                                return $query->orWhere('product_entries.product_id', $category_id);
                                            })
                                            ->when($product_id != 0, function ($query) use ($product_id) {
                                                return $query->orWhere('product_entries.id', $product_id);
                                            })
                                            ->when($brand_id != 0, function ($query) use ($brand_id) {
                                                return $query->orWhere('product_entries.brand_id', $brand_id);
                                            })
                                            ->when($variation_id != 0, function ($query) use ($variation_id) {
                                                return $query->orWhereIn('product_variation_values.id', $variation_id);
                                            })
                                            ->whereRaw('product_entries.stock_in_hand <= product_entries.alert_quantity')
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                         GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT products.id) as product_id
                                                        ')
                                            ->orderBy('product_entries.product_id', 'ASC')
                                            ->get();
        
        $products               = $product->sortBy('name')->all();
        $products               = collect($product)->groupBy('category_name');

        $item_category_name     = Products::find($category_id);
        $product_name           = ProductEntries::find($product_id);
        $brand_name             = Categories::find($brand_id);

        if ($variation_id != 0)
        {
            $variations             = ProductVariationValues::whereIn('id', $variation_id)->selectRaw('name')->get();
            $variation_name         = $variations->implode('name', ',');
        }
        else
        {
            $variation_name         = '';
        }
        
        return view('reports::emergency_item_list_print', compact('user_info', 'item_category_name', 'product_name', 'brand_name', 'products', 'variation_name'));
    }

    public function dueReportCustomerPrintDue()
    {
        $branches               = Branches::orderBy('created_at', 'ASC')->get();
        $user_info              = Users::find(1);
        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime('01-01-2020'));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $ledger_type            = 1;
        $branch_id              = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;

        $customers              = Customers::where('contact_type', 0)
                                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('customers.id', $customer_id);
                                            })
                                            ->where('id', '!=', 1)
                                            ->where('customers.branch_id', $branch_id)
                                            ->orderBy('name', 'ASC')
                                            ->get();

        $total_balance              = 0;

        if ($customers->count() > 0)
        {
            foreach ($customers as $key => $value)
            {
                if ($ledger_type == 1)
                {
                    if ($value->balance != 0)
                    {
                        $data[$value['id']]['customer_id']      = $value['id'];
                        $data[$value['id']]['customer_name']    = $value['name'];
                        $data[$value['id']]['phone']            = $value['phone'];
                        $data[$value['id']]['address']          = $value['address'];
                        $data[$value['id']]['balance']          = $value->balance;

                        $total_balance           = $total_balance + $value->balance;
                    }
                }
                else
                {
                    $data[$value['id']]['customer_id']      = $value['id'];
                    $data[$value['id']]['customer_name']    = $value['name'];
                    $data[$value['id']]['phone']            = $value['phone'];
                    $data[$value['id']]['address']          = $value['address'];
                    $data[$value['id']]['balance']          = $value->balance;
                    
                    $total_balance           = $total_balance + $value->balance;
                }
            }
        }
        
        if(!empty($data))
        {
            $data = $data;
        }
        else
        {
            $data = [];
        }

        $customer_name    = Customers::find($customer_id);

        return view('reports::due_report_customer_due', compact('data', 'total_balance', 'from_date', 'to_date', 'user_info', 'customers', 'customer_name', 'ledger_type', 'branches', 'branch_id'));
    }

    public function dueReportCustomer()
    {
        $branches               = Branches::orderBy('created_at', 'ASC')->get();
        $user_info              = Users::find(1);
        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime('01-01-2020'));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $ledger_type            = isset($_GET['ledger_type']) ? $_GET['ledger_type'] : 0;
        $branch_id              = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;

        $customers              = Customers::where('contact_type', 0)
                                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('customers.id', $customer_id);
                                            })
                                            ->where('id', '!=', 1)
                                            ->where('customers.branch_id', $branch_id)
                                            ->orderBy('name', 'ASC')
                                            ->get();
        $total_balance              = 0;

        if ($customers->count() > 0)
        {
            foreach ($customers as $key => $value)
            {   
                if ($ledger_type == 1)
                {
                    if ($value->balance != 0)
                    {
                        $data[$value['id']]['customer_id']      = $value['id'];
                        $data[$value['id']]['customer_name']    = $value['name'];
                        $data[$value['id']]['phone']            = $value['phone'];
                        $data[$value['id']]['address']          = $value['address'];
                        $data[$value['id']]['balance']          = $value->balance;

                        $total_balance           = $total_balance + $value->balance;
                    }
                }
                else
                {
                    $data[$value['id']]['customer_id']      = $value['id'];
                    $data[$value['id']]['customer_name']    = $value['name'];
                    $data[$value['id']]['phone']            = $value['phone'];
                    $data[$value['id']]['address']          = $value['address'];
                    $data[$value['id']]['balance']          = $value->balance;
                    
                    $total_balance           = $total_balance + $value->balance;
                }
            }
        }
        // dd('ok');
        if(!empty($data))
        {
            $data = $data;
        }
        else
        {
            $data = [];
        }

        $customer_name    = Customers::find($customer_id);

        return view('reports::due_report_customer', compact('data', 'total_balance', 'from_date', 'to_date', 'user_info', 'customers', 'customer_name', 'ledger_type', 'branches', 'branch_id'));
    }

    public function dueReportCustomerPrint()
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End
        $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id_search']) ? $_GET['customer_id_search'] : 0;
        $branch_id            = isset($_GET['branch_id']) ? $_GET['branch_id'] : 0;

        $invoices               = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('invoices.customer_id', $customer_id);
                                                })
                                                ->when($branch_id != 1, function ($query) use ($branch_id) {
                                                    return $query->where('invoices.branch_id', $branch_id);
                                                })
                                                ->select('invoices.customer_id as customer_id',
                                                    'customers.phone as phone',
                                                    'invoices.invoice_amount as invoice_amount',
                                                    'invoices.return_amount as return_amount',
                                                    'invoices.due_amount as due_amount')
                                                ->get();

        $payment_entries_return = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                                ->leftjoin('sales_return', 'sales_return.id', 'payment_entries.sales_return_id')
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('payments.customer_id', $customer_id);
                                                })
                                                ->when($branch_id != 1, function ($query) use ($branch_id) {
                                                    return $query->where('payments.branch_id', $branch_id);
                                                })
                                                ->where('payments.type', 2)
                                                ->select('payment_entries.*',
                                                         'payments.customer_id as customer_id')
                                                ->get();

        $customers              = Customers::where('contact_type', 0)
                                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('customers.id', $customer_id);
                                            })
                                            ->orderBy('created_at', 'DESC')
                                            ->get();

        $total_invoice_amount       = 0;
        $total_paid_amount          = 0;
        $total_due_amount           = 0;
        $total_return_amount        = 0;
        $total_return_due_amount    = 0;

        if ($invoices->count() > 0)
        {
            foreach ($customers as $key => $value)
            {
                $invoice_amount         = $invoices->where('customer_id', $value['id'])->sum('invoice_amount'); 
                $return_amount          = $invoices->where('customer_id', $value['id'])->sum('return_amount'); 
                $due_amount             = $invoices->where('customer_id', $value['id'])->sum('due_amount');
                $paid_amount            = $invoice_amount - $due_amount;
                $sales_return_paid      = $payment_entries_return->where('customer_id', $value['id'])->sum('amount');
                $sale_diff              = $invoice_amount - $return_amount;
                $paid_diff              = $paid_amount - $sales_return_paid;

                $data[$value['id']]['customer_id']      = $value['id'];
                $data[$value['id']]['customer_name']    = $value['name'];
                $data[$value['id']]['phone']            = $value['phone'];
                $data[$value['id']]['invoice_amount']   = $invoice_amount;
                $data[$value['id']]['paid_amount']      = $paid_amount;
                $data[$value['id']]['due_amount']       = $due_amount;
                $data[$value['id']]['return_amount']    = $return_amount;
                $data[$value['id']]['return_due']       = $return_amount - $sales_return_paid;

                // if ($due_amount > 0)
                // {
                    $total_invoice_amount               = $total_invoice_amount + $invoice_amount;
                    $total_paid_amount                  = $total_paid_amount + $paid_amount;
                    $total_due_amount                   = $total_due_amount + $due_amount;
                    $total_return_amount                = $total_return_amount + $return_amount;
                    $total_return_due_amount            = $total_return_due_amount + ($return_amount - $sales_return_paid);
                // }
            }
        }
        else
        {
            $data  =  [];   
        }

        $customer_name    = Customers::find($customer_id);

        return view('reports::due_report_customer_print', compact('data', 'total_invoice_amount', 'total_paid_amount', 'total_due_amount', 'total_return_amount', 'total_return_due_amount', 'from_date', 'to_date', 'user_info', 'customers', 'customer_name'));
    }

    public function dueReportCustomerDetails($customer_id)
    {
        $user_info          = Users::find(1);
        $customer_name      = Customers::find($customer_id);
        $branch_id          = $customer_name['branch_id'];
        $date               = date('Y-m-d');
        $from_date          = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date            = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $opening_debit      = JournalEntries::where('date','<', $from_date)
                                        ->where('customer_id', $customer_id)
                                        ->whereIn('transaction_head', ['sales', 'customer-opening-balance', 'payment-made', 'customer-settlement'])
                                        ->where('debit_credit', 1)
                                        ->where('branch_id', $branch_id)
                                        ->sum('amount');
        $opening_credit     = JournalEntries::where('date','<', $from_date)
                                        ->where('customer_id', $customer_id)
                                        ->whereIn('transaction_head', ['payment-receive', 'sales-return'])
                                        ->where('debit_credit', 0)
                                        ->where('branch_id', $branch_id)
                                        ->sum('amount');
        $opening_balance    = $opening_debit - $opening_credit;

        $credit = JournalEntries::whereBetween('journal_entries.date', [$from_date, $to_date])
                                ->where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['sales', 'customer-opening-balance', 'payment-made', 'customer-settlement'])
                                ->where('debit_credit', 0)
                                ->where('branch_id', $branch_id)
                                ->get();
        $debit  = JournalEntries::whereBetween('journal_entries.date', [$from_date, $to_date])
                                ->where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['payment-receive', 'sales-return'])
                                ->where('debit_credit', 1)
                                ->where('branch_id', $branch_id)
                                ->get();
        $data   = $debit->merge($credit)->sortBy('id');

        return view('reports::due_report_customer_details', compact('data', 'from_date', 'to_date', 'user_info', 'customer_name', 'opening_balance'));
    }

    public function dueLedger()
    {
        return view('reports::due_ledger');
    }

    public function dueLedgerPrint()
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End
        $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id            = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;

        $invoices               = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('invoices.customer_id', $customer_id);
                                                })
                                                ->select('invoices.customer_id as customer_id',
                                                    'customers.phone as phone',
                                                    'invoices.invoice_amount as invoice_amount',
                                                    'invoices.return_amount as return_amount',
                                                    'invoices.due_amount as due_amount')
                                                ->get();

        $bills                  = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('bills.vendor_id', $customer_id);
                                                })
                                                ->select('bills.vendor_id as vendor_id',
                                                    'customers.phone as phone',
                                                    'bills.bill_amount as bill_amount',
                                                    'bills.return_amount as return_amount',
                                                    'bills.due_amount as due_amount')
                                                ->get();


        $customers              = Customers::where('contact_type', 0)
                                            ->orWhere('contact_type', 1)
                                            ->when($customer_id != 0, function ($query) use ($customer_id) {
                                                    return $query->where('customers.id', $customer_id);
                                            })
                                            ->orderBy('created_at', 'DESC')
                                            ->get();

        $total_invoice_amount       = 0;
        $total_customer_paid_amount = 0;
        $total_customer_due_amount  = 0;
        $total_bill_amount          = 0;
        $total_supplier_paid_amount = 0;
        $total_supplier_due_amount  = 0;

        if (($invoices->count() > 0) || ($bills->count() > 0))
        {
            foreach ($customers as $key => $value)
            {
                $invoice_amount         = $invoices->where('customer_id', $value['id'])->sum('invoice_amount'); 
                $customer_due_amount    = $invoices->where('customer_id', $value['id'])->sum('due_amount');
                $customer_paid_amount   = $invoice_amount - $customer_due_amount;
                $bill_amount            = $bills->where('vendor_id', $value['id'])->sum('bill_amount'); 
                $supplier_due_amount    = $bills->where('vendor_id', $value['id'])->sum('due_amount');
                $supplier_paid_amount   = $bill_amount - $supplier_due_amount;

                $data[$value['id']]['customer_name']            = $value['name'];
                $data[$value['id']]['phone']                    = $value['phone'];
                $data[$value['id']]['invoice_amount']           = $invoice_amount;
                $data[$value['id']]['customer_paid_amount']     = $customer_paid_amount;
                $data[$value['id']]['customer_due_amount']      = $customer_due_amount;
                $data[$value['id']]['bill_amount']              = $bill_amount;
                $data[$value['id']]['supplier_paid_amount']     = $supplier_paid_amount;
                $data[$value['id']]['supplier_due_amount']      = $supplier_due_amount;

                if ($customer_due_amount > 0)
                {
                    $total_invoice_amount           = $total_invoice_amount + $invoice_amount;
                    $total_customer_paid_amount     = $total_customer_paid_amount + $customer_paid_amount;
                    $total_customer_due_amount      = $total_customer_due_amount + $customer_due_amount;
                }

                if ($supplier_due_amount > 0)
                {
                    $total_bill_amount              = $total_bill_amount + $bill_amount;
                    $supplier_paid_amount           = $supplier_paid_amount + $supplier_paid_amount;
                    $total_supplier_due_amount      = $total_supplier_due_amount + $supplier_due_amount;
                }
            }
        }
        else
        {
            $data  =  [];   
        }

        $customer_name    = Customers::find($customer_id);

        return view('reports::due_ledger_print', compact('data', 'total_invoice_amount', 'total_customer_paid_amount', 'total_customer_due_amount', 'total_bill_amount', 'supplier_paid_amount', 'total_supplier_due_amount', 'from_date', 'to_date', 'user_info', 'customers', 'customer_name'));
    }

    public function productWiseSalesReport()
    {
        $branches  = Branches::orderBy('created_at', 'ASC')->get();
        return view('reports::product_wise_sales_report_index', compact('branches'));
    }

    public function productWiseSalesReportPrint()
    {
        $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $product_id             = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
        $branch_id              = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;

        $invoice_entries        = InvoiceEntries::leftjoin('invoices', 'invoices.id', 'invoice_entries.invoice_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('invoice_entries.product_entry_id', $product_id);
                                                })
                                                ->when($branch_id != 1, function ($query) use ($branch_id) {
                                                    return $query->where('invoices.branch_id', $branch_id);
                                                })
                                                ->select('invoice_entries.*')
                                                ->get();

        $products               = ProductEntries::select('id', 'name', 'product_code', 'unit_id')
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('product_entries.id', $product_id);
                                                })
                                                ->get();

        if ($invoice_entries->count() > 0)
        {
            foreach ($products as $key => $value)
            {
                $data[$value['id']]['product_name']       = $value['name'];
                $data[$value['id']]['product_code']       = str_pad($value['product_code'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['unit_name']          = $value->unit_id != null ? $value->unit->name : '';
                $data[$value['id']]['total_amount']       = $invoice_entries->where('product_entry_id', $value['id'])->sum('quantity');
            }
        }
        else
        {
            $data = [];
        }

        $item_name     = ProductEntries::find($product_id);

        return view('reports::product_wise_sales_report_print', compact('user_info', 'from_date', 'to_date', 'data', 'item_name'));
    }

    public function IncomeExpenseLedger()
    {
        $branches       = Branches::orderBy('created_at', 'ASC')->get();

        return view('reports::income_expense_ledger', compact('branches'));
    }

    public function IncomeExpenseLedgerPrint()
    {
        $user_info  = Users::find(1);
        $branch_id  = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;
        $date       = date('Y-m-d');
        $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $opening_data       = JournalEntries::where('journal_entries.date', '<', $from_date)
                                                ->where('journal_entries.account_id', 1)
                                                ->where('branch_id', $branch_id)
                                                ->select('journal_entries.debit_credit', 'journal_entries.amount')
                                                ->get();

        $opening_debit      = $opening_data->where('debit_credit', 1)->sum('amount');
        $opening_credit     = $opening_data->where('debit_credit', 0)->sum('amount');
        $opening_balance    = $opening_debit - $opening_credit;

        $data               = JournalEntries::whereBetween('journal_entries.date',[$from_date,$to_date])
                                                ->where('journal_entries.account_id', 1)
                                                ->where('branch_id', $branch_id)
                                                ->select('journal_entries.*')
                                                ->orderBy('id', 'ASC')
                                                ->get();

        return view('reports::income_expense_ledger_print', compact('user_info', 'from_date', 'to_date', 'data', 'opening_balance'));
    }

    public function GeneralLedger()
    {
        $branches       = Branches::orderBy('created_at', 'ASC')->get();

        return view('reports::general_ledger', compact('branches'));
    }

    public function GeneralLedgerPrint()
    {
        $user_info  = Users::find(1);
        $branch_id  = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;
        $date       = date('Y-m-d');
        $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $opening_data   = AccountTransactions::where('account_transactions.type', '!=', 2)
                                            ->when($branch_id != 1, function ($query) use ($branch_id) {
                                                return $query->where('account_transactions.branch_id', $branch_id);
                                            })
                                            ->whereIn('account_transactions.transaction_head', ['sales', 'purchase', 'customer-advance', 'supplier-advance', 'customer-advance-adjustment', 'supplier-advance-adjustment'])
                                            ->where('account_transactions.transaction_date', '<', $from_date)
                                            ->get();

        $opening_transactions   = Transactions::where('transactions.date', '<', $from_date)
                                            ->when($branch_id != 1, function ($query) use ($branch_id) {
                                                return $query->where('transactions.branch_id', $branch_id);
                                            })
                                            ->where('transactions.account_head', '!=', 'customer-opening-balance')
                                            ->where('transactions.account_head', '!=', 'supplier-opening-balance')
                                            ->get();

        $opening_result['sales']                = $opening_transactions->where('type', 0)->where('account_head', 'sales')->sum('amount');
        $opening_result['sales_return']         = $opening_transactions->where('type', 0)->where('account_head', 'sales-return')->sum('amount');
        $opening_result['purchase']             = $opening_transactions->where('type', 1)->where('account_head', 'purchase')->sum('amount');
        $opening_result['purchase_return']      = $opening_transactions->where('type', 1)->where('account_head', 'purchase-return')->sum('amount');

        $opening_result['received']             = $opening_data->where('transaction_head', 'sales')->sum('amount');
        $opening_result['paid']                 = $opening_data->where('transaction_head', 'purchase')->sum('amount');
        $opening_result['customer_advance']     = $opening_data->where('transaction_head', 'customer-advance')->sum('amount');
        $opening_result['supplier_advance']     = $opening_data->where('transaction_head', 'supplier-advance')->sum('amount');
        $opening_result['customer_advance_adj'] = $opening_data->where('transaction_head', 'customer-advance-adjustment')->sum('amount');
        $opening_result['supplier_advance_adj'] = $opening_data->where('transaction_head', 'supplier-advance-adjustment')->sum('amount');

        $data           = AccountTransactions::where('account_transactions.type', '!=', 2)
                                        ->when($branch_id != 1, function ($query) use ($branch_id) {
                                            return $query->where('account_transactions.branch_id', $branch_id);
                                        })
                                        ->whereIn('account_transactions.transaction_head', ['sales', 'purchase', 'customer-advance', 'supplier-advance', 'customer-advance-adjustment', 'supplier-advance-adjustment'])
                                        ->whereBetween('account_transactions.transaction_date', [$from_date, $to_date])
                                        ->get();

        $transactions   = Transactions::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('transactions.date', [$from_date, $to_date]);
                                        })
                                        ->when($branch_id != 1, function ($query) use ($branch_id) {
                                            return $query->where('transactions.branch_id', $branch_id);
                                        })
                                        ->where('transactions.account_head', '!=', 'customer-opening-balance')
                                        ->where('transactions.account_head', '!=', 'supplier-opening-balance')
                                        ->get();

        $result['sales']                = $transactions->where('type', 0)->where('account_head', 'sales')->sum('amount');
        $result['sales_return']         = $transactions->where('type', 0)->where('account_head', 'sales-return')->sum('amount');
        $result['purchase']             = $transactions->where('type', 1)->where('account_head', 'purchase')->sum('amount');
        $result['purchase_return']      = $transactions->where('type', 1)->where('account_head', 'purchase-return')->sum('amount');

        $result['received']             = $data->where('transaction_head', 'sales')->sum('amount');
        $result['paid']                 = $data->where('transaction_head', 'purchase')->sum('amount');
        $result['customer_advance']     = $data->where('transaction_head', 'customer-advance')->sum('amount');
        $result['supplier_advance']     = $data->where('transaction_head', 'supplier-advance')->sum('amount');
        $result['customer_advance_adj'] = $data->where('transaction_head', 'customer-advance-adjustment')->sum('amount');
        $result['supplier_advance_adj'] = $data->where('transaction_head', 'supplier-advance-adjustment')->sum('amount');

        return view('reports::general_ledger_print', compact('user_info', 'from_date', 'to_date', 'result', 'opening_result'));
    }

    public function stockStatusDetails()
    {
        $branches  = Branches::orderBy('created_at', 'ASC')->get();
        return view('reports::stock_status_details', compact('branches'));
    }

    public function stockStatusDetailsPrint()
    {
        $user_info                  = Users::find(1);
        $date                       = date('Y-m-d');
        $from_date                  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $branch_id                  = isset($_GET['branch_id']) ? $_GET['branch_id'] : 0;
        $category_id                = isset($_GET['category_id']) ? $_GET['category_id'] : 0;
        $product_id                 = isset($_GET['product_id']) ? $_GET['product_id'] : 0;

        $invoices                   = InvoiceEntries::leftjoin('invoices', 'invoices.id', 'invoice_entries.invoice_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->where('invoices.branch_id', $branch_id)
                                                ->when($category_id != 0, function ($query) use ($category_id) {
                                                    return $query->where('invoice_entries.product_id', $category_id);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('invoice_entries.product_entry_id', $product_id);
                                                })
                                                ->where('invoices.type', 1)
                                                ->selectRaw('invoice_entries.*')
                                                ->get();

        $opening_invoices           = InvoiceEntries::leftjoin('invoices', 'invoices.id', 'invoice_entries.invoice_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->where('invoices.invoice_date', '<', $from_date);
                                                })
                                                ->where('invoices.branch_id', $branch_id)
                                                ->when($category_id != 0, function ($query) use ($category_id) {
                                                    return $query->where('invoice_entries.product_id', $category_id);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('invoice_entries.product_entry_id', $product_id);
                                                })
                                                ->where('invoices.type', 1)
                                                ->selectRaw('invoice_entries.*')
                                                ->get();

        $sales_returns              = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.sales_return_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('sales_return.sales_return_date', [$from_date, $to_date]);
                                                })
                                                ->where('sales_return.branch_id', $branch_id)
                                                ->when($category_id != 0, function ($query) use ($category_id) {
                                                    return $query->where('sales_return_entries.product_id', $category_id);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('sales_return_entries.product_entry_id', $product_id);
                                                })
                                                ->selectRaw('sales_return_entries.*')
                                                ->get();

        $opening_sales_returns      = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.sales_return_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->where('sales_return.sales_return_date', '<', $from_date);
                                                })
                                                ->where('sales_return.branch_id', $branch_id)
                                                ->when($category_id != 0, function ($query) use ($category_id) {
                                                    return $query->where('sales_return_entries.product_id', $category_id);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('sales_return_entries.product_entry_id', $product_id);
                                                })
                                                ->selectRaw('sales_return_entries.*')
                                                ->get();

        $bills                      = BillEntries::leftjoin('bills', 'bills.id', 'bill_entries.bill_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('bills.bill_date', [$from_date, $to_date]);
                                                })
                                                ->where('bills.branch_id', $branch_id)
                                                ->when($category_id != 0, function ($query) use ($category_id) {
                                                    return $query->where('bill_entries.product_id', $category_id);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('bill_entries.product_entry_id', $product_id);
                                                })
                                                ->where('bills.type', 1)
                                                ->selectRaw('bill_entries.*')
                                                ->get();

        $opening_bills              = BillEntries::leftjoin('bills', 'bills.id', 'bill_entries.bill_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->where('bills.bill_date', '<', $from_date);
                                                })
                                                ->where('bills.branch_id', $branch_id)
                                                ->when($category_id != 0, function ($query) use ($category_id) {
                                                    return $query->where('bill_entries.product_id', $category_id);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('bill_entries.product_entry_id', $product_id);
                                                })
                                                ->where('bills.type', 1)
                                                ->selectRaw('bill_entries.*')
                                                ->get();

        $purchase_returns           = PurchaseReturnEntries::leftjoin('purchase_return', 'purchase_return.id', 'purchase_return_entries.purchase_return_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('purchase_return.purchase_return_date', [$from_date, $to_date]);
                                                })
                                                ->where('purchase_return.branch_id', $branch_id)
                                                ->when($category_id != 0, function ($query) use ($category_id) {
                                                    return $query->where('purchase_return_entries.product_id', $category_id);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('purchase_return_entries.product_entry_id', $product_id);
                                                })
                                                ->selectRaw('purchase_return_entries.*')
                                                ->get();

        $opening_purchase_returns   = PurchaseReturnEntries::leftjoin('purchase_return', 'purchase_return.id', 'purchase_return_entries.purchase_return_id')
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->where('purchase_return.purchase_return_date', '<', $from_date);
                                                })
                                                ->where('purchase_return.branch_id', $branch_id)
                                                ->when($category_id != 0, function ($query) use ($category_id) {
                                                    return $query->where('purchase_return_entries.product_id', $category_id);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('purchase_return_entries.product_entry_id', $product_id);
                                                })
                                                ->selectRaw('purchase_return_entries.*')
                                                ->get();

        $stock_transfer             = StockTransfers::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('stock_transfers.date', [$from_date, $to_date]);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('stock_transfers.product_entry_id', $product_id);
                                                })
                                                ->get();

        $opening_stock_transfer     = StockTransfers::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->where('stock_transfers.date', '<', $from_date);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('stock_transfers.product_entry_id', $product_id);
                                                })
                                                ->get();

        $products                   = ProductEntries::when($category_id != 0, function ($query) use ($category_id) {
                                                    return $query->where('product_id', $category_id);
                                                })
                                                ->when($product_id != 0, function ($query) use ($product_id) {
                                                    return $query->where('id', $product_id);
                                                })
                                                ->get();

        if ($products->count() > 0)
        {
            foreach ($products as $key => $value)
            {   
                if($branch_id == 1)
                {
                    $open_stock = $value['opening_stock'];
                }
                else
                {
                    $open_stock = 0;
                }
                
                $to_transfers                       = $stock_transfer->where('product_entry_id', $value['id'])->where('transfer_from', $branch_id)->sum('quantity');
                $from_transfers                     = $stock_transfer->where('product_entry_id', $value['id'])->where('transfer_to', $branch_id)->sum('quantity');

                $opening_from_transfers             = $opening_stock_transfer->where('product_entry_id', $value['id'])->where('transfer_to', $branch_id)->sum('quantity');
                $opening_to_transfers               = $opening_stock_transfer->where('product_entry_id', $value['id'])->where('transfer_from', $branch_id)->sum('quantity');

                $sales                              = $invoices->where('product_entry_id', $value['id'])->sum('quantity');
                $sale_return                        = $sales_returns->where('product_entry_id', $value['id'])->sum('quantity');
                $purchases                          = $bills->where('product_entry_id', $value['id'])->sum('quantity');
                $purchase_return                    = $purchase_returns->where('product_entry_id', $value['id'])->sum('quantity');

                $opening_sales                      = $opening_invoices->where('product_entry_id', $value['id'])->sum('quantity');
                $opening_sale_return                = $opening_sales_returns->where('product_entry_id', $value['id'])->sum('quantity');
                $opening_purchases                  = $opening_bills->where('product_entry_id', $value['id'])->sum('quantity');
                $opening_purchase_return            = $opening_purchase_returns->where('product_entry_id', $value['id'])->sum('quantity');

                $data[$value['id']]['product_code']         = str_pad($value['product_code'], 6, "0", STR_PAD_LEFT);
                $data[$value['id']]['product_name']         = $value['name'];
                $data[$value['id']]['product_category']     = $value->product_id != null ? $value->product->name : '';
                $data[$value['id']]['unit_name']            = $value->unit_id != null ? $value->unit->name : '';
                $data[$value['id']]['opening_stock']        = $open_stock + $opening_from_transfers + $opening_purchases + $opening_sale_return - $opening_to_transfers - $opening_sales + $opening_purchase_return;
                $data[$value['id']]['to_transfers']         = $to_transfers;
                $data[$value['id']]['purchase']             = $purchases;
                $data[$value['id']]['sale_return']          = $sale_return;
                $data[$value['id']]['inward']               = $from_transfers + $purchases + $sale_return;
                $data[$value['id']]['from_transfers']       = $from_transfers;
                $data[$value['id']]['sales']                = $sales;
                $data[$value['id']]['purchase_return']      = $purchase_return;
                $data[$value['id']]['outward']              = $to_transfers + $sales + $purchase_return;
                $data[$value['id']]['closing_balance']      = $open_stock + $opening_from_transfers + $opening_purchases + $opening_sale_return - $opening_to_transfers - $opening_sales + $opening_purchase_return + $from_transfers + $purchases + $sale_return - $to_transfers - $sales - $purchase_return;
                
                // $update_brance  = BranchInventories::where('branch_id', $branch_id)->where('product_entry_id', $value->id)->first();
                // if($update_brance != null)
                // {
                //     $update_brance->stock_in_hand = $open_stock + $opening_from_transfers + $opening_purchases + $opening_sale_return - $opening_to_transfers - $opening_sales + $opening_purchase_return + $from_transfers + $purchases + $sale_return - $to_transfers - $sales - $purchase_return;
                //     $update_brance->save(); 
                // }
                               
            }
        }
        else
        {
            $data   = [];
        }

        $data  = collect($data)->groupBy('product_category');

        $branch_name    = Branches::find($branch_id);
        $category_name  = Products::find($category_id);
        $product_name   = ProductEntries::find($product_id);

        return view('reports::stock_status_details_print', compact('branch_name','category_name', 'product_name', 'user_info', 'from_date', 'to_date', 'data'));
    }

    public function DailyReport()
    {
        $branches       = Branches::orderBy('created_at', 'ASC')->get();

        return view('reports::daily_report', compact('branches'));
    }

    public function DailyReportPrint()
    {
        $user_info  = Users::find(1);
        $branch_id  = Auth::user()->branch_id;
        $date       = date('Y-m-d');
        $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

        $data['incomes']    = Transactions::whereIn('transactions.account_head', ['sales','previoue-due-collection','purchase-return','income'])
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('transactions.date', [$from_date, $to_date]);
                                                })
                                                ->when($branch_id != 1, function ($query) use ($branch_id) {
                                                    return $query->where('transactions.branch_id', $branch_id);
                                                })
                                                ->where('transactions.account_head', '!=', 'customer-opening-balance')
                                                ->where('transactions.account_head', '!=', 'supplier-opening-balance')
                                                ->where('transactions.type', 1)
                                                ->select('transactions.*')
                                                ->get();

        $data['expenses']   = Transactions::whereIn('transactions.account_head', ['purchase','previoue-due-paid','sales-return','expense'])
                                                ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('transactions.date', [$from_date, $to_date]);
                                                })
                                                ->when($branch_id != 1, function ($query) use ($branch_id) {
                                                    return $query->where('transactions.branch_id', $branch_id);
                                                })
                                                ->where('transactions.account_head', '!=', 'customer-opening-balance')
                                                ->where('transactions.account_head', '!=', 'supplier-opening-balance')
                                                ->where('transactions.type', 0)
                                                ->select('transactions.*')
                                                ->get();

        return view('reports::daily_report_print', compact('user_info', 'from_date', 'to_date', 'data'));
    }

    public function DailyCollectionReport()
    {
        $branches       = Branches::orderBy('created_at', 'ASC')->get();
        return view('reports::daily_collection_report', compact('branches'));
    }

    public function DailyCollectionReportPrint()
    {
        $user_info      = Users::find(1);
        $date           = date('Y-m-d');
        $from_date      = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date        = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $branch_id      = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;

        $customers      = Customers::where('contact_type', 0)
                                    ->when($customer_id != 0, function ($query) use ($customer_id) {
                                        return $query->where('customers.id', $customer_id);
                                    })
                                    ->where('customers.branch_id', $branch_id)
                                    ->get();

        $invoices       = Invoices::when($customer_id != 0, function ($query) use ($customer_id) {
                                        return $query->where('invoices.customer_id', $customer_id);
                                    })
                                    ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                        return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                    })
                                    ->where('invoices.branch_id', $branch_id)
                                    ->where('invoices.type', 1)
                                    ->select('invoices.id as invoice_id',
                                             'invoices.invoice_amount as invoice_amount',
                                             'invoices.customer_id as customer_id',
                                             'invoices.due_amount as due_amount'
                                            )
                                    ->get();

        $invoice_entries    = InvoiceEntries::leftjoin('invoices', 'invoices.id', 'invoice_entries.invoice_id')
                                    ->when($customer_id != 0, function ($query) use ($customer_id) {
                                        return $query->where('invoices.customer_id', $customer_id);
                                    })
                                    ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                        return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                    })
                                    ->where('invoice_entries.branch_id', $branch_id)
                                    ->where('invoices.type', 1)
                                    ->select('invoice_entries.invoice_id as invoice_id',
                                             'invoice_entries.product_entry_id as product_entry_id',
                                             'invoice_entries.customer_id as customer_id',
                                             'invoice_entries.rate as rate',
                                             'invoice_entries.quantity as quantity',
                                             'invoice_entries.total_amount as total_amount'
                                            )
                                    ->get();
        $expenses           = Expenses::when($customer_id != 0, function ($query) use ($customer_id) {
                                        return $query->where('expenses.customer_id', $customer_id);
                                    })
                                    ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                        return $query->whereBetween('expenses.expense_date', [$from_date, $to_date]);
                                    })
                                    ->where('expenses.branch_id', $branch_id)
                                    ->select('expenses.*')
                                    ->get();

        $genaral_expense    = $expenses->sum('amount');
        $payments           = CurrentBalance::when($customer_id != 0, function ($query) use ($customer_id) {
                                        return $query->where('current_balance.customer_id', $customer_id);
                                    })
                                    ->when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                        return $query->whereBetween('current_balance.transaction_date', [$from_date, $to_date]);
                                    })
                                    ->where('current_balance.branch_id', $branch_id)
                                    ->select('current_balance.*')
                                    ->get();

        if ($customers->count() > 0)
        {
            foreach ($customers as $key => $customer)
            {
                $data[$customer->id]['customer_id']     = $customer->id;
                $data[$customer->id]['customer_name']   = $customer->name;
                $data[$customer->id]['customer_advance']= $payments->where('customer_id', $customer->id)->where('paid_through_id', 1)->where('account_head', 'customer_advance_payment')->sum('amount');
                $data[$customer->id]['sale']            = $invoice_entries->where('customer_id', $customer->id)->sum('total_amount');
                $data[$customer->id]['due_amount']      = $invoices->where('customer_id', $customer->id)->sum('due_amount');
                $data[$customer->id]['dsr_expense']     = $expenses->where('customer_id', $customer->id)->sum('amount');
                $data[$customer->id]['cash_payment']    = $payments->where('customer_id', $customer->id)->where('paid_through_id', 1)->sum('amount');
                $data[$customer->id]['bank_payment']    = $payments->where('customer_id', $customer->id)->where('paid_through_id', '!=', 1)->sum('amount');
            }
        }
        else
        {
            $data = [];
        }

        $find_customer  = Customers::find($customer_id);
        
        $transactions           = CurrentBalance::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('current_balance.transaction_date', [$from_date, $to_date]);
                                        })
                                        ->where('current_balance.branch_id', $branch_id)
                                        ->get();
                                        
        $opening_transactions   = CurrentBalance::when($from_date && $to_date, function ($query) use ($from_date, $to_date) {
                                            return $query->where('current_balance.transaction_date', '<', $from_date);
                                        })
                                        ->where('current_balance.branch_id', $branch_id)
                                        ->get();
                                        
        $accounts   = PaidThroughAccounts::get();
    
        foreach ($accounts as $key => $value)
        {   
            $in_amount      = $transactions->where('paid_through_id', $value['id'])->where('type', 0)->sum('amount');
            $out_amount     = $transactions->where('paid_through_id', $value['id'])->where('type', 1)->sum('amount');
            $transfer_in    = $transactions->where('type', 2)->where('transfer_to', $value['id'])->sum('amount');
            $transfer_out   = $transactions->where('type', 2)->where('transfer_from', $value['id'])->sum('amount');
            $balance        = $in_amount - $out_amount + $transfer_in - $transfer_out;
            
            $opening_in_amount      = $opening_transactions->where('paid_through_id', $value['id'])->where('type', 0)->sum('amount');
            $opening_out_amount     = $opening_transactions->where('paid_through_id', $value['id'])->where('type', 1)->sum('amount');
            $opening_transfer_in    = $opening_transactions->where('type', 2)->where('transfer_to', $value['id'])->sum('amount');
            $opening_transfer_out   = $opening_transactions->where('type', 2)->where('transfer_from', $value['id'])->sum('amount');
            $opening_balance        = $opening_in_amount - $opening_out_amount + $opening_transfer_in - $opening_transfer_out;

            $data_current[$value['id']]['account_id']       = $value['id'];
            $data_current[$value['id']]['account_name']     = $value['name'];
            $data_current[$value['id']]['opening_balance']  = $opening_balance;
            $data_current[$value['id']]['cash_in_sum']      = $in_amount + $transfer_in;
            $data_current[$value['id']]['cash_out_sum']     = $out_amount + $transfer_out;
            $data_current[$value['id']]['balance']          = $opening_balance + $balance;
        }
        
        return view('reports::daily_collection_report_print', compact('user_info', 'from_date', 'to_date', 'data', 'genaral_expense', 'find_customer', 'data_current'));
    }

    public function stockTransferReport()
    {
        $branches  = Branches::orderBy('created_at', 'ASC')->get();
        return view('reports::stock_transfer_report', compact('branches'));
    }

    public function stockTransferReportPrint()
    {
        $user_info              = Users::find(1);

        $date                   = date('Y-m-d');
        $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
        $branch_id              = isset($_GET['branch_id']) ? $_GET['branch_id'] : Auth::user()->branch_id;
        
        $receive                = StockTransfers::where('transfer_to', $branch_id)
                                                ->whereBetween('date', [$from_date, $to_date])
                                                ->get();

        $transfer               = StockTransfers::where('transfer_from', $branch_id)
                                                ->whereBetween('date', [$from_date, $to_date])
                                                ->get();
 
        $branch_name            = Branches::find($branch_id);

        return view('reports::stock_transfer_report_print', compact('user_info', 'from_date', 'to_date', 'branch_name', 'receive', 'transfer'));
    }

    //Common Ajax Functions
    public function invoiceListAjax()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Invoices::orderBy('invoices.created_at', 'DESC')
                                    ->where('invoices.type', 1)
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Invoices::where('invoices.invoice_number', 'LIKE', "%$search%")
                                    ->where('invoices.type', 1)
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            $invoiceNumber  = 'INV - '.str_pad($value['invoice_number'], 6, "0", STR_PAD_LEFT);

            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[]         = array("id"=>$value['id'], "text"=>$invoiceNumber);

            $i++;
        }
   
        return Response::json($data);
    }

    public function billListAjax()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Bills::orderBy('bills.created_at', 'DESC')
                                    ->where('bills.type', 1)
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Bills::where('bills.bill_number', 'LIKE', "%$search%")
                                    ->where('bills.type', 1)
                                    ->orderBy('bills.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            $billNumber     = 'BILL - '.str_pad($value['bill_number'], 6, "0", STR_PAD_LEFT);

            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[]         = array("id"=>$value['id'], "text"=>$billNumber);

            $i++;
        }
   
        return Response::json($data);
    }

    public function usersListAjax()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Users::orderBy('users.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Users::where('users.name', 'LIKE', "%$search%")
                                    ->orderBy('users.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[] = array("id"=>$value['id'], "text"=>$value['name']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function categoryListAjax()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Products::orderBy('products.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Products::where('products.name', 'LIKE', "%$search%")
                                    ->orderBy('products.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[] = array("id"=>$value['id'], "text"=>$value['name']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function productListAjax()
    {
        if(!isset($_GET['searchTerm']))
        {   
            $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                        ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                        ->groupBy('product_entries.id')
                                        ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                     GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                     GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                     GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                     GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                     GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                     GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                     GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                     GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                     GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                     GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                     GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                     GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                     GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                     GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                     GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                     GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                    ')
                                        ->orderBy('product_entries.product_id', 'ASC')
                                        ->take(100)
                                        ->get();

            $product            = $data->sortBy('name')->all();
            $fetchData          = collect($product);
        }
        else
        { 
            $search     = $_GET['searchTerm'];

            $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                        ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                        ->where('product_entries.name', 'LIKE', "%$search%")
                                        ->groupBy('product_entries.id')
                                        ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                     GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                     GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                     GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                     GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                     GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                     GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                     GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                     GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                     GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                     GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                     GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                     GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                     GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                     GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                     GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                     GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                    ')
                                        ->orderBy('product_entries.product_id', 'ASC')
                                        ->take(100)
                                        ->get();

            $product            = $data->sortBy('name')->all();
            $fetchData          = collect($product);
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($value['variations'] != null)
            {
                $variations = ' - ' . $value['variations'];
            }
            else
            {
                $variations = '';
            }

            $product_name   = $value['name'] . $variations;

            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[]         = array("id"=>$value['id'], "text"=>$product_name);

            $i++;
        }
   
        return Response::json($data);
    }

    public function productCodeListAjax()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = ProductEntries::orderBy('product_entries.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = ProductEntries::where('product_entries.product_code', 'LIKE', "%$search%")
                                    ->orderBy('product_entries.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            $productCode  = str_pad($value['product_code'], 6, "0", STR_PAD_LEFT);

            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[] = array("id"=>$value['id'], "text"=>$productCode);

            $i++;
        }
   
        return Response::json($data);
    }

    public function brandListAjax()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Categories::orderBy('categories.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Categories::where('categories.name', 'LIKE', "%$search%")
                                    ->orderBy('categories.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[] = array("id"=>$value['id'], "text"=>$value['name']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function variationListAjax()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = ProductVariationValues::orderBy('product_variation_values.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = ProductVariationValues::where('product_variation_values.name', 'LIKE', "%$search%")
                                    ->orderBy('product_variation_values.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        foreach ($fetchData as $key => $value)
        {   
            $data[] = array("id"=>$value['id'], "text"=>$value['name']);
        }
   
        return Response::json($data);
    }

    public function paidAccountListAjax()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = PaidThroughAccounts::where('paid_through_accounts.name', 'LIKE', "%$search%")
                                    ->orderBy('paid_through_accounts.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[] = array("id"=>$value['id'], "text"=>$value['name']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function emergencyCategoryListAjax()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Products::orderBy('products.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Products::where('products.name', 'LIKE', "%$search%")
                                    ->orderBy('products.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[] = array("id"=>$value['id'], "text"=>$value['name']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function emergencyProductListAjax()
    {
        if(!isset($_GET['searchTerm']))
        {   
            $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                        ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                        ->whereRaw('product_entries.stock_in_hand <= product_entries.alert_quantity')
                                        ->groupBy('product_entries.id')
                                        ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                     GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                     GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                     GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                     GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                     GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                     GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                     GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                     GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                     GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                     GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                     GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                     GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                     GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                     GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                     GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                     GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                    ')
                                        ->orderBy('product_entries.product_id', 'ASC')
                                        ->take(100)
                                        ->get();

            $product            = $data->sortBy('name')->all();
            $fetchData          = collect($product);
        }
        else
        { 
            $search     = $_GET['searchTerm'];

            $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                        ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                        ->where('product_entries.name', 'LIKE', "%$search%")
                                        ->whereRaw('product_entries.stock_in_hand <= product_entries.alert_quantity')
                                        ->groupBy('product_entries.id')
                                        ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                     GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                     GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                     GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                     GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                     GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                     GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                     GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                     GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                     GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                     GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                     GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                     GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                     GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                     GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                     GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                     GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                     GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                     GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                    ')
                                        ->orderBy('product_entries.product_id', 'ASC')
                                        ->take(100)
                                        ->get();

            $product            = $data->sortBy('name')->all();
            $fetchData          = collect($product);
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($value['variations'] != null)
            {
                $variations = ' - ' . $value['variations'];
            }
            else
            {
                $variations = '';
            }

            $product_name   = $value['name'] . $variations;

            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[]         = array("id"=>$value['id'], "text"=>$product_name);

            $i++;
        }
   
        return Response::json($data);
    }
    
    public function branchContacts($id)
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Customers::orderBy('customers.created_at', 'DESC')
                                    ->where('customers.branch_id', $id)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Customers::where('customers.name', 'LIKE', "%$search%")
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->where('customers.branch_id', $id)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[] = array("id"=>$value['id'], "text"=>$value['name'], "contact_type"=>$value['contact_type']);

            $i++;
        }
   
        return Response::json($data);
    }
}

