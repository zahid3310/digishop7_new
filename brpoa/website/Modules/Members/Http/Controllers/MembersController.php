<?php

namespace Modules\Members\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Users;
use Auth;

class MembersController extends Controller
{
    
    /**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        if(Auth::user())
        {
          $lists = Users::where('role',4)->get();
          return view('members::index',compact('lists'));  
        }else
        {
            return abort(404);
        }
        
    }

    public function create()
    {
        return view('members::create');
    }

}
