@extends('layouts.app')

@section('title', 'About List')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">

  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
      <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center" style="border-top:3px solid #563d7c;
        border-radius: 6px;height: 70px!important;"> 
        <h4 class="page-title">ADD Video</h4>
        <a class="nav-link" href="{{route('GalleryVideoList')}}">
          <span class="btn btn-primary"><i class="fa fa-list"></i> Video List </span>
        </a>
      </div>
      <div class="card-body">
        @if(session()->has('success'))
                <div class="alert alert-success" role="alert" id="success-alert">
                    {{ session()->get('success') }}
                </div>
        @endif
        <form action="{{route('StoreGalleryVideo')}}" method="POST" enctype="multipart/form-data">
          @csrf
          <fieldset>
            <div class="form-group">
              <label for="cemail">Thumbnail</label>
              <input class="form-control" type="file" name="thumbnail">
            </div>             

            <div class="form-group" style="display:none;">
              <label for="cemail">Video</label>
              <input class="form-control" type="file" name="video_file">
            </div>            

            <div class="form-group">
              <label for="cemail">YouTube Link Code</label>
              <input class="form-control" type="text" name="link_code">
            </div>


            <div class="form-group">
              <label for="curl">Status</label>
              <select class="form-control" name="status">
                <option value="1">Active</option>
                <option value="2">InActive</option>
              </select>
            </div>
            <input class="btn btn-primary" type="submit" value="Save">
          </fieldset>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-2"></div>
</div>
</div>
</div>
</div>
@endsection

@section('scripts')


@endsection