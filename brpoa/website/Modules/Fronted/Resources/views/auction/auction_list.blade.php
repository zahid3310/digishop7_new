@extends('fronted::layouts.master')
@section('title','Auction List')
@section('styles')

@endsection

@section('content')

        <section class="bg-half-100 d-table w-100" style="background: url({{url("public/website/images/brpel-footer.jpg")}});">
            <div class="bg-overlay bg-gradient-overlay"></div>
            <div class="container">
                <div class="row mt-5 justify-content-center">
                    <div class="col-12">
                        <div class="title-heading text-center">
                            <h5 class="heading fw-semibold page-heading text-white">BRPOA</h5>
                            <p class="text-white-50 para-desc mx-auto mb-0">Auction List</p>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section>

        <!-- Start -->
        <section class="section">
            <div class="container">

                 <div class="row">
                     <div class="col-12">
                             <div class="card" style="background-color: #c9bebe33;border: 1px solid #dfdede;    margin-bottom: 15px;">
                             <div class="card-body">
                                 <div class="row">
                                     <div class="col-4">
                                         <img class="card-img-top" style="height: 190px; width: 250px;" src="{{ url('public/'.$AuctionSchedule->auction_banner) }}">
                                     </div>
                                     <div class="col-8">
                                        <h4>Auction - {{$AuctionSchedule->auction_name}}</h4>



                                        <strong>Auction Start Date : </strong> {{date('d-m-Y', strtotime($AuctionSchedule->auction_start_date))}} ||

                                        <strong>  End Date : </strong> {{date('d-m-Y', strtotime($AuctionSchedule->auction_end_date))}}

                                        <p style="text-align:justify">{{$AuctionSchedule->auction_description}}</p>
                                        <a href="{{route('Web Pigeon List',str_replace(' ', '-', $AuctionSchedule->auction_name))}}" class="btn btn-info">See Pigeon List</a>
                                    </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>


            </div><!--end container-->

        </section><!--end section-->
        <!-- End -->

        @endsection
