@extends('fronted::layouts.master')
@section('title','Photo Gallery')
@section('styles')

@endsection

@section('content')

  <!-- Start -->
        <section class="bg-half-100 d-table w-100" style="background: url({{url("public/website/images/brpel-footer.jpg")}});">
            <div class="bg-overlay bg-gradient-overlay"></div>
            <div class="container">
                <div class="row mt-5 justify-content-center">
                    <div class="col-12">
                        <div class="title-heading text-center">
                            <h5 class="heading fw-semibold page-heading text-white">BRPOA</h5>
                            <p class="text-white-50 para-desc mx-auto mb-0">Video Gallery</p>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section>
        <!-- End -->

        <!-- Start -->
                <!-- Start -->
        <section class="section">
            <div class="container">
                <div class="row">
                    @foreach($data as $key=> $item)
                    <div class="col-lg-4 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                        <div class="sidebar sticky-bar">
                            <!-- Author -->
                            <div class="widget text-center position-relative">
                                <div class="rounded shadow bg-white">
                                    <div class="position-relative">
                                        <img src="{{url('public/website/thumbnail',$item->thumbnail)}}" class="img-fluid rounded shadow" alt="">
                                        <div class="play-icon">
                                            <a href="javascript:void(0)" data-type="youtube" data-id="{{$item->link_code}}" class="play-btn lightbox">
                                                <i class="mdi mdi-play text-primary rounded-circle bg-white shadow"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Author -->
                        </div>
                    </div><!--end col-->
                    @endforeach

                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- End -->
        <!-- End -->

        @endsection