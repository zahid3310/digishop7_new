@extends('fronted::layouts.master')
@section('title','Result Archive')
@section('styles')

@endsection

@section('content')

<section class="bg-half-100 d-table w-100" style="background: url({{url("public/website/images/brpel-footer.jpg")}});">
    <div class="bg-overlay bg-gradient-overlay"></div>
    <div class="container">
        <div class="row mt-5 justify-content-center">
            <div class="col-12">
                <div class="title-heading text-center">
                    <h5 class="heading fw-semibold page-heading text-white">Result</h5>
                    <p class="text-white-50 para-desc mx-auto mb-0">Result Archive</p>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>

                <!-- Start -->
        <section class="section">
            <div class="container">
                <div class="row">
                    <div class="col-12">

                        <div class="table-responsive bg-white shadow rounded">


                            <table id="example" class="table table-success table-striped table-bordered">
                                 <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Spot Name</th>
                                        <th class="text-center">Race Date</th>
                                        <th></th>
                                    </tr>
                                </thead>

                                <tbody>
                                @foreach($race_archive_list as $key=> $value)                 
                                <tr bgcolor="#fff" style="color:#000 !important; font-weight:500; text-transform:uppercase;">
                                    <td>{{$loop->index+1}}</td>
                                    <td>{{$value->spot_name}}</td>
                                    <td>{{$value->race_date}}</td>
                                    <td><a style="color:#000;" href="/index.php/result-archive?spot=MIRSHORAI STADIUM&amp;date=2022-03-13">Show</a> </td>
                                </tr>
                                @endforeach
                       </tbody>
                            </table>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->

            </div><!--end container-->
        </section><!--end section-->
        <!-- End -->

@endsection