@extends('fronted::layouts.master')
@section('title','Photo Gallery')
@section('styles')

@endsection

@section('content')

        <section class="bg-half-100 d-table w-100" style="background: url({{url("public/website/images/brpel-footer.jpg")}});">
            <div class="bg-overlay bg-gradient-overlay"></div>
            <div class="container">
                <div class="row mt-5 justify-content-center">
                    <div class="col-12">
                        <div class="title-heading text-center">
                            <h5 class="heading fw-semibold page-heading text-white">BRPOA</h5>
                            <p class="text-white-50 para-desc mx-auto mb-0">Photo Gallery</p>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section>

        <!-- Start -->
        <section class="section">
            <div class="container">

                 <div class="row" style="display:none;">
                    
                        <h3>Photo Gallery </h3>
                        <div class="col-lg-3 col-md-3">
                            <label>Title</label>
                            <select id="single" class="js-states form-control">
                                <option>Select Title</option>
                                <option>A</option>
                                <option>B</option>
                                <option>C</option>
                                <option>D</option>
                            </select>
                        </div>

                        <div class="col-lg-3 col-md-3">
                            <label>Display Number</label>
                            <select id="single" class="js-states form-control">
                                <option>Select Title</option>
                                <option>20</option>
                                <option>50</option>
                                <option>100</option>
                                <option>All</option>
                            </select>
                        </div>

                        <div class="col-lg-3 col-md-3">
                            <label>Ordering</label>
                            <select id="single" class="js-states form-control">
                                <option>Select Ordering</option>
                                <option>Name</option>
                                <option>Title</option>
                                <option>C</option>
                                <option>D</option>
                            </select>
                        </div>

                        <div class="col-lg-3 col-md-3">
                            <a href="javascript:void(0)" class="btn btn-primary mt-4" style="padding: 6px 20px!important;">Search</a>
                        </div>
                    

                </div>


                <hr>


                <div id="grid" class="row">

                    @foreach($data as $key=> $item)
                    <div class="col-md-3 col-12 spacing picture-item" data-groups='["branding"]'>
                        <div class="card portfolio portfolio-classic portfolio-primary rounded overflow-hidden">
                            <div class="card-img position-relative">
                                <img src="{{url('public/website/photoGallery/',$item->image)}}" class="img-fluid" alt="">
                                <div class="card-overlay"></div>

                                <div class="pop-icon">
                                    <a href="{{url('public/website/photoGallery/',$item->image)}}" class="btn btn-pills btn-icon lightbox"><i class="uil uil-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </div><!--end col-->
                    @endforeach

                </div><!--end row-->
            </div><!--end container-->

        </section><!--end section-->
        <!-- End -->

        @endsection