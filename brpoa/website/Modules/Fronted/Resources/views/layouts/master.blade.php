<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Bangladesh Racing Pigeon Owner’s Association</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 5 Landing Page Template" />
        <meta name="keywords" content="Startup, Business, Multi-uses, HTML, Clean, Modern, Creative" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="shreethemes@gmail.com" />
        <meta name="website" content="https://shreethemes.in/" />
        <meta name="Version" content="v1.0.0" />
        <!-- favicon -->
        <link href="{{ url('public/website/images/favicon.ico') }}" rel="shortcut icon">
        <!-- Bootstrap and other css -->
        <link href="{{ url('public/website/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('public/website/css/swiper.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('public/website/css/tobii.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('public/website/css/tiny-slider.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('public/website/css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="../../unicons.iconscout.com/release/v3.0.6/css/line.css" rel="stylesheet" />
        <!-- Main Css -->
        <link href="{{ url('public/website/css/style.min.css') }}" rel="stylesheet" type="text/css" id="theme-opt" />
        <link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" rel="stylesheet" />

    </head>

    <body>        
        
        <!-- Navbar STart -->
        <header id="topnav" class="defaultscroll sticky">
            <div class="container">
                <!-- Logo container-->
                <a class="logo" href="{{route('cover')}}">
                    <h3 style="font-size: 40px!important;margin-top: 10px;margin-left: 30px;" class="l-dark">B.R.P.O.A</h3>
                    <img src="{{ url('public/website/images/logo__12_-removebg-preview.png') }}" class="l-light mt-2" height="100" alt="">
                </a>
                <!-- End Logo container-->
                <div class="menu-extras">
                    <div class="menu-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle" id="isToggle" onclick="toggleMenu()">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </div>
                </div>

                <ul class="buy-button list-inline mb-0">
                    <li class="list-inline-item search-icon mb-0">
                        <div class="dropdown">
                            <button type="button" class="btn btn-link text-decoration-none dropdown-toggle mb-0 p-0" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="uil uil-search h5 text-dark nav-light-icon-dark mb-0"></i>
                                <i class="uil uil-search h5 text-white nav-light-icon-white mb-0"></i>
                            </button>
                            <div class="dropdown-menu dd-menu dropdown-menu-end bg-white shadow rounded border-0 mt-4 py-0" style="width: 300px;">
                                <form class="p-4">
                                    <input type="text" id="text" name="name" class="form-control border bg-white" placeholder="Search...">
                                </form>
                            </div>
                        </div>
                    </li>
                </ul>

                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class="navigation-menu nav-light">

                        <li class="has-submenu parent-parent-menu-item">
                            <a href="javascript:void(0)">B.R.P.O.A</a><span class="menu-arrow"></span>
                            <ul class="submenu">
                                <li><a href="{{route('cover')}}" class="sub-menu-item">B.R.P.O.A</a></li>
                                <!--<li><a href="javascript::" class="sub-menu-item">Executive Member</a></li>-->
                                <li><a href="{{route('Member List')}}" class="sub-menu-item">Member</a></li>
                            </ul>
                        </li>

                        
                        <li class="has-submenu parent-parent-menu-item">
                            <a href="javascript:void(0)">Race</a><span class="menu-arrow"></span>
                            <ul class="submenu">
                                <li><a href="{{route('Race Result')}}" class="sub-menu-item">Race Result</a></li>
                                <li><a href="{{route('Result Archive')}}" class="sub-menu-item">Result Archive</a></li>
                            </ul>
                        </li>

                        <li><a href="{{route('Search Velocity')}}" class="sub-menu-item">Velocity</a></li>
                        <li><a href="{{route('Search Distances')}}" class="sub-menu-item">Distances</a></li>
                        <li><a href="{{route('Search Pigeon')}}" class="sub-menu-item">Search Pigeon</a></li>
        
                        <li class="has-submenu parent-parent-menu-item">
                            <a href="javascript:void(0)">Media</a><span class="menu-arrow"></span>
                            <ul class="submenu">
                                <li><a href="{{route('photo_gallery')}}" class="sub-menu-item">Photo Gallery</a></li>
                                <li><a href="{{route('video_gallery')}}" class="sub-menu-item">Video Gallery</a></li>
                            </ul>
                        </li>

                        <li class="has-submenu parent-parent-menu-item">
                            <a href="javascript:void(0)">Login</a><span class="menu-arrow"></span>
                            <ul class="submenu">
                                <li><a href="https://digishop7.com/brpoa/website/login/" class="sub-menu-item" target="_blank">Member Log In</a></li>
                                <li><a href="https://digishop7.com/brpoa/admin/" target="_blank" class="sub-menu-item">Admin Log In</a></li>
                                <li><a href="{{route('auction registration')}}" target="_blank" class="sub-menu-item">Auction Registration</a></li>
                            </ul>
                        </li>

                        <li class="has-submenu parent-parent-menu-item">
                            <a style="padding-top: 5px!important; padding-bottom: 5px!important; min-height: 0px!important;margin-top: 19px;" class="btn btn-danger" href="javascript:void(0)">Auction</a><span class="menu-arrow"></span>
                            <ul class="submenu">
                                <li><a href="{{route('Web Auction')}}" class="sub-menu-item">{{AuctionSchedule()->auction_name}}</a></li>
                                <li><a href="javascript::" class="sub-menu-item">Auction Archive</a></li>
                            </ul>
                        </li>

                    </ul><!--end navigation menu-->
                </div><!--end navigation-->
            </div><!--end container-->
        </header><!--end header-->
        <!-- Navbar End -->

        <!-- Body lay out -->
        @yield('content')
        <!-- Body lay out end -->

        <!-- Footer Start -->
        <footer class="footer bg-footer">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="footer-py-60">
                            <div class="row">
                                <div class="col-lg-5 col-12 mb-0 mb-md-4 pb-0 pb-md-2">
                                    <a href="#" class="logo-footer">
                                        <img src="{{url('public/website/images/logo__12_-removebg-preview.png')}}" class="l-light" height="100" alt="">
                                    </a>
                                    <p class="mt-4">Bangladesh Racing Pigeon Owner’s Association (BRPOA)</p>

                                    <h4>B.R.P.O.A Office</h4>
                                    <p class="mt-4">Address: Mirpur dhaka 1216</p>
                                    <p class="mt-4">Mobile: 01615888377, 01760100009, 01979440770</p>
                                    <ul class="list-unstyled social-icon foot-social-icon mb-0 mt-4">
                                        <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                                        <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
                                        <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="twitter" class="fea icon-sm fea-social"></i></a></li>
                                        <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="linkedin" class="fea icon-sm fea-social"></i></a></li>
                                    </ul><!--end icon-->
                                </div><!--end col-->
                                
                                <div class="col-lg-4 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                                    <h6 class="footer-head">Quick Links</h6>
                                    <ul class="list-unstyled footer-list mt-4">
                                        <li><a href="javascript::" class="text-foot"><i class="uil uil-angle-right-b me-1"></i> Online Registration</a></li>
                                        <li><a href="javascript::" class="text-foot"><i class="uil uil-angle-right-b me-1"></i> Our Members</a></li>
                                        <li><a href="javascript::" class="text-foot"><i class="uil uil-angle-right-b me-1"></i> Race Result Archive</a></li>
                                        <li><a href="javascript::" class="text-foot"><i class="uil uil-angle-right-b me-1"></i> Contact Us</a></li>
                                    </ul>
                                </div><!--end col-->

                                <div class="col-lg-3 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                                    <h6 class="footer-head">Newsletter</h6>
                                    <p class="mt-4">Sign up and receive the latest tips via email.</p>
                                    
                                    <div class="subcribe-form footer-subscribe">
                                        <form>
                                            <input name="email" id="email2" type="email" class="form-control rounded-pill shadow" placeholder="Email :" required>
                                            <button type="submit" class="btn btn-pills btn-icon btn-primary"><i class="uil uil-search"></i></button>
                                        </form><!--end form-->
                                    </div>
                                </div><!--end col-->
                            </div><!--end row-->
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->

            <div class="footer-py-30 footer-bar bg-footer">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col">
                            <div class="text-center">
                                <p class="mb-0 text-muted"><script>document.write(new Date().getFullYear())</script> Copyright 2016 Bangladesh Racing Pigeon Owner’s Association(BRPOA).<br>Powerd by OMER FAROOQUE <i class="mdi mdi-heart text-danger"></i> Design & developed by<a href="https://cyberdynetechnologyltd.com/" target="_blank" class="text-reset"> Cyberdyne Technology Ltd</a>.</p>
                            </div>
                        </div><!--end col-->
                    </div><!--end row-->
                </div><!--end container-->
            </div>
        </footer><!--end footer-->
        <!-- End -->

        <!-- Back to top -->
        <a href="javascript:void(0)" onclick="topFunction()" id="back-to-top" class="back-to-top rounded-pill"><i class="mdi mdi-arrow-up"></i></a>
        <!-- Back to top -->

        <!-- javascript -->
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="{{ url('public/website/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ url('public/website/js/swiper.min.js') }}"></script>
        <script src="{{ url('public/website/js/parallax.js') }}"></script>
        <script src="{{ url('public/website/js/tobii.min.js') }}"></script>
        <script src="{{ url('public/website/js/tiny-slider.js') }}"></script>
        <script src="{{ url('public/website/js/feather.min.js') }}"></script>
        <!-- Main Js -->
        <script src="{{ url('public/website/js/plugins.init.js') }}"></script><!--Note: All init js like tiny slider, counter, countdown, maintenance, lightbox, gallery, swiper slider, aos animation etc.-->
        <script src="{{ url('public/website/js/app.js') }}"></script><!--Note: All important javascript like page loader, menu, sticky menu, menu-toggler, one page menu etc. -->
    </body>
</html>

