@extends('fronted::layouts.master')
@section('title','Distances')
@section('styles')

@endsection

@section('content')

<section class="bg-half-100 d-table w-100" style="background: url({{url("public/website/images/brpel-footer.jpg")}});">
    <div class="bg-overlay bg-gradient-overlay"></div>
    <div class="container">
        <div class="row mt-5 justify-content-center">
            <div class="col-12">
                <div class="title-heading text-center">
                    <h5 class="heading fw-semibold page-heading text-white">Distances</h5>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>

@extends('fronted::layouts.master')
@section('title','Pigeon')
@section('styles')

@endsection

@section('content')

<section class="bg-half-100 d-table w-100" style="background: url({{url("public/website/images/brpel-footer.jpg")}});">
    <div class="bg-overlay bg-gradient-overlay"></div>
    <div class="container">
        <div class="row mt-5 justify-content-center">
            <div class="col-12">
                <div class="title-heading text-center">
                    <h5 class="heading fw-semibold page-heading text-white">Pigeon</h5>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>

            <!-- Start -->
        <section class="section">
            <div class="container">
                <div class="row">
                    <div class="col-12">

                        <div class="table-responsive bg-white shadow rounded">


                            <table id="example3" class="table table-success table-striped table-bordered">
                                
                
                            <thead>
                    <tr>
                        
                        <th width="1%">ID</th>
                        <th width="15%">Name</th>
                                                     <th width="8%">Sirajgonj (Fuljor College Math) </th>
                                                         <th width="8%">BOGRA </th>
                                                         <th width="8%">PIRGONJ </th>
                                                     <th width="10%">BIRGONJ </th>
                        
                                                     <th width="8%">TETULIA </th>
                                                         <th width="8%">JAMUNA TOLL PLAZA </th>
                                                         <th width="8%">SARIYAKANDI (BOGRA) </th>
                                                         <th width="8%">PALASHBARI </th>
                                                         <th width="8%">RANGPUR </th>
                                                     <th width="10%">DOMAR </th>
                        
                                                     <th width="8%">BANGLABANDHA </th>
                                                         <th width="8%">Chowmuhani-2 </th>
                                                         <th width="8%">Mirshorai-2 </th>
                                                         <th width="8%">Chittagong-2 </th>
                                                         <th width="8%">Chokoria </th>
                                                         <th width="8%">KATHIR MATHA TEKNAF </th>
                                                         <th width="8%">SAINTMARTIN ISLAND </th>
                                                         <th width="8%">SONAIMURI </th>
                                                         <th width="8%">NANGALKOT </th>
                                                         <th width="8%">BASHKHALI </th>
                                                         <th width="8%">COXS BAZAR </th>
                                                         <th width="8%">MONOHORGONJ </th>
                                                         <th width="8%">MIRSHORAI STADIUM </th>
                                                    
                    </tr>
                </thead>
                
                <tbody>
                
                	                	    
                    <tr bgcolor="#cbdcfa">
                                            <td>1</td>
                        <td> Mohammed Younus</td>
                                                <td>104.35</td>
                      
                        
                                                <td>149.95</td>
                      
                        
                                                <td>210.84</td>
                      
                        
                                                <td>289.61</td>
                      
                        
                                                <td>358.11</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>122.11</td>
                      
                        
                                                <td>166.92</td>
                      
                        
                                                <td>211.44</td>
                      
                        
                                                <td>279.15</td>
                      
                        
                                                <td>318.23</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>114.51</td>
                      
                        
                                                <td>110.79</td>
                      
                        
                                                <td>257.14</td>
                      
                        
                                                <td>314.45</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>6</td>
                        <td>MD. AMDAD HOSSAIN BHUIYAN</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>89.74</td>
                      
                        
                                                <td>146.74</td>
                      
                        
                                                <td>204.17</td>
                      
                        
                                                <td>253.36</td>
                      
                        
                                                <td>302.81</td>
                      
                        
                                                <td>371.86</td>
                      
                        
                                                <td>117.60</td>
                      
                        
                                                <td>163.09</td>
                      
                        
                                                <td>207.19</td>
                      
                        
                                                <td>274.58</td>
                      
                        
                                                <td>339.25</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>7</td>
                        <td>MD. MAHBUBUR RAHMAN &amp; MD MAHFUZUR RAHMAN</td>
                                                <td>110.22</td>
                      
                        
                                                <td>156.20</td>
                      
                        
                                                <td>217.48</td>
                      
                        
                                                <td>296.04</td>
                      
                        
                                                <td>364.66</td>
                      
                        
                                                <td>91.54</td>
                      
                        
                                                <td>148.36</td>
                      
                        
                                                <td>205.71</td>
                      
                        
                                                <td>254.88</td>
                      
                        
                                                <td>304.35</td>
                      
                        
                                                <td>373.44</td>
                      
                        
                                                <td>115.76</td>
                      
                        
                                                <td>161.03</td>
                      
                        
                                                <td>205.26</td>
                      
                        
                                                <td>272.77</td>
                      
                        
                                                <td>311.68</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>105.08</td>
                      
                        
                                                <td>250.82</td>
                      
                        
                                                <td>307.87</td>
                      
                        
                                                <td>105.05</td>
                      
                        
                                                <td>162.41</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>9</td>
                        <td>Md. Babul Miah</td>
                                                <td>104.91</td>
                      
                        
                                                <td>150.82</td>
                      
                        
                                                <td>212.15</td>
                      
                        
                                                <td>290.66</td>
                      
                        
                                                <td>359.30</td>
                      
                        
                                                <td>86.16</td>
                      
                        
                                                <td>143.01</td>
                      
                        
                                                <td>200.38</td>
                      
                        
                                                <td>249.56</td>
                      
                        
                                                <td>299.03</td>
                      
                        
                                                <td>368.10</td>
                      
                        
                                                <td>121.15</td>
                      
                        
                                                <td>166.33</td>
                      
                        
                                                <td>210.63</td>
                      
                        
                                                <td>278.16</td>
                      
                        
                                                <td>317.05</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>113.62</td>
                      
                        
                                                <td>110.32</td>
                      
                        
                                                <td>256.20</td>
                      
                        
                                                <td>313.23</td>
                      
                        
                                                <td>110.40</td>
                      
                        
                                                <td>167.71</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>10</td>
                        <td>Sheikh Meraz ( Mizu )</td>
                                                <td>121.47</td>
                      
                        
                                                <td>169.01</td>
                      
                        
                                                <td>221.37</td>
                      
                        
                                                <td>271.95</td>
                      
                        
                                                <td>313.64</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>11</td>
                        <td>MEHEDI HASAN SUMON</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>105.55</td>
                      
                        
                                                <td>162.40</td>
                      
                        
                                                <td>219.66</td>
                      
                        
                                                <td>268.81</td>
                      
                        
                                                <td>318.30</td>
                      
                        
                                                <td>387.41</td>
                      
                        
                                                <td>101.77</td>
                      
                        
                                                <td>147.33</td>
                      
                        
                                                <td>191.33</td>
                      
                        
                                                <td>258.77</td>
                      
                        
                                                <td>323.73</td>
                      
                        
                                                <td>297.69</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>14</td>
                        <td>MD. BELLAL HOSSAIN SHAKE</td>
                                                <td>115.74</td>
                      
                        
                                                <td>161.77</td>
                      
                        
                                                <td>222.97</td>
                      
                        
                                                <td>301.60</td>
                      
                        
                                                <td>370.19</td>
                      
                        
                                                <td>97.12</td>
                      
                        
                                                <td>153.91</td>
                      
                        
                                                <td>211.23</td>
                      
                        
                                                <td>260.39</td>
                      
                        
                                                <td>309.87</td>
                      
                        
                                                <td>378.98</td>
                      
                        
                                                <td>110.20</td>
                      
                        
                                                <td>155.52</td>
                      
                        
                                                <td>199.70</td>
                      
                        
                                                <td>267.20</td>
                      
                        
                                                <td>306.15</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>102.68</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>15</td>
                        <td>MD. KAMRUL HASAN</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>111.01</td>
                      
                        
                                                <td>154.90</td>
                      
                        
                                                <td>199.86</td>
                      
                        
                                                <td>267.96</td>
                      
                        
                                                <td>307.61</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>17</td>
                        <td>ABU SAYEM MOHAMMAD JEWEL</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>92.12</td>
                      
                        
                                                <td>149.03</td>
                      
                        
                                                <td>206.42</td>
                      
                        
                                                <td>255.59</td>
                      
                        
                                                <td>305.06</td>
                      
                        
                                                <td>374.13</td>
                      
                        
                                                <td>115.20</td>
                      
                        
                                                <td>160.60</td>
                      
                        
                                                <td>204.75</td>
                      
                        
                                                <td>272.19</td>
                      
                        
                                                <td>336.99</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>18</td>
                        <td>Md. Badruzzaman Milon</td>
                                                <td>121.40</td>
                      
                        
                                                <td>168.94</td>
                      
                        
                                                <td>221.30</td>
                      
                        
                                                <td>271.88</td>
                      
                        
                                                <td>313.58</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>121.21</td>
                      
                        
                                                <td>165.98</td>
                      
                        
                                                <td>210.52</td>
                      
                        
                                                <td>278.24</td>
                      
                        
                                                <td>343.42</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>21</td>
                        <td>TUSHAR MD. MAMUN NOWAB (MAMUN)</td>
                                                <td>129.41</td>
                      
                        
                                                <td>175.71</td>
                      
                        
                                                <td>237.04</td>
                      
                        
                                                <td>315.64</td>
                      
                        
                                                <td>384.27</td>
                      
                        
                                                <td>111.11</td>
                      
                        
                                                <td>167.99</td>
                      
                        
                                                <td>225.31</td>
                      
                        
                                                <td>274.46</td>
                      
                        
                                                <td>323.94</td>
                      
                        
                                                <td>393.05</td>
                      
                        
                                                <td>96.24</td>
                      
                        
                                                <td>142.09</td>
                      
                        
                                                <td>185.89</td>
                      
                        
                                                <td>253.22</td>
                      
                        
                                                <td>292.07</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>88.82</td>
                      
                        
                                                <td>86.57</td>
                      
                        
                                                <td>231.30</td>
                      
                        
                                                <td>288.27</td>
                      
                        
                                                <td>85.79</td>
                      
                        
                                                <td>143.45</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>23</td>
                        <td>Hayder Ali khan</td>
                                                <td>104.91</td>
                      
                        
                                                <td>150.84</td>
                      
                        
                                                <td>212.20</td>
                      
                        
                                                <td>290.69</td>
                      
                        
                                                <td>359.34</td>
                      
                        
                                                <td>86.18</td>
                      
                        
                                                <td>143.04</td>
                      
                        
                                                <td>200.42</td>
                      
                        
                                                <td>249.60</td>
                      
                        
                                                <td>299.07</td>
                      
                        
                                                <td>368.14</td>
                      
                        
                                                <td>121.13</td>
                      
                        
                                                <td>166.33</td>
                      
                        
                                                <td>210.61</td>
                      
                        
                                                <td>278.13</td>
                      
                        
                                                <td>317.01</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>113.60</td>
                      
                        
                                                <td>110.33</td>
                      
                        
                                                <td>256.18</td>
                      
                        
                                                <td>313.19</td>
                      
                        
                                                <td>110.39</td>
                      
                        
                                                <td>167.71</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>26</td>
                        <td>MD. FARUK CHOWDHURY</td>
                                                <td>124.53</td>
                      
                        
                                                <td>170.72</td>
                      
                        
                                                <td>231.97</td>
                      
                        
                                                <td>310.60</td>
                      
                        
                                                <td>379.21</td>
                      
                        
                                                <td>106.09</td>
                      
                        
                                                <td>162.92</td>
                      
                        
                                                <td>220.24</td>
                      
                        
                                                <td>269.39</td>
                      
                        
                                                <td>318.87</td>
                      
                        
                                                <td>387.98</td>
                      
                        
                                                <td>101.23</td>
                      
                        
                                                <td>146.85</td>
                      
                        
                                                <td>190.82</td>
                      
                        
                                                <td>258.23</td>
                      
                        
                                                <td>297.13</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>93.76</td>
                      
                        
                                                <td>91.16</td>
                      
                        
                                                <td>236.29</td>
                      
                        
                                                <td>293.34</td>
                      
                        
                                                <td>90.66</td>
                      
                        
                                                <td>148.22</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>29</td>
                        <td>M.S. RASEL</td>
                                                <td>104.95</td>
                      
                        
                                                <td>150.62</td>
                      
                        
                                                <td>211.60</td>
                      
                        
                                                <td>290.31</td>
                      
                        
                                                <td>358.84</td>
                      
                        
                                                <td>85.92</td>
                      
                        
                                                <td>142.55</td>
                      
                        
                                                <td>199.84</td>
                      
                        
                                                <td>249.00</td>
                      
                        
                                                <td>298.48</td>
                      
                        
                                                <td>367.60</td>
                      
                        
                                                <td>121.42</td>
                      
                        
                                                <td>166.30</td>
                      
                        
                                                <td>210.78</td>
                      
                        
                                                <td>278.45</td>
                      
                        
                                                <td>317.50</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>113.83</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>31</td>
                        <td>MD. JAMAL HOSSAIN PATOWARY</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>178.20</td>
                      
                        
                                                <td>235.25</td>
                      
                        
                                                <td>293.54</td>
                      
                        
                                                <td>342.67</td>
                      
                        
                                                <td>391.17</td>
                      
                        
                                                <td>460.31</td>
                      
                        
                                                <td>30.68</td>
                      
                        
                                                <td>80.39</td>
                      
                        
                                                <td>120.40</td>
                      
                        
                                                <td>186.38</td>
                      
                        
                                                <td>250.88</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>34</td>
                        <td>KAZI DILAL UDDIN</td>
                                                <td>124.59</td>
                      
                        
                                                <td>170.77</td>
                      
                        
                                                <td>232.02</td>
                      
                        
                                                <td>310.66</td>
                      
                        
                                                <td>379.26</td>
                      
                        
                                                <td>106.14</td>
                      
                        
                                                <td>162.97</td>
                      
                        
                                                <td>220.28</td>
                      
                        
                                                <td>269.43</td>
                      
                        
                                                <td>318.92</td>
                      
                        
                                                <td>388.02</td>
                      
                        
                                                <td>101.18</td>
                      
                        
                                                <td>146.79</td>
                      
                        
                                                <td>190.76</td>
                      
                        
                                                <td>258.17</td>
                      
                        
                                                <td>297.08</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>91.10</td>
                      
                        
                                                <td>236.24</td>
                      
                        
                                                <td>293.29</td>
                      
                        
                                                <td>90.60</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>35</td>
                        <td>MD. JUWEL HOSSAIN</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>110.15</td>
                      
                        
                                                <td>167.15</td>
                      
                        
                                                <td>224.53</td>
                      
                        
                                                <td>273.70</td>
                      
                        
                                                <td>323.18</td>
                      
                        
                                                <td>392.25</td>
                      
                        
                                                <td>97.25</td>
                      
                        
                                                <td>143.32</td>
                      
                        
                                                <td>186.97</td>
                      
                        
                                                <td>254.18</td>
                      
                        
                                                <td>318.86</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>36</td>
                        <td>MD . JAKIR HOSSAIN (BABUL)</td>
                                                <td>106.38</td>
                      
                        
                                                <td>154.34</td>
                      
                        
                                                <td>206.14</td>
                      
                        
                                                <td>256.45</td>
                      
                        
                                                <td>297.99</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>105.90</td>
                      
                        
                                                <td>151.42</td>
                      
                        
                                                <td>195.46</td>
                      
                        
                                                <td>262.90</td>
                      
                        
                                                <td>327.80</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>39</td>
                        <td>MOHAMMAD RASEL</td>
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>97.88</td>
                      
                        
                                                <td>154.61</td>
                      
                        
                                                <td>211.90</td>
                      
                        
                                                <td>261.05</td>
                      
                        
                                                <td>310.53</td>
                      
                        
                                                <td>379.65</td>
                      
                        
                                                <td>109.43</td>
                      
                        
                                                <td>154.66</td>
                      
                        
                                                <td>198.89</td>
                      
                        
                                                <td>266.45</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>101.89</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>40</td>
                        <td>Syful Karim Munna</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>86.19</td>
                      
                        
                                                <td>142.89</td>
                      
                        
                                                <td>200.21</td>
                      
                        
                                                <td>249.37</td>
                      
                        
                                                <td>298.85</td>
                      
                        
                                                <td>367.95</td>
                      
                        
                                                <td>121.13</td>
                      
                        
                                                <td>166.10</td>
                      
                        
                                                <td>210.53</td>
                      
                        
                                                <td>278.16</td>
                      
                        
                                                <td>343.18</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>41</td>
                        <td>RUBEL KHAN</td>
                                                <td>107.10</td>
                      
                        
                                                <td>152.00</td>
                      
                        
                                                <td>211.80</td>
                      
                        
                                                <td>291.16</td>
                      
                        
                                                <td>359.34</td>
                      
                        
                                                <td>87.30</td>
                      
                        
                                                <td>143.16</td>
                      
                        
                                                <td>200.14</td>
                      
                        
                                                <td>249.20</td>
                      
                        
                                                <td>298.72</td>
                      
                        
                                                <td>367.95</td>
                      
                        
                                                <td>120.61</td>
                      
                        
                                                <td>164.52</td>
                      
                        
                                                <td>209.51</td>
                      
                        
                                                <td>277.59</td>
                      
                        
                                                <td>317.13</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>108.22</td>
                      
                        
                                                <td>255.47</td>
                      
                        
                                                <td>313.46</td>
                      
                        
                                                <td>109.26</td>
                      
                        
                                                <td>165.94</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>42</td>
                        <td>TAROK NATH</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>97.31</td>
                      
                        
                                                <td>143.36</td>
                      
                        
                                                <td>187.03</td>
                      
                        
                                                <td>254.25</td>
                      
                        
                                                <td>318.94</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>43</td>
                        <td>ASHRAFUL ISLAM RAJIB</td>
                                                <td>109.37</td>
                      
                        
                                                <td>155.46</td>
                      
                        
                                                <td>216.93</td>
                      
                        
                                                <td>295.37</td>
                      
                        
                                                <td>364.05</td>
                      
                        
                                                <td>90.83</td>
                      
                        
                                                <td>147.76</td>
                      
                        
                                                <td>205.16</td>
                      
                        
                                                <td>254.34</td>
                      
                        
                                                <td>303.80</td>
                      
                        
                                                <td>372.87</td>
                      
                        
                                                <td>116.49</td>
                      
                        
                                                <td>161.90</td>
                      
                        
                                                <td>206.04</td>
                      
                        
                                                <td>273.48</td>
                      
                        
                                                <td>312.30</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>109.01</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>45</td>
                        <td>ALI JABED SOBUJ</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>81.99</td>
                      
                        
                                                <td>137.46</td>
                      
                        
                                                <td>194.33</td>
                      
                        
                                                <td>243.35</td>
                      
                        
                                                <td>292.88</td>
                      
                        
                                                <td>362.14</td>
                      
                        
                                                <td>126.36</td>
                      
                        
                                                <td>169.85</td>
                      
                        
                                                <td>215.06</td>
                      
                        
                                                <td>283.28</td>
                      
                        
                                                <td>349.32</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>46</td>
                        <td>MD. ARIFUL AZIZ</td>
                                                <td>116.78</td>
                      
                        
                                                <td>164.78</td>
                      
                        
                                                <td>216.81</td>
                      
                        
                                                <td>267.18</td>
                      
                        
                                                <td>308.71</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>90.71</td>
                      
                        
                                                <td>147.53</td>
                      
                        
                                                <td>204.88</td>
                      
                        
                                                <td>254.05</td>
                      
                        
                                                <td>303.52</td>
                      
                        
                                                <td>372.61</td>
                      
                        
                                                <td>116.60</td>
                      
                        
                                                <td>161.83</td>
                      
                        
                                                <td>206.09</td>
                      
                        
                                                <td>273.61</td>
                      
                        
                                                <td>338.51</td>
                      
                        
                                                <td>312.52</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>47</td>
                        <td>MD. DIPU/ MD. PAPPU</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>99.64</td>
                      
                        
                                                <td>156.14</td>
                      
                        
                                                <td>213.33</td>
                      
                        
                                                <td>262.45</td>
                      
                        
                                                <td>311.95</td>
                      
                        
                                                <td>381.12</td>
                      
                        
                                                <td>107.74</td>
                      
                        
                                                <td>152.65</td>
                      
                        
                                                <td>197.06</td>
                      
                        
                                                <td>264.77</td>
                      
                        
                                                <td>330.10</td>
                      
                        
                                                <td>303.95</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>48</td>
                        <td>MD. ALAMGIR SHIKDAR</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>110.68</td>
                      
                        
                                                <td>167.69</td>
                      
                        
                                                <td>225.07</td>
                      
                        
                                                <td>274.24</td>
                      
                        
                                                <td>323.71</td>
                      
                        
                                                <td>392.79</td>
                      
                        
                                                <td>96.73</td>
                      
                        
                                                <td>142.83</td>
                      
                        
                                                <td>186.46</td>
                      
                        
                                                <td>253.66</td>
                      
                        
                                                <td>318.33</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>50</td>
                        <td>MAMUN MOLLAH</td>
                                                <td>119.55</td>
                      
                        
                                                <td>166.49</td>
                      
                        
                                                <td>219.23</td>
                      
                        
                                                <td>270.11</td>
                      
                        
                                                <td>312.08</td>
                      
                        
                                                <td>380.09</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>88.37</td>
                      
                        
                                                <td>144.44</td>
                      
                        
                                                <td>201.49</td>
                      
                        
                                                <td>250.58</td>
                      
                        
                                                <td>300.09</td>
                      
                        
                                                <td>369.30</td>
                      
                        
                                                <td>119.33</td>
                      
                        
                                                <td>163.50</td>
                      
                        
                                                <td>208.36</td>
                      
                        
                                                <td>276.34</td>
                      
                        
                                                <td>342.00</td>
                      
                        
                                                <td>315.76</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>53</td>
                        <td>KAIFEE IFTEKHAR</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>125.80</td>
                      
                        
                                                <td>169.48</td>
                      
                        
                                                <td>214.60</td>
                      
                        
                                                <td>282.75</td>
                      
                        
                                                <td>348.66</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>55</td>
                        <td>Dr. MOHAMMAD ZAKARIA</td>
                                                <td>111.40</td>
                      
                        
                                                <td>159.64</td>
                      
                        
                                                <td>211.48</td>
                      
                        
                                                <td>261.75</td>
                      
                        
                                                <td>303.23</td>
                      
                        
                                                <td>371.18</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>96.10</td>
                      
                        
                                                <td>153.00</td>
                      
                        
                                                <td>210.37</td>
                      
                        
                                                <td>259.54</td>
                      
                        
                                                <td>309.02</td>
                      
                        
                                                <td>378.10</td>
                      
                        
                                                <td>111.23</td>
                      
                        
                                                <td>156.72</td>
                      
                        
                                                <td>200.80</td>
                      
                        
                                                <td>268.22</td>
                      
                        
                                                <td>333.02</td>
                      
                        
                                                <td>307.05</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>56</td>
                        <td>MD. DIDAR HOSSAIN</td>
                                                <td>116.31</td>
                      
                        
                                                <td>162.30</td>
                      
                        
                                                <td>223.43</td>
                      
                        
                                                <td>302.08</td>
                      
                        
                                                <td>370.63</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>109.64</td>
                      
                        
                                                <td>154.90</td>
                      
                        
                                                <td>199.12</td>
                      
                        
                                                <td>266.65</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>99.05</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>98.94</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>57</td>
                        <td>MD. SUMON AHAMED</td>
                                                <td>122.96</td>
                      
                        
                                                <td>170.54</td>
                      
                        
                                                <td>222.88</td>
                      
                        
                                                <td>273.43</td>
                      
                        
                                                <td>315.09</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>84.59</td>
                      
                        
                                                <td>141.19</td>
                      
                        
                                                <td>198.47</td>
                      
                        
                                                <td>247.63</td>
                      
                        
                                                <td>297.11</td>
                      
                        
                                                <td>366.23</td>
                      
                        
                                                <td>122.77</td>
                      
                        
                                                <td>167.58</td>
                      
                        
                                                <td>212.10</td>
                      
                        
                                                <td>279.80</td>
                      
                        
                                                <td>344.92</td>
                      
                        
                                                <td>318.87</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>58</td>
                        <td>SOHEL MADBOR</td>
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>81.45</td>
                      
                        
                                                <td>136.89</td>
                      
                        
                                                <td>193.75</td>
                      
                        
                                                <td>242.78</td>
                      
                        
                                                <td>292.31</td>
                      
                        
                                                <td>361.56</td>
                      
                        
                                                <td>126.94</td>
                      
                        
                                                <td>170.40</td>
                      
                        
                                                <td>215.63</td>
                      
                        
                                                <td>283.86</td>
                      
                        
                                                <td>323.56</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>59</td>
                        <td>MD. SHAHADAT HOSSAIN (SHAMOL)</td>
                                                <td>129.27</td>
                      
                        
                                                <td>175.69</td>
                      
                        
                                                <td>237.23</td>
                      
                        
                                                <td>315.70</td>
                      
                        
                                                <td>384.40</td>
                      
                        
                                                <td>111.11</td>
                      
                        
                                                <td>168.10</td>
                      
                        
                                                <td>225.47</td>
                      
                        
                                                <td>274.64</td>
                      
                        
                                                <td>324.11</td>
                      
                        
                                                <td>393.20</td>
                      
                        
                                                <td>96.29</td>
                      
                        
                                                <td>142.35</td>
                      
                        
                                                <td>186.00</td>
                      
                        
                                                <td>253.22</td>
                      
                        
                                                <td>291.95</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>62</td>
                        <td>MD. TANU GAZI</td>
                                                <td>127.06</td>
                      
                        
                                                <td>172.93</td>
                      
                        
                                                <td>233.59</td>
                      
                        
                                                <td>312.58</td>
                      
                        
                                                <td>380.99</td>
                      
                        
                                                <td>108.23</td>
                      
                        
                                                <td>164.73</td>
                      
                        
                                                <td>221.89</td>
                      
                        
                                                <td>270.99</td>
                      
                        
                                                <td>320.50</td>
                      
                        
                                                <td>389.68</td>
                      
                        
                                                <td>99.13</td>
                      
                        
                                                <td>144.19</td>
                      
                        
                                                <td>188.49</td>
                      
                        
                                                <td>256.16</td>
                      
                        
                                                <td>295.38</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>63</td>
                        <td>AQIL ALAMGIR (HIMU)</td>
                                                <td>112.15</td>
                      
                        
                                                <td>158.46</td>
                      
                        
                                                <td>220.22</td>
                      
                        
                                                <td>298.49</td>
                      
                        
                                                <td>367.26</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>113.50</td>
                      
                        
                                                <td>159.24</td>
                      
                        
                                                <td>203.16</td>
                      
                        
                                                <td>270.44</td>
                      
                        
                                                <td>309.12</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>106.09</td>
                      
                        
                                                <td>103.49</td>
                      
                        
                                                <td>248.56</td>
                      
                        
                                                <td>305.26</td>
                      
                        
                                                <td>103.04</td>
                      
                        
                                                <td>160.61</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>64</td>
                        <td>MD SHAHDAT ISLAM RABBI</td>
                                                <td>111.01</td>
                      
                        
                                                <td>159.27</td>
                      
                        
                                                <td>211.09</td>
                      
                        
                                                <td>261.36</td>
                      
                        
                                                <td>302.83</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>96.51</td>
                      
                        
                                                <td>153.42</td>
                      
                        
                                                <td>210.79</td>
                      
                        
                                                <td>259.96</td>
                      
                        
                                                <td>309.43</td>
                      
                        
                                                <td>378.51</td>
                      
                        
                                                <td>110.82</td>
                      
                        
                                                <td>156.34</td>
                      
                        
                                                <td>200.40</td>
                      
                        
                                                <td>267.81</td>
                      
                        
                                                <td>332.61</td>
                      
                        
                                                <td>306.64</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>67</td>
                        <td>MD.  SARWAR HOSSAIN RUBEL</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>100.62</td>
                      
                        
                                                <td>157.44</td>
                      
                        
                                                <td>214.77</td>
                      
                        
                                                <td>263.92</td>
                      
                        
                                                <td>313.41</td>
                      
                        
                                                <td>382.51</td>
                      
                        
                                                <td>106.70</td>
                      
                        
                                                <td>152.17</td>
                      
                        
                                                <td>196.25</td>
                      
                        
                                                <td>263.70</td>
                      
                        
                                                <td>328.63</td>
                      
                        
                                                <td>302.61</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>68</td>
                        <td>MD. PARVEZ</td>
                                                <td>114.14</td>
                      
                        
                                                <td>159.79</td>
                      
                        
                                                <td>220.45</td>
                      
                        
                                                <td>299.38</td>
                      
                        
                                                <td>367.81</td>
                      
                        
                                                <td>95.08</td>
                      
                        
                                                <td>151.54</td>
                      
                        
                                                <td>208.73</td>
                      
                        
                                                <td>257.85</td>
                      
                        
                                                <td>307.35</td>
                      
                        
                                                <td>376.51</td>
                      
                        
                                                <td>112.32</td>
                      
                        
                                                <td>157.10</td>
                      
                        
                                                <td>201.61</td>
                      
                        
                                                <td>269.35</td>
                      
                        
                                                <td>308.55</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>69</td>
                        <td>MD. BABUL HOSSAIN</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>106.43</td>
                      
                        
                                                <td>151.79</td>
                      
                        
                                                <td>195.93</td>
                      
                        
                                                <td>263.44</td>
                      
                        
                                                <td>328.45</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>76</td>
                        <td>MASUD/MONIR</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>107.43</td>
                      
                        
                                                <td>152.80</td>
                      
                        
                                                <td>196.94</td>
                      
                        
                                                <td>264.44</td>
                      
                        
                                                <td>329.43</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>79</td>
                        <td>ABDUL HANIF</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>112.91</td>
                      
                        
                                                <td>157.50</td>
                      
                        
                                                <td>202.11</td>
                      
                        
                                                <td>269.94</td>
                      
                        
                                                <td>335.40</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>80</td>
                        <td>MD SOHEL MOLLAH TAINA</td>
                                                <td>118.55</td>
                      
                        
                                                <td>164.32</td>
                      
                        
                                                <td>225.03</td>
                      
                        
                                                <td>303.96</td>
                      
                        
                                                <td>372.40</td>
                      
                        
                                                <td>99.62</td>
                      
                        
                                                <td>156.13</td>
                      
                        
                                                <td>213.32</td>
                      
                        
                                                <td>262.44</td>
                      
                        
                                                <td>311.94</td>
                      
                        
                                                <td>381.10</td>
                      
                        
                                                <td>107.75</td>
                      
                        
                                                <td>152.68</td>
                      
                        
                                                <td>197.08</td>
                      
                        
                                                <td>264.79</td>
                      
                        
                                                <td>303.96</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>100.14</td>
                      
                        
                                                <td>96.67</td>
                      
                        
                                                <td>242.77</td>
                      
                        
                                                <td>300.22</td>
                      
                        
                                                <td>96.83</td>
                      
                        
                                                <td>154.07</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>83</td>
                        <td>MD. ABU BAKAR SIDDIK</td>
                                                <td>118.00</td>
                      
                        
                                                <td>163.84</td>
                      
                        
                                                <td>224.71</td>
                      
                        
                                                <td>303.55</td>
                      
                        
                                                <td>372.03</td>
                      
                        
                                                <td>99.15</td>
                      
                        
                                                <td>155.75</td>
                      
                        
                                                <td>212.99</td>
                      
                        
                                                <td>262.12</td>
                      
                        
                                                <td>311.62</td>
                      
                        
                                                <td>380.76</td>
                      
                        
                                                <td>108.18</td>
                      
                        
                                                <td>153.24</td>
                      
                        
                                                <td>197.57</td>
                      
                        
                                                <td>265.21</td>
                      
                        
                                                <td>304.32</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>100.59</td>
                      
                        
                                                <td>97.27</td>
                      
                        
                                                <td>243.22</td>
                      
                        
                                                <td>300.56</td>
                      
                        
                                                <td>97.33</td>
                      
                        
                                                <td>154.63</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>85</td>
                        <td>MD. SOKKUR ALI</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>97.66</td>
                      
                        
                                                <td>154.41</td>
                      
                        
                                                <td>211.71</td>
                      
                        
                                                <td>260.86</td>
                      
                        
                                                <td>310.34</td>
                      
                        
                                                <td>379.46</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>86</td>
                        <td>Md. Faruq Dewan</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>85.98</td>
                      
                        
                                                <td>142.35</td>
                      
                        
                                                <td>199.54</td>
                      
                        
                                                <td>248.67</td>
                      
                        
                                                <td>298.17</td>
                      
                        
                                                <td>367.32</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>88</td>
                        <td>MD. MONIR HOSSAIN</td>
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>110.16</td>
                      
                        
                                                <td>155.50</td>
                      
                        
                                                <td>199.67</td>
                      
                        
                                                <td>267.17</td>
                      
                        
                                                <td>306.12</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>102.65</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>91</td>
                        <td>Tuhin Ahmed</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>86.22</td>
                      
                        
                                                <td>142.59</td>
                      
                        
                                                <td>199.78</td>
                      
                        
                                                <td>248.91</td>
                      
                        
                                                <td>298.41</td>
                      
                        
                                                <td>367.57</td>
                      
                        
                                                <td>121.25</td>
                      
                        
                                                <td>165.79</td>
                      
                        
                                                <td>210.46</td>
                      
                        
                                                <td>278.28</td>
                      
                        
                                                <td>343.64</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>93</td>
                        <td>Md. Islam</td>
                                                <td>104.95</td>
                      
                        
                                                <td>150.55</td>
                      
                        
                                                <td>211.41</td>
                      
                        
                                                <td>290.19</td>
                      
                        
                                                <td>358.69</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>95</td>
                        <td>MD. SAIFUL ISLAM</td>
                                                <td>128.49</td>
                      
                        
                                                <td>174.70</td>
                      
                        
                                                <td>235.90</td>
                      
                        
                                                <td>314.58</td>
                      
                        
                                                <td>383.17</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>97.25</td>
                      
                        
                                                <td>142.92</td>
                      
                        
                                                <td>186.83</td>
                      
                        
                                                <td>254.24</td>
                      
                        
                                                <td>293.18</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>96</td>
                        <td>MD. BADAL</td>
                                                <td>107.04</td>
                      
                        
                                                <td>155.05</td>
                      
                        
                                                <td>206.82</td>
                      
                        
                                                <td>257.09</td>
                      
                        
                                                <td>298.60</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>100.76</td>
                      
                        
                                                <td>157.65</td>
                      
                        
                                                <td>214.99</td>
                      
                        
                                                <td>264.16</td>
                      
                        
                                                <td>313.64</td>
                      
                        
                                                <td>382.73</td>
                      
                        
                                                <td>106.57</td>
                      
                        
                                                <td>152.14</td>
                      
                        
                                                <td>196.15</td>
                      
                        
                                                <td>263.56</td>
                      
                        
                                                <td>328.40</td>
                      
                        
                                                <td>302.41</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>97</td>
                        <td>Md. Mahabubur Rahman Bhuiyan</td>
                                                <td>104.91</td>
                      
                        
                                                <td>150.85</td>
                      
                        
                                                <td>212.21</td>
                      
                        
                                                <td>290.70</td>
                      
                        
                                                <td>359.35</td>
                      
                        
                                                <td>86.20</td>
                      
                        
                                                <td>143.06</td>
                      
                        
                                                <td>200.44</td>
                      
                        
                                                <td>249.61</td>
                      
                        
                                                <td>299.09</td>
                      
                        
                                                <td>368.16</td>
                      
                        
                                                <td>121.12</td>
                      
                        
                                                <td>166.32</td>
                      
                        
                                                <td>210.60</td>
                      
                        
                                                <td>278.12</td>
                      
                        
                                                <td>317.00</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>113.59</td>
                      
                        
                                                <td>110.34</td>
                      
                        
                                                <td>256.17</td>
                      
                        
                                                <td>313.16</td>
                      
                        
                                                <td>110.39</td>
                      
                        
                                                <td>167.72</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>100</td>
                        <td>Md. Rakib Hossain/Md. Rony Hossain</td>
                                                <td>123.02</td>
                      
                        
                                                <td>170.63</td>
                      
                        
                                                <td>222.95</td>
                      
                        
                                                <td>273.49</td>
                      
                        
                                                <td>315.14</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>84.51</td>
                      
                        
                                                <td>141.12</td>
                      
                        
                                                <td>198.41</td>
                      
                        
                                                <td>247.56</td>
                      
                        
                                                <td>297.05</td>
                      
                        
                                                <td>366.16</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>101</td>
                        <td>MD. SOLAIMAN</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>88.78</td>
                      
                        
                                                <td>145.72</td>
                      
                        
                                                <td>203.13</td>
                      
                        
                                                <td>252.31</td>
                      
                        
                                                <td>301.78</td>
                      
                        
                                                <td>370.83</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>102</td>
                        <td>MOKTAR MUNNA</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>91.64</td>
                      
                        
                                                <td>148.44</td>
                      
                        
                                                <td>205.78</td>
                      
                        
                                                <td>254.95</td>
                      
                        
                                                <td>304.43</td>
                      
                        
                                                <td>373.52</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>103</td>
                        <td>A R SOBUJ</td>
                                                <td>124.98</td>
                      
                        
                                                <td>171.31</td>
                      
                        
                                                <td>224.42</td>
                      
                        
                                                <td>275.54</td>
                      
                        
                                                <td>317.71</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>410.96</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>104</td>
                        <td>S.M. REZWAN AHMED</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>90.67</td>
                      
                        
                                                <td>134.68</td>
                      
                        
                                                <td>179.48</td>
                      
                        
                                                <td>247.59</td>
                      
                        
                                                <td>313.85</td>
                      
                        
                                                <td>287.37</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>107</td>
                        <td>Md. Rowshan Hossain Tutul</td>
                                                <td>104.42</td>
                      
                        
                                                <td>150.03</td>
                      
                        
                                                <td>210.92</td>
                      
                        
                                                <td>289.68</td>
                      
                        
                                                <td>358.19</td>
                      
                        
                                                <td>85.32</td>
                      
                        
                                                <td>141.90</td>
                      
                        
                                                <td>199.18</td>
                      
                        
                                                <td>248.33</td>
                      
                        
                                                <td>297.81</td>
                      
                        
                                                <td>366.94</td>
                      
                        
                                                <td>122.04</td>
                      
                        
                                                <td>166.84</td>
                      
                        
                                                <td>211.37</td>
                      
                        
                                                <td>279.07</td>
                      
                        
                                                <td>318.16</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>114.43</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>108</td>
                        <td>MD. APU KHAN</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>109.90</td>
                      
                        
                                                <td>166.89</td>
                      
                        
                                                <td>224.27</td>
                      
                        
                                                <td>273.43</td>
                      
                        
                                                <td>322.91</td>
                      
                        
                                                <td>391.99</td>
                      
                        
                                                <td>97.50</td>
                      
                        
                                                <td>143.53</td>
                      
                        
                                                <td>187.20</td>
                      
                        
                                                <td>254.43</td>
                      
                        
                                                <td>319.13</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>109</td>
                        <td>JT-TUHIN</td>
                                                <td>112.39</td>
                      
                        
                                                <td>160.35</td>
                      
                        
                                                <td>212.40</td>
                      
                        
                                                <td>262.81</td>
                      
                        
                                                <td>304.41</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>95.10</td>
                      
                        
                                                <td>151.85</td>
                      
                        
                                                <td>209.16</td>
                      
                        
                                                <td>258.31</td>
                      
                        
                                                <td>307.80</td>
                      
                        
                                                <td>376.91</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>111</td>
                        <td>MD. SABBIR HOSSAIN TOPU</td>
                                                <td>127.21</td>
                      
                        
                                                <td>173.22</td>
                      
                        
                                                <td>234.12</td>
                      
                        
                                                <td>312.97</td>
                      
                        
                                                <td>381.46</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>112</td>
                        <td>MD. RAJU AHAMED/ MD. SOJIB</td>
                                                <td>116.12</td>
                      
                        
                                                <td>162.10</td>
                      
                        
                                                <td>223.23</td>
                      
                        
                                                <td>301.91</td>
                      
                        
                                                <td>370.48</td>
                      
                        
                                                <td>97.44</td>
                      
                        
                                                <td>154.19</td>
                      
                        
                                                <td>211.49</td>
                      
                        
                                                <td>260.64</td>
                      
                        
                                                <td>310.12</td>
                      
                        
                                                <td>379.24</td>
                      
                        
                                                <td>109.87</td>
                      
                        
                                                <td>155.13</td>
                      
                        
                                                <td>199.35</td>
                      
                        
                                                <td>266.89</td>
                      
                        
                                                <td>305.87</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>102.34</td>
                      
                        
                                                <td>99.23</td>
                      
                        
                                                <td>244.92</td>
                      
                        
                                                <td>302.08</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>156.51</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>114</td>
                        <td>SAIDUR RAHMAN</td>
                                                <td>109.93</td>
                      
                        
                                                <td>157.60</td>
                      
                        
                                                <td>209.62</td>
                      
                        
                                                <td>260.04</td>
                      
                        
                                                <td>301.66</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>117</td>
                        <td>MD. LITON</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>110.83</td>
                      
                        
                                                <td>167.79</td>
                      
                        
                                                <td>225.15</td>
                      
                        
                                                <td>274.32</td>
                      
                        
                                                <td>323.80</td>
                      
                        
                                                <td>392.89</td>
                      
                        
                                                <td>96.55</td>
                      
                        
                                                <td>142.57</td>
                      
                        
                                                <td>186.26</td>
                      
                        
                                                <td>253.50</td>
                      
                        
                                                <td>318.24</td>
                      
                        
                                                <td>292.26</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>119</td>
                        <td>MD. HASAN KHANDOKER</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>89.88</td>
                      
                        
                                                <td>146.99</td>
                      
                        
                                                <td>204.46</td>
                      
                        
                                                <td>253.66</td>
                      
                        
                                                <td>303.11</td>
                      
                        
                                                <td>372.13</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>121</td>
                        <td>Md. Rubel</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>78.23</td>
                      
                        
                                                <td>134.95</td>
                      
                        
                                                <td>192.30</td>
                      
                        
                                                <td>241.48</td>
                      
                        
                                                <td>290.95</td>
                      
                        
                                                <td>360.03</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>123</td>
                        <td>Kazi Imran</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>86.43</td>
                      
                        
                                                <td>143.30</td>
                      
                        
                                                <td>200.68</td>
                      
                        
                                                <td>249.86</td>
                      
                        
                                                <td>299.33</td>
                      
                        
                                                <td>368.40</td>
                      
                        
                                                <td>120.88</td>
                      
                        
                                                <td>166.10</td>
                      
                        
                                                <td>210.38</td>
                      
                        
                                                <td>277.89</td>
                      
                        
                                                <td>342.72</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>124</td>
                        <td>MD. KHAIRUL ISLAM</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>78.02</td>
                      
                        
                                                <td>134.74</td>
                      
                        
                                                <td>192.10</td>
                      
                        
                                                <td>241.28</td>
                      
                        
                                                <td>290.75</td>
                      
                        
                                                <td>359.83</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>125</td>
                        <td>Md. Polash</td>
                                                <td>122.23</td>
                      
                        
                                                <td>170.14</td>
                      
                        
                                                <td>222.25</td>
                      
                        
                                                <td>272.64</td>
                      
                        
                                                <td>314.15</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>122.05</td>
                      
                        
                                                <td>167.19</td>
                      
                        
                                                <td>211.52</td>
                      
                        
                                                <td>279.06</td>
                      
                        
                                                <td>343.94</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>126</td>
                        <td>A K M MAHABUBUR RAHMAN (CHAYON)</td>
                                                <td>109.22</td>
                      
                        
                                                <td>154.39</td>
                      
                        
                                                <td>214.49</td>
                      
                        
                                                <td>293.71</td>
                      
                        
                                                <td>361.97</td>
                      
                        
                                                <td>89.67</td>
                      
                        
                                                <td>145.76</td>
                      
                        
                                                <td>202.81</td>
                      
                        
                                                <td>251.90</td>
                      
                        
                                                <td>301.41</td>
                      
                        
                                                <td>370.62</td>
                      
                        
                                                <td>118.02</td>
                      
                        
                                                <td>162.21</td>
                      
                        
                                                <td>207.05</td>
                      
                        
                                                <td>275.02</td>
                      
                        
                                                <td>314.45</td>
                      
                        
                                                <td>401.77</td>
                      
                        
                                                <td>110.27</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>132</td>
                        <td>Md. Mahfuz Khan</td>
                                                <td>104.82</td>
                      
                        
                                                <td>150.76</td>
                      
                        
                                                <td>212.13</td>
                      
                        
                                                <td>290.62</td>
                      
                        
                                                <td>359.27</td>
                      
                        
                                                <td>86.10</td>
                      
                        
                                                <td>142.97</td>
                      
                        
                                                <td>200.36</td>
                      
                        
                                                <td>249.53</td>
                      
                        
                                                <td>299.00</td>
                      
                        
                                                <td>368.07</td>
                      
                        
                                                <td>121.21</td>
                      
                        
                                                <td>166.41</td>
                      
                        
                                                <td>210.70</td>
                      
                        
                                                <td>278.21</td>
                      
                        
                                                <td>318.07</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>113.68</td>
                      
                        
                                                <td>110.42</td>
                      
                        
                                                <td>256.26</td>
                      
                        
                                                <td>313.26</td>
                      
                        
                                                <td>110.47</td>
                      
                        
                                                <td>167.80</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>133</td>
                        <td>MD. SHARIFUL ISLAM JEWEL</td>
                                                <td>105.64</td>
                      
                        
                                                <td>151.07</td>
                      
                        
                                                <td>211.66</td>
                      
                        
                                                <td>290.60</td>
                      
                        
                                                <td>359.01</td>
                      
                        
                                                <td>86.36</td>
                      
                        
                                                <td>142.75</td>
                      
                        
                                                <td>199.95</td>
                      
                        
                                                <td>249.07</td>
                      
                        
                                                <td>298.57</td>
                      
                        
                                                <td>367.73</td>
                      
                        
                                                <td>121.11</td>
                      
                        
                                                <td>165.67</td>
                      
                        
                                                <td>210.32</td>
                      
                        
                                                <td>278.13</td>
                      
                        
                                                <td>317.35</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>113.44</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>138</td>
                        <td>NILOY</td>
                                                <td>115.45</td>
                      
                        
                                                <td>163.51</td>
                      
                        
                                                <td>215.49</td>
                      
                        
                                                <td>265.84</td>
                      
                        
                                                <td>307.35</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>92.05</td>
                      
                        
                                                <td>148.89</td>
                      
                        
                                                <td>206.25</td>
                      
                        
                                                <td>255.42</td>
                      
                        
                                                <td>304.90</td>
                      
                        
                                                <td>373.98</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>139</td>
                        <td>MAMUN  TONGI</td>
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>81.84</td>
                      
                        
                                                <td>137.33</td>
                      
                        
                                                <td>194.19</td>
                      
                        
                                                <td>243.22</td>
                      
                        
                                                <td>292.75</td>
                      
                        
                                                <td>362.01</td>
                      
                        
                                                <td>126.49</td>
                      
                        
                                                <td>169.99</td>
                      
                        
                                                <td>215.19</td>
                      
                        
                                                <td>283.41</td>
                      
                        
                                                <td>323.10</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>146</td>
                        <td>MD. RIAD UZZAMAN RUBEL</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>97.91</td>
                      
                        
                                                <td>154.64</td>
                      
                        
                                                <td>211.93</td>
                      
                        
                                                <td>261.08</td>
                      
                        
                                                <td>310.57</td>
                      
                        
                                                <td>379.69</td>
                      
                        
                                                <td>109.41</td>
                      
                        
                                                <td>154.66</td>
                      
                        
                                                <td>198.88</td>
                      
                        
                                                <td>266.43</td>
                      
                        
                                                <td>331.47</td>
                      
                        
                                                <td>305.42</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>147</td>
                        <td>MD. SHOEL MOLLAH</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>111.23</td>
                      
                        
                                                <td>168.10</td>
                      
                        
                                                <td>225.42</td>
                      
                        
                                                <td>274.57</td>
                      
                        
                                                <td>324.06</td>
                      
                        
                                                <td>393.17</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>152</td>
                        <td>MD. JASIM UDDIN JITU</td>
                                                <td>106.42</td>
                      
                        
                                                <td>154.28</td>
                      
                        
                                                <td>206.16</td>
                      
                        
                                                <td>256.52</td>
                      
                        
                                                <td>298.10</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>153</td>
                        <td>MD. FARID AHMMAD TITO</td>
                                                <td>108.94</td>
                      
                        
                                                <td>154.08</td>
                      
                        
                                                <td>214.15</td>
                      
                        
                                                <td>293.38</td>
                      
                        
                                                <td>361.64</td>
                      
                        
                                                <td>89.36</td>
                      
                        
                                                <td>145.42</td>
                      
                        
                                                <td>202.47</td>
                      
                        
                                                <td>251.55</td>
                      
                        
                                                <td>301.07</td>
                      
                        
                                                <td>370.28</td>
                      
                        
                                                <td>118.35</td>
                      
                        
                                                <td>162.51</td>
                      
                        
                                                <td>207.36</td>
                      
                        
                                                <td>275.35</td>
                      
                        
                                                <td>314.79</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>110.59</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>154</td>
                        <td>MD. MONIR HOSSAIN/SOHAG</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>96.55</td>
                      
                        
                                                <td>153.46</td>
                      
                        
                                                <td>210.83</td>
                      
                        
                                                <td>260.00</td>
                      
                        
                                                <td>309.47</td>
                      
                        
                                                <td>378.55</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>156</td>
                        <td>SOHEL RANA</td>
                                                <td>97.85</td>
                      
                        
                                                <td>143.39</td>
                      
                        
                                                <td>204.41</td>
                      
                        
                                                <td>283.07</td>
                      
                        
                                                <td>351.62</td>
                      
                        
                                                <td>78.69</td>
                      
                        
                                                <td>135.33</td>
                      
                        
                                                <td>192.66</td>
                      
                        
                                                <td>241.82</td>
                      
                        
                                                <td>291.30</td>
                      
                        
                                                <td>360.39</td>
                      
                        
                                                <td>128.67</td>
                      
                        
                                                <td>173.45</td>
                      
                        
                                                <td>218.00</td>
                      
                        
                                                <td>285.70</td>
                      
                        
                                                <td>324.73</td>
                      
                        
                                                <td>411.73</td>
                      
                        
                                                <td>121.06</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>157</td>
                        <td>MD. ABDULLAH-AL-MASUD (ROBIN)</td>
                                                <td>121.24</td>
                      
                        
                                                <td>169.23</td>
                      
                        
                                                <td>221.28</td>
                      
                        
                                                <td>271.62</td>
                      
                        
                                                <td>313.10</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>158</td>
                        <td>MD SAFIQ DEWAN</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>85.88</td>
                      
                        
                                                <td>142.27</td>
                      
                        
                                                <td>199.46</td>
                      
                        
                                                <td>248.59</td>
                      
                        
                                                <td>298.09</td>
                      
                        
                                                <td>367.24</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>160</td>
                        <td>SHEIKH MUHAMMAD ALI (SUNNY)</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>100.79</td>
                      
                        
                                                <td>157.41</td>
                      
                        
                                                <td>214.65</td>
                      
                        
                                                <td>263.78</td>
                      
                        
                                                <td>313.28</td>
                      
                        
                                                <td>382.42</td>
                      
                        
                                                <td>106.53</td>
                      
                        
                                                <td>151.66</td>
                      
                        
                                                <td>195.94</td>
                      
                        
                                                <td>263.56</td>
                      
                        
                                                <td>328.75</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>161</td>
                        <td>MD. ALAMIN SARKAR</td>
                                                <td>137.49</td>
                      
                        
                                                <td>183.94</td>
                      
                        
                                                <td>245.37</td>
                      
                        
                                                <td>323.94</td>
                      
                        
                                                <td>392.59</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>88.03</td>
                      
                        
                                                <td>134.27</td>
                      
                        
                                                <td>177.78</td>
                      
                        
                                                <td>244.96</td>
                      
                        
                                                <td>283.75</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>163</td>
                        <td>MD. SHARIFUL ISLAM POLASH</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>97.35</td>
                      
                        
                                                <td>143.28</td>
                      
                        
                                                <td>187.02</td>
                      
                        
                                                <td>254.31</td>
                      
                        
                                                <td>319.10</td>
                      
                        
                                                <td>293.11</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>167</td>
                        <td>MD. OMAR FAROQUE</td>
                                                <td>133.83</td>
                      
                        
                                                <td>179.30</td>
                      
                        
                                                <td>239.14</td>
                      
                        
                                                <td>318.60</td>
                      
                        
                                                <td>386.74</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>93.19</td>
                      
                        
                                                <td>137.43</td>
                      
                        
                                                <td>182.13</td>
                      
                        
                                                <td>250.15</td>
                      
                        
                                                <td>289.80</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>85.38</td>
                      
                        
                                                <td>81.33</td>
                      
                        
                                                <td>228.04</td>
                      
                        
                                                <td>286.19</td>
                      
                        
                                                <td>81.86</td>
                      
                        
                                                <td>138.83</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>168</td>
                        <td>MD. SELIM</td>
                                                <td>133.41</td>
                      
                        
                                                <td>178.86</td>
                      
                        
                                                <td>238.70</td>
                      
                        
                                                <td>318.16</td>
                      
                        
                                                <td>386.30</td>
                      
                        
                                                <td>114.13</td>
                      
                        
                                                <td>170.15</td>
                      
                        
                                                <td>227.07</td>
                      
                        
                                                <td>276.09</td>
                      
                        
                                                <td>325.61</td>
                      
                        
                                                <td>394.88</td>
                      
                        
                                                <td>93.62</td>
                      
                        
                                                <td>137.86</td>
                      
                        
                                                <td>182.56</td>
                      
                        
                                                <td>250.59</td>
                      
                        
                                                <td>290.24</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>81.75</td>
                      
                        
                                                <td>228.48</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>82.30</td>
                      
                        
                                                <td>139.26</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>172</td>
                        <td>SHAHOSH</td>
                                                <td>103.65</td>
                      
                        
                                                <td>149.28</td>
                      
                        
                                                <td>210.24</td>
                      
                        
                                                <td>288.96</td>
                      
                        
                                                <td>357.48</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>122.77</td>
                      
                        
                                                <td>167.61</td>
                      
                        
                                                <td>212.12</td>
                      
                        
                                                <td>279.79</td>
                      
                        
                                                <td>318.86</td>
                      
                        
                                                <td>405.91</td>
                      
                        
                                                <td>115.17</td>
                      
                        
                                                <td>111.50</td>
                      
                        
                                                <td>257.80</td>
                      
                        
                                                <td>315.07</td>
                      
                        
                                                <td>111.86</td>
                      
                        
                                                <td>169.01</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>175</td>
                        <td>ARMAN/JAHID</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>88.79</td>
                      
                        
                                                <td>145.73</td>
                      
                        
                                                <td>203.13</td>
                      
                        
                                                <td>252.32</td>
                      
                        
                                                <td>301.78</td>
                      
                        
                                                <td>370.84</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>178</td>
                        <td>RAFE AND MAMUN</td>
                                                <td>101.65</td>
                      
                        
                                                <td>149.72</td>
                      
                        
                                                <td>201.44</td>
                      
                        
                                                <td>251.73</td>
                      
                        
                                                <td>293.29</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>106.13</td>
                      
                        
                                                <td>162.96</td>
                      
                        
                                                <td>220.28</td>
                      
                        
                                                <td>269.43</td>
                      
                        
                                                <td>318.92</td>
                      
                        
                                                <td>388.03</td>
                      
                        
                                                <td>101.19</td>
                      
                        
                                                <td>146.81</td>
                      
                        
                                                <td>190.77</td>
                      
                        
                                                <td>258.18</td>
                      
                        
                                                <td>323.11</td>
                      
                        
                                                <td>297.08</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>180</td>
                        <td>SOWKAT ISLAM</td>
                                                <td>129.67</td>
                      
                        
                                                <td>177.32</td>
                      
                        
                                                <td>229.63</td>
                      
                        
                                                <td>280.11</td>
                      
                        
                                                <td>321.67</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>77.85</td>
                      
                        
                                                <td>134.58</td>
                      
                        
                                                <td>191.94</td>
                      
                        
                                                <td>241.11</td>
                      
                        
                                                <td>290.58</td>
                      
                        
                                                <td>359.66</td>
                      
                        
                                                <td>129.48</td>
                      
                        
                                                <td>174.35</td>
                      
                        
                                                <td>218.86</td>
                      
                        
                                                <td>286.50</td>
                      
                        
                                                <td>351.46</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>181</td>
                        <td>MD. ASIKUR RAHMAN</td>
                                                <td>105.10</td>
                      
                        
                                                <td>150.59</td>
                      
                        
                                                <td>211.29</td>
                      
                        
                                                <td>290.16</td>
                      
                        
                                                <td>358.61</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>166.19</td>
                      
                        
                                                <td>210.80</td>
                      
                        
                                                <td>278.56</td>
                      
                        
                                                <td>317.74</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>113.90</td>
                      
                        
                                                <td>110.03</td>
                      
                        
                                                <td>256.55</td>
                      
                        
                                                <td>313.98</td>
                      
                        
                                                <td>110.53</td>
                      
                        
                                                <td>167.59</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>182</td>
                        <td>MD. SOHAN MONDOL</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>174.50</td>
                      
                        
                                                <td>219.00</td>
                      
                        
                                                <td>286.62</td>
                      
                        
                                                <td>325.59</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>122.03</td>
                      
                        
                                                <td>118.47</td>
                      
                        
                                                <td>264.76</td>
                      
                        
                                                <td>321.77</td>
                      
                        
                                                <td>118.74</td>
                      
                        
                                                <td>175.90</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>183</td>
                        <td>MD. ALAM SHEKH</td>
                                                <td>119.58</td>
                      
                        
                                                <td>166.52</td>
                      
                        
                                                <td>219.26</td>
                      
                        
                                                <td>270.13</td>
                      
                        
                                                <td>312.10</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>88.34</td>
                      
                        
                                                <td>144.41</td>
                      
                        
                                                <td>201.47</td>
                      
                        
                                                <td>250.56</td>
                      
                        
                                                <td>300.07</td>
                      
                        
                                                <td>369.28</td>
                      
                        
                                                <td>119.35</td>
                      
                        
                                                <td>163.53</td>
                      
                        
                                                <td>208.38</td>
                      
                        
                                                <td>276.36</td>
                      
                        
                                                <td>342.02</td>
                      
                        
                                                <td>315.78</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>184</td>
                        <td>MD. AFZAL HOSSAIN</td>
                                                <td>123.06</td>
                      
                        
                                                <td>170.66</td>
                      
                        
                                                <td>222.99</td>
                      
                        
                                                <td>273.53</td>
                      
                        
                                                <td>315.18</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>188</td>
                        <td>MD. DELOWAR  </td>
                                                <td>103.43</td>
                      
                        
                                                <td>149.04</td>
                      
                        
                                                <td>209.97</td>
                      
                        
                                                <td>288.71</td>
                      
                        
                                                <td>357.23</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>123.02</td>
                      
                        
                                                <td>167.83</td>
                      
                        
                                                <td>212.35</td>
                      
                        
                                                <td>280.05</td>
                      
                        
                                                <td>319.12</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>189</td>
                        <td>MD. RAFIKUL ISLAM</td>
                                                <td>104.92</td>
                      
                        
                                                <td>150.86</td>
                      
                        
                                                <td>212.23</td>
                      
                        
                                                <td>290.72</td>
                      
                        
                                                <td>359.37</td>
                      
                        
                                                <td>86.21</td>
                      
                        
                                                <td>143.08</td>
                      
                        
                                                <td>200.46</td>
                      
                        
                                                <td>249.64</td>
                      
                        
                                                <td>299.11</td>
                      
                        
                                                <td>368.18</td>
                      
                        
                                                <td>121.10</td>
                      
                        
                                                <td>166.31</td>
                      
                        
                                                <td>210.59</td>
                      
                        
                                                <td>278.11</td>
                      
                        
                                                <td>316.98</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>113.58</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>190</td>
                        <td>MD. NAZMUL HUDA NASIR</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>85.88</td>
                      
                        
                                                <td>142.52</td>
                      
                        
                                                <td>199.82</td>
                      
                        
                                                <td>248.98</td>
                      
                        
                                                <td>298.46</td>
                      
                        
                                                <td>367.57</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>192</td>
                        <td>NEAMUL FARUQUE</td>
                                                <td>110.37</td>
                      
                        
                                                <td>156.36</td>
                      
                        
                                                <td>217.65</td>
                      
                        
                                                <td>296.20</td>
                      
                        
                                                <td>364.83</td>
                      
                        
                                                <td>91.70</td>
                      
                        
                                                <td>148.54</td>
                      
                        
                                                <td>205.89</td>
                      
                        
                                                <td>255.05</td>
                      
                        
                                                <td>304.53</td>
                      
                        
                                                <td>373.44</td>
                      
                        
                                                <td>115.60</td>
                      
                        
                                                <td>160.88</td>
                      
                        
                                                <td>205.10</td>
                      
                        
                                                <td>272.61</td>
                      
                        
                                                <td>311.52</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>108.09</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>196</td>
                        <td>MD. SEKANDAR ALI</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>77.52</td>
                      
                        
                                                <td>133.85</td>
                      
                        
                                                <td>191.07</td>
                      
                        
                                                <td>240.21</td>
                      
                        
                                                <td>289.70</td>
                      
                        
                                                <td>358.85</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>197</td>
                        <td>MD. JAHANGIR ALAM</td>
                                                <td>97.86</td>
                      
                        
                                                <td>143.40</td>
                      
                        
                                                <td>204.42</td>
                      
                        
                                                <td>283.08</td>
                      
                        
                                                <td>351.63</td>
                      
                        
                                                <td>78.70</td>
                      
                        
                                                <td>135.34</td>
                      
                        
                                                <td>192.66</td>
                      
                        
                                                <td>241.83</td>
                      
                        
                                                <td>291.31</td>
                      
                        
                                                <td>360.41</td>
                      
                        
                                                <td>128.65</td>
                      
                        
                                                <td>173.43</td>
                      
                        
                                                <td>217.99</td>
                      
                        
                                                <td>285.68</td>
                      
                        
                                                <td>324.71</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>121.05</td>
                      
                        
                                                <td>117.28</td>
                      
                        
                                                <td>263.68</td>
                      
                        
                                                <td>320.92</td>
                      
                        
                                                <td>117.73</td>
                      
                        
                                                <td>174.84</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>200</td>
                        <td>MD. RONY</td>
                                                <td>128.82</td>
                      
                        
                                                <td>175.25</td>
                      
                        
                                                <td>236.81</td>
                      
                        
                                                <td>315.26</td>
                      
                        
                                                <td>383.96</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>96.72</td>
                      
                        
                                                <td>142.82</td>
                      
                        
                                                <td>186.45</td>
                      
                        
                                                <td>253.65</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>201</td>
                        <td>MASUD PARVEZ</td>
                                                <td>112.36</td>
                      
                        
                                                <td>160.34</td>
                      
                        
                                                <td>212.37</td>
                      
                        
                                                <td>262.78</td>
                      
                        
                                                <td>304.37</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>95.13</td>
                      
                        
                                                <td>151.89</td>
                      
                        
                                                <td>209.20</td>
                      
                        
                                                <td>258.35</td>
                      
                        
                                                <td>307.84</td>
                      
                        
                                                <td>376.93</td>
                      
                        
                                                <td>112.17</td>
                      
                        
                                                <td>157.40</td>
                      
                        
                                                <td>201.64</td>
                      
                        
                                                <td>269.19</td>
                      
                        
                                                <td>334.19</td>
                      
                        
                                                <td>308.16</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>204</td>
                        <td>MAHMUDUL HASAN</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>83.92</td>
                      
                        
                                                <td>140.46</td>
                      
                        
                                                <td>197.73</td>
                      
                        
                                                <td>246.88</td>
                      
                        
                                                <td>296.36</td>
                      
                        
                                                <td>365.49</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>210</td>
                        <td>SAHIB-MUHIN LOFT</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>165.03</td>
                      
                        
                                                <td>209.39</td>
                      
                        
                                                <td>276.96</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>109.00</td>
                      
                        
                                                <td>255.00</td>
                      
                        
                                                <td>312.12</td>
                      
                        
                                                <td>109.15</td>
                      
                        
                                                <td>166.42</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>217</td>
                        <td>MOHAMMAD POLOK KHANDOKAR</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>117.52</td>
                      
                        
                                                <td>163.19</td>
                      
                        
                                                <td>207.16</td>
                      
                        
                                                <td>274.47</td>
                      
                        
                                                <td>339.00</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>220</td>
                        <td>MD. SERAJUL ISLAM BHUIYAN</td>
                                                <td>108.27</td>
                      
                        
                                                <td>155.56</td>
                      
                        
                                                <td>207.83</td>
                      
                        
                                                <td>258.44</td>
                      
                        
                                                <td>300.24</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>99.65</td>
                      
                        
                                                <td>156.13</td>
                      
                        
                                                <td>213.31</td>
                      
                        
                                                <td>262.43</td>
                      
                        
                                                <td>311.93</td>
                      
                        
                                                <td>381.10</td>
                      
                        
                                                <td>107.73</td>
                      
                        
                                                <td>152.61</td>
                      
                        
                                                <td>197.04</td>
                      
                        
                                                <td>264.77</td>
                      
                        
                                                <td>330.12</td>
                      
                        
                                                <td>303.97</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>221</td>
                        <td>MD. ABADUR RAHMAN</td>
                                                <td>108.13</td>
                      
                        
                                                <td>155.45</td>
                      
                        
                                                <td>207.69</td>
                      
                        
                                                <td>258.30</td>
                      
                        
                                                <td>300.09</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>99.78</td>
                      
                        
                                                <td>156.28</td>
                      
                        
                                                <td>213.46</td>
                      
                        
                                                <td>262.58</td>
                      
                        
                                                <td>312.08</td>
                      
                        
                                                <td>381.25</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>222</td>
                        <td>KAZI RIYAZ</td>
                                                <td>107.72</td>
                      
                        
                                                <td>153.85</td>
                      
                        
                                                <td>215.44</td>
                      
                        
                                                <td>293.80</td>
                      
                        
                                                <td>362.52</td>
                      
                        
                                                <td>89.23</td>
                      
                        
                                                <td>146.23</td>
                      
                        
                                                <td>203.65</td>
                      
                        
                                                <td>252.84</td>
                      
                        
                                                <td>302.29</td>
                      
                        
                                                <td>371.34</td>
                      
                        
                                                <td>118.09</td>
                      
                        
                                                <td>163.56</td>
                      
                        
                                                <td>207.68</td>
                      
                        
                                                <td>275.07</td>
                      
                        
                                                <td>313.85</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>110.63</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>224</td>
                        <td>MD. SARAJ HAWLADAR</td>
                                                <td>127.43</td>
                      
                        
                                                <td>175.04</td>
                      
                        
                                                <td>239.01</td>
                      
                        
                                                <td>315.70</td>
                      
                        
                                                <td>385.16</td>
                      
                        
                                                <td>111.41</td>
                      
                        
                                                <td>169.33</td>
                      
                        
                                                <td>227.1</td>
                      
                        
                                                <td>276.33</td>
                      
                        
                                                <td>325.61</td>
                      
                        
                                                <td>394.31</td>
                      
                        
                                                <td>99.34</td>
                      
                        
                                                <td>147.61</td>
                      
                        
                                                <td>189.28</td>
                      
                        
                                                <td>254.94</td>
                      
                        
                                                <td>292.14</td>
                      
                        
                                                <td>377.96</td>
                      
                        
                                                <td>92.82</td>
                      
                        
                                                <td>94.01</td>
                      
                        
                                                <td>233.50</td>
                      
                        
                                                <td>287.98</td>
                      
                        
                                                <td>90.78</td>
                      
                        
                                                <td>148.86</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>225</td>
                        <td>MD. ABU FISHAL</td>
                                                <td>103.59</td>
                      
                        
                                                <td>153.55</td>
                      
                        
                                                <td>203.59</td>
                      
                        
                                                <td>252.68</td>
                      
                        
                                                <td>293.21</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>226</td>
                        <td>ASHRAF SIDDIQUE</td>
                                                <td>105.45</td>
                      
                        
                                                <td>156.55</td>
                      
                        
                                                <td>205.10</td>
                      
                        
                                                <td>253.14</td>
                      
                        
                                                <td>292.80</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>229</td>
                        <td>MD. MASUM RANA</td>
                                                <td>123.02</td>
                      
                        
                                                <td>169.38</td>
                      
                        
                                                <td>222.47</td>
                      
                        
                                                <td>273.58</td>
                      
                        
                                                <td>315.75</td>
                      
                        
                                                <td>383.79</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>85.45</td>
                      
                        
                                                <td>141.03</td>
                      
                        
                                                <td>197.92</td>
                      
                        
                                                <td>246.95</td>
                      
                        
                                                <td>296.47</td>
                      
                        
                                                <td>365.73</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>232</td>
                        <td>MAHABABUL HAQ SHAMOL</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>122.38</td>
                      
                        
                                                <td>165.98</td>
                      
                        
                                                <td>211.13</td>
                      
                        
                                                <td>279.31</td>
                      
                        
                                                <td>345.33</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>233</td>
                        <td>MD. AL AMIN</td>
                                                <td>122.50</td>
                      
                        
                                                <td>168.85</td>
                      
                        
                                                <td>221.94</td>
                      
                        
                                                <td>273.06</td>
                      
                        
                                                <td>315.24</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>85.97</td>
                      
                        
                                                <td>141.55</td>
                      
                        
                                                <td>198.42</td>
                      
                        
                                                <td>247.45</td>
                      
                        
                                                <td>296.97</td>
                      
                        
                                                <td>366.23</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>235</td>
                        <td>MD MOFIZ UDDIN</td>
                                                <td>121.05</td>
                      
                        
                                                <td>169.01</td>
                      
                        
                                                <td>221.08</td>
                      
                        
                                                <td>271.44</td>
                      
                        
                                                <td>312.95</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>86.44</td>
                      
                        
                                                <td>143.29</td>
                      
                        
                                                <td>200.67</td>
                      
                        
                                                <td>249.84</td>
                      
                        
                                                <td>299.31</td>
                      
                        
                                                <td>368.39</td>
                      
                        
                                                <td>120.87</td>
                      
                        
                                                <td>166.06</td>
                      
                        
                                                <td>210.35</td>
                      
                        
                                                <td>277.88</td>
                      
                        
                                                <td>342.73</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>236</td>
                        <td>MD. FARUK</td>
                                                <td>129.67</td>
                      
                        
                                                <td>177.29</td>
                      
                        
                                                <td>229.62</td>
                      
                        
                                                <td>280.11</td>
                      
                        
                                                <td>321.68</td>
                      
                        
                                                <td>389.64</td>
                      
                        
                                                <td>412.59</td>
                      
                        
                                                <td>77.84</td>
                      
                        
                                                <td>134.55</td>
                      
                        
                                                <td>191.90</td>
                      
                        
                                                <td>241.07</td>
                      
                        
                                                <td>290.55</td>
                      
                        
                                                <td>359.63</td>
                      
                        
                                                <td>129.49</td>
                      
                        
                                                <td>174.33</td>
                      
                        
                                                <td>218.85</td>
                      
                        
                                                <td>286.51</td>
                      
                        
                                                <td>351.49</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>237</td>
                        <td>MD. SALAUDDIN MAMUN &amp; RAJIB HOSSAIN</td>
                                                <td>105.38</td>
                      
                        
                                                <td>152.42</td>
                      
                        
                                                <td>204.84</td>
                      
                        
                                                <td>255.59</td>
                      
                        
                                                <td>297.52</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>102.66</td>
                      
                        
                                                <td>158.97</td>
                      
                        
                                                <td>216.06</td>
                      
                        
                                                <td>265.14</td>
                      
                        
                                                <td>314.66</td>
                      
                        
                                                <td>383.86</td>
                      
                        
                                                <td>104.83</td>
                      
                        
                                                <td>149.46</td>
                      
                        
                                                <td>194.01</td>
                      
                        
                                                <td>261.85</td>
                      
                        
                                                <td>327.44</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>240</td>
                        <td>MD. SHISHIR PRODHAN</td>
                                                <td>93.63</td>
                      
                        
                                                <td>140.35</td>
                      
                        
                                                <td>192.93</td>
                      
                        
                                                <td>243.89</td>
                      
                        
                                                <td>286.09</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>241</td>
                        <td>MD. ARIF HOSSAIN RIPON</td>
                                                <td>110.96</td>
                      
                        
                                                <td>159.38</td>
                      
                        
                                                <td>210.84</td>
                      
                        
                                                <td>260.85</td>
                      
                        
                                                <td>302.11</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>244</td>
                        <td>A K M KAMRUL ISLAM</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>116.74</td>
                      
                        
                                                <td>161.14</td>
                      
                        
                                                <td>205.87</td>
                      
                        
                                                <td>273.76</td>
                      
                        
                                                <td>339.30</td>
                      
                        
                                                <td>313.10</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>245</td>
                        <td>MD. SELIM</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>97.65</td>
                      
                        
                                                <td>154.7</td>
                      
                        
                                                <td>212.13</td>
                      
                        
                                                <td>261.32</td>
                      
                        
                                                <td>310.78</td>
                      
                        
                                                <td>379.83</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>247</td>
                        <td>SHA ALAM SHIKDER</td>
                                                <td>117.92</td>
                      
                        
                                                <td>163.80</td>
                      
                        
                                                <td>224.71</td>
                      
                        
                                                <td>303.52</td>
                      
                        
                                                <td>372.02</td>
                      
                        
                                                <td>99.11</td>
                      
                        
                                                <td>155.74</td>
                      
                        
                                                <td>212.98</td>
                      
                        
                                                <td>262.11</td>
                      
                        
                                                <td>311.61</td>
                      
                        
                                                <td>380.75</td>
                      
                        
                                                <td>108.21</td>
                      
                        
                                                <td>153.32</td>
                      
                        
                                                <td>197.62</td>
                      
                        
                                                <td>265.24</td>
                      
                        
                                                <td>304.33</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>100.64</td>
                      
                        
                                                <td>97.36</td>
                      
                        
                                                <td>243.26</td>
                      
                        
                                                <td>300.57</td>
                      
                        
                                                <td>97.39</td>
                      
                        
                                                <td>154.70</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>248</td>
                        <td>MARUF/PALASH</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>96.26</td>
                      
                        
                                                <td>153.15</td>
                      
                        
                                                <td>210.51</td>
                      
                        
                                                <td>259.68</td>
                      
                        
                                                <td>309.16</td>
                      
                        
                                                <td>378.24</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>249</td>
                        <td>HAJEE ROSTUM</td>
                                                <td>116.19</td>
                      
                        
                                                <td>163.34</td>
                      
                        
                                                <td>215.94</td>
                      
                        
                                                <td>266.73</td>
                      
                        
                                                <td>308.65</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>91.59</td>
                      
                        
                                                <td>147.81</td>
                      
                        
                                                <td>204.91</td>
                      
                        
                                                <td>254</td>
                      
                        
                                                <td>303.51</td>
                      
                        
                                                <td>372.71</td>
                      
                        
                                                <td>115.97</td>
                      
                        
                                                <td>160.35</td>
                      
                        
                                                <td>205.09</td>
                      
                        
                                                <td>272.99</td>
                      
                        
                                                <td>338.55</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>251</td>
                        <td>IQBAL HABIB</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>92.22</td>
                      
                        
                                                <td>148.7</td>
                      
                        
                                                <td>205.9</td>
                      
                        
                                                <td>255.03</td>
                      
                        
                                                <td>304.53</td>
                      
                        
                                                <td>373.69</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>252</td>
                        <td>MONIRUL ISLAM</td>
                                                <td>115.20</td>
                      
                        
                                                <td>162.74</td>
                      
                        
                                                <td>215.08</td>
                      
                        
                                                <td>265.70</td>
                      
                        
                                                <td>307.45</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>92.38</td>
                      
                        
                                                <td>148.87</td>
                      
                        
                                                <td>206.08</td>
                      
                        
                                                <td>255.21</td>
                      
                        
                                                <td>304.70</td>
                      
                        
                                                <td>373.86</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>254</td>
                        <td>HAZI MD ROKIB UDDIN</td>
                                                <td>114.80</td>
                      
                        
                                                <td>171.89</td>
                      
                        
                                                <td>229.30</td>
                      
                        
                                                <td>278.47</td>
                      
                        
                                                <td>327.94</td>
                      
                        
                                                <td>397.01</td>
                      
                        
                                                <td>92.72</td>
                      
                        
                                                <td>139.13</td>
                      
                        
                                                <td>182.53</td>
                      
                        
                                                <td>249.59</td>
                      
                        
                                                <td>288.19</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>255</td>
                        <td>MD RASHID FATMI</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>91.90</td>
                      
                        
                                                <td>148.53</td>
                      
                        
                                                <td>205.80</td>
                      
                        
                                                <td>254.95</td>
                      
                        
                                                <td>304.44</td>
                      
                        
                                                <td>373.56</td>
                      
                        
                                                <td>115.44</td>
                      
                        
                                                <td>160.42</td>
                      
                        
                                                <td>204.82</td>
                      
                        
                                                <td>272.47</td>
                      
                        
                                                <td>337.59</td>
                      
                        
                                                <td>311.53</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>256</td>
                        <td> ABDULLAH-AL-MAMUN</td>
                                                <td>116.18</td>
                      
                        
                                                <td>163.34</td>
                      
                        
                                                <td>215.93</td>
                      
                        
                                                <td>266.72</td>
                      
                        
                                                <td>308.63</td>
                      
                        
                                                <td>376.64</td>
                      
                        
                                                <td>399.73</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>257</td>
                        <td>MD WASHIM</td>
                                                <td>118.42</td>
                      
                        
                                                <td>164.71</td>
                      
                        
                                                <td>226.28</td>
                      
                        
                                                <td>304.69</td>
                      
                        
                                                <td>373.40</td>
                      
                        
                                                <td>100.11</td>
                      
                        
                                                <td>157.11</td>
                      
                        
                                                <td>214.50</td>
                      
                        
                                                <td>263.68</td>
                      
                        
                                                <td>313.15</td>
                      
                        
                                                <td>382.21</td>
                      
                        
                                                <td>107.24</td>
                      
                        
                                                <td>153.00</td>
                      
                        
                                                <td>196.89</td>
                      
                        
                                                <td>264.20</td>
                      
                        
                                                <td>302.96</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>259</td>
                        <td>MUHAMMED MAMUN NASIR</td>
                                                <td>113.66</td>
                      
                        
                                                <td>159.92</td>
                      
                        
                                                <td>221.57</td>
                      
                        
                                                <td>299.92</td>
                      
                        
                                                <td>368.66</td>
                      
                        
                                                <td>95.32</td>
                      
                        
                                                <td>152.35</td>
                      
                        
                                                <td>209.77</td>
                      
                        
                                                <td>258.96</td>
                      
                        
                                                <td>308.42</td>
                      
                        
                                                <td>377.47</td>
                      
                        
                                                <td>112.03</td>
                      
                        
                                                <td>157.71</td>
                      
                        
                                                <td>201.67</td>
                      
                        
                                                <td>268.99</td>
                      
                        
                                                <td>307.71</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>101.95</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>260</td>
                        <td>SYED AMINUL EHSAN (PIAL) &amp; MD MOAZAMMEL HAQUE</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>95.60</td>
                      
                        
                                                <td>152.11</td>
                      
                        
                                                <td>209.31</td>
                      
                        
                                                <td>258.44</td>
                      
                        
                                                <td>307.94</td>
                      
                        
                                                <td>377.09</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>261</td>
                        <td>MD SAHADAT HOSSAIN CHOWDHURY</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>106.61</td>
                      
                        
                                                <td>151.80</td>
                      
                        
                                                <td>196.05</td>
                      
                        
                                                <td>263.63</td>
                      
                        
                                                <td>328.80</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>262</td>
                        <td>MD SHWOKOT ALI</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>95.73</td>
                      
                        
                                                <td>152.63</td>
                      
                        
                                                <td>210.00</td>
                      
                        
                                                <td>259.17</td>
                      
                        
                                                <td>308.64</td>
                      
                        
                                                <td>377.72</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>263</td>
                        <td>MD RIAD</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>97.6</td>
                      
                        
                                                <td>154.47</td>
                      
                        
                                                <td>211.83</td>
                      
                        
                                                <td>261.00</td>
                      
                        
                                                <td>310.47</td>
                      
                        
                                                <td>379.56</td>
                      
                        
                                                <td>109.72</td>
                      
                        
                                                <td>155.21</td>
                      
                        
                                                <td>199.28</td>
                      
                        
                                                <td>266.71</td>
                      
                        
                                                <td>331.56</td>
                      
                        
                                                <td>305.58</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>264</td>
                        <td>MD MAHALAM</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>94.19</td>
                      
                        
                                                <td>150.83</td>
                      
                        
                                                <td>208.1</td>
                      
                        
                                                <td>257.24</td>
                      
                        
                                                <td>306.73</td>
                      
                        
                                                <td>375.86</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>265</td>
                        <td>SK MINHAZUL ISLAM/IMRAN</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>97.59</td>
                      
                        
                                                <td>154.47</td>
                      
                        
                                                <td>211.82</td>
                      
                        
                                                <td>260.99</td>
                      
                        
                                                <td>310.46</td>
                      
                        
                                                <td>379.55</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>266</td>
                        <td>MD AZMOT ALI </td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>95.83</td>
                      
                        
                                                <td>152.29</td>
                      
                        
                                                <td>209.48</td>
                      
                        
                                                <td>258.59</td>
                      
                        
                                                <td>308.1</td>
                      
                        
                                                <td>377.27</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>267</td>
                        <td>HAZI MD QAFIL UDDIN</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>96.56</td>
                      
                        
                                                <td>152.9</td>
                      
                        
                                                <td>210.04</td>
                      
                        
                                                <td>259.15</td>
                      
                        
                                                <td>308.66</td>
                      
                        
                                                <td>377.84</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>268</td>
                        <td>MD HARUN-UR-RASHID/ ALI AKBAR</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>96.64</td>
                      
                        
                                                <td>152.64</td>
                      
                        
                                                <td>209.63</td>
                      
                        
                                                <td>258.68</td>
                      
                        
                                                <td>308.21</td>
                      
                        
                                                <td>377.44</td>
                      
                        
                                                <td>111.13</td>
                      
                        
                                                <td>155.25</td>
                      
                        
                                                <td>200.09</td>
                      
                        
                                                <td>268.12</td>
                      
                        
                                                <td>333.94</td>
                      
                        
                                                <td>307.64</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>269</td>
                        <td>NAZMUL HASAN</td>
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>97.09</td>
                      
                        
                                                <td>152.99</td>
                      
                        
                                                <td>209.93</td>
                      
                        
                                                <td>258.97</td>
                      
                        
                                                <td>308.49</td>
                      
                        
                                                <td>377.75</td>
                      
                        
                                                <td>110.78</td>
                      
                        
                                                <td>154.75</td>
                      
                        
                                                <td>199.67</td>
                      
                        
                                                <td>267.75</td>
                      
                        
                                                <td>307.35</td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>270</td>
                        <td>MD NAHIDUL KARIM/MOSTOFA</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>97.79</td>
                      
                        
                                                <td>154.07</td>
                      
                        
                                                <td>211.16</td>
                      
                        
                                                <td>260.26</td>
                      
                        
                                                <td>309.77</td>
                      
                        
                                                <td>378.97</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>271</td>
                        <td>SHANTO ROY</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>98.13</td>
                      
                        
                                                <td>154.07</td>
                      
                        
                                                <td>211.02</td>
                      
                        
                                                <td>260.07</td>
                      
                        
                                                <td>309.59</td>
                      
                        
                                                <td>378.84</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>272</td>
                        <td>MAKSUDUR RAHMAN</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>108.27</td>
                      
                        
                                                <td>164.82</td>
                      
                        
                                                <td>222.00</td>
                      
                        
                                                <td>271.12</td>
                      
                        
                                                <td>320.62</td>
                      
                        
                                                <td>389.80</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>273</td>
                        <td>MD MAHBUB ALAM CHOWDHURY</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>108.57</td>
                      
                        
                                                <td>165.12</td>
                      
                        
                                                <td>222.31</td>
                      
                        
                                                <td>271.42</td>
                      
                        
                                                <td>320,92</td>
                      
                        
                                                <td>390.10</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>275</td>
                        <td> KAWSAR MAMUN SHUVO</td>
                                                <td>127.64</td>
                      
                        
                                                <td>173.90</td>
                      
                        
                                                <td>235.21</td>
                      
                        
                                                <td>313.82</td>
                      
                        
                                                <td>382.44</td>
                      
                        
                                                <td>109.28</td>
                      
                        
                                                <td>166.15</td>
                      
                        
                                                <td>223.47</td>
                      
                        
                                                <td>272.62</td>
                      
                        
                                                <td>322.10</td>
                      
                        
                                                <td>391.21</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>143.81</td>
                      
                        
                                                <td>187.67</td>
                      
                        
                                                <td>255.02</td>
                      
                        
                                                <td>293.90</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>90.61</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>276</td>
                        <td>Md. Arif Hossain</td>
                                                <td>105.51</td>
                      
                        
                                                <td>151.39</td>
                      
                        
                                                <td>212.65</td>
                      
                        
                                                <td>291.20</td>
                      
                        
                                                <td>359.82</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>120.58</td>
                      
                        
                                                <td>165.72</td>
                      
                        
                                                <td>210.05</td>
                      
                        
                                                <td>277.59</td>
                      
                        
                                                <td>316.53</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>109.70</td>
                      
                        
                                                <td>255.64</td>
                      
                        
                                                <td>312.71</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>277</td>
                        <td> MD  RAFIQ LOFT </td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>98.43</td>
                      
                        
                                                <td>155.5</td>
                      
                        
                                                <td>212.93</td>
                      
                        
                                                <td>262.12</td>
                      
                        
                                                <td>311.58</td>
                      
                        
                                                <td>380.63</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>278</td>
                        <td> Md Sazzad Hossain</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>121.07</td>
                      
                        
                                                <td>166.25</td>
                      
                        
                                                <td>210.55</td>
                      
                        
                                                <td>278.07</td>
                      
                        
                                                <td>342.95</td>
                      
                        
                                                <td>316.98</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>279</td>
                        <td> MOJIBUR RAHMAN</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>100.56</td>
                      
                        
                                                <td>157.37</td>
                      
                        
                                                <td>214.69</td>
                      
                        
                                                <td>263.84</td>
                      
                        
                                                <td>313.33</td>
                      
                        
                                                <td>382.43</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>280</td>
                        <td> ANIS KHAN</td>
                                                <td>116.37</td>
                      
                        
                                                <td>163.83</td>
                      
                        
                                                <td>227.64</td>
                      
                        
                                                <td>304.46</td>
                      
                        
                                                <td>373.85</td>
                      
                        
                                                <td>100.07</td>
                      
                        
                                                <td>157.97</td>
                      
                        
                                                <td>215.73</td>
                      
                        
                                                <td>264.97</td>
                      
                        
                                                <td>314.26</td>
                      
                        
                                                <td>382.97</td>
                      
                        
                                                <td>109.73</td>
                      
                        
                                                <td>157.38</td>
                      
                        
                                                <td>199.72</td>
                      
                        
                                                <td>265.80</td>
                      
                        
                                                <td>303.27</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>102.94</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>281</td>
                        <td> HASAN CHOWDHURY</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>92.38</td>
                      
                        
                                                <td>148.86</td>
                      
                        
                                                <td>206.06</td>
                      
                        
                                                <td>255.19</td>
                      
                        
                                                <td>304.69</td>
                      
                        
                                                <td>373.84</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>282</td>
                        <td> MASBA UDDIN</td>
                                                <td>96.77</td>
                      
                        
                                                <td>142.53</td>
                      
                        
                                                <td>203.89</td>
                      
                        
                                                <td>282.34</td>
                      
                        
                                                <td>351.00</td>
                      
                        
                                                <td>77.86</td>
                      
                        
                                                <td>134.70</td>
                      
                        
                                                <td>192.11</td>
                      
                        
                                                <td>241.29</td>
                      
                        
                                                <td>290.76</td>
                      
                        
                                                <td>359.81</td>
                      
                        
                                                <td>129.46</td>
                      
                        
                                                <td>174.47</td>
                      
                        
                                                <td>218.89</td>
                      
                        
                                                <td>286.47</td>
                      
                        
                                                <td>325.35</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>121.91</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>288</td>
                        <td> Md  Safiqul Islam ( Jitu ) </td>
                                                <td>132.00</td>
                      
                        
                                                <td>178.10</td>
                      
                        
                                                <td>239.03</td>
                      
                        
                                                <td>317.88</td>
                      
                        
                                                <td>386.38</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>93.87</td>
                      
                        
                                                <td>139.35</td>
                      
                        
                                                <td>183.37</td>
                      
                        
                                                <td>250.89</td>
                      
                        
                                                <td>289.98</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>83.68</td>
                      
                        
                                                <td>228.93</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>83.20</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>291</td>
                        <td> Mohammad Shahin Kabir</td>
                                                <td>122.03</td>
                      
                        
                                                <td>168.11</td>
                      
                        
                                                <td>229.26</td>
                      
                        
                                                <td>307.95</td>
                      
                        
                                                <td>376.52</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>103.84</td>
                      
                        
                                                <td>149.28</td>
                      
                        
                                                <td>193.37</td>
                      
                        
                                                <td>260.84</td>
                      
                        
                                                <td>299.82</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>292</td>
                        <td> Mohammed Masud</td>
                                                <td>132.75</td>
                      
                        
                                                <td>178.98</td>
                      
                        
                                                <td>240.13</td>
                      
                        
                                                <td>318.85</td>
                      
                        
                                                <td>387.42</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>92.97</td>
                      
                        
                                                <td>138.72</td>
                      
                        
                                                <td>182.57</td>
                      
                        
                                                <td>249.97</td>
                      
                        
                                                <td>288.93</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>85.51</td>
                      
                        
                                                <td>83.18</td>
                      
                        
                                                <td>228.03</td>
                      
                        
                                                <td>285.15</td>
                      
                        
                                                <td>82.44</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>294</td>
                        <td> Tanvirul Islam</td>
                                                <td>111.77</td>
                      
                        
                                                <td>158.10</td>
                      
                        
                                                <td>219.91</td>
                      
                        
                                                <td>298.15</td>
                      
                        
                                                <td>366.93</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>113.86</td>
                      
                        
                                                <td>159.63</td>
                      
                        
                                                <td>203.53</td>
                      
                        
                                                <td>270.79</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>103.90</td>
                      
                        
                                                <td>248.92</td>
                      
                        
                                                <td>305.58</td>
                      
                        
                                                <td>103.42</td>
                      
                        
                                                <td>161.00</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>297</td>
                        <td> Md Rustam Ali Tutul</td>
                                                <td>99.81</td>
                      
                        
                                                <td>143.80</td>
                      
                        
                                                <td>202.67</td>
                      
                        
                                                <td>282.43</td>
                      
                        
                                                <td>350.34</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>129.59</td>
                      
                        
                                                <td>172.72</td>
                      
                        
                                                <td>218.11</td>
                      
                        
                                                <td>286.45</td>
                      
                        
                                                <td>326.29</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>116.32</td>
                      
                        
                                                <td>264.27</td>
                      
                        
                                                <td>322.69</td>
                      
                        
                                                <td>117.96</td>
                      
                        
                                                <td>174.17</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>300</td>
                        <td>Sheikh Md  Liton </td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>101.41</td>
                      
                        
                                                <td>147.01</td>
                      
                        
                                                <td>190.99</td>
                      
                        
                                                <td>258.40</td>
                      
                        
                                                <td>323.34</td>
                      
                        
                                                <td>297.31</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>301</td>
                        <td> Md Ziyaul Islam (Jewel)</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>101.22</td>
                      
                        
                                                <td>146.85</td>
                      
                        
                                                <td>190.81</td>
                      
                        
                                                <td>258.22</td>
                      
                        
                                                <td>323.15</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>302</td>
                        <td> Tauhidul Islam Hamim</td>
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>99.43</td>
                      
                        
                                                <td>147.15</td>
                      
                        
                                                <td>189.43</td>
                      
                        
                                                <td>255.57</td>
                      
                        
                                                <td>293.20</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>92.66</td>
                      
                        
                                                <td>92.93</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>303</td>
                        <td> Md Robin</td>
                                                <td>126.78</td>
                      
                        
                                                <td>174.09</td>
                      
                        
                                                <td>237.38</td>
                      
                        
                                                <td>314.62</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>99.13</td>
                      
                        
                                                <td>146.80</td>
                      
                        
                                                <td>189.14</td>
                      
                        
                                                <td>255.32</td>
                      
                        
                                                <td>292.99</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>307</td>
                        <td> Mahmudur Rahman</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>93.38</td>
                      
                        
                                                <td>139.87</td>
                      
                        
                                                <td>183.21</td>
                      
                        
                                                <td>250.21</td>
                      
                        
                                                <td>288.77</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>86.14</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>309</td>
                        <td> Md Rubel Hossain</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>121.89</td>
                      
                        
                                                <td>166.79</td>
                      
                        
                                                <td>211.26</td>
                      
                        
                                                <td>278.91</td>
                      
                        
                                                <td>343.99</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>310</td>
                        <td> Md Dipu</td>
                                                <td>97.52</td>
                      
                        
                                                <td>143.04</td>
                      
                        
                                                <td>204.02</td>
                      
                        
                                                <td>282.70</td>
                      
                        
                                                <td>351.28</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>129.02</td>
                      
                        
                                                <td>173.78</td>
                      
                        
                                                <td>218.35</td>
                      
                        
                                                <td>286.05</td>
                      
                        
                                                <td>325.10</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>121.43</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>118.10</td>
                      
                        
                                                <td>175.18</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>311</td>
                        <td> Zahidul Islam</td>
                                                <td>94.03</td>
                      
                        
                                                <td>137.11</td>
                      
                        
                                                <td>195.15</td>
                      
                        
                                                <td>275.22</td>
                      
                        
                                                <td>342.92</td>
                      
                        
                                                <td>72.82</td>
                      
                        
                                                <td>127.09</td>
                      
                        
                                                <td>183.59</td>
                      
                        
                                                <td>232.51</td>
                      
                        
                                                <td>282.04</td>
                      
                        
                                                <td>351.37</td>
                      
                        
                                                <td>137.11</td>
                      
                        
                                                <td>179.67</td>
                      
                        
                                                <td>225.33</td>
                      
                        
                                                <td>293.85</td>
                      
                        
                                                <td>333.90</td>
                      
                        
                                                <td>421.57</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>123.24</td>
                      
                        
                                                <td>271.62</td>
                      
                        
                                                <td>330.34</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>312</td>
                        <td> Md Alauddin</td>
                                                <td>137.74</td>
                      
                        
                                                <td>183.10</td>
                      
                        
                                                <td>242.66</td>
                      
                        
                                                <td>322.28</td>
                      
                        
                                                <td>390.33</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>89.60</td>
                      
                        
                                                <td>133.55</td>
                      
                        
                                                <td>178.37</td>
                      
                        
                                                <td>246.50</td>
                      
                        
                                                <td>286.32</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>314</td>
                        <td> MD  MONOWAR HOSSAIN DIPJOL </td>
                                                <td>95.67</td>
                      
                        
                                                <td>142.35</td>
                      
                        
                                                <td>205.13</td>
                      
                        
                                                <td>282.67</td>
                      
                        
                                                <td>351.74</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>318</td>
                        <td> Md  Sajib Khan </td>
                                                <td>128.31</td>
                      
                        
                                                <td>174.66</td>
                      
                        
                                                <td>236.10</td>
                      
                        
                                                <td>314.63</td>
                      
                        
                                                <td>383.30</td>
                      
                        
                                                <td>110.07</td>
                      
                        
                                                <td>167.00</td>
                      
                        
                                                <td>224.35</td>
                      
                        
                                                <td>273.51</td>
                      
                        
                                                <td>322.99</td>
                      
                        
                                                <td>392.08</td>
                      
                        
                                                <td>97.29</td>
                      
                        
                                                <td>143.23</td>
                      
                        
                                                <td>186.97</td>
                      
                        
                                                <td>254.24</td>
                      
                        
                                                <td>293.05</td>
                      
                        
                                                <td>380.04</td>
                      
                        
                                                <td>89.90</td>
                      
                        
                                                <td>87.73</td>
                      
                        
                                                <td>232.36</td>
                      
                        
                                                <td>289.23</td>
                      
                        
                                                <td>86.90</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>319</td>
                        <td> Md  Atiqur Rahman  </td>
                                                <td>132.79</td>
                      
                        
                                                <td>179.10</td>
                      
                        
                                                <td>240.40</td>
                      
                        
                                                <td>319.03</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>114.49</td>
                      
                        
                                                <td>171.35</td>
                      
                        
                                                <td>228.65</td>
                      
                        
                                                <td>277.80</td>
                      
                        
                                                <td>327.29</td>
                      
                        
                                                <td>396.41</td>
                      
                        
                                                <td>92.84</td>
                      
                        
                                                <td>138.76</td>
                      
                        
                                                <td>182.50</td>
                      
                        
                                                <td>249.81</td>
                      
                        
                                                <td>288.70</td>
                      
                        
                                                <td>375.76</td>
                      
                        
                                                <td>85.43</td>
                      
                        
                                                <td>83.30</td>
                      
                        
                                                <td>227.90</td>
                      
                        
                                                <td>284.90</td>
                      
                        
                                                <td>82.41</td>
                      
                        
                                                <td>140.11</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>321</td>
                        <td> Mohammad Yeasin Arafath</td>
                                                <td>103.40</td>
                      
                        
                                                <td>149.03</td>
                      
                        
                                                <td>209.97</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>84.33</td>
                      
                        
                                                <td>140.94</td>
                      
                        
                                                <td>198.23</td>
                      
                        
                                                <td>247.38</td>
                      
                        
                                                <td>296.87</td>
                      
                        
                                                <td>365.98</td>
                      
                        
                                                <td>123.02</td>
                      
                        
                                                <td>167.86</td>
                      
                        
                                                <td>212.37</td>
                      
                        
                                                <td>280.05</td>
                      
                        
                                                <td>319.12</td>
                      
                        
                                                <td>406.17</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>322</td>
                        <td> Babu</td>
                                                <td>97.37</td>
                      
                        
                                                <td>143.08</td>
                      
                        
                                                <td>204.35</td>
                      
                        
                                                <td>282.86</td>
                      
                        
                                                <td>351.49</td>
                      
                        
                                                <td>78.40</td>
                      
                        
                                                <td>135.20</td>
                      
                        
                                                <td>192.58</td>
                      
                        
                                                <td>241.76</td>
                      
                        
                                                <td>291.22</td>
                      
                        
                                                <td>360.29</td>
                      
                        
                                                <td>128.91</td>
                      
                        
                                                <td>173.88</td>
                      
                        
                                                <td>218.33</td>
                      
                        
                                                <td>285.92</td>
                      
                        
                                                <td>324.86</td>
                      
                        
                                                <td>411.79</td>
                      
                        
                                                <td>121.35</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>323</td>
                        <td> Md Mahfujur Rahman</td>
                                                <td>97.82</td>
                      
                        
                                                <td>154.53</td>
                      
                        
                                                <td>211.81</td>
                      
                        
                                                <td>260.96</td>
                      
                        
                                                <td>310.45</td>
                      
                        
                                                <td>379.57</td>
                      
                        
                                                <td>109.49</td>
                      
                        
                                                <td>154.70</td>
                      
                        
                                                <td>198.95</td>
                      
                        
                                                <td>266.50</td>
                      
                        
                                                <td>305.52</td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>324</td>
                        <td> Shushanto Dash</td>
                                                <td>96.99</td>
                      
                        
                                                <td>153.71</td>
                      
                        
                                                <td>211.01</td>
                      
                        
                                                <td>260.16</td>
                      
                        
                                                <td>309.64</td>
                      
                        
                                                <td>378.76</td>
                      
                        
                                                <td>110.32</td>
                      
                        
                                                <td>155.55</td>
                      
                        
                                                <td>199.79</td>
                      
                        
                                                <td>267.33</td>
                      
                        
                                                <td>306.34</td>
                      
                        
                                                <td>393.40</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>325</td>
                        <td> Md Billal</td>
                                                <td>86.36</td>
                      
                        
                                                <td>142.75</td>
                      
                        
                                                <td>199.95</td>
                      
                        
                                                <td>249.07</td>
                      
                        
                                                <td>298.57</td>
                      
                        
                                                <td>367.73</td>
                      
                        
                                                <td>121.11</td>
                      
                        
                                                <td>165.67</td>
                      
                        
                                                <td>210.32</td>
                      
                        
                                                <td>278.13</td>
                      
                        
                                                <td>317.35</td>
                      
                        
                                                <td>86.36</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>326</td>
                        <td> Md Shamim</td>
                                                <td>128.82</td>
                      
                        
                                                <td>175.71</td>
                      
                        
                                                <td>238.15</td>
                      
                        
                                                <td>316.03</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>96.65</td>
                      
                        
                                                <td>143.62</td>
                      
                        
                                                <td>186.59</td>
                      
                        
                                                <td>253.28</td>
                      
                        
                                                <td>291.50</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>89.58</td>
                      
                        
                                                <td>88.79</td>
                      
                        
                                                <td>231.56</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>86.95</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>327</td>
                        <td> Mazharul Islam Dulal</td>
                                                <td>108.96</td>
                      
                        
                                                <td>154.14</td>
                      
                        
                                                <td>214.26</td>
                      
                        
                                                <td>293.46</td>
                      
                        
                                                <td>361.73</td>
                      
                        
                                                <td>89.41</td>
                      
                        
                                                <td>145.50</td>
                      
                        
                                                <td>202.57</td>
                      
                        
                                                <td>251.65</td>
                      
                        
                                                <td>301.17</td>
                      
                        
                                                <td>370.37</td>
                      
                        
                                                <td>118.26</td>
                      
                        
                                                <td>162.48</td>
                      
                        
                                                <td>207.31</td>
                      
                        
                                                <td>275.26</td>
                      
                        
                                                <td>314.69</td>
                      
                        
                                                <td>402.01</td>
                      
                        
                                                <td>110.52</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>328</td>
                        <td> Hridoy Bhuiyan</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>113.72</td>
                      
                        
                                                <td>170.09</td>
                      
                        
                                                <td>215.28</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>261.34</td>
                      
                        
                                                <td>319.52</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>329</td>
                        <td> Mominul Islam</td>
                                                <td>103.74</td>
                      
                        
                                                <td>149.62</td>
                      
                        
                                                <td>210.93</td>
                      
                        
                                                <td>289.45</td>
                      
                        
                                                <td>358.08</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>122.35</td>
                      
                        
                                                <td>167.49</td>
                      
                        
                                                <td>211.82</td>
                      
                        
                                                <td>279.35</td>
                      
                        
                                                <td>318.27</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>114.82</td>
                      
                        
                                                <td>111.47</td>
                      
                        
                                                <td>257.41</td>
                      
                        
                                                <td>314.45</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>331</td>
                        <td> Moazzem Hossain</td>
                                                <td>93.61</td>
                      
                        
                                                <td>136.79</td>
                      
                        
                                                <td>194.97</td>
                      
                        
                                                <td>274.98</td>
                      
                        
                                                <td>342.72</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>137.29</td>
                      
                        
                                                <td>179.94</td>
                      
                        
                                                <td>225.56</td>
                      
                        
                                                <td>294.05</td>
                      
                        
                                                <td>334.05</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>123.51</td>
                      
                        
                                                <td>271.83</td>
                      
                        
                                                <td>330.48</td>
                      
                        
                                                <td>125.50</td>
                      
                        
                                                <td>181.40</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>338</td>
                        <td> Md  Imran Khan </td>
                                                <td>75.83</td>
                      
                        
                                                <td>123.98</td>
                      
                        
                                                <td>190.06</td>
                      
                        
                                                <td>164.71</td>
                      
                        
                                                <td>334.67</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>339</td>
                        <td> Md Soyeb Hossain</td>
                                                <td>75.88</td>
                      
                        
                                                <td>124.03</td>
                      
                        
                                                <td>190.12</td>
                      
                        
                                                <td>264.76</td>
                      
                        
                                                <td>334.72</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>340</td>
                        <td>Md Jakir Alam</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>75.24</td>
                      
                        
                                                <td>123.27</td>
                      
                        
                                                <td>189.06</td>
                      
                        
                                                <td>364.02</td>
                      
                        
                                                <td>333.90</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>343</td>
                        <td> Md  Younus Miah </td>
                                                <td>99.56</td>
                      
                        
                                                <td>143.53</td>
                      
                        
                                                <td>202.39</td>
                      
                        
                                                <td>282.15</td>
                      
                        
                                                <td>350.06</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>218.39</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>118.24</td>
                      
                        
                                                <td>174.44</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>346</td>
                        <td> Md Zahid Sharif</td>
                                                <td>93.20</td>
                      
                        
                                                <td>139.44</td>
                      
                        
                                                <td>201.62</td>
                      
                        
                                                <td>279.55</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>348</td>
                        <td> MOHAMMAD KHOLILUR RAHMAN</td>
                                                <td>103.83</td>
                      
                        
                                                <td>149.50</td>
                      
                        
                                                <td>210.51</td>
                      
                        
                                                <td>289.20</td>
                      
                        
                                                <td>357.74</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>167.42</td>
                      
                        
                                                <td>211.90</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>111.32</td>
                      
                        
                                                <td>257.57</td>
                      
                        
                                                <td>314.81</td>
                      
                        
                                                <td>111.65</td>
                      
                        
                                                <td>168.82</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>349</td>
                        <td> SHUVRO</td>
                                                <td>105.50</td>
                      
                        
                                                <td>151.17</td>
                      
                        
                                                <td>212.12</td>
                      
                        
                                                <td>290.85</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>350</td>
                        <td> MAMUN TALUKDAR</td>
                                                <td>110.17</td>
                      
                        
                                                <td>156.13</td>
                      
                        
                                                <td>217.38</td>
                      
                        
                                                <td>295.96</td>
                      
                        
                                                <td>364.57</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>161.07</td>
                      
                        
                                                <td>205.32</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>105.11</td>
                      
                        
                                                <td>250.89</td>
                      
                        
                                                <td>307.96</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>351</td>
                        <td> AWLAD HOSSAIN RAZA</td>
                                                <td>106.56</td>
                      
                        
                                                <td>152.68</td>
                      
                        
                                                <td>214.28</td>
                      
                        
                                                <td>292.63</td>
                      
                        
                                                <td>361.35</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>208.84</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>0</td>
                      
                        
                                                <td>166.09</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>355</td>
                        <td> Aminul Ilsam</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>158.78</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>356</td>
                        <td> Md Jasim Uddin ( Jakir)</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>171.19</td>
                      
                        
                                                <td>216.56</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>114.78</td>
                      
                        
                                                <td>262.72</td>
                      
                        
                                                <td>321.15</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>357</td>
                        <td> Saddam Hossain</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>166.86</td>
                      
                        
                                                <td>211.22</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>110.82</td>
                      
                        
                                                <td>256.83</td>
                      
                        
                                                <td>313.92</td>
                      
                        
                                                <td>110.99</td>
                      
                        
                                                <td>168.25</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>358</td>
                        <td> Md Mafizul Islam</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>166.67</td>
                      
                        
                                                <td>211.03</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>110.64</td>
                      
                        
                                                <td>256.64</td>
                      
                        
                                                <td>313.73</td>
                      
                        
                                                <td>110.80</td>
                      
                        
                                                <td>168.06</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>359</td>
                        <td> Yasin</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>158.78</td>
                      
                        
                                                <td>201.95</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>103.59</td>
                      
                        
                                                <td>246.93</td>
                      
                        
                                                <td>302.74</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	                    <tr>
                                            <td>360</td>
                        <td> Md Golamur Rahman (Milon)</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>211.82</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>111.64</td>
                      
                        
                                                <td>169.07</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    	    
                    <tr bgcolor="#cbdcfa">
                                            <td>361</td>
                        <td> Md Farhad Raihan</td>
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>195.21</td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td></td>
                      
                        
                                                <td>95.08</td>
                      
                        
                                                <td>152.66</td>
                      
                        
                                               
                        
                    </tr>
                    
                                    </tbody>
                            </table>
                            </div>
                            </div>
                            </div>
                            </div>
                            </section>

@endsection
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script><!--Note: Data Table. -->
        <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script><!--Note: Data Table. -->

        <script type="text/javascript">
            $(document).ready(function () {
                $('#example3').DataTable();
            });
        </script>
@endsection