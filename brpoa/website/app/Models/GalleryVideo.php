<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryVideo extends Model
{
    protected $table = "gallery_video";

    protected $fillable = [

        'video_file',
        'status',
    ];
}
