<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;

//Models
use App\Models\Customers;
use App\Models\Branches;
use App\Models\Users;
use App\Models\AuctionSchedule;
use App\Models\PigeonInfo;
use Carbon\Carbon;
use Response;
use Artisan;
use Auth;
use DB;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth()->user()->status == 1)
        {   
            $user           = Users::find(1);
            $url            = Http::get(billingApiBaseUrl().'/get-notification/'.$user['billing_id']);

            if($url->status() == 200)
            {
                $response       = Http::get(billingApiBaseUrl().'/get-notification/'.$user['billing_id']); 
                $notification   = json_decode($response);
            }
            else
            {
                $notification   = [];
            }
            
            $user_id = Auth::user()->id;
            
            $total_members              = Users::where('role',4)->count();
            $total_auction              = AuctionSchedule::count();
            $total_pigeon               = PigeonInfo::count();
            $total_member_pigeon        = PigeonInfo::where('participant_id',$user_id)->count();
            
            
            $Balance                = Http::get('https://digishop7.com/brpoa/admin/get-due-balance/'.$user_id);
            $responseBalance        = json_decode($Balance);
            
            $TotalDueBalance                = Http::get('https://digishop7.com/brpoa/admin/get-all-due-balance/');
            $responseTotalDueBalance        = json_decode($TotalDueBalance);
            
            $TotalPayable                = Http::get('https://digishop7.com/brpoa/admin/get-all-payable-balance/');
            $responseTotalPayable        = json_decode($TotalPayable);
            

            return view('home', compact('notification','total_members','total_auction','total_pigeon','total_member_pigeon','responseBalance','responseTotalDueBalance','responseTotalPayable'));
        }
        else
        {
            return back();
        }
    }

    public function dashboardItems()
    {
        $branch_id                  = Auth::user()->branch_id;
        $total_members              = Users::where('role',4)->count();
        
        $data['total_members']      = $total_members;

        return Response::json($data);
    }


}
