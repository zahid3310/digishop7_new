<?php

use Illuminate\Support\Facades\Route;
use App\Models\Users;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

	if (Cache::has('key'))
	{
	   Cache::get('key');
	} 
	else 
	{
	   Cache::put('key', 30, 20);
	}

    return view('auth.login');
});

Route::get('/member-registration-website', function () {
    
    $full_name = $_GET['full_name'];
    $user_name = $_GET['user_name'];
    $email = $_GET['email'];
    $phone = $_GET['phone'];
    $address = $_GET['address'];
    $memberId = $_GET['member_id'];
    
    $users                       = new Users();
    $users->name                 = $full_name;
    $users->email                = $user_name;
    $users->address              = $address;
    $users->contact_number       = $phone;
    $users->contact_email        = $email;
    $users->password             = Hash::make($phone);
    $users->role                 = 4;
    $users->status               = 1;
    $users->branch_id            = 1;
    $users->member_id            = $memberId;
    $users->created_by           = 1;
    $users->save();
    
    return Response::json(1);


});

Route::get('/login-page-info', function () {

	$info = Users::find(1);
    $data['organization_name'] = $info['organization_name'];

    return Response::json($data);
});

// artisan command
Route::get('/clear', function() {
	$exitCode = Artisan::call('view:clear');
	$exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:cache');
    // $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('config:cache');
    return 'Clear and Config All';
});

// Route::get('/login-page-info', 'HomeController@loginPageInfo');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard/items', 'HomeController@dashboardItems')->name('dashboard_items');
