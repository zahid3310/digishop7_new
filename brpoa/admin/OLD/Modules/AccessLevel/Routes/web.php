<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('accesslevel')->group(function() {
    Route::get('/', 'AccessLevelController@index')->name('urls_index');
    Route::post('/update', 'AccessLevelController@update')->name('urls_update');
});

Route::prefix('accesslevel/set-access')->group(function() {
    Route::get('/', 'AccessLevelController@setAccessIndex')->name('set_access_index');
    Route::post('/update', 'AccessLevelController@setAccessUpdate')->name('set_access_update');
    Route::get('/get-module-access', 'AccessLevelController@getModuleAccess')->name('get_module_access');
});