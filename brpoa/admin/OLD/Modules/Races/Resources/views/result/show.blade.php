<!DOCTYPE html>
<html>

<head>
    <title>Race Result</title>
    <link rel="icon" href="{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>   
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Race Result</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Race Spot</th>
                                    <th style="text-align: center">Number of Participant</th>
                                    <th style="text-align: center">Race Date</th>
                                    <th style="text-align: center">Number of Pigaon</th>
                                    <th style="text-align: center">Race Starting time</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $result->spot->name }}</td>
                                    
                                    <td style="text-align: center">{{ $total_participants }}</td>
                                    <td style="text-align: center">{{ date('d-m-Y', strtotime($result->race->date)) }}</td>
                                    <td style="text-align: center">{{ $result_details->count() }}</td>
                                    <td style="text-align: center">{{ date('H:i:s', strtotime($result->race->starting_time)) }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">SL</th>
                                    <th style="text-align: center;width: 20%">Member Name</th>
                                    <th style="text-align: center;width: 10%">Member ID</th>
                                    <th style="text-align: center;width: 10%">Race ID</th>
                                    <th style="text-align: center;width: 10%">BAN ID</th>
                                    <th style="text-align: center;width: 10%">Distance [KM]</th>
                                    <th style="text-align: center;width: 10%">Tripping Time</th>
                                    <th style="text-align: center;width: 10%">Total Time</th>
                                    <th style="text-align: center;width: 15%">Velocity</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                @foreach($result_details as $key => $value)
                                <tr>
                                    <td style="text-align: center">{{ $key + 1 }}</td>
                                    <td style="text-align: left">{{ $value->member->ClientName }}</td>
                                    <td style="text-align: center">{{ $value->member_id }}</td>
                                    <td style="text-align: left">{{ $value->result->race->name }}</td>
                                    <td style="text-align: center">{{ $value->ban_id }}</td>
                                    <td style="text-align: center">{{ number_format($value->distance,2,'.',',') }}</td>
                                    <td style="text-align: center">{{ date('H:i:s', strtotime($value->trapping_time)) }}</td>
                                    <td style="text-align: center">{{ number_format($value->total_time,5,'.',',') }}</td>
                                    <td style="text-align: center">{{ number_format($value->velocity,5,'.',',') }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>
</body>
</html>