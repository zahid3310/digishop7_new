@extends('layouts.app')

@push('styles')
<style type="text/css">
    body[data-sidebar=dark].vertical-collpsed {
        min-height: 0px !important;
    }
    
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }

    .tCenter {
        text-align: center !important;
    }

    .tLeft {
        text-align: left !important;
    }

    .tRight {
        text-align: right !important;
    }
</style>
@endpush

@section('title', 'List of Collection')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Billing</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Billing</a></li>
                                    <li class="breadcrumb-item active">List of Collection</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {!! Session::get('success') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('unsuccess'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! Session::get('unsuccess') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('errors'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! 'Some required fields are missing..!! Please try again..' !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            <div class="card-body table-responsive">
                                <table id="der" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;width: 5%;font-size: 12px">SL</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Payment Date</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Billing Date</th>
                                            <th style="text-align: center;width: 25%;font-size: 12px">Customer</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Paid Through</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Amount</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Action</th>
                                        </tr>
                                    </thead>   
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
            
        var site_url  = $('.site_url').val();

        $('#der').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: site_url + '/payments/load-payments-datatable',
                type: 'GET',
            },
            columns: [
                {data: 'sl', "sClass": "tCenter"},
                {data: 'payment_date', "sClass": "tCenter"},
                {data: 'billing_date', "sClass": "tCenter"},
                {data: 'customer', "sClass": "tLeft"},
                {data: 'paid_through', "sClass": "tLeft"},
                {data: 'amount', "sClass": "tRight"},
                {data: 'action', "sClass": "tCenter"}
            ],
        });
    });
</script>
@endsection