

<?php $__env->startSection('title', 'List of Spots'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">List of Spots</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Races</a></li>
                                    <li class="breadcrumb-item active">List of Spots</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class=col-12>
                        <?php if(Session::has('success')): ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <?php echo Session::get('success'); ?>

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <?php endif; ?>

                        <?php if(Session::has('unsuccess')): ?>
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            <?php echo Session::get('unsuccess'); ?>

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <?php endif; ?>

                        <?php if(Session::has('errors')): ?>
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <?php endif; ?>

                        <div class="card">
                            <div class="card-body table-responsive">

                                <form id="FormSubmit" action="<?php echo e(route('spots_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    <?php echo e(csrf_field()); ?>


                                    <div class="row">
                                        <div class="col-lg-8 col-md-8 col-sm-12 col-12 form-group">
                                            <label for="productname" class="col-form-label">Spot Name *</label>
                                            <input type="text" name="name" class="inner form-control" id="name" placeholder="Spot Name" required />
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                            <label for="productname" class="col-form-label">Spot Position</label>
                                            <input type="text" name="position" class="inner form-control" id="position" placeholder="Spot Position" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('spots_index_all')); ?>">Close</a></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">List of Spots</h4>

                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">SL#</th>
                                            <th style="text-align: center;">Name</th>
                                            <th style="text-align: center;">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php if(!empty($spots) && ($spots->count() > 0)): ?>
                                        <?php $__currentLoopData = $spots; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $spot): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td style="text-align: center"><?php echo e($key + 1); ?></td>
                                                <td style="text-align: left"><?php echo e($spot['name']); ?></td>
                                                <td style="text-align: center">
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="<?php echo e(route('spots_edit', $spot['id'])); ?>">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/brpoa/admin/Modules/Races/Resources/views/spots/index_all.blade.php ENDPATH**/ ?>