

<?php $__env->startPush('styles'); ?>
<style type="text/css">
    body[data-sidebar=dark].vertical-collpsed {
        min-height: 0px !important;
    }
    
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }

    .tCenter {
        text-align: center !important;
    }

    .tLeft {
        text-align: left !important;
    }

    .tRight {
        text-align: right !important;
    }

    div.dataTables_wrapper div.dataTables_filter label {
        font-weight: normal;
        white-space: nowrap;
        text-align: left;
        display: none;
    }
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('title', 'List of Vouchers'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">List of Vouchers</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Accounts</a></li>
                                    <li class="breadcrumb-item active">List of Vouchers</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form method="get" action="<?php echo e(route('list_of_voucher')); ?>" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div style="margin-bottom: 15px;margin-top: 0px" class="form-group row">
                                                <label for="productname" class="col-md-12 col-form-label">From Date</label>
                                                <div class="col-md-12">
                                                    <input id="from_date" name="from_date" type="text" value="<?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                                </div>
                                            </div>
                                        </div>
    
                                        <div class="col-md-3">
                                            <div style="margin-bottom: 15px;margin-top: 0px" class="form-group row">
                                                <label for="productname" class="col-md-12 col-form-label">To Date</label>
                                                <div class="col-md-12">
                                                    <input id="to_date" name="to_date" type="text" value="<?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div style="margin-bottom: 15px;margin-top: 0px" class="form-group row">
                                                <label for="productname" class="col-md-12 col-form-label">Voucher Type</label>
                                                <div class="col-md-12">
                                                    <select style="cursor: pointer" id="type" class="form-control" name="type">
                                                        <option value="0" <?php echo e(isset($_GET['type']) && ($_GET['type'] == 0) ? 'selected' : ''); ?>>All</option>
                                                        <option value="BP" <?php echo e(isset($_GET['type']) && ($_GET['type'] == 'BP') ? 'selected' : ''); ?>>BANK PAYMENT</option>
                                                        <option value="BR" <?php echo e(isset($_GET['type']) && ($_GET['type'] == 'BR') ? 'selected' : ''); ?>>BANK RECEIPT</option>
                                                        <option value="CP" <?php echo e(isset($_GET['type']) && ($_GET['type'] == 'CP') ? 'selected' : ''); ?>>CASH PAYMENT</option>
                                                        <option value="CR" <?php echo e(isset($_GET['type']) && ($_GET['type'] == 'CR') ? 'selected' : ''); ?>>CASH RECEIPT</option>
                                                        <option value="CN" <?php echo e(isset($_GET['type']) && ($_GET['type'] == 'CN') ? 'selected' : ''); ?>>CONTRA</option>
                                                        <option value="JR" <?php echo e(isset($_GET['type']) && ($_GET['type'] == 'JR') ? 'selected' : ''); ?>>JOURNAL</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div style="margin-bottom: 15px;margin-top: 0px" class="form-group row">
                                                <label for="productname" class="col-md-12 col-form-label">Status</label>
                                                <div class="col-md-12">
                                                    <select style="cursor: pointer" id="status_type" class="form-control" name="status">
                                                        <option value="2" <?php echo e(isset($_GET['status']) && ($_GET['status'] == 2) ? 'selected' : ''); ?>>All</option>
                                                        <option value="0" <?php echo e(isset($_GET['status']) && ($_GET['status'] == 0) ? 'selected' : ''); ?>>Unposted</option>
                                                        <option value="1" <?php echo e(isset($_GET['status']) && ($_GET['status'] == 1) ? 'selected' : ''); ?>>Posted</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
    
                                        <div class="col-md-1">
                                            <button style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center;margin-top: 36px" class="form-control" type="submit">
                                                <i class="bx bx-search font-size-24"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>

                                <hr style="margin-top: 0px">

                                <table id="der" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;text-align: center">SL</th>
                                            <th style="width: 10%;text-align: center">Date</th>
                                            <th style="width: 10%;text-align: center">Voucher#</th>
                                            <th style="width: 10%;text-align: center">Money Receipt#</th>
                                            <th style="width: 10%;text-align: center">Voucher Type</th>
                                            <th style="width: 10%;text-align: center">Total Amount</th>
                                            <th style="width: 10%;text-align: center">Status</th>
                                            <th style="width: 10%;text-align: center">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    $( document ).ready(function() {
            
        var site_url    = $('.site_url').val();
        var fromDate    = (new URL(location.href)).searchParams.get('from_date');
        var toDate      = (new URL(location.href)).searchParams.get('to_date');
        var typeData    = (new URL(location.href)).searchParams.get('type');
        var statusData  = (new URL(location.href)).searchParams.get('status');
        
        if (fromDate != null)
        {
            var from_date = fromDate;
        }
        else
        {
            var from_date = formatDate(new Date());
        }
        
        if (toDate != null)
        {
            var to_date = toDate;
        }
        else
        {
            var to_date = formatDate(new Date());
        }

        if (typeData != null)
        {
            var type = typeData;
        }
        else
        {
            var type = 0;
        }

        if (statusData != null)
        {
            var status = statusData;
        }
        else
        {
            var status = 2;
        }

        $('#der').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: site_url + '/voucherPosting/load-vouchers-datatable?from_date=' + from_date + '&to_date=' + to_date + '&type=' + type + '&status=' + status,
                type: 'GET',
            },
            columns: [
                {data: 'sl', "sClass": "tCenter"},
                {data: 'date', "sClass": "tCenter"},
                {data: 'voucher_number', "sClass": "tCenter"},
                {data: 'receipt_number', "sClass": "tCenter"},
                {data: 'voucher_type', "sClass": "tCenter"},
                {data: 'amount', "sClass": "tCenter"},
                {data: 'status', "sClass": "tCenter"},
                {data: 'action', "sClass": "tCenter"}
            ],
        });
    });

    function formatDate(date)
    {
        var d       = new Date(date),
            month   = '' + (d.getMonth() + 1),
            day     = '' + d.getDate(),
            year    = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day, month, year].join('-');
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/brpoa/admin/Modules/Invoices/Resources/views/list_of_voucher.blade.php ENDPATH**/ ?>