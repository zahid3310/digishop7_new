

<?php $__env->startSection('title', 'Chart of Registers'); ?>

<link href="<?php echo e(url('public/tree-plugin/jsTree/style.min.css')); ?>" rel="stylesheet">

<style type="text/css">
    #treePane ul {
        list-style: none !important;
    }

    #treePane .node {
        color: red !important;
        background-image: url("<?php echo e(url('public/tree-plugin/jsTree/plus.gif')); ?>");
        background-position: left center !important;
        background-repeat: no-repeat !important;
        padding-left: 12px !important;
        cursor: pointer !important;
    }

    #treePane .open {
        background-image: url("<?php echo e(url('public/tree-plugin/jsTree/minus.gif')); ?>");
    }

    #treePane ul li {
        background-image: url("<?php echo e(url('public/tree-plugin/jsTree/treeview-default-line.gif')); ?>");
        background-repeat: no-repeat !important;
        padding-left: 20px !important;
        margin-left: -42px !important;
    }

    .custom-menu {
        /*display: none !important;*/
        z-index: 1000 !important;
        position: absolute !important;
        overflow: hidden !important;
        border: 1px solid #CCC !important;
        white-space: nowrap !important;
        font-family: sans-serif !important;
        background: #FFF !important;
        color: #333 !important;
        border-radius: 5px !important;
        padding: 0 !important;
    }

    .custom-menuT {
        /*display: none !important;*/
        z-index: 1000 !important;
        position: absolute !important;
        overflow: hidden !important;
        border: 1px solid #CCC !important;
        white-space: nowrap !important;
        font-family: sans-serif !important;
        background: #FFF !important;
        color: #333 !important;
        border-radius: 5px !important;
        padding: 0 !important;
    }

    /* Each of the items in the list */
    .custom-menu li {
        padding: 8px 12px !important;
        cursor: pointer !important;
        list-style-type: none !important;
    }

    .custom-menuT li {
        padding: 8px 12px !important;
        cursor: pointer !important;
        list-style-type: none !important;
    }

    .custom-menu li:hover {
        background-color: #DEF !important;
    }

    .custom-menuT li:hover {
        background-color: #DEF !important;
    }

    /* TransactableCss */
    .customT-menu {
        display: none !important;
        z-index: 1000 !important;
        position: absolute !important;
        overflow: hidden !important;
        border: 1px solid #CCC !important;
        white-space: nowrap !important;
        font-family: sans-serif !important;
        background: #FFF !important;
        color: #333 !important;
        border-radius: 5px !important;
        padding: 0 !important;
    }

    /* Each of the items in the list */
    .customT-menu li {
        padding: 8px 53px 8px 17px !important;
        cursor: pointer !important;
        list-style-type: none !important;
    }

    .customT-menu li:hover {
        background-color: #DEF !important;
    }

    .modal-content {
        width: 60% !important;
        margin-left: 218px !important;
    }

    .modal-body {
        padding-bottom: 0px !important;
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Chart of Registers</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Accounts</a></li>
                                    <li class="breadcrumb-item active">Chart of Registers</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <div class="row">
                                    <?php function getchild($itemQueryResult, $parentId){ ?>
                                        <ul id="uls_<?php echo $parentId;?>">
                                        <?php foreach($itemQueryResult as $key => $info){
                                            if(($info->PtnID == $parentId)  && ($info->process_status == 0)){

                                                $info->process_status = 1; //processed, If a row use one time next time it will not work

                                                //IsTransactable= 0 = Non-Transactable, 1 = Transactable
                                                if($info->IsTransactable == 0){ ?>
                                                    <li>
                                                        <label id="<?php echo $info->id;?>" class="node" onmousedown="mouseDown(<?php echo $info->id;?>,<?php echo '\''.$info->GroupID.'\'';?>,<?php echo '\''.$info->HeadName.'\'';?>)">

                                                            <?php echo $info->HeadName; ?>
                                                        </label>

                                                        <?php getchild($itemQueryResult, $info->id);?>

                                                    </li><?php  
                                                }
                                                else{?>
                                                    <li id="<?php echo $info->id;?>" onmousedown="mouseDownT(<?php echo $info->id;?>,<?php echo '\''.$info->GroupID.'\'';?>,<?php echo '\''.$info->HeadName.'\'';?>)">

                                                        <?php echo $info->HeadName . ' || ID : '. $info->id; ?>

                                                    </li>
                                                <?php
                                                }
                                            }
                                         } //foreach end
                                        ?>
                                        </ul>
                                    <?php }?>

                                    <div id="treePane">
                                        <ul id="uls_0">
                                            <li>
                                                <?php foreach($itemQueryResult as $key=>$info){?>
                                                    <?php if($info->process_status == 0){ ?>

                                                        <label id="<?php echo $info->id;?>" class="node" onmousedown="mouseDown(<?php echo $info->id;?>,<?php echo '\''.$info->GroupID.'\'';?>,<?php echo '\''.$info->HeadName.'\'';?>)">
                                                            
                                                            <?php echo $info->HeadName;?>
                                                                
                                                        </label>

                                                        <?php
                                                            $info->process_status = 1; //If a row use one time next time it will not work

                                                            getchild($itemQueryResult, $info->id);
                                                    }
                                                }?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>

    <input type="hidden" id="HeadCompanyID" value="<?php echo e(Auth()->user()->branch_id); ?>"/>

    <!--ContextMenuOptions(Non Translatable) Start-->
    <ul class='custom-menu'>
        <!--<li data-toggle="modal"  class="open-myModal4" href="#myModal4"  >New Transactable Item</li>-->
        <li data-toggle="modal" class="open-nonTransactable" href="#nonTransactable" >New Parent/Control Head</li>
        <li data-toggle="modal" class="open-myModal4" href="#myModal4">New Transactable Head</li>
        <hr style="margin:0px;border-color:#ddd">
        <li data-toggle="modal" class="open-EditNonTransactable" href="#EditNonTransactable" >Edit Parent/Control Head</li> 
        <!-- <li data-toggle="modal"  class="open-Edit_Transactable" href="#Edit_Transactable" >Edit Transactable Head</li> -->
        <!-- <hr style="margin:0px;border-color:#ddd"> -->
        <!-- <li data-toggle="modal" class="open-printList" href="#printList">Print Chart of Accounts Head</li> -->
        <!-- <li data-toggle="modal" class="open-printList" href="#printList1">Print Transactable Accounts Head</li> -->
    </ul>
    <!---ContextMenuOptions(Non Translatable) End-->

    <!--ContextMenuOptions(Non Translatable) Start-->
    <!-- <ul class='custom-menuT'>
        <li data-toggle="modal" class="open-myModal4" href="#myModal4">Transactable Head</li>
        <li data-toggle="modal" class="open-printList" href="#printList">Print Chart of Accounts Head</li>
        <li data-toggle="modal" class="open-printList" href="#printList1">Print Transactable Accounts Head</li>
    </ul> -->
    <!---ContextMenuOptions(Non Translatable) End-->
    
    <!--ContextMenuOptions(Translatable) Start-->
    <ul class='custom-menuT'>
        <li data-toggle="modal" class="open-Edit_Transactable" href="#Edit_Transactable">Edit Transactable Head</li>
        <!-- <li data-toggle="modal" id="Edit_bAccount" class="open-Edit_BankAccount" href="#Edit_BankAccount">Edit Bank Account</li> -->
        <!-- <li data-toggle="modal" class="open-printList" href="#printList">Print This List</li> -->
    </ul>
    <!--ContextMenuOptions(Translatable) End-->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script src="<?php echo e(url('public/tree-plugin/jsTree/jstree.min.js')); ?>"></script>

<script type="text/javascript">
    (function() {
        var treePaneNodes = $('#treePane').find(".node");
        treePaneNodes.next("ul").hide();
        $('#treePane').find(".open").next("ul").show();

        treePaneNodes.on('click', function() 
        {
            $(this).next("ul").first().toggle();
            $(this).toggleClass("open");
        })
    })();
</script>

<script type="text/javascript">
    // JAVASCRIPT (jQuery)
    //GlobalVariableDeclaration 
    //DataFromNonTransactable
    var non_ptnID;
    var non_GroupID;
    var non_tra_HeadName;
    var non_Transactable_HeadName;
    var HeadCompanyID   = $('#HeadCompanyID').val();

    //DataFromTransactable//`HeadID``HeadName``PtnID``PtnGroupCode``GroupID``CompanyID``IsTransactable`
    var tra_HeadID;
    // Trigger action when the contexmenu is about to be shown

    function mouseDown(PtnID,GroupID,HeadName) {
        //non_ptnID = "";
        non_ptnID                   = PtnID;
        non_GroupID                 = GroupID;
        non_tra_HeadName            = HeadName;
        non_Transactable_HeadName   = "";
        $('#'+PtnID).bind("contextmenu", function (event) {
        
            // Avoid the real one
            event.preventDefault();
            
            // Show contextmenu
            $(".custom-menu").finish().toggle(100).
            
            // In the right position (the mouse)
            css({
                top: event.pageY-25 + "px",
                left: event.pageX+20 + "px"
            });    
        });
    }

    // If the document is clicked somewhere
    $(document).bind("mousedown", function (e) {
        
        // If the clicked element is not the menu
        if (!$(e.target).parents(".custom-menu").length > 0) {
            
            // Hide it
            $(".custom-menu").hide(100);
        }
        //$(document).click(function(){$(".custom-menu").hide(100);});
    });

    $(document).bind("mousedown", function (e) {
        
        // If the clicked element is not the menu
        if (!$(e.target).parents(".custom-menuT").length > 0) {
            
            // Hide it
            $(".custom-menuT").hide(100);
        }
        //$(document).click(function(){$(".customT-menu").hide(100);});
    });

    // If the menu element is clicked
    $(".custom-menu li").click(function() {
        
        // This is the triggered action name
        switch($(this).attr("data-action")) {
            
            // A case for each action. Your actions here
            case "first": alert(id); break;
            case "second": alert("second"); break;
            case "third": alert("third"); break;
        }
      
        // Hide it AFTER the action was triggered
        $(".custom-menu").hide(100);
    });
      
    //Transanctable
    // Trigger action when the contexmenu is about to be shown
    function mouseDownT(HeadID, PtnID) {
        //alert(value);
        tra_HeadID  = HeadID;
        non_ptnID   = PtnID;

        $('#'+HeadID).bind("contextmenu", function (event) {
            // Avoid the real one
            event.preventDefault();
            
            // Show contextmenu
            $(".custom-menuT").finish().toggle(100).
          
            // In the right position (the mouse)
            css({
                top: event.pageY-25 + "px",
                left: event.pageX+20 + "px"
            });
        });
       //var editBank = $("#uls_33").val();
    }

    // If the document is clicked somewhere
    $(document).bind("mousedown", function (e) {
      
        // If the clicked element is not the menu
        if (!$(e.target).parents(".custom-menuT").length > 0) {
            
            // Hide it
            $(".custom-menuT").hide(100);
        }
        //$(document).click(function(){$(".customT-menu").hide(100);});
    });

    // If the menu element is clicked
    $(".custom-menuT li").click(function() {
    
        // This is the triggered action name
        switch($(this).attr("data-action")) {
            
            // A case for each action. Your actions here
            case "first": alert(id); break;
            case "second": alert("second"); break;
            case "third": alert("third"); break;
        }
      
        // Hide it AFTER the action was triggered
        $(".custom-menuT").hide(100);
    }); 
</script>

<!--TrsansactableModalCodeSection Start-->
    <script type="text/javascript">
        //javaScriptForTransactablevar non_ptnID;   var non_GroupID;
        $(document).on("click", ".open-myModal4", function () {
            document.getElementById("notesmodal").reset();

            $("#new_tra_PtnID").val(non_ptnID);   
            $("#tra_HeadName").val(non_tra_HeadName);

            var traHeadName = non_tra_HeadName;

            if (traHeadName == 'Members') 
            {
                $(".CustomersFields").show();
            }
            else
            {
                $(".CustomersFields").hide();
            }      
        });
    </script>

    <div id="myModal4" class="modal inmodal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">New Transactable Head Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="form-horizontal form_class_new_trans" id="notesmodal" role="form" method="get" enctype="multipart/form-data"> 
                  
                    <div class="modal-body">
                        <input type="hidden" name="BranchID" id="new_tra_HeadCompanyID">
                        <input type="hidden" name="new_tra_PtnID" id="new_tra_PtnID">
                        <input type="hidden" name="is_transactable" id="is_transactable" value="1" class="form-control">
                        <input type="hidden" name="type" id="type" value="1" class="form-control">

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Name *</label>
                            <div class="col-lg-7">
                                <input type="text" name="new_tra_HeadName" id="new_tra_HeadName"  placeholder="Please input Item Name" class="form-control" required>
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Address 1</label>
                            <div class="col-lg-7">
                                <input type="text" name="Address" id="Address" class="form-control">
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Email</label>
                            <div class="col-lg-7">
                                <input type="text" name="clientEmail" id="clientEmail" class="form-control">
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Contact Number</label>
                            <div class="col-md-7">
                                <input type="text" name="ContactNumber" id="ContactNumber" class="form-control"> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">SMS Number</label>
                            <div class="col-md-7">
                                <input type="text" name="smsnumber" id="smsnumber" class="form-control">  
                            </div>
                        </div>

                        <div style="padding: 0px;display: none;" class="form-group row CustomersFields">
                            <label style="text-align: right" class="col-md-5 col-form-label">Bill Type</label>
                            <div class="col-md-7">
                                <select class="form-control" id="BillType" name="BillType">
                                    <option value="0">--Select Type--</option>
                                    <option value="1">Pre Paid</option>
                                    <option value="2">Post Paid</option>
                                </select>
                            </div>
                        </div>

                        <div style="padding: 0px;display: none;" class="form-group row CustomersFields">
                            <label style="text-align: right" class="col-md-5 col-form-label">Bill Amount</label>
                            <div class="col-md-7">
                                <input type="text" name="BillAmount" id="BillAmount" class="form-control"> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Parent Group</label>
                            <div class="col-md-7">
                                <input type="text" id="tra_HeadName"  placeholder="" class="form-control" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Submit</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <script type="text/javascript">
    jQuery(document).ready(function ($) {
        $("#notesmodal").submit(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo route('chart_of_registers_store'); ?>",
                data: $('form.form_class_new_trans').serialize(),
                success: function (insert_id) {
                    // $("#categoryModal").modal('hide');
                    var PtnID       = $('#new_tra_PtnID').val();
                    var nodeName    = $('#new_tra_HeadName').val();
                    document.getElementById("new_tra_HeadName").select();
                    var transactablezType   = 1; //this is only for Transactable Items
                    var ul                  = document.getElementById("uls_"+PtnID);//getting ul's id

                    if(transactablezType == 1)
                    {
                        var li = document.createElement("li");
                            li.setAttribute("id",insert_id);
                            var functoinStr = "mouseDownT("+ insert_id +")";
                            li.setAttribute("onmousedown", functoinStr);
                            li.appendChild(document.createTextNode(nodeName));
                        ul.appendChild(li);
                    }
                    else
                    {
                        var li = document.createElement("li");
                        
                        var itemLabel = document.createElement("Label");
                            //itemLabel.setAttribute("for", nodeName);
                            itemLabel.innerHTML = nodeName;
                            itemLabel.setAttribute("class","node");
                            itemLabel.setAttribute("id",insert_id);
                            var functoinStr = "mouseDown("+ insert_id +")";
                            itemLabel.setAttribute("onmousedown", functoinStr);
                        li.appendChild(itemLabel);
                        var new_ul = document.createElement("ul");
                            new_ul.setAttribute("id","uls_"+insert_id);
                        li.appendChild(new_ul);
                        
                        ul.appendChild(li);
                    }
                },
                error: function () {
                    alert("failure");
                }
            });
            return false;
        });
    });
    </script>
<!---------TrsansactableModalCodeSection End------------>

<!--Edit_Transactable modal Start-->
    <script type="text/javascript">
        //javaScriptForEditTransactable 
        $(document).on("click", ".open-Edit_Transactable", function () 
        {
            document.getElementById("EditTransactableForm").reset();
            
            var site_url   = $(".site_url").val();
            $.getJSON(site_url + "/accounts/chart-of-registers/edit/" + tra_HeadID, function(data)
            {
                $("#editTraHeadID").val(data.id);
                $("#edit_tra_ClientName").val(data.ClientName);
                $("#edit_tra_ClientAddress").val(data.ClientAddress);
                $("#edit_tra_ClientEmail").val(data.ClientEmail);
                $("#edit_tra_ClientContactNumber").val(data.ClientContactNumber);
                $("#editsmsnumber").val(data.SMSNumber);
                $("#BillAmountEdit").val(data.BillAmount);

                if(data.ActiveStatus == 1)
                {
                    $('input:radio[name="ActiveStatuss"]').filter('[value= 1]').attr('checked', true);
                }
                else
                {
                    $('input:radio[name="ActiveStatuss"]').filter('[value= 0]').attr('checked', true);
                }

                $("#BillTypeEdit").val(data.BillType);

                if (data.BillType !== null) 
                {
                    $(".CustomersFields").show();
                }
                else
                {
                    $(".CustomersFields").hide();
                } 
                
            });            
        });
    </script>

    <div id="Edit_Transactable" class="modal inmodal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Edit Transactable Head Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="form-horizontal form_class_transactable_edit" id="EditTransactableForm" role="form" method="GET" enctype="multipart/form-data">

                    <div class="modal-body">
                        <input type="hidden" name="editTraHeadID" id="editTraHeadID">
                        <input type="hidden" name="is_transactable" id="is_transactable" value="1">
                        <input type="hidden" name="type" id="type" value="1">

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Name *</label>

                            <div class="col-lg-7">
                                <input type="text" name="edit_tra_ClientName" id="edit_tra_ClientName"  placeholder="Please input Item Name" class="form-control" required> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Address 1</label>

                            <div class="col-lg-7">
                                <input type="text" name="edit_tra_ClientAddress" id="edit_tra_ClientAddress" class="form-control"> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Email</label>
                            <div class="col-lg-7">
                                <input type="text" name="edit_tra_ClientEmail" id="edit_tra_ClientEmail" class="form-control"> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Contact Number</label>

                            <div class="col-lg-7">
                                <input type="text" name="edit_tra_ClientContactNumber" id="edit_tra_ClientContactNumber" class="form-control">  
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">SMS Number</label>

                            <div class="col-lg-7">
                                <input type="text" name="editsmsnumber" id="editsmsnumber" class="form-control">
                            </div>
                        </div>

                        <div style="padding: 0px;display: none;" class="form-group row CustomersFields">
                            <label style="text-align: right" class="col-md-5 col-form-label">Bill Type</label>
                            <div class="col-md-7">
                                <select class="form-control" id="BillTypeEdit" name="BillTypeEdit">
                                    <option value="0">--Select Type--</option>
                                    <option value="1">Pre Paid</option>
                                    <option value="2">Post Paid</option>
                                </select>
                            </div>
                        </div>

                        <div style="padding: 0px;display: none;" class="form-group row CustomersFields">
                            <label style="text-align: right" class="col-md-5 col-form-label">Bill Amount</label>
                            <div class="col-md-7">
                                <input type="text" name="BillAmountEdit" id="BillAmountEdit" class="form-control"> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Active Status</label>
                            <div class="col-lg-7">
                                <div class="inline-radio"><label> <input type="radio" value="1" name="ActiveStatuss"> Active </label></div>
                                <div class="inline-radio"><label> <input type="radio" value="0" name="ActiveStatuss"> Inactive </label></div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <input type="submit" name="submit" class="btn btn-primary" id="submitnote" value="Update" />
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    jQuery(document).ready(function ($) {
        $("#EditTransactableForm").submit(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo route('chart_of_registers_update'); ?>",
                data: $('form.form_class_transactable_edit').serialize(),
                success: function (insert_id) {
                    $("#Edit_Transactable").modal('hide');

                    var PtnID       = $('#editTraHeadID').val();
                    var nodeName    = $('#edit_tra_ClientName').val();
                    
                    document.getElementById(PtnID).innerHTML = nodeName;
                    
                },
                error: function () {
                    alert("failure");
                }
            });
            return false;
        });
    });
    </script>
<!---------Edit_BankAccount modal End------------>

<!--Non-TrsansactableModalCodeSection Start-->
    <script type="text/javascript">
        $(document).on("click", ".open-nonTransactable", function () {
            document.getElementById("nonTransactableForm").reset();
            
            $("#New_Non_PtnID").val(non_ptnID); 
            $("#Non_Tra_HeadName").val(non_tra_HeadName);

        });
    </script>

    <div id="nonTransactable" class="modal inmodal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">New Non Transactable Head Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="form-horizontal form_class_new_non_trans" id="nonTransactableForm" role="form" method="get" enctype="multipart/form-data">

                    <div class="modal-body">
                        <input type="hidden" id="New_Non_PtnID" name="New_Non_PtnID" value="" />
                        <input type="hidden" name="is_transactable" id="is_transactable" value="0">
                        <input type="hidden" name="type" id="type" value="0">

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Client Group Name</label>
                            <div class="col-lg-7">
                                <input type="text" name="New_Non_ClientName" id="New_Non_ClientName"  placeholder="Please input Client Group Name" class="form-control" required> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Parent Group</label>
                            <div class="col-lg-7">
                                <input type="text" id="Non_Tra_HeadName" class="form-control" readonly> 
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                            <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    jQuery(document).ready(function ($) {
        $("#nonTransactableForm").submit(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo route('chart_of_registers_store'); ?>",
                data: $('form.form_class_new_non_trans').serialize(),
                success: function (insert_id) {
                    // $("#grpCategoryModal").modal('hide');
                    var PtnID       = $('#New_Non_PtnID').val();
                    var nodeName    = $('#New_Non_ClientName').val();
                    document.getElementById("New_Non_ClientName").select();
                    var transactablezType = 0; //this is only for Non_Transactable Items
                    var ul = document.getElementById("uls_"+PtnID);//getting ul's id
                    
                    if(transactablezType == 1)
                    {
                        var li = document.createElement("li");
                            li.setAttribute("id",insert_id);
                            var functoinStr = "mouseDownT("+ insert_id +")";
                            li.setAttribute("onmousedown", functoinStr);
                            li.appendChild(document.createTextNode(nodeName));
                        ul.appendChild(li);
                    }
                    else
                    {
                        var li = document.createElement("li");
                        
                        var itemLabel = document.createElement("Label");
                            //itemLabel.setAttribute("for", nodeName);
                            itemLabel.innerHTML = nodeName;
                            itemLabel.setAttribute("class","node");
                            itemLabel.setAttribute("id",insert_id);
                            var functoinStr = "mouseDown("+ insert_id +")";
                            itemLabel.setAttribute("onmousedown", functoinStr);
                        li.appendChild(itemLabel);
                        var new_ul = document.createElement("ul");
                            new_ul.setAttribute("id","uls_"+insert_id);
                        li.appendChild(new_ul);
                        
                        ul.appendChild(li);
                    }
                },
                error: function () {
                    alert("failure");
                }
            });
            return false;
        });
    });
    </script>
<!---------Non-TrsansactableModalCodeSection End------------>

<!--EditNon-Transactable Start-->
    <script type="text/javascript">
        //javaScriptForEditNonTransactable
        $(document).on("click", ".open-EditNonTransactable", function ()
        {
            document.getElementById("EditNonTransactableForm").reset();

             var site_url   = $(".site_url").val();
             $.getJSON(site_url + "/accounts/chart-of-registers/edit/" + non_ptnID, function(data)
             {
                $("#edit_non_HeadID").val(data.id);
                $("#edit_non_HeadName").val(data.ClientName);

                if(data.ActiveStatus == 1)
                {
                    $('input:radio[name="ActiveStatus"]').filter('[value= 1]').attr('checked', true);
                }
                else
                {
                    $('input:radio[name="ActiveStatus"]').filter('[value= 0]').attr('checked', true);
                }
            });

            $("#new_non_PtnID").val(non_ptnID);        
        }); 
    </script>
        
    <div id="EditNonTransactable" class="modal inmodal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Edit Non Transactable Item form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form class="form-horizontal form_class_edit_trans" id="EditNonTransactableForm" role="form" method="GET" enctype="multipart/form-data">
                    
                    <div class="modal-body">
                        <!--<p>Sign in today for more expirience.</p>-->
                        <input type="hidden" id="edit_non_HeadID" name="edit_non_HeadID" value="" />
                        <input type="hidden" name="type" id="type" value="0" class="form-control">

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Group Category Name</label>
                            <div class="col-lg-7">
                                <input type="text" name="edit_non_HeadName" id="edit_non_HeadName"  placeholder="Please Input Group Category Name" class="form-control" required> 
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group row">
                            <label style="text-align: right" class="col-md-5 col-form-label">Active Status</label>
                            <div class="col-lg-7">
                                <div class="inline-radio"><label> <input type="radio" value="1" name="ActiveStatus"> Active </label></div>
                                <div class="inline-radio"><label> <input type="radio" value="0" name="ActiveStatus"> Inactive </label></div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    jQuery(document).ready(function ($) {
        $("#EditNonTransactableForm").submit(function () {
            $.ajax({
            // alert(Edit_Non_HeadName);
                type: "GET",
                url: "<?php echo route('chart_of_registers_update'); ?>",
                data: $('form.form_class_edit_trans').serialize(),
                success: function (insert_id) {
                    $("#EditNonTransactable").modal('hide');

                    var PtnID       = $('#edit_non_HeadID').val();
                    var nodeName    = $('#edit_non_HeadName').val();
                    
                    document.getElementById(PtnID).innerHTML = nodeName;
                    
                },
                error: function () {
                    alert("failure");
                }
            });
            return false;
        });
    });
    </script>
<!---------EditNon-Transactable End------------>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/brpoa/admin/Modules/Accounts/Resources/views/chart_of_registers/index.blade.php ENDPATH**/ ?>