<!DOCTYPE html>
<html>
<head>
    <title>Current Balance</title>

    <link rel="icon" href="<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/bk_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/bk_assets/css/style.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/bk_assets/css/custom.css')); ?>">

    <script type="text/javascript">
        function printDiv(divName) {
            var printContents       = document.getElementById(divName).innerHTML;
            var originalContents    = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
</head>

<style type="text/css" media="print">
    @page  {
        size: auto;   /* auto is the initial value */
        margin: 15mm 5mm 5mm 10mm;   /* this affects the margin in the printer settings */
    }
</style>

<body id="print-container-body">
    <input style="float:right" type="button" onclick="printDiv('printableArea')" value="Print" />
    <div id="printableArea">
        <div style="display: none;">
            <button id="btnExport">Export to excel</button>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="" style="">
                            <div style="width:20%;">
                                <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                            </div>

                            <div class="company-head" style="text-align: center; min-height: 82px;">
                                <h3><strong><?php echo e(userDetails()->organization_name); ?></strong></h3>
                                <p><?php echo e(userDetails()->address); ?></p>
                                <p><?php echo e(userDetails()->contact_number); ?></p>
                                <p><?php echo e(userDetails()->contact_email); ?></p>
                                <p><?php echo e(userDetails()->website); ?></p>
                                <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>              
                            </div>
                            
                            <div class="ibox-title" style="height:25px;">
                              <h6 style="font-size: 13px; margin-top: 0px;">CURRENT BALANCE</h6>
                            </div>

                            <div class="ibox-content" id="app">
                                <router-view></router-view>
                            </div>

                            <table class="table table-striped table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;">&copy; <?php echo date("Y"); ?> <a style="color: black" target="blank" href="http://www.cyberdynetechnologyltd.com">Cyberdyne Technology Ltd. | Contact: 01715-515755 </a></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="<?php echo e(asset('public/js/app.js')); ?>"></script>
        <script src="<?php echo e(url('public/bk_assets/js/jquery-2.1.1.js')); ?>"></script>
        <script src="<?php echo e(url('public/bk_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
        <script src="<?php echo e(url('public/bk_assets/js/jquery.base64.js')); ?>"></script>

        <script type="text/javascript">
            var isCtrl = false;$(document).keyup(function (e) {
            if(e.which == 17) isCtrl=false;
            }).keydown(function (e) {
                if(e.which == 17) isCtrl=true;
                if(e.which == 69 && isCtrl == true) {
                    $("#tblExport").btechco_excelexport({
                        containerid: "tblExport"
                       , datatype: $datatype.Table
                    });
                    return false;
                }
            });
        </script>
    </div>
</body>
</html><?php /**PATH /home/digishop7/public_html/brpoa/admin/Modules/AccountsReport/Resources/views/currentBalance/index.blade.php ENDPATH**/ ?>