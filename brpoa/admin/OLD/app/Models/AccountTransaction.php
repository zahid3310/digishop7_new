<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class AccountTransaction extends Model
{  
    protected $table = "tbl_acc_accounts_transaction";

    public function headName()
    {
        return $this->belongsTo('App\Models\AccountHead','HeadID');
    }

    public function cbaHeadName()
    {
        return $this->belongsTo('App\Models\AccountHead','CBAccount');
    }

    public function bankName()
    {
        return $this->belongsTo('App\Models\BankAccounts','HeadID');
    }

    public function projectName()
    {
        return $this->belongsTo('App\Models\Projects','ProjectID');
    }

    public function registerName()
    {
        return $this->belongsTo('App\Models\Clients','RegisterID');
    }

    public function yearlyBill()
    {
        return $this->belongsTo('App\Models\BillSummary','invoice_id');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoices','invoice_id');
    }

    public function bill()
    {
        return $this->belongsTo('App\Models\Bills','bill_id');
    }

    public function salesReturn()
    {
        return $this->belongsTo('App\Models\SalesReturn','sales_return_id');
    }

    public function purchaseReturn()
    {
        return $this->belongsTo('App\Models\PurchaseReturn','purchase_return_id');
    }

    public function voucherNumber()
    {
        return $this->belongsTo('App\Models\VoucherSummary','VoucherId');
    }
}
