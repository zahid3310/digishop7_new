<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Results extends Model
{  
    protected $table = "results";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function race()
    {
        return $this->belongsTo('App\Models\Races','race_id');
    }

    public function spot()
    {
        return $this->belongsTo('App\Models\Spots','spot_id');
    }
}
