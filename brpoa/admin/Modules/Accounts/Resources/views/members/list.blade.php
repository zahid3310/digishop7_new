@extends('layouts.app')

@section('title', 'Member Registration')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Member Lists</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Member</a></li>
                                    <li class="breadcrumb-item active">Member Lists</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif
                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<h4 class="card-title">All Member Lists</h4>
                                <br>

                                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Address</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                    	@if(!empty($lists) && ($lists->count() > 0))
                                    	@foreach($lists as $key => $list)
	                                        <tr>
	                                            <td>{{ $key + 1 }}</td>
	                                            <td style="text-align: -webkit-center;">
	                                                @if($list['member_image'] != null)
                                                    <img style="height: 60px;width: 60px;padding: 0px" src="{{ url('public/'.$list['member_image']) }}" class="form-control">
                                                    @else
                                                    <img class="rounded" style="height: 50px;width: 80px;padding: 0px" src="{{ url('public/default.png') }}" class="form-control">
                                                    @endif
	                                            </td>
	                                            <td>{{ $list['ClientName'] }}</td>
	                                            <td>{{ $list['ClientContactNumber'] }}</td>
	                                            <td>{{ $list['ClientEmail'] }}</td>
	                                            <td>{{ $list['ClientAddress'] }}</td>
	                                        </tr>
	                                    @endforeach
	                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

    		</div>
		</div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        function preventDoubleClick()
        {   
            var d_val = $('.enableOnInput').val();

            $('#pRint').val(d_val);
            $('.enableOnInput').prop('disabled', true);
            $('#FormSubmit').submit();
        }
    </script>
@endsection