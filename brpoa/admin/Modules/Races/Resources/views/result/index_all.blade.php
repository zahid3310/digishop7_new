@extends('layouts.app')

@section('title', 'Process Race Result')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Process Race Result</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Races</a></li>
                                    <li class="breadcrumb-item active">Process Race Result</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class=col-12>
                        @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {!! Session::get('success') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('unsuccess'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! Session::get('unsuccess') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('errors'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! 'Some required fields are missing..!! Please try again..' !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        <div class="card">
                            <div class="card-body table-responsive">

                                <form id="FormSubmit" action="{{ route('races_results_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    {{ csrf_field() }}

                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                            <label for="productname" class="col-form-label">Race *</label>
                                            <select id="race_id" name="race_id" class="form-control select2" required>
                                               <option value="">--Select Race--</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                            <label for="productname" class="col-form-label">Spot *</label>
                                            <select id="spot_id" name="spot_id" class="form-control select2" required>
                                               <option value="">--Select Spot--</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                            <label for="productname" class="col-form-label">Upload Excel *</label>
                                            <input name="result_file" type="file" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('races_results_index_all') }}">Close</a></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">List of Result</h4>

                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">SL#</th>
                                            <th style="text-align: center;">Race Name</th>
                                            <th style="text-align: center;">Spot Name</th>
                                            <th style="text-align: center;">Race Date</th>
                                            <th style="text-align: center;">Race Starting Time</th>
                                            <th style="text-align: center;">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($results) && ($results->count() > 0))
                                        @foreach($results as $key => $result)
                                            <tr>
                                                <td style="text-align: center">{{ $key + 1 }}</td>
                                                <td style="text-align: left">{{ $result->race->name }}</td>
                                                <td style="text-align: center">{{ $result->spot->name }}</td>
                                                <td style="text-align: center">{{ date('d-m-Y', strtotime($result->race->date)) }}</td>
                                                <td style="text-align: center">{{ date('H:i A', strtotime($result->race->starting_time)) }}</td>
                                                <td style="text-align: center">
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('races_results_show', $result['id']) }}" target="_blank">View Result</a>
                                                            <a class="dropdown-item" href="{{ route('races_results_delete', $result['id']) }}">Delete</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        var site_url = $('.site_url').val();

        $("#race_id").select2({
            ajax: { 
            url:  site_url + '/races/get-race-list/',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                return result['text'];
            },
        });

        $("#spot_id").select2({
            ajax: { 
            url:  site_url + '/spots/get-spot-list/',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                return result['text'];
            },
        });

    });
</script>
@endsection