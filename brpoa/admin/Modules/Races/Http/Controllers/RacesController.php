<?php

namespace Modules\Races\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Races;
use App\Models\AccountHead;
use App\Models\Projects;
use App\Models\Clients;
use App\Models\VoucherSummary;
use App\Models\AccountTransaction;
use App\Models\BankAccounts;
use App\Models\ChequeTransactions;
use App\Models\BillSummary;
use App\Models\BillTransactions;
use App\Models\Users;
use App\Models\Spots;
use App\Models\MemberDistances;
use App\Models\Results;
use App\Models\ResultEntries;
use App\Imports\ProductsImport;
use Maatwebsite\Excel\Facades\Excel;
use DateTime;
use DB;
use Response;

class RacesController extends Controller
{   
    //Races Start
        public function indexAll()
        {
            //Users Access Level Start
            // $access_check  = userAccess(Auth::user()->id);
            // if ($access_check == 0)
            // {
            //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            // }
            //Users Access Level End
            
            $races       = Races::get();

            return view('races::index_all', compact('races'));
        }

        public function store(Request $request)
        {
            //Users Access Level Start
            // $access_check  = userAccess(Auth::user()->id);
            // if ($access_check == 0)
            // {
            //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            // }
            //Users Access Level End

            $rules = array(
                'name'   => 'required',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id            = Auth::user()->id;
            $data               = $request->all();

            DB::beginTransaction();

            try{
                $race                   = new Races;
                $race->name             = $data['name'];
                $race->race_spot        = $data['race_spot'];
                $race->date             = date('Y-m-d', strtotime($data['race_date']));
                $race->starting_time    = date('H:i:s', strtotime($data['starting_time']));
                $race->created_by       = $user_id;

                if ($race->save())
                {   
                    DB::commit();
                    return back()->with("success","Race Added Successfully !!");
                }
            }catch (\Exception $exception){
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }

        public function edit($id)
        {
            //Users Access Level Start
            // $access_check  = userAccess(Auth::user()->id);
            // if ($access_check == 0)
            // {
            //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            // }
            //Users Access Level End

            $races       = Races::get();
            $find_race  = Races::find($id);

            return view('races::edit', compact('find_race', 'races'));
        }

        public function update(Request $request, $id)
        {
            //Users Access Level Start
            // $access_check  = userAccess(Auth::user()->id);
            // if ($access_check == 0)
            // {
            //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            // }
            //Users Access Level End

            $rules = array(
                'name'   => 'required',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id            = Auth::user()->id;
            $data               = $request->all();

            DB::beginTransaction();

            try{
                $race                   = Races::find($id);
                $race->name             = $data['name'];
                $race->race_spot        = $data['race_spot'];
                $race->date             = date('Y-m-d', strtotime($data['race_date']));
                $race->starting_time    = date('H:i:s', strtotime($data['starting_time']));
                $race->updated_by       = $user_id;

                if ($race->save())
                {   
                    DB::commit();
                    return back()->with("success","Race Updated Successfully !!");
                }
            }catch (\Exception $exception){
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }

        public function getRaceList()
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = Races::get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = Races::where('name', 'LIKE', "%$search%")->get();
            }

            $data = array();
            foreach ($fetchData as $key => $value)
            {
                $data[] = array("id"=>$value['id'], "text"=>$value['name']);
            }
       
            return Response::json($data);
        }
    //Races Start

    //Spots Start
        public function indexAllSpot()
        {
            //Users Access Level Start
            // $access_check  = userAccess(Auth::user()->id);
            // if ($access_check == 0)
            // {
            //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            // }
            //Users Access Level End
            
            $spots       = Spots::orderBy('position', 'ASC')->get();

            return view('races::spots.index_all', compact('spots'));
        }

        public function storeSpot(Request $request)
        {
            //Users Access Level Start
            // $access_check  = userAccess(Auth::user()->id);
            // if ($access_check == 0)
            // {
            //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            // }
            //Users Access Level End

            $rules = array(
                'name' => 'required',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id            = Auth::user()->id;
            $data               = $request->all();
            $branch_id          = Auth()->user()->branch_id;

            DB::beginTransaction();

            try{
                $spot                   = new Spots;
                $spot->name             = $data['name'];
                $spot->position         = $data['position'] != null ? $data['position'] : 1000;
                $spot->created_by       = $user_id;

                if ($spot->save())
                {   
                    $data['clients']    = Clients::where('IsTransactable', 1)
                                            ->where('ActiveStatus', 1)
                                            ->where('CompanyID', $branch_id)
                                            ->where('PtnID', 338)
                                            ->orderBy('PtnGroupCode', 'ASC')
                                            ->get();

                    $spot_data          = Spots::get();
                    $member_distances   = MemberDistances::get();
                    foreach($data['clients'] as $key => $val)
                    {
                        foreach($spot_data as $key1 => $val1)
                        {
                            $find_distance_data = $member_distances->where('member_id', $val->id)
                                                                ->where('spot_id', $val1->id)
                                                                ->count();

                            if($find_distance_data == 0)
                            {
                                $create_distance                = new MemberDistances;
                                $create_distance->member_id     = $val->id;
                                $create_distance->spot_id       = $val1->id;
                                $create_distance->created_by    = Auth()->user()->id;
                                $create_distance->save();
                            }
                        }
                    }

                    DB::commit();
                    return back()->with("success","Spot Added Successfully !!");
                }
            }catch (\Exception $exception){
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }

        public function editSpot($id)
        {
            //Users Access Level Start
            // $access_check  = userAccess(Auth::user()->id);
            // if ($access_check == 0)
            // {
            //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            // }
            //Users Access Level End

            $spots      = Spots::orderBy('position', 'ASC')->get();
            $find_spot  = Spots::find($id);

            return view('races::spots.edit', compact('find_spot', 'spots'));
        }

        public function updateSpot(Request $request, $id)
        {
            //Users Access Level Start
            // $access_check  = userAccess(Auth::user()->id);
            // if ($access_check == 0)
            // {
            //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            // }
            //Users Access Level End

            $rules = array(
                'name' => 'required',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id            = Auth::user()->id;
            $data               = $request->all();

            DB::beginTransaction();

            try{
                $spot                   = Spots::find($id);
                $spot->name             = $data['name'];
                $spot->position         = $data['position'] != null ? $data['position'] : 1000;
                $spot->updated_by       = $user_id;

                if ($spot->save())
                {   
                    DB::commit();
                    return back()->with("success","Spot Updated Successfully !!");
                }
            }catch (\Exception $exception){
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }

        public function getSpotList()
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = Spots::get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = Spots::where('name', 'LIKE', "%$search%")->get();
            }

            $data = array();
            foreach ($fetchData as $key => $value)
            {
                $data[] = array("id"=>$value['id'], "text"=>$value['name']);
            }
       
            return Response::json($data);
        }
    //Spots Start

    //Add Distance Start
        public function createDistance()
        {
            //Users Access Level Start
            // $access_check  = userAccess(Auth::user()->id);
            // if ($access_check == 0)
            // {
            //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            // }
            //Users Access Level End
            
            $branch_id              = Auth()->user()->branch_id;
            $data['clients']        = Clients::where('IsTransactable', 1)
                                            ->where('ActiveStatus', 1)
                                            ->where('CompanyID', $branch_id)
                                            ->where('PtnID', 338)
                                            ->orderBy('PtnGroupCode', 'ASC')
                                            ->get();

            return view('races::addDistance.create', compact('data'));
        }

        public function storeDistance(Request $request)
        {
            $rules = array(
                'customer_id.*' => 'required',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id    = Auth::user()->id;
            $data       = $request->all();
          
            DB::beginTransaction();

            try{
                $bill   = BillSummary::get();
                foreach ($data['customer_id'] as $key => $value)
                { 
                    if($data['distance'][$key] != null)
                    {
                        $update_distance                = MemberDistances::find($data['distance_id'][$key]);
                        $update_distance->distance      = $data['distance'][$key];
                        $update_distance->updated_by    = Auth()->user()->id;
                        $update_distance->save();
                    }
                }

                DB::commit();
                return back()->with("success","Distance Added Successfully !!");
            }
            catch (\Exception $exception)
            {
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }

        public function getMemberDistances($member_id)
        {
            //Users Access Level Start
            // $access_check  = userAccess(Auth::user()->id);
            // if ($access_check == 0)
            // {
            //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            // }
            //Users Access Level End
            
            $data = MemberDistances::leftjoin('tbl_clients', 'tbl_clients.id', 'member_distances.member_id')
                                ->leftjoin('spots', 'spots.id', 'member_distances.spot_id')
                                ->where('member_distances.member_id', $member_id)
                                ->select('member_distances.*', 'tbl_clients.ClientName as ClientName', 'spots.name as SpotName', 'tbl_clients.id as ID')
                                ->orderBy('spots.position', 'ASC')
                                ->get();

            return Response::json($data);
        }
    //Add Distance End

    //Process Result Start
        public function indexAllResult()
        {
            //Users Access Level Start
            // $access_check  = userAccess(Auth::user()->id);
            // if ($access_check == 0)
            // {
            //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            // }
            //Users Access Level End
            
            $results       = Results::get();

            return view('races::result.index_all', compact('results'));
        }

        public function storeResult(Request $request)
        {
            $rows               = Excel::toArray(new ProductsImport, $request->file('result_file')); 
            $data               = $request->all();
            $race               = Races::find($data['race_id']);
            $race_starting_time = $race->date . ' ' . $race->starting_time;

            DB::beginTransaction();

            try{
                $result                  = new Results;
                $result->race_id         = $data['race_id'];
                $result->spot_id         = $data['spot_id'];
                $result->created_by      = Auth()->user()->id; 
                $result->save(); 

                foreach ($rows[0] as $key => $value)
                {   
                    $ring_number        = $value[0];
                    $member_id_expld    = explode('.', $value[1]);
                    $member_id          = $member_id_expld[0];
                    $trapping_date      = $value[2];
                    $trapping_time      = $trapping_date . ' ' . $value[3];
                    $ban_id             = $value[4];
                    $color              = $value[5];

                    //Velocity Calculation Start
                        $km_data    = MemberDistances::where('member_id', $member_id)
                                                ->where('spot_id', $data['spot_id'])
                                                ->first();

                        $km         = $km_data['distance'];
                        $constant   = 1093.6133;
                        $diffInSec  = strtotime($trapping_time) - strtotime($race_starting_time);

                        $minute     = $diffInSec/60;
                        $velocity   = ($km*$constant)/$minute;
                    //Velocity Calculation End

                    $result_entries[] = [
                        'result_id'         => $result->id,
                        'member_id'         => $member_id,
                        'ban_id'            => $ban_id,
                        'color'             => $color,
                        'distance'          => $km_data['distance'],
                        'trapping_date'     => date('Y-m-d', strtotime($trapping_date)),
                        'trapping_time'     => date('H:i:s', strtotime($trapping_time)),
                        'total_time'        => $minute,
                        'velocity'          => $velocity,
                        'created_by'        => Auth()->user()->id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('result_entries')->insert($result_entries);

                DB::commit();
                return back()->with("success","Result Processed Successfully !!");

            }catch (\Exception $exception){
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }

        public function deleteResult($id)
        {
            //Users Access Level Start
            // $access_check  = userAccess(Auth::user()->id);
            // if ($access_check == 0)
            // {
            //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            // }
            //Users Access Level End

            DB::beginTransaction();

            try{
                $result             = Results::where('id', $id)->delete();
                $result_details     = ResultEntries::where('result_id', $id)->delete();

                DB::commit();
                return back()->with("success","Result Deleted Successfully !!");

            }catch (\Exception $exception){
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }

        public function showResult($id)
        {
            //Users Access Level Start
            // $access_check  = userAccess(Auth::user()->id);
            // if ($access_check == 0)
            // {
            //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            // }
            //Users Access Level End

            $result             = Results::find($id);
            $result_details     = ResultEntries::where('result_id', $id)->orderBy('velocity', 'DESC')->get();
            $total_participants = ResultEntries::where('result_id', $id)->distinct('member_id')->count();

            return view('races::result.show', compact('result', 'result_details', 'total_participants'));
        }
    //Process Result Start
}
