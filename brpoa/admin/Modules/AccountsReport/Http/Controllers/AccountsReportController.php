<?php

namespace Modules\AccountsReport\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Models
use DB;
use Auth;
use Response;
use Carbon\Carbon;
use App\Models\AccountTransaction;
use App\Models\AccountHead;
use App\Models\Clients;

class AccountsReportController extends Controller
{   
    //Cash Book Start
        public function cashBookIndex()
        {
            //Users Access Level Start
            $access_check  = userAccess(Auth::user()->id);
            if ($access_check == 0)
            {
                return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            }
            //Users Access Level End

            return view('accountsreport::cashBook.index');
        }

        public function cashBookPrint()
        {
            $branch_id  = Auth()->user()->branch_id;
            $date       = date('Y-m-d');
            $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
            $to_date    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

            $data['from_date']      = $from_date;
            $data['to_date']        = $to_date;

            return view('accountsreport::cashBook.print', compact('data'));
        }

        public function cashBookPrintApi()
        {
            $branch_id  = Auth()->user()->branch_id;
            $date       = date('Y-m-d');
            $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
            $to_date    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));

            $data_list      = AccountTransaction::leftjoin('tbl_acc_head', 'tbl_acc_head.id', 'tbl_acc_accounts_transaction.CBAccount')
                                            ->leftjoin('tbl_projects', 'tbl_projects.id', 'tbl_acc_accounts_transaction.ProjectID')
                                            ->leftjoin('tbl_clients', 'tbl_clients.id', 'tbl_acc_accounts_transaction.RegisterID')
                                            ->leftjoin('tbl_acc_voucher_summary', 'tbl_acc_voucher_summary.id', 'tbl_acc_accounts_transaction.VoucherId')
                                            ->whereBetween('tbl_acc_accounts_transaction.VoucherDate', [$from_date, $to_date])
                                            ->where('tbl_acc_accounts_transaction.IsPosted', 1)
                                            ->where('tbl_acc_accounts_transaction.CompanyID', $branch_id)
                                            ->where('tbl_acc_accounts_transaction.PurposeAccHeadName', 'cash_ac')
                                            ->whereIn('tbl_acc_accounts_transaction.VoucherType', ['CP', 'CR', 'JR', 'CN'])
                                            ->select('tbl_acc_accounts_transaction.*',
                                                     'tbl_acc_head.HeadName as HeadName',
                                                     'tbl_projects.ProjectName as ProjectName',
                                                     'tbl_clients.ClientName as ClientName',
                                                     'tbl_acc_voucher_summary.Type as Type',
                                                     'tbl_acc_voucher_summary.VoucherNumber as VoucherNumber'
                                                    )
                                            ->get();

            $cash_in_hand_debit     = $data_list->sum('DebitAmount');
            $cash_in_hand_credit    = $data_list->sum('CreditAmount');
            $cash_in_hand           = $cash_in_hand_debit - $cash_in_hand_credit;

            $data['from_date']      = $from_date;
            $data['to_date']        = $to_date;
            $data['debit']          = $cash_in_hand_debit;
            $data['credit']         = $cash_in_hand_credit;
            $data['result']         = $data_list;
            $data['cash_in_hand']   = $cash_in_hand;

            return Response::json($data);
        }
    //Cash Book End

    //Bank Book Start
        public function bankBookIndex()
        {
            //Users Access Level Start
            $access_check  = userAccess(Auth::user()->id);
            if ($access_check == 0)
            {
                return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            }
            //Users Access Level End

            return view('accountsreport::bankBook.index');
        }

        public function bankBookPrint()
        {
            $branch_id  = Auth()->user()->branch_id;
            $date       = date('Y-m-d');
            $from_date  = isset($_GET['from_date']) ? date('Y-m-d 00:00:00', strtotime($_GET['from_date'])) : date('Y-m-d 00:00:00', strtotime($date));
            $to_date    = isset($_GET['to_date']) ? date('Y-m-d 00:00:00', strtotime($_GET['to_date'])) : date('Y-m-d 00:00:00', strtotime($date));
            $account_id = isset($_GET['account_id']) ? $_GET['account_id'] : 0;

            $data_list      = AccountTransaction::whereBetween('VoucherDate', [$from_date, $to_date])
                                            ->where('IsPosted', 1)
                                            ->where('CompanyID', $branch_id)
                                            ->where('PurposeAccHeadName', 'bank_ac')
                                            ->whereIn('VoucherType', ['BP', 'BR', 'JR', 'CN'])
                                            ->when($account_id != 0, function ($query) use ($account_id) {
                                                return $query->where('HeadID', $account_id);
                                            })
                                            ->get();

            $bank_debit             = $data_list->sum('DebitAmount');
            $bank_credit            = $data_list->sum('CreditAmount');
            $balance                = $bank_debit - $bank_credit;

            $data['from_date']      = $from_date;
            $data['to_date']        = $to_date;
            $data['result']         = $data_list;
            $data['balance']        = $balance;
            $data['acc_head']       = AccountHead::find($account_id);

            return view('accountsreport::bankBook.print', compact('data'));
        }
    //Bank Book End

    //Current Balance Start
        public function currentBalanceIndex()
        {
            //Users Access Level Start
            $access_check  = userAccess(Auth::user()->id);
            if ($access_check == 0)
            {
                return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            }
            //Users Access Level End

            $branch_id      = Auth()->user()->branch_id;
            $accounts       = AccountHead::whereIn('PurposeHeadName', ['cash_ac', 'bank_ac'])
                                            ->where('IsTransactable', 1)
                                            ->get();
            $trns_data      = AccountTransaction::where('IsPosted', 1)
                                            ->where('CompanyID', $branch_id)
                                            ->whereIn('PurposeAccHeadName', ['cash_ac', 'bank_ac'])
                                            ->whereIn('VoucherType', ['BP', 'BR', 'CP', 'CR', 'JR', 'CN'])
                                            ->get();

            foreach($accounts as $key => $account)
            {   
                if ($account->PurposeHeadName == 'cash_ac')
                {
                    $data['Cash In Hand'][$account->id]['head_name']        = $account->HeadName;
                    $data['Cash In Hand'][$account->id]['debit_amount']     = $trns_data->where('PurposeAccHeadName', 'cash_ac')                                                            ->where('HeadID', $account->id)
                                                                                    ->sum('DebitAmount');
                    $data['Cash In Hand'][$account->id]['credit_amount']    = $trns_data->where('PurposeAccHeadName', 'cash_ac')
                                                                                    ->where('HeadID', $account->id)
                                                                                    ->sum('CreditAmount');
                }

                if ($account->PurposeHeadName == 'bank_ac')
                {
                    $data['Cash At Bank'][$account->id]['head_name']        = $account->HeadName;
                    $data['Cash At Bank'][$account->id]['debit_amount']     = $trns_data->where('PurposeAccHeadName', 'bank_ac')                                                            ->where('HeadID', $account->id)
                                                                                    ->sum('DebitAmount');
                    $data['Cash At Bank'][$account->id]['credit_amount']    = $trns_data->where('PurposeAccHeadName', 'bank_ac')
                                                                                    ->where('HeadID', $account->id)
                                                                                    ->sum('CreditAmount');
                }
            }

            if(isset($data))
            {
                $data = $data;
            }
            else
            {
                $data = [];
            }

            return view('accountsreport::currentBalance.index', compact('data'));
        }

        public function currentBalanceIndexApi()
        {
            $branch_id      = Auth()->user()->branch_id;
            $accounts       = AccountHead::whereIn('PurposeHeadName', ['cash_ac', 'bank_ac'])
                                            ->where('IsTransactable', 1)
                                            ->get();
            $trns_data      = AccountTransaction::where('IsPosted', 1)
                                            ->where('CompanyID', $branch_id)
                                            ->whereIn('PurposeAccHeadName', ['cash_ac', 'bank_ac'])
                                            ->whereIn('VoucherType', ['BP', 'BR', 'CP', 'CR', 'JR', 'CN'])
                                            ->get();

            $sl_cash        = 1;
            $sl_bank        = 1;
            $grand_total    = 0;
            $sub_total_cash = 0;
            $sub_total_bank = 0;
            foreach($accounts as $key => $account)
            {   
                if ($account->PurposeHeadName == 'cash_ac')
                {   
                    $cash_credit_sum    = $trns_data->where('PurposeAccHeadName', 'cash_ac')
                                                    ->where('HeadID', $account->id)
                                                    ->sum('CreditAmount');
                    $cash_debit_sum     = $trns_data->where('PurposeAccHeadName', 'cash_ac')                                
                                                    ->where('HeadID', $account->id)
                                                    ->sum('DebitAmount');
                    $cash_balance       = $cash_debit_sum - $cash_credit_sum;

                    $result['Cash In Hand'][$account->id]['sl']            = $sl_cash;
                    $result['Cash In Hand'][$account->id]['head_name']     = $account->HeadName;
                    $result['Cash In Hand'][$account->id]['balance']       = number_format($cash_balance,2,'.',',');

                    $sl_cash++;
                    $grand_total    = $grand_total + $cash_balance;
                    $sub_total_cash = $sub_total_cash + $cash_balance;
                }

                if ($account->PurposeHeadName == 'bank_ac')
                {   
                    $bank_credit_sum    = $trns_data->where('PurposeAccHeadName', 'bank_ac')
                                                    ->where('HeadID', $account->id)
                                                    ->sum('CreditAmount');
                    $bank_debit_sum     = $trns_data->where('PurposeAccHeadName', 'bank_ac')                                 
                                                    ->where('HeadID', $account->id)
                                                    ->sum('DebitAmount');
                    $bank_balance       = $bank_debit_sum - $bank_credit_sum;

                    $result['Cash At Bank'][$account->id]['sl']            = $sl_bank;
                    $result['Cash At Bank'][$account->id]['head_name']     = $account->HeadName;
                    $result['Cash At Bank'][$account->id]['balance']       = number_format($bank_balance,2,'.',',');

                    $sl_bank++;
                    $grand_total    = $grand_total + $bank_balance;
                    $sub_total_bank = $sub_total_bank + $bank_balance;
                }
            }

            if(isset($result))
            {
                $result = $result;
            }
            else
            {
                $result = [];
            }

            $data['result']             = $result;
            $data['sub_total_cash']     = $sub_total_cash;
            $data['sub_total_bank']     = $sub_total_bank;
            $data['grand_total']        = $grand_total;

            return Response::json($data);
        }
    //Current Balance End

    //Ledger Book Start
        public function ledgerBookIndex()
        {
            //Users Access Level Start
            $access_check  = userAccess(Auth::user()->id);
            if ($access_check == 0)
            {
                return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            }
            //Users Access Level End

            return view('accountsreport::ledgerBook.index');
        }

        public function ledgerBookPrint()
        {
            $branch_id  = Auth()->user()->branch_id;
            $date       = date('Y-m-d');
            $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
            $to_date    = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
            $transactable_account_id    = isset($_GET['transactable_account_id']) ? $_GET['transactable_account_id'] : 0;

            $trns_data  = AccountTransaction::leftjoin('tbl_acc_head', 'tbl_acc_head.id', 'tbl_acc_accounts_transaction.HeadID')
                                            ->whereBetween('tbl_acc_accounts_transaction.VoucherDate', [$from_date, $to_date])
                                            ->where('tbl_acc_accounts_transaction.IsPosted', 1)
                                            ->where('tbl_acc_head.IsTransactable', 1)
                                            ->whereNotIn('tbl_acc_accounts_transaction.VoucherType', ['SALES','PURCHASE', 'SR', 'PR'])
                                            ->where('tbl_acc_accounts_transaction.CompanyID', $branch_id)
                                            ->when($transactable_account_id != 0, function ($query) use ($transactable_account_id) {
                                                return $query->where('tbl_acc_accounts_transaction.HeadID', $transactable_account_id);
                                            })
                                            ->select('tbl_acc_accounts_transaction.*' , 'tbl_acc_head.HeadName as HeadName')
                                            ->get();

            if((isset($trns_data)) && ($trns_data != null))
            {
                $data['result'] = $trns_data->groupBy('HeadName');
            }
            else
            {
                $data['result'] = [];
            }

            $data['from_date']      = $from_date;
            $data['to_date']        = $to_date;

            return view('accountsreport::ledgerBook.print', compact('data'));
        }
    //Ledger Book End

    //Code for select2 satrt
        public function getsBankAccounts()
        {
            $branch_id  = Auth()->user()->branch_id;

            if(!isset($_GET['searchTerm']))
            {   
                $fetchData  = AccountHead::where('IsTransactable', 1)
                                    ->where('ActiveStatus', 1)
                                    ->where('PurposeHeadName', 'bank_ac')
                                    ->orderBy('PtnGroupCode', 'ASC')
                                    ->get();
            }
            else
            {
                $search     = $_GET['searchTerm'];   
                $fetchData  = AccountHead::where('IsTransactable', 1)
                                    ->where('ActiveStatus', 1)
                                    ->where('PurposeHeadName', 'bank_ac')
                                    ->where('name', 'LIKE', "%$search%")
                                    ->orderBy('PtnGroupCode', 'ASC')
                                    ->get();
            }

            $fetchData->where('CompanyID', 1);
            foreach ($fetchData as $key => $value)
            {
                $data[] = array("id"=>$value['id'], "text"=>$value['HeadName']);
            }

            return Response::json($data);
        }

        public function getsRegistersCustomers()
        {
            $branch_id  = Auth()->user()->branch_id;

            if(!isset($_GET['searchTerm']))
            {   
                $fetchData  = Clients::where('IsTransactable', 1)
                                    ->where('ActiveStatus', 1)
                                    ->where('PtnID', 338)
                                    ->orderBy('PtnGroupCode', 'ASC')
                                    ->get();
            }
            else
            {
                $search     = $_GET['searchTerm'];   
                $fetchData  = Clients::where('IsTransactable', 1)
                                    ->where('ActiveStatus', 1)
                                    ->where('PtnID', 338)
                                    ->where('ClientName', 'LIKE', "%$search%")
                                    ->orderBy('PtnGroupCode', 'ASC')
                                    ->get();
            }

            $fetchData->where('CompanyID', 1);
            $data[] = array("id"=>0, "text"=>'--All Customer--');
            foreach ($fetchData as $key => $value)
            {
                $data[] = array("id"=>$value['id'], "text"=>$value['ClientName']);
            }

            return Response::json($data);
        }

        public function getsTransactableAccounts()
        {
            $branch_id  = Auth()->user()->branch_id;

            if(!isset($_GET['searchTerm']))
            {   
                $fetchData  = AccountHead::where('IsTransactable', 1)
                                    ->where('ActiveStatus', 1)
                                    ->orderBy('PtnGroupCode', 'ASC')
                                    ->get();
            }
            else
            {
                $search     = $_GET['searchTerm'];   
                $fetchData  = AccountHead::where('IsTransactable', 1)
                                    ->where('ActiveStatus', 1)
                                    ->where('name', 'LIKE', "%$search%")
                                    ->orderBy('PtnGroupCode', 'ASC')
                                    ->get();
            }

            $fetchData->where('CompanyID', 1);
            foreach ($fetchData as $key => $value)
            {
                $data[] = array("id"=>$value['id'], "text"=>$value['HeadName']);
            }

            return Response::json($data);
        }

        public function getsCashBankAccounts()
        {
            $branch_id  = Auth()->user()->branch_id;

            if(!isset($_GET['searchTerm']))
            {   
                $fetchData  = AccountHead::where('IsTransactable', 1)
                                    ->where('ActiveStatus', 1)
                                    ->whereIn('PurposeHeadName', ['bank_ac', 'cash_ac'])
                                    ->orderBy('PtnGroupCode', 'ASC')
                                    ->get();
            }
            else
            {
                $search     = $_GET['searchTerm'];   
                $fetchData  = AccountHead::where('IsTransactable', 1)
                                    ->where('ActiveStatus', 1)
                                    ->whereIn('PurposeHeadName', ['bank_ac', 'cash_ac'])
                                    ->where('name', 'LIKE', "%$search%")
                                    ->orderBy('PtnGroupCode', 'ASC')
                                    ->get();
            }

            $fetchData->where('CompanyID', 1);
            foreach ($fetchData as $key => $value)
            {
                $data[] = array("id"=>$value['id'], "text"=>$value['HeadName']);
            }

            return Response::json($data);
        }
    //Code for select2 end

    //Accounts Voucher Start
        public function accountsVoucherIndex()
        {
            //Users Access Level Start
            $access_check  = userAccess(Auth::user()->id);
            if ($access_check == 0)
            {
                return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
            }
            //Users Access Level End

            return view('accountsreport::accountsVoucher.index');
        }

        public function accountsVoucherPrint()
        {
            $branch_id              = Auth()->user()->branch_id;
            $date                   = date('Y-m-d');
            $from_date              = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
            $to_date                = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
            $data['from_date']      = $from_date;
            $data['to_date']        = $to_date;
            $data['type']           = $_GET['type'];

            return view('accountsreport::accountsVoucher.print', compact('data'));
        }

        public function accountsVoucherPrintApi()
        {
            $branch_id      = Auth()->user()->branch_id;
            $date           = date('Y-m-d');
            $from_date      = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
            $to_date        = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d', strtotime($date));
            $type           = $_GET['type'];

            $data_list      = AccountTransaction::leftjoin('tbl_acc_head', 'tbl_acc_head.id', 'tbl_acc_accounts_transaction.CBAccount')
                                            ->leftjoin('tbl_projects', 'tbl_projects.id', 'tbl_acc_accounts_transaction.ProjectID')
                                            ->leftjoin('tbl_clients', 'tbl_clients.id', 'tbl_acc_accounts_transaction.RegisterID')
                                            ->leftjoin('tbl_acc_voucher_summary', 'tbl_acc_voucher_summary.id', 'tbl_acc_accounts_transaction.VoucherId')
                                            ->whereBetween('tbl_acc_accounts_transaction.VoucherDate', [$from_date, $to_date])
                                            ->where('tbl_acc_accounts_transaction.IsPosted', 1)
                                            ->whereNotIn('tbl_acc_accounts_transaction.VoucherType', ['SALES','PURCHASE', 'SR', 'PR'])
                                            ->where('tbl_acc_accounts_transaction.CompanyID', $branch_id)
                                            ->when($type != 'ALL', function ($query) use ($type) {
                                                return $query->where('tbl_acc_accounts_transaction.VoucherType', $type);
                                            })
                                            ->select('tbl_acc_accounts_transaction.*',
                                                     'tbl_acc_head.HeadName as HeadName',
                                                     'tbl_projects.ProjectName as ProjectName',
                                                     'tbl_clients.ClientName as ClientName',
                                                     'tbl_acc_voucher_summary.Type as Type',
                                                     'tbl_acc_voucher_summary.VoucherNumber as VoucherNumber'
                                                    )
                                            ->get();

            $cash_in_hand_debit     = $data_list->sum('DebitAmount');
            $cash_in_hand_credit    = $data_list->sum('CreditAmount');
            $cash_in_hand           = $cash_in_hand_debit - $cash_in_hand_credit;

            $data['from_date']      = $from_date;
            $data['to_date']        = $to_date;
            $data['debit']          = $cash_in_hand_debit;
            $data['credit']         = $cash_in_hand_credit;
            $data['result']         = $data_list;
            $data['cash_in_hand']   = $cash_in_hand;

            return Response::json($data);
        }
    //Accounts Voucher End
}
