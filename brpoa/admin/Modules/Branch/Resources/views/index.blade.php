@extends('layouts.app')

@section('title', 'Company Information')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Company Information</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Company Information</a></li>
                                    <li class="breadcrumb-item active">Manage Information</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<h4 class="card-title">All Branches</h4>

                                <br>

                                <table id="datatable" class="table table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Branch Name</th>
                                            <th>Address</th>
                                            <th>Contact Number</th>
                                            <th>Logo</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                    	@if(!empty($branches) && ($branches->count() > 0))
                                    	@foreach($branches as $key => $branch)
	                                        <tr>
	                                            <td>{{ $key + 1 }}</td>
                                                <td>{{ $branch['name'] }}</td>
                                                <td>{{ $branch['address'] }}</td>
                                                <td>{{ $branch['contact_number'] }}</td>
                                                <td>
                                                    <img style="height: 40px;width: 40px" src="{{ url('public/'.$branch->logo) }}">
                                                </td>
	                                            <td>
	                                            	<div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('branch_edit', $branch['id']) }}">Edit</a>
                                                        </div>
                                                    </div>
	                                            </td>
	                                        </tr>
	                                    @endforeach
	                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection