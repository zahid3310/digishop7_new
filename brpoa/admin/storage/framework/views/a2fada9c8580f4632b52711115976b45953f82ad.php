

<?php $__env->startSection('title', 'Accounts Voucher'); ?>

<?php $__env->startSection('styles'); ?>
    <style type="text/css">
        @media  print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page  {
                margin: 0cm ! important;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Accounts Voucher</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Accounts Voucher</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="margin-left: 150px;margin-right: 150px" class="row d-print-none">
                    <div class="col-12">
                        <div class="card">
                            <div style="padding-bottom: 4px" class="card-body table-responsive">
                                <form method="get" action="<?php echo e(route('accounts_report_accounts_voucher_print')); ?>" enctype="multipart/form-data" target="_blank">
	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"> From Date </label>
	                                    <div class="col-md-9">
	                                    	<div class="row">
	                                    		<div class="input-group-append col-md-5">
	                                    			<span style="border-radius: 0px" class="input-group-text"><i class="mdi mdi-calendar"></i></span>
		                                    		<input style="border-radius: 0px" type="text" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" value="<?php echo e(date('d-m-Y')); ?>" name="from_date">
		                                    	</div>

		                                    	<div class="input-group-append col-md-7">
		                                    		<label style="text-align: right" for="productname" class="col-md-2 col-form-label"> To </label>
		                                    		<span style="border-radius: 0px" class="input-group-text"><i class="mdi mdi-calendar"></i></span>
		                                    		<input style="border-radius: 0px" type="text" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" value="<?php echo e(date('d-m-Y')); ?>" name="to_date">
		                                    	</div>
	                                    	</div>
	                                    </div>
	                                </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Voucher Type</label>
                                        <div class="col-md-9">
                                            <select class="form-control select2" name="type">
                                                <option value="ALL">--Select Voucher Type--</option>
                                                <option value="BP">BANK PAYMENT</option>
                                                <option value="BR">BANK RECEIPT</option>
                                                <option value="CP">CASH PAYMENT</option>
                                                <option value="CR">CASH RECEIPT</option>
                                                <option value="CN">CONTRA</option>
                                                <option value="JR">JOURNAL</option>
                                            </select>
                                        </div>
                                    </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"></label>
	                                    <div class="col-md-9">
	                                        <button style="border-radius: 0px;background-color: #297049;color: white" class="btn" type="submit" target="_blank">
	                                        	Print
	                                    	</button>
	                                    </div>
	                                </div>
                            	</form>   
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/brpoa/admin/Modules/AccountsReport/Resources/views/accountsVoucher/index.blade.php ENDPATH**/ ?>