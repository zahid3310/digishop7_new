

<?php $__env->startSection('title', 'Contra Voucher'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Orders</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">New Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('contra_voucher_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                <?php echo e(csrf_field()); ?>


                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Voucher Date *</label>
                                            <div class="col-md-8">
                                                <input id="voucher_date" name="voucher_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Voucher Number</label>
                                            <div class="col-md-8">
                                                <input id="voucher_number" name="voucher_number" type="text" class="form-control" value="<?php echo e($data['voucherNumber']); ?>" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-3 col-form-label">Narration</label>
                                            <div class="col-md-9">
                                                <input id="narration" name="narration" type="text" class="form-control" placeholder="Narration">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Contra Type</label>
                                            <div class="col-md-8">
                                                <select style="width: 100%;cursor: pointer" id="contraType" name="contra_type" class="form-control">
                                                    <option value="0">Select A Voucher Type</option>
                                                    <option value="1">Cash To Bank</option>
                                                    <option value="2">Bank To Cash</option>
                                                    <option value="3">Bank To Bank</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Bank Account</label>
                                            <div class="col-md-8">
                                                <select style="width: 100%;cursor: pointer" name="bank_account_id" class="inner form-control select2" id="bank_account_id" required>
                                                <option value="0">--Select Account--</option>
                                                <?php if($data["bankAccounts"]->count() > 0): ?>
                                                <?php $__currentLoopData = $data["bankAccounts"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bankAccounts): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($bankAccounts->id); ?>"><?php echo e($bankAccounts->HeadName); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label id="bbShow" for="productname" class="col-md-6 col-form-label">Available Balance </label>
                                            <div class="col-md-6">
                                                <input id="cash_balance" name="cash_balance" type="text" value="0" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-1">
                                        <i id="add_field_button" style="padding: 0.68rem 2.5rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button col-md-3"></i>
                                    </div>
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #FFD4A3;height: 30px;padding-top: 5px" class="col-md-12">
                                        <h5 style="text-align: center">Contra Voucher</h5>
                                    </div>
 
                                    <div style="background-color: #D2D2D2;height: 240px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        <div class="row di_0">
                                        </div>
                                    </div>

                                    <div style="background-color: #FFD4A3;height: 30px;padding-top: 5px" class="col-md-12">
                                        <h5 style="text-align: center">Cheque Entry 
                                            <span style="float: right">
                                                <i id="add_field_button1" style="padding: 0.25rem 3rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button1 col-md-3"></i>
                                            </span>
                                        </h5>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 170px;padding-top: 15px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap1 getMultipleRow1">
                                        <div class="row di1_0">
                                        </div>
                                    </div>

                                    <div style="background-color: #D2D2D2;height: 60px;padding-top: 13px" class="col-md-8">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Save & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('cash_payment_voucher_index')); ?>">Close</a></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #D2D2D2;height: 60px;padding-top: 13px" class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Sub Total</label>
                                            <div class="col-md-7">
                                                <input type="text" id="subTotalBdt" class="form-control">
                                                <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                            <input id="add_field_button1" type="button" class="btn btn-success btn-block inner add_field_button1" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url            = $('.site_url').val();

            $('#add_field_button').click();
            $('#add_field_button1').click();
        });

        $(document).change("#bank_account_id", function(){
            var site_url    = $(".site_url").val();
            var bankId      = $("#bank_account_id").val();

            $.get(site_url + '/voucherPosting/getBankBalance/'+ bankId, function(data)
            {
                $("#cash_balance").val(0);
                $("#cash_balance").val(data);
            });
        });
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = -1;
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var account_head_label  = '<label class="hidden-xs" for="productname">Account Head Name</label>\n';
                    var particulars_label   = '<label class="hidden-xs" for="productname">Particulars</label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Amount(BDT)</label>\n';  
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner">'+ ' ' + serial +'</i>' +
                                                '</div>\n';
                }
                else
                {
                    var account_head_label  = '';
                    var particulars_label   = '';
                    var amount_label        = '';  
                    var action_label        = '';

                    var add_btn             = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner">'+ ' ' + serial +'</i>' +
                                                '</div>\n';
                }

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                        '<div class="col-lg-5 col-md-5 col-sm-12 col-12 form-group">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Account Head Name</label>\n' +
                                                            account_head_label +
                                                            '<select style="width: 100%;cursor: pointer" name="account_head_name[]" class="inner form-control single_select2" id="account_head_name_'+x+'" required>\n' +
                                                            '<option value="">'+ '--Select Account Head--' +'</option>' +
                                                            '<?php if($data["accountsHeadList"]->count() > 0): ?>' +
                                                            '<?php $__currentLoopData = $data["accountsHeadList"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $accountsHeadList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>' +
                                                            '<option value="<?php echo e($accountsHeadList->id); ?>"><?php echo e($accountsHeadList->HeadName); ?></option>' +
                                                            '<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>' +
                                                            '<?php endif; ?>' +
                                                            '</select>\n' +
                                                        '</div>\n' +

                                                        '<div style="padding: 0px" class="col-lg-4 col-md-4 col-sm-6 col-6">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Particulars</label>\n' +
                                                            particulars_label  +
                                                            '<input type="text" name="particular[]" class="inner form-control" id="particular_'+x+'" placeholder="Enter Particular" />\n' +
                                                        '</div>\n' +

                                                        '<div class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Amount</label>\n' +
                                                            amount_label  +
                                                            '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Enter Amount" required oninput="calculateActualAmount('+x+')" />\n' +
                                                        '</div>\n' +
                                                        
                                                        add_btn +
                                                    '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }                                   
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            //Calculating Subtotal Amount
            var total = 0;
            $('.amount').each(function()
            {
                total += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(parseFloat(total).toFixed());
            $("#subTotalBdtShow").val(parseFloat(total).toFixed());
        }
    </script>

    <script type="text/javascript">
        var max_fields1       = 50;                           //maximum input boxes allowed
        var wrapper1          = $(".input_fields_wrap1");      //Fields wrapper
        var add_button1       = $(".add_field_button1");       //Add button ID
        var index_no1         = 1;

        //For apending another rows start
        var y = -1;
        $(add_button1).click(function(e)
        {
            e.preventDefault();

            // var y = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields1)
            {
                y++;

                var serial = y + 1;

                if (serial == y + 1)
                {
                    var bank_account_label  = '<label class="hidden-xs" for="productname">Bank Account</label>\n';
                    var cheque_number_label = '<label class="hidden-xs" for="productname">Cheque#</label>\n';
                    var cq_date_label       = '<label class="hidden-xs" for="productname">CQ. Date</label>\n';
                    var en_date_label       = '<label class="hidden-xs" for="productname">EN. Date</label>\n';
                    var type_label          = '<label class="hidden-xs" for="productname">Type</label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Amount</label>\n';  
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             = '<div style="margin-bottom: 5px;" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field1" data-val="'+y+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner">'+ ' ' + serial +'</i>' +
                                                '</div>\n';
                }
                else
                {
                    var bank_account_label  = '';
                    var cheque_number_label = '';
                    var cq_date_label       = '';
                    var en_date_label       = '';
                    var type_label          = '';
                    var amount_label        = '';  
                    var action_label        = '';

                    var add_btn             = '<div style="margin-bottom: 5px;" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field1" data-val="'+y+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner">'+ ' ' + serial +'</i>' +
                                                '</div>\n';
                }

                $('.getMultipleRow1').prepend(' ' + '<div class="row di1_'+y+'">' +
                                                        '<div style="padding-right: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Bank Account</label>\n' +
                                                            bank_account_label +
                                                            '<select style="width: 100%;cursor: pointer" name="check_account_head_name[]" class="inner form-control select2" id="check_account_head_name_'+y+'">\n' +
                                                            '<option value="">'+ '--Select--' +'</option>' +
                                                            '<?php if($data["bankAccounts"]->count() > 0): ?>' +
                                                            '<?php $__currentLoopData = $data["bankAccounts"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bankAccounts): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>' +
                                                            '<option value="<?php echo e($bankAccounts->id); ?>"><?php echo e($bankAccounts->HeadName); ?></option>' +
                                                            '<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>' +
                                                            '<?php endif; ?>' +
                                                            '</select>\n' +
                                                        '</div>\n' +

                                                        '<div style="padding: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Cheque#</label>\n' +
                                                            cheque_number_label  +
                                                            '<input type="text" name="check_cheque_number[]" class="inner form-control" id="check_cheque_number_'+y+'" placeholder="Cheque#" />\n' +
                                                        '</div>\n' +

                                                        '<div style="padding: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">CQ. Date</label>\n' +
                                                            cq_date_label  +
                                                            '<input id="cq_date_'+y+'" name="cq_date[]" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true">\n' +
                                                        '</div>\n' +

                                                        '<div style="padding: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">EN. Date</label>\n' +
                                                            en_date_label  +
                                                            '<input id="en_date_'+y+'" name="en_date[]" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true">\n' +
                                                        '</div>\n' +

                                                        '<div style="padding: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Amount</label>\n' +
                                                            amount_label  +
                                                            '<input type="text" name="check_amount[]" class="inner form-control" id="check_amount_'+y+'" placeholder="Amount" />\n' +
                                                        '</div>\n' +

                                                        '<div style="padding: 0px" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Type</label>\n' +
                                                            type_label +
                                                            '<select style="width: 100%;cursor: pointer" name="type[]" class="inner form-control" id="type_'+y+'">\n' +
                                                                '<option value="CHEQUE">CHEQUE</option>\n' +
                                                                '<option value="CASH">CASH</option>\n' +
                                                                '<option value="DD">DD</option>\n' +
                                                                '<option value="PAY ORDER">PAY ORDER</option>\n' +
                                                                '<option value="TRANSFER">TRANSFER</option>\n' +
                                                                '<option value="TT">TT</option>\n' +
                                                                '<option value="OTHERS">OTHERS</option>\n' +
                                                            '</select>\n' +
                                                        '</div>\n' +
                                                        
                                                        add_btn +
                                                    '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }                                   
        });
        //For apending another rows end

        $(wrapper1).on("click",".remove_field1", function(e)
        {
            e.preventDefault();

            var y = $(this).attr("data-val");

            $('.di1_'+y).remove(); y--;
        });
    </script>

    <script type="text/javascript">
        $(document).change('#contraType', function(){

            var site_url    = $(".site_url").val();
            var ContraType  = $("#contraType").val();

            if(ContraType == 1)
            {
                $("#bank_account_id").attr('disabled', true);

                $.get(site_url + '/voucherPosting/getCashBalance/', function(data)
                {   
                    console.log(data);
                    $("#cash_balance").val(0);
                    $("#cash_balance").val(data);
                });
            }
            else
            {
                $("#bank_account_id").attr('disabled', false);
                $("#cash_balance").val(0);
            }
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/brpoa/admin/Modules/Invoices/Resources/views/contraVoucher/index.blade.php ENDPATH**/ ?>