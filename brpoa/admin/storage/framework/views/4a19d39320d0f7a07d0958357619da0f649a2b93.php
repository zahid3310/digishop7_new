

<?php $__env->startSection('title', 'Member Registration'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Member Registration</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Member</a></li>
                                    <li class="breadcrumb-item active">Member Registration</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>
                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

            					<form id="FormSubmit" action="<?php echo e(route('member_registration')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                <?php echo e(csrf_field()); ?>


                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Member Full Name *</label>
                                        <input type="text" name="full_name" class="inner form-control" id="full_name" placeholder="Enter Member Full Name" required />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Member User Name *</label>
                                        <input type="text" name="user_name" class="inner form-control" id="user_name" placeholder="Enter Member User Name" required />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Member Email *</label>
                                        <input type="email" name="email" class="inner form-control" id="email" placeholder="Enter Member Email" required />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Member Number *</label>
                                        <input type="number" name="phone" class="inner form-control" id="phone" placeholder="Enter Member Number" required />
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Member Address *</label>
                                        <input type="text" name="address" class="inner form-control" id="address" placeholder="Enter Member Address" required />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Member Profile Image *</label>
                                        <input type="file" name="member_image" class="inner form-control" id="member_image">
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('home')); ?>">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        function preventDoubleClick()
        {   
            var d_val = $('.enableOnInput').val();

            $('#pRint').val(d_val);
            $('.enableOnInput').prop('disabled', true);
            $('#FormSubmit').submit();
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/brpoa/admin/Modules/Accounts/Resources/views/members/create.blade.php ENDPATH**/ ?>