<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ResultEntries extends Model
{  
    protected $table = "result_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function result()
    {
        return $this->belongsTo('App\Models\Results','result_id');
    }

    public function member()
    {
        return $this->belongsTo('App\Models\Clients','member_id');
    }
}
