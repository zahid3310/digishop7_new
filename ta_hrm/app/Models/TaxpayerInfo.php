<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaxpayerInfo extends Model
{
    protected $table = "tax_holder_info";
    
    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }
}
