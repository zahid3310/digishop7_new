<?php

namespace Modules\Customers\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\JournalEntries;
use App\Models\Customers;
use App\Models\Users;
use App\Models\Departments;
use App\Models\Sections;
use App\Models\TaxpayerInfo;
use App\Models\TradeLicenseInfo;
use Response;
use DB;

class CustomersController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $branch_id      = Auth::user()->branch_id;
        $users          = Users::where('branch_id', $branch_id)
                                ->where('id', '!=', 1)
                                ->orderBy('created_at', 'ASC')
                                ->get();
        $departments    = Departments::orderBy('created_at', 'DESC')->get();
        $sections       = Sections::orderBy('created_at', 'DESC')->get();

        return view('customers::index', compact('users', 'departments', 'sections'));
    }

    public function create()
    {
        return view('customers::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                      = new Customers;
            $customers->name                = $data['customer_name'];
            $customers->employee_id         = $data['employee_id'];
            $customers->serial              = $data['serial'];
            $customers->organization_name   = $data['organization_name'];
            $customers->address             = $data['address'];
            $customers->phone               = $data['mobile_number'];
            $customers->nid_number          = $data['nid_number'];
            $customers->alternative_contact = $data['alternative_mobile_number'];
            $customers->contact_type        = $data['contact_type'];
            $customers->joining_date        = date('Y-m-d', strtotime($data['joining_date']));
            $customers->designation         = $data['designation'];
            $customers->salary              = $data['salary'];
            $customers->user_id             = $data['user_id'];
            $customers->department_id       = $data['department_id'];
            $customers->section_id          = $data['section_id'];
            $customers->branch_id           = $branch_id;
            $customers->created_by          = $user_id;

            if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
            {
                $customers->opening_balance   = $data['opening_balance'];
            }

            if($request->hasFile('image'))
            {
                $companyLogo            = $request->file('image');
                $logoName               = time().'1'.".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->image       = $logoUrl;
            }

            if($request->hasFile('nid_image'))
            {
                $companyLogo1            = $request->file('nid_image');
                $logoName1               = time().'2'.".".$companyLogo1->getClientOriginalExtension();
                $directory1              = 'images/customers/';
                $companyLogo1->move(public_path($directory1), $logoName1);
                $logoUrl1                = $directory1.$logoName1;
                $customers->nid          = $logoUrl1;
            }

            if($request->hasFile('certificate'))
            {
                $companyLogo2            = $request->file('certificate');
                $logoName2               = time().'3'.".".$companyLogo2->getClientOriginalExtension();
                $directory2              = 'images/customers/';
                $companyLogo2->move(public_path($directory2), $logoName2);
                $logoUrl2                = $directory2.$logoName2;
                $customers->certificate  = $logoUrl2;
            }

            if ($customers->save())
            {   
                if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
                {   
                    openingBalanceStore($data['opening_balance'], $customers['id'], $data['contact_type']);
                }

                DB::commit();
                
                if ((isset($data['contact_type_reference'])))
                {
                    return redirect()->route('customers_index','contact_type='.$data['contact_type_reference'])->with("success","Contact Added Successfully !!");
                }
                else
                {
                    return back()->with("success","Contact Added Successfully !!");
                }
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('customers::show');
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $branch_id      = Auth::user()->branch_id;
        $find_customer  = Customers::find($id);
        $users          = Users::where('branch_id', $branch_id)
                                ->where('id', '!=', 1)
                                ->orderBy('created_at', 'ASC')
                                ->get();
        $departments    = Departments::orderBy('created_at', 'DESC')->get();
        $sections       = Sections::orderBy('created_at', 'DESC')->get();

        return view('customers::edit', compact('find_customer', 'users', 'departments', 'sections'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                      = Customers::find($id);
            $customers->name                = $data['customer_name'];
            $customers->employee_id         = $data['employee_id'];
            $customers->serial              = $data['serial'];
            $customers->organization_name   = $data['organization_name'];
            $customers->address             = $data['address'];
            $customers->phone               = $data['mobile_number'];
            $customers->nid_number          = $data['nid_number'];
            $customers->alternative_contact = $data['alternative_mobile_number'];
            $customers->contact_type        = $data['contact_type'];
            $customers->joining_date        = date('Y-m-d', strtotime($data['joining_date']));
            $customers->designation         = $data['designation'];
            $customers->salary              = $data['salary'];
            $customers->user_id             = $data['user_id'];
            $customers->department_id       = $data['department_id'];
            $customers->section_id          = $data['section_id'];
            $customers->branch_id           = $branch_id;
            $customers->updated_by          = $user_id;

            if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
            {
                $customers->opening_balance   = $data['opening_balance'];
            }

            if($request->hasFile('image'))
            {
                if ($customers->image != null)
                {
                    unlink('public/'.$customers->image);
                }

                $companyLogo            = $request->file('image');
                $logoName               = time().'1'.".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->image       = $logoUrl;
            }

            if($request->hasFile('nid_image'))
            {
                if ($customers->nid != null)
                {
                    unlink('public/'.$customers->nid);
                }

                $companyLogo1            = $request->file('nid_image');
                $logoName1               = time().'2'.".".$companyLogo1->getClientOriginalExtension();
                $directory1              = 'images/customers/';
                $companyLogo1->move(public_path($directory1), $logoName1);
                $logoUrl1                = $directory1.$logoName1;
                $customers->nid          = $logoUrl1;
            }

            if($request->hasFile('certificate'))
            {
                if ($customers->certificate != null)
                {
                    unlink('public/'.$customers->certificate);
                }

                $companyLogo2            = $request->file('certificate');
                $logoName2               = time().'3'.".".$companyLogo2->getClientOriginalExtension();
                $directory2              = 'images/customers/';
                $companyLogo2->move(public_path($directory2), $logoName2);
                $logoUrl2                = $directory2.$logoName2;
                $customers->certificate  = $logoUrl2;
            }

            if ($customers->save())
            {   
                if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
                {   
                    openingBalanceUpdate($data['opening_balance'], $customers['id'], $data['contact_type']);
                }

                DB::commit();

                if ((isset($data['contact_type_reference'])))
                {
                    return redirect()->route('customers_index','contact_type='.$data['contact_type_reference'])->with("success","Contact Updated Successfully !!");
                }
                else
                {
                    return redirect()->route('customers_index')->with("success","Contact Updated Successfully !!");
                }
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
        $data           = Customers::find($id);
        $data->status   = 0;
        
        if ($data->save())
        {
            return redirect()->route('customers_index', '?contact_type=2')->with("success","Contact Deleted Successfully !!");
        }
        else
        {
            return redirect()->route('customers_index', '?contact_type=2')->with("unsuccess","Something Wrong.Try again. !!");
        }
    }

    public function customerListLoad()
    {
        $branch_id  = Auth::user()->branch_id;
        $data       = Customers::leftjoin('sections', 'sections.id', 'customers.section_id')
                                ->leftjoin('departments', 'departments.id', 'sections.department_id')
                                ->where('customers.branch_id', $branch_id)
                                ->where('customers.status', 1)
                                ->orderBy('customers.serial', 'ASC')
                                ->selectRaw('customers.*, sections.name as section_name, departments.name as department_name')
                                ->get();

        return Response::json($data);
    }

    public function customerListSearch($id)
    {
        $branch_id  = Auth::user()->branch_id;

        if ($id != 'No_Text')
        {
            $data   = Customers::leftjoin('sections', 'sections.id', 'customers.section_id')
                                ->leftjoin('departments', 'departments.id', 'sections.department_id')
                                ->where('customers.name', 'LIKE', "%$id%")
                                ->orWhere('customers.phone', 'LIKE', "%$id%")
                                ->orWhere('customers.alternative_contact', 'LIKE', "%$id%")
                                ->orWhere('customers.address', 'LIKE', "%$id%")
                                ->orWhere('customers.nid_number', 'LIKE', "%$id%")
                                ->where('customers.status', 1)
                                ->orderBy('customers.serial', 'ASC')
                                ->selectRaw('customers.*, sections.name as section_name, departments.name as department_name')
                                ->get();

            $data   = $data->where('branch_id', $branch_id);
        }
        else
        {
            $data   = Customers::leftjoin('sections', 'sections.id', 'customers.section_id')
                                ->leftjoin('departments', 'departments.id', 'sections.department_id')
                                ->where('customers.branch_id', $branch_id)
                                ->where('customers.status', 1)
                                ->orderBy('customers.serial', 'ASC')
                                ->selectRaw('customers.*, sections.name as section_name, departments.name as department_name')
                                ->get();
        }

        return Response::json($data);
    }
    
        public function addTaxHolder()
    {

        $lists = TaxpayerInfo::all();
        return view('customers::add_tax_holder',compact('lists'));
    }

    public function taxpayerInfoStore(Request $request)
    {
        
        $rules = array(
            // 'customer_name'     => 'required',
            // 'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $taxpayers                      = new TaxpayerInfo;
            $taxpayers->holding_no          = $data['holding_no'];
            $taxpayers->taxpayer_id         = $data['taxpayer_id'];
            $taxpayers->taxpayer_name       = $data['taxpayer_name'];
            $taxpayers->name                = $data['taxpayer_name'];
            $taxpayers->area_name           = $data['area_name'];
            $taxpayers->mobile_number       = $data['mobile_number'];
            $taxpayers->phone               = $data['mobile_number'];
            $taxpayers->type                = $data['type'];
            $taxpayers->branch_id           = $branch_id;
            $taxpayers->created_by          = $user_id;
            $taxpayers->save();
            DB::commit();
            return back()->with("success","Information Added Successfully");

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }
    
    public function editTaxHolder($id)
    {
        $lists = TaxpayerInfo::all();
        $data = TaxpayerInfo::find($id);
        return view('customers::edit_tax_holder',compact('lists','data'));
    }

    public function taxpayerInfoUpdate(Request $request,$id)
    {

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $taxpayers                      = TaxpayerInfo::find($id);
            $taxpayers->holding_no          = $data['holding_no'];
            $taxpayers->taxpayer_id         = $data['taxpayer_id'];
            $taxpayers->taxpayer_name       = $data['taxpayer_name'];
            $taxpayers->name                = $data['taxpayer_name'];
            $taxpayers->area_name           = $data['area_name'];
            $taxpayers->mobile_number       = $data['mobile_number'];
            $taxpayers->phone               = $data['mobile_number'];
            $taxpayers->type                = $data['type'];
            $taxpayers->branch_id           = $branch_id;
            $taxpayers->updated_by          = $user_id;
            $taxpayers->save();

            DB::commit();
            return redirect()->route('add_tax_holder')->with("success","Information Updated Successfully");


        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        } 
    }
    
    public function addTradeLicense()
    {
        $lists = TradeLicenseInfo::all();
        return view('customers::add_trade_license',compact('lists'));
    }
    
    public function storeTradeLicense(Request $request)
    {
        $rules = array(
            // 'customer_name'     => 'required',
            // 'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $taxpayers                      = new TradeLicenseInfo;
            $taxpayers->license_no          = $data['license_no'];
            $taxpayers->license_id          = $data['license_id'];
            $taxpayers->business_name       = $data['business_name'];
            $taxpayers->owner_name          = $data['owner_name'];
            $taxpayers->name                = $data['owner_name'];
            $taxpayers->type_of_business    = $data['type_of_business'];
            $taxpayers->ward_no             = $data['ward_no'];
            $taxpayers->area_name           = $data['area_name'];
            $taxpayers->business_address    = $data['business_address'];
            $taxpayers->mobile_number       = $data['mobile_number'];
            $taxpayers->phone               = $data['mobile_number'];
            $taxpayers->branch_id           = $branch_id;
            $taxpayers->created_by          = $user_id;
            $taxpayers->save();
            DB::commit();
            return back()->with("success","Information Added Successfully");

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }
    
    public function EditTradeLicense($id)
    {
        $lists  = TradeLicenseInfo::all();
        $data   = TradeLicenseInfo::find($id);
        return view('customers::edit_trade_license',compact('lists','data'));
    }

    public function UpdateTradeLicense(Request $request,$id)
    {
        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $Trade                      = TradeLicenseInfo::find($id);
            $Trade->license_no          = $data['license_no'];
            $Trade->license_id          = $data['license_id'];
            $Trade->business_name       = $data['business_name'];
            $Trade->owner_name          = $data['owner_name'];
            $Trade->name                = $data['owner_name'];
            $Trade->type_of_business    = $data['type_of_business'];
            $Trade->ward_no             = $data['ward_no'];
            $Trade->area_name           = $data['area_name'];
            $Trade->business_address    = $data['business_address'];
            $Trade->mobile_number       = $data['mobile_number'];
            $Trade->phone               = $data['mobile_number'];
            $Trade->branch_id           = $branch_id;
            $Trade->updated_by          = $user_id;
            $Trade->save();

            DB::commit();
            return redirect()->route('add_trade_license')->with("success","Information Updated Successfully");


        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        } 
    }
}
