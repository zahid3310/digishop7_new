@extends('layouts.app')

@section('title', 'Balance Transfer')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Transfer Balance </h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Balance Transfer</a></li>
                                    <li class="breadcrumb-item active">Balance Transfer</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('balance_transfer_store') }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Transfer Date *</label>
                                        <input id="transfer_date" name="transfer_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">From Branch *</label>
                                        <select style="width: 100%" id="branch_from" name="branch_from" class="form-control select2" required disabled>
                                           <option value="">--Select Branch--</option>
                                           @if($branches->count() > 0)
                                           @foreach($branches as $from_branch)
                                           <option value="{{ $from_branch->id }}" {{ $from_branch['id'] == Auth::user()->branch_id ? 'selected' : '' }}>{{ $from_branch->name }}</option>
                                           @endforeach
                                           @endif
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">To Branch *</label>
                                        <select style="width: 100%" id="branch_to" name="branch_to" class="form-control select2" required>
                                           <option value="">--Select Branch--</option>
                                           @if($branches->count() > 0)
                                           @foreach($branches as $to_branch)
                                           <option value="{{ $to_branch->id }}">{{ $to_branch->name }}</option>
                                           @endforeach
                                           @endif
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Transfer From *</label>
                                        <select style="width: 100%" id="transfer_from" name="transfer_from" class="form-control select2" onchange="availableBalance()" required>
                                           <option value="">--Select Account--</option>
                                           @if($paid_accounts->count() > 0)
                                           @foreach($paid_accounts as $from_account)
                                           <option value="{{ $from_account->id }}">{{ $from_account->account_name }}</option>
                                           @endforeach
                                           @endif
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Transfer To *</label>
                                        <select style="width: 100%" id="transfer_to" name="transfer_to" class="form-control select2" required>
                                           <option value="">--Select Account--</option>
                                           @if($paid_accounts->count() > 0)
                                           @foreach($paid_accounts as $to_account)
                                           <option value="{{ $to_account->id }}">{{ $to_account->account_name }}</option>
                                           @endforeach
                                           @endif
                                        </select>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Amount *</label>
                                        <input type="text" name="amount" class="inner form-control" id="amount" placeholder="Amount" required />

                                        <span id="available_balance" style="color: black;font-size: 14px;color: red;">Available Balance : </span>
                                    </div>

                                    <div class="col-lg-5 col-md-5 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Note</label>
                                        <input type="text" name="note" class="inner form-control" id="note" placeholder="Transfer Note" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('balance_transfer_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Transfers</h4>

                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Date</th>
                                            <th>Transfer From</th>
                                            <th>Transfer To</th>
                                            <th>Note</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($transfers) && ($transfers->count() > 0))
                                        @foreach($transfers as $key => $transfer)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ date('d-m-Y', strtotime($transfer['transaction_date'])) }}</td>
                                                <td>{{ $transfer->transferFrom->account_name }} <br> {{ $transfer->branch_from != null ? $transfer->branchFrom->name : '' }}</td>
                                                <td>{{ $transfer->transferTo->account_name }} <br> {{ $transfer->branch_to != null ? $transfer->branchTo->name : '' }}</td>
                                                <td>{{ $transfer->note }}</td>
                                                <td>{{ $transfer->amount }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('balance_transfer_edit', $transfer['id']) }}">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    function availableBalance()
    {   
        var site_url    = $(".site_url").val();
        var branceId    = $("#branch_from").val();
        var accountId   = $("#transfer_from").val();

        $.get(site_url + '/balancetransfer/get-available-balance/'+ branceId + '/' + accountId, function(data){

            $("#available_balance").html();
            $("#available_balance").html('Available Balance : ' + data);

        });
    }
</script>
@endsection