<!DOCTYPE html>
<html>

<head>
    <title>Trial Balance</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Trial Balance</h6>
                    </div>

                    <div class="ibox-content">
                        <!-- Header Start -->
                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Header End -->

                        <!-- Sales Statement Start -->
                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">SL</th>
                                    <th style="text-align: center;width: 10%">ACCOUNT HEAD</th>
                                    <th style="text-align: center;width: 10%">INCOME</th>
                                    <th style="text-align: center;width: 10%">EXPENSE</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php
                                $total_income   = 0;
                                $total_expense  = 0;
                                $balance        = 0;
                                ?>
                                <tr>
                                    <th colspan="3" style="text-align: right;">Opening Balance</th>
                                    <th style="text-align: right;">{{ number_format($opening_result,0,'.',',') }}</th>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">1</td>
                                    <td style="text-align: left;">Sales</td>
                                    <td style="text-align: right;">{{ number_format($result['sales'],0,'.',',') }}</td>
                                    <td style="text-align: right;">0</td>
                                </tr>
                                <?php
                                    $total_income   = $total_income + $result['sales'];
                                    $balance        = $balance + $result['sales'];
                                ?>


                                <tr>
                                    <td style="text-align: center;">2</td>
                                    <td style="text-align: left;">Sales Return</td>
                                    <td style="text-align: right;">0</td>
                                    <td style="text-align: right;">{{ number_format($result['sales_return'],0,'.',',') }}</td>
                                </tr>
                                <?php
                                    $total_expense  = $total_expense + $result['sales_return'];
                                    $balance        = $balance - $result['sales_return'];
                                ?>


                                <tr>
                                    <td style="text-align: center;">3</td>
                                    <td style="text-align: left;">Purchase</td>
                                    <td style="text-align: right;">0</td>
                                    <td style="text-align: right;">{{ number_format($result['purchase'],0,'.',',') }}</td>
                                </tr>
                                <?php
                                    $total_expense  = $total_expense + $result['purchase'];
                                    $balance        = $balance - $result['purchase'];
                                ?>


                                <tr>
                                    <td style="text-align: center;">4</td>
                                    <td style="text-align: left;">Purchase Return</td>
                                    <td style="text-align: right;">{{ number_format($result['purchase_return'],0,'.',',') }}</td>
                                    <td style="text-align: right;">0</td>
                                </tr>
                                <?php
                                    $total_income   = $total_income + $result['purchase_return'];
                                    $balance        = $balance + $result['purchase_return'];
                                    $key1           = 5;
                                ?>


                                @foreach($result['income'] as $cat_name => $income)
                                <tr>
                                    <td style="text-align: center;">{{ $key1 }}</td>
                                    <td style="text-align: left;">{{ $cat_name }}</td>
                                    <td style="text-align: right;">{{ number_format($income->sum('amount'),0,'.',',') }}</td>
                                    <td style="text-align: right;">0</td>
                                </tr>
                                <?php
                                    $total_income   = $total_income + $income->sum('amount');
                                    $balance        = $balance + $income->sum('amount');
                                    $key1++;
                                ?>
                                @endforeach

                                @foreach($result['expense'] as $cat_name_exp => $expense)
                                <tr>
                                    <td style="text-align: center;">{{ $key1 }}</td>
                                    <td style="text-align: left;">{{ $cat_name_exp }}</td>
                                    <td style="text-align: right;">0</td>
                                    <td style="text-align: right;">{{ number_format($expense->sum('amount'),0,'.',',') }}</td>
                                </tr>
                                <?php
                                    $total_expense  = $total_expense + $expense->sum('amount');
                                    $balance        = $balance - $expense->sum('amount');
                                    $key1++;
                                ?>
                                @endforeach
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="2" style="text-align: right;">Total</th>
                                    <th style="text-align: right;">{{ number_format($total_income,0,'.',',') }}</th>
                                    <th style="text-align: right;">{{ number_format($total_expense,0,'.',',') }}</th>
                                </tr>
                                <tr>
                                    <th colspan="3" style="text-align: right;">Balance</th>
                                    <th style="text-align: right;">{{ number_format($balance,0,'.',',') }}</th>
                                </tr>
                                <tr>
                                    <th colspan="3" style="text-align: right;">Net Balance</th>
                                    <th style="text-align: right;">{{ number_format($balance + $opening_result,0,'.',',') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- Sales Statement End -->

                        <!-- Footer Start -->
                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>