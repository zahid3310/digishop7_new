<?php

namespace Modules\StockTransfers\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

use App\Models\StockTransfers;
use App\Models\Branches;
use App\Models\ProductEntries;
use App\Models\BranchInventories;
use App\Models\UnitConversions;
use Response;
use DB;

class StockTransfersController extends Controller
{
    public function index()
    {
        $branch_id          = Auth()->user()->branch_id;
        $branches           = Branches::orderBy('created_at', 'ASC')->get();
        $transfers          = StockTransfers::where('stock_transfers.transfer_from', $branch_id)
                                            ->orderBy('date', 'DESC')
                                            ->get();

        return view('stocktransfers::index', compact('transfers', 'branches'));
    }

    public function create()
    {
        return view('stocktransfers::create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'transfer_date'     => 'required',
            'product_entries.*' => 'required',
            'transfer_to'       => 'required',
            'quantity'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            foreach ($data['product_entries'] as $key => $value)
            {
                $item                           = ProductEntries::find($value);

                if ($item['type'] != 1)
                {
                    $conversion_rate_find = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key])
                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                        ->where('product_entry_id', $value)
                                        ->first();

                    $converted_quantity   = $conversion_rate_find != null ? $data['quantity'][$key]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key];
                }
                else
                {
                    $converted_quantity   = $data['quantity'][$key];
                }

                $transfer                       = new StockTransfers;
                $transfer->date                 = date('Y-m-d', strtotime($data['transfer_date']));
                $transfer->product_entry_id     = $value;
                $transfer->transfer_from        = $branch_id;
                $transfer->transfer_to          = $data['transfer_to'];
                $transfer->quantity             = $data['quantity'][$key];
                $transfer->cartoon              = $data['cartoon'][$key];
                $transfer->pcs                  = $data['pcs'][$key];
                $transfer->main_unit_id         = $data['main_unit_id'][$key];
                $transfer->conversion_unit_id   = $data['unit_id'][$key];
                $transfer->note                 = $data['note'][$key];
                $transfer->branch_id            = $branch_id;
                $transfer->created_by           = $user_id;

                if ($transfer->save())
                {   
                    $find_from_branch_inventory = BranchInventories::where('product_entry_id', $value)
                                                    ->where('branch_id', $branch_id)
                                                    ->first();

                    $find_to_branch_inventory   = BranchInventories::where('product_entry_id', $value)
                                                    ->where('branch_id', $data['transfer_to'])
                                                    ->first();

                    //Transfer From Start
                    if ($branch_id == 1)
                    {
                        $from_branch_item                   = ProductEntries::find($value);
                        $from_branch_item->stock_in_hand    = $from_branch_item['stock_in_hand'] - $converted_quantity;

                        if ($from_branch_item['stock_in_hand'] <= 0)
                        {
                            DB::rollback();
                            return back()->with('unsuccess', 'You do not have enough stock to transfer !!');
                        }
                        else
                        {
                            $from_branch_item->save();
                        }
                    }
                    else
                    {
                        if ($find_from_branch_inventory == null)
                        {
                            DB::rollback();
                            return back()->with('unsuccess', 'Do not have available stock to transfer');
                        }
                        else
                        {
                            if ($find_from_branch_inventory['stock_in_hand'] < $converted_quantity)
                            {
                                DB::rollback();
                                return back()->with('unsuccess', 'You do not have enough stock to transfer !!');
                            }

                            $find_from_branch_inventory->stock_in_hand  = $find_from_branch_inventory['stock_in_hand'] - $converted_quantity;
                            $find_from_branch_inventory->save();
                        }
                    }
                    //Transfer From End

                    //Transfer To Start
                    if ($data['transfer_to'] == 1)
                    {
                        $to_branch_item                     = ProductEntries::find($value);
                        $to_branch_item->stock_in_hand      = $to_branch_item['stock_in_hand'] + $converted_quantity;
                        $to_branch_item->save();
                    }
                    else
                    {
                        if ($find_to_branch_inventory == null)
                        {
                            $new_to_branch_item                     = new BranchInventories;
                            $new_to_branch_item->product_entry_id   = $value;
                            $new_to_branch_item->branch_id          = $data['transfer_to'];
                            $new_to_branch_item->stock_in_hand      = $converted_quantity;
                            $new_to_branch_item->created_by         = $user_id;
                            $new_to_branch_item->save();
                        }
                        else
                        {
                            $find_to_branch_inventory->stock_in_hand     = $find_to_branch_inventory['stock_in_hand'] + $converted_quantity;
                            $find_to_branch_inventory->save();
                        }
                    }
                    //Transfer To End
                }
            }

            DB::commit();
            return redirect()->route('stock_transfer_index')->with("success","Transfer Created Successfully !!");

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        return view('stocktransfers::show');
    }
    
    public function transferList($data_date)
    {
        $branch_id  = Auth()->user()->branch_id;
        $branches   = Branches::orderBy('created_at', 'DESC')->get();
        
        if($branch_id == 1)
        {                  
            $transfers  = StockTransfers::leftjoin('product_entries', 'product_entries.id', 'stock_transfers.product_entry_id')
                                ->leftjoin('units', 'units.id', 'stock_transfers.conversion_unit_id')
                                ->select('stock_transfers.*', 'product_entries.name', 'product_entries.type as type', 'units.name as unit_name')
                                ->when($branch_id != 5, function ($query) use ($branch_id) {
                                    return $query->where('stock_transfers.transfer_from', $branch_id);
                                })
                                ->orderBy('stock_transfers.created_at', 'DESC')
                                ->where('stock_transfers.date', $data_date)
                                ->get();
        }
        else
        {
            $transfers  = StockTransfers::leftjoin('product_entries', 'product_entries.id', 'stock_transfers.product_entry_id')
                                ->leftjoin('units', 'units.id', 'stock_transfers.conversion_unit_id')
                                ->select('stock_transfers.*', 'product_entries.name', 'product_entries.type as type', 'units.name as unit_name')
                                ->when($branch_id != 1, function ($query) use ($branch_id) {
                                    return $query->where('stock_transfers.transfer_from', $branch_id);
                                })
                                ->orderBy('stock_transfers.created_at', 'DESC')
                                ->where('stock_transfers.date', $data_date)
                                ->get();
        }

         return Response::json($transfers);
    }

    public function edit($id)
    {
        $branches           = Branches::orderBy('created_at', 'DESC')->get();
        $transfers          = StockTransfers::orderBy('created_at', 'DESC')->get();
        $find_transfer      = StockTransfers::find($id);

        return view('stocktransfers::edit', compact('branches', 'transfers', 'find_transfer'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'transfer_date' => 'required',
            'transfer_from' => 'required',
            'product_id'    => 'required',
            'transfer_to'   => 'required',
            'quantity'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $old_transfer                   = StockTransfers::find($id);
            $transfer                       = StockTransfers::find($id);
            $transfer->date                 = date('Y-m-d', strtotime($data['transfer_date']));
            $transfer->product_entry_id     = $data['product_id'];
            $transfer->transfer_from        = $data['transfer_from'];
            $transfer->transfer_to          = $data['transfer_to'];
            $transfer->quantity             = $data['quantity'];
            $transfer->note                 = $data['note'];
            $transfer->main_unit_id         = $data['main_unit_id'];
            $transfer->conversion_unit_id   = $data['unit_id'];
            $transfer->updated_by           = $user_id;

            if ($transfer->save())
            {   
                $find_from_branch_inventory = BranchInventories::where('product_entry_id', $data['product_id'])
                                                                ->where('branch_id', $data['transfer_from'])
                                                                ->first();

                $find_to_branch_inventory   = BranchInventories::where('product_entry_id', $data['product_id'])
                                                                ->where('branch_id', $data['transfer_to'])
                                                                ->first();

                if ($data['transfer_from'] == 1)
                {
                    $from_branch_item                   = ProductEntries::find($data['product_id']);
                    $conversion_rate_find               = UnitConversions::where('main_unit_id', $from_branch_item['unit_id'])
                                                                    ->where('converted_unit_id', $data['unit_id'])
                                                                    ->where('product_entry_id', $data['product_id'])
                                                                    ->first();

                    $converted_quantity                 = $conversion_rate_find != null ?  $data['quantity']/$conversion_rate_find['conversion_rate'] : $data['quantity'];
                    $from_branch_item->stock_in_hand    = $from_branch_item['stock_in_hand'] + $old_transfer['stock_in_hand'] - $conversion_rate_find;

                    if ($from_branch_item['stock_in_hand'] < 0)
                    {
                        return back()->with('unsuccess', 'You do not have enough stock to transfer !!');
                    }
                    else
                    {
                        $from_branch_item->save();
                    }
                }
                else
                {
                    if ($find_from_branch_inventory == null)
                    {
                        DB::rollback();
                        return back()->with('unsuccess', 'Do not have available stock to transfer');
                    }
                    else
                    {
                        $from_branch_item                           = ProductEntries::find($data['product_id']);
                        $branch_conversion_rate_find                = UnitConversions::where('main_unit_id', $from_branch_item['unit_id'])
                                                                                        ->where('converted_unit_id', $data['unit_id'])
                                                                                        ->where('product_entry_id', $data['product_id'])
                                                                                        ->first();

                        $branch_converted_quantity                  = $branch_conversion_rate_find != null ?  $data['quantity']/$branch_conversion_rate_find['conversion_rate'] : $data['quantity'];
                        $find_from_branch_inventory->stock_in_hand  = $find_from_branch_inventory['stock_in_hand'] + $old_transfer['stock_in_hand'] - $branch_converted_quantity;

                        if ($from_branch_item['stock_in_hand'] < 0)
                        {
                            return back()->with('unsuccess', 'You do not have enough stock to transfer !!');
                        }
                        else
                        {
                            $find_from_branch_inventory->save();
                        }
                    }
                }

                if ($data['transfer_to'] == 1)
                {
                    $to_branch_item                     = ProductEntries::find($data['product_id']);
                    $to_conversion_rate_find            = UnitConversions::where('main_unit_id', $to_branch_item['unit_id'])
                                                                    ->where('converted_unit_id', $data['unit_id'])
                                                                    ->where('product_entry_id', $data['product_id'])
                                                                    ->first();

                    $to_converted_quantity              = $to_conversion_rate_find != null ?  $data['quantity']/$to_conversion_rate_find['conversion_rate'] : $data['quantity'];
                    $to_branch_item->stock_in_hand      = $from_branch_item['stock_in_hand'] - $old_transfer['stock_in_hand'] + $to_converted_quantity;
                    $to_branch_item->save();
                }
                else
                {
                    if ($find_to_branch_inventory == null)
                    {
                        $new_to_branch_item                    = new BranchInventories;
                        $new_to_branch_item->product_entry_id  = $data['product_id'];
                        $new_to_branch_item->branch_id         = $data['transfer_to'];
                        $new_to_branch_item->stock_in_hand     = $data['quantity'];
                        $new_to_branch_item->created_by        = $user_id;
                        $new_to_branch_item->save();
                    }
                    else
                    {   
                        $to_branch_item                     = ProductEntries::find($data['product_id']);
                        $to_branch_conversion_rate_find     = UnitConversions::where('main_unit_id', $to_branch_item['unit_id'])
                                                                    ->where('converted_unit_id', $data['unit_id'])
                                                                    ->where('product_entry_id', $data['product_id'])
                                                                    ->first();

                        $to_branch_converted_quantity       = $to_branch_conversion_rate_find != null ?  $data['quantity']/$to_branch_conversion_rate_find['conversion_rate'] : $data['quantity'];
                        $find_to_branch_inventory->stock_in_hand     = $find_to_branch_inventory['stock_in_hand'] - $data['quantity'] + $to_branch_converted_quantity;
                        $find_to_branch_inventory->save();
                    }
                }

                DB::commit();
                return redirect()->route('stock_transfer_index')->with("success","Transfer Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function destroy($id)
    {
    }
}
