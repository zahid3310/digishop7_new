<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="<?php echo e(route('home')); ?>" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                
                <li class="<?php echo e(Route::currentRouteName() == 'attendance_manual_attendance_index' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Route::currentRouteName() == 'attendance_manual_attendance_index' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fas fa-fingerprint"></i><span>Attendance</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('attendance_manual_attendance_index')); ?>">Manual Attendance</a> </li>
                    </ul>
                </li>
                
                <li>
                    <a href="<?php echo e(route('leave_application_create')); ?>" class="waves-effect">
                        <i class="fa fa-list"></i>
                        <span>Leave Application</span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo e(route('leave_application_index')); ?>" class="waves-effect">
                        <i class="fa fa-list"></i>
                        <span>Leave Application List</span>
                    </a>
                </li>

                <li class="<?php echo e(Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'pay_slip_show' ? 'mm-active' : '' || Route::currentRouteName() == 'pay_slip_all_show' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'pay_slip_show' ? 'mm-active' : '' || Route::currentRouteName() == 'pay_slip_all_show' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fas fa-money-check-alt"></i><span>Payroll</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('process_monthly_salary_index')); ?>">List of Salary Sheet</a> </li>
                        <li> <a href="<?php echo e(route('process_monthly_salary_create')); ?>">Process Salary Sheet</a> </li>
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'pay_slip_all_show' ? 'mm-active' : ''); ?>" href="<?php echo e(route('pay_slip_index')); ?>">Pay Slip</a> </li>
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'pay_slip_show' ? 'mm-active' : ''); ?>" href="<?php echo e(route('pay_slip_list')); ?>">List of Pay Slip</a> </li>
                    </ul>
                </li>
                
                <li>
                    <a class="has-arrow waves-effect">
                        <i class="far fa-envelope"></i><span>Messaging</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('messages_send_index')); ?>">Send Message</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>Reports</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('monthly_alary_report_index')); ?>">Monthly Salary Sheet</a> </li>
                        <li> <a href="<?php echo e(route('attendance_report_index')); ?>">Daily Attendance Report</a> </li>
                        <li> <a href="<?php echo e(route('attendance_sheet_index')); ?>">Monthly Summary Report</a> </li>
                        <li> <a href="<?php echo e(route('register_list_index').'?type=2'); ?>" target="_blank">Employee List</a> </li>
                    </ul>
                </li>

                <li class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' ||
                Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' ||
                Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' ||
                Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' ||
                Request::getQueryString() == 'contact_type=4' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'grades_index' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'grades_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'users_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'users_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'department_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'section_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'depasection_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'leave_category_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'leave_category_create' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'shifts_index' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'shifts_create' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'duty_roster_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'holiday_calender_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'holiday_calender_create' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'holiday_calender_edit' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' ||
                    Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || 
                    Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || 
                    Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || 
                    Request::getQueryString() == 'contact_type=4' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'grades_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'grades_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'users_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'users_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'department_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'section_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'depasection_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'leave_category_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'leave_category_create' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'shifts_index' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'shifts_create' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'duty_roster_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'holiday_calender_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'holiday_calender_create' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'holiday_calender_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>Basic Settings</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">

                        <li class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'department_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'department_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Registers
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="<?php echo e(Request::getQueryString() == 'contact_type=2' ? 'mm-active' : ''); ?>" href="<?php echo e(route('customers_index').'?contact_type=2'); ?>">Add Employee</a> </li>
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'department_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('department_index')); ?>">Add Department</a> </li>
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'section_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('section_index')); ?>">Add Section</a> </li>
                                <!--<li> <a class="<?php echo e(Route::currentRouteName() == 'add_tax_holder' ? 'mm-active' : ''); ?>" href="<?php echo e(route('add_tax_holder')); ?>">Add Tax Holder</a> </li>-->
                                <!--<li> <a class="<?php echo e(Route::currentRouteName() == 'add_trade_license' ? 'mm-active' : ''); ?>" href="<?php echo e(route('add_trade_license')); ?>">Add Trade License</a> </li>-->
                            </ul>
                        </li>
                        
                        <li>
                            <a class="has-arrow waves-effect">
                                Messaging
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('messages_index')); ?>">Create Text Message</a> </li>
                                <li> <a href="<?php echo e(route('messages_phone_book_index')); ?>">Phone Book</a> </li>
                            </ul>
                        </li>
                        
                        <li class="<?php echo e(Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Security System
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('users_index')); ?>">Add User</a> </li>
                                <li> <a href="<?php echo e(route('users_index_all')); ?>">List of User</a> </li>
                                <li> <a href="<?php echo e(route('set_access_index')); ?>">Set Permission</a> </li>
                            </ul>
                        </li>
                        
                        <li class="<?php echo e(Route::currentRouteName() == 'leave_category_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_create' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'leave_category_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_create' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Leave Settings
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li class="<?php echo e(Route::currentRouteName() == 'leave_category_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_create' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : ''); ?>">
                                    <a class="<?php echo e(Route::currentRouteName() == 'leave_category_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_create' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                        Leave Category
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="<?php echo e(Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('leave_category_index')); ?>">List of Category</a> </li>
                                        <li> <a href="<?php echo e(route('leave_category_create')); ?>">New Category</a> </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="<?php echo e(Route::currentRouteName() == 'salary_grades_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_grades_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' ||
                                      Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' ||
                                      Route::currentRouteName() == 'salary_increaments_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_increaments_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'salary_grades_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_grades_create' ? 'mm-active' : ''|| Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'salary_increaments_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_increaments_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Payroll Settings
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li class="<?php echo e(Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : ''); ?>">
                                    <a class="<?php echo e(Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                        Grades
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="<?php echo e(Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('salary_grades_index')); ?>">List of Grades</a> </li>
                                        <li> <a href="<?php echo e(route('salary_grades_create')); ?>">New Grade</a> </li>
                                    </ul>
                                </li>

                                <li class="<?php echo e(Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : ''); ?>">
                                    <a class="<?php echo e(Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                        Salary Statements
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="<?php echo e(Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('salary_statements_index')); ?>">List of Statements</a> </li>
                                        <li> <a href="<?php echo e(route('salary_statements_create')); ?>">New Statement</a> </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="<?php echo e(Route::currentRouteName() == 'holiday_calender_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'holiday_calender_create' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'holiday_calender_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'holiday_calender_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'holiday_calender_create' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'holiday_calender_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Holiday Calender
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li class="<?php echo e(Route::currentRouteName() == 'holiday_calender_edit' ? 'mm-active' : ''); ?>"> <a class="<?php echo e(Route::currentRouteName() == 'holiday_calender_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('holiday_calender_index')); ?>">List of Holidays</a> </li>
                                <li> <a href="<?php echo e(route('holiday_calender_create')); ?>">Add Holiday</a> </li>
                            </ul>
                        </li>
                        
                        <li class="<?php echo e(Route::currentRouteName() == 'shifts_index' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'shifts_create' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'shifts_index' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'shifts_create' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Shift
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('shifts_index')); ?>">List of Shift</a> </li>
                                <li> <a href="<?php echo e(route('shifts_create')); ?>">Add Shift</a> </li>
                            </ul>
                        </li>
                        
                        <li class="<?php echo e(Route::currentRouteName() == 'duty_roster_index' ? 'mm-active' : '' || Route::currentRouteName() == 'general_roster_index' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'duty_roster_index' ? 'mm-active' : '' || Route::currentRouteName() == 'general_roster_index' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Duty Roster
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('duty_roster_index')); ?>">Add Roster</a> </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div><?php /**PATH /home/digishop7/public_html/ta_hrm/resources/views/layouts/headers.blade.php ENDPATH**/ ?>