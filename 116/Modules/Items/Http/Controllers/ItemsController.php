<?php

namespace Modules\Items\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tables         = TableNameByUsers();
        $table_id       = Auth::user()->associative_contact_id;

        $sub_categories = $tables['sub_categories']->where('status', 1)->orderBy('created_at', 'DESC')->get();
        $items          = $tables['items']
                                ->leftjoin('sub_categories_'.$table_id.'', 'sub_categories_'.$table_id.'.id', 'items_'.$table_id.'.sub_category_id')
                                ->orderBy('items_'.$table_id.'.created_at', 'DESC')
                                ->select('items_'.$table_id.'.*',
                                         'sub_categories_'.$table_id.'.name as sub_category_name')
                                ->get();

        return view('items::index', compact('sub_categories', 'items'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $tables                 = TableNameByUsers();
            $item                   = $tables['items'];
            $item->name             = $data['name'];
            $item->sub_category_id  = $data['sub_category_id'];
            $item->status           = $data['status'];
            $item->created_by       = $user_id;

            if ($item->save())
            {   
                return back()->with("success","Sub Category Created Successfully !!");
            }else
            {
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){

            return back()->with("unsuccess","Not Added");
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $tables             = TableNameByUsers();
        $table_id           = Auth::user()->associative_contact_id;

        $sub_categories     = $tables['sub_categories']->where('status', 1)->orderBy('created_at', 'DESC')->get();
        $items              = $tables['items']
                                    ->leftjoin('sub_categories_'.$table_id.'', 'sub_categories_'.$table_id.'.id', 'items_'.$table_id.'.sub_category_id')
                                    ->orderBy('items_'.$table_id.'.created_at', 'DESC')
                                    ->select('items_'.$table_id.'.*',
                                             'sub_categories_'.$table_id.'.name as sub_category_name')
                                    ->get();

        $find_item          = $tables['items']
                                    ->leftjoin('sub_categories_'.$table_id.'', 'sub_categories_'.$table_id.'.id', 'items_'.$table_id.'.sub_category_id')
                                    ->orderBy('items_'.$table_id.'.created_at', 'DESC')
                                    ->select('items_'.$table_id.'.*',
                                             'sub_categories_'.$table_id.'.id as sub_category_id',
                                             'sub_categories_'.$table_id.'.name as sub_category_name')
                                    ->find($id);

        return view('items::edit', compact('sub_categories', 'items', 'find_item'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $tables                 = TableNameByUsers();
            $item                   = $tables['items']->find($id);
            $item->name             = $data['name'];
            $item->sub_category_id  = $data['sub_category_id'];
            $item->status           = $data['status'];
            $item->updated_by       = $user_id;

            if ($item->save())
            {   
                return redirect()->route('items_index')->with("success","Item Updated Successfully !!");
            }else
            {
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){

            return back()->with("unsuccess","Not Added");
        }
    }
}
