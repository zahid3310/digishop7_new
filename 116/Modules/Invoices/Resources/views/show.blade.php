@extends('layouts.app')

@section('title', 'Print Cash Memo')

<style type="text/css">

.jol {
    position: absolute;
    left: 28%;
    height: 270px;
    top: 110px;
    opacity: 0.30;
    display: none;
}

address {
    margin-bottom: 0!important;
}
/*u {
    text-decoration-style: dotted!important;
    text-underline-offset: 1px;
}*/

.underline {
  text-decoration: none;
  background-image: linear-gradient(to right, #000 75%, transparent 75%);
  background-position: 0 1.04em;
  background-repeat: repeat-x;
  background-size: 8px 3px;
}

.table td, .table th {
    padding: 1.00rem!important;
}

.table-bordered thead td, .table-bordered thead th {
    border-bottom-width: 1px!important;
}

.table thead th {
    vertical-align: bottom;
    border-bottom: 1px solid #000!important;
}

.table-bordered td, .table-bordered th {
    border: 1px solid #000!important;
}


 @media print {
     
        address {
            margin-bottom: 0!important;
        }

        .table td, .table th {
            padding: 1.00rem!important;
 
        }
        
        .table-bordered tbody td, .table-bordered thead th {
            border-bottom-width: 1px!important;
        }
        
        .table thead th {
            vertical-align: bottom;
            border: 2px solid #000!important;
        }
        
        .table-bordered tbody td, .table-bordered th {
            border: 1px solid #000!important;
        } 
        body {
            -webkit-print-color-adjust: exact;
        }
    }

</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Cash Memo</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Cash Memo</a></li>
                                    <li class="breadcrumb-item active">Print</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class=" row " style="display: flex; width:100%;padding-top: 20px;">
                                <div style="width: 30%;text-align:center;">
                                    <img src="{{ url('public/cash_memo/bsrm.jpg') }}" alt="" style="height: 50px;width: 100px;">
                                    
                                </div>
                                <div style="width: 40%;text-align:center;">
                                    <span style="font-size: 25px;">বিসমিল্লাহির রাহমানির রাহীম </span> <br>
                                    <span style="    font-size: 15px;background-color: rgb(34, 34, 34);color: white;padding: 3px 14px;border-radius: 30%;">ক্যাশ মেমো </span>
                                </div>
                                <div style="width: 30%;text-align:center;">
                                    <img src="{{ url('public/cash_memo/holcim_img.png') }}" alt="" style="height: 50px;width: 100px;">
                                </div>
                            </div>

                            <div class=" row" style="display: flex; width:100%">
                                <div style="width: 15%;text-align: right;">
                                    <img src="{{ url('public/'.userDetails()->logo) }}" alt="" style="height: 100px;width: 100px;">
                                </div>
                                <div style="width: 70%;text-align:center;">
                                    <span style="font-size: 50px;">আল-রোয়াদ এন্টারপ্রাইজ</span> <br>
                                    <span style="font-size: 20px;">প্রোঃ আব্দুল অদুদ </span> <br>
                                    {{-- <span style="    font-size: 15px;background-color: rgb(34, 34, 34);color: white;padding: 3px 14px;border-radius: 30%;">ক্যাশ মেমো </span> --}}
                                </div>
                                <div style="width: 15%;text-align: left;">
                                    <img src="{{ url('public/cash_memo/holcim.png') }}" alt="" style="height: 100px;width: 100px;">
                                </div>
                            </div>
                            <div>
                                <p style="padding: 2px 44px;font-size: 22px;text-align: center;">
                                    এখানে রড, সিমেন্ট, থাই  এলমুনিয়াম, স্টিলের আসবাবপত্র তৈরী, এস. এস. সীটের যাবতীয় কাজ, প্লাস্টিকের দরজা, প্লেটবার, এঞ্জেল, সিট পাইকারি ও খুচরা বিক্রয় এবং অর্ডার সরবরাহ করা হয়।  
                                </p>
                            </div>
                            <div>
                                <p style="text-align: center;background-color: black;color: white;font-size: 17px;padding: 4px 0px;">ইসমাইল ম্যানশন, বুড়িচং দক্ষিণ বাজার, বুড়িচং, কুমিল্লা।</p>
                            </div>
                            <div class="card-body row" style="display: flex; width:100%;padding-top: 0px;">
                                <div style="width: 15%;text-align: start;">
                                    <span style="font-size: 15px;padding-left:20px;">নং-&nbsp; {{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }} </span>
                                </div>
                                <div style="width: 67%;text-align:center;">
                                    <span style="font-size: 16px;">মোবাঃ 01914-743313, 01715-946804, &nbsp; বিজয়ঃ 01815-278128</span>
                                    
                                    {{-- <span style="    font-size: 15px;background-color: rgb(34, 34, 34);color: white;padding: 3px 14px;border-radius: 30%;">ক্যাশ মেমো </span> --}}
                                </div>
                                <div style="width: 18%;text-align: right;">
                                    <span style="font-size: 16px;">তারিখঃ &nbsp; {{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}</span>
                                </div>
                            </div>

                            <div style="margin: 0px 20px;">
                                <div class="card-body row" style="padding-top: 0px;padding-bottom: 5px;">
                                    <div style="display: flex;width:100%;">
                                        <span style="width: 9%;padding-left: 38px;font-size: 16px;background-color: black;color: white;padding-bottom: 2px;padding-top: 2px;">নামঃ &nbsp;</span>
                                        <span style="width: 91%;border: 1px solid;margin-left: 10px;font-size: 16px;font-weight: 600;">{{ $invoice['customer_name'] }}</span>
                                    </div>
                                </div>
    
                                <div class="card-body row" style="padding-top: 0px;">
                                    <div style="display: flex;width:100%;">
                                        <span style="width: 9%;padding-left: 18px;font-size: 16px;background-color: black;color: white;padding-bottom: 2px;padding-top: 2px;">ঠিকানাঃ &nbsp;</span>
                                        <span style="width: 91%;border: 1px solid;margin-left: 10px;font-size: 16px;font-weight: 600;">{{ $invoice->customer->address != null ? $invoice->customer->address : ''  }}</span>
                                    </div>
                                </div>
                            </div>

                                <div class="row" style="padding-top:20px;">
                                    
                                    <div class="table-responsive">
                                    
                                    
                                    <center>
                                        <table class="table table-nowrap table-bordered" style="line-height: 13px;width: 990px;margin: 0px 25px;">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th class="text-center" style="width:10%">নং</th>
                                                <th class="text-center" style="width:50%">মালের বিবরণ</th>
                                                <th class="text-center" style="width:10%">পরিমাণ</th>
                                                <th class="text-center" style="width:10%">দর</th>
                                                <th class="text-center" style="width:20%">টাকা</th>
                                            </tr>
                                        @if($entries->count() > 0)

                                        <?php
                                            $total_amount                   = 0;
                                        ?>

                                        @foreach($entries as $key => $value)
                                        <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $variation_name = ProductVariationName($value['product_entry_id']);

                                            if ($value['product_code'] != null)
                                            {
                                                $productCode  = ' - '.$value['product_code'];
                                            }
                                            else
                                            {
                                                $productCode  = '';
                                            }

                                            if ($value['product_name'] != null)
                                            {
                                                $category  = ' - '.$value['product_name'];
                                            }
                                            else
                                            {
                                                $category  = '';
                                            }

                                            if ($value['brand_name'] != null)
                                            {
                                                $brandName  = $value['brand_name'];
                                            }
                                            else
                                            {
                                                $brandName  = '';
                                            }

                                            if ($value['unit_name'] != null)
                                            {
                                                $unit  = ' '.$value['unit_name'];
                                            }
                                            else
                                            {
                                                $unit  = '';
                                            }

                                            if ($variation_name != null)
                                            {
                                                $variation  = ' '.$variation_name;
                                            }
                                            else
                                            {
                                                $variation  = '';
                                            }

                                            $pre_dues = $invoice['previous_due'];
                                            $net_paya = round($total_amount, 2);
                                            $paid     = round($invoice['cash_given'], 2);
                                            $dues     = round($net_paya - $paid, 2);
                                        ?>
                                        
                                        <tr class="tr-height">
                                            <td style="text-align: center;border:1px solid #000!important">{{ $key + 1 }}</td>
                                            <td style="padding-left: 30px;border:1px solid #000!important">{{ $value['product_entry_name'] . $productCode }}</td>
                                            <td style="text-align: center;border:1px solid #000!important">{{ $value['quantity'] }}</td>
                                            <td style="text-align: center;border:1px solid #000!important">{{ $value['rate'] . $unit }}</td>
                                            <td style="text-align: center;border:1px solid #000!important">{{ round($value['total_amount'], 2) }}</td>
                                        </tr>
                                        @endforeach
                                        @endif
                                           
                                        </thead>
                                        <tbody style="font-size: 16px">
                                            <tr style="">
                                                <td style="font-size: 16px" rowspan="5" class="text-left"></td>
                                               
                                                <td style="font-size: 16px" rowspan="5" class="text-left"></td>
                                                <td style="font-size: 16px;" rowspan="5" class="text-left"></td>
                                            </tr>
                                            
                                            <tr>
                                                <td style="font-size: 16px;height:600px" class="text-right"></td>
                                                <td style="font-size: 16px" class="text-left"></td>
                                            </tr>
                                            <tr >
                                                <td style="font-size: 16px" class="text-right">লেবার খরচ -</td>
                                                <td style="font-size: 16px" class="text-left">{{ $invoice['labor_cost'] }}</td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 16px" class="text-right">মোট  -</td>
                                                <td style="font-size: 16px" class="text-left">{{ $net_paya != 0 ? round($net_paya - $invoice['total_discount']) : '' }}</td>
                                            </tr>
                                            <tr style="">
                                                <td style="font-size: 16px" class="text-right">অগ্রিম -</td>
                                                <td style="font-size: 16px" class="text-left">{{ $paid != 0 ? round($paid, 2) : '' }}</td>
                                            </tr>

                                            <tr>
                                                <td style="font-size: 16px" colspan="3" class="text-left">কথায়ঃ </td>
                                                <td style="font-size: 16px" class="text-right">বাকী -</td>
                                                <td style="font-size: 16px" class="text-left">{{ $invoice['invoice_amount'] + $pre_dues ? round($invoice['invoice_amount'] + $pre_dues) : '' }}</td>
                                            </tr>
                                            
                                            
                                        </tbody>
                                    </table>
                                    </center>
                                        
                                        
                                
                                    <br>
                                    <h4 style="text-align:center;">ধন্যবাদ আবার  আসবেন। </h4>

                                    <div style="display: flex;dwidth:100%;padding-top: 50px;">
                                        <div style="width: 30%;padding-left:20px;">
                                            <span>........................................</span><br>
                                            <span style="font-size:16px;text-align:center;padding-left:5px;">ক্রেতার স্বাক্ষর</span>
                                        </div>
                                        <div style="width: 40%"></div>
                                        <div style="width: 30%;padding-right:20px;text-align:right;">
                                            <span>.........................................</span><br>
                                            <span style="font-size:16px;text-align:center;padding-right:5px;">বিক্রেতার স্বাক্ষর</span>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div> <!-- end col --><br><br>
                
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection