

<?php $__env->startSection('title', 'Sales'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Orders</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">New Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('invoices_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                <?php echo e(csrf_field()); ?>


                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Customer *</label>
                                            <div class="col-md-8">
                                                <select style="width: 75%" id="customer_id" name="customer_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9" required>
                                                   <option value="1">Walk-In Customer</option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Name </label>
                                            <div class="col-md-8">
                                                <input id="customer_name_s" name="customer_name" type="text" value="" class="form-control" placeholder="Customer Name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Reference</label>
                                            <div class="col-md-8">
                                                <select style="width: 75%" id="reference_id" name="reference_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                   <option value="">Select Reference</option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal1">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Address </label>
                                            <div class="col-md-8">
                                                <input id="customer_address" name="customer_address" type="text" value="" class="form-control" placeholder="Address">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">O/Bal. </label>
                                            <div class="col-md-8">
                                                <input id="balance" name="balance" type="text" value="" class="form-control" value="0">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Phone </label>
                                            <div class="col-md-8">
                                                <input id="customer_phone" name="customer_phone" type="text" value="" class="form-control" placeholder="Phone">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Date *</label>
                                            <div class="col-md-8">
                                                <input id="selling_date" name="selling_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Barcode </label>
                                            <div class="col-md-8">
                                                <input type="text" name="product_code" class="inner form-control" id="product_code" autofocus />
                                                <p id="alertMessage" style="display: none">No Product Found !</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="display: none">
                                    <select style="padding: 6px;border-radius: 4px;cursor: pointer" class="taxType" id="tax_type_0" name="tax_type" oninput="calculateActualAmount(0)">
                                        <option style="padding: 10px" value="1" selected>BDT</option>
                                        <option style="padding: 10px" value="0">%</option>
                                    </select>
                                    <input id="tax_amount_0" type="text" class="form-control width-xs taxAmount" name="tax_amount" value="0" oninput="calculateActualAmount(0)">
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 230px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        <div class="row di_0">
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #F4F4F7;height: 260px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Sub Total</label>
                                            <div class="col-md-7">
                                                <input type="text" id="subTotalBdt" class="form-control">
                                                <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">VAT</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control" id="vat_type_0" name="vat_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="0" <?php echo e(Auth::user()->vat_type == 0 ? 'selected' : ''); ?>>%</option>
                                                    <option style="padding: 10px" value="1" <?php echo e(Auth::user()->vat_type == 1 ? 'selected' : ''); ?>>BDT</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="vat_amount_0" type="text" class="form-control width-xs" name="vat_amount" value="<?php echo e(Auth::user()->vat_amount); ?>" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Discount</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control totalDiscountType" id="total_discount_type_0" name="total_discount_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="1" selected>BDT</option>
                                                    <option style="padding: 10px" value="0">%</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="total_discount_0" type="text" class="form-control totalDiscount" name="total_discount_amount" value="0" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Dis. Note</label>
                                            <div class="col-md-7">
                                                <input id="total_discount_note" type="text" class="form-control width-xs" name="total_discount_note" value="<?php echo e(old('total_discount_note')); ?>" placeholder="Discount Note">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Coupon</label>
                                            <div class="col-md-7">
                                                <input id="coupon_code" type="number" class="form-control width-xs couponCode" name="coupon_code" onchange="couponMembership()" placeholder="Coupon/Membership">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <div style="padding-left: 10px" class="form-check form-check-right">
                                                <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="is_installment" onclick="installmentShow()">
                                                <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="is_installment">
                                                    Is Installment
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 260px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Total Payable</label>
                                            <div class="col-md-7">
                                                <input type="text" id="totalBdt" class="form-control">
                                                <input style="display: none" type="text" id="totalBdtShow" name="total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Cash Given</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="cash_given" name="cash_given" placeholder="Cash Given" oninput="calculateChangeAmount()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Due Amount</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="due_amount" placeholder="Due Amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Change</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="change_amount" name="change_amount" placeholder="Change Amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Send SMS</label>
                                            <div class="col-md-12">
                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Non Masking
                                                    </label>
                                                </div>

                                                <div style="padding-left: 10px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Masking
                                                    </label>
                                                </div>

                                                <div style="padding-left: 10px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Voice
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 260px;padding-top: 13px;padding-right: 0px;padding-left: 0px" class="col-md-6">
                                        <div style="background-color: #D2D2D2;height: 240px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap_payment getMultipleRowPayment">
                                            <div class="row row di_payment_0">
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>

                                    <div style="display: none;" class="row justify-content-end">
                                        <div class="col-lg-1 col-md-2 form-group">
                                            <input id="pos_add_button" type="button" class="btn btn-success btn-block inner add_field_button_pos" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                <div id="installment" style="margin-top: 5px;display: none" class="row">
                                    <div id="installment_list" style="background-color: #F4F4F7;height: 360px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-6">
                                    </div>

                                    <div style="background-color: #D2D2D2;height: 360px;padding-top: 5px;padding-right: 10px" class="col-md-6">
                                        <h4 style="text-align: left">Installment Information</h4>

                                        <hr style="margin-top: 0px !important">

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-4 col-form-label">Total Amount</label>
                                            <div class="col-md-8">
                                                <input id="total_installment_amount" type="text" class="form-control">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-4 col-form-label">Service Charge</label>

                                            <div style="padding-right: 0px" class="col-md-4">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control" id="service_charge_type" name="service_charge_type" onchange="calculateInstallment()">
                                                    <option style="padding: 10px" value="1" selected>BDT</option>
                                                    <option style="padding: 10px" value="0">%</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="service_charge_amount" type="text" class="form-control" name="service_charge_amount" value="0" oninput="calculateInstallment()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-4 col-form-label">Total Payable</label>
                                            <div class="col-md-8">
                                                <input id="total_payable_amount" type="text" class="form-control">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-4 col-form-label">Number of Installment</label>
                                            <div class="col-md-8">
                                                <input id="total_installment" name="total_installment" type="text" class="form-control" oninput="calculateInstallment()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-4 col-form-label">Payable Per Installment</label>
                                            <div class="col-md-8">
                                                <input id="payable_per_installment" name="payable_per_installment" type="text" class="form-control">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-4 col-form-label">Start Date</label>
                                            <div class="col-md-8">
                                                <input id="first_installment_date" name="first_installment_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" onchange="calculateInstallment()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-4 col-form-label">Type</label>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <select id="month" name="month" value="" class="form-control">
                                                            <option value="0">--Months--</option>
                                                            <option value="1">1 Month</option>
                                                            <option value="2">2 Months</option>
                                                            <option value="3">3 Months</option>
                                                            <option value="4">4 Months</option>
                                                            <option value="5">5 Months</option>
                                                            <option value="6">6 Months</option>
                                                            <option value="7">7 Months</option>
                                                            <option value="8">8 Months</option>
                                                            <option value="9">9 Months</option>
                                                            <option value="10">10 Months</option>
                                                            <option value="11">11 Months</option>
                                                            <option value="12">12 Months</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <select id="days" name="days" value="" class="form-control">
                                                            <option value="0">--Days--</option>
                                                            <option value="1">1 Day</option>
                                                            <option value="2">2 Days</option>
                                                            <option value="3">3 Days</option>
                                                            <option value="4">4 Days</option>
                                                            <option value="5">5 Days</option>
                                                            <option value="6">6 Days</option>
                                                            <option value="7">7 Days</option>
                                                            <option value="8">8 Days</option>
                                                            <option value="9">9 Days</option>
                                                            <option value="10">10 Days</option>
                                                            <option value="11">11 Days</option>
                                                            <option value="12">12 Days</option>
                                                            <option value="13">13 Days</option>
                                                            <option value="14">14 Days</option>
                                                            <option value="15">15 Days</option>
                                                            <option value="16">16 Days</option>
                                                            <option value="17">17 Days</option>
                                                            <option value="18">18 Days</option>
                                                            <option value="19">19 Days</option>
                                                            <option value="20">20 Days</option>
                                                            <option value="21">21 Days</option>
                                                            <option value="22">22 Days</option>
                                                            <option value="23">23 Days</option>
                                                            <option value="24">24 Days</option>
                                                            <option value="25">25 Days</option>
                                                            <option value="26">26 Days</option>
                                                            <option value="27">27 Days</option>
                                                            <option value="28">28 Days</option>
                                                            <option value="29">29 Days</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <button type="button" class="btn btn-success form-control" id="bal">Create</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px !important;padding-bottom: 0px !important" class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Save & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('invoices_index')); ?>">Close</a></button>
                                              
                                                <button style="border-radius: 0px !important" class="btn btn-info waves-effect waves-light" type="button" data-toggle="modal" data-target="#myModal2" onclick="printInvoiceList()">Print Invoices</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control">
                        </div>
                    </div>

                    <input id="contact_type" name="contact_type" type="hidden" value="0" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Reference</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name1" name="customer_name" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number1" name="mobile_number" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address1" name="address" type="text" class="form-control">
                        </div>
                    </div>

                    <input id="contact_type1" name="contact_type" type="hidden" value="3" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal2" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Print Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label for="productname" class="col-md-4 col-form-label">Date</label>
                                <div class="col-md-8">
                                    <input style="cursor: pointer" id="search_date" type="date" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Customer </label>
                                <div class="col-md-8">
                                    <input id="customer" type="text" class="form-control"  placeholder="Enter Customer Name">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Search Invoice </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <input style="width: 95%" type="text" class="form-control col-lg-9 col-md-9 col-sm-9 col-9" id="invoiceNumber" placeholder="Enter Invoice ID">
                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="printInvoicesSearch()">
                                            <i class="bx bx-search font-size-24"></i>
                                        </span>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Order#</th>
                                <th>Customer</th>
                                <th>Amount</th>
                                <th>Paid</th>
                                <th>Due</th>
                                <th>Creator</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody id="print_invoice_list">
                        </tbody>
                    </table>
                </div>
                
                <div class="modal-footer">
                    <button id="CloseButton2" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0)
                    {
                        if (result['phone'] != null)
                        {
                            var phone = ' | ' + result['phone'];
                        }
                        else
                        {
                            var phone = '';
                        }

                        return result['text'] + phone;
                    }
                },
            });

            $("#reference_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 3)
                    {
                        return result['text'];
                    }
                },
            });

            $('#add_field_button').click();
            $('#add_field_button_payment').click();
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = $("#contact_type").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '<?php echo e(csrf_token()); ?>' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                        }
                        
                        $("#customer_id").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });

        $('#submitBtn1').click(function() {
            
            var customer_name               = $("#customer_name1").val();
            var address                     = $("#address1").val();
            var mobile_number               = $("#mobile_number1").val();
            var contact_type                = $("#contact_type1").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '<?php echo e(csrf_token()); ?>' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton1').click();
                        }
                        
                        $("#reference_id").empty();
                        $('#reference_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });     
    </script>

    <script type="text/javascript">
        // Get the input field
        var input = document.getElementById("product_code");

        // Execute a function when the user releases a key on the keyboard
        input.addEventListener("keyup", function(event) {
          // Number 13 is the "Enter" key on the keyboard
          if (event.keyCode === 13) {
            // Cancel the default action, if needed
            event.preventDefault();
            
            $("#alertMessage").hide();

            var site_url      = $(".site_url").val();
            var read_code     = $("#product_code").val();

            if (read_code.length == 6)
            {
                var product_code  = read_code.replace(/^0+/, '');
            }
            else
            {
                var product_code  = $("#product_code").val();
            }

            if(product_code != '')
            {   
                $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code, function(data_pos){

                    if ($.isEmptyObject(data_pos))
                    {   
                        $('input[name=product_code').val('');
                        $("#alertMessage").show();
                    }
                    else
                    {
                        if (data_pos.warrenty_type == 1)
                        {
                            var state = 0;

                            $('.productEntries').each(function()
                            {
                                var entry_id    = $(this).val();
                                var value_x     = $(this).prop("id");

                                if (data_pos.id == entry_id)
                                {
                                    var explode          = value_x.split('_');
                                    var quantity_id      = explode[2];
                                    var quantity         = $('#quantity_'+quantity_id).val();
                                    var inc_quantity     = parseFloat(quantity) + 1;

                                    $('#quantity_'+quantity_id).val(inc_quantity);
                                    $('#quantity_'+quantity_id).change();
                                    $('#rate_'+quantity_id).val(data_pos.sell_price);
                                    $('#discount_'+quantity_id).val(0);
                                    $('input[name=product_code').val('');
                                    calculateActualAmount(quantity_id);

                                    state++;
                                }

                                if (entry_id == '')
                                {
                                    var explode          = value_x.split('_');
                                    var quantity_id      = explode[2];
                                    
                                    ProductEntriesList(quantity_id);

                                    state++;
                                }
                            });

                            if (state == 0)
                            {   
                                $("#pos_add_button").click();
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {   
                            var state1 = 0;
                            $('.productEntries').each(function()
                            {
                                var entry_id1    = $(this).val();
                                var value_x1     = $(this).prop("id");

                                if ($.isEmptyObject(entry_id1))
                                {   
                                    var explode1          = value_x1.split('_');
                                    var quantity_id1      = explode1[2];

                                    $('#product_entries_'+quantity_id1).append('<option value = "' +  data_pos.id + '" selected>' + data_pos.name + data_pos.product_code + '</option>');
                                    $('#serial_number_'+quantity_id1).append('<option value="'+ data_pos.serial_id +'" selected>'+ data_pos.serial_number +'</option>');

                                    $('#quantity_'+quantity_id1).val(1);
                                    $('#quantity_'+quantity_id1).change();
                                    $('#rate_'+quantity_id1).val(data_pos.sell_price);
                                    $('#discount_'+quantity_id1).val(0);
                                    $('input[name=product_code').val('');
                                    calculateActualAmount(quantity_id1);

                                    state1++;
                                }
                            });

                            if (state1 == 0)
                            {   
                                $("#pos_add_button").click();
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }

                });
            } 
          }
        });

        function ProductEntriesList(x) 
        {
            //For getting item commission information from items table start
            var site_url = $(".site_url").val();

            $.get(site_url + '/bills/product/entries/list/invoice/', function(data){
                var list5 = '';
                var list7 = '';

                $.each(data, function(i, data)
                {
                    if (data.stock_in_hand != null)
                    {
                        var stock = data.stock_in_hand;
                    }
                    else
                    {
                        var stock = 0;
                    }

                    if (data.product_type == 2)
                    {
                        var productName = data.name + ' - ' + data.variations;
                    }
                    else
                    {
                        var productName = data.name;
                    }

                    if (data.product_code != null)
                    {
                        var productCode = ' ( ' + pad(data.product_code, 6) + ' )';
                    }
                    else
                    {
                        var productCode = '';
                    }

                    list5 += '<option value = "' +  data.id + '">' + productName + productCode + '</option>';
                });

                list7 += '<option value = "">' + '--Select Product--' +'</option>';

                $("#product_entries_"+x).empty();
                $("#product_entries_"+x).append(list7);
                $("#product_entries_"+x).append(list5);

                var read_code     = $("#product_code").val();

                if (read_code.length == 6)
                {
                    var product_code  = read_code.replace(/^0+/, '');
                }
                else
                {
                    var product_code  = $("#product_code").val();
                }

                if(product_code != '')
                {
                    $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code, function(data_pos){

                        if (data_pos.warrenty_type == 1)
                        {
                            $('#product_entries_'+x).append('<option value = "' +  data_pos.id + '" selected>' + data_pos.name + data_pos.product_code + '</option>');

                            $('#quantity_'+x).val(1);
                            $('#quantity_'+x).change();
                            $('#rate_'+x).val(data_pos.sell_price);
                            $('#discount_'+x).val(0);
                            $('input[name=product_code').val('');
                            calculateActualAmount(x);
                        }
                        else
                        {
                            $('#product_entries_'+x).append('<option value = "' + data_pos.id + '" selected>' + data_pos.name + data_pos.product_code + '</option>');
                            $('#serial_number_'+x).append('<option value="'+ data_pos.serial_id +'" selected>'+ data_pos.serial_number +'</option>');

                            $('#quantity_'+x).val(1);
                            $('#quantity_'+x).change();
                            $('#rate_'+x).val(data_pos.sell_price)
                            $('#discount_'+x).val(0);
                            $('input[name=product_code').val('');
                            calculateActualAmount(x);
                        }
                        
                    });
                } 
            });
        }

        function checkValue(value,arr)
        {
            var status = 'Not exist';
             
            for(var i=0; i<arr.length; i++)
            {
                var name = arr[i];
                if(name == value)
                {
                    status = 'Exist';
                    break;
                }
            }

            return status;
        }

        function getItemPrice(x)
        {
            //For getting item commission information from items table start
            var entry_id  = $("#product_entries_"+x).val();
            var site_url  = $(".site_url").val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    if (data.price.stock_in_hand == '')
                    {
                        var stockInHand  = 0;
                    }
                    else
                    {
                        var stockInHand  = data.price.stock_in_hand;
                    }

                    $("#rate_"+x).val(data.price.sell_price);
                    $("#stock_"+x).val(stockInHand);
                    $("#stock_show_"+x).html('Stock : ' + stockInHand);
                    $("#quantity_"+x).val(1);
                    $("#discount_"+x).val(0);

                    calculateActualAmount(x);

                    var read_code     = $("#product_code").val();

                    if (read_code)
                    {
                        var product_code  = read_code.replace(/^0+/, '');
                    }
                    else
                    {
                        var product_code  = '';
                    }

                    if(product_code == '')
                    {
                        var list = '';
                        list     += '<option value="">' + '--Select Serial--' + '</option>';
                        $.each(data.serials, function(i, serial_data)
                        {
                            list += '<option value = "' +  serial_data.id + '">' + serial_data.serial_number + '</option>';
                        });

                        $("#serial_number_"+x).empty();
                        $("#serial_number_"+x).append(list);
                    }

                    $('input[name=product_code').val('');
                });
            }
            // calculateActualAmount(x);
        }

        function pad (str, max)
        {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }

        function addButtonPayment()
        {
            $('.add_field_button_payment').click();
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = -1;
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {   
                x++;

                var serial = x + 1;

                if (serial == 1)
                {
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var stock_label         = '<label class="hidden-xs" for="productname">Stock</label>\n';
                    var serial_label        = '<label class="hidden-xs" for="productname">Serial#</label>\n';
                    var from_date_label     = '<label class="hidden-xs" for="productname">W.From</label>\n';
                    var to_date_label       = '<label class="hidden-xs" for="productname">W.To</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                    var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                    var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">Discount</label>\n';
                    var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButton()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var product_label       = '';
                    var stock_label         = '';
                    var serial_label        = '';
                    var from_date_label     = '';
                    var to_date_label       = '';
                    var rate_label          = '';
                    var quantity_label      = '';
                    var discount_label      = '';
                    var type_label          = '';
                    var amount_label        = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                ProductEntriesList(x);

                $('.getMultipleRow').append(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" readonly />\n' + 

                                                    '<div id="serial_hide_show_'+x+'" style="margin-bottom: 5px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Serial# </label>\n' +
                                                        serial_label +
                                                        '<select style="width: 100%" name="serial_number[]" class="inner form-control single_select2 serialNumber" id="serial_number_'+x+'" onchange="takeInput('+x+')">\n' +
                                                        '<option value="">' + '--Select Serial--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div id="serial_input_hide_show_'+x+'" style="margin-bottom: 5px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Serial# </label>\n' +
                                                        serial_label +
                                                        '<input type="text" name="serial_input[]" class="inner form-control" id="serial_input_'+x+'" placeholder="Serial" value="" />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px" class="col-lg-1 col-md-1 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">W.From</label>\n' +
                                                        from_date_label +
                                                        '<input id="from_date_'+x+'" name="from_date[]" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px" class="col-lg-1 col-md-1 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">W.To</label>\n' +
                                                        to_date_label +
                                                        '<input id="to_date_'+x+'" name="to_date[]" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control" id="quantity_'+x+'" placeholder="Quantity" oninput="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n' +
                                                                type_label +
                                                                '<select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_'+x+'" oninput="calculateActualAmount('+x+')">\n' +
                                                                    '<option value="1">BDT</option>' +
                                                                    '<option value="0" selected>%</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" name="discount[]" class="inner form-control" id="discount_'+x+'" placeholder="Discount" oninput="calculateActualAmount('+x+')"/>\n' +
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                        amount_label +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }                                    
        });
        //For apending another rows end

        //For apending another rows start
        $(add_button_pos).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

               if (serial == 1)
                {
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var stock_label         = '<label class="hidden-xs" for="productname">Stock</label>\n';
                    var serial_label        = '<label class="hidden-xs" for="productname">Serial#</label>\n';
                    var from_date_label     = '<label class="hidden-xs" for="productname">W.From</label>\n';
                    var to_date_label       = '<label class="hidden-xs" for="productname">W.To</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                    var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                    var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">Discount</label>\n';
                    var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButton()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var product_label       = '';
                    var stock_label         = '';
                    var serial_label        = '';
                    var from_date_label     = '';
                    var to_date_label       = '';
                    var rate_label          = '';
                    var quantity_label      = '';
                    var discount_label      = '';
                    var type_label          = '';
                    var amount_label        = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                ProductEntriesList(x);
                
                $('.getMultipleRow').append(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" readonly />\n' + 

                                                    '<div id="serial_hide_show_'+x+'" style="margin-bottom: 5px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Serial# </label>\n' +
                                                        serial_label +
                                                        '<select style="width: 100%" name="serial_number[]" class="inner form-control single_select2 serialNumber" id="serial_number_'+x+'" onchange="takeInput('+x+')">\n' +
                                                        '<option value="">' + '--Select Serial--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div id="serial_input_hide_show_'+x+'" style="margin-bottom: 5px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Serial# </label>\n' +
                                                        serial_label +
                                                        '<input type="text" name="serial_input[]" class="inner form-control" id="serial_input_'+x+'" placeholder="Serial" value="" />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px" class="col-lg-1 col-md-1 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">W.From</label>\n' +
                                                        from_date_label +
                                                        '<input id="from_date_'+x+'" name="from_date[]" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px" class="col-lg-1 col-md-1 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">W.To</label>\n' +
                                                        to_date_label +
                                                        '<input id="to_date_'+x+'" name="to_date[]" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control" id="quantity_'+x+'" placeholder="Quantity" oninput="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n' +
                                                                type_label +
                                                                '<select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_'+x+'" oninput="calculateActualAmount('+x+')">\n' +
                                                                    '<option value="1">BDT</option>' +
                                                                    '<option value="0" selected>%</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" name="discount[]" class="inner form-control" id="discount_'+x+'" placeholder="Discount" oninput="calculateActualAmount('+x+')"/>\n' +
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                        amount_label +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

            }
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            var rate                    = $("#rate_"+x).val();
            var quantity                = $("#quantity_"+x).val();
            var stock                   = $("#stock_"+x).val();
            var discount                = $("#discount_"+x).val();
            var discountType            = $("#discount_type_"+x).val();
            var vatType                 = $("#vat_type_0").val();
            var vatAmount               = $("#vat_amount_0").val();
            var taxType                 = $("#tax_type_0").val();
            var taxAmount               = $("#tax_amount_0").val();
            var totalDiscount           = $("#total_discount_0").val();
            var totalDiscountType       = $("#total_discount_type_0").val();

            if (rate == '')
            {
                var rateCal             = 0;
            }
            else
            {
                var rateCal             = $("#rate_"+x).val();
            }

            if (quantity == '')
            {
                var quantityCal         = 1;
            }
            else
            {
                var quantityCal         = $("#quantity_"+x).val();
            }

            if (discount == '')
            {
                var discountCal         = 1;
            }
            else
            {
                var discountCal         = $("#discount_"+x).val();
            }

            if (discount == '')
            {
                var discountTypeCal     = 0;
            }
            else
            {
                if (discountType == 0)
                {
                    var discountTypeCal     = (parseFloat(discountCal)*parseFloat(rateCal)*parseFloat(quantityCal))/100;
                }
                else
                {
                    var discountTypeCal     = $("#discount_"+x).val();
                }
            }

            var AmountIn              =  (parseFloat(rateCal)*parseFloat(quantityCal)) - parseFloat(discountTypeCal);
     
            $("#amount_"+x).val(AmountIn);

            //Calculating Subtotal Amount
            var total       = 0;

            $('.amount').each(function()
            {
                total       += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(total);
            $("#subTotalBdtShow").val(total);

            if (vatAmount == '')
            {   
                $("#vat_amount_0").val(0);
                var vatCal         = 0;
            }
            else
            {
                var vatCal         = $("#vat_amount_0").val();
            }

            if (vatType == 0)
            {
                var vatTypeCal     = (parseFloat(vatCal)*(parseFloat(total)))/100;
            }
            else
            {
                var vatTypeCal     = $("#vat_amount_0").val();
            }

            if (taxAmount == '')
            {   
                $("#tax_amount_0").val(0);
                var taxCal         = 0;
            }
            else
            {
                var taxCal         = $("#tax_amount_0").val();
            }

            if (taxType == 0)
            {
                var taxTypeCal     = (parseFloat(taxCal)*(parseFloat(total)))/100;
            }
            else
            {
                var taxTypeCal     = $("#tax_amount_0").val();
            }

            if (totalDiscount > 0)
            {   
                if (totalDiscountType == 0)
                {
                    var totalDiscountTypeCal     = (parseFloat(totalDiscount)*(parseFloat(total) + parseFloat(vatTypeCal)))/100;
                }
                else
                {
                    var totalDiscountTypeCal     = $("#total_discount_0").val();
                }
            }
            else
            {
                var totalDiscountTypeCal     = 0;
            }

            var totalShow = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountTypeCal);

            $("#totalBdtShow").val(totalShow);
            $("#totalBdt").val(parseFloat(totalShow));
            $("#due_amount").val(parseFloat(totalShow));
            $("#total_installment_amount").val(parseFloat(totalShow));
            $("#total_payable_amount").val(parseFloat(totalShow));

            //Checking Overselling Start
            var check_quantity  = parseFloat(quantity);
            var check_stock     = parseFloat(stock);

            if (check_quantity > check_stock)
            {   
                $("#quantity_"+x).val(check_stock);
            }
            //Checking Overselling End

            calculateChangeAmount();
        }
    </script>

    <script type="text/javascript">
        var max_fields_payment       = 50;                           //maximum input boxes allowed
        var wrapper_payment          = $(".input_fields_wrap_payment");      //Fields wrapper
        var add_button_payment       = $(".add_field_button_payment");       //Add button ID
        var index_no_payment         = 1;

        //For apending another rows start
        var y = -1;
        $(add_button_payment).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(y < max_fields_payment)
            {   
                y++;

                var serial = y + 1;

                if (serial == 1)
                {
                    var amount_paid_label   = '<label class="hidden-xs" for="productname">Amount Paid *</label>\n';
                    var paid_through_label  = '<label class="hidden-xs" for="productname">Paid Through *</label>\n';
                    var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button_payment">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonPayment()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var amount_paid_label   = '';
                    var paid_through_label  = '';
                    var note_label          = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field_payment" data-val="'+y+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRowPayment').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_payment_'+y+'">' +
                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Amount Paid *</label>\n' +
                                                        amount_paid_label +
                                                        '<input type="text" name="amount_paid[]" class="form-control paidAmount" id="amount_paid_'+y+'" value="0" required />\n' + 
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-4 col-md-14col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid Through *</label>\n' +
                                                        paid_through_label  +
                                                        '<select id="paid_through_'+y+'" style="cursor: pointer" name="paid_through[]" class="form-control single_select2">\n' +
                                                            '<?php if(!empty($paid_accounts) && ($paid_accounts->count() > 0)): ?>\n' +
                                                            '<?php $__currentLoopData = $paid_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $paid_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>\n' +
                                                                '<option value="<?php echo e($paid_account['id']); ?>"><?php echo e($paid_account['name']); ?></option>\n' +
                                                            '<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>\n' +
                                                            '<?php endif; ?>\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-4 col-md-4 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                        note_label +
                                                        '<input type="text" name="note[]" class="form-control" id="note_'+y+'" placeholder="note"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +

                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
                }                                    
        });
        //For apending another rows end

        $(wrapper_payment).on("click",".remove_field_payment", function(e)
        {
            e.preventDefault();

            var y = $(this).attr("data-val");

            $('.di_payment_'+y).remove(); y--;

            calculateActualAmount(y);
        });
    </script>

    <script type="text/javascript">
        function calculateInstallment()
        {   
            var total_installment_amount = $("#total_installment_amount").val();
            var service_charge_type      = $("#service_charge_type").val();
            var service_charge_amount    = $("#service_charge_amount").val();
            var total_installment        = $("#total_installment").val();

            if (service_charge_amount == '')
            {
                var serviceCharge     = 0;
            }
            else
            {
                if (service_charge_type == 0)
                {
                    var serviceCharge     = (parseFloat(service_charge_amount)*parseFloat(total_installment_amount))/100;
                }
                else
                {
                    var serviceCharge     = $("#service_charge_amount").val();
                }
            }

            $("#total_payable_amount").val(parseFloat(total_installment_amount) + parseFloat(serviceCharge));

            //Calculating Paid Amount
            var total_paid       = 0;

            $('.paidAmount').each(function()
            {
                total_paid       += parseFloat($(this).val());
            });

            var total_installment_amount  = parseFloat(total_installment_amount) + parseFloat(serviceCharge) - parseFloat(total_paid);
            var payable_per_installment   = parseFloat(total_installment_amount)/parseFloat(total_installment);

            $('#payable_per_installment').val(parseFloat(payable_per_installment).toFixed());
        }
    </script>

    <script type="text/javascript">
        $(document).on("click", "#bal" , function() {

            var total_installment       = $("#total_installment").val();
            var payable_per_installment = $("#payable_per_installment").val();
            var month                   = $("#month").val();
            var days                    = $("#days").val();
            var date                    = $("#first_installment_date").datepicker("getDate");
            
            var i;
            var installment_list  = '';
            var date_inc          = 0;
            for (i = 0; i < total_installment; i++)
            {   
                var serial        = parseFloat(i) + 1;
                var date_count    = (parseFloat(month)*30) + parseFloat(days);
                date_inc         += parseFloat(date_count);         
                date.setDate(date. getDate() + parseFloat(date_count));
                var date_result   = formatDate(date); 

                if (i == 0)
                {
                    var sl_label     = '<label class="hidden-xs" for="productname">SL</label>\n';
                    var date_label   = '<label class="hidden-xs" for="productname">Installment Date</label>\n';
                    var amount_label = '<label class="hidden-xs" for="productname">Amount</label>\n';
                }
                else
                {
                    var sl_label     = '';
                    var date_label   = '';
                    var amount_label = '';
                }

                installment_list += '<div class="row">' +
                                        '<div style="margin-bottom: 5px" class="col-lg-1 col-md-1 col-sm-6 col-12">\n' +
                                            '<label style="display: none" class="show-xs" for="productname">SL</label>\n' +
                                            sl_label +
                                            '<p>\n' + serial + '</p>' +
                                        '</div>\n' +

                                        '<div style="margin-bottom: 5px" class="col-lg-5 col-md-5 col-sm-6 col-12">\n' +
                                            '<label style="display: none" class="show-xs" for="productname">Installment Date</label>\n' +
                                            date_label +
                                            '<input name="installment_date[]" type="text" value="'+ date_result +'" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">\n' +
                                        '</div>\n' +

                                        '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-6 col-md-6 col-sm-6 col-6">\n' + 
                                            '<label style="display: none" class="show-xs" for="productname">Amount</label>\n' +
                                            amount_label +
                                            '<input type="text" name="installment_amount[]" class="inner form-control" value="'+ parseFloat(payable_per_installment).toFixed() +'" />\n' + 
                                        '</div>\n' +
                                    '</div>\n';
            }

            $("#installment_list").empty();
            $("#installment_list").append(installment_list);
        });
    </script>

    <script type="text/javascript">
        $(document).on("change", "#defaultCheck2" , function() {

            var checkbox_value    = $("#defaultCheck2")[0].checked;

            if (checkbox_value == true)
            {
                $("#vatShow").show();
                $("#taxShow").show();
            }
            else
            {
                $("#vatShow").hide();
                $("#taxShow").hide();
            }
        });

        function couponMembership()
        {
            var site_url      = $(".site_url").val();
            var coupon_code   = $("#coupon_code").val();
    
            $('.DiscountType').val(1);
            $('.DiscountAmount').val(0);

            $.get(site_url + '/invoices/search/coupon-code/'+ coupon_code, function(data_coupon){

                if (data_coupon == '')
                {
                    alert('Invalid Coupon Code or Membership Card !!');
                    $('#coupon_code').val('');
                }
                else
                {
                    var state = 0;

                    $('.productEntries').each(function()
                    {
                        var entry_id    = $(this).val();
                        var value_x     = $(this).prop("id");
                        
                        if (data_coupon[0].product_id != null)
                        {
                            for (var i = data_coupon.length - 1; i >= 0; i--)
                            {   
                                if (data_coupon[i].product_id == entry_id)
                                {
                                    var explode    = value_x.split('_');
                                    var di_id      = explode[2];

                                    $('#discount_type_'+di_id).val(data_coupon[i].discount_type);
                                    $('#discount_'+di_id).val(data_coupon[i].discount_amount);
                                    
                                    calculateActualAmount(di_id);

                                    state++;
                                }
                            }
                        }
                        else
                        {
                            var explode    = value_x.split('_');
                            var di_id      = explode[2];

                            $('#discount_type_'+di_id).val(data_coupon[0].discount_type);
                            $('#discount_'+di_id).val(data_coupon[0].discount_amount);
                            
                            calculateActualAmount(di_id);
                        }    
                    });
                }
            });
        }

        function calculateChangeAmount()
        {
            var tAmount             = $("#totalBdtShow").val();
            var cGiven              = $("#cash_given").val();

            if (tAmount != '')
            {
                var totalAmount     = $("#totalBdtShow").val();
            }
            else
            {
                var totalAmount     = 0;
            }

            if (cGiven != '')
            {
                var cashGiven          = $("#cash_given").val();
            }
            else
            {
                var cashGiven          = 0;
            }

            var changeAmount    = parseFloat(cashGiven) - parseFloat(totalAmount);

            if (changeAmount > 0)
            {   
                $("#change_amount").val(0);
                $("#change_amount").val(parseFloat(changeAmount));
                $("#amount_paid_0").val(parseFloat(cashGiven) - parseFloat(changeAmount));
                $("#due_amount").val(0);
                $("#total_installment_amount").val(0);
                $("#total_payable_amount").val(0);
            }

            if (parseFloat(changeAmount) < 0)
            {   
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(cashGiven));
                $("#due_amount").val(parseFloat(tAmount) - parseFloat(cashGiven));
                $("#total_installment_amount").val(parseFloat(tAmount) - parseFloat(cashGiven));
                $("#total_payable_amount").val(parseFloat(tAmount) - parseFloat(cashGiven));
            }

            if (parseFloat(changeAmount) == 0)
            {   
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(cashGiven));
                $("#due_amount").val(parseFloat(tAmount) - parseFloat(cashGiven));
                $("#total_installment_amount").val(parseFloat(tAmount) - parseFloat(cashGiven));
                $("#total_installment_amount").val(parseFloat(tAmount) - parseFloat(cashGiven));
            }
        }
    </script>

    <script type="text/javascript">
        function printInvoiceList()
        {
            var site_url  = $('.site_url').val();

            $.get(site_url + '/invoices/print-invoices-list', function(data){

                var invoice_list = '';
                var sl_no        = 1;
                $.each(data, function(i, invoice_data)
                {
                    var serial              = parseFloat(i) + 1;
                    var site_url            = $('.site_url').val();
                    var print_url_pos       = site_url + '/invoices/show-pos/' + invoice_data.id;

                    if (invoice_data.customer_name != null)
                    {
                        var customer  = invoice_data.customer_name;
                    }
                    else
                    {
                        var customer  = invoice_data.contact_name;
                    }

                    invoice_list += '<tr>' +
                                        '<td>' +
                                            sl_no +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(invoice_data.invoice_date) +
                                        '</td>' +
                                        '<td>' +
                                            'INV - ' + invoice_data.invoice_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            customer + 
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.invoice_amount +
                                        '</td>' +
                                        '<td>' +
                                           (parseFloat(invoice_data.invoice_amount) - parseFloat(invoice_data.due_amount)) +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.due_amount +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.user_name +
                                        '</td>' +
                                        '<td>' +
                                            '<a href="'+ print_url_pos +'">' +
                                                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +'<i class="fa fa-print">' + '</i>' +
                                            '</a>' +
                                        '</td>' +
                                    '</tr>';

                                    sl_no++;
                });

                $("#print_invoice_list").empty();
                $("#print_invoice_list").append(invoice_list);
                
            });
        }

        function printInvoicesSearch()
        {
            var site_url            = $('.site_url').val();
            var date_val            = $('#search_date').val();
            var invoiceNumber_val   = $('#invoiceNumber').val();
            var customer_val        = $('#customer').val();

            if (date_val != '')
            {
                var date = $('#search_date').val();
            }
            else
            {
                var date  = 0;
            }

            if (invoiceNumber_val != '')
            {
                var invoiceNumber = $('#invoiceNumber').val();
            }
            else
            {
                var invoiceNumber  = 0;
            }

            if (customer_val != '')
            {
                var customer = $('#customer').val();
            }
            else
            {
                var customer  = 0;
            }

            $.get(site_url + '/invoices/print-invoices-search/' + date + '/' + customer + '/' + invoiceNumber , function(data){

                var invoice_list = '';
                var sl_no        = 1;
                $.each(data, function(i, invoice_data)
                {
                    var serial              = parseFloat(i) + 1;
                    var site_url            = $('.site_url').val();
                    var print_url_pos       = site_url + '/invoices/show-pos/' + invoice_data.id;

                    if (invoice_data.customer_name != null)
                    {
                        var customer  = invoice_data.customer_name;
                    }
                    else
                    {
                        var customer  = invoice_data.contact_name;
                    }

                    invoice_list += '<tr>' +
                                        '<td>' +
                                            sl_no +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(invoice_data.invoice_date) +
                                        '</td>' +
                                        '<td>' +
                                            'INV - ' + invoice_data.invoice_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            customer + 
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.invoice_amount +
                                        '</td>' +
                                        '<td>' +
                                           (parseFloat(invoice_data.invoice_amount) - parseFloat(invoice_data.due_amount)) +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.due_amount +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.user_name +
                                        '</td>' +
                                        '<td>' +
                                            '<a href="'+ print_url_pos +'">' +
                                                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +'<i class="fa fa-print">' + '</i>' +
                                            '</a>' +
                                        '</td>' +
                                    '</tr>';

                                    sl_no++;
                });

                $("#print_invoice_list").empty();
                $("#print_invoice_list").append(invoice_list);
                
            });
        }

        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        $(document).on("change", "#customer_id" , function() {

            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            $.get(site_url + '/invoices/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data);
                
            });
        });

        function takeInput(x)
        {
            var warrentyProductId       = $('#serial_number_'+x).val();
            var warrentyProductHtml     = $("#serial_number_"+x+" option:selected").text();

            if (warrentyProductHtml == 'null')
            {   
                $('#serial_hide_show_'+x).css('display', 'none');
                $('#serial_input_hide_show_'+x).css('display', 'block');
            }
        }

        function installmentShow()
        {   
            if ($('#is_installment').is(":checked"))
            {
                $("#installment").show();
            }
            else
            {
                $("#installment").hide();
            }
        };
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/29/Modules/Invoices/Resources/views/index.blade.php ENDPATH**/ ?>