

<?php $__env->startSection('title', 'Show'); ?>

<style type="text/css">
    @media  print {
        #footerInvoice {
            position: fixed;
            bottom: 60;
        }
    }

    .textstyle {
        background-color: white; /* Changing background color */
        font-weight: bold; /* Making font bold */
        border-radius: 20px; /* Making border radius */
        border: 3px solid black; /* Making border radius */
        width: auto; /* Making auto-sizable width */
        height: auto; /* Making auto-sizable height */
        padding: 5px 10px 5px 10px; /* Making space around letters */
        font-size: 18px; /* Changing font size */
    }

    .column-bordered-table thead td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table tfoot tr {
        border-top: 1px solid #c3c3c3;
        border-bottom: 1px solid #c3c3c3;
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div class="row">
                                    <?php if($user_info['header_image'] == null): ?>
                                        <div class="col-md-2 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-8 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 55px"><?php echo e($user_info['organization_name']); ?></h2>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['address']); ?></p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold"><?php echo e($user_info['contact_email']); ?></p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['contact_number']); ?></p>
                                        </div>
                                        <div class="col-md-2 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-5 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-2 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 20px;margin-bottom: 0px;text-align: center;font-size: 35px;" class="textstyle">Invoice</h2>
                                        </div>
                                        <div class="col-md-5 col-xs-12 col-sm-12"></div>
                                    <?php else: ?>
                                        <img class="float-left" src="<?php echo e(url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image'])); ?>" alt="logo" style="width: 100%" />
                                    <?php endif; ?>
                                </div>

                                <div style="margin-top: 20px" class="row">
                                    <div style="font-size: 16px" class="col-sm-4 col-6">
                                        <address>
                                            <strong>Customer Name &nbsp;&nbsp;: 
                                            <?php echo e($invoice['customer_name'] != null ? $invoice['customer_name'] : $invoice['contact_name']); ?> </strong><br>
                                            <strong>Address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong>
                                            <?php if($invoice['customer_address'] != null): ?>
                                               <?php echo $invoice['customer_address']; ?> <br>
                                            <?php else: ?>
                                            <?php echo $invoice['address']; ?> <br>
                                            <?php endif; ?>
                                            <strong>Mobile &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                            <?php echo e($invoice['customer_phone'] != null ? $invoice['customer_phone'] : $invoice['phone']); ?></strong>
                                        </address>
                                    </div>

                                    <div class="col-sm-3 hidden-xs">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div style="font-size: 16px" class="col-sm-5 col-6 text-sm-left">
                                        <address style=";border-bottom: 2px solid black">
                                            <strong>Invoice Number &nbsp;&nbsp;: </strong>
                                            <?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?> <br>
                                            <strong>Ref No &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong>

                                            <br>
                                            <strong>Sold By &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong>
                                            <?php echo e($invoice->createdBy->name); ?>

                                            <br>
                                        </address>
                                        <address>
                                            <strong>Date & Time : </strong>
                                            <?php echo e(date('d/m/Y', strtotime($invoice['invoice_date']))); ?> &nbsp;&nbsp;&nbsp; Print Time : <?php echo e(date('h:i:s A', strtotime($invoice['created_at']))); ?>

                                        </address>
                                    </div>
                                </div>

                                <div style="border: 2px solid black;height: 1180px" class="table-responsive">
                                    <table style="border-bottom: 1px solid gray" class="table column-bordered-table">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th style="width: 70px;">SL</th>
                                                <th>Description</th>
                                                <th class="text-right">Sold Qty</th>
                                                <th class="text-right">Unit Price</th>
                                                <th class="text-right">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">

                                            <?php if(!empty($entries) && ($entries->count() > 0)): ?>

                                            <?php $sub_total = 0; ?>

                                            <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($key + 1); ?></td>
                                                <td>
                                                    <?php if($value['product_type'] == 1): ?>
                                                        <?php echo $value['product_entry_name']; ?>
                                                    <?php else: ?>
                                                        <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                                    <?php endif; ?>

                                                    <?php
                                                        $w_pr = WarrentyProducts($value['warrenty_product_id']);
                                                    ?>

                                                    <?php if($w_pr != null): ?>
                                                    <br>
                                                    <br>
                                                    <strong>
                                                        <?php echo $w_pr; ?>
                                                    </strong>
                                                    <?php endif; ?>
                                                </td>
                                                <td class="text-right"><?php echo e(number_format($value['quantity'],2,'.',',')); ?></td>
                                                <td class="text-right"><?php echo e(number_format($value['rate'],2,'.',',')); ?></td>
                                                <td class="text-right"><?php echo e(number_format($value['total_amount'],2,'.',',')); ?></td>
                                            </tr>

                                            <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        
                                        </tbody>
                                    </table>

                                    <div style="margin-top: 20px" class="row">
                                        <div style="vertical-align: middle;text-align: center" class="col-sm-7 hidden-xs">
                                            <?php if($invoice['due_amount'] == 0): ?>
                                            <h1>PAID</h1>
                                            <?php endif; ?>
                                        </div>

                                        <div style="font-size: 16px;border-bottom: 3px solid black" class="col-sm-5 col-6 text-sm-left">
                                            <address>
                                                Total Amount 
                                                <span style="float: right;padding-right: 10px"><?php echo e(number_format($sub_total,2,'.',',')); ?></span> <br>

                                                Add Vat 
                                                <span style="float: right;padding-right: 10px"><?php echo e($invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : number_format($invoice['total_vat'],2,'.',',')); ?></span> <br>

                                                Less Discount 
                                                <span style="float: right;padding-right: 10px"><?php echo e($invoice['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$invoice['total_vat'])/100))*$invoice['total_discount_amount'])/100) : number_format($invoice['total_discount_amount'],2,'.',',')); ?></span> <br>

                                                Add Installation / Service Charge 
                                                <span style="float: right;padding-right: 10px"></span> <br>
                                                
                                            </address>
                                        </div>
                                    </div>

                                    <div style="margin-top: 5px" class="row">
                                        <div style="vertical-align: middle;text-align: center" class="col-sm-7 hidden-xs">
                                        </div>

                                        <div style="font-size: 16px;" class="col-sm-5 col-6 text-sm-left">
                                            <address>
                                                Net Payable Amount
                                                <span style="float: right;padding-right: 10px"><?php echo e(number_format($invoice['invoice_amount'] - $invoice['due_amount'],2,'.',',')); ?></span> 
                                            </address>
                                        </div>
                                    </div>

                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>

                                    <div class="row">

                                        <div style="text-align: left" class="col-md-6">
                                            
                                            <span style="border-top: 1px solid black"> Customer Signature </span>
                                        </div>

                                        <div style="text-align: right" class="col-md-6">

                                            <span style="border-top: 1px solid black"> Authorised Signature </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/29/Modules/Invoices/Resources/views/show.blade.php ENDPATH**/ ?>