

<?php $__env->startSection('title', 'Print Pos'); ?>

<?php if($user_info['pos_printer'] == 0): ?>
<style type="text/css">
    @media  print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page  {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 60mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }

        .PStyle {
            line-height: 20px;
        }
    }
</style>
<?php endif; ?>

<?php if($user_info['pos_printer'] == 1): ?>
<style type="text/css">
    @media  print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page  {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 100mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }
    }
</style>
<?php endif; ?>

<?php if($user_info['pos_printer'] == 2 || $user_info['pos_printer'] == 3): ?>
<style type="text/css">
    @media  print {
        #footerInvoice {
            position: fixed;
            bottom: 60;
        }
    }

    .textstyle {
        background-color: white; /* Changing background color */
        font-weight: bold; /* Making font bold */
        border-radius: 20px; /* Making border radius */
        border: 3px solid black; /* Making border radius */
        width: auto; /* Making auto-sizable width */
        height: auto; /* Making auto-sizable height */
        padding: 5px 10px 5px 10px; /* Making space around letters */
        font-size: 18px; /* Changing font size */
    }

    .column-bordered-table thead td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table tfoot tr {
        border-top: 1px solid #c3c3c3;
        border-bottom: 1px solid #c3c3c3;
    }
</style>
<?php endif; ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content marginTopPrint">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row d-print-none">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Print Pos</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <?php if($user_info['pos_printer'] == 0): ?>
                <br>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div style="text-align: right" class="col-md-4 col-4 col-xs-4">
                                        <img style="height: 100px;width: 100px;border: 1px dotted gray" src="<?php echo e(url('public/company-profile-images/'.userDetails()->logo)); ?>">
                                    </div>

                                    <div style="padding-left: 0px" class="col-md-8 col-8 col-xs-8">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: left;font-size: 18px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: left;font-size: 14px;line-height: 20px"><?php echo e($user_info['address']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: left;font-size: 14px;line-height: 20px"><?php echo e($user_info['contact_number']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: left;font-size: 14px;line-height: 20px"><?php echo e($user_info['contact_email']); ?></p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 20px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">
                                               <?php echo e($entries[0]['brand_name']); ?>

                                        </p>
                                    </div>

                                    <div style="text-align: center" class="col-12">
                                        <img class="center-block"style="max-width:100%; !important;height: 0.3in !important" src="data:image/png;base64,<?php echo e(DNS1D::getBarcodePNG('INV-' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT), 'C93',3,33)); ?>" alt="barcode" /><br>
                                        <p style="font-size: 12px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">
                                            <?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?>

                                        </p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 16px">Date : <?php echo e(date('d/m/Y', strtotime($invoice['invoice_date']))); ?></span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 16px">Time : <?php echo e(date('h:i a', strtotime(now()))); ?></span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 16px">Bill To : <?php echo e($invoice['customer_name'] != null ? $invoice['customer_name'] : $invoice['contact_name']); ?> <?php if($invoice['customer_phone'] != null): ?> | <?php echo $invoice['customer_phone']; ?> <?php endif; ?> <?php if($invoice['customer_address'] != null): ?> | <?php echo $invoice['customer_address']; ?> <?php endif; ?></span>
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">  
                                </div>

                                <div style="border-top: 2px solid black;border-bottom: 2px solid black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-1"><strong>#</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-4"><strong>Item Description</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-2"><strong>Qty</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-2"><strong>Price</strong></div>
                                    <div style="font-size: 12px;text-align: right;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Line Total</strong></div>
                                </div>

                                <?php if(!empty($entries) && ($entries->count() > 0)): ?>

                                    <?php 
                                        $sub_total              = 0; 
                                        $total_discount         = 0;
                                    ?>

                                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <div style="padding-bottom: 10px;" id="DetailsFontSize" class="row">

                                        <?php 
                                            $discount           = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; 
                                            $total_discount     = $total_discount + $discount;
                                        ?>

                                        <div style="font-size: 10px" class="col-1">
                                            <?php echo e($key + 1); ?>

                                        </div>

                                        <div style="font-size: 10px" class="col-4">
                                            <?php
                                                $serial         = WarrentyProducts($value['warrenty_product_id']);
                                                $serial_number  = $serial != null ? $serial : ''
                                            ?>

                                            <?php if($value['product_type'] == 1): ?>
                                                <?php echo $value['product_entry_name'] . "<br>" . $serial_number; ?>
                                            <?php else: ?>
                                                <?php echo $value['product_entry_name'] . "<br>" . ' - ' . ProductVariationName($value['product_entry_id']) . $serial_number; ?>
                                            <?php endif; ?>
                                        </div>

                                        <div style="font-size: 10px;text-align: center" class="col-2">
                                            <?php echo e($value['quantity']); ?>

                                        </div>

                                        <div style="font-size: 10px" class="col-2">
                                            <?php echo e(number_format($value['rate'],2,'.',',')); ?>

                                        </div>
                                        <div style="font-size: 10px;text-align: right" class="col-3"><?php echo e(number_format($value['total_amount'],2,'.',',')); ?></div>
                                    </div>

                                    <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                                <hr>

                                <div class="row">
                                    <div style="text-align: center" class="col-md-5 col-sm-5 col-5">
                                        <img style="height: 180px;width: 150px" src="<?php echo e(url('public/pos_logo.jpeg')); ?>">
                                    </div>
                                    <div style="border: 1px dashed gray" class="col-md-7 col-sm-7 col-7">
                                        <div style="padding-top: 5px;padding-bottom: 5px" class="row">
                                            <div style="font-size: 12px;text-align: right" class="col-6">Invoice Total :</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format($sub_total,2,'.',',')); ?></div>
                                        </div>

                                        <?php
                                            $total_vat_amount       =  $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : $invoice['total_vat']; 
                                        ?>

                                        <div style="padding-top: 5px;padding-bottom: 5px;text-align: right" class="row">
                                            <div style="font-size: 12px" class="col-6">Total VAT :</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format($total_vat_amount,2,'.',',')); ?></div>
                                        </div>

                                        <?php
                                            $total_discount_amount       = $invoice['total_discount_type'] == 0 ? ((($sub_total + $total_vat_amount)*$invoice['total_discount_amount'])/100) : $invoice['total_discount_amount']; 
                                        ?>

                                        <div style="padding-top: 5px;padding-bottom: 5px;text-align: right" class="row">
                                            <div style="font-size: 12px" class="col-6">Discount :</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format($total_discount + $total_discount_amount,2,'.',',')); ?></div>
                                        </div>

                                        <div style="padding-top: 5px;padding-bottom: 5px;text-align: right" class="row">
                                            <div style="font-size: 12px" class="col-6">NET Payable :</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format($sub_total + $total_vat_amount - $total_discount_amount,2,'.',',')); ?></div>
                                        </div>

                                        <div style="padding-top: 5px;padding-bottom: 5px;text-align: right" class="row">
                                            <div style="font-size: 12px;text-align: right" class="col-6">Previous Due</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format($previous_dues,2,'.',',')); ?></div>
                                        </div>

                                        <div style="padding-top: 5px;padding-bottom: 5px;text-align: right" class="row">
                                            <div style="font-size: 12px;text-align: right" class="col-6">Grand Total</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format($sub_total + $total_vat_amount - $total_discount_amount + $previous_dues,2,'.',',')); ?></div>
                                        </div>

                                        <div style="padding-top: 5px;padding-bottom: 5px;padding-bottom: 5px" class="row">
                                            <div style="font-size: 12px;text-align: right" class="col-6">Paid</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format($invoice['invoice_amount'] - $invoice['due_amount'],2,'.',',')); ?></div>
                                        </div>

                                        <div style="padding-top: 5px;padding-bottom: 5px;padding-bottom: 5px" class="row">
                                            <div style="font-size: 12px;text-align: right" class="col-6">Due Balance</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format(($sub_total + $total_vat_amount - $total_discount_amount + $previous_dues) - ($invoice['invoice_amount'] - $invoice['due_amount']),2,'.',',')); ?></div>
                                        </div>
                                    </div>
                                </div>

                                <br>

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 15px;font-weight: bold" class="col-12">Thanks for your bussiness co-operation</div>
                                    <div style="text-align: center;font-size: 15px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if($user_info['pos_printer'] == 1): ?>
                <br>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div style="text-align: right" class="col-md-4 col-4 col-xs-4">
                                        <img style="height: 100px;width: 100px;border: 1px dotted gray" src="<?php echo e(url('public/company-profile-images/'.userDetails()->logo)); ?>">
                                    </div>

                                    <div style="padding-left: 0px" class="col-md-8 col-8 col-xs-8">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: left;font-size: 18px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: left;font-size: 14px;line-height: 20px"><?php echo e($user_info['address']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: left;font-size: 14px;line-height: 20px"><?php echo e($user_info['contact_number']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: left;font-size: 14px;line-height: 20px"><?php echo e($user_info['contact_email']); ?></p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 20px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">
                                               <?php echo e($entries[0]['brand_name']); ?>

                                        </p>
                                    </div>

                                    <div style="text-align: center" class="col-12">
                                        <img class="center-block"style="max-width:100%; !important;height: 0.3in !important" src="data:image/png;base64,<?php echo e(DNS1D::getBarcodePNG('INV-' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT), 'C93',3,33)); ?>" alt="barcode" /><br>
                                        <p style="font-size: 12px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">
                                            <?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?>

                                        </p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 16px">Date : <?php echo e(date('d/m/Y', strtotime($invoice['invoice_date']))); ?></span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 16px">Time : <?php echo e(date('h:i a', strtotime(now()))); ?></span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 16px">Bill To : <?php echo e($invoice['customer_name'] != null ? $invoice['customer_name'] : $invoice['contact_name']); ?> <?php if($invoice['customer_phone'] != null): ?> | <?php echo $invoice['customer_phone']; ?> <?php endif; ?> <?php if($invoice['customer_address'] != null): ?> | <?php echo $invoice['customer_address']; ?> <?php endif; ?></span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">  
                                </div>

                                <div style="border-top: 2px solid black;border-bottom: 2px solid black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-1"><strong>#</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-4"><strong>Item Description</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-2"><strong>Qty</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-2"><strong>Price</strong></div>
                                    <div style="font-size: 12px;text-align: right;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Line Total</strong></div>
                                </div>

                                <?php if(!empty($entries) && ($entries->count() > 0)): ?>

                                    <?php 
                                        $sub_total              = 0; 
                                        $total_discount         = 0;
                                    ?>

                                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <div style="padding-bottom: 10px;" id="DetailsFontSize" class="row">

                                        <?php 
                                            $discount           = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; 
                                            $total_discount     = $total_discount + $discount;
                                        ?>

                                        <div style="font-size: 10px" class="col-1">
                                            <?php echo e($key + 1); ?>

                                        </div>

                                        <div style="font-size: 10px" class="col-4">
                                            <?php
                                                $serial         = WarrentyProducts($value['warrenty_product_id']);
                                                $serial_number  = $serial != null ? $serial : ''
                                            ?>

                                            <?php if($value['product_type'] == 1): ?>
                                                <?php echo $value['product_entry_name'] . "<br>" . $serial_number; ?>
                                            <?php else: ?>
                                                <?php echo $value['product_entry_name'] . "<br>" . ' - ' . ProductVariationName($value['product_entry_id']) . $serial_number; ?>
                                            <?php endif; ?>
                                        </div>

                                        <div style="font-size: 10px;text-align: center" class="col-2">
                                            <?php echo e($value['quantity']); ?>

                                        </div>

                                        <div style="font-size: 10px" class="col-2">
                                            <?php echo e(number_format($value['rate'],2,'.',',')); ?>

                                        </div>
                                        <div style="font-size: 10px;text-align: right" class="col-3"><?php echo e(number_format($value['total_amount'],2,'.',',')); ?></div>
                                    </div>

                                    <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                                <hr>

                                <div class="row">
                                    <div style="text-align: center" class="col-md-5 col-sm-5 col-5">
                                        <img style="height: 180px;width: 150px" src="<?php echo e(url('public/pos_logo.jpeg')); ?>">
                                    </div>
                                    <div style="border: 1px dashed gray" class="col-md-7 col-sm-7 col-7">
                                        <div style="padding-top: 5px;padding-bottom: 5px" class="row">
                                            <div style="font-size: 12px;text-align: right" class="col-6">Invoice Total :</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format($sub_total,2,'.',',')); ?></div>
                                        </div>

                                        <?php
                                            $total_vat_amount       =  $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : $invoice['total_vat']; 
                                        ?>

                                        <div style="padding-top: 5px;padding-bottom: 5px;text-align: right" class="row">
                                            <div style="font-size: 12px" class="col-6">Total VAT :</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format($total_vat_amount,2,'.',',')); ?></div>
                                        </div>

                                        <?php
                                            $total_discount_amount       = $invoice['total_discount_type'] == 0 ? ((($sub_total + $total_vat_amount)*$invoice['total_discount_amount'])/100) : $invoice['total_discount_amount']; 
                                        ?>

                                        <div style="padding-top: 5px;padding-bottom: 5px;text-align: right" class="row">
                                            <div style="font-size: 12px" class="col-6">Discount :</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format($total_discount + $total_discount_amount,2,'.',',')); ?></div>
                                        </div>

                                        <div style="padding-top: 5px;padding-bottom: 5px;text-align: right" class="row">
                                            <div style="font-size: 12px" class="col-6">NET Payable :</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format($sub_total + $total_vat_amount - $total_discount_amount,2,'.',',')); ?></div>
                                        </div>

                                        <div style="padding-top: 5px;padding-bottom: 5px;text-align: right" class="row">
                                            <div style="font-size: 12px;text-align: right" class="col-6">Previous Due</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format($previous_dues,2,'.',',')); ?></div>
                                        </div>

                                        <div style="padding-top: 5px;padding-bottom: 5px;text-align: right" class="row">
                                            <div style="font-size: 12px;text-align: right" class="col-6">Grand Total</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format($sub_total + $total_vat_amount - $total_discount_amount + $previous_dues,2,'.',',')); ?></div>
                                        </div>

                                        <div style="padding-top: 5px;padding-bottom: 5px;padding-bottom: 5px" class="row">
                                            <div style="font-size: 12px;text-align: right" class="col-6">Paid</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format($invoice['invoice_amount'] - $invoice['due_amount'],2,'.',',')); ?></div>
                                        </div>

                                        <div style="padding-top: 5px;padding-bottom: 5px;padding-bottom: 5px" class="row">
                                            <div style="font-size: 12px;text-align: right" class="col-6">Due Balance</div>
                                            <div style="font-size: 12px;text-align: right;border-bottom: 1px dashed gray" class="col-6"><?php echo e(number_format(($sub_total + $total_vat_amount - $total_discount_amount + $previous_dues) - ($invoice['invoice_amount'] - $invoice['due_amount']),2,'.',',')); ?></div>
                                        </div>
                                    </div>
                                </div>

                                <br>

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 15px;font-weight: bold" class="col-12">Thanks for your bussiness co-operation</div>
                                    <div style="text-align: center;font-size: 15px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if($user_info['pos_printer'] == 2 || $user_info['pos_printer'] == 3): ?>
                <div class="main-content">
                    <div class="page-content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12">
                                    <div class="page-title-box d-flex align-items-center justify-content-between">
                                        <h4 class="mb-0 font-size-18">Sales</h4>

                                        <div class="page-title-right">
                                            <ol class="breadcrumb m-0">
                                                <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                                <li class="breadcrumb-item active">Show</li>
                                            </ol>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-print-none">
                                                <div class="float-right">
                                                    <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <?php if($user_info['header_image'] == null): ?>
                                                    <div class="col-md-2 col-xs-12 col-sm-12"></div>
                                                    <div class="col-md-8 col-xs-12 col-sm-12">
                                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 55px"><?php echo e($user_info['organization_name']); ?></h2>
                                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['address']); ?></p>
                                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold"><?php echo e($user_info['contact_email']); ?></p>
                                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['contact_number']); ?></p>
                                                    </div>
                                                    <div class="col-md-2 col-xs-12 col-sm-12"></div>
                                                    <div class="col-md-5 col-xs-12 col-sm-12"></div>
                                                    <div class="col-md-2 col-xs-12 col-sm-12">
                                                        <h2 style="margin-top: 20px;margin-bottom: 0px;text-align: center;font-size: 35px;" class="textstyle">Invoice</h2>
                                                    </div>
                                                    <div class="col-md-5 col-xs-12 col-sm-12"></div>
                                                <?php else: ?>
                                                    <img class="float-left" src="<?php echo e(url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image'])); ?>" alt="logo" style="width: 100%" />
                                                <?php endif; ?>
                                            </div>

                                            <div style="margin-top: 20px" class="row">
                                                <div style="font-size: 16px" class="col-sm-4 col-6">
                                                    <address>
                                                        <strong>Customer Name &nbsp;&nbsp;: 
                                                        <?php echo e($invoice['customer_name']); ?> </strong><br>
                                                        <strong>Address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong>
                                                        <?php if($invoice['address'] != null): ?>
                                                           <?php echo $invoice['address']; ?> <br>
                                                        <?php endif; ?>
                                                        <?php if($invoice['address'] == null): ?>
                                                            <br>
                                                        <?php endif; ?>
                                                        <strong>Mobile &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                        <?php echo e($invoice['phone']); ?> </strong>
                                                    </address>
                                                </div>

                                                <div class="col-sm-3 hidden-xs">
                                                    <address>
                                                        
                                                    </address>
                                                </div>

                                                <div style="font-size: 16px" class="col-sm-5 col-6 text-sm-left">
                                                    <address style=";border-bottom: 2px solid black">
                                                        <strong>Invoice Number &nbsp;&nbsp;: </strong>
                                                        <?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?> <br>
                                                        <strong>Ref No &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong>

                                                        <br>
                                                        <strong>Sold By &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong>
                                                        <?php echo e($invoice->createdBy->name); ?>

                                                        <br>
                                                    </address>
                                                    <address>
                                                        <strong>Date & Time : </strong>
                                                        <?php echo e(date('d/m/Y', strtotime($invoice['invoice_date']))); ?> &nbsp;&nbsp;&nbsp; Print Time : <?php echo e(date('h:i:s A', strtotime($invoice['created_at']))); ?>

                                                    </address>
                                                </div>
                                            </div>

                                            <div style="border: 2px solid black;height: 1180px" class="table-responsive">
                                                <table style="border-bottom: 1px solid gray" class="table column-bordered-table">
                                                    <thead style="font-size: 18px">
                                                        <tr>
                                                            <th style="width: 70px;">SL</th>
                                                            <th>Description</th>
                                                            <th class="text-left">Warrenty</th>
                                                            <th class="text-right">Sold Qty</th>
                                                            <th class="text-right">Unit Price</th>
                                                            <th class="text-right">Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="font-size: 16px">

                                                        <?php if(!empty($entries) && ($entries->count() > 0)): ?>

                                                        <?php $sub_total = 0; ?>

                                                        <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr>
                                                            <td><?php echo e($key + 1); ?></td>
                                                            <td>
                                                                <?php if($value['product_type'] == 1): ?>
                                                                    <?php echo $value['product_entry_name']; ?>
                                                                <?php else: ?>
                                                                    <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                                                <?php endif; ?>

                                                                <?php
                                                                    $w_pr = WarrentyProducts($value['warrenty_product_id']);
                                                                ?>

                                                                <?php if($w_pr != null): ?>
                                                                <br>
                                                                <br>
                                                                <strong>
                                                                    <?php echo $w_pr; ?>
                                                                </strong>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td class="text-left">
                                                            <?php echo e(dateDifference($value['from_date'] , $value['to_date'] , '%m Month %d Day')); ?> 
                                                            </td>
                                                            <td class="text-right"><?php echo e(number_format($value['quantity'],2,'.',',')); ?></td>
                                                            <td class="text-right"><?php echo e(number_format($value['rate'],2,'.',',')); ?></td>
                                                            <td class="text-right"><?php echo e(number_format($value['total_amount'],2,'.',',')); ?></td>
                                                        </tr>

                                                        <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <?php endif; ?>
                                                    
                                                    </tbody>
                                                </table>

                                                <div style="margin-top: 20px" class="row">
                                                    <div style="vertical-align: middle;text-align: center" class="col-sm-7 hidden-xs">
                                                        <?php if($invoice['due_amount'] == 0): ?>
                                                        <h1>PAID</h1>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div style="font-size: 16px;border-bottom: 3px solid black" class="col-sm-5 col-6 text-sm-left">
                                                        <address>
                                                            Total Amount 
                                                            <span style="float: right;padding-right: 10px"><?php echo e(number_format($sub_total,2,'.',',')); ?></span> <br>

                                                            Add Vat 
                                                            <span style="float: right;padding-right: 10px"><?php echo e($invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : number_format($invoice['total_vat'],2,'.',',')); ?></span> <br>

                                                            Less Discount 
                                                            <span style="float: right;padding-right: 10px"><?php echo e($invoice['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$invoice['total_vat'])/100))*$invoice['total_discount_amount'])/100) : number_format($invoice['total_discount_amount'],2,'.',',')); ?></span> <br>

                                                            Add Installation / Service Charge 
                                                            <span style="float: right;padding-right: 10px"></span> <br>
                                                            
                                                        </address>
                                                    </div>
                                                </div>

                                                <div style="margin-top: 5px" class="row">
                                                    <div style="vertical-align: middle;text-align: center" class="col-sm-7 hidden-xs">
                                                    </div>

                                                    <div style="font-size: 16px;" class="col-sm-5 col-6 text-sm-left">
                                                        <address>
                                                            Net Payable Amount
                                                            <span style="float: right;padding-right: 10px"><?php echo e(number_format($invoice['invoice_amount'] - $invoice['due_amount'],2,'.',',')); ?></span> 
                                                        </address>
                                                    </div>
                                                </div>

                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <br>

                                                <div class="row">

                                                    <div style="text-align: left" class="col-md-6">
                                                        
                                                        <span style="border-top: 1px solid black"> Customer Signature </span>
                                                    </div>

                                                    <div style="text-align: right" class="col-md-6">

                                                        <span style="border-top: 1px solid black"> Authorised Signature </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end col -->
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    $( document ).ready(function() {
        javascript:window.print();
    });

    window.onafterprint = function(e){
        var site_url  = $('.site_url').val();
        window.location.replace(site_url + '/invoices');
    };
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/29/Modules/Invoices/Resources/views/show_pos.blade.php ENDPATH**/ ?>