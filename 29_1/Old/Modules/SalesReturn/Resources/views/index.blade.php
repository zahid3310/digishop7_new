@extends('layouts.app')

@section('title', 'Sales Return')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales Return</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Sales Return</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('sales_return_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="return_date">Return Date *</label>
                                            <input id="return_date" name="return_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Select Customer * </label>
                                            <select id="customer_id" name="customer_id" class="form-control select2 col-md-12" onchange="InvoiceList()" required>
                                               <option value="">-- Select Customer --</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Order Number * </label>
                                            <select id="invoice_id" name="invoice_id" class="form-control select2 col-md-12" onchange="invoiceDetails()" required>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="return_note">Note</label>
                                            <input id="return_note" name="return_note" type="text" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="card-title">Order Details</h4>

                                        <div class="inner-repeater mb-4">
                                            <div data-repeater-list="inner-group" class="inner form-group row">
                                                <div id="invoice_entry_list" class="inner col-lg-12 ml-md-auto">
                                                    
                                                </div>
                                            </div>
                                        </div>

                                        <div style="display: none" id="totalDivShow" class="row justify-content-end">
                                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 customAlign table-responsive">
                                                <table class="table input_fields_wrap table-bordered table-striped dataTable">
                                                    <tfoot style="line-height: 0px">
                                                        <tr style="border-color: white">
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="text-align: right;border-color: white"><b>Sub Total</b></th>
                                                            <th style="text-align: right;border-color: white"></th>
                                                            <th style="border-color: white">
                                                                <a class="width-xs" style="border: none;text-decoration: none;color: black" id="subTotalBdtShow">0.00</a>
                                                            </th>
                                                            <th style="border-color: white">
                                                                <input style="display: none"  type="text" id="subTotalBdt" name="sub_total_amount">
                                                            </th>
                                                        </tr>

                                                        <tr style="border-color: white">
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="text-align: right;border-color: white;vertical-align: middle !important"><b>Discount</b></th>
                                                            <th style="text-align: right;border-color: white">
                                                                <select style="padding: 6px;border-radius: 4px" class="totalDiscountType" id="total_discount_type_0" name="total_discount_type">
                                                                </select>
                                                            </th>
                                                            <th style="border-color: white">
                                                                <input id="total_discount_0" type="text" class="form-control totalDiscount width-xs" name="total_discount_amount" value="0" readonly>
                                                            </th>
                                                            <th style="border-color: white"></th>
                                                        </tr>

                                                        <tr style="border-color: white">
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="text-align: right;border-color: white;vertical-align: middle !important"><b>Discount</b></th>
                                                            <th style="text-align: right;border-color: white;vertical-align: middle !important"><b>Note</b></th>
                                                            <th style="border-color: white">
                                                                <textarea id="total_discount_note" type="text" class="form-control width-xs" name="total_discount_note" readonly></textarea>
                                                            </th>
                                                            <th style="border-color: white"></th>
                                                        </tr>
                                                        
                                                        <tr style="border-color: white;">
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="text-align: right;border-color: white;vertical-align: middle !important"><b>Vat</b></th>
                                                            <th style="text-align: right;border-color: white">
                                                                <select style="padding: 6px;border-radius: 4px" class="vatType" id="vat_type_0" name="vat_type">
                                                                </select>
                                                            </th>
                                                            <th style="border-color: white">
                                                                <input id="vat_amount_0" type="text" class="form-control vatAmount width-xs" name="vat_amount" value="0" readonly>
                                                            </th>
                                                            <th style="border-color: white"></th>
                                                        </tr>

                                                        <tr style="border-color: white;display: none">
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="text-align: right;border-color: white;vertical-align: middle !important"><b>Tax</b></th>
                                                            <th style="text-align: right;border-color: white">
                                                                <select style="padding: 6px;border-radius: 4px" class="taxType" id="tax_type_0" name="tax_type">
                                                                </select>
                                                            </th>
                                                            <th style="border-color: white">
                                                                <input id="tax_amount_0" type="text" class="form-control taxAmount width-xs" name="tax_amount" value="0" readonly>
                                                            </th>
                                                            <th style="border-color: white"></th>
                                                        </tr>

                                                        <tr style="border-color: white">
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="text-align: right;border-color: white"><b>Total</b></th>
                                                            <th style="text-align: right;border-color: white">(BDT)</th>
                                                            <th style="border-color: white">
                                                                <a class="width-xs" style="border: none;text-decoration: none;color: black" id="totalBdtShow">0.00</a>
                                                            </th>
                                                            <th style="border-color: white">
                                                                <input style="display: none" type="text" id="totalBdt" name="total_amount">
                                                            </th>
                                                        </tr>

                                                        <tr style="border-color: white">
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="border-color: white" class="hidden-xs"></th>
                                                            <th style="text-align: right;border-color: white;vertical-align: middle !important"><b>Return</b></th>
                                                            <th style="text-align: right;border-color: white;vertical-align: middle !important">(BDT)</th>
                                                            <th style="border-color: white">
                                                                <input id="totalReturnedBdt" type="text" class="form-control width-xs" name="total_return_amount">
                                                            </th>
                                                            <th style="border-color: white"></th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <h4 class="card-title">Cash Back Information</h4>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="amount_paid">Amount Paid</label>
                                            <input id="amount_paid" name="amount_paid" type="text" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-4 form-group">
                                        <label class="control-label">Paid Through</label>
                                        <select style="cursor: pointer;" name="paid_through" class="form-control select2">
                                            @if(!empty($paid_accounts) && ($paid_accounts->count() > 0))
                                            @foreach($paid_accounts as $key => $paid_account)
                                                <option value="{{ $paid_account['id'] }}">{{ $paid_account['name'] }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="note">Note</label>
                                            <input id="note" name="note" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <hr style="margin-top: 0px">
                                
                                <div class="form-group row">
                                    <div class="button-items col-lg-12">
                                        <button name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Save & Print</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('sales_return_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Sales Returns</h4>

                                <div style="margin-right: 0px" class="row">
                                    <div class="col-lg-9 col-md-6 col-sm-4 col-4"></div>
                                    <div class="col-lg-1 col-md-2 col-sm-4 col-4">Search : </div>
                                    <div class="col-lg-2 col-md-4 col-sm-4 col-4">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th>Return#</th>
                                            <th>Invoice#</th>
                                            <th>Customer</th>
                                            <th>Amount</th>
                                            <th>Paid</th>
                                            <th>Due</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="sales_return_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Are you sure want to delete the entry ?</label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="delete_btn" class="btn btn-primary waves-effect waves-light delete_btn">Delete</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0)
                    {
                        var type = 'Customer';
                    }

                    if (result['contact_type'] == 1)
                    {
                        var type = 'Supplier';
                    }

                    if (result['contact_type'] == 2)
                    {
                        var type = 'Employee';
                    }

                    if (result['contact_type'] == 3)
                    {
                        var type = 'Agent';
                    }

                    if (result['contact_type'] == 0 || result['contact_type'] == 1)
                    {
                        return result['text'] + ' | ' + type;
                    }
                },
            });

            if (location.search != "")
            {
                var locationValue = (new URL(location.href)).searchParams.get('customer_id'); 
            
                if (locationValue != null)
                {
                    var selected_customer_id = (new URL(location.href)).searchParams.get('customer_id');
                    $.get(site_url + '/salesreturn/find-customer-name/' + selected_customer_id, function(data){
                        var customer_name = data.name;
                        $("#customer_id").empty().append('<option value="'+ selected_customer_id +'">' + customer_name + '</option>').val(selected_customer_id);
                    });

                    var selected_invoice_id  = (new URL(location.href)).searchParams.get('invoice_id');
                    $.get(site_url + '/salesreturn/find-invoice-details/' + selected_invoice_id, function(data){
                        $("#invoice_id").empty().append('<option value = "' +  selected_invoice_id + '">' + 'INV - ' + data.invoice_number.padStart(6, '0') + ' | Date : ' + formatDate(data.invoice_date) + ' | Amount : ' + data.invoice_amount + '</option>').val(selected_invoice_id).trigger('change');
                    });
                }
            }

            $('#registeredCustomer').hide();
            $('#newCustomer').hide();
            $('.newCustomer').hide();

            var site_url        = $('.site_url').val();

            $.get(site_url + '/salesreturn/sales-return/list/load', function(data){

                var sales_return_list = '';
                $.each(data, function(i, sales_return_data)
                {   
                    var serial      = parseFloat(i) + 1;
                    var delete_url  = site_url + '/salesreturn/delete/' + sales_return_data.id;
                    var print_url   = site_url + '/salesreturn/show/' + sales_return_data.id;

                    sales_return_list += '<tr>' +
                                        '<input class="form-control salesReturnId" type="hidden" value="' +  sales_return_data.id + '">' +
                                        '<td>' +
                                            serial +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(sales_return_data.sales_return_date) +
                                        '</td>' +
                                        '<td>' +
                                           'SR - ' + sales_return_data.sales_return_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                           'INV - ' + sales_return_data.invoice_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            sales_return_data.customer_name +
                                        '</td>' +
                                        '<td>' +
                                           (sales_return_data.return_amount).toFixed(2) +
                                        '</td>' +
                                        '<td>' +
                                           (parseFloat(sales_return_data.return_amount) - parseFloat(sales_return_data.due_amount)).toFixed(2) +
                                        '</td>' +
                                        '<td>' +
                                           (sales_return_data.due_amount).toFixed(2) +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '@if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3)' +
                                                    '<a class="dropdown-item" href="' + delete_url +'" data-toggle="modal" data-target="#myModal">' + 'Delete' + '</a>' +
                                                    '@endif' +
                                                    '<a class="dropdown-item" href="' + print_url +'" target="_blank">' + 'Print' + '</a>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                });

                $("#sales_return_list").empty();
                $("#sales_return_list").append(sales_return_list);
            });
        });

        function searchPayment()
        {
            var search_text     = $('#searchPayment').val();
            var site_url        = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/salesreturn/sales-return/search/list/' + search_text, function(data){

                var sales_return_list = '';
                $.each(data, function(i, sales_return_data)
                {   
                    var serial      = parseFloat(i) + 1;
                    var delete_url  = site_url + '/salesreturn/delete/' + sales_return_data.id;
                    var print_url   = site_url + '/salesreturn/show/' + sales_return_data.id;

                    sales_return_list += '<tr>' +
                                        '<input class="form-control salesReturnId" type="hidden" value="' +  sales_return_data.id + '">' +
                                        '<td>' +
                                            serial +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(sales_return_data.sales_return_date) +
                                        '</td>' +
                                        '<td>' +
                                           'SR - ' + sales_return_data.sales_return_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                           'INV - ' + sales_return_data.invoice_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            sales_return_data.customer_name +
                                        '</td>' +
                                        '<td>' +
                                           (sales_return_data.return_amount).toFixed(2) +
                                        '</td>' +
                                        '<td>' +
                                           (parseFloat(sales_return_data.return_amount) - parseFloat(sales_return_data.due_amount)).toFixed(2) +
                                        '</td>' +
                                        '<td>' +
                                           (sales_return_data.due_amount).toFixed(2) +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '@if(Auth::user()->role == 1)' +
                                                    '<a class="dropdown-item" href="' + delete_url +'" data-toggle="modal" data-target="#myModal">' + 'Delete' + '</a>' +
                                                    '@endif' +
                                                    '<a class="dropdown-item" href="' + print_url +'" target="_blank">' + 'Print' + '</a>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                });

                $("#sales_return_list").empty();
                $("#sales_return_list").append(sales_return_list);
            });
        }
    </script>

    <script type="text/javascript">
        function InvoiceList() 
        {
            var customer_id     = $('#customer_id').val();
            var site_url        = $(".site_url").val();

            if(customer_id)
            {   
                $.get(site_url + '/salesreturn/invoice-list-by-customer/'+ customer_id, function(data){

                    var list = '';

                    list    += '<option value = "">' + '--Select Invoice--' + '</option>';

                    $.each(data, function(i, data)
                    {
                        list += '<option value = "' +  data.id + '">' + 'INV - ' + data.invoice_number.padStart(6, '0') + ' | Date : ' + formatDate(data.invoice_date) + ' | Amount : ' + data.invoice_amount.toFixed(2) + '</option>';

                    });

                    $("#invoice_id").empty();
                    $("#invoice_id").append(list);
                });
            }
        }

        function invoiceDetails()
        {
            $("#totalDivShow").show();

            var site_url    = $('.site_url').val();
            var invoice_id  = $('#invoice_id').val();

            $.get(site_url + '/salesreturn/invoice-entries-list-by-invoice/' + invoice_id, function(data){

                var invoice_enrty_list  = '';
                var sub_total           = 0;
                var serial              = 1;
                $.each(data.invoice_entries, function(i, invoice_entry_data)
                {
                    if (serial == 1)
                    {
                        var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                        var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                        var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                        var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">Discount</label>\n';
                        var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n';
                        var amount_label        = '<label class="hidden-xs" for="productname">Amount</label>\n';
                        var action_label        = '<label class="hidden-xs" for="productname">Return Qty</label>\n';
                    }
                    else
                    {
                        var product_label       = '';
                        var rate_label          = '';
                        var quantity_label      = '';
                        var discount_label      = '';
                        var type_label          = '';
                        var amount_label        = '';
                        var action_label        = '';
                    }

                    sub_total           += parseFloat(invoice_entry_data.total_amount);

                    if (invoice_entry_data.discount_type == 1)
                    {
                        var discoumt_percent    =   '<div style="padding-left: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-md-5 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n' +
                                                                type_label +
                                                        '<select name="discount_type[]" class="form-control">\n' +
                                                            '<option value="1">BDT</option>' +
                                                        '</select>\n';
                    }
                    else
                    {
                        var discoumt_percent    =   '<div style="padding-left: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-md-5 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n' +
                                                                type_label +
                                                        '<select name="discount_type[]" class="form-control">\n' +
                                                            '<option value="0">%</option>' +
                                                        '</select>\n';
                    }

                    if (invoice_entry_data.quantity == 0)
                    {
                        var return_quantity = '<input id="return_quantity_'+ serial +'" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="0" oninput="calculate('+ serial +')" readonly />\n' 
                    }
                    else
                    {
                        var return_quantity = '<input id="return_quantity_'+ serial +'" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="0" oninput="calculate('+ serial +')" />\n' 
                    }

                    invoice_enrty_list += ' ' + '<div style="margin-bottom: 0px !important" class="mb-3 row align-items-center">' +
                                                    '<div style="padding: 10px;padding-bottom: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 co-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Category *</label>\n' +
                                                        '<input type="text" class="inner form-control" value="'+ invoice_entry_data.product_name +'" readonly />\n' +
                                                        '<input name="product_id[]" type="hidden" value="'+ invoice_entry_data.product_id +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-3 col-md-3 col-sm-6 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<input type="text" class="inner form-control" value="'+ invoice_entry_data.entry_name + ' ( ' + pad(invoice_entry_data.product_code, 6) + ' )' +'" readonly />\n' +
                                                        '<input name="product_entries[]" type="hidden" value="'+ invoice_entry_data.product_entry_id +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input id="rate_'+ serial +'" type="text" class="inner form-control rate" value="'+ invoice_entry_data.rate +'" readonly />\n' + 
                                                        '<input name="rate[]" type="hidden" value="'+ invoice_entry_data.rate +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label +
                                                        '<input id="quantity_'+ serial +'" type="text" class="inner form-control" value="'+ invoice_entry_data.quantity +'" readonly />\n' +
                                                        '<input name="quantity[]" type="hidden" class="quantity" value="'+ invoice_entry_data.original_quantity +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 10px;padding-bottom: 0px" class="col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" class="inner form-control" value="'+ invoice_entry_data.discount_amount +'"  readonly />\n' +
                                                                '<input name="discount_amount[]" type="hidden" value="'+ invoice_entry_data.discount_amount +'" />\n' +
                                                            '</div>\n' +

                                                            discoumt_percent +
                                                            
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Amount</label>\n' +
                                                        amount_label +
                                                        '<input id="amount_'+ serial +'" type="text" class="inner form-control amount" value="'+ invoice_entry_data.total_amount +'" readonly />\n' + 
                                                        '<input name="amount[]" type="hidden" value="'+ invoice_entry_data.total_amount +'" readonly />\n' +
                                                    '</div>\n' + 

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Return Qty</label>\n' +
                                                        action_label +
                                                        return_quantity +
                                                    '</div>\n' +
                                                '</div>\n';

                                                serial++;                             
                });

                if (data.invoice.total_discount_type == 1)
                {
                    var total_discount_type    = '<option value="1" selected>' + 'BDT' + '</option>\n';
                }
                else
                {
                    var total_discount_type    = '<option value="0" selected>' + '%' + '</option>\n';
                }

                if (data.invoice.vat_type == 1)
                {
                    var vat_type    = '<option value="1" selected>' + 'BDT' + '</option>\n';
                }
                else
                {
                    var vat_type    = '<option value="0" selected>' + '%' + '</option>\n';
                }

                if (data.invoice.tax_type == 1)
                {
                    var tax_type    = '<option value="1" selected>' + 'BDT' + '</option>\n';
                }
                else
                {
                    var tax_type    = '<option value="0" selected>' + '%' + '</option>\n';
                }
 
                $("#total_discount_type_0").empty();
                $("#total_discount_type_0").append(total_discount_type);
                $("#vat_type_0").empty();
                $("#vat_type_0").append(vat_type);
                $("#tax_type_0").empty();
                $("#tax_type_0").append(tax_type);

                $("#subTotalBdtShow").html(sub_total.toFixed(2));
                $("#subTotalBdt").val(sub_total);
                $("#totalBdtShow").html(data.invoice.invoice_amount.toFixed(2));
                $("#totalBdt").val(data.invoice.invoice_amount);
                $("#total_discount_0").val(data.invoice.total_discount_amount);
                $("#total_discount_note").val(data.invoice.total_discount_note);
                $("#vat_amount_0").val(data.invoice.total_vat);
                $("#tax_amount_0").val(data.invoice.total_tax);

                $("#invoice_entry_list").empty();
                $("#invoice_entry_list").append(invoice_enrty_list);
            });
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function calculate(x)
        {
            var return_quantity     = $('.returnQuantity').map((_,el) => el.value).get();
            var amount              = $('.amount').map((_,el) => el.value).get();
            var quantity            = $('.quantity').map((_,el) => el.value).get();
            var rate                = $('.rate').map((_,el) => el.value).get();

            var vatType             = $("#vat_type_0").val();
            var vatAmount           = $("#vat_amount_0").val();
            var taxType             = $("#tax_type_0").val();
            var taxAmount           = $("#tax_amount_0").val();
            var totalDiscount       = $("#total_discount_0").val();
            var totalDiscountType   = $("#total_discount_type_0").val();
            var totalBdt            = $("#totalBdt").val();
            var subTotal            = $("#subTotalBdt").val();

            var total                   = 0;
            var total_quantity          = 0;
            var total_return_quantity   = 0;

            for (var i = 0; i < return_quantity.length; i++)
            {
                if (return_quantity[i] > 0)
                {   
                    var result   = (parseFloat(amount[i])/parseFloat(quantity[i]))*parseFloat(return_quantity[i]);

                    total       += parseFloat(result);
                }

                total_quantity          += parseFloat(quantity[i]);
                total_return_quantity   += parseFloat(return_quantity[i]);
            }

            if (vatType == 0)
            {
                var vatTypeCal     = (parseFloat(total)*parseFloat(vatAmount))/100;
            }
            else
            {
                var vatTypeCal     = (parseFloat(vatAmount)*parseFloat(total))/parseFloat(subTotal);
            }

            if (totalDiscountType == 0)
            {
                var totalDiscountCal     = ((parseFloat(total) + parseFloat(vatTypeCal))*parseFloat(totalDiscount))/100;
            }
            else
            {
                var totalDiscountCal     = (parseFloat(total)*parseFloat(totalDiscount))/(parseFloat(subTotal) + parseFloat(vatAmount));
            }

            if (taxType == 0)
            {
                var taxTypeCal     = (parseFloat(total)*parseFloat(taxAmount))/100;
            }
            else
            {
                var taxTypeCal     = $("#tax_amount_0").val();
            }

            var total_result       = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountCal);

            $('#totalReturnedBdt').val(total_result.toFixed(2));
            $("#amount_paid").val(parseFloat(total_result.toFixed(2)));
        }

        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>

    <script type="text/javascript">
        $('.delete_btn').click(function () {
            var site_url            = $('.site_url').val();
            var id                  = $('.salesReturnId').val();
            window.location.href    = site_url + "/salesreturn/delete/"+id;
        });
    </script>
@endsection