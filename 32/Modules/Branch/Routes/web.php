<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('branch')->group(function() {
    Route::get('/', 'BranchController@index')->name('branch_index');
    Route::post('/store', 'BranchController@store')->name('branch_store');
    Route::get('/edit/{id}', 'BranchController@edit')->name('branch_edit');
    Route::post('/update/{id}', 'BranchController@update')->name('branch_update');
    Route::get('/assign-branch', 'BranchController@assignBranch')->name('branch_assign_branch');
    Route::post('/assign-branch-update', 'BranchController@assignBranchUpdate')->name('branch_assign_branch_update');
    Route::get('/get-branch-list', 'BranchController@getBranchList')->name('branch_get_branch_list');
});
