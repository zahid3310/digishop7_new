@extends('layouts.app')

@section('title', 'Customer Due List')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')

    <style type="text/css">
        .custom td, .custom th {
            border: 1px solid #000!important;
          }
    </style>

    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Customer Due List</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.reports')}}</a></li>
                                    <li class="breadcrumb-item active">Customer Due List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <div class="row">

                                


                                        <?php
                                            $date = date('d-m-Y');
                                            $search=array("0","1","2","3","4","5",'6',"7","8","9"); 
                                            $replace=array("০","১","২","৩","৪","৫","৬","৭","৮","৯");
                                            $value = str_replace($search,$replace,$date);   
                                        ?>

                                    
                                    
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <h1 style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 30px;">{{ $user_info['organization_name'] }}</h1>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-weight: bold;font-size: 25px;">{{ $user_info['address'] }}</p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-weight: bold;font-size: 30px;">{{ $user_info['contact_number'] }}</p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-weight: bold;font-size: 30px;">Customer Due List</h4> 

                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-weight: bold;font-size: 30px;">
                                             @if(!empty($area_name->area_name))
                                              {{$area_name->area_name}}
                                             @endif
                                        </h4>

                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-weight: bold;font-size: 30px;">
                                             তারিখ : {{$value}}
                                        </h4>

                                    </div>
                                   
                                </div>

                                <!-- <?php echo($tatalData = round(count($customers)/2)); ?> -->

                                <br>

                                <form method="get" action="{{ route('due_list_report_customer_index') }}" enctype="multipart/form-data">
                                    <div class="form-group row">

                                        <div class="col-lg-4 col-md-4 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="0" selected>--{{__('messages.all_customer')}}--</option>
                                                    @if(!empty($customers) && ($customers->count() > 0))
                                                    @foreach($customers as $key => $value)
                                                        <option {{ isset($_GET['customer_id']) && ($_GET['customer_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-5 col-md-5 col-sm-8 col-6 d-print-none">
                                            <div class="form-group">
                                                <select style="width: 100" class="form-control select2" name="area_id">
                                                    <option value="0" selected>--All Area--</option>
                                                    @if(!empty($area) && ($area->count() > 0))
                                                    @foreach($area as $key => $value)
                                                        <option value="{{ $value['id'] }}" {{ isset($_GET['area_id']) && ($_GET['area_id'] == $value['id']) ? 'selected' : '' }}>{{ $value['name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="{{ __('messages.search')}}"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <div class="row">
                                    <div class="col-6">
                                        <table class="custom" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center;width:10%!important">{{ __('messages.sl')}}</th>
                                                    <th style="text-align: center;width:30%!important">{{ __('messages.customer')}}</th>
                                                    <th style="text-align: center;width:30%!important">{{ __('messages.address')}}</th>
                                                    <th style="text-align: center;width:20%!important">{{ __('messages.balance')}}</th>
                                                    <th style="width:10%!important" class="d-print-none">{{ __('messages.action')}}</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php
                                                $i                  = 1;
                                                $tatalData = round(count($customers)/2);
                                                $address = null;
                                                 // dd($tatalData);

                                                ?>
                                                @foreach($customers as $key1 => $value)

                                                @if($i <= $tatalData)
                                                
                                                <?php
                                                        $search=array("0","1","2","3","4","5",'6',"7","8","9"); 
                                                        $replace=array("০","১","২","৩","৪","৫","৬","৭","৮","৯");
                                                        $bangla_serial = str_replace($search,$replace,$i);      
                                                        $bangla_customer_balance = str_replace($search,$replace,$value['customers_balance']);      
                                                 ?>
                                                <tr>
                                                    <td style="text-align: center;font-size: 20px;">{{ $bangla_serial }}</td>
                                                    <td style="text-align: center;font-size: 20px;">{{ $value['customer_name'] }} </td>
                                                    @if($address != $value['address'])
                                                    <td style="text-align: center;font-size: 20px;background-color:#B8CB19">{{ $address = $value['address'] }}</td>
                                                    @else
                                                    <td style="text-align: center;font-size: 20px;">{{ $value['address'] }}</td>
                                                    @endif
                                                    <td style="text-align: center;font-size: 20px;">

                                                    @if(($value['customers_balance'] == 0) || ($value['customers_balance'] < 0))
                                                    ---
                                                    @else
                                                    {{ $bangla_customer_balance }}
                                                    @endif
                                                   </td>

                                                    <td style="text-align: center;" class="d-print-none">
                                                        <div class="dropdown">
                                                            <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                                <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                            </a>
                                                            <div class="dropdown-menu dropdown-menu-right" style="">
                                                                <a style="cursor: pointer" class="dropdown-item" data-toggle="modal" data-target="#myModal_{{$value['customer_id']}}" >{{ __('messages.details')}}</a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                 $i++;
                                                 
                                                ?>
                                                

                                                

                                                <div id="myModal_{{$value['customer_id']}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title mt-0" id="myModalLabel">{{ __('messages.select_date_range')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>

                                                            <form method="get" action="{{ route('due_list_customer_details', $value['customer_id']) }}" enctype="multipart/form-data" target="_blank">

                                                                <div style="padding-top: 0px !important" class="modal-body">
                                                                    <div style="margin-bottom: 0px !important" class="form-group row">
                                                                        <label for="example-text-input" class="col-md-12 col-form-label">{{ __('messages.from_date')}} *</label>
                                                                        <div class="col-md-12">
                                                                            <input style="cursor: pointer" id="from_date" name="from_date" type="date" class="form-control" value="<?= date("2020-01-01") ?>" required>
                                                                        </div>
                                                                    </div>

                                                                    <div style="margin-bottom: 0px !important" class="form-group row">
                                                                        <label for="example-text-input" class="col-md-12 col-form-label">{{ __('messages.to_date')}} *</label>
                                                                        <div class="col-md-12">
                                                                            <input style="cursor: pointer" id="to_date" name="to_date" type="date" class="form-control" value="<?= date("Y-m-d") ?>" required>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">{{ __('messages.search')}}</button>
                                                                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">{{ __('messages.close')}}</button>
                                                                </div>

                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach

                                            </tbody>

                                        </table>
                                    </div>
                                    <div class="col-6">

                                        <table class="custom" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center;width:10%!important">{{ __('messages.sl')}}</th>
                                                    <th style="text-align: center;width:30%!important">{{ __('messages.customer')}}</th>
                                                    <th style="text-align: center;width:30%!important">{{ __('messages.address')}}</th>
                                                    <th style="text-align: center;width:20%!important">{{ __('messages.balance')}}</th>
                                                    <th style="text-align: center;width:10%!important" class="d-print-none">{{ __('messages.action')}}</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php
                                                $i                  = 1;
                                                $tatalData1 = round(count($customers)/2);
                                                $address2 = null;
                                                $total_due       = 0;

                                                

                                                ?>
                                                @foreach($customers as $key2 => $value)
                                                @if($i > $tatalData1)
                                                
                                                <?php
                                                        $search=array("0","1","2","3","4","5",'6',"7","8","9"); 
                                                        $replace=array("০","১","২","৩","৪","৫","৬","৭","৮","৯");
                                                        $bangla_serial1 = str_replace($search,$replace,$i);      
                                                        $bangla_customer_balance1 = str_replace($search,$replace,$value['customers_balance']);      
                                                 ?>
                                                <tr>
                                                    <td style="text-align: center;font-size: 20px;">{{ $bangla_serial1 }}</td>
                                                    <td style="text-align: center;font-size: 20px;">{{ $value['customer_name'] }} </td>
                                                    @if($address2 != $value['address'])
                                                    <td style="text-align: center;font-size: 20px;background-color:#B8CB19;">{{ $address2 = $value['address'] }}</td>
                                                    @else
                                                    <td style="text-align: center;font-size: 20px;">{{ $value['address'] }}</td>
                                                    @endif
                                                    <td style="text-align: center;font-size: 20px;">
                                                    @if(($value['customers_balance'] == 0) || ($value['customers_balance'] < 0))
                                                    ---
                                                    @else
                                                    {{ $bangla_customer_balance1 }}
                                                    @endif
                                                    </td>

                                                    <td style="text-align: center;" class="d-print-none">
                                                        <div class="dropdown">
                                                            <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                                <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                            </a>
                                                            <div class="dropdown-menu dropdown-menu-right" style="">
                                                                <a style="cursor: pointer" class="dropdown-item" data-toggle="modal" data-target="#myModal_{{$value['customer_id']}}" >{{ __('messages.details')}}</a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                

                                               

                                                <div id="myModal_{{$value['customer_id']}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title mt-0" id="myModalLabel">{{ __('messages.select_date_range')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>

                                                            <form method="get" action="{{ route('due_list_customer_details', $value['customer_id']) }}" enctype="multipart/form-data" target="_blank">

                                                                <div style="padding-top: 0px !important" class="modal-body">
                                                                    <div style="margin-bottom: 0px !important" class="form-group row">
                                                                        <label for="example-text-input" class="col-md-12 col-form-label">{{ __('messages.from_date')}} *</label>
                                                                        <div class="col-md-12">
                                                                            <input style="cursor: pointer" id="from_date" name="from_date" type="date" class="form-control" value="<?= date("2020-01-01") ?>" required>
                                                                        </div>
                                                                    </div>

                                                                    <div style="margin-bottom: 0px !important" class="form-group row">
                                                                        <label for="example-text-input" class="col-md-12 col-form-label">{{ __('messages.to_date')}} *</label>
                                                                        <div class="col-md-12">
                                                                            <input style="cursor: pointer" id="to_date" name="to_date" type="date" class="form-control" value="<?= date("Y-m-d") ?>" required>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">{{ __('messages.search')}}</button>
                                                                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">{{ __('messages.close')}}</button>
                                                                </div>

                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                <?php $i++; 
                                                
                                                ?>

                                                <?php 
                                                if( $value['customers_balance'] > 0){
                                                $total_due       = $total_due + $value['customers_balance'];
                                                }
                                                 ?>
                                                @endforeach
                                                
                                                <?php
                                                        $search=array("0","1","2","3","4","5",'6',"7","8","9"); 
                                                        $replace=array("০","১","২","৩","৪","৫","৬","৭","৮","৯");     
                                                        $bangla_total_due = str_replace($search,$replace,$total_due);      
                                                 ?>

                                                <tr>
                                                    <th colspan="3" style="text-align: right;">{{ __('messages.total')}}</th>
                                                    <th colspan="1" style="text-align: right;">{{ $bangla_total_due }}</th>

                                                    
                                                    <th colspan="1" style="text-align: right;" class="d-print-none"></th>
                                                </tr>

                                            </tbody>

                                        </table>

                                    </div>
                                </div>

                                

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')


@endsection