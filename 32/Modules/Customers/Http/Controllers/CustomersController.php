<?php

namespace Modules\Customers\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Customers;
use App\Models\Users;
use App\Models\Areas;
use App\Models\AreaZone;
use Response;
use DB;

class CustomersController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $customers  = Customers::orderBy('created_at', 'DESC')->get();
        $users      = Users::orderBy('created_at', 'DESC')->get();
        $areas          = Areas::all();
        $area_zone          = AreaZone::all();

        return view('customers::index', compact('customers', 'users','areas','area_zone'));
    }

    public function create()
    {
        return view('customers::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                              = new Customers;
            $customers->name                        = $data['customer_name'];
            $customers->address                     = $data['address'];
            $customers->phone                       = $data['mobile_number'];
            $customers->nid_number                  = $data['nid_number'];
            $customers->alternative_contact         = $data['alternative_mobile_number'];
            $customers->contact_type                = $data['contact_type'];
            $customers->area_id                     = $data['area_id'];
            $customers->area_zone_id                = $data['area_zone_id'];
            $customers->position                    = $data['position'];
            $customers->joining_date                = date('Y-m-d', strtotime($data['joining_date']));
            $customers->designation                 = $data['designation'];
            $customers->salary                      = $data['salary'];
            $customers->branch_id                   = $branch_id;
            $customers->user_id                     = $data['user_id'];

            if($request->hasFile('image'))
            {
                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->image       = $logoUrl;
            }

            if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
            {
                $customers->opening_balance         = $data['opening_balance'];
            }

            $customers->created_by                  = $user_id;

            if ($customers->save())
            {   
                if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
                {   
                    openingBalanceStore($data['opening_balance'], $customers['id'], $data['contact_type']);
                }

                DB::commit();
                
                if ((isset($data['contact_type_reference'])))
                {
                    return redirect()->route('customers_index','contact_type='.$data['contact_type_reference'])->with("success","Contact Added Successfully !!");
                }
                else
                {
                    return back()->with("success","Contact Added Successfully !!");
                }
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('customers::show');
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $find_customer  = Customers::find($id);
        $customers      = Customers::orderBy('created_at', 'DESC')->get();
        $users          = Users::orderBy('created_at', 'DESC')->get();
        $areas          = Areas::all();
        $area_zone          = AreaZone::all();

        return view('customers::edit', compact('customers', 'find_customer', 'users','areas','area_zone'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                              = Customers::find($id);

            if ($customers['opening_balance'] != 0)
            {
                if ($customers['opening_balance'] != $data['opening_balance'])
                {
                    return back()->with("unsuccess","Opening balance can be changed only when the previouse balance is 0. !!");
                }
            }

            $customers->name                        = $data['customer_name'];
            $customers->address                     = $data['address'];
            $customers->phone                       = $data['mobile_number'];
            $customers->nid_number                  = $data['nid_number'];
            $customers->alternative_contact         = $data['alternative_mobile_number'];
            $customers->contact_type                = $data['contact_type'];
            $customers->area_id                     = $data['area_id'];
            $customers->area_zone_id                = $data['area_zone_id'];
            $customers->position                    = $data['postion'];
            $customers->joining_date                = date('Y-m-d', strtotime($data['joining_date']));
            $customers->designation                 = $data['designation'];
            $customers->salary                      = $data['salary'];
            $customers->branch_id                   = $branch_id;
            $customers->user_id                     = $data['user_id'];

            if($request->hasFile('image'))
            {   
                if ($customers->image != null)
                {
                    unlink('public/'.$customers->image);
                }

                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->image       = $logoUrl;
            }

            if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
            {
                $customers->opening_balance         = $data['opening_balance'];
            }

            $customers->updated_by                  = $user_id;

            if ($customers->save())
            {   
                if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
                {   
                    openingBalanceUpdate($data['opening_balance'], $customers['id'], $data['contact_type']);
                }

                DB::commit();

                if ((isset($data['contact_type_reference'])))
                {
                    return redirect()->route('customers_index','contact_type='.$data['contact_type_reference'])->with("success","Contact Updated Successfully !!");
                }
                else
                {
                    return redirect()->route('customers_index')->with("success","Contact Updated Successfully !!");
                }
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function customerListLoad()
    {   
        $branch_id      = Auth()->user()->branch_id;
        $data           = Customers::leftjoin('users', 'users.id', 'customers.user_id')
                                ->orderBy('customers.created_at', 'DESC')
                                ->when($branch_id != 1, function ($query) use ($branch_id) {
                                    return $query->where('customers.branch_id', $branch_id);
                                })
                                ->select('customers.*',
                                         'users.name as user_name')
                                // ->take(100)
                                ->get();

        return Response::json($data);
    }

    public function customerListSearch($id)
    {   
        $branch_id      = Auth()->user()->branch_id;
        if ($id != 'No_Text')
        {
            $data           = Customers::leftjoin('users', 'users.id', 'customers.user_id')
                                        ->where('customers.name', 'LIKE', "%$id%")
                                        ->orWhere('customers.phone', 'LIKE', "%$id%")
                                        ->orWhere('customers.alternative_contact', 'LIKE', "%$id%")
                                        ->orWhere('customers.address', 'LIKE', "%$id%")
                                        ->orWhere('customers.nid_number', 'LIKE', "%$id%")
                                        ->orderBy('customers.created_at', 'DESC')
                                        ->when($branch_id != 1, function ($query) use ($branch_id) {
                                            return $query->where('customers.branch_id', $branch_id);
                                        })
                                        ->select('customers.*',
                                                 'users.name as user_name')
                                        // ->take(100)
                                        ->get();
        }
        else
        {
            $data           = Customers::leftjoin('users', 'users.id', 'customers.user_id')
                                        ->orderBy('customers.created_at', 'DESC')
                                        ->when($branch_id != 1, function ($query) use ($branch_id) {
                                            return $query->where('customers.branch_id', $branch_id);
                                        })
                                        ->select('customers.*',
                                                 'users.name as user_name')
                                        // ->take(100)
                                        ->get();
        }

        return Response::json($data);
    }
    
    public function areaAdd()
    {
       $lists = Areas::all();
       return view('customers::add_area',compact('lists'));
    }

    public function areaAddZone()
    {
        $area = Areas::all();
        $lists = AreaZone::select('area_zone.*','areas.name as area_name')
        ->leftjoin('areas','areas.id','area_zone.area_id')
        ->orderBy('area_zone.position')
        ->get();
        return view('customers::add_area_zone',compact('lists','area'));
    }

    public function areaZoneEdit($id)
    {
        $area = Areas::all();

        $lists = AreaZone::select('area_zone.*','areas.name as area_name')
        ->leftjoin('areas','areas.id','area_zone.area_id')
        ->orderBy('area_zone.position')
        ->get();

        $zone = AreaZone::find($id);

 

 

        return view('customers::area_zone_edit',compact('area','zone','lists'));
    }

    public function areaZoneUpdate(Request $request,$id)
    {
        $area_zone = AreaZone::find($id);
        $area_zone->area_id = $request->area_id;
        $area_zone->name = $request->name;
        $area_zone->position = $request->position;
        $area_zone->save();
        return back();
    }

    public function areaStore(Request $request)
    {
        $area = new Areas;
        $area->name = $request->area_name;
        $area->save();
        return back();
    } 

    public function areaZoneStore(Request $request)
    {
        $area_zone = new AreaZone;
        $area_zone->area_id = $request->area_id;
        $area_zone->name = $request->name;
        $area_zone->position = $request->position;
        $area_zone->save();
        return back();
    }
    
    public function area_update(Request $request)
    {
        $area = Areas::find($request->area_id);
        $area->name = $request->area_name;
        $area->save();
        return back();
    }
}
