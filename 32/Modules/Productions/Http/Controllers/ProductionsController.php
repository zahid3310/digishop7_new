<?php

namespace Modules\Productions\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Productions;
use App\Models\ProductionEntries;
use App\Models\ProductionReturn;
use App\Models\ProductionReturnEntries;
use App\Models\ProductionDamage;
use App\Models\ProductionDamageEntries;
use App\Models\UnitConversions;
use Carbon\Carbon;
use Response;
use DB;
use View;

class ProductionsController extends Controller
{
    public function transferIndex()
    {
        $productions = Productions::where('productions.type', 0)
                            ->orderBy('productions.id', 'DESC')
                            ->with('productionEntries')
                            ->get();

        return view('productions::send_to_transfer.index', compact('productions'));
    }

    public function transferCreate()
    {
        $productions        = Productions::orderBy('productions.created_at', 'DESC')->first();
        $production_number  = $productions != null ? $productions['production_number'] + 1 : 1;

        return view('productions::send_to_transfer.create', compact('production_number'));
    }

    public function transferStore(Request $request)
    {
        $rules = array(
            'production_date'       => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $productions            = Productions::orderBy('productions.created_at', 'DESC')->first();
            $production_number      = $productions != null ? $productions['production_number'] + 1 : 1;

            $productions                        = new Productions;;
            $productions->date                  = date('Y-m-d', strtotime($data['production_date']));
            $productions->production_number     = $production_number;
            $productions->type                  = 0;
            $productions->total_amount          = $data['sub_total_amount'];
            $productions->note                  = $data['note'];
            $productions->created_by            = $user_id;

            if ($productions->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {   
                    $find_product_entries    = ProductEntries::find($value);

                    $production_entries[] = [
                        'production_id'      => $productions['id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'stock_quantity'     => $find_product_entries['stock_in_hand'],
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'amount'             => $data['rate'][$key]*$data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('production_entries')->insert($production_entries);

                //Here Item Id is used for update purpose
                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['quantity'][$key4];
                    $product_entries->save();
                }

                DB::commit();

                return back()->with("success","Transfer To Production Created Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function transferEdit($id)
    {
        $find_production            = Productions::select('productions.*')->find($id);

        $find_production_entries    = ProductionEntries::leftjoin('product_entries', 'product_entries.id', 'production_entries.product_entry_id')
                                            ->where('production_entries.production_id', $id)
                                            ->select('production_entries.*',
                                                    'product_entries.id as item_id',
                                                    'product_entries.product_code as product_code',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.name as item_name')
                                            ->get();

        $entries_count          = $find_production_entries->count();

        return view('productions::send_to_transfer.edit', compact('find_production', 'find_production_entries', 'entries_count'));
    }

    public function transferUpdate(Request $request, $id)
    {
        $rules = array(
            'production_date'       => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            $productions                        = Productions::find($id);
            $productions->date                  = date('Y-m-d', strtotime($data['production_date']));
            $productions->type                  = 0;
            $productions->total_amount          = $data['sub_total_amount'];
            $productions->note                  = $data['note'];
            $productions->updated_by            = $user_id;

            if ($productions->save())
            {  
                $item_id     = ProductionEntries::where('production_id', $productions['id'])->get();
                $item_delete = ProductionEntries::where('production_id', $productions['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {   
                    $find_product_entries    = ProductEntries::find($value);

                    $production_entries[] = [
                        'production_id'      => $productions['id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'rate'               => $data['rate'][$key],
                        'stock_quantity'     => $find_product_entries['stock_in_hand'],
                        'quantity'           => $data['quantity'][$key],
                        'amount'             => $data['rate'][$key]*$data['quantity'][$key],
                        'updated_by'         => $user_id,
                        'updated_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('production_entries')->insert($production_entries);

                //Here Item Id is used for update purpose
                if ($item_id != null)
                {
                    foreach ($item_id as $key => $value)
                    {
                        $old_item_entry_id[]    = $value['product_entry_id'];
                        $old_items_stock[]      = $value['quantity'];
                    }

                    foreach ($old_item_entry_id as $key2 => $value2)
                    { 
                        $quantity_add_to_product_entry                   = ProductEntries::find($value2);
                        $quantity_add_to_product_entry->stock_in_hand    = $quantity_add_to_product_entry['stock_in_hand'] + $old_items_stock[$key2];
                        $quantity_add_to_product_entry->save();
                    }
                }

                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['quantity'][$key4];
                    $product_entries->save();
                }

                DB::commit();

                return redirect()->route('productions_transfer_to_production_index')->with("success","Transfer Updated Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function finishedGoodsIndex()
    {
        $productions = Productions::where('productions.type', 1)
                            ->orderBy('productions.created_at', 'DESC')
                            ->get();

        return view('productions::received_finished_goods.index', compact('productions'));
    }

    public function finishedGoodsCreate()
    {   
        $production_list  = Productions::where('type', 0)->get();

        return view('productions::received_finished_goods.create', compact('production_list'));
    }

    public function finishedGoodsStore(Request $request)
    {
        $rules = array(
            'production_date'       => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $productions                        = new Productions;
            $productions->date                  = date('Y-m-d', strtotime($data['production_date']));
            $productions->production_number     = $data['production_id'];
            $productions->type                  = 1;
            $productions->total_amount          = $data['sub_total_amount'];
            $productions->note                  = $data['note'];
            $productions->created_by            = $user_id;

            if ($productions->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {   
                    $find_product_entries    = ProductEntries::find($value);

                    $production_entries[] = [
                        'production_id'      => $productions['id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'rate'               => $data['rate'][$key],
                        'stock_quantity'     => $find_product_entries['stock_in_hand'],
                        'quantity'           => $data['quantity'][$key],
                        'amount'             => $data['rate'][$key]*$data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('production_entries')->insert($production_entries);

                //Here Item Id is used for update purpose
                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] + $data['quantity'][$key4];
                    $product_entries->save();
                }

                DB::commit();

                return back()->with("success","Transfer To Production Created Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function finishedGoodsEdit($id)
    {
        $find_production            = Productions::select('productions.*')->find($id);

        $find_production_entries    = ProductionEntries::leftjoin('product_entries', 'product_entries.id', 'production_entries.product_entry_id')
                                            ->where('production_entries.production_id', $id)
                                            ->select('production_entries.*',
                                                    'product_entries.id as item_id',
                                                    'product_entries.product_code as product_code',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.name as item_name')
                                            ->get();

        $entries_count          = $find_production_entries->count();

        return view('productions::received_finished_goods.edit', compact('find_production', 'find_production_entries', 'entries_count'));
    }

    public function finishedGoodsUpdate(Request $request, $id)
    {
        $rules = array(
            'production_date'       => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            $productions                        = Productions::find($id);
            $productions->date                  = date('Y-m-d', strtotime($data['production_date']));
            $productions->type                  = 1;
            $productions->total_amount          = $data['sub_total_amount'];
            $productions->note                  = $data['note'];
            $productions->updated_by            = $user_id;

            if ($productions->save())
            {  
                $item_id     = ProductionEntries::where('production_id', $productions['id'])->get();
                $item_delete = ProductionEntries::where('production_id', $productions['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {   
                    $find_product_entries    = ProductEntries::find($value);

                    $production_entries[] = [
                        'production_id'      => $productions['id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'rate'               => $data['rate'][$key],
                        'stock_quantity'     => $find_product_entries['stock_in_hand'],
                        'quantity'           => $data['quantity'][$key],
                        'amount'             => $data['rate'][$key]*$data['quantity'][$key],
                        'updated_by'         => $user_id,
                        'updated_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('production_entries')->insert($production_entries);

                //Here Item Id is used for update purpose
                if ($item_id != null)
                {
                    foreach ($item_id as $key => $value)
                    {
                        $old_item_entry_id[]    = $value['product_entry_id'];
                        $old_items_stock[]      = $value['quantity'];
                    }

                    foreach ($old_item_entry_id as $key2 => $value2)
                    { 
                        $quantity_add_to_product_entry                   = ProductEntries::find($value2);
                        $quantity_add_to_product_entry->stock_in_hand    = $quantity_add_to_product_entry['stock_in_hand'] - $old_items_stock[$key2];
                        $quantity_add_to_product_entry->save();
                    }
                }

                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] + $data['quantity'][$key4];
                    $product_entries->save();
                }

                DB::commit();

                return redirect()->route('productions_received_finished_goods_index')->with("success","Transfer Updated Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function rawMaterialsList()
    {
        if(!isset($_GET['searchTerm']))
        {
            $fetchData  = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('categories.id', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->take(100)
                                    ->get();
        }
        else
        {
            $search     = $_GET['searchTerm'];   
            $fetchData  = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('product_entries.name', 'LIKE', "%$search%")
                                    ->where('product_entries.product_id', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->take(100)
                                    ->get();
        }

        foreach ($fetchData as $key => $value)
        {
            if ($value['product_type'] == 2)
            {
                $variations  = ' - ' . $value['variations'];
            }
            else
            {
                $variations  = '';
            }

            if ($value['brand_name'] != null)
            {
                $brand  = ' - '.$value['brand_name'];
            }
            else
            {
                $brand  = '';
            }

            if ($value['product_code'] != null)
            {
                $code  = $value['product_code'];
            }
            else
            {
                $code  = '';
            }

            $name   = $value['name'] . $variations . ' ' . '( ' . $code . $brand . ' )';

            $data[] = array("id"=>$value['id'], "text"=>$name);
        }

        return Response::json($data);
    }

    public function getProductionnembers()
    {
        if(!isset($_GET['searchTerm']))
        {
            $fetchData  = Productions::orderBy('created_at', 'DESC')
                                    ->get();
        }
        else
        {
            $search     = $_GET['searchTerm'];   
            $fetchData  = Productions::where('productions.production_number', 'LIKE', "%$search%")
                                    ->orderBy('created_at', 'DESC')
                                    ->get();
        }

        foreach ($fetchData as $key => $value)
        {
            $data[] = array("id"=>$value['id'], "text"=>$value['production_number'], "type"=>$value['type']);
        } 

        return Response::json($data);
    }

    //Production Return
    public function returnToWarehouseIndex()
    {
        return view('productions::production_return.index');
    }

    public function returnToWarehouseStore(Request $request)
    {
        $rules = array(
            'return_date'       => 'required',
            'production_number' => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $production_return                       = new ProductionReturn;
            $production_return->date                 = date('Y-m-d', strtotime($data['return_date']));
            $production_return->production_id        = $data['production_number'];
            $production_return->note                 = $data['return_note'];
            $production_return->total_amount         = $data['total_return_amount'];
            $production_return->created_by           = $user_id;

            if ($production_return->save())
            {   
                foreach ($data['product_entries'] as $key => $value)
                {   
                    if ($data['return_quantity'][$key] != 0)
                    {   
                        $update_return_quantity                     = ProductionEntries::where('product_entry_id', $value)->where('production_id', $production_return['production_id'])->first();
                        $update_return_quantity->return_quantity    = $update_return_quantity['return_quantity'] + $data['return_quantity'][$key];
                        $update_return_quantity->save();

                        $production_return_entries[] = [
                            'production_return_id' => $production_return['id'],
                            'product_entry_id'     => $value,
                            'main_unit_id'         => $data['main_unit_id'][$key],
                            'conversion_unit_id'   => $data['unit_id'][$key],
                            'rate'                 => $data['amount'][$key]/$data['quantity'][$key],
                            'quantity'             => $data['return_quantity'][$key],
                            'amount'               => ($data['amount'][$key]/$data['quantity'][$key])*$data['return_quantity'][$key],
                            'created_by'           => $user_id,
                            'created_at'           => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('production_return_entries')->insert($production_return_entries);

                stockInReturn($data, $item_id=null);

                DB::commit();
                return back()->with("success","Back Raw Materials To Warehouse Successfull !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function productionList()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Productions::where('productions.type', 0)
                                    ->orderBy('productions.id', 'DESC')
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Productions::where('productions.type', 0)
                                    ->where('productions.production_number', 'LIKE', "$search%")
                                    ->orderBy('productions.id', 'DESC')
                                    ->get();
        }

        foreach ($fetchData as $key => $value)
        {   
            $data[] = array("id"=>$value['id'], "text"=>$value['production_number']);
        }

        return Response::json($data);
    }

    public function findProductionNumber($id)
    {
        $data  = Productions::find($id);

        return Response::json($data);
    }

    public function returnList()
    {
        $data  = ProductionReturn::leftjoin('productions', 'productions.id', 'production_return.production_id')
                                ->orderBy('production_return.created_at', 'DESC')
                                ->select('production_return.*',
                                         'productions.production_number as production_number')
                                ->get();

        return Response::json($data);
    }

    public function returnSearchList($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data   = ProductionReturn::leftjoin('productions', 'productions.id', 'production_return.production_id')
                                ->orWhere('production_return.date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                    return $query->orWhere('production_return.production_return_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('production_return.created_at', 'DESC')
                                ->select('production_return.*',
                                         'productions.production_number as production_number')
                                ->get();
        }
        else
        {
            $data   = ProductionReturn::leftjoin('productions', 'productions.id', 'production_return.production_id')
                                ->orderBy('production_return.created_at', 'DESC')
                                ->select('production_return.*',
                                         'productions.production_number as production_number')
                                ->get();
        }

        return Response::json($data);
    }

    public function productionEntriesListByProductionNumber($id)
    {
        $production_entry           = ProductionEntries::leftjoin('productions', 'productions.id', 'production_entries.production_id')
                                                    ->leftjoin('product_entries', 'product_entries.id', 'production_entries.product_entry_id')
                                                    ->leftjoin('units', 'units.id', 'production_entries.conversion_unit_id')
                                                    ->where('production_entries.production_id', $id)
                                                    ->orderBy('production_entries.created_at', 'DESC')
                                                    ->select('production_entries.*',
                                                             'product_entries.name as entry_name',
                                                             'product_entries.product_code as product_code',
                                                             'units.name as conversion_unit_name')
                                                    ->get();

        $production_return_entry    = ProductionReturnEntries::leftjoin('production_return', 'production_return.id', 'production_return_entries.production_return_id')
                                            ->where('production_return.production_id', $id)
                                            ->get();

        $production                 = Productions::find($id);

        if ($production_entry->count() > 0)
        {
            foreach ($production_entry as $key => $value)
            {
                $production_return_entry_sum                        = $production_return_entry->where('product_entry_id', $value['product_entry_id']) ->sum('quantity');

                $entry_data[$value['id']]['id']                     =  $value['id'];
                $entry_data[$value['id']]['production_id']          =  $value['production_id'];
                $entry_data[$value['id']]['product_code']           =  $value['product_code'];
                $entry_data[$value['id']]['entry_name']             =  $value['entry_name'];
                $entry_data[$value['id']]['product_entry_id']       =  $value['product_entry_id'];
                $entry_data[$value['id']]['main_unit_id']           =  $value['main_unit_id'];
                $entry_data[$value['id']]['conversion_unit_id']     =  $value['conversion_unit_id'];
                $entry_data[$value['id']]['conversion_unit_name']   =  $value['conversion_unit_name'];
                $entry_data[$value['id']]['rate']                   =  $value['rate'];
                $entry_data[$value['id']]['original_quantity']      =  $value['quantity'];
                $entry_data[$value['id']]['quantity']               =  $value['quantity'] - $production_return_entry_sum;
                $entry_data[$value['id']]['total_amount']           =  $value['amount'];
            }
        }
        else
        {
            $entry_data = [];
        }

        $data['production']             = $production;
        $data['production_entries']     = $entry_data;
   
        return Response::json($data);
    }

    //Damage finished goods
    public function damagedFinishedGoodsIndex()
    {
        return view('productions::damage_finished_goods.index');
    }

    public function damagedFinishedGoodsStore(Request $request)
    {
        $rules = array(
            'damage_date'       => 'required',
            'production_number' => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $production_damage                       = new ProductionDamage;
            $production_damage->date                 = date('Y-m-d', strtotime($data['damage_date']));
            $production_damage->production_id        = $data['production_number'];
            $production_damage->note                 = $data['return_note'];
            $production_damage->total_amount         = $data['total_return_amount'];
            $production_damage->created_by           = $user_id;

            if ($production_damage->save())
            {   
                foreach ($data['product_entries'] as $key => $value)
                {   
                    if ($data['return_quantity'][$key] != 0)
                    {   
                        $update_damage_quantity                     = ProductionEntries::where('product_entry_id', $value)->where('production_id', $production_damage['production_id'])->first();
                        $update_damage_quantity->damage_quantity    = $update_damage_quantity['damage_quantity'] + $data['return_quantity'][$key];
                        $update_damage_quantity->save();

                        $production_return_entries[] = [
                            'production_return_id' => $production_damage['id'],
                            'product_entry_id'     => $value,
                            'main_unit_id'         => $data['main_unit_id'][$key],
                            'conversion_unit_id'   => $data['unit_id'][$key],
                            'rate'                 => $data['amount'][$key]/$data['quantity'][$key],
                            'quantity'             => $data['return_quantity'][$key],
                            'amount'               => ($data['amount'][$key]/$data['quantity'][$key])*$data['return_quantity'][$key],
                            'created_by'           => $user_id,
                            'created_at'           => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('production_damage_entries')->insert($production_return_entries);

                //Here Item Id is used for update purpose
                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['quantity'][$key4];
                    $product_entries->save();
                }

                DB::commit();
                return back()->with("success","Damage Added Successfull !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function damageList()
    {
        $data  = ProductionDamage::leftjoin('productions', 'productions.id', 'production_damage.production_id')
                                ->orderBy('production_damage.created_at', 'DESC')
                                ->select('production_damage.*',
                                         'productions.production_number as production_number')
                                ->get();

        return Response::json($data);
    }

    public function damageSearchList($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data   = ProductionDamage::leftjoin('productions', 'productions.id', 'production_damage.production_id')
                                ->orWhere('production_damage.date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                    return $query->orWhere('production_damage.production_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('production_damage.created_at', 'DESC')
                                ->select('production_damage.*',
                                         'productions.production_number as production_number')
                                ->get();
        }
        else
        {
            $data   = ProductionDamage::leftjoin('productions', 'productions.id', 'production_damage.production_id')
                                ->orderBy('production_damage.created_at', 'DESC')
                                ->select('production_damage.*',
                                         'productions.production_number as production_number')
                                ->get();
        }

        return Response::json($data);
    }
}
