<?php

namespace Modules\Payments\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\Bills;
use App\Models\InvoiceEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\PaidThroughAccounts;
use App\Models\AccountTransactions;
use App\Models\Transactions;
use App\Models\JournalEntries;
use Response;
use DB;

class PaymentsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('payments::index');
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $branch_id      = Auth::user()->branch_id;
        $customers      = Customers::where('contact_type', 0)
                                    ->orWhere('contact_type', 1)
                                    ->when($branch_id != 1, function ($query) use ($branch_id) {
                                        return $query->where('customers.branch_id', $branch_id);
                                    })
                                    ->orderBy('created_at', 'DESC')
                                    ->get();

        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $find_customer  = Customers::where('id', $customer_id)->first();
        $paid_accounts  = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();

        return view('payments::create', compact('customers', 'customer_id', 'find_customer', 'paid_accounts'));
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_id'   => 'required',
            'type'          => 'required',
            'payment_date'  => 'required',
            'amount'        => 'required',
            'paid.*'        => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        if ($data['amount'] == 0 || $data['amount'] == null || $data['payment_date'] == null)
        {
            return redirect()->back()->withInput();
        }

        DB::beginTransaction();

        try{
            $data_find                  = Payments::orderBy('id', 'DESC')->first();
            $payment_number             = $data_find != null ? $data_find['payment_number'] + 1 : 1;
            
            $payment                    = new Payments;
            $payment->payment_number    = $payment_number;
            $payment->customer_id       = $data['customer_id'];
            $payment->payment_date      = date('Y-m-d', strtotime($data['payment_date']));
            $payment->amount            = $data['amount'];
            $payment->paid_through      = $data['paid_through'];
            $payment->note              = $data['note'];
            $payment->type              = $data['type'];
            $payment->branch_id         = $branch_id;
            $payment->created_by        = $user_id;

            $find_customer              = $data['customer_id'];
            $find_type                  = $data['type'];

            if ($payment->save())
            {   
                if ($data['type'] == 0)
                {
                    //Financial Accounting Start
                        $journal_entry_debit                      = new JournalEntries;
                        $journal_entry_debit->date                = date('Y-m-d', strtotime($data['payment_date']));
                        $journal_entry_debit->account_id          = $data['paid_through'];
                        $journal_entry_debit->debit_credit        = 0; //Credit
                        $journal_entry_debit->customer_id         = $data['customer_id'];
                        $journal_entry_debit->amount              = $data['amount'];
                        $journal_entry_debit->transaction_head    = 'payment-receive';
                        $journal_entry_debit->payment_id          = $payment->id;
                        $journal_entry_debit->note                = $data['note'];
                        $journal_entry_debit->branch_id           = $branch_id;
                        $journal_entry_debit->created_by          = $user_id;
                        $journal_entry_debit->save();
                    //Financial Accounting End

                    //Customer Balance Update Start
                        $cus_update1             = Customers::find($data['customer_id']);
                        $cus_update1->balance    = $cus_update1['balance'] - $data['amount'];
                        $cus_update1->save();
                    //Customer Balance Update End
                }
                
                if ($data['type'] == 1)
                {
                    //Financial Accounting Start
                        $journal_entry_debit                      = new JournalEntries;
                        $journal_entry_debit->date                = date('Y-m-d', strtotime($data['payment_date']));
                        $journal_entry_debit->account_id          = $data['paid_through'];
                        $journal_entry_debit->debit_credit        = 1; //Credit
                        $journal_entry_debit->customer_id         = $data['customer_id'];
                        $journal_entry_debit->amount              = $data['amount'];
                        $journal_entry_debit->transaction_head    = 'payment-made';
                        $journal_entry_debit->payment_id          = $payment->id;
                        $journal_entry_debit->note                = $data['note'];
                        $journal_entry_debit->branch_id           = $branch_id;
                        $journal_entry_debit->created_by          = $user_id;
                        $journal_entry_debit->save();
                    //Financial Accounting End

                    //Customer Balance Update Start
                        $cus_update1             = Customers::find($data['customer_id']);
                        $cus_update1->balance    = $cus_update1['balance'] - $data['amount'];
                        $cus_update1->save();
                    //Customer Balance Update End
                }

                if ($data['type'] == 2)
                {
                    foreach ($data['sales_return_id'] as $key => $value)
                    {
                        if ($data['paid'][$key] != 0)
                        {
                            $payment_entries[] = [
                                'sales_return_id'   => $value,
                                'payment_id'        => $payment['id'],
                                'amount'            => $data['paid'][$key],
                                'branch_id'         => $branch_id,
                                'created_by'        => $user_id,
                                'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            $account_transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                'amount'                => $data['paid'][$key],
                                'paid_through_id'       => $payment['paid_through'],
                                'note'                  => $data['note'] . '(রিটার্ন বাবদ প্রদান)',
                                'type'                  => 1,  // 0 = In , 1 = Out
                                'transaction_head'      => 'sales-return',
                                'associated_id'         => $value,
                                'branch_id'             => $branch_id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $current_balance[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                'amount'                => $data['paid'][$key],
                                'paid_through_id'       => $payment['paid_through'],
                                'note'                  => $data['note'] . '(রিটার্ন বাবদ প্রদান)',
                                'type'                  => 1,  // 0 = In , 1 = Out
                                'transaction_head'      => 'sales-return',
                                'associated_id'         => $value,
                                'branch_id'             => $branch_id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $update_return_dues                     = SalesReturn::find($value);
                            $update_return_dues->due_amount         = $update_return_dues['due_amount'] - $data['paid'][$key];
                            $update_return_dues->save();

                            // $find_invoice_id                        = $tables['sales_return_entries']->where('sales_return_id', $value)->first(); 
                            // $update_invoice_dues                    = $tables['invoices']->find($find_invoice_id['invoice_id']);
                            // $update_invoice_dues->return_amount     = $update_invoice_dues['return_amount'] - $data['paid'][$key];
                            // $update_invoice_dues->save();
                        }
                    }

                    if ($data['amount'] > 0)
                    {
                        $transaction_data['date']           = date('Y-m-d', strtotime($data['payment_date']));
                        $transaction_data['type']           = 0;
                        $transaction_data['account_head']   = 'sales-return';
                        $transaction_data['transaction_id'] = $payment->id;
                        $transaction_data['customer_id']    = $data['customer_id'];
                        $transaction_data['note']           = 'পূর্বের বিক্রয় রিটার্ন প্রদান';
                        $transaction_data['amount']         = $data['amount'];
                        $transaction_data['paid_through']   = $data['paid_through'];
                        transactions($transaction_data);
                    }
                }

                if ($data['type'] == 3)
                {
                    foreach ($data['purchase_return_id'] as $key => $value)
                    {
                        if ($data['paid'][$key] != 0)
                        {
                            $payment_entries[] = [
                                'purchase_return_id'    => $value,
                                'payment_id'            => $payment['id'],
                                'amount'                => $data['paid'][$key],
                                'branch_id'             => $branch_id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $account_transactions[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                'amount'                => $data['paid'][$key],
                                'paid_through_id'       => $payment['paid_through'],
                                'note'                  => $data['note'] . '(রিটার্ন বাবদ গ্রহণ)',
                                'type'                  => 0,  // 0 = In , 1 = Out
                                'transaction_head'      => 'purchase-return',
                                'associated_id'         => $value,
                                'branch_id'             => $branch_id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $current_balance[] = [
                                'customer_id'           => $data['customer_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                                'amount'                => $data['paid'][$key],
                                'paid_through_id'       => $payment['paid_through'],
                                'note'                  => $data['note'] . '(রিটার্ন বাবদ গ্রহণ)',
                                'type'                  => 0,  // 0 = In , 1 = Out
                                'transaction_head'      => 'purchase-return',
                                'associated_id'         => $value,
                                'branch_id'             => $branch_id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $update_return_dues                 = PurchaseReturn::find($value);
                            $update_return_dues->due_amount     = $update_return_dues['due_amount'] - $data['paid'][$key];
                            $update_return_dues->save();

                            // $find_bill_id                       = $tables['purchase_return_entries']->where('purchase_return_id', $value)->first();   
                            // $update_bill_dues                   = $tables['bills']->find($find_bill_id['bill_id']);
                            // $update_bill_dues->return_amount    = $update_bill_dues['return_amount'] - $data['paid'][$key];
                            // $update_bill_dues->save();
                        }
                    }

                    if ($data['amount'] > 0)
                    {
                        $transaction_data['date']           = date('Y-m-d', strtotime($data['payment_date']));
                        $transaction_data['type']           = 1;
                        $transaction_data['account_head']   = 'purchase-return';
                        $transaction_data['transaction_id'] = $payment->id;
                        $transaction_data['customer_id']    = $data['customer_id'];
                        $transaction_data['note']           = 'পূর্বের ক্রয় রিটার্ন বাবদ আয়';
                        $transaction_data['amount']         = $data['amount'];
                        $transaction_data['paid_through']   = $data['paid_through'];
                        transactions($transaction_data);
                    }
                }

                if (isset($payment_entries))
                {
                    DB::table('payment_entries')->insert($payment_entries);
                }

                if (isset($account_transactions))
                {
                    DB::table('account_transactions')->insert($account_transactions);
                }

                if (isset($current_balance))
                {
                    DB::table('current_balance')->insert($current_balance);
                }

                DB::commit();
                return back()->with("success","Payment Created Successfully !!")->with("find_customer", $find_customer)->with('find_type', $find_type)->with('payment_date', $data['payment_date']);
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.")->with('find_customer', $find_customer)->with('find_type', $find_type);
            }
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return back()->with("unsuccess","Not Added")->with('find_customer', $find_customer)->with('find_type', $find_type);
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $payment        = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                ->select('payments.*',
                                         'customers.name as customer_name',
                                         'customers.address as address',
                                         'customers.phone as phone')
                                ->find($id);

        if ($payment['type'] == 0)
        {
            $entries    = PaymentEntries::leftjoin('invoices', 'invoices.id', 'payment_entries.invoice_id')
                                ->where('payment_entries.payment_id', $id)
                                ->select('payment_entries.*', 'invoices.invoice_number as number')
                                ->get();
        }
        else
        {
            $entries    = PaymentEntries::leftjoin('bills', 'bills.id', 'payment_entries.bill_id')
                                ->where('payment_entries.payment_id', $id)
                                ->select('payment_entries.*', 'bills.bill_number as number')
                                ->get();
        }

        $user_info  = userDetails();

        return view('payments::show', compact('entries', 'payment', 'user_info'));
    }

    public function edit($id)
    {
        dd($entries);
        return view('payments::edit');
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        DB::beginTransaction();

        try{
            $payment                = Payments::find($id);
            $payment_delete         = Payments::where('id', $id)->delete();
            $delete_transactions    = JournalEntries::where('payment_id', $id)->whereIn('transaction_head', ['payment-receive'])->delete();

            $debit      = JournalEntries::where('customer_id', $payment['customer_id'])
                                        // ->where('transaction_head', 'receivable')
                                        ->where('debit_credit', 1)
                                        ->sum('amount');

            $credit     = JournalEntries::where('customer_id', $payment['customer_id'])
                                        // ->whereIn('transaction_head', ['receivable', 'sales', 'payment-receive'])
                                        ->where('debit_credit', 0)
                                        ->sum('amount');

            $balance    = $debit - $credit;

            // //Customer Balance Update Start
                $cus_update             = Customers::find($payment['customer_id']);
                $cus_update->balance    = $balance;
                $cus_update->save();
            // //Customer Balance Update End

            DB::commit();
            return back()->with("success","Sales Return Deleted Successfully !!");

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Deleted");
        }
          
    }
    
    public function paymentListCustomer()
    {

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Customers::orderBy('customers.created_at', 'DESC')
                                    ->select('customers.*')
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Customers::where('customers.name', 'LIKE', "%$search%")
                                    ->orWhere('customers.phone', 'LIKE', "%$search%")
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->select('customers.*')
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "phone" =>'', "contact_type" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name'], "phone" =>$value['phone'], "contact_type" =>$value['contact_type']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function paymentListAjax()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Payments::orderBy('payments.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Payments::where('payments.payment_number', 'LIKE', "%$search%")
                                    ->orderBy('payments.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            $paymentNumber  = 'PM - '.str_pad($value['payment_number'], 6, "0", STR_PAD_LEFT);

            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[]         = array("id"=>$value['id'], "text"=>$paymentNumber);

            $i++;
        }
   
        return Response::json($data);
    }

    public function paymentSearchList($from_date, $to_date, $customer, $paymentNumber)
    {
        $branch_id                      = Auth()->user()->branch_id;
        $search_by_from_date            = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date              = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
        $search_by_customer             = $customer != 0 ? $customer : 0;
        $search_by_payment_number       = $paymentNumber != 0 ? $paymentNumber : 0;


        if ($branch_id == 1)
        {

             $data            = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->when($search_by_from_date != 0 && $search_by_to_date != 0, function ($query) use ($search_by_from_date, $search_by_to_date) {
                                            return $query->whereBetween('payments.payment_date', [$search_by_from_date, $search_by_to_date]);
                                        })
                                        ->when($search_by_customer != 0, function ($query) use ($search_by_customer) {
                                            return $query->where('customers.id', $search_by_customer);
                                        })
                                        ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                            return $query->where('payments.id', $search_by_payment_number);
                                        })
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*',
                                                 'paid_through_accounts.name as paid_through_accounts_name',
                                                 'customers.name as customer_name',
                                                 'customers.phone as phone')
                                        ->distinct('payments.id')
                                        ->take(100)
                                        ->get();

        }else{
             $data            = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->when($search_by_from_date != 0 && $search_by_to_date != 0, function ($query) use ($search_by_from_date, $search_by_to_date) {
                                            return $query->whereBetween('payments.payment_date', [$search_by_from_date, $search_by_to_date]);
                                        })
                                        ->when($search_by_customer != 0, function ($query) use ($search_by_customer) {
                                            return $query->where('customers.id', $search_by_customer);
                                        })
                                        ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                            return $query->where('payments.id', $search_by_payment_number);
                                        })
            
                                        ->where('payments.branch_id', $branch_id)
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*',
                                                 'paid_through_accounts.name as paid_through_accounts_name',
                                                 'customers.name as customer_name',
                                                 'customers.phone as phone')
                                        ->distinct('payments.id')
                                        ->take(100)
                                        ->get();
        }

        return Response::json($data);
    }

    public function contactList($id)
    {
        $customers      = Customers::find($id);
        $receivable     = JournalEntries::where('customer_id', $id)
                                    ->whereIn('transaction_head', ['receivable', 'opening-balance'])
                                    ->where('debit_credit', 1)
                                    ->sum('amount');
                                
        $received       = JournalEntries::where('customer_id', $id)
                                    ->whereIn('transaction_head', ['sales', 'payment-receive'])
                                    ->where('debit_credit', 0)
                                    ->sum('amount');

        $data['name']           = $customers['name'];
        $data['phone']          = $customers['phone'];
        $data['address']        = $customers['address'];
        $data['receivable']     = $receivable;
        $data['received']       = $received;
        $data['dues']           = $customers['balance'];

        return Response::json($data);
    }

    public function contactListBill($id)
    {
        $customers      = Customers::find($id);

        $payable        = JournalEntries::where('customer_id', $id)
                                    ->whereIn('transaction_head', ['payable', 'opening-balance'])
                                    ->where('debit_credit', 0)
                                    ->sum('amount');

        $paid           = JournalEntries::where('customer_id', $id)
                                    ->whereIn('transaction_head', ['purchase', 'payment-made'])
                                    ->where('debit_credit', 1)
                                    ->sum('amount');

        $data['name']           = $customers['name'];
        $data['phone']          = $customers['phone'];
        $data['address']        = $customers['address'];
        $data['payable']        = $payable;
        $data['paid']           = $paid;
        $data['dues']           = $customers['balance'];

        return Response::json($data);
    }

    public function contactListSalesReturn($id)
    {
        $customers      = Customers::find($id);

        $payable        = SalesReturn::where('customer_id', $id)
                                ->selectRaw('SUM(return_amount) as total_payable')
                                ->first();

        $paid           = Payments::where('customer_id', $id)
                                ->where('payments.type', 2)
                                ->selectRaw('SUM(amount) as total_paid')
                                ->first();

        $sale_returns   = SalesReturn::where('customer_id', $id)
                                ->where('due_amount', '>', 0)
                                ->selectRaw('sales_return.*')
                                ->get();

        $data['name']           = $customers['name'];
        $data['phone']          = $customers['phone'];
        $data['address']        = $customers['address'];
        $data['payable']        = $payable['total_payable'];
        $data['paid']           = $paid['total_paid'];
        $data['sale_returns']   = $sale_returns;

        return Response::json($data);
    }

    public function contactListPurchaseReturn($id)
    {
        $customers          = Customerrs::find($id);

        $payable            = PurchaseReturn::where('customer_id', $id)
                                ->selectRaw('SUM(return_amount) as total_payable')
                                ->first();

        $paid               = Payments::where('customer_id', $id)
                                ->where('payments.type', 2)
                                ->selectRaw('SUM(amount) as total_paid')
                                ->first();

        $purchase_returns   = PurchaseReturn::where('customer_id', $id)
                                ->where('due_amount', '>', 0)
                                ->selectRaw('purchase_return.*')
                                ->get();

        $data['name']               = $customers['name'];
        $data['phone']              = $customers['phone'];
        $data['address']            = $customers['address'];
        $data['payable']            = $payable['total_payable'];
        $data['paid']               = $paid['total_paid'];
        $data['purchase_returns']   = $purchase_returns;

        return Response::json($data);
    }

    public function paymentList($id)
    {
        $branch_id  = Auth::user()->branch_id;

         if ($id == 0)
        {
            if ($branch_id == 1)
            {
                $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*',
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                        ->orderBy('payments.id', 'DESC')
                                        ->get();
            }
            else
            {
                $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->where('payments.branch_id', $branch_id)
                                        ->where('payments.customer_id', $id)
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*',
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                        
                                        ->get();
            }
        }
        else
        {
            if ($branch_id == 1)
            {
                $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->where('payments.customer_id', $id)
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                               
                                        ->get();
            }
            else
            {
                $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->where('payments.customer_id', $id)
                                        ->where('payments.branch_id', $branch_id)
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                      
                                        ->get();
            }
        }

        return Response::json($data);
    }

    public function paymentListSearch($id)
    {
        $tables         = TableNameByUsers();
        $table_id       = Auth::user()->associative_contact_id;

        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            if ($branch_id == 1)
            {
                $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->where('customers.name', 'LIKE', "%$id%")
                                        ->orWhere('payments.payment_date', 'LIKE', "%$search_by_date%")
                                        ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number, $table_id) {
                                            return $query->orWhere('payments.payment_number', 'LIKE', "%$search_by_payment_number%");
                                        })
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                        ->take(20)
                                        ->get();
            }
            else
            {
                $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->where('customers.name', 'LIKE', "%$id%")
                                        ->orWhere('payments.payment_date', 'LIKE', "%$search_by_date%")
                                        ->where('payments.branch_id', $branch_id)
                                        ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number, $table_id) {
                                            return $query->orWhere('payments.payment_number', 'LIKE', "%$search_by_payment_number%");
                                        })
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                        ->take(20)
                                        ->get();
            }
        }
        else
        {
            if ($branch_id == 1)
            {

                $data   = Payments::join('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                        ->take(20)
                                        ->get();
            }
            else
            {
                $data   = Payments::join('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('paid_through_accounts', 'paid_through_accounts.id', 'payments.paid_through')
                                        ->where('payments.branch_id', $branch_id)
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'paid_through_accounts.name as paid_through_accounts_name', 
                                                  'customers.name as customer_name')
                                        ->take(20)
                                        ->get();
            }
        }

        return Response::json($data);
    }
}
