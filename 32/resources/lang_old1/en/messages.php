<?php

return [
	'menu' => 'Menu',
    'dashboard' => 'Dashboard',
   /////////////////////////////////////////////////////////////


    'sales' => 'Sales',
    'daily_sales' => 'Daily Sales',
    'daily_sales' => 'Daily Sales',
    'list_of_sales' => 'List of Sales',
    'sales_return' => 'Sales Return',
    'invoice_wise_collection' => 'Invoice Wise Collection',
    /////////////////////////////////////////////////////////////


    'list_of_receive/purchase' => 'List of Receive/Purchase',
    'receive/purchase_return' =>  'Receive/Purchase Return',
    'purchase_return' =>  'Purchase Return',


    // ////////////////////////////////////////

    'Send_Production'=> 'Send To Production',
    'new_send' => 'New Send',
    'list_of_send' => 'List of Send',
    'rece_finished_goods' => 'Rece Finished Goods',
    'new_receive' =>'New Receive',
    'new_transfer' =>'New Transfer',



    'purchase' => 'Purchase',
    'purchase_items' => 'Purchase Item',
    'list_of_purchase' => 'List of Purchase',
    'purchase_return' => 'Purchase Return',
    'bill_wise_payment' => 'Bill Wise Payment',
    /////////////////////////////////////////////////////////////
   

    'accounts' => 'Accounts',
    'expenses' => 'Expenses',
    'incomes' => 'Incomes',
    'balance_transfer' => 'Balance Transfer',
    'cash_book' => 'Cash Book',
    'ledger_book' => 'Ledger Book',
    /////////////////////////////////////////////////////////////
   

    'messaging' => 'Messaging',
    'send_meg' => 'Send Message',
    /////////////////////////////////////////////////////////////
   

    'reports' => 'Reports',
    'sales_statement' => 'Statement of Sales',
    'sales_summary' => 'Sales Summary',
    'product_wise_sales' => 'Product Wise Sales',


    'purchase_statement' => 'Statement of Pur.',
    'purchase_summary' => 'Purchase Summary',
    'receive_or_purchase_report' => 'Receive/Purchase',
    'receive/purchase_summary' =>'Receive/Purchase Summary',


    'list_of_expense' => 'List of Expense',
    'list_of_income' => 'List of Income',
    'income_statement' => 'Income Statement',


    'payment' => 'Payment',
    's_payment' => 'Supplier Payment',
    'c_payment' => 'Customer Payment',
    'make_payment'=>'Make Payment',

    'mis' => 'MIS',
    'current_balance' => 'Current Balance',
    'stock_status' => 'Stock Status',
    'buyer_Ledger' => 'Buyer Ledger',
    'supplier_Ledger' => 'Supplier Ledger',
    'due_ledger' => 'Due Ledger',
    'SR/Customer_Ledger' => 'SR/Customer Ledger',
    'daily_report' => 'Daily Report',

    
    'basic_report' => 'Basic Report',
    'list_items' => 'List of Items',
    'emergency_purchase' => 'Emergency Purchase',
    'item_wise_supplier' => 'Item Wise Supplier',
    'item_wise_customer' => 'Item Wise Customer',
    'buyer_list' => 'Buyer List',
    'supplier_list' => 'Supplier List',
    'item_wise_SR/Customer'=> 'Item Wise SR/Customer',
    'SR/Customer_list' => 'SR/Customer List',

    /////////////////////////////////////////////////////////////

    'variations' => 'Variations',
    'brand_name' => 'Brand Name',
    //////////////////////////////////////////////

    'ac_info' => 'AC Info',
    'update' => 'Update',
    'update_print' => 'Update & Print',

    ///////////////////////////////////////////////

    'basic_settings' => 'Basic Settings',

    'product' => 'Product',
    'create_product'=> 'Create Product',
    'major_category' => 'Mejor Category',
    'product_code'=> 'Product Code',
    'product_name'=> 'Product Name',
    'retail_price'=> 'Retail Price',
    'wholesale_price'=> 'Wholesale Price',
    'product_type'=> 'Product Type',
    'stock_quantity'=> 'Stock Quantity',
    'alert_quantity'=> 'Alert Quantity',
    'single'=> 'Single',
    'active' => 'Active',
    'inactive'=> 'Inactive',
    'select_major_category'=> 'Select Major Category',
    'select_category' => 'Select Category',
    'select_sub_category' => 'Select Sub Category',
    'select_unit'=> 'Select Unit',
    'list_product' => 'List Of Products',
    'add_product' => 'Add Product',
    'add_customer' => 'Add Customer',
    'add_categories' => 'Add Categories',
    'product_category' => 'Product Category',
    'product_category_name' => 'Product Category Name',
    'other' => 'Others',
    'all_major_category'=> 'All Major Category',
    'all_unit'=> 'All Unit',
    'variation' => 'Variation',
    'variation_list' => 'Variation List',
    'add_variation_value' => 'Add Variation Value',
    'value' => 'Values',
    'all_store'=>'All Stores',
    'select_product' => 'Select Product',
    'quantity' => 'Quantity',
    'a4_size_printer' => 'A4 Size Printer',
    'label_printer' => 'Label Printer',
    'barcode_label_size' => 'Barcode Label Size',
    'print_type'=>'Print Type',
    'organization_name'=>'Organization Name',
    '40labels_per_sheet'=>'40 Labels Per Sheet',
    '36labels_per_sheet'=>'36 Labels Per Sheet',
    '22labels_per_sheet'=>'22 Labels Per Sheet',
    'page_margin' => 'Page Margin',
    'information_show_labels'=>'Information to show in Labels',
    'product_price'=>'Product Price',
    'print_preview'=>'Print Preview',
    'opening_stock'=>'Opening Stock',
    


    'brand' => 'Brand/Manufacturer',
    'add_unit_measure' => 'Add Unit Measure',
    'product_variations' => 'Product Variations',
    'print_barcode' => 'Print Barcode',
    'bulk_opening_stock' => 'Bulk Opening Stock',
    'bulk_product_update' => 'Bulk Product Update',
    'add_mejor_categories' => 'Add Mejor Categories',
    'add_sr/customer' => 'Add SR/Customer',

    'registers' => 'Registers',
    'add_BuyerCustomer' => 'Add Buyer/Customer',
    'add_supplier' => 'Add Supplier',
    'add_employee' => 'Add Employee',
    'add_reference' => 'Add Reference',
    'add_area'     => 'Add Area',

    'paid_through' => 'Paid Through',

    'create_message' => 'Create Text Message',
    'pb' => 'Phone Book',


    'security_system' => 'Security System',
    'add_user' => 'Add User',
    'list_user' => 'List of user',
    'permission' => 'Set Permission',
    'add_branch' => 'Add Branch',
    'branch' => 'Branch',
    'branch_name' => 'Branch Name',
    'all_branch' => 'All Branches',
    'manage_branch' => 'Manage Branch',
    /////////////////////////////////////////////////////////////
   

   /**************************Sideber End******************************/

   	// Navber
    'stock_out' => "Stock Out",

    /**************************Navber End******************************/

    // Footer
    'ctl' => 'Cyberdyne Technology Ltd.',
    'contact' => 'Contact',
    'contact_no' => '01715317133',

    /**************************Footer End******************************/


   'today_sale' => "Today's Sales",
   'today_sale_return' => "Today's Sales Return",
   'today_expense' => "Today's Expenses",
   'sms_balance' => "SMS Balance",
   'c_dues' => "Customer Dues",
   's_dues' => "Supplier  Dues",
   't_sales' => "Total Sales",
   't_purchase' => "Total Purchase",
   't_customer' => "Total Customers",
   't_suppliers' => "Total Suppliers",
   't_invoices' => "Total Invoices",
   't_bills' => "Total Bills",
   'top_selling_products' => "Top Selling Products",
   'stock_out_products' => "Stock Out Products",
   'due_invoices' => "Due Invoices",
   'year' => "Year",
   'month' => "Month",
   'week' => "Week",
   'sl' => "SL",
   'code' => "Code",
   'name' => "Name",
   'date' => "Date",
   'invoice' => "Invoice",
   'amount' => "Amount",

   /**************************Home Page End******************************/


   'customer' => 'Customer',
   'walk_in_customer' => 'Walk-In Customer',
   'reference' => 'Reference',
   'select_reference' => 'Select Reference',
   'balance' => 'Balance',
   'phone' => 'Phone',
   'note' => 'Note',
   'barcode' => 'Barcode',
   'customer_name' => 'Customer Name',
   'scan_product_code' => 'Scan Product Code',
   'no_product_found' => 'No Product Found',
   'sub_total' => 'Sub Total',
   'vat' => 'VAT',
   'discount' => 'Discount',
   'dis_note' => 'Dis. Note',
   'coupon' => 'Coupon',
   'total_payable' => 'Total Payable',
   'cash_given' => 'Cash Given',
   'change' => 'Change',
   'change_amount' => 'Change Amount',
   'send_sms' => 'Send SMS',
   'masking' => 'Masking',
   'non_masking' => 'Non Masking',
   'voice' => 'Voice',
   'save' => 'Save',
   'save_print' => 'Save & Print',
   'close' => 'Close',
   'print_invoice' => 'Print Invoice',
   'action' => 'Action',
   'account_information' => 'Account Information',
   'ac_info' => 'AC Info',
   'amount_paid' => 'Amount Paid',
   'add_new_customer' => 'Add New Customer',
   'add_new_reference' => 'Add New Reference',
   'mobile_no' => 'Mobile Number',
   'alt_phone'=> 'Alternative Phone',
   'nid_number' => 'NID Number',
   'opening_balance' => 'Opening Balance',
   'address' => 'Address',
   'unit' => 'Unit',
   'qty' => 'Qty',
   'rate' => 'Rate',
   'total' => 'Total',
   'sr/customer' => 'SR/Customer',
   'sr/customer_payment' => 'SR/Customer Payments',
   'select_sr/customer' => 'Select SR/Customers',




   // All Sales
   'all_sales' => 'All Sales',
   'paid' => 'Paid',
   'paid_to' => 'Paid To',
   'due' => 'Due',
   'from_date' => 'From Date',
   'to_date' => 'To Date',
   'all' => 'All',

   // SALES RETURN
   'return_date' => 'Return Date',
   'select_customer' => 'Select Customer',
   'order_number' => 'Order Number',
   'return_amount' => 'Return Amount',
   'all_sales_return' => 'All Sales Return',
   'search' => 'Search',
   'return' => 'Return',

   // PAYMENTS
   'payment_type' => 'Payment Type',
   'payment_date' => 'Payment Date',
   'payment_number' => 'Payment Number',
   'payment_details' => 'Payment Details',
   'search_contact' => 'Search Contact',
   'total_receivables' => 'Total Receivables',
   'total_received' => 'Total Received',
   'order' => 'Order',
   'payment_list' => 'Payment list',
   'type' => 'Type',
   'total_paid' => 'Total Paid',
   'payable' => 'Payable',
   'total_due' => 'Total Due',


   // //////////////////////

   'transfer_to_production' => 'Transfer To Production',
   'list_of_transfer' => 'List of Transfers',
   'items' => 'Items',
   'edit_transfer' => 'Edit Transfer',

   // ///////////////////////////////////////


   'receive_finished_goods' => 'Receive Finished Goods',
   'production_number' => 'Production Number',
   'list_receive_finished_goods' => 'List Of Receive Finished Goods',


   // Purchase items
   'walk_in_supplier' => 'Walk-In Supplier',
   'supplier'=> 'Supplier',
   'add_new_supplier' => 'Add New Supplier',
   'category' => 'Category',
   'stock' => 'Stock',

   //Purchases list
   'all_purchases' => 'All Purchases',

   //Purchases Return
   'select_supplier' => 'Select Supplier',
   'all_purchases_return' => 'All Purchases Return',
   'purchase_number' => 'Purchase Number',
   'return_number' => 'Return Number',
   'bill_number' => 'Bill Number',
   'coupon/membership' => 'Coupon/Membership',

   // Account
   'add' => 'Add',
   'expense_date' => 'Expense Date',
   'expense_category' => 'Expense Category',
   'expense_list' => 'Expense List',
   'expense_number' => 'Expense Number',

   'income_date' => 'Income Date',
   'income_category' => 'Income Category',
   'income_list' => 'Income List',
   'income_number' => 'Income Number',
   'create_income_category' => 'Create New Income Category',
   'create_expense_category' => 'Create New Expense Category',
   'category_name' => 'Category Name',

    'transfer_balance'=> 'Transfer Balance',
    'all_transfer'=> 'All Transfer',
    'transfer_date' => 'Transfer Date',
    'transfer_from' => 'Transfer From',
    'transfer_to' => 'Transfer To',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'transfer_note'=> 'Transfer Note',
    'select_account' => 'Select Account',

    // Message 
    'sms_type' => 'SMS Type',
    'select_msg' => 'Select Message',
    'contact_category' => 'Contact Category',
    'select_contact_category' => 'Select Contact Category',
    'select_contact' => 'Select Contact',
    'employee' => 'Employee',
    'message' => 'Message',
    'msg_body' => 'Message Body',
    'send' => 'Send',
    'contact_type' => 'Contact Type',
    'image' => 'Image',
    'all_contact' => 'All Contact',
    'meg_title'=>'Message Title',
    'title'=>'Title',
    'all_meg'=> 'All Messages',
    'body'=> 'Body',


    'list_of_item_print' => 'আইটেম প্রিন্টের তালিকা',
     'stock_in_hand' => 'মজুদ মাল',
     'emergency_Item_list'=> 'জরুরী আইটেমের তালিকা',
     'u/m' => ' ইউ/এম',
     'report_type'=> 'রিপোর্ট টাইপ',
     'supplier_wise'=> 'সরবরাহকারী অনুসারে',
     'product_wise'=>'পণ্য অনুসারে',
     'customer_wise'=>'কাস্টমার অনুসারে',
     'wholesale_rate'=> 'পাইকারি দর',
     'retail_rate'=> 'রিটেল দর',
     'customer_rate' => 'কাস্টমার দর',
     'payment_account'=> 'পেমেন্ট একাউন্ট',
     'account_name' => 'হিসাবের নাম',
     'in' => 'ইন',
     'out'=> 'আউট ',
     'product_report' => 'পণ্য রিপোর্ট',
     'stocK_report' => 'স্টক রিপোর্ট',
     'sold' => 'বিক্রি',
     'stock_value'=>'স্টক মূল্য',
     'sell_value'=> 'বিক্রয় মূল্য',
     'show_all' => 'সমস্ত প্রদর্শন করুন',
     'due_available'=> 'বকেয়া প্রাপ্য',
     'details' => 'বিস্তারিত',
     'select_date_range' => 'তারিখের সীমা নির্বাচন করুন',
     'customer_ledger' => 'কাস্টমার লেজের',
     'list_customer_payment' => 'গ্রাহক প্রদানের তালিকা',
     'list_supplier_payment' => 'সরবরাহকারী প্রদানের তালিকা',
     'item_code'=> 'আইটেম কোড',
     'profit'=> 'লাভ',
     'loss'=> 'ক্ষতি',
     'supplier_name' => 'সরবরাহকারী নাম',
     'unit_price'=> 'ইউনিট প্রাইস',
     'expense_statement' => 'ব্যয় বিবৃতি',
     'expense_note' => 'ব্যয় বিঃদ্রঃ',
     'income_note'=>'আয় বিঃদ্রঃ',
     'T/date' => 'টি / তারিখ',
     'T/price' => 'টি/প্রাইস',
     'T/payable' => 'টি/পরিশোধযোগ্য',
     'T/discount' => 'টি/ডিসকাউন্ট',
     'N/payable' => 'এন/পরিশোধযোগ্য ',
     'number' => 'নম্বর',
     'head' => 'হেড',
     'particulars'=>'পার্টিকুলারস',
     'entry_by'=> 'দ্বারা প্রবেশ',
     'general_ledger' => 'জেনারেল লেজার',
     'account_head'=>'একাউন্ট হেড',
     'item_category'=>'আইটেম ক্যাটাগরি',
     'purchase_type'=>'ক্রয়ের ধরণ',
     'select_branch' => 'শাখা নির্বাচন করুন',
     'sales_type'=> 'বিক্রয় প্রকার',
     'add_new_product_category' => 'নতুন পণ্য বিভাগ যুক্ত করুন',
     'add_new_major_category' => 'নতুন প্রধান বিভাগ যুক্ত করুন',
     'already_added' => 'ইতিমধ্যে যোগ করা হয়েছে',
     'set_unit_conversion' => 'ইউনিট রূপান্তর সেট করুন',
     'paid_through_account'=>'অ্যাকাউন্টের মাধ্যমে অর্থ প্রদান করুন',
     'description'=> 'বর্ণনা',
     'all_accounts'=> 'সমস্ত অ্যাকাউন্ট',
     'edit_accounts'=> 'অ্যাকাউন্টগুলি সম্পাদনা করুন',
     'add_contact' => 'পরিচিতি যোগ করুন',
     'all_phone_contact'=> 'সমস্ত ফোন যোগাযোগ',
     'description_deposit'=> 'বিবরণ/জমা',
     'description_withdraw'=> 'বিবরণ/খরচ',
     'taka'=> 'টাকা',
     'previous_dues'=> 'পূর্বের জের',

     // All Product
     'purchase_price' => 'Purchase Price',
     'sell_price' => 'Sell Price',
     'status' => 'Status',

     // Production
     'production' => 'Production',
     'stock_transfer' => 'Stock Transfer',
     'sales_to_sr/customer' => 'Sales To SR/Customer',
     // Security System
     'user_access' => 'User Access',
     'setting' => 'Settings',
     'user' => 'User',
     'username' => 'User Name',
     'select_user' => 'Select User',
     'password'=> 'Password',
     'role'=>'Role',
     'user_list'=> 'User List',
     'all_user' => 'All User',
     'edit_user' => 'Edit User',


     // Reports

     'list_of_item_print' => 'List of Item Print',
     'stock_in_hand' => 'Stock in hand',
     'emergency_Item_list'=> 'Emergency Item List',
     'u/m' => 'U/M',
     'report_type'=> 'Report Type',
     'supplier_wise'=> 'Supplier Wise',
     'product_wise'=>'Product Wise',
     'customer_wise'=>'Customer Wise',
     'wholesale_rate'=> 'Wholesale Rate',
     'retail_rate'=> 'Retail Rate',
     'customer_rate' => 'Customer Rate',
     'payment_account'=> 'Payment Account',
     'account_name' => 'Account Name',
     'in' => 'In',
     'out'=> 'Out',
     'product_report' => 'Product Report',
     'stocK_report' => 'Stock Report',
     'sold' => 'Sold',
     'stock_value'=> 'Stock Value',
     'sell_value'=> 'Sell Value',
     'show_all' => 'Show All',
     'due_available'=> 'Dues Available',
     'details' => 'Details',
     'select_date_range' => 'Select Date Range',
     'customer_ledger' => 'Customer Ledger',
     'list_customer_payment' => 'List of Customer Payment',
     'list_supplier_payment' => 'List of Supplier Payment',
     'item_code'=> 'Item Code',
     'profit'=> 'Profit',
     'loss'=> 'Loss',
     'supplier_name' => 'Supplier Name',
     'unit_price'=> 'Unit Price',
     'expense_statement' => 'Expense Statement',
     'expense_note'=>'Expense Note',
     'income_note'=>'Income Note',
     'T/date' => 'T/Date',
     'T/price' => 'T/PRICE',
     'T/payable' => 'T/PAYABLE',
     'T/discount' => 'T/DISCOUNT',
     'N/payable' => 'N/PAYABLE ',
     'number' => 'Number',
     'head' => 'Head',
     'particulars'=>'Particulars',
     'entry_by'=> 'Entry By',
     'general_ledger' => 'General Ledger',
     'account_head'=>'Account Head',
     'item_category'=>'Item Category',
     'purchase_type'=>'Purchase Type',
     'select_branch' => 'Select Branch',
     'sales_type'=> 'Sales Type',
     'add_new_product_category' => 'Add New Product Category',
     'add_new_major_category' => 'Add New Major Category',
     'already_added' => 'Already Added',
     'set_unit_conversion' => 'set_unit_conversion',
     'paid_through_account'=>'PAID THROUGH ACCOUNTS',
     'description'=> 'Description',
     'all_accounts'=> 'All Accounts',
     'edit_accounts'=> 'Edit Accounts',
     'add_contact' => 'Add Contact',
     'all_phone_contact'=> 'All Phone Contact',
     'description_deposit'=> 'Description/Deposit',
     'description_withdraw'=> 'Description/Withdraw',
     'taka'=> 'Taka',
     'previous_dues'=> 'Previous Due',
     'show' => 'Show',
     'payable_to' => 'Payable To',
     'purchase_date' => 'Purchase Date',
     'bill' => 'Bill',
     'no' => 'No',
     'price' => 'Price',
     'store_name' => 'Store Name',
     'enter_variation_name' => 'Enter Variation Value',
     'previous_image' => 'Previous Image',
     'joining_date' => 'Joining Date',
     'designation' => 'Designation',
     'associative_user_id' => 'Associative User ID',
     'salary'=> 'Salary',
     'print' => 'Print',



];