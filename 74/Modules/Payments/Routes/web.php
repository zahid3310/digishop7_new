<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('payments')->group(function() {
    Route::get('/create', 'PaymentsController@create')->name('payments_create');
    Route::get('/edit/{id}', 'PaymentsController@edit')->name('payments_edit');
    Route::get('/contact/list/{id}', 'PaymentsController@contactList')->name('contact_list');
    Route::get('/contact/list/bill/{id}', 'PaymentsController@contactListBill')->name('contact_list_bill');
    Route::get('/contact/list/sales-return/{id}', 'PaymentsController@contactListSalesReturn')->name('contact_list_sales_return');
    Route::get('/contact/list/purchase-return/{id}', 'PaymentsController@contactListPurchaseReturn')->name('contact_list_purchase_return');
    Route::post('/store', 'PaymentsController@store')->name('payments_store');
    Route::post('/update/{id}', 'PaymentsController@update')->name('payments_update');
    Route::get('/delete/{id}', 'PaymentsController@destroy')->name('payments_delete');
    Route::get('/show/{id}', 'PaymentsController@show')->name('payments_print');
    Route::get('/payment/list/{id}', 'PaymentsController@paymentList')->name('payment_list');
    Route::get('/payment/list/search/{id}', 'PaymentsController@paymentListSearch')->name('payment_list_search');
    Route::get('/payment/customerList', 'PaymentsController@paymentListCustomer');
});

Route::prefix('payments/settlements')->group(function() {
    Route::get('/create', 'PaymentsController@settlement')->name('settlements_create');
    Route::post('/store', 'PaymentsController@settlementStore')->name('settlements_store');
    Route::get('/get-balance/{contact_id}', 'PaymentsController@getBalance')->name('get_settlement_balance');
    Route::get('/get-settlement-data/{contact_id}', 'PaymentsController@getSettlementData')->name('get_settlements_data');
});