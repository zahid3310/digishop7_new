<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('productions/transfer-to-production')->group(function() {
    Route::get('/', 'ProductionsController@transferIndex')->name('productions_transfer_to_production_index');
    Route::get('/create', 'ProductionsController@transferCreate')->name('productions_transfer_to_production_create');
    Route::post('/store', 'ProductionsController@transferStore')->name('productions_transfer_to_production_store');
    Route::get('/edit/{id}', 'ProductionsController@transferEdit')->name('productions_transfer_to_production_edit');
    Route::post('/update/{id}', 'ProductionsController@transferUpdate')->name('productions_transfer_to_production_update');
    Route::get('/get-raw-materials', 'ProductionsController@rawMaterialsList')->name('productions_raw_materials_list');
});

Route::prefix('productions/received-finished-goods')->group(function() {
    Route::get('/', 'ProductionsController@finishedGoodsIndex')->name('productions_received_finished_goods_index');
    Route::get('/create', 'ProductionsController@finishedGoodsCreate')->name('productions_received_finished_goods_create');
    Route::post('/store', 'ProductionsController@finishedGoodsStore')->name('productions_received_finished_goods_store');
    Route::get('/edit/{id}', 'ProductionsController@finishedGoodsEdit')->name('productions_received_finished_goods_edit');
    Route::post('/update/{id}', 'ProductionsController@finishedGoodsUpdate')->name('productions_received_finished_goods_update');
});
