@extends('layouts.app')

@section('title', 'Show')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Purchase</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Purchase</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div style="padding: 15px;padding-top: 30px" class="card-body">
                                <div class="row">
                                    @if($user_info['header_image'] == null)
                                        <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-4 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">{{ $user_info['organization_name'] }}</h2>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_number'] }}</p>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    @else
                                        <img class="float-left" src="{{ url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-4 col-6">
                                        <address>
                                            <strong>Payable To:</strong><br>
                                            {{ $bill['customer_name'] }}
                                            @if($bill['address'] != null)
                                               <br> <?php echo $bill['address']; ?> <br>
                                            @endif
                                            @if($bill['address'] == null)
                                                <br>
                                            @endif
                                            {{ $bill['phone'] }}
                                        </address>
                                    </div>

                                    <div class="col-sm-4 hidden-xs">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div style="font-size: 16px" class="col-sm-4 col-6 text-sm-right">
                                        <address>
                                            <strong>Purchase Date:</strong><br>
                                            {{ date('d-m-Y', strtotime($bill['bill_date'])) }}<br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6 hidden-xs">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Purchase summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Purchase # {{ 'BILL - ' . str_pad($bill['bill_number'], 6, "0", STR_PAD_LEFT) }}</h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-nowrap">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th style="width: 70px;">No.</th>
                                                <th>Item</th>
                                                <th class="text-right">Rate</th>
                                                <th class="text-right">Quantity</th>
                                                <th class="text-right">Discount</th>
                                                <th class="text-right">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">

                                            @if(!empty($entries) && ($entries->count() > 0))

                                            <?php $total_amount = 0; ?>

                                            @foreach($entries as $key => $value)

                                            <?php 
                                                $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                                $variation_name = ProductVariationName($value['product_entry_id']);

                                                if ($value['product_code'] != null)
                                                {
                                                    $productCode  = ' - '.$value['product_code'];
                                                }
                                                else
                                                {
                                                    $productCode  = '';
                                                }

                                                if ($value['brand_name'] != null)
                                                {
                                                    $brandName  = ' - '.$value['brand_name'];
                                                }
                                                else
                                                {
                                                    $brandName  = '';
                                                }

                                                if (($value['height'] != null) && ($value['width'] != null))
                                                {
                                                    $dimension  = ' - '.$value['height'] . ' X ' . $value['width'];
                                                }
                                                else
                                                {
                                                    $dimension  = '';
                                                }

                                                if ($value['unit_name'] != null)
                                                {
                                                    $unit  = ' '.$value['unit_name'];
                                                }
                                                else
                                                {
                                                    $unit  = ' SFT';
                                                }

                                                $pre_dues = previousDuesBill($bill['vendor_id']);
                                                $net_paya = round($total_amount, 2);
                                                $paid     = round($bill['bill_amount'] - $bill['due_amount'], 2);
                                                $dues     = round($pre_dues + $net_paya - $paid, 2);
                                            ?>

                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>
                                                    {{ $value['product_entry_name'] }} <br> {{ $value['product_name'] . $productCode . $brandName . $dimension }} {{ $variation_name != null ? ' - ' . $variation_name : '' }}
                                                </td>
                                                <td class="text-right">{{ number_format($value['rate'],2,'.',',') }}</td>
                                                <td class="text-right">{{ number_format($value['quantity'],2,'.',',') . ' ' .$value['unit_name'] }}</td>
                                                <td class="text-right">
                                                    <?php $total_dis = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; ?>
                                                    @if($value['discount_type'] == 0)
                                                        <?php echo number_format($total_dis,2,'.',','); ?><br>
                                                        <?php echo '('.$value['discount_amount'].'%'.')'; ?>
                                                    @else
                                                        <?php echo number_format($value['discount_amount'],2,'.',','); ?>
                                                    @endif
                                                </td>
                                                <td class="text-right">{{ number_format($value['total_amount'],2,'.',',') }}</td>
                                            </tr>

                                            @endforeach
                                            @endif

                                            <?php
                                                if ($bill['vat_type'] == 0)
                                                {
                                                    $vat_amount  = ($bill['total_vat']*($net_paya - $bill['total_discount']))/100;
                                                }
                                                else
                                                {
                                                    $vat_amount  = $bill['total_vat'];
                                                }

                                                if ($bill['total_discount_type'] == 0)
                                                {
                                                    $discount_on_total_amount  = ($bill['total_discount_amount']*($net_paya + $vat_amount - $bill['total_discount']))/100;
                                                }
                                                else
                                                {
                                                    $discount_on_total_amount  = $bill['total_discount_amount'];
                                                }
                                            ?>
                                            
                                            <tr>
                                                <td style="font-size: 16px;font-weight: bold" colspan="5" class="text-right">Sub Total</td>
                                                <td style="font-size: 16px;font-weight: bold" class="text-right">{{ number_format($net_paya - $bill['total_discount'],2,'.',',') }}</td>
                                            </tr>

                                            <tr>
                                                <td style="font-size: 16px;font-weight: bold" colspan="5" class="text-right">VAT ({{ $bill['vat_type'] == 0 ? $bill['total_vat'].'%' : 'BDT' }})</td>
                                                <td style="font-size: 16px;font-weight: bold" class="text-right">{{ number_format($vat_amount,2,'.',',') }}</td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="5" class="border-0 text-right">
                                                    Discount On Total Amount ({{ $bill['total_discount_type'] == 0 ? $bill['total_discount_amount'].'%' : 'BDT' }})</td>
                                                <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">{{ number_format($discount_on_total_amount,2,'.',',') }}</td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="5" class="border-0 text-right">Payable</td>
                                                <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                    {{ number_format($bill['bill_amount'],2,'.',',') }}
                                                </td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="5" class="border-0 text-right">Previous Dues</td>
                                                <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                    {{ number_format($pre_dues - $bill['due_amount'],2,'.',',') }}
                                                </td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="5" class="border-0 text-right">Net Payable</td>
                                                <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                    {{ number_format($bill['bill_amount'] + $pre_dues - $bill['due_amount'],2,'.',',') }}
                                                </td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="5" class="border-0 text-right">Paid</td>
                                                <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                    {{ number_format($bill['bill_amount'] - $bill['due_amount'],2,'.',',') }}
                                                </td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px;font-weight: bold" colspan="5" class="border-0 text-right">Dues</td>
                                                <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                    {{ number_format($pre_dues - $bill['due_amount'] + $bill['due_amount'],2,'.',',') }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                @if($bill['bill_note'] != null)
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 style="font-size: 16px"><strong>Note :</strong> {{ $bill['bill_note'] }}</h6>
                                    </div>
                                </div>
                                @endif

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="position: fixed;bottom: 0">
                                    <!-- <h4 class="float-right font-size-16">Order # 12345</h4> -->
                                    <!-- <div class="col-md-4">
                                        <img class="float-left" src="{{ url('public/az-ai.png') }}" alt="logo" height="20"/>
                                    </div>

                                    <div class="col-md-4">
                                        <h2 style="text-align: center">{{ $user_info['organization_name'] }}</h2>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['address'] }}</p>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['contact_number'] }}</p>
                                    </div>

                                    <div class="col-md-4">
                                        <p style="line-height: 18px;text-align: right;padding: 0px">Phone - 01718937082<br>01711418731<br>01711418731</p>
                                    </div> -->
                                    @if($user_info['footer_image'] != null)
                                        <img class="float-left" src="{{ url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection