<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class LeaveTransactions extends Model
{  
    protected $table = "leave_transactions";

    public function employee()
    {
        return $this->belongsTo('App\Models\Customers','employee_id');
    }

    public function leaveCategory()
    {
        return $this->belongsTo('App\Models\LeaveCategories','leave_category');
    }

    public function reliever()
    {
        return $this->belongsTo('App\Models\Customers','reliever_id');
    }
}
