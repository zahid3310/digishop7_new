<?php

namespace Modules\Payroll\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\SalaryGrades;
use App\Models\SalaryStatements;
use App\Models\SalaryStatementTrns;
use App\Models\Customers;
use DB;
use Response;

class SalaryStatementsController extends Controller
{
    public function index()
    {
        $salary_statements = SalaryStatements::orderByRaw('employee_id', 'asc')->get();
        

        return view('payroll::SalaryStatements.index', compact('salary_statements'));
    }

    public function create()
    {
        $grades = SalaryGrades::orderBy('position', 'ASC')->get();

        return view('payroll::SalaryStatements.create', compact('grades'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'employee_id'   => 'required|integer',
            'grade_id'      => 'nullable|integer',
            'position'      => 'nullable|integer',
            'basic_salary'  => 'nullable|numeric',
            'house_rent'    => 'nullable|numeric',
            'medical'       => 'nullable|numeric',
            'convence'      => 'nullable|numeric',
            'food'          => 'nullable|numeric',
            'mobile_bill'   => 'nullable|numeric',
            'others'        => 'nullable|numeric',
            'gross'         => 'nullable|numeric',
            'tax'           => 'nullable|numeric',
            'bonus'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();
        $check      = SalaryStatements::where('employee_id', $data['employee_id'])->count();

        if ($check > 0)
        {
            return back()->with('unsuccess', 'This employee has already been added to the salary statement list');
        }

        DB::beginTransaction();

        try{
            $salary_statement                = new SalaryStatements;
            $salary_statement->employee_id   = $data['employee_id'];
            $salary_statement->grade_id      = $data['grade_id'];
            $salary_statement->position      = $data['position'];
            $salary_statement->basic         = $data['basic_salary'];
            $salary_statement->house_rent    = $data['house_rent'];
            $salary_statement->medical       = $data['medical'];
            $salary_statement->convence      = $data['convence'];
            $salary_statement->food          = $data['food'];
            $salary_statement->mobile        = $data['mobile_bill'];
            $salary_statement->others        = $data['others'];
            $salary_statement->gross         = $data['gross'];
            $salary_statement->pf_amount     = $data['pf_amount'];
            $salary_statement->tax_amount    = $data['tax'];
            $salary_statement->bonus_amount  = $data['bonus'];
            $salary_statement->created_by    = $user_id;

            if ($salary_statement->save())
            {   
                $find_employee                        = Customers::find($data['employee_id']);
                $salary_statement_trns                = new SalaryStatementTrns;
                $salary_statement_trns->employee_id   = $data['employee_id'];
                $salary_statement_trns->designation   = $find_employee->designation;
                $salary_statement_trns->statement_date= date('Y-m-d');
                $salary_statement_trns->grade_id      = $data['grade_id'];
                $salary_statement_trns->position      = $data['position'];
                $salary_statement_trns->basic         = $data['basic_salary'];
                $salary_statement_trns->house_rent    = $data['house_rent'];
                $salary_statement_trns->medical       = $data['medical'];
                $salary_statement_trns->convence      = $data['convence'];
                $salary_statement_trns->food          = $data['food'];
                $salary_statement_trns->mobile        = $data['mobile_bill'];
                $salary_statement_trns->others        = $data['others'];
                $salary_statement_trns->gross         = $data['gross'];
                $salary_statement_trns->pf_amount     = $data['pf_amount'];
                $salary_statement_trns->tax_amount    = $data['tax'];
                $salary_statement_trns->bonus_amount  = $data['bonus'];
                $salary_statement_trns->created_by    = $user_id;
                $salary_statement_trns->save();

                DB::commit();
                return back()->with("success","Salary Statement Added Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        return view('payroll::show');
    }

    public function edit($id)
    {
        $find_statement = SalaryStatements::find($id);
        $grades         = SalaryGrades::orderBy('position', 'DESC')->get();

        return view('payroll::SalaryStatements.edit', compact('find_statement', 'grades'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'employee_id'   => 'required|integer',
            'grade_id'      => 'nullable|integer',
            'position'      => 'nullable|integer',
            'basic_salary'  => 'nullable|numeric',
            'house_rent'    => 'nullable|numeric',
            'medical'       => 'nullable|numeric',
            'convence'      => 'nullable|numeric',
            'food'          => 'nullable|numeric',
            'mobile_bill'   => 'nullable|numeric',
            'others'        => 'nullable|numeric',
            'gross'         => 'nullable|numeric',
            'tax'           => 'nullable|numeric',
            'bonus'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $salary_statement                = SalaryStatements::find($id);
            $salary_statement->employee_id   = $data['employee_id'];
            $salary_statement->grade_id      = $data['grade_id'];
            $salary_statement->position      = $data['position'];
            $salary_statement->basic         = $data['basic_salary'];
            $salary_statement->house_rent    = $data['house_rent'];
            $salary_statement->medical       = $data['medical'];
            $salary_statement->convence      = $data['convence'];
            $salary_statement->food          = $data['food'];
            $salary_statement->mobile        = $data['mobile_bill'];
            $salary_statement->others        = $data['others'];
            $salary_statement->gross         = $data['gross'];
            $salary_statement->pf_amount     = $data['pf_amount'];
            $salary_statement->tax_amount    = $data['tax'];
            $salary_statement->bonus_amount  = $data['bonus'];
            $salary_statement->updated_by    = $user_id;

            if ($salary_statement->save())
            {   
                $find_employee                        = Customers::find($data['employee_id']);
                $salary_statement_trns                = new SalaryStatementTrns;
                $salary_statement_trns->employee_id   = $data['employee_id'];
                $salary_statement_trns->designation   = $find_employee->designation;
                $salary_statement_trns->statement_date= date('Y-m-d');
                $salary_statement_trns->grade_id      = $data['grade_id'];
                $salary_statement_trns->position      = $data['position'];
                $salary_statement_trns->basic         = $data['basic_salary'];
                $salary_statement_trns->house_rent    = $data['house_rent'];
                $salary_statement_trns->medical       = $data['medical'];
                $salary_statement_trns->convence      = $data['convence'];
                $salary_statement_trns->food          = $data['food'];
                $salary_statement_trns->mobile        = $data['mobile_bill'];
                $salary_statement_trns->others        = $data['others'];
                $salary_statement_trns->gross         = $data['gross'];
                $salary_statement_trns->pf_amount     = $data['pf_amount'];
                $salary_statement_trns->tax_amount    = $data['tax'];
                $salary_statement_trns->bonus_amount  = $data['bonus'];
                $salary_statement_trns->created_by    = $user_id;
                $salary_statement_trns->save();

                DB::commit();
                return redirect()->route('salary_statements_index')->with("success","Salary Statement Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function gradeDetails($grade_id)
    {
        $data = SalaryGrades::find($grade_id);

        return Response::json($data);
    }
}