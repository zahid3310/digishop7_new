@extends('layouts.app')

@section('title', 'Show')

<style>
    table,th {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 2px;
    }

    td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        padding: 2px;
    }

    @page {
        size: A4;
        page-break-after: always;
    }
    

</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Purchase</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Purchase</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="padding: 10px;padding-top: 25px" class="row">
                    <div style="padding-bottom: 30px" class="d-print-none col-md-12">
                        <div class="float-right">
                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                        </div>
                    </div>

                    <div class="col-12" style="height:100vh;">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 32px;padding-top: 10px;font-weight: bold">সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</h2>
                                        <p style="line-height: 1.2;font-size: 20px;font-weight: bold" class="text-center">{{ $user_info['organization_name'] }}</p>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        {{ QrCode::size(70)->generate("string") }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px">অত্যাধুনিক স্বয়ংক্রিয় মেশিনে উন্নত ধান থেকে উন্নতমানের বিভিন্ন প্রকার চাউল উৎপাদন ও সরবরাহ ক্রা হয় ।  </p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">বাঁকা ব্রিক্স ফিল্ড, জীবননগর, চুয়াডাঙ্গা । ০১৬৮৮ ৯৩৯৩৯৩, ০১৯১৬ ০৪১১২৯১, ০১৭১৫ ৭০৪১২৬</p>
                                        <p style="line-height: 3;text-align: center"><span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 20px">মাল আনলোডের প্রমাণপত্র</span>
                                            <span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 15px;float: right">গ্রাহক কপি</span>
                                            <span style="font-size: 15px;float: left">{{ str_pad($bill['bill_number'], 6, "0", STR_PAD_LEFT) }}</span>
                                        </p>
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="row">
                                    <div style="font-size: 15px" class="col-md-8">
                                        <address style="margin-bottom: 0px !important">
                                            <span style="font-weight: bold">প্রতিষ্ঠানের নাম : </span>{{ $bill['customer_name'] }}
                                        </address>

                                        <address style="margin-bottom: 0px !important">
                                            <span style="font-weight: bold">প্রাপকের নাম : </span>{{ $bill->vendor->ClientName }}
                                        </address>
                                    </div>
                                    <div style="font-size: 15px;" class="col-md-4 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>তারিখ :  </strong>
                                            {{ date('d-m-Y', strtotime($bill['bill_date'])) }}<br><br>
                                        </address>
                                    </div>
                                    <div style="font-size: 15px" class="col-md-12">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Address : </strong> {{ $bill->vendor->Address != null ? $bill->vendor->Address : ''  }}
                                        </address>
                                    </div>
                                </div>

                                <div style="padding-top: 10px;padding-bottom: 20px">
                                    <table style="width: 100%" class="table-bordered">
                                        <tr>
                                            <th style="font-size: 18px;width: 30%;text-align: center;">মালের বিবরণ</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">পরিমাণ</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">ওজন</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">মোট ওজন</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">দর</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">মোট টাকা</th>
                                        </tr>

                                        @if($entries->count() > 0)

                                        <?php
                                            $total_amount                   = 0;
                                        ?>

                                        @foreach($entries as $key => $value)
                                        <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $variation_name = ProductVariationName($value['product_entry_id']);

                                            if ($value['product_code'] != null)
                                            {
                                                $productCode  = ' - '.$value['product_code'];
                                            }
                                            else
                                            {
                                                $productCode  = '';
                                            }

                                            if ($value['product_name'] != null)
                                            {
                                                $category  = ' - '.$value['product_name'];
                                            }
                                            else
                                            {
                                                $category  = '';
                                            }

                                            if ($value['brand_name'] != null)
                                            {
                                                $brandName  = $value['brand_name'];
                                            }
                                            else
                                            {
                                                $brandName  = '';
                                            }

                                            if ($value['unit_name'] != null)
                                            {
                                                $unit  = ' '.$value['unit_name'];
                                            }
                                            else
                                            {
                                                $unit  = '';
                                            }

                                            if ($variation_name != null)
                                            {
                                                $variation  = ' '.$variation_name;
                                            }
                                            else
                                            {
                                                $variation  = '';
                                            }

                                            $pre_dues = $bill['previous_due'];
                                            $net_paya = round($total_amount, 2);
                                            $paid     = round($bill['cash_given'], 2);
                                            $dues     = round($net_paya - $paid, 2);
                                        ?>

                                        <tr class="tr-height">
                                            <td style="padding-left: 30px">{{ $value['product_entry_name'] . $productCode }}</td>
                                            <td style="text-align: center">{{ $value['bosta'] }}</td>
                                            <!--<td style="text-align: center">{{ $value['quantity']/$value['bosta'] }}</td>-->
                                            <td style="text-align: center">{{ $value['weight_per_bosta'] }}</td>
                                            <td style="text-align: center">{{ $value['quantity'] }}</td>
                                            <td style="text-align: center">{{ $value['rate'] }}</td>
                                            <td style="text-align: center">{{ round($value['total_amount'], 2) }}</td>
                                        </tr>
                                        @endforeach
                                        @endif

                                        <?php
                                            if ($bill['vat_type'] == 0)
                                            {
                                                $vat_amount  = ($bill['total_vat']*($net_paya - $bill['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $vat_amount  = $bill['total_vat'];
                                            }

                                            if ($bill['total_discount_type'] == 0)
                                            {
                                                $discount_on_total_amount  = ($bill['total_discount_amount']*($net_paya + $vat_amount - $bill['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $discount_on_total_amount  = $bill['total_discount_amount'];
                                            }
                                        ?>

                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="4"><strong></strong></th>
                                            <th style="text-align: left;text-align: right;color: #e50606;" colspan="1"><strong>সর্বমোট</strong></th>
                                            <th style="text-align: center;color: #e50606;">{{ $net_paya != 0 ? round($net_paya - $bill['total_discount']) : '' }}</th>
                                        </tr>                                        
                                        
                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="4"><strong></strong></th>
                                            <th style="text-align: left;text-align: right;" colspan="1"><strong>জমা</strong></th>
                                            <th style="text-align: center;">{{ $bill['cash_given'] }}</th>
                                        </tr>
                                        
                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="4"><strong></strong></th>
                                            <th style="text-align: left;text-align: right;" colspan="1"><strong>মোট বাকি</strong></th>
                                            <th style="text-align: center;">{{ round($net_paya - $bill['cash_given']) }}</th>
                                        </tr>
                                        

                                        <tr>
                                            <th style="text-align: left" colspan="6">গাড়ি নং : </strong>{{ $bill->vichele_id != null ? $bill->vichele_id : '' }}</th>
                                            <!--<th style="text-align: center" colspan="2">মাল আনলোডের স্থান</th>-->
                                        </tr>

                                        <tr>
                                            <th style="text-align: left" colspan="6">ড্রাইভারের নাম : </strong>{{ $bill['driver_name'] }}</th>
                                            <!--<th style="text-align: center" colspan="2" rowspan="3"></th>-->
                                        </tr>

                                        <tr>
                                            <th style="text-align: left" colspan="6"><strong>ড্রাইভারের মোবা : </strong>{{ $bill['driver_phone'] }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: left" colspan="6"><strong>গাড়ি ভাড়া : </strong>{{ $bill['rent'] }}</th>
                                        </tr>
                                        
                                        <tr>
                                            <th style="text-align: left;" colspan="6"><strong>মাল আনলোডের স্থান : {{ $bill['unload_place_name'] }}</strong></th>
                                        </tr>
                                    </table>
                                </div>

                                <br>
                                <br>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">আনলোড ম্যানেজার </span> </h6>
                                    </div>
                                    <div class="col-md-3">
                                        <h6 style="text-align: center"> <span style="border-top: 1px dotted black;padding: 5px">কম্পিউটার অপারেটর</span> </h6>
                                    </div>
                                    <div class="col-md-3">
                                        <h6 style="text-align: center"> <span style="border-top: 1px dotted black;padding: 5px">জেনারেল ম্যানেজার</span> </h6>
                                    </div>
                                    <div class="col-md-3">
                                        <h6 style="text-align: right"> <span style="padding: 5px">পক্ষে : সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</span> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
  
  

                    <div class="col-12" style="height:100vh;">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 32px;padding-top: 10px;font-weight: bold">সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</h2>
                                        <p style="line-height: 1.2;font-size: 20px;font-weight: bold" class="text-center">{{ $user_info['organization_name'] }}</p>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        {{ QrCode::size(70)->generate("string") }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px">অত্যাধুনিক স্বয়ংক্রিয় মেশিনে উন্নত ধান থেকে উন্নতমানের বিভিন্ন প্রকার চাউল উৎপাদন ও সরবরাহ ক্রা হয় ।  </p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">বাঁকা ব্রিক্স ফিল্ড, জীবননগর, চুয়াডাঙ্গা । ০১৬৮৮ ৯৩৯৩৯৩, ০১৯১৬ ০৪১১২৯১, ০১৭১৫ ৭০৪১২৬</p>
                                        <p style="line-height: 3;text-align: center"><span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 20px">মাল আনলোডের প্রমাণপত্র</span>
                                            <span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 15px;float: right">লেবার কপি</span>
                                            <span style="font-size: 15px;float: left">{{ str_pad($bill['bill_number'], 6, "0", STR_PAD_LEFT) }}</span>
                                        </p>
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="row">
                                    <div style="font-size: 15px" class="col-md-8">
                                        <address style="margin-bottom: 0px !important">
                                            <span style="font-weight: bold">প্রতিষ্ঠানের নাম : </span>{{ $bill['customer_name'] }}
                                        </address>

                                        <address style="margin-bottom: 0px !important">
                                            <span style="font-weight: bold">প্রাপকের নাম : </span>{{ $bill->vendor->proprietor_name }}
                                        </address>
                                    </div>
                                    <div style="font-size: 15px;" class="col-md-4 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>তারিখ :  </strong>
                                            {{ date('d-m-Y', strtotime($bill['bill_date'])) }}<br><br>
                                        </address>
                                    </div>
                                    <div style="font-size: 15px" class="col-md-12">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Address : </strong> {{ $bill->vendor->address != null ? $bill->vendor->address : ''  }}
                                        </address>
                                    </div>
                                </div>

                                <div style="padding-top: 10px;padding-bottom: 20px">
                                    <table style="width: 100%" class="table-bordered">
                                        <tr>
                                            <th style="font-size: 18px;width: 30%;text-align: center;">মালের বিবরণ</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">পরিমাণ</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">ওজন</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">মোট ওজন</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">দর</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">মোট টাকা</th>
                                        </tr>

                                        @if($entries->count() > 0)

                                        <?php
                                            $total_amount                   = 0;
                                        ?>

                                        @foreach($entries as $key => $value)
                                        <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $variation_name = ProductVariationName($value['product_entry_id']);

                                            if ($value['product_code'] != null)
                                            {
                                                $productCode  = ' - '.$value['product_code'];
                                            }
                                            else
                                            {
                                                $productCode  = '';
                                            }

                                            if ($value['product_name'] != null)
                                            {
                                                $category  = ' - '.$value['product_name'];
                                            }
                                            else
                                            {
                                                $category  = '';
                                            }

                                            if ($value['brand_name'] != null)
                                            {
                                                $brandName  = $value['brand_name'];
                                            }
                                            else
                                            {
                                                $brandName  = '';
                                            }

                                            if ($value['unit_name'] != null)
                                            {
                                                $unit  = ' '.$value['unit_name'];
                                            }
                                            else
                                            {
                                                $unit  = '';
                                            }

                                            if ($variation_name != null)
                                            {
                                                $variation  = ' '.$variation_name;
                                            }
                                            else
                                            {
                                                $variation  = '';
                                            }

                                            $pre_dues = $bill['previous_due'];
                                            $net_paya = round($total_amount, 2);
                                            $paid     = round($bill['cash_given'], 2);
                                            $dues     = round($net_paya - $paid, 2);
                                        ?>

                                        <tr class="tr-height">
                                            <td style="padding-left: 30px">{{ $value['product_entry_name'] . $productCode }}</td>
                                            <td style="text-align: center">{{ $value['bosta'] }}</td>
                                            <td style="text-align: center">{{ $value['quantity']/$value['bosta'] }}</td>
                                            <td style="text-align: center">{{ $value['quantity'] }}</td>
                                            <td style="text-align: center">{{ $value['rate'] }}</td>
                                            <td style="text-align: center">{{ round($value['total_amount'], 2) }}</td>
                                        </tr>
                                        @endforeach
                                        @endif

                                        <?php
                                            if ($bill['vat_type'] == 0)
                                            {
                                                $vat_amount  = ($bill['total_vat']*($net_paya - $bill['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $vat_amount  = $bill['total_vat'];
                                            }

                                            if ($bill['total_discount_type'] == 0)
                                            {
                                                $discount_on_total_amount  = ($bill['total_discount_amount']*($net_paya + $vat_amount - $bill['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $discount_on_total_amount  = $bill['total_discount_amount'];
                                            }
                                        ?>

                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="4"><strong></strong></th>
                                            <th style="text-align: left;text-align: right;color: #e50606;" colspan="1"><strong>সর্বমোট</strong></th>
                                            <th style="text-align: center;color: #e50606;">{{ $net_paya != 0 ? round($net_paya - $bill['total_discount']) : '' }}</th>
                                        </tr>
                                        
                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="4"><strong></strong></th>
                                            <th style="text-align: left;text-align: right;" colspan="1"><strong>জমা</strong></th>
                                            <th style="text-align: center;">{{ $bill['cash_given'] }}</th>
                                        </tr>
                                        
                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="4"><strong></strong></th>
                                            <th style="text-align: left;text-align: right;" colspan="1"><strong>মোট বাকি</strong></th>
                                            <th style="text-align: center;">{{ round($net_paya - $bill['cash_given']) }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: left" colspan="6">গাড়ি নং : </strong>{{ $bill->vichele_id != null ? $bill->vichele_id : '' }}</th>
                                            <!--<th style="text-align: center" colspan="2">মাল আনলোডের স্থান</th>-->
                                        </tr>

                                        <tr>
                                            <th style="text-align: left" colspan="6">ড্রাইভারের নাম : </strong>{{ $bill['driver_name'] }}</th>
                                            <!--<th style="text-align: center" colspan="2" rowspan="3"></th>-->
                                        </tr>

                                        <tr>
                                            <th style="text-align: left" colspan="6"><strong>ড্রাইভারের মোবা : </strong>{{ $bill['driver_phone'] }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: left" colspan="6"><strong>গাড়ি ভাড়া : </strong>{{ $bill['rent'] }}</th>
                                        </tr>
                                        
                                        <tr>
                                            <th style="text-align: left;" colspan="6"><strong>মাল আনলোডের স্থান : {{ $bill['unload_place_name'] }}</strong></th>
                                        </tr>
                                    </table>
                                </div>

                                <br>
                                <br>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">আনলোড ম্যানেজার </span> </h6>
                                    </div>
                                    <div class="col-md-3">
                                        <h6 style="text-align: center"> <span style="border-top: 1px dotted black;padding: 5px">কম্পিউটার অপারেটর</span> </h6>
                                    </div>
                                    <div class="col-md-3">
                                        <h6 style="text-align: center"> <span style="border-top: 1px dotted black;padding: 5px">জেনারেল ম্যানেজার</span> </h6>
                                    </div>
                                    <div class="col-md-3">
                                        <h6 style="text-align: right"> <span style="padding: 5px">পক্ষে : সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</span> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 32px;padding-top: 10px;font-weight: bold">সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</h2>
                                        <p style="line-height: 1.2;font-size: 20px;font-weight: bold" class="text-center">{{ $user_info['organization_name'] }}</p>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        {{ QrCode::size(70)->generate("string") }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px">অত্যাধুনিক স্বয়ংক্রিয় মেশিনে উন্নত ধান থেকে উন্নতমানের বিভিন্ন প্রকার চাউল উৎপাদন ও সরবরাহ ক্রা হয় ।  </p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">বাঁকা ব্রিক্স ফিল্ড, জীবননগর, চুয়াডাঙ্গা । ০১৬৮৮ ৯৩৯৩৯৩, ০১৯১৬ ০৪১১২৯১, ০১৭১৫ ৭০৪১২৬</p>
                                        <p style="line-height: 3;text-align: center"><span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 20px">মাল আনলোডের প্রমাণপত্র</span>
                                            <span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 15px;float: right">মুল কপি</span>
                                            <span style="font-size: 15px;float: left">{{ str_pad($bill['bill_number'], 6, "0", STR_PAD_LEFT) }}</span>
                                        </p>
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="row">
                                    <div style="font-size: 15px" class="col-md-8">
                                        <address style="margin-bottom: 0px !important">
                                            <span style="font-weight: bold">প্রতিষ্ঠানের নাম : </span>{{ $bill['customer_name'] }}
                                        </address>

                                        <address style="margin-bottom: 0px !important">
                                            <span style="font-weight: bold">প্রাপকের নাম : </span>{{ $bill->vendor->proprietor_name }}
                                        </address>
                                    </div>
                                    <div style="font-size: 15px;" class="col-md-4 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>তারিখ :  </strong>
                                            {{ date('d-m-Y', strtotime($bill['bill_date'])) }}<br><br>
                                        </address>
                                    </div>
                                    <div style="font-size: 15px" class="col-md-12">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Address : </strong> {{ $bill->vendor->address != null ? $bill->vendor->address : ''  }}
                                        </address>
                                    </div>
                                </div>

                                <div style="padding-top: 10px;padding-bottom: 20px">
                                    <table style="width: 100%" class="table-bordered">
                                        <tr>
                                            <th style="font-size: 18px;width: 30%;text-align: center;">মালের বিবরণ</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">পরিমাণ</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">ওজন</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">মোট ওজন</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">দর</th>
                                            <th style="font-size: 18px;width: 10%;text-align: center;">মোট টাকা</th>
                                        </tr>

                                        @if($entries->count() > 0)

                                        <?php
                                            $total_amount                   = 0;
                                        ?>

                                        @foreach($entries as $key => $value)
                                        <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $variation_name = ProductVariationName($value['product_entry_id']);

                                            if ($value['product_code'] != null)
                                            {
                                                $productCode  = ' - '.$value['product_code'];
                                            }
                                            else
                                            {
                                                $productCode  = '';
                                            }

                                            if ($value['product_name'] != null)
                                            {
                                                $category  = ' - '.$value['product_name'];
                                            }
                                            else
                                            {
                                                $category  = '';
                                            }

                                            if ($value['brand_name'] != null)
                                            {
                                                $brandName  = $value['brand_name'];
                                            }
                                            else
                                            {
                                                $brandName  = '';
                                            }

                                            if ($value['unit_name'] != null)
                                            {
                                                $unit  = ' '.$value['unit_name'];
                                            }
                                            else
                                            {
                                                $unit  = '';
                                            }

                                            if ($variation_name != null)
                                            {
                                                $variation  = ' '.$variation_name;
                                            }
                                            else
                                            {
                                                $variation  = '';
                                            }

                                            $pre_dues = $bill['previous_due'];
                                            $net_paya = round($total_amount, 2);
                                            $paid     = round($bill['cash_given'], 2);
                                            $dues     = round($net_paya - $paid, 2);
                                        ?>

                                        <tr class="tr-height">
                                            <td style="padding-left: 30px">{{ $value['product_entry_name'] . $productCode }}</td>
                                            <td style="text-align: center">{{ $value['bosta'] }}</td>
                                            <td style="text-align: center">{{ $value['quantity']/$value['bosta'] }}</td>
                                            <td style="text-align: center">{{ $value['quantity'] }}</td>
                                            <td style="text-align: center">{{ $value['rate'] }}</td>
                                            <td style="text-align: center">{{ round($value['total_amount'], 2) }}</td>
                                        </tr>
                                        @endforeach
                                        @endif

                                        <?php
                                            if ($bill['vat_type'] == 0)
                                            {
                                                $vat_amount  = ($bill['total_vat']*($net_paya - $bill['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $vat_amount  = $bill['total_vat'];
                                            }

                                            if ($bill['total_discount_type'] == 0)
                                            {
                                                $discount_on_total_amount  = ($bill['total_discount_amount']*($net_paya + $vat_amount - $bill['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $discount_on_total_amount  = $bill['total_discount_amount'];
                                            }
                                        ?>

                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="4"><strong></strong></th>
                                            <th style="text-align: left;text-align: right;color: #e50606;" colspan="1"><strong>সর্বমোট</strong></th>
                                            <th style="text-align: center;color: #e50606;">{{ $net_paya != 0 ? round($net_paya - $bill['total_discount']) : '' }}</th>
                                        </tr>
                                        
                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="4"><strong></strong></th>
                                            <th style="text-align: left;text-align: right;" colspan="1"><strong>জমা</strong></th>
                                            <th style="text-align: center;">{{ $bill['cash_given'] }}</th>
                                        </tr>
                                        
                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="4"><strong></strong></th>
                                            <th style="text-align: left;text-align: right;" colspan="1"><strong>মোট বাকি</strong></th>
                                            <th style="text-align: center;">{{ round($net_paya - $bill['cash_given']) }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: left" colspan="6">গাড়ি নং : </strong>{{ $bill->vichele_id != null ? $bill->vichele_id : '' }}</th>
                                            <!--<th style="text-align: center" colspan="2">মাল আনলোডের স্থান</th>-->
                                        </tr>

                                        <tr>
                                            <th style="text-align: left" colspan="6">ড্রাইভারের নাম : </strong>{{ $bill['driver_name'] }}</th>
                                            <!--<th style="text-align: center" colspan="2" rowspan="3"></th>-->
                                        </tr>

                                        <tr>
                                            <th style="text-align: left" colspan="6"><strong>ড্রাইভারের মোবা : </strong>{{ $bill['driver_phone'] }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: left" colspan="6"><strong>গাড়ি ভাড়া : </strong>{{ $bill['rent'] }}</th>
                                        </tr>
                                        
                                        <tr>
                                            <th style="text-align: left;" colspan="6"><strong>মাল আনলোডের স্থান : {{ $bill['unload_place_name'] }}</strong></th>
                                        </tr>
                                    </table>
                                </div>

                                <br>
                                <br>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">আনলোড ম্যানেজার </span> </h6>
                                    </div>
                                    <div class="col-md-3">
                                        <h6 style="text-align: center"> <span style="border-top: 1px dotted black;padding: 5px">কম্পিউটার অপারেটর</span> </h6>
                                    </div>
                                    <div class="col-md-3">
                                        <h6 style="text-align: center"> <span style="border-top: 1px dotted black;padding: 5px">জেনারেল ম্যানেজার</span> </h6>
                                    </div>
                                    <div class="col-md-3">
                                        <h6 style="text-align: right"> <span style="padding: 5px">পক্ষে : সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</span> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection
