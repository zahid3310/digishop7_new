<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('productions/transfer-to-production')->group(function() {
    Route::get('/', 'ProductionsController@transferIndex')->name('productions_transfer_to_production_index');
    Route::get('/create', 'ProductionsController@transferCreate')->name('productions_transfer_to_production_create');
    Route::post('/store', 'ProductionsController@transferStore')->name('productions_transfer_to_production_store');
    Route::get('/edit/{id}', 'ProductionsController@transferEdit')->name('productions_transfer_to_production_edit');
    Route::post('/update/{id}', 'ProductionsController@transferUpdate')->name('productions_transfer_to_production_update');
    Route::get('/get-raw-materials', 'ProductionsController@rawMaterialsList')->name('productions_raw_materials_list');
});

Route::prefix('productions/received-finished-goods')->group(function() {
    Route::get('/', 'ProductionsController@finishedGoodsIndex')->name('productions_received_finished_goods_index');
    Route::get('/create', 'ProductionsController@finishedGoodsCreate')->name('productions_received_finished_goods_create');
    Route::post('/store', 'ProductionsController@finishedGoodsStore')->name('productions_received_finished_goods_store');
    Route::get('/edit/{id}', 'ProductionsController@finishedGoodsEdit')->name('productions_received_finished_goods_edit');
    Route::post('/update/{id}', 'ProductionsController@finishedGoodsUpdate')->name('productions_received_finished_goods_update');
    Route::get('/production-numbers', 'ProductionsController@getProductionnembers');
});

Route::prefix('productions/return-to-warehouse')->group(function() {
    Route::get('/', 'ProductionsController@returnToWarehouseIndex')->name('productions_return_to_warehouse_index');
    Route::post('/store', 'ProductionsController@returnToWarehouseStore')->name('productions_return_to_warehouse_store');
    Route::get('/edit/{id}', 'ProductionsController@returnToWarehouseEdit')->name('productions_return_to_warehouse_edit');
    Route::post('/update/{id}', 'ProductionsController@returnToWarehouseUpdate')->name('productions_return_to_warehouse_update');
    Route::get('/delete/{id}', 'ProductionsController@returnToWarehouseDelete')->name('productions_return_to_warehouse_delete');
    Route::get('/production-list', 'ProductionsController@productionList');
    Route::get('/find-production-number/{id}', 'ProductionsController@findProductionNumber');
    Route::get('/return-list', 'ProductionsController@returnList');
    Route::get('/return-search-list/{id}', 'ProductionsController@returnSearchList');
    Route::get('/production-entries-list-by-production-number/{id}', 'ProductionsController@productionEntriesListByProductionNumber');
});

Route::prefix('productions/damage-finished-goods')->group(function() {
    Route::get('/', 'ProductionsController@damagedFinishedGoodsIndex')->name('productions_damaged_finished_goods_index');
    Route::post('/store', 'ProductionsController@damagedFinishedGoodsStore')->name('productions_damaged_finished_goods_store');
    Route::get('/edit/{id}', 'ProductionsController@damagedFinishedGoodsEdit')->name('productions_damaged_finished_goods_edit');
    Route::post('/update/{id}', 'ProductionsController@damagedFinishedGoodsUpdate')->name('productions_damaged_finished_goods_update');
    Route::get('/delete/{id}', 'ProductionsController@damagedFinishedGoodsDelete')->name('productions_damaged_finished_goods_delete');
    Route::get('/damage-list', 'ProductionsController@damageList');
    Route::get('/damage-search-list/{id}', 'ProductionsController@damageSearchList');
});

Route::prefix('productions/daily-report-production')->group(function() {
    Route::get('/', 'ProductionsController@dailyReport')->name('productions_daily_report_index');
    Route::get('/print', 'ProductionsController@dailyReportPrint')->name('productions_daily_report_print');
});