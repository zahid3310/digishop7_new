<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('balancetransfer')->group(function() {
    Route::get('/', 'BalanceTransferController@index')->name('balance_transfer_index');
    Route::post('/store', 'BalanceTransferController@store')->name('balance_transfer_store');
    Route::get('/edit/{id}', 'BalanceTransferController@edit')->name('balance_transfer_edit');
    Route::get('/show/{id}', 'BalanceTransferController@show')->name('balance_transfer_show');
    Route::post('/update/{id}', 'BalanceTransferController@update')->name('balance_transfer_update');
});

Route::prefix('balancetransfer/print-check')->group(function() {
    Route::get('/', 'BalanceTransferController@printCheckIndex')->name('balance_transfer_print_check_index');
    Route::post('/store', 'BalanceTransferController@printCheckStore')->name('balance_transfer_print_check_store');
    Route::get('/edit/{id}', 'BalanceTransferController@printCheckEdit')->name('balance_transfer_print_check_edit');
    Route::post('/update/{id}', 'BalanceTransferController@printCheckUpdate')->name('balance_transfer_print_check_update');
    Route::get('/show/{id}', 'BalanceTransferController@printCheckShow')->name('balance_transfer_print_check_show');
});
