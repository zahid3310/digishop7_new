@extends('layouts.app')

@section('title', 'Show')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.purchase_return')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.purchase_return')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.show')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if($user_info['header_image'] == null)
                                        <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-4 col-xs-12 col-sm-12">
                                            <h2 style="text-align: center">{{ $user_info['organization_name'] }}</h2>
                                            <p style="line-height: 0px;text-align: center">{{ $user_info['address'] }}</p>
                                            <p style="line-height: 0px;text-align: center">{{ $user_info['contact_number'] }}</p>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    @else
                                        <img class="float-left" src="{{ url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <address>
                                            <strong>{{ __('messages.Return_to')}}:</strong><br>
                                            {{ $purchase_return['customer_name'] }}
                                            @if($purchase_return['address'] != null)
                                               <br> <?php echo $purchase_return['address']; ?> <br>
                                            @endif
                                            @if($purchase_return['address'] == null)
                                                <br>
                                            @endif
                                            {{ $purchase_return['phone'] }}
                                        </address>
                                    </div>

                                    <div class="col-sm-4">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div class="col-sm-4 text-sm-right">
                                        <address class="mt-2 mt-sm-0">
                                            <strong>{{ __('messages.return_date')}}:</strong><br>
                                            {{ date('d-m-Y', strtotime($purchase_return['purchase_return_date'])) }}<br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h3 class="font-size-15 font-weight-bold">{{ __('messages.return_summary')}}</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 class="font-size-15 font-weight-bold float-right">{{ __('messages.return')}} # {{ 'PR - ' . str_pad($purchase_return['purchase_return_number'], 6, "0", STR_PAD_LEFT) }}</h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-nowrap">
                                        <thead>
                                            <tr>
                                                <th style="width: 70px;">{{ __('messages.no')}}.</th>
                                                <th>{{ __('messages.items')}}</th>
                                                <th class="text-right">{{ __('messages.rate')}}</th>
                                                <th class="text-right">{{ __('messages.qty')}}</th>
                                                <th class="text-center">{{ __('messages.unit')}}</th>
                                                <th class="text-right">{{ __('messages.price')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @if(!empty($entries) && ($entries->count() > 0))

                                            <?php $sub_total = 0; ?>

                                            @foreach($entries as $key => $value)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $value['product_name'] }}<br>{{ $value['product_entry_name'] }}</td>
                                                <td class="text-right">{{ number_format($value['rate'],2,'.',',') }}</td>
                                                <td class="text-right">{{ $value['quantity'] }}</td>
                                                <td class="text-center">{{ $value->convertedUnit->name }}</td>
                                                <td class="text-right">{{ number_format($value['total_amount'],2,'.',',') }}</td>
                                            </tr>

                                            <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                            @endforeach
                                            @endif
                                            
                                            <tr>
                                                <td colspan="5" class="text-right">{{ __('messages.sub_total')}}</td>
                                                <td class="text-right">{{ number_format($sub_total,2,'.',',') }}</td>
                                            </tr>

                                            <tr style="line-height: 0px">
                                                <td colspan="5" class="border-0 text-right">{{ __('messages.vat/tax/adjustment')}}</td>
                                                <td class="border-0 text-right">{{ number_format($purchase_return['return_amount'] - $sub_total,2,'.',',') }}</td>
                                            </tr>
                                           
                                            <tr style="line-height: 0px">
                                                <td colspan="5" class="border-0 text-right">
                                                    <strong>{{ __('messages.total')}}</strong></td>
                                                <td class="border-0 text-right"><h4 class="m-0">{{ number_format($purchase_return['return_amount'],2,'.',',') }}</h4></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                @if($purchase_return['return_note'] != null)
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6><strong>Note :</strong> {{ $purchase_return['return_note'] }}</h6>
                                    </div>
                                </div>
                                @endif

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="position: fixed;bottom: 0">
                                    <!-- <h4 class="float-right font-size-16">Order # 12345</h4> -->
                                    <!-- <div class="col-md-4">
                                        <img class="float-left" src="{{ url('public/az-ai.png') }}" alt="logo" height="20"/>
                                    </div>

                                    <div class="col-md-4">
                                        <h2 style="text-align: center">{{ $user_info['organization_name'] }}</h2>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['address'] }}</p>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['contact_number'] }}</p>
                                    </div>

                                    <div class="col-md-4">
                                        <p style="line-height: 18px;text-align: right;padding: 0px">Phone - 01718937082<br>01711418731<br>01711418731</p>
                                    </div> -->
                                    @if($user_info['footer_image'] != null)
                                        <img class="float-left" src="{{ url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection