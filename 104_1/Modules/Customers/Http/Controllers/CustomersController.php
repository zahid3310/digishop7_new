<?php

namespace Modules\Customers\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Customers;
use App\Models\Users;
use App\Models\CustomerTypes;
use App\Models\Departments;
use App\Models\Sections;
use App\Models\Areas;
use App\Models\Accounts;
use App\Models\Invoices;
use App\Models\Bills;
use App\Models\Clients;
use App\Models\Registers;
use Response;
use DB;

class CustomersController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $customers      = Customers::orderBy('serial', 'ASC')->where('contact_type', $_GET['contact_type'])->get();
        $types          = CustomerTypes::orderBy('created_at', 'DESC')->get();
        $departments    = Departments::orderBy('created_at', 'DESC')->get();
        $sections       = Sections::orderBy('created_at', 'DESC')->get();
        $references     = Customers::where('contact_type', '=', 3)->orderBy('created_at', 'DESC')->get();
        $areas          = Areas::orderBy('created_at', 'ASC')->get();


        // $cus   = Customers::whereNotIn('id', [1,2])->get();

        // foreach($cus as $key => $data)
        // {
        //     if($data['contact_type'] == 0)
        //     {
        //         $CompanyID          = Auth()->user()->branch_id;
        //         $PtnID              = 263;

        //         //getting parent's PtnGroupCode
        //         $GroupIdQuery       = Registers::where('id', $PtnID)->get();
        //         $GetGroupId         = isset($GroupIdQuery[0]["GroupID"]) ? $GroupIdQuery[0]["GroupID"] : "";
        //         $PtnGroupCodeParent = isset($GroupIdQuery[0]["PtnGroupCode"]) ? $GroupIdQuery[0]["PtnGroupCode"] : "";
        //         $GroupID            = $GetGroupId + 1;

        //         //getting parent's PtnGroupCode
        //         $parentChild        = Registers::where('PtnID', $PtnID)->get();
        //         $parentChildCount   = $parentChild->count();
        //         $parentChildCount++;
        //         $PtnGroupCode       = $PtnGroupCodeParent."-".$parentChildCount;

        //         $register                       = new Registers;
        //         $register->PtnID                = $PtnID;
        //         $register->CompanyID            = $CompanyID;
        //         $register->PtnGroupCode         = $PtnGroupCode;
        //         $register->ClientName           = $data['name'];
        //         $register->ClientAddress        = $data['address'];
        //         $register->ClientContactNumber  = $data['phone'];
        //         $register->GroupID              = $GroupID;
        //         $register->OrderID              = $parentChildCount;
        //         $register->IsTransactable       = 1;
        //         $register->ActiveStatus         = 1;
        //         $register->save();
        //     }

        //     if($data['contact_type'] == 1)
        //     {
        //         $CompanyID          = Auth()->user()->branch_id;
        //         $PtnID              = 264;

        //         //getting parent's PtnGroupCode
        //         $GroupIdQuery       = Registers::where('id', $PtnID)->get();
        //         $GetGroupId         = isset($GroupIdQuery[0]["GroupID"]) ? $GroupIdQuery[0]["GroupID"] : "";
        //         $PtnGroupCodeParent = isset($GroupIdQuery[0]["PtnGroupCode"]) ? $GroupIdQuery[0]["PtnGroupCode"] : "";
        //         $GroupID            = $GetGroupId + 1;

        //         //getting parent's PtnGroupCode
        //         $parentChild        = Registers::where('PtnID', $PtnID)->get();
        //         $parentChildCount   = $parentChild->count();
        //         $parentChildCount++;
        //         $PtnGroupCode       = $PtnGroupCodeParent."-".$parentChildCount;

        //         $register                       = new Registers;
        //         $register->PtnID                = $PtnID;
        //         $register->CompanyID            = $CompanyID;
        //         $register->PtnGroupCode         = $PtnGroupCode;
        //         $register->ClientName           = $data['name'];
        //         $register->ClientAddress        = $data['address'];
        //         $register->ClientContactNumber  = $data['phone'];
        //         $register->GroupID              = $GroupID;
        //         $register->OrderID              = $parentChildCount;
        //         $register->IsTransactable       = 1;
        //         $register->ActiveStatus         = 1;
        //         $register->save();
        //     }

        //     if($data['contact_type'] == 2)
        //     {
        //         $CompanyID          = Auth()->user()->branch_id;
        //         $PtnID              = 49;

        //         //getting parent's PtnGroupCode
        //         $GroupIdQuery       = Registers::where('id', $PtnID)->get();
        //         $GetGroupId         = isset($GroupIdQuery[0]["GroupID"]) ? $GroupIdQuery[0]["GroupID"] : "";
        //         $PtnGroupCodeParent = isset($GroupIdQuery[0]["PtnGroupCode"]) ? $GroupIdQuery[0]["PtnGroupCode"] : "";
        //         $GroupID            = $GetGroupId + 1;

        //         //getting parent's PtnGroupCode
        //         $parentChild        = Registers::where('PtnID', $PtnID)->get();
        //         $parentChildCount   = $parentChild->count();
        //         $parentChildCount++;
        //         $PtnGroupCode       = $PtnGroupCodeParent."-".$parentChildCount;

        //         $register                       = new Registers;
        //         $register->PtnID                = $PtnID;
        //         $register->CompanyID            = $CompanyID;
        //         $register->PtnGroupCode         = $PtnGroupCode;
        //         $register->ClientName           = $data['name'];
        //         $register->ClientAddress        = $data['address'];
        //         $register->ClientContactNumber  = $data['phone'];
        //         $register->GroupID              = $GroupID;
        //         $register->OrderID              = $parentChildCount;
        //         $register->IsTransactable       = 1;
        //         $register->ActiveStatus         = 1;
        //         $register->save();
        //     }

        //     if($data['contact_type'] == 3)
        //     {
        //         $CompanyID          = Auth()->user()->branch_id;
        //         $PtnID              = 265;

        //         //getting parent's PtnGroupCode
        //         $GroupIdQuery       = Registers::where('id', $PtnID)->get();
        //         $GetGroupId         = isset($GroupIdQuery[0]["GroupID"]) ? $GroupIdQuery[0]["GroupID"] : "";
        //         $PtnGroupCodeParent = isset($GroupIdQuery[0]["PtnGroupCode"]) ? $GroupIdQuery[0]["PtnGroupCode"] : "";
        //         $GroupID            = $GetGroupId + 1;

        //         //getting parent's PtnGroupCode
        //         $parentChild        = Registers::where('PtnID', $PtnID)->get();
        //         $parentChildCount   = $parentChild->count();
        //         $parentChildCount++;
        //         $PtnGroupCode       = $PtnGroupCodeParent."-".$parentChildCount;

        //         $register                       = new Registers;
        //         $register->PtnID                = $PtnID;
        //         $register->CompanyID            = $CompanyID;
        //         $register->PtnGroupCode         = $PtnGroupCode;
        //         $register->ClientName           = $data['name'];
        //         $register->ClientAddress        = $data['address'];
        //         $register->ClientContactNumber  = $data['phone'];
        //         $register->GroupID              = $GroupID;
        //         $register->OrderID              = $parentChildCount;
        //         $register->IsTransactable       = 1;
        //         $register->ActiveStatus         = 1;
        //         $register->save();
        //     }

        //     $cus_up             = Customers::find($data['id']);
        //     $cus_up->client_id  = $register->id;
        //     $cus_up->save();
        // }

        // dd($cus);

        return view('customers::index', compact('customers', 'types', 'departments', 'sections', 'references', 'areas'));
    }

    public function create()
    {
        return view('customers::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            //Insert into clients Table Start
                if($data['contact_type'] == 0)
                {
                    $CompanyID          = Auth()->user()->branch_id;
                    $PtnID              = 263;

                    //getting parent's PtnGroupCode
                    $GroupIdQuery       = Registers::where('id', $PtnID)->get();
                    $GetGroupId         = isset($GroupIdQuery[0]["GroupID"]) ? $GroupIdQuery[0]["GroupID"] : "";
                    $PtnGroupCodeParent = isset($GroupIdQuery[0]["PtnGroupCode"]) ? $GroupIdQuery[0]["PtnGroupCode"] : "";
                    $GroupID            = $GetGroupId + 1;

                    //getting parent's PtnGroupCode
                    $parentChild        = Registers::where('PtnID', $PtnID)->get();
                    $parentChildCount   = $parentChild->count();
                    $parentChildCount++;
                    $PtnGroupCode       = $PtnGroupCodeParent."-".$parentChildCount;

                    $register                       = new Registers;
                    $register->PtnID                = $PtnID;
                    $register->CompanyID            = $CompanyID;
                    $register->PtnGroupCode         = $PtnGroupCode;
                    $register->ClientName           = $data['customer_name'];
                    $register->ClientAddress        = $data['address'];
                    $register->ClientContactNumber  = $data['mobile_number'];
                    $register->GroupID              = $GroupID;
                    $register->OrderID              = $parentChildCount;
                    $register->IsTransactable       = 1;
                    $register->ActiveStatus         = 1;
                    $register->save();
                }

                if($data['contact_type'] == 1)
                {
                    $CompanyID          = Auth()->user()->branch_id;
                    $PtnID              = 264;

                    //getting parent's PtnGroupCode
                    $GroupIdQuery       = Registers::where('id', $PtnID)->get();
                    $GetGroupId         = isset($GroupIdQuery[0]["GroupID"]) ? $GroupIdQuery[0]["GroupID"] : "";
                    $PtnGroupCodeParent = isset($GroupIdQuery[0]["PtnGroupCode"]) ? $GroupIdQuery[0]["PtnGroupCode"] : "";
                    $GroupID            = $GetGroupId + 1;

                    //getting parent's PtnGroupCode
                    $parentChild        = Registers::where('PtnID', $PtnID)->get();
                    $parentChildCount   = $parentChild->count();
                    $parentChildCount++;
                    $PtnGroupCode       = $PtnGroupCodeParent."-".$parentChildCount;

                    $register                       = new Registers;
                    $register->PtnID                = $PtnID;
                    $register->CompanyID            = $CompanyID;
                    $register->PtnGroupCode         = $PtnGroupCode;
                    $register->ClientName           = $data['customer_name'];
                    $register->ClientAddress        = $data['address'];
                    $register->ClientContactNumber  = $data['mobile_number'];
                    $register->GroupID              = $GroupID;
                    $register->OrderID              = $parentChildCount;
                    $register->IsTransactable       = 1;
                    $register->ActiveStatus         = 1;
                    $register->save();
                }

                if($data['contact_type'] == 2)
                {
                    $CompanyID          = Auth()->user()->branch_id;
                    $PtnID              = 49;

                    //getting parent's PtnGroupCode
                    $GroupIdQuery       = Registers::where('id', $PtnID)->get();
                    $GetGroupId         = isset($GroupIdQuery[0]["GroupID"]) ? $GroupIdQuery[0]["GroupID"] : "";
                    $PtnGroupCodeParent = isset($GroupIdQuery[0]["PtnGroupCode"]) ? $GroupIdQuery[0]["PtnGroupCode"] : "";
                    $GroupID            = $GetGroupId + 1;

                    //getting parent's PtnGroupCode
                    $parentChild        = Registers::where('PtnID', $PtnID)->get();
                    $parentChildCount   = $parentChild->count();
                    $parentChildCount++;
                    $PtnGroupCode       = $PtnGroupCodeParent."-".$parentChildCount;

                    $register                       = new Registers;
                    $register->PtnID                = $PtnID;
                    $register->CompanyID            = $CompanyID;
                    $register->PtnGroupCode         = $PtnGroupCode;
                    $register->ClientName           = $data['customer_name'];
                    $register->ClientAddress        = $data['address'];
                    $register->ClientContactNumber  = $data['mobile_number'];
                    $register->GroupID              = $GroupID;
                    $register->OrderID              = $parentChildCount;
                    $register->IsTransactable       = 1;
                    $register->ActiveStatus         = 1;
                    $register->save();
                }

                if($data['contact_type'] == 3)
                {
                    $CompanyID          = Auth()->user()->branch_id;
                    $PtnID              = 265;

                    //getting parent's PtnGroupCode
                    $GroupIdQuery       = Registers::where('id', $PtnID)->get();
                    $GetGroupId         = isset($GroupIdQuery[0]["GroupID"]) ? $GroupIdQuery[0]["GroupID"] : "";
                    $PtnGroupCodeParent = isset($GroupIdQuery[0]["PtnGroupCode"]) ? $GroupIdQuery[0]["PtnGroupCode"] : "";
                    $GroupID            = $GetGroupId + 1;

                    //getting parent's PtnGroupCode
                    $parentChild        = Registers::where('PtnID', $PtnID)->get();
                    $parentChildCount   = $parentChild->count();
                    $parentChildCount++;
                    $PtnGroupCode       = $PtnGroupCodeParent."-".$parentChildCount;

                    $register                       = new Registers;
                    $register->PtnID                = $PtnID;
                    $register->CompanyID            = $CompanyID;
                    $register->PtnGroupCode         = $PtnGroupCode;
                    $register->ClientName           = $data['customer_name'];
                    $register->ClientAddress        = $data['address'];
                    $register->ClientContactNumber  = $data['mobile_number'];
                    $register->GroupID              = $GroupID;
                    $register->OrderID              = $parentChildCount;
                    $register->IsTransactable       = 1;
                    $register->ActiveStatus         = 1;
                    $register->save();
                }
            //Insert into clients Table End

            $customers                      = new Customers;
            $customers->client_id           = $register->id;
            $customers->name                = $data['customer_name'];
            $customers->proprietor_name     = $data['proprietor_name'];
            $customers->employee_id         = $data['employee_id'];
            $customers->serial              = $data['serial'];
            $customers->address             = $data['address'];
            $customers->phone               = $data['mobile_number'];
            $customers->nid_number          = $data['nid_number'];
            $customers->alternative_contact = $data['alternative_mobile_number'];

            if($data['contact_type'] == 2) 
            {
                $customers->father_name     = $data['father_name'];
                $customers->mother_name     = $data['mother_name'];
                $customers->village_name    = $data['village_name'];
                $customers->union_name      = $data['union_name'];
                $customers->thana_name      = $data['thana_name'];
                $customers->district_name   = $data['district_name'];
            }

            $customers->contact_type        = $data['contact_type'];
            $customers->joining_date        = date('Y-m-d', strtotime($data['joining_date']));
            $customers->exemption_date      = isset($data['exemption_date']) ? date('Y-m-d', strtotime($data['joining_date'])) : null;
            $customers->date_of_birth       = isset($data['date_of_birth']) ? date('Y-m-d', strtotime($data['date_of_birth'])) : null;
            $customers->designation         = $data['designation'];
            $customers->salary              = $data['salary'];
            $customers->customer_type       = $data['customer_type'];
            $customers->department_id       = $data['department_id'];
            $customers->section_id          = $data['section_id'];

            if(($data['contact_type'] == 0) || ($data['contact_type'] == 1)) 
            {
                $customers->bank_name          = $data['bank_name'];
                $customers->bank_branch_name   = $data['bank_branch_name'];
                $customers->account_name       = $data['account_name'];
                $customers->account_number     = $data['account_number'];
                $customers->bank_name2         = $data['bank_name2'];
                $customers->bank_branch_name2  = $data['bank_branch_name2'];
                $customers->account_name2      = $data['account_name2'];
                $customers->account_number2    = $data['account_number2'];
                $customers->bank_name3         = $data['bank_name3'];
                $customers->bank_branch_name3  = $data['bank_branch_name3'];
                $customers->account_name3      = $data['account_name3'];
                $customers->account_number3    = $data['account_number3'];
                $customers->bank_name4         = $data['bank_name4'];
                $customers->bank_branch_name4  = $data['bank_branch_name4'];
                $customers->account_name4      = $data['account_name4'];
                $customers->account_number4    = $data['account_number4'];
                $customers->guarantor          = $data['guarantor'];
                $customers->area_id            = $data['area_id'];
            }

            $customers->reference_id        = isset($data['reference_id']) ? $data['reference_id'] : null;
            $customers->branch_id           = $branch_id;

            if($request->hasFile('image'))
            {
                $companyLogo       = $request->file('image');
                $logoName          = time().".".$companyLogo->getClientOriginalExtension();
                $directory         = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl           = $directory.$logoName;
                $customers->image  = $logoUrl;
            }
            
            if($request->hasFile('nid_image'))
            {
                $companyLogo          = $request->file('nid_image');
                $logoName             = time().".".$companyLogo->getClientOriginalExtension();
                $directory            = 'images/customers/NID/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl              = $directory.$logoName;
                $customers->nid_image = $logoUrl;
            }

            $customers->created_by          = $user_id;

            if ($customers->save())
            {
                DB::commit();
 
                return redirect()->route('customers_index','contact_type='.$data['contact_type'])->with("success","Contact Added Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('customers::show');
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $find_customer  = Customers::find($id);
        $customers      = Customers::orderBy('serial', 'ASC')->where('contact_type', $_GET['contact_type'])->get();
        $types          = CustomerTypes::orderBy('created_at', 'DESC')->get();
        $find_type      = CustomerTypes::get();
        $departments    = Departments::orderBy('created_at', 'DESC')->get();
        $sections       = Sections::orderBy('created_at', 'DESC')->get();
        $references     = Customers::where('contact_type', '=', 3)->orderBy('created_at', 'DESC')->get();
        $areas          = Areas::orderBy('created_at', 'ASC')->get();

        return view('customers::edit', compact('customers', 'find_customer', 'types', 'find_type', 'departments', 'sections', 'references', 'areas'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth()->user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers = Customers::find($id);

            //Update clients Table Start
                $register                       = Registers::find($customers['client_id']);
                $register->ClientName           = $data['customer_name'];
                $register->ClientAddress        = $data['address'];
                $register->ClientContactNumber  = $data['mobile_number'];
                $register->save();
            //Update clients Table End

            $customers->name                = $data['customer_name'];
            $customers->proprietor_name     = $data['proprietor_name'];
            $customers->employee_id         = $data['employee_id'];
            $customers->serial              = $data['serial'];
            $customers->address             = $data['address'];
            $customers->phone               = $data['mobile_number'];
            $customers->nid_number          = $data['nid_number'];
            $customers->alternative_contact = $data['alternative_mobile_number'];

            if($data['contact_type'] == 2) 
            {
                $customers->father_name    = $data['father_name'];
                $customers->mother_name    = $data['mother_name'];
                $customers->village_name   = $data['village_name'];
                $customers->union_name     = $data['union_name'];
                $customers->thana_name     = $data['thana_name'];
                $customers->district_name  = $data['district_name'];
            }

            $customers->contact_type    = $data['contact_type'];
            $customers->joining_date    = date('Y-m-d', strtotime($data['joining_date']));
            $customers->exemption_date  = isset($data['exemption_date']) ? date('Y-m-d', strtotime($data['joining_date'])) : null;
            $customers->date_of_birth   = isset($data['date_of_birth']) ? date('Y-m-d', strtotime($data['date_of_birth'])) : null;
            $customers->designation     = $data['designation'];
            $customers->salary          = $data['salary'];
            $customers->customer_type   = $data['customer_type'];
            $customers->department_id   = $data['department_id'];
            $customers->section_id      = $data['section_id'];

            if(($data['contact_type'] == 0) || ($data['contact_type'] == 1)) 
            {
                $customers->bank_name          = $data['bank_name'];
                $customers->bank_branch_name   = $data['bank_branch_name'];
                $customers->account_name       = $data['account_name'];
                $customers->account_number     = $data['account_number'];
                $customers->bank_name2         = $data['bank_name2'];
                $customers->bank_branch_name2  = $data['bank_branch_name2'];
                $customers->account_name2      = $data['account_name2'];
                $customers->account_number2    = $data['account_number2'];
                $customers->bank_name3         = $data['bank_name3'];
                $customers->bank_branch_name3  = $data['bank_branch_name3'];
                $customers->account_name3      = $data['account_name3'];
                $customers->account_number3    = $data['account_number3'];
                $customers->bank_name4         = $data['bank_name4'];
                $customers->bank_branch_name4  = $data['bank_branch_name4'];
                $customers->account_name4      = $data['account_name4'];
                $customers->account_number4    = $data['account_number4'];
                $customers->guarantor          = $data['guarantor'];
                $customers->area_id            = $data['area_id'];
            }

            $customers->reference_id  = isset($data['reference_id']) ? $data['reference_id'] : null;
            $customers->branch_id     = $branch_id;

            if($request->hasFile('image'))
            {
                if ($customers->image != null)
                {
                    unlink('public/'.$customers->image);
                }

                $companyLogo      = $request->file('image');
                $logoName         = time().".".$companyLogo->getClientOriginalExtension();
                $directory        = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl          = $directory.$logoName;
                $customers->image = $logoUrl;
            }
            
            if($request->hasFile('nid_image'))
            {
                if ($customers->nid_image != null)
                {
                    unlink('public/'.$customers->nid_image);
                }

                $companyLogo          = $request->file('nid_image');
                $logoName             = time().".".$companyLogo->getClientOriginalExtension();
                $directory            = 'images/customers/NID/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl              = $directory.$logoName;
                $customers->nid_image = $logoUrl;
            }

            $customers->updated_by = $user_id;

            if ($customers->save())
            {
                DB::commit();

                return redirect()->route('customers_index','contact_type='.$data['contact_type'])->with("success","Contact Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
        $data           = Customers::find($id);
        $data->status   = 0;
        
        if ($data->save())
        {
            return redirect()->route('customers_index', '?contact_type=2')->with("success","Contact Deleted Successfully !!");
        }
        else
        {
            return redirect()->route('customers_index', '?contact_type=2')->with("unsuccess","Something Wrong.Try again. !!");
        }
    }

    public function customerListLoad()
    {
        $branch_id  = Auth::user()->branch_id;
        $data       = Customers::leftjoin('sections', 'sections.id', 'customers.section_id')
                                ->leftjoin('departments', 'departments.id', 'sections.department_id')
                                ->where('customers.branch_id', $branch_id)
                                ->where('customers.status', 1)
                                ->orderBy('customers.serial', 'ASC')
                                ->selectRaw('customers.*, sections.name as section_name, departments.name as department_name')
                                ->get();

        return Response::json($data);
    }

    public function customerListSearch($id)
    {
        $branch_id  = Auth::user()->branch_id;

        if ($id != 'No_Text')
        {
            $data   = Customers::leftjoin('sections', 'sections.id', 'customers.section_id')
                                ->leftjoin('departments', 'departments.id', 'sections.department_id')
                                ->where('customers.name', 'LIKE', "%$id%")
                                ->orWhere('customers.phone', 'LIKE', "%$id%")
                                ->orWhere('customers.alternative_contact', 'LIKE', "%$id%")
                                ->orWhere('customers.address', 'LIKE', "%$id%")
                                ->orWhere('customers.nid_number', 'LIKE', "%$id%")
                                ->where('customers.status', 1)
                                ->orderBy('customers.serial', 'ASC')
                                ->selectRaw('customers.*, sections.name as section_name, departments.name as department_name')
                                ->get();

            $data   = $data->where('branch_id', $branch_id);
        }
        else
        {
            $data   = Customers::leftjoin('sections', 'sections.id', 'customers.section_id')
                                ->leftjoin('departments', 'departments.id', 'sections.department_id')
                                ->where('customers.branch_id', $branch_id)
                                ->where('customers.status', 1)
                                ->orderBy('customers.serial', 'ASC')
                                ->selectRaw('customers.*, sections.name as section_name, departments.name as department_name')
                                ->get();
        }

        return Response::json($data);
    }

    //Customer Types
    public function indexType()
    {
        $types  = CustomerTypes::orderBy('created_at', 'DESC')->get();

        return view('customers::types.index', compact('types'));
    }

    public function storeType(Request $request)
    {
        $rules = array(
            'name'     => 'required',
            'status'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                              = new CustomerTypes;
            $customers->name                        = $data['name'];
            $customers->status                      = $data['status'];
            $customers->created_by                  = $user_id;

            if ($customers->save())
            {   
                DB::commit();
                return back()->with("success","Customer Type Added Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function editType($id)
    {
        $find_type  = CustomerTypes::find($id);
        $types      = CustomerTypes::orderBy('created_at', 'DESC')->get();

        return view('customers::types.edit', compact('types', 'find_type'));
    }

    public function updateType(Request $request, $id)
    {
        $rules = array(
            'name'     => 'required',
            'status'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth()->user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                              = CustomerTypes::find($id);
            $customers->name                        = $data['name'];
            $customers->status                      = $data['status'];
            $customers->updated_by                  = Auth::user()->id;

            if ($customers->save())
            {   
                 DB::commit();
                return redirect()->route('customers_type_index')->with("success","Customer Type Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }
    
    public function addCustomerOpeningBalance()
    {
        $customer_id         = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0; 
        $reference_id        = isset($_GET['reference_id']) ? $_GET['reference_id'] : 0; 
        
        $customers = Customers::where('contact_type',0)
                                ->where('id','!=',1)
                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                    return $query->where('customers.id', $customer_id);
                                })
                                ->when($reference_id != 0, function ($query) use ($reference_id) {
                                    return $query->where('customers.reference_id', $reference_id);
                                })
                                ->orderBy('customers.created_at', 'DESC')
                                ->get();
                                
        return view('customers::add_customer_opening_balance',compact('customers'));
    }    
    
    public function addSupplierOpeningBalance()
    {
        $customer_id         = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0; 
        $reference_id        = isset($_GET['reference_id']) ? $_GET['reference_id'] : 0;  
        
        $customers = Customers::where('contact_type',1)
                                ->where('id','!=',2)
                                ->when($customer_id != 0, function ($query) use ($customer_id) {
                                    return $query->where('customers.id', $customer_id);
                                })
                                ->when($reference_id != 0, function ($query) use ($reference_id) {
                                    return $query->where('customers.reference_id', $reference_id);
                                })
                                ->orderBy('customers.created_at', 'DESC')
                                ->get();
                                
        return view('customers::add_supplier_opening_balance',compact('customers'));
    }
    
    public function updateOpeningBalance(Request $request)
    {
        
        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            
        foreach ($data['opening_balance'] as $key => $value)
            {  
                
                // $invoice                    = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                //                                 ->where('invoices.customer_id', $data['id'][$key])
                //                                 ->where('invoices.type', 2)
                //                                 ->select('invoices.*')
                //                                 ->first();
                
                
                                                
                // if ($invoice != null)
                // {
                //     $invoice->invoice_amount    = $value;
                //     $invoice->updated_by        = Auth::user()->id;
                //     $invoice->save();
        
                //     $find_journal_debit = JournalEntries::where('invoice_id', $invoice['id'])
                //                                         ->where('transaction_head', 'customer-opening-balance')
                //                                         ->where('debit_credit', 1)
                //                                         ->first();
                //     $find_journal_credit= JournalEntries::where('invoice_id', $invoice['id'])
                //                                         ->where('transaction_head', 'customer-opening-balance')
                //                                         ->where('debit_credit', 0)
                //                                         ->first();
        
                 
                //     debitUpdate($find_journal_debit['id'], $customer_id=$data['id'][$key], $date=date('Y-m-d'), $account_id=8, $amount=$value, $note='Customer Opening Balance', $transaction_head='customer-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //     creditUpdate($find_journal_credit['id'], $customer_id=$data['id'][$key], $date=date('Y-m-d'), $account_id=2, $amount=$value, $note='Customer Opening Balance', $transaction_head='customer-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //     customerBalanceUpdate($data['id'][$key]);
                    
                // }
                // else
                // {
                //     $data_find                  = Invoices::orderBy('created_at', 'DESC')->first();
                //     $invoice_number             = $data_find != null ? $data_find['invoice_number'] + 1 : 1;
        
                //     $invoice                    = new Invoices;
                //     $invoice->invoice_number    = $invoice_number;
                //     $invoice->customer_id       = $data['id'][$key];
                //     $invoice->invoice_date      = date('Y-m-d');
                //     $invoice->invoice_amount    = $value;
                //     $invoice->due_amount        = $value;
                //     $invoice->total_buy_price   = 0;
                //     $invoice->total_discount    = 0;
                //     $invoice->type              = 2;
                //     $invoice->branch_id         = $branch_id;
                //     $invoice->created_by        = Auth::user()->id;
                //     $invoice->save();
        
                 
                //     debit($customer_id=$data['id'][$key], $date=date('Y-m-d'), $account_id=8, $amount=$value, $note='Customer Opening Balance', $transaction_head='customer-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //     credit($customer_id=$data['id'][$key], $date=date('Y-m-d'), $account_id=2, $amount=$value, $note='Customer Opening Balance', $transaction_head='customer-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //     customerBalanceUpdate($data['id'][$key]);
                   
                // }
                
                
               
                //     $product_entry                       = Customers::find($data['id'][$key]);
                //     $product_entry->opening_balance        = $value;
                //     $product_entry->updated_by           = $user_id;
                //     $product_entry->save();
                
                
                $bill   = Bills::leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                        ->where('bills.vendor_id', $data['id'][$key])
                        ->where('bills.type', 2)
                        ->select('bills.*')
                        ->first();
                        
                if ($bill != null)
                {
                    $bill->bill_amount          = $value;
                    $bill->updated_by           = Auth::user()->id;
                    $bill->save();
        
                    $find_journal_debit = JournalEntries::where('bill_id', $bill['id'])
                                                        ->where('transaction_head', 'supplier-opening-balance')
                                                        ->where('debit_credit', 1)
                                                        ->first();
                    $find_journal_credit= JournalEntries::where('bill_id', $bill['id'])
                                                        ->where('transaction_head', 'supplier-opening-balance')
                                                        ->where('debit_credit', 0)
                                                        ->first();
        
                    //Financial Accounting Part Start
                    debitUpdate($find_journal_debit['id'], $customer_id=$data['id'][$key], $date=date('Y-m-d'), $account_id=3, $amount=$value, $note='Supplier Opening Balance', $transaction_head='supplier-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    creditUpdate($find_journal_credit['id'], $customer_id=$data['id'][$key], $date=date('Y-m-d'), $account_id=9, $amount=$value, $note='Supplier Opening Balance', $transaction_head='supplier-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    supplierBalanceUpdate($customer_id);
                    //Financial Accounting Part End
                }
                else
                {
                    $data_find                  = Bills::orderBy('created_at', 'DESC')->first();
                    $bill_number                = $data_find != null ? $data_find['bill_number'] + 1 : 1;
        
                    $bill                       = new Bills;
                    $bill->bill_number          = $bill_number;
                    $bill->vendor_id            = $data['id'][$key];
                    $bill->bill_date            = date('Y-m-d');
                    $bill->bill_amount          = $value;
                    $bill->due_amount           = $value;
                    $bill->total_discount       = 0;
                    $bill->type                 = 2;
                    $bill->branch_id            = $branch_id;
                    $bill->created_by           = Auth::user()->id;
                    $bill->save();
        
                    //Financial Accounting Part Start
                    debit($customer_id=$data['id'][$key], $date=date('Y-m-d'), $account_id=3, $amount=$value, $note='Supplier Opening Balance', $transaction_head='supplier-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    credit($customer_id=$data['id'][$key], $date=date('Y-m-d'), $account_id=9, $amount=$value, $note='Supplier Opening Balance', $transaction_head='supplier-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    supplierBalanceUpdate($data['id'][$key]);
                    //Financial Accounting Part End
                }
                
                    $product_entry                       = Customers::find($data['id'][$key]);
                    $product_entry->opening_balance        = $value;
                    $product_entry->updated_by           = $user_id;
                    $product_entry->save();
                
            }
            


            return back()->with("success","Opening Balance Updated Successfully !!");

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }
}
