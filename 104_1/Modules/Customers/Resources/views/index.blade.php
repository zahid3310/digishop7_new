@extends('layouts.app')

<?php
    if (isset($_GET['contact_type']) && ($_GET['contact_type'] == 0))
    {
        $title = 'Customers';
    }

    if (isset($_GET['contact_type']) && ($_GET['contact_type'] == 1))
    {
        $title = 'Suppliers';
    }

    if (isset($_GET['contact_type']) && ($_GET['contact_type'] == 2))
    {
        $title = 'Employees';
    }

    if (isset($_GET['contact_type']) && ($_GET['contact_type'] == 3))
    {
        $title = 'Reference';
    }

    if (!isset($_GET['contact_type']))
    {
        $title = 'Contacts';
    }
?>

@section('title', $title)

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ $title }}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.reference')}}</a></li>
                                    <li class="breadcrumb-item active">Add {{ $title }}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('customers_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group additionalFields">
                                        <label for="example-text-input" class=" col-form-label">Serial</label>
                                        <input name="serial" type="number" class="form-control" placeholder="Enter Serial">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Name *</label>
                                        <input name="customer_name" type="text" class="form-control" placeholder="Enter Name" required>
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group contType">
                                        <label for="example-text-input" class=" col-form-label">Proprietor Name</label>
                                        <input name="proprietor_name" type="text" class="form-control" placeholder="Enter Proprietor Name">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group additionalFields">
                                        <label for="example-text-input" class=" col-form-label">Employee Id</label>
                                        <input name="employee_id" type="text" class="form-control" placeholder="Enter Employee Id">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">{{ __('messages.address')}}</label>
                                        <input name="address" type="text" class="form-control" placeholder="{{ __('messages.address')}}">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">{{ __('messages.mobile_no')}}</label>
                                        <input name="mobile_number" type="number" class="form-control" placeholder="{{ __('messages.mobile_no')}}">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">{{ __('messages.alt_phone')}}</label>
                                        <input name="alternative_mobile_number" type="number" class="form-control" placeholder="{{ __('messages.alt_phone')}}">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">{{ __('messages.nid_number')}}</label>
                                        <input name="nid_number" type="text" class="form-control" placeholder="{{ __('messages.nid_number')}}">
                                    </div>
                                    
                                    @if(isset($_GET['contact_type']) && ($_GET['contact_type'] == 2))
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Father's Name</label>
                                        <input name="father_name" type="text" class="form-control" placeholder="Father's Name">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Mother's Name</label>
                                        <input name="mother_name" type="text" class="form-control" placeholder="Mother's Name">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Village Name</label>
                                        <input name="village_name" type="text" class="form-control" placeholder="Village Name">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Union Name</label>
                                        <input name="union_name" type="text" class="form-control" placeholder="Union Name">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Thana Name</label>
                                        <input name="thana_name" type="text" class="form-control" placeholder="Thana Name">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">District Name</label>
                                        <input name="district_name" type="text" class="form-control" placeholder="District Name">
                                    </div>
                                    @endif

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">{{ __('messages.contact_type')}} *</label>
                                        <select style="cursor: pointer" name="contact_type" class="form-control" id="contact_type" onchange="additionalFields()" required>
                                            @if(isset($_GET['contact_type']) && ($_GET['contact_type'] == 0))
                                            <option value="0">Customers</option>
                                            @endif

                                            @if(isset($_GET['contact_type']) && ($_GET['contact_type'] == 1))
                                            <option value="1">Supplier</option>
                                            @endif

                                            @if(isset($_GET['contact_type']) && ($_GET['contact_type'] == 2))
                                            <option value="2">Employee</option>
                                            @endif

                                            @if(isset($_GET['contact_type']) && ($_GET['contact_type'] == 3))
                                            <option value="3">Reference</option>
                                            @endif

                                            @if(!isset($_GET['contact_type']))
                                            <option value="0">Customers</option>
                                            <option value="1">Supplier</option>
                                            <option value="2">Employee</option>
                                            <option value="3">Reference</option>
                                            @endif
                                        </select>
                                    </div>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group contType">
                                        <label for="example-text-input" class="col-form-label">{{ __('messages.customer_type')}} *</label>
                                        <select style="cursor: pointer" name="customer_type" class="form-control" id="customer_type" required>
                                            @if($types->count() > 0)
                                            @foreach($types as $key => $type)
                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    @if(isset($_GET['contact_type']))
                                        <input type="hidden" name="contact_type_reference" value="{{ $_GET['contact_type'] }}">
                                    @endif
                                    
                                    <div  style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group additionalFields">
                                        <label for="example-text-input" class="col-form-label">Date of Birth</label>
                                        <input id="date_of_birth" name="date_of_birth" type="text"  class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                    </div>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group additionalFields">
                                        <label for="example-text-input" class="col-form-label">{{ __('messages.joining_date')}}</label>
                                        <input id="joining_date" name="joining_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                    </div>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group additionalFields">
                                        <label for="example-text-input" class="col-form-label">{{ __('messages.designation')}}</label>
                                        <input name="designation" type="text" class="form-control" placeholder="{{ __('messages.designation')}}">
                                    </div>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">{{ __('messages.salary')}}</label>
                                        <input name="salary" type="number" class="form-control" placeholder="{{ __('messages.salary')}}">
                                    </div>
                                    
                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group contType">
                                        <label for="example-text-input" class="col-form-label">Reference</label>
                                        <select style="cursor: pointer;width: 100%;" name="reference_id" class="form-control select2" id="reference_id">
                                            <option value="">--Select Reference--</option>
                                            @if(!empty($references) && ($references->count() > 0))
                                                @foreach($references as $key => $reference)
                                                <option value="{{ $reference->id }}">{{ $reference->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    
                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Department</label>
                                        <select style="cursor: pointer" name="department_id" class="form-control" id="department_id">
                                            <option value="">--Select Department--</option>
                                            @if(!empty($departments) && ($departments->count() > 0))
                                                @foreach($departments as $key => $department)
                                                <option value="{{ $department->id }}">{{ $department->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    
                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group additionalFields">
                                        <label for="example-text-input" class="col-form-label">Section</label>
                                        <select style="cursor: pointer" name="section_id" class="form-control" id="section_id">
                                            <option value="">--Select Section--</option>
                                            @if(!empty($sections) && ($sections->count() > 0))
                                                @foreach($sections as $key => $section)
                                                <option value="{{ $section->id }}">{{ $section->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    
                                    @if((isset($_GET['contact_type'])) && ($_GET['contact_type'] != 2))
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Area</label>
                                        <select style="cursor: pointer" name="area_id" class="form-control" id="area_id">
                                            <option value="">--Select Area--</option>
                                            @if(!empty($areas) && ($areas->count() > 0))
                                                @foreach($areas as $key => $area)
                                                <option value="{{ $area->id }}">{{ $area->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    @endif
                                    
                                    <div  style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group additionalFields">
                                        <label for="example-text-input" class="col-form-label">Resign  Date</label>
                                        <input id="exemption_date" name="exemption_date" type="text"  class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                    </div>
                                    
                                    <div  style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group guarantor">
                                        <label for="example-text-input" class="col-form-label">Guarantor</label>
                                        <input id="guarantor" name="guarantor" type="text" class="form-control">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">NID Image</label>
                                        <input name="nid_image" type="file">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">{{ __('messages.image')}}</label>
                                        <input name="image" type="file">
                                    </div>
                                </div>

                                <hr style="margin: 0px !important">

                                <div class="row">
                                    @if((isset($_GET['contact_type'])) && ($_GET['contact_type'] == 0) || (isset($_GET['contact_type'])) && ($_GET['contact_type'] == 1))
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Bank Name 1</label>
                                        <input name="bank_name" type="text" class="form-control"  placeholder="Bank Name 1">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Branch Name 1</label>
                                        <input name="bank_branch_name" type="text" class="form-control" placeholder="Branch Name 1">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Account Name 1</label>
                                        <input name="account_name" type="text" class="form-control"  placeholder="Account Name 1">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Account Number 1</label>
                                        <input name="account_number" type="text" class="form-control"  placeholder="Account Number 1">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Bank Name 2</label>
                                        <input name="bank_name2" type="text" class="form-control"  placeholder="Bank Name 2">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Branch Name 2</label>
                                        <input name="bank_branch_name2" type="text" class="form-control" placeholder="Branch Name 2">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Account Name 2</label>
                                        <input name="account_name2" type="text" class="form-control"  placeholder="Account Name 2">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Account Number 2</label>
                                        <input name="account_number2" type="text" class="form-control"  placeholder="Account Number 2">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Bank Name 3</label>
                                        <input name="bank_name3" type="text" class="form-control"  placeholder="Bank Name 3">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Branch Name 3</label>
                                        <input name="bank_branch_name3" type="text" class="form-control" placeholder="Branch Name 3">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Account Name 3</label>
                                        <input name="account_name3" type="text" class="form-control"  placeholder="Account Name 3">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Account Number 3</label>
                                        <input name="account_number3" type="text" class="form-control"  placeholder="Account Number 3">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Bank Name 4</label>
                                        <input name="bank_name4" type="text" class="form-control"  placeholder="Bank Name 4">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Branch Name 4</label>
                                        <input name="bank_branch_name4" type="text" class="form-control" placeholder="Branch Name 4">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Account Name 4</label>
                                        <input name="account_name4" type="text" class="form-control"  placeholder="Account Name 4">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Account Number 4</label>
                                        <input name="account_number4" type="text" class="form-control"  placeholder="Account Number 4">
                                    </div>
                                    @endif
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">{{ __('messages.save')}}</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('customers_index') }}">{{ __('messages.close')}}</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="card">
                    <div class="card-body table-responsive">
                        <h4 class="card-title">List of {{ ' ' . $title }}</h4>

                        <br>

                        <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    @if((isset($_GET['contact_type'])) && ($_GET['contact_type'] == 0) || (isset($_GET['contact_type'])) && ($_GET['contact_type'] == 1))
                                    <th>Proprietor</th>
                                    @endif
                                    <th>Address</th>
                                    <th>Phone</th>
                                    @if((isset($_GET['contact_type'])) && ($_GET['contact_type'] == 0))
                                    <th>Guarantor</th>
                                    @endif
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @if($customers->count() > 0)
                                @foreach($customers as $key => $customer)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>
                                        @if ($customer->image != null)
                                        <img style="height: 30px;width: 30px" src="{{ url('public/'.$customer->image) }}">
                                        @else
                                        <img style="height: 30px;width: 30px" src="{{ url('public/default.png') }}">
                                        @endif
                                    </td>
                                    @if($customer->contact_type == 2)
                                    <td>
                                        {{ $customer->name }}<br>
                                        <strong>Designation : </strong>{{ $customer->designation }}<br>
                                        <!-- <strong>Department : </strong>{{ $customer->department_id != null ? $customer->department->name : '' }}<br> -->
                                        <strong>Section : </strong>{{ $customer->section_id != null ? $customer->section->name : '' }}<br>
                                        <strong>Machine ID : </strong>{{ $customer->id }}<br>
                                        <strong>Employee ID : </strong>{{ $customer->employee_id }}<br>
                                        <strong>Date Of Birth : </strong>{{ date('d-m-Y', strtotime($customer->joining_date)) }}
                                    </td>
                                    @else
                                    <td>{{ $customer->name }}</td>
                                    @endif
                                    @if($customer->contact_type == 0 || $customer->contact_type == 1) 
                                    <td>{{ $customer->proprietor_name }}</td>
                                    @endif
                                    <td>{{ $customer->address }}</td>
                                    <td>{{ $customer->phone }}</td>
                                    @if((isset($_GET['contact_type'])) && ($_GET['contact_type'] == 0))
                                    <td>{{ $customer->guarantor }}</td>
                                    @endif
                                    <td>
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right" style="">
                                                <a class="dropdown-item" href="{{ route('customers_edit', $customer['id']).'?contact_type='.$_GET['contact_type'] }}">Edit</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url       = $('.site_url').val();
            var contact_type   = $('#contact_type').val();

            if (contact_type == 0)
            {
                $('.AssociativeContactId').hide();
                $('.additionalFields').hide();
                $('.organizationName').show();
                $('.contType').show();
                $('.guarantor').show();
            }

            if (contact_type == 2)
            {
                $('.additionalFields').show();
                $('.AssociativeContactId').show();
                $('.contType').hide();
            }
            
            if (contact_type == 1)
            {
                $('.additionalFields').hide();
                $('.AssociativeContactId').hide();
                $('.organizationName').show();
                $('.contType').show();
            }

            if (contact_type == 3)
            {
                $('.additionalFields').hide();
                $('.AssociativeContactId').hide();
                $('.contType').hide();
            }

            if (contact_type == 4)
            {
                $('.AssociativeContactId').hide();
                $('.additionalFields').hide();
                $('.contType').hide();
            }
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function additionalFields()
        {
            var contact_type   = $('#contact_type').val();

            if (contact_type == 0)
            {
                $('.AssociativeContactId').hide();
                $('.additionalFields').hide();
                $('.organizationName').show();
                $('.contType').show();
            }

            if (contact_type == 2)
            {
                $('.additionalFields').show();
                $('.AssociativeContactId').show();
                $('.contType').hide();
            }
            
            if (contact_type == 1)
            {
                $('.additionalFields').hide();
                $('.AssociativeContactId').hide();
                $('.organizationName').show();
                $('.contType').show();
            }

            if (contact_type == 3)
            {
                $('.additionalFields').hide();
                $('.AssociativeContactId').hide();
                $('.contType').hide();
            }

            if (contact_type == 4)
            {
                $('.AssociativeContactId').hide();
                $('.additionalFields').hide();
                $('.contType').hide();
            }
        }
    </script>
@endsection