<!DOCTYPE html>
<html>

<head>
    <title>Production Profit Loss</title>
    <link rel="icon" href="{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">
</head>

<style type="text/css">
    @page {
        size: auto;   
        margin: 15mm 5mm 5mm 10mm;
    }

    #print-footer {
        display: none;
        position: fixed;
        bottom: 0;
        left:0;
    }

    #hide-print {
        display: none;
    }

    #print-footer {
        display: none;
    }

    body {
        margin-bottom:10mm;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #ddd;
    }

    .table td, .table th {
        font-size: 12px !important;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Production Profit Loss</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">{{__('messages.from_date')}}</th>
                                    <th style="text-align: center">Product</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>{{__('messages.to')}}</strong> {{ $to_date }}</td>
                                    <td style="text-align: center">
                                        @if($production_number != 0)
                                            {{ $production_number }}
                                        @else
                                            {{__('messages.all')}}
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">SL</th>
                                    <th style="text-align: center;width: 10%">Production#</th>
                                    <th style="text-align: center;width: 10%">Date</th>
                                    <th style="text-align: center;width: 10%">Milling Charge(40 Kg)</th>
                                    <th style="text-align: center;width: 10%">Porta(Kg)</th>
                                    <th style="text-align: center;width: 10%">Ratio(Taka)</th>
                                    <th style="text-align: center;width: 10%">Profit</th>
                                    <th style="text-align: center;width: 10%">Loss</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i             = 1;
                                    $total_profit  = 0;
                                    $total_loss    = 0;
                                ?>
                                
                                @if(count($data) > 0)
                                @foreach($data as $key => $value)
                                <tr>
                                    <td style="text-align: center;">{{ $i }}</td>
                                    <td style="text-align: center;">{{ $value['production_number'] }}</td>
                                    <td style="text-align: center;">{{ date('d-m-Y', strtotime($value['date'])) }}</td>
                                    <td style="text-align: right;">{{ $value['milling_charge'] }}</td>
                                    <td style="text-align: right;">{{ $value['porta'] }}</td>
                                    <td style="text-align: right;">{{ $value['resio'] }}</td>
                                    <td style="text-align: right;">{{ $value['profit'] }}</td>
                                    <td style="text-align: right;">{{ $value['loss'] }}</td>
                                </tr>
                                
                                <?php
                                    $i++;
                                    
                                    $total_profit  = $total_profit + $value['profit'];
                                    $total_loss    = $total_loss + $value['loss'];
                                ?>
                                @endforeach
                                @endif
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="6" style="text-align: right;">Total</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_profit }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_loss }}</th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script> -->
<!-- <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script> -->
<!-- <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script> -->

</body>
</html>