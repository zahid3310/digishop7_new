<!DOCTYPE html>
<html>
<head>
    <title>Ledger Book</title>

    <link rel="icon" href="{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/bk_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/bk_assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/bk_assets/css/custom.css') }}">

    <script type="text/javascript">
        function printDiv(divName) {
            var printContents       = document.getElementById(divName).innerHTML;
            var originalContents    = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
</head>

<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 15mm 5mm 5mm 10mm;   /* this affects the margin in the printer settings */
    }
</style>

<body id="print-container-body">
    <input style="float:right" type="button" onclick="printDiv('printableArea')" value="Print" />
    <div id="printableArea">
        <div style="display: none;">
            <button id="btnExport">Export to excel</button>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="" style="">
                            <div style="width:20%;">
                                <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                            </div>

                            <div class="company-head" style="text-align: center; min-height: 82px;">
                                <h3><strong>{{ userDetails()->organization_name }}</strong></h3>
                                <p>{{ userDetails()->address }}</p>
                                <p>{{ userDetails()->contact_number }}</p>
                                <p>{{ userDetails()->contact_email }}</p>
                                <p>{{ userDetails()->website }}</p>
                                <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>              
                            </div>
                            
                            <div class="ibox-title" style="height:25px;">
                              <h6 style="font-size: 13px; margin-top: 0px;">LEDGER BOOK</h6>
                            </div>

                            <div class="ibox-content">
                                <table class="table table-striped table-hover dataTables-example topics" style="margin-bottom: 5px;">
                                    <tbody class="top-header">
                                        <tr>
                                            <td colspan="6" style="border-top: none;">DATE FROM : {{ date('d-m-Y', strtotime($data['from_date'])) . ' To ' . date('d-m-Y', strtotime($data['to_date'])) }}</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                                    <thead class="theight">
                                        <tr>
                                            <th style="width: 5%; text-align: center;">SL#</th>
                                            <th style="width: 10%; text-align: center;">DATE</th>
                                            <th style="width: 15%; text-align: center;">ACCOUNTS HEAD NAME</th>
                                            <th style="width: 15%; text-align: center;">PROJECT</th>
                                            <th style="width: 15%; text-align: center;">REGISTER</th>
                                            <th style="width: 10%; text-align: center;">PARTICULARS</th>
                                            <th style="width: 10%; text-align: center;">VR.NO</th>
                                            <th style="width: 10%; text-align: center;">DEBIT</th>
                                            <th style="width: 10%; text-align: center;">CREDIT</th>
                                        </tr>
                                    </thead>
                                  
                                    <tbody class="theight">
                                        <?php 
                                            $grand_total_debit  = 0; 
                                            $grand_total_credit = 0; 
                                        ?>

                                        @if(count($data) > 0)
                                        @foreach($data['result'] as $key => $data_val)
                                        <tr style="background-color: #F5F5F6">
                                            <th style="text-align: left;font-weight: bold;" colspan="9">{{ 'Accounts Head Name >> ' . $key }}</th>
                                        </tr>

                                        <tr class="gradeC">
                                            <td style="text-align: right;" colspan="7">Opening Balance =</td>
                                            <td style="text-align: right;" colspan="2">{{ number_format(0,2,'.',',') }}</td>
                                        </tr>

                                        <?php 
                                            $j                  = 1; 
                                            $sub_total_debit    = 0; 
                                            $sub_total_credit   = 0; 
                                        ?>
                                        
                                        @foreach($data_val as $key1 => $data_val1)
                                        <tr class="gradeC">
                                            <td style="text-align: center;">{{ $j }}</td>
                                            <td style="text-align: center;">{{ date('d-m-Y', strtotime($data_val1->VoucherDate)) }}</td>
                                            <td style="text-align: left;">{{ $data_val1->cbaHeadName->HeadName }}</td>
                                            <td style="text-align: left;">{{ $data_val1->ProjectID != null ? $data_val1->projectName->ProjectName : '' }}</td>
                                            <td style="text-align: left;">{{ $data_val1->RegisterID != null ?  $data_val1->registerName->ClientName : '' }}</td>
                                            <td style="text-align: left;">{{ $data_val1->Particulars }}</td>
                                            <td style="text-align: center;">{{ $data_val1->VoucherType. '/' . $data_val1->VoucherNumber }}</td>
                                            <td style="text-align: right;">{{ number_format($data_val1->DebitAmount,0,'.',',') }}</td>
                                            <td style="text-align: right;">{{ number_format($data_val1->CreditAmount,0,'.',',') }}</td>
                                        </tr>

                                        <?php 
                                            $j++; 
                                            $sub_total_debit    = $sub_total_debit + $data_val1->DebitAmount; 
                                            $sub_total_credit   = $sub_total_credit + $data_val1->CreditAmount; 
                                        ?>
                                        @endforeach

                                        <tr>
                                            <th style="text-align: right;font-weight: bold;" colspan="7">SUB TOTAL =</th>
                                            <th style="text-align: right;font-weight: bold;">{{ number_format($sub_total_debit,0,'.',',') }}</th>
                                            <th style="text-align: right;font-weight: bold;">{{ number_format($sub_total_credit,0,'.',',') }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right;font-weight: bold;" colspan="7">SUB TOTAL(With Opening Balance) =</th>
                                            <th style="text-align: right;font-weight: bold;">{{ number_format($sub_total_debit,0,'.',',') }}</th>
                                            <th style="text-align: right;font-weight: bold;">{{ number_format($sub_total_credit,0,'.',',') }}</th>
                                        </tr>
                                        
                                        <?php 
                                            $grand_total_debit  = $grand_total_debit + $sub_total_debit; 
                                            $grand_total_credit = $grand_total_credit + $sub_total_credit; 
                                        ?>
                                        @endforeach
                                        @endif
                                        
                                    </tbody>
                                
                                    <tfoot class="tfheight">
                                        <tr style="background-color: #F5F5F6">
                                            <th style="text-align: right;font-weight: bold;" colspan="7">GRAND TOTAL =</th>
                                            <th style="text-align: right;">{{ number_format($grand_total_debit,0,'.',',') }}</th>
                                            <th style="text-align: right;">{{ number_format($grand_total_credit,0,'.',',') }}</th>
                                        </tr>
                                    </tfoot>
                                </table>

                                <table class="table table-striped table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <td style="font-size:8px;">&copy; <?php echo date("Y"); ?> <a style="color: black" target="blank" href="http://www.cyberdynetechnologyltd.com">Cyberdyne Technology Ltd. | Contact: 01715-515755 </a></td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ url('public/bk_assets/js/jquery-2.1.1.js') }}"></script>
        <script src="{{ url('public/bk_assets/js/jquery.btechco.excelexport.js') }}"></script>
        <script src="{{ url('public/bk_assets/js/jquery.base64.js') }}"></script>

        <script type="text/javascript">
            var isCtrl = false;$(document).keyup(function (e) {
            if(e.which == 17) isCtrl=false;
            }).keydown(function (e) {
                if(e.which == 17) isCtrl=true;
                if(e.which == 69 && isCtrl == true) {
                    $("#tblExport").btechco_excelexport({
                        containerid: "tblExport"
                       , datatype: $datatype.Table
                    });
                    return false;
                }
            });
        </script>
    </div>
</body>
</html>