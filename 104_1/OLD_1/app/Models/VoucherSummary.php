<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class VoucherSummary extends Model
{  
    protected $table = "tbl_acc_voucher_summary";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function accountTransactions()
    {
        return $this->hasMany(AccountTransaction::class, "VoucherId");
    }
}
