<!DOCTYPE html>
<html>

<head>
    <title>Supplier Ledger Details</title>
    <link rel="icon" href="{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-top:5mm;
            margin-bottom:5mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>  
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: center;line-height: .8">
                        <p style="font-size: 40px"><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p style="font-size: 20px">{{ $user_info['address'] }}</p>
                        <p style="font-size: 20px">{{ $user_info['contact_number'] }}</p>
                        <p style="font-size: 20px">{{ $user_info['contact_email'] }}</p>
                        <p style="font-size: 14px;text-align: right">{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Supplier Ledger Details</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center;font-size: 12px !important">{{ __('messages.from_date')}}</th>
                                    <th style="text-align: center;font-size: 12px !important">Supplier Name</th>
                                    <th style="text-align: center;font-size: 12px !important">Proprietor Name</th>
                                    <th style="text-align: center;font-size: 12px !important">Address</th>
                                    <th style="text-align: center;font-size: 12px !important">{{ __('messages.phone')}}</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center;font-size: 12px !important">{{ $from_date }} <strong>{{ __('messages.to_date')}}</strong> {{ $to_date }}</td>
                                    
                                    <td style="text-align: center;font-size: 12px !important">
                                        @if($customer_name != null)
                                            {{ $customer_name['ClientName'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>

                                    <td style="text-align: center;font-size: 12px !important">
                                        @if($customer_name != null)
                                        @else
                                            ALL
                                        @endif
                                    </td>

                                    <td style="text-align: center;font-size: 12px !important">
                                        @if($customer_name != null)
                                            {{ $customer_name['ClientAddress'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                    
                                    <td style="text-align: center;font-size: 12px !important">
                                        @if($customer_name != null)
                                            {{ $customer_name['ClientContactNumber'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 7%;vertical-align: bottom">{{ __('messages.date')}}</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">{{ __('messages.trans')}}#</th>
                                    <th style="text-align: center;width: 8%">{{ __('messages.description')}}</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">Account Head</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom">{{ __('messages.debit')}}</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom">{{ __('messages.credit')}}</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom">{{ __('messages.balance')}}</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr>
                                    <td style="text-align: right;font-size: 12px !important" colspan="6"><strong>{{ __('messages.opening_balance')}}</strong></a>
                                    </td>
                                    <td style="text-align: right;font-size: 12px !important;" colspan="1"><strong>{{  number_format($opening_balance,0,'.',',') }}</strong></td>
                                </tr>

                                <?php
                                    $i           = 1;
                                    $sub_total   = $opening_balance;
                                ?>
                                @if($data->count() > 0)
                                @foreach($data as $key => $value)
                                @if((($value->VoucherType == 'PURCHASE') && ($value->HeadID == '1322')) || 
                                    (($value->VoucherType == 'BR') && ($value->PurposeAccHeadName != 'others')) || 
                                    (($value->VoucherType == 'BP') && ($value->PurposeAccHeadName != 'others')) || 
                                    (($value->VoucherType == 'JR') && ($value->PurposeAccHeadName != 'others')) ||
                                    (($value->VoucherType == 'CR') && ($value->PurposeAccHeadName != 'others')) || 
                                    (($value->VoucherType == 'CP') && ($value->PurposeAccHeadName != 'others')) || 
                                    (($value->VoucherType == 'PR') && ($value->HeadID == '1324'))
                                    )
                                <?php
                                    $sub_total = $sub_total + $value->DebitAmount - $value->CreditAmount;

                                    if ($value['VoucherType'] == 'SALES')
                                    {
                                        $trans_number       = 'INV - '.str_pad($value->invoice->invoice_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = $value->headName->HeadName;
                                        $transaction_note   = $value->invoice->invoice_note;
                                        $register           = $value->registerName->ClientName;
                                        $text               = 'Item Sales To ';
                                    }
                                    elseif ($value['VoucherType'] == 'PURCHASE')
                                    {
                                        $trans_number       = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = $value->headName->HeadName;
                                        $transaction_note   = $value->bill->bill_note;
                                        $register           = $value->registerName->ClientName;
                                        $text               = 'Item Purchase From ';
                                    }
                                    elseif ($value['VoucherType'] == 'SR')
                                    {
                                        $trans_number       = 'SR - '.str_pad($value->salesReturn->sales_return_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = $value->headName->HeadName;
                                        $transaction_note   = $value->return_notev;
                                        $register           = $value->registerName->ClientName;
                                        $text               = 'Return Back From ';
                                    }
                                    elseif ($value['VoucherType'] == 'PR')
                                    {
                                        $trans_number       = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = $value->headName->HeadName;
                                        $transaction_note   = $value->return_note;
                                        $register           = $value->registerName->ClientName;
                                        $text               = 'Return Back To ';
                                    }
                                    else
                                    {
                                        $trans_number       = $value->voucherNumber->Type . '/' .$value->voucherNumber->VoucherNumber;
                                        $transaction_head   = $value->headName->HeadName;
                                        $register           = $value->registerName->ClientName;
                                        $transaction_note   = $value->voucherNumber->Narration;
                                        $text               = '';
                                    } 
                                ?>

                                <tr>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" >{{ date('d-m-Y', strtotime($value['VoucherDate'])) }}</a>
                                    </td>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle">{{ $trans_number }}</td>
                                    <td style="text-align: left;font-size: 12px !important;vertical-align: middle">{{ $transaction_note }}</td>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle">{{ $transaction_head }}</td>
                                    <td style="text-align: right;font-size: 12px !important;vertical-align: middle">{{ number_format($value['DebitAmount'],0,'.',',') }}</td>
                                    <td style="text-align: right;font-size: 12px !important;vertical-align: middle">{{ number_format($value['CreditAmount'],0,'.',',') }}</td>
                                    <td style="text-align: right;font-size: 12px !important;vertical-align: middle">{{ number_format($sub_total,0,'.',',') }}</td>
                                </tr>
                             
                            <?php $i++; ?>
                                
                            @endif
                            @endforeach
                            @endif
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="6" style="text-align: right;font-size: 12px !important">{{ __('messages.total')}}</th>
                                    <th colspan="1" style="text-align: right;font-size: 12px !important">{{ number_format($sub_total,0,'.',',') }}</th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>