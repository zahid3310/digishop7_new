@extends('layouts.app')

@section('title', 'Print Pos')

@if($user_info['printer_type'] == 0)
<style type="text/css">
    @media print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 60mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }
    }
</style>
@endif

@if($user_info['printer_type'] == 1)
<style type="text/css">
    @media print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 90mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }
    }
</style>
@endif

@if($user_info['printer_type'] == 2 || $user_info['printer_type'] == 3)
<style type="text/css">
    @media print {
        #footerInvoice {
            position: fixed;
            bottom: 60;
        }
        
    .border-none{
        border-bottom: 1px solid #fff!important;
        border-left: 1px solid #fff!important;
        border-right: 1px solid #fff!important;
        border-top: 1px solid #fff!important;
    }
    
    .border-none-2{
        border-bottom: 1px solid #fff!important;
        border-left: 1px solid #fff!important;
        border-top: 1px solid #fff!important;
    }
        
        
    }

    .textstyle {
        background-color: white; /* Changing background color */
        font-weight: bold; /* Making font bold */
        border-radius: 20px; /* Making border radius */
        border: 3px solid black; /* Making border radius */
        width: auto; /* Making auto-sizable width */
        height: auto; /* Making auto-sizable height */
        padding: 5px 10px 5px 10px; /* Making space around letters */
        font-size: 18px; /* Changing font size */
    }

    .column-bordered-table thead td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table tfoot tr {
        border-top: 1px solid #c3c3c3;
        border-bottom: 1px solid #c3c3c3;
    }

    table,th {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 2px;
        font-weight: bold;
        font-size: 18px
    }

   /* td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        padding: 2px;
        font-weight: bold;
        font-size: 18px
    }*/

    td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        /*padding: 2px;*/
        font-weight: bold;
        font-size: 16px
    }

     table, th, td {
  border: 1px solid black!important;
}

    @page {
        size: A4;
        page-break-after: always;
    }
</style>
@endif

@section('content')
    <div class="main-content marginTopPrint">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row d-print-none">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.sales')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.sales')}}</a></li>
                                    <li class="breadcrumb-item active">Print Pos</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                @if($user_info['printer_type'] == 0)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div id="hedColor" style="margin-top: 0px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight: bold">{{ $user_info['organization_name'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold">{{ $user_info['address'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 18px;font-weight: bold">{{ $user_info['contact_number'] }}</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 18px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">Invoice</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 14px">Date : {{ date('d/m/Y', strtotime($invoice['invoice_date'])) }}</span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 14px">{{ date('h:i a', strtotime(now())) }}</span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 14px">Invoice# : {{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }}</span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">
                                    
                                </div>

                                <div style="border-top: 2px dashed black;border-bottom: 2px dashed black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px;" class="col-2"><strong>Qt</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-4"><strong>Item</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Price</strong></div>
                                    <div style="font-size: 12px;text-align: right;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Total</strong></div>
                                </div>

                                @if(!empty($entries) && ($entries->count() > 0))

                                    <?php 
                                        $sub_total              = 0; 
                                        $total_discount         = 0;
                                    ?>

                                    @foreach($entries as $key => $value)

                                    <div style="padding-bottom: 10px;" id="DetailsFontSize" class="row">

                                        <?php 
                                            $discount           = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; 
                                            $total_discount     = $total_discount + $discount;
                                        ?>

                                        <div style="font-size: 12px" class="col-2">
                                            {{ $value['quantity'] }} <br> {{ $value->convertedUnit->name }}
                                        </div>

                                        <div style="font-size: 12px" class="col-4">
                                            @if($value['product_type'] == 1)
                                                <?php echo $value['product_entry_name']; ?>
                                            @else
                                                <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                            @endif
                                        </div>
                                        <div style="font-size: 12px" class="col-3">
                                            {{ number_format($value['rate']) }}
                                        </div>
                                        <div style="font-size: 12px;text-align: right" class="col-3">{{ number_format($value['total_amount']) }}</div>
                                    </div>

                                    <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                    @endforeach
                                @endif

                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>GROSS Total :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong>{{ number_format($sub_total) }}</strong></div>
                                </div>

                                <!-- <div style="padding-bottom: 10px" class="row"></div> -->

                                <?php
                                    $total_vat_amount       = $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : $invoice['total_vat']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>Total VAT :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong>{{ number_format($total_vat_amount) }}</strong></div>
                                </div>

                                <?php
                                    $total_discount_amount       = $invoice['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$invoice['total_vat'])/100))*$invoice['total_discount_amount'])/100) : $invoice['total_discount_amount']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>Total Discount :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong>{{ number_format($total_discount + $total_discount_amount) }}</strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;" class="row">
                                    <div style="font-size: 14px" class="col-6"><strong>NET Total :</strong></div>
                                    <div style="font-size: 14px;text-align: right" class="col-6"><strong>{{ number_format($sub_total + $total_vat_amount - $total_discount_amount) }}</strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px;" class="row"></div> -->
                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px;" class="row"></div> -->

                               <!--  <div class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-12">Payments</div>
                                </div> -->

                                <div style="padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 14px;font-weight: bold" class="col-6">Total Paid</div>
                                    <div style="font-size: 14px;text-align: right;font-weight: bold" class="col-6">{{ number_format($invoice['invoice_amount'] - $invoice['due_amount']) }}</div>
                                </div>

                                <!-- <div style="padding-left: 15px;border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 16px" class="col-6">-TOTAL DUES</div>
                                    <div style="font-size: 16px;text-align: right" class="col-6">{{ number_format($invoice['due_amount'],2,'.',',') }}</div>
                                </div> -->

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 12px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if($user_info['printer_type'] == 1)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div id="hedColor" style="margin-top: 0px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 26px;font-weight: bold">{{ $user_info['organization_name'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight: bold">{{ $user_info['address'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 20px;font-weight: bold">{{ $user_info['contact_number'] }}</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 20px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">Invoice</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 16px">Date : {{ date('d/m/Y', strtotime($invoice['invoice_date'])) }}</span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 16px">Time : {{ date('h:i a', strtotime(now())) }}</span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 16px">Invoice# : {{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }}</span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">
                                    
                                </div>

                                <div style="border-top: 2px dashed black;border-bottom: 2px dashed black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-2"><strong>Qty</strong></div>
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-4"><strong>Item Name</strong></div>
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Price</strong></div>
                                    <div style="font-size: 16px;text-align: right;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>T.Price</strong></div>
                                </div>

                                @if(!empty($entries) && ($entries->count() > 0))

                                    <?php 
                                        $sub_total              = 0; 
                                        $total_discount         = 0;
                                    ?>

                                    @foreach($entries as $key => $value)

                                    <div style="padding-bottom: 10px;" id="DetailsFontSize" class="row">

                                        <?php 
                                            $discount           = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; 
                                            $total_discount     = $total_discount + $discount;
                                        ?>

                                        <div style="font-size: 16px" class="col-2">
                                            {{ $value['quantity'] }} <br> {{ $value->convertedUnit->name }}
                                        </div>

                                        <div style="font-size: 16px" class="col-4">
                                            @if($value['product_type'] == 1)
                                                <?php echo $value['product_entry_name']; ?>
                                            @else
                                                <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                            @endif
                                        </div>
                                        <div style="font-size: 16px" class="col-3">
                                            {{ number_format($value['rate']) }}
                                        </div>
                                        <div style="font-size: 16px;text-align: right" class="col-3">{{ number_format($value['total_amount']) }}</div>
                                    </div>

                                    <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                    @endforeach
                                @endif

                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>GROSS Total :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong>{{ number_format($sub_total) }}</strong></div>
                                </div>

                                <!-- <div style="padding-bottom: 10px" class="row"></div> -->

                                <?php
                                    $total_vat_amount       =  $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : $invoice['total_vat']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>Total VAT :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong>{{  number_format($total_vat_amount) }}</strong></div>
                                </div>

                                <?php
                                    $total_discount_amount       = $invoice['total_discount_type'] == 0 ? ((($sub_total + $total_vat_amount)*$invoice['total_discount_amount'])/100) : $invoice['total_discount_amount']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>Total Discount :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong>{{ number_format($total_discount + $total_discount_amount) }}</strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;" class="row">
                                    <div style="font-size: 20px" class="col-6"><strong>NET Total :</strong></div>
                                    <div style="font-size: 20px;text-align: right" class="col-6"><strong>{{ number_format($sub_total + $total_vat_amount - $total_discount_amount) }}</strong></div>
                                </div>

                                <!-- <div style="border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 20px" class="col-12"><strong>VAT Included</strong></div>
                                </div> -->

                                <!-- <div style="padding-top: 10px;" class="row"></div> -->
                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px;" class="row"></div> -->

                               <!--  <div class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-12">Payments</div>
                                </div> -->

                                <div style="padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-6">Total Paid</div>
                                    <div style="font-size: 18px;text-align: right;font-weight: bold" class="col-6">{{ number_format($invoice['invoice_amount'] - $invoice['due_amount']) }}</div>
                                </div>

                                <!-- <div style="padding-left: 15px;border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 16px" class="col-6">-TOTAL DUES</div>
                                    <div style="font-size: 16px;text-align: right" class="col-6">{{ number_format($invoice['due_amount'],2,'.',',') }}</div>
                                </div> -->

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 16px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if($user_info['printer_type'] == 2 || $user_info['printer_type'] == 3)
                    <div style="padding: 10px;padding-top: 25px" class="row">
                        <div style="padding-bottom: 30px" class="d-print-none col-md-12">
                            <div class="float-right">
                                <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                            </div>
                        </div>

                        <div  class="col-12" style="height: 100vh;">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                            <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                                <img style="width: 40px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                                <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 32px;padding-top: 10px;font-weight: bold">সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</h2>
                                                <p style="line-height: 1.2;font-size: 20px;font-weight: bold" class="text-center">{{ $user_info['organization_name'] }}</p>
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                                {{ QrCode::size(70)->generate("string") }}
                                            </div>
                                        </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px">অত্যাধুনিক স্বয়ংক্রিয় মেশিনে উন্নত ধান থেকে উন্নতমানের বিভিন্ন প্রকার চাউল উৎপাদন ও সরবরাহ ক্রা হয় ।  </p>
                                                <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">বাঁকা ব্রিক্স ফিল্ড, জীবননগর, চুয়াডাঙ্গা । ০১৬৮৮ ৯৩৯৩৯৩, ০১৯১৬ ০৪১১২৯১, ০১৭১৫ ৭০৪১২৬</p>
                                                <p style="line-height: 3;text-align: center"><span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 20px">বিক্রয় চালান / বিল</span>
                                                    <span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 15px;float: right">গ্রাহক কপি</span>

                                                 <span style="font-size: 15px;float: left">{{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }}</span>
                                        </div>
                                    </div>

                                    <hr style="margin: 5px !important">

                                    <div class="row" style="line-height: 25px;">
                                        <div style="font-size: 18px" class="col-md-7">
                                          <strong>প্রতিষ্ঠানের নাম  - {{ $invoice['customer_name'] != null ? $invoice['customer_name'] : $invoice->customer->customer_name}} </strong>
                                        </div>

                                        <div style="font-size: 18px" class="col-md-5 text-sm-right">
                                           
                                                <strong>তারিখ: </strong>
                                                <span style="font-weight: bold; color: green;">
                                                    {{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}
                                                </span>    
                                        </div>

                                        <div style="font-size: 18px" class="col-md-12">
                                           
                                                <span style="font-weight: bold">প্রাপকের নাম:  </span><span style="font-weight: bold; color: red;">{{ $invoice['proprietor_name'] }} </span><br>
                                                <span style="font-weight: bold">মোবাইল নং: </span><strong>{{ $invoice['customer_phone'] != null ? $invoice['customer_phone'] : $invoice->customer->ClientContactNumber }}</strong>     
                                        </div>
                                    </div>

                                    <div style="padding-top: 0px;padding-bottom: 0px">
                                        <table style="width: 100%;">
                                            <tr>
                                                <th colspan="2" style="font-size: 23px;width: 50%;text-align: center">মালের বিবরণ</th>
                                                <th style="font-size: 18px;width: 15%;text-align: center">দর</th>
                                                <th style="font-size: 18px;width: 10%;text-align: center">পরিমাণ</th>
                                                <th style="font-size: 18px;width: 20%;text-align: center">মোট টাকা</th>
                                            </tr>

                                            @if($entries->count() > 0)

                                            <?php
                                            $total_amount                   = 0;
                                            ?>

                                            @foreach($entries as $key => $value)
                                            <?php
                                                $total_amount   = $total_amount + ($value['total_amount']);
                                                $variation_name = ProductVariationName($value['product_entry_id']);

                                                if ($value['product_code'] != null)
                                                {
                                                    $productCode  = ' - '.$value['product_code'];
                                                }
                                                else
                                                {
                                                    $productCode  = '';
                                                }

                                                if ($value['product_name'] != null)
                                                {
                                                    $category  = ' - '.$value['product_name'];
                                                }
                                                else
                                                {
                                                    $category  = '';
                                                }

                                                if ($value['brand_name'] != null)
                                                {
                                                    $brandName  = $value['brand_name'];
                                                }
                                                else
                                                {
                                                    $brandName  = '';
                                                }

                                                if ($value['unit_name'] != null)
                                                {
                                                    $unit  = ' '.$value['unit_name'];
                                                }
                                                else
                                                {
                                                    $unit  = '';
                                                }

                                                if ($variation_name != null)
                                                {
                                                    $variation  = ' '.$variation_name;
                                                }
                                                else
                                                {
                                                    $variation  = '';
                                                }

                                                $pre_dues = $invoice['previous_due'];
                                                $net_paya = round($total_amount, 2);
                                                $paid     = round($invoice['cash_given'], 2);
                                                $dues     = round($net_paya - $paid, 2);
                                            ?>



                                            <tr class="tr-height">
                                                <td @if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6) style="padding-left: 30px;color: red" @endif colspan="2" style="padding-left: 30px">{{ $value['product_entry_name'] . $variation }}</td>
                                                <td @if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6) style="text-align: center;color: red" @endif style="text-align: center">{{$value['rate'] }}</td>
                                                <td @if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6) style="text-align: center;color: red" @endif style="text-align: center">{{ $value['quantity'] . $unit }}</td>
                                                <td @if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6) style="text-align: right;color: red" @endif style="text-align: right">{{ round($value['total_amount'], 2) }}</td>
                                            </tr>
                                            @endforeach
                                            @endif

                                            <?php
                                                if ($invoice['vat_type'] == 0)
                                                {
                                                    $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                                }
                                                else
                                                {
                                                    $vat_amount  = $invoice['total_vat'];
                                                }

                                                if ($invoice['total_discount_type'] == 0)
                                                {
                                                    $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                                }
                                                else
                                                {
                                                    $discount_on_total_amount  = $invoice['total_discount_amount'];
                                                }
                                            ?>

                                            <tr>
                                                <th style="text-align: left;text-align: right;color: #e50606;" colspan="4"><strong>সর্বমোট</strong></th>
                                                <th style="text-align: right;font-weight: bold; color: green;">{{ $net_paya != 0 ? round($net_paya - $invoice['total_discount']) : '' }}</th>
                                            </tr>

                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong>মোট পরিমাণে ছাড়({{ $invoice['total_discount_type'] == 0 ? $invoice['total_discount_amount'].'%' : 'BDT' }})</strong></th>
                                                <th style="text-align: right">{{ round($discount_on_total_amount) }}</th>
                                            </tr>
                                            
                                            

                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong>পূর্বের জের</strong></th>
                                                <th style="text-align: right">{{ $pre_dues != 0 ? round($pre_dues) : '' }}</th>
                                            </tr>
                                            

                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong style="color: red;">মোট  </strong></th>
                                                <th style="text-align: right;font-weight: bold; color: red;">{{ $net_paya - $invoice['total_discount'] + $pre_dues  - $discount_on_total_amount  != 0 ? round($net_paya - $invoice['total_discount'] + $pre_dues  - $discount_on_total_amount) : '' }}</th>
                                            </tr>

                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong>জমা</strong></th>
                                                <th style="text-align: right">{{$invoice['cash_given']}}</th>
                                            </tr>

                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong>মোট বাকি</strong></th>
                                                <th style="text-align: right">{{ ($net_paya - $invoice['total_discount'] + $pre_dues != 0 ? round($net_paya - $invoice['total_discount'] - $discount_on_total_amount + $pre_dues) : '') -$invoice['cash_given'] }} </th>
                                            </tr>
                                            


                                                <tr>
                                                    <th style="text-align: left" colspan="6">গাড়ি নং : </strong>{{ $invoice->vichele_id != null ? $invoice->vichele_id : '' }}</th>
                                                    <!--<th style="text-align: center" colspan="2">মাল আনলোডের স্থান</th>-->
                                                </tr>

                                                <tr>
                                                    <th style="text-align: left" colspan="6">ড্রাইভারের নাম : </strong>{{ $invoice['driver_name'] }}</th>
                                                    <!--<th style="text-align: center" colspan="2" rowspan="3"></th>-->
                                                </tr>

                                                <tr>
                                                    <th style="text-align: left" colspan="6"><strong>ড্রাইভারের মোবা : </strong>{{ $invoice['driver_phone'] }}</th>
                                                </tr>

                                                <tr>
                                                    <th style="text-align: left" colspan="6"><strong>গাড়ি ভাড়া : </strong>{{ $invoice['rent'] }}</th>
                                                </tr>
                                                
                                                <tr>
                                                    <th style="text-align: left;" colspan="6"><strong> মাল লোডের স্থান : {{ $invoice['unload_place_name'] }}</strong></th>
                                                </tr>

                                           <!--  <tr>
                                                <th class="border-none"></th>
                                                <th class="border-none-2"></th>
                                                <th style="text-align: right"><strong>Other Paid</strong></th>
                                                <th colspan="3" style="text-align: right"></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none"></th>
                                                <th class="border-none-2"></th>
                                                <th style="text-align: right"><strong>Current Balance</strong></th>
                                                <th colspan="3" style="text-align: right"></th>
                                            </tr> -->
                                        </table>
                                    </div>

                                        <br>
                                        <br>
                                        <br>

                                    <div class="row">
                                            <div class="col-md-3">
                                                <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">লোড ম্যানেজার </span> </h6>
                                            </div>
                                            <div class="col-md-3">
                                                <h6 style="text-align: center"> <span style="border-top: 1px dotted black;padding: 5px">কম্পিউটার অপারেটর</span> </h6>
                                            </div>
                                            <div class="col-md-3">
                                                <h6 style="text-align: center"> <span style="border-top: 1px dotted black;padding: 5px">জেনারেল ম্যানেজার</span> </h6>
                                            </div>
                                            <div class="col-md-3">
                                                <h6 style="text-align: right"> <span style="padding: 5px">পক্ষে : সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</span> </h6>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>


                        <div  class="col-12" style="height: 100vh;">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                            <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                                <img style="width: 40px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                                <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 32px;padding-top: 10px;font-weight: bold">সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</h2>
                                                <p style="line-height: 1.2;font-size: 20px;font-weight: bold" class="text-center">{{ $user_info['organization_name'] }}</p>
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                                {{ QrCode::size(70)->generate("string") }}
                                            </div>
                                        </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px">অত্যাধুনিক স্বয়ংক্রিয় মেশিনে উন্নত ধান থেকে উন্নতমানের বিভিন্ন প্রকার চাউল উৎপাদন ও সরবরাহ ক্রা হয় ।  </p>
                                                <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">বাঁকা ব্রিক্স ফিল্ড, জীবননগর, চুয়াডাঙ্গা । ০১৬৮৮ ৯৩৯৩৯৩, ০১৯১৬ ০৪১১২৯১, ০১৭১৫ ৭০৪১২৬</p>
                                                <p style="line-height: 3;text-align: center"><span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 20px">বিক্রয় চালান / বিল</span>
                                                    <span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 15px;float: right">লেবার কপি</span>

                                                 <span style="font-size: 15px;float: left">{{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }}</span>
                                        </div>
                                    </div>

                                    <hr style="margin: 5px !important">

                                    <div class="row" style="line-height: 25px;">
                                        <div style="font-size: 18px" class="col-md-7">
                                          <strong>প্রতিষ্ঠানের নাম  - {{ $invoice['customer_name'] != null ? $invoice['customer_name'] : $invoice->customer->customer_name}} </strong>
                                        </div>
                                        <div style="font-size: 18px" class="col-md-5 text-sm-right">
                                           
                                                <strong>তারিখ: </strong>
                                                <span style="font-weight: bold; color: green;">
                                                    {{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}
                                                </span>    
                                        </div>

                                        <div style="font-size: 18px" class="col-md-12">
                                           
                                               <span style="font-weight: bold">প্রাপকের নাম:  </span><span style="font-weight: bold; color: red;">{{ $invoice['proprietor_name'] }} </span><br>
                                                <span style="font-weight: bold">মোবাইল নং: </span><strong>{{ $invoice['customer_phone'] != null ? $invoice['customer_phone'] : $invoice->customer->ClientContactNumber }}</strong>     
                                        </div>
                                    </div>

                                    <div style="padding-top: 0px;padding-bottom: 0px">
                                        <table style="width: 100%;">
                                            <tr>
                                                <th colspan="2" style="font-size: 23px;width: 50%;text-align: center">মালের বিবরণ</th>
                                                <th style="font-size: 18px;width: 15%;text-align: center">দর</th>
                                                <th style="font-size: 18px;width: 10%;text-align: center">পরিমাণ</th>
                                                <th style="font-size: 18px;width: 20%;text-align: center">মোট টাকা</th>
                                            </tr>

                                            @if($entries->count() > 0)

                                            <?php
                                            $total_amount                   = 0;
                                            ?>

                                            @foreach($entries as $key => $value)
                                            <?php
                                                $total_amount   = $total_amount + ($value['total_amount']);
                                                $variation_name = ProductVariationName($value['product_entry_id']);

                                                if ($value['product_code'] != null)
                                                {
                                                    $productCode  = ' - '.$value['product_code'];
                                                }
                                                else
                                                {
                                                    $productCode  = '';
                                                }

                                                if ($value['product_name'] != null)
                                                {
                                                    $category  = ' - '.$value['product_name'];
                                                }
                                                else
                                                {
                                                    $category  = '';
                                                }

                                                if ($value['brand_name'] != null)
                                                {
                                                    $brandName  = $value['brand_name'];
                                                }
                                                else
                                                {
                                                    $brandName  = '';
                                                }

                                                if ($value['unit_name'] != null)
                                                {
                                                    $unit  = ' '.$value['unit_name'];
                                                }
                                                else
                                                {
                                                    $unit  = '';
                                                }

                                                if ($variation_name != null)
                                                {
                                                    $variation  = ' '.$variation_name;
                                                }
                                                else
                                                {
                                                    $variation  = '';
                                                }

                                                $pre_dues = $invoice['previous_due'];
                                                $net_paya = round($total_amount, 2);
                                                $paid     = round($invoice['cash_given'], 2);
                                                $dues     = round($net_paya - $paid, 2);
                                            ?>



                                            <tr class="tr-height">
                                                <td @if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6) style="padding-left: 30px;color: red" @endif colspan="2" style="padding-left: 30px">{{ $value['product_entry_name'] . $variation }}</td>
                                                <td @if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6) style="text-align: center;color: red" @endif style="text-align: center">{{$value['rate'] }}</td>
                                                <td @if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6) style="text-align: center;color: red" @endif style="text-align: center">{{ $value['quantity'] . $unit }}</td>
                                                <td @if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6) style="text-align: right;color: red" @endif style="text-align: right">{{ round($value['total_amount'], 2) }}</td>
                                            </tr>
                                            @endforeach
                                            @endif

                                            <?php
                                                if ($invoice['vat_type'] == 0)
                                                {
                                                    $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                                }
                                                else
                                                {
                                                    $vat_amount  = $invoice['total_vat'];
                                                }

                                                if ($invoice['total_discount_type'] == 0)
                                                {
                                                    $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                                }
                                                else
                                                {
                                                    $discount_on_total_amount  = $invoice['total_discount_amount'];
                                                }
                                            ?>

                                            <tr>
                                                <th style="text-align: left;text-align: right;color: #e50606;" colspan="4"><strong>সর্বমোট</strong></th>
                                                <th style="text-align: right;font-weight: bold; color: green;">{{ $net_paya != 0 ? round($net_paya - $invoice['total_discount']) : '' }}</th>
                                            </tr>

                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong>মোট পরিমাণে ছাড়({{ $invoice['total_discount_type'] == 0 ? $invoice['total_discount_amount'].'%' : 'BDT' }})</strong></th>
                                                <th style="text-align: right">{{ round($discount_on_total_amount) }}</th>
                                            </tr>
                                            
                                            

                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong>পূর্বের জের</strong></th>
                                                <th style="text-align: right">{{ $pre_dues != 0 ? round($pre_dues) : '' }}</th>
                                            </tr>
                                            
                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong style="color: red;">মোট  </strong></th>
                                                <th style="text-align: right;font-weight: bold; color: red;">{{ $net_paya - $invoice['total_discount'] + $pre_dues  - $discount_on_total_amount  != 0 ? round($net_paya - $invoice['total_discount'] + $pre_dues  - $discount_on_total_amount) : '' }}</th>
                                            </tr>

                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong>জমা</strong></th>
                                                <th style="text-align: right">{{$invoice['cash_given']}}</th>
                                            </tr>

                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong>মোট বাকি</strong></th>
                                                <th style="text-align: right">{{ ($net_paya - $invoice['total_discount'] + $pre_dues != 0 ? round($net_paya - $invoice['total_discount'] - $discount_on_total_amount + $pre_dues) : '') -$invoice['cash_given'] }} </th>
                                            </tr>
                                            


                                                <tr>
                                                    <th style="text-align: left" colspan="6">গাড়ি নং : </strong>{{ $invoice->vichele_id != null ? $invoice->vichele_id : '' }}</th>
                                                    <!--<th style="text-align: center" colspan="2">মাল আনলোডের স্থান</th>-->
                                                </tr>

                                                <tr>
                                                    <th style="text-align: left" colspan="6">ড্রাইভারের নাম : </strong>{{ $invoice['driver_name'] }}</th>
                                                    <!--<th style="text-align: center" colspan="2" rowspan="3"></th>-->
                                                </tr>

                                                <tr>
                                                    <th style="text-align: left" colspan="6"><strong>ড্রাইভারের মোবা : </strong>{{ $invoice['driver_phone'] }}</th>
                                                </tr>

                                                <tr>
                                                    <th style="text-align: left" colspan="6"><strong>গাড়ি ভাড়া : </strong>{{ $invoice['rent'] }}</th>
                                                </tr>
                                                
                                                <tr>
                                                    <th style="text-align: left;" colspan="6"><strong>মাল লোডের  স্থান : {{ $invoice['unload_place_name'] }}</strong></th>
                                                </tr>

                                           <!--  <tr>
                                                <th class="border-none"></th>
                                                <th class="border-none-2"></th>
                                                <th style="text-align: right"><strong>Other Paid</strong></th>
                                                <th colspan="3" style="text-align: right"></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none"></th>
                                                <th class="border-none-2"></th>
                                                <th style="text-align: right"><strong>Current Balance</strong></th>
                                                <th colspan="3" style="text-align: right"></th>
                                            </tr> -->
                                        </table>
                                    </div>

                                        <br>
                                        <br>
                                        <br>

                                    <div class="row">
                                            <div class="col-md-3">
                                                <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">লোড ম্যানেজার </span> </h6>
                                            </div>
                                            <div class="col-md-3">
                                                <h6 style="text-align: center"> <span style="border-top: 1px dotted black;padding: 5px">কম্পিউটার অপারেটর</span> </h6>
                                            </div>
                                            <div class="col-md-3">
                                                <h6 style="text-align: center"> <span style="border-top: 1px dotted black;padding: 5px">জেনারেল ম্যানেজার</span> </h6>
                                            </div>
                                            <div class="col-md-3">
                                                <h6 style="text-align: right"> <span style="padding: 5px">পক্ষে : সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</span> </h6>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div  class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                            <img style="width: 40px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 32px;padding-top: 10px;font-weight: bold">সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</h2>
                                            <p style="line-height: 1.2;font-size: 20px;font-weight: bold" class="text-center">{{ $user_info['organization_name'] }}</p>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                            {{ QrCode::size(70)->generate("string") }}
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px">অত্যাধুনিক স্বয়ংক্রিয় মেশিনে উন্নত ধান থেকে উন্নতমানের বিভিন্ন প্রকার চাউল উৎপাদন ও সরবরাহ ক্রা হয় ।  </p>
                                                <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">বাঁকা ব্রিক্স ফিল্ড, জীবননগর, চুয়াডাঙ্গা । ০১৬৮৮ ৯৩৯৩৯৩, ০১৯১৬ ০৪১১২৯১, ০১৭১৫ ৭০৪১২৬</p>
                                                <p style="line-height: 3;text-align: center"><span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 20px">বিক্রয় চালান / বিল</span>
                                                    <span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 15px;float: right">মুল কপি</span>

                                                 <span style="font-size: 15px;float: left">{{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }}</span>
                                        </div>
                                    </div>

                                    <hr style="margin: 5px !important">

                                    <div class="row" style="line-height: 25px;">

                                        <div style="font-size: 18px" class="col-md-7">
                                          <strong>প্রতিষ্ঠানের নাম  - {{ $invoice['customer_name'] != null ? $invoice['customer_name'] : $invoice->customer->customer_name}} </strong>
                                        </div>
                                        <div style="font-size: 18px" class="col-md-5 text-sm-right">
                                           
                                                <strong>তারিখ: </strong>
                                                <span style="font-weight: bold; color: green;">
                                                    {{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}
                                                </span>    
                                        </div>

                                        <div style="font-size: 18px" class="col-md-12">
                                           
                                                <span style="font-weight: bold">প্রাপকের নাম:  </span><span style="font-weight: bold; color: red;">{{ $invoice['proprietor_name'] }} </span><br>
                                                <span style="font-weight: bold">মোবাইল নং: </span><strong>{{ $invoice['customer_phone'] != null ? $invoice['customer_phone'] : $invoice->customer->ClientContactNumber }}</strong>     
                                        </div>
                                    </div>

                                    <div style="padding-top: 0px;padding-bottom: 0px">
                                        <table style="width: 100%;">
                                            <tr>
                                                <th colspan="2" style="font-size: 23px;width: 50%;text-align: center">মালের বিবরণ</th>
                                                <th style="font-size: 18px;width: 15%;text-align: center">দর</th>
                                                <th style="font-size: 18px;width: 10%;text-align: center">পরিমাণ</th>
                                                <th style="font-size: 18px;width: 20%;text-align: center">মোট টাকা</th>
                                            </tr>

                                            @if($entries->count() > 0)

                                            <?php
                                            $total_amount                   = 0;
                                            ?>

                                            @foreach($entries as $key => $value)
                                            <?php
                                                $total_amount   = $total_amount + ($value['total_amount']);
                                                $variation_name = ProductVariationName($value['product_entry_id']);

                                                if ($value['product_code'] != null)
                                                {
                                                    $productCode  = ' - '.$value['product_code'];
                                                }
                                                else
                                                {
                                                    $productCode  = '';
                                                }

                                                if ($value['product_name'] != null)
                                                {
                                                    $category  = ' - '.$value['product_name'];
                                                }
                                                else
                                                {
                                                    $category  = '';
                                                }

                                                if ($value['brand_name'] != null)
                                                {
                                                    $brandName  = $value['brand_name'];
                                                }
                                                else
                                                {
                                                    $brandName  = '';
                                                }

                                                if ($value['unit_name'] != null)
                                                {
                                                    $unit  = ' '.$value['unit_name'];
                                                }
                                                else
                                                {
                                                    $unit  = '';
                                                }

                                                if ($variation_name != null)
                                                {
                                                    $variation  = ' '.$variation_name;
                                                }
                                                else
                                                {
                                                    $variation  = '';
                                                }

                                                $pre_dues = $invoice['previous_due'];
                                                $net_paya = round($total_amount, 2);
                                                $paid     = round($invoice['cash_given'], 2);
                                                $dues     = round($net_paya - $paid, 2);
                                            ?>



                                            <tr class="tr-height">
                                                <td @if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6) style="padding-left: 30px;color: red" @endif colspan="2" style="padding-left: 30px">{{ $value['product_entry_name'] . $variation }}</td>
                                                <td @if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6) style="text-align: center;color: red" @endif style="text-align: center">{{$value['rate'] }}</td>
                                                <td @if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6) style="text-align: center;color: red" @endif style="text-align: center">{{ $value['quantity'] . $unit }}</td>
                                                <td @if($value['product_entry_id'] == 94 ||$value['product_entry_id'] == 4 || $value['product_entry_id'] == 5 || $value['product_entry_id'] == 6) style="text-align: right;color: red" @endif style="text-align: right">{{ round($value['total_amount'], 2) }}</td>
                                            </tr>
                                            @endforeach
                                            @endif

                                            <?php
                                                if ($invoice['vat_type'] == 0)
                                                {
                                                    $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                                }
                                                else
                                                {
                                                    $vat_amount  = $invoice['total_vat'];
                                                }

                                                if ($invoice['total_discount_type'] == 0)
                                                {
                                                    $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                                }
                                                else
                                                {
                                                    $discount_on_total_amount  = $invoice['total_discount_amount'];
                                                }
                                            ?>

                                            <tr>
                                                <th style="text-align: left;text-align: right;color: #e50606;" colspan="4"><strong>সর্বমোট</strong></th>
                                                <th style="text-align: right;font-weight: bold; color: green;">{{ $net_paya != 0 ? round($net_paya - $invoice['total_discount']) : '' }}</th>
                                            </tr>

                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong>মোট পরিমাণে ছাড়({{ $invoice['total_discount_type'] == 0 ? $invoice['total_discount_amount'].'%' : 'BDT' }})</strong></th>
                                                <th style="text-align: right">{{ round($discount_on_total_amount) }}</th>
                                            </tr>
                                            
                                            

                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong>পূর্বের জের</strong></th>
                                                <th style="text-align: right">{{ $pre_dues != 0 ? round($pre_dues) : '' }}</th>
                                            </tr>
                                            
                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong style="color: red;">মোট  </strong></th>
                                                <th style="text-align: right;font-weight: bold; color: red;">{{ $net_paya - $invoice['total_discount'] + $pre_dues  - $discount_on_total_amount  != 0 ? round($net_paya - $invoice['total_discount'] + $pre_dues  - $discount_on_total_amount) : '' }}</th>
                                            </tr>

                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong>জমা</strong></th>
                                                <th style="text-align: right">{{$invoice['cash_given']}}</th>
                                            </tr>

                                            <tr>
                                                <th style="text-align: left;text-align: right;" colspan="4"><strong>মোট বাকি</strong></th>
                                                <th style="text-align: right">{{ ($net_paya - $invoice['total_discount'] + $pre_dues != 0 ? round($net_paya - $invoice['total_discount'] - $discount_on_total_amount + $pre_dues) : '') -$invoice['cash_given'] }} </th>
                                            </tr>
                                            


                                                <tr>
                                                    <th style="text-align: left" colspan="6">গাড়ি নং : </strong>{{ $invoice->vichele_id != null ? $invoice->vichele_id : '' }}</th>
                                                    <!--<th style="text-align: center" colspan="2">মাল আনলোডের স্থান</th>-->
                                                </tr>

                                                <tr>
                                                    <th style="text-align: left" colspan="6">ড্রাইভারের নাম : </strong>{{ $invoice['driver_name'] }}</th>
                                                    <!--<th style="text-align: center" colspan="2" rowspan="3"></th>-->
                                                </tr>

                                                <tr>
                                                    <th style="text-align: left" colspan="6"><strong>ড্রাইভারের মোবা : </strong>{{ $invoice['driver_phone'] }}</th>
                                                </tr>

                                                <tr>
                                                    <th style="text-align: left" colspan="6"><strong>গাড়ি ভাড়া : </strong>{{ $invoice['rent'] }}</th>
                                                </tr>
                                                
                                                <tr>
                                                    <th style="text-align: left;" colspan="6"><strong>মাল লোডের স্থান : {{ $invoice['unload_place_name'] }}</strong></th>
                                                </tr>

                                           <!--  <tr>
                                                <th class="border-none"></th>
                                                <th class="border-none-2"></th>
                                                <th style="text-align: right"><strong>Other Paid</strong></th>
                                                <th colspan="3" style="text-align: right"></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none"></th>
                                                <th class="border-none-2"></th>
                                                <th style="text-align: right"><strong>Current Balance</strong></th>
                                                <th colspan="3" style="text-align: right"></th>
                                            </tr> -->
                                        </table>
                                    </div>

                                        <br>
                                        <br>
                                        <br>

                                    <div class="row">
                                            <div class="col-md-3">
                                                <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">লোড ম্যানেজার </span> </h6>
                                            </div>
                                            <div class="col-md-3">
                                                <h6 style="text-align: center"> <span style="border-top: 1px dotted black;padding: 5px">কম্পিউটার অপারেটর</span> </h6>
                                            </div>
                                            <div class="col-md-3">
                                                <h6 style="text-align: center"> <span style="border-top: 1px dotted black;padding: 5px">জেনারেল ম্যানেজার</span> </h6>
                                            </div>
                                            <div class="col-md-3">
                                                <h6 style="text-align: right"> <span style="padding: 5px">পক্ষে : সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</span> </h6>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        javascript:window.print();
    });

    window.onafterprint = function(e){
        var site_url  = $('.site_url').val();
        window.location.replace(site_url + '/invoices/all-sales');
    };
</script>
@endsection