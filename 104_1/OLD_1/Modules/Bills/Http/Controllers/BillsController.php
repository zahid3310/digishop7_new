<?php

namespace Modules\Bills\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Products;
use App\Models\Customers;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\ProductEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Users;
use App\Models\ProductVariations;
use App\Models\UnitConversions;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\Vicheles;

use App\Models\AccountHead;
use App\Models\Projects;
use App\Models\Clients;
use App\Models\VoucherSummary;
use App\Models\AccountTransaction;
use App\Models\BankAccounts;
use App\Models\ChequeTransactions;

use Response;
use DB;
use App\Models\Units;

class BillsController extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        // $branch_id          = Auth()->user()->branch_id;
        // $customer_id        = 795;
        // $sales              = AccountTransaction::where('IsPosted', 1)
        //                             ->where('CompanyID', $branch_id)
        //                             ->where('RegisterID', $customer_id)
        //                             ->where('VoucherType', 'SALES')
        //                             ->where('HeadID', 1189)
        //                             ->sum('CreditAmount');

        // $sales_return       = AccountTransaction::where('IsPosted', 1)
        //                             ->where('CompanyID', $branch_id)
        //                             ->where('RegisterID', $customer_id)
        //                             ->where('VoucherType', 'SR')
        //                             ->where('HeadID', 1323)
        //                             ->sum('DebitAmount');

        // $purchase           = AccountTransaction::where('IsPosted', 1)
        //                             ->where('CompanyID', $branch_id)
        //                             ->where('RegisterID', $customer_id)
        //                             ->where('VoucherType', 'PURCHASE')
        //                             ->where('HeadID', 1322)
        //                             ->sum('DebitAmount');

        // $purchase_return    = AccountTransaction::where('IsPosted', 1)
        //                             ->where('CompanyID', $branch_id)
        //                             ->where('RegisterID', $customer_id)
        //                             ->where('VoucherType', 'PR')
        //                             ->where('HeadID', 1324)
        //                             ->sum('CreditAmount');

        // $ac                 = AccountTransaction::where('IsPosted', 1)
        //                             ->where('CompanyID', $branch_id)
        //                             ->where('RegisterID', $customer_id)
        //                             ->whereIn('PurposeAccHeadName', ['bank_ac', 'cash_ac'])
        //                             ->get();

        // $bal                = $ac->sum('CreditAmount') - $ac->sum('DebitAmount');
        // $balance            = $sales - $sales_return + $purchase - $purchase_return - $bal;


        // dd($purchase, $purchase_return, $bal, $balance);






        $branch_id          = Auth()->user()->branch_id;
        $products           = ProductEntries::orderBy('product_code', 'DESC')
                                            ->get();

        $product_id         = array_values($products->sortByDesc('product_code')->take(1)->toArray());
        $paid_accounts      = AccountHead::where('IsTransactable', 1)
                                        ->where('ActiveStatus', 1)
                                        ->where('CompanyID', $branch_id)
                                        ->whereIn('PtnID', ['227', '228'])
                                        ->orderBy('PtnGroupCode', 'ASC')
                                        ->get();

        $units              = Units::orderBy('id', 'ASC')->get();
        $variations         = ProductVariations::orderBy('id', 'ASC')->get();
        $vicheles           = Vicheles::orderBy('id', 'ASC')->get();

        return view('bills::index', compact('products', 'product_id', 'paid_accounts', 'units', 'variations', 'vicheles'));
    }

    public function allBills()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('bills::all_bills');
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('bills::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'      => 'required',
            'vendor_id'         => 'required',
            'product_entries.*' => 'required',
            'amount.*'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();
   
        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            $data_find                      = Bills::orderBy('created_at', 'DESC')->first();
            $bill_number                    = $data_find != null ? $data_find['bill_number'] + 1 : 1;

            $find_reference                 = Customers::where('client_id', $data['vendor_id'])->first();
            $bill                           = new Bills;
            $bill->bill_number              = $bill_number;
            $bill->vendor_id                = $data['vendor_id'];
            $bill->reference_id             = $find_reference['reference_id'];
            $bill->bill_date                = date('Y-m-d', strtotime($data['selling_date']));
            $bill->bill_amount              = $data['total_amount'];
            $bill->due_amount               = $data['total_amount'];
            $bill->total_discount           = $discount;
            $bill->bill_note                = $data['bill_note'];
            $bill->total_vat                = $vat;
            $bill->vat_type                 = $data['vat_type'];
            $bill->total_discount_type      = $data['total_discount_type'];
            $bill->total_discount_amount    = $data['total_discount_amount'];
            $bill->total_discount_note      = $data['total_discount_note'];
            $bill->cash_given               = $data['cash_given'];
            $bill->change_amount            = $data['change_amount'];
            $bill->previous_due             = $data['previous_due'];
            $bill->previous_due_type        = $data['balance_type'];
            $bill->adjusted_amount          = $data['adjustment'];
            $bill->branch_id                = $branch_id;
            $bill->major_category_id        = $data['major_category_id'] != 0 ? $data['major_category_id'] : null;
            $bill->account_id               = $data['account_id'];
            $bill->vichele_id               = $data['vichele_id'];
            $bill->driver_name              = $data['driver_name'];
            $bill->driver_phone             = $data['driver_phone'];
            $bill->rent                     = $data['rent'];
            $bill->rent_type                = isset($data['rent_type']) ? 1 : 2;
            $bill->unload_place_name        = $data['unload_place_name'];
            $bill->created_by               = $user_id; 

            if ($bill->save())
            {
                foreach ($data['product_entries'] as $key1 => $value1)
                {
                    $product_buy_price      = ProductEntries::find($value1);

                    $bill_entries[] = [
                        'bill_id'            => $bill['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value1,
                        'main_unit_id'       => $data['main_unit_id'][$key1],
                        'conversion_unit_id' => $data['unit_id'][$key1],
                        'vendor_id'          => $bill['vendor_id'],
                        'weight_per_bosta'   => $data['weight_per_bosta'][$key1],
                        'rate'               => $data['rate'][$key1],
                        'quantity'           => $data['quantity'][$key1],
                        'bosta'              => $data['bosta'][$key1],
                        'total_amount'       => $data['amount'][$key1],
                        'discount_type'      => $data['discount_type'][$key1],
                        'discount_amount'    => $data['discount'][$key1],
                        'branch_id'          => $branch_id,
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('bill_entries')->insert($bill_entries);

                stockIn($data, $item_id=null);

                //Financial Accounting Start
                    accountTransactionsCreditStoreInventory($VoucherId = null, 
                            $VoucherDate        = date('Y-m-d', strtotime($data['selling_date'])), 
                            $VoucherNumber      = null, 
                            $ProjectID          = 0, 
                            $RegisterID         = $data['vendor_id'], 
                            $HeadID             = $data['account_id'], 
                            $PurposeAccHeadName = null, 
                            $Particulars        = $data['bill_note'], 
                            $DebitAmount        = '0.00', 
                            $CreditAmount       = $data['total_amount'], 
                            $AccountNumber      = null, 
                            $VoucherType        = 'PURCHASE', 
                            $CBAccount          = 1322, 
                            $IsPosted           = 1, 
                            $CompanyID          = $branch_id, 
                            $Status             = 1, 
                            $created_by         = $user_id,
                            $invoice_id         = null,
                            $bill_id            = $bill->id,
                            $sales_return_id    = null,
                            $purchase_return_id = null
                        );

                    accountTransactionsDebitStoreInventory($VoucherId = null, 
                            $VoucherDate        = date('Y-m-d', strtotime($data['selling_date'])), 
                            $VoucherNumber      = null, 
                            $ProjectID          = 0, 
                            $RegisterID         = $data['vendor_id'], 
                            $HeadID             = 1322, 
                            $PurposeAccHeadName = null, 
                            $Particulars        = $data['bill_note'], 
                            $DebitAmount        = $data['total_amount'], 
                            $CreditAmount       = '0.00', 
                            $AccountNumber      = null, 
                            $VoucherType        = 'PURCHASE', 
                            $CBAccount          = $data['account_id'], 
                            $IsPosted           = 1, 
                            $CompanyID          = $branch_id, 
                            $Status             = 1, 
                            $created_by         = $user_id,
                            $invoice_id         = null,
                            $bill_id            = $bill->id,
                            $sales_return_id    = null,
                            $purchase_return_id = null
                        );

                    //Insert into journal_entries 
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {
                                $accountHeadType = AccountHead::find($data['current_balance_paid_through'][$i]);
                                if($accountHeadType['PurposeHeadName'] == 'cash_ac')
                                {
                                    $type           = 'CP';
                                    $voucherNumber  = $this->gets_voucherNumber_name($branch_id, $type);
                                    $storeSummary   = voucherSummaryCreate($VoucherNumber = $voucherNumber, 
                                            $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                            $ProjectID            = 0, 
                                            $RegisterID           = 0, 
                                            $Type                 = $type, 
                                            $BankAccountNumber    = 0, 
                                            $Status               = 1, 
                                            $TotalAmount          = $data['current_balance_amount_paid'][$i], 
                                            $Narration            = $data['current_balance_note'][$i], 
                                            $MoneyReceiptNo       = null, 
                                            $CompanyID            = $branch_id, 
                                            $created_by           = $user_id
                                        );
                    
                                    if ($storeSummary > 0)
                                    {
                                        //For Debit
                                        accountTransactionsDebitStoreInventory($VoucherId = $storeSummary, 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['vendor_id'], 
                                                $HeadID               = 1248, 
                                                $PurposeAccHeadName   = 'others', 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = $data['current_balance_amount_paid'][$i], 
                                                $CreditAmount         = '0.00', 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = $data['current_balance_paid_through'][$i], 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $created_by           = $user_id,
                                                $invoice_id           = null,
                                                $bill_id              = $bill->id,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );

                                        //For Credit
                                        accountTransactionsCreditStoreInventory($VoucherId = $storeSummary, 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['vendor_id'], 
                                                $HeadID               = $data['current_balance_paid_through'][$i], 
                                                $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = '0.00', 
                                                $CreditAmount         = $data['current_balance_amount_paid'][$i], 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = 1248, 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $created_by           = $user_id,
                                                $invoice_id           = null,
                                                $bill_id              = $bill->id,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );
                                    }
                                }

                                if($accountHeadType['PurposeHeadName'] == 'bank_ac')
                                {
                                    $type           = 'BP';
                                    $voucherNumber  = $this->gets_voucherNumber_name($branch_id, $type);
                                    $storeSummary   = voucherSummaryCreate($VoucherNumber = $voucherNumber, 
                                            $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                            $ProjectID            = 0, 
                                            $RegisterID           = 0, 
                                            $Type                 = $type, 
                                            $BankAccountNumber    = $data['current_balance_paid_through'][$i], 
                                            $Status               = 1, 
                                            $TotalAmount          = $data['current_balance_amount_paid'][$i], 
                                            $Narration            = $data['current_balance_note'][$i], 
                                            $MoneyReceiptNo       = null, 
                                            $CompanyID            = $branch_id, 
                                            $created_by           = $user_id
                                        );
                    
                                    if ($storeSummary > 0)
                                    {
                                        //For Debit
                                        accountTransactionsDebitStoreInventory($VoucherId = $storeSummary, 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['vendor_id'], 
                                                $HeadID               = 1248, 
                                                $PurposeAccHeadName   = 'others', 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = $data['current_balance_amount_paid'][$i], 
                                                $CreditAmount         = '0.00', 
                                                $AccountNumber        = $data['current_balance_paid_through'][$i], 
                                                $VoucherType          = $type, 
                                                $CBAccount            = $data['current_balance_paid_through'][$i], 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $created_by           = $user_id,
                                                $invoice_id           = null,
                                                $bill_id              = $bill->id,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );

                                        //For Credit
                                        accountTransactionsCreditStoreInventory($VoucherId = $storeSummary, 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['vendor_id'], 
                                                $HeadID               = $data['current_balance_paid_through'][$i], 
                                                $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = '0.00', 
                                                $CreditAmount         = $data['current_balance_amount_paid'][$i], 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = 1248, 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $created_by           = $user_id,
                                                $invoice_id           = null,
                                                $bill_id              = $bill->id,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );
                                    }
                                }
                            }
                        }
                    }

                    balanceUpdate($data['vendor_id']);
                //Financial Accounting End

                DB::commit();

                if ($data['print'] == 1)
                {
                    return redirect()->route('bills_index')->with("success","Purchase Created Successfully !!");
                }
                else
                {
                    return redirect()->route('bills_show', $bill['id']);
                }     
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $bill       = Bills::leftjoin('tbl_clients', 'tbl_clients.id', 'bills.vendor_id')
                                ->leftjoin('customers', 'customers.client_id', 'tbl_clients.id')
                                ->select('bills.*',
                                         'customers.name as customer_name',
                                         'customers.address as address',
                                         'customers.phone as phone')
                                ->find($id);

        $entries    = BillEntries::leftjoin('products', 'products.id', 'bill_entries.product_id')
                                ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                ->where('bill_entries.bill_id', $id)
                                ->select('bill_entries.*',
                                         'product_entries.product_type as product_type',
                                         'product_entries.name as product_entry_name',
                                         'products.name as product_name')
                                ->orderBy('id', 'DESC')
                                ->get(); 
                     
        $user_info  = userDetails();

        return view('bills::show', compact('entries', 'bill', 'user_info'));
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $find_bill              = Bills::leftjoin('tbl_clients', 'tbl_clients.id', 'bills.vendor_id')
                                        ->orderBy('bills.created_at', 'DESC')
                                        ->select('bills.*',
                                                 'tbl_clients.id as vendor_id',
                                                 'tbl_clients.ClientAddress as address',
                                                 'tbl_clients.ClientName as vendor_name')
                                        ->find($id);

        $find_bill_entries      = BillEntries::leftjoin('customers', 'customers.id', 'bill_entries.vendor_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                        ->where('bill_entries.bill_id', $id)
                                        ->select('bill_entries.*',
                                                 'customers.id as vendor_id',
                                                 'customers.name as vendor_name',
                                                 'product_entries.id as item_id',
                                                 'product_entries.product_code as product_code',
                                                 'product_entries.stock_in_hand as stock_in_hand',
                                                 'product_entries.name as item_name')
                                        ->get();

        $entries_count          = $find_bill_entries->count();

        $find_customer          = Customers::find($find_bill['vendor_id']);
        $current_balance        = AccountTransaction::where('IsPosted', 1)
                                            ->where('CompanyID', $find_bill['branch_id'])
                                            ->where('RegisterID', $find_bill['vendor_id'])
                                            ->where('bill_id', $id)
                                            ->where('CreditAmount', '>', 0)
                                            ->where('VoucherType', '!=', 'PURCHASE')
                                            ->get();

        $current_balance_count  = $current_balance->count();

        $paid_accounts          = AccountHead::where('IsTransactable', 1)
                                        ->where('ActiveStatus', 1)
                                        ->where('CompanyID', $find_bill['branch_id'])
                                        ->whereIn('PtnID', ['227', '228'])
                                        ->orderBy('PtnGroupCode', 'ASC')
                                        ->get();

        $accounts               = Accounts::where('account_type_id',12)->where('status', 1)->get();

        if ((Auth::user()->branch_id != $find_bill->branch_id) && (Auth::user()->branch_id != 1))
        {
            return back()->with('unsuccess', 'You are not allowed to edit this invoice');
        }

        $vicheles  = Vicheles::orderBy('id', 'ASC')->get();

        return view('bills::edit', compact('find_bill', 'find_bill_entries', 'entries_count', 'paid_accounts', 'find_customer', 'current_balance', 'current_balance_count', 'accounts', 'vicheles'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'      => 'required',
            'vendor_id'         => 'required',
            'product_entries.*' => 'required',
            'amount.*'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            $old_bill       = Bills::find($id);
            $bill           = Bills::find($id);
            $branch_id      = $bill['branch_id'];

            $bill->vendor_id                = $data['vendor_id'];
            $bill->bill_date                = date('Y-m-d', strtotime($data['selling_date']));
            $bill->bill_amount              = $data['total_amount'];
            $bill->due_amount               = $data['total_amount'];
            $bill->total_discount           = $discount;
            $bill->bill_note                = $data['bill_note'];
            $bill->total_vat                = $vat;
            $bill->vat_type                 = $data['vat_type'];
            $bill->total_discount_type      = $data['total_discount_type'];
            $bill->total_discount_amount    = $data['total_discount_amount'];
            $bill->total_discount_note      = $data['total_discount_note'];
            $bill->cash_given               = $data['cash_given'];
            $bill->change_amount            = $data['change_amount'];
            $bill->adjusted_amount          = $data['adjustment'];
            $bill->major_category_id        = $data['major_category_id'] != 0 ? $data['major_category_id'] : null;
            $bill->vichele_id               = $data['vichele_id'];
            $bill->driver_name              = $data['driver_name'];
            $bill->driver_phone             = $data['driver_phone'];
            $bill->rent                     = $data['rent'];
            $bill->rent_type                = isset($data['rent_type']) ? 1 : 2;
            $bill->unload_place_name        = $data['unload_place_name'];
            $bill->updated_by               = $user_id;

            if ($bill->save())
            {
                $item_id                = BillEntries::where('bill_id', $bill['id'])->get();
                $item_delete            = BillEntries::where('bill_id', $bill['id'])->delete();

                foreach ($data['product_entries'] as $key1 => $value1)
                {
                    $product_buy_price      = ProductEntries::find($value1);

                    $bill_entries[] = [
                        'bill_id'            => $bill['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value1,
                        'main_unit_id'       => $data['main_unit_id'][$key1],
                        'conversion_unit_id' => $data['unit_id'][$key1],
                        'vendor_id'          => $bill['vendor_id'],
                        'rate'               => $data['rate'][$key1],
                        'weight_per_bosta'   => $data['weight_per_bosta'][$key1],
                        'quantity'           => $data['quantity'][$key1],
                        'bosta'              => $data['bosta'][$key1],
                        'total_amount'       => $data['amount'][$key1],
                        'discount_type'      => $data['discount_type'][$key1],
                        'discount_amount'    => $data['discount'][$key1],
                        'branch_id'          => $old_bill['branch_id'],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('bill_entries')->insert($bill_entries);

                stockIn($data, $item_id);

                $jour_ent_debit     = AccountTransaction::where('IsPosted', 1)
                                            ->where('CompanyID', $bill['branch_id'])
                                            ->where('RegisterID', $bill['vendor_id'])
                                            ->where('bill_id', $id)
                                            ->where('VoucherType', 'PURCHASE')
                                            ->where('DebitAmount', '>', 0)
                                            ->first();

                $jour_ent_credit    = AccountTransaction::where('IsPosted', 1)
                                            ->where('CompanyID', $bill['branch_id'])
                                            ->where('RegisterID', $bill['vendor_id'])
                                            ->where('bill_id', $id)
                                            ->where('VoucherType', 'PURCHASE')
                                            ->where('CreditAmount', '>', 0)
                                            ->first();

                //Financial Accounting Start
                    accountTransactionsCreditUpdateInventory($id = $jour_ent_credit['id'],
                            $VoucherId          = null, 
                            $VoucherDate        = $data['selling_date'], 
                            $VoucherNumber      = null, 
                            $ProjectID          = 0, 
                            $RegisterID         = $data['vendor_id'], 
                            $HeadID             = $data['account_id'], 
                            $PurposeAccHeadName = null, 
                            $Particulars        = $data['bill_note'], 
                            $DebitAmount        = '0.00', 
                            $CreditAmount       = $data['total_amount'], 
                            $AccountNumber      = null, 
                            $VoucherType        = 'PURCHASE', 
                            $CBAccount          = 1322, 
                            $IsPosted           = 1, 
                            $CompanyID          = $branch_id, 
                            $Status             = 1, 
                            $created_by         = $user_id,
                            $invoice_id         = null,
                            $bill_id            = $bill->id,
                            $sales_return_id    = null,
                            $purchase_return_id = null
                        );

                    accountTransactionsDebitUpdateInventory($id = $jour_ent_debit['id'],
                            $VoucherId          = null, 
                            $VoucherDate        = $data['selling_date'], 
                            $VoucherNumber      = null, 
                            $ProjectID          = 0, 
                            $RegisterID         = $data['vendor_id'], 
                            $HeadID             = 1322, 
                            $PurposeAccHeadName = null, 
                            $Particulars        = $data['bill_note'], 
                            $DebitAmount        = $data['total_amount'], 
                            $CreditAmount       = '0.00', 
                            $AccountNumber      = null, 
                            $VoucherType        = 'PURCHASE', 
                            $CBAccount          = $data['account_id'], 
                            $IsPosted           = 1, 
                            $CompanyID          = $branch_id, 
                            $Status             = 1, 
                            $created_by         = $user_id,
                            $invoice_id         = null,
                            $bill_id            = $bill->id,
                            $sales_return_id    = null,
                            $purchase_return_id = null
                        );

                //Financial Accounting End

                //Insert into journal_entries Start
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {   
                                $trn_data  = AccountTransaction::find($data['current_balance_id'][$i]);

                                if ($trn_data != null)
                                {
                                    $accountHeadType = AccountHead::find($data['current_balance_paid_through'][$i]);
                                    if($accountHeadType['PurposeHeadName'] == 'cash_ac')
                                    {
                                        $type           = 'CP';
                                        $voucherNumber  = $trn_data['VoucherNumber'];
                                        $updateSummary  = voucherSummaryUpdate($id = $trn_data['VoucherId'],
                                                $VoucherNumber        = $voucherNumber, 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['current_balance_payment_date'][$i])), 
                                                $ProjectID            = 0, 
                                                $RegisterID           = 0, 
                                                $Type                 = $type, 
                                                $BankAccountNumber    = 0, 
                                                $Status               = 1, 
                                                $TotalAmount          = $data['current_balance_amount_paid'][$i], 
                                                $Narration            = $data['current_balance_note'][$i], 
                                                $MoneyReceiptNo       = null, 
                                                $CompanyID            = $branch_id, 
                                                $updated_by           = $user_id
                                            );

                                        accountTransactionsDebitUpdateInventory($id = $trn_data['id'] - 1,
                                                $VoucherId            = $trn_data['VoucherId'],
                                                $VoucherDate          = date('Y-m-d', strtotime($data['current_balance_payment_date'][$i])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['vendor_id'], 
                                                $HeadID               = 1248, 
                                                $PurposeAccHeadName   = 'others', 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = $data['current_balance_amount_paid'][$i], 
                                                $CreditAmount         = '0.00', 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = $data['current_balance_paid_through'][$i], 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $updated_by           = $user_id,
                                                $invoice_id           = null,
                                                $bill_id              = $bill->id,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );

                                        accountTransactionsCreditUpdateInventory($id = $trn_data['id'],
                                                $VoucherId            = $trn_data['VoucherId'],
                                                $VoucherDate          = date('Y-m-d', strtotime($data['current_balance_payment_date'][$i])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['vendor_id'], 
                                                $HeadID               = $data['current_balance_paid_through'][$i], 
                                                $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = '0.00', 
                                                $CreditAmount         = $data['current_balance_amount_paid'][$i], 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = 1248, 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $updated_by           = $user_id,
                                                $invoice_id           = null,
                                                $bill_id              = $bill->id,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );
                                    }

                                    if($accountHeadType['PurposeHeadName'] == 'bank_ac')
                                    {
                                        $type           = 'BP';
                                        $voucherNumber  = $trn_data['VoucherNumber'];
                                        $updateSummary  = voucherSummaryUpdate($id = $trn_data['VoucherId'],
                                                $VoucherNumber        = $voucherNumber, 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['current_balance_payment_date'][$i])), 
                                                $ProjectID            = 0, 
                                                $RegisterID           = 0, 
                                                $Type                 = $type, 
                                                $BankAccountNumber    = $data['current_balance_paid_through'][$i], 
                                                $Status               = 1, 
                                                $TotalAmount          = $data['current_balance_amount_paid'][$i], 
                                                $Narration            = $data['current_balance_note'][$i], 
                                                $MoneyReceiptNo       = null, 
                                                $CompanyID            = $branch_id, 
                                                $updated_by           = $user_id
                                            );

                                        accountTransactionsDebitUpdateInventory($id = $trn_data['id'] - 1,
                                                $VoucherId            = $trn_data['VoucherId'],
                                                $VoucherDate          = date('Y-m-d', strtotime($data['current_balance_payment_date'][$i])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['vendor_id'], 
                                                $HeadID               = 1248, 
                                                $PurposeAccHeadName   = 'others', 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = $data['current_balance_amount_paid'][$i], 
                                                $CreditAmount         = '0.00', 
                                                $AccountNumber        = $data['current_balance_paid_through'][$i], 
                                                $VoucherType          = $type, 
                                                $CBAccount            = $data['current_balance_paid_through'][$i], 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $updated_by           = $user_id,
                                                $invoice_id           = null,
                                                $bill_id              = $bill->id,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );

                                        accountTransactionsCreditUpdateInventory($id = $trn_data['id'],
                                                $VoucherId            = $trn_data['VoucherId'], 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['current_balance_payment_date'][$i])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['vendor_id'], 
                                                $HeadID               = $data['current_balance_paid_through'][$i], 
                                                $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = '0.00', 
                                                $CreditAmount         = $data['current_balance_amount_paid'][$i], 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = 1248, 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $updated_by           = $user_id,
                                                $invoice_id           = null,
                                                $bill_id              = $bill->id,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );
                                    }
                                }
                                else
                                {
                                    $accountHeadType = AccountHead::find($data['current_balance_paid_through'][$i]);
                                    if($accountHeadType['PurposeHeadName'] == 'cash_ac')
                                    {
                                        $type           = 'CP';
                                        $voucherNumber  = $this->gets_voucherNumber_name($branch_id, $type);
                                        $storeSummary   = voucherSummaryCreate($VoucherNumber = $voucherNumber, 
                                                $VoucherDate          = $data['selling_date'], 
                                                $ProjectID            = 0, 
                                                $RegisterID           = 0, 
                                                $Type                 = $type, 
                                                $BankAccountNumber    = 0, 
                                                $Status               = 1, 
                                                $TotalAmount          = $data['current_balance_amount_paid'][$i], 
                                                $Narration            = $data['current_balance_note'][$i], 
                                                $MoneyReceiptNo       = null, 
                                                $CompanyID            = $branch_id, 
                                                $created_by           = $user_id
                                            );
                        
                                        if ($storeSummary > 0)
                                        {
                                            //For Debit
                                            accountTransactionsDebitStoreInventory($VoucherId = $storeSummary, 
                                                    $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                    $VoucherNumber        = $voucherNumber, 
                                                    $ProjectID            = 0, 
                                                    $RegisterID           = $data['vendor_id'], 
                                                    $HeadID               = 1248, 
                                                    $PurposeAccHeadName   = 'others', 
                                                    $Particulars          = $data['current_balance_note'][$key], 
                                                    $DebitAmount          = $data['current_balance_amount_paid'][$key], 
                                                    $CreditAmount         = '0.00', 
                                                    $AccountNumber        = null, 
                                                    $VoucherType          = $type, 
                                                    $CBAccount            = $data['current_balance_paid_through'][$i], 
                                                    $IsPosted             = 1, 
                                                    $CompanyID            = $branch_id, 
                                                    $Status               = 1, 
                                                    $created_by           = $user_id,
                                                    $invoice_id           = null,
                                                    $bill_id              = $bill->id,
                                                    $sales_return_id      = null,
                                                    $purchase_return_id   = null
                                                );

                                            //For Credit
                                            accountTransactionsCreditStoreInventory($VoucherId = $storeSummary, 
                                                    $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                    $VoucherNumber        = $voucherNumber, 
                                                    $ProjectID            = 0, 
                                                    $RegisterID           = $data['vendor_id'], 
                                                    $HeadID               = $data['current_balance_paid_through'][$i], 
                                                    $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                                    $Particulars          = $data['current_balance_note'][$key], 
                                                    $DebitAmount          = '0.00', 
                                                    $CreditAmount         = $data['current_balance_amount_paid'][$key], 
                                                    $AccountNumber        = null, 
                                                    $VoucherType          = $type, 
                                                    $CBAccount            = 1248, 
                                                    $IsPosted             = 1, 
                                                    $CompanyID            = $branch_id, 
                                                    $Status               = 1, 
                                                    $created_by           = $user_id,
                                                    $invoice_id           = null,
                                                    $bill_id              = $bill->id,
                                                    $sales_return_id      = null,
                                                    $purchase_return_id   = null
                                                );
                                        }
                                    }

                                    if($accountHeadType['PurposeHeadName'] == 'bank_ac')
                                    {
                                        $type           = 'BP';
                                        $voucherNumber  = $this->gets_voucherNumber_name($branch_id, $type);
                                        $storeSummary   = voucherSummaryCreate($VoucherNumber = $voucherNumber, 
                                                $VoucherDate          = $data['selling_date'], 
                                                $ProjectID            = 0, 
                                                $RegisterID           = 0, 
                                                $Type                 = $type, 
                                                $BankAccountNumber    = $data['current_balance_paid_through'][$i], 
                                                $Status               = 1, 
                                                $TotalAmount          = $data['current_balance_amount_paid'][$i], 
                                                $Narration            = $data['current_balance_note'][$i], 
                                                $MoneyReceiptNo       = null, 
                                                $CompanyID            = $branch_id, 
                                                $created_by           = $user_id
                                            );
                        
                                        if ($storeSummary > 0)
                                        {
                                            //For Debit
                                            accountTransactionsDebitStoreInventory($VoucherId = $storeSummary, 
                                                    $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                    $VoucherNumber        = $voucherNumber, 
                                                    $ProjectID            = 0, 
                                                    $RegisterID           = $data['vendor_id'], 
                                                    $HeadID               = 1248, 
                                                    $PurposeAccHeadName   = 'others', 
                                                    $Particulars          = $data['current_balance_note'][$i], 
                                                    $DebitAmount          = $data['current_balance_amount_paid'][$i], 
                                                    $CreditAmount         = '0.00', 
                                                    $AccountNumber        = $data['current_balance_paid_through'][$i], 
                                                    $VoucherType          = $type, 
                                                    $CBAccount            = $data['current_balance_paid_through'][$i], 
                                                    $IsPosted             = 1, 
                                                    $CompanyID            = $branch_id, 
                                                    $Status               = 1, 
                                                    $created_by           = $user_id,
                                                    $invoice_id           = null,
                                                    $bill_id              = $bill->id,
                                                    $sales_return_id      = null,
                                                    $purchase_return_id   = null
                                                );

                                            //For Credit
                                            accountTransactionsCreditStoreInventory($VoucherId = $storeSummary, 
                                                    $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                    $VoucherNumber        = $voucherNumber, 
                                                    $ProjectID            = 0, 
                                                    $RegisterID           = $data['vendor_id'], 
                                                    $HeadID               = $data['current_balance_paid_through'][$i], 
                                                    $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                                    $Particulars          = $data['current_balance_note'][$i], 
                                                    $DebitAmount          = '0.00', 
                                                    $CreditAmount         = $data['current_balance_amount_paid'][$i], 
                                                    $AccountNumber        = null, 
                                                    $VoucherType          = $type, 
                                                    $CBAccount            = 1248, 
                                                    $IsPosted             = 1, 
                                                    $CompanyID            = $branch_id, 
                                                    $Status               = 1, 
                                                    $created_by           = $user_id,
                                                    $invoice_id           = null,
                                                    $bill_id              = $bill->id,
                                                    $sales_return_id      = null,
                                                    $purchase_return_id   = null
                                                );
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if($data['current_balance_account_id'][$i] > 0)
                                {
                                    $trn_data_del   = AccountTransaction::find($data['current_balance_id'][$i]);

                                    if ($trn_data_del != null)
                                    {
                                        $accountHeadTypeDel = AccountHead::find($data['current_balance_paid_through'][$i]);
                                        if($accountHeadTypeDel['PurposeHeadName'] == 'cash_ac')
                                        {
                                            $delete_debit       = AccountTransaction::where('id', $data['current_balance_paid_through'][$i])->delete();
                                            $delete_dcredit     = AccountTransaction::where('id', $data['current_balance_paid_through'][$i] + 1)->delete();
                                        }

                                        if($accountHeadTypeDel['PurposeHeadName'] == 'bank_ac')
                                        {
                                            $delete_debit       = AccountTransaction::where('id', $data['current_balance_paid_through'][$i])->delete();
                                            $delete_credit      = AccountTransaction::where('id', $data['current_balance_paid_through'][$i] + 1)->delete();
                                        }
                                    }
                                }
                            }
                        }
                    }
                //Insert into journal_entries End

                balanceUpdate($data['vendor_id']);

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return redirect()->route('bills_all_bills')->with("success","Purchase Updated Successfully !!");
                }
                else
                {
                    return redirect()->route('bills_show', $bill['id']);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function ProductEntriesList()
    {
        $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product    = $data->sortBy('name')->all();
        $products   = collect($product);

        return Response::json($products);
    }

    public function ProductEntriesListInvoice()
    {
        $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.stock_in_hand', '>', 0)
                                    ->Where('product_entries.stock_in_hand', '!=', null)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product    = $data->sortBy('name')->all();
        $products   = collect($product);

        return Response::json($products);
    }

    public function billListLoad()
    {
        $branch_id      = Auth::user()->branch_id;
        $data           = Bills::leftjoin('purchase_return', 'purchase_return.bill_id', 'bills.id')
                                ->leftjoin('tbl_clients', 'tbl_clients.id', 'bills.vendor_id')
                                ->leftjoin('customers', 'customers.client_id', 'tbl_clients.id')
                                ->leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                ->where('bills.type', 1)
                                ->where('bills.branch_id', $branch_id)
                                ->orderBy('bills.id', 'DESC')
                                ->select('bills.*',
                                         'purchase_return.id as return_id',
                                         'customers.contact_type as contact_type',
                                         'tbl_clients.ClientName as customer_name')
                                ->distinct('bills.id')
                                ->take(500)
                                ->get();

        return Response::json($data);
    }

    public function billListSearch($id)
    {
        $branch_id                   = Auth::user()->branch_id;
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data           = Bills::leftjoin('purchase_return', 'purchase_return.bill_id', 'bills.id')
                                ->leftjoin('tbl_clients', 'tbl_clients.id', 'bills.vendor_id')
                                ->leftjoin('customers', 'customers.client_id', 'tbl_clients.id')
                                ->leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                ->where('tbl_clients.ClientName', 'LIKE', "%$id%")
                                ->orWhere('bills.bill_date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                    return $query->orWhere('bills.bill_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('bills.id', 'DESC')
                                ->select('bills.*',
                                         'purchase_return.id as return_id',
                                         'customers.contact_type as contact_type',
                                         'tbl_clients.ClientName as customer_name')
                                ->distinct('bills.id')
                                ->take(100)
                                ->get();
        }
        else
        {
            $data           = Bills::leftjoin('purchase_return', 'purchase_return.bill_id', 'bills.id')
                                ->leftjoin('tbl_clients', 'tbl_clients.id', 'bills.vendor_id')
                                ->leftjoin('customers', 'customers.client_id', 'tbl_clients.id')
                                ->leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                ->where('bills.type', 1)
                                ->orderBy('bills.id', 'DESC')
                                ->select('bills.*',
                                         'purchase_return.id as return_id',
                                         'customers.contact_type as contact_type',
                                         'tbl_clients.ClientName as customer_name')
                                ->distinct('bills.id')
                                ->get();
        }
        
        $data = $data->where('branch_id', $branch_id);
        
        return Response::json($data);
    }

    public function storeProduct(Request $request)
    {
        $user_id                        = Auth::user()->id;
        $data                           = $request->all();

        DB::beginTransaction();

        try{
            $data_find                  = ProductEntries::orderBy('id', 'DESC')->first();
            $code                       = $data_find != null ? $data_find['product_code'] + 1 : 1;

            $product                    = new ProductEntries;
            $product->product_id        = $data['product_category_id'];
            $product->sub_category_id   = $data['product_sub_category_id'];
            $product->name              = $data['product_name'];
            $product->product_code      = $code;
            $product->sell_price        = $data['selling_price'];
            $product->buy_price         = $data['buying_price'];

            if ($data['unit_id'] != null)
            {
                $product->unit_id       = $data['unit_id'];
            }

            $product->status                        = $data['status'];
            $product->alert_quantity                = $data['alert_quantity'];
            $product->product_type                  = $data['product_type'];
            $product->created_by                    = $user_id;

            if ($product->save())
            {   
                DB::commit();
                return Response::json($product);
            }
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return Response::json(0);
        }
    }

    public function billProductList()
    {
        $data       = Products::orderBy('products.total_sold', 'DESC')
                                    ->select('products.*')
                                    ->get();

        return Response::json($data);
    }

    public function posSearchProductBill($id)
    {
        $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_code', $id)
                                    ->where('product_entries.product_id', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->first();

        return Response::json($data);
    }

    public function ProductEntriesListBill($id)
    {
        $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_id', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product    = $data->sortBy('name')->all();
        $products   = collect($product);

        return Response::json($products);
    }

    public function productListLoadBill($major_category_id)
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->when($major_category_id != 0, function ($query) use ($major_category_id) {
                                        return $query->where('product_entries.brand_id', $major_category_id);
                                    })
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.bosta) as bosta,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.name', 'ASC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->when($major_category_id != 0, function ($query) use ($major_category_id) {
                                        return $query->where('product_entries.brand_id', $major_category_id);
                                    })
                                    ->where('product_entries.name', 'LIKE', "$search%")
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.name', 'ASC')
                                    ->get();
        }

        foreach ($fetchData as $key => $value)
        {   
            if ($value['product_type'] == 2)
            {
                $variations  = ' - ' . $value['variations'];
            }
            else
            {
                $variations  = '';
            }

            if ($value['brand_name'] != null)
            {
                $brand  = ' - '.$value['brand_name'];
            }
            else
            {
                $brand  = '';
            }

            if ($value['product_code'] != null)
            {
                $code  = $value['product_code'];
            }
            else
            {
                $code  = '';
            }

            $name   = $value['name'] . $variations . ' ' . '( ' . $code . $brand . ' )';

            $data[] = array("id"=>$value['id'], "text"=>$name);
        }

        return Response::json($data);
    }

    public function getConversionParam($product_entry_id, $conversion_unit_id)
    {
        $data       = UnitConversions::where('unit_conversions.product_entry_id', $product_entry_id)
                                    ->where('unit_conversions.converted_unit_id', $conversion_unit_id)
                                    ->selectRaw('unit_conversions.*')
                                    ->first();

        return Response::json($data);
    }

    public function calculateOpeningBalance($customer_id)
    {
        $data  = calculateOpeningBalance($customer_id);

        return Response::json($data);
    }

    public function adjustAdvancePayment($customer_id)
    {
        $data  = Clients::find($customer_id);

        if ($data['balance'] < 0)
        {
            $result = abs($data['balance']);
        }
        else
        {
            $result = 0;
        }

        return Response::json($result);
    }

    public function supplierList()
    {
        $branch_id      = Auth()->user()->branch_id;

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Clients::where('IsTransactable', 1)
                                ->where('ActiveStatus', 1)
                                ->where('CompanyID', $branch_id)
                                ->where('PtnID', 264)
                                ->orderBy('PtnGroupCode', 'ASC')
                                ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Clients::where('IsTransactable', 1)
                                ->where('ActiveStatus', 1)
                                ->where('CompanyID', $branch_id)
                                ->where('PtnID', 264)
                                ->where('tbl_clients.ClientName', 'LIKE', "%$search%")
                                ->orderBy('PtnGroupCode', 'ASC')
                                ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "phone" =>'', "address" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['ClientName'], "phone" =>$value['ClientContactNumber'], "address" =>$value['ClientAddress']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function gets_voucherNumber_name($branch_id, $type) 
    {
        $voucher        = VoucherSummary::where('Type', $type)->orderBy('id', 'DESC')->first();
        $voucherNumber  = $voucher != null ? $voucher['VoucherNumber'] + 1 : 1;

        return $voucherNumber;
    }
}
