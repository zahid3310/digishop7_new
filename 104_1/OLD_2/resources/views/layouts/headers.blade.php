<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">{{__('messages.menu')}}</li>

                <li>
                    <a href="{{ route('home') }}" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span>{{ __('messages.dashboard')}}</span>
                    </a>
                </li>

                @if(Auth::user()->branch_id == 1)
                <li class="{{ 
                Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-suitcase"></i><span>{{ __('messages.receive_or_purchase_report')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('bills_index') }}">{{ __('messages.receive_purchase_item')}}</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' }}" href="{{ route('bills_all_bills') }}">{{ __('messages.list_of_receive/purchase')}}</a> </li>
                        <li> <a class="{{ Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' }}" href="{{ route('purchase_return_index') }}">{{ __('messages.receive/purchase_return')}}</a> </li>
                    </ul>
                </li>
                @endif

                @if(Auth()->user()->branch_id == 1)
                <li class="{{ 
                Route::currentRouteName() == 'productions_transfer_to_production_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_edit' ? 'mm-active' : '' ||  Route::currentRouteName() == 'productions_received_finished_goods_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'productions_daily_report_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'productions_daily_report_print' ? 'mm-active' : ''  }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'productions_transfer_to_production_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_edit' ? 'mm-active' : '' ||  Route::currentRouteName() == 'productions_received_finished_goods_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'productions_daily_report_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_daily_report_print' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-suitcase"></i><span>{{ __('messages.production')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li class="{{ Route::currentRouteName() == 'productions_transfer_to_production_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'productions_transfer_to_production_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                {{ __('messages.Send_Production')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('productions_transfer_to_production_create') }}">{{ __('messages.new_send')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'productions_transfer_to_production_edit' ? 'mm-active' : '' }}" href="{{ route('productions_transfer_to_production_index') }}">{{ __('messages.list_of_send')}}</a> </li>
                                <!-- <li> <a href="{{ route('productions_return_to_warehouse_index') }}">{{ __('messages.back_to_warehouse')}}</a> </li> -->
                            </ul>
                        </li>

                        <li class="{{ Route::currentRouteName() == 'productions_received_finished_goods_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'productions_received_finished_goods_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                {{ __('messages.rece_finished_goods')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('productions_received_finished_goods_create') }}">{{ __('messages.new_receive')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'productions_received_finished_goods_edit' ? 'mm-active' : '' }}" href="{{ route('productions_received_finished_goods_index') }}">List of Receive</a> </li>
                                <!-- <li> <a class="{{ Route::currentRouteName() == 'productions_damaged_finished_goods_index' ? 'mm-active' : '' }}" href="{{ route('productions_damaged_finished_goods_index') }}">Damage Finished Goods</a> </li> -->
                            </ul>
                        </li>

                        <li> <a class="{{ Route::currentRouteName() == 'productions_daily_report_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_daily_report_print' ? 'mm-active' : '' }}" href="{{ route('productions_daily_report_index') }}">Daily Report</a> </li>
                    </ul>
                </li>
                @endif

                <li class="{{ 
                Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-shopping-basket"></i><span>{{ __('messages.sales_to_sr/customer')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('invoices_index') }}">{{ __('messages.daily_sales')}}</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' }}" href="{{ route('invoices_all_sales') }}">{{ __('messages.list_of_sales')}}</a> </li>
                        <li> <a class="{{ Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' }}"  href="{{ route('sales_return_index') }}">{{ __('messages.sales_return')}}</a> </li>
                    </ul>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'cash_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'cash_receipt_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'bank_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'contra_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'journal_voucher_edit' ? 'mm-active' : '' ||
                Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' ||
                Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' }}">
                    <a class="{{ 
                Route::currentRouteName() == 'cash_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'cash_receipt_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'bank_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'contra_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'journal_voucher_edit' ? 'mm-active' : '' ||
                Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' ||
                Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fab fa-adn"></i><span>Accounts</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="{{ Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' }}" href="{{ route('payments_create').'?payment_type=0' }}">Receive Form Customer</a> </li>
                        <li> <a class="{{ Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' }}" href="{{ route('payments_create').'?payment_type=1' }}">Pay To Supplier</a> </li>
                        <li> <a class="" href="{{ route('cash_payment_voucher_index') }}">Cash Payment Voucher</a> </li>
                        <li> <a class="" href="{{ route('cash_receipt_voucher_index') }}">Cash Receipt Voucher</a> </li>
                        <li> <a class="" href="{{ route('bank_payment_voucher_index') }}">Bank Payment Voucher</a> </li>
                        <li> <a class="" href="{{ route('bank_receipt_voucher_index') }}">Bank Receipt Voucher</a> </li>
                        <li> <a class="" href="{{ route('journal_voucher_index') }}">Journal Voucher</a> </li>
                        <li> <a class="" href="{{ route('contra_voucher_index') }}">Contra Voucher</a> </li>
                        <li class=""> <a class="{{ 
                Route::currentRouteName() == 'cash_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'cash_receipt_voucher_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'bank_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'contra_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'journal_voucher_edit' ? 'mm-active' : '' }}" href="{{ route('voucher_posting_index') }}">Voucher Posting</a> </li>
                        <li> <a class="" href="{{ route('list_of_voucher') }}">List of Voucher</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="far fa-envelope"></i><span>{{ __('messages.messaging')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('messages_send_index') }}">{{ __('messages.send_meg')}}</a> </li>
                    </ul>
                </li>

                <li class="{{ Route::currentRouteName() == 'attendance_manual_attendance_index' ? 'mm-active' : '' }}">
                    <a class="{{ Route::currentRouteName() == 'attendance_manual_attendance_index' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-fingerprint"></i><span>Attendance</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('attendance_manual_attendance_index') }}">Manual Attendance</a> </li>
                        <li> <a href="{{ route('leave_application_create') }}">Add Leave Application</a> </li>
                        <li> <a href="{{ route('leave_application_index') }}">Leave Application List</a> </li>
                    </ul>
                </li>

                <li class="{{ Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'pay_slip_show' ? 'mm-active' : '' || Route::currentRouteName() == 'pay_slip_all_show' ? 'mm-active' : '' }}">
                    <a class="{{ Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'pay_slip_show' ? 'mm-active' : '' || Route::currentRouteName() == 'pay_slip_all_show' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-money-check-alt"></i><span>Payroll</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('process_monthly_salary_index') }}">List of Salary Sheet</a> </li>
                        <li> <a href="{{ route('process_monthly_salary_create') }}">Process Salary Sheet</a> </li>
                        <li> <a class="{{ Route::currentRouteName() == 'pay_slip_all_show' ? 'mm-active' : '' }}" href="{{ route('pay_slip_index') }}">Pay Slip</a> </li>
                        <li> <a class="{{ Route::currentRouteName() == 'pay_slip_show' ? 'mm-active' : '' }}" href="{{ route('pay_slip_list') }}">List of Pay Slip</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>{{ __('messages.reports')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('salary_statement_print_index') }}" target="_blank">Salary Statement Sheet</a> </li>
                        <li> <a href="{{ route('monthly_alary_report_index') }}">Monthly Salary Sheet</a> </li>
                        <li> <a href="{{ route('attendance_report_index') }}">Attendance Report</a> </li>
                        <li> <a href="{{ route('attendance_sheet_index') }}">Attendance Sheet</a> </li>
                        <li class="">
                            <a class="has-arrow waves-effect">
                                Production
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('production_report_index') }}">Production Report</a> </li>
                                <li> <a href="{{ route('finished_goods_report_index') }}">Finished Goods Report</a> </li>
                                <li> <a href="{{ route('production_finished_report_print') }}" target="_blank">Production Summary</a> </li>
                                <li> <a href="{{ route('production_profit_loss_index') }}">Production Profit & Loss</a> </li>
                                <li> <a href="{{ route('production_milling_charge_index') }}">Milling Charge Report</a> </li>
                            </ul>
                        </li>
                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.sales')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('sales_statement_index') }}">{{ __('messages.sales_statement')}}</a> </li>
                                <li> <a href="{{ route('sales_summary_index') }}">{{ __('messages.sales_summary')}}</a> </li>
                                <li> <a href="{{ route('product_wise_sales_report_index') }}">{{ __('messages.product_wise_sales')}}</a> </li>
                            </ul>
                        </li>

                        @if(Auth()->user()->branch_id == 1)
                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.receive_or_purchase_report')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('purchase_statement_index') }}">{{ __('messages.purchase_statement')}}</a> </li>
                                <li> <a href="{{ route('purchase_summary_index') }}">{{ __('messages.receive/purchase_summary')}}</a> </li>
                            </ul>
                        </li>
                        @endif

                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.mis')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <!-- <li> <a href="{{ route('custom_collection_report_index') }}">Daily Report</a> </li> -->
                                <li> <a href="{{ route('finished_stock_index') }}">Finished Stock</a> </li>
                                <li> <a href="{{ route('stock_report_index') }}">{{ __('messages.stock_status')}}</a> </li>
                                <!--<li> <a href="{{ route('stock_status_details_index') }}">Stock Status Details</a> </li>-->
                                @if(Auth::user()->branch_id == 1)
                                <li> <a href="{{ route('due_report_supplier_index') }}">{{ __('messages.supplier_Ledger')}}</a> </li>
                                @endif
                                <li> <a href="{{ route('due_report_customer_index') }}">{{ __('messages.SR/Customer_Ledger')}}</a> </li>
                                <!-- <li> <a href="{{ route('due_report_customer_print_due') }}">{{ __('messages.Customer_due_list')}}</a> </li> -->
                                @if(Auth::user()->branch_id == 1)
                                <!-- <li> <a href="{{ route('due_report_supplier_index_due') }}">{{ __('messages.Supplier_due_list')}}</a> </li> -->
                                @endif
                                <!-- <li> <a href="{{ route('due_ledger_index') }}">Due Ledger</a> </li> -->
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                {{ __('messages.basic_report')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                @if(Auth()->user()->branch_id == 1)
                                <li> <a href="{{ route('item_list_index') }}">{{ __('messages.list_items')}}</a> </li>
                                <li> <a href="{{ route('emergency_item_list_index') }}">{{ __('messages.emergency_purchase')}}</a> </li>
                                <li> <a href="{{ route('product_suppliers_index') }}">{{ __('messages.item_wise_supplier')}}</a> </li>
                                <li> <a href="{{ route('product_customers_index') }}">{{ __('messages.item_wise_SR/Customer')}}</a> </li>
                                @endif
                                <li> <a href="{{ route('register_list_index').'?type=0' }}" target="_blank">Customer Bank Account List</a> </li>
                                <li> <a href="{{ route('register_list_index').'?type=1' }}" target="_blank">Supplier Bank Account List</a> </li>
                            </ul>
                        </li>
                        
                        <!-- <li> <a href="#">List of Sending SMS</a> </li>
                        <li> <a href="{{ route('salary_report_index') }}">Salary Report</a> </li> -->
                    </ul>
                </li>

                <li class="">
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>Account Reports</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="" href="{{ route('accounts_report_current_balance_index') }}" target="_blank">Current Balance</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_accounts_voucher_index') }}">Accounts Voucher</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_cash_book_index') }}">Cash Book</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_bank_book_index') }}">Bank Book</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_ledger_book_index') }}">Ledger Book</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_balance_sheet_index') }}">Balance Sheet</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_income_statement_index') }}">Income Statement</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_trial_balance_index') }}">Trial Balance</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_cash_flow_index') }}">Cash Flow</a> </li>
                    </ul>
                </li>

                <li class="{{ 
                    Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || 
                    Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || 
                    Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || 
                    Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'users_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'users_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'department_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'department_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'section_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'section_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'products_areas_index' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'products_areas_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'leave_category_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'leave_category_create' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'shifts_index' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'shifts_create' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'duty_roster_index' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || 
                    Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || 
                    Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || 
                    Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'users_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'users_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'department_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'department_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'section_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'section_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_areas_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_areas_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'leave_category_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'leave_category_create' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'shifts_index' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'shifts_create' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'duty_roster_index' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>{{ __('messages.basic_settings')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        @if(Auth()->user()->branch_id == 1)
                        <li class="{{ Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                 {{ __('messages.product')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' }}" href="{{ route('products_index') }}">{{ __('messages.add_product')}}</a> </li>
                                <li> <a class="" href="{{ route('products_index_all') }}">{{ __('messages.list_product')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }}" href="{{ route('products_category_index') }}">{{ __('messages.add_categories')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' }}" href="{{ route('categories_index') }}" href="{{ route('categories_index') }}">{{ __('messages.add_major_categories')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' }}" href="{{ route('products_units_index') }}">{{ __('messages.add_unit_measure')}}</a> </li>
                                <!--<li> <a class="{{ Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' }}" href="{{ route('products_variations_index') }}">{{ __('messages.product_variations')}}</a> </li>-->
                                <!--<li> <a href="{{ route('products_barcode_print') }}">{{ __('messages.print_barcode')}}</a> </li>-->
                                <li> <a href="{{ route('products_opening_stock') }}">{{ __('messages.bulk_opening_stock')}}</a> </li>
                                <!--<li> <a href="{{ route('products_bulk_product_list_update') }}">{{ __('messages.bulk_product_update')}}</a> </li>-->
                                <!--<li> <a href="{{ route('add_customer_opening_balance') }}">Customer Opening Balance</a> </li>-->
                                <!--<li> <a href="{{ route('add_supplier_opening_balance') }}">Supplier Opening Balance</a> </li>-->
                                
                            </ul>
                        </li>
                        @endif

                        <li class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'section_index' ? 'mm-active' : '' || Route::currentRouteName() == 'section_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'department_index' ? 'mm-active' : '' || Route::currentRouteName() == 'department_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_areas_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_areas_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'section_index' ? 'mm-active' : '' || Route::currentRouteName() == 'section_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'department_index' ? 'mm-active' : '' || Route::currentRouteName() == 'department_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_areas_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_areas_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                 {{ __('messages.registers')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=0' }}">{{ __('messages.add_sr/customer')}}</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=1' }}">{{ __('messages.add_supplier')}}</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=2' }}">{{ __('messages.add_employee')}}</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=3' }}">{{ __('messages.add_reference')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_areas_edit' ? 'mm-active' : '' }}" href="{{ route('products_areas_index') }}">{{ __('messages.add_area')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'department_edit' ? 'mm-active' : '' }}" href="{{ route('department_index') }}">Add Department</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'section_edit' ? 'mm-active' : '' }}" href="{{ route('section_index') }}">Add Section</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Chart of Accounts
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="" href="{{ route('chart_of_accounts_index') }}">Chart of Accounts Head</a> </li>
                                <li> <a class="" href="{{ route('chart_of_projects_index') }}">Chart of Projects</a> </li>
                                <li> <a class="" href="{{ route('chart_of_registers_index') }}">Chart of Registers</a> </li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow waves-effect">
                                {{ __('messages.messaging')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('messages_index') }}">{{ __('messages.create_message')}}</a> </li>
                                <li> <a href="{{ route('messages_phone_book_index') }}">{{ __('messages.pb')}}</a> </li>
                            </ul>
                        </li>

                        @if(Auth()->user()->branch_id == 1)
                        <li>
                            <a class="has-arrow waves-effect">
                                {{ __('messages.security_system')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('branch_index') }}">{{ __('messages.add_branch')}}</a> </li>
                                <li> <a href="{{ route('users_index') }}">{{ __('messages.add_user')}}</a> </li>
                                <li> <a href="{{ route('users_index_all') }}">{{ __('messages.list_user')}}</a> </li>
                                <li> <a href="{{ route('set_access_index') }}">{{ __('messages.permission')}}</a> </li>
                            </ul>
                        </li>
                        @endif
                         
                        <li class="{{ Route::currentRouteName() == 'salary_grades_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_grades_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' ||
                                      Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' ||
                                      Route::currentRouteName() == 'salary_increaments_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_increaments_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'salary_grades_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_grades_create' ? 'mm-active' : ''|| Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'salary_increaments_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_increaments_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Payroll Settings
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li class="{{ Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' }}">
                                    <a class="{{ Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                        Grades
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="{{ Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' }}" href="{{ route('salary_grades_index') }}">List of Grades</a> </li>
                                        <li> <a href="{{ route('salary_grades_create') }}">New Grade</a> </li>
                                    </ul>
                                </li>

                                <li class="{{ Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' }}">
                                    <a class="{{ Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                        Salary Statements
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="{{ Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' }}" href="{{ route('salary_statements_index') }}">List of Statements</a> </li>
                                        <li> <a href="{{ route('salary_statements_create') }}">New Statement</a> </li>
                                    </ul>
                                </li>

                                <!-- <li class="{{ Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : '' }}">
                                    <a class="{{ Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                        Increaments
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="{{ Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : '' }}" href="{{ route('salary_increaments_index') }}">List of Increaments</a> </li>
                                        <li> <a href="{{ route('salary_increaments_create') }}">Add Increament</a> </li>
                                    </ul>
                                </li> -->
                            </ul>
                        </li>
                        
                        <li>
                            <a class="has-arrow waves-effect">
                                Holiday Calender
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('holiday_calender_index') }}">List of Holidays</a> </li>
                                <li> <a href="{{ route('holiday_calender_create') }}">Add Holiday</a> </li>
                            </ul>
                        </li>
                        
                        <li class="{{ Route::currentRouteName() == 'shifts_index' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'shifts_create' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'shifts_index' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'shifts_create' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Shift
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : '' }}" href="{{ route('shifts_index') }}">List of Shift</a> </li>
                                <li> <a href="{{ route('shifts_create') }}">Add Shift</a> </li>
                            </ul>
                        </li>
                        
                        <li class="{{ Route::currentRouteName() == 'duty_roster_index' ? 'mm-active' : '' || Route::currentRouteName() == 'general_roster_index' ? 'mm-active' : ''  }}">
                            <a class="{{ Route::currentRouteName() == 'duty_roster_index' ? 'mm-active' : '' || Route::currentRouteName() == 'general_roster_index' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Duty Roster
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('duty_roster_index') }}">Add Roster</a> </li>
                            </ul>
                        </li>
                        
                    </ul>
                </li>

                <!-- <li class="menu-title">Financial Accounts</li>

                <li class="">
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>Admin</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="" href="{{ route('chart_of_accounts_index') }}">Chart of Accounts Head</a> </li>
                        <li> <a class="" href="{{ route('chart_of_projects_index') }}">Chart of Projects</a> </li>
                        <li> <a class="" href="{{ route('chart_of_registers_index') }}">Chart of Registers</a> </li>
                    </ul>
                </li> -->
            </ul>
        </div>
    </div>
</div>