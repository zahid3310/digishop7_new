<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class DailyAttendance extends Model
{  
    protected $table = "daily_attendance";
    
    public function client()
    {
        return $this->belongsTo('App\Models\Clients','user_id');
    }
    
    public function shift()
    {
        return $this->belongsTo('App\Models\Shifts','shift_id');
    }
}
