@extends('layouts.app')

@section('title', 'Customer Opening Balance')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Customer Opening Balance </h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Opening Balance</a></li>
                                    <li class="breadcrumb-item active">Customer Opening Balance</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif
                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif
                                
                                <form method="get" action="{{ route('add_customer_opening_balance') }}" enctype="multipart/form-data">
                                    <div class="row">
                                        
                                        <div class="col-md-4 form-group">
                                            <label style="text-align: left" for="productname">Search By Customer</label>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select style="width: 100%" id="customer_id" name="customer_id" class="form-control select2">
                                                       <option value="0">{{ __('messages.all')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4 form-group">
                                            <label style="text-align: left" for="productname">Search By Reference </label>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <select style="width: 100%" id="reference_id" name="reference_id" class="form-control select2">
                                                       <option value="0">{{ __('messages.all')}}</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">{{ __('messages.search')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </form>
                                
                                <hr>

                                <form id="FormSubmit" action="{{ route('update_opening_balance') }}" method="post" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}
      
                                @if(!empty($customers) && ($customers->count() > 0))
                                    @foreach($customers as $key => $value)
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-md-3 col-form-label">{{ $loop->index+1}} .  {{ $value['name'] }}</label>
                                            <label for="example-text-input" class="col-md-3 col-form-label">{{ $value['phone'] }}</label>
                                            <label for="example-text-input" class="col-md-3 col-form-label">{{ $value['address'] }}</label>
                                            
                                            <div class="col-md-3">
                                                <input class="form-control" type="number" name="opening_balance[]" id="opening_balance" placeholder="Opening Balance" value="{{ $value['opening_balance'] }}" required>
                                                <input class="form-control" type="hidden" value="{{ $value['id'] }}" name="id[]">
                                            </div>
                                            
                                        </div>
                                    @endforeach

                                    <div class="form-group row">
                                        <div class="col-md-4"></div>
                                        <div class="button-items col-md-2 pull-right">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">{{ __('messages.save')}}</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('home') }}">{{ __('messages.close')}}</a></button>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                @else
                                <div class="form-group row">
                                    <div style="text-align: center" class="col-md-12">
                                        <h6>No Customer Available For Adding Opening Balance.</h6>
                                    </div>
                                </div>
                                @endif
                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }
        
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if ((result['contact_type'] == 0) && result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });
            
            $("#reference_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 3)
                    {
                        return result['text'];
                    }
                },
            });
        });
    </script>
@endsection