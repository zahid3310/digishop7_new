<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('attendance/import-user')->group(function() {
    Route::get('/', 'AttendanceController@index')->name('attendance_import_user_index');
    Route::get('/import-user', 'AttendanceController@importUser')->name('attendance_import_user');
    Route::post('/import-user-post', 'AttendanceController@importUserStore')->name('attendance_import_user_store');
    Route::post('/update-user-post', 'AttendanceController@importUserUpdate')->name('attendance_import_user_update');
    Route::get('/delete-user/{id}', 'AttendanceController@deleteUser')->name('attendance_delete_user');
	Route::get('/pull-data', 'AttendanceController@pullData')->name('attendance_pull_data');
});

Route::prefix('attendance/devices')->group(function() {
    Route::get('/', 'AttendanceController@deviceList')->name('attendance_devices_index');
    Route::post('/store', 'AttendanceController@deviceStore')->name('attendance_devices_store');
});

Route::prefix('attendance/manual-attendance')->group(function() {
    Route::get('/', 'AttendanceController@manualAttendanceIndex')->name('attendance_manual_attendance_index');
    Route::post('/store', 'AttendanceController@manualAttendanceStore')->name('attendance_manual_attendance_store');
});

Route::prefix('attendance/leave-category')->group(function() {
    Route::get('/', 'AttendanceController@leaveCategoryIndex')->name('leave_category_index');
    Route::get('/create', 'AttendanceController@leaveCategoryCreate')->name('leave_category_create');
    Route::post('/store', 'AttendanceController@leaveCategoryStore')->name('leave_category_store');
    Route::get('/edit/{id}', 'AttendanceController@leaveCategoryEdit')->name('leave_category_edit');
    Route::post('/update/{id}', 'AttendanceController@leaveCategoryUpdate')->name('leave_category_update');
});

Route::prefix('attendance/leave-application')->group(function() {
    Route::get('/', 'AttendanceController@leaveApplicationIndex')->name('leave_application_index');
    Route::get('/create', 'AttendanceController@leaveApplicationCreate')->name('leave_application_create');
    Route::post('/store', 'AttendanceController@leaveApplicationStore')->name('leave_application_store');
    Route::get('/edit/{id}', 'AttendanceController@leaveApplicationEdit')->name('leave_application_edit');
    Route::post('/update/{id}', 'AttendanceController@leaveApplicationUpdate')->name('leave_application_update');
    Route::get('/get-de-data/{employee_id}', 'AttendanceController@getDeData');
});

Route::prefix('attendance/holiday-calender')->group(function() {
    Route::get('/', 'AttendanceController@holidayCalenderIndex')->name('holiday_calender_index');
    Route::get('/create', 'AttendanceController@holidayCalenderCreate')->name('holiday_calender_create');
    Route::post('/store', 'AttendanceController@holidayCalenderStore')->name('holiday_calender_store');
    Route::get('/edit/{id}', 'AttendanceController@holidayCalenderEdit')->name('holiday_calender_edit');
    Route::post('/update/{id}', 'AttendanceController@holidayCalenderUpdate')->name('holiday_calender_update');
});

Route::prefix('attendance/configuration')->group(function() {
    Route::get('/edit', 'AttendanceController@attendanceConfigurationEdit')->name('attendance_configuration_edit');
    Route::post('/update', 'AttendanceController@attendanceConfigurationUpdate')->name('attendance_configuration_update');
});

Route::prefix('attendance/shifts')->group(function() {
    Route::get('/', 'AttendanceController@shiftsIndex')->name('shifts_index');
    Route::get('/create', 'AttendanceController@shiftsCreate')->name('shifts_create');
    Route::post('/store', 'AttendanceController@shiftsStore')->name('shifts_store');
    Route::get('/edit/{id}', 'AttendanceController@shiftsEdit')->name('shifts_edit');
    Route::post('/update/{id}', 'AttendanceController@shiftsUpdate')->name('shifts_update');
});

Route::prefix('attendance/duty-roster')->group(function() {
    Route::get('/', 'AttendanceController@dutyRosterIndex')->name('duty_roster_index');
    Route::post('/store', 'AttendanceController@dutyRosterStore')->name('duty_roster_store');
    Route::get('/get-all-section', 'AttendanceController@getAllSection');
    Route::get('/get-all-department', 'AttendanceController@getAllDepartment');
});