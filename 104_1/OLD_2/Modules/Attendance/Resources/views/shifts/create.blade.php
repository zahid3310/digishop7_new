@extends('layouts.app')

@section('title', 'Shifts')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Shifts</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Basic Settings</a></li>
                                    <li class="breadcrumb-item active">Shifts</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('shifts_store') }}" method="post" files="true" enctype="multipart/form-data">
            					{{ csrf_field() }}

                                <div style="margin-bottom: 0px !important" class="form-group row">
                                    <div class="col-md-3 form-group">
                                        <label for="productname">Shift Title *</label>
                                        <input class="form-control" type="test" id="name" name="name" required>
                                    </div>
                                    
                                    <div class="col-md-3 form-group">
                                        <label for="productname">Start Time *</label>
                                        <input class="form-control" type="time" id="start_time" name="start_time" min="00:00" max="24:00" required>
                                    </div>
                                    
                                    <div class="col-md-3 form-group">
                                        <label for="productname">End Time *</label>
                                        <input class="form-control" type="time" id="end_time" name="end_time" min="00:00" max="24:00" required>
                                    </div>
                                    
                                    <div class="col-md-3 form-group">
                                        <label for="productname">Late In Time *</label>
                                        <input class="form-control" type="time" id="late_in_time" name="late_in_time" min="00:00" max="24:00">
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname">Duty Hour *</label>
                                        <input class="form-control" type="number" id="duty_hour" name="duty_hour" required>
                                    </div>
                                    
                                    <div class="col-md-3 form-group">
                                        <label for="productname">Overtime Start</label>
                                        <input class="form-control" type="time" id="overtime_start" name="overtime_start" min="00:00" max="24:00">
                                    </div>
                                    
                                    <div class="col-md-3 form-group">
                                        <label for="productname">Overtime End</label>
                                        <input class="form-control" type="time" id="overtime_end" name="overtime_end" min="00:00" max="24:00">
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                    	<button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                    	<button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('shifts_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection