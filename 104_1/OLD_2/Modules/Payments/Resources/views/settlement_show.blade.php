@extends('layouts.app')

@section('title', 'Show Payment')
<style type="text/css">
    address {
     margin-bottom: 0px!important;}
</style>
@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.payment')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.payment')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.print')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card" style="border: 1px solid #0000003b;border-top-left-radius: 25px;border-bottom-left-radius: 25px;border-top-right-radius: 25px;border-bottom-right-radius: 25px;background: aliceblue;">
                            <div class="card-body" style="margin:20px;">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 32px;padding-top: 10px;font-weight: bold;color: #a74d4d;">সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</h2>
                                        <p style="line-height: 1.2;font-size: 20px;font-weight: bold" class="text-center">{{ $user_info['organization_name'] }}</p>

                                        <p style="line-height: 1.2;font-size: 20px;font-weight: bold" class="text-center">বাঁকা ব্রিক্স ফিল্ড, জীবননগর, চুয়াডাঙ্গা ।</p>

                                        <p style="line-height: 0.5;font-size: 20px;font-weight: bold;color: #8f6f0f;" class="text-center"><strong>মোবাঃ </strong>০১৬৮৮ ৯৩৯৩৯৩, ০১৯১৬ ০৪১১২৯১, ০১৭১৫ ৭০৪১২৬</p>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 15px;float: right;color: #a74d4d; font-weight: 700;">গ্রাহক কপি</span><br>
                                        <span style="border: 1px solid gray;padding: 5px;font-size: 15px;float: right;background: #a74d4d; color: #fff; font-weight: 700;width: 190px;margin-top: 7px;">
                                                @if(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 0))
                                                জমার প্রমাণপত্র
                                                @elseif(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 1))
                                                পেমেন্ট প্রমাণপত্র
                                                @endif   </span>
                                        <span style="border: 1px solid gray;padding: 5px;font-size: 15px;float: right;background: #a74d4d57; color: #000; font-weight: 700;width: 190px;border-right: 1px solid;"><strong>টাকা - </strong> {{eng2bang($find_settlement->amount)}} /-</span>
                                    </div>
                                </div>

                                <p style="text-align: center;margin-top: 10px;"><span style="border: 2px solid #fff;border-top-left-radius: 25px;border-bottom-left-radius: 25px;border-top-right-radius: 25px;border-bottom-right-radius: 25px;padding: 0px 23px 0px 23px;font-size: 25px;background: #a74d4d;color: #fff;"> সেটেলমেন্ট রশিদ</span> </p>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <address  style="font-size: 18px;">
                                            <strong style="color: #1d1032;">
                                            @if(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 0))
                                                কাস্টমারের নাম :
                                            @elseif(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 1))
                                            সরবরাহকারীর নাম :
                                            @endif
                                            </strong>
                                            {{ $find_settlement['customer_name'] }} <br>
                                        </address>
                                    </div>                                    


                                    <div class="col-sm-6 text-sm-right">
                                        <address class="mt-2 mt-sm-0"  style="font-size: 18px;">
                                            <strong style="font-size: 18px;color: #1d1032;">
                                                @if(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 0))
                                                জমার তাং
                                                @elseif(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 1))
                                                পেমেন্ট তাং
                                                @endif
            :
                                            </strong>{{ date('d-m-Y', strtotime($find_settlement['payment_date'])) }}<br><br>
                                        </address>
                                    </div>


                                    <div class="col-sm-12" style="margin-bottom: 35px;line-height: 0px;">
                                        <address  style="font-size: 18px;">
                                            <strong style="color: #1d1032;">প্রোপাইটর নাম : </strong> {{ $find_settlement['proprietor_name'] }} <br>
                                        </address>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6" style="margin-top: -10px;">
                                        <address style="font-size: 18px;">
                                            <strong style="font-size: 18px;color: #1d1032;">ঠিকানা  : </strong>@if($find_settlement['address'] != null){{$find_settlement['address']}}@endif<br>
                                        </address>
                                    </div>

                                    <div class="col-sm-6 text-sm-right" style="margin-top: -10px;">
                                        <address class="mt-2 mt-sm-0" style="font-size: 18px;">
                                            <strong style="color: #1d1032;">মোবাইল নং : </strong>{{ $find_settlement['phone'] }}<br><br>
                                        </address>
                                    </div>
                                </div>


                                <div class="py-2 mt-3" style="display:none">
                                    <h3 class="font-size-15 font-weight-bold">{{ __('messages.payment_details')}}</h3>
                                </div>

                                <table class="table" style="line-height: 0.9;border-collapse: collapse; border-spacing: 0; width: 100%;">
                                   
                                    <thead>
                                        <tr>

                                            <th style="text-align: left;width: 70%;border: 1px solid #a74d4d!important;background: #a74d4d!important;color: #fff;">
                                                @if(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 0))
                                                জমার বিবরণ
                                                @elseif(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 1))
                                                পেমেন্ট বিবরণ
                                                @endif
                                                
                                                </th>
                                            <th style="text-align: left;width: 70%;border: 1px solid #a74d4d!important;background: #a74d4d!important;color: #fff;">টাকা</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            
                                            <td style="border: 1px solid #a74d4d!important;">{{  $find_settlement['note']  }}</td>
                                            <td style="text-align: right;border: 1px solid #a74d4d!important;">{{eng2bang(number_format($find_settlement['amount'],0,'.',',')) }}</td>
                                        </tr>

                                        <tr>
                                            <td colspan="1" style="border: 1px solid #a74d4d!important;"><strong style="color:#1d1032">কথায় :</strong> {{$inwordsAmount}} টাকা মাত্র <strong style="float: right;    float: right;border: 2px solid #fff;border-top-left-radius: 25px;border-bottom-left-radius: 25px;border-top-right-radius: 25px;border-bottom-right-radius: 25px;padding: 7px 14px 4px 12px;font-size: 16px;background: #1d1032;color: #fff;margin-top: -12px;">সর্বমোট </strong></td>
                                            <td style="text-align: right;border: 1px solid #a74d4d!important;">{{eng2bang($find_settlement['amount'])}}</td>
                                            
                                        </tr>         
                                    </tbody>
                                        
                                </table>


                                @if($find_settlement['note'] != null)
                                <br>
                                <div class="row">
                                    <div class="col-md-12" style="font-size: 16px;">
                                        <strong style="color: #a74d4d">জমার মাধ্যম </strong> {{$find_settlement->account_name }}
                                    </div>
                                </div>

                                <br>
                                <br>
                                @endif
                                
                                    <div class="row">

                                        <div style="text-align: left" class="col-md-3 col-sm-3">
                                            <span style="border-top: 1px solid #a74d4d;color: #a74d4d"> প্রদানকারীর স্বাক্ষর  </span><br><br>
                                        </div>
                                        
                                        <div style="text-align: left" class="col-md-3 col-sm-3">
                                            <span style="border-top: 1px solid #a74d4d;color: #a74d4d"> কম্পিউটার অপারেটর স্বাক্ষর  </span><br><br>
                                        </div>

                                        <div style="text-align: center;" class="col-md-3 col-sm-3">
                                            
                                            <span style="border-top: 1px solid #a74d4d;color: #a74d4d"> জেনারেল ম্যানেজার  </span>
                                        </div>

                                        <div style="text-align: right" class="col-md-3 col-sm-3">

                                            <span style="border-top: 1px solid #a74d4d;color: #a74d4d"> পক্ষে- সাথী অটো রাইস মিলস (প্রা.) লিঃ</span>
                                        </div>
                                    </div>


                                <div class="d-print-none" style="display:none;">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="position: fixed;bottom: 0">

                                    @if($user_info['footer_image'] != null)
                                        <img class="float-left" src="{{ url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card" style="border: 1px solid #0000003b;border-top-left-radius: 25px;border-bottom-left-radius: 25px;border-top-right-radius: 25px;border-bottom-right-radius: 25px;background: aliceblue;">
                            <div class="card-body" style="margin:20px;">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 32px;padding-top: 10px;font-weight: bold;color: #a74d4d;">সাথী অটো রাইস মিলস (প্রাঃ) লিঃ</h2>
                                        <p style="line-height: 1.2;font-size: 20px;font-weight: bold" class="text-center">{{ $user_info['organization_name'] }}</p>

                                        <p style="line-height: 1.2;font-size: 20px;font-weight: bold" class="text-center">বাঁকা ব্রিক্স ফিল্ড, জীবননগর, চুয়াডাঙ্গা ।</p>

                                        <p style="line-height: 0.5;font-size: 20px;font-weight: bold;color: #8f6f0f;" class="text-center"><strong>মোবাঃ </strong>০১৬৮৮ ৯৩৯৩৯৩, ০১৯১৬ ০৪১১২৯১, ০১৭১৫ ৭০৪১২৬</p>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 15px;float: right;color: #a74d4d; font-weight: 700;">গ্রাহক কপি</span><br>
                                        <span style="border: 1px solid gray;padding: 5px;font-size: 15px;float: right;background: #a74d4d; color: #fff; font-weight: 700;width: 190px;margin-top: 7px;">
                                                @if(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 0))
                                                জমার প্রমাণপত্র
                                                @elseif(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 1))
                                                পেমেন্ট প্রমাণপত্র
                                                @endif   </span>
                                        <span style="border: 1px solid gray;padding: 5px;font-size: 15px;float: right;background: #a74d4d57; color: #000; font-weight: 700;width: 190px;border-right: 1px solid;"><strong>টাকা - </strong> {{eng2bang($find_settlement->amount)}} /-</span>
                                    </div>
                                </div>

                                <p style="text-align: center;margin-top: 10px;"><span style="border: 2px solid #fff;border-top-left-radius: 25px;border-bottom-left-radius: 25px;border-top-right-radius: 25px;border-bottom-right-radius: 25px;padding: 0px 23px 0px 23px;font-size: 25px;background: #a74d4d;color: #fff;"> সেটেলমেন্ট রশিদ</span> </p>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <address  style="font-size: 18px;">
                                            <strong style="color: #1d1032;">
                                            @if(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 0))
                                                কাস্টমারের নাম :
                                            @elseif(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 1))
                                            সরবরাহকারীর নাম :
                                            @endif
                                            </strong>
                                            {{ $find_settlement['customer_name'] }} <br>
                                        </address>
                                    </div>                                    


                                    <div class="col-sm-6 text-sm-right">
                                        <address class="mt-2 mt-sm-0"  style="font-size: 18px;">
                                            <strong style="font-size: 18px;color: #1d1032;">
                                                @if(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 0))
                                                জমার তাং
                                                @elseif(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 1))
                                                পেমেন্ট তাং
                                                @endif
            :
                                            </strong>{{ date('d-m-Y', strtotime($find_settlement['payment_date'])) }}<br><br>
                                        </address>
                                    </div>


                                    <div class="col-sm-12" style="margin-bottom: 35px;line-height: 0px;">
                                        <address  style="font-size: 18px;">
                                            <strong style="color: #1d1032;">প্রোপাইটর নাম : </strong> {{ $find_settlement['proprietor_name'] }} <br>
                                        </address>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6" style="margin-top: -10px;">
                                        <address style="font-size: 18px;">
                                            <strong style="font-size: 18px;color: #1d1032;">ঠিকানা  : </strong>@if($find_settlement['address'] != null){{$find_settlement['address']}}@endif<br>
                                        </address>
                                    </div>

                                    <div class="col-sm-6 text-sm-right" style="margin-top: -10px;">
                                        <address class="mt-2 mt-sm-0" style="font-size: 18px;">
                                            <strong style="color: #1d1032;">মোবাইল নং : </strong>{{ $find_settlement['phone'] }}<br><br>
                                        </address>
                                    </div>
                                </div>


                                <div class="py-2 mt-3" style="display:none">
                                    <h3 class="font-size-15 font-weight-bold">{{ __('messages.payment_details')}}</h3>
                                </div>

                                <table class="table" style="line-height: 0.9;border-collapse: collapse; border-spacing: 0; width: 100%;">
                                   
                                    <thead>
                                        <tr>

                                            <th style="text-align: left;width: 70%;border: 1px solid #a74d4d!important;background: #a74d4d!important;color: #fff;">
                                                @if(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 0))
                                                জমার বিবরণ
                                                @elseif(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 1))
                                                পেমেন্ট বিবরণ
                                                @endif
                                                
                                                </th>
                                            <th style="text-align: left;width: 70%;border: 1px solid #a74d4d!important;background: #a74d4d!important;color: #fff;">টাকা</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            
                                            <td style="border: 1px solid #a74d4d!important;">{{  $find_settlement['note']  }}</td>
                                            <td style="text-align: right;border: 1px solid #a74d4d!important;">{{eng2bang(number_format($find_settlement['amount'],0,'.',',')) }}</td>
                                        </tr>

                                        <tr>
                                            <td colspan="1" style="border: 1px solid #a74d4d!important;"><strong style="color:#1d1032">কথায় :</strong> {{$inwordsAmount}} টাকা মাত্র <strong style="float: right;    float: right;border: 2px solid #fff;border-top-left-radius: 25px;border-bottom-left-radius: 25px;border-top-right-radius: 25px;border-bottom-right-radius: 25px;padding: 7px 14px 4px 12px;font-size: 16px;background: #1d1032;color: #fff;margin-top: -12px;">সর্বমোট </strong></td>
                                            <td style="text-align: right;border: 1px solid #a74d4d!important;">{{eng2bang($find_settlement['amount'])}}</td>
                                            
                                        </tr>         
                                    </tbody>
                                        
                                </table>


                                @if($find_settlement['note'] != null)
                                <br>
                                <div class="row">
                                    <div class="col-md-12" style="font-size: 16px;">
                                        <strong style="color: #a74d4d">জমার মাধ্যম </strong> {{$find_settlement->account_name }}
                                    </div>
                                </div>

                                <br>
                                <br>
                                @endif
                                
                                    <div class="row">

                                        <div style="text-align: left" class="col-md-3 col-sm-3">
                                            <span style="border-top: 1px solid #a74d4d;color: #a74d4d"> প্রদানকারীর স্বাক্ষর  </span><br><br>
                                        </div>
                                        
                                        <div style="text-align: left" class="col-md-3 col-sm-3">
                                            <span style="border-top: 1px solid #a74d4d;color: #a74d4d"> কম্পিউটার অপারেটর স্বাক্ষর  </span><br><br>
                                        </div>

                                        <div style="text-align: center;" class="col-md-3 col-sm-3">
                                            
                                            <span style="border-top: 1px solid #a74d4d;color: #a74d4d"> জেনারেল ম্যানেজার  </span>
                                        </div>

                                        <div style="text-align: right" class="col-md-3 col-sm-3">

                                            <span style="border-top: 1px solid #a74d4d;color: #a74d4d"> পক্ষে- সাথী অটো রাইস মিলস (প্রা.) লিঃ</span>
                                        </div>
                                    </div>


                                <div class="d-print-none" style="display:none;">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="position: fixed;bottom: 0">

                                    @if($user_info['footer_image'] != null)
                                        <img class="float-left" src="{{ url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>


            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection