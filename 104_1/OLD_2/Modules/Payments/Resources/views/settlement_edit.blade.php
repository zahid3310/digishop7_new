@extends('layouts.app')

<?php
    if (isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 1))
    {
        $title = 'Supplier Settlement';
    }

    if (isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 0))
    {
        $title = 'Customer Settlement';
    }
?>

@section('title', $title)

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.settlement')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.settlement')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.settlement')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <form id="FormSubmit" action="{{ route('settlements_update', $find_settlement['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
                        <div class="card">
                            <div style="height: 250px;padding-bottom: 0px" class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
                                            <label class="control-label">{{ __('messages.contact_type')}} *</label>
                                            <select id="type" style="width: 100%;cursor: pointer" class="form-control" name="type">
                                                @if(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 1))
                                                <option value="1">Supplier</option>
                                                @endif
                                                @if(isset($_GET['settlement_type']) && ($_GET['settlement_type'] == 0))
                                                <option value="0">Customer</option>
                                                @endif
                                                
                                                @if(!isset($_GET['settlement_type']))
                                                <option value="0">Customers</option>
                                                <option value="1">Supplier</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                    	<div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
	                                        <label class="control-label">{{ __('messages.search_contact')}} *</label>
	                                        <select id="contact_id" name="customer_id" style="width: 100%;cursor: pointer" class="form-control select2 contact_id" onchange="searchContact()">
	                                        	<option value="">--{{ __('messages.select_contact')}}--</option>
												@if(!empty($customers))
                                                    <?php 
                                                        if (isset($_GET['settlement_type']))
                                                        {
                                                            $customers = $customers->where('contact_type', $_GET['settlement_type']);
                                                        }
                                                        else
                                                        {
                                                            $customers = $customers;
                                                        }
                                                    ?>
													@foreach($customers as $key => $customer)
													<option @if(isset($find_customer)) {{ $find_customer['id'] == $customer['id'] ? 'selected' : '' }} @endif value="{{ $customer->id }}" {{ $customer->id == $find_settlement->customer_id ? 'selected' : '' }}>{{ $customer->name }} {{ $customer->phone != null ? ' | ' . $customer->phone : ''}}</option>
													@endforeach
												@endif
	                                        </select>
	                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
                        <div class="card">
                            <div style="padding-bottom: 0px" class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

        						<div class="row">
    			                	<div class="col-sm-6">
    			                        <div class="form-group">
    			                            <label for="settlement_date">{{ __('messages.settlement')}} {{ __('messages.date')}} *</label>
    			                            <input id="settlement_date" name="settlement_date" type="text" value="{{ date('d-m-Y', strtotime($find_settlement->date)) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
    			                        </div>
    			                    </div>

    			                    <div class="col-sm-6">
    			                        <div class="form-group">
    			                            <label for="amount">{{ __('messages.amount')}} *</label>
    			                            <input id="amount" name="amount" type="text" class="form-control" value="{{ $find_settlement->amount }}">
    			                        </div>
    			                    </div>

    			                    <div class="col-sm-6 form-group">
    		                            <label class="control-label">{{ __('messages.paid_through')}}</label>
                                        <select style="cursor: pointer" name="paid_through" class="form-control select2">
                                            @if(!empty($paid_accounts) && ($paid_accounts->count() > 0))
                                            @foreach($paid_accounts as $key => $paid_account)
                                                <option value="{{ $paid_account['id'] }}" {{ $find_paid_account['account_id'] == $paid_account->id ? 'selected' : '' }}>{{ $paid_account['account_name'] }}</option>
                                            @endforeach
                                            @endif
                                        </select>
    		                        </div>

    		                        <div class="col-sm-6">
    			                        <div class="form-group">
    			                            <label for="note">{{ __('messages.note')}}</label>
    			                            <input id="note" name="note" type="text" class="form-control" value="{{ $find_settlement->note }}">
    			                        </div>
    			                    </div>
    		                	</div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">{{ __('messages.update')}}</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('settlements_create') }}">{{ __('messages.close')}}</a></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </form>

                <hr style="margin-top: 0px">

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">{{ __('messages.settlement_list')}}</h4>

                                <div style="margin-right: 10px" class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-1">Search : </div>
                                    <div class="col-md-2">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.sl')}}</th>
                                            <th>{{ __('messages.date')}}</th>
                                            <th>{{ __('messages.contact')}}</th>
                                            <th>{{ __('messages.paid_through')}}</th>
                                            <th>{{ __('messages.note')}}</th>
                                            <th>{{ __('messages.amount')}}</th>
                                            <th>{{ __('messages.action')}}</th>
                                        </tr>
                                    </thead>

                                    <tbody id="settlement_list"></tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url    = $('.site_url').val();
            var contact_id  = $('#contact_id').val();

            if (contact_id == '')
            {
                var contact_id_find = 0;
            }
            else
            {
                var contact_id_find = $('#contact_id').val();
            }

            $.get(site_url + '/payments/settlements/get-settlement-data/' + contact_id_find, function(data){

                settlementList(data);
                
            });
        });

        function searchContact()
        {
            var site_url    = $('.site_url').val();
            var contact_id  = $('#contact_id').val();

            $.get(site_url + '/payments/settlements/get-balance/' + contact_id, function(data){

                $("#amount").val(data.balance);
            });

            $.get(site_url + '/payments/settlements/get-settlement-data/' + contact_id, function(data){

                settlementList(data);
            });
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }
    </script>

    <script type="text/javascript">
        $('.delete_btn').click(function () {
            var site_url            = $('.site_url').val();
            var id                  = $('.paymentId').val();
            window.location.href    = site_url + "/payments/delete/"+id;
        })
    </script>

    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }
    </script>

    <script type="text/javascript">
        function settlementList(data)
        {
            var settlement_list     = '';
            var site_url            = $('.site_url').val();
            var serial              = 0;
            $.each(data, function(i, settlement_data)
            {
                var edit_url    = site_url + '/payments/settlements/edit/' + settlement_data.id + '?settlement_type=' + settlement_data.type;
                var delete_url  = site_url + '/payments/settlements/delete/' + settlement_data.id + '?settlement_type=' + settlement_data.type;

                if (settlement_data.note == null)
                {
                    var note = '';
                }
                else
                {
                    var note = settlement_data.note;
                }

                var locationValue   = (new URL(location.href)).searchParams.get('settlement_type');

                if (settlement_data.type == locationValue)
                {
                    serial++;

                    settlement_list += '<tr>' +
                                        '<input class="form-control paymentId" type="hidden" name="settlement_id[]" value="' +  settlement_data.id + '">' +
                                        '<td style="text-align: left">' +
                                            serial +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            formatDate(settlement_data.date) +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           settlement_data.customer_name +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           settlement_data.paid_through_account_name +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           note +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           settlement_data.amount +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                '<a class="dropdown-item" href="' + edit_url +'" target="_blank">' + '{{ __('messages.edit')}}' + '</a>' +
                                                '<a class="dropdown-item" href="' + delete_url +'" >' + '{{ __('messages.delete')}}' + '</a>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                }
            });

            $("#settlement_list").empty();
            $("#settlement_list").append(settlement_list);
        }
    </script>
@endsection