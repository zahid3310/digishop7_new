<!DOCTYPE html>
<html>

<head>
    <title>Attendance Report</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>   
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Attendance Report</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                    <th style="text-align: center">Employee Name</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ date('d-m-Y', strtotime($from_date)) }} <strong>To</strong> {{ date('d-m-Y', strtotime($to_date)) }}</td>
                                    
                                    <td style="text-align: center">
                                        @if($employee_name != null)
                                            {{ $employee_name['ClientName'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 4%">DATE</th>
                                    <th style="text-align: center;width: 2%">MACHINE ID</th>
                                    <th style="text-align: center;width: 2%">EMPLOYEE ID</th>
                                    <th style="text-align: center;width: 7%">EMPLOYEE NAME</th>
                                    <th style="text-align: center;width: 6%">DESIGNATION</th>
                                    <th style="text-align: center;width: 4%">SHIFT</th>
                                    <th style="text-align: center;width: 4%">STATUS</th>
                                    <th style="text-align: center;width: 4%">IN TIME</th>
                                    <th style="text-align: center;width: 5%">OUT TIME</th>
                                    <th style="text-align: center;width: 5%">TOTAL HOUR</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                @foreach($result as $key => $value)
                                <tr>
                                    <td style="text-align: center;background-color: #C0C0C0;!important" colspan="11"><strong>{{ 'Attendance Date : ' . date('d-m-Y', strtotime($key)) }}</strong></td>
                                </tr>

                                @foreach($value as $key1 => $value1)
                                    <tr>
                                        <td style="text-align: left;" colspan="11"><strong>{{ 'Department : ' . $key1 }}</strong></td>
                                    </tr>

                                    <?php $i = 1; ?>
                                    @foreach($value1 as $key2 => $value2)
                                    <tr>
                                        <td style="text-align: center;">{{ $i }}</td>
                                        <td style="text-align: center;">{{ date('d-m-Y', strtotime($key)) }}</td>
                                        <td style="text-align: center;">{{ $key2 }}</td>
                                        <td style="text-align: center;">{{ $value2['employee_id'] }}</td>
                                        <td style="text-align: center;text-align: left;">{{ $value2['name'] != null ? $value2['name'] : $key1 }}</td>
                                        <td style="text-align: center;text-align: left;">{{ $value2['designation'] != null ? $value2['designation'] : '' }}</td>
                                        <td style="text-align: center;text-align: center;font-weight: bold">{{ $value2['shift'] }}</td>
                                        <td style="text-align: center;"><strong>
                                            @if($value2['status'] == 'Present')
                                            <span style="color:green!important">{{$value2['status']}}</span>
                                            @elseif($value2['status'] == 'Absent')
                                            <span style="color:red!important">{{$value2['status']}}</span>
                                            @elseif($value2['status'] == 'L/C')
                                            <span style="color:#0014ff!important">{{$value2['status']}}</span>
                                            @elseif($value2['status'] == 'SL' || $value2['status'] == 'CL' || $value2['status'] == 'EL')
                                            <span style="color:#ffc107!important">{{$value2['status']}}</span>
                                            @elseif($value2['status'] == 'W/H')
                                            <span style="color:merun!important">{{$value2['status']}}</span>
                                            @else
                                            <span style="color:black!important">{{$value2['status']}}</span>
                                            @endif
                                        </strong></td>
                                        <td style="text-align: center;">{{ $value2['in_time'] != 0 ? $value2['in_time'] : '00:00' }}</td>
                                        <td style="text-align: center;">{{ $value2['out_time'] != 0 ? $value2['out_time'] : '00:00' }}</td>
                                        <td style="text-align: center;">{{ $value2['duty_time'] != 0 ? $value2['duty_time'] : 0 }}</td>
                                    </tr>
                                    <?php $i++; ?>
                                    @endforeach
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>
</body>
</html>