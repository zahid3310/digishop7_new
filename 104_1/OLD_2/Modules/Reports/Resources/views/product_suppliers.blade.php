@extends('layouts.app')

@section('title', 'Suppliers Rate')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.supplier')}} {{ __('messages.rate')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.reports')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.supplier')}} {{ __('messages.rate')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="margin-left: 150px;margin-right: 150px" class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <form method="get" action="{{ route('product_suppliers_print') }}" enctype="multipart/form-data" target="_blank">
	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label">{{ __('messages.supplier')}} </label>
	                                    <div class="col-md-9">
	                                        <select style="width: 100%" id="supplier_id" name="supplier_id" class="form-control select2">
	                                           <option value="0">{{ __('messages.all')}}</option>
	                                        </select>
	                                    </div>
	                                </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">{{ __('messages.product')}}</label>
                                        <div class="col-md-9">
                                            <select style="width: 100%" id="product_id" name="product_id" class="form-control select2">
                                               <option value="0">{{ __('messages.all')}}</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">{{ __('messages.report_type')}} </label>
                                        <div class="col-md-9">
                                            <select style="width: 100%" id="report_type" name="report_type" class="form-control">
                                               <option value="1">{{ __('messages.supplier_wise')}}</option>
                                               <option value="2">{{ __('messages.product_wise')}}</option>
                                            </select>
                                        </div>
                                    </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"></label>
	                                    <div class="col-md-9">
	                                        <button style="border-radius: 0px;background-color: #297049;color: white" class="btn" type="submit" target="_blank">
	                                        	{{ __('messages.print')}}
	                                    	</button>
	                                    </div>
	                                </div>
                            	</form>
                                
                            </div>

                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#supplier_id").select2({
                ajax: { 
                url:  site_url + '/reports/supplier-list/get-supplier-list/',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'] + ' || ' + result['address'];
                },
            });

            $("#product_id").select2({
                ajax: { 
                url:  site_url + '/reports/sales-statement/product-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });

        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>
@endsection