<!DOCTYPE html>
<html>

<head>
    <title>Attendance Sheet</title>
    <link rel="icon" href="{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>   
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Attendance Sheet</h6>
                    </div>

                    <div class="ibox-content">
                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                    <th style="text-align: center">Employee Name</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ date('d-m-Y', strtotime($from_date)) }} <strong>To</strong> {{ date('d-m-Y', strtotime($to_date)) }}</td>
                                    
                                    <td style="text-align: center">
                                        @if($employee_name != null)
                                            {{ $employee_name['name'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;vertical-align: middle" colspan="4"></th>
                                    <th style="text-align: center" colspan="{{ count($date_range_days) }}">DATE</th>
                                    <th style="text-align: center;" colspan="5">Total</th>
                                    <th style="text-align: center;vertical-align: middle;width: 5%;" rowspan="3">GT</th>
                                </tr>

                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%;vertical-align: middle" rowspan="2">SL</th>
                                    <th style="text-align: center;width: 15%;vertical-align: middle" rowspan="2">Name</th>
                                    <th style="text-align: center;width: 5%;vertical-align: middle" rowspan="2">ID</th>
                                    <th style="text-align: center;width: 12%;vertical-align: middle" rowspan="2">Designation</th>
                                    
                                    @foreach($date_range_days as $date_range_days_val)
                                    <th style="text-align: center;width: 1.5%">{{ $date_range_days_val }}</th>
                                    @endforeach

                                    <th style="text-align: center;width: 3%;vertical-align: middle" rowspan="2">P</th>
                                    <th style="text-align: center;width: 3%;vertical-align: middle" rowspan="2">A</th>
                                    <th style="text-align: center;width: 3%;vertical-align: middle" rowspan="2">L</th>
                                    <th style="text-align: center;width: 3%;vertical-align: middle" rowspan="2">W/H</th>
                                    <th style="text-align: center;width: 3%;vertical-align: middle" rowspan="2">G/H</th>
                                </tr>

                                <tr style="background:#ddd;">
                                    @foreach($date_range_week as $date_range_week_val)
                                    <th style="text-align: center;width: 1.5%">{{ $date_range_week_val }}</th>
                                    @endforeach
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php $i = 1; ?>
                                @foreach($result as $key => $value)
                                
                                    <?php
                                        $present = attendancePresentCheck($value['id'], $from_date, $to_date);
                                        $absent  = attendanceAbsentCheck($value['id'], $from_date, $to_date);
                                        $leave   = attendancLeaveCheck($value['id'], $from_date, $to_date);
                                        $wh      = attendanceWeeklyHolidayCheck($value['id'], $from_date, $to_date);
                                        $gh      = attendanceGovmentHolidayCheck($value['id'], $from_date, $to_date);
                                    ?>
                                    
                                    <tr>
                                        <td style="text-align: center">{{ $i }}</td>
                                        <td style="text-align: left">{{ $value['name'] }}</td>
                                        <td style="text-align: center">{{ $value['id'] }}</td>
                                        <td style="text-align: left">{{ $value['designation'] }}</td>
                                        
                                        @foreach($date_range as $date_range_days_val)
                                            
                                            <td style="text-align: center">{{ attendanceStatusCheck($value['id'], $date_range_days_val) }}</td>
                                            
                                        @endforeach

                                        <td style="text-align: center">{{ $present }}</td>
                                        <td style="text-align: center">{{ $absent }}</td>
                                        <td style="text-align: center">{{ $leave }}</td>
                                        <td style="text-align: center">{{ $wh }}</td>
                                        <td style="text-align: center">{{ $gh }}</td>


                                        <td style="vertical-align: middle;text-align: center;">{{ $present + $leave + $wh + $gh }}</td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                            </tbody>
                            
                            <tfoot class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%;vertical-align: middle" rowspan="2">SL</th>
                                    <th style="text-align: center;width: 15%;vertical-align: middle" rowspan="2">Name</th>
                                    <th style="text-align: center;width: 5%;vertical-align: middle" rowspan="2">ID</th>
                                    <th style="text-align: center;width: 12%;vertical-align: middle" rowspan="2">Designation</th>
                                    
                                    @foreach($date_range_days as $date_range_days_val)
                                    <th style="text-align: center;width: 1.5%">{{ $date_range_days_val }}</th>
                                    @endforeach

                                    <th style="text-align: center;width: 3%;vertical-align: middle" rowspan="2">P</th>
                                    <th style="text-align: center;width: 3%;vertical-align: middle" rowspan="2">A</th>
                                    <th style="text-align: center;width: 3%;vertical-align: middle" rowspan="2">L</th>
                                    <th style="text-align: center;width: 3%;vertical-align: middle" rowspan="2">W/H</th>
                                    <th style="text-align: center;width: 3%;vertical-align: middle" rowspan="2">G/H</th>
                                    <th style="text-align: center;vertical-align: middle;width: 5%;" rowspan="3">GT</th>
                                </tr>

                                <tr style="background:#ddd;">
                                    @foreach($date_range_week as $date_range_week_val)
                                    <th style="text-align: center;width: 1.5%">{{ $date_range_week_val }}</th>
                                    @endforeach
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>
</body>
</html>