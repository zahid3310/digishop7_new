<!DOCTYPE html>
<html>

<head>
    <title>Print Egergency Item List</title>
    <link rel="icon" href="{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<style type="text/css">
    @page {
      size: A4 landscape;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">{{ __('messages.emergency_item_list')}}</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">{{ __('messages.category')}}</th>
                                    <th style="text-align: center">{{ __('messages.product')}}</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">
                                        @if($item_category_name != null)
                                            {{ $item_category_name['name'] }}
                                        @else
                                            {{ __('messages.all')}}
                                        @endif
                                    </td>

                                    <td style="text-align: center">
                                        @if($product_name != null)
                                            {{ $product_name['name'] }}
                                        @else
                                            {{ __('messages.all')}}
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">{{ __('messages.sl')}} </th>
                                    <th style="text-align: center;width: 15%">{{ __('messages.category')}}</th>
                                    <th style="text-align: center;width: 8%">{{ __('messages.code')}}</th>
                                    <th style="text-align: center;width: 25%">{{ __('messages.product')}}</th>
                                    <th style="text-align: center;width: 10%">Major Category</th>
                                    <th style="text-align: center;width: 7%">{{ __('messages.u/m')}}</th>
                                    <th style="text-align: center;width: 9%">{{ __('messages.stock_in_hand')}}</th>
                                    <th style="text-align: center;width: 5%">{{ __('messages.status')}}</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php
                                    $i                      = 1; 
                                    $total_stock_in_hand    = 0;
                                ?>

                                @foreach($products as $key => $value)
                                @if($value->count() > 0)
                                <tr>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value->count() }}">{{ $i }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value->count() }}">{{ $key }}</td>

                                    <td style="text-align: center;">{{ str_pad($value[0]['product_code'], 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: left;">
                                        {{ $value[0]['name'] }}
                                        <?php
                                            if ($value[0]['product_type'] == 2)
                                            {
                                                echo ' - ' . $value[0]['variations'];
                                            } 
                                        ?>
                                    </td>
                                    <td style="text-align: center;">{{ $value[0]['brand_name'] }}</td>
                                    <td style="text-align: center;">{{ $value[0]['unit_name'] }}</td>
                                    <td style="text-align: right;">{{ $value[0]['stock_in_hand'] }}</td>
                                    <td style="text-align: center;">{{ $value[0]['status'] == 1 ? 'Active' : 'Inactive'}}</td>
                                </tr>
                                
                                <?php
                                    $sub_total_stock_in_hand    = 0;
                                ?>
                                @foreach($value as $key1 => $value1)
                                @if($key1 != 0)
                                <tr>
                                    <td style="text-align: center;">{{ str_pad($value1['product_code'], 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: left;">
                                        {{ $value1['name'] }}
                                        <?php
                                            if ($value1['product_type'] == 2)
                                            {
                                                echo ' - ' . $value1['variations'];
                                            } 
                                        ?>
                                    </td>
                                    <td style="text-align: center;">{{ $value1['brand_name'] }}</td>
                                    <td style="text-align: center;">{{ $value1['unit_name'] }}</td>
                                    <td style="text-align: right;">{{ $value1['stock_in_hand'] }}</td>
                                    <td style="text-align: center;">{{ $value1['status'] == 1 ? 'Active' : 'Inactive'}}</td>
                                </tr>
                                @endif

                                <?php
                                    $sub_total_stock_in_hand    = $sub_total_stock_in_hand + $value1['stock_in_hand'];
                                ?>

                                @endforeach
                                <?php $i++; ?>
                                @endif
                                <?php
                                    $total_stock_in_hand    = $total_stock_in_hand + $sub_total_stock_in_hand;
                                ?>
                                @endforeach
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="6" style="text-align: right;">TOTAL</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_stock_in_hand }}</th>
                                    <th colspan="1" style="text-align: right;"></th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>