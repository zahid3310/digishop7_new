<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Sections extends Model
{  
    protected $table = "sections";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }
    
    public function department()
    {
        return $this->belongsTo('App\Models\Departments','department_id');
    }
}
