<?php
//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\SalesReturn;
use App\Models\SalesReturnEntries;
use App\Models\PurchaseReturn;
use App\Models\PurchaseReturnEntries;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Expenses;
use App\Models\Users;
use App\Models\Units;
use App\Models\Discounts;
use App\Models\DiscountProducts;
use App\Models\URLS;
use App\Models\Permissions;
use App\Models\Modules;
use App\Models\ModulesAccess;
use App\Models\UnitConversions;
use App\Models\BranchInventories;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\Branches;
use App\Models\DailyAttendance;

use App\Models\AccountHead;
use App\Models\VoucherSummary;
use App\Models\AccountTransaction;
use App\Models\ChequeTransactions;
use App\Models\Clients;

function userAccess($user_id)
{
    $current_route      = Route::currentRouteName();

    $access             = Permissions::leftjoin('urls', 'urls.id', 'permissions.url_id')
                                ->where('permissions.user_id', $user_id)
                                ->where('urls.url', $current_route)
                                ->first();

    return $access['access_level'];
}

function moduleAccess($user_id)
{
    $access     = ModulesAccess::leftjoin('modules', 'modules.id', 'modules_access.module_id')
                                ->where('modules_access.user_id', $user_id)
                                ->get();

    return $access;
}

function stockOut($data, $item_id)
{
    $branch_id  = Auth::user()->branch_id;

    if ($branch_id == 1)
    {
        if ($item_id != null)
        {
            foreach ($item_id as $key => $value)
            {
                $old_item_entry_id[]           = $value['product_entry_id'];
                $old_items_main_unit_id[]      = $value['main_unit_id'];
                $old_items_converted_unit_id[] = $value['conversion_unit_id'];
                $old_items_stock[]             = $value['quantity'];
            }

            foreach ($old_item_entry_id as $key2 => $value2)
            {
                $conversion_rate_find       = UnitConversions::where('main_unit_id', $old_items_main_unit_id[$key2])
                                            ->where('converted_unit_id', $old_items_converted_unit_id[$key2])
                                            ->where('product_entry_id', $value2)
                                            ->first();

                $quantity_add_to_product_entry                  = ProductEntries::find($value2);
                $old_converted_quantity                         = $conversion_rate_find != null ? $old_items_stock[$key2]/$conversion_rate_find['conversion_rate'] : $old_items_stock[$key2];
                $quantity_add_to_product_entry->stock_in_hand   = $quantity_add_to_product_entry['stock_in_hand'] + $old_converted_quantity;
                $quantity_add_to_product_entry->total_sold      = $quantity_add_to_product_entry ['total_sold'] - $old_converted_quantity;
                $quantity_add_to_product_entry->save();
            }
        }

        foreach ($data['product_entries'] as $key4 => $value4)
        {
            $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                                ->where('converted_unit_id', $data['unit_id'][$key4])
                                                                ->where('product_entry_id', $value4)
                                                                ->first();

            $product_entries                = ProductEntries::find($value4);
            $converted_quantity             = $conversion_rate_find != null ? $data['quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key4];
            $product_entries->stock_in_hand = $product_entries['stock_in_hand'] - $converted_quantity;
            $product_entries->total_sold    = $product_entries ['total_sold'] + $converted_quantity;
            $product_entries->save();
        }
    }
    else
    {
        if ($item_id != null)
        {
            foreach ($item_id as $key => $value)
            {
                $old_item_entry_id[]           = $value['product_entry_id'];
                $old_items_main_unit_id[]      = $value['main_unit_id'];
                $old_items_converted_unit_id[] = $value['conversion_unit_id'];
                $old_items_stock[]             = $value['quantity'];
            }

            foreach ($old_item_entry_id as $key2 => $value2)
            {
                $conversion_rate_find       = UnitConversions::where('main_unit_id', $old_items_main_unit_id[$key2])
                                            ->where('converted_unit_id', $old_items_converted_unit_id[$key2])
                                            ->where('product_entry_id', $value2)
                                            ->first();

                $quantity_add_to_product_entry                  = BranchInventories::where('product_entry_id', $value2)->where('branch_id', $branch_id)->first();
                $old_converted_quantity                         = $conversion_rate_find != null ? $old_items_stock[$key2]/$conversion_rate_find['conversion_rate'] : $old_items_stock[$key2];
                $quantity_add_to_product_entry->stock_in_hand   = $quantity_add_to_product_entry['stock_in_hand'] + $old_converted_quantity;
                $quantity_add_to_product_entry->save();

                //update main stock total_sold
                $old_total_sold_update              = ProductEntries::find($value2);
                $old_total_sold_update->total_sold  = $old_total_sold_update['total_sold'] - $old_converted_quantity;
                $old_total_sold_update->save();
            }
        }

        foreach ($data['product_entries'] as $key4 => $value4)
        {
            $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                                ->where('converted_unit_id', $data['unit_id'][$key4])
                                                                ->where('product_entry_id', $value4)
                                                                ->first();

            $product_entries                = BranchInventories::where('product_entry_id', $value4)->where('branch_id', $branch_id)->first();
            $converted_quantity             = $conversion_rate_find != null ? $data['quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key4];
            $product_entries->stock_in_hand = $product_entries['stock_in_hand'] - $converted_quantity;
            $product_entries->save();

            //update main stock total_sold
            $total_sold_update              = ProductEntries::find($value4);
            $total_sold_update->total_sold  = $total_sold_update['total_sold'] + $converted_quantity;
            $total_sold_update->save();
        }
    }
    
    return 0;
}

function stockIn($data, $item_id)
{
    if ($item_id != null)
    {
        foreach ($item_id as $key => $value)
        {
            $old_item_entry_id[]           = $value['product_entry_id'];
            $old_items_main_unit_id[]      = $value['main_unit_id'];
            $old_items_converted_unit_id[] = $value['conversion_unit_id'];
            $old_items_stock[]             = $value['quantity'];
            $old_items_bosta[]             = $value['bosta'];
        }

        foreach ($old_item_entry_id as $key2 => $value2)
        {
            $conversion_rate_find       = UnitConversions::where('main_unit_id', $old_items_main_unit_id[$key2])
                                        ->where('converted_unit_id', $old_items_converted_unit_id[$key2])
                                        ->where('product_entry_id', $value2)
                                        ->first();

            $quantity_add_to_product_entry                  = ProductEntries::find($value2);
            $old_converted_quantity                         = $conversion_rate_find != null ? $old_items_stock[$key2]/$conversion_rate_find['conversion_rate'] : $old_items_stock[$key2];
            $quantity_add_to_product_entry->stock_in_hand   = $quantity_add_to_product_entry['stock_in_hand'] - $old_converted_quantity;
            $quantity_add_to_product_entry->bosta           = $quantity_add_to_product_entry['bosta'] - $old_items_bosta[$key2];
            $quantity_add_to_product_entry->save();
        }
    }

    foreach ($data['product_entries'] as $key4 => $value4)
    {   
        $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                            ->where('converted_unit_id', $data['unit_id'][$key4])
                                                            ->where('product_entry_id', $value4)
                                                            ->first();

        $product_entries                = ProductEntries::find($value4);
        $converted_quantity             = $conversion_rate_find != null ? $data['quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key4];
        $product_entries->stock_in_hand = $product_entries['stock_in_hand'] + $converted_quantity;
        $product_entries->bosta         = $product_entries['bosta'] + $data['bosta'][$key4];
        $product_entries->buy_price     = $data['rate'][$key4];
        $product_entries->save();

        $avg_purchase_price             = avgPurchasePrice($value4);
        $product_entries_avg_price                          = ProductEntries::find($value4);
        $product_entries_avg_price->avg_purchase_price      = round($avg_purchase_price, 2);
        $product_entries_avg_price->save();
    
        if($conversion_rate_find != null)
        {
            $conversion_rate_find->avg_purchase_price       = round($avg_purchase_price/$conversion_rate_find['conversion_rate'], 2);
            $conversion_rate_find->save();
        }
    }

    return 0;
}

function avgPurchasePrice($product_entry_id)
{
    $end_date   = date('Y-m-d');
    $start_date = date('Y-m-d', strtotime("-30 days"));

    $bill_entries   = BillEntries::leftjoin('bills', 'bills.id', 'bill_entries.bill_id')
                                ->whereBetween('bills.bill_date', [$start_date, $end_date])
                                ->where('bill_entries.product_entry_id', $product_entry_id)
                                ->select('bill_entries.*', 'bills.rent as rent')
                                ->get();
    $total_qty = 0;
    $total_amo = 0;
    $toral_rent =0;
    foreach($bill_entries as $entries)
    {   
        $bill       = Bills::find($entries->bill_id);
        if($bill != null)
        {
            $entries_bal    = BillEntries::where('bill_id', $entries->bill_id)->get();
            if($bill['rent_type'] == 2)
            {
                $rent       = $bill['rent'];
                $qty        = $entries_bal->sum('quantity');
                $per_qty    = $rent/$qty;
                $toral_rent = $per_qty*$entries['quantity'];
            }
        }
        
        $conversion_rate_find   = UnitConversions::where('main_unit_id', $entries['main_unit_id'])
                                                ->where('converted_unit_id', $entries['conversion_unit_id'])
                                                ->where('product_entry_id', $product_entry_id)
                                                ->first();

        $converted_quantity     = $conversion_rate_find != null ? $entries['quantity']/$conversion_rate_find['conversion_rate'] : $entries['quantity'];
        $total_qty              = $total_qty + $converted_quantity;
        $total_amo              = $total_amo + $entries->total_amount + $toral_rent;
    }

    $avg_rate  = $total_amo/$total_qty;

    return $avg_rate;
}

function stockInReturn($data, $item_id)
{
    $branch_id  = Auth::user()->branch_id;

    if ($branch_id == 1)
    {
        foreach ($data['product_entries'] as $key4 => $value4)
        {
            $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                            ->where('converted_unit_id', $data['unit_id'][$key4])
                                                            ->where('product_entry_id', $value4)
                                                            ->first();

            $converted_quantity_to_main_unit= $conversion_rate_find != null ? $data['return_quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['return_quantity'][$key4];

            $product_entries                = ProductEntries::find($value4);
            $product_entries->stock_in_hand = $product_entries['stock_in_hand'] + $converted_quantity_to_main_unit;
            $product_entries->buy_price     = $data['rate'][$key4];
            $product_entries->save();
        }
    }
    else
    {
        foreach ($data['product_entries'] as $key4 => $value4)
        {
            $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                            ->where('converted_unit_id', $data['unit_id'][$key4])
                                                            ->where('product_entry_id', $value4)
                                                            ->first();

            $converted_quantity_to_main_unit= $conversion_rate_find != null ? $data['return_quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['return_quantity'][$key4];

            $product_entries                = BranchInventories::where('product_entry_id', $value4)->first();
            $product_entries->stock_in_hand = $product_entries['stock_in_hand'] + $converted_quantity_to_main_unit;
            $product_entries->save();
        }
    }

    return 0;
}

function stockOutReturn($data, $item_id)
{
    $branch_id  = Auth::user()->branch_id;

    foreach ($data['product_entries'] as $key4 => $value4)
    {
        $conversion_rate_find    = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                        ->where('converted_unit_id', $data['unit_id'][$key4])
                                        ->where('product_entry_id', $value4)
                                        ->first();

        $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $data['return_quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['return_quantity'][$key4];

        $product_entries                = ProductEntries::find($value4);
        $product_entries->stock_in_hand = $product_entries['stock_in_hand'] - $converted_quantity_to_main_unit;
        // $product_entries->bosta         = $product_entries['bosta'] - $data['return_bosta'][$key4];
        $product_entries->save();
    }

    return 0;
}

function createCustomer($data)
{
    $customer                       = new Customers;
    $customer->name                 = $data['customer_name'];
    $customer->phone                = $data['mobile_number'];
    $customer->address              = $data['address'];
    $customer->nid_number           = $data['nid_number'];
    $customer->alternative_contact  = $data['alternative_mobile_number'];
    $customer->contact_type         = $data['contact_type'];
    $customer->save();

    return $customer['id'];
}

function userDetails()
{
    $branch_id  = Auth::user()->branch_id;
    $user       = Branches::find($branch_id);
    return $user;
}

function stockSellValue($product_id)
{
    $product_entries    = ProductEntries::where('product_entries.id', $product_id)
                                ->get();

    $stock_value   = 0;
    foreach ($product_entries as $key => $value)
    {
       $stock_value   = $stock_value + ($value['stock_in_hand']*$value['sell_price']);
    }

    return $stock_value;
}

function stockPurchaseValue($product_id)
{
    $product_entries    = ProductEntries::where('product_entries.id', $product_id)
                                ->get();

    $stock_value   = 0;
    foreach ($product_entries as $key => $value)
    {
       $stock_value   = $stock_value + ($value['stock_in_hand']*$value['buy_price']);
    }

    return $stock_value;
}

function accessLevel($route)
{
    //All Routes start

    //products Module Start
        $products_create        = 'products_create';
        $products_store         = 'products_store';
        $products_edit          = 'products_edit';
        $products_update        = 'products_update';
    //products Module End

    //Invoice Module Start
        $invoices_edit       = 'invoices_edit';
        $invoices_update     = 'invoices_update';
    //Invoice Module End

    //Bill Module Start
       $bills_index    = 'bills_index';
       $bills_create   = 'bills_create';
       $bills_store    = 'bills_store';
       $bills_edit     = 'bills_edit';
       $bills_update   = 'bills_update';
       $bills_show     = 'bills_show';
    //Bill Module End

    //Payments Module Start
        $payments_edit              = 'payments_edit';
        $payments_update            = 'payments_update';
    //Payments Module End

    //expenses Module Start
        $expenses_edit             = 'expenses_edit';
        $expenses_update           = 'expenses_update';
    //expenses Module End

    //Users Module Start
        $users_index             = 'users_index';
        $users_create            = 'users_create';
        $users_store             = 'users_store';
        $users_edit              = 'users_edit';
        $users_update            = 'users_update';
    //Users Module End

    //Customers Module Start
      $customers_edit            = 'customers_edit';
      $customers_update          = 'customers_update';
    //Customers Module End

    //All Routes End

    if (Auth::user()->role == 0)
    {
        if ( ($route == $products_create) || 
            ($route == $products_store) || 
            ($route == $products_edit) || 
            ($route == $products_update) ||
            ($route == $invoices_edit) || 
            ($route == $invoices_update) || 
            ($route == $bills_index) || 
            ($route == $bills_create) || 
            ($route == $bills_store) || 
            ($route == $bills_edit) ||
            ($route == $bills_update) || 
            ($route == $bills_show) ||  
            ($route == $expenses_edit) ||  
            ($route == $expenses_update) ||  
            ($route == $payments_edit) || 
            ($route == $users_index) || 
            ($route == $users_create) || 
            ($route == $users_store) || 
            ($route == $users_edit) || 
            ($route == $users_update) )
        {
            return "No";
        }
    else{
      return "accepted";
    }
    }   
    else
    {
            return "accepted";
    }
}

function salesReturn($invoice_id)
{
    $returns                = SalesReturn::leftjoin('customers', 'customers.id', 'sales_return.customer_id')
                                            ->where('sales_return.invoice_id', $invoice_id)
                                            ->select('sales_return.sales_return_date as sales_return_date',
                                                'sales_return.id as id',
                                                'sales_return.invoice_id as invoice_id',
                                                'sales_return.sales_return_number as sales_return_number',
                                                'sales_return.customer_id as customer_id',
                                                'customers.name as customer_name',
                                                'sales_return.return_amount as return_amount',
                                                'sales_return.due_amount as due_amount',
                                                'sales_return.return_note as return_note',
                                                'sales_return.vat_type as vat_type',
                                                'sales_return.total_vat as total_vat',
                                                'sales_return.tax_type as tax_type',
                                                'sales_return.total_tax as total_tax')
                                            ->get();

    $return_entries         = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.sales_return_id')
                                            ->leftjoin('products', 'products.id', 'sales_return_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'sales_return_entries.product_entry_id')
                                            ->where('sales_return.invoice_id', $invoice_id)
                                            ->select('sales_return_entries.*',
                                                     'product_entries.name as product_entry_name',
                                                     'products.name as product_name',
                                                     'product_entries.product_code as product_code')
                                            ->get();

    $total_return_amount        = 0;
    $total_paid_amount          = 0;
    $total_due_amount           = 0;

    if ($returns->count() > 0)
    {
        foreach ($returns as $key => $value)
        {   
            $sub_total_value                            = $return_entries->where('invoice_id', $value['id'])->sum('total_amount');

            $data[$value['id']]['sales_return_number']  = 'SR - ' . str_pad($value['sales_return_number'], 6, "0", STR_PAD_LEFT);
            $data[$value['id']]['sales_return_date']    = date('d-m-Y', strtotime($value['sales_return_date']));
            $data[$value['id']]['sales_return_id']      = $value['id'];
            $data[$value['id']]['customer_name']        = $value['customer_name'];
            $data[$value['id']]['invoice_id']           = $value['invoice_id'];
            $data[$value['id']]['return_amount']        = $value['return_amount'];
            $data[$value['id']]['paid_amount']          = $value['return_amount'] - $value['due_amount'];
            $data[$value['id']]['due_amount']           = $value['due_amount'];
            $data[$value['id']]['return_note']          = $value['return_note'];
            $data[$value['id']]['sub_total']            = $sub_total_value;
            $data[$value['id']]['return_entries']       = $return_entries->where('sales_return_id', $value['id']);

            $total_return_amount                        = $total_return_amount + $value['return_amount'];
            $total_paid_amount                          = $total_paid_amount + ($value['return_amount'] - $value['due_amount']);
            $total_due_amount                           = $total_due_amount + $value['due_amount'];
        }
    }
    else
    {
        $data                       = [];
        $total_return_amount        = 0;
        $total_paid_amount          = 0;
        $total_due_amount           = 0;
    }

    return ['data'                  => $data,
            'total_return_amount'   => $total_return_amount,
            'total_paid_amount'     => $total_paid_amount,
            'total_due_amount'      => $total_due_amount,
        ];
}

function purchaseReturn($bill_id)
{
    $returns                = PurchaseReturn::leftjoin('customers', 'customers.id', 'purchase_return.customer_id')
                                            ->where('purchase_return.bill_id', $bill_id)
                                            ->select('purchase_return.purchase_return_date as purchase_return_date',
                                                'purchase_return.id as id',
                                                'purchase_return.bill_id as bill_id',
                                                'purchase_return.purchase_return_number as purchase_return_number',
                                                'purchase_return.customer_id as customer_id',
                                                'customers.name as customer_name',
                                                'purchase_return.return_amount as return_amount',
                                                'purchase_return.due_amount as due_amount',
                                                'purchase_return.return_note as return_note',
                                                'purchase_return.vat_type as vat_type',
                                                'purchase_return.total_vat as total_vat',
                                                'purchase_return.tax_type as tax_type',
                                                'purchase_return.total_tax as total_tax')
                                            ->get();

    $return_entries         = PurchaseReturnEntries::leftjoin('purchase_return', 'purchase_return.id', 'purchase_return_entries.purchase_return_id')
                                            ->leftjoin('products', 'products.id', 'purchase_return_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'purchase_return_entries.product_entry_id')
                                            ->where('purchase_return.bill_id', $bill_id)
                                            ->select('purchase_return_entries.*',
                                                     'product_entries.name as product_entry_name',
                                                     'products.name as product_name',
                                                     'product_entries.product_code as product_code')
                                            ->get();

    $total_return_amount        = 0;
    $total_paid_amount          = 0;
    $total_due_amount           = 0;
    $total_tax_amount           = 0;
    $total_vat_amount           = 0;

    if ($returns->count() > 0)
    {
        foreach ($returns as $key => $value)
        {   
            $sub_total_value                                = $return_entries->where('bill_id', $value['id'])->sum('total_amount');

            $data[$value['id']]['purchase_return_number']   = 'PR - ' . str_pad($value['purchase_return_number'], 6, "0", STR_PAD_LEFT);
            $data[$value['id']]['purchase_return_date']     = date('d-m-Y', strtotime($value['purchase_return_date']));
            $data[$value['id']]['purchase_return_id']       = $value['id'];
            $data[$value['id']]['customer_name']            = $value['customer_name'];
            $data[$value['id']]['invoice_id']               = $value['invoice_id'];
            $data[$value['id']]['return_amount']            = $value['return_amount'];
            $data[$value['id']]['paid_amount']              = $value['return_amount'] - $value['due_amount'];
            $data[$value['id']]['due_amount']               = $value['due_amount'];
            $data[$value['id']]['return_note']              = $value['return_note'];
            $data[$value['id']]['sub_total']                = $sub_total_value;
            $data[$value['id']]['return_entries']           = $return_entries->where('purchase_return_id', $value['id']);

            $total_return_amount                            = $total_return_amount + $value['return_amount'];
            $total_paid_amount                              = $total_paid_amount + ($value['return_amount'] - $value['due_amount']);
            $total_due_amount                               = $total_due_amount + $value['due_amount'];
        }
    }
    else
    {
        $data = [];
    }

    return ['data'                  => $data,
            'total_return_amount'   => $total_return_amount,
            'total_paid_amount'     => $total_paid_amount,
            'total_due_amount'      => $total_due_amount,
            'total_tax_amount'      => $total_tax_amount,
            'total_vat_amount'      => $total_vat_amount,
        ];
}

function discountProducts($discount_id)
{
    $discounts      = DiscountProducts::leftjoin('product_entries', 'product_entries.id', 'discount_products.product_id')
                            ->where('discount_products.discount_id', $discount_id)
                            ->orderBy('discount_products.discount_id', 'DESC')
                            ->select('product_entries.name as product_name')
                            ->get();

    return $discounts;
}

function customersTableDetails($id)
{
    $customers  = Customers::find($id);

    return $customers;
}

function ProductVariationName($product_entry_id)
{
    $product            = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations')
                                            ->find($product_entry_id);

    return $product['variations'];
}

function ProductName($product_entry_id)
{
    $product            = ProductEntries::find($product_entry_id);

    return $product['name'];
}

function numberTowords(float $amount)
{
    $amount_after_decimal = round($amount - ($num = floor($amount)), 2) * 100;
   // Check if there is any number after decimal
   $amt_hundred = null;
   $count_length = strlen($num);
   $x = 0;
   $string = array();
   $change_words = array(0 => '', 1 => 'One', 2 => 'Two',
     3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
     7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
     10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
     13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
     16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
     19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
     40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
     70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
  $here_digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
  while( $x < $count_length ) {
       $get_divider = ($x == 2) ? 10 : 100;
       $amount = floor($num % $get_divider);
       $num = floor($num / $get_divider);
       $x += $get_divider == 10 ? 1 : 2;
       if ($amount) {
         $add_plural = (($counter = count($string)) && $amount > 9) ? 's' : null;
         $amt_hundred = ($counter == 1 && $string[0]) ? ' and ' : null;
         $string [] = ($amount < 21) ? $change_words[$amount].' '. $here_digits[$counter]. $add_plural.' 
         '.$amt_hundred:$change_words[floor($amount / 10) * 10].' '.$change_words[$amount % 10]. ' 
         '.$here_digits[$counter].$add_plural.' '.$amt_hundred;
         }else $string[] = null;
       }
   $implode_to_Rupees = implode('', array_reverse($string));
   $get_paise = ($amount_after_decimal > 0) ? "And " . ($change_words[$amount_after_decimal / 10] . " 
   " . $change_words[$amount_after_decimal % 10]) . ' Paisa' : '';
   return ($implode_to_Rupees ? $implode_to_Rupees . 'Taka ' : '') . $get_paise;
}

function previousDues($customer_id)
{
    $data  = Invoices::where('customer_id', $customer_id)->sum('due_amount');

    return $data;
}

function previousDuesBill($supplier_id)
{
    $data  = Bills::where('vendor_id', $supplier_id)->sum('due_amount');

    return $data;
}

function findStock($product_entry_id, $branch_id)
{
    $data  = BranchInventories::where('product_entry_id', $product_entry_id)
                            ->where('branch_id', $branch_id)
                            ->first();

    return $data;
}

function actualStockInEdit($product_entry_id, $quantity, $main_unit_id, $conversion_unit_id)
{
    if ($main_unit_id == $conversion_unit_id)
    {
        $data   = $quantity;
    }
    else
    {
        $find_conversion_rate   = UnitConversions::where('product_entry_id', $product_entry_id)
                                                ->where('main_unit_id', $main_unit_id)
                                                ->where('conversion_unit_id', $conversion_unit_id)
                                                ->first();

        $data                   = $quantity/$find_conversion_rate['conversion_rate'];
    }

    return $data;
}

function invoiceDetails($invoice_id)
{
    $data = InvoiceEntries::where('invoice_id', $invoice_id)->get();

    return $data;
}

function billDetails($bill_id)
{
    $data = BillEntries::where('bill_id', $bill_id)->get();

    return $data;
}

function balanceUpdate($customer_id)
{
    $cust_type          = Clients::find($customer_id);
    $branch_id          = Auth()->user()->branch_id;

    if($cust_type->PtnID == '263')
    {
        $sales              = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'SALES')
                                    ->where('HeadID', 1189)
                                    ->sum('CreditAmount');

        $sales_return       = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'SR')
                                    ->where('HeadID', 1323)
                                    ->sum('DebitAmount');

        $purchase           = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'PURCHASE')
                                    ->where('HeadID', 1322)
                                    ->sum('DebitAmount');

        $purchase_return    = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'PR')
                                    ->where('HeadID', 1324)
                                    ->sum('CreditAmount');
                                    
        $opening_balance_debit  = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'JR')
                                    ->where('HeadID', 1094)
                                    ->sum('DebitAmount');

        $opening_balance_credit = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'JR')
                                    ->where('HeadID', 1325)
                                    ->sum('CreditAmount');

        $ac                 = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->whereIn('PurposeAccHeadName', ['bank_ac', 'cash_ac'])
                                    ->get();

        $bal                = $ac->sum('DebitAmount') - $ac->sum('CreditAmount');
        $balance            = $opening_balance_credit - $opening_balance_debit + $sales - $sales_return + $purchase - $purchase_return - $bal;
    }
    elseif($cust_type->PtnID == '264')
    {
        $sales              = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'SALES')
                                    ->where('HeadID', 1189)
                                    ->sum('CreditAmount');

        $sales_return       = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'SR')
                                    ->where('HeadID', 1323)
                                    ->sum('DebitAmount');

        $purchase           = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'PURCHASE')
                                    ->where('HeadID', 1322)
                                    ->sum('DebitAmount');

        $purchase_return    = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'PR')
                                    ->where('HeadID', 1324)
                                    ->sum('CreditAmount');
                                    
        $opening_balance_debit  = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'JR')
                                    ->where('HeadID', 1094)
                                    ->sum('DebitAmount');

        $opening_balance_credit = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'JR')
                                    ->where('HeadID', 1325)
                                    ->sum('CreditAmount');

        $ac                 = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->whereIn('PurposeAccHeadName', ['bank_ac', 'cash_ac'])
                                    ->get();

        $bal                = $ac->sum('CreditAmount') - $ac->sum('DebitAmount');
        $balance            = $opening_balance_debit - $opening_balance_credit + $sales - $sales_return + $purchase - $purchase_return - $bal;

    }
    else
    {
        $ac                 = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->whereIn('PurposeAccHeadName', ['bank_ac', 'cash_ac'])
                                    ->get();

        $balance            = $ac->sum('DebitAmount') - $ac->sum('CreditAmount');
    }

    //Customer Balance Update Start
        $cus_update             = Clients::find($customer_id);
        $cus_update->balance    = $balance;
        $cus_update->save();
    //Customer Balance Update End
}

function calculateOpeningBalance($customer_id)
{
    $customer_advance  = Clients::find($customer_id);

    if ($customer_advance['balance'] > 0)
    {
        $data['balance']   = $customer_advance['balance'];
        $data['type']      = 1; //1 = previous dues
    }

    if ($customer_advance['balance'] < 0)
    {
        $data['balance']   = abs($customer_advance['balance']);
        $data['type']      = 2; //2 = advance amount
    }

    if ($customer_advance['balance'] == 0)
    {
        $data['balance']   = 0;
        $data['type']      = 1; //1 = previous dues
    }

    return $data;
}

function eng2bang($number)
{
    $bn   = array("০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯");
    $en   = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

    return  str_replace($en, $bn, $number);
}

function ban2eng($number)
{
    $bn   = array("০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯");
    $en   = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

    return  str_replace($bn, $en, $number);
}

function eng2bangMonth($number)
{
    $bn   = array("জানুয়ারি", "ফেব্রুয়ারী", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টতবর", "নভেম্বার", "ডিসেম্বর");
    $en   = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");

    return  str_replace($en, $bn, $number);
}

function dateDiff($date1, $date2)
{
    $date1_ts   = strtotime($date1);
    $date2_ts   = strtotime($date2);
    $diff       = $date2_ts - $date1_ts;
    
    return round($diff / 86400) + 1;
}

function attendanceStatusCheck($employee_id, $date)
{
    $data   = DailyAttendance::where('daily_attendance.date', $date)
                             ->where('daily_attendance.user_id', $employee_id)
                             ->select('daily_attendance.status')
                             ->first();
                             
    if($data != null)
    {
        if($data['status'] == 'Present')
        {
            $status = 'P';
        }
        elseif($data['status'] == 'L/C')
        {
            $status = 'L/C';
        }
        elseif($data['status'] == 'Absent')
        {
            $status = 'A';
        }
        elseif($data['status'] == 'W/H')
        {
            $status = 'W/H';
        }
        elseif($data['status'] == 'G/H')
        {
            $status = 'G/H';
        }
        elseif($data['status'] == 'CL')
        {
            $status = 'CL';
        }
        elseif($data['status'] == 'EL')
        {
            $status = 'EL';
        }
        elseif($data['status'] == 'SL')
        {
            $status = 'SL';
        }
        else
        {
            $status = '';
        }
    }
    else
    {
        $status = '';
    }
    
    return $status;
}

function attendancePresentCheck($employee_id, $from_date, $to_date)
{
    $data   = DailyAttendance::whereBetween('daily_attendance.date', [$from_date, $to_date])
                             ->where('daily_attendance.user_id', $employee_id)
                             ->whereIn('daily_attendance.status', ['Present', 'L/C'])
                            //  ->orWhere('daily_attendance.status', 'L/C')
                             ->count();
    return $data;
}

function attendanceAbsentCheck($employee_id, $from_date, $to_date)
{
    $data   = DailyAttendance::whereBetween('daily_attendance.date', [$from_date, $to_date])
                             ->where('daily_attendance.user_id', $employee_id)
                             ->where('daily_attendance.status', 'Absent')
                             ->count();
    return $data;
}

function attendancLeaveCheck($employee_id, $from_date, $to_date)
{
    $data   = DailyAttendance::whereBetween('daily_attendance.date', [$from_date, $to_date])
                             ->where('daily_attendance.user_id', $employee_id)
                             ->get();
                             
    $data1  = $data->where('status', 'SL')->count();
    $data2  = $data->where('status', 'EL')->count();
    $data3  = $data->where('status', 'CL')->count();
    $data   = $data1 + $data2 + $data3;
    
    return $data;
}

function attendanceWeeklyHolidayCheck($employee_id, $from_date, $to_date)
{
    $data   = DailyAttendance::whereBetween('daily_attendance.date', [$from_date, $to_date])
                             ->where('daily_attendance.user_id', $employee_id)
                             ->where('daily_attendance.status', 'W/H')
                             ->count();
    return $data;
}

function attendanceGovmentHolidayCheck($employee_id, $from_date, $to_date)
{
    $data   = DailyAttendance::whereBetween('daily_attendance.date', [$from_date, $to_date])
                             ->where('daily_attendance.user_id', $employee_id)
                             ->where('daily_attendance.status', 'G/H')
                             ->count();
    return $data;
}


//Financial Accounting Start
    //Voucher store
    function voucherSummaryCreate($VoucherNumber, $VoucherDate, $ProjectID, $RegisterID, $Type, $BankAccountNumber, $Status, $TotalAmount, $Narration, $MoneyReceiptNo, $CompanyID, $created_by)
    {
        $voucher_summary                        = new VoucherSummary;
        $voucher_summary->VoucherNumber         = $VoucherNumber;
        $voucher_summary->VoucherDate           = date('Y-m-d', strtotime($VoucherDate));
        $voucher_summary->ProjectID             = $ProjectID;
        $voucher_summary->RegisterID            = $RegisterID;
        $voucher_summary->Type                  = $Type;
        $voucher_summary->BankAccountNumber     = $BankAccountNumber;
        $voucher_summary->Status                = $Status;
        $voucher_summary->TotalAmount           = $TotalAmount;
        $voucher_summary->Narration             = $Narration;
        $voucher_summary->MoneyReceiptNo        = $MoneyReceiptNo;
        $voucher_summary->CompanyID             = $CompanyID;
        $voucher_summary->created_by            = $created_by;

        if ($voucher_summary->save())
        {
            return $voucher_summary->id;
        }
        else
        {
            return 0;
        }
    }

    //Voucher update
    function voucherSummaryUpdate($id, $VoucherNumber, $VoucherDate, $ProjectID, $RegisterID, $Type, $BankAccountNumber, $Status, $TotalAmount, $Narration, $MoneyReceiptNo, $CompanyID, $created_by)
    {
        $voucher_summary                        = VoucherSummary::find($id);
        $voucher_summary->VoucherNumber         = $VoucherNumber;
        $voucher_summary->VoucherDate           = date('Y-m-d', strtotime($VoucherDate));
        $voucher_summary->ProjectID             = $ProjectID;
        $voucher_summary->RegisterID            = $RegisterID;
        $voucher_summary->Type                  = $Type;
        $voucher_summary->BankAccountNumber     = $BankAccountNumber;
        $voucher_summary->Status                = $Status;
        $voucher_summary->TotalAmount           = $TotalAmount;
        $voucher_summary->Narration             = $Narration;
        $voucher_summary->MoneyReceiptNo        = $MoneyReceiptNo;
        $voucher_summary->CompanyID             = $CompanyID;
        $voucher_summary->updated_by            = $created_by;

        if ($voucher_summary->save())
        {
            return $voucher_summary->id;
        }
        else
        {
            return 0;
        }
    }

    //Debit store
    function accountTransactionsDebitStore($VoucherId, $VoucherDate, $VoucherNumber, $ProjectID, $RegisterID, $HeadID, $PurposeAccHeadName, $Particulars, $DebitAmount, $CreditAmount, $AccountNumber, $VoucherType, $CBAccount, $IsPosted, $CompanyID, $Status, $created_by)
    {
        $account_transactions                       = new AccountTransaction;
        $account_transactions->VoucherId            = $VoucherId;
        $account_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
        $account_transactions->VoucherNumber        = $VoucherNumber;
        $account_transactions->ProjectID            = $ProjectID;
        $account_transactions->RegisterID           = $RegisterID;
        $account_transactions->HeadID               = $HeadID;
        $account_transactions->PurposeAccHeadName   = $PurposeAccHeadName;
        $account_transactions->Particulars          = $Particulars;
        $account_transactions->DebitAmount          = $DebitAmount;
        $account_transactions->CreditAmount         = $CreditAmount;
        $account_transactions->AccountNumber        = $AccountNumber;
        $account_transactions->VoucherType          = $VoucherType;
        $account_transactions->CBAccount            = $CBAccount;
        $account_transactions->IsPosted             = $IsPosted;
        $account_transactions->CompanyID            = $CompanyID;
        $account_transactions->Status               = $Status;
        $account_transactions->created_by           = $created_by;
        $account_transactions->save();

        return 1;
    }

    //Credit store
    function accountTransactionsCreditStore($VoucherId, $VoucherDate, $VoucherNumber, $ProjectID, $RegisterID, $HeadID, $PurposeAccHeadName, $Particulars, $DebitAmount, $CreditAmount, $AccountNumber, $VoucherType, $CBAccount, $IsPosted, $CompanyID, $Status, $created_by)
    {
        $account_transactions                       = new AccountTransaction;
        $account_transactions->VoucherId            = $VoucherId;
        $account_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
        $account_transactions->VoucherNumber        = $VoucherNumber;
        $account_transactions->ProjectID            = $ProjectID;
        $account_transactions->RegisterID           = $RegisterID;
        $account_transactions->HeadID               = $HeadID;
        $account_transactions->PurposeAccHeadName   = $PurposeAccHeadName;
        $account_transactions->Particulars          = $Particulars;
        $account_transactions->DebitAmount          = $DebitAmount;
        $account_transactions->CreditAmount         = $CreditAmount;
        $account_transactions->AccountNumber        = $AccountNumber;
        $account_transactions->VoucherType          = $VoucherType;
        $account_transactions->CBAccount            = $CBAccount;
        $account_transactions->IsPosted             = $IsPosted;
        $account_transactions->CompanyID            = $CompanyID;
        $account_transactions->Status               = $Status;
        $account_transactions->created_by           = $created_by;
        $account_transactions->save();

        return 1;
    }

    //Debit update
    function accountTransactionsDebitUpdate($id, $VoucherId, $VoucherDate, $VoucherNumber, $ProjectID, $RegisterID, $HeadID, $PurposeAccHeadName, $Particulars, $DebitAmount, $CreditAmount, $AccountNumber, $VoucherType, $CBAccount, $IsPosted, $CompanyID, $Status, $updated_by)
    {
        $account_transactions                       = AccountTransaction::find($id);
        $account_transactions->VoucherId            = $VoucherId;
        $account_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
        $account_transactions->VoucherNumber        = $VoucherNumber;
        $account_transactions->ProjectID            = $ProjectID;
        $account_transactions->RegisterID           = $RegisterID;
        $account_transactions->HeadID               = $HeadID;
        $account_transactions->PurposeAccHeadName   = $PurposeAccHeadName;
        $account_transactions->Particulars          = $Particulars;
        $account_transactions->DebitAmount          = $DebitAmount;
        $account_transactions->CreditAmount         = $CreditAmount;
        $account_transactions->AccountNumber        = $AccountNumber;
        $account_transactions->VoucherType          = $VoucherType;
        $account_transactions->CBAccount            = $CBAccount;
        $account_transactions->CompanyID            = $CompanyID;
        $account_transactions->updated_by           = $updated_by;
        $account_transactions->save();

        return 1;
    }

    //Credit update
    function accountTransactionsCreditUpdate($id, $VoucherId, $VoucherDate, $VoucherNumber, $ProjectID, $RegisterID, $HeadID, $PurposeAccHeadName, $Particulars, $DebitAmount, $CreditAmount, $AccountNumber, $VoucherType, $CBAccount, $IsPosted, $CompanyID, $Status, $updated_by)
    {
        $account_transactions                       = AccountTransaction::find($id);
        $account_transactions->VoucherId            = $VoucherId;
        $account_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
        $account_transactions->VoucherNumber        = $VoucherNumber;
        $account_transactions->ProjectID            = $ProjectID;
        $account_transactions->RegisterID           = $RegisterID;
        $account_transactions->HeadID               = $HeadID;
        $account_transactions->PurposeAccHeadName   = $PurposeAccHeadName;
        $account_transactions->Particulars          = $Particulars;
        $account_transactions->DebitAmount          = $DebitAmount;
        $account_transactions->CreditAmount         = $CreditAmount;
        $account_transactions->AccountNumber        = $AccountNumber;
        $account_transactions->VoucherType          = $VoucherType;
        $account_transactions->CBAccount            = $CBAccount;
        $account_transactions->CompanyID            = $CompanyID;
        $account_transactions->updated_by           = $updated_by;
        $account_transactions->save();

        return 1;
    }

    //cheque Store
    function chequeStore($VoucherDate, $VoucherNumber, $CompanyID, $ProjectID, $RegisterID, $HeadID, $ChequeState, $Type, $StaffID, $ChequeNumber, $ChequeType, $ChequeDate, $EnCashDate, $Amount, $created_by)
    {
        $cheque_transactions                       = new ChequeTransactions;
        $cheque_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
        $cheque_transactions->VoucherNumber        = $VoucherNumber;
        $cheque_transactions->CompanyID            = $CompanyID;
        $cheque_transactions->ProjectID            = $ProjectID;
        $cheque_transactions->RegisterID           = $RegisterID;
        $cheque_transactions->HeadID               = $HeadID;
        $cheque_transactions->ChequeState          = $ChequeState;
        $cheque_transactions->Type                 = $Type;
        $cheque_transactions->StaffID              = $StaffID;
        $cheque_transactions->ChequeNumber         = $ChequeNumber;
        $cheque_transactions->ChequeType           = $ChequeType;
        $cheque_transactions->ChequeDate           = date('Y-m-d', strtotime($ChequeDate));
        $cheque_transactions->EnCashDate           = date('Y-m-d', strtotime($EnCashDate));
        $cheque_transactions->Amount               = $Amount;
        $cheque_transactions->created_by           = $created_by;
        $cheque_transactions->save();

        return 1;
    }

    //cheque Update
    function chequeUpdate($id, $VoucherDate, $VoucherNumber, $CompanyID, $ProjectID, $RegisterID, $HeadID, $ChequeState, $Type, $StaffID, $ChequeNumber, $ChequeType, $ChequeDate, $EnCashDate, $Amount, $updated_by)
    {
        $cheque_transactions                       = ChequeTransactions::find($id);
        $cheque_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
        $cheque_transactions->VoucherNumber        = $VoucherNumber;
        $cheque_transactions->CompanyID            = $CompanyID;
        $cheque_transactions->ProjectID            = $ProjectID;
        $cheque_transactions->RegisterID           = $RegisterID;
        $cheque_transactions->HeadID               = $HeadID;
        $cheque_transactions->ChequeState          = $ChequeState;
        $cheque_transactions->Type                 = $Type;
        $cheque_transactions->StaffID              = $StaffID;
        $cheque_transactions->ChequeNumber         = $ChequeNumber;
        $cheque_transactions->ChequeType           = $ChequeType;
        $cheque_transactions->ChequeDate           = date('Y-m-d', strtotime($ChequeDate));
        $cheque_transactions->EnCashDate           = date('Y-m-d', strtotime($EnCashDate));
        $cheque_transactions->Amount               = $Amount;
        $cheque_transactions->updated_by           = $updated_by;
        $cheque_transactions->save();

        return 1;
    }

    function cachInHand()
    {
        $branch_id      = Auth()->user()->branch_id;
        $data_list      = AccountTransaction::where('IsPosted', 1)
                                            ->where('CompanyID', $branch_id)
                                            ->where('PurposeAccHeadName', 'cash_ac')
                                            ->whereIn('VoucherType', ['CP', 'CR', 'JR', 'CN'])
                                            ->get();

        $cash_in_hand_debit     = $data_list->sum('DebitAmount');
        $cash_in_hand_credit    = $data_list->sum('CreditAmount');
        $cash_in_hand           = $cash_in_hand_debit - $cash_in_hand_credit;

        return $cash_in_hand;
    }

    function cachInBank($bank_id)
    {
        $branch_id      = Auth()->user()->branch_id;
        $data_list      = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('PurposeAccHeadName', 'bank_ac')
                                    ->where('HeadID', $bank_id)
                                    ->whereIn('VoucherType', ['BP', 'BR', 'JR', 'CN'])
                                    ->get();

        $bank_debit     = $data_list->sum('DebitAmount');
        $bank_credit    = $data_list->sum('CreditAmount');
        $balance        = $bank_debit - $bank_credit;

        return $balance;
    }
//Financial Accounting End


//Inventory With Accounting Start
    //Debit store
    function accountTransactionsDebitStoreInventory($VoucherId, $VoucherDate, $VoucherNumber, $ProjectID, $RegisterID, $HeadID, $PurposeAccHeadName, $Particulars, $DebitAmount, $CreditAmount, $AccountNumber, $VoucherType, $CBAccount, $IsPosted, $CompanyID, $Status, $created_by, $invoice_id, $bill_id, $sales_return_id, $purchase_return_id)
    {
        $account_transactions                       = new AccountTransaction;
        $account_transactions->VoucherId            = $VoucherId;
        $account_transactions->invoice_id           = $invoice_id;
        $account_transactions->bill_id              = $bill_id;
        $account_transactions->sales_return_id      = $sales_return_id;
        $account_transactions->purchase_return_id   = $purchase_return_id;
        $account_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
        $account_transactions->VoucherNumber        = $VoucherNumber;
        $account_transactions->ProjectID            = $ProjectID;
        $account_transactions->RegisterID           = $RegisterID;
        $account_transactions->HeadID               = $HeadID;
        $account_transactions->PurposeAccHeadName   = $PurposeAccHeadName;
        $account_transactions->Particulars          = $Particulars;
        $account_transactions->DebitAmount          = $DebitAmount;
        $account_transactions->CreditAmount         = $CreditAmount;
        $account_transactions->AccountNumber        = $AccountNumber;
        $account_transactions->VoucherType          = $VoucherType;
        $account_transactions->CBAccount            = $CBAccount;
        $account_transactions->IsPosted             = $IsPosted;
        $account_transactions->CompanyID            = $CompanyID;
        $account_transactions->Status               = $Status;
        $account_transactions->created_by           = $created_by;
        $account_transactions->save();

        return 1;
    }

    //Credit store
    function accountTransactionsCreditStoreInventory($VoucherId, $VoucherDate, $VoucherNumber, $ProjectID, $RegisterID, $HeadID, $PurposeAccHeadName, $Particulars, $DebitAmount, $CreditAmount, $AccountNumber, $VoucherType, $CBAccount, $IsPosted, $CompanyID, $Status, $created_by, $invoice_id, $bill_id, $sales_return_id, $purchase_return_id)
    {
        $account_transactions                       = new AccountTransaction;
        $account_transactions->VoucherId            = $VoucherId;
        $account_transactions->invoice_id           = $invoice_id;
        $account_transactions->bill_id              = $bill_id;
        $account_transactions->sales_return_id      = $sales_return_id;
        $account_transactions->purchase_return_id   = $purchase_return_id;
        $account_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
        $account_transactions->VoucherNumber        = $VoucherNumber;
        $account_transactions->ProjectID            = $ProjectID;
        $account_transactions->RegisterID           = $RegisterID;
        $account_transactions->HeadID               = $HeadID;
        $account_transactions->PurposeAccHeadName   = $PurposeAccHeadName;
        $account_transactions->Particulars          = $Particulars;
        $account_transactions->DebitAmount          = $DebitAmount;
        $account_transactions->CreditAmount         = $CreditAmount;
        $account_transactions->AccountNumber        = $AccountNumber;
        $account_transactions->VoucherType          = $VoucherType;
        $account_transactions->CBAccount            = $CBAccount;
        $account_transactions->IsPosted             = $IsPosted;
        $account_transactions->CompanyID            = $CompanyID;
        $account_transactions->Status               = $Status;
        $account_transactions->created_by           = $created_by;
        $account_transactions->save();

        return 1;
    }

    //Debit store
    function accountTransactionsDebitUpdateInventory($id, $VoucherId, $VoucherDate, $VoucherNumber, $ProjectID, $RegisterID, $HeadID, $PurposeAccHeadName, $Particulars, $DebitAmount, $CreditAmount, $AccountNumber, $VoucherType, $CBAccount, $IsPosted, $CompanyID, $Status, $updated_by, $invoice_id, $bill_id, $sales_return_id, $purchase_return_id)
    {
        $account_transactions                       = AccountTransaction::find($id);
        $account_transactions->VoucherId            = $VoucherId;
        $account_transactions->invoice_id           = $invoice_id;
        $account_transactions->bill_id              = $bill_id;
        $account_transactions->sales_return_id      = $sales_return_id;
        $account_transactions->purchase_return_id   = $purchase_return_id;
        $account_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
        $account_transactions->VoucherNumber        = $VoucherNumber;
        $account_transactions->ProjectID            = $ProjectID;
        $account_transactions->RegisterID           = $RegisterID;
        $account_transactions->HeadID               = $HeadID;
        $account_transactions->PurposeAccHeadName   = $PurposeAccHeadName;
        $account_transactions->Particulars          = $Particulars;
        $account_transactions->DebitAmount          = $DebitAmount;
        $account_transactions->CreditAmount         = $CreditAmount;
        $account_transactions->AccountNumber        = $AccountNumber;
        $account_transactions->VoucherType          = $VoucherType;
        $account_transactions->CBAccount            = $CBAccount;
        $account_transactions->IsPosted             = $IsPosted;
        $account_transactions->CompanyID            = $CompanyID;
        $account_transactions->Status               = $Status;
        $account_transactions->updated_by           = $updated_by;
        $account_transactions->save();

        return 1;
    }

    //Credit store
    function accountTransactionsCreditUpdateInventory($id, $VoucherId, $VoucherDate, $VoucherNumber, $ProjectID, $RegisterID, $HeadID, $PurposeAccHeadName, $Particulars, $DebitAmount, $CreditAmount, $AccountNumber, $VoucherType, $CBAccount, $IsPosted, $CompanyID, $Status, $updated_by, $invoice_id, $bill_id, $sales_return_id, $purchase_return_id)
    {
        $account_transactions                       = AccountTransaction::find($id);
        $account_transactions->VoucherId            = $VoucherId;
        $account_transactions->invoice_id           = $invoice_id;
        $account_transactions->bill_id              = $bill_id;
        $account_transactions->sales_return_id      = $sales_return_id;
        $account_transactions->purchase_return_id   = $purchase_return_id;
        $account_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
        $account_transactions->VoucherNumber        = $VoucherNumber;
        $account_transactions->ProjectID            = $ProjectID;
        $account_transactions->RegisterID           = $RegisterID;
        $account_transactions->HeadID               = $HeadID;
        $account_transactions->PurposeAccHeadName   = $PurposeAccHeadName;
        $account_transactions->Particulars          = $Particulars;
        $account_transactions->DebitAmount          = $DebitAmount;
        $account_transactions->CreditAmount         = $CreditAmount;
        $account_transactions->AccountNumber        = $AccountNumber;
        $account_transactions->VoucherType          = $VoucherType;
        $account_transactions->CBAccount            = $CBAccount;
        $account_transactions->IsPosted             = $IsPosted;
        $account_transactions->CompanyID            = $CompanyID;
        $account_transactions->Status               = $Status;
        $account_transactions->updated_by           = $updated_by;
        $account_transactions->save();

        return 1;
    }
//Inventory With Accounting End