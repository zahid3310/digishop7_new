<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MonthlySalarySheets extends Model
{  
    protected $table = "monthly_salary_sheets";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function salaryStatements()
    {
        return $this->belongsTo('App\Models\SalaryStatements','statement_id');
    }
}
