<!DOCTYPE html >
<html>

<head>
    <meta charset="utf-8">
    <title>Cash Memo</title>
    <link href="https://fonts.maateen.me/bangla/font.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Libre Barcode 39' rel='stylesheet'>

    <style>
        *{margin:0;padding:0;outline:0}

        body {
            font-size:14px;
            line-height:18px;
            color:#000;
            height:203mm;
            width: 292mm;/*297*/
            margin:0 auto;
            font-family: 'Bangla', Arial, sans-serif !important;  
        }
        .barcode {font-family: 'Libre Barcode 39';font-size: 10vh;line-height: 0}

        table,th {
            /*border: 1px solid black;*/
            border-collapse: collapse;
            padding: 10px;
        }
        
        .height{line-height: 15px;font-size:17px;}

        td {
           /* border-left: 1px solid white;
            border-right: 1px solid white;*/
            padding: 3px;
        }

        .bg_templete{
            background:url(images/bg3.png) no-repeat;
            background-size: 100%;
            -webkit-print-color-adjust: exact;
        }

        .templete{
            width:100%;
            background:none;
            margin:0 auto;
        }

        .width-47{
            width: 47%;
        }
        
        .width-96{
            width: 92%;
            padding-right: 4%;
            padding-left: 2%;
        }
        
        .clear{
            overflow:hidden;
        }

        .text-center{
            text-align: center;
        }

        .text-end{
            text-align: end;
        }

        .item2 {
            grid-area: com_logo;
        }

        .item3 {
            grid-area: com_info;
            padding-top:15px;
            padding-right: 135px;
        }

        .item4 {
            grid-area: other_info;
        }

        .grid-container {
            display: grid;
            grid-template-areas: "com_logo com_info other_info ";
            /*padding-top: 10px;*/
        }

        .grid-container div {
            text-align: center;
        }

        .grid-container-aline-end {
            text-align: end !important;
        }

        .grid-container-aline-start {
            text-align: start !important;
        }

        .com_logo_img {
            height: 130px;
            width: 160px;
        }

        .tr-height{
            padding: 10px;
        }

        .item11 { grid-area: sig_text2; }
        .item22 { grid-area: some_info; }
        .item33 { grid-area: sig1; }
        .item44 { grid-area: sig2; }
        .item55 { grid-area: sig_text1; }

        .grid-container-1 {
            display: grid;
            grid-template-areas:
            ' sig1 sig1 some_info some_info sig2 sig2'
            ' sig_text1 sig_text1 some_info some_info sig_text2 sig_text2';
        }

        .signaturesection1 {
            border-top: 1px solid #000;
            margin-top: 45px;
        }

        .signaturesection2 {
            border-top: 1px solid #000;
            margin-top: 45px;
        }

        .borderNone{border-left: 1px solid #fff;border-bottom:1px solid #fff ;}

        @page {
            size: A4;
            page-break-after: always;
        }
        
        @media print { 

        #footer {
          position: fixed;
          bottom: 0;
          }
          
        .barcode {font-family: 'Libre Barcode 39';font-size: 10vh;line-height:0.5}
        }
    </style>
</head>

<body>
    <div class="bg_templete clear">

        <div class="templete clear width-96">

            <br>
            <br>
            
            <div class="grid-container" style="border:1px solid;padding: 5px;">
                <div class="item2 grid-container-aline-start">
                    <img class="com_logo_img" src="{{ url('public/'.userDetails()->logo) }}"alt="qr sample"
                    />
                </div>

                <div class="item3">
                    <h1 style="line-height: 1;font-size: 40px">{{ userDetails()->organization_name }}</h1>
                    <h2 style="line-height: 1;font-size: 22px">{{ userDetails()->address }}</h2>
                    <p style="line-height: 1;font-size: 18px;padding-top:5px;">Authorized Dealer:Fresh Ceramics,Fondy Ceramic.</p>
                    <p style="line-height: 1;font-size: 18px;padding-top:5px;">Contact : {{ userDetails()->contact_number }}</p>
                    <p style="line-height: 1;font-size: 18px">{{ userDetails()->contact_email }}</p>
                </div>

                <div class="item4 grid-container-aline-end">

                    <p style="padding-right: 25px;line-height: 1;font-size: 28px"></p>
                    
                </div>
            </div>

            <p style="line-height: 2;text-align: center"><span style="padding: 5px;border-radius: 13px;font-size: 25px;font-weight: bold;">PAYMENT DETAILS</span></p>

            <div class="grid-container" style="border:1px solid;padding: 5px;font-size: 18px;">
                <div class="item2 grid-container-aline-start" style="line-height: 1.40;">
                    <strong style="font-size:18px;">Customer Name :</strong>  {{ $payment['customer_name'] != null ? $payment['customer_name'] : $payment['contact_name'] }}<br>
                    <strong style="font-size:18px;">Address :</strong> {{ $payment['address'] != null ? $payment['address'] : $payment['contact_address'] }}<br>
                    <strong style="font-size:18px;">Mobile No :</strong> {{ $payment['phone'] != null ? $payment['phone'] : $payment['contact_phone'] }}
                </div>

                <div class="item3">
                     <p style="padding-right: 25px;line-height: 1;font-size: 28px"></p>
                </div>

                <div class="item4 grid-container-aline-end">
                    <strong style="font-size:18px;">Payment No :</strong>  {{ 'PM - ' . str_pad($payment['payment_number'], 6, "0", STR_PAD_LEFT) }}<br>
                    <strong style="font-size:18px;">Date :{{ date('d-m-Y', strtotime($payment['payment_date'])) }}</strong>  <br><br><br>
                    <p class="barcode">{{ str_pad($payment['payment_number'], 6, "0", STR_PAD_LEFT) }}</p>
                    
                </div>
            </div>


            <div style="padding-top: 10px;padding-bottom: 20px">
                <table style="width: 100%">
                    
                    <thead>
                        <tr style="font-size: 17px;">
                            <!--<th>{{ __('messages.payment_date')}}</th>-->
                            <!--<th>{{ __('messages.payment_number')}}</th>-->
                            <!--<th style="text-align: right">{{ __('messages.paid_through')}}</th>-->
                            <!--<th style="text-align: center">{{ __('messages.amount')}}</th>-->
                            <th></th>
                            <th></th>
                            <th style="text-align: end">Description</th>
                            <th style="text-align: center">Amount</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr style="font-size: 17px;">
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: right"></td>
                            <td style="text-align: center"></td>
                        </tr style="font-size: 17px;">
                        
                        <tr style="font-size: 17px;">
                            <td style="text-align: right;" colspan="3">Total Previous Due =</td>
                            <td style="text-align: center">{{ number_format($payment['amount'] + $payment['customer_balance']  ,0,'.',',') }}</td>
                        </tr style="font-size: 17px;">
                        
                        <tr style="font-size: 17px;">
                            <td style="text-align: right" colspan="3">Discount =</td>
                            <td style="text-align: center">{{ number_format($payment['discount'] ,0,'.',',') }}</td>
                        </tr style="font-size: 17px;">
                        
                        
                        <tr style="font-size: 17px;">
                            <td style="text-align: right" colspan="3">Sub Total =</td>
                            <td style="text-align: center">{{ number_format($payment['amount'] - $payment['discount'] ,0,'.',',') }}</td>
                        </tr style="font-size: 17px;">
                        
                        <tr style="font-size: 17px;">
                            <td style="text-align: right" colspan="3">Total Paid Amount =</td>
                            <td style="text-align: center">{{ number_format($payment['amount'] - $payment['discount'] ,0,'.',',') }}</td>
                        </tr> 
                        
                        
                        <tr style="font-size: 17px;">
                            <td style="text-align: right" colspan="3">Current Due Amount =</td>
                            <td style="text-align: center">{{ number_format($payment['customer_balance'],0,'.',',') }}</td>
                        </tr>

                </table>
            </div>
            
            <br>

            <div id="footer" style="width:100%">
            <div class="grid-container-1" style="font-size:18px">

                <div class="item33"></div>  
                <div class="item55"><p class="signaturesection1" style="font-size: 18px;text-align: center">Customer Signature</p></div>

                <div class="item22" style="text-align: center;">
                    <h3 style="font-size: 18px"> </h3>
                    <div>
                        <p style="font-size: 18px"></p>
                        <p style="font-size: 18px"> </p>
                    </div>
                </div>

                <div class="item44"></div>
                <div class="item1"><p class="signaturesection2" style="font-size: 18px;text-align: center">Authorization Signature</p></div>
            </div>
            <br>
            <p style="font-size:18px;text-align:justify;padding-right: 60px;"> N.B.The sold product is not returnable and changeable. No objection will be accepted without 				
                     cash memo. Received  the product according to the Challan, No further complaint is acceptable 				
                             after received  the products. Cash memo must be brought along for to the next need.The shop or company is not responsible for any unintentional incidents or illegal activities by the
transport or its people.</p><br>
                             <strong style="text-transform: uppercase;font-size:18px">Friday Is Off Day</strong>
                             <strong style="text-transform: uppercase;padding-left: 680px;font-size:18px">Thank You</strong>
                             <strong style="padding-left: 830px;font-size:18px">Megha Builders</strong>
            </div>
        </div>
    </div>
</body>

</html>