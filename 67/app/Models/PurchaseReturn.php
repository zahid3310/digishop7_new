<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class purchaseReturn extends Model
{
    protected $table = "purchase_return";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function purchaseReturnEntries()
    {
        return $this->hasMany(PurchaseReturnEntries::class, "purchase_return_id");
    }
}
