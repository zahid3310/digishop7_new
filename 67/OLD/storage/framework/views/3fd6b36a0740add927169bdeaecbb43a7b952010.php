

<?php $__env->startSection('title', 'Profit Loss'); ?>

<?php $__env->startSection('styles'); ?>
    <style type="text/css">
        @media  print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page  {
                margin: 0cm ! important;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Profit Loss Report</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Profit Loss Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['address']); ?></p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['contact_number']); ?></p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">Profit Loss Report</h4>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><strong>From</strong> <?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?> <strong>To</strong> <?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="<?php echo e(route('profit_loss_index_reduced')); ?>" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div class="col-lg-10 col-md-8 col-sm-8 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="<?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="<?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-4 col-sm-4 col-12 d-print-none margin-top-20-xs">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                   
                                    <tbody>
                                        <tr>
                                            <td style="text-align: right;width: 25%"><strong></strong></td>
                                            <td style="text-align: right;width: 25%"><strong>Sell</strong></td>
                                            <td style="text-align: left;width: 25%"><strong><?php echo e(number_format($total_invoice_amount,2,'.',',')); ?></strong></td>
                                            <td style="text-align: right;width: 25%"><strong></strong></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: right;width: 25%"><strong></strong></td>
                                            <td style="text-align: right;width: 25%"><strong>Purchase Cost</strong></td>
                                            <td style="text-align: left;width: 25%"><strong><?php echo e(number_format($total_purchase_cost,2,'.',',')); ?></strong></td>
                                            <td style="text-align: left;width: 25%"><strong></strong></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: right;width: 25%"><strong></strong></td>
                                            <?php
                                                $profit_loss  = $total_invoice_amount - $total_purchase_cost;
                                            ?>
                                            <td style="text-align: right;width: 25%"><strong><?php if($profit_loss > 0): ?> Profit <?php elseif($profit_loss < 0): ?> Loss <?php else: ?> Profit/Loss <?php endif; ?></strong></td>
                                            <td style="text-align: left;width: 25%"><strong><?php echo e(number_format(abs($profit_loss),2,'.',',')); ?></strong></td>
                                            <td style="text-align: left;width: 25%"><strong></strong></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: right;width: 25%"><strong></strong></td>
                                            <td style="text-align: right;width: 25%"><strong>Expense</strong></td>
                                            <td style="text-align: left;width: 25%"><strong><?php echo e(number_format($total_expense,2,'.',',')); ?></strong></td>
                                            <td style="text-align: left;width: 25%"><strong></strong></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: right;width: 25%"><strong></strong></td>
                                            <?php
                                                $net_profit_loss  = ($total_invoice_amount - $total_purchase_cost) - $total_expense;
                                            ?>
                                            <td style="text-align: right;width: 25%"><strong><?php if($net_profit_loss > 0): ?> Net Profit <?php elseif($net_profit_loss < 0): ?> Net Loss <?php else: ?> Net Profit/Loss <?php endif; ?></strong></td>
                                            <td style="text-align: left;width: 25%"><strong><?php echo e(number_format(abs($net_profit_loss),2,'.',',')); ?></strong></td>
                                            <td style="text-align: left;width: 25%"><strong></strong></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/67/Modules/Reports/Resources/views/profit_loss_reduced.blade.php ENDPATH**/ ?>