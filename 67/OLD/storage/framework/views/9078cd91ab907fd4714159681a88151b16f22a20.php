

<?php $__env->startSection('title', 'Opening Stock'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Opening Stock</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Opening Stock</a></li>
                                    <li class="breadcrumb-item active">Opening Stock</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form action="<?php echo e(route('products_opening_stock')); ?>" method="get">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Category </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="category_id" name="category_id" class="form-control select2">
                                                    <?php if($category_name != null): ?>
                                                    <option value="<?php echo e($category_id); ?>"><?php echo e($category_name != null ? $category_name->name : ''); ?></option>
                                                    <?php else: ?>
                                                    <option value="0">All</option>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Brand </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="brand_id" name="brand_id" class="form-control select2">
                                                    <?php if($brand_name != null): ?>
                                                    <option value="<?php echo e($brand_id); ?>"><?php echo e($brand_name != null ? $brand_name->name : ''); ?></option></option>
                                                    <?php else: ?>
                                                    <option value="0">All</option>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Size </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="size_id" name="size_id" class="form-control select2">
                                                    <option value="0" selected>-- All Sizes --</option>
                                                    <?php if(!empty($sizes) && ($sizes->count() > 0)): ?>
                                                    <?php $__currentLoopData = $sizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option <?php echo e(isset($_GET['size_id']) && ($_GET['size_id'] == $value['id']) ? 'selected' : ''); ?> value="<?php echo e($value['id']); ?>"><?php echo e($value['height'] . ' X ' . $value['width']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Code </label>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <select style="width: 73%" id="product_code" name="product_code" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                        <?php if($product_code_name != null): ?>
                                                        <option value="<?php echo e($product_code); ?>"><?php echo e($product_code_name != null ? $product_code_name->product_code : ''); ?></option>
                                                        <?php else: ?>
                                                        <option value="0">All</option>
                                                        <?php endif; ?>
                                                    </select>
                                                    <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="searchPayment()">
                                                        <i class="bx bx-search font-size-24"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <button style="display: none" type="submit" id="openingStockSubmit">Save</button>
                                </form>

                                <hr>

            					<form id="FormSubmit" action="<?php echo e(route('products_opening_stock_store')); ?>" method="post" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					<?php echo e(csrf_field()); ?>


                                <?php if(!empty($product_entries) && ($product_entries->count() > 0)): ?>
                                    <?php $__currentLoopData = $product_entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-md-8 col-form-label">
                                                <?php echo e($value['product_code'] . ' | '); ?>

                                                <?php echo e($value['category_name']. ' | '); ?>

                                                <?php echo e($value['size_name']. ' | '); ?>

                                                <?php echo e($value['brand_name']. ' | '); ?>

                                                <?php echo e($value['name']); ?>

                                                <?php
                                                    if ($value['product_type'] == 2)
                                                    {
                                                        echo ' - ' . $value['variations'];
                                                    } 
                                                ?>
                                            </label>
                                            
                                            <div class="col-md-4">
                                                <input class="form-control" type="text" value="" name="stock_in_hand[]" id="stock_in_hand_<?php echo e($key); ?>" placeholder="Enter Opening Stock In SFT" required>
                                                <input class="form-control" type="hidden" value="<?php echo e($value['id']); ?>" name="product_entry_id[]">
                                                <input class="form-control" type="hidden" value="<?php echo e($value['product_id']); ?>" name="product_id[]">
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    <div class="form-group row">
                                        <div class="col-md-8"></div>
                                        <div class="button-items col-md-2 pull-right">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('home')); ?>">Close</a></button>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                <?php else: ?>
                                <div class="form-group row">
                                    <div style="text-align: center" class="col-md-12">
                                        <h6>No Product Available For Adding Opening Stock.</h6>
                                    </div>
                                </div>
                                <?php endif; ?>
                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            var site_url  = $('.site_url').val();

            $("#category_id").select2({
                ajax: { 
                url:  site_url + '/reports/sales-statement/category-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $("#brand_id").select2({
                ajax: { 
                url:  site_url + '/reports/item-list/brand-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $("#product_code").select2({
                ajax: { 
                url:  site_url + '/reports/sales-statement/product-code-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });
    </script>

    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }

        function searchPayment()
        {
            $('#openingStockSubmit').click();
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/67/Modules/Products/Resources/views/opening_stock.blade.php ENDPATH**/ ?>