@extends('layouts.app')

@section('title', 'Damages')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Orders</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">New Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('damages_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                {{ csrf_field() }}

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Date *</label>
                                            <div class="col-md-8">
                                                <input id="damage_date" name="damage_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-2 col-form-label">Note</label>
                                            <div class="col-md-10">
                                                <input id="note" name="note" type="text" class="form-control" placeholder="Damage Note">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 530px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        <div class="row di_0">
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px !important;padding-bottom: 0px !important" class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('damages_index') }}">Close</a></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $('#add_field_button').click();

            $("#product_entries_0").select2({
                ajax: { 
                url:  site_url + '/bills/product-list-load-bill',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });
    </script>

    <script type="text/javascript">
        function getItemPrice(x)
        {
            var site_url    = $(".site_url").val();
            var entry_id    = $("#product_entries_"+x).val();
            var cartoon_val = $("#cartoon_"+x).val();
            var pcs_val     = $("#pcs_"+x).val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    var pcs_equivalent_sft  = (parseFloat(data.height)*parseFloat(data.width))/144;
                    var stock_in_pcs        = parseFloat(data.stock_in_hand)/parseFloat(pcs_equivalent_sft);
                    var stock_in_cart       = parseFloat(stock_in_pcs)/parseFloat(data.pcs_per_cartoon);

                    //
                        if (data.stock_in_hand == '' || data.stock_in_hand == null)
                        {
                            var stockInHand  = 0;
                        }
                        else
                        {
                            var stockInHand  = data.stock_in_hand;
                        }

                    //
                        if (cartoon_val == '')
                        {
                            var cartoonVal  = 0;
                        }
                        else
                        {
                            var cartoonVal  = parseFloat(cartoon_val);
                        }

                    //
                        if (pcs_val == '')
                        {
                            var pcsVal  = 0;
                        }
                        else
                        {
                            var pcsVal  = parseFloat(pcs_val);
                        }

                    if (cartoonVal != 0)
                    {
                        var qtyVal1  = parseFloat(cartoonVal)*parseFloat(data.pcs_per_cartoon)*parseFloat(pcs_equivalent_sft);
                    }
                    else
                    {
                        var qtyVal1  = 0;
                    }

                    if (pcsVal != 0)
                    {
                        var qtyVal2  = parseFloat(pcsVal)*parseFloat(pcs_equivalent_sft);
                    }
                    else
                    {
                        var qtyVal2  = 0;
                    }

                    var qtyVal = parseFloat(qtyVal1) + parseFloat(qtyVal2);

                    $("#quantity_"+x).val(parseFloat(qtyVal).toFixed(2));
                    $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
                    $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' SFT ' + ' | ' + parseFloat(stock_in_cart).toFixed(2) + ' Cart ' + ' | ' + parseFloat(stock_in_pcs).toFixed(2) + ' PCS ');
                });
            }
        }

        function getItemPriceBackCalculation(x)
        {
            var site_url    = $(".site_url").val();
            var entry_id    = $("#product_entries_"+x).val();
            var sft_val     = $("#quantity_"+x).val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    var pcs_equivalent_sft  = (parseFloat(data.height)*parseFloat(data.width))/144;
                    var stock_in_pcs        = parseFloat(data.stock_in_hand)/parseFloat(pcs_equivalent_sft);
                    var stock_in_cart       = parseFloat(stock_in_pcs)/parseFloat(data.pcs_per_cartoon);

                    //
                        if (sft_val == '')
                        {
                            var sftVal  = 0;
                        }
                        else
                        {
                            var sftVal  = parseFloat(sft_val);
                        }

                    var findPcs = parseFloat(sft_val)/parseFloat(pcs_equivalent_sft);

                    //x = 50; y = 15; res = x % y; x = (x - res) / y; [result = 3]
                    var cartVal = (parseFloat(findPcs) - (parseFloat(findPcs)%parseFloat(data.pcs_per_cartoon)))/parseFloat(data.pcs_per_cartoon);

                    var pcsVal  = parseFloat(findPcs) - (parseFloat(cartVal)*parseFloat(data.pcs_per_cartoon));

                    $("#cartoon_"+x).val(parseFloat(cartVal).toFixed(2));
                    $("#pcs_"+x).val(parseFloat(pcsVal).toFixed(2));
      
                    calculateActualAmount(x);
                });
            }
        }

        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = -1;
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {   
                x++;

                var serial = x + 1;

                if (serial == 1)
                {
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var cartoon_label       = '<label class="hidden-xs" for="productname">Cart</label>\n';
                    var pcs_label           = '<label class="hidden-xs" for="productname">PCS</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButton()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var product_label  = '';
                    var cartoon_label  = '';
                    var pcs_label      = '';
                    var quantity_label = '';
                    var action_label   = '';

                    var add_btn        = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRow').append(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px" class="col-lg-5 col-md-5 col-sm-12 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Product--' + '</option>' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Cart</label>\n' +
                                                        cartoon_label +
                                                        '<input type="text" name="cartoon[]" class="inner form-control" id="cartoon_'+x+'" placeholder="Cartoon" oninput="getItemPrice('+x+')" />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">PCS</label>\n' +
                                                        pcs_label +
                                                        '<input type="text" name="pcs[]" class="inner form-control" id="pcs_'+x+'" placeholder="PCS" oninput="getItemPrice('+x+')" />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control" id="quantity_'+x+'" placeholder="Quantity" oninput="getItemPriceBackCalculation('+x+')" required />\n' +
                                                    '</div>\n' +
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

                var site_url = $(".site_url").val();
                $(".productEntries").select2({
                    ajax: { 
                    url:  site_url + '/bills/product-list-load-bill',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                    },
                });
            }                                    
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });
    </script>
@endsection