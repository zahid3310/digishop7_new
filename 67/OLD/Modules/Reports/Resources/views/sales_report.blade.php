@extends('layouts.app')

@section('title', 'Sales Report')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales Report</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Sales Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center">{{ $user_info['organization_name'] }}</h2>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['address'] }}</p>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['contact_number'] }}</p>
                                        <h4 style="line-height: 10px;text-align: center">Sales Report</h4>
                                        <p style="line-height: 20px;text-align: center"><strong>From</strong> {{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }} <strong>To</strong> {{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}</p>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="{{ route('sales_report_index') }}" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div style="margin-bottom: 10px" class="col-lg-5 col-md-5 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="{{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="{{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        <div class="col-lg-5 col-md-5 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="0" selected>-- All Customers --</option>
                                                    @if(!empty($customers) && ($customers->count() > 0))
                                                    @foreach($customers as $key => $value)
                                                        <option {{ isset($_GET['customer_id']) && ($_GET['customer_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Details</th>
                                            <th>Product Name</th>
                                            <th>Product Code</th>
                                            <th style="text-align: right">Rate</th>
                                            <th style="text-align: right">Quantity</th>
                                            <th style="text-align: right">Discount</th>
                                            <th style="text-align: right">Amount</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php 
                                            $total_return_am        = 0; 
                                            $total_return_paid_am   = 0;
                                        ?>
                                        @if(!empty($data))
                                        @foreach($data as $key => $value)
                                            <tr>
                                                <td colspan="7"><strong>{{ $value['invoice_date'] }}, {{ $value['invoice_number'] }}, {{ $value['customer_name'] }}</strong></td>
                                            </tr>

                                            @if(!empty($value['invoice_entries']))

                                            <?php $serial = 0; ?>

                                            @foreach($value['invoice_entries'] as $key1 => $value1)
                                                <tr>
                                                    <td style="text-align: right">{{ $serial + 1 }}</td>
                                                    <td>{{ $value1->product_name }} <br> {{ $value1->product_entry_name }}</td>
                                                    <td>{{ str_pad($value1->product_code, 6, "0", STR_PAD_LEFT) }}</td>
                                                    <td style="text-align: right">{{ number_format($value1->rate,2,'.',',') }}</td>
                                                    <td style="text-align: right">{{ $value1->quantity }}</td>
                                                    <td style="text-align: right">
                                                        {{ $value1->discount_type == 1 ? number_format($value1->discount_amount,2,'.',',') : number_format((($value1->discount_amount*$value1['rate']*$value1['quantity'])/100),2,'.',',') }}
                                                        @if(($value1->discount_type == 0) && ($value1->discount_amount > 0)) 
                                                            <br> <?php echo '('.$value1->discount_amount.'%)'; ?>
                                                        @endif
                                                    </td>
                                                    <td style="text-align: right">{{ number_format($value1->total_amount,2,'.',',') }}</td>
                                                </tr>

                                                <?php $serial++; ?>
                                            @endforeach

                                            <?php $sales_return = salesReturn($value['invoice_id']); ?>

                                            @if(!empty($sales_return))
                                            @foreach($sales_return['data'] as $key => $value3)
                                                <tr>
                                                    <td colspan="7"><strong>{{ $value3['sales_return_date'] }}, {{ $value3['sales_return_number'] }}, {{ $value3['customer_name'] }}</strong></td>
                                                </tr>

                                                @if(!empty($value3['return_entries']))

                                                    <?php $serial1 = 0; ?>

                                                    @foreach($value3['return_entries'] as $key4 => $value4)
                                                        <tr>
                                                            <td style="text-align: right">{{ $serial1 + 1 }}</td>
                                                            <td>{{ $value4->product_name }} <br> {{ $value4->product_entry_name }}</td>
                                                            <td>{{ str_pad($value4->product_code, 10, "0", STR_PAD_LEFT) }}</td>
                                                            <td style="text-align: right">{{ number_format($value4->rate,2,'.',',') }}</td>
                                                            <td style="text-align: right">{{ $value4->quantity }}</td>
                                                            <td style="text-align: right">0.00</td>
                                                            <td style="text-align: right">{{ number_format($value4->total_amount,2,'.',',') }}</td>
                                                        </tr>

                                                        <?php $serial1++; ?>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                            @endif

                                            @endif

                                            <tr>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6">@if($value['invoice_note'] != null)<span style="float: left"><strong>Note :</strong> {{ $value['invoice_note'] }}</span>@endif<strong>Sub Total</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong>{{ number_format($value['sub_total'],2,'.',',') }}</strong></td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Adjustment {{ ($value['adjustment_type'] == 0) ? '('.$value['adjustment'].'%'.')' : '' }}</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong>{{ ($value['adjustment_type'] == 0) ? number_format($value['adjustment_perc'],2,'.',',') : number_format($value['adjustment'],2,'.',',') }}</strong></td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Vat {{ ($value['vat_type'] == 0) ? '('.$value['vat'].'%'.')' : '' }}</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong>{{ ($value['vat_type'] == 0) ? number_format($value['vat_perc'],2,'.',',') : number_format($value['vat'],2,'.',',') }}</strong></td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Tax {{ ($value['tax_type'] == 0) ? '('.$value['tax'].'%'.')' : '' }}</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong>{{ ($value['tax_type'] == 0) ? number_format($value['tax_perc'],2,'.',',') : number_format($value['tax'],2,'.',',') }}</strong></td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Receivable</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong>{{ number_format($value['invoice_amount'],2,'.',',') }}</strong></td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Sales Return</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong>{{ number_format($sales_return['total_return_amount'],2,'.',',') }}</strong></td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Net Receivable</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong>{{ number_format($value['invoice_amount'] - $sales_return['total_return_amount'],2,'.',',') }}</strong></td>
                                            </tr>

                                            <tr style="padding: 0px !important">
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Received</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong>{{ number_format($value['paid_amount'] - $value['return_paid'] - $value['return_due'],2,'.',',') }}</strong></td>
                                            </tr>

                                            <tr style="padding: 0px !important">
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Dues</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong>{{ number_format($value['due_amount'],2,'.',',') }}</strong></td>
                                            </tr>

                                            <tr style="padding: 0px !important">
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Payable</strong></td>
                                                <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong>{{ number_format($value['return_due'],2,'.',',') }}</strong></td>
                                            </tr>

                                            <?php
                                                $total_return_am        = $total_return_am + $sales_return['total_return_amount'];
                                                $total_return_paid_am   = $total_return_paid_am + $sales_return['total_paid_amount'];
                                            ?>
                                        @endforeach
                                        @endif

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Adjustment</strong></td>
                                            <td style="text-align: right;padding-bottom: 0px !important;"><strong>{{ number_format($total_adjustment_amount,2,'.',',') }}</strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Vat</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;"><strong>{{ number_format($total_vat_amount,2,'.',',') }}</strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Tax</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;"><strong>{{ number_format($total_tax_amount,2,'.',',') }}</strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Receivable</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;"><strong>{{ number_format($total_invoice_amount,2,'.',',') }}</strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Sales Return</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;"><strong>{{ number_format($total_return_am,2,'.',',') }}</strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Net Receivable</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;"><strong>{{ number_format($total_invoice_amount - $total_return_am,2,'.',',') }}</strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Received</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;"><strong>{{ number_format($total_paid_amount - $total_return_paid - $total_return_due,2,'.',',') }}</strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Dues</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong>{{ number_format($total_due_amount,2,'.',',') }}</strong></td>
                                        </tr>

                                        <tr style="color: #697FE9">
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important;border-color: white !important" colspan="6"><strong>Total Payable</strong></td>
                                            <td style="text-align: right;padding-top: 0px !important;padding-bottom: 0px !important"><strong>{{ number_format($total_return_due,2,'.',',') }}</strong></td>
                                        </tr>

                                    </tbody>
                                    
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection