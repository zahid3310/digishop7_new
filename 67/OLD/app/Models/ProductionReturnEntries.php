<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ProductionReturnEntries extends Model
{  
    protected $table = "production_return_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function productionEntries()
    {
         return $this->hasMany(ProductionEntries::class, "production_id");
    }
}
