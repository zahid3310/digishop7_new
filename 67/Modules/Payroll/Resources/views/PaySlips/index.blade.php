@extends('layouts.app')

@section('title', 'Pay Slips')

@push('scripts')
<style type="text/css">
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>
@endpush

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">List of Salary Sheets</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Payroll</a></li>
                                    <li class="breadcrumb-item active">List of Salary Sheets</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {!! Session::get('success') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('unsuccess'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! Session::get('unsuccess') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('errors'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! 'Some required fields are missing..!! Please try again..' !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        <hr style="margin-top: 0px">
                            
                        <form id="FormSubmit" action="{{ route('pay_slip_index') }}" method="GET">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                    <label style="text-align: right" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Search </label>
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                    <select id="employee_id" style="width: 100" class="form-control select2" name="employee_id">
                                        <option value="{{ $customer_name != null ? $customer_name['name'] : '' }}">{{ $customer_name != null ? $customer_name['name'] : '--All--' }}</option>
                                    </select>
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                                </div>

                                <div class="col-lg-1 col-md-1 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                </div>
                            </div>
                        </form>

                        <hr style="margin-top: 0px">

                        <div class="card">
                            <div class="card-body table-responsive">
                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>SALARY FOR THE MONTH</th>
                                            <th>Employee Name</th>
                                            <th style="text-align: right">Total Payable</th>
                                            <th style="text-align: right">Paid</th>
                                            <th style="text-align: right">Dues</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($salary_statements) && ($salary_statements->count() > 0))
                                        @foreach($salary_statements as $key => $salary_statement)
                                            <?php
                                                if ($salary_statement->month == 1)
                                                {
                                                    $month  = 'January';
                                                }
                                                elseif ($salary_statement->month == 1)
                                                {
                                                    $month  = 'February';
                                                }
                                                elseif ($salary_statement->month == 3)
                                                {
                                                    $month  = 'March';
                                                }
                                                elseif ($salary_statement->month == 4)
                                                {
                                                    $month  = 'April';
                                                }
                                                elseif ($salary_statement->month == 5)
                                                {
                                                    $month  = 'May';
                                                }
                                                elseif ($salary_statement->month == 6)
                                                {
                                                    $month  = 'Jun';
                                                }
                                                elseif ($salary_statement->month == 7)
                                                {
                                                    $month  = 'July';
                                                }
                                                elseif ($salary_statement->month == 8)
                                                {
                                                    $month  = 'August';
                                                }
                                                elseif ($salary_statement->month == 9)
                                                {
                                                    $month  = 'September';
                                                }
                                                elseif ($salary_statement->month == 10)
                                                {
                                                    $month  = 'October';
                                                }
                                                elseif ($salary_statement->month == 11)
                                                {
                                                    $month  = 'November';
                                                }
                                                elseif ($salary_statement->month == 12)
                                                {
                                                    $month  = 'December';
                                                }
                                            ?>

                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $month . ', ' . $salary_statement->year }}</td>
                                                <td>{{ $salary_statement->salaryStatements->employee->name }}</td>
                                                <td style="text-align: right">{{ number_format($salary_statement->net_payable,2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format($salary_statement->paid,2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format($salary_statement->net_payable - $salary_statement->paid,2,'.',',') }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a style="cursor: pointer" class="dropdown-item" data-toggle="modal" data-target="#myModal1" onclick="getEmployeeData({{$salary_statement->id}})">Pay</a>
                                                            <a style="cursor: pointer" href="{{ route('pay_slip_all_show', $salary_statement->id) }}" class="dropdown-item" target="_blank">Print</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Make New Payment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div style="padding: 0px" class="form-group col-md-4 col-4">
                            <label for="example-text-input" class="col-md-12 col-form-label">Payable</label>
                            <div class="col-md-12">
                                <input id="payable" type="text" class="form-control" readonly>
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group col-md-4 col-4">
                            <label for="example-text-input" class="col-md-12 col-form-label">Paid</label>
                            <div class="col-md-12">
                                <input id="paid" type="text" class="form-control" readonly>
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group col-md-4 col-4">
                            <label for="example-text-input" class="col-md-12 col-form-label">Dues</label>
                            <div class="col-md-12">
                                <input id="dues" type="text" class="form-control" readonly>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <form methode="get" action="{{ route('pay_slip_payment_store') }}" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                    {{ csrf_field() }}

                    <input id="employee_id_payment" name="employee_id_payment" type="hidden">
                    <input id="monthly_salary_sheet_id" name="monthly_salary_sheet_id" type="hidden">

                    <div class="form-group row">
                        <div style="padding: 0px" class="form-group col-md-4 col-4">
                            <label for="productname" class="col-md-12 col-form-label">Payment Date *</label>
                            <div class="col-md-12">
                                <input style="cursor: pointer" id="payment_date" name="payment_date" type="date" value="<?= date("Y-m-d") ?>" class="form-control" required>
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group col-md-4 col-4">
                            <label for="productname" class="col-md-12 col-form-label">Paid Amount *</label>
                            <div class="col-md-12">
                                <input id="paid_amount" name="paid_amount" type="text" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="modal-footer">
                        <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Make Payment</button>
                        <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {

        var site_url        = $('.site_url').val();

        $("#employee_id").select2({
            ajax: { 
            url:  site_url + '/invoices/customer/list/invoices',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['contact_type'] == 2 || result['id'] == 0)
                {
                    return result['text'];
                }
            },
        });
    });

    function getEmployeeData(monthly_salary_statement_id)
    {   
        var site_url  = $(".site_url").val();

        $.get(site_url + '/payroll/pay-slip/get-employee-data/'+ monthly_salary_statement_id, function(data){
            
            $("#payable").val(0);
            $("#payable").val(data.net_payable);

            $("#paid").val(0);
            $("#paid").val(data.paid);

            $("#dues").val(0);
            $("#dues").val(parseFloat(data.net_payable) - parseFloat(data.paid));


            $("#employee_id_payment").val(0);
            $("#employee_id_payment").val(data.employee_id);

            $("#monthly_salary_sheet_id").val(0);
            $("#monthly_salary_sheet_id").val(data.id);
        });
    }
</script>
@endsection