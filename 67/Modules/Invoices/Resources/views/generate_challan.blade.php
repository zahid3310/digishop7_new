@extends('layouts.app')

@section('title', 'Generate Challan')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Generate Challan</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Generate Challan</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                            Payment Successfull !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">

                                <button style="padding: 0px !important" class="btn btn-success">
                                    <a href="{{ route('invoices_challan_list', $find_invoice['id']) }}" class="btn btn-success float-right">List of ChaChallan</a>
                                </button>

                                <hr style="padding: 0px !important">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('invoices_challan_generate_store', $find_invoice['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                {{ csrf_field() }}

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Customer *</label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" class="form-control" required>
                                                   <option value="{{ $find_invoice['customer_id'] }}" selected>{{ $find_invoice['contact_name'] }}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Name </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ $find_invoice['customer_name'] }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Reference</label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" class="form-control">
                                                   <option value="{{ customersTableDetails($find_invoice['reference_id'])['id'] }}" selected>{{ customersTableDetails($find_invoice['reference_id'])['name'] }}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Address </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ $find_invoice['customer_address'] }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Date *</label>
                                            <div class="col-md-8">
                                                <input type="text" value="{{ date('d-m-Y', strtotime($find_invoice['invoice_date'])) }}" name="challan_date" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Phone </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ $find_invoice['customer_phone'] }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 280px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        @foreach($challan_items as $key => $value)
                                            <div style="margin-bottom: 0px !important" class="row di_{{$key}}">
                                                <div style="margin-bottom: 5px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label class="hidden-xs" for="productname">Product *</label>
                                                            <label style="display: none" class="show-xs" for="productname">Product *</label>
                                                            <select id="product_entries_{{$key}}" style="width: 100%" name="product_entries[]" name="product_entries[]" class="inner form-control select2" required>
                                                                <option value="{{ $value['item_id'] }}">
                                                                <?php
                                                                    $vari       = ProductVariationName($value['item_id']);
                                                                    $variation  = $vari != null ? ' - ' . $vari : '';

                                                                    if ($value['product_type'] == 1)
                                                                    {
                                                                        if ($value['pcs_per_cartoon'] != 0)
                                                                        {
                                                                            $pcs_per_cartoon = $value['pcs_per_cartoon'];
                                                                        }
                                                                        else
                                                                        {
                                                                            $pcs_per_cartoon = 1;
                                                                        }

                                                                        $pcs        = $value['stock_in_hand']/(($value['height']*$value['width'])/144);
                                                                        $cartoon    = $pcs/$pcs_per_cartoon;
                                                                    }
                                                                ?>

                                                                {{ $value['item_name'] . $variation . '(' . str_pad($value['product_code'], 6, "0", STR_PAD_LEFT) . ')' }}
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                @if($value->product_type == 1)
                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-2 col-sm-12 col-12 form-group">
                                                    <label class="hidden-xs" for="productname">Cart</label>
                                                    <label style="display: none" class="show-xs" for="productname">Cart</label>
                                                    <input type="text" class="inner form-control" value="{{ $value['cartoon'] }}" readonly />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-2 col-sm-12 col-12 form-group">
                                                    <label class="hidden-xs" for="productname">PCS</label>
                                                    <label style="display: none" class="show-xs" for="productname">PCS</label>
                                                    <input type="text" class="inner form-control" value="{{ $value['pcs'] }}" readonly />
                                                </div>
                                                @else
                                                <div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-2 col-sm-12 col-12 form-group">
                                                    <label class="hidden-xs" for="productname">Cart</label>
                                                    <label style="display: none" class="show-xs" for="productname">Cart</label>
                                                    <input type="text" class="inner form-control" value="{{ $value['cartoon'] }}" readonly />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-2 col-sm-12 col-12 form-group">
                                                    <label class="hidden-xs" for="productname">PCS</label>
                                                    <label style="display: none" class="show-xs" for="productname">PCS</label>
                                                    <input type="text" class="inner form-control" value="{{ $value['pcs'] }}" readonly />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-2 col-sm-12 col-12 form-group">
                                                    <label class="hidden-xs" for="productname">Unit</label>
                                                    <label style="display: none" class="show-xs" for="productname">Unit</label>
                                                    <input type="text" class="inner form-control" value="{{ $value->unit_name }}" readonly />
                                                </div>
                                                @endif

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">Inv. SFT/Qty </label>
                                                    <label style="display: none" class="show-xs" for="productname">Inv. SFT/Qty </label>
                                                    <input type="text" class="inner form-control invoiceQuantityCheck" value="{{ round($value['invoice_quantity'], 2) }}" placeholder="Invoice Quantity" required readonly/>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">Del. SFT/Qty </label>
                                                    <label style="display: none" class="show-xs" for="productname">Del. SFT/Qty </label>
                                                    <input type="text" class="inner form-control deliveredQuantityCheck" value="{{ round($value['delivered_quantity'], 2) }}" placeholder="Delivered Quantity" required readonly/>
                                                </div>
                                                
                                                @if($value->product_type == 1)
                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">Chal. Cart </label>
                                                    <label style="display: none" class="show-xs" for="productname">Chal. Cart </label>
                                                    <input id="cartoon_{{$key}}" type="text" name="challan_cart[]" class="inner form-control" oninput="getItemPrice({{$key}})" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">Chal. PCS </label>
                                                    <label style="display: none" class="show-xs" for="productname">Chal. PCS </label>
                                                    <input id="pcs_{{$key}}"  type="text" name="challan_pcs[]" class="inner form-control" oninput="getItemPrice({{$key}})" />
                                                </div>
                                                @else
                                                <div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">Chal. Cart </label>
                                                    <label style="display: none" class="show-xs" for="productname">Chal. Cart </label>
                                                    <input id="cartoon_{{$key}}" type="text" name="challan_cart[]" class="inner form-control" oninput="getItemPrice({{$key}})" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">Chal. PCS </label>
                                                    <label style="display: none" class="show-xs" for="productname">Chal. PCS </label>
                                                    <input id="pcs_{{$key}}"  type="text" name="challan_pcs[]" class="inner form-control" oninput="getItemPrice({{$key}})" />
                                                </div>
                                                @endif
                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <label class="hidden-xs" for="productname">Chal. SFT/Qty </label>
                                                    <label style="display: none" class="show-xs" for="productname">Chal. SFT/Qty </label>
                                                    <input id="challan_quantity_{{$key}}" type="text" name="challan_quantity[]" class="inner form-control" value="0" oninput="getItemPriceBackCalculation({{$key}})"  />
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>

                                <div style="margin-top: 5px !important;padding-bottom: 0px !important" class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Create</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Create & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('invoices_challan_list', $find_invoice['id']) }}">Close</a></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    function getItemPrice(x)
    {
        // $("#cartoon_"+x).val(0);

        var site_url    = $(".site_url").val();
        var entry_id    = $("#product_entries_"+x).val();
        var cartoon_val = $("#cartoon_"+x).val();
        var pcs_val     = $("#pcs_"+x).val();

        if(entry_id)
        {
            $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                if (data.type == 1)
                {
                    var pcs_equivalent_sft  = (parseFloat(data.height)*parseFloat(data.width))/144;

                    //
                        if (cartoon_val == '')
                        {
                            var cartoonVal  = 0;
                        }
                        else
                        {
                            var cartoonVal  = parseFloat(cartoon_val);
                        }

                    //
                        if (pcs_val == '')
                        {
                            var pcsVal  = 0;
                        }
                        else
                        {
                            var pcsVal  = parseFloat(pcs_val);
                        }

                    if (cartoonVal != 0)
                    {
                        var qtyVal1  = parseFloat(cartoonVal)*parseFloat(data.pcs_per_cartoon)*parseFloat(pcs_equivalent_sft);
                    }
                    else
                    {
                        var qtyVal1  = 0;
                    }

                    if (pcsVal != 0)
                    {
                        var qtyVal2  = parseFloat(pcsVal)*parseFloat(pcs_equivalent_sft);

                        if (cartoonVal == '')
                        {
                            var pcsToCartoon = parseFloat(pcsVal)/parseFloat(data.pcs_per_cartoon);

                            //x = 50; y = 15; res = x % y; x = (x - res) / y; [result = 3]
                            var cartAmount = (parseFloat(pcsVal) - (parseFloat(pcsVal)%parseFloat(data.pcs_per_cartoon)))/parseFloat(data.pcs_per_cartoon);
                            var pcsAmount  = parseFloat(pcsVal) - (parseFloat(cartAmount)*parseFloat(data.pcs_per_cartoon));

                            $("#cartoon_"+x).val(parseFloat(cartAmount).toFixed(2));
                            $("#pcs_"+x).val(parseFloat(pcsAmount).toFixed(2));
                        }
                    }
                    else
                    {
                        var qtyVal2  = 0;
                    }

                    var qtyVal = parseFloat(qtyVal1) + parseFloat(qtyVal2);

                    $("#challan_quantity_"+x).val(parseFloat(qtyVal).toFixed(2));
                }
                else
                {
                    $("#cartoon_"+x).val(0);
                    $("#pcs_"+x).val(0);
                }
            });
        }
    }

    function getItemPriceBackCalculation(x)
    {
        var site_url    = $(".site_url").val();
        var entry_id    = $("#product_entries_"+x).val();
        var sft_val     = $("#challan_quantity_"+x).val();

        if(entry_id)
        {
            $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                if (data.type == 1)
                {
                    var pcs_equivalent_sft  = (parseFloat(data.height)*parseFloat(data.width))/144;
                    var stock_in_pcs        = parseFloat(data.stock_in_hand)/parseFloat(pcs_equivalent_sft);
                    var stock_in_cart       = parseFloat(stock_in_pcs)/parseFloat(data.pcs_per_cartoon);

                    //
                        if (sft_val == '')
                        {
                            var sftVal  = 0;
                        }
                        else
                        {
                            var sftVal  = parseFloat(sft_val);
                        }

                    var findPcs = parseFloat(sft_val)/parseFloat(pcs_equivalent_sft);

                    //x = 50; y = 15; res = x % y; x = (x - res) / y; [result = 3]
                    var cartVal = (parseFloat(findPcs) - (parseFloat(findPcs)%parseFloat(data.pcs_per_cartoon)))/parseFloat(data.pcs_per_cartoon);

                    var pcsVal  = parseFloat(findPcs) - (parseFloat(cartVal)*parseFloat(data.pcs_per_cartoon));

                    $("#cartoon_"+x).val(parseFloat(cartVal).toFixed(2));
                    $("#pcs_"+x).val(parseFloat(pcsVal).toFixed(2));
                }

            });
        }
    }
</script>
@endsection