<!DOCTYPE html>
<html>

<head>
    <title>Income Statement</title>
    <link rel="icon" href="{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div class="col-md-12"  style="text-align: center;">
                        <p style="margin: 0px !important;font-size: 22px"><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p style="margin: 0px !important;font-size: 14px">{{ $user_info['address'] }}</p>
                        <p style="margin: 0px !important;font-size: 16px"><strong>Income Statement</strong></p>
                        <p style="margin: 0px !important;font-size: 14px"><strong>For the year ended {{ date('M, Y', strtotime($to_date)) }}</strong></p>
                        <p style="text-align: right">{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-content">
                        <!-- Sales Statement Start -->
                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 30%">PARTICULARS</th>
                                    <th style="text-align: center;width: 10%">TK</th>
                                    <th style="text-align: center;width: 10%">TK</th>
                                    <th style="text-align: center;width: 10%">TK</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php $grand_total = 0; ?>

                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;">Sales</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;">{{ number_format($sales + $sales_discount,2,'.',',') }}</td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                </tr>

                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;border-bottom: 1px solid white!important;">Sales Return</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;">{{ number_format($sales_return,2,'.',',') }}</td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;">{{ number_format($sales - $sales_return  + $sales_return_discount,2,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;">Sales Discount</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;">{{ number_format($sales_discount,2,'.',',') }}</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                </tr>

                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;">Sales Return Discount</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid black!important;">{{ number_format($sales_return_discount,2,'.',',') }}</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                </tr>

                                <!-- gap -->
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                <!-- gap -->

                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;">Opening Inventory</td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;">{{ number_format($opening_inventory,2,'.',',') }}</td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                </tr>

                                <!-- gap -->
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                <!-- gap -->

                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;">Purchase</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;">{{ number_format($purchase + $purchase_discount,2,'.',',') }}</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                </tr>

                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;">Purchase Return</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;">{{ number_format($purchase_return + $purchase_return_discount,2,'.',',') }}</td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                </tr>
                                
                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;">Purchase Profit</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;">{{ number_format($pur_free_item['total'],2,'.',',') }}</td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                </tr>

                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                </tr>

                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;">Purchase Discount</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;">{{ number_format($purchase_discount,2,'.',',') }}</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                </tr>

                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;">Purchase Return Discount</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;">{{ number_format($purchase_return_discount,2,'.',',') }}</td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid black!important;">{{ number_format($purchase - $purchase_return - $pur_free_item['total'] + $purchase_discount,2,'.',',') }}</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                </tr>

                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;">{{ number_format(($purchase - $purchase_return - $pur_free_item['total'] + $purchase_discount) + $opening_inventory,2,'.',',') }}</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                </tr>

                                <!-- gap -->
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                <!-- gap -->

                                <tr>
                                    <td style="text-align: left;font-size: 12px;">Closing Inventory</td>
                                    <td style="text-align: right;font-size: 12px;"></td>
                                    <td style="text-align: center;font-size: 12px;">{{ number_format($closing_inventory,2,'.',',') }}</td>
                                    <td style="text-align: center;font-size: 12px;">{{ number_format((($purchase - $purchase_return - $pur_free_item['total'] + $purchase_discount) + $opening_inventory) - $closing_inventory,2,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <th colspan="1" style="text-align: right;">GROSS PROFIT</th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: center;">{{ number_format(($sales - $sales_return  + $sales_return_discount) - ((($purchase - $purchase_return - $pur_free_item['total'] + $purchase_discount) + $opening_inventory) - $closing_inventory),2,'.',',') }}</th>
                                </tr>

                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;font-weight: bold;">Income</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                </tr>

                                @if(count($income_result) > 0)
                                <?php $numItemsIncome = count($income_result); $j=0; ?>
                                @foreach($income_result as $income_key => $income_value)
                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;">{{ $income_value['account_name'] }}</td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;">{{ number_format($income_value['balance'],2,'.',',') }}</td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;">{{ ++$j === $numItemsIncome ? number_format($total_income,2,'.',',') : '' }}</td>
                                </tr>
                                @endforeach
                                @endif

                                <!-- gap -->
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;" colspan="4"></td>
                                    </tr>
                                <!-- gap -->

                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;font-weight: bold;">Expense</td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: right;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                </tr>

                                @if(count($expense_result) > 0)
                                <?php $numItemsExpense = count($expense_result); $j=0; $grand_expense = 0; ?>
                                @foreach($expense_result as $expense_key => $expense_value)
                                <tr>
                                    <td style="text-align: left;font-size: 12px;border-bottom: 1px solid white!important;">{{ $expense_value['account_name'] }}</td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;"></td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;">{{ number_format($expense_value['balance'],2,'.',',') }}</td>
                                    <td style="text-align: center;font-size: 12px;border-bottom: 1px solid white!important;">{{ ++$j === $numItemsExpense ? number_format($total_expense,2,'.',',') : '' }}</td>
                                </tr>
                                @endforeach
                                @endif

                                <tr>
                                    <td style="text-align: left;font-size: 12px;"></td>
                                    <td style="text-align: right;font-size: 12px;"></td>
                                    <td style="text-align: right;font-size: 12px;"></td>
                                    <td style="text-align: right;font-size: 12px;"></td>
                                </tr>
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="1" style="text-align: right;">NET PROFIT</th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: center;">{{  number_format(($sales - $sales_return  + $sales_return_discount) - ((($purchase - $purchase_return - $pur_free_item['total'] + $purchase_discount) + $opening_inventory) - $closing_inventory) + $total_income - $total_expense,2,'.',',') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- Sales Statement End -->

                        <!-- Footer Start -->
                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>