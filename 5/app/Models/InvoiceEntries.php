<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceEntries extends Model
{
    protected $table = "invoice_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoices','invoice_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Products','product_id');
    }
}
