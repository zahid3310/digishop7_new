<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('users')->group(function() {
    Route::get('/', 'UsersController@index')->name('users_index');
    Route::get('/all-users', 'UsersController@indexAll')->name('users_index_all');
    Route::post('/store', 'UsersController@store')->name('users_store');
    Route::get('/edit/{id}', 'UsersController@edit')->name('users_edit');
    Route::post('/update/{id}', 'UsersController@update')->name('users_update');
    Route::get('/delete/{id}', 'UsersController@destroy')->name('users_destroy');
    Route::get('/truncate', 'UsersController@truncate')->name('users_truncate');
    Route::get('/cache-clear', 'UsersController@cacheClear')->name('cache_clear');
    Route::get('/edit-profile', 'UsersController@editProfile')->name('users_edit_profile');
    Route::post('/update-profile', 'UsersController@updateProfile')->name('users_update_profile');
    Route::get('/edit-settings', 'UsersController@editSettings')->name('users_edit_settings');
    Route::post('/update-settings', 'UsersController@updateSettings')->name('users_update_settings');
});