

<?php $__env->startSection('title', 'Send Message'); ?>

<style type="text/css">
    .table td, .table th {
        padding: 0px !important;
    }

    .button-items .btn {
        margin-bottom: 5px;
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Send Message</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Message</a></li>
                                    <li class="breadcrumb-item active">Send Message</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <form id="FormSubmit" action="<?php echo e(route('messages_send_send')); ?>" method="post" files="true" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>


                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div style="padding: 0.5rem !important" class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <!--<div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button style="border-radius: 0px !important" type="button" class="btn btn-success waves-effect waves-light"  data-toggle="modal" data-target="#myModal">
                                            Send Custom SMS
                                        </button>
                                        <button style="border-radius: 0px !important" type="button" class="btn btn-danger waves-effect waves-light">
                                            <a style="color: white" href="<?php echo e(route('messages_send_list')); ?>" target="_blank">Already Send</a>
                                        </button>
                                    </div>
                                </div>

                                <hr> -->

                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">SMS Type *</label>
                                        <select style="cursor: pointer" id="message_id" class="form-control select2" name="message_id" onchange="SelectContact()">
                                            <option value="">--Select Message--</option>
                                            <?php if(!empty($messages) && ($messages->count() > 0)): ?>
                                            <?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($message->id); ?>"><?php echo e($message->title); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Contact Category *</label>
                                        <select style="cursor: pointer" id="contact_category_id" class="form-control select2" name="contact_category_id" onchange="SelectContact()">
                                            <option value="">--Select Contact Category--</option>
                                            <option value="0">Customer</option>
                                            <option value="1">Supplier</option>
                                            <option value="2">Employee</option>
                                            <option value="3">Phone Book</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Message</label>
                                        <textarea type="text" class="form-control" id="messageBody" name="message_body" rows="3"></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div style="padding-left: 15px" class="form-check form-check-right">
                        <label style="font-size: 14px;margin-top: -5px" class="form-check-label">
                            Send SMS
                        </label>
                    </div>

                    <div style="padding-left: 10px" class="form-check form-check-right">
                        <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck1" checked>
                        <label style="font-size: 14px;margin-top: -5px" class="form-check-label" for="defaultCheck1">
                            Non Masking
                        </label>
                    </div>

                    <div style="padding-left: 10px" class="form-check form-check-right">
                        <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                        <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                            Masking
                        </label>
                    </div>

                    <div style="padding-left: 10px" class="form-check form-check-right">
                        <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck3">
                        <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck3">
                            Voice
                        </label>
                    </div>

                    <div style="padding-left: 40px" class="form-check form-check-right">
                        <button style="border-radius: 0px;padding: 0.3rem .5rem !important" type="submit" class="btn btn-primary waves-effect waves-light col-md-12">Send</button>
                    </div>


                    <div style="padding-top: 10px" class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <table id="contactList" class="table table-striped">

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Send SMS</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Phone *</label>
                        <div class="col-md-12">
                            <input id="phone" name="phone" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Message Body *</label>
                        <div class="col-md-12">
                            <textarea class="ckeditor" type="text" name="body" id="body" class="inner form-control" required><?php echo e(old('body')); ?></textarea>
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Send</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(url('public/admin_panel_assets/editor/ckeditor.js')); ?>"></script>

    <script type="text/javascript">
        function SelectContact()
        {
            var site_url                = $(".site_url").val();
            var message_id              = $("#message_id").val();
            var contact_category_id     = $("#contact_category_id").val();

            $.get(site_url + '/messages/get-message/'+ message_id, function(data){

                var body = data.body;

                $("#messageBody").val(body);

            });

            $.get(site_url + '/messages/contact-list-by-category/'+ message_id + '/' + contact_category_id, function(data){

                var message = data.message.body.replace(/(<([^>]+)>)/ig,"");
                var list    = '';
                    list    += '<tr>' +
                                '<th id="select-all" style="width: 4%" class="active">' +
                                    '<input id="selectAll" type="checkbox" class="select-all" onclick="checkVal()" />' +
                                '</th>' +
                                '<th style="width: 6%" class="success">' + 'SL' + '</th>' +
                                '<th style="width: 15%" class="success">' + 'Phone' + '</th>' +
                                '<th style="width: 25%" class="warning">' + 'Name' + '</th>' +
                                '<th style="width: 50%" class="danger">' + 'Message' + '</th>' +
                            '</tr>';

                var serial  = 1;
                $.each(data.contacts, function(i, data_list)
                {   
                    list += '<tr>' +
                                '<td class="active">' +
                                    '<input id="checkbox_'+i+'" type="checkbox" class="select-item checkbox" onclick="checkValUnik('+i+')" />' +
                                    '<input id="check_status_'+i+'" type="hidden" name="check_box_id[]" class="checkboxId" value="0" />' +
                                    '<input type="hidden" name="contact_id[]" value="'+ data_list.id +'" />' +
                                '</td>' +
                                '<td class="success">' + serial + '</td>' +
                                '<td class="success">' + data_list.phone + '</td>' +
                                '<td class="warning">' + data_list.name + '</td>' +
                                '<td class="danger">' + message + '</td>' +
                            '</tr>';
                    serial++;
                });

                $("#contactList").empty();
                $("#contactList").append(list);

            });
        }
    </script>

    <script type="text/javascript">
        function checkVal()
        {
            if ($('#selectAll').is(":checked"))
            {
                $('.checkbox').prop('checked', true);
                $('.checkboxId').val(1);
            }
            else
            {
                $('.checkbox').prop('checked', false);
                $('.checkboxId').val(0);
            }
        }

        function checkValUnik(i)
        {
            if ($('#checkbox_'+i).is(":checked") == true)
            {
                $('#check_status_'+i).val(1);
            }
            else
            {
                $('#check_status_'+i).val(0);
            }

        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/52/Modules/Messages/Resources/views/message_send_index.blade.php ENDPATH**/ ?>