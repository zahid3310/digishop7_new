@extends('layouts.app')

@section('title', 'Print KOT')



<style type="text/css">
    @media print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 95mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }

        .circle {
            width: 95px;
            height: 95px;
            line-height: 20px;
            padding-top: 30px;
            border-radius: 50%;
            font-size: 20px;
            color: #fff;
            text-align: center;
            background: gray;
        }
    }
</style>


@section('content')
    <div class="main-content marginTopPrint">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row d-print-none">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Print Pos</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>


   
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div id="hedColor" style="margin-top: 15px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px;font-weight: bold">{{ $user_info['organization_name'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px;font-weight: bold">{{ $user_info['address'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 15px;font-weight: bold">{{ $user_info['contact_number'] }}</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 15px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">Invoice</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 15px">Date : {{ date('d/m/Y', strtotime($invoice['invoice_date'])) }}</span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 15px">Time : {{ date('h:i a', strtotime(now())) }}</span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 15px">Invoice# : {{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }}</span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">
                                    
                                </div>

                                <div style="border-top: 2px dashed black;border-bottom: 2px dashed black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 15px;padding-top: 10px;padding-bottom: 10px" class="col-6"><strong>Qty</strong></div>
                                    <div style="font-size: 15px;padding-top: 10px;padding-bottom: 10px" class="col-6"><strong>Item Name</strong></div>
                                </div>

                                @if(!empty($find_invoice_entries) && ($find_invoice_entries->count() > 0))


                                    @foreach($find_invoice_entries as $key => $value)

                                    <div style="padding-bottom: 10px;border-bottom: 2px dashed black" id="DetailsFontSize" class="row">

                                        <div  class="col-6">
                                            {{ $value['quantity'] }} {{ $value->convertedUnit->name }}
                                        </div>

                                        <div  class="col-6">
                                            @if($value['product_type'] == 1)
                                                <?php echo $value['product_entry_name']; ?>
                                            @else
                                                <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                            @endif
                                        </div>
       
                                    </div>

                                

                                    @endforeach
                                @endif

                                

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 15px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01748715715</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          

            </div>
        </div>
    </div>
    <input id="invoice_id" type="hidden" value="{{$invoice['id']}}">
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        javascript:window.print();
        
        setTimeout(function () { 
            var site_url  = $('.site_url').val();
            var invoice_id  = $('#invoice_id').val();
            window.location.replace(site_url + '/invoices/edit/' + invoice_id);
         }, 100);
    });

    // window.onafterprint = function(e){
    //     var site_url  = $('.site_url').val();
    //     window.location.replace(site_url + '/invoices');
    // };
</script>
@endsection