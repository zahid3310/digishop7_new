@extends('layouts.app')

@section('title', 'Edit Stock Transfer')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Transfer Stock </h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Stock Transfer</a></li>
                                    <li class="breadcrumb-item active">Stock Transfer</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('stock_transfer_update', $find_transfer['id']) }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <input type="hidden" id="main_unit_id" name="main_unit_id" value="{{ $find_transfer['main_unit_id'] }}">

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Transfer Date *</label>
                                        <input id="transfer_date" name="transfer_date" type="text" value="{{ date('d-m-Y', strtotime($find_transfer['date'])) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Product *</label>
                                        <select style="width: 100%" id="product_id" name="product_id" class="form-control select2" onchange="getItemPrice()">
                                           <option value="{{ $find_transfer['product_entry_id'] }}">{{ $find_transfer->productEntries->name }}</option>
                                        </select>
                                    </div>

                                    <div style="display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Transfer From *</label>
                                        <select style="width: 100%" id="transfer_from" name="transfer_from" class="form-control select2">
                                           <option value="">--Transfer From--</option>
                                           @if($branches->count() > 0)
                                           @foreach($branches as $branche)
                                           <option value="{{ $branche->id }}" {{ $find_transfer['transfer_from'] == $branche['id'] ? 'selected' : '' }}>{{ $branche->name }}</option>
                                           @endforeach
                                           @endif
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Transfer To *</label>
                                        <select style="width: 100%" id="transfer_to" name="transfer_to" class="form-control select2" required>
                                           <option value="">--Transfer To--</option>
                                           @if($branches->count() > 0)
                                           @foreach($branches as $branche)
                                           <option value="{{ $branche->id }}" {{ $find_transfer['transfer_to'] == $branche['id'] ? 'selected' : '' }}>{{ $branche->name }}</option>
                                           @endforeach
                                           @endif
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Note</label>
                                        <input type="text" name="note" class="inner form-control" id="note" value="{{ $find_transfer['note'] }}" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Quantity *</label>
                                        <input type="text" name="quantity" class="inner form-control" id="quantity" value="{{ $find_transfer['quantity'] }}" required />
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Unit</label>
                                        <select style="cursor: pointer" name="unit_id" class="inner form-control single_select2" id="unit_id">
                                            <option value="{{ $find_transfer['main_unit_id'] }}" selected>{{ $find_transfer->productEntries->unit->name }}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('stock_transfer_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Transfers</h4>

                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Date</th>
                                            <th>Transfer From</th>
                                            <th>Transfer To</th>
                                            <th>Note</th>
                                            <th style="text-align: center">Quantity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($transfers) && ($transfers->count() > 0))
                                        @foreach($transfers as $key => $transfer)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ date('d-m-Y', strtotime($transfer['date'])) }}</td>
                                                <td>{{ $transfer->transfer_from != null ? $transfer->transferFrom->name : '' }}</td>
                                                <td>{{ $transfer->transfer_to != null ? $transfer->transferTo->name : '' }}</td>
                                                <td>{{ $transfer->note }}</td>
                                                <td style="text-align: center">{{ $transfer->quantity }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('stock_transfer_edit', $transfer['id']) }}">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        
        var site_url  = $('.site_url').val();

        $("#product_id").select2({
            ajax: { 
            url:  site_url + '/reports/sales-statement/product-list',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['id'] != 0)
                {
                    return result['text'];
                }
            },
        });
    }); 

    function getItemPrice()
    {
        //For getting item commission information from items table start
        var entry_id  = $("#product_id").val();
        var site_url  = $(".site_url").val();

        if(entry_id)
        {
            $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                var list    = '';
                $.each(data.unit_conversions, function(i, data_list)
                {   
                    list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                });

                $("#unit_id").empty();
                $("#unit_id").append(list);

                $("#main_unit_id").val(data.product_entries.unit_id);
            });
        }
        //For getting item commission information from items table end
    }   
</script>
@endsection