

<?php $__env->startSection('title', 'Purchase Return'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Purchase Return</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Purchase</a></li>
                                    <li class="breadcrumb-item active">Sales Return</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('purchase_return_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                <?php echo e(csrf_field()); ?>


                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="return_date">Return Date *</label>
                                            <input id="return_date" name="return_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Select Supplier * </label>
                                            <select id="customer_id" name="customer_id" class="form-control select2 col-md-12" onchange="BillList()" required>
                                               <option value="">-- Select Supplier --</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label id="bbShow" class="control-label">Payable </label>
                                            <input id="balance" name="previous_due" type="text" value="" class="form-control" value="0" readonly>
                                            <input id="bbBalance" type="hidden" name="balance_type" class="form-control" value="1">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Purchase Number * </label>
                                            <select id="bill_id" name="bill_id" class="form-control select2 col-md-12" onchange="billDetails()" required>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="return_note">Note</label>
                                            <input id="return_note" name="return_note" type="text" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px;" class="row">
                                    <div style="background-color: #D2D2D2;padding-top: 13px;height: 230px;overflow-y: auto;overflow-x: auto" class="col-md-12">
                                        <div class="inner-repeater mb-4">
                                            <div data-repeater-list="inner-group" class="inner form-group row">
                                                <div id="bill_entry_list" class="inner col-lg-12 ml-md-auto">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                
                                    <div style="background-color: #F4F4F7;height: 145px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Pur. Amount</label>
                                            <div class="col-md-7">
                                                <input type="text" id="subTotalBdt" name="sub_total_amount" class="form-control" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Discount</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control totalDiscountType" id="total_discount_type_0" name="total_discount_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="1" selected>BDT</option>
                                                    <option style="padding: 10px" value="0">%</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="total_discount_0" type="text" class="form-control totalDiscount" name="total_discount_amount" value="0" oninput="calculateActualAmount(0)" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Net Payable</label>
                                            <div class="col-md-7">
                                                <input type="text" id="totalBdt" class="form-control" readonly>
                                                <input style="display: none" type="text" id="totalBdtShow" name="total_amount" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 145px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Less</label>
                                            <div class="col-md-7">
                                                <input id="less" type="text" class="form-control width-xs" value="0" oninput="lessCalculate()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Ret. Amount</label>
                                            <div class="col-md-7">
                                                <input id="totalReturnedBdt"  name="total_return_amount" type="text" class="form-control width-xs" readonly>
                                                <input id="totalReturnedBdtShow" type="hidden" class="form-control width-xs" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Return Paid</label>
                                            <div class="col-md-7">
                                                <input id="totalReturnedBdtReturn" type="text" class="form-control width-xs" name="total_return_amount_paid" oninput="amoutPaid()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">VAT</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control" id="vat_type_0" name="vat_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="0" <?php echo e(Auth::user()->vat_type == 0 ? 'selected' : ''); ?>>%</option>
                                                    <option style="padding: 10px" value="1" <?php echo e(Auth::user()->vat_type == 1 ? 'selected' : ''); ?>>BDT</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="vat_amount_0" type="text" class="form-control width-xs" name="vat_amount" value="<?php echo e(Auth::user()->vat_amount); ?>" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Dis. Note</label>
                                            <div class="col-md-7">
                                                <input id="total_discount_note" type="text" class="form-control width-xs" name="total_discount_note" value="<?php echo e(old('total_discount_note')); ?>" placeholder="Discount Note">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Coupon</label>
                                            <div class="col-md-7">
                                                <input id="coupon_code" type="number" class="form-control width-xs couponCode" name="coupon_code" onchange="couponMembership()" placeholder="Coupon/Membership">
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #B0B0B0;height: 145px;padding-top: 13px;padding-right: 10px" class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="amount_paid">Amount Paid</label>
                                                    <input id="amount_paid" name="amount_paid" type="text" class="form-control" readonly>
                                                </div>
                                            </div>

                                            <div style="padding-left: 0px" class="col-sm-4 form-group">
                                                <label class="control-label">Paid Through</label>
                                                <select style="cursor: pointer;" name="paid_through" class="form-control select2">
                                                    <?php if(!empty($paid_accounts) && ($paid_accounts->count() > 0)): ?>
                                                    <?php $__currentLoopData = $paid_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $paid_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($paid_account['id']); ?>"><?php echo e($paid_account['account_name']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="note">Note</label>
                                                    <input id="note" name="note" type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div style="margin-top: 5px !important;padding-bottom: 0px !important" class="form-group row">
                                    <div class="button-items col-lg-12">
                                        <button style="border-radius: 0px !important" name="print" type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                        <input type="hidden" name="print" id="pRint">
                                        <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('purchase_return_index')); ?>">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Purchase Returns</h4>

                                <div style="margin-right: 0px" class="row">
                                    <div class="col-lg-9 col-md-6 col-sm-4 col-4"></div>
                                    <div class="col-lg-1 col-md-2 col-sm-4 col-4">Search : </div>
                                    <div class="col-lg-2 col-md-4 col-sm-4 col-4">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th>Return#</th>
                                            <th>Bill#</th>
                                            <th>Supplier</th>
                                            <th>Return Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="purchase_return_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <input type="hidden" id="purchase_return_id_put">
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Are you sure want to delete the entry ?</label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="delete_btn" class="btn btn-primary waves-effect waves-light delete_btn">Delete</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if ((result['contact_type'] == 1) && result['id'] != 0)
                    {
                        if (result['address'] != null)
                        {
                            var address =  ' || ' + result['address'];
                        }
                        else
                        {
                            var address = '';
                        }

                        return result['text'] + address;
                    }
                },
            });

            if (location.search != "")
            {
                var locationValue = (new URL(location.href)).searchParams.get('supplier_id'); 
            
                if (locationValue != null)
                {
                    var selected_customer_id = (new URL(location.href)).searchParams.get('supplier_id');
                    $.get(site_url + '/salesreturn/find-customer-name/' + selected_customer_id, function(data){
                        var customer_name = data.name;
                        $("#customer_id").empty().append('<option value="'+ selected_customer_id +'">' + customer_name + '</option>').val(selected_customer_id);
                    });

                    var selected_bill_id = (new URL(location.href)).searchParams.get('bill_id');
                    $.get(site_url + '/purchasereturn/find-bill-details/' + selected_bill_id, function(data){
                        $("#bill_id").empty().append('<option value = "' +  selected_bill_id + '">' + 'BILL - ' + data.bill_number.padStart(6, '0') + ' | Date : ' + formatDate(data.bill_date) + ' | Amount : ' + data.bill_amount + '</option>').val(selected_bill_id).trigger('change');
                    });

                    var type = $("#bbBalance").val();

                    if (type == 1)
                    {
                        $("#bbShow").html('Payable');
                        $("#bbBalance").val(type);
                        $("#totalReturnedBdtReturn").val(0);
                        $("#amount_paid").val(0);
                    }
                    else
                    {
                        $("#bbShow").html('Advance');
                        $("#bbBalance").val(type);
                    }

                    var customerId      = locationValue;

                    $.get(site_url + '/purchasereturn/calculate-opening-balance/' + customerId, function(data){

                        $("#balance").empty();
                        $("#balance").val(data.balance);

                        if (data.type == 1)
                        {
                            $("#bbShow").html('Payable');
                            $("#bbBalance").val(data.type);
                        }
                        else
                        {
                            $("#bbShow").html('Advance');
                            $("#bbBalance").val(data.type);
                        }
                    });
                }
            }

            $('#registeredCustomer').hide();
            $('#newCustomer').hide();
            $('.newCustomer').hide();

            var site_url        = $('.site_url').val();

            $.get(site_url + '/purchasereturn/purchase-return/list/load', function(data){

                listLoad(data);
            });
        });

        function searchPayment()
        {
            var search_text     = $('#searchPayment').val();
            var site_url        = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/purchasereturn/purchase-return/search/list/' + search_text, function(data){

                listLoad(data);
            });
        }

        function lessCalculate()
        {
            var lessAmount      = $("#less").val();
            var returnAmount    = $("#totalReturnedBdtShow").val();

            if (lessAmount == '')
            {
                var lessAmount = 0;
            }
            else
            {
                var lessAmount = $("#less").val();;
            }

            if (returnAmount == '')
            {
                var returnAmount = 0;
            }
            else
            {
                var returnAmount = $("#totalReturnedBdtShow").val();;
            }

            var actualAmount    = parseFloat(returnAmount) - parseFloat(lessAmount);
            var type            = $("#bbBalance").val();

            if (type == 1)
            {
                $("#totalReturnedBdt").val(actualAmount);
                $("#amount_paid").val(0);
                $("#totalReturnedBdtReturn").val(0);
            }
            else
            {
                $("#totalReturnedBdt").val(actualAmount);
                $("#amount_paid").val(actualAmount);
                $("#totalReturnedBdtReturn").val(0);
            }
        }
    </script>

    <script type="text/javascript">
        function BillList() 
        {
            var customer_id     = $('#customer_id').val();
            var site_url        = $(".site_url").val();

            if(customer_id)
            {   
                $.get(site_url + '/purchasereturn/bill-list-by-customer/'+ customer_id, function(data){

                    var list = '';

                    list    += '<option value = "">' + '--Select Bill--' + '</option>';

                    $.each(data, function(i, data)
                    {
                        list += '<option value = "' +  data.id + '">' + 'BILL - ' + data.bill_number.padStart(6, '0') + ' | Date : ' + formatDate(data.bill_date) + ' | Amount : ' + data.bill_amount.toFixed(2) + '</option>';

                    });

                    $("#bill_id").empty();
                    $("#bill_id").append(list);
                });
            }
        }

        function billDetails()
        {
            $("#totalDivShow").show();

            var site_url    = $('.site_url').val();
            var bill_id     = $('#bill_id').val();

            $.get(site_url + '/purchasereturn/bill-entries-list-by-bill/' + bill_id, function(data){

                var bill_enrty_list      = '';
                var sub_total            = 0;
                var serial               = 1;
                $.each(data.bill_entries, function(i, bill_entry_data)
                {    
                    if (serial == 1)
                    {
                        var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                        var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                        var unit_label          = '<label class="hidden-xs" for="productname">Unit</label>\n';
                        var return_unit_label   = '<label class="hidden-xs" for="productname">R/Unit</label>\n';
                        var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                        var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">Discount</label>\n';
                        var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n';
                        var amount_label        = '<label class="hidden-xs" for="productname">Amount</label>\n';
                        var action_label        = '<label class="hidden-xs" for="productname">Return Qty</label>\n';
                    }
                    else
                    {
                        var product_label       = '';
                        var rate_label          = '';
                        var unit_label          = '';
                        var return_unit_label   = '';
                        var quantity_label      = '';
                        var discount_label      = '';
                        var type_label          = '';
                        var amount_label        = '';
                        var action_label        = '';
                    }

                    sub_total           += parseFloat(bill_entry_data.total_amount);

                    if (bill_entry_data.discount_type == 1)
                    {
                        var discoumt_percent    =   '<div style="padding-left: 0px;padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px" class="col-md-5 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n' +
                                                                type_label +
                                                        '<select name="discount_type[]" class="form-control">\n' +
                                                            '<option value="1">BDT</option>' +
                                                        '</select>\n';
                    }
                    else
                    {
                        var discoumt_percent    =   '<div style="padding-left: 0px;padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px" class="col-md-5 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n' +
                                                                type_label +
                                                        '<select name="discount_type[]" class="form-control">\n' +
                                                            '<option value="0">%</option>' +
                                                        '</select>\n';
                    }

                    if (bill_entry_data.quantity == 0)
                    {
                        var return_quantity = '<input id="return_quantity_'+ serial +'" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="0" oninput="calculate('+ serial +')" readonly />\n' 
                    }
                    else
                    {
                        var return_quantity = '<input id="return_quantity_'+ serial +'" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="0" oninput="calculate('+ serial +')" />\n' 
                    }

                    var units    = '';
                    $.each(bill_entry_data.unit_conversions, function(i, data_list_unit)
                    {   
                        units    += '<option value = "' + data_list_unit.unit_id + '">' + data_list_unit.unit_name + '</option>';
                    });

                    units    += '<option value = "' + bill_entry_data.converted_unit_id + '" selected>' + bill_entry_data.conversion_unit_name + '</option>';

                    bill_enrty_list += ' ' + '<div style="margin-bottom: 0px !important" class="mb-3 row align-items-center">' +
                                                    '<div style="padding-left: 0px;padding-top: 10px;padding-bottom: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 co-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Category *</label>\n' +
                                                        '<input type="text" class="inner form-control" value="'+ bill_entry_data.product_name +'" readonly />\n' +
                                                        '<input name="product_id[]" type="hidden" value="'+ bill_entry_data.product_id +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+serial+'" value="'+ bill_entry_data.main_unit_id +'" readonly />\n' +

                                                    '<div style="padding-left: 10px;padding-top: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-3 col-md-3 col-sm-6 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<p style="color: black;font-weight: bold;font-size: 12px;">' + bill_entry_data.entry_name + ' ( ' + pad(bill_entry_data.product_code, 6) + ' )' + '</p>\n' +
                                                        '<input id="product_entry_id_'+serial+'" name="product_entries[]" type="hidden" value="'+ bill_entry_data.product_entry_id +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+serial+'" value="'+bill_entry_data.main_unit_id+'" />\n' +

                                                    '<div style="padding-left: 0px;padding-top: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                        unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_'+serial+'" >\n' +
                                                        '<option value="'+ bill_entry_data.conversion_unit_id +'">'+ bill_entry_data.conversion_unit_name +'</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="padding-left: 0px;padding-top: 10px;padding-bottom: 0px;margin-bottom: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input id="rate_'+ serial +'" type="text" class="inner form-control rate" value="'+ bill_entry_data.rate +'" readonly />\n' + 
                                                        '<input name="rate[]" type="hidden" value="'+ bill_entry_data.rate +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<div style="padding-left: 0px;padding-top: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label +
                                                        '<input id="quantity_'+ serial +'" type="text" class="inner form-control" value="'+ bill_entry_data.quantity +'" readonly />\n' +
                                                        '<input name="quantity[]" type="hidden" class="quantity" value="'+ bill_entry_data.original_quantity +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<div style="padding-left: 0px;padding-top: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 10px;padding-bottom: 0px" class="col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" class="inner form-control" value="'+ bill_entry_data.discount_amount +'"  readonly />\n' +
                                                                '<input name="discount_amount[]" type="hidden" value="'+ bill_entry_data.discount_amount +'" />\n' +
                                                            '</div>\n' +

                                                            discoumt_percent +
                                                            
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="padding-left: 0px;padding-top: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Amount</label>\n' +
                                                        amount_label +
                                                        '<input id="amount_'+ serial +'" type="text" class="inner form-control amount" value="'+ bill_entry_data.total_amount +'" readonly />\n' + 
                                                        '<input name="amount[]" type="hidden" value="'+ bill_entry_data.total_amount +'" readonly />\n' +
                                                    '</div>\n' + 

                                                    '<div style="padding-left: 0px;padding-top: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Return Qty</label>\n' +
                                                        action_label +
                                                        return_quantity +
                                                    '</div>\n' +
                                                '</div>\n';

                    serial++;
                });

                if (data.bill.total_discount_type == 1)
                {
                    var total_discount_type    = '<option value="1" selected>' + 'BDT' + '</option>\n';
                }
                else
                {
                    var total_discount_type    = '<option value="0" selected>' + '%' + '</option>\n';
                }

                if (data.bill.vat_type == 1)
                {
                    var vat_type    = '<option value="1" selected>' + 'BDT' + '</option>\n';
                }
                else
                {
                    var vat_type    = '<option value="0" selected>' + '%' + '</option>\n';
                }

                if (data.bill.tax_type == 1)
                {
                    var tax_type    = '<option value="1" selected>' + 'BDT' + '</option>\n';
                }
                else
                {
                    var tax_type    = '<option value="0" selected>' + '%' + '</option>\n';
                }

                $("#total_discount_type_0").empty();
                $("#total_discount_type_0").append(total_discount_type);
                $("#vat_type_0").empty();
                $("#vat_type_0").append(vat_type);
                $("#tax_type_0").empty();
                $("#tax_type_0").append(tax_type);

                $("#subTotalBdtShow").html(sub_total.toFixed());
                $("#subTotalBdt").val(sub_total.toFixed());
                $("#totalBdtShow").html(data.bill.bill_amount.toFixed());
                $("#totalBdt").val(data.bill.bill_amount.toFixed());
                $("#total_discount_0").val(data.bill.total_discount_amount.toFixed());
                $("#total_discount_note").val(data.bill.total_discount_note);
                $("#vat_amount_0").val(data.bill.total_vat);
                $("#tax_amount_0").val(data.bill.total_tax);

                $("#bill_entry_list").empty();
                $("#bill_entry_list").append(bill_enrty_list);
            });
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entry_id_"+x).val();
            var unit_id             = $("#unit_id"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                var mainStock         = $("#quantity_"+x).val();
                // var convertToMain     = 
                var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                $("#quantity_"+x).val(convertedStock);
                $("#rate_"+x).val(parseFloat(data.purchase_price).toFixed(2));
            });
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function calculate(x)
        {
            var return_quantity     = $('.returnQuantity').map((_,el) => el.value).get();
            var amount              = $('.amount').map((_,el) => el.value).get();
            var quantity            = $('.quantity').map((_,el) => el.value).get();
            var rate                = $('.rate').map((_,el) => el.value).get();

            var vatType             = $("#vat_type_0").val();
            var vatAmount           = $("#vat_amount_0").val();
            var taxType             = $("#tax_type_0").val();
            var taxAmount           = $("#tax_amount_0").val();
            var totalDiscount       = $("#total_discount_0").val();
            var totalDiscountType   = $("#total_discount_type_0").val();
            var totalBdt            = $("#totalBdt").val();
            var subTotal            = $("#subTotalBdt").val();

            var total                   = 0;
            var total_quantity          = 0;
            var total_return_quantity   = 0;

            for (var i = 0; i < return_quantity.length; i++)
            {
                if (return_quantity[i] > 0)
                {   
                    var result   = (parseFloat(amount[i])/parseFloat(quantity[i]))*parseFloat(return_quantity[i]);

                    total       += parseFloat(result);
                }

                total_quantity          += parseFloat(quantity[i]);
                total_return_quantity   += parseFloat(return_quantity[i]);
            }

            if (vatType == 0)
            {
                var vatTypeCal     = (parseFloat(total)*parseFloat(vatAmount))/100;
            }
            else
            {
                var vatTypeCal     = (parseFloat(vatAmount)*parseFloat(total))/parseFloat(subTotal);
            }

            if (totalDiscountType == 0)
            {
                var totalDiscountCal     = ((parseFloat(total) + parseFloat(vatTypeCal))*parseFloat(totalDiscount))/100;
            }
            else
            {
                var totalDiscountCal     = (parseFloat(total)*parseFloat(totalDiscount))/(parseFloat(subTotal) + parseFloat(vatAmount));
            }

            if (taxType == 0)
            {
                var taxTypeCal     = (parseFloat(total)*parseFloat(taxAmount))/100;
            }
            else
            {
                var taxTypeCal     = $("#tax_amount_0").val();
            }
            var total_result       = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountCal);

            $('#totalReturnedBdt').val(total_result.toFixed());
            $('#totalReturnedBdtShow').val(total_result.toFixed());
            $('#totalReturnedBdtReturn').val(0);
        }

        function pad (str, max) 
        {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        function amoutPaid()
        {
            var getAmount = $("#totalReturnedBdtReturn").val();

            if (getAmount == '')
            {
                var getAmount = 0;
            }
            else
            {
                var getAmount = $("#totalReturnedBdtReturn").val();
            }

            $("#amount_paid").val(parseFloat(getAmount));
        }

        $(document).on("change", "#customer_id" , function() 
        {
            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            $.get(site_url + '/purchasereturn/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data.balance);

                if (data.type == 1)
                {
                    $("#bbShow").html('Payable');
                    $("#bbBalance").val(data.type);
                    $("#totalReturnedBdtReturn").val(0);
                }
                else
                {
                    $("#bbShow").html('Advance');
                    $("#bbBalance").val(data.type);
                }
            });
        });

        function listLoad(data)
        {
            var site_url                = $('.site_url').val();
            var purchase_return_list    = '';
            $.each(data, function(i, purchase_return_data)
            {   
                var serial      = parseFloat(i) + 1;
                var delete_url  = site_url + '/purchasereturn/delete/' + purchase_return_data.id;
                var print_url   = site_url + '/purchasereturn/show/' + purchase_return_data.id;

                purchase_return_list += '<tr>' +
                                    '<input class="form-control purchaseReturnId" type="hidden" id="purchase_return_'+purchase_return_data.id +'" value="' +  purchase_return_data.id + '">' +
                                    '<td>' +
                                        serial +
                                    '</td>' +
                                    '<td>' +
                                       formatDate(purchase_return_data.purchase_return_date) +
                                    '</td>' +
                                    '<td>' +
                                       'PR - ' + purchase_return_data.purchase_return_number.padStart(6, '0') +
                                    '</td>' +
                                    '<td>' +
                                       'BILL - ' + purchase_return_data.bill_number.padStart(6, '0') +
                                    '</td>' +
                                    '<td>' +
                                        purchase_return_data.customer_name +
                                    '</td>' +
                                    '<td>' +
                                       (purchase_return_data.return_amount).toFixed() +
                                    '</td>' +
                                    '<td>' +
                                        '<div class="dropdown">' +
                                            '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                            '</a>' +
                                            '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                '<a class="dropdown-item" href="' + delete_url +'" data-toggle="modal" data-target="#myModal" onclick="putId('+purchase_return_data.id+')">' + 'Delete' + '</a>' +
                                                '<a class="dropdown-item" href="' + print_url +'" target="_blank">' + 'Print' + '</a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</td>' +
                                '</tr>';
            });

            $("#purchase_return_list").empty();
            $("#purchase_return_list").append(purchase_return_list);
        }
    </script>

    <script type="text/javascript">
        function putId(x)
        {
            $('#purchase_return_id_put').val(x);
        }

        $('.delete_btn').click(function () {
            var site_url            = $('.site_url').val();
            var id                  = $('#purchase_return_id_put').val();
            window.location.href    = site_url + "/purchasereturn/delete/"+id;
        });
    </script>

    <script type="text/javascript">
        function preventDoubleClick()
        {   
            var d_val = $('.enableOnInput').val();

            $('#pRint').val(d_val);
            $('.enableOnInput').prop('disabled', true);
            $('#FormSubmit').submit();
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/axis/aea/Modules/PurchaseReturn/Resources/views/index.blade.php ENDPATH**/ ?>