<!DOCTYPE html>
<html>

<head>
    <title>SR Statement Print</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">

        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
    
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p><?php echo e($user_info['address']); ?></p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">SR Statement Print</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">DATE FROM</th>
                                    <th style="text-align: center">SR NAME</th>
                                    <th style="text-align: center">ITEM CATEGORY</th>
                                    <th style="text-align: center">ITEM NAME</th>
                                    <th style="text-align: center">ITEM CODE</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center"><?php echo e($from_date); ?> <strong>To</strong> <?php echo e($to_date); ?></td>
                                    <td style="text-align: center">
                                        <?php if($sr_name != null): ?>
                                            <?php echo e($sr_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>

                                    <td style="text-align: center">
                                        <?php if($item_category_name != null): ?>
                                            <?php echo e($item_category_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>

                                    <td style="text-align: center">
                                        <?php if($item_name != null): ?>
                                            <?php echo e($item_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>

                                    <td style="text-align: center">
                                        <?php if($item_code != null): ?>
                                            <?php echo e($item_code['product_code']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">SL</th>
                                    <th style="text-align: center;width: 20%">SR NAME</th>
                                    <th style="text-align: center;width: 10%">DATE</th>
                                    <th style="text-align: center;width: 10%">ISSUE ID</th>
                                    <th style="text-align: center;width: 10%">ITEM CODE</th>
                                    <th style="text-align: center;width: 25%">NAME</th>
                                    <th style="text-align: center;width: 5%">U/M</th>
                                    <th style="text-align: center;width: 10%">Qty</th>
                                    <th style="text-align: center;width: 5%">STATUS</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i                  = 1;
                                    $total_quantity     = 0;
                                ?>
                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td style="text-align: center;vertical-align: middle;" rowspan="<?php echo e($value->count()); ?>"><?php echo e($i); ?></td>
                                    <td style="text-align: left;vertical-align: middle;" rowspan="<?php echo e($value->count()); ?>"><?php echo e($key); ?></td>

                                    <td style="text-align: center;"><?php echo e(date('d-m-Y', strtotime($value[0]['date']))); ?></td>
                                    <td style="text-align: center;"><?php echo e($value[0]['issue_number'] != null ? str_pad($value[0]['issue_number'], 6, "0", STR_PAD_LEFT) : ''); ?></td>
                                    <td style="text-align: center;"><?php echo e(str_pad($value[0]['product_code'], 6, "0", STR_PAD_LEFT)); ?></td>
                                    <td style="text-align: left;"><?php echo e($value[0]['product_name']); ?></td>
                                    <td style="text-align: center;"><?php echo e($value[0]['unit_name']); ?></td>
                                    <td style="text-align: center;"><?php echo e($value[0]['quantity']); ?></td>
                                    <td style="text-align: center;">
                                        <?php if($value[0]['type'] == 1): ?>
                                            I
                                        <?php endif; ?>

                                        <?php if($value[0]['type'] == 2): ?>
                                            S
                                        <?php endif; ?>

                                        <?php if($value[0]['type'] == 3): ?>
                                            R
                                        <?php endif; ?>
                                    </td>
                                </tr>

                                <?php $__currentLoopData = $value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($key1 != 0): ?>
                                <tr>
                                    <td style="text-align: center;"><?php echo e(date('d-m-Y', strtotime($value1['date']))); ?></td>
                                    <td style="text-align: center;"><?php echo e($value1['issue_number'] != null ? str_pad($value1['issue_number'], 6, "0", STR_PAD_LEFT) : ''); ?></td>
                                    <td style="text-align: center;"><?php echo e(str_pad($value1['product_code'], 6, "0", STR_PAD_LEFT)); ?></td>
                                    <td style="text-align: left;"><?php echo e($value1['product_name']); ?></td>
                                    <td style="text-align: center;"><?php echo e($value1['unit_name']); ?></td>
                                    <td style="text-align: center;"><?php echo e($value1['quantity']); ?></td>
                                    <td style="text-align: center;">
                                        <?php if($value1['type'] == 1): ?>
                                            I
                                        <?php endif; ?>

                                        <?php if($value1['type'] == 2): ?>
                                            S
                                        <?php endif; ?>

                                        <?php if($value1['type'] == 3): ?>
                                            R
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 
                                <?php
                                    $total_quantity      = 0;
                                    $i++;
                                ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>

                            <!-- <tfoot class="tfheight">
                                <tr>
                                    <th colspan="8" style="text-align: right;">TOTAL</th>
                                    <th colspan="1" style="text-align: right;"></th>
                            </tfoot> -->
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/axis/aea/Modules/Reports/Resources/views/sr_statement_print.blade.php ENDPATH**/ ?>