@extends('layouts.app')

@section('title', 'Edit Return Issues')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('issues_return_update', $find_return['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

                                <div class="row">

                                    <div style="background-color: #D2D2D2;height: 620px;padding-top: 10px;" class="col-md-7">
                                        <div style="display: none" class="inner-repeater mb-4 issueDetails">
                                            <div data-repeater-list="inner-group" class="inner form-group row">
                                                <div id="return_product_list" class="inner col-lg-12 ml-md-auto">

                                                    @foreach($find_ireturn_entries as $key => $value)
                                                    <div style="margin-bottom: 0px !important" class="mb-3 row align-items-center">

                                                        <div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-5 col-md-5 col-sm-7 col-12 form-group">
                                                            @if($key == 0)
                                                                <label class="hidden-xs" for="productname">Product</label>
                                                            @endif
                                                            <label style="display: none" class="show-xs" for="productname">Product</label>
                                                            <input type="text" class="inner form-control" value="{{ $value['product_name'] }}" readonly />
                                                            <input name="product_entries[]" type="hidden" value="{{ $value['product_entry_id'] }}" readonly />
                                                        </div>

                                                        <input id="main_unit_id_{{$key}}" type="hidden" class="inner form-control" value="{{$value->main_unit_id}}" name="main_unit_id[]" />
                                                        <input id="main_stock_{{$key}}" type="hidden" class="inner form-control" value="{{$value->quantity}}" name="main_stock[]" />

                                                        <div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6 form-group">
                                                            @if($key == 0)
                                                                <label class="hidden-xs" for="productname">U/M</label>
                                                            @endif
                                                            <label style="display: none" class="show-xs" for="productname">U/M</label>
                                                            <select style="width: 100%;cursor: pointer" name="converted_unit_id[]" class="inner form-control" id="converted_unit_id_{{$key}}" required>
                                                                <option value="{{ $value->conversion_unit_id }}" selected>
                                                                    {{ $value->convertedUnit->name }}
                                                                </option>
                                                            </select>
                                                        </div>

                                                        <div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-4 col-md-4 col-sm-5 col-6 form-group">
                                                            @if($key == 0)
                                                                <label class="hidden-xs" for="productname">Return Qty</label>
                                                            @endif
                                                            <label style="display: none" class="show-xs" for="productname">Return Qty</label>
                                                            <input id="return_quantity_{{$key}}" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="{{ $value['quantity'] }}" oninput="calculate()" />
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 620px;padding-top: 10px" class="col-md-5">

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Select DSR * </label>
                                                    <select id="sr_id" name="sr_id" class="form-control col-md-12" required>
                                                       <option value="{{ $value['sr_id'] }}">{{ $value['sr_name'] }}</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div style="display: none" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Issue Number * </label>
                                                    <div style="padding-left: 10px" class="row">
                                                        <select style="width: 100%" id="issue_id" name="issue_id" class="form-control col-lg-12 col-md-12 col-sm-12 col-12">
                                                           <option value="{{ $value['issue_id'] }}">{{ $value['issue_number'] }}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="return_date">Return Date *</label>
                                                    <input id="return_date" name="return_date" type="text" value="{{ date('d-m-Y', strtotime($find_return['date'])) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="return_note">Note</label>
                                                    <input id="return_note" name="return_note" type="text" value="{{ $find_return['note'] }}" class="form-control">
                                                </div>
                                            </div>

                                            <div style="display: none;margin-top: 20px" class="button-items col-lg-12 issueDetails">
                                                <button name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                                <!-- <button name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Update & Print</button> -->
                                                <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('all_return_issues') }}">Close</a></button>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            $('.issueDetails').show();

        });
    </script>
@endsection