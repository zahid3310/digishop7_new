<!DOCTYPE html>
<html>

<head>
    <title>Salary Sheet</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>   
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">

                    <div style="text-align: center" class="ibox-title" style="padding: 14px 15px 0px;">
                        <?php
                            if ($_GET['month'] == 1)
                            {
                                $month  = 'January';
                            }
                            elseif ($_GET['month'] == 1)
                            {
                                $month  = 'February';
                            }
                            elseif ($_GET['month'] == 3)
                            {
                                $month  = 'March';
                            }
                            elseif ($_GET['month'] == 4)
                            {
                                $month  = 'April';
                            }
                            elseif ($_GET['month'] == 5)
                            {
                                $month  = 'May';
                            }
                            elseif ($_GET['month'] == 6)
                            {
                                $month  = 'Jun';
                            }
                            elseif ($_GET['month'] == 7)
                            {
                                $month  = 'July';
                            }
                            elseif ($_GET['month'] == 8)
                            {
                                $month  = 'August';
                            }
                            elseif ($_GET['month'] == 9)
                            {
                                $month  = 'September';
                            }
                            elseif ($_GET['month'] == 10)
                            {
                                $month  = 'October';
                            }
                            elseif ($_GET['month'] == 11)
                            {
                                $month  = 'November';
                            }
                            elseif ($_GET['month'] == 12)
                            {
                                $month  = 'December';
                            }
                        ?>
                        <p style="margin-bottom: 0px;font-size: 15px"><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p style="margin-bottom: 0px;font-size: 12px">{{ $user_info['address'] }}</p>
                        <p style="margin-bottom: 0px;font-size: 12px">{{ $month . ', ' . $_GET['year'] }}</p>
                    </div>

                    <div class="ibox-content">

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 15%">NAME</th>
                                    <th style="text-align: center;width: 10%">DESIGNATION</th>
                                    <th style="text-align: center;width: 10%">TOTAL ATTENDANCE</th>
                                    <th style="text-align: center;width: 10%">MONTHLY SALARY</th>
                                    <th style="text-align: center;width: 10%">PAYABLE</th>
                                    <th style="text-align: center;width: 10%">ADVANCE</th>
                                    <th style="text-align: center;width: 10%">NET PAYABLE</th>
                                    <th style="text-align: center;width: 10%">SIGNATURE</th>

                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php 
                                    $i = 1;
                                ?>

                                @foreach($data as $key1 => $value1)

                                <tr>
                                    <td style="text-align: center;" colspan="9"><strong>{{ $key1 }}</strong></td>
                                </tr>

                                <?php 
                                    $total_advance      = 0;
                                    $totalNetPayable    = 0;
                                ?>

                                @foreach($value1 as $key => $value)
                                <tr>
                                    <td style="text-align: center;">{{ $i }}</td>
                                    <td style="text-align: left;">{{ $value['employee_name'] }}</td>
                                    <td style="text-align: left;">{{ $value['designation'] }}</td>
                                    <td style="text-align: right;">{{ $value['total_attendance'] }}</td>
                                    <td style="text-align: right;">{{ $value['gross'] }}</td>
                                    <td style="text-align: right;">{{ $value['payable'] }}</td>
                                    <td style="text-align: right;">{{ $value['advance'] }}</td>
                                    <td style="text-align: right;">{{ $value['net_payable'] }}</td>
                                    <td style="text-align: right;"></td>
                                </tr>
                                <?php
                                    $i++; 
                                    $total_advance     = $total_advance + $value['advance']; 
                                    $totalNetPayable   = $totalNetPayable + $value['net_payable']; 
                                    ?>
                                @endforeach
                                @endforeach

                                <tr>
                                    <td style="text-align: right" colspan="6"><strong>TOTAL</strong></td>
                                    <td style="text-align: right"><strong>{{ isset($total_advance) ? $total_advance : 0  }}</strong></td>
                                    <td style="text-align: right"><strong>{{ isset($totalNetPayable) ? $totalNetPayable : 0 }}</strong></td>
                                    <td style="text-align: right"></td>
                                </tr>
                            </tbody>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>