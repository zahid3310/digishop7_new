<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Bills extends Model
{  
    protected $table = "bills";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\Customers','vendor_id');
    }

}
