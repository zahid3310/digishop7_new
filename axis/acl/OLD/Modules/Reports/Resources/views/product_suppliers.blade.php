@extends('layouts.app')

<?php
    if (isset($_GET['branch_id']))
    {
        $branchId = $_GET['branch_id'];
    }
    else
    {
        $branchId = 0;
    }
?>

@section('title', 'Suppliers Rate')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Suppliers Rate</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Suppliers Rate</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="margin-left: 150px;margin-right: 150px" class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <form method="get" action="{{ route('product_suppliers_print') }}" enctype="multipart/form-data" target="_blank">
	                                <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Select Branch </label>
                                        <div class="col-md-9">
                                            <select style="width: 100%" id="branch_id" name="branch_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" required>
                                               <option {{ $branchId == 1 ? 'selected' : '' }} value="1">NEXT FACE</option>
                                               <option {{ $branchId == 2 ? 'selected' : '' }} value="2">AXIS ELECTRICAL APPLIANCE</option>
                                               <option {{ $branchId == 3 ? 'selected' : '' }} value="3">ZOOM POWER UPS</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Supplier </label>
	                                    <div class="col-md-9">
	                                        <select style="width: 100%" id="supplier_id" name="supplier_id" class="form-control select2">
	                                           <option value="0">All</option>
	                                        </select>
	                                    </div>
	                                </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Product</label>
                                        <div class="col-md-9">
                                            <select style="width: 100%" id="product_id" name="product_id" class="form-control select2">
                                               <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Report Type </label>
                                        <div class="col-md-9">
                                            <select style="width: 100%" id="report_type" name="report_type" class="form-control">
                                               <option value="1">Supplier Wise</option>
                                               <option value="2">Product Wise</option>
                                            </select>
                                        </div>
                                    </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"></label>
	                                    <div class="col-md-9">
	                                        <button style="border-radius: 0px;background-color: #297049;color: white" class="btn" type="submit" target="_blank">
	                                        	Print
	                                    	</button>
	                                    </div>
	                                </div>
                            	</form>
                                
                            </div>

                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#supplier_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 1 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#product_id").select2({
                ajax: { 
                url:  site_url + '/reports/sales-statement/product-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });

        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>
@endsection