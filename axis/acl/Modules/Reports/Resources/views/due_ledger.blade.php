@extends('layouts.app')

<?php
    if (isset($_GET['branch_id']))
    {
        $branchId = $_GET['branch_id'];
    }
    else
    {
        $branchId = 0;
    }
?>

@section('title', 'Due Ledger')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Due Ledger</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Due Ledger</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="margin-left: 150px;margin-right: 150px" class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <form method="get" action="{{ route('due_ledger_print') }}" enctype="multipart/form-data" target="_blank">
                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Select Branch </label>
                                        <div class="col-md-9">
                                            <select style="width: 100%" id="branch_id" name="branch_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" required>
                                                <option {{ $branchId == 2 ? 'selected' : '' }} value="2">AXIS ELECTRICAL APPLIANCE</option>
                                                <option {{ $branchId == 3 ? 'selected' : '' }} value="3">NEXT FACE</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label"> From Date </label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="input-group-append col-md-5">
                                                    <span style="border-radius: 0px" class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                    <input style="border-radius: 0px" type="text" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" value="{{ date('d-m-Y') }}" name="from_date">
                                                </div>

                                                <div class="input-group-append col-md-7">
                                                    <label style="text-align: right" for="productname" class="col-md-2 col-form-label"> To </label>
                                                    <span style="border-radius: 0px" class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                    <input style="border-radius: 0px" type="text" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" value="{{ date('d-m-Y') }}" name="to_date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Customer </label>
                                        <div class="col-md-9">
                                            <select style="width: 100%" id="customer_id" name="customer_id" class="form-control select2">
                                               <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label"></label>
                                        <div class="col-md-9">
                                            <button style="border-radius: 0px;background-color: #297049;color: white" class="btn" type="submit" target="_blank">
                                                Print
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0 || result['contact_type'] == 1 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });
        });

        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>
@endsection