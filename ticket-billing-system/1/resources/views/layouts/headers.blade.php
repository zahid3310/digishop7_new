<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="{{ route('home') }}" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('branch_edit', 1) }}" class="waves-effect">
                        <i class="fas fa-list"></i>
                        <span>Company Info.</span>
                    </a>
                </li>

                <li class="">
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>Admin</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                         <li> <a class="" href="{{ route('chart_of_accounts_index') }}">Chart of Accounts Head</a> </li> 
                        <!-- <li> <a class="" href="{{ route('chart_of_projects_index') }}">Chart of Projects</a> </li> -->
                        <li> <a class="" href="{{ route('chart_of_registers_index') }}">Chart of Registers</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-ticket-alt"></i><span>Ticket Billing</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('sales_of_ticket_create') }}">Sales Of Ticket</a> </li>
                        <li> <a href="{{ route('sales_of_ticket_index') }}">List Of Sales</a> </li>
                    </ul>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'cash_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'cash_receipt_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'bank_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'contra_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'journal_voucher_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                Route::currentRouteName() == 'cash_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'cash_receipt_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'bank_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'contra_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'journal_voucher_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fab fa-adn"></i><span>Accounts</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="" href="{{ route('cash_payment_voucher_index') }}">Cash Payment Voucher</a> </li>
                        <li> <a class="" href="{{ route('cash_receipt_voucher_index') }}">Cash Receipt Voucher</a> </li>
                        <li> <a class="" href="{{ route('bank_payment_voucher_index') }}">Bank Payment Voucher</a> </li>
                        <li> <a class="" href="{{ route('bank_receipt_voucher_index') }}">Bank Receipt Voucher</a> </li>
                        <li> <a class="" href="{{ route('journal_voucher_index') }}">Journal Voucher</a> </li>
                        <li> <a class="" href="{{ route('contra_voucher_index') }}">Contra Voucher</a> </li>
                        <!-- <li> <a class="" href="#">Cheque State Change</a> </li> -->
                        <!-- <li> <a class="" href="#">Bank Reconcilation</a> </li> -->
                        <li class=""> <a class="{{ 
                Route::currentRouteName() == 'cash_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'cash_receipt_voucher_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'bank_payment_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'contra_voucher_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'journal_voucher_edit' ? 'mm-active' : '' }}" href="{{ route('voucher_posting_index') }}">Voucher Posting</a> </li>
                        <li> <a class="" href="{{ route('list_of_voucher') }}">List of Voucher</a> </li>
                    </ul>
                </li>

                <li class="">
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>Account Reports</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="" href="{{ route('accounts_report_current_balance_index') }}" target="_blank">Current Balance</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_accounts_voucher_index') }}">Accounts Voucher</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_cash_book_index') }}">Cash Book</a> </li>
                        <li> <a class="" href="{{ route('accounts_report_bank_book_index') }}">Bank Book</a> </li>
                        <!--<li> <a class="" href="#">Journal Book</a> </li>-->
                        <li> <a class="" href="{{ route('accounts_report_ledger_book_index') }}">Ledger Book</a> </li>
                        <!--<li> <a class="" href="#">Detail Annual Phase Cost</a> </li>-->
                        <!--<li> <a class="" href="#">Daily Transaction</a> </li>-->
                        <!--<li> <a class="" href="#">Transaction of Cheque</a> </li>-->
                        <!--<li> <a class="" href="#">Bank Reconcilation</a> </li>-->
                        <!--<li> <a class="" href="#">Project Wise Summary</a> </li>-->
                        <!--<li> <a class="" href="#">Register Wise Summary</a> </li>-->
                        <!--<li> <a class="" href="#">Accounts Wise Summary</a> </li>-->
                        <!--<li> <a class="" href="#">Transactions of Voucher</a> </li>-->
                        <!--<li> <a class="" href="#">Trial Balance</a> </li>-->
                        <!--<li> <a class="" href="#">Trial Balance With Trns</a> </li>-->
                        <!--<li> <a class="" href="#">Balance Sheet</a> </li>-->
                        <!--<li> <a class="" href="#">Income Statement</a> </li>-->
                    </ul>
                </li>


                <li style="display: none;">
                    <a class="has-arrow waves-effect">
                        <i class="far fa-envelope"></i><span>Messaging</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('messages_send_index') }}">Send Message</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>Reports</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li class="">
                            

                            <a href="{{ route('due_report_customer_index') }}">Customer Ledger</a>
                            <a href="{{ route('due_report_supplier_index') }}">Agent/Airlines Ledger</a>
                            <a href="{{ route('party_statement_index') }}">Customer Statement</a>
                            <a href="{{ route('airlines_statement_index') }}">Agent/Airlines Statement</a>

                            <ul class="sub-menu" aria-expanded="false" style="display:none;">
                                <li> <a href="{{ route('sales_statement_index') }}" style="display: none;">Statement of Sales</a> </li>
                                <li> <a href="{{ route('sales_summary_index') }}">Party Ledger</a> </li>
                                <li> <a href="{{ route('product_wise_sales_report_index') }}" style="display: none;">Product Wise Sales</a> </li>
                            </ul>
                        </li>

                        @if(Auth()->user()->branch_id == 1)
                        <li class="" style="display: none;">
                            <a class="has-arrow waves-effect">
                                Receive/Purchase
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('purchase_statement_index') }}">Statement of Pur.</a> </li>
                                <li> <a href="{{ route('purchase_summary_index') }}">Receive/Purchase Summary</a> </li>
                            </ul>
                        </li>
                        @endif

                        <li class="" style="display: none;">
                            <a class="has-arrow waves-effect">
                                Accounts
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('expense_report_index') }}">List of Expense</a> </li>
                                <li> <a href="{{ route('income_report_index') }}">List of Income</a> </li>
                                <!-- <li> <a href="{{ route('general_ledger_index') }}">Ledger Book</a> </li> -->
                                <li> <a href="{{ route('income_expense_ledger_index') }}">Cash Book</a> </li>
                                <li> <a href="{{ route('income_statement_index') }}">Bank Book</a> </li>
                                <!-- <li> <a href="{{ route('daily_collection_report_index') }}">Daily Report</a> </li> -->
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="{{ 
                Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'users_index' ? 'mm-active' : '' || Route::currentRouteName() == 'users_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'users_index' ? 'mm-active' : '' || Route::currentRouteName() == 'users_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>Basic Settings</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li style="display: none;">
                            <a class="has-arrow waves-effect">
                                Messaging
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('messages_index') }}">Create Text Message</a> </li>
                                <li> <a href="{{ route('messages_phone_book_index') }}">Phone Book</a> </li>
                            </ul>
                        </li>
                        
                        <li>
                            <a class="has-arrow waves-effect">
                                Updte Tax & AIT
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('update_tax_ait') }}">Tax & AIT</a> </li>
                            </ul>
                        </li>

                        @if(Auth()->user()->branch_id == 1)
                        <li>
                            <a class="has-arrow waves-effect">
                                Security System
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li style="display: none;"> <a href="{{ route('branch_index') }}">Add Branch</a> </li>
                                <li> <a href="{{ route('users_index') }}">Add User</a> </li>
                                <li> <a href="{{ route('users_index_all') }}">List of User</a> </li>
                                <li> <a href="{{ route('set_access_index') }}">Set Permission</a> </li>
                            </ul>
                        </li>
                         @endif
                        
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>