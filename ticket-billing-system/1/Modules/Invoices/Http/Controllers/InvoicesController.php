<?php

namespace Modules\Invoices\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\UnitConversions;
use App\Models\Users;
use App\Models\BranchInventories;
use App\Models\Expenses;
use App\Models\Accounts;
use App\Models\JournalEntries;

use App\Models\AccountHead;
use App\Models\Projects;
use App\Models\Clients;
use App\Models\VoucherSummary;
use App\Models\AccountTransaction;
use App\Models\BankAccounts;
use App\Models\ChequeTransactions;
use App\Models\Registers;

use Response;
use DB;
use View;
use Carbon\Carbon;

class InvoicesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End



        $branch_id          = Auth()->user()->branch_id;
        $customer_id        = 366;
        $sales              = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'SALES')
                                    ->where('HeadID', 1189)
                                    ->sum('CreditAmount');

        $sales_return       = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'SR')
                                    ->where('HeadID', 1323)
                                    ->sum('DebitAmount');

        $purchase           = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'PURCHASE')
                                    ->where('HeadID', 1322)
                                    ->sum('DebitAmount');

        $purchase_return    = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->where('VoucherType', 'PR')
                                    ->where('HeadID', 1324)
                                    ->sum('CreditAmount');

        $ac                 = AccountTransaction::where('IsPosted', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('RegisterID', $customer_id)
                                    ->whereIn('PurposeAccHeadName', ['bank_ac', 'cash_ac'])
                                    ->get();

        $bal                = $ac->sum('DebitAmount') - $ac->sum('CreditAmount');
        $balance            = $sales - $sales_return - $purchase - $purchase_return - $bal;


        // dd($sales_return, $bal, $balance);





        $branch_id          = Auth()->user()->branch_id;
        $invoices           = Invoices::orderBy('id', 'DESC')->first();
        $paid_accounts      = AccountHead::where('IsTransactable', 1)
                                        ->where('ActiveStatus', 1)
                                        ->where('CompanyID', $branch_id)
                                        ->whereIn('PtnID', ['227', '228'])
                                        ->orderBy('PtnGroupCode', 'ASC')
                                        ->get();

        return view('invoices::index', compact('paid_accounts', 'invoices'));
    }

    public function AllSales()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
 
        $paid_accounts      = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();

        return view('invoices::all_sales', compact('paid_accounts'));
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('invoices::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'          => 'required',
            'customer_id'           => 'required',
            'product_entries.*'     => 'required',
            'amount.*'              => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];
             
            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {   
                    if($data['main_unit_id'][$key] == $data['unit_id'][$key])
                    {
                        $product    = ProductEntries::find($value);
                        $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                    }
                    else
                    {
                        $product1   = UnitConversions::where('product_entry_id', $value)
                                                        ->where('main_unit_id', $data['main_unit_id'][$key])
                                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                                        ->first();
                       
                        $buy_price  = $buy_price + ($product1['purchase_price']*$data['quantity'][$key]);
                    }
                }

            $data_find                          = Invoices::orderBy('created_at', 'DESC')->first();
            $invoice_number                     = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

            $invoice                            = new Invoices;
            $invoice->invoice_number            = $invoice_number;
            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->customer_name             = $data['customer_name'];
            $invoice->customer_phone            = $data['customer_phone'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->due_date                  = date('Y-m-d', strtotime($data['due_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $data['total_amount'];
            $invoice->total_buy_price           = $buy_price;
            $invoice->total_discount            = $discount;
            $invoice->invoice_note              = $data['invoice_note'];
            $invoice->total_vat                 = $vat;
            $invoice->branch_id                 = $branch_id;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->previous_due              = $data['previous_due'];
            $invoice->previous_due_type         = $data['balance_type'];
            $invoice->adjusted_amount           = $data['adjustment'];
            $invoice->major_category_id         = $data['major_category_id'] != 0 ? $data['major_category_id'] : null;
            $invoice->account_id                = $data['account_id'];
            $invoice->vichele_id                = $data['vichele_id'];
            $invoice->driver_name               = $data['driver_name'];
            $invoice->driver_phone              = $data['driver_phone'];
            $invoice->rent                      = $data['rent'];
            $invoice->unload_place_name         = $data['unload_place_name'];
            $invoice->created_by                = $user_id;

            if ($invoice->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'        => $invoice['id'],
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value,
                        'main_unit_id'      => $data['main_unit_id'][$key],
                        'conversion_unit_id'=> $data['unit_id'][$key],
                        'customer_id'       => $invoice['customer_id'],
                        'reference_id'      => $data['reference_id'],
                        'buy_price'         => $product_buy_price['buy_price'],
                        'rate'              => $data['rate'][$key],
                        'quantity'          => $data['quantity'][$key],
                        'total_amount'      => $data['amount'][$key],
                        'discount_type'     => $data['discount_type'][$key],
                        'discount_amount'   => $data['discount'][$key],
                        'branch_id'         => $branch_id,
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('invoice_entries')->insert($invoice_entries);

                stockOut($data, $item_id=null);

                //Financial Accounting Start
                    accountTransactionsDebitStoreInventory($VoucherId = null, 
                            $VoucherDate        = $data['selling_date'], 
                            $VoucherNumber      = null, 
                            $ProjectID          = 0, 
                            $RegisterID         = $data['customer_id'], 
                            $HeadID             = 1247, 
                            $PurposeAccHeadName = null, 
                            $Particulars        = $data['invoice_note'], 
                            $DebitAmount        = $data['total_amount'], 
                            $CreditAmount       = '0.00', 
                            $AccountNumber      = null, 
                            $VoucherType        = 'SALES', 
                            $CBAccount          = 1189, 
                            $IsPosted           = 1, 
                            $CompanyID          = $branch_id, 
                            $Status             = 1, 
                            $created_by         = $user_id,
                            $invoice_id         = $invoice->id,
                            $bill_id            = null,
                            $sales_return_id    = null,
                            $purchase_return_id = null
                        );

                    accountTransactionsCreditStoreInventory($VoucherId = null, 
                            $VoucherDate        = $data['selling_date'], 
                            $VoucherNumber      = null, 
                            $ProjectID          = 0, 
                            $RegisterID         = $data['customer_id'], 
                            $HeadID             = 1189, 
                            $PurposeAccHeadName = null, 
                            $Particulars        = $data['invoice_note'], 
                            $DebitAmount        = '0.00', 
                            $CreditAmount       = $data['total_amount'], 
                            $AccountNumber      = null, 
                            $VoucherType        = 'SALES', 
                            $CBAccount          = 1247, 
                            $IsPosted           = 1, 
                            $CompanyID          = $branch_id, 
                            $Status             = 1, 
                            $created_by         = $user_id,
                            $invoice_id         = $invoice->id,
                            $bill_id            = null,
                            $sales_return_id    = null,
                            $purchase_return_id = null
                        );

                    //Insert into journal_entries 
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {
                                $accountHeadType = AccountHead::find($data['current_balance_paid_through'][$i]);
                                if($accountHeadType['PurposeHeadName'] == 'cash_ac')
                                {
                                    $type           = 'CR';
                                    $voucherNumber  = $this->gets_voucherNumber_name($branch_id, $type);
                                    $storeSummary   = voucherSummaryCreate($VoucherNumber = $voucherNumber, 
                                            $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                            $ProjectID            = 0, 
                                            $RegisterID           = 0, 
                                            $Type                 = $type, 
                                            $BankAccountNumber    = 0, 
                                            $Status               = 1, 
                                            $TotalAmount          = $data['current_balance_amount_paid'][$i], 
                                            $Narration            = $data['current_balance_note'][$i], 
                                            $MoneyReceiptNo       = null, 
                                            $CompanyID            = $branch_id, 
                                            $created_by           = $user_id
                                        );
                    
                                    if ($storeSummary > 0)
                                    {   
                                        //For Credit
                                        accountTransactionsCreditStoreInventory($VoucherId = $storeSummary, 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['customer_id'], 
                                                $HeadID               = 1247, 
                                                $PurposeAccHeadName   = 'others', 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = '0.00', 
                                                $CreditAmount         = $data['current_balance_amount_paid'][$i], 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = $data['current_balance_paid_through'][$i], 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $created_by           = $user_id,
                                                $invoice_id           = $invoice->id,
                                                $bill_id              = null,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );

                                        //For Debit
                                        accountTransactionsDebitStoreInventory($VoucherId = $storeSummary, 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['customer_id'], 
                                                $HeadID               = $data['current_balance_paid_through'][$i], 
                                                $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = $data['current_balance_amount_paid'][$i], 
                                                $CreditAmount         = '0.00', 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = 1247, 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $created_by           = $user_id,
                                                $invoice_id           = $invoice->id,
                                                $bill_id              = null,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );
                                    }
                                }

                                if($accountHeadType['PurposeHeadName'] == 'bank_ac')
                                {
                                    $type           = 'BR';
                                    $voucherNumber  = $this->gets_voucherNumber_name($branch_id, $type);
                                    $storeSummary   = voucherSummaryCreate($VoucherNumber = $voucherNumber, 
                                            $VoucherDate          = $data['selling_date'], 
                                            $ProjectID            = 0, 
                                            $RegisterID           = 0, 
                                            $Type                 = $type, 
                                            $BankAccountNumber    = $data['current_balance_paid_through'][$i], 
                                            $Status               = 1, 
                                            $TotalAmount          = $data['current_balance_amount_paid'][$i], 
                                            $Narration            = $data['current_balance_note'][$i], 
                                            $MoneyReceiptNo       = null, 
                                            $CompanyID            = $branch_id, 
                                            $created_by           = $user_id
                                        );
                    
                                    if ($storeSummary > 0)
                                    {   
                                        //For Credit
                                        accountTransactionsCreditStoreInventory($VoucherId = $storeSummary, 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['customer_id'], 
                                                $HeadID               = 1247, 
                                                $PurposeAccHeadName   = 'others', 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = '0.00', 
                                                $CreditAmount         = $data['current_balance_amount_paid'][$i], 
                                                $AccountNumber        = $data['current_balance_paid_through'][$i], 
                                                $VoucherType          = $type, 
                                                $CBAccount            = $data['current_balance_paid_through'][$i], 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $created_by           = $user_id,
                                                $invoice_id           = $invoice->id,
                                                $bill_id              = null,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );

                                        //For Debit
                                        accountTransactionsDebitStoreInventory($VoucherId = $storeSummary, 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['customer_id'], 
                                                $HeadID               = $data['current_balance_paid_through'][$i], 
                                                $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = $data['current_balance_amount_paid'][$i], 
                                                $CreditAmount         = '0.00', 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = 1247, 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $created_by           = $user_id,
                                                $invoice_id           = $invoice->id,
                                                $bill_id              = null,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );
                                    }
                                }
                            }
                        }
                    }

                    balanceUpdate($data['customer_id']);
                //Financial Accounting End

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Sales Created Successfully !!")->with('invoice_date', $data['selling_date'])->with('due_date', $data['due_date']);
                }
                else
                {
                    $user_info  = userDetails();

                    if ($user_info['printer_type'] == 0 || $user_info['printer_type'] == 1)
                    {
                        return redirect()->route('invoices_show_pos', $invoice->id);
                    }
                    else
                    {
                        return redirect()->route('invoices_show', $invoice->id);
                    }
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::leftjoin('tbl_clients', 'tbl_clients.id', 'invoices.customer_id')
                                ->leftjoin('customers', 'customers.client_id', 'tbl_clients.id')
                                ->select('invoices.*',
                                         'customers.name as customer_name',
                                         'customers.address as address',
                                         'customers.proprietor_name as proprietor_name',
                                         'customers.phone as phone')
                                ->find($id);

        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                ->where('invoice_entries.invoice_id', $id)
                                ->select('invoice_entries.*',
                                         'product_entries.product_type as product_type',
                                         'product_entries.name as product_entry_name',
                                         'products.name as product_name')
                                ->orderBy('id', 'DESC')
                                ->get();  

        $user_info  = userDetails();

        return View::make('invoices::show')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info);
    }

    public function showPos($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::leftjoin('tbl_clients', 'tbl_clients.id', 'invoices.customer_id')
                                ->leftjoin('customers', 'customers.client_id', 'tbl_clients.id')
                                ->select('invoices.*',
                                         'customers.name as customer_name',
                                         'customers.address as address',
                                         'customers.proprietor_name as proprietor_name',
                                         'customers.phone as phone')
                                ->find($id);

        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                ->where('invoice_entries.invoice_id', $id)
                                ->select('invoice_entries.*',
                                         'product_entries.product_type as product_type',
                                         'product_entries.name as product_entry_name',
                                         'products.name as product_name')
                                ->get();

        $user_info  = userDetails();

        return View::make('invoices::show_pos')->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info);
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $find_invoice           = Invoices::leftjoin('tbl_clients', 'tbl_clients.id', 'invoices.customer_id')
                                            ->select('invoices.*',
                                                 'tbl_clients.id as customer_id',
                                                 'tbl_clients.ClientName as contact_name')
                                            ->find($id);

        //Check if the invoice is created from this branch
        if ($find_invoice['branch_id'] != Auth::user()->branch_id)
        {
            return back()->with('unsuccess', 'This sales has not created from your branch !!');
        }                                
        //Check if the invoice is created from this branch

        $find_customer          = Customers::find($find_invoice['customer_id']);
        $find_invoice_entries   = InvoiceEntries::leftjoin('tbl_clients', 'tbl_clients.id', 'invoice_entries.customer_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                            ->where('invoice_entries.invoice_id', $id)
                                            ->select('invoice_entries.*',
                                                    'tbl_clients.id as customer_id',
                                                    'tbl_clients.ClientName as customer_name',
                                                    'product_entries.id as item_id',
                                                    'product_entries.product_code as product_code',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.name as item_name')
                                            ->get();

        $entries_count          = $find_invoice_entries->count();

        $current_balance        = AccountTransaction::where('IsPosted', 1)
                                            ->where('CompanyID', $find_invoice['branch_id'])
                                            ->where('RegisterID', $find_invoice['customer_id'])
                                            ->where('invoice_id', $id)
                                            ->where('DebitAmount', '>', 0)
                                            ->where('VoucherType', '!=', 'SALES')
                                            ->get();

        $current_balance_count  = $current_balance->count();

        $paid_accounts          = AccountHead::where('IsTransactable', 1)
                                        ->where('ActiveStatus', 1)
                                        ->where('CompanyID', $find_invoice['branch_id'])
                                        ->whereIn('PtnID', ['227', '228'])
                                        ->orderBy('PtnGroupCode', 'ASC')
                                        ->get();

        if ((Auth::user()->branch_id != $find_invoice->branch_id) && (Auth::user()->branch_id != 1))
        {
            return back()->with('unsuccess', 'You are not allowed to edit this invoice');
        }

        //Previous Opening Balance Calculation End

        return view('invoices::edit', compact('find_invoice', 'find_invoice_entries', 'entries_count', 'paid_accounts', 'find_customer', 'current_balance', 'current_balance_count'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'      => 'required',
            'customer_id'       => 'required',
            'product_entries.*' => 'required',
            'amount.*'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat           = $data['vat_amount'];
            $old_invoice   = Invoices::find($id);
            $invoice       = Invoices::find($id);
            $branch_id     = $old_invoice['branch_id'];

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {   
                    if($data['main_unit_id'][$key] == $data['unit_id'][$key])
                    {
                        $product    = ProductEntries::find($value);
                        $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                    }
                    else
                    {
                        $product1   = UnitConversions::where('product_entry_id', $value)
                                                        ->where('main_unit_id', $data['main_unit_id'][$key])
                                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                                        ->first();
                       
                        $buy_price  = $buy_price + ($product1['purchase_price']*$data['quantity'][$key]);
                    }
                }

            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->customer_name             = $data['customer_name'];
            $invoice->customer_phone            = $data['customer_phone'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->due_date                  = date('Y-m-d', strtotime($data['due_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $data['total_amount'];
            $invoice->total_buy_price           = $buy_price;
            $invoice->total_discount            = $discount;
            $invoice->invoice_note              = $data['invoice_note'];
            $invoice->total_vat                 = $vat;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->adjusted_amount           = $data['adjustment'];
            $invoice->major_category_id         = $data['major_category_id'] != 0 ? $data['major_category_id'] : null;
            $invoice->account_id                = $data['account_id'];
            $invoice->vichele_id                = $data['vichele_id'];
            $invoice->driver_name               = $data['driver_name'];
            $invoice->driver_phone              = $data['driver_phone'];
            $invoice->rent                      = $data['rent'];
            $invoice->unload_place_name         = $data['unload_place_name'];
            $invoice->updated_by                = $user_id;

            if ($invoice->save())
            {
                $item_id                = InvoiceEntries::where('invoice_id', $invoice['id'])->get();
                $item_delete            = InvoiceEntries::where('invoice_id', $invoice['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'        => $invoice['id'],
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value,
                        'brand_id'          => $product_buy_price['brand_id'],
                        'main_unit_id'      => $data['main_unit_id'][$key],
                        'conversion_unit_id'=> $data['unit_id'][$key],
                        'customer_id'       => $invoice['customer_id'],
                        'reference_id'      => $data['reference_id'],
                        'buy_price'         => $product_buy_price['buy_price'],
                        'rate'              => $data['rate'][$key],
                        'quantity'          => $data['quantity'][$key],
                        'total_amount'      => $data['amount'][$key],
                        'discount_type'     => $data['discount_type'][$key],
                        'discount_amount'   => $data['discount'][$key],
                        'branch_id'         => $old_invoice['branch_id'],
                        'updated_by'        => $user_id,
                    ];
                }

                DB::table('invoice_entries')->insert($invoice_entries);

                stockOut($data, $item_id);

                $jour_ent_debit     = AccountTransaction::where('IsPosted', 1)
                                            ->where('CompanyID', $invoice['branch_id'])
                                            ->where('RegisterID', $invoice['customer_id'])
                                            ->where('invoice_id', $id)
                                            ->where('VoucherType', 'SALES')
                                            ->where('DebitAmount', '>', 0)
                                            ->first();

                $jour_ent_credit    = AccountTransaction::where('IsPosted', 1)
                                            ->where('CompanyID', $invoice['branch_id'])
                                            ->where('RegisterID', $invoice['customer_id'])
                                            ->where('invoice_id', $id)
                                            ->where('VoucherType', 'SALES')
                                            ->where('CreditAmount', '>', 0)
                                            ->first();

                //Financial Accounting Start
                    accountTransactionsDebitUpdateInventory($id = $jour_ent_debit['id'],
                            $VoucherId          = null, 
                            $VoucherDate        = $data['selling_date'], 
                            $VoucherNumber      = null, 
                            $ProjectID          = 0, 
                            $RegisterID         = $data['customer_id'], 
                            $HeadID             = 1247, 
                            $PurposeAccHeadName = null, 
                            $Particulars        = $data['invoice_note'], 
                            $DebitAmount        = $data['total_amount'], 
                            $CreditAmount       = '0.00', 
                            $AccountNumber      = null, 
                            $VoucherType        = 'SALES', 
                            $CBAccount          = 1189, 
                            $IsPosted           = 1, 
                            $CompanyID          = $branch_id, 
                            $Status             = 1, 
                            $updated_by         = $user_id,
                            $invoice_id         = $invoice->id,
                            $bill_id            = null,
                            $sales_return_id    = null,
                            $purchase_return_id = null
                        );

                    accountTransactionsCreditUpdateInventory($id = $jour_ent_credit['id'],
                            $VoucherId          = null, 
                            $VoucherDate        = $data['selling_date'], 
                            $VoucherNumber      = null, 
                            $ProjectID          = 0, 
                            $RegisterID         = $data['customer_id'], 
                            $HeadID             = 1189, 
                            $PurposeAccHeadName = null, 
                            $Particulars        = $data['invoice_note'], 
                            $DebitAmount        = '0.00', 
                            $CreditAmount       = $data['total_amount'], 
                            $AccountNumber      = null, 
                            $VoucherType        = 'SALES', 
                            $CBAccount          = 1247, 
                            $IsPosted           = 1, 
                            $CompanyID          = $branch_id, 
                            $Status             = 1, 
                            $updated_by         = $user_id,
                            $invoice_id         = $invoice->id,
                            $bill_id            = null,
                            $sales_return_id    = null,
                            $purchase_return_id = null
                        );
                //Financial Accounting End

                //Insert into journal_entries Start
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {   
                                $trn_data  = AccountTransaction::find($data['current_balance_id'][$i]);

                                if ($trn_data != null)
                                {
                                    $accountHeadType = AccountHead::find($data['current_balance_paid_through'][$i]);
                                    if($accountHeadType['PurposeHeadName'] == 'cash_ac')
                                    {
                                        $type           = 'CR';
                                        $voucherNumber  = $trn_data['VoucherNumber'];
                                        $updateSummary  = voucherSummaryUpdate($id = $trn_data['VoucherId'],
                                                $VoucherNumber        = $voucherNumber, 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['current_balance_payment_date'][$i])), 
                                                $ProjectID            = 0, 
                                                $RegisterID           = 0, 
                                                $Type                 = $type, 
                                                $BankAccountNumber    = 0, 
                                                $Status               = 1, 
                                                $TotalAmount          = $data['current_balance_amount_paid'][$i], 
                                                $Narration            = $data['current_balance_note'][$i], 
                                                $MoneyReceiptNo       = null, 
                                                $CompanyID            = $branch_id, 
                                                $updated_by           = $user_id
                                            );

                                        accountTransactionsCreditUpdateInventory($id = $trn_data['id'] - 1,
                                                $VoucherId            = $trn_data['VoucherId'], 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['current_balance_payment_date'][$i])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['customer_id'], 
                                                $HeadID               = 1247, 
                                                $PurposeAccHeadName   = 'others', 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = '0.00', 
                                                $CreditAmount         = $data['current_balance_amount_paid'][$i], 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = $data['current_balance_paid_through'][$i], 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $updated_by           = $user_id,
                                                $invoice_id           = $invoice->id,
                                                $bill_id              = null,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );

                                        accountTransactionsDebitUpdateInventory($id = $trn_data['id'],
                                                $VoucherId            = $trn_data['VoucherId'], 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['current_balance_payment_date'][$i])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['customer_id'], 
                                                $HeadID               = $data['current_balance_paid_through'][$i], 
                                                $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = $data['current_balance_amount_paid'][$i], 
                                                $CreditAmount         = '0.00', 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = 1247, 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $updated_by           = $user_id,
                                                $invoice_id           = $invoice->id,
                                                $bill_id              = null,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );
                                    }

                                    if($accountHeadType['PurposeHeadName'] == 'bank_ac')
                                    {
                                        $type           = 'BR';
                                        $voucherNumber  = $trn_data['VoucherNumber'];
                                        $updateSummary  = voucherSummaryUpdate($id = $trn_data['VoucherId'],
                                                $VoucherNumber        = $voucherNumber, 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['current_balance_payment_date'][$i])), 
                                                $ProjectID            = 0, 
                                                $RegisterID           = 0, 
                                                $Type                 = $type, 
                                                $BankAccountNumber    = $data['current_balance_paid_through'][$i], 
                                                $Status               = 1, 
                                                $TotalAmount          = $data['current_balance_amount_paid'][$i], 
                                                $Narration            = $data['current_balance_note'][$i], 
                                                $MoneyReceiptNo       = null, 
                                                $CompanyID            = $branch_id, 
                                                $updated_by           = $user_id
                                            );

                                        accountTransactionsCreditUpdateInventory($id = $trn_data['id'] - 1,
                                                $VoucherId            = $trn_data['VoucherId'], 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['current_balance_payment_date'][$i])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['customer_id'], 
                                                $HeadID               = 1247, 
                                                $PurposeAccHeadName   = 'others', 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = '0.00', 
                                                $CreditAmount         = $data['current_balance_amount_paid'][$i], 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = $data['current_balance_paid_through'][$i], 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $updated_by           = $user_id,
                                                $invoice_id           = $invoice->id,
                                                $bill_id              = null,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );

                                        accountTransactionsDebitUpdateInventory($id = $trn_data['id'],
                                                $VoucherId            = $trn_data['VoucherId'], 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['current_balance_payment_date'][$i])), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['customer_id'], 
                                                $HeadID               = $data['current_balance_paid_through'][$i], 
                                                $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = $data['current_balance_amount_paid'][$i], 
                                                $CreditAmount         = '0.00', 
                                                $AccountNumber        = $data['current_balance_paid_through'][$i], 
                                                $VoucherType          = $type, 
                                                $CBAccount            = 1247, 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $updated_by           = $user_id,
                                                $invoice_id           = $invoice->id,
                                                $bill_id              = null,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );
                                    }
                                }
                                else
                                {
                                    $accountHeadType = AccountHead::find($data['current_balance_paid_through'][$i]);
                                    if($accountHeadType['PurposeHeadName'] == 'cash_ac')
                                    {
                                        $type           = 'CR';
                                        $voucherNumber  = $this->gets_voucherNumber_name($branch_id, $type);
                                        $storeSummary   = voucherSummaryCreate($VoucherNumber = $voucherNumber, 
                                                $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                $ProjectID            = 0, 
                                                $RegisterID           = 0, 
                                                $Type                 = $type, 
                                                $BankAccountNumber    = 0, 
                                                $Status               = 1, 
                                                $TotalAmount          = $data['current_balance_amount_paid'][$i], 
                                                $Narration            = $data['current_balance_note'][$i], 
                                                $MoneyReceiptNo       = null, 
                                                $CompanyID            = $branch_id, 
                                                $created_by           = $user_id
                                            );
                        
                                        if ($storeSummary > 0)
                                        {   
                                            //For Credit
                                            accountTransactionsCreditStoreInventory($VoucherId = $storeSummary, 
                                                    $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                    $VoucherNumber        = $voucherNumber, 
                                                    $ProjectID            = 0, 
                                                    $RegisterID           = $data['customer_id'], 
                                                    $HeadID               = 1247, 
                                                    $PurposeAccHeadName   = 'others', 
                                                    $Particulars          = $data['current_balance_note'][$i], 
                                                    $DebitAmount          = '0.00', 
                                                    $CreditAmount         = $data['current_balance_amount_paid'][$i], 
                                                    $AccountNumber        = null, 
                                                    $VoucherType          = $type, 
                                                    $CBAccount            = $data['current_balance_paid_through'][$i], 
                                                    $IsPosted             = 1, 
                                                    $CompanyID            = $branch_id, 
                                                    $Status               = 1, 
                                                    $created_by           = $user_id,
                                                    $invoice_id           = $invoice->id,
                                                    $bill_id              = null,
                                                    $sales_return_id      = null,
                                                    $purchase_return_id   = null
                                                );

                                            //For Debit
                                            accountTransactionsDebitStoreInventory($VoucherId = $storeSummary, 
                                                    $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                    $VoucherNumber        = $voucherNumber, 
                                                    $ProjectID            = 0, 
                                                    $RegisterID           = $data['customer_id'], 
                                                    $HeadID               = $data['current_balance_paid_through'][$i], 
                                                    $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                                    $Particulars          = $data['current_balance_note'][$i], 
                                                    $DebitAmount          = $data['current_balance_amount_paid'][$i], 
                                                    $CreditAmount         = '0.00', 
                                                    $AccountNumber        = null, 
                                                    $VoucherType          = $type, 
                                                    $CBAccount            = 1247, 
                                                    $IsPosted             = 1, 
                                                    $CompanyID            = $branch_id, 
                                                    $Status               = 1, 
                                                    $created_by           = $user_id,
                                                    $invoice_id           = $invoice->id,
                                                    $bill_id              = null,
                                                    $sales_return_id      = null,
                                                    $purchase_return_id   = null
                                                );
                                        }
                                    }

                                    if($accountHeadType['PurposeHeadName'] == 'bank_ac')
                                    {
                                        $type           = 'BR';
                                        $voucherNumber  = $this->gets_voucherNumber_name($branch_id, $type);
                                        $storeSummary   = voucherSummaryCreate($VoucherNumber = $voucherNumber, 
                                                $VoucherDate          = $data['selling_date'], 
                                                $ProjectID            = 0, 
                                                $RegisterID           = 0, 
                                                $Type                 = $type, 
                                                $BankAccountNumber    = $data['current_balance_paid_through'][$i], 
                                                $Status               = 1, 
                                                $TotalAmount          = $data['current_balance_amount_paid'][$i], 
                                                $Narration            = $data['current_balance_note'][$i], 
                                                $MoneyReceiptNo       = null, 
                                                $CompanyID            = $branch_id, 
                                                $created_by           = $user_id
                                            );
                        
                                        if ($storeSummary > 0)
                                        {   
                                            //For Credit
                                            accountTransactionsCreditStoreInventory($VoucherId = $storeSummary, 
                                                    $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                    $VoucherNumber        = $voucherNumber, 
                                                    $ProjectID            = 0, 
                                                    $RegisterID           = $data['customer_id'], 
                                                    $HeadID               = 1247, 
                                                    $PurposeAccHeadName   = 'others', 
                                                    $Particulars          = $data['current_balance_note'][$i], 
                                                    $DebitAmount          = '0.00', 
                                                    $CreditAmount         = $data['current_balance_amount_paid'][$i], 
                                                    $AccountNumber        = $data['current_balance_paid_through'][$i], 
                                                    $VoucherType          = $type, 
                                                    $CBAccount            = $data['current_balance_paid_through'][$i], 
                                                    $IsPosted             = 1, 
                                                    $CompanyID            = $branch_id, 
                                                    $Status               = 1, 
                                                    $created_by           = $user_id,
                                                    $invoice_id           = $invoice->id,
                                                    $bill_id              = null,
                                                    $sales_return_id      = null,
                                                    $purchase_return_id   = null
                                                );

                                            //For Debit
                                            accountTransactionsDebitStoreInventory($VoucherId = $storeSummary, 
                                                    $VoucherDate          = date('Y-m-d', strtotime($data['selling_date'])), 
                                                    $VoucherNumber        = $voucherNumber, 
                                                    $ProjectID            = 0, 
                                                    $RegisterID           = $data['customer_id'], 
                                                    $HeadID               = $data['current_balance_paid_through'][$i], 
                                                    $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                                    $Particulars          = $data['current_balance_note'][$i], 
                                                    $DebitAmount          = $data['current_balance_amount_paid'][$i], 
                                                    $CreditAmount         = '0.00', 
                                                    $AccountNumber        = null, 
                                                    $VoucherType          = $type, 
                                                    $CBAccount            = 1247, 
                                                    $IsPosted             = 1, 
                                                    $CompanyID            = $branch_id, 
                                                    $Status               = 1, 
                                                    $created_by           = $user_id,
                                                    $invoice_id           = $invoice->id,
                                                    $bill_id              = null,
                                                    $sales_return_id      = null,
                                                    $purchase_return_id   = null
                                                );
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if($data['current_balance_account_id'][$i] > 0)
                                {
                                    $trn_data_del   = AccountTransaction::find($data['current_balance_id'][$i]);

                                    if ($trn_data != null)
                                    {
                                        $accountHeadTypeDel = AccountHead::find($data['current_balance_paid_through'][$i]);
                                        if($accountHeadTypeDel['PurposeHeadName'] == 'cash_ac')
                                        {
                                            $delete_debit       = AccountTransaction::where('id', $data['current_balance_paid_through'][$i])->delete();
                                            $delete_credit     = AccountTransaction::where('id', $data['current_balance_paid_through'][$i] - 1)->delete();
                                        }

                                        if($accountHeadTypeDel['PurposeHeadName'] == 'bank_ac')
                                        {
                                            $delete_credit     = AccountTransaction::where('id', $data['current_balance_paid_through'][$i] - 1)->delete();
                                            $delete_debit       = AccountTransaction::where('id', $data['current_balance_paid_through'][$i])->delete();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    balanceUpdate($data['customer_id']);
                //Insert into journal_entries End

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return redirect()->route('invoices_all_sales')->with("success","Sales Updated Successfully !!");
                }
                else
                {
                    $user_info  = userDetails();

                    if ($user_info['printer_type'] == 0)
                    {
                        return redirect()->route('invoices_show_pos', $invoice->id)->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info);
                    }
                    else
                    {
                        return redirect()->route('invoices_show', $invoice->id)->with("invoice", $invoice)->with("entries", $entries)->with("user_info", $user_info);
                    }
                }
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function productList()
    {
        $data       = ProductEntries::where('product_entries.stock_in_hand', '>', 0)
                                    ->Where('product_entries.stock_in_hand', '!=', null)
                                    ->orderBy('product_entries.total_sold', 'DESC')
                                    ->select('product_entries.*')
                                    ->get();

        return Response::json($data);
    }

    public function makePayment($id)
    {
        $data       = Invoices::find($id);

        return Response::json($data);
    }

    public function storePayment(Request $request)
    {
        $user_id   = Auth()->user()->id;
        $branch_id = Auth()->user()->branch_id;
        $data      = $request->all();

        DB::beginTransaction();

        try{
            if (isset($data['amount_paid']))
            {
                for($i = 0; $i < count($data['amount_paid']); $i++)
                {
                    if ($data['amount_paid'][$i] > 0)
                    {
                        $accountHeadType = AccountHead::find($data['paid_through'][$i]);
                        if($accountHeadType['PurposeHeadName'] == 'cash_ac')
                        {
                            $type           = 'CR';
                            $voucherNumber  = $this->gets_voucherNumber_name($branch_id, $type);
                            $storeSummary   = voucherSummaryCreate($VoucherNumber = $voucherNumber, 
                                    $VoucherDate          = date('Y-m-d'), 
                                    $ProjectID            = 0, 
                                    $RegisterID           = 0, 
                                    $Type                 = $type, 
                                    $BankAccountNumber    = 0, 
                                    $Status               = 1, 
                                    $TotalAmount          = $data['amount_paid'][$i], 
                                    $Narration            = $data['note'][$i], 
                                    $MoneyReceiptNo       = null, 
                                    $CompanyID            = $branch_id, 
                                    $created_by           = $user_id
                                );
            
                            if ($storeSummary > 0)
                            {   
                                //For Credit
                                accountTransactionsCreditStoreInventory($VoucherId = $storeSummary, 
                                        $VoucherDate          = date('Y-m-d'), 
                                        $VoucherNumber        = $voucherNumber, 
                                        $ProjectID            = 0, 
                                        $RegisterID           = $data['customer_id'], 
                                        $HeadID               = 1247, 
                                        $PurposeAccHeadName   = 'others', 
                                        $Particulars          = $data['note'][$i], 
                                        $DebitAmount          = '0.00', 
                                        $CreditAmount         = $data['amount_paid'][$i], 
                                        $AccountNumber        = null, 
                                        $VoucherType          = $type, 
                                        $CBAccount            = $data['paid_through'][$i], 
                                        $IsPosted             = 1, 
                                        $CompanyID            = $branch_id, 
                                        $Status               = 1, 
                                        $created_by           = $user_id,
                                        $invoice_id           = $sales_ticket->id,
                                        $bill_id              = null,
                                        $sales_return_id      = null,
                                        $purchase_return_id   = null
                                    );

                                //For Debit
                                accountTransactionsDebitStoreInventory($VoucherId = $storeSummary, 
                                        $VoucherDate          = date('Y-m-d'), 
                                        $VoucherNumber        = $voucherNumber, 
                                        $ProjectID            = 0, 
                                        $RegisterID           = $data['customer_id'], 
                                        $HeadID               = $data['paid_through'][$i], 
                                        $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                        $Particulars          = $data['note'][$i], 
                                        $DebitAmount          = $data['amount_paid'][$i], 
                                        $CreditAmount         = '0.00', 
                                        $AccountNumber        = null, 
                                        $VoucherType          = $type, 
                                        $CBAccount            = 1247, 
                                        $IsPosted             = 1, 
                                        $CompanyID            = $branch_id, 
                                        $Status               = 1, 
                                        $created_by           = $user_id,
                                        $invoice_id           = $sales_ticket->id,
                                        $bill_id              = null,
                                        $sales_return_id      = null,
                                        $purchase_return_id   = null
                                    );
                            }
                        }

                        if($accountHeadType['PurposeHeadName'] == 'bank_ac')
                        {
                            $type           = 'BR';
                            $voucherNumber  = $this->gets_voucherNumber_name($branch_id, $type);
                            $storeSummary   = voucherSummaryCreate($VoucherNumber = $voucherNumber, 
                                    $VoucherDate          = date('Y-m-d'), 
                                    $ProjectID            = 0, 
                                    $RegisterID           = 0, 
                                    $Type                 = $type, 
                                    $BankAccountNumber    = $data['paid_through'][$i], 
                                    $Status               = 1, 
                                    $TotalAmount          = $data['amount_paid'][$i], 
                                    $Narration            = $data['note'][$i], 
                                    $MoneyReceiptNo       = null, 
                                    $CompanyID            = $branch_id, 
                                    $created_by           = $user_id
                                );
            
                            if ($storeSummary > 0)
                            {   
                                //For Credit
                                accountTransactionsCreditStoreInventory($VoucherId = $storeSummary, 
                                        $VoucherDate          = date('Y-m-d'), 
                                        $VoucherNumber        = $voucherNumber, 
                                        $ProjectID            = 0, 
                                        $RegisterID           = $data['customer_id'], 
                                        $HeadID               = 1247, 
                                        $PurposeAccHeadName   = 'others', 
                                        $Particulars          = $data['note'][$i], 
                                        $DebitAmount          = '0.00', 
                                        $CreditAmount         = $data['amount_paid'][$i], 
                                        $AccountNumber        = $data['paid_through'][$i], 
                                        $VoucherType          = $type, 
                                        $CBAccount            = $data['paid_through'][$i], 
                                        $IsPosted             = 1, 
                                        $CompanyID            = $branch_id, 
                                        $Status               = 1, 
                                        $created_by           = $user_id,
                                        $invoice_id           = $sales_ticket->id,
                                        $bill_id              = null,
                                        $sales_return_id      = null,
                                        $purchase_return_id   = null
                                    );

                                //For Debit
                                accountTransactionsDebitStoreInventory($VoucherId = $storeSummary, 
                                        $VoucherDate          = date('Y-m-d'), 
                                        $VoucherNumber        = $voucherNumber, 
                                        $ProjectID            = 0, 
                                        $RegisterID           = $data['customer_id'], 
                                        $HeadID               = $data['paid_through'][$i], 
                                        $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                        $Particulars          = $data['note'][$i], 
                                        $DebitAmount          = $data['amount_paid'][$i], 
                                        $CreditAmount         = '0.00', 
                                        $AccountNumber        = null, 
                                        $VoucherType          = $type, 
                                        $CBAccount            = 1247, 
                                        $IsPosted             = 1, 
                                        $CompanyID            = $branch_id, 
                                        $Status               = 1, 
                                        $created_by           = $user_id,
                                        $invoice_id           = $sales_ticket->id,
                                        $bill_id              = null,
                                        $sales_return_id      = null,
                                        $purchase_return_id   = null
                                    );
                            }
                        }
                    }
                }
            }

            balanceUpdate($data['customer_id']);
            
            DB::commit();
            return Response::json(1);
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return Response::json(0);
        }
    }

    public function productPriceList($id)
    {
        $branch_id                      = Auth()->user()->branch_id;
        $data['product_entries']        = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                    ->selectRaw('product_entries.*, units.name as unit_name')
                                                    ->find($id);

        $data['branch_product']         = BranchInventories::where('product_entry_id', $id)->where('branch_id', $branch_id)->first();
        $data['branch_id']              = Auth()->user()->branch_id;

        $data1                          = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                        ->where('product_entries.id', $id)
                                                        ->selectRaw('units.id as unit_id, units.name as unit_name')
                                                        ->get()
                                                        ->toArray();

        $data2                          = UnitConversions::leftjoin('units', 'units.id', 'unit_conversions.converted_unit_id')
                                                    ->where('unit_conversions.product_entry_id', $id)
                                                    ->selectRaw('unit_conversions.converted_unit_id as unit_id, units.name as unit_name')
                                                    ->get()
                                                    ->toArray();

        $data['unit_conversions']       = collect(array_merge($data1, $data2));

        return Response::json($data);
    }

    public function invoiceListLoad()
    {
        $branch_id      = Auth()->user()->branch_id;
        $data           = Invoices::leftjoin('sales_return', 'sales_return.invoice_id', 'invoices.id')
                                    ->leftjoin('tbl_clients', 'tbl_clients.id', 'invoices.customer_id')
                                    ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->where('invoices.type', 1)
                                    // ->where('invoices.branch_id', $branch_id)
                                    ->orderBy('invoices.invoice_date', 'DESC')
                                    ->orderBy('invoices.id', 'ASC')
                                    ->select('invoices.*',
                                            'sales_return.id as return_id',
                                            'tbl_clients.ClientName as customer_name',
                                            'tbl_clients.ClientContactNumber as phone')
                                    ->distinct('invoices.id')
                                    ->get();
                                    
        $data   =  $data->where('branch_id', $branch_id);
        
        return Response::json($data);
    }

    public function invoiceListSearch($from_date, $to_date, $customer, $invoice)
    {
        $branch_id              = Auth()->user()->branch_id;
        $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
        $search_by_customer     = $customer != 0 ? $customer : 0;
        $search_by_invoice      = $invoice != 0 ? $invoice : 0;

        $data           = Invoices::leftjoin('sales_return', 'sales_return.invoice_id', 'invoices.id')
                                        ->leftjoin('tbl_clients', 'tbl_clients.id', 'invoices.customer_id')
                                        ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                        ->when($search_by_from_date != 0 && $search_by_to_date != 0, function ($query) use ($search_by_from_date, $search_by_to_date) {
                                            return $query->whereBetween('invoices.invoice_date', [$search_by_from_date, $search_by_to_date]);
                                        })
                                        ->when($search_by_customer != 0, function ($query) use ($search_by_customer) {
                                            return $query->where('tbl_clients.id', $search_by_customer);
                                        })
                                        ->when($search_by_invoice != 0, function ($query) use ($search_by_invoice) {
                                            return $query->where('invoices.id', $search_by_invoice);
                                        })
                                        ->where('invoices.type', 1)
                                        // ->where('invoices.branch_id', $branch_id)
                                        ->orderBy('invoices.invoice_date', 'DESC')
                                        ->select('invoices.*',
                                                 'sales_return.id as return_id',
                                                 'tbl_clients.ClientName as customer_name',
                                                 'tbl_clients.ClientContactNumber as phone')
                                        ->distinct('invoices.id')
                                        // ->take(100)
                                        ->get();
                                        
        $data  =  $data->where('branch_id', $branch_id);
        
        return Response::json($data);
    }

    public function posSearchProduct($id)
    {
        $data           = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_code', $id)
                                    ->where('product_entries.stock_in_hand', '>', 0)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->first();

        return Response::json($data);
    }

    public function customerStore(Request $request)
    {
        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        if($data['contact_type'] == 0)
        {
            $CompanyID          = Auth()->user()->branch_id;
            $PtnID              = 263;

            //getting parent's PtnGroupCode
            $GroupIdQuery       = Registers::where('id', $PtnID)->get();
            $GetGroupId         = isset($GroupIdQuery[0]["GroupID"]) ? $GroupIdQuery[0]["GroupID"] : "";
            $PtnGroupCodeParent = isset($GroupIdQuery[0]["PtnGroupCode"]) ? $GroupIdQuery[0]["PtnGroupCode"] : "";
            $GroupID            = $GetGroupId + 1;

            //getting parent's PtnGroupCode
            $parentChild        = Registers::where('PtnID', $PtnID)->get();
            $parentChildCount   = $parentChild->count();
            $parentChildCount++;
            $PtnGroupCode       = $PtnGroupCodeParent."-".$parentChildCount;

            $register                       = new Registers;
            $register->PtnID                = $PtnID;
            $register->CompanyID            = $CompanyID;
            $register->PtnGroupCode         = $PtnGroupCode;
            $register->ClientName           = $data['customer_name'];
            $register->ClientAddress        = $data['address'];
            $register->ClientContactNumber  = $data['mobile_number'];
            $register->GroupID              = $GroupID;
            $register->OrderID              = $parentChildCount;
            $register->IsTransactable       = 1;
            $register->ActiveStatus         = 1;
            $register->save();
        }

        if($data['contact_type'] == 1)
        {
            $CompanyID          = Auth()->user()->branch_id;
            $PtnID              = 264;

            //getting parent's PtnGroupCode
            $GroupIdQuery       = Registers::where('id', $PtnID)->get();
            $GetGroupId         = isset($GroupIdQuery[0]["GroupID"]) ? $GroupIdQuery[0]["GroupID"] : "";
            $PtnGroupCodeParent = isset($GroupIdQuery[0]["PtnGroupCode"]) ? $GroupIdQuery[0]["PtnGroupCode"] : "";
            $GroupID            = $GetGroupId + 1;

            //getting parent's PtnGroupCode
            $parentChild        = Registers::where('PtnID', $PtnID)->get();
            $parentChildCount   = $parentChild->count();
            $parentChildCount++;
            $PtnGroupCode       = $PtnGroupCodeParent."-".$parentChildCount;

            $register                       = new Registers;
            $register->PtnID                = $PtnID;
            $register->CompanyID            = $CompanyID;
            $register->PtnGroupCode         = $PtnGroupCode;
            $register->ClientName           = $data['customer_name'];
            $register->ClientAddress        = $data['address'];
            $register->ClientContactNumber  = $data['mobile_number'];
            $register->GroupID              = $GroupID;
            $register->OrderID              = $parentChildCount;
            $register->IsTransactable       = 1;
            $register->ActiveStatus         = 1;
            $register->save();
        }
        
        $customers                      = new Customers;
        $customers->client_id           = $register->id;
        $customers->name                = $data['customer_name'];
        $customers->address             = $data['address'];
        $customers->phone               = $data['mobile_number'];
        $customers->contact_type        = $data['contact_type'];
        $customers->branch_id           = $branch_id;
        $customers->created_by          = $user_id;

        if ($customers->save())
        {   
            return Response::json($register);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function customersListInvoice()
    {
        $branch_id      = Auth()->user()->branch_id;

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Clients::where('IsTransactable', 1)
                                ->where('ActiveStatus', 1)
                                ->where('CompanyID', $branch_id)
                                ->where('PtnID', 263)
                                ->orderBy('PtnGroupCode', 'ASC')
                                ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Clients::where('IsTransactable', 1)
                                ->where('ActiveStatus', 1)
                                ->where('CompanyID', $branch_id)
                                ->where('PtnID', 263)
                                ->where('tbl_clients.ClientName', 'LIKE', "%$search%")
                                ->orderBy('PtnGroupCode', 'ASC')
                                ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "phone" =>'', "address" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['ClientName'], "phone" =>$value['ClientContactNumber'], "address" =>$value['ClientAddress']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function couponCode($id)
    {
        $today          = Carbon::today();

        $data           = Discounts::leftjoin('discount_products', 'discount_products.discount_id', 'discounts.id')
                                ->where('discounts.coupon_code', $id)
                                ->orWhere('discounts.card_number', $id)
                                ->whereDate('discounts.expire_date', '>=', $today->format('Y-m-d'))
                                ->where('discounts.status', 1)
                                ->select('discount_products.product_id as product_id', 
                                         'discounts.discount_type as discount_type',
                                         'discounts.discount_amount as discount_amount',
                                         'discounts.expire_date as expire_date'
                                        )
                                ->get();

        return Response::json($data);
    }

    public function printInvoicesList()
    {
        $user_id        = Auth::user()->id;
        $data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->where('invoices.created_by', $user_id)
                                    ->orderBy('invoices.invoice_date', 'DESC')
                                    ->select('invoices.*',
                                            'customers.name as customer_name',
                                            'customers.phone as phone',
                                            'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function printInvoicesSearch($date,$customer,$invoice_number)
    {
        $search_by_date              = $date != 0 ? date('Y-m-d', strtotime($date)) : 0;
        $search_by_invoice_number    = $invoice_number != 0 ? $invoice_number : 0;
        $search_by_invoice_customer  = $customer != 0 ? $customer : 0;
        $user_info                   = userDetails();

        $data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->when($search_by_date != 0, function ($query) use ($search_by_date) {
                                        return $query->where('invoices.invoice_date', 'LIKE', "%$search_by_date%");
                                    })
                                    ->when($search_by_invoice_number != 0, function ($query) use ($search_by_invoice_number) {
                                        return $query->where('invoices.invoice_number', 'LIKE', "%$search_by_invoice_number%");
                                    })
                                    ->when($search_by_invoice_customer != 0, function ($query) use ($search_by_invoice_customer) {
                                        return $query->where('customers.name', 'LIKE', "%$search_by_invoice_customer%");
                                    })
                                    ->orderBy('invoices.invoice_date', 'DESC')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.phone as phone',
                                             'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function calculateOpeningBalance($customer_id)
    {
        $data  = calculateOpeningBalance($customer_id);

        return Response::json($data);
    }
    
    public function customerInfo($customer_id)
    {
        $data  = CLients::leftjoin('customers', 'customers.client_id', 'tbl_clients.id')
                        ->where('tbl_clients.id', $customer_id)
                        ->select('customers.proprietor_name', 'customers.phone', 'customers.address')
                        ->first();

        return Response::json($data);
    }

    public function productListLoadInvoice()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('product_entries.name', 'LIKE', "%$search%")
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->take(100)
                                    ->get();
        }

        foreach ($fetchData as $key => $value)
        {   
            if ($value['product_type'] == 2)
            {
                $variations  = ' - ' . $value['variations'];
            }
            else
            {
                $variations  = '';
            }

            if ($value['brand_name'] != null)
            {
                $brand  = ' - '.$value['brand_name'];
            }
            else
            {
                $brand  = '';
            }

            if ($value['product_code'] != null)
            {
                $code  = $value['product_code'];
            }
            else
            {
                $code  = '';
            }

            $name   = $value['name'] . $variations . ' ' . '( ' . $code . $brand . ' )';

            $data[] = array("id"=>$value['id'], "text"=>$name, "product_id"=>$value['brand_id']);
        }

        return Response::json($data);
    }

    public function expenseCategories()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = ExpenseCategories::whereNotIn('expense_categories.id', [1,2])->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = ExpenseCategories::where('expense_categories.name', 'LIKE', "%$search%")
                                            ->whereNotIn('expense_categories.id', [1,2])
                                            ->get();
        }

        foreach ($fetchData as $key => $value)
        {   
            $data[] = array("id"=>$value['id'], "text"=>$value['name']);
        }

        return Response::json($data);
    }

    public function adjustAdvancePayment($customer_id)
    {
        $data  = Clients::find($customer_id);

        if ($data['balance'] < 0)
        {
            $result = abs($data['balance']);
        }
        else
        {
            $result = 0;
        }

        return Response::json($result);
    }

    //Financial Accounting Start
        public function listOfVouchers()
        {
            $branch_id                = Auth()->user()->branch_id;
            $data['voucherList']      = VoucherSummary::where('CompanyID', $branch_id)->orderBy('id', 'DESC')->get();

            return view('invoices::list_of_voucher', compact('data'));
        }

        public function voucherprint($id)
        {
            $data       = VoucherSummary::find($id);
            $cheques    = ChequeTransactions::where('VoucherNumber', $data['id'])->get();

            return view('invoices::voucher_print', compact('data', 'cheques'));
        }

        ##Cash Payment Voucher Start##
            public function cashPaymentVoucherIndex()
            {
                $branch_id                      = Auth()->user()->branch_id;
                $type                           = 'CP';
                $data['accountsHeadList']       = $this->gets_accountsHead_names($branch_id);
                $data['projectList']            = $this->gets_projects_name($branch_id);
                $data['registerList']           = $this->gets_register_names($branch_id);
                $data['voucherNumber']          = $this->gets_voucherNumber_name($branch_id, $type);
                
                return view('invoices::cashPaymentVoucher.index', compact('data'));
            }

            public function cashPaymentVoucherStore(Request $request)
            {
                $rules = array(
                    'voucher_date'          => 'required',
                    'voucher_number'        => 'nullable',
                    'bill_challan_number'   => 'nullable',
                    'narration'             => 'nullable',
                    'cash_balance'          => 'required',
                );

                $validation = Validator::make(\Request::all(),$rules);

                if ($validation->fails()) {
                    return redirect()->back()->withInput()->withErrors($validation);
                }

                $user_id                = Auth()->user()->id;
                $branch_id              = Auth()->user()->branch_id;
                $data                   = $request->all();
                $cashHeadQuery          = AccountHead::where('CompanyID', $branch_id)
                                                    ->where('PurposeHeadName', 'cash_ac')
                                                    ->orderBy('id', 'ASC')
                                                    ->first();
                $cashHeadID             = isset($cashHeadQuery['id']) ? $cashHeadQuery['id'] : 0;
                $cashHeadName           = isset($cashHeadQuery['HeadName']) ? $cashHeadQuery['HeadName'] : 0;
                $cashPurposeHeadName    = isset($cashHeadQuery['PurposeHeadName']) ? $cashHeadQuery['PurposeHeadName'] : 0;
              
                DB::beginTransaction();

                try{
                    $storeSummary  = voucherSummaryCreate($VoucherNumber = $data['voucher_number'], 
                                          $VoucherDate          = $data['voucher_date'], 
                                          $ProjectID            = 0, 
                                          $RegisterID           = 0, 
                                          $Type                 = 'CP', 
                                          $BankAccountNumber    = 0, 
                                          $Status               = 0, 
                                          $TotalAmount          = $data['sub_total_amount'], 
                                          $Narration            = $data['narration'], 
                                          $MoneyReceiptNo       = $data['bill_challan_number'], 
                                          $CompanyID            = $branch_id, 
                                          $created_by           = $user_id
                                        );
                    
                    if ($storeSummary > 0)
                    {
                        foreach ($data['account_head_name'] as $key => $value)
                        {
                            $account_head = AccountHead::find($value);

                            //For Debit
                            accountTransactionsDebitStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                  $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                  $HeadID               = $value, 
                                  $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = $data['amount'][$key], 
                                  $CreditAmount         = '0.00', 
                                  $AccountNumber        = null, 
                                  $VoucherType          = 'CP', 
                                  $CBAccount            = $cashHeadID, 
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 0, 
                                  $created_by           = $user_id
                                );

                            //For Credit
                            accountTransactionsCreditStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                  $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                  $HeadID               = $cashHeadID, 
                                  $PurposeAccHeadName   = $cashPurposeHeadName, 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = '0.00', 
                                  $CreditAmount         = $data['amount'][$key], 
                                  $AccountNumber        = null, 
                                  $VoucherType          = 'CP', 
                                  $CBAccount            = $value, 
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 1, 
                                  $created_by           = $user_id
                                );
                        }

                        if ($data['register_name'][$key] != null)
                        {
                            balanceUpdate($data['register_name'][$key]);
                        }

                        DB::commit();

                        if ($data['print'] == 1)
                        {
                            return back()->with("success","Cash Payment Voucher Added Successfully.");
                        }

                        if ($data['print'] == 2)
                        {
                            return redirect()->route('voucher_print', $storeSummary);
                        }
                    }
                }catch (\Exception $exception){
                    DB::rollback();
                    dd($exception);
                    return back()->with("unsuccess","Not Added");
                }
            }

            public function cashPaymentVoucherEdit($id)
            {
                $branch_id                      = Auth()->user()->branch_id;
                $data['accountsHeadList']       = $this->gets_accountsHead_names($branch_id);
                $data['projectList']            = $this->gets_projects_name($branch_id);
                $data['registerList']           = $this->gets_register_names($branch_id);
                $data['voucherSummary']         = VoucherSummary::find($id);
                $data['accountTransaction']     = AccountTransaction::where('VoucherNumber', $data['voucherSummary']['VoucherNumber'])->where('VoucherType', 'CP')->where('DebitAmount', '>', 0)->get();

                return view('invoices::voucherPosting.cash_payment_voucher_edit', compact('data'));
            }

            public function cashPaymentVoucherUpdate(Request $request, $id)
            {
                $rules = array(
                    'voucher_date'          => 'required',
                    'voucher_number'        => 'nullable',
                    'bill_challan_number'   => 'nullable',
                    'narration'             => 'nullable',
                    'cash_balance'          => 'required',
                );

                $validation = Validator::make(\Request::all(),$rules);

                if ($validation->fails()) {
                    return redirect()->back()->withInput()->withErrors($validation);
                }

                $user_id                = Auth()->user()->id;
                $branch_id              = Auth()->user()->branch_id;
                $data                   = $request->all();
                $cashHeadQuery          = AccountHead::where('CompanyID', $branch_id)
                                                    ->where('PurposeHeadName', 'cash_ac')
                                                    ->orderBy('id', 'ASC')
                                                    ->first();
                $cashHeadID             = isset($cashHeadQuery['id']) ? $cashHeadQuery['id'] : 0;
                $cashHeadName           = isset($cashHeadQuery['HeadName']) ? $cashHeadQuery['HeadName'] : 0;
                $cashPurposeHeadName    = isset($cashHeadQuery['PurposeHeadName']) ? $cashHeadQuery['PurposeHeadName'] : 0;
              
                DB::beginTransaction();

                try{
                    $storeSummary   = voucherSummaryUpdate($id  = $id,
                                          $VoucherNumber        = $data['voucher_number'], 
                                          $VoucherDate          = $data['voucher_date'], 
                                          $ProjectID            = 0, 
                                          $RegisterID           = 0, 
                                          $Type                 = 'CP', 
                                          $BankAccountNumber    = 0, 
                                          $Status               = 0, 
                                          $TotalAmount          = $data['sub_total_amount'], 
                                          $Narration            = $data['narration'], 
                                          $MoneyReceiptNo       = $data['bill_challan_number'], 
                                          $CompanyID            = $branch_id, 
                                          $updated_by           = $user_id
                                        );
                    
                    if ($storeSummary > 0)
                    {
                        foreach ($data['account_head_name'] as $key => $value)
                        {
                            $account_head = AccountHead::find($value);

                            if ($data['edit_type_check'][$key] == 0)
                            {
                                //For Debit
                            accountTransactionsDebitStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                  $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                  $HeadID               = $value, 
                                  $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = $data['amount'][$key], 
                                  $CreditAmount         = '0.00', 
                                  $AccountNumber        = null, 
                                  $VoucherType          = 'CP', 
                                  $CBAccount            = $cashHeadID, 
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 0, 
                                  $created_by           = $user_id
                                );

                            //For Credit
                            accountTransactionsCreditStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                  $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                  $HeadID               = $cashHeadID, 
                                  $PurposeAccHeadName   = $cashPurposeHeadName, 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = '0.00', 
                                  $CreditAmount         = $data['amount'][$key], 
                                  $AccountNumber        = null, 
                                  $VoucherType          = 'CP', 
                                  $CBAccount            = $value, 
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 1, 
                                  $created_by           = $user_id
                                );
                            }
                            else
                            {
                                //For Debit
                                accountTransactionsDebitUpdate($id = $data['edit_type_check'][$key],
                                      $VoucherId            = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                      $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                      $HeadID               = $value, 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = $data['amount'][$key], 
                                      $CreditAmount         = '0.00', 
                                      $AccountNumber        = null, 
                                      $VoucherType          = 'CP', 
                                      $CBAccount            = $cashHeadID, 
                                      $IsPosted             = null, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = null, 
                                      $updated_by           = $user_id
                                    );

                                //For Credit
                                accountTransactionsCreditUpdate($id = $data['edit_type_check'][$key] + 1,
                                      $VoucherId            = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                      $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                      $HeadID               = $cashHeadID, 
                                      $PurposeAccHeadName   = $cashPurposeHeadName, 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = '0.00', 
                                      $CreditAmount         = $data['amount'][$key], 
                                      $AccountNumber        = null, 
                                      $VoucherType          = 'CP', 
                                      $CBAccount            = $value, 
                                      $IsPosted             = null, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = null, 
                                      $updated_by           = $user_id
                                    );
                            }
                        }

                        if ($data['register_name'][$key] != null)
                        {
                            balanceUpdate($data['register_name'][$key]);
                        }

                        DB::commit();

                        return back()->with("success","Voucher Updated Successfully.");
                    }
                }catch (\Exception $exception){
                    DB::rollback();
                    return back()->with("unsuccess","Not Added");
                }
            }
        ##Cash Payment Voucher End##

        ##Cash Receipt Voucher Start##
            public function cashReceiptVoucherIndex()
            {
                $branch_id                      = Auth()->user()->branch_id;
                $data['accountsHeadList']       = $this->gets_accountsHead_names($branch_id);
                $data['projectList']            = $this->gets_projects_name($branch_id);
                $data['registerList']           = $this->gets_register_names($branch_id);
                $data['voucherNumber']          = $this->gets_voucherNumber_name($branch_id, $type='CR');

                return view('invoices::cashReceiptVoucher.index', compact('data'));
            }

            public function cashReceiptVoucherStore(Request $request)
            {
                $rules = array(
                    'voucher_date'          => 'required',
                    'voucher_number'        => 'nullable',
                    'narration'             => 'nullable',
                    'cash_balance'          => 'required',
                );

                $validation = Validator::make(\Request::all(),$rules);

                if ($validation->fails()) {
                    return redirect()->back()->withInput()->withErrors($validation);
                }

                $user_id                = Auth()->user()->id;
                $branch_id              = Auth()->user()->branch_id;
                $data                   = $request->all();
                $cashHeadQuery          = AccountHead::where('CompanyID', $branch_id)
                                                    ->where('PurposeHeadName', 'cash_ac')
                                                    ->orderBy('id', 'ASC')
                                                    ->first();
                $cashHeadID             = isset($cashHeadQuery['id']) ? $cashHeadQuery['id'] : 0;
                $cashHeadName           = isset($cashHeadQuery['HeadName']) ? $cashHeadQuery['HeadName'] : 0;
                $cashPurposeHeadName    = isset($cashHeadQuery['PurposeHeadName']) ? $cashHeadQuery['PurposeHeadName'] : 0;
              
                DB::beginTransaction();

                try{
                    $storeSummary   = voucherSummaryCreate($VoucherNumber = $data['voucher_number'], 
                                          $VoucherDate          = $data['voucher_date'], 
                                          $ProjectID            = 0, 
                                          $RegisterID           = 0, 
                                          $Type                 = 'CR', 
                                          $BankAccountNumber    = 0, 
                                          $Status               = 0, 
                                          $TotalAmount          = $data['sub_total_amount'], 
                                          $Narration            = $data['narration'], 
                                          $MoneyReceiptNo       = $data['bill_challan_number'], 
                                          $CompanyID            = $branch_id, 
                                          $created_by           = $user_id
                                        );
                    
                    if ($storeSummary > 0)
                    {
                        foreach ($data['account_head_name'] as $key => $value)
                        {
                            $account_head = AccountHead::find($value);

                            //For Credit
                            accountTransactionsCreditStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                  $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                  $HeadID               = $value, 
                                  $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = '0.00', 
                                  $CreditAmount         = $data['amount'][$key], 
                                  $AccountNumber        = null, 
                                  $VoucherType          = 'CR', 
                                  $CBAccount            = $cashHeadID, 
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 0, 
                                  $created_by           = $user_id
                                );

                            //For Debit
                            accountTransactionsDebitStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                  $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                  $HeadID               = $cashHeadID, 
                                  $PurposeAccHeadName   = $cashPurposeHeadName, 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = $data['amount'][$key], 
                                  $CreditAmount         = '0.00', 
                                  $AccountNumber        = null, 
                                  $VoucherType          = 'CR', 
                                  $CBAccount            = $value, 
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 1, 
                                  $created_by           = $user_id
                                );
                        }

                        if ($data['register_name'][$key] != null)
                        {
                            balanceUpdate($data['register_name'][$key]);
                        }

                        DB::commit();

                        if ($data['print'] == 1)
                        {
                            return back()->with("success","Cash Receipt Voucher Added Successfully.");
                        }

                        if ($data['print'] == 2)
                        {
                            return redirect()->route('voucher_print', $storeSummary);
                        }
                    }
                }catch (\Exception $exception){
                    DB::rollback();
                    return back()->with("unsuccess","Not Added");
                }
            }

            public function cashReceiptVoucherEdit($id)
            {
                $branch_id                      = Auth()->user()->branch_id;
                $data['accountsHeadList']       = $this->gets_accountsHead_names($branch_id);
                $data['projectList']            = $this->gets_projects_name($branch_id);
                $data['registerList']           = $this->gets_register_names($branch_id);
                // $data['voucherNumber']          = $this->gets_voucherNumber_name($branch_id, $type='CR');
                $data['voucherSummary']         = VoucherSummary::find($id);
                $data['accountTransaction']     = AccountTransaction::where('VoucherNumber', $data['voucherSummary']['VoucherNumber'])->where('VoucherType', 'CR')->where('CreditAmount', '>', 0)->get();

                return view('invoices::voucherPosting.cash_receipt_voucher_edit', compact('data'));
            }

            public function cashReceiptVoucherUpdate(Request $request, $id)
            {
                $rules = array(
                    'voucher_date'          => 'required',
                    'voucher_number'        => 'nullable',
                    'narration'             => 'nullable',
                    'cash_balance'          => 'required',
                );

                $validation = Validator::make(\Request::all(),$rules);

                if ($validation->fails()) {
                    return redirect()->back()->withInput()->withErrors($validation);
                }

                $user_id                = Auth()->user()->id;
                $branch_id              = Auth()->user()->branch_id;
                $data                   = $request->all();
                $cashHeadQuery          = AccountHead::where('CompanyID', $branch_id)
                                                    ->where('PurposeHeadName', 'cash_ac')
                                                    ->orderBy('id', 'ASC')
                                                    ->first();
                $cashHeadID             = isset($cashHeadQuery['id']) ? $cashHeadQuery['id'] : 0;
                $cashHeadName           = isset($cashHeadQuery['HeadName']) ? $cashHeadQuery['HeadName'] : 0;
                $cashPurposeHeadName    = isset($cashHeadQuery['PurposeHeadName']) ? $cashHeadQuery['PurposeHeadName'] : 0;
              
                DB::beginTransaction();

                try{
                    $storeSummary  = voucherSummaryUpdate($id   = $id,
                                          $VoucherNumber        = $data['voucher_number'], 
                                          $VoucherDate          = $data['voucher_date'], 
                                          $ProjectID            = 0, 
                                          $RegisterID           = 0, 
                                          $Type                 = 'CR', 
                                          $BankAccountNumber    = 0, 
                                          $Status               = 0, 
                                          $TotalAmount          = $data['sub_total_amount'], 
                                          $Narration            = $data['narration'], 
                                          $MoneyReceiptNo       = $data['bill_challan_number'], 
                                          $CompanyID            = $branch_id, 
                                          $created_by           = $user_id
                                        );
                    
                    if ($storeSummary > 0)
                    {
                        foreach ($data['account_head_name'] as $key => $value)
                        {
                            $account_head = AccountHead::find($value);

                            if ($data['edit_type_check'][$key] == 0)
                            {
                                //For Credit
                                accountTransactionsCreditStore($VoucherId = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                      $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                      $HeadID               = $value, 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = '0.00', 
                                      $CreditAmount         = $data['amount'][$key], 
                                      $AccountNumber        = null, 
                                      $VoucherType          = 'CR', 
                                      $CBAccount            = $cashHeadID, 
                                      $IsPosted             = 0, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = 0, 
                                      $created_by           = $user_id
                                    );

                                //For Debit
                                accountTransactionsDebitStore($VoucherId = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                      $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                      $HeadID               = $cashHeadID, 
                                      $PurposeAccHeadName   = $cashPurposeHeadName, 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = $data['amount'][$key], 
                                      $CreditAmount         = '0.00', 
                                      $AccountNumber        = null, 
                                      $VoucherType          = 'CR', 
                                      $CBAccount            = $value, 
                                      $IsPosted             = 0, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = 1, 
                                      $created_by           = $user_id
                                    );
                            }
                            else
                            {   
                                //For Credit
                                accountTransactionsCreditUpdate($id = $data['edit_type_check'][$key],
                                      $VoucherId            = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                      $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                      $HeadID               = $value, 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = '0.00', 
                                      $CreditAmount         = $data['amount'][$key], 
                                      $AccountNumber        = null, 
                                      $VoucherType          = 'CR', 
                                      $CBAccount            = $cashHeadID, 
                                      $IsPosted             = null, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = null, 
                                      $updated_by           = $user_id
                                    );

                                //For Debit
                                accountTransactionsDebitUpdate($id = $data['edit_type_check'][$key] + 1,
                                      $VoucherId            = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                      $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                      $HeadID               = $cashHeadID, 
                                      $PurposeAccHeadName   = $cashPurposeHeadName, 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = $data['amount'][$key], 
                                      $CreditAmount         = '0.00', 
                                      $AccountNumber        = null, 
                                      $VoucherType          = 'CR', 
                                      $CBAccount            = $value, 
                                      $IsPosted             = null, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = null, 
                                      $updated_by           = $user_id
                                    );
                            }
                        }

                        if ($data['register_name'][$key] != null)
                        {
                            balanceUpdate($data['register_name'][$key]);
                        }

                        DB::commit();

                        return back()->with("success","Voucher Updated Successfully.");
                    }
                }catch (\Exception $exception){
                    DB::rollback();
                    return back()->with("unsuccess","Not Added");
                }
            }
        ##Cash Receipt Voucher End##

        ##Bank Payment Voucher Start##
            public function bankPaymentVoucherIndex()
            {
                $branch_id                      = Auth()->user()->branch_id;
                $data['accountsHeadList']       = $this->gets_accountsHead_names($branch_id);
                $data['projectList']            = $this->gets_projects_name($branch_id);
                $data['registerList']           = $this->gets_register_names($branch_id);
                $data['voucherNumber']          = $this->gets_voucherNumber_name($branch_id, $type='BP');
                $data['bankAccounts']           = $this->gets_bank_accounts($branch_id);

                return view('invoices::bankPaymentVoucher.index', compact('data'));
            }

            public function bankPaymentVoucherStore(Request $request)
            {
                $rules = array(
                    'voucher_date'          => 'required',
                    'voucher_number'        => 'nullable',
                    'bill_challan_number'   => 'nullable',
                    'narration'             => 'nullable',
                    'cash_balance'          => 'required',
                );

                $validation = Validator::make(\Request::all(),$rules);

                if ($validation->fails()) {
                    return redirect()->back()->withInput()->withErrors($validation);
                }

                $user_id                = Auth()->user()->id;
                $branch_id              = Auth()->user()->branch_id;
                $data                   = $request->all();
                $bankHeadQuery          = AccountHead::find($data['bank_account_id']);
                $bankHeadID             = isset($bankHeadQuery['id']) ? $bankHeadQuery['id'] : 0;
                $bankHeadName           = isset($bankHeadQuery['HeadName']) ? $bankHeadQuery['HeadName'] : 0;
                $bankPurposeHeadName    = isset($bankHeadQuery['PurposeHeadName']) ? $bankHeadQuery['PurposeHeadName'] : 0;
              
                DB::beginTransaction();

                try{
                    $storeSummary   = voucherSummaryCreate($VoucherNumber = $data['voucher_number'], 
                                          $VoucherDate          = $data['voucher_date'], 
                                          $ProjectID            = 0, 
                                          $RegisterID           = 0, 
                                          $Type                 = 'BP', 
                                          $BankAccountNumber    = $data['bank_account_id'], 
                                          $Status               = 0, 
                                          $TotalAmount          = $data['sub_total_amount'], 
                                          $Narration            = $data['narration'], 
                                          $MoneyReceiptNo       = $data['bill_challan_number'], 
                                          $CompanyID            = $branch_id, 
                                          $created_by           = $user_id
                                        );
                    
                    if ($storeSummary > 0)
                    {
                        foreach ($data['account_head_name'] as $key => $value)
                        {
                            $account_head = AccountHead::find($value);

                            //For Debit
                            accountTransactionsDebitStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                  $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                  $HeadID               = $value, 
                                  $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = $data['amount'][$key], 
                                  $CreditAmount         = '0.00', 
                                  $AccountNumber        = $data['bank_account_id'], 
                                  $VoucherType          = 'BP', 
                                  $CBAccount            = $bankHeadID, 
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 0, 
                                  $created_by           = $user_id
                                );

                            //For Credit
                            accountTransactionsCreditStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                  $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                  $HeadID               = $bankHeadID, 
                                  $PurposeAccHeadName   = $bankPurposeHeadName, 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = '0.00', 
                                  $CreditAmount         = $data['amount'][$key], 
                                  $AccountNumber        = null, 
                                  $VoucherType          = 'BP', 
                                  $CBAccount            = $value, 
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 1, 
                                  $created_by           = $user_id
                                );
                        }

                        if(count($data['check_cheque_number']) > 0)
                        {
                            foreach ($data['check_cheque_number'] as $key1 => $cheque)
                            {   
                                if($cheque != null)
                                {   
                                    chequeStore($VoucherDate    = $data['voucher_date'], 
                                            $VoucherNumber      = $storeSummary, 
                                            $CompanyID          = $branch_id, 
                                            $ProjectID          = $data['check_project_name'][$key1] != null ? $data['check_project_name'][$key1] : 0, 
                                            $RegisterID         = $data['check_register_name'][$key1] != null ? $data['check_register_name'][$key1] : 0, 
                                            $HeadID             = $bankHeadID, 
                                            $ChequeState        = 'ISSUED', 
                                            $Type               = $data['type'][$key1],
                                            $StaffID            = $user_id, 
                                            $ChequeNumber       = $data['check_cheque_number'][$key1], 
                                            $ChequeType         = $data['type'][$key1], 
                                            $ChequeDate         = $data['cq_date'][$key1], 
                                            $EnCashDate         = $data['en_date'][$key1], 
                                            $Amount             = $data['check_amount'][$key1], 
                                            $created_by         = $user_id
                                        );
                                }
                            }
                        }

                        if ($data['register_name'][$key] != null)
                        {
                            balanceUpdate($data['register_name'][$key]);
                        }

                        DB::commit();

                        if ($data['print'] == 1)
                        {
                            return back()->with("success","Bank Payment Voucher Added Successfully.");
                        }

                        if ($data['print'] == 2)
                        {
                            return redirect()->route('voucher_print', $storeSummary);
                        }
                    }
                }catch (\Exception $exception){
                    DB::rollback();
                    return back()->with("unsuccess","Not Added");
                }
            }

            public function bankPaymentVoucherEdit($id)
            {
                $branch_id                      = Auth()->user()->branch_id;
                $data['accountsHeadList']       = $this->gets_accountsHead_names($branch_id);
                $data['projectList']            = $this->gets_projects_name($branch_id);
                $data['registerList']           = $this->gets_register_names($branch_id);
                $data['bankAccounts']           = $this->gets_bank_accounts($branch_id);
                $data['voucherSummary']         = VoucherSummary::find($id);
                $data['accountTransaction']     = AccountTransaction::where('VoucherNumber', $data['voucherSummary']['VoucherNumber'])->where('VoucherType', 'BP')->where('DebitAmount', '>', 0)->get();
                $data['chequeTransaction']      = ChequeTransactions::where('VoucherNumber', $data['voucherSummary']['id'])->get();

                return view('invoices::voucherPosting.bank_payment_voucher_edit', compact('data'));
            }

            public function bankPaymentVoucherUpdate(Request $request, $id)
            {
                $rules = array(
                    'voucher_date'          => 'required',
                    'voucher_number'        => 'nullable',
                    'bill_challan_number'   => 'nullable',
                    'narration'             => 'nullable',
                    'cash_balance'          => 'required',
                );

                $validation = Validator::make(\Request::all(),$rules);

                if ($validation->fails()) {
                    return redirect()->back()->withInput()->withErrors($validation);
                }

                $user_id                = Auth()->user()->id;
                $branch_id              = Auth()->user()->branch_id;
                $data                   = $request->all();
                $bankHeadQuery          = AccountHead::find($data['bank_account_id']);
                $bankHeadID             = isset($bankHeadQuery['id']) ? $bankHeadQuery['id'] : 0;
                $bankHeadName           = isset($bankHeadQuery['HeadName']) ? $bankHeadQuery['HeadName'] : 0;
                $bankPurposeHeadName    = isset($bankHeadQuery['PurposeHeadName']) ? $bankHeadQuery['PurposeHeadName'] : 0;
              
                DB::beginTransaction();

                try{
                    $storeSummary   = voucherSummaryUpdate($id  = $id,
                                          $VoucherNumber        = $data['voucher_number'], 
                                          $VoucherDate          = $data['voucher_date'], 
                                          $ProjectID            = 0, 
                                          $RegisterID           = 0, 
                                          $Type                 = 'BP', 
                                          $BankAccountNumber    = $data['bank_account_id'], 
                                          $Status               = 0, 
                                          $TotalAmount          = $data['sub_total_amount'], 
                                          $Narration            = $data['narration'], 
                                          $MoneyReceiptNo       = $data['bill_challan_number'], 
                                          $CompanyID            = $branch_id, 
                                          $updated_by           = $user_id
                                        );
                
                    if ($storeSummary > 0)
                    {
                        foreach ($data['account_head_name'] as $key => $value)
                        {
                            $account_head = AccountHead::find($value);

                            if ($data['edit_type_check'][$key] == 0)
                            {
                                //For Debit
                                accountTransactionsDebitStore($VoucherId = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                      $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                      $HeadID               = $value, 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = $data['amount'][$key], 
                                      $CreditAmount         = '0.00', 
                                      $AccountNumber        = $data['bank_account_id'], 
                                      $VoucherType          = 'BP', 
                                      $CBAccount            = $bankHeadID, 
                                      $IsPosted             = 0, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = 0, 
                                      $created_by           = $user_id
                                    );

                                //For Credit
                                accountTransactionsCreditStore($VoucherId = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                      $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                      $HeadID               = $bankHeadID, 
                                      $PurposeAccHeadName   = $bankPurposeHeadName, 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = '0.00', 
                                      $CreditAmount         = $data['amount'][$key], 
                                      $AccountNumber        = $data['bank_account_id'], 
                                      $VoucherType          = 'BP', 
                                      $CBAccount            = $value, 
                                      $IsPosted             = 0, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = 1, 
                                      $created_by           = $user_id
                                    );
                            }
                            else
                            {
                                //For Debit
                                accountTransactionsDebitUpdate($id  = $data['edit_type_check'][$key],
                                      $VoucherId            = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                      $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                      $HeadID               = $value, 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = $data['amount'][$key], 
                                      $CreditAmount         = '0.00', 
                                      $AccountNumber        = $data['bank_account_id'], 
                                      $VoucherType          = 'BP', 
                                      $CBAccount            = $bankHeadID, 
                                      $IsPosted             = null, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = null, 
                                      $created_by           = $user_id
                                    );

                                //For Credit
                                accountTransactionsCreditUpdate($id = $data['edit_type_check'][$key] + 1,
                                      $VoucherId            = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                      $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                      $HeadID               = $bankHeadID, 
                                      $PurposeAccHeadName   = $bankPurposeHeadName, 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = '0.00', 
                                      $CreditAmount         = $data['amount'][$key], 
                                      $AccountNumber        = $data['bank_account_id'], 
                                      $VoucherType          = 'BP', 
                                      $CBAccount            = $value, 
                                      $IsPosted             = null, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = null, 
                                      $created_by           = $user_id
                                    );
                            }
                        }

                        if((isset($data['check_cheque_number'])) && (count($data['check_cheque_number']) > 0))
                        {
                            foreach ($data['check_cheque_number'] as $key1 => $cheque)
                            {   
                                if ($data['edit_cheque_check'][$key1] == 0)
                                {   
                                    if ($cheque != null)
                                    {
                                        chequeStore($VoucherDate= $data['voucher_date'], 
                                            $VoucherNumber      = $storeSummary, 
                                            $CompanyID          = $branch_id, 
                                            $ProjectID          = $data['check_project_name'][$key1] != null ? $data['check_project_name'][$key1] : 0, 
                                            $RegisterID         = $data['check_register_name'][$key1] != null ? $data['check_register_name'][$key1] : 0, 
                                            $HeadID             = $bankHeadID, 
                                            $ChequeState        = 'ISSUED', 
                                            $Type               = $data['type'][$key1],
                                            $StaffID            = $user_id, 
                                            $ChequeNumber       = $data['check_cheque_number'][$key1], 
                                            $ChequeType         = $data['type'][$key1], 
                                            $ChequeDate         = $data['cq_date'][$key1], 
                                            $EnCashDate         = $data['en_date'][$key1], 
                                            $Amount             = $data['check_amount'][$key1], 
                                            $created_by         = $user_id
                                        );
                                    }
                                }
                                else
                                {
                                    chequeUpdate($id            = $data['edit_cheque_check'][$key1],
                                            $VoucherDate        = $data['voucher_date'], 
                                            $VoucherNumber      = $storeSummary, 
                                            $CompanyID          = $branch_id, 
                                            $ProjectID          = $data['check_project_name'][$key1] != null ? $data['check_project_name'][$key1] : 0, 
                                            $RegisterID         = $data['check_register_name'][$key1] != null ? $data['check_register_name'][$key1] : 0, 
                                            $HeadID             = $bankHeadID, 
                                            $ChequeState        = 'ISSUED', 
                                            $Type               = $data['type'][$key1],
                                            $StaffID            = $user_id, 
                                            $ChequeNumber       = $data['check_cheque_number'][$key1], 
                                            $ChequeType         = $data['type'][$key1], 
                                            $ChequeDate         = $data['cq_date'][$key1], 
                                            $EnCashDate         = $data['en_date'][$key1], 
                                            $Amount             = $data['check_amount'][$key1], 
                                            $updated_by         = $user_id
                                        );
                                }
                            }
                        }

                        if ($data['register_name'][$key] != null)
                        {
                            balanceUpdate($data['register_name'][$key]);
                        }

                        DB::commit();

                        return back()->with("success","Voucher Updated Successfully.");
                    }
                }catch (\Exception $exception){
                    DB::rollback();
                    dd($exception);
                    return back()->with("unsuccess","Not Added");
                }
            }
        ##Bank Payment Voucher End##

        ##Bank Receipt Voucher Start##
            public function bankReceiptVoucherIndex()
            {
                $branch_id                      = Auth()->user()->branch_id;
                $data['accountsHeadList']       = $this->gets_accountsHead_names($branch_id);
                $data['projectList']            = $this->gets_projects_name($branch_id);
                $data['registerList']           = $this->gets_register_names($branch_id);
                $data['voucherNumber']          = $this->gets_voucherNumber_name($branch_id, $type='BR');
                $data['bankAccounts']           = $this->gets_bank_accounts($branch_id);

                return view('invoices::bankReceiptVoucher.index', compact('data'));
            }

            public function bankReceiptVoucherStore(Request $request)
            {
                $rules = array(
                    'voucher_date'          => 'required',
                    'voucher_number'        => 'nullable',
                    'bill_challan_number'   => 'nullable',
                    'narration'             => 'nullable',
                    'cash_balance'          => 'required',
                );

                $validation = Validator::make(\Request::all(),$rules);

                if ($validation->fails()) {
                    return redirect()->back()->withInput()->withErrors($validation);
                }

                $user_id                = Auth()->user()->id;
                $branch_id              = Auth()->user()->branch_id;
                $data                   = $request->all();
                $bankHeadQuery          = AccountHead::find($data['bank_account_id']);
                $bankHeadID             = isset($bankHeadQuery['id']) ? $bankHeadQuery['id'] : 0;
                $bankHeadName           = isset($bankHeadQuery['HeadName']) ? $bankHeadQuery['HeadName'] : 0;
                $bankPurposeHeadName    = isset($bankHeadQuery['PurposeHeadName']) ? $bankHeadQuery['PurposeHeadName'] : 0;
              
                DB::beginTransaction();

                try{
                    $storeSummary   = voucherSummaryCreate($VoucherNumber = $data['voucher_number'], 
                                          $VoucherDate          = $data['voucher_date'], 
                                          $ProjectID            = 0, 
                                          $RegisterID           = 0, 
                                          $Type                 = 'BR', 
                                          $BankAccountNumber    = $data['bank_account_id'], 
                                          $Status               = 0, 
                                          $TotalAmount          = $data['sub_total_amount'], 
                                          $Narration            = $data['narration'], 
                                          $MoneyReceiptNo       = $data['bill_challan_number'], 
                                          $CompanyID            = $branch_id, 
                                          $created_by           = $user_id
                                        );
                    
                    if ($storeSummary > 0)
                    {
                        foreach ($data['account_head_name'] as $key => $value)
                        {
                            $account_head = AccountHead::find($value);

                            //For Credit
                            accountTransactionsCreditStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                  $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                  $HeadID               = $value, 
                                  $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = '0.00', 
                                  $CreditAmount         = $data['amount'][$key], 
                                  $AccountNumber        = '', 
                                  $VoucherType          = 'BR', 
                                  $CBAccount            = $bankHeadID, 
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 0, 
                                  $created_by           = $user_id
                                );

                            //For Debit
                            accountTransactionsDebitStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                  $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                  $HeadID               = $bankHeadID, 
                                  $PurposeAccHeadName   = $bankPurposeHeadName, 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = $data['amount'][$key], 
                                  $CreditAmount         = '0.00', 
                                  $AccountNumber        = $value, 
                                  $VoucherType          = 'BR', 
                                  $CBAccount            = $value, 
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 1, 
                                  $created_by           = $user_id
                                );
                        }

                        if(count($data['check_cheque_number']) > 0)
                        {
                            foreach ($data['check_cheque_number'] as $key1 => $cheque)
                            {   
                                if($cheque != null)
                                {
                                    chequeStore($VoucherDate    = $data['voucher_date'], 
                                            $VoucherNumber      = $storeSummary, 
                                            $CompanyID          = $branch_id, 
                                            $ProjectID          = $data['check_project_name'][$key1] != null ? $data['check_project_name'][$key1] : 0, 
                                            $RegisterID         = $data['check_register_name'][$key1] != null ? $data['check_register_name'][$key1] : 0, 
                                            $HeadID             = $bankHeadID, 
                                            $ChequeState        = 'ISSUED', 
                                            $Type               = $data['type'][$key1],
                                            $StaffID            = $user_id, 
                                            $ChequeNumber       = $data['check_cheque_number'][$key1], 
                                            $ChequeType         = $data['type'][$key1], 
                                            $ChequeDate         = $data['cq_date'][$key1], 
                                            $EnCashDate         = $data['en_date'][$key1], 
                                            $Amount             = $data['check_amount'][$key1], 
                                            $created_by         = $user_id
                                        );
                                }
                            }
                        }

                        if ($data['register_name'][$key] != null)
                        {
                            balanceUpdate($data['register_name'][$key]);
                        }

                        DB::commit();

                        if ($data['print'] == 1)
                        {
                            return back()->with("success","Bank Receipt Voucher Added Successfully.");
                        }

                        if ($data['print'] == 2)
                        {
                            return redirect()->route('voucher_print', $storeSummary);
                        }
                    }
                }catch (\Exception $exception){
                    DB::rollback();
                    dd($exception);
                    return back()->with("unsuccess","Not Added");
                }
            }

            public function bankReceiptVoucherEdit($id)
            {
                $branch_id                      = Auth()->user()->branch_id;
                $data['accountsHeadList']       = $this->gets_accountsHead_names($branch_id);
                $data['projectList']            = $this->gets_projects_name($branch_id);
                $data['registerList']           = $this->gets_register_names($branch_id);
                $data['bankAccounts']           = $this->gets_bank_accounts($branch_id);
                $data['voucherSummary']         = VoucherSummary::find($id);
                $data['accountTransaction']     = AccountTransaction::where('VoucherNumber', $data['voucherSummary']['VoucherNumber'])->where('VoucherType', 'BR')->where('CreditAmount', '>', 0)->get();
                $data['chequeTransaction']      = ChequeTransactions::where('VoucherNumber', $data['voucherSummary']['id'])->get();

                return view('invoices::voucherPosting.bank_receipt_voucher_edit', compact('data'));
            }

            public function bankReceiptVoucherUpdate(Request $request, $id)
            {
                $rules = array(
                    'voucher_date'          => 'required',
                    'voucher_number'        => 'nullable',
                    'bill_challan_number'   => 'nullable',
                    'narration'             => 'nullable',
                    'cash_balance'          => 'required',
                );

                $validation = Validator::make(\Request::all(),$rules);

                if ($validation->fails()) {
                    return redirect()->back()->withInput()->withErrors($validation);
                }

                $user_id                = Auth()->user()->id;
                $branch_id              = Auth()->user()->branch_id;
                $data                   = $request->all();

                $bankHeadQuery          = AccountHead::find($data['bank_account_id']);
                $bankHeadID             = isset($bankHeadQuery['id']) ? $bankHeadQuery['id'] : 0;
                $bankHeadName           = isset($bankHeadQuery['HeadName']) ? $bankHeadQuery['HeadName'] : 0;
                $bankPurposeHeadName    = isset($bankHeadQuery['PurposeHeadName']) ? $bankHeadQuery['PurposeHeadName'] : 0;
              
                DB::beginTransaction();

                try{
                    $storeSummary   = voucherSummaryUpdate($id  = $id,
                                          $VoucherNumber        = $data['voucher_number'], 
                                          $VoucherDate          = $data['voucher_date'], 
                                          $ProjectID            = 0, 
                                          $RegisterID           = 0, 
                                          $Type                 = 'BR', 
                                          $BankAccountNumber    = $data['bank_account_id'], 
                                          $Status               = 0, 
                                          $TotalAmount          = $data['sub_total_amount'], 
                                          $Narration            = $data['narration'], 
                                          $MoneyReceiptNo       = $data['bill_challan_number'], 
                                          $CompanyID            = $branch_id, 
                                          $updated_by           = $user_id
                                        );
                
                    if ($storeSummary > 0)
                    {
                        foreach ($data['account_head_name'] as $key => $value)
                        {
                            $account_head = AccountHead::find($value);

                            if ($data['edit_type_check'][$key] == 0)
                            {
                                //For Credit
                                accountTransactionsCreditStore($VoucherId = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                      $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                      $HeadID               = $value, 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = '0.00', 
                                      $CreditAmount         = $data['amount'][$key], 
                                      $AccountNumber        = '', 
                                      $VoucherType          = 'BR', 
                                      $CBAccount            = $bankHeadID, 
                                      $IsPosted             = 0, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = 0, 
                                      $created_by           = $user_id
                                    );

                                //For Debit
                                accountTransactionsDebitStore($VoucherId = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                      $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                      $HeadID               = $bankHeadID, 
                                      $PurposeAccHeadName   = $bankPurposeHeadName, 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = $data['amount'][$key], 
                                      $CreditAmount         = '0.00', 
                                      $AccountNumber        = $data['bank_account_id'], 
                                      $VoucherType          = 'BR', 
                                      $CBAccount            = $value, 
                                      $IsPosted             = 0, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = 1, 
                                      $created_by           = $user_id
                                    );
                            }
                            else
                            {
                                //For Credit
                                accountTransactionsCreditUpdate($id = $data['edit_type_check'][$key],
                                      $VoucherId            = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                      $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                      $HeadID               = $value, 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = '0.00', 
                                      $CreditAmount         = $data['amount'][$key], 
                                      $AccountNumber        = '', 
                                      $VoucherType          = 'BR', 
                                      $CBAccount            = $bankHeadID, 
                                      $IsPosted             = null, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = null, 
                                      $created_by           = $user_id
                                    );

                                //For Debit
                                accountTransactionsDebitUpdate($id = $data['edit_type_check'][$key] + 1,
                                      $VoucherId            = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = $data['project_name'][$key] != null ? $data['project_name'][$key] : 0, 
                                      $RegisterID           = $data['register_name'][$key] != null ? $data['register_name'][$key] : 0, 
                                      $HeadID               = $bankHeadID, 
                                      $PurposeAccHeadName   = $bankPurposeHeadName, 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = $data['amount'][$key], 
                                      $CreditAmount         = '0.00', 
                                      $AccountNumber        = $data['bank_account_id'], 
                                      $VoucherType          = 'BR', 
                                      $CBAccount            = $value, 
                                      $IsPosted             = null, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = null, 
                                      $created_by           = $user_id
                                    );
                            }
                        }

                        if(count($data['check_cheque_number']) > 0)
                        {
                            foreach ($data['check_cheque_number'] as $key1 => $cheque)
                            {   
                                if ($data['edit_cheque_check'][$key1] == 0)
                                {
                                    if ($cheque != null)
                                    {
                                        chequeStore($VoucherDate= $data['voucher_date'], 
                                            $VoucherNumber      = $storeSummary, 
                                            $CompanyID          = $branch_id, 
                                            $ProjectID          = $data['check_project_name'][$key1] != null ? $data['check_project_name'][$key1] : 0, 
                                            $RegisterID         = $data['check_register_name'][$key1] != null ? $data['check_register_name'][$key1] : 0, 
                                            $HeadID             = $bankHeadID, 
                                            $ChequeState        = 'ISSUED', 
                                            $Type               = $data['type'][$key1],
                                            $StaffID            = $user_id, 
                                            $ChequeNumber       = $data['check_cheque_number'][$key1], 
                                            $ChequeType         = $data['type'][$key1], 
                                            $ChequeDate         = $data['cq_date'][$key1], 
                                            $EnCashDate         = $data['en_date'][$key1], 
                                            $Amount             = $data['check_amount'][$key1], 
                                            $created_by         = $user_id
                                        );
                                    }
                                }
                                else
                                {   
                                    if ($cheque != null)
                                    {
                                        chequeUpdate($id            = $data['edit_cheque_check'][$key1],
                                                $VoucherDate        = $data['voucher_date'], 
                                                $VoucherNumber      = $storeSummary, 
                                                $CompanyID          = $branch_id, 
                                                $ProjectID          = $data['check_project_name'][$key1] != null ? $data['check_project_name'][$key1] : 0, 
                                                $RegisterID         = $data['check_register_name'][$key1] != null ? $data['check_register_name'][$key1] : 0, 
                                                $HeadID             = $bankHeadID, 
                                                $ChequeState        = 'ISSUED', 
                                                $Type               = $data['type'][$key1],
                                                $StaffID            = $user_id, 
                                                $ChequeNumber       = $data['check_cheque_number'][$key1], 
                                                $ChequeType         = $data['type'][$key1], 
                                                $ChequeDate         = $data['cq_date'][$key1], 
                                                $EnCashDate         = $data['en_date'][$key1], 
                                                $Amount             = $data['check_amount'][$key1], 
                                                $updated_by         = $user_id
                                            );
                                    }
                                }
                            }
                        }

                        if ($data['register_name'][$key] != null)
                        {
                            balanceUpdate($data['register_name'][$key]);
                        }

                        DB::commit();

                        return back()->with("success","Voucher Updated Successfully.");
                    }
                }catch (\Exception $exception){
                    DB::rollback();
                    return back()->with("unsuccess","Not Added");
                }
            }
        ##Bank Receipt Voucher End##

        ##Journal Voucher Start##
            public function journalVoucherIndex()
            {
                $branch_id                      = Auth()->user()->branch_id;
                $data['accountsHeadList']       = $this->gets_accountsHead_names($branch_id);
                $data['projectList']            = $this->gets_projects_name($branch_id);
                $data['registerList']           = $this->gets_register_names($branch_id);
                $data['voucherNumber']          = $this->gets_voucherNumber_name($branch_id, $type='JR');

                return view('invoices::journalVoucher.index', compact('data'));
            }

            public function journalVoucherStore(Request $request)
            {
                $rules = array(
                    'voucher_date'          => 'required',
                    'voucher_number'        => 'nullable',
                    'narration'             => 'nullable',
                );

                $validation = Validator::make(\Request::all(),$rules);

                if ($validation->fails()) {
                    return redirect()->back()->withInput()->withErrors($validation);
                }

                $user_id    = Auth()->user()->id;
                $branch_id  = Auth()->user()->branch_id;
                $data       = $request->all();

                DB::beginTransaction();

                try{
                    $storeSummary   = voucherSummaryCreate($VoucherNumber = $data['voucher_number'], 
                                          $VoucherDate          = $data['voucher_date'], 
                                          $ProjectID            = 0, 
                                          $RegisterID           = 0, 
                                          $Type                 = 'JR', 
                                          $BankAccountNumber    = 0, 
                                          $Status               = 0, 
                                          $TotalAmount          = $data['sub_total_amount_debit'], 
                                          $Narration            = $data['narration'], 
                                          $MoneyReceiptNo       = '', 
                                          $CompanyID            = $branch_id, 
                                          $created_by           = $user_id
                                        );
                    
                    if ($storeSummary > 0)
                    {   
                        foreach ($data['account_head_name'] as $key => $value)
                        {
                            if($data['debit_amount'][$key] > 0)
                            {
                                $debit[] = $value;
                            }

                            if($data['credit_amount'][$key] > 0)
                            {
                                $credit[] = $value;
                            }
                        }

                        $debit_count   = count($debit);
                        $credit_count  = count($credit);
                  
                        $debit_cba_ids = "";
                        for($i=0; $i<$debit_count; $i++){
                            if($i != $debit_count - 1) {
                                $debit_cba_ids .= "".strval($debit[$i]).",";
                            } else {
                                $debit_cba_ids .= "".strval($debit[$i]);
                            }               
                        }

                        $credit_cba_ids = "";
                        for($j=0; $j<$credit_count; $j++){
                            if($j != $credit_count - 1) {
                                $credit_cba_ids .= "".strval($credit[$j]).",";
                            } else {
                                $credit_cba_ids .= "".strval($credit[$j]);
                            }               
                        }

                        foreach ($data['account_head_name'] as $key => $value)
                        {
                            $account_head = AccountHead::find($value);

                            $tbl_acc_trns_data[] = [
                                'VoucherDate'       => date('Y-m-d', strtotime($data['voucher_date'])),
                                'VoucherId'         => $storeSummary, 
                                'VoucherNumber'     => $data['voucher_number'], 
                                'ProjectID'         => $data['project_name'][$key] != null ? $data['project_name'][$key] : 0,
                                'RegisterID'        => $data['register_name'][$key] != null ? $data['register_name'][$key] : 0,
                                'HeadID'            => $value,
                                'PurposeAccHeadName'=> $account_head['PurposeHeadName'],
                                'Particulars'       => $data['particular'][$key],
                                'DebitAmount'       => $data['debit_amount'][$key],
                                'CreditAmount'      => $data['credit_amount'][$key],
                                'CompanyID'         => $branch_id,
                                'VoucherType'       => 'JR',
                                'CBAccount'         => $data['debit_amount'][$key] > 0 ? $credit_cba_ids : $debit_cba_ids,
                                'AccountNumber'     => '',
                                'IsPosted'          => 0,
                                'created_by'        => $user_id,
                                'Status'            => $data['debit_amount'][$key] > 0 ? 0 : 1,
                            ];
                        }

                        DB::table('tbl_acc_accounts_transaction')->insert($tbl_acc_trns_data);

                        if ($data['register_name'][0] != null)
                        {
                            balanceUpdate($data['register_name'][0]);
                        }

                        DB::commit();

                        if ($data['print'] == 1)
                        {
                            return back()->with("success","Journal Voucher Added Successfully.");
                        }

                        if ($data['print'] == 2)
                        {
                            return redirect()->route('voucher_print', $storeSummary);
                        }
                    }
                }catch (\Exception $exception){
                    DB::rollback();
                    return back()->with("unsuccess","Not Added");
                }
            }

            public function journalVoucherEdit($id)
            {
                $branch_id                      = Auth()->user()->branch_id;
                $data['accountsHeadList']       = $this->gets_accountsHead_names($branch_id);
                $data['projectList']            = $this->gets_projects_name($branch_id);
                $data['registerList']           = $this->gets_register_names($branch_id);
                $data['voucherSummary']         = VoucherSummary::find($id);
                $data['accountTransaction']     = AccountTransaction::where('VoucherNumber', $data['voucherSummary']['VoucherNumber'])->get();

                return view('invoices::voucherPosting.journal_voucher_edit', compact('data'));
            }

            public function journalVoucherUpdate(Request $request, $id)
            {
                $rules = array(
                    'voucher_date'          => 'required',
                    'voucher_number'        => 'nullable',
                    'narration'             => 'nullable',
                );

                $validation = Validator::make(\Request::all(),$rules);

                if ($validation->fails()) {
                    return redirect()->back()->withInput()->withErrors($validation);
                }

                $user_id    = Auth()->user()->id;
                $branch_id  = Auth()->user()->branch_id;
                $data       = $request->all();

                DB::beginTransaction();

                try{
                    $storeSummary   = voucherSummaryUpdate($id  = $id,
                                          $VoucherNumber        = $data['voucher_number'], 
                                          $VoucherDate          = $data['voucher_date'], 
                                          $ProjectID            = 0, 
                                          $RegisterID           = 0, 
                                          $Type                 = 'JR', 
                                          $BankAccountNumber    = 0, 
                                          $Status               = 0, 
                                          $TotalAmount          = $data['sub_total_amount_debit'], 
                                          $Narration            = $data['narration'], 
                                          $MoneyReceiptNo       = '', 
                                          $CompanyID            = $branch_id, 
                                          $updated_by           = $user_id
                                        );
                
                    if ($storeSummary > 0)
                    {   
                        foreach ($data['account_head_name'] as $key => $value)
                        {
                            if($data['debit_amount'][$key] > 0)
                            {
                                $debit[] = $value;
                            }

                            if($data['credit_amount'][$key] > 0)
                            {
                                $credit[] = $value;
                            }
                        }

                        $debit_count   = count($debit);
                        $credit_count  = count($credit);
                  
                        $debit_cba_ids = "";
                        for($i=0; $i<$debit_count; $i++){
                            if($i != $debit_count - 1) {
                                $debit_cba_ids .= "".strval($debit[$i]).",";
                            } else {
                                $debit_cba_ids .= "".strval($debit[$i]);
                            }               
                        }

                        $credit_cba_ids = "";
                        for($j=0; $j<$credit_count; $j++){
                            if($j != $credit_count - 1) {
                                $credit_cba_ids .= "".strval($credit[$j]).",";
                            } else {
                                $credit_cba_ids .= "".strval($credit[$j]);
                            }               
                        }
                        
                        foreach ($data['account_head_name'] as $key => $value)
                        {   
                            $account_head = AccountHead::find($value);

                            if ($data['edit_type_check'][$key] == 0)
                            {
                                $tbl_acc_trns_data[] = [
                                    'VoucherDate'       => date('Y-m-d', strtotime($data['voucher_date'])),
                                    'VoucherId'         => $storeSummary, 
                                    'VoucherNumber'     => $data['voucher_number'], 
                                    'ProjectID'         => $data['project_name'][$key] != null ? $data['project_name'][$key] : 0,
                                    'RegisterID'        => $data['register_name'][$key] != null ? $data['register_name'][$key] : 0,
                                    'HeadID'            => $value,
                                    'PurposeAccHeadName'=> $account_head['PurposeHeadName'],
                                    'Particulars'       => $data['particular'][$key],
                                    'DebitAmount'       => $data['debit_amount'][$key],
                                    'CreditAmount'      => $data['credit_amount'][$key],
                                    'CompanyID'         => $branch_id,
                                    'VoucherType'       => 'JR',
                                    'CBAccount'         => $data['debit_amount'][$key] > 0 ? $credit_cba_ids : $debit_cba_ids,
                                    'AccountNumber'     => '',
                                    'IsPosted'          => 0,
                                    'created_by'        => $user_id,
                                    'Status'            => $data['debit_amount'][$key] > 0 ? 0 : 1,
                                ];
                            }
                            else
                            {   
                                $tbl_acc_trns_data_update = [
                                    'VoucherDate'       => date('Y-m-d', strtotime($data['voucher_date'])),
                                    'VoucherId'         => $storeSummary, 
                                    'VoucherNumber'     => $data['voucher_number'], 
                                    'ProjectID'         => $data['project_name'][$key] != null ? $data['project_name'][$key] : 0,
                                    'RegisterID'        => $data['register_name'][$key] != null ? $data['register_name'][$key] : 0,
                                    'HeadID'            => $value,
                                    'PurposeAccHeadName'=> $account_head['PurposeHeadName'],
                                    'Particulars'       => $data['particular'][$key],
                                    'DebitAmount'       => $data['debit_amount'][$key],
                                    'CreditAmount'      => $data['credit_amount'][$key],
                                    'CompanyID'         => $branch_id,
                                    'VoucherType'       => 'JR',
                                    'CBAccount'         => $data['debit_amount'][$key] > 0 ? $credit_cba_ids : $debit_cba_ids,
                                    'AccountNumber'     => '',
                                    'created_by'        => $user_id,
                                ];

                                DB::table('tbl_acc_accounts_transaction')->where('id', $data['edit_type_check'][$key])->update($tbl_acc_trns_data_update);
                            }
                        }

                        if (isset($tbl_acc_trns_data))
                        {
                            DB::table('tbl_acc_accounts_transaction')->insert($tbl_acc_trns_data);
                        }

                        if ($data['register_name'][0] != null)
                        {
                            balanceUpdate($data['register_name'][0]);
                        }

                        DB::commit();

                        return back()->with("success","Voucher Updated Successfully.");
                    }
                }catch (\Exception $exception){
                    DB::rollback();
                    return back()->with("unsuccess","Not Added");
                }
            }
        ##Journal Voucher End##

        ##Contra Voucher Start##
            public function contraVoucherIndex()
            {
                $branch_id                      = Auth()->user()->branch_id;
                $data['accountsHeadList']       = $this->gets_accountsHead_names($branch_id);
                $data['projectList']            = $this->gets_projects_name($branch_id);
                $data['registerList']           = $this->gets_register_names($branch_id);
                $data['voucherNumber']          = $this->gets_voucherNumber_name($branch_id, $type='CN');
                $data['bankAccounts']           = $this->gets_bank_accounts($branch_id);

                return view('invoices::contraVoucher.index', compact('data'));
            }

            public function contraVoucherStore(Request $request)
            {
                $rules = array(
                    'voucher_date'          => 'required',
                    'voucher_number'        => 'nullable',
                    'narration'             => 'nullable',
                    'cash_balance'          => 'required',
                );

                $validation = Validator::make(\Request::all(),$rules);

                if ($validation->fails()) {
                    return redirect()->back()->withInput()->withErrors($validation);
                }

                $user_id    = Auth()->user()->id;
                $branch_id  = Auth()->user()->branch_id;
                $data       = $request->all();

                DB::beginTransaction();

                try{
                    $storeSummary   = voucherSummaryCreate($VoucherNumber = $data['voucher_number'], 
                                          $VoucherDate          = $data['voucher_date'], 
                                          $ProjectID            = 0, 
                                          $RegisterID           = 0, 
                                          $Type                 = 'CN', 
                                          $BankAccountNumber    = 0, 
                                          $Status               = 0, 
                                          $TotalAmount          = $data['sub_total_amount'], 
                                          $Narration            = $data['narration'], 
                                          $MoneyReceiptNo       = '', 
                                          $CompanyID            = $branch_id, 
                                          $created_by           = $user_id
                                        );

                    if ($storeSummary > 0)
                    {   
                        $find_voucher                       = VoucherSummary::find($storeSummary);
                        $find_voucher->contra_type          = $data['contra_type'];
                        $find_voucher->BankAccountNumber    = isset($data['bank_account_id']) ? $data['bank_account_id'] : '';
                        $find_voucher->save();

                        foreach ($data['account_head_name'] as $key => $value)
                        {
                            if ($data['contra_type'] == 1)
                            {
                                $account_head       = AccountHead::find($value);
                                $acc_account_head   = AccountHead::where('PurposeHeadName', 'cash_ac')->first();

                                //For Debit
                                accountTransactionsDebitStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = 0, 
                                  $RegisterID           = 0, 
                                  $HeadID               = $value, 
                                  $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = $data['amount'][$key], 
                                  $CreditAmount         = '0.00', 
                                  $AccountNumber        = null, 
                                  $VoucherType          = 'CN', 
                                  $CBAccount            = $acc_account_head['id'], 
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 0, 
                                  $created_by           = $user_id
                                );

                                //For Credit
                                accountTransactionsCreditStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = 0, 
                                  $RegisterID           = 0, 
                                  $HeadID               = $acc_account_head['id'], 
                                  $PurposeAccHeadName   = $acc_account_head['PurposeHeadName'], 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = '0.00', 
                                  $CreditAmount         = $data['amount'][$key], 
                                  $AccountNumber        = $acc_account_head['id'], 
                                  $VoucherType          = 'CN', 
                                  $CBAccount            = $value, 
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 1, 
                                  $created_by           = $user_id
                                );
                            }

                            if ($data['contra_type'] == 2)
                            {
                                $account_head       = AccountHead::find($data['bank_account_id']);
                                $acc_account_head   = AccountHead::where('PurposeHeadName', 'cash_ac')->first();

                                //For Debit
                                accountTransactionsDebitStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = 0, 
                                  $RegisterID           = 0, 
                                  $HeadID               = $acc_account_head['id'], 
                                  $PurposeAccHeadName   = $acc_account_head['PurposeHeadName'], 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = $data['amount'][$key], 
                                  $CreditAmount         = '0.00', 
                                  $AccountNumber        = null, 
                                  $VoucherType          = 'CN', 
                                  $CBAccount            = $data['bank_account_id'], 
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 0, 
                                  $created_by           = $user_id
                                );

                                //For Credit
                                accountTransactionsCreditStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = 0, 
                                  $RegisterID           = 0, 
                                  $HeadID               = $data['bank_account_id'], 
                                  $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = '0.00', 
                                  $CreditAmount         = $data['amount'][$key], 
                                  $AccountNumber        = $data['bank_account_id'], 
                                  $VoucherType          = 'CN', 
                                  $CBAccount            = $acc_account_head['id'], 
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 1, 
                                  $created_by           = $user_id
                                );
                            }

                            if ($data['contra_type'] == 3)
                            {
                                $account_head       = AccountHead::find($value);

                                //For Debit
                                accountTransactionsDebitStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = 0, 
                                  $RegisterID           = 0, 
                                  $HeadID               = $value, 
                                  $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                  $Particulars          = $data['particular'][$key], 
                                  $DebitAmount          = $data['amount'][$key], 
                                  $CreditAmount         = '0.00',
                                  $AccountNumber        = $data['bank_account_id'], 
                                  $VoucherType          = 'CN', 
                                  $CBAccount            = $data['bank_account_id'],  
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 0, 
                                  $created_by           = $user_id
                                );

                                //For Credit
                                accountTransactionsCreditStore($VoucherId = $storeSummary, 
                                  $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                  $VoucherNumber        = $data['voucher_number'], 
                                  $ProjectID            = 0, 
                                  $RegisterID           = 0, 
                                  $HeadID               = $data['bank_account_id'], 
                                  $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                  $Particulars          = $data['particular'][$key],
                                  $DebitAmount          = '0.00', 
                                  $CreditAmount         = $data['amount'][$key],  
                                  $AccountNumber        = null, 
                                  $VoucherType          = 'CN', 
                                  $CBAccount            = $value,
                                  $IsPosted             = 0, 
                                  $CompanyID            = $branch_id, 
                                  $Status               = 1, 
                                  $created_by           = $user_id
                                );
                            }
                        }

                        if(count($data['check_cheque_number']) > 0)
                        {
                            foreach ($data['check_cheque_number'] as $key1 => $cheque)
                            {   
                                if($cheque != null)
                                {
                                    chequeStore($VoucherDate    = $data['voucher_date'], 
                                            $VoucherNumber      = $storeSummary, 
                                            $CompanyID          = $branch_id, 
                                            $ProjectID          = 0, 
                                            $RegisterID         = 0, 
                                            $HeadID             = $data['check_account_head_name'][$key1], 
                                            $ChequeState        = 'ISSUED', 
                                            $Type               = $data['type'][$key1],
                                            $StaffID            = $user_id, 
                                            $ChequeNumber       = $data['check_cheque_number'][$key1], 
                                            $ChequeType         = $data['type'][$key1], 
                                            $ChequeDate         = $data['cq_date'][$key1], 
                                            $EnCashDate         = $data['en_date'][$key1], 
                                            $Amount             = $data['check_amount'][$key1], 
                                            $created_by         = $user_id
                                        );
                                }
                            }
                        }

                        if (isset($cheque_entry_data))
                        {
                            DB::table('tbl_acc_cheque_transaction')->insert($cheque_entry_data);
                        }

                        DB::commit();

                        if ($data['print'] == 1)
                        {
                            return back()->with("success","Bank Payment Voucher Added Successfully.");
                        }

                        if ($data['print'] == 2)
                        {
                            return redirect()->route('voucher_print', $storeSummary);
                        }
                    }
                }catch (\Exception $exception){
                    DB::rollback();
                    dd($exception);
                    return back()->with("unsuccess","Not Added");
                }
            }

            public function contraVoucherEdit($id)
            {
                $branch_id                      = Auth()->user()->branch_id;
                $data['accountsHeadList']       = $this->gets_accountsHead_names($branch_id);
                $data['projectList']            = $this->gets_projects_name($branch_id);
                $data['registerList']           = $this->gets_register_names($branch_id);
                $data['bankAccounts']           = $this->gets_bank_accounts($branch_id);
                $data['voucherSummary']         = VoucherSummary::find($id);
                $data['accountTransaction']     = AccountTransaction::where('VoucherNumber', $data['voucherSummary']['VoucherNumber'])->where('VoucherType', 'CN')->where('DebitAmount', '>', 0)->get();
                $data['chequeTransaction']      = ChequeTransactions::where('VoucherNumber', $data['voucherSummary']['id'])->get();

                return view('invoices::voucherPosting.contra_voucher_edit', compact('data'));
            }

            public function contraVoucherUpdate(Request $request, $id)
            {
                $rules = array(
                    'voucher_date'          => 'required',
                    'voucher_number'        => 'nullable',
                    'narration'             => 'nullable',
                    'cash_balance'          => 'required',
                );

                $validation = Validator::make(\Request::all(),$rules);

                if ($validation->fails()) {
                    return redirect()->back()->withInput()->withErrors($validation);
                }

                $user_id    = Auth()->user()->id;
                $branch_id  = Auth()->user()->branch_id;
                $data       = $request->all();

                DB::beginTransaction();

                try{
                    $storeSummary   = voucherSummaryUpdate($id  = $id,
                                          $VoucherNumber        = $data['voucher_number'], 
                                          $VoucherDate          = $data['voucher_date'], 
                                          $ProjectID            = 0, 
                                          $RegisterID           = 0, 
                                          $Type                 = 'CN', 
                                          $BankAccountNumber    = 0, 
                                          $Status               = 0, 
                                          $TotalAmount          = $data['sub_total_amount'], 
                                          $Narration            = $data['narration'], 
                                          $MoneyReceiptNo       = '', 
                                          $CompanyID            = $branch_id, 
                                          $updated_by           = $user_id
                                        );

                    if ($storeSummary > 0)
                    {   
                        $find_voucher                       = VoucherSummary::find($storeSummary);
                        $find_voucher->contra_type          = $data['contra_type'];
                        $find_voucher->BankAccountNumber    = isset($data['bank_account_id']) ? $data['bank_account_id'] : '';
                        $find_voucher->save();

                        foreach ($data['account_head_name'] as $key => $value)
                        {
                            if ($data['contra_type'] == 1)
                            {
                                $account_head       = AccountHead::find($value);
                                $acc_account_head   = AccountHead::where('PurposeHeadName', 'cash_ac')->first();

                                if ($data['edit_type_check'][$key] == 0)
                                {
                                    //For Debit
                                    accountTransactionsDebitStore($VoucherId = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = 0, 
                                      $RegisterID           = 0, 
                                      $HeadID               = $value, 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = $data['amount'][$key], 
                                      $CreditAmount         = '0.00', 
                                      $AccountNumber        = null, 
                                      $VoucherType          = 'CN', 
                                      $CBAccount            = $acc_account_head['id'], 
                                      $IsPosted             = 0, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = 0, 
                                      $created_by           = $user_id
                                    );

                                    //For Credit
                                    accountTransactionsCreditStore($VoucherId = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = 0, 
                                      $RegisterID           = 0, 
                                      $HeadID               = $acc_account_head['id'], 
                                      $PurposeAccHeadName   = $acc_account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = '0.00', 
                                      $CreditAmount         = $data['amount'][$key], 
                                      $AccountNumber        = $acc_account_head['id'], 
                                      $VoucherType          = 'CN', 
                                      $CBAccount            = $value, 
                                      $IsPosted             = 0, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = 1, 
                                      $created_by           = $user_id
                                    );
                                }
                                else
                                {
                                    //For Debit
                                    accountTransactionsDebitUpdate($id = $data['edit_type_check'][$key],
                                      $VoucherId            = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = 0, 
                                      $RegisterID           = 0, 
                                      $HeadID               = $value, 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = $data['amount'][$key], 
                                      $CreditAmount         = '0.00', 
                                      $AccountNumber        = null, 
                                      $VoucherType          = 'CN', 
                                      $CBAccount            = $acc_account_head['id'], 
                                      $IsPosted             = null, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = null, 
                                      $updated_by           = $user_id
                                    );

                                    //For Credit
                                    accountTransactionsCreditUpdate($id = $data['edit_type_check'][$key] + 1,
                                      $VoucherId            = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = 0, 
                                      $RegisterID           = 0, 
                                      $HeadID               = $acc_account_head['id'], 
                                      $PurposeAccHeadName   = $acc_account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = '0.00', 
                                      $CreditAmount         = $data['amount'][$key], 
                                      $AccountNumber        = $acc_account_head['id'], 
                                      $VoucherType          = 'CN', 
                                      $CBAccount            = $value, 
                                      $IsPosted             = null, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = null, 
                                      $updated_by           = $user_id
                                    );
                                }
                            }

                            if ($data['contra_type'] == 2)
                            {
                                $account_head       = AccountHead::find($data['bank_account_id']);
                                $acc_account_head   = AccountHead::where('PurposeHeadName', 'cash_ac')->first();

                                if ($data['edit_type_check'][$key] == 0)
                                {
                                    //For Debit
                                    accountTransactionsDebitStore($VoucherId = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = 0, 
                                      $RegisterID           = 0, 
                                      $HeadID               = $acc_account_head['id'], 
                                      $PurposeAccHeadName   = $acc_account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = $data['amount'][$key], 
                                      $CreditAmount         = '0.00', 
                                      $AccountNumber        = null, 
                                      $VoucherType          = 'CN', 
                                      $CBAccount            = $data['bank_account_id'], 
                                      $IsPosted             = 0, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = 0, 
                                      $created_by           = $user_id
                                    );

                                    //For Credit
                                    accountTransactionsCreditStore($VoucherId = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = 0, 
                                      $RegisterID           = 0, 
                                      $HeadID               = $data['bank_account_id'], 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = '0.00', 
                                      $CreditAmount         = $data['amount'][$key], 
                                      $AccountNumber        = $data['bank_account_id'], 
                                      $VoucherType          = 'CN', 
                                      $CBAccount            = $acc_account_head['id'], 
                                      $IsPosted             = 0, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = 1, 
                                      $created_by           = $user_id
                                    );
                                }
                                else
                                {
                                    //For Debit
                                    accountTransactionsDebitUpdate($id = $data['edit_type_check'][$key], 
                                      $VoucherId            = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = 0, 
                                      $RegisterID           = 0, 
                                      $HeadID               = $acc_account_head['id'], 
                                      $PurposeAccHeadName   = $acc_account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = $data['amount'][$key], 
                                      $CreditAmount         = '0.00', 
                                      $AccountNumber        = null, 
                                      $VoucherType          = 'CN', 
                                      $CBAccount            = $data['bank_account_id'], 
                                      $IsPosted             = null, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = null, 
                                      $updated_by           = $user_id
                                    );

                                    //For Credit
                                    accountTransactionsCreditUpdate($id = $data['edit_type_check'][$key] + 1,
                                      $VoucherId            = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = 0, 
                                      $RegisterID           = 0, 
                                      $HeadID               = $data['bank_account_id'], 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = '0.00', 
                                      $CreditAmount         = $data['amount'][$key], 
                                      $AccountNumber        = $data['bank_account_id'], 
                                      $VoucherType          = 'CN', 
                                      $CBAccount            = $acc_account_head['id'], 
                                      $IsPosted             = null, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = null, 
                                      $updated_by           = $user_id
                                    );
                                }
                            }

                            if ($data['contra_type'] == 3)
                            {
                                $account_head       = AccountHead::find($value);

                                if ($data['edit_type_check'][$key] == 0)
                                {
                                    //For Debit
                                    accountTransactionsDebitStore($VoucherId = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = 0, 
                                      $RegisterID           = 0, 
                                      $HeadID               = $value, 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = $data['amount'][$key], 
                                      $CreditAmount         = '0.00',
                                      $AccountNumber        = '', 
                                      $VoucherType          = 'CN', 
                                      $CBAccount            = $data['bank_account_id'],  
                                      $IsPosted             = 0, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = 0, 
                                      $created_by           = $user_id
                                    );

                                    //For Credit
                                    accountTransactionsCreditStore($VoucherId = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = 0, 
                                      $RegisterID           = 0, 
                                      $HeadID               = $data['bank_account_id'], 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key],
                                      $DebitAmount          = '0.00', 
                                      $CreditAmount         = $data['amount'][$key],  
                                      $AccountNumber        = null, 
                                      $VoucherType          = 'CN', 
                                      $CBAccount            = $value,
                                      $IsPosted             = 0, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = 1, 
                                      $created_by           = $user_id
                                    );
                                }
                                else
                                {
                                    //For Debit
                                    accountTransactionsDebitUpdate($id = $data['edit_type_check'][$key], 
                                      $VoucherId            = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = 0, 
                                      $RegisterID           = 0, 
                                      $HeadID               = $value, 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = $data['amount'][$key], 
                                      $CreditAmount         = '0.00', 
                                      $AccountNumber        = null, 
                                      $VoucherType          = 'CN', 
                                      $CBAccount            = $data['bank_account_id'], 
                                      $IsPosted             = null, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = null, 
                                      $updated_by           = $user_id
                                    );

                                    //For Credit
                                    accountTransactionsCreditUpdate($id = $data['edit_type_check'][$key] + 1,
                                      $VoucherId            = $storeSummary, 
                                      $VoucherDate          = date('Y-m-d', strtotime($data['voucher_date'])), 
                                      $VoucherNumber        = $data['voucher_number'], 
                                      $ProjectID            = 0, 
                                      $RegisterID           = 0, 
                                      $HeadID               = $data['bank_account_id'], 
                                      $PurposeAccHeadName   = $account_head['PurposeHeadName'], 
                                      $Particulars          = $data['particular'][$key], 
                                      $DebitAmount          = '0.00', 
                                      $CreditAmount         = $data['amount'][$key], 
                                      $AccountNumber        = $data['bank_account_id'], 
                                      $VoucherType          = 'CN', 
                                      $CBAccount            = $value, 
                                      $IsPosted             = null, 
                                      $CompanyID            = $branch_id, 
                                      $Status               = null, 
                                      $updated_by           = $user_id
                                    );
                                }
                            }
                        }

                        if((isset($data['check_cheque_number'])) && (count($data['check_cheque_number']) > 0))
                        {
                            foreach ($data['check_cheque_number'] as $key1 => $cheque)
                            {   
                                if ($data['edit_cheque_check'][$key1] == 0)
                                {
                                    if ($cheque != null)
                                    {
                                        chequeStore($VoucherDate= $data['voucher_date'], 
                                            $VoucherNumber      = $storeSummary, 
                                            $CompanyID          = $branch_id, 
                                            $ProjectID          = 0, 
                                            $RegisterID         = 0, 
                                            $HeadID             = $bankHeadID, 
                                            $ChequeState        = 'ISSUED', 
                                            $Type               = $data['type'][$key1],
                                            $StaffID            = $user_id, 
                                            $ChequeNumber       = $data['check_cheque_number'][$key1], 
                                            $ChequeType         = $data['type'][$key1], 
                                            $ChequeDate         = $data['cq_date'][$key1], 
                                            $EnCashDate         = $data['en_date'][$key1], 
                                            $Amount             = $data['check_amount'][$key1], 
                                            $created_by         = $user_id
                                        );
                                    }
                                }
                                else
                                {   
                                    if ($cheque != null)
                                    {
                                        chequeUpdate($id            = $data['edit_cheque_check'][$key1],
                                                $VoucherDate        = $data['voucher_date'], 
                                                $VoucherNumber      = $storeSummary, 
                                                $CompanyID          = $branch_id, 
                                                $ProjectID          = 0, 
                                                $RegisterID         = 0, 
                                                $HeadID             = $bankHeadID, 
                                                $ChequeState        = 'ISSUED', 
                                                $Type               = $data['type'][$key1],
                                                $StaffID            = $user_id, 
                                                $ChequeNumber       = $data['check_cheque_number'][$key1], 
                                                $ChequeType         = $data['type'][$key1], 
                                                $ChequeDate         = $data['cq_date'][$key1], 
                                                $EnCashDate         = $data['en_date'][$key1], 
                                                $Amount             = $data['check_amount'][$key1], 
                                                $updated_by         = $user_id
                                            );
                                    }
                                }
                            }
                        }

                        if (isset($cheque_entry_data))
                        {
                            DB::table('tbl_acc_cheque_transaction')->insert($cheque_entry_data);
                        }

                        DB::commit();

                        if ($data['print'] == 1)
                        {
                            return back()->with("success","Voucher Updated Successfully.");
                        }

                        if ($data['print'] == 2)
                        {
                            return redirect()->route('voucher_print', $storeSummary);
                        }
                    }
                }catch (\Exception $exception){
                    DB::rollback();
                    return back()->with("unsuccess","Not Updated");
                }
            }
        ##Contra Voucher End##

        ##Common Codes Start##
            public function gets_accountsHead_names($branch_id)
            {
                $result = AccountHead::where('IsTransactable', 1)
                                        ->where('ActiveStatus', 1)
                                        ->where('CompanyID', $branch_id)
                                        ->orderBy('PtnGroupCode', 'ASC')
                                        ->get();

                return $result;
            }

            public function gets_projects_name($branch_id)
            {
                $result = Projects::where('IsTransactable', 1)
                                        ->where('ActiveStatus', 1)
                                        ->where('CompanyID', $branch_id)
                                        ->orderBy('PtnGroupCode', 'ASC')
                                        ->get();

                return $result;
            }

            public function gets_register_names($branch_id)
            {
                $result = Clients::where('IsTransactable', 1)
                                        ->where('ActiveStatus', 1)
                                        ->where('CompanyID', $branch_id)
                                        ->orderBy('PtnGroupCode', 'ASC')
                                        ->get();

                return $result;
            }

            public function gets_bank_accounts($branch_id)
            {
                $result = AccountHead::where('IsTransactable', 1)
                                        ->where('ActiveStatus', 1)
                                        ->where('CompanyID', $branch_id)
                                        ->where('PurposeHeadName', 'bank_ac')
                                        ->orderBy('PtnGroupCode', 'ASC')
                                        ->get();

                return $result;
            }

            public function gets_voucherNumber_name($branch_id, $type) 
            {
                $voucher        = VoucherSummary::where('Type', $type)->orderBy('id', 'DESC')->first();
                $voucherNumber  = $voucher != null ? $voucher['VoucherNumber'] + 1 : 1;

                return $voucherNumber;
            }

            public function getBankBalance($bank_id)
            {
                $balance = cachInBank($bank_id);

                return Response::json($balance);
            }

            public function getCashBalance()
            {
                $cash_in_hand = cachInHand();

                return Response::json($cash_in_hand);
            }
        ##Common Codes End##

        public function voucherPostingIndex()
        {
            $branch_id              = Auth()->user()->branch_id;
            // $data['voucherNumber']  = $this->gets_voucherNumber_name($branch_id);
            $data['userList']       = Users::where('branch_id', $branch_id)->get();
            $data['voucherList']    = VoucherSummary::where('status', 0)->get();

            return view('invoices::voucherPosting.index', compact('data'));
        }

        public function getVoucherDetails($voucher_id)
        {
            $summary  = VoucherSummary::find($voucher_id);

            $data['summary']['id']                  = $summary->id;
            $data['summary']['type']                = $summary->Type;
            $data['summary']['status']              = $summary->Status;
            $data['summary']['voucher_number']      = $summary->VoucherNumber;
            $data['summary']['voucher_date']        = date('d-m-Y', strtotime($summary->VoucherDate));
            $data['summary']['entry_by']            = $summary->createdBy->name;

            foreach($summary->accountTransactions as $key => $val)
            {
                $data['details'][$key]['account_head_name']     = isset($val->headName->HeadName) ? $val->headName->HeadName : '';
                $data['details'][$key]['project_name']          = $val->ProjectID != 0 ? $val->projectName->ProjectName : '';
                $data['details'][$key]['register_name']         = $val->RegisterID != 0 ? $val->registerName->ClientName : '';
                $data['details'][$key]['particular']            = $val->Particulars;
                $data['details'][$key]['debit_amount']          = $val->DebitAmount;
                $data['details'][$key]['credit_amount']         = $val->CreditAmount;
            }

            return Response::json($data);
        }

        public function voucherPostingApprove(Request $request)
        {
            $rules = array(
                'voucher_id' => 'required',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id    = Auth()->user()->id;
            $branch_id  = Auth()->user()->branch_id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                $voucher_summary         = VoucherSummary::find($data['voucher_id']);
                $voucher_summary->Status = 1;
                
                if ($voucher_summary->save())
                {   
                    $voucher_transactions = AccountTransaction::where('VoucherId', $data['voucher_id'])->get();

                    foreach ($voucher_transactions as $key => $value)
                    {
                        $account_trans           = AccountTransaction::find($value->id);
                        $account_trans->IsPosted = 1;
                        $account_trans->save();
                    }

                    if ($account_trans['RegisterID'] != 0)
                    {
                        balanceUpdate($account_trans['RegisterID']);
                    }

                    DB::commit();

                    return back()->with("success","Voucher Posted Successfully.");
                }
            }catch (\Exception $exception){
                DB::rollback();
                return back()->with("unsuccess","Not Added");
            }
        }

        public function voucherPostingUnapprove($id)
        {
            DB::beginTransaction();

            try{
                $voucher_summary         = VoucherSummary::find($id);
                $voucher_summary->Status = 0;
                
                if ($voucher_summary->save())
                {   
                    $voucher_transactions = AccountTransaction::where('VoucherId', $id)->get();

                    foreach ($voucher_transactions as $key => $value)
                    {
                        $account_trans           = AccountTransaction::find($value->id);
                        $account_trans->IsPosted = 0;
                        $account_trans->save();
                    }

                    DB::commit();

                    return back()->with("success","Voucher Unposted Successfully.");
                }
            }catch (\Exception $exception){
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }

        public function voucherList($voucher_type, $entry_by, $type, $date_from, $date_to)
        {
            $from_date = date('Y-m-d', strtotime($date_from));
            $to_date   = date('Y-m-d', strtotime($date_to));

            if ($voucher_type == 0) 
            {
                $data   = VoucherSummary::when($entry_by != 0, function ($query) use ($entry_by) {
                                            return $query->where('StaffID', $entry_by);
                                        })
                                        ->when(!is_numeric($type), function ($query) use ($type) {
                                            return $query->where('Type', $type);
                                        })
                                        ->when($from_date != 0 && $to_date != 0, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('VoucherDate', [$from_date, $to_date]);
                                        })
                                        ->where('Status', 0)
                                        ->get();
            }
            elseif ($voucher_type == 1)
            {
                $data   = VoucherSummary::when($entry_by != 0, function ($query) use ($entry_by) {
                                            return $query->where('StaffID', $entry_by);
                                        })
                                        ->when(!is_numeric($type), function ($query) use ($type) {
                                            return $query->where('Type', $type);
                                        })
                                        ->when($from_date != 0 && $to_date != 0, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('VoucherDate', [$from_date, $to_date]);
                                        })
                                        ->where('Status', 1)
                                        ->get();
            }
            else
            {
                $data   = VoucherSummary::when($entry_by != 0, function ($query) use ($entry_by) {
                                            return $query->where('StaffID', $entry_by);
                                        })
                                        ->when(!is_numeric($type), function ($query) use ($type) {
                                            return $query->where('Type', $type);
                                        })
                                        ->when($from_date != 0 && $to_date != 0, function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('VoucherDate', [$from_date, $to_date]);
                                        })
                                        ->get();
            }

            return Response::json($data);
        }

        public function loadVouchersDatatable()
        {
            ## Read value
            $draw            = $_GET['draw'];
            $row             = $_GET['start'];
            $rowperpage      = $_GET['length']; // Rows display per page
            $columnIndex     = $_GET['order'][0]['column']; // Column index
            $columnName      = $_GET['columns'][$columnIndex]['data']; // Column name
            $columnSortOrder = $_GET['order'][0]['dir']; // asc or desc
            $searchValue     = $_GET['search']['value']; // Search value
            $from_date       = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d');
            $to_date         = isset($_GET['to_date']) ? date('Y-m-d', strtotime($_GET['to_date'])) : date('Y-m-d');
            $type            = isset($_GET['type']) ? $_GET['type'] : 0;
            $typeStatus      = isset($_GET['status']) ? $_GET['status'] : 2;
            $role            = Auth()->user()->role;
            $user_id         = Auth()->user()->id;

            ## Total number of records without filtering
            if(($role == 1 || $role == 2))
            {
                $totalRecords   = VoucherSummary::where('VoucherDate', '>=', $from_date)
                                            ->where('VoucherDate', '<=', $to_date)
                                            ->when(!is_numeric($type), function ($query) use ($type) {
                                                return $query->where('tbl_acc_voucher_summary.Type', $type);
                                            })
                                            ->when($typeStatus != 2, function ($query) use ($typeStatus) {
                                                return $query->where('tbl_acc_voucher_summary.Status', $typeStatus);
                                            })
                                            ->orderBy('id', 'DESC')
                                            ->count();
            }
            else
            {
                $totalRecords   = VoucherSummary::where('VoucherDate', '>=', $from_date)
                                            ->where('VoucherDate', '<=', $to_date)
                                            ->when(!is_numeric($type), function ($query) use ($type) {
                                                return $query->where('tbl_acc_voucher_summary.Type', $type);
                                            })
                                            ->when($typeStatus != 2, function ($query) use ($typeStatus) {
                                                return $query->where('tbl_acc_voucher_summary.Status', $typeStatus);
                                            })
                                            ->where('tbl_acc_voucher_summary.created_by', $user_id)
                                            ->orderBy('id', 'DESC')
                                            ->count();
            }

            $totalRecordwithFilter  = $totalRecords;

            if(($role == 1 || $role == 2))
            {
                $empQuery   = VoucherSummary::where('VoucherDate', '>=', $from_date)
                                            ->where('VoucherDate', '<=', $to_date)
                                            ->when(!is_numeric($type), function ($query) use ($type) {
                                                return $query->where('tbl_acc_voucher_summary.Type', $type);
                                            })
                                            ->when($typeStatus != 2, function ($query) use ($typeStatus) {
                                                return $query->where('tbl_acc_voucher_summary.Status', $typeStatus);
                                            })
                                            ->offset($row)
                                            ->limit($rowperpage)
                                            ->selectRaw('tbl_acc_voucher_summary.*')
                                            ->orderBy('id', 'DESC')
                                            ->get();
            }
            else
            {
                $empQuery   = VoucherSummary::where('VoucherDate', '>=', $from_date)
                                            ->where('VoucherDate', '<=', $to_date)
                                            ->when(!is_numeric($type), function ($query) use ($type) {
                                                return $query->where('tbl_acc_voucher_summary.Type', $type);
                                            })
                                            ->when($typeStatus != 2, function ($query) use ($typeStatus) {
                                                return $query->where('tbl_acc_voucher_summary.Status', $typeStatus);
                                            })
                                            ->where('tbl_acc_voucher_summary.created_by', $user_id)
                                            ->offset($row)
                                            ->limit($rowperpage)
                                            ->selectRaw('tbl_acc_voucher_summary.*')
                                            ->orderBy('id', 'DESC')
                                            ->get();                           
            }


            $data       = array();
            $i          = $row;
            foreach ($empQuery as $key => $value)
            {
                if(($value->Type == 'CP') && ($value->Status == 0))
                {
                    $edit  = '<a class="dropdown-item" href="'.route("cash_payment_voucher_edit", $value["id"]).'" target="_blank">Edit</a>';
                }
                elseif(($value->Type == 'CR') && ($value->Status == 0))
                {
                    $edit  = '<a class="dropdown-item" href="'.route("cash_receipt_voucher_edit", $value["id"]).'" target="_blank">Edit</a>';
                }
                elseif(($value->Type == 'BP') && ($value->Status == 0))
                {
                    $edit  = '<a class="dropdown-item" href="'.route("bank_payment_voucher_index", $value["id"]).'" target="_blank">Edit</a>';
                }
                elseif(($value->Type == 'BR') && ($value->Status == 0))
                {
                    $edit  = '<a class="dropdown-item" href="'.route("bank_receipt_voucher_index", $value["id"]).'" target="_blank">Edit</a>';
                }
                elseif(($value->Type == 'JR') && ($value->Status == 0))
                {
                    $edit  = '<a class="dropdown-item" href="'.route("journal_voucher_index", $value["id"]).'" target="_blank">Edit</a>';
                }
                elseif(($value->Type == 'CN') && ($value->Status == 0))
                {
                    $edit  = '<a class="dropdown-item" href="'.route("contra_voucher_index", $value["id"]).'" target="_blank">Edit</a>';
                }
                else
                {
                    $edit  = '';
                }

                if($value->Status == 0)
                {
                    $status = 'Unposted';
                }
                elseif($value->Status == 1)
                {
                   $status = 'Posted'; 
                }

                $i++;
                $action  = '<div class="dropdown">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" style="">
                                    <a class="dropdown-item" href="'.route("voucher_print", $value["id"]).'" target="_blank">Print</a>
                                    '.$edit.'
                                </div>
                            </div>';

                $data[] = array( 
                  "sl"              =>$i,
                  "date"            =>date('d-m-Y', strtotime($value->VoucherDate)),
                  "voucher_number"  =>$value->Type.'/'.$value->VoucherNumber,
                  "receipt_number"  =>$value->MoneyReceiptNo,
                  "voucher_type"    =>$value->Type,
                  "amount"          =>$value->TotalAmount,
                  "status"          =>$status,
                  "action"          =>$action,
               );
            }

            ## Response
            $response = array(
              "draw"                 => intval($draw),
              "iTotalRecords"        => $totalRecords,
              "iTotalDisplayRecords" => $totalRecordwithFilter,
              "aaData"               => $data
            );

            return Response::json($response);
        }
    //Financial Accounting End
}
