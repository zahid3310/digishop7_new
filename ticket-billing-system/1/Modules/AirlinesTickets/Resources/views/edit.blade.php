@extends('layouts.app')

@section('title', 'Edit Sales of Ticket')

<style type="text/css">
    body[data-sidebar=dark].vertical-collpsed {
        min-height: 0px !important;
    }
    
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Orders</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">New Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('sales_ticket_update', $find_sales->id) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                {{ csrf_field() }}

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-5 col-form-label">Customer Name *</label>
                                            <div class="col-md-7">
                                                <select style="width: 100%" name="customer_id" class="form-control select2" required>
                                                   <option value="{{ $find_sales->customer_id }}">{{ $find_sales->customer->ClientName }}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label id="bbShow" for="productname" class="col-md-5 col-form-label">Receivable</label>
                                            <div class="col-md-7">
                                                <input id="balance" name="previous_due" type="text" value="" class="form-control" value="0" readonly>
                                                <input id="bbBalance" type="hidden" name="balance_type" class="form-control" value="1">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-5 col-form-label">Issue By</label>
                                            <div class="col-md-7">
                                                <input id="issue_by" name="issue_by" type="text" class="form-control" value="{{ $find_sales->issue_by }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-5 col-form-label">Issue Date</label>
                                            <div class="col-md-7">
                                                <input id="issue_date" name="issue_date" type="text" value="{{ date('d-m-Y', strtotime($find_sales->issue_date)) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-1" style="margin-bottom: 5px;">
                                        <button style="padding: 10px 35px;" id="add_field_button" class=" btn btn-success btn-block inner add_field_button col-md-3"><i  class="fas fa-plus"></i></button>
                                    </div>
                                </div>


                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 305px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        @foreach($sales_entries as $key => $value)
                                            <div class="row di_{{$key}}">
                                                <div style="margin-bottom: 5px;padding-left: 5px" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                    <input type="text" name="pax_name[]" class="inner form-control" id="pax_name_{{$key}}" placeholder="Pax Name" required value="{{ $value->pax_name }}" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                    <input type="text" name="eticket_no[]" class="inner form-control" id="eticket_no_{{$key}}" placeholder="Eticket No"/> 
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <input type="text" name="pnr[]" class="inner form-control" id="pnr_{{$key}}" placeholder="PNR" value="{{ $value->pnr }}" />
                                                </div>                                                    

                                                <div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <input type="text" name="passport_number[]" class="inner form-control" id="passport_number_{{$key}}" placeholder="Passport Number"/> 
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px;" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <input type="text" name="sector[]" class="inner form-control" id="sector_{{$key}}" placeholder="Sector" value="{{ $value->sector }}" /> 
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6"> 
                                                    <input type="text" name="carrier_name[]" class="inner form-control" id="carrier_name_{{$key}}" placeholder="Carrier" value="{{ $value->carrier_name }}"/> 
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6"> 
                                                    <input type="text" name="eq_fare[]" class="inner form-control" id="eq_fare_{{$key}}" placeholder="EQ Fare" /> 
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 5px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <input type="text" name="tax[]" class="inner form-control" id="tax_{{$key}}" placeholder="Tax"/> 
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                    <input type="text" name="gross_fare[]" class="inner form-control grossFare" id="gross_fare_{{$key}}" placeholder="Gross Fare" oninput="calculateActualAmount({{$key}})" value="{{ $value->gross_fare }}"/>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <input type="text" name="commission[]" class="inner form-control" id="commission_{{$key}}" placeholder="Commission" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <input type="text" name="ait_tax[]" class="inner form-control" id="ait_tax_{{$key}}" placeholder="AIT Tax"/> 
                                                </div>  

                                                <div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <input type="text" name="net_fare[]" class="inner form-control agentFare" id="net_fare_{{$key}}" placeholder="Agent Fare" oninput="calculateActualAmount({{$key}})" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 5px" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                    <select id="airline_id_{{$key}}" style="cursor: pointer" name="ariline_id[]" class="form-control single_select2">
                                                        <option value="">--Select Agent/Airlines--</option>
                                                        @if(!empty($airlines) && ($airlines->count() > 0))
                                                        @foreach($airlines as $key => $airline)
                                                            <option value="{{ $airline['id'] }}" {{ $airline['id'] == $value['airlines_id'] ? 'selected' : '' }}>{{ $airline['ClientName'] }}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6"> 
                                                    <input type="text" name="airline_fare[]" class="inner form-control airlineFare" id="airline_fare_{{$key}}" placeholder="Net Fare" oninput="calculateActualAmount({{$key}})" value="{{ $value->airline_fare }}"/>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <input type="text" name="fly_date[]" value="{{ date('d-m-Y', strtotime($value->fly_date)) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" id="fly_date_{{$key}}" />
                                                </div>
                                                
                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="{{$key}}"> 
                                                    <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>
                                                </div>
                                            </div>

                                            <hr style="margin-top: 5px !important;margin-bottom: 10px !important">
                                        @endforeach
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>

                                    <div style="display: none;" class="row justify-content-end">
                                        <div class="col-lg-1 col-md-2 form-group">
                                            <input id="pos_add_button" type="button" class="btn btn-success btn-block inner add_field_button_pos" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                <div style="background-color: #F4F4F7;height:65px;padding-top: 13px" class="row">
                                    <div style="margin-bottom: 5px;padding-left: 10px;" class="form-group row">
                                        <label for="productname" class="col-md-4 col-form-label">Sub Total</label>
                                        <div class="col-md-8">
                                            <input id="subTotalBdt" type="text" value="" class="form-control" placeholder="Sub Total">
                                            <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount" readonly>
                                            <input style="display: none"  type="text" id="totalGrossFare" name="total_gross_fare" readonly>
                                            <input style="display: none"  type="text" id="totalAirlineFare" name="total_airline_fare" readonly>
                                        </div>
                                    </div>

                                    <div style="margin-bottom: 5px;padding-left: 10px;" class="form-group row">
                                        <label for="productname" class="col-md-6 col-form-label">Discount</label>
                                        <div class="col-md-6">
                                            <select style="border-radius: 4px;cursor: pointer" class="form-control totalDiscountType" id="total_discount_type_0" name="total_discount_type" oninput="calculateActualAmount(0)">
                                                <option style="padding: 10px" value="1" selected>BDT</option>
                                            </select>
                                        </div>
                                    </div>
                          
                                    <div style="margin-bottom: 5px;padding-left: 10px;" class="form-group row">
                                        <div class="col-md-6">
                                             <input id="total_discount_0" type="text" class="form-control totalDiscount" name="total_discount_amount" value="0" oninput="calculateActualAmount(0)">
                                        </div>
                                    </div>

                                    <div style="margin-bottom: 5px;padding-left:0px;" class="form-group row">
                                        <label for="productname" class="col-md-5 col-form-label">Total Payable</label>
                                        <div class="col-md-7">
                                            <input type="text" id="totalBdt" class="form-control">
                                            <input style="display: none" type="text" id="totalBdtShow" name="total_amount" readonly>
                                        </div>
                                    </div>

                                    <div style="margin-bottom: 5px;padding-left: 10px;" class="form-group row">
                                        <label for="productname" class="col-md-4 col-form-label">Received</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="cash_given" name="cash_given" placeholder="Cash Given" value="{{ $find_sales['cash_given'] }}" oninput="calculateChangeAmount()">
                                        </div>
                                    </div>
    
                                    <input type="hidden" name="customer_advance_amount" id="customer_advance_amount" value="0">

                                    <div style="margin-bottom: 5px;display: none;" class="form-group row">
                                        <label style="text-align: right" class="col-md-5 col-form-label">Advance</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control" id="change_amount" name="change_amount" placeholder="Advance Amount" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px !important;padding-bottom: 0px !important;" class="row">
                                    <div style="background-color: #D2D2D2;height: 115px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-9 input_fields_wrap_current_balance getMultipleRowCurrentBalance">
                                        @if($current_balance->count() > 0)
                                        @foreach($current_balance as $key_current_balance => $current_balance_value)
                                        <div class="row di_current_balance_{{$key_current_balance}}">
                                            <div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                @if($key_current_balance == 0)
                                                    <label class="hidden-xs" for="productname">Date *</label>
                                                @endif
                                                <label style="display: none" class="show-xs" for="productname">Date *</label>
                                                <input id="current_balance_payment_date" name="current_balance_payment_date[]" type="text" value="{{ date('d-m-Y', strtotime($current_balance_value['VoucherDate'])) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                            </div>

                                            <div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                @if($key_current_balance == 0)
                                                    <label class="hidden-xs" for="productname">Amount Paid *</label>
                                                @endif
                                                <label style="display: none" class="show-xs" for="productname">Amount Paid *</label>
                                                <input type="text" name="current_balance_amount_paid[]" class="form-control currentBalancePaidAmount" id="current_balance_amount_paid_{{$key_current_balance}}" value="{{$current_balance_value['DebitAmount']}}" oninput="checkCurrentBalance({{$key_current_balance}})" required />
                                            </div>

                                            <div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                @if($key_current_balance == 0)
                                                    <label class="hidden-xs" for="productname">Paid Through *</label>
                                                @endif
                                                <label style="display: none" class="show-xs" for="productname">Paid Through *</label>
                                                <select id="current_balance_paid_through_{{$key_current_balance}}" style="cursor: pointer" name="current_balance_paid_through[]" class="form-control single_select2">
                                                    @if(!empty($paid_accounts) && ($paid_accounts->count() > 0))
                                                    @foreach($paid_accounts as $key => $paid_account)
                                                        <option {{ $paid_account['id'] == $current_balance_value['HeadID'] ? 'selected' : '' }} value="{{ $paid_account['id'] }}">{{ $paid_account['HeadName'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>

                                            <div style="" class="col-lg-2 col-md-3 col-sm-6 col-6">
                                                @if($key_current_balance == 0)
                                                    <label class="hidden-xs" for="productname">AC Info.</label>
                                                @endif
                                                <label style="display: none" class="show-xs" for="productname">Account Info.</label>
                                                <input type="text" name="current_balance_account_information[]" class="form-control" id="current_balance_account_information_{{$key_current_balance}}" value="{{$current_balance_value['account_information']}}"/>
                                            </div>

                                            <div style="" class="col-lg-2 col-md-3 col-sm-6 col-6">
                                                @if($key_current_balance == 0)
                                                    <label class="hidden-xs" for="productname">Note</label>
                                                @endif
                                                <label style="display: none" class="show-xs" for="productname">Note</label>
                                                <input type="text" name="current_balance_note[]" class="form-control" id="current_balance_note_{{$key_current_balance}}" value="{{$current_balance_value['note']}}"/>
                                            </div>

                                            <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group">
                                                @if($key_current_balance == 0)
                                                    <label class="hidden-xs" for="productname">Action</label>
                                                @endif
                                                <label style="display: none" class="show-xs" for="productname">Action</label>
                                                @if($key_current_balance == 0)
                                                <i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonCurrentBalance()"></i>
                                                @else
                                                <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>
                                                @endif
                                            </div>
                                        </div>
                                        <input type="hidden" name="current_balance_id[]" value="{{ $current_balance_value['id'] }}">
                                        <input type="hidden" name="current_balance_account_id[]" value="{{ $current_balance_value['HeadID'] }}">
                                        @endforeach
                                        @endif

                                        <div style="display: none" class="row justify-content-end">
                                            <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                                <input id="add_field_button_current_balance" type="button" class="btn btn-success btn-block inner add_field_button_current_balance" value="Add"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3" style="text-align: right;background-color: #D2D2D2;height: 90px;padding-top: 5px;">
                                        <div style="margin-top: 25px;" class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" type="submit" class="btn btn-primary waves-effect waves-light" name="print" value="1">Update</button>
                                                <button style="border-radius: 0px !important;display:none" type="submit" class="btn btn-success waves-effect waves-light" name="print" value="2">Save & Print</button>
                                                <button style="border-radius: 0px !important;display:none" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('invoices_index') }}">Close</a></button>

                                                <button style="border-radius: 0px !important;display: none;" class="btn btn-info waves-effect waves-light" type="button" data-toggle="modal" data-target="#myModal2" onclick="printInvoiceList()">Print Invoices</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button_current_balance" type="button" class="btn btn-success btn-block inner add_field_button_current_balance" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" id="taxPercentage">
                                <input type="hidden" id="taxAmount">
                                <input type="hidden" id="aitAmount">

                                </form>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control commonCustomerClass">
                        </div>
                    </div>

                    <input id="contact_type" name="contact_type" type="hidden" value="0" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Reference</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name1" name="customer_name" type="text" class="form-control commonReferenceClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number1" name="mobile_number" type="text" class="form-control commonReferenceClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address1" name="address" type="text" class="form-control commonReferenceClass">
                        </div>
                    </div>

                    <input id="contact_type1" name="contact_type" type="hidden" value="3" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal2" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Print Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label for="productname" class="col-md-4 col-form-label">Date</label>
                                <div class="col-md-8">
                                    <input style="cursor: pointer" id="search_date" type="date" value="<?= date("Y-m-d") ?>" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Customer </label>
                                <div class="col-md-8">
                                    <input id="customer" type="text" class="form-control"  placeholder="Enter Customer Name">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Search Invoice </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <input style="width: 95%" type="text" class="form-control col-lg-9 col-md-9 col-sm-9 col-9" id="invoiceNumber" placeholder="Enter Invoice ID">
                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="printInvoicesSearch()">
                                            <i class="bx bx-search font-size-24"></i>
                                        </span>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Order#</th>
                                <th>Customer</th>
                                <th>Amount</th>
                                <th>Paid</th>
                                <th>Due</th>
                                <th>Creator</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody id="print_invoice_list">
                        </tbody>
                    </table>
                </div>
                
                <div class="modal-footer">
                    <button id="CloseButton2" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            $('#vertical-menu-btn').click();
            
            var site_url = $('.site_url').val();

            $.get(site_url + '/airlinestickets/tax-Ait', function(data){
                $("#taxPercentage").val(data.tax_percentage);
                $("#taxAmount").val(data.tax_amount);
                $("#aitAmount").val(data.ait_amont);
            });

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/airlinestickets/clients/list/tickets',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'] + ' || ' + result['ClientContactNumber'];
                },
            });

            $('#add_field_button_current_balance').click();

            var type = $("#bbBalance").val();

            if (type == 1)
            {
                $("#bbShow").html('Receivable');
                $("#bbBalance").val(type);
            }
            else
            {
                $("#bbShow").html('Advance');
                $("#bbBalance").val(type);
            }

            var customerId      = $("#customer_id").val();

            $.get(site_url + '/invoices/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data.balance);

                if (data.type == 1)
                {
                    $("#bbShow").html('Receivable');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', false);
                }
                else
                {
                    $("#bbShow").html('Advance');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', true);
                }
            });

            calculateActualAmount(0);
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = $("#contact_type").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                            $('.commonCustomerClass').val('');
                            $("#bbBalance").val(0);
                            $("#balance").val(0);
                        }
                        
                        $("#customer_id").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });

        $('#submitBtn1').click(function() {
            
            var customer_name               = $("#customer_name1").val();
            var address                     = $("#address1").val();
            var mobile_number               = $("#mobile_number1").val();
            var contact_type                = $("#contact_type1").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton1').click();
                            $('.commonReferenceClass').val('');
                        }
                        
                        $("#reference_id").empty();
                        $('#reference_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });     
    </script>

    <script type="text/javascript">
        // Get the input field
        var input = document.getElementById("product_code");

            // Execute a function when the user releases a key on the keyboard
            input.addEventListener("keyup", function(event) {
            // Number 13 is the "Enter" key on the keyboard
            if (event.keyCode === 13) {
                // Cancel the default action, if needed
                event.preventDefault();
                $("#alertMessage").hide();

                var site_url            = $(".site_url").val();
                var product_code_pos    = $("#product_code").val();

                if(product_code_pos != '')
                {
                    $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code_pos, function(data_pos){

                        if ($.isEmptyObject(data_pos))
                        {   
                            $('input[name=product_code').val('');
                            $("#alertMessage").show();
                        }
                        else
                        {
                            var state = 0;

                            $('.productEntries').each(function()
                            {
                                var entry_id    = $(this).val();
                                var value_x     = $(this).prop("id");

                                if (data_pos.id == entry_id)
                                {
                                    var explode          = value_x.split('_');
                                    var quantity_id      = explode[2];
                                    var quantity         = $('#quantity_'+quantity_id).val();
                                    var inc_quantity     = parseFloat(quantity) + 1;

                                    $('#quantity_'+quantity_id).val(inc_quantity);
                                    $('#quantity_'+quantity_id).change();
                                    $('input[name=product_code').val('');
                                    calculateActualAmount(quantity_id);

                                    state++;
                                }

                                if (entry_id == '')
                                {   
                                    var explode          = value_x.split('_');
                                    var quantity_id      = explode[2];
                                    
                                    ProductEntriesList(quantity_id);

                                    state++;
                                }
                            });

                            if (state == 0)
                            {   
                                $("#pos_add_button").click();
                            }
                            else
                            {
                                return false;
                            }
                        }
                    });
                } 
            }
        });

        function ProductEntriesList(x) 
        {
            //Barcode Scan Section Start
            var site_url            = $(".site_url").val();
            var product_code_pos    = $("#product_code").val();

            if (product_code_pos != undefined)
            {

                var product_code        = $("#product_code").val();
                $('input[name=product_code').val('');
            }
            else
            {
                var product_code        = $(".product_entries_"+x).val();
            }

            if (product_code)
            {
                $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code, function(data_pos){

                    if (data_pos.product_type == 2)
                    {
                        var variations  = ' - ' + data_pos.variations;
                    }
                    else
                    {
                        var variations  = '';
                    }

                    $('#product_entries_'+x).append('<option value="'+ data_pos.id +'" selected>'+ data_pos.name + variations + ' ' + '( ' + data_pos.product_code + ' )' +'</option>');
                    $('#product_entries_'+x).change();
                });
            }
        }
        
        function checkValue(value,arr)
        {
            var status = 'Not exist';
             
            for(var i=0; i<arr.length; i++)
            {
                var name = arr[i];
                if(name == value)
                {
                    status = 'Exist';
                    break;
                }
            }

            return status;
        }

        function getItemPrice(x)
        {
            //For getting item commission information from items table start
            var entry_id  = $("#product_entries_"+x).val();
            var site_url  = $(".site_url").val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    var list    = '';
                    $.each(data.unit_conversions, function(i, data_list)
                    {   
                        list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                    });

                    $("#unit_id_"+x).empty();
                    $("#unit_id_"+x).append(list);

                    if (data.product_entries.stock_in_hand == '' || data.product_entries.stock_in_hand == null)
                    {
                        var stockInHand  = 0;
                    }
                    else
                    {
                        var stockInHand  = data.product_entries.stock_in_hand;
                    }

                    if (data.product_entries.unit_id == null)
                    {
                        var unit  = '';
                    }
                    else
                    {
                        var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                    }

                    $("#rate_"+x).val(parseFloat(data.product_entries.sell_price).toFixed(2));
                    $("#quantity_"+x).val(1);

                    if (data.branch_id == 1)
                    {
                        $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
                        $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' ' + unit);
                    }
                    else
                    {
                        if (data.branch_product != null)
                        {
                            $("#stock_"+x).val(parseFloat(data.branch_product.stock_in_hand).toFixed(2));
                            $("#stock_show_"+x).html('Stock : ' + parseFloat(data.branch_product.stock_in_hand).toFixed(2) + ' ' + unit);
                        }
                        else
                        {
                            $("#stock_"+x).val(0);
                            $("#stock_show_"+x).html('Stock : ' + '0' + ' ' + unit);
                        }
                    }

                    $("#main_unit_id_"+x).val(data.product_entries.unit_id);
                    $("#main_unit_name_"+x).val(data.product_entries.unit_name);
                    $("#discount_"+x).val(0);

                    calculateActualAmount(x);

                });
            }
            
            //For getting item commission information from items table end
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                    $("#stock_show_"+x).html('Stock : ' + convertedStock + ' ( ' + convertedUnitName + ' )');
                    $("#rate_"+x).val(parseFloat(data.sell_price).toFixed(2));

                    calculateActualAmount(x);
                }

            });
        }

        function pad (str, max)
        {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }

        function addButtonCurrentBalance()
        {
            $('.add_field_button_current_balance').click();
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = {{ $sales_entries_count }};
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var pax_name_label          = '<label class="hidden-xs" for="productname">Pax Name</label>\n';
                    var eticket_no_label        = '<label class="hidden-xs" for="productname">Eticket No</label>\n';
                    var pnr_label               = '<label class="hidden-xs" for="productname">PNR</label>\n';
                    var passport_number_label   = '<label class="hidden-xs" for="productname">Passport Number</label>\n';
                    var sector_label            = '<label class="hidden-xs" for="productname">Sector</label>\n';
                    var carrier_name_label      = '<label class="hidden-xs" for="productname">Carrier Name</label>\n';
                    var eq_fare_label           = '<label class="hidden-xs" for="productname">EQ Fare</label>\n';
                    var tax_label               = '<label class="hidden-xs  for="productname">TAX</label>\n';
                    var gross_fare_label        = '<label class="hidden-xs" for="productname">Gross Fare</label>\n';
                    var commission_label        = '<label class="hidden-xs" for="productname">Commission</label>\n';
                    var ait_tax_label           = '<label class="hidden-xs" for="productname">AIT TAX (0.3%)</label>\n';
                    var net_fare_label          = '<label class="hidden-xs" for="productname">Agent Fare</label>\n';
                    var fly_date_label          = '<label class="hidden-xs" for="productname">Fly Date</label>\n';
                    var action_label            = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        '<i style="width: 60%!important;padding: 0.68rem 0.75rem !important;" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }
                else
                {
                    var pax_name_label          = '';
                    var eticket_no_label        = '';
                    var pnr_label               = '';
                    var passport_number_label   = '';
                    var sector_label            = '';
                    var carrier_name_label      = '';
                    var eq_fare_label           = '';
                    var tax_label               = '';
                    var gross_fare_label        = '';
                    var commission_label        = '';
                    var ait_tax_label           = '';
                    var net_fare_label          = '';
                    var fly_date_label          = '';
                    var action_label            = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                ProductEntriesList(x);

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +

                                                    '<div style="margin-bottom: 5px;padding-left: 5px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        '<input type="text" name="pax_name[]" class="inner form-control" id="pax_name_'+x+'" placeholder="Pax Name" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        '<input type="text" name="eticket_no[]" class="inner form-control" id="eticket_no_'+x+'" placeholder="Eticket No"/>\n' + 
                                                    '</div>\n' +


                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        '<input type="text" name="pnr[]" class="inner form-control" id="pnr_'+x+'" placeholder="PNR"/>\n' + 
                                                    '</div>\n' +                                                    

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Passport Number *</label>\n' +
                                                        '<input type="text" name="passport_number[]" class="inner form-control" id="passport_number_'+x+'" placeholder="Passport Number"/>\n' + 
                                                    '</div>\n' +


                                                    '<div style="margin-bottom: 5px;padding-left: 0px;" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Sector</label>\n' +
                                                        '<input type="text" name="sector[]" class="inner form-control" id="sector_'+x+'" placeholder="Sector"/>\n' + 
                                                    '</div>\n' +


                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Carrier Name</label>\n' +
                                                        '<input type="text" name="carrier_name[]" class="inner form-control" id="carrier_name_'+x+'" placeholder="Carrier" />\n' + 
                                                    '</div>\n' + 

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">EQ Fare</label>\n' +
                                                        '<input type="text" name="eq_fare[]" class="inner form-control" id="eq_fare_'+x+'" placeholder="EQ Fare"/>\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 5px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Tax</label>\n' +
                                                        '<input type="text" name="tax[]" class="inner form-control" id="tax_'+x+'" placeholder="Tax"/>\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Gross Fare</label>\n' +
                                                        '<input type="text" name="gross_fare[]" class="inner form-control grossFare" id="gross_fare_'+x+'" placeholder="Gross Fare" oninput="calculateActualAmount('+x+')"/>\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Commission</label>\n' +
                                                        '<input type="text" name="commission[]" class="inner form-control" id="commission_'+x+'" placeholder="Commission" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">AIT Tax</label>\n' +
                                                        '<input type="text" name="ait_tax[]" class="inner form-control" id="ait_tax_'+x+'" placeholder="AIT Tax"/>\n' + 
                                                    '</div>\n' +  

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display: none" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Agent Fare</label>\n' +
                                                        '<input type="text" name="net_fare[]" class="inner form-control agentFare" id="net_fare_'+x+'" placeholder="Agent Fare" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 5px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Agent/Airlines Name</label>\n' +
                                                        '<select id="airline_id_'+x+'" style="cursor: pointer" name="ariline_id[]" class="form-control single_select2">\n' +
                                                            '<option value="">--Select Agent/Airlines--</option>\n' +
                                                            '@if(!empty($airlines) && ($airlines->count() > 0))\n' +
                                                            '@foreach($airlines as $key => $airline)\n' +
                                                                '<option value="{{ $airline['id'] }}">{{ $airline['ClientName'] }}</option>\n' +
                                                            '@endforeach\n' +
                                                            '@endif\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Net Fare</label>\n' +
                                                        '<input type="text" name="airline_fare[]" class="inner form-control airlineFare" id="airline_fare_'+x+'" placeholder="Net Fare" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Fly Date</label>\n' +
                                                        '<input type="text" name="fly_date[]" value="{{ date('d-m-Y', strtotime(date('d-m-Y'))) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" id="fly_date_'+x+'"/>\n' + 
                                                    '</div>\n' +
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' +

                                                '<hr style="margin-top: 5px !important;margin-bottom: 10px !important">'
                                            );

                                            $('.single_select2').select2();
            }                                   
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            var eq_fare                 = $("#eq_fare_"+x).val();
            var tax                     = $("#tax_"+x).val();

            var totalDiscount           = $("#total_discount_0").val();
            var totalDiscountType       = $("#total_discount_type_0").val();


            // if (eq_fare == '')
            // {
            //     var eqFareAmont             = 0;
            // }
            // else
            // {
            //     var eqFareAmont             = $("#eq_fare_"+x).val();
            // }            

            // var  taxPercentageNumber    =  $("#taxPercentage").val();
            // var  taxAmountCal           =  $("#taxAmount").val();
            // var  aitAmountCal           =  $("#aitAmount").val();


            // $("#tax_"+x).val(taxAmountCal);
            // $("#tax_"+x).val(taxAmountCal);
            // $("#ait_tax_"+x).val(aitAmountCal);

    
            // $("#commission_"+x).val((parseFloat(eqFareAmont) * taxPercentageNumber) / 100);

            // $("#gross_fare_"+x).val(parseFloat(eqFareAmont)- ((parseFloat(eqFareAmont) * taxPercentageNumber) / 100));

            // var AmountIn              =  (parseFloat(eqFareAmont)- ((parseFloat(eqFareAmont) * taxPercentageNumber) / 100))  + parseFloat(taxAmountCal) + parseFloat(aitAmountCal) ;
     
            // $("#net_fare_"+x).val(AmountIn);

            //Calculating Subtotal Amount
            var total_gross_fare  = 0;
            $('.grossFare').each(function()
            {
                total_gross_fare   += parseFloat($(this).val());
            });

            var total_agent_fare  = 0;
            $('.agentFare').each(function()
            {
                total_agent_fare   += parseFloat($(this).val());
            });

            var total_airline_fare  = 0;
            $('.airlineFare').each(function()
            {
                total_airline_fare   += parseFloat($(this).val());
            });

            if (totalDiscount > 0)
            {
                if (totalDiscountType == 0)
                {
                    var totalDiscountTypeCal     = (parseFloat(totalDiscount)*(parseFloat(total)))/100;
                }
                else
                {
                    var totalDiscountTypeCal     = $("#total_discount_0").val();
                }
            }
            else
            {
                var totalDiscountTypeCal     = 0;
            }

            var totalShow = parseFloat(total_gross_fare) - parseFloat(totalDiscountTypeCal);

            $("#totalBdt").val(parseFloat(totalShow).toFixed());
            $("#totalBdtShow").val(parseFloat(totalShow).toFixed());

            $("#subTotalBdt").val(parseFloat(total_gross_fare).toFixed());
            $("#subTotalBdtShow").val(parseFloat(total_gross_fare).toFixed());
            $("#totalGrossFare").val(parseFloat(total_gross_fare).toFixed());
            $("#totalAirlineFare").val(parseFloat(total_airline_fare).toFixed());


            //Checking Overselling End

            calculateChangeAmount();

            var type = $("#bbBalance").val();

            if (type == 2)
            {
                var site_url        = $('.site_url').val();
                var customerId      = $("#customer_id").val();

                $.get(site_url + '/invoices/adjust-advance-payment/' + customerId, function(data){

                    var payable  = $("#totalBdtShow").val();

                    if (parseFloat(payable) <= parseFloat(data))
                    {
                        $("#adjustment").val(parseFloat(payable).toFixed());
                    }
                    else
                    {
                        $("#adjustment").val(parseFloat(data).toFixed());
                    }

                    $("#customer_advance_amount").val(parseFloat(data).toFixed());
                    calculateChangeAmount();
                });
            }
        }
    </script>

    <script type="text/javascript">
        var max_fields_current_balance = 50;                           //maximum input boxes allowed
        var wrapper_current_balance    = $(".input_fields_wrap_current_balance");      //Fields wrapper
        var add_button_current_balance = $(".add_field_button_current_balance");       //Add button ID
        var index_no_current_balance   = 1;

        //For apending another rows start
        var c = {{$current_balance_count > 0 ? $current_balance_count : -1}};
        $(add_button_current_balance).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(c < max_fields_current_balance)
            {   
                c++;

                var serial = c + 1;

                if (serial == 1)
                {
                    var amount_paid_label   = '<label class="hidden-xs" for="productname">Amount Paid *</label>\n';
                    var date_label          = '<label class="hidden-xs" for="productname">Date *</label>\n';
                    var paid_through_label  = '<label class="hidden-xs" for="productname">Paid Through *</label>\n';
                    var account_info_label  = '<label class="hidden-xs" for="productname">Account Info.</label>\n';
                    var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button_current_balance">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonCurrentBalance()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var amount_paid_label   = '';
                    var date_label          = '';
                    var paid_through_label  = '';
                    var account_info_label  = '';
                    var note_label          = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field_current_balance" data-val="'+c+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRowCurrentBalance').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_current_balance_'+c+'">' +
                                                    '<input type="hidden" name="current_balance_id[]" value="-1">' +
                                                    '<input type="hidden" name="current_balance_account_id[]" value="-1">' +
                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Date *</label>\n' +
                                                        date_label +
                                                        '<input id="current_balance_payment_date" name="current_balance_payment_date[]" type="text" value="{{ date("d-m-Y") }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Amount Paid *</label>\n' +
                                                        amount_paid_label +
                                                        '<input type="text" name="current_balance_amount_paid[]" class="form-control currentBalancePaidAmount" id="current_balance_amount_paid_'+c+'" value="0" oninput="checkCurrentBalance('+c+')" required />\n' + 
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid Through *</label>\n' +
                                                        paid_through_label  +
                                                        '<select id="current_balance_paid_through_'+c+'" style="cursor: pointer" name="current_balance_paid_through[]" class="form-control single_select2">\n' +
                                                            '@if(!empty($paid_accounts) && ($paid_accounts->count() > 0))\n' +
                                                            '@foreach($paid_accounts as $key => $paid_account)\n' +
                                                                '<option value="{{ $paid_account['id'] }}">{{ $paid_account['HeadName'] }}</option>\n' +
                                                            '@endforeach\n' +
                                                            '@endif\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Account Info.</label>\n' +
                                                        account_info_label +
                                                        '<input type="text" name="current_balance_account_information[]" class="form-control" id="current_balance_account_information_'+c+'" placeholder="Account Information"/>\n' + 
                                                    '</div>\n' + 

                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                        note_label +
                                                        '<input type="text" name="current_balance_note[]" class="form-control" id="current_balance_note_'+c+'" placeholder="note"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +

                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
                }                                    
        });
        //For apending another rows end

        $(wrapper_current_balance).on("click",".remove_field_current_balance", function(e)
        {
            e.preventDefault();

            var c = $(this).attr("data-val");

            $('.di_current_balance_'+c).remove(); c--;

            calculateActualAmount(c);
        });
    </script>

    <script type="text/javascript">
        $(document).on("change", "#defaultCheck2" , function() {

            var checkbox_value    = $("#defaultCheck2")[0].checked;

            if (checkbox_value == true)
            {
                $("#vatShow").show();
                $("#taxShow").show();
            }
            else
            {
                $("#vatShow").hide();
                $("#taxShow").hide();
            }
        });

        function couponMembership()
        {
            var site_url      = $(".site_url").val();
            var coupon_code   = $("#coupon_code").val();
    
            $('.DiscountType').val(1);
            $('.DiscountAmount').val(0);

            $.get(site_url + '/invoices/search/coupon-code/'+ coupon_code, function(data_coupon){

                if (data_coupon == '')
                {
                    alert('Invalid Coupon Code or Membership Card !!');
                    $('#coupon_code').val('');
                }
                else
                {
                    var state = 0;

                    $('.productEntries').each(function()
                    {
                        var entry_id    = $(this).val();
                        var value_x     = $(this).prop("id");
                        
                        if (data_coupon[0].product_id != null)
                        {
                            for (var i = data_coupon.length - 1; i >= 0; i--)
                            {   
                                if (data_coupon[i].product_id == entry_id)
                                {
                                    var explode    = value_x.split('_');
                                    var di_id      = explode[2];

                                    $('#discount_type_'+di_id).val(data_coupon[i].discount_type);
                                    $('#discount_'+di_id).val(data_coupon[i].discount_amount);
                                    
                                    calculateActualAmount(di_id);

                                    state++;
                                }
                            }
                        }
                        else
                        {
                            var explode    = value_x.split('_');
                            var di_id      = explode[2];

                            $('#discount_type_'+di_id).val(data_coupon[0].discount_type);
                            $('#discount_'+di_id).val(data_coupon[0].discount_amount);
                            
                            calculateActualAmount(di_id);
                        }    
                    });
                }
            });
        }

        function calculateChangeAmount()
        {
            var tAmount             = $("#totalBdtShow").val();
            var balance             = $("#balance").val();
            var cGiven              = $("#cash_given").val();
            var type                = $("#bbBalance").val();

            if (tAmount != '')
            {
                var totalAmount     = $("#totalBdtShow").val();
            }
            else
            {
                var totalAmount     = 0;
            }


            if (cGiven != '')
            {
                var cashGiven       = $("#cash_given").val();
            }
            else
            {
                var cashGiven       = 0;
            }

            if (balance != '')
            {
                var balanceAmount   = $("#balance").val();
            }
            else
            {
                var balanceAmount   = 0;
            }

            var paidAmount  = parseFloat(cashGiven);

            if (type == 1)
            {   
                var payaAmount  = parseFloat(balanceAmount) + parseFloat(totalAmount);
            }
            else
            {
                var payaAmount  = parseFloat(totalAmount);
            }

            var changeAmount            = parseFloat(paidAmount) - parseFloat(payaAmount);
            var changeAmountOriginal    = parseFloat(paidAmount) - parseFloat(totalAmount);
            
            if (parseFloat(paidAmount) <= parseFloat(totalAmount))
            { 
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(paidAmount).toFixed());
                $("#current_balance_amount_paid_0").val(parseFloat(cashGiven).toFixed());
            }

            if ((parseFloat(paidAmount) > parseFloat(totalAmount)) && (parseFloat(paidAmount) <= parseFloat(payaAmount)))
            {   
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(totalAmount).toFixed());
                $("#current_balance_amount_paid_0").val(parseFloat(cashGiven).toFixed());
            }

            if (parseFloat(paidAmount) > parseFloat(payaAmount))
            {   
                $("#change_amount").val(parseFloat(changeAmount).toFixed());
                $("#amount_paid_0").val(parseFloat(totalAmount).toFixed());
                $("#current_balance_amount_paid_0").val(parseFloat(cashGiven).toFixed());
            }
        }
    </script>

    <script type="text/javascript">
        function printInvoiceList()
        {
            var site_url  = $('.site_url').val();

            $.get(site_url + '/invoices/print-invoices-list', function(data){

                var invoice_list = '';
                var sl_no        = 1;
                $.each(data, function(i, invoice_data)
                {
                    var serial              = parseFloat(i) + 1;
                    var site_url            = $('.site_url').val();
                    var print_url_pos       = site_url + '/invoices/show-pos/' + invoice_data.id;

                    if (invoice_data.customer_name != null)
                    {
                        var customer  = invoice_data.customer_name;
                    }
                    else
                    {
                        var customer  = invoice_data.contact_name;
                    }

                    invoice_list += '<tr>' +
                                        '<td>' +
                                            sl_no +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(invoice_data.invoice_date) +
                                        '</td>' +
                                        '<td>' +
                                            'INV - ' + invoice_data.invoice_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            customer + 
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.invoice_amount +
                                        '</td>' +
                                        '<td>' +
                                           (parseFloat(invoice_data.invoice_amount) - parseFloat(invoice_data.due_amount)) +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.due_amount +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.user_name +
                                        '</td>' +
                                        '<td>' +
                                            '<a href="'+ print_url_pos +'">' +
                                                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +'<i class="fa fa-print">' + '</i>' +
                                            '</a>' +
                                        '</td>' +
                                    '</tr>';

                                    sl_no++;
                });

                $("#print_invoice_list").empty();
                $("#print_invoice_list").append(invoice_list);
                
            });
        }

        function printInvoicesSearch()
        {
            var site_url            = $('.site_url').val();
            var date_val            = $('#search_date').val();
            var invoiceNumber_val   = $('#invoiceNumber').val();
            var customer_val        = $('#customer').val();

            if (date_val != '')
            {
                var date = $('#search_date').val();
            }
            else
            {
                var date  = 0;
            }

            if (invoiceNumber_val != '')
            {
                var invoiceNumber = $('#invoiceNumber').val();
            }
            else
            {
                var invoiceNumber  = 0;
            }

            if (customer_val != '')
            {
                var customer = $('#customer').val();
            }
            else
            {
                var customer  = 0;
            }

            $.get(site_url + '/invoices/print-invoices-search/' + date + '/' + customer + '/' + invoiceNumber , function(data){

                var invoice_list = '';
                var sl_no        = 1;
                $.each(data, function(i, invoice_data)
                {
                    var serial              = parseFloat(i) + 1;
                    var site_url            = $('.site_url').val();
                    var print_url_pos       = site_url + '/invoices/show-pos/' + invoice_data.id;

                    if (invoice_data.customer_name != null)
                    {
                        var customer  = invoice_data.customer_name;
                    }
                    else
                    {
                        var customer  = invoice_data.contact_name;
                    }

                    invoice_list += '<tr>' +
                                        '<td>' +
                                            sl_no +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(invoice_data.invoice_date) +
                                        '</td>' +
                                        '<td>' +
                                            'INV - ' + invoice_data.invoice_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            customer + 
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.invoice_amount +
                                        '</td>' +
                                        '<td>' +
                                           (parseFloat(invoice_data.invoice_amount) - parseFloat(invoice_data.due_amount)) +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.due_amount +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.user_name +
                                        '</td>' +
                                        '<td>' +
                                            '<a href="'+ print_url_pos +'">' +
                                                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +'<i class="fa fa-print">' + '</i>' +
                                            '</a>' +
                                        '</td>' +
                                    '</tr>';

                                    sl_no++;
                });

                $("#print_invoice_list").empty();
                $("#print_invoice_list").append(invoice_list);
                
            });
        }

        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        $(document).on("change", "#customer_id" , function() {

            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            $.get(site_url + '/invoices/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data.balance);

                if (data.type == 1)
                {
                    $("#bbShow").html('Receivable');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', false);
                }
                else
                {
                    $("#bbShow").html('Advance');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', true);
                }
            });
        });

        $(document).on("change", "#major_category_id" , function() {
            var site_url            = $('.site_url').val();
            var major_category_id   = $('#major_category_id').val();
            
            $("#product_entries_0").select2({
                ajax: { 
                url:  site_url + '/bills/product-list-load-bill/' + major_category_id,
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });

        function adjustAdvancePayment()
        {
            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            if ($('#adjustAdvancePayment').is(":checked"))
            {
                $("#adjustment").val(0);
                $("#customer_advance_amount").val(0);
                calculateChangeAmount();
            }
            else
            {
                $.get(site_url + '/invoices/adjust-advance-payment/' + customerId, function(data){

                    var payable  = $("#totalBdtShow").val();

                    if (parseFloat(payable) <= parseFloat(data))
                    {
                        $("#adjustment").val(parseFloat(payable).toFixed());
                    }
                    else
                    {
                        $("#adjustment").val(parseFloat(data).toFixed());
                    }

                    $("#customer_advance_amount").val(parseFloat(data));
                    calculateChangeAmount();
                });
            }
        }

        function checkBalance(id)
        {
            var total  = 0;
            $('.paidAmount').each(function()
            {
                total  += parseFloat($(this).val());
            });

            var payable_amount  = $("#totalBdt").val();
            var amount_paid     = $("#amount_paid_"+id).val();

            if (parseFloat(total) > parseFloat(payable_amount))
            {
                $("#amount_paid_"+id).val(0);
            }
        }

        function checkCurrentBalance(id)
        {
            var total  = 0;
            $('.currentBalancePaidAmount').each(function()
            {
                total  += parseFloat($(this).val());
            });

            var cash_given      = $("#cash_given").val();


            var amount_paid     = $("#current_balance_amount_paid_"+id).val();

            if (parseFloat(total) > parseFloat(cash_given))
            {
                $("#current_balance_amount_paid_"+id).val(0);
            }
        }
    </script>

    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true);
            $('#FormSubmit').submit();
        }
    </script>
@endsection