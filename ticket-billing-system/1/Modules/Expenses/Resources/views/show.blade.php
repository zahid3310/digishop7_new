@extends('layouts.app')

@section('title', 'Show')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Expense</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Expense</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">{{ $user_info['organization_name'] }}</h2>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_number'] }}</p>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-4 col-6">
                                        <address>
                                            <strong>Paid To:</strong><br>
                                            {{ $expense->customer_id != null ? $expense->customer->name : '' }}
                                            @if($expense->customer_id != null)
                                               <br> <?php echo $expense->customer_id != null ? $expense->customer->address : ''; ?> <br>
                                            @endif
                                            @if($expense->customer_id != null)
                                                <br>
                                            @endif
                                            {{ $expense->customer_id != null ? $expense->customer->phone : '' }}
                                        </address>
                                    </div>

                                    <div class="col-sm-4 hidden-xs">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div style="font-size: 16px" class="col-sm-4 col-6 text-sm-right">
                                        <address>
                                            <strong>Expense Date:</strong><br>
                                            {{ date('d-m-Y', strtotime($expense['expense_date'])) }}<br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6 hidden-xs">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Expense Summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Expense# {{ 'EXP - ' . str_pad($expense['expense_number'], 6, "0", STR_PAD_LEFT) }}</h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-nowrap">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th class="text-center">SL</th>
                                                <th class="text-left">Account Head</th>
                                                <th class="text-center">Paid Through</th>
                                                <th class="text-center">Account Information</th>
                                                <th class="text-left">Note</th>
                                                <th class="text-center">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">
                                            <tr>
                                                <td class="text-center">1</td>
                                                <td class="text-left">{{ $expense->account->account_name }}</td>
                                                <td class="text-center">{{ $expense->paidThroughAccount->account_name }}</td>
                                                <td class="text-center">{{ $expense->account_information }}</td>
                                                <td class="text-left">{{ $expense->note }}</td>
                                                <td class="text-right">{{ number_format($expense['amount'],2,'.',',') }}</td>
                                            </tr>
                                            
                                            <tr>
                                                <td style="font-size: 16px" colspan="5" class="text-right"><strong>Total</strong></td>
                                                <td style="font-size: 16px" class="text-right"><strong>{{ number_format($expense['amount'],2,'.',',') }}</strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection