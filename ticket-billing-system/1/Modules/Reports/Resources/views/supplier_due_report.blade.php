@extends('layouts.app')

@section('title', 'Agent/Airlines Ledger')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Agent/Airlines Ledger</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Agent/Airlines Ledger</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['organization_name'] }}</h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['address'] }}</p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['contact_number'] }}</p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">Agent/Airlines Ledger</h4>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><strong>From</strong> {{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }} <strong>To</strong> {{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="{{ route('due_report_supplier_index') }}" enctype="multipart/form-data">
                                    <div class="form-group row">
                                        <div style="margin-bottom: 10px;display: none" class="col-lg-3 col-md-3 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="{{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="{{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        @if(Auth()->user()->branch_id == 1)
                                        <div style="display: none" class="col-lg-2 col-md-2 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select style="width: 100%" id="branch_id" name="branch_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" onchange="getBranchCustomers()" required>
                                                   @if($branches->count() > 0)
                                                   @foreach($branches as $key => $value)
                                                   <option value="{{ $value['id'] }}" {{ $value->id == $branch_id ? 'selected' : '' }}>{{ $value['name'] }}</option>
                                                   @endforeach
                                                   @endif
                                                </select>
                                            </div>
                                        </div>
                                        @endif

                                        <div class="col-lg-3 col-md-3 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="customer_id" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="{{ $customer_name != null ? $customer_name['id'] : 0 }}" selected>{{ $customer_name != null ? $customer_name['ClientName'] : '--All Supplier--' }}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="ledger_type" style="width: 100" class="form-control select2" name="ledger_type">
                                                    <option value="0" selected>--Show All--</option>
                                                    <option {{ $ledger_type == 1 ? 'selected' : '' }} value="1">Dues Available</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center">Serial</th>
                                            <th style="text-align: center">Supplier</th>
                                            <th style="text-align: center">Address</th>
                                            <th style="text-align: center">Phone</th>
                                            <th style="text-align: center">Balance</th>
                                            <th style="text-align: center" class="d-print-none">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                            $i = 1;
                                        ?>
                                        @foreach($data as $key => $value)
                                        <tr>
                                            <td style="text-align: center;">{{ $i }}</td>
                                            <td style="text-align: left;">{{ $value['customer_name'] }} </td>
                                            <td style="text-align: left;">{{ $value['address'] }}</td>
                                            <td style="text-align: center;">{{ $value['phone'] }}</td>
                                            <td style="text-align: right;">{{ number_format($value['balance'],0,'.',',') }}</td>
                                            <td style="text-align: center;" class="d-print-none">
                                                <div class="dropdown">
                                                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                        <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" style="">
                                                        <a style="cursor: pointer" class="dropdown-item" data-toggle="modal" data-target="#myModal_{{$value['customer_id']}}" >Details</a>
                                                        <a style="cursor: pointer" class="dropdown-item" data-toggle="modal" data-target="#myModal1_{{$value['customer_id']}}" >Paid To Agen/Airlines</a>
                                                        <a style="cursor: pointer" class="dropdown-item" data-toggle="modal" data-target="#myModal2_{{$value['customer_id']}}" onclick="mrList({{$value['customer_id']}})">List of MR</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                         
                                        <?php $i++; ?>

                                        <div id="myModal_{{$value['customer_id']}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title mt-0" id="myModalLabel">Select Date Range</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <form method="get" action="{{ route('due_report_supplier_details', $value['customer_id']) }}" enctype="multipart/form-data" target="_blank">

                                                        <div style="padding-top: 0px !important" class="modal-body">
                                                            <div style="margin-bottom: 0px !important" class="form-group row">
                                                                <label for="example-text-input" class="col-md-12 col-form-label">From Date *</label>
                                                                <div class="col-md-12">
                                                                    <input style="cursor: pointer" id="from_date" name="from_date" type="date" class="form-control" value="<?= date("2020-01-01") ?>" required>
                                                                </div>
                                                            </div>

                                                            <div style="margin-bottom: 0px !important" class="form-group row">
                                                                <label for="example-text-input" class="col-md-12 col-form-label">To Date *</label>
                                                                <div class="col-md-12">
                                                                    <input style="cursor: pointer" id="to_date" name="to_date" type="date" class="form-control" value="<?= date("Y-m-d") ?>" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Search</button>
                                                            <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="myModal1_{{$value['customer_id']}}" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-xl">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title mt-0" id="myModalLabel">Make New Payment</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <div class="modal-body">
                                                        <input id="customer_id_payment_{{$value['customer_id']}}" name="customer_id" type="hidden" value="{{$value['customer_id']}}">

                                                        <div class="form-group row">
                                                            <label for="productname" class="col-md-2 col-form-label">Payment Date *</label>
                                                            <div class="col-md-2">
                                                                <input style="cursor: pointer" id="payment_date_{{$value['customer_id']}}" name="payment_date" type="date" value="<?= date("Y-m-d") ?>" class="form-control" required>
                                                            </div>

                                                            <label for="productname" style="text-align: right" class="col-md-2 col-form-label">Note</label>
                                                            <div class="col-md-6">
                                                                <input id="payment_note_{{$value['customer_id']}}" name="payment_note" type="text" class="form-control" placeholder="Enter Note">
                                                            </div>
                                                        </div>

                                                        <hr style="margin-top: 0px !important;">

                                                        <div class="inner-repeater">
                                                            <div style="margin-bottom: 0px !important" data-repeater-list="inner-group" class="inner form-group row">
                                                                <div class="inner col-lg-12 input_fields_wrap_payment getMultipleRowPayment">
                                                                    <div style="margin-bottom: 0px !important;" class="row di_payment_0">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row justify-content-end">
                                                                <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                                                    <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br>

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary waves-effect waves-light" onclick="makePayment({{$value['customer_id']}})">Make Payment</button>
                                                            <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="myModal2_{{$value['customer_id']}}" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-xl">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title mt-0" id="myModalLabel">Print MR</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                                                    <label for="productname" class="col-md-3 col-form-label">Date</label>
                                                                    <div class="col-md-7">
                                                                        <input style="cursor: pointer" id="search_date_{{$value['customer_id']}}" type="date" value="<?= date("Y-m-d") ?>" class="form-control">
                                                                    </div>

                                                                    <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-2 col-md-2 col-sm-2 col-2" onclick="mrList({{$value['customer_id']}})">
                                                                        <i class="bx bx-search font-size-24"></i>
                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <input type="hidden" id="cst_id_{{$value['customer_id']}}" value="{{ $value['customer_id'] }}">
                                                        </div>

                                                        <hr style="margin-top: 0px !important;">

                                                        <div class="row">
                                                            <div class="col-md-1" style="text-align: center;border-bottom: 1px solid #E5E5E5;padding: 5px">SL</div>
                                                            <div class="col-md-2" style="text-align: center;border-bottom: 1px solid #E5E5E5;padding: 5px">Date</div>
                                                            <div class="col-md-3" style="text-align: center;border-bottom: 1px solid #E5E5E5;padding: 5px">Agent/Airlines</div>
                                                            <div class="col-md-2" style="text-align: center;border-bottom: 1px solid #E5E5E5;padding: 5px">Amount</div>
                                                            <div class="col-md-4" style="text-align: center;border-bottom: 1px solid #E5E5E5;padding: 5px">Creator</div>
                                                        </div>

                                                        <div class="row mr_list">
                                                        </div>

                                                        <br>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                        <tr>
                                            <th colspan="4" style="text-align: right;">TOTAL</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_balance,0,'.',',') }}</th>
                                            <th colspan="1" style="text-align: right;" class="d-print-none"></th>
                                        </tr>

                                    </tbody>
                                    
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        
        var site_url    = $('.site_url').val();
        var branch_ids  = $("#branch_id").val();

        $('#add_field_button_payment').click();

        $("#customer_id").select2({
            ajax: { 
            url:  site_url + '/bills/supplier/list/bill',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                return result['text'];
            },
        });
    });

    function pad (str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }
    
    function getBranchCustomers()
    {
        var site_url    = $('.site_url').val();
        var branch_id  = $("#branch_id").val();
         
        $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/reports/branch-contacts/get-contacts/' + branch_id,
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },
    
                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 1 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
        });
    }
</script>

<script type="text/javascript">
    var max_fields_payment       = 50;                              //maximum input boxes allowed
    var wrapper_payment          = $(".input_fields_wrap_payment"); //Fields wrapper
    var add_button_payment       = $(".add_field_button_payment");  //Add button ID
    var index_no_payment         = 1;

    //For apending another rows start
    var x = -1;
    $(add_button_payment).click(function(e)
    {
        e.preventDefault();

        // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

        if(x < max_fields_payment)
        {   
            x++;

            var serial = x + 1;

            if (serial == 1)
            {
                var amount_paid_label   = '<label class="hidden-xs" for="productname">Amount Paid *</label>\n';
                var paid_through_label  = '<label class="hidden-xs" for="productname">Paid Through *</label>\n';
                var account_info_label  = '<label class="hidden-xs" for="productname">Account Information</label>\n';
                var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';
            }
            else
            {
                var amount_paid_label   = '';
                var paid_through_label  = '';
                var account_info_label  = '';
                var note_label          = '';
                var action_label        = '';
            }

            $('.getMultipleRowPayment').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_payment_'+x+'">' +
                                                '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                    '<label style="display: none" class="show-xs" for="productname">Amount Paid *</label>\n' +
                                                    amount_paid_label +
                                                    '<input type="number" name="amount_paid[]" class="inner form-control amountPaid" id="amount_paid_'+x+'" value="0" required />\n' + 
                                                '</div>\n' +

                                                '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6 form-group">\n' +
                                                    '<label style="display: none" class="show-xs" for="productname">Paid Through *</label>\n' +
                                                    paid_through_label  +
                                                    '<select id="paid_through_'+x+'" style="cursor: pointer" name="paid_through[]" class="form-control paidThrough">\n' +
                                                        '@if(!empty($paid_accounts) && ($paid_accounts->count() > 0))\n' +
                                                        '@foreach($paid_accounts as $key => $paid_account)\n' +
                                                            '<option value="{{ $paid_account['id'] }}">{{ $paid_account['HeadName'] }}</option>\n' +
                                                        '@endforeach\n' +
                                                        '@endif\n' +
                                                    '</select>\n' +
                                                '</div>\n' +

                                                '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Account Information</label>\n' +
                                                    account_info_label +
                                                    '<input type="text" name="account_information[]" class="form-control" id="account_information_'+x+'" placeholder="Account Information"/>\n' + 
                                                '</div>\n' + 

                                                '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6 form-group">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                    note_label +
                                                    '<input type="text" name="note[]" class="inner form-control" id="note_'+x+'" placeholder="note"/>\n' + 
                                                '</div>\n' + 
                                                
                                                '<div style="" class="col-lg-1 col-md-2 col-sm-6 col-6 form-group remove_field_payment" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                    action_label +
                                                    '<input type="button" class="btn btn-primary btn-block inner" value="Delete"/>\n' + 
                                                '</div>\n' + 
                                            '</div>\n' 
                                        );

                                        $('.single_select2').select2();
            }                                    
    });
    //For apending another rows end

    $(wrapper_payment).on("click",".remove_field_payment", function(e)
    {
        e.preventDefault();

        var x = $(this).attr("data-val");

        $('.di_payment_'+x).remove(); x--;
    });

    function makePayment(x) 
    {
        var customer_id   = $("#customer_id_payment_"+x).val();
        var payment_date  = $("#payment_date_"+x).val();
        var payment_note  = $("#payment_note_"+x).val();
        var site_url      = $('.site_url').val();

        var amount_paid = [];
        $('input[name^="amount_paid"]').each(function() {
            amount_paid.push(this.value);
        })

        var paid_through = [];
        $('.paidThrough').each(function() {
            paid_through.push(this.value);
        })

        var note = [];
        $('input[name^="note"]').each(function() {
            note.push(this.value);
        })

        var account_information = [];
        $('input[name^="account_information"]').each(function() {
            account_information.push(this.value);
        })

        if (customer_id == '' || payment_date == '')
        {
            $('#mandatoryAlert').removeClass('hidden');
        }
        else
        {
            $.ajax({
                type:   'post',
                url:    site_url + '/airlinestickets/store-supplier-payment',
                data:   { customer_id : customer_id, payment_date : payment_date, amount_paid : amount_paid, paid_through : paid_through, note : note, account_information : account_information, payment_note:payment_note, _token: '{{csrf_token()}}' },

                success: function (data) {
                    if(data != 0)
                    {
                        $('#CloseButton1').click();
                        $('.remove_field_payment').click();

                        alert('Payment Successfull !');

                        location.reload();
                    }
                    else
                    {
                        alert('Please Try Again !');
                    }
                }
            });
        }
    }

    function mrList(x)
    {
        var site_url            = $('.site_url').val();
        var search_by_date      = $('#search_date_'+x).val();
        var search_by_customer  = $('#cst_id_'+x).val();

        $.get(site_url + '/airlinestickets/print-mr-list?search_by_date='+ search_by_date + '&search_by_customer=' + search_by_customer, function(data){

            var mr_list = '';
            var sl_no   = 1;
            $.each(data, function(i, mr_data)
            {
                var serial              = parseFloat(i) + 1;
                var site_url            = $('.site_url').val();
                var print_url_pos       = site_url + '/airlinestickets/print-mr/' + mr_data.id;

                mr_list +=  '<div class="col-md-1" style="text-align: center;border-bottom: 1px solid #E5E5E5;padding: 5px">'+ sl_no +'</div>' +
                            '<div class="col-md-2" style="text-align: center;border-bottom: 1px solid #E5E5E5;padding: 5px">'+ formatDate(mr_data.VoucherDate) +'</div>' +
                            '<div class="col-md-3" style="text-align: left;border-bottom: 1px solid #E5E5E5;padding: 5px">'+ mr_data.customer_name +'</div>' +
                            '<div class="col-md-2" style="text-align: center;border-bottom: 1px solid #E5E5E5;padding: 5px">'+ mr_data.TotalAmount +'</div>' +
                            '<div class="col-md-4" style="text-align: left;border-bottom: 1px solid #E5E5E5;padding: 5px">'+ mr_data.user_name +'</div>';
                
                            sl_no++;
            });

            $(".mr_list").empty();
            $(".mr_list").append(mr_list);    
        });
    }

    function formatDate(date)
    {
        var d       = new Date(date),
            month   = '' + (d.getMonth() + 1),
            day     = '' + d.getDate(),
            year    = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day, month, year].join('-');
    }
</script>
@endsection