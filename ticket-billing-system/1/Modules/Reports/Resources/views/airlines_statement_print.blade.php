<!DOCTYPE html>
<html>

<head>
    <title>Agent/Airlines Statement</title>
    <link rel="icon" href="{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">
</head>

<style type="text/css">
    @page {
        size: auto;   
        margin: 15mm 5mm 5mm 10mm;
    }

    #print-footer {
        display: none;
        position: fixed;
        bottom: 0;
        left:0;
    }

    #hide-print {
        display: none;
    }

    #print-footer {
        display: none;
    }

    body {
        margin-bottom:10mm;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #ddd;
    }

    .table td, .table th {
        font-size: 12px !important;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Agent/Airlines Statement</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                    <th style="text-align: center">Agent/Airlines Name</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                    <td style="text-align: center">
                                        @if($customer_name != null)
                                            {{ $customer_name['ClientName'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">SL No</th>
                                    <th style="text-align: center;width: 15%">Agent/Airlines Name</th>
                                    <th style="text-align: center;width: 10%">Issue Date</th>
                                    <th style="text-align: center;width: 15%">Pax Name</th>
                                    <th style="text-align: center;width: 10%">PNR</th>
                                    <th style="text-align: center;width: 10%">Carrier Name</th>
                                    <th style="text-align: center;width: 10%">Net Fare</th>
                                    <!-- <th style="text-align: center;width: 10%">Received Amount</th> -->
                                </tr>
                            </thead>
                            
                                <tbody class="theight">

                                <?php
                                    $i                  = 1;
                                    $total_amount       = 0;
                                ?>
                                @foreach($data as $key => $value)
                               
                                <tr>
                                    <td style="text-align: center;vertical-align: middle;" rowspan="{{ count($value) }}">{{ $i }}</td>
                                    <td style="text-align: left;vertical-align: middle;" rowspan="{{ count($value) }}">{{$value[0]->airline->ClientName}}</td>
                                    <td style="text-align: center;vertical-align: middle;" rowspan="{{ count($value) }}">{{ date('d-m-Y', strtotime($value[0]['issue_date'])) }}</td>
                                    <td style="text-align: left;">{{$value[0]['pax_name']}}</td>
                                    <td style="text-align: center;">{{$value[0]['pnr']}}</td>
                                    <td style="text-align: center;">{{$value[0]['carrier_name']}}</td>
                                    <td style="text-align: right;vertical-align: middle;" rowspan="{{ count($value) }}">{{$value[0]['airline_fare']}}</td>
                                    <!-- <td style="text-align: right;vertical-align: middle;" rowspan="{{ count($value) }}">{{$value[0]['cash_given']}}</td> -->
                                </tr>
                                
                                <?php
                                    $total_amount    = $total_amount + $value[0]['airline_fare'];
                                ?>

                                @foreach($value as $key1 => $value1)
                                @if(($key1 != 0))
                                <tr>
                                    <td style="text-align: left;">{{$value1['pax_name']}}</td>
                                    <td style="text-align: left;">{{$value1['pnr']}}</td>
                                    <td style="text-align: left;">{{$value1['carrier_name']}}</td>
                                </tr>
                                @endif
                                @endforeach
                                 
                                <?php
                                    $total_quantity      = 0;
                                    $i++;
                                ?>
                                @endforeach
                            </tbody>

                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script> -->
<!-- <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script> -->
<!-- <script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script> -->

</body>
</html>