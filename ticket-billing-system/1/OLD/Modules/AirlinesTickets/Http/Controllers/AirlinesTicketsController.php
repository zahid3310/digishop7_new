<?php

namespace Modules\AirlinesTickets\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\UnitConversions;
use App\Models\Users;
use App\Models\BranchInventories;
use App\Models\Expenses;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\SalesTicket;

use App\Models\AccountHead;
use App\Models\Projects;
use App\Models\Clients;
use App\Models\VoucherSummary;
use App\Models\AccountTransaction;
use App\Models\BankAccounts;
use App\Models\ChequeTransactions;
use App\Models\Registers;

use App\Models\TaxAit;
use Response;
use DB;
use View;
use Carbon\Carbon;

class AirlinesTicketsController extends Controller
{
    public function index()
    {
        $data = SalesTicket::all();
        return view('airlinestickets::index',compact('data'));
    }

    public function create()
    {
        $branch_id      = Auth()->user()->branch_id;
        $airlines       = Clients::where('IsTransactable', 1)
                                        ->where('ActiveStatus', 1)
                                        ->where('PtnID', '264')
                                        ->orderBy('PtnGroupCode', 'ASC')
                                        ->get();

        $paid_accounts  = AccountHead::where('IsTransactable', 1)
                                        ->where('ActiveStatus', 1)
                                        ->where('CompanyID', $branch_id)
                                        ->whereIn('PtnID', ['227', '228'])
                                        ->orderBy('PtnGroupCode', 'ASC')
                                        ->get();

        return view('airlinestickets::create',compact('airlines', 'paid_accounts'));
    }

    public function store(Request $all_store_data)
    {
        $user_id    = Auth()->user()->id;
        $branch_id  = Auth()->user()->branch_id;
        $data       = $all_store_data->all();
    
        DB::beginTransaction();

        try{
            $data_find    = SalesTicket::orderBy('created_at', 'DESC')->first();
            $sales_number = $data_find != null ? $data_find['sales_number'] + 1 : 1;

            $sales_ticket                            = new SalesTicket;
            $sales_ticket->sales_number              = $sales_number;
            $sales_ticket->sales_date                = date('Y-m-d');
            $sales_ticket->issue_date                = date('Y-m-d', strtotime($data['issue_date']));
            $sales_ticket->issue_by                  = $data['issue_by'];
            $sales_ticket->customer_id               = $data['customer_id'];
            $sales_ticket->total_discount_type       = $data['total_discount_type'];
            $sales_ticket->total_discount_amount     = $data['total_discount_amount'];
            $sales_ticket->gross_fare                = $data['total_gross_fare'];
            $sales_ticket->net_fare                  = $data['sub_total_amount'];
            $sales_ticket->airlines_fare             = $data['total_airline_fare'];
            $sales_ticket->cash_given                = $data['cash_given'];
            $sales_ticket->due_amount                = $data['sub_total_amount'];
            $sales_ticket->previous_due              = $data['previous_due'];
            $sales_ticket->previous_due_type         = $data['balance_type'];
            $sales_ticket->branch_id                 = $branch_id;
            $sales_ticket->created_by                = $user_id;

            if ($sales_ticket->save())
            {
                foreach ($data['pax_name'] as $key => $value)
                {
                    $sales_ticket_entries[] = [
                        'sales_id'          => $sales_ticket->id,
                        'pax_name'          => $data['pax_name'][$key],
                        'eticket_no'        => $data['eticket_no'][$key],
                        'pnr'               => $data['pnr'][$key],
                        'passport_number'   => $data['passport_number'][$key],
                        'sector'            => $data['sector'][$key],
                        'carrier_name'      => $data['carrier_name'][$key],
                        'eq_fare'           => $data['eq_fare'][$key],
                        'tax'               => $data['tax'][$key],
                        'net_fare'          => $data['net_fare'][$key],
                        'gross_fare'        => $data['gross_fare'][$key],
                        'airline_fare'      => $data['airline_fare'][$key],
                        'airlines_id'       => $data['ariline_id'][$key],
                        'commission'        => $data['commission'][$key],
                        'ait_tax'           => $data['ait_tax'][$key],
                        'fly_date'          => date('Y-m-d', strtotime($data['fly_date'][$key])),
                        'customer_id'       => $data['customer_id'],
                        'branch_id'         => $branch_id,
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('sales_ticket_entries')->insert($sales_ticket_entries);

                //Financial Accounting Sales Start
                    accountTransactionsDebitStoreInventory($VoucherId = null, 
                            $VoucherDate        = date('Y-m-d'), 
                            $VoucherNumber      = null, 
                            $ProjectID          = 0, 
                            $RegisterID         = $data['customer_id'], 
                            $HeadID             = 1247, 
                            $PurposeAccHeadName = null, 
                            $Particulars        = '', 
                            $DebitAmount        = $data['sub_total_amount'], 
                            $CreditAmount       = '0.00', 
                            $AccountNumber      = null, 
                            $VoucherType        = 'SALES', 
                            $CBAccount          = 1189, 
                            $IsPosted           = 1, 
                            $CompanyID          = $branch_id, 
                            $Status             = 1, 
                            $created_by         = $user_id,
                            $invoice_id         = $sales_ticket->id,
                            $bill_id            = null,
                            $sales_return_id    = null,
                            $purchase_return_id = null
                        );

                    accountTransactionsCreditStoreInventory($VoucherId = null, 
                            $VoucherDate        = date('Y-m-d'), 
                            $VoucherNumber      = null, 
                            $ProjectID          = 0, 
                            $RegisterID         = $data['customer_id'], 
                            $HeadID             = 1189, 
                            $PurposeAccHeadName = null, 
                            $Particulars        = '', 
                            $DebitAmount        = '0.00', 
                            $CreditAmount       = $data['sub_total_amount'], 
                            $AccountNumber      = null, 
                            $VoucherType        = 'SALES', 
                            $CBAccount          = 1247, 
                            $IsPosted           = 1, 
                            $CompanyID          = $branch_id, 
                            $Status             = 1, 
                            $created_by         = $user_id,
                            $invoice_id         = $sales_ticket->id,
                            $bill_id            = null,
                            $sales_return_id    = null,
                            $purchase_return_id = null
                        );

                    //Insert into journal_entries 
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {
                                $accountHeadType = AccountHead::find($data['current_balance_paid_through'][$i]);
                                if($accountHeadType['PurposeHeadName'] == 'cash_ac')
                                {
                                    $type           = 'CR';
                                    $voucherNumber  = $this->gets_voucherNumber_name($branch_id, $type);
                                    $storeSummary   = voucherSummaryCreate($VoucherNumber = $voucherNumber, 
                                            $VoucherDate          = date('Y-m-d'), 
                                            $ProjectID            = 0, 
                                            $RegisterID           = 0, 
                                            $Type                 = $type, 
                                            $BankAccountNumber    = 0, 
                                            $Status               = 1, 
                                            $TotalAmount          = $data['current_balance_amount_paid'][$i], 
                                            $Narration            = $data['current_balance_note'][$i], 
                                            $MoneyReceiptNo       = null, 
                                            $CompanyID            = $branch_id, 
                                            $created_by           = $user_id
                                        );
                    
                                    if ($storeSummary > 0)
                                    {   
                                        //For Credit
                                        accountTransactionsCreditStoreInventory($VoucherId = $storeSummary, 
                                                $VoucherDate          = date('Y-m-d'), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['customer_id'], 
                                                $HeadID               = 1247, 
                                                $PurposeAccHeadName   = 'others', 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = '0.00', 
                                                $CreditAmount         = $data['current_balance_amount_paid'][$i], 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = $data['current_balance_paid_through'][$i], 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $created_by           = $user_id,
                                                $invoice_id           = $sales_ticket->id,
                                                $bill_id              = null,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );

                                        //For Debit
                                        accountTransactionsDebitStoreInventory($VoucherId = $storeSummary, 
                                                $VoucherDate          = date('Y-m-d'), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['customer_id'], 
                                                $HeadID               = $data['current_balance_paid_through'][$i], 
                                                $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = $data['current_balance_amount_paid'][$i], 
                                                $CreditAmount         = '0.00', 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = 1247, 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $created_by           = $user_id,
                                                $invoice_id           = $sales_ticket->id,
                                                $bill_id              = null,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );
                                    }
                                }

                                if($accountHeadType['PurposeHeadName'] == 'bank_ac')
                                {
                                    $type           = 'BR';
                                    $voucherNumber  = $this->gets_voucherNumber_name($branch_id, $type);
                                    $storeSummary   = voucherSummaryCreate($VoucherNumber = $voucherNumber, 
                                            $VoucherDate          = date('Y-m-d'), 
                                            $ProjectID            = 0, 
                                            $RegisterID           = 0, 
                                            $Type                 = $type, 
                                            $BankAccountNumber    = $data['current_balance_paid_through'][$i], 
                                            $Status               = 1, 
                                            $TotalAmount          = $data['current_balance_amount_paid'][$i], 
                                            $Narration            = $data['current_balance_note'][$i], 
                                            $MoneyReceiptNo       = null, 
                                            $CompanyID            = $branch_id, 
                                            $created_by           = $user_id
                                        );
                    
                                    if ($storeSummary > 0)
                                    {   
                                        //For Credit
                                        accountTransactionsCreditStoreInventory($VoucherId = $storeSummary, 
                                                $VoucherDate          = date('Y-m-d'), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['customer_id'], 
                                                $HeadID               = 1247, 
                                                $PurposeAccHeadName   = 'others', 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = '0.00', 
                                                $CreditAmount         = $data['current_balance_amount_paid'][$i], 
                                                $AccountNumber        = $data['current_balance_paid_through'][$i], 
                                                $VoucherType          = $type, 
                                                $CBAccount            = $data['current_balance_paid_through'][$i], 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $created_by           = $user_id,
                                                $invoice_id           = $sales_ticket->id,
                                                $bill_id              = null,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );

                                        //For Debit
                                        accountTransactionsDebitStoreInventory($VoucherId = $storeSummary, 
                                                $VoucherDate          = date('Y-m-d'), 
                                                $VoucherNumber        = $voucherNumber, 
                                                $ProjectID            = 0, 
                                                $RegisterID           = $data['customer_id'], 
                                                $HeadID               = $data['current_balance_paid_through'][$i], 
                                                $PurposeAccHeadName   = $accountHeadType['PurposeHeadName'], 
                                                $Particulars          = $data['current_balance_note'][$i], 
                                                $DebitAmount          = $data['current_balance_amount_paid'][$i], 
                                                $CreditAmount         = '0.00', 
                                                $AccountNumber        = null, 
                                                $VoucherType          = $type, 
                                                $CBAccount            = 1247, 
                                                $IsPosted             = 1, 
                                                $CompanyID            = $branch_id, 
                                                $Status               = 1, 
                                                $created_by           = $user_id,
                                                $invoice_id           = $sales_ticket->id,
                                                $bill_id              = null,
                                                $sales_return_id      = null,
                                                $purchase_return_id   = null
                                            );
                                    }
                                }
                            }
                        }
                    }

                    balanceUpdate($data['customer_id']);
                //Financial Accounting Sales End

                //Financial Accounting Purchase Start
                    foreach ($data['pax_name'] as $key1 => $value1)
                    {
                        accountTransactionsCreditStoreInventory($VoucherId = null, 
                                $VoucherDate        = date('Y-m-d'), 
                                $VoucherNumber      = null, 
                                $ProjectID          = 0, 
                                $RegisterID         = $data['ariline_id'][$key1], 
                                $HeadID             = 1248, 
                                $PurposeAccHeadName = null, 
                                $Particulars        = '', 
                                $DebitAmount        = '0.00', 
                                $CreditAmount       = $data['total_airline_fare'], 
                                $AccountNumber      = null, 
                                $VoucherType        = 'PURCHASE', 
                                $CBAccount          = 1322, 
                                $IsPosted           = 1, 
                                $CompanyID          = $branch_id, 
                                $Status             = 1, 
                                $created_by         = $user_id,
                                $invoice_id         = $sales_ticket->id,
                                $bill_id            = null,
                                $sales_return_id    = null,
                                $purchase_return_id = null
                            );

                        accountTransactionsDebitStoreInventory($VoucherId = null, 
                                $VoucherDate        = date('Y-m-d'), 
                                $VoucherNumber      = null, 
                                $ProjectID          = 0, 
                                $RegisterID         = $data['ariline_id'][$key1], 
                                $HeadID             = 1322, 
                                $PurposeAccHeadName = null, 
                                $Particulars        = '', 
                                $DebitAmount        = $data['total_airline_fare'], 
                                $CreditAmount       = '0.00', 
                                $AccountNumber      = null, 
                                $VoucherType        = 'PURCHASE', 
                                $CBAccount          = 1248, 
                                $IsPosted           = 1, 
                                $CompanyID          = $branch_id, 
                                $Status             = 1, 
                                $created_by         = $user_id,
                                $invoice_id         = $sales_ticket->id,
                                $bill_id            = null,
                                $sales_return_id    = null,
                                $purchase_return_id = null
                            );

                        balanceUpdate($data['ariline_id'][$key1]);
                    }
                //Financial Accounting Purchase End

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Sales Created Successfully !!");
                }
                else
                {
                    $user_info  = userDetails();

                    if ($user_info['pos_printer'] == 0)
                    {
                        return redirect()->route('invoices_show_pos', $invoice->id);
                    }
                    else
                    {
                        return redirect()->route('invoices_show', $invoice->id);
                    }
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();

            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }
    
    public function show($id)
    {
        $data = SalesTicket::find($id);
        return view('airlinestickets::show',compact('data'));
    }
    
    public function clientsList()
    {
        $branch_id      = Auth()->user()->branch_id;

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Clients::orderBy('tbl_clients.created_at', 'DESC')
                                    ->where('tbl_clients.PtnID', 265)
                                    ->select('tbl_clients.*')
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Clients::where('tbl_clients.PtnID', 265)
                                    ->where('tbl_clients.ClientName', 'LIKE', "$search%")
                                    ->orWhere('tbl_clients.ClientContactNumber', 'LIKE', "$search%")
                                    ->orderBy('tbl_clients.created_at', 'DESC')
                                    ->select('tbl_clients.*')
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "ClientContactNumber" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['ClientName'], "ClientContactNumber"=>$value['ClientContactNumber'] != null ? $value['ClientContactNumber'] : '');

            $i++;
        }
   
        return Response::json($data);
    }
    
    public function updateTaxAIT()
    {
        $data = DB::table('tax_ait')->first();
        return view('airlinestickets::edit_tax_ait',compact('data'));
    }
    
    public function updateTaxAITsubmit(Request $request)
    {
        
        $data = $request->all();
        
        $tax_ait = TaxAit::find($data['id']);
        
        $tax_ait->tax_amount            = $data['tax_amount'];
        $tax_ait->tax_percentage        = $data['tax_percentage'];
        $tax_ait->ait_amont             = $data['ait_amont'];
        $tax_ait->ait_percentage        = $data['ait_percentage'];
        
        if ($tax_ait->save())
        {   
            return back()->with("success","Update Successfully !!");
        }else
        {
            return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
        }    
    }

    public function taxAit()
    {
        $data = DB::table('tax_ait')->first();

        return Response::json($data);
    }

    ##Common Codes Start##
        public function gets_accountsHead_names($branch_id)
        {
            $result = AccountHead::where('IsTransactable', 1)
                                    ->where('ActiveStatus', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->orderBy('PtnGroupCode', 'ASC')
                                    ->get();

            return $result;
        }

        public function gets_projects_name($branch_id)
        {
            $result = Projects::where('IsTransactable', 1)
                                    ->where('ActiveStatus', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->orderBy('PtnGroupCode', 'ASC')
                                    ->get();

            return $result;
        }

        public function gets_register_names($branch_id)
        {
            $result = Clients::where('IsTransactable', 1)
                                    ->where('ActiveStatus', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->orderBy('PtnGroupCode', 'ASC')
                                    ->get();

            return $result;
        }

        public function gets_bank_accounts($branch_id)
        {
            $result = AccountHead::where('IsTransactable', 1)
                                    ->where('ActiveStatus', 1)
                                    ->where('CompanyID', $branch_id)
                                    ->where('PurposeHeadName', 'bank_ac')
                                    ->orderBy('PtnGroupCode', 'ASC')
                                    ->get();

            return $result;
        }

        public function gets_voucherNumber_name($branch_id, $type) 
        {
            $voucher        = VoucherSummary::where('Type', $type)->orderBy('id', 'DESC')->first();
            $voucherNumber  = $voucher != null ? $voucher['VoucherNumber'] + 1 : 1;

            return $voucherNumber;
        }

        public function getBankBalance($bank_id)
        {
            $balance = cachInBank($bank_id);

            return Response::json($balance);
        }

        public function getCashBalance()
        {
            $cash_in_hand = cachInHand();

            return Response::json($cash_in_hand);
        }
    ##Common Codes End##
}
