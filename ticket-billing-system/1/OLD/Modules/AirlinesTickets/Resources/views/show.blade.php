@extends('layouts.app')

@section('title', 'Show')

<style>
    table,th {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 2px;
    }

    td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        padding: 2px;
    }

    @page {
        size: A4;
        page-break-after: always;
    }
</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="padding: 10px;padding-top: 25px" class="row">
                    <div style="padding-bottom: 30px" class="d-print-none col-md-12">
                        <div class="float-right">
                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <img  src="{{ Url('public/showheader.jpg') }}" style="width: 100%; height: 270px;">
                                </div>

                                <hr>

                                <div>
                                    <h4 style="text-align: center;"><u><strong>MONEY RECEIPT(Customer copy)</strong></u></h4>
                                </div>

                                <div class="row">
                                    <div style="font-size: 15px" class="col-md-8">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Receipt No : </strong> {{ '2022'.str_pad($data['sales_number'], 6, "0", STR_PAD_LEFT) }} <br>
                                            <span style="font-weight: bold">Dated :   </span>  {{ date('d-m-Y', strtotime($data['issue_date'])) }}
                                        </address>
                                    </div>
                                    <div style="font-size: 15px;" class="col-md-4 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <address style="margin-bottom: 0px !important">
                                                <strong>Issued By :  </strong> {{ $data['issue_by'] }} <br>
                                                <span style="font-weight: bold">Created By :   </span>  {{$data->CreatedBy->name}}
                                            </address>
                                        </address>
                                    </div>
                                   
                                </div>

                                <div style="padding-top: 10px;padding-bottom: 20px; border:2px solid;margin-top: 15px;">
                                   <div style="padding: 5px;font-size: 14px;">
                                        <span>Received with thanks from &nbsp;&nbsp;&nbsp;<strong> {{$data->customer->ClientName}}</strong></span>  <br>
                                        <address>Address : &nbsp;&nbsp;&nbsp; {{$data->customer->ClientAddress}}</address>
                                        <span>the sum of taka in words </span> &nbsp;&nbsp;&nbsp; <strong>{{numberTowords($data->net_fare)}}</strong> <br>
                                        <span>By Cheque on Dated : 04 Sep 2022</span>
                                        <span>CQ IBBL 686570 Deposited To SOUTH EAST BANK From 48 NAZIM KHAN AIR</span><br><br>
                                        <span>Current Balance &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp; </span> <strong>{{$data->net_fare}}</strong> <br>
                                        <span><strong>Payment Amount</strong> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</span> <strong>{{$data->cash_given}}</strong> <br>
                                        <span>Balance Due &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</span> <strong>{{ $data->net_fare - $data->cash_given }}</strong>
                                   </div>
                                </div>

                                <br>
                                <br>
                                

                                <div class="row" style="padding-top: 5px;">
                                    <div class="col-md-6">
                                         <?php
                                           use Carbon\Carbon;
                                           $date = Carbon::now();
                                         ?>
                                        <h6 style="text-align: left;font-size: 16px;"> <span>{{ date('d-m-Y') }} </span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <span>{{$date->format('h:i:s a')}} </span> </h6>
                                    </div>
                                    <div class="col-md-6">
                                        <h6 style="text-align: right;font-size: 16px;"> <span>Received By</span> </h6>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <img  src="{{ Url('public/showheader.jpg') }}" style="width: 100%; height: 270px;">
                                </div>

                                <hr>

                                <div>
                                    <h4 style="text-align: center;"><u><strong>MONEY RECEIPT(Customer copy)</strong></u></h4>
                                </div>

                                <div class="row">
                                    <div style="font-size: 15px" class="col-md-8">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Receipt No : </strong> {{ '2022'.str_pad($data['sales_number'], 6, "0", STR_PAD_LEFT) }} <br>
                                            <span style="font-weight: bold">Dated :   </span>  {{ date('d-m-Y', strtotime($data['issue_date'])) }}
                                        </address>
                                    </div>
                                    <div style="font-size: 15px;" class="col-md-4 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <address style="margin-bottom: 0px !important">
                                                <strong>Issued By :  </strong> {{ $data['issue_by'] }} <br>
                                                <span style="font-weight: bold">Created By :   </span>  {{$data->CreatedBy->name}}
                                            </address>
                                        </address>
                                    </div>
                                   
                                </div>

                                <div style="padding-top: 10px;padding-bottom: 20px; border:2px solid;margin-top: 15px;">
                                   <div style="padding: 5px;font-size: 14px;">
                                        <span>Received with thanks from &nbsp;&nbsp;&nbsp;<strong> {{$data->customer->ClientName}}</strong></span>  <br>
                                        <address>Address : &nbsp;&nbsp;&nbsp; {{$data->customer->ClientAddress}}</address>
                                        <span>the sum of taka in words </span> &nbsp;&nbsp;&nbsp; <strong>{{numberTowords($data->net_fare)}}</strong> <br>
                                        <span>By Cheque on Dated : 04 Sep 2022</span>
                                        <span>CQ IBBL 686570 Deposited To SOUTH EAST BANK From 48 NAZIM KHAN AIR</span><br><br>
                                        <span>Current Balance &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp; </span> <strong>{{$data->net_fare}}</strong> <br>
                                        <span><strong>Payment Amount</strong> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</span> <strong>{{$data->cash_given}}</strong> <br>
                                        <span>Balance Due &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</span> <strong>{{ $data->net_fare - $data->cash_given }}</strong>
                                        

                                   </div>
                                </div>

                                <br>
                                <br>
                                

                                <div class="row" style="padding-top: 5px;">
                                    <div class="col-md-6">
                                         <?php
                                           $date = Carbon::now();
                                         ?>
                                        <h6 style="text-align: left;font-size: 16px;"> <span>{{ date('d-m-Y') }} </span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <span>{{$date->format('h:i:s a')}} </span> </h6>
                                    </div>
                                    <div class="col-md-6">
                                        <h6 style="text-align: right;font-size: 16px;"> <span>Received By</span> </h6>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection