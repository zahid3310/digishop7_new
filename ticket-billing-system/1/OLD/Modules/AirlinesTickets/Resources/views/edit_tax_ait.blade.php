@extends('layouts.app')

@section('title', 'Update Tax & AIT')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Update Tax & AIT</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Tax & AIT</a></li>
                                    <li class="breadcrumb-item active">Update Tax & AIT</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('update_tax_ait_submit') }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label for="productname">Tax Amount *</label>
                                        <input type="hidden" name="id" class="inner form-control" id="id" value="{{$data->id}}">
                                        <input type="text" name="tax_amount" class="inner form-control" id="tax_amount" value="{{$data->tax_amount}}">
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <label for="productname">Tax Discount *</label>
                                        <input type="text" name="tax_percentage" class="inner form-control" id="tax_amount" value="{{$data->tax_percentage}}">
                                    </div>                                    
                                    
                                    <div class="col-md-3">
                                        <label for="productname">AIT Amount *</label>
                                        <input type="text" name="ait_amont" class="inner form-control" id="ait_amont" value="{{$data->ait_amont}}">
                                    </div>                                    
                                    
                                    <div class="col-md-3">
                                        <label for="productname">AIT Discount *</label>
                                        <input type="text" name="ait_percentage" class="inner form-control" id="ait_percentage" value="{{$data->ait_percentage}}">
                                    </div>
                                    
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Messages</h4>

                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Tax</th>
                                            <th>Tax %</th>
                                            <th>AIT</th>
                                            <th>AIT %</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                            <tr>
                                                <td>{{$data->tax_amount}}</td>
                                                <td>{{$data->tax_percentage}}</td>
                                                <td>{{$data->ait_amont}}</td>
                                                <td>{{$data->ait_percentage}}</td>
                                            </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{ url('public/admin_panel_assets/editor/ckeditor.js')}}"></script>
@endsection