@extends('layouts.app')

@section('title', 'Voucher Posting')

<style type="text/css">
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>

@section('content')
    <div class="overlay"></div>

    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Orders</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">New Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->
                
                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div class="row">
                                <div style="margin-top: -10px !important;margin-bottom: -20px !important;padding-left: 35px" class="card-body col-md-4">
                                    <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                        <h6 style="padding: 10px;padding-bottom: 0px;color: black;">Control Area</h6>
                                        
                                        <div class="col-md-12">
                                            <hr style="margin-top: 0px !important">

                                            <div style="margin-bottom: 0.1rem !important" class="form-group row">
                                                <label class="col-md-4 col-form-label">Status</label>
                                                <div class="col-md-8">
                                                    <select style="cursor: pointer" id="status_type" class="form-control" name="status">
                                                        <option value="0" selected>Unposted</option>
                                                        <option value="1">Posted</option>
                                                        <option value="2">All</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div style="margin-bottom: 0.1rem !important" class="form-group row">
                                                <label class="col-md-4 col-form-label">Entry By</label>
                                                <div class="col-md-8">
                                                    <select style="cursor: pointer" id="entry_by_searech" class="form-control" name="entry_by">
                                                        <option value="0">--Select--</option>
                                                        @if($data['userList']->count() > 0)
                                                        @foreach($data['userList'] as $key => $user_val)
                                                        <option value="{{ $user_val->id }}">{{ $user_val->name }}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>

                                            <div style="margin-bottom: 0.1rem !important" class="form-group row">
                                                <label class="col-md-4 col-form-label">Type</label>
                                                <div class="col-md-8">
                                                    <select style="cursor: pointer" id="type" class="form-control" name="type">
                                                        <option value="0">--Select--</option>
                                                        <option value="BP">BANK PAYMENT</option>
                                                        <option value="BR">BANK RECEIPT</option>
                                                        <option value="CP">CASH PAYMENT</option>
                                                        <option value="CR">CASH RECEIPT</option>
                                                        <option value="CN">CONTRA</option>
                                                        <option value="JR">JOURNAL</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div style="margin-bottom: 0.1rem !important" class="form-group row">
                                                <label class="col-md-4 col-form-label">Date From</label>
                                                <div class="col-md-8">
                                                    <input id="date_from" name="date_from" type="text" placeholder="01-01-2022" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                                </div>
                                            </div>

                                            <div style="margin-bottom: 0.1rem !important" class="form-group row">
                                                <label class="col-md-4 col-form-label">Date To</label>
                                                <div class="col-md-8">
                                                    <input id="date_to" name="date_to" type="text" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" placeholder="{{ date('d-m-Y') }}">
                                                </div>
                                            </div>

                                            <div style="margin-bottom: 0.1rem !important" class="form-group row">
                                                <label class="col-md-4 col-form-label"></label>
                                                <div class="col-md-8">
                                                    <button style="width: 100%" class="btn btn-success" onclick="searchVoucherList()">Search</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12" style="max-height: 255px;overflow-y: auto;overflow-x: auto">

                                            <hr>

                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th id="select-all" style="width: 4%;padding: 0.2rem !important" class="active">
                                                            <input id="selectAll" type="checkbox" class="select-all" onclick="checkVal()" />
                                                        </th>
                                                        <th style="width: 6%;font-size: 12px;padding: 0.2rem !important" class="success">Status</th>
                                                        <th style="width: 15%;font-size: 12px;padding: 0.2rem !important" class="success">Voucher#</th>
                                                        <th style="width: 25%;font-size: 12px;padding: 0.2rem !important" class="warning">Date</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="voucherList">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: -10px !important;margin-bottom: -20px !important;padding-right: 35px" class="card-body col-md-8">
                                    @if(Session::has('success'))
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        {!! Session::get('success') !!}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    @endif

                                    @if(Session::has('unsuccess'))
                                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                        {!! Session::get('unsuccess') !!}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    @endif

                                    @if(Session::has('errors'))
                                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                        {!! 'Some required fields are missing..!! Please try again..' !!}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    @endif

                                    <form id="FormSubmit" action="{{ route('voucher_posting_approve') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                        
                                    {{ csrf_field() }}

                                    <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                        <div class="col-md-7">
                                            <div style="margin-bottom: 5px" class="form-group row">
                                                <label for="productname" class="col-md-3 col-form-label">Voucher#</label>
                                                <div class="col-md-9">
                                                    <input id="voucher_number" name="voucher_number" type="text" class="form-control" readonly>
                                                    <input id="voucher_id" name="voucher_id" type="hidden">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-5">
                                            <div style="margin-bottom: 5px" class="form-group row">
                                                <label for="productname" class="col-md-5 col-form-label">Voucher Date</label>
                                                <div class="col-md-7">
                                                    <input id="voucher_date" name="voucher_date" type="text" class="form-control" readonly>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div style="margin-bottom: 5px" class="form-group row">
                                                <label for="productname" class="col-md-2 col-form-label">Entry By</label>
                                                <div class="col-md-10">
                                                    <input id="entry_by" name="entry_by" type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="margin-top: 5px" class="row">
                                        <div style="background-color: #FFD4A3;height: 30px;padding-top: 5px" class="col-md-12">
                                            <h5 style="text-align: center">VOUCHER EVALUTION FOR POSTING</h5>
                                        </div>
     
                                        <div style="background-color: #D2D2D2;height: 350px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 15%;font-size: 12px">A/C HEAD NAME</th>
                                                        <th style="width: 15%;font-size: 12px">REGISTER NAME</th>
                                                        <th style="width: 15%;font-size: 12px">PROJECT NAME</th>
                                                        <th style="width: 15%;font-size: 12px">PARTICULARS</th>
                                                        <th style="width: 15%;font-size: 12px">DEBIT AMOUNT</th>
                                                        <th style="width: 15%;font-size: 12px">CREDIT AMOUNT</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="voucher_details"></tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div style="margin-top: 5px !important;padding-bottom: 0px !important" class="row">
                                        <div style="background-color: #F4F4F7;height: 60px;padding-top: 13px" class="col-md-12">
                                            <div class="form-group row">
                                                <div class="button-items col-lg-12 ">
                                                    <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">POST</button>
                                                    <a id="voucherEdit" target="_blank">
                                                        <button style="border-radius: 0px !important" type="button" class="btn btn-danger waves-effect waves-light">EDIT</button>
                                                    </a>
                                                    <a id="voucherUnpost">
                                                        <button style="border-radius: 0px !important" type="button" class="btn btn-primary waves-effect waves-light">UNPOST</button>
                                                    </a>
                                                    <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('voucher_posting_index') }}">Close</a></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="loader"></div> -->

    <div id="myModal2" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Print Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label for="productname" class="col-md-4 col-form-label">Date</label>
                                <div class="col-md-8">
                                    <input style="cursor: pointer" id="search_date" type="date" value="<?= date("Y-m-d") ?>" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Customer </label>
                                <div class="col-md-8">
                                    <input id="customer" type="text" class="form-control"  placeholder="Enter Customer Name">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Search Invoice </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <input style="width: 95%" type="text" class="form-control col-lg-9 col-md-9 col-sm-9 col-9" id="invoiceNumber" placeholder="Enter Invoice ID">
                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="printInvoicesSearch()">
                                            <i class="bx bx-search font-size-24"></i>
                                        </span>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Order#</th>
                                <th>Customer</th>
                                <th>Amount</th>
                                <th>Paid</th>
                                <th>Due</th>
                                <th>Creator</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody id="print_invoice_list">
                        </tbody>
                    </table>
                </div>
                
                <div class="modal-footer">
                    <button id="CloseButton2" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url    = $('.site_url').val();
            var VoucherType = $('#status_type').val();
            var entryBy     = $('#entry_by_searech').val();
            var type        = $('#type').val();
            var date_from   = $('#date_from').val();
            var date_to     = $('#date_to').val();

            if (date_from == '') 
            {
                var fromDate = '01-01-2020';
            }
            else
            {
                var fromDate = $('#date_from').val();
            }

            if (date_to == '') 
            {
                var toDates = new Date();
                var toDate  = toDates.getDate() + "-" + (toDates.getMonth()+1) + "-" + toDates.getFullYear();
            }
            else
            {
                var toDate = $('#date_to').val();
            }

            $.get(site_url + '/voucherPosting/voucher-list/'+ VoucherType + '/' + entryBy + '/' + type + '/' + fromDate + '/' + toDate, function(data){

                var list        = '';
                var totalItems  = 0;
                $.each(data, function(i, data_list)
                {
                    if (data_list.Status == 0)
                    {
                        var statusSign = '<i style="color:red;" class="fa fa-ban"></i>';
                    }
                    else
                    {
                        var statusSign = '<i style="color:red;" class="fa fa-check"></i>';
                    }

                    list += '<tr onclick="getVoucherDetails('+data_list.id+')">' +
                                '<td style="width: 4%;padding: 0.2rem !important" class="active">' +
                                    '<input id="checkbox_'+i+'" type="checkbox" class="select-item checkbox" onclick="checkValUnik()" />' +
                                    '<input id="check_status_'+i+'" type="hidden" name="check_box_id[]" class="checkboxId" value="0" />' +
                                '</td>' +
                                '<td style="width: 6%;font-size: 12px;padding: 0.2rem !important" class="success">' +
                                    statusSign +
                                '</td>' +
                                '<td style="width: 15%;font-size: 12px;padding: 0.2rem !important" class="success">'+ data_list.Type + '/' + data_list.VoucherNumber +'</td>' + 
                                '<td style="width: 25%;font-size: 12px;padding: 0.2rem !important" class="warning">'+ data_list.VoucherDate +'</td>' +
                            '</tr>';

                    totalItems++;
                });

                $("#voucherList").empty();
                $("#voucherList").append(list);
                $("#voucherList").append('<tr>' +
                                            '<td style="width: 50%" colspan="3">' +
                                                '<button style="width: 100%; font-size: 12px;padding: 0.2rem 0.75rem !important" class="btn btn-success" type="submit"><i class="fa fa-paper-plane"></i> POST ALL</button>' +
                                            '</td>' +
                                            '<td style="width: 25%;font-size: 12px;text-align: right;font-weight: bold" class="success">'+ 'Total Items' + ' &nbsp;&nbsp;&nbsp;' + totalItems +'</td>' + 
                                        '</tr>');
            });

            $('#add_field_button').click();
        });

        function searchVoucherList()
        {
            $("body").addClass("loading"); 

            var site_url    = $('.site_url').val();
            var VoucherType = $('#status_type').val();
            var entryBy     = $('#entry_by_searech').val();
            var type        = $('#type').val();
            var date_from   = $('#date_from').val();
            var date_to     = $('#date_to').val();

            if (date_from == '') 
            {
                var fromDate = '01-01-2020';
            }
            else
            {
                var fromDate = $('#date_from').val();
            }

            if (date_to == '') 
            {
                var toDate = {!! date('d-m-Y') !!};
            }
            else
            {
                var toDate = $('#date_to').val();
            }

            $.get(site_url + '/voucherPosting/voucher-list/'+ VoucherType + '/' + entryBy + '/' + type + '/' + fromDate + '/' + toDate, function(data){

                var list        = '';
                var totalItems  = 0;
                $.each(data, function(i, data_list)
                {
                    if (data_list.Status == 0)
                    {
                        var statusSign  = '<i style="color:red;" class="fa fa-ban" onclick="getVoucherDetails('+data_list.id+')"></i>';
                        var checkBox    =   '<input id="checkbox_'+i+'" type="checkbox" class="select-item checkbox" onclick="checkValUnik()" />' +
                                            '<input id="check_status_'+i+'" type="hidden" name="check_box_id[]" class="checkboxId" value="0" />';
                    }
                    else
                    {
                        var statusSign  = '<i style="color:red;" class="fa fa-check" onclick="getVoucherDetails('+data_list.id+')"></i>';
                        var checkBox    =  '';
                    }

                    list += '<tr>' +
                                '<td style="width: 4%;padding: 0.2rem !important" class="active">' +
                                    checkBox +
                                '</td>' +
                                '<td style="width: 6%;font-size: 12px;padding: 0.2rem !important" class="success">' +
                                    statusSign +
                                '</td>' +
                                '<td style="width: 15%;font-size: 12px;padding: 0.2rem !important" class="success">'+ data_list.Type + '/' + data_list.VoucherNumber +'</td>' + 
                                '<td style="width: 25%;font-size: 12px;padding: 0.2rem !important" class="warning">'+ data_list.VoucherDate +'</td>' +
                            '</tr>';

                    totalItems++;
                });

                $("#voucherList").empty();
                $("#voucherList").append(list);
                $("#voucherList").append('<tr>' +
                                            '<td style="width: 50%" colspan="3">' +
                                                '<button style="width: 100%; font-size: 12px;padding: 0.2rem 0.75rem !important" class="btn btn-success" type="submit"><i class="fa fa-paper-plane"></i> POST ALL</button>' +
                                            '</td>' +
                                            '<td style="width: 25%;font-size: 12px;text-align: right;font-weight: bold" class="success">'+ 'Total Items' + ' &nbsp;&nbsp;&nbsp;' + totalItems +'</td>' + 
                                        '</tr>');

                $("body").removeClass("loading");
            });
        }
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }
    </script>

    <script type="text/javascript">
        function getVoucherDetails(VoucherId)
        {
            $("body").addClass("loading"); 

            var site_url = $(".site_url").val();

            $.get(site_url + '/voucherPosting/voucher-details/'+ VoucherId, function(data){

                $("#voucher_number").val(data.summary.type + '/' + data.summary.voucher_number);
                $("#voucher_id").val(data.summary.id);
                $("#voucher_date").val(data.summary.voucher_date);
                $("#entry_by").val(data.summary.entry_by);

                if (data.summary.status == 0)
                {   
                    if(data.summary.type == 'CP')
                    {
                        $("#voucherEdit").attr("href", site_url + '/voucherPosting/cashPaymentVoucherEdit/' + data.summary.id);
                    }
                    
                    if(data.summary.type == 'CR')
                    {
                        $("#voucherEdit").attr("href", site_url + '/voucherPosting/cashReceiptVoucherEdit/' + data.summary.id);
                    }

                    if(data.summary.type == 'BP')
                    {
                        $("#voucherEdit").attr("href", site_url + '/voucherPosting/bankPaymentVoucherEdit/' + data.summary.id);
                    }

                    if(data.summary.type == 'BR')
                    {
                        $("#voucherEdit").attr("href", site_url + '/voucherPosting/bankReceiptVoucherEdit/' + data.summary.id);
                    }

                    if(data.summary.type == 'JR')
                    {
                        $("#voucherEdit").attr("href", site_url + '/voucherPosting/journalVoucherEdit/' + data.summary.id);
                    }

                    if(data.summary.type == 'CN')
                    {
                        $("#voucherEdit").attr("href", site_url + '/voucherPosting/contraVoucherEdit/' + data.summary.id);
                    }
                }
                else
                {
                    $("#voucherEdit").removeAttr("href", "");
                }

                $("#voucherUnpost").attr("href", site_url + '/voucherPosting/posting-unapprove/' + data.summary.id);

                var list    = '';
                $.each(data.details, function(i, data_list)
                {
                    if(data_list.particular == null)
                    {
                        var particular = '';
                    }
                    else
                    {
                        var particular = data_list.particular;
                    }

                    list += '<tr style="table-layout:fixed;">' +
                                '<td style="width: 15%;font-size: 12px;padding: 0px">' + 
                                    '<input style="width: 100%" class="form-control" type="text" value="'+data_list.account_head_name+'" />' + 
                                '</td>' +
                                '<td style="width: 15%;font-size: 12px;padding: 0px">'+ 
                                    '<input style="width: 100%" class="form-control" type="text" value="'+data_list.register_name+'" />' +
                                '</td>' +
                                '<td style="width: 15%;font-size: 12px;padding: 0px">' +
                                    '<input style="width: 100%" class="form-control" type="text" value="'+data_list.project_name+'" />' + 
                                 '</td>' +
                                '<td style="width: 15%;font-size: 12px;padding: 0px">' + 
                                    '<input style="width: 100%" class="form-control" type="text" value="'+particular+'" />' + 
                                '</td>' +
                                '<td style="width: 15%;font-size: 12px;padding: 0px">' + 
                                    '<input style="width: 100%" class="form-control" type="text" value="'+data_list.debit_amount+'" />' + 
                                '</td>' +
                                '<td style="width: 15%;font-size: 12px;padding: 0px">' + 
                                    '<input style="width: 100%" class="form-control" type="text" value="'+data_list.credit_amount+'" />' + 
                                '</td>' +
                            '</tr>';
                });

                $("#voucher_details").empty();
                $("#voucher_details").append(list);

                $("body").removeClass("loading");
            });
        }
    </script>

    <script type="text/javascript">
        function checkVal()
        {
            if ($('#selectAll').is(":checked"))
            {
                $('.checkbox').prop('checked', true);
                $('.checkboxId').val(1);
            }
            else
            {
                $('.checkbox').prop('checked', false);
                $('.checkboxId').val(0);
            }
        }

        function checkValUnik(i)
        {
            if ($('#checkbox_'+i).is(":checked") == true)
            {
                $('#check_status_'+i).val(1);
            }
            else
            {
                $('#check_status_'+i).val(0);
            }

        }
    </script>
@endsection