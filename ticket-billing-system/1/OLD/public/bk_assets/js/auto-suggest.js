$(function() {
    $( "#from_date" ).datepicker();
    $( "#to_date" ).datepicker();
});
$(document).ready(function() { 
	$('.catetory-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="categoryName"]').val(self);
	});
	$('.ItemCode-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="ItemCode"]').val(self);
	});
	$('.ItemName-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="ItemName"]').val(self);
	});
	$('.StaffName-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="StaffName"]').val(self);
	});
	$('.DamageId-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="DamageId"]').val(self);
	});
	$('.DamageSR-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="DamageSR"]').val(self);
	});
	$('.supplierName-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="supplierName"]').val(self);
	});
	$('.customerName-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="customerName"]').val(self);
	});
	$('.pOrderId-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="pOrderId"]').val(self);
	});
	$('.SalesId-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="SalesId"]').val(self);
	});
	$('.PurchaseReturn-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="PurchaseReturn"]').val(self);
	});
	$('.SaleReturn-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="SaleReturn"]').val(self);
	});
	$('.voucherNo-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="voucherNo"]').val(self);
	});
	$('.BankAccount-select').on('change', function() {
		
		var self = $(this).find(":selected").text();
		$('input[name="BankAccount"]').val(self);
	});
	$('.acHeadName-select').on('change', function() {
		
		var self = $(this).find(":selected").text();
		$('input[name="acHeadName"]').val(self);
	});
	$('.acRegisterName-select').on('change', function() {
		
		var self = $(this).find(":selected").text();
		$('input[name="acRegisterName"]').val(self);
	});
	$('.chqRunStatus-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="chqRunStatus"]').val(self);
	});
	$('.PaymentReceipt-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="PaymentReceipt"]').val(self);
	});
	$('.voucherType-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="voucherType"]').val(self);
	});
	$('.RegisterName-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="RegisterName"]').val(self);
	});
	$('.accountHead-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('.accountHead').val(self);
	});
	/*$('.BankAccount-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="BankAccount"]').val(self);
	});*/
	$('.chtype-select').on('change', function() {
		
		var self = $(this).find(":selected").val();
		$('input[name="chtype"]').val(self);
	});
	$('.ChqStateChange-select').on('change', function() {
		var self = $(this).find(":selected").val();
		$('input[name="ChqStateChange"]').val(self);
	});
	$('.sampleId-select').on('change', function() {
		var self = $(this).find(":selected").val();
		$('input[name="sampleId"]').val(self);
	});
	$('.soldby-select').on('change', function() {
		var self = $(this).find(":selected").val();
		$('input[name="soldby"]').val(self);
	});
});

function CategoryName(inputString){
	if(inputString.length == 0) {
		$('#categorySuggestions').fadeOut(); //#categorySuggestions don't have any css
	} else {
	$('.category-search').addClass('load');
		$.post(baseurl+"report/autoSuggest/categoryNameAuto", {queryString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#categorySuggestions').fadeIn();
				$('#categorySuggestionsList').html(data);
				$('.category-search').removeClass('load');
			}
		});
	}
}

function fillCategory(thisValue) {
	$('.category-search').val(thisValue);
	setTimeout("$('#categorySuggestions').fadeOut();", 600);
}

function itemCode(inputString){
	if(inputString.length == 0) {
		$('#itemcodeSuggestions').fadeOut();
	} else {
	$('.itemcode-search').addClass('load');
		$.post(baseurl+"report/autoSuggest/itemCodeAuto", {queryString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#itemcodeSuggestions').fadeIn();
				$('#itemcodeSuggestionsList').html(data);
				$('.itemcode-search').removeClass('load');
			}
		});
	}
}

function fillItemcode(thisValue) {
	$('.itemcode-search').val(thisValue);
	setTimeout("$('#itemcodeSuggestions').fadeOut();", 600);
}

function itemName(inputString){
	if(inputString.length == 0) {
		$('#itemnameSuggestions').fadeOut();
	} else {
	$('.itemname-search').addClass('load');
		$.post(baseurl+"report/autoSuggest/itemNameAuto", {queryString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#itemnameSuggestions').fadeIn();
				$('#itemnameSuggestionsList').html(data);
				$('.itemname-search').removeClass('load');
			}
		});
	}
}

function fillItemName(thisValue) {
	$('.itemname-search').val(thisValue);
	setTimeout("$('#itemnameSuggestions').fadeOut();", 600);
}

function staffName(inputString){
	if(inputString.length == 0) {
		$('#StaffNameSuggestions').fadeOut();
	} else {
	$('.staffname-search').addClass('load');
		$.post(baseurl+"report/autoSuggest/StaffNameNameAuto", {queryString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#StaffNameSuggestions').fadeIn();
				$('#StaffNameSuggestionsList').html(data);
				$('.staffname-search').removeClass('load');
			}
		});
	}
}

function fillStaffName(thisValue) {
	$('.staffname-search').val(thisValue);
	setTimeout("$('#StaffNameSuggestions').fadeOut();", 600);
}


function damageId(inputString){
	if(inputString.length == 0) {
		$('#damageIdSuggestions').fadeOut();
	} else {
	$('.damageid-search').addClass('load');
		$.post(baseurl+"report/autoSuggest/damageidAuto", {queryString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#damageIdSuggestions').fadeIn();
				$('#damageIdSuggestionsList').html(data);
				$('.damageid-search').removeClass('load');
			}
		});
	}
}

function fillDamageId(thisValue) {
	$('.damageid-search').val(thisValue);
	setTimeout("$('#damageIdSuggestions').fadeOut();", 600);
}

function supplierN(inputString){
	if(inputString.length == 0) {
		$('#supplierNameSuggestions').fadeOut();
	} else {
	$('.supplierName-search').addClass('load');
		$.post(baseurl+"report/autoSuggest/supplierNameAuto", {queryString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#supplierNameSuggestions').fadeIn();
				$('#supplierNameSuggestionsList').html(data);
				$('.supplierName-search').removeClass('load');
			}
		});
	}
}

function fillsupplierName(thisValue) {
	$('.supplierName-search').val(thisValue);
	setTimeout("$('#supplierNameSuggestions').fadeOut();", 600);
}

function customerN(inputString){
	if(inputString.length == 0) {
		$('#customerNameSuggestions').fadeOut();
	} else {
	$('.customerName-search').addClass('load');
		$.post(baseurl+"report/autoSuggest/customerNameAuto", {queryString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#customerNameSuggestions').fadeIn();
				$('#customerNameSuggestionsList').html(data);
				$('.customerName-search').removeClass('load');
			}
		});
	}
}

function fillcustomerName(thisValue) {
	$('.customerName-search').val(thisValue);
	setTimeout("$('#customerNameSuggestions').fadeOut();", 600);
}

function purchaseId(inputString){
	if(inputString.length == 0) {
		$('#pOrderIdSuggestions').fadeOut();
	} else {
	$('.pOrderId-search').addClass('load');
		$.post(baseurl+"report/autoSuggest/pOrderIdAuto", {queryString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#pOrderIdSuggestions').fadeIn();
				$('#pOrderIdSuggestionsList').html(data);
				$('.pOrderId-search').removeClass('load');
			}
		});
	}
}

function fillpOrderId(thisValue) {
	$('.pOrderId-search').val(thisValue);
	setTimeout("$('#pOrderIdSuggestions').fadeOut();", 600);
}

function salesId(inputString){
	if(inputString.length == 0) {
		$('#SalesIdSuggestions').fadeOut();
	} else {
	$('.SalesId-search').addClass('load');
		$.post(baseurl+"report/autoSuggest/SalesIdAuto", {queryString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#SalesIdSuggestions').fadeIn();
				$('#SalesIdSuggestionsList').html(data);
				$('.SalesId-search').removeClass('load');
			}
		});
	}
}

function fillSalesId(thisValue) {
	$('.SalesId-search').val(thisValue);
	setTimeout("$('#SalesIdSuggestions').fadeOut();", 600);
}

function voucherno(inputString){
	if(inputString.length == 0) {
		$('#voucherNoSuggestions').fadeOut();
	} else {
	$('.voucherNo-search').addClass('load');
		$.post(baseurl+"report/autoSuggest/voucherNoAuto", {queryString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#voucherNoSuggestions').fadeIn();
				$('#voucherNoSuggestionsList').html(data);
				$('.voucherNo-search').removeClass('load');
			}
		});
	}
}

function fillvoucherNo(thisValue) {
	$('.voucherNo-search').val(thisValue);
	setTimeout("$('#voucherNoSuggestions').fadeOut();", 600);
}

function bankAccount(inputString){
	if(inputString.length == 0) {
		$('#BankAccountSuggestions').fadeOut();
	} else {
	$('.BankAccount-search').addClass('load');
		$.post(baseurl+"report/autoSuggest/BankAccountAuto", {queryString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#BankAccountSuggestions').fadeIn();
				$('#BankAccountSuggestionsList').html(data);
				$('.BankAccount-search').removeClass('load');
			}
		});
	}
}

function fillBankAccount(thisValue) {
	$('.BankAccount-search').val(thisValue);
	setTimeout("$('#BankAccountSuggestions').fadeOut();", 600);
}

function acheadname(inputString){
	if(inputString.length == 0) {
		$('#acHeadNameSuggestions').fadeOut();
	} else {
	$('.acHeadName-search').addClass('load');
		$.post(baseurl+"report/autoSuggest/acHeadNameAuto", {queryString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#acHeadNameSuggestions').fadeIn();
				$('#acHeadNameSuggestionsList').html(data);
				$('.acHeadName-search').removeClass('load');
			}
		});
	}
}

function fillacHeadName(thisValue) {
	$('.acHeadName-search').val(thisValue);
	setTimeout("$('#acHeadNameSuggestions').fadeOut();", 600);
}

function acregistername(inputString){
	if(inputString.length == 0) {
		$('#acRegisterNameSuggestions').fadeOut();
	} else {
	$('.acRegisterName-search').addClass('load');
		$.post(baseurl+"report/autoSuggest/acRegisterNameAuto", {queryString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#acRegisterNameSuggestions').fadeIn();
				$('#acRegisterNameSuggestionsList').html(data);
				$('.acRegisterName-search').removeClass('load');
			}
		});
	}
}

function fillacRegisterName(thisValue) {
	$('.acRegisterName-search').val(thisValue);
	setTimeout("$('#acRegisterNameSuggestions').fadeOut();", 600);
}

function sampleid(inputString){
	var CompanyID = $("#CompanyID").val();
	//alert(CompanyID);
	if(inputString.length == 0) {
		$('#sampleIdSuggestions').fadeOut();
	} else {
	$('.sampleId-search').addClass('load');
		$.post(baseurl+"report/autoSuggest/sampleIdAuto?1=1&CompanyID="+CompanyID, {queryString: ""+inputString+""}, function(data){
			if(data.length >0) {
				$('#sampleIdSuggestions').fadeIn();
				$('#sampleIdSuggestionsList').html(data);
				$('.sampleId-search').removeClass('load');
			}
		});
	}
}

function fillsampleId(thisValue) {
	$('.fillsampleId-search').val(thisValue);
	setTimeout("$('#fillsampleIdSuggestions').fadeOut();", 600);
}