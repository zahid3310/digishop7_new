<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('accountsreport/cash-book')->group(function() {
    Route::get('/', 'AccountsReportController@cashBookIndex')->name('accounts_report_cash_book_index');
    Route::get('/print', 'AccountsReportController@cashBookPrint')->name('accounts_report_cash_book_print');
    Route::get('/print-api', 'AccountsReportController@cashBookPrintApi');
});

Route::prefix('accountsreport/bank-book')->group(function() {
    Route::get('/', 'AccountsReportController@bankBookIndex')->name('accounts_report_bank_book_index');
    Route::get('/print', 'AccountsReportController@bankBookPrint')->name('accounts_report_bank_book_print');
});

Route::prefix('accountsreport/current-balance')->group(function() {
    Route::get('/', 'AccountsReportController@currentBalanceIndex')->name('accounts_report_current_balance_index');
    Route::get('/api', 'AccountsReportController@currentBalanceIndexApi');
});

Route::prefix('accountsreport/ledger-book')->group(function() {
    Route::get('/', 'AccountsReportController@ledgerBookIndex')->name('accounts_report_ledger_book_index');
    Route::get('/print', 'AccountsReportController@ledgerBookPrint')->name('accounts_report_ledger_book_print');
});

Route::prefix('accountsreport/select2-api')->group(function() {
    Route::get('/get-bank-accounts-api', 'AccountsReportController@getsBankAccounts');
    Route::get('/get-registers-customers-api', 'AccountsReportController@getsRegistersCustomers');
    Route::get('/get-transactable-accounts-api', 'AccountsReportController@getsTransactableAccounts');
});

Route::prefix('accountsreport/accounts-voucher')->group(function() {
    Route::get('/', 'AccountsReportController@accountsVoucherIndex')->name('accounts_report_accounts_voucher_index');
    Route::get('/print', 'AccountsReportController@accountsVoucherPrint')->name('accounts_report_accounts_voucher_print');
    Route::get('/print-api', 'AccountsReportController@accountsVoucherPrintApi');
});