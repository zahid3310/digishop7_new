<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('airlinestickets')->group(function() {
    Route::get('/', 'AirlinesTicketsController@index');
    Route::get('/ticket-sales', 'AirlinesTicketsController@create')->name('sales_of_ticket_create');
    Route::post('/ticket-sales-store', 'AirlinesTicketsController@store')->name('sales_ticket_store');
    Route::get('/tax-Ait/', 'AirlinesTicketsController@taxAit');
});
