<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class Clients extends Model
{  
    protected $table = "tbl_clients";

    public function dueLedger()
    {
        return $this->hasOne(BillSummary::class, 'customer_id')
                    ->select('customer_id',
                        DB::raw('sum(amount) as receivable'),
                        DB::raw('sum(amount_paid) as received'),
                        DB::raw('sum(amount_due) as dues')
                    )
                    ->groupBy('customer_id');
    }
}
