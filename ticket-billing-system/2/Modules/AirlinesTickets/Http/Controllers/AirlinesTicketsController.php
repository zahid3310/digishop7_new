<?php

namespace Modules\AirlinesTickets\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\UnitConversions;
use App\Models\Users;
use App\Models\BranchInventories;
use App\Models\Expenses;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\SalesTicket;
use App\Models\Clients;
use App\Models\TaxAit;
use Response;
use DB;
use View;
use Carbon\Carbon;

class AirlinesTicketsController extends Controller
{
    
    public function index()
    {
        $data = SalesTicket::all();
        return view('airlinestickets::index',compact('data'));
    }

    public function create()
    {
        $paid_accounts      = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();

        $accounts           = Accounts::where('account_type_id',9)->where('status', 1)->get();

        return view('airlinestickets::create',compact('accounts','paid_accounts'));
    }


    public function store(Request $all_store_data)
    {

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $all_store_data->all();
      
        DB::beginTransaction();

        try{

                //Calculating Total Net Fare
                $net_fare      = 0;

                foreach ($data['pax_name'] as $key => $value)
                {   
                    
                    $net_fare  = $net_fare + $data['net_fare'][$key];
                }


            $data_find                        = SalesTicket::orderBy('created_at', 'DESC')->first();
            $sales_number                     = $data_find != null ? $data_find['sales_number'] + 1 : 1;

            $sales_ticket                            = new SalesTicket;
            $sales_ticket->sales_number              = $sales_number;
            $sales_ticket->sales_date                = date('Y-m-d');
            $sales_ticket->issue_date                = date('Y-m-d', strtotime($data['issue_date']));
            $sales_ticket->issue_by                  = $data['issue_by'];
            $sales_ticket->customer_id               = $data['customer_id'];
            $sales_ticket->sales_amount              = $data['sub_total_amount'];
            $sales_ticket->total_discount_type       = $data['total_discount_type'];
            $sales_ticket->total_discount_amount     = $data['total_discount_amount'];
            $sales_ticket->total_amount              = $data['total_amount'];
            $sales_ticket->cash_given                = $data['cash_given'];
            $sales_ticket->due_amount                = $data['total_amount'];
            $sales_ticket->total_net_fare            = $net_fare;
            $sales_ticket->previous_due              = $data['previous_due'];
            $sales_ticket->previous_due_type         = $data['balance_type'];
            $sales_ticket->account_id                = $data['account_id'];
            $sales_ticket->branch_id                 = $branch_id;
            $sales_ticket->created_by                = $user_id;

            if ($sales_ticket->save())
            {
                foreach ($data['pax_name'] as $key => $value)
                {

                    $sales_ticket_entries[] = [
                        'sales_id'          => $sales_ticket['id'],
                        'pax_name'          => $data['pax_name'][$key],
                        'eticket_no'        => $data['eticket_no'][$key],
                        'pnr'               => $data['pnr'][$key],
                        'passport_number'   => $data['passport_number'][$key],
                        'sector'            => $data['sector'][$key],
                        'carrier_name'      => $data['carrier_name'][$key],
                        'eq_fare'           => $data['eq_fare'][$key],
                        'tax'               => $data['tax'][$key],
                        'gross_fare'        => $data['gross_fare'][$key],
                        'commission'        => $data['commission'][$key],
                        'ait_tax'           => $data['ait_tax'][$key],
                        'net_fare'          => $data['net_fare'][$key],
                        'fly_date'          => date('Y-m-d', strtotime($data['fly_date'][$key])),
                        'customer_id'       => $sales_ticket['customer_id'],
                        'branch_id'         => $branch_id,
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('sales_ticket_entries')->insert($sales_ticket_entries);

                // stockOut($data, $item_id=null);

                //Financial Accounting Start
                    debit($customer_id=$data['customer_id'], 
                        $date=$data['issue_date'], 
                        $account_id=8, 
                        $amount=$data['total_amount'], 
                        $note=null, 
                        $transaction_head='ticket-sales', 
                        $income_id=null, 
                        $expense_id=null, 
                        $balance_transfer_id=null, 
                        $invoice_id=$sales_ticket->id, 
                        $bill_id=null, 
                        $payment_id=null, 
                        $production_id=null, 
                        $settlement_id=null, 
                        $sales_return_id=null, 
                        $purchase_return_id=null);

                    credit($customer_id=$data['customer_id'], 
                        $date=$data['issue_date'], 
                        $account_id=$data['account_id'], 
                        $amount=$data['total_amount'], 
                        $note=null, 
                        $transaction_head='ticket-sales', 
                        $income_id=null, 
                        $expense_id=null, 
                        $balance_transfer_id=null, 
                        $invoice_id=$sales_ticket->id, 
                        $bill_id=null, 
                        $payment_id=null, 
                        $production_id=null, 
                        $settlement_id=null, 
                        $sales_return_id=null, 
                        $purchase_return_id=null);

                    //Insert into journal_entries 
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {
                                debit($customer_id=$data['customer_id'], 
                                    $date=$data['issue_date'], 
                                    $account_id=$data['current_balance_paid_through'][$i], 
                                    $amount=$data['current_balance_amount_paid'][$i], 
                                    $note=$data['current_balance_note'][$i], 
                                    $transaction_head='payment-receive', 
                                    $income_id=null, 
                                    $expense_id=null, 
                                    $balance_transfer_id=null, 
                                    $invoice_id=$sales_ticket->id, 
                                    $bill_id=null, 
                                    $payment_id=null, 
                                    $production_id=null, 
                                    $settlement_id=null, 
                                    $sales_return_id=null, 
                                    $purchase_return_id=null);

                                credit($customer_id=$data['customer_id'], 
                                    $date=$data['issue_date'], 
                                    $account_id=8, 
                                    $amount=$data['current_balance_amount_paid'][$i], 
                                    $note=$data['current_balance_note'][$i], 
                                    $transaction_head='payment-receive', 
                                    $income_id=null, $expense_id=null, 
                                    $balance_transfer_id=null, 
                                    $invoice_id=$sales_ticket->id, 
                                    $bill_id=null, 
                                    $payment_id=null, 
                                    $production_id=null, 
                                    $settlement_id=null, 
                                    $sales_return_id=null, 
                                    $purchase_return_id=null);
                            }
                        }
                    }

                    customerBalanceUpdate($data['customer_id']);
                //Financial Accounting End

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Sales Created Successfully !!");
                }
                else
                {
                    $user_info  = userDetails();

                    if ($user_info['pos_printer'] == 0)
                    {
                        return redirect()->route('invoices_show_pos', $invoice->id);
                    }
                    else
                    {
                        return redirect()->route('invoices_show', $invoice->id);
                    }
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();

            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }
    
    
    public function show($id)
    {
        $data = SalesTicket::find($id);
        return view('airlinestickets::show',compact('data'));
    }
    
    
    public function clientsList()
    {
        $branch_id      = Auth()->user()->branch_id;

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Clients::orderBy('tbl_clients.created_at', 'DESC')
                                    ->where('tbl_clients.PtnID', 260)
                                    ->select('tbl_clients.*')
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Clients::where('tbl_clients.PtnID', 260)
                                    ->where('tbl_clients.ClientName', 'LIKE', "$search%")
                                    ->orWhere('tbl_clients.ClientContactNumber', 'LIKE', "$search%")
                                    ->orderBy('tbl_clients.created_at', 'DESC')
                                    ->select('tbl_clients.*')
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "ClientContactNumber" =>'');
            }
            
            $name = $value['ClientName'] . '-' . $value['ClientContactNumber'];
            
            $data[] = array("id"=>$value['id'], "text"=>$name, "ClientContactNumber" =>$value['ClientContactNumber']);

            $i++;
        }
   
        return Response::json($data);
    }
    
    public function updateTaxAIT()
    {
        $data = DB::table('tax_ait')->first();
        return view('airlinestickets::edit_tax_ait',compact('data'));
    }
    
    public function updateTaxAITsubmit(Request $request)
    {
        
        $data = $request->all();
        
        $tax_ait = TaxAit::find($data['id']);
        
        $tax_ait->tax_amount            = $data['tax_amount'];
        $tax_ait->tax_percentage        = $data['tax_percentage'];
        $tax_ait->ait_amont             = $data['ait_amont'];
        $tax_ait->ait_percentage        = $data['ait_percentage'];
        
        if ($tax_ait->save())
        {   
            return back()->with("success","Update Successfully !!");
        }else
        {
            return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
        }
        
    }


    public function taxAit()
    {
        $data = DB::table('tax_ait')->first();

        return Response::json($data);
    }

}
