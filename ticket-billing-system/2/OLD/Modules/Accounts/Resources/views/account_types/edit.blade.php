@extends('layouts.app')

@section('title', 'Edit Account Types')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Account Types</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Accounts</a></li>
                                    <li class="breadcrumb-item active">Edit Account Types</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif
                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('account_types_update', $find_account_type['id']) }}" method="post" enctype="multipart/formdata">
            					{{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Account Name *</label>
                                        <input type="text" name="name" class="inner form-control" id="name" placeholder="Enter AccountName" value="{{ $find_account_type['account_name'] }}" required />
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Description</label>
                                        <input type="text" name="description" class="inner form-control" id="description" placeholder="Enter Description" value="{{ $find_account_type['description'] }}" />
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Parent Account Type</label>
                                        <select style="cursor: pointer" class="form-control select2" name="parent_account_id" required>
                                        	@if($parent_accounts->count() > 0)
                                        	@foreach($parent_accounts as $key => $value)
                                        	@if($value['id'] == $find_account_type['parent_account_type_id'])
                                            <option value="{{ $value->id }}" {{ $value['id'] == $find_account_type['parent_account_type_id'] ? 'selected' : '' }}>{{ $value->account_name }}</option>
                                            @endif
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Status</label>
                                        <select style="cursor: pointer" class="form-control" name="status">
                                            <option value="1" {{ $find_account_type['status'] == 1 ? 'selected' : '' }}>Active</option>
                                            <option value="0" {{ $find_account_type['status'] == 0 ? 'selected' : '' }}>Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('account_types_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            	<h4 class="card-title">All Account Types</h4>
                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%">SL</th>
                                            <th style="width: 20%">Account Name</th>
                                            <th style="width: 15%">Parent Account</th>
                                            <th style="width: 25%">Description</th>
                                            <th style="width: 10%">Status</th>
                                            <th style="width: 10%">Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                    	@if(!empty($account_types) && ($account_types->count() > 0))
                                    	@foreach($account_types as $key => $account_type)
	                                        <tr>
	                                            <td>{{ $key + 1 }}</td>
	                                            <td>{{ $account_type['account_name'] }}</td>
	                                            <td>{{ $account_type->parentAccountType->account_name }}</td>
	                                            <td>{{ $account_type['description'] }}</td>
	                                            <td>{{ $account_type['status'] == 1 ? 'Active' : 'Inactive' }}</td>
	                                            <td>
	                                            	<div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('account_types_edit', $account_type['id']) }}">Edit</a>
                                                        </div>
                                                    </div>
	                                            </td>
	                                        </tr>
	                                    @endforeach
	                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }
    </script>
@endsection