<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesTicketEntites extends Model
{
    protected $table = "sales_ticket_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }
}
