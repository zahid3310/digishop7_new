<!DOCTYPE html>
<html>

<head>
    <title>Daily Report</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style> 
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Daily Report</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 20%"></th>
                                    <th style="text-align: center;width: 10%">Name</th>
                                    <th style="text-align: center;width: 10%">Amount</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php 
                                    $opening_net_income     = $opening['actual_sales'] + $opening['customer_due_collection'] + $opening['purchase_return'] + $opening['income'];
                                    $opening_net_expense    = $opening['actual_purchase'] + $opening['supplier_due_collection'] + $opening['sales_return'] + $opening['expense'];
                                    $opening_balance        = $opening_net_income - $opening_net_expense;
                                ?>
                                <tr>
                                    <td style="text-align: right;font-weight: bold" colspan="2">Opening Balance</td>
                                    <td style="text-align: right;font-weight: bold">{{ number_format($opening_balance,2,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;vertical-align: middle;font-weight: bold" rowspan="7">Net Income</td>
                                   
                                    <td style="text-align: left;">Sales Of Item</td>
                                    <td style="text-align: right;">{{ number_format($data['sales'],2,'.',',') }}</td>
                                </tr>
                                 
                                <tr>
                                    <td style="text-align: left;">Actual Sales</td>
                                    <td style="text-align: right;">{{ number_format($data['actual_sales'],2,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left;">Customer Due Collection</td>
                                    <td style="text-align: right;">{{ number_format($data['customer_due_collection'],2,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left;">Income(Accounts)</td>
                                    <td style="text-align: right;">{{ number_format($data['income'],2,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left;">Purchase Return</td>
                                    <td style="text-align: right;">{{ number_format($data['purchase_return'],2,'.',',') }}</td>
                                </tr>

                                <?php 
                                    $sub_total  = $data['actual_sales'] + $data['customer_due_collection'] + $data['purchase_return'] + $data['income'];
                                    $net_income = $opening_balance + $sub_total;
                                ?>

                                <tr> 
                                    <td style="text-align: right;font-weight: bold">Sub Total</td>
                                    <td style="text-align: right;font-weight: bold">{{ number_format($sub_total,2,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: right;font-weight: bold">Net Income</td>
                                    <td style="text-align: right;font-weight: bold">{{ number_format($net_income,2,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;vertical-align: middle;font-weight: bold" rowspan="7">Net Expense</td>
                                   
                                    <td style="text-align: left;">Purchase Of Item</td>
                                    <td style="text-align: right;">{{ number_format($data['purchase'],2,'.',',') }}</td>
                                </tr>
                                 
                                <tr>
                                    <td style="text-align: left;">Actual Purchase</td>
                                    <td style="text-align: right;">{{ number_format($data['actual_purchase'],2,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left;">Supplier Due Collection</td>
                                    <td style="text-align: right;">{{ number_format($data['supplier_due_collection'],2,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left;">Expense(Accounts)</td>
                                    <td style="text-align: right;">{{ number_format($data['expense'],2,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: left;">Sales Return</td>
                                    <td style="text-align: right;">{{ number_format($data['sales_return'],2,'.',',') }}</td>
                                </tr>

                                <?php 
                                    $net_expense = $data['actual_purchase'] + $data['supplier_due_collection'] + $data['sales_return'] + $data['expense'];
                                ?>

                                <tr>
                                    <td style="text-align: right;font-weight: bold">Net Expense</td>
                                    <td style="text-align: right;font-weight: bold">{{ number_format($net_expense,2,'.',',') }}</td>
                                </tr>    
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="2" style="text-align: right;">Closing Balance</th>
                                    <th colspan="1" style="text-align: right;">{{ number_format($net_income - $net_expense,2,'.',',') }}</th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>