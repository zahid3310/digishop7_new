@extends('layouts.app')

@section('title', 'Table Booking')


@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Table Booking</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Table Booking</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <!-- <div class="row">
                                    <div style="background-color: #DA4625;text-align: center;color: white;padding: 10px;font-size: 20px" class="col-md-12">
                                       <span>Select Table</span>
                                    </div>
                                </div> -->

                                <div style="margin-top: 20px" class="row">

                                    @if($tables->count()  > 0)
                                    @foreach($tables as $key => $table)
                                    <?php
                                        $inv  = $invoices->where('table_id', $table->id)->count();
                                        $inv1 = $invoices->where('table_id', $table->id)->first();

                                        if ($inv > 0)
                                        {
                                            $waiter  = App\Models\Users::find($inv1->created_by);
                                        }
                                        else
                                        {
                                            $waiter  = null;
                                        }
                                    ?>

                                        <div style="{{ $inv > 0 ? 'border: 1px solid grey;height: 130px;background-color: red;margin: 15px;cursor: pointer' : 'border: 1px solid grey;height: 130px;background-color: #ECB707;margin: 15px;cursor: pointer' }}" id="" class="col-md-2">
                                            <a href="{{ route('quick_sales').'?table_id='.$table->id }}" target="_blank">
                                                <h5 style="text-align: center;font-size: 20px;margin-top: 40px">{{ $table->name }} <br> 
                                                <span style="font-size: 12px">{{ $waiter != null ? $waiter->name : '' }}</span></h5>
                                            </a>
                                        </div>
                                    @endforeach
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection