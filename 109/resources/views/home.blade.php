@extends('layouts.app')

@section('title', 'Dashboard')

<style type="text/css">
    .card-counter{
        box-shadow: 2px 2px 10px #DADADA;
        margin: 5px;
        padding: 20px 10px;
        background-color: #fff;
        height: 100px;
        border-radius: 5px;
        transition: .3s linear all;
    }

    .card-counter:hover{
        box-shadow: 4px 4px 20px #DADADA;
        transition: .3s linear all;
    }

    .card-counter.primary{
        background-color: #007bff;
        color: #FFF;
    }

    .card-counter.danger{
        background-color: #ef5350;
        color: #FFF;
    }  

    .card-counter.success{
        background-color: #66bb6a;
        color: #FFF;
    }  

    .card-counter.info{
        background-color: #26c6da;
        color: #FFF;
    }  

    .card-counter i{
        font-size: 5em;
        opacity: 0.2;
    }

    .card-counter .count-numbers{
        position: absolute;
        right: 35px;
        top: 20px;
        font-size: 32px;
        display: block;
    }

    .card-counter .count-name{
        position: absolute;
        right: 35px;
        top: 65px;
        font-style: italic;
        text-transform: capitalize;
        display: block;
        font-size: 18px;
    }

    @media screen and (max-width: 48em) {
        .row-offcanvas {
            position: relative;
            -webkit-transition: all 0.25s ease-out;
            -moz-transition: all 0.25s ease-out;
            transition: all 0.25s ease-out;
        }

        .row-offcanvas-left .sidebar-offcanvas {
            left: -33%;
        }

        .row-offcanvas-left.active {
            left: 33%;
            margin-left: -6px;
        }

        .sidebar-offcanvas {
            position: absolute;
            top: 0;
            width: 33%;
            height: 100%;
        }
    }

    @media screen and (max-width: 34em) {
        .row-offcanvas-left .sidebar-offcanvas {
            left: -45%;
        }

        .row-offcanvas-left.active {
            left: 45%;
            margin-left: -6px;
        }

        .sidebar-offcanvas {
            width: 45%;
        }
    }
    
    .card {
        overflow: hidden;
    }
    
    .card-block .rotate {
        z-index: 8;
        float: right;
        height: 100%;
    }
    
    .card-block .rotate i {
        color: rgba(20, 20, 20, 0.15);
        position: absolute;
        left: 0;
        left: auto;
        right: -10px;
        bottom: 0;
        display: block;
        -webkit-transform: rotate(-44deg);
        -moz-transform: rotate(-44deg);
        -o-transform: rotate(-44deg);
        -ms-transform: rotate(-44deg);
        transform: rotate(-44deg);
    }

    .display-1 {
        font-size: 2rem !important;
        color: white !important;
    }

    a, button {
        outline: 0!important;
        /*display: none;*/
    }

    .canvasjs-chart-credit
    {
        display: none !important;
    }

    .f-size
    {
        font-size: 25px !important;
    }

    .a-size
    {
        font-size: 15px !important;
    }
</style>

@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Dashboard</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger col-sm-12" role="alert">
                      <marquee behavior=scroll direction="left" scrollamount="5" onmouseover="this.stop();" onmouseout="this.start();">
                          <b>
                               <?php echo strip_tags(isset($notification->digishop_notification) ? $notification->digishop_notification : ''); ?>
                          </b>
                      </marquee>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="card-counter success">
                        <i class="fa fa-database"></i>
                        <span id="todaysSales" class="count-numbers f-size">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">Today's Sales</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter info">
                        <i class="fa fa-users"></i>
                        <span id="todaysSalesReturn" class="count-numbers f-size">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">Today's Sales Return</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter success">
                        <i class="fa fa-database"></i>
                        <span id="todaysPurchase" class="count-numbers f-size">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">Today's Purchase</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter info">
                        <i class="fa fa-users"></i>
                        <span id="todaysPurchaseReturn" class="count-numbers f-size">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">Today's Purchase Return</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter danger">
                        <i class="fa fa-database"></i>
                        <span id="todaysExpense" class="count-numbers f-size">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">Today's Expense</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter info">
                        <i class="fa fa-users"></i>
                        <span id="customerDues" class="count-numbers f-size">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">Customer Dues</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter info">
                        <i class="fa fa-users"></i>
                        <span id="supplierDues" class="count-numbers f-size">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">Supplier Dues</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter primary">
                        <i class="fa fa-envelope"></i>

                        <?php
                            // $url        = "https://esms.mimsms.com/miscapi/C20072945fa0e98b1d9f42.77965009/getBalance";
                            // $response   = file_get_contents($url);
                            // $tmt        = substr($response, strrpos($response, ' ' ) + 1); 
                        ?>

                        <span class="count-numbers f-size" id="total_cash_in_hand">
                            <h4 style="color: white" class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">Total Cash In Hand</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter success">
                        <i class="fa fa-database"></i>
                        <span id="totalSells" class="count-numbers f-size">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">Total Sales</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter success">
                        <i class="fa fa-database"></i>
                        <span id="totalPurchase" class="count-numbers f-size">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">Total Purchase</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter danger">
                        <i class="fa fa-database"></i>
                        <span id="totalCustomers" class="count-numbers f-size">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">Total Customers</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter danger">
                        <i class="fa fa-database"></i>
                        <span id="totalSuppliers" class="count-numbers f-size">
                            <h4 class="mb-0">0</h4>
                        </span>
                        <span class="count-name a-size">Total Suppliers</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4 float-sm-left">Sales Summary</h4>
                            <div class="clearfix"></div>
                            <div id="chartContainer3" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4 float-sm-left">Purchase Summary</h4>
                            <div class="clearfix"></div>
                            <div id="chartContainer4" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            var site_url  = $('.site_url').val();

            $.get(site_url + '/dashboard/items', function(data){

                $("#totalCustomers").empty();
                $("#totalCustomers").append(data.total_customers);

                $("#totalSuppliers").empty();
                $("#totalSuppliers").append(data.total_suppliers);

                $("#totalProducts").empty();
                $("#totalProducts").append(data.total_products);

                $("#todaysSales").empty();
                $("#todaysSales").append(data.todays_sells.toFixed());

                $("#todaysSalesReturn").empty();
                $("#todaysSalesReturn").append(data.todays_sells_return.toFixed());

                $("#todaysPurchase").empty();
                $("#todaysPurchase").append(data.todays_purchase.toFixed());

                $("#todaysPurchaseReturn").empty();
                $("#todaysPurchaseReturn").append(data.todays_purchase_return.toFixed());

                $("#todaysExpense").empty();
                $("#todaysExpense").append(data.todays_expenses.toFixed());

                $("#totalSells").empty();
                $("#totalSells").append(data.total_sells.toFixed());

                $("#totalPurchase").empty();
                $("#totalPurchase").append(data.total_purchase.toFixed());

                $("#customerDues").empty();
                $("#customerDues").append(data.customer_dues.toFixed());

                $("#supplierDues").empty();
                $("#supplierDues").append(data.supplier_dues.toFixed());                
                
                $("#total_cash_in_hand").empty();
                $("#total_cash_in_hand").append(data.total_cash_in_hand.toFixed());


                //Sales Summary Chart Start
                    var mounth               = [];
                    var amount               = [];

                    $.each(data.sales_summary,function (e, value) {

                        if (value.month == 1)
                        {
                            var SetMonth = "Jan";
                        }

                        if (value.month == 2)
                        {
                            var SetMonth = "Feb";
                        }

                        if (value.month == 3)
                        {
                            var SetMonth = "Mar";
                        }

                        if (value.month == 4)
                        {
                            var SetMonth = "Apr";
                        }

                        if (value.month == 5)
                        {
                            var SetMonth = "May";
                        }

                        if (value.month == 6)
                        {
                            var SetMonth = "Jun";
                        }

                        if (value.month == 7)
                        {
                            var SetMonth = "Jul";
                        }

                        if (value.month == 8)
                        {
                            var SetMonth = "Aug";
                        }

                        if (value.month == 9)
                        {
                            var SetMonth = "Sep";
                        }

                        if (value.month == 10)
                        {
                            var SetMonth = "Oct";
                        }

                        if (value.month == 11)
                        {
                            var SetMonth = "Nov";
                        }

                        if (value.month == 12)
                        {
                            var SetMonth = "Dec";
                        }

                        mounth.push(SetMonth);
                        amount.push(value.total);
                    });

                    var myChart = echarts.init(document.getElementById('chartContainer3'));

                    colors = ['#00C8C4'];
                    option = {
                        color:colors,
                        tooltip : {
                            trigger: 'axis',
                            
                        },
                        grid: {
                            left: '12%',
                            right: '1%',
                            bottom: '8%',
                            top:'10%',
                        },
                        xAxis : [
                            {
                                type : 'category',
                                data : mounth
                            }
                        ],
                        yAxis : [
                            {
                                type : 'value',
                            }
                        ],
                        series : [
                            {
                                name:'Sales',
                                type:'bar',
                                data:amount
                            }
                        ]
                    };

                    var salesY = option;

                    setTimeout(function(){ 
                        myChart.setOption(salesY); }, 
                    3000);
                //Sales Summary Chart End

                //Purchase Summary Chart Start
                    var mounth               = [];
                    var amount               = [];

                    $.each(data.purchase_summary,function (e, value) {

                        if (value.month == 1)
                        {
                            var SetMonth = "Jan";
                        }

                        if (value.month == 2)
                        {
                            var SetMonth = "Feb";
                        }

                        if (value.month == 3)
                        {
                            var SetMonth = "Mar";
                        }

                        if (value.month == 4)
                        {
                            var SetMonth = "Apr";
                        }

                        if (value.month == 5)
                        {
                            var SetMonth = "May";
                        }

                        if (value.month == 6)
                        {
                            var SetMonth = "Jun";
                        }

                        if (value.month == 7)
                        {
                            var SetMonth = "Jul";
                        }

                        if (value.month == 8)
                        {
                            var SetMonth = "Aug";
                        }

                        if (value.month == 9)
                        {
                            var SetMonth = "Sep";
                        }

                        if (value.month == 10)
                        {
                            var SetMonth = "Oct";
                        }

                        if (value.month == 11)
                        {
                            var SetMonth = "Nov";
                        }

                        if (value.month == 12)
                        {
                            var SetMonth = "Dec";
                        }

                        mounth.push(SetMonth);
                        amount.push(value.total);
                    });

                    var myChart1 = echarts.init(document.getElementById('chartContainer4'));

                    colors = ['#28a745'];
                    option = {
                        color:colors,
                        tooltip : {
                            trigger: 'axis',
                            
                        },
                        grid: {
                            left: '12%',
                            right: '1%',
                            bottom: '8%',
                            top:'10%',
                        },
                        xAxis : [
                            {
                                type : 'category',
                                data : mounth
                            }
                        ],
                        yAxis : [
                            {
                                type : 'value',
                            }
                        ],
                        series : [
                            {
                                name:'Purchase',
                                type:'bar',
                                data:amount
                            }
                        ]
                    };

                    var salesY1 = option;
                    setTimeout(function(){ 
                        myChart1.setOption(salesY1); }, 
                    3000);
                //Purchase Summary Chart End
            });

            $("#DueInvoiceYear").addClass("active"); 
        });
    
        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }

        function dueInvoiceClick()
        {
            var click_id  = $(this).id;

            console.log(click_id);
        }
    </script>
@endsection

@push('scripts')
    <!-- echarts js -->
    <script src="{{ url('public/admin_panel_assets/libs/echarts/echarts.min.js') }}"></script>
    <!-- echarts init -->
    <script src="{{ url('public/admin_panel_assets/js/pages/echarts.init.js') }}"></script>
    <!-- <script src="{{ url('public/admin_panel_assets/js/canvasCharts.min.js') }}"></script> -->
@endpush