<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class OrderJournalEntries extends Model
{  
    protected $table = "order_journal_entries";

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoices','invoice_id');
    }
    
    public function account()
    {
        return $this->belongsTo('App\Models\Accounts','account_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }
}
