<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class IssueReturn extends Model
{  
    protected $table = "issue_return";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function issue()
    {
        return $this->belongsTo('App\Models\Issues','issue_id');
    }

    public function srName()
    {
        return $this->belongsTo('App\Models\Customers','sr_id');
    }
}
