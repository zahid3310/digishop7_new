@extends('layouts.app')

@section('title', 'SMS Purchase')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">SMS Purchase</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Pay Bill</a></li>
                                    <li class="breadcrumb-item active">SMS Purchase</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('software_billing_sms_purchase_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h3 style="text-align: center">Bkash Merchant Number : <span style="color: blue">01715317133</span></h3>
                                        <hr>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Purchase Date</label>
                                        <input id="date" name="date" type="text" value="{{ date('d-m-Y') }}" class="form-control" readonly required>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">SMS Type</label>
                                        <select id="sms_type" style="width: 100%" class="form-control select2" name="sms_type" onchange="getRate()" required> 
                                            <option value="">--Select Type--</option>
                                            <option value="1">Non Masking</option>
                                            <option value="2">Masking</option>
                                            <option value="3">Voice</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">SMS Rate</label>
                                        <input type="text" name="sms_rate" class="inner form-control" id="sms_rate" readonly required/>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Quantity *</label>
                                        <input type="text" name="quantity" class="inner form-control" id="quantity" oninput="calculate()" required/>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Total Payable</label>
                                        <input type="text" name="total_payable" class="inner form-control" id="total_payable" readonly required/>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Paid Amount</label>
                                        <input type="text" name="paid_amount" class="inner form-control" id="paid_amount" required/>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Payment Methode</label>
                                        <select id="payment Methode" style="width: 100%" class="form-control select2" name="payment_methode" required>
                                            <option value="2">Bkash</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Transaction ID</label>
                                        <input type="text" name="transaction_id" class="inner form-control" id="transaction_id" required/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Purchase</button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <hr style="margin-top: 0px">

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;width: 5%;font-size: 12px">SL</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Date</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Particular</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">SMS Type</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Quantity</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Payable</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Paid</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Paid Through</th>
                                            <th style="text-align: center;width: 15%;font-size: 12px">Transaction#</th>
                                            <th style="text-align: center;width: 10%;font-size: 12px">Status</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(count($data) > 0)
                                        @foreach($data as $key => $value)
                                        <tr>
                                            <td style="font-size: 12px;text-align: center">{{ $key + 1 }}</td>
                                            <td style="font-size: 12px;text-align: center">{{ date('d-m-Y', strtotime($value->date)) }}</td>
                                            <td style="text-align: center;font-size: 12px;text-align: left">SMS Purchase</td>
                                            <td style="text-align: center;font-size: 12px">
                                                @if($value->sms_type == 1)
                                                Non Masking
                                                @elseif($value->sms_type == 2)
                                                Masking
                                                @else
                                                Voice
                                                @endif
                                            </td>
                                            <td style="font-size: 12px;text-align: center">{{ $value->quantity }}</td>
                                            <td style="text-align: right;font-size: 12px">{{ number_format($value->total_payable,0,'.',',') }}</td>
                                            <td style="text-align: right;font-size: 12px">{{ number_format($value->paid_amount,0,'.',',') }}</td>
                                            <td style="font-size: 12px;text-align: center">Bkash</td>
                                            <td style="font-size: 12px;text-align: center">{{ $value->transaction_id }}</td>
                                            <td style="text-align: center;font-size: 12px;font-weight: bold;">
                                                @if($value->status == 0)
                                                <span style="color: red">Pending</span>
                                                @else
                                                <span style="color: green">Approved</span>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    function getRate()
    {
        var typeId      = $("#sms_type").val();
        var site_url    = $('.site_url').val();

        $.get(site_url + '/softwarebilling/sms-rate/' + typeId, function(data){
            
            if (typeId == 1)
            {
                $("#sms_rate").val(parseFloat(data.non_masking_sms_rate));
            }

            if(typeId == 2)
            {
                $("#sms_rate").val(parseFloat(data.masking_sms_rate));
            }

            if(typeId == 3)
            {
                $("#sms_rate").val(parseFloat(data.voice_sms_rate));
            }
        });
    }

    function calculate()
    {
        var sms_rate = $('#sms_rate').val();
        var sms_qty  = $('#quantity').val();

        if (sms_rate == '')
        {
            var rateCal     = 0;
        }
        else
        {
            var rateCal     = $("#sms_rate").val();
        }

        if (sms_qty == '')
        {
            var quantityCal = 0;
        }
        else
        {
            var quantityCal = $("#quantity").val();
        }

        var total = parseFloat(rateCal)*parseFloat(quantityCal);

        $("#total_payable").val(total.toFixed());
    }
</script>
@endsection