@extends('layouts.app')

@section('title', 'Order')

@push('styles')
<style type="text/css">
    body[data-sidebar=dark].vertical-collpsed {
        min-height: 0px !important;
    }
    
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>
@endpush

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Orders</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">New Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('orders_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                {{ csrf_field() }}

                                <input type="hidden" id="default_stock" value="0">

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Customer *</label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="customer_id" name="customer_id" class="form-control select2" required>
                                                    <option value="1081">Walk-In Customer</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Reference</label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="reference_id" name="reference_id" class="form-control select2">
                                                   <option value="">Select Reference</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label id="bbShow" for="productname" class="col-md-5 col-form-label">Previous Due </label>
                                            <div class="col-md-7">
                                                <input id="balance" name="previous_due" type="text" value="" class="form-control" value="0" readonly>
                                                <input id="bbBalance" type="hidden" name="balance_type" class="form-control" value="1">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-3 col-form-label">Note </label>
                                            <div class="col-md-9">
                                                <input id="invoice_note" name="invoice_note" type="text" value="" class="form-control" placeholder="Note">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">SM </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="sr_id" name="sr_id" class="Sm form-control select2">
                                                    <option value="0">--Select SM--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Code </label>
                                            <div class="col-md-8">
                                                <input type="text" name="" class="form-control code" readonly="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-3 col-form-label">Address *</label>
                                            <div class="col-md-9">
                                               <input type="text" id="address" name="address" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Date *</label>
                                            <div class="col-md-8">
                                                <input id="selling_date" name="selling_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Category </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="cat_id" class="form-control select2 productCategory">
                                                    <option>--Select Category--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Barcode </label>
                                            <div class="col-md-8">
                                                <input type="text" name="product_code" class="inner form-control" id="product_code" placeholder="Scan Product Code" oninput="PosScannerSearch()" autofocus />
                                                <p id="alertMessage" style="display: none">No Product Found !</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div style="margin-top: 0px;" class="form-group row">
                                            <div class="col-md-12">
                                                <i id="add_field_button" style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button col-md-3"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="display: none">
                                    <select style="padding: 6px;border-radius: 4px;cursor: pointer" class="taxType" id="tax_type_0" name="tax_type" oninput="calculateActualAmount(0)">
                                        <option style="padding: 10px" value="1" selected>BDT</option>
                                        <option style="padding: 10px" value="0">%</option>
                                    </select>
                                    <input id="tax_amount_0" type="text" class="form-control width-xs taxAmount" name="tax_amount" value="0" oninput="calculateActualAmount(0)">
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;padding-top: 5px;height: 30px;overflow-x: auto;overflow-y: hidden;" class="col-md-12">
                                        <div class="row">
                                            <div style="padding-left: 15px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">
                                                <label for="productname">Category *</label>
                                            </div>
                                            
                                            <div style="padding-left: 15px" class="col-lg-3 col-md-3 col-sm-6 col-12">
                                                <label for="productname">Product *</label>
                                            </div>

                                            <div style="padding-left: 3px" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">
                                                <label for="productname">Unit</label>
                                            </div>

                                            <div style="padding-left: 3px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                <label for="productname">Qty *</label>
                                            </div>

                                            <div style="padding-left: 3px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                <label for="productname">Rate *</label>
                                            </div>

                                            <div style="padding-left: 7px" class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                <div class="row">
                                                    <div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">
                                                        <label style="padding-top: 13px" for="productname"></label>
                                                    </div>

                                                    <div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">
                                                        <label for="productname">Discount</label>
                                                    </div>                                               
                                                </div>
                                            </div>

                                            <div style="padding-left: 3px" class="col-lg-1 col-md-1 col-sm-6 col-6"> 
                                                <label for="productname">Total</label>
                                            </div>

                                            <div style="padding-left: 15px;display:none!important;" class="col-lg-2 col-md-2 col-sm-6 col-12">
                                                <label for="productname">Free Item</label>
                                            </div>

                                            <div style="padding-left: 15px;display:none!important;" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                <label for="productname">Free Qty *</label>
                                            </div> 

                                            <div style="padding-left: 3px;" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                <label for="productname">Action</label>
                                            </div>
                                        </div>    
                                    </div>

                                    <div style="background-color: #D2D2D2;height: 260px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        <div class="row di_0">
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 150px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Sub Total</label>
                                            <div class="col-md-7">
                                                <input type="text" id="subTotalBdt" class="form-control">
                                                <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">VAT</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control" id="vat_type_0" name="vat_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="0" {{ Auth::user()->vat_type == 0 ? 'selected' : '' }}>%</option>
                                                    <option style="padding: 10px" value="1" {{ Auth::user()->vat_type == 1 ? 'selected' : '' }}>BDT</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="vat_amount_0" type="text" class="form-control width-xs" name="vat_amount" value="{{ Auth::user()->vat_amount }}" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Discount</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control totalDiscountType" id="total_discount_type_0" name="total_discount_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="1" selected>BDT</option>
                                                    <option style="padding: 10px" value="0">%</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="total_discount_0" type="text" class="form-control totalDiscount" name="total_discount_amount" value="0" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 150px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Dis. Note</label>
                                            <div class="col-md-7">
                                                <input id="total_discount_note" type="text" class="form-control width-xs" name="total_discount_note" value="{{ old('total_discount_note') }}" placeholder="Discount Note">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Coupon</label>
                                            <div class="col-md-7">
                                                <input id="coupon_code" type="number" class="form-control width-xs couponCode" name="coupon_code" onchange="couponMembership()" placeholder="Coupon/Membership">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Send SMS</label>
                                            <div class="col-md-12">
                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Non Masking
                                                    </label>
                                                </div>

                                                <div style="padding-left: 10px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Masking
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 150px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Order Amount</label>
                                            <div class="col-md-7">
                                                <input type="text" id="totalBdt" class="form-control">
                                                <input style="display: none" type="text" id="totalBdtShow" name="total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Net Payable</label>
                                            <div class="col-md-7">
                                                <input type="text" id="netPa" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 150px;padding-top: 13px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Cash Given</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="cash_given" name="cash_given" placeholder="Cash Given" value="0" oninput="calculateChangeAmount()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Adjustment</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="adjustment" name="adjustment" placeholder="Adjustment" value="0" oninput="calculateChangeAmount()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <div class="col-md-12">
                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="adjustAdvancePayment" name="adjust_cash_given">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="adjustAdvancePayment" onclick="adjustAdvancePayment()">
                                                        Adjust Advance Payment
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <input type="hidden" name="customer_advance_amount" id="customer_advance_amount" value="0">

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Advance</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="change_amount" name="change_amount" placeholder="Advance Amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Acc. Head</label>
                                            <div class="col-md-7">
                                                <select style="width: 100%;cursor: pointer;" id="account_id" name="account_id" class="form-control" required>
                                                    <option value="">--Select Account Head--</option>
                                                    @if($accounts->count() > 0)
                                                    @foreach($accounts as $key=> $account)
                                                    <option value="{{ $account->id }}" {{ $account->id == 4 ? 'selected' : '' }}>{{ $account->account_name }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #D2D2D2;height: 120px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap_current_balance getMultipleRowCurrentBalance">
                                        <div class="row row di_current_balance_0">
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>

                                    <div style="display: none;" class="row justify-content-end">
                                        <div class="col-lg-1 col-md-2 form-group">
                                            <input id="pos_add_button" type="button" class="btn btn-success btn-block inner add_field_button_pos" value="Add"/>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button_current_balance" type="button" class="btn btn-success btn-block inner add_field_button_current_balance" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px !important;padding-bottom: 0px !important" class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Save & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('orders_index') }}">Close</a></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control commonCustomerClass">
                        </div>
                    </div>

                    <input id="contact_type" name="contact_type" type="hidden" value="0" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Reference</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name1" name="customer_name" type="text" class="form-control commonReferenceClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number1" name="mobile_number" type="text" class="form-control commonReferenceClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address1" name="address" type="text" class="form-control commonReferenceClass">
                        </div>
                    </div>

                    <input id="contact_type1" name="contact_type" type="hidden" value="3" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal2" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Print Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label for="productname" class="col-md-4 col-form-label">Date</label>
                                <div class="col-md-8">
                                    <input style="cursor: pointer" id="search_date" type="date" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Customer </label>
                                <div class="col-md-8">
                                    <input id="customer" type="text" class="form-control"  placeholder="Enter Customer Name">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Search Invoice </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <input style="width: 95%" type="text" class="form-control col-lg-9 col-md-9 col-sm-9 col-9" id="invoiceNumber" placeholder="Enter Invoice ID">
                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="printInvoicesSearch()">
                                            <i class="bx bx-search font-size-24"></i>
                                        </span>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Order#</th>
                                <th>Customer</th>
                                <th>Amount</th>
                                <th>Paid</th>
                                <th>Due</th>
                                <th>Creator</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody id="print_invoice_list">
                        </tbody>
                    </table>
                </div>
                
                <div class="modal-footer">
                    <button id="CloseButton2" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal3" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New SR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name_sr" name="customer_name" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number_sr" name="mobile_number" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address_sr" name="address" type="text" class="form-control commonCustomerClass">
                        </div>
                    </div>

                    <input id="contact_type_sr" name="contact_type" type="hidden" value="4" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn3" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton3" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url    = $('.site_url').val();
            var type        = (new URL(location.href)).searchParams.get('sales_type');

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if ((result['contact_type'] == 0) && result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#reference_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 3)
                    {
                        return result['text'];
                    }
                },
            });

            $("#sr_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 4 && result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#cat_id").select2({
                ajax: { 
                url:  site_url + '/orders/cat-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';
                    return result['text'];
                },
            });

            $('#add_field_button').click();
            $('#add_field_button_current_balance').click();
            $('#vertical-menu-btn').click();

            var sr_id     = $("#sr_id").val();

            if (sr_id == 0)
            {
                // $(".productEntries").prop('disabled', true);
                $(".freeItems").prop('disabled', true);
            }
            else
            {
                // $(".productEntries").prop('disabled', false);
                $(".freeItems").prop('disabled', false);
            }
            
            ProductCategoryList();

            var type = $("#bbBalance").val();

            if (type == 1)
            {
                $("#bbShow").html('Previous Due');
                $("#bbBalance").val(type);
            }
            else
            {
                $("#bbShow").html('Advance');
                $("#bbBalance").val(type);
            }

            var customerId      = $("#customer_id").val();

            $.get(site_url + '/invoices/calculate-opening-balance/' + customerId, function(data){
                
                $("#balance").empty();
                $("#balance").val(data.balance);

                if (data.type == 1)
                {
                    $("#bbShow").html('Previous Due');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', false);
                }
                else
                {
                    $("#bbShow").html('Advance');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', true);
                }
            });
        });


        $(document).on("change", "#customer_id" , function() {

            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();
            $(".Sm").empty();
           
            $.get(site_url + '/orders/calculate-opening-balance/' + customerId, function(data){
                $(".code").val(data.code);
                $("#balance").empty();
                $("#balance").val(data.toFixed());
                
                
            });
            
             $.get(site_url + '/invoices/customer/address/' + customerId, function(data){

                $("#address").empty();
                $("#address").val(data.address);
                
            });

            $.get(site_url + '/invoices/customer/sm/' + customerId, function(data)
            {   
                if (jQuery.isEmptyObject(data))
                {
                    $(".Sm").append('<option value="0">'+ '--Select SM--' +'</option>');
                }
                else
                {
                    $(".Sm").append('<option value="'+ data.id+'">'+ data.name+'</option>');
                }
            });
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = $("#contact_type").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                            $('.commonCustomerClass').val('');
                        }
                        
                        $("#customer_id").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });

        $('#submitBtn1').click(function() {
            
            var customer_name               = $("#customer_name1").val();
            var address                     = $("#address1").val();
            var mobile_number               = $("#mobile_number1").val();
            var contact_type                = $("#contact_type1").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton1').click();
                            $('.commonReferenceClass').val('');
                        }
                        
                        $("#reference_id").empty();
                        $('#reference_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });

        $('#submitBtn3').click(function() {
            
            var customer_name               = $("#customer_name_sr").val();
            var address                     = $("#address_sr").val();
            var mobile_number               = $("#mobile_number_sr").val();
            var contact_type                = $("#contact_type_sr").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton3').click();
                            $('.commonCustomerClass').val('');
                        }
                        
                        $("#sr_id").empty();
                        $('#sr_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });     
    </script>

    <script type="text/javascript">
        function PosScannerSearch()
        {
            $("#alertMessage").hide();

            var site_url      = $(".site_url").val();
            var read_code     = $("#product_code").val();

            if (read_code.length == 6)
            {
                var product_code  = read_code.replace(/^0+/, '');
            }
            else
            {
                var product_code  = '0';
            }

            if(product_code != 0)
            {
                $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code, function(data_pos){

                    if ($.isEmptyObject(data_pos))
                    {   
                        $('input[name=product_code').val('');
                        $("#alertMessage").show();
                    }
                    else
                    {
                        var state = 0;

                        $('.productEntries').each(function()
                        {
                            var entry_id    = $(this).val();
                            var value_x     = $(this).prop("id");

                            if (data_pos.id == entry_id)
                            {
                                var explode          = value_x.split('_');
                                var quantity_id      = explode[2];
                                var quantity         = $('#quantity_'+quantity_id).val();
                                var inc_quantity     = parseFloat(quantity) + 1;

                                $('#quantity_'+quantity_id).val(inc_quantity);
                                $('#quantity_'+quantity_id).change();
                                $('input[name=product_code').val('');
                                calculateActualAmount(quantity_id);

                                state++;
                            }

                            if (entry_id == '')
                            {   
                                var explode          = value_x.split('_');
                                var quantity_id      = explode[2];
                                
                                ProductEntriesList(quantity_id);

                                state++;
                            }
                        });

                        if (state == 0)
                        {   
                            $("#pos_add_button").click();
                        }
                        else
                        {
                            return false;
                        }
                    }

                });
            } 
        }
        
        function ProductCategoryList(x)
        {
            var site_url = $(".site_url").val();

            $.get(site_url + '/bills/product/category/list', function(data){

                var category_list  = '';
               
                $.each(data, function(i, data_list)
                {
                    category_list += '<option value = "' +  data_list.id + '">' + data_list.name + '</option>';
                });
                
                var last_category_text = $("#cat_id option:selected").last().text();
                var last_category_val  = $("#cat_id option:selected").last().val();
    
                category_list  += '<option value = "' + last_category_val + '" selected>' + last_category_text + '</option>';
                
                $("#product_category_id_"+x).append(category_list);
                $("#product_category_id_"+x).change();

            });
        }

        function ProductEntriesList(x,entry_id) 
        {
            //For getting item commission information from items table start
            var site_url  = $(".site_url").val();
            
            $.get(site_url + '/bills/product/entries/category-list/invoice/'+ entry_id, function(data){

                var list5           = '';
                var list7           = '';
                var free_item_list  = '';
                var free_list       = '';
                var checkProduct    = [];

                $.each(data, function(i, data)
                {
                    if (data.stock_in_hand != null)
                    {
                        var stock = data.stock_in_hand;
                    }
                    else
                    {
                        var stock = 0;
                    }

                    if (data.product_type == 2)
                    {
                        var productName = data.name + ' - ' + data.variations;
                    }
                    else
                    {
                        var productName = data.name;
                    }

                    // $('.productEntries').each(function()
                    // {
                    //     var entry_id    = $(this).val();

                    //     if(checkValue(entry_id, checkProduct) == 'Not exist')
                    //     {
                    //         checkProduct.push(entry_id);
                    //     }       
                    // });

                    // if(checkValue(data.id, checkProduct) == 'Not exist')
                    // {

                        list5 += '<option value = "' +  data.id + '">' + productName + '</option>';
                    // }

                    free_item_list += '<option value = "' +  data.id + '">' + productName + '</option>';
                });

                list7           += '<option value = "">' + '--Select Product--' +'</option>';
                free_list       += '<option value = "">' + '--Select Product--' +'</option>';

                $("#product_entries_"+x).empty();
                $("#product_entries_"+x).append(list7);
                $("#product_entries_"+x).append(list5);

                $("#free_items_"+x).empty();
                $("#free_items_"+x).append(free_list);
                $("#free_items_"+x).append(free_item_list);

                var read_code     = $("#product_code").val();

                if (read_code)
                {
                    var product_code  = read_code.replace(/^0+/, '');
                }
                else
                {
                    var product_code  = '';
                }

                if(product_code)
                {
                    $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code, function(data_pos){

                        $('#product_entries_'+x).val(data_pos.id).change();
                    });

                    $('input[name=product_code').val('');
                } 
            });
        }

        function freeProductCheck(x)
        {
            var total       = '';
            var stock_qty           = $('.stockQuantity').map((_,el) => el.value).get();
            var main_products       = $('.productEntries').map((_,el) => el.value).get();
            var main_products_qty   = $('.mainProductQuantity').map((_,el) => el.value).get();
            var free_products       = $('.freeItems').map((_,el) => el.value).get();
            var free_products_qty   = $('.freeProductQuantity').map((_,el) => el.value).get();

            var free_product_id     = $('#free_items_'+x).val();
            var sr_id               = $('#sr_id').val();

            var total_main_quantity     = 0;
            var total_free_quantity     = 0;

            for (var i = 0; i < main_products.length; i++)
            {
                if (free_product_id == main_products[i])
                {  
                    total_main_quantity += parseFloat(main_products_qty[i]);

                    var stockQty    = stock_qty[i];
                }

                if (free_product_id == free_products[i])
                {  
                    total_free_quantity += parseFloat(free_products_qty[i]);
                }
            }

            var total       = parseFloat(total_main_quantity) + parseFloat(total_free_quantity);

            var site_url    = $('.site_url').val();




            // if (free_product_id != undefined)
            if (free_product_id != '')
            {
                $.get(site_url+'/invoices/return-sr-product-stock/' + sr_id + '/' + free_product_id, function(data){

                    var stockQtys   = parseFloat(data.receive_quantity) - parseFloat(data.sold_quantity) - parseFloat(data.return_quantity) - parseFloat(total);

                    if (parseFloat(stockQtys) < 0)
                    {   
                        $("#free_quantity_"+x).val(0);
                        $('#free_item_stock_'+x).val('');
                        $('#free_item_stock_show_'+x).html('Stock : ' + '');
                        $('#free_item_stock_'+x).val(parseFloat(data.receive_quantity) - parseFloat(data.sold_quantity));
                        $('#free_item_stock_show_'+x).html('Stock : ' + (parseFloat(data.receive_quantity) - parseFloat(data.sold_quantity)));
                    }
                    else
                    {
                        if (parseFloat(stockQtys) != NaN)
                        {
                            $('#free_item_stock_show_'+x).html(0);
                            $('#free_item_stock_show_'+x).html('Stock : ' + stockQtys);
                            $('#free_item_stock_'+x).val(0);
                            $('#free_item_stock_'+x).val(stockQtys);
                        }
                        else
                        {
                            $('#free_item_stock_show_'+x).html(0);
                            $('#free_item_stock_show_'+x).html('Stock : 0');
                            $('#free_item_stock_'+x).val(0);
                            $('#free_item_stock_'+x).val(0);
                        }
                    }
                    
                });
            }
        }

        function checkValue(value,arr)
        {
            var status = 'Not exist';
             
            for(var i=0; i<arr.length; i++)
            {
                var name = arr[i];
                if(name == value)
                {
                    status = 'Exist';
                    break;
                }
            }

            return status;
        }
        
        function getProductList(x)
        {
            var entry_id  = $("#product_category_id_"+x).val();
            ProductEntriesList(x,entry_id);
        }

        function getItemPrice(x)
        {
            //For getting item commission information from items table start
            var entry_id  = $("#product_entries_"+x).val();
            var site_url  = $(".site_url").val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    var list    = '';
                    $.each(data.unit_conversions, function(i, data_list)
                    {   
                        list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                    });

                    $("#unit_id_"+x).empty();
                    $("#unit_id_"+x).append(list);

                    if (data.product_entries.stock_in_hand == '' || data.product_entries.stock_in_hand == null)
                    {
                        var stockInHand  = 0;
                    }
                    else
                    {
                        var stockInHand  = data.product_entries.stock_in_hand;
                    }

                    if (data.product_entries.unit_id == null)
                    {
                        var unit  = '';
                    }
                    else
                    {
                        var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                    }

                    $("#rate_"+x).val(parseFloat(data.product_entries.sell_price).toFixed(2));
                    $("#quantity_"+x).val(1);
                    $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
                    $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' ' + unit);
                    $("#main_unit_id_"+x).val(data.product_entries.unit_id);
                    $("#main_unit_name_"+x).val(data.product_entries.unit_name);
                    $("#discount_"+x).val(0);
                    // $("#unit_id_"+x).val(data.product_entries.unit_id).trigger('change');
      
                    calculateActualAmount(x);
                });
            }
            // calculateActualAmount(x);
            //For getting item commission information from items table end
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                    $("#stock_show_"+x).html('Stock : ' + convertedStock + ' ( ' + convertedUnitName + ' )');
                    $("#rate_"+x).val(parseFloat(data.sell_price).toFixed(2));

                    calculateActualAmount(x);
                }

            });
        }

        function getFreeConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#free_items_"+x).val();
            var unit_id             = $("#free_unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    var site_url  = $(".site_url").val();
                    var entry_id  = $("#free_items_"+x).val();
                    var sr_id     = $("#sr_id").val();

                    if(entry_id)
                    {
                        $.get(site_url + '/invoices/products-price-list/'+ entry_id + '/' + sr_id, function(data){

                            if (jQuery.isEmptyObject(data) == false)
                            {   
                                if (data.product_entries != null)
                                {
                                    if (data.product_entries.stock_in_hand == 0 || data.product_entries.stock_in_hand == '')
                                    {
                                        var stockInHand  = 0;
                                    }
                                    else
                                    {
                                        var stockInHand  = data.product_entries.stock_in_hand;
                                    }

                                    if (data.product_entries.unit_id == null)
                                    {
                                        var unit  = '';
                                    }
                                    else
                                    {
                                        var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                                    }
                                }
                                else
                                {
                                     var stockInHand    = 0;
                                     var unit           = '';
                                }
                                
                                $("#free_item_stock_"+x).val(stockInHand);
                                $("#free_item_stock_show_"+x).html('Stock : ' + stockInHand + unit);
                  
                                // calculateActualAmount(x);
                            }
                            else
                            {
                                $("#free_item_stock_"+x).val(0);
                                $("#free_item_stock_show_"+x).html('Stock : 0');
                  
                                // calculateActualAmount(x);
                            }
                            
                        });
                    }
                    // calculateActualAmount(x);
                }
                else
                {   
                    var convertedUnitName = $('#free_unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#free_item_stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                    $("#free_item_stock_"+x).val(convertedStock);
                    $("#free_item_stock_show_"+x).html('Stock : ' + convertedStock + ' ( ' + convertedUnitName + ' )');
                    // calculateActualAmount(x);
                }

            });
        }

        function getFreeItemUnit(x)
        {
            //For getting item commission information from items table start
            var entry_id  = $("#free_items_"+x).val();
            var site_url  = $(".site_url").val();
            var sr_id     = $("#sr_id").val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products-price-list/'+ entry_id + '/' + sr_id, function(data){

                    var list    = '';
                    $.each(data.unit_conversions, function(i, data_list)
                    {   
                        list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                    });

                    if (data.product_entries.stock_in_hand == 0)
                    {
                        var stockInHand  = 0;
                    }
                    else
                    {
                        var stockInHand  = data.product_entries.stock_in_hand;
                    }

                    if (data.product_entries.unit_id == null)
                    {
                        var unit  = '';
                    }
                    else
                    {
                        var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                    }

                    $("#free_item_stock_"+x).val(stockInHand);
                    $("#free_item_stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' ' + unit);
                    $("#free_unit_id_"+x).empty();
                    $("#free_unit_id_"+x).append(list);
                    $("#free_item_main_unit_id_"+x).val(data.product_entries.unit_id);
                });
            }
            //For getting item commission information from items table end
        }

        function pad (str, max)
        {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }

        function addButtonCurrentBalance()
        {
            $('.add_field_button_current_balance').click();
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 500;                          //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = -1;
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                // ProductEntriesList(x);
                ProductCategoryList(x)

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        
                                                        '<select style="width: 100%" class="inner form-control single_select2 productCategory productCategoryId" id="product_category_id_'+x+'" onchange="getProductList('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Category--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +
                                                    
                                                    '<div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_'+x+'" required onchange="getConversionParam('+x+')">\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        
                                                        '<input type="text" name="quantity[]" class="inner form-control mainProductQuantity" id="quantity_'+x+'" placeholder="Quantity" onchange="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' +
                                                                
                                                                '<select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_'+x+'" oninput="calculateActualAmount('+x+')">\n' +
                                                                    '<option value="1">BDT</option>' +
                                                                    '<option value="0" selected>%</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">\n' +
                                                                
                                                                '<input type="text" name="discount[]" class="inner form-control" id="discount_'+x+'" placeholder="Discount" oninput="calculateActualAmount('+x+')"/>\n' +
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' + 
                                                        
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display:none!important;" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 1px" class="col-md-7">' +
                                                                '<select style="width: 100%" name="free_items[]" class="inner form-control single_select2 freeItems" id="free_items_'+x+'" onchange="getFreeItemUnit('+x+')">\n' +
                                                                '<option value="">' + '--Select Product--' + '</option>' +
                                                                '</select>\n' +
                                                                '<p id="free_item_stock_show_'+x+'" style="padding: 0px;margin: 0px">' + 'Stock : 0' + '</p>' +
                                                            '</div>' +

                                                            '<div style="padding-left: 1px" class="col-md-5">' +
                                                                '<select style="width: 100%;cursor: pointer" name="free_unit_id[]" class="inner form-control single_select2" id="free_unit_id_'+x+'" onchange="getFreeConversionParam('+x+')">\n' +
                                                                '</select>\n' +
                                                            '</div>' + 
                                                        '</div>' +
                                                    '</div>\n' + 

                                                    '<input type="hidden" id="free_item_stock_'+x+'">' +
                                                    '<input type="hidden" name="free_item_main_unit_id[]" class="inner form-control" id="free_item_main_unit_id_'+x+'" />\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display:none!important;" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        
                                                        '<input type="text" name="free_quantity[]" class="inner form-control freeProductQuantity" id="free_quantity_'+x+'" value="0" />\n' +
                                                    '</div>\n' +   
                                                    
                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                        '<i style="padding: 0.60rem 0.70rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                    '</div>\n' +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }                                    
        });
        //For apending another rows end

        //For apending another rows start
        $(add_button_pos).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                // ProductEntriesList(x);
                ProductCategoryList(x)

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        
                                                        '<select style="width: 100%" class="inner form-control single_select2 productCategory productCategoryId" id="product_category_id_'+x+'" onchange="getProductList('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Category--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +
                                                    
                                                    '<div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_'+x+'" required onchange="getConversionParam('+x+')">\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        
                                                        '<input type="text" name="quantity[]" class="inner form-control mainProductQuantity" id="quantity_'+x+'" placeholder="Quantity" onchange="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' +
                                                                
                                                                '<select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_'+x+'" oninput="calculateActualAmount('+x+')">\n' +
                                                                    '<option value="1">BDT</option>' +
                                                                    '<option value="0" selected>%</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">\n' +
                                                                
                                                                '<input type="text" name="discount[]" class="inner form-control" id="discount_'+x+'" placeholder="Discount" oninput="calculateActualAmount('+x+')"/>\n' +
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' + 
                                                        
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display:none!important;" class="col-lg-2 col-md-2 col-sm-6 col-12">\n' +
                                                        
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 1px" class="col-md-7">' +
                                                                '<select style="width: 100%" name="free_items[]" class="inner form-control single_select2 freeItems" id="free_items_'+x+'" onchange="getFreeItemUnit('+x+')">\n' +
                                                                '<option value="">' + '--Select Product--' + '</option>' +
                                                                '</select>\n' +
                                                                '<p id="free_item_stock_show_'+x+'" style="padding: 0px;margin: 0px">' + 'Stock : 0' + '</p>' +
                                                            '</div>' +

                                                            '<div style="padding-left: 1px" class="col-md-5">' +
                                                                '<select style="width: 100%;cursor: pointer" name="free_unit_id[]" class="inner form-control single_select2" id="free_unit_id_'+x+'" onchange="getFreeConversionParam('+x+')">\n' +
                                                                '</select>\n' +
                                                            '</div>' + 
                                                        '</div>' +
                                                    '</div>\n' + 

                                                    '<input type="hidden" id="free_item_stock_'+x+'">' +
                                                    '<input type="hidden" name="free_item_main_unit_id[]" class="inner form-control" id="free_item_main_unit_id_'+x+'" />\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px;display:none!important;" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        
                                                        '<input type="text" name="free_quantity[]" class="inner form-control freeProductQuantity" id="free_quantity_'+x+'" value="0" />\n' +
                                                    '</div>\n' +   
                                                    
                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                        '<i style="padding: 0.60rem 0.70rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                    '</div>\n' +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }                                    
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            var rate                    = $("#rate_"+x).val();
            var quantity                = $("#quantity_"+x).val();
            var stock                   = $("#stock_"+x).val();
            var discount                = $("#discount_"+x).val();
            var discountType            = $("#discount_type_"+x).val();
            var vatType                 = $("#vat_type_0").val();
            var vatAmount               = $("#vat_amount_0").val();
            var taxType                 = $("#tax_type_0").val();
            var taxAmount               = $("#tax_amount_0").val();
            var totalDiscount           = $("#total_discount_0").val();
            var totalDiscountType       = $("#total_discount_type_0").val();

            //Checking Overselling Start
            // var stock_qty           = $('.stockQuantity').map((_,el) => el.value).get();
            // var main_products       = $('.productEntries').map((_,el) => el.value).get();
            // var main_products_qty   = $('.mainProductQuantity').map((_,el) => el.value).get();
            // var free_products       = $('.freeItems').map((_,el) => el.value).get();
            // var free_products_qty   = $('.freeProductQuantity').map((_,el) => el.value).get();

            // var main_product_id     = $('#product_entries_'+x).val();
            // var total_main_quantity = 0;
            // var total_free_quantity = 0;

            // for (var i = 0; i < main_products.length; i++)
            // {
            //     if (main_product_id == main_products[i])
            //     {  
            //         total_main_quantity += parseFloat(main_products_qty[i]);
            //     }

            //     if (main_product_id == free_products[i])
            //     {  
            //         total_free_quantity += parseFloat(free_products_qty[i]);
            //     }
            // }

            // var total = parseFloat(stock) - parseFloat(total_main_quantity) - parseFloat(total_free_quantity);

            // if (parseFloat(total) >= 0)
            // {   
            //     $("#quantity_"+x).val(quantity);
            // }
            // else
            // {
            //     $("#quantity_"+x).val('');
            // }
            //Checking Overselling End

            if (rate == '')
            {
                var rateCal             = 0;
            }
            else
            {
                var rateCal             = $("#rate_"+x).val();
            }

            if (quantity == '')
            {
                var quantityCal         = 1;
            }
            else
            {
                var quantityCal         = $("#quantity_"+x).val();
            }

            if (discount == '')
            {
                var discountCal         = 1;
            }
            else
            {
                var discountCal         = $("#discount_"+x).val();
            }

            if (discount == '')
            {
                var discountTypeCal     = 0;
            }
            else
            {
                if (discountType == 0)
                {
                    var discountTypeCal     = (parseFloat(discountCal)*parseFloat(rateCal)*parseFloat(quantityCal))/100;
                }
                else
                {
                    var discountTypeCal     = $("#discount_"+x).val();
                }
            }

            var AmountIn              =  (parseFloat(rateCal)*parseFloat(quantityCal)) - parseFloat(discountTypeCal);
     
            $("#amount_"+x).val(AmountIn);

            //Calculating Subtotal Amount
            var total       = 0;

            $('.amount').each(function()
            {
                total       += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(total.toFixed());
            $("#subTotalBdtShow").val(total.toFixed());

            if (vatAmount == '')
            {   
                $("#vat_amount_0").val(0);
                var vatCal         = 0;
            }
            else
            {
                var vatCal         = $("#vat_amount_0").val();
            }

            if (vatType == 0)
            {
                var vatTypeCal     = (parseFloat(vatCal)*(parseFloat(total)))/100;
            }
            else
            {
                var vatTypeCal     = $("#vat_amount_0").val();
            }

            if (taxAmount == '')
            {   
                $("#tax_amount_0").val(0);
                var taxCal         = 0;
            }
            else
            {
                var taxCal         = $("#tax_amount_0").val();
            }

            if (taxType == 0)
            {
                var taxTypeCal     = (parseFloat(taxCal)*(parseFloat(total)))/100;
            }
            else
            {
                var taxTypeCal     = $("#tax_amount_0").val();
            }

            if (totalDiscount > 0)
            {   
                if (totalDiscountType == 0)
                {
                    var totalDiscountTypeCal     = (parseFloat(totalDiscount)*(parseFloat(total) + parseFloat(vatTypeCal)))/100;
                }
                else
                {
                    var totalDiscountTypeCal     = $("#total_discount_0").val();
                }
            }
            else
            {
                var totalDiscountTypeCal     = 0;
            }

            var totalShow = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountTypeCal);

            $("#totalBdtShow").val(totalShow.toFixed());
            $("#totalBdt").val(parseFloat(totalShow).toFixed());

            var preBal      = $("#balance").val();
            var preBalType  = $("#bbBalance").val();

            if (preBalType == 1)
            {
                var netPaShowVal   = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountTypeCal) + parseFloat(preBal);
            }

            if (preBalType == 2)
            {
                var netPaShowVal   = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountTypeCal) - parseFloat(preBal);
            }

            $("#netPa").val(parseFloat(netPaShowVal).toFixed());

            calculateChangeAmount();
            freeProductCheck(x)

            var type = $("#bbBalance").val();

            if (type == 2)
            {
                var site_url        = $('.site_url').val();
                var customerId      = $("#customer_id").val();

                $.get(site_url + '/invoices/adjust-advance-payment/' + customerId, function(data){

                    var payable  = $("#totalBdtShow").val();

                    if (parseFloat(payable) <= parseFloat(data))
                    {
                        $("#adjustment").val(parseFloat(payable).toFixed());
                    }
                    else
                    {
                        $("#adjustment").val(parseFloat(data).toFixed());
                    }

                    $("#customer_advance_amount").val(parseFloat(data).toFixed());
                    calculateChangeAmount();
                });
            }
        }
    </script>

    <script type="text/javascript">
        var max_fields_current_balance = 50;                           //maximum input boxes allowed
        var wrapper_current_balance    = $(".input_fields_wrap_current_balance");      //Fields wrapper
        var add_button_current_balance = $(".add_field_button_current_balance");       //Add button ID
        var index_no_current_balance   = 1;

        //For apending another rows start
        var c = -1;
        $(add_button_current_balance).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(c < max_fields_current_balance)
            {   
                c++;

                var serial = c + 1;

                if (serial == 1)
                {
                    var amount_paid_label   = '<label class="hidden-xs" for="productname"> Paid *</label>\n';
                    var paid_through_label  = '<label class="hidden-xs" for="productname">Paid Through *</label>\n';
                    var account_info_label  = '<label class="hidden-xs" for="productname">Acc/Info</label>\n';
                    var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button_current_balance">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonCurrentBalance()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var amount_paid_label   = '';
                    var paid_through_label  = '';
                    var account_info_label  = '';
                    var note_label          = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field_current_balance" data-val="'+c+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRowCurrentBalance').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_current_balance_'+c+'">' +
                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid *</label>\n' +
                                                        amount_paid_label +
                                                        '<input type="text" name="current_balance_amount_paid[]" class="form-control currentBalancePaidAmount" id="current_balance_amount_paid_'+c+'" value="0" oninput="checkCurrentBalance('+c+')" required />\n' + 
                                                    '</div>\n' +

                                                    '<div style="padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid Through *</label>\n' +
                                                        paid_through_label  +
                                                        '<select id="current_balance_paid_through_'+c+'" style="cursor: pointer;width: 100%" name="current_balance_paid_through[]" class="form-control single_select2">\n' +
                                                            '@if(!empty($paid_accounts) && ($paid_accounts->count() > 0))\n' +
                                                            '@foreach($paid_accounts as $key => $paid_account)\n' +
                                                                '<option value="{{ $paid_account['id'] }}">{{ $paid_account['account_name'] }}</option>\n' +
                                                            '@endforeach\n' +
                                                            '@endif\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Acc/Info</label>\n' +
                                                        account_info_label +
                                                        '<input type="text" name="current_balance_account_information[]" class="form-control" id="current_balance_account_information_'+c+'" placeholder="Account Information"/>\n' + 
                                                    '</div>\n' + 

                                                    '<div style="padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                        note_label +
                                                        '<input type="text" name="current_balance_note[]" class="form-control" id="current_balance_note_'+c+'" placeholder="note"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +

                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
                }                                    
        });
        //For apending another rows end

        $(wrapper_current_balance).on("click",".remove_field_current_balance", function(e)
        {
            e.preventDefault();

            var c = $(this).attr("data-val");

            $('.di_current_balance_'+c).remove(); c--;

            calculateActualAmount(c);
        });
    </script>

    <script type="text/javascript">
        $(document).on("change", "#defaultCheck2" , function() {

            var checkbox_value    = $("#defaultCheck2")[0].checked;

            if (checkbox_value == true)
            {
                $("#vatShow").show();
                $("#taxShow").show();
            }
            else
            {
                $("#vatShow").hide();
                $("#taxShow").hide();
            }
        });

        function couponMembership()
        {
            var site_url      = $(".site_url").val();
            var coupon_code   = $("#coupon_code").val();
    
            $('.DiscountType').val(1);
            $('.DiscountAmount').val(0);

            $.get(site_url + '/invoices/search/coupon-code/'+ coupon_code, function(data_coupon){

                if (data_coupon == '')
                {
                    alert('Invalid Coupon Code or Membership Card !!');
                    $('#coupon_code').val('');
                }
                else
                {
                    var state = 0;

                    $('.productEntries').each(function()
                    {
                        var entry_id    = $(this).val();
                        var value_x     = $(this).prop("id");
                        
                        if (data_coupon[0].product_id != null)
                        {
                            for (var i = data_coupon.length - 1; i >= 0; i--)
                            {   
                                if (data_coupon[i].product_id == entry_id)
                                {
                                    var explode    = value_x.split('_');
                                    var di_id      = explode[2];

                                    $('#discount_type_'+di_id).val(data_coupon[i].discount_type);
                                    $('#discount_'+di_id).val(data_coupon[i].discount_amount);
                                    
                                    calculateActualAmount(di_id);

                                    state++;
                                }
                            }
                        }
                        else
                        {
                            var explode    = value_x.split('_');
                            var di_id      = explode[2];

                            $('#discount_type_'+di_id).val(data_coupon[0].discount_type);
                            $('#discount_'+di_id).val(data_coupon[0].discount_amount);
                            
                            calculateActualAmount(di_id);
                        }    
                    });
                }
            });
        }

        function calculateChangeAmount()
        {
            var tAmount             = $("#totalBdtShow").val();
            var balance             = $("#balance").val();
            var cGiven              = $("#cash_given").val();
            var adjustment          = $("#adjustment").val();
            var type                = $("#bbBalance").val();

            if (tAmount != '')
            {
                var totalAmount     = $("#totalBdtShow").val();
            }
            else
            {
                var totalAmount     = 0;
            }

            if (adjustment != '')
            {
                var adjustmentAmount     = $("#adjustment").val();
            }
            else
            {
                var adjustmentAmount     = 0;
            }

            if (cGiven != '')
            {
                var cashGiven       = $("#cash_given").val();
            }
            else
            {
                var cashGiven       = 0;
            }

            if (balance != '')
            {
                var balanceAmount   = $("#balance").val();
            }
            else
            {
                var balanceAmount   = 0;
            }

            var paidAmount  = parseFloat(cashGiven) + parseFloat(adjustmentAmount);

            if (type == 1)
            {   
                var payaAmount  = parseFloat(balanceAmount) + parseFloat(totalAmount);
            }
            else
            {
                var payaAmount  = parseFloat(totalAmount);
            }

            var changeAmount            = parseFloat(paidAmount) - parseFloat(payaAmount);
            var changeAmountOriginal    = parseFloat(paidAmount) - parseFloat(totalAmount);
            
            if (parseFloat(paidAmount) <= parseFloat(totalAmount))
            { 
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(paidAmount).toFixed());
                $("#current_balance_amount_paid_0").val(parseFloat(cashGiven).toFixed());
            }

            if ((parseFloat(paidAmount) > parseFloat(totalAmount)) && (parseFloat(paidAmount) <= parseFloat(payaAmount)))
            {   
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(totalAmount).toFixed());
                $("#current_balance_amount_paid_0").val(parseFloat(cashGiven).toFixed());
            }

            if (parseFloat(paidAmount) > parseFloat(payaAmount))
            {   
                $("#change_amount").val(parseFloat(changeAmount).toFixed());
                $("#amount_paid_0").val(parseFloat(totalAmount).toFixed());
                $("#current_balance_amount_paid_0").val(parseFloat(cashGiven).toFixed());
            }
        }
    </script>

    <script type="text/javascript">
        function printInvoiceList()
        {
            var site_url  = $('.site_url').val();

            $.get(site_url + '/invoices/print-invoices-list', function(data){

                var invoice_list = '';
                var sl_no        = 1;
                $.each(data, function(i, invoice_data)
                {
                    var serial              = parseFloat(i) + 1;
                    var site_url            = $('.site_url').val();
                    var print_url_pos       = site_url + '/invoices/show-pos/' + invoice_data.id;

                    if (invoice_data.customer_name != null)
                    {
                        var customer  = invoice_data.customer_name;
                    }
                    else
                    {
                        var customer  = invoice_data.contact_name;
                    }

                    invoice_list += '<tr>' +
                                        '<td>' +
                                            sl_no +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(invoice_data.invoice_date) +
                                        '</td>' +
                                        '<td>' +
                                            'INV - ' + invoice_data.invoice_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            customer + 
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.invoice_amount +
                                        '</td>' +
                                        '<td>' +
                                           (parseFloat(invoice_data.invoice_amount) - parseFloat(invoice_data.due_amount)) +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.due_amount +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.user_name +
                                        '</td>' +
                                        '<td>' +
                                            '<a href="'+ print_url_pos +'">' +
                                                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +'<i class="fa fa-print">' + '</i>' +
                                            '</a>' +
                                        '</td>' +
                                    '</tr>';

                                    sl_no++;
                });

                $("#print_invoice_list").empty();
                $("#print_invoice_list").append(invoice_list);
                
            });
        }

        function printInvoicesSearch()
        {
            var site_url            = $('.site_url').val();
            var date_val            = $('#search_date').val();
            var invoiceNumber_val   = $('#invoiceNumber').val();
            var customer_val        = $('#customer').val();

            if (date_val != '')
            {
                var date = $('#search_date').val();
            }
            else
            {
                var date  = 0;
            }

            if (invoiceNumber_val != '')
            {
                var invoiceNumber = $('#invoiceNumber').val();
            }
            else
            {
                var invoiceNumber  = 0;
            }

            if (customer_val != '')
            {
                var customer = $('#customer').val();
            }
            else
            {
                var customer  = 0;
            }

            $.get(site_url + '/invoices/print-invoices-search/' + date + '/' + customer + '/' + invoiceNumber , function(data){

                var invoice_list = '';
                var sl_no        = 1;
                $.each(data, function(i, invoice_data)
                {
                    var serial              = parseFloat(i) + 1;
                    var site_url            = $('.site_url').val();
                    var print_url_pos       = site_url + '/invoices/show-pos/' + invoice_data.id;

                    if (invoice_data.customer_name != null)
                    {
                        var customer  = invoice_data.customer_name;
                    }
                    else
                    {
                        var customer  = invoice_data.contact_name;
                    }

                    invoice_list += '<tr>' +
                                        '<td>' +
                                            sl_no +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(invoice_data.invoice_date) +
                                        '</td>' +
                                        '<td>' +
                                            'INV - ' + invoice_data.invoice_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            customer + 
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.invoice_amount +
                                        '</td>' +
                                        '<td>' +
                                           (parseFloat(invoice_data.invoice_amount) - parseFloat(invoice_data.due_amount)) +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.due_amount +
                                        '</td>' +
                                        '<td>' +
                                           invoice_data.user_name +
                                        '</td>' +
                                        '<td>' +
                                            '<a href="'+ print_url_pos +'">' +
                                                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +'<i class="fa fa-print">' + '</i>' +
                                            '</a>' +
                                        '</td>' +
                                    '</tr>';

                                    sl_no++;
                });

                $("#print_invoice_list").empty();
                $("#print_invoice_list").append(invoice_list);
                
            });
        }

        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        $(document).on("change", "#customer_id" , function() {

            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            $.get(site_url + '/invoices/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data.balance);

                if (data.type == 1)
                {
                    $("#bbShow").html('Previous Due');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', false);
                }
                else
                {
                    $("#bbShow").html('Advance');
                    $("#bbBalance").val(data.type);
                    $("#adjustAdvancePayment").prop('checked', true);
                }
            });
        });

        $(document).on("change", "#sr_id" , function() {

            var sr_id     = $("#sr_id").val();

            if (sr_id == 0)
            {
                $(".productEntries").prop('disabled', true);
                $(".freeItems").prop('disabled', true);
            }
            else
            {
                $(".productEntries").prop('disabled', false);
                $(".freeItems").prop('disabled', false);
            }
        });

        function adjustAdvancePayment()
        {
            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            if ($('#adjustAdvancePayment').is(":checked"))
            {
                $("#adjustment").val(0);
                $("#customer_advance_amount").val(0);
                calculateChangeAmount();
            }
            else
            {
                $.get(site_url + '/invoices/adjust-advance-payment/' + customerId, function(data){

                    var payable  = $("#totalBdtShow").val();

                    if (parseFloat(payable) <= parseFloat(data))
                    {
                        $("#adjustment").val(parseFloat(payable).toFixed());
                    }
                    else
                    {
                        $("#adjustment").val(parseFloat(data).toFixed());
                    }

                    $("#customer_advance_amount").val(parseFloat(data));
                    calculateChangeAmount();
                });
            }
        }

        function checkBalance(id)
        {
            var total  = 0;
            $('.paidAmount').each(function()
            {
                total  += parseFloat($(this).val());
            });

            var payable_amount  = $("#totalBdt").val();
            var amount_paid     = $("#amount_paid_"+id).val();

            if (parseFloat(total) > parseFloat(payable_amount))
            {
                $("#amount_paid_"+id).val(0);
            }
        }

        function checkCurrentBalance(id)
        {
            var total  = 0;
            $('.currentBalancePaidAmount').each(function()
            {
                total  += parseFloat($(this).val());
            });

            var cash_given      = $("#cash_given").val();
            var amount_paid     = $("#current_balance_amount_paid_"+id).val();

            if (parseFloat(total) > parseFloat(cash_given))
            {
                $("#current_balance_amount_paid_"+id).val(0);
            }
        }
    </script>
@endsection