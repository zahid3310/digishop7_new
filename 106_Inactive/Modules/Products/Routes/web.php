<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('products')->group(function() {
    Route::get('/', 'ProductsController@index')->name('products_index');
    Route::post('/store', 'ProductsController@store')->name('products_store');
    Route::get('/edit/{id}', 'ProductsController@edit')->name('products_edit');
    Route::post('/update/{id}', 'ProductsController@update')->name('products_update');
    Route::get('/product-list-ajax', 'ProductsController@productListAjax');
    Route::get('/barcode-print', 'ProductsController@barcodePrint')->name('products_barcode_print');
    Route::post('/barcode-print-print', 'ProductsController@barcodePrintPrint')->name('products_barcode_print_print');
    Route::get('/barcode-print-ajax/{product_id}/{product_entry_id}', 'ProductsController@barcodePrintAjax')->name('products_barcode_print_ajax');
    Route::get('/product-category-wise-list/{entry_id}', 'ProductsController@productCategoryWiseList')->name('product_category_wise_list');
    Route::get('/product-category-list', 'ProductsController@producCategorytList');
    Route::get('/brand-brand-list', 'ProductsController@productBrandList');
    Route::get('/add-new-category', 'ProductsController@productStoreProduct');
    Route::get('/add-new-brand', 'ProductsController@productStoreBrand');
    Route::get('/add-opening-stock', 'ProductsController@openingStock')->name('products_opening_stock');
    Route::post('/store-opening-stock', 'ProductsController@storeOpeningStock')->name('products_opening_stock_store');

    Route::get('/category-index', 'ProductsController@categoryIndex')->name('products_category_index');
    Route::post('/category-store', 'ProductsController@categorystore')->name('products_category_store');
    Route::get('/category-edit/{id}', 'ProductsController@categoryEdit')->name('products_category_edit');
    Route::post('/category-update/{id}', 'ProductsController@categoryUpdate')->name('products_category_update');
    Route::get('/get-variation-value-list/{id}', 'ProductsController@variationValuesListLoad')->name('products_variation_value_list_load');
    Route::get('/get-supplier-list/{id}', 'ProductsController@getSupplierList')->name('products_get_supplier_list');
    Route::post('/supplier-list-update', 'ProductsController@supplierListUpdate')->name('products_supplier_list_update');

    Route::get('/get-customer-list/{id}', 'ProductsController@getCustomerList')->name('products_get_customer_list');
    Route::post('/customer-list-update', 'ProductsController@customerListUpdate')->name('products_customer_list_update');
});

Route::prefix('units')->group(function() {
    Route::get('/index', 'ProductsController@unitsIndex')->name('products_units_index');
    Route::post('/store', 'ProductsController@unitsstore')->name('products_units_store');
    Route::get('/edit/{id}', 'ProductsController@unitsEdit')->name('products_units_edit');
    Route::post('/update/{id}', 'ProductsController@unitsUpdate')->name('products_units_update');
});

Route::prefix('products/variations')->group(function() {
    Route::get('/index', 'ProductsController@variationsIndex')->name('products_variations_index');
    Route::post('/store', 'ProductsController@variationsstore')->name('products_variations_store');
    Route::get('/edit/{id}', 'ProductsController@variationsEdit')->name('products_variations_edit');
    Route::post('/update/{id}', 'ProductsController@variationsUpdate')->name('products_variations_update');
});

Route::prefix('areas')->group(function() {
    Route::get('/index', 'ProductsController@areasIndex')->name('products_areas_index');
    Route::post('/store', 'ProductsController@areasStore')->name('products_areas_store');
    Route::get('/edit/{id}', 'ProductsController@areasEdit')->name('products_areas_edit');
    Route::post('/update/{id}', 'ProductsController@areasUpdate')->name('products_areas_update');
});

Route::prefix('stores')->group(function() {
    Route::get('/index', 'ProductsController@storesIndex')->name('products_stores_index');
    Route::post('/store', 'ProductsController@storesStore')->name('products_stores_store');
    Route::get('/edit/{id}', 'ProductsController@storesEdit')->name('products_stores_edit');
    Route::post('/update/{id}', 'ProductsController@storesUpdate')->name('products_stores_update');
});