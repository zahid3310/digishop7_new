<?php

namespace Modules\Products\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Units;
use App\Models\BillEntries;
use App\Models\Branches;
use App\Models\ProductVariations;
use App\Models\ProductVariationValues;
use App\Models\ProductVariationEntries;
use App\Models\ProductSuppliers;
use App\Models\ProductCustomers;
use App\Models\UnitConversions;
use App\Models\Areas;
use App\Models\Stores;
use DB;
use Response;

class ProductsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $product            = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.sell_price) as sell_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                 GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                 GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                 GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                 GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                 GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                 GROUP_CONCAT(DISTINCT products.id) as product_id
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product_id         = array_values($product->sortByDesc('product_code')->take(1)->toArray());

        $product            = $product->sortBy('name')->all();
        $products           = collect($product);
        $units              = Units::orderBy('id', 'ASC')
                                    ->get();
        $variations         = ProductVariations::orderBy('id', 'ASC')->get();
    
        return view('products::index', compact('products', 'product_id', 'units', 'variations'));
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            // 'product_category_id'   => 'required',
            'buying_price'          => 'required',
            'selling_price'         => 'required',
            'product_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $data_find                  = ProductEntries::orderBy('id', 'DESC')
                                                        ->first();

            // $code                       = $data_find != null ? $data_find['product_code'] + 1 : 1;

            $product                    = new ProductEntries;
            // $product->product_id        = $data['product_category_id'];
            $product->brand_id          = $data['brand_id'];
            // $product->supplier_id       = $data['supplier_id'];
            $product->name              = $data['product_name'];

            $product->product_code      = $data['code'];
            $product->sell_price        = $data['selling_price'];
            $product->buy_price         = $data['buying_price'];

            if ($data['unit_id'] != null)
            {
                $product->unit_id       = $data['unit_id'];
            }

            if (($data['stock_quantity'] != null) && ($data['stock_quantity'] > 0))
            {
                $product->stock_in_hand   = $data['stock_quantity'];
                $product->opening_stock   = $data['stock_quantity'];
            }

            // $product->tax_type                      = $data['tax_type'];
            // $product->selling_price_exclusive_tax   = $data['selling_price_exclusive_tax'];
            // $product->vat_percentage                = $data['vat_percentage'];
            // $product->service_charge                = $data['service_charge'];
            $product->status                        = $data['status'];
            $product->alert_quantity                = $data['alert_quantity'];
            $product->product_type                  = $data['product_type'];
            $product->profit_margin                 = $data['profit_margin'];
            $product->profit_margin_type            = $data['profit_margin_type'];
            $product->created_by                    = $user_id;

            if ($product->save())
            {   
                if ($data['product_type'] == 2)
                {
                    foreach ($data['variation_id'] as $key => $value)
                    {   
                        if (($value != null) && ($data['variation_value'] != null))
                        {
                            $variation_values[] = [
                                'product_entry_id'      => $product['id'],
                                'variation_id'          => $value,
                                'variation_value_id'    => $data['variation_value'][$key],
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];
                        }
                    }

                    DB::table('product_variation_entries')->insert($variation_values);
                }

                if (isset($data['conversion_rate']))
                {
                    foreach ($data['conversion_rate'] as $key => $value)
                    {   
                        if ($value > 0)
                        {
                            $unit_conversions[] = [
                                'product_entry_id'  => $product->id,
                                'main_unit_id'      => $data['unit_id'],
                                'converted_unit_id' => $data['converted_unit_id'][$key],
                                'conversion_rate'   => $data['conversion_rate'][$key],
                                'purchase_price'    => $data['purchase_price'][$key],
                                'sell_price'        => $data['sell_price'][$key],
                                'created_by'        => $user_id,
                                'created_at'        => date('Y-m-d H:i:s'),
                            ];
                        }
                    }

                    if (isset($unit_conversions))
                    {
                        DB::table('unit_conversions')->insert($unit_conversions);
                    }
                }

                DB::commit();
                return back()->with("success","Product Added Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $product            = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.profit_margin) as profit_margin,
                                                 GROUP_CONCAT(DISTINCT product_entries.profit_margin_type) as profit_margin_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.sell_price) as sell_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                 GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                 GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                 GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                 GROUP_CONCAT(DISTINCT products.id) as product_id
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();


        $product            = $product->sortBy('name')->all();
        $products           = collect($product);

        $product_entries    = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.profit_margin) as profit_margin,
                                                 GROUP_CONCAT(DISTINCT product_entries.profit_margin_type) as profit_margin_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.sell_price) as sell_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                 GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                 GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                 GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                 GROUP_CONCAT(DISTINCT products.id) as product_id
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->find($id);


        $entry_count        = $product_entries->count();
        $product_id         = ProductEntries::select('product_entries.*')
                                    ->orderBy('product_entries.product_code', 'DESC')
                                    ->first();

        $units              = Units::orderBy('id', 'ASC')
                                    ->get();

        $variation_entries  = ProductVariationEntries::where('product_entry_id', $id)
                                                        ->orderBy('id', 'ASC')
                                                        ->get();

        $varia_entry_count  = $variation_entries->count();
        $variations         = ProductVariations::orderBy('id', 'ASC')->get();
        $variation_values   = ProductVariationValues::orderBy('id', 'ASC')->get();
        $unit_conversions   = UnitConversions::where('product_entry_id', $id)->get();
        $conversion_count   = $unit_conversions->count();

        return view('products::edit', compact('products', 'product_entries', 'entry_count', 'product_id', 'units', 'variation_entries', 'varia_entry_count', 'variations', 'variation_values', 'unit_conversions', 'conversion_count'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            // 'product_category_id'   => 'required',
            'buying_price'          => 'required',
            'selling_price'         => 'required',
            'product_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $product                    = ProductEntries::find($id);
            // $product->product_id        = $data['product_category_id'];
            $product->brand_id          = $data['brand_id'];
            // $product->supplier_id       = $data['supplier_id'];
            $product->name              = $data['product_name'];
            $product->product_code      = $data['code'];
            $product->sell_price        = $data['selling_price'];
            $product->buy_price         = $data['buying_price'];

            if ($data['unit_id'] != null)
            {
                $product->unit_id       = $data['unit_id'];
            }

            $product->stock_in_hand     = $data['stock_quantity'];

            // $product->tax_type                      = $data['tax_type'];
            // $product->selling_price_exclusive_tax   = $data['selling_price_exclusive_tax'];
            // $product->vat_percentage                = $data['vat_percentage'];
            // $product->service_charge                = $data['service_charge'];
            $product->status                        = $data['status'];
            $product->alert_quantity                = $data['alert_quantity'];
            $product->product_type                  = $data['product_type'];
            $product->profit_margin                 = $data['profit_margin'];
            $product->profit_margin_type            = $data['profit_margin_type'];
            $product->created_by                    = $user_id;

            if ($product->save())
            {   
                $find_product_variation_entries = ProductVariationEntries::where('product_entry_id', $product['id'])->delete();
                $find_unit_conversions_entries  = UnitConversions::where('product_entry_id', $product['id'])->delete();

                if ($data['product_type'] == 2)
                {
                    foreach ($data['variation_id'] as $key => $value)
                    {   
                        if (($value != null) && ($data['variation_value'] != null))
                        {
                            $variation_values[] = [
                                'product_entry_id'      => $product['id'],
                                'variation_id'          => $value,
                                'variation_value_id'    => $data['variation_value'][$key],
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];
                        }
                    }

                    DB::table('product_variation_entries')->insert($variation_values);
                }

                if (isset($data['conversion_rate']))
                {
                    foreach ($data['conversion_rate'] as $key => $value)
                    {   
                        if ($value > 0)
                        {
                            $unit_conversions[] = [
                                'product_entry_id'  => $product->id,
                                'main_unit_id'      => $data['unit_id'],
                                'converted_unit_id' => $data['converted_unit_id'][$key],
                                'conversion_rate'   => $data['conversion_rate'][$key],
                                'purchase_price'    => $data['purchase_price'][$key],
                                'sell_price'        => $data['sell_price'][$key],
                                'created_by'        => $user_id,
                                'created_at'        => date('Y-m-d H:i:s'),
                            ];
                        }
                    }

                    if (isset($unit_conversions))
                    {
                        DB::table('unit_conversions')->insert($unit_conversions);
                    }
                }

                DB::commit();
                return redirect()->route('products_index')->with("success","Product Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function barcodePrint()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $bill_id            = isset($_GET['bill_id']) ? $_GET['bill_id'] : 0;

        $product_entries    = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                 GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                 GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product            = $product_entries->sortBy('name')->all();
        $product_entries    = collect($product);

        $bill_entries       = BillEntries::where('bill_entries.bill_id', $bill_id)
                                    ->select('bill_entries.*')
                                    ->get();

        $entry_count        = $bill_entries->count();

        $user_info          = userDetails();

        return view('products::barcodes', compact('product_entries', 'user_info', 'bill_entries', 'bill_id', 'entry_count'));
    }

    public function barcodePrintPrint(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_entry_id.*'  => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $product_entry_id   = $request->product_entry_id;
        $quantity           = $request->quantity;
        $print_type         = $request->type_id;
        $label_size         = $request->label_size;
        $margin             = 'margin-left: '.$request->margin.'px'.';margin-right: '.$request->margin.'px';
        $user_info          = userDetails();

        foreach ($request->product_entry_id as $key => $value)
        {
            $product_entries    = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->where('product_entries.id', $value)
                                    ->orderBy('product_entries.total_sold', 'DESC')
                                    ->select('product_entries.*', 'products.name as product_name')
                                    ->first();

            $data[$product_entries['id']]['product_name_show']      = isset($request->product_name) ? 1 : 0;
            $data[$product_entries['id']]['organization_name_show'] = isset($request->organization_name) ? 1 : 0;
            $data[$product_entries['id']]['sell_price_show']        = isset($request->product_price) ? 1 : 0;
            $data[$product_entries['id']]['name']                   = $product_entries['name'];
            $data[$product_entries['id']]['organization_name']      = $user_info['organization_name'];
            $data[$product_entries['id']]['product_code']           = $product_entries['product_code'];
            $data[$product_entries['id']]['sell_price']             = $product_entries['sell_price'];
            $data[$product_entries['id']]['quantity']               = $quantity[$key];
        }

        return view('products::barcodes_print', compact('product_entries', 'data', 'print_type', 'user_info', 'label_size', 'margin'));
    }

    public function barcodePrintAjax($product_id, $product_entry_id)
    {
        $tables             = TableNameByUsers();
        $table_id           = Auth::user()->associative_contact_id;

        $product_entries    = $tables['product_entries']
                                    ->where('product_entries_'.$table_id.'.product_id', $product_id)
                                    ->orderBy('product_entries_'.$table_id.'.total_sold', 'DESC')
                                    ->select('product_entries_'.$table_id.'.*')
                                    ->get();

        $product_entries->sortBy('name');


        return Response::json($product_entries);
    }

    public function productListAjax()
    {
        $user_id            = Auth::user()->id;
        $user_branch_id     = Auth::user()->branch_id;
        $user_role          = Auth::user()->role;

        $product_entries    = ProductEntries::get();
        $product_entries->sortBy('name');

        return Response::json($product_entries);
    }

    public function producCategorytList()
    {
        $user_id            = Auth::user()->id;
        $user_branch_id     = Auth::user()->branch_id;
        $user_role          = Auth::user()->role;

        $data               = Products::orderBy('products.total_sold', 'DESC')
                                    ->select('products.*')
                                    ->get();

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData      = Products::orderBy('products.total_sold', 'DESC')
                                    ->take(50)
                                    ->get();
        }
        else
        { 
            $search         = $_GET['searchTerm'];   
            $fetchData      = Products::where('products.name', 'LIKE', "%$search%")
                                    ->orderBy('products.total_sold', 'DESC')
                                    ->take(50)
                                    ->get();
        }

        $data = array();
        foreach ($fetchData as $key => $value)
        {
            $data[] = array("id"=>$value['id'], "text"=>$value['name']);
        }

        return Response::json($data);
    }

    public function productBrandList()
    {
        $user_id            = Auth::user()->id;
        $user_branch_id     = Auth::user()->branch_id;
        $user_role          = Auth::user()->role;

        $data               = Categories::orderBy('categories.created_at', 'DESC')
                                            ->select('categories.*')
                                            ->get();

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData      = Categories::orderBy('categories.created_at', 'DESC')
                                            ->take(50)
                                            ->get();
        }
        else
        { 
            $search         = $_GET['searchTerm'];   
            $fetchData      = Categories::where('categories.name', 'LIKE', "%$search%")
                                            ->orderBy('categories.created_at', 'DESC')
                                            ->take(50)
                                            ->get();
        }

        $data = array();
        foreach ($fetchData as $key => $value)
        {
            $data[] = array("id"=>$value['id'], "text"=>$value['name']);
        }

        return Response::json($data);
    }

    public function productStoreProduct(Request $request)
    {
        $user_id                = Auth::user()->id;
        $data                   = $request->all();

        $products               = new Products;
        $products->name         = $data['product_name'];
        $products->status       = 1;
        $products->created_by   = $user_id;

        if ($products->save())
        {   
            return Response::json($products);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function productStoreBrand(Request $request)
    {
        $user_id                = Auth::user()->id;
        $data                   = $request->all();

        $brand                  = new Categories;
        $brand->name            = $data['brand_name'];
        $brand->status          = 1;
        $brand->created_by      = $user_id;

        if ($brand->save())
        {   
            return Response::json($brand);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function openingStock()
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End

        $product_entries    = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->whereNull('product_entries.stock_in_hand')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                 GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                 GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product            = $product_entries->sortBy('name')->all();
        $product_entries    = collect($product);
        $product_category   = Products::all();

        return view('products::opening_stock', compact('product_entries','product_category'));
    }
    
     public function productCategoryWiseList($entry_id)
    {
        $product_entries    = ProductEntries::where('product_entries.product_id',$entry_id)
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->whereNull('product_entries.stock_in_hand')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                 GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                 GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product            = $product_entries->sortBy('name')->all();
        $product_entries    = collect($product);
        return Response::json($product_entries);
        
    }

    public function storeOpeningStock(Request $request)
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End

        $rules = array(
            'product_entry_id'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            foreach ($data['stock_in_hand'] as $key => $value)
            {   
                if ($value != null)
                {
                    $product_entry                       = ProductEntries::find($data['product_entry_id'][$key]);
                    $product_entry->stock_in_hand        = $value;
                    $product_entry->opening_stock        = $value;
                    $product_entry->updated_by           = $user_id;
                    $product_entry->save();
                }
            }

            return back()->with("success","Stock Updated Successfully !!");

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function categoryIndex()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_role          = Auth::user()->role;
        $products           = Products::orderBy('products.id', 'DESC')->get();

        $units              = Units::orderBy('id', 'ASC')->get();

        return view('products::category_index', compact('products', 'units'));
    }

    public function categoryStore(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $product                    = new Products;
            $product->name              = $data['product_name'];
            $product->status            = $data['status'];
            $product->created_by        = $user_id;
            
            if ($product->save())
            {
                return redirect()->route('products_category_index')->with("success","Product Category Created Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function categoryEdit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $products           = Products::orderBy('products.id', 'DESC')
                                        ->get();

        $find_product       = Products::find($id);
        $units              = Units::orderBy('id', 'ASC')
                                        ->get();

        return view('products::category_edit', compact('products', 'find_product', 'units'));
    }

    public function categoryUpdate(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $product                    = Products::find($id);
            $product->name              = $data['product_name'];
            $product->status              = $data['status'];
            $product->updated_by        = $user_id;
            
            if ($product->save())
            {
                return redirect()->route('products_category_index')->with("success","Product Category Updated Successfully !!");
            }

        }catch (\Exception $exception){
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function unitsIndex()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $units              = Units::select('units.*')
                                    ->orderBy('units.id', 'DESC')
                                    ->get();

        return view('products::units_index', compact('units'));
    }

    public function unitsstore(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $table                    = new Units;
            $table->name              = $data['name'];
            $table->created_by        = $user_id;
            
            if ($table->save())
            {
                return redirect()->route('products_units_index')->with("success","Unit Created Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function unitsEdit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $units              = Units::select('units.*')
                                    ->orderBy('units.id', 'DESC')
                                    ->get();

        $find_unit          = Units::find($id);

        return view('products::units_edit', compact('find_unit', 'units'));
    }

    public function unitsUpdate(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $product                    = Units::find($id);
            $product->name              = $data['name'];
            $product->updated_by        = $user_id;
            
            if ($product->save())
            {
                return redirect()->route('products_units_index')->with("success","Unit Updated Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function variationValuesListLoad($id)
    {
        $data    = ProductVariationValues::where('product_variation_id', $id)->get();

        return Response::json($data);
    }

    //Product Variations
    public function variationsIndex()
    {
        $variations     = ProductVariations::leftjoin('product_variation_values', 'product_variation_values.product_variation_id', 'product_variations.id')
                                    ->groupBy('product_variations.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_variations.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_variations.name) as name,
                                                 GROUP_CONCAT(product_variation_values.name SEPARATOR ", ") as product_variation_values
                                                ')
                                    ->orderBy('product_variations.id', 'DESC')
                                    ->get();

        return view('products::variation_index', compact('variations'));
    }

    public function variationsstore(Request $request)
    {
        $rules = array(
            'name'              => 'required',
            'value_name.*'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $product_variation               = new ProductVariations;
            $product_variation->name         = $data['name'];
            $product_variation->created_by   = $user_id;
            
            if ($product_variation->save())
            {   
                foreach ($data['value_name'] as $key => $value)
                {
                    $variation_values[] = [
                        'product_variation_id'      => $product_variation['id'],
                        'name'                      => $value,
                        'created_by'                => $user_id,
                        'created_at'                => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('product_variation_values')->insert($variation_values);

                DB::commit();
                return redirect()->route('products_variations_index')->with("success","Variation Created Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function variationsEdit($id)
    {
        $variations                 = ProductVariations::leftjoin('product_variation_values', 'product_variation_values.product_variation_id', 'product_variations.id')
                                                        ->groupBy('product_variations.id')
                                                        ->selectRaw('GROUP_CONCAT(DISTINCT product_variations.id) as id,
                                                                     GROUP_CONCAT(DISTINCT product_variations.name) as name,
                                                                     GROUP_CONCAT(product_variation_values.name SEPARATOR ", ") as product_variation_values
                                                                    ')
                                                        ->orderBy('product_variations.id', 'DESC')
                                                        ->get();

        $find_variation             = ProductVariations::find($id);
        $variation_values           = ProductVariationValues::where('product_variation_id', $id)->get();
        $variation_values_count     = $variation_values->count();

        return view('products::variation_edit', compact('variations', 'find_variation', 'variation_values', 'variation_values_count'));
    }

    public function variationsUpdate(Request $request, $id)
    {
        $rules = array(
            'name'              => 'required',
            'value_name.*'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $product_variation               = ProductVariations::find($id);
            $product_variation->name         = $data['name'];
            $product_variation->updated_by   = $user_id;
            
            if ($product_variation->save())
            {   
                foreach ($data['value_name'] as $key => $value)
                {   
                    if ($data['type'][$key] == 0)
                    {
                        $find_old_data                = ProductVariationValues::find($data['value_ids'][$key]);
                        $find_old_data->name          = $value;
                        $find_old_data->updated_by    = $user_id;
                        $find_old_data->save();
                    }

                    if ($data['type'][$key] == 1)
                    {
                        $variation_values[] = [
                            'product_variation_id'      => $product_variation['id'],
                            'name'                      => $value,
                            'created_by'                => $user_id,
                            'created_at'                => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                if (isset($variation_values))
                {
                    DB::table('product_variation_values')->insert($variation_values);
                }

                DB::commit();
                return redirect()->route('products_variations_index')->with("success","Variation Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function getSupplierList($product_id)
    {
        $suppliers    = ProductSuppliers::leftjoin('customers', 'customers.id', 'product_suppliers.supplier_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'product_suppliers.product_entry_id')
                                        ->where('product_suppliers.product_entry_id', $product_id)
                                        ->selectRaw('product_suppliers.*, customers.name as supplier_name, product_entries.name as product_name')
                                        ->get();

        return Response::json($suppliers);
    }

    public function supplierListUpdate(Request $request)
    {
        $rules = array(
            'product_id'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id      = Auth::user()->id;
        $data         = $request->all();

        DB::beginTransaction();

        try{
            $data_delete  = ProductSuppliers::where('product_entry_id', $data['product_id'])->delete();

            foreach ($data['sup_id'] as $key => $value)
            {
                $suppliers[] = [
                    'product_entry_id'     => $data['product_id'],
                    'supplier_id'          => $data['sup_id'][$key],
                    'whole_sale_price'     => $data['whole_rate'][$key],
                    'retail_price'         => $data['retail_rate'][$key],
                    'created_by'           => $user_id,
                    'created_at'           => date('Y-m-d H:i:s'),
                ];
            }

            if (isset($suppliers))
            {
                DB::table('product_suppliers')->insert($suppliers);

                DB::commit();

                return redirect()->route('products_index')->with("success","Suppliers List Updated Successfully !!");
            }

            DB::rollback();
            return redirect()->route('products_index')->with("unsuccess","Not Updated !!");

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function getCustomerList($product_id)
    {
        $customers      = ProductCustomers::leftjoin('customers', 'customers.id', 'product_customers.customer_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'product_customers.product_entry_id')
                                        ->where('product_customers.product_entry_id', $product_id)
                                        ->selectRaw('product_customers.*, customers.name as supplier_name, product_entries.name as product_name')
                                        ->get();

        return Response::json($customers);
    }

    public function customerListUpdate(Request $request)
    {
        $rules = array(
            'product_id_customer'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id      = Auth::user()->id;
        $data         = $request->all();

        DB::beginTransaction();

        try{
            $data_delete  = ProductCustomers::where('product_entry_id', $data['product_id_customer'])->delete();

            foreach ($data['cus_id'] as $key => $value)
            {
                $customers[] = [
                    'product_entry_id'     => $data['product_id_customer'],
                    'customer_id'          => $data['cus_id'][$key],
                    'whole_sale_price'     => $data['whole_rate'][$key],
                    'retail_price'         => $data['retail_rate'][$key],
                    'created_by'           => $user_id,
                    'created_at'           => date('Y-m-d H:i:s'),
                ];
            }

            if (isset($customers))
            {
                DB::table('product_customers')->insert($customers);

                DB::commit();

                return redirect()->route('products_index')->with("success","Customers List Updated Successfully !!");
            }

            DB::rollback();
            return redirect()->route('products_index')->with("unsuccess","Not Updated !!");

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    //Areas
    public function areasIndex()
    {
        $areas  = Areas::select('areas.*')->orderBy('areas.id', 'DESC')->get();

        return view('products::areas.index', compact('areas'));
    }

    public function areasStore(Request $request)
    {
        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $area                    = new Areas;
            $area->name              = $data['name'];
            $area->created_by        = $user_id;
            
            if ($area->save())
            {
                return redirect()->route('products_areas_index')->with("success","Area Created Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function areasEdit($id)
    {
        $areas      = Areas::select('areas.*')->orderBy('areas.id', 'DESC')->get();
        $find_area  = Areas::find($id);

        return view('products::areas.edit', compact('areas', 'find_area'));
    }

    public function areasUpdate(Request $request, $id)
    {
        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $area                    = Areas::find($id);
            $area->name              = $data['name'];
            $area->updated_by        = $user_id;
            
            if ($area->save())
            {
                return redirect()->route('products_areas_index')->with("success","Area Updated Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    //Stores
    public function storesIndex()
    {
        $stores  = Stores::select('stores.*')->orderBy('stores.id', 'DESC')->get();

        return view('products::stores.index', compact('stores'));
    }

    public function storesStore(Request $request)
    {
        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $stores                    = new Stores;
            $stores->name              = $data['name'];
            $stores->address           = $data['address'];
            $stores->phone             = $data['phone'];
            $stores->email             = $data['email'];
            $stores->created_by        = $user_id;
            
            if ($stores->save())
            {
                return redirect()->route('products_stores_index')->with("success","Stores Created Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function storesEdit($id)
    {
        $stores      = Stores::select('stores.*')->orderBy('stores.id', 'DESC')->get();
        $find_store  = Stores::find($id);

        return view('products::stores.edit', compact('stores', 'find_store'));
    }

    public function storesUpdate(Request $request, $id)
    {
        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $stores                    = Stores::find($id);
            $stores->name              = $data['name'];
            $stores->address           = $data['address'];
            $stores->phone             = $data['phone'];
            $stores->email             = $data['email'];
            $stores->updated_by        = $user_id;
            
            if ($stores->save())
            {
                return redirect()->route('products_stores_index')->with("success","Stores Updated Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }
}
