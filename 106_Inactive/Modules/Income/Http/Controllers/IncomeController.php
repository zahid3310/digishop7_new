<?php

namespace Modules\Income\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Customers;
use App\Models\Users;
use App\Models\Incomes;
use App\Models\Accounts;
use App\Models\JournalEntries;
use DB;
use Response;

class IncomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $incomes        = Incomes::orderBy('incomes.created_at', 'DESC')
                                    ->select('incomes.*')
                                    ->get();

        $accounts       = Accounts::where('parent_account_type_id',3)
                                    ->whereNotIn('id', [2,3,4,5,6,7,8,9,10,11])
                                    ->where('status', 1)
                                    ->get();

        $paid_accounts  = Accounts::where('account_type_id', 4)
                                    ->whereNotIn('id', [2,3])
                                    ->where('status', 1)
                                    ->get();

        return view('income::index', compact('incomes', 'paid_accounts', 'accounts'));
    }

    public function create()
    {
        return view('income::create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'income_date'          => 'required',
            'amount'               => 'required',
            'paid_through'         => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $data_find                      = Incomes::orderBy('created_at', 'DESC')->first();
            $income_number                  = $data_find != null ? $data_find['income_number'] + 1 : 1;
            $income                         = new Incomes;
            $income->income_number          = $income_number;
            $income->income_date            = date('Y-m-d', strtotime($data['income_date']));
            $income->customer_id            = $data['customer_id'];
            $income->amount                 = $data['amount'];
            $income->paid_through_id        = $data['paid_through'];
            $income->account_id       		= $data['account_id'];
            $income->account_information    = $data['account_information'];
            $income->note                   = $data['note'];
            $income->created_by             = $user_id;

            if ($income->save())
            {   
                //Financial Accounting Part Start
                debit($customer_id=$data['customer_id'], $date=$data['income_date'], $account_id=$data['paid_through'], $amount=$data['amount'], $note=$data['note'], $transaction_head='income', $income_id=$income->id, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                credit($customer_id=$data['customer_id'], $date=$data['income_date'], $account_id=$data['account_id'], $amount=$data['amount'], $note=$data['note'], $transaction_head='income', $income_id=$income->id, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //Financial Accounting Part End

                DB::commit();
                return back()->with("success","Income Created Successfully !!")->with('income_date', $data['income_date']);
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function categoryStore(Request $request)
    {
        $rules = array(
            'income_category_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $income_categories              = new IncomeCategories;
            $income_categories->name        = $data['income_category_name'];
            $income_categories->created_by  = $user_id;

            if ($income_categories->save())
            {   
                DB::commit();
                return back()->with("success","Category Created Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        return view('income::show');
    }

    public function edit($id)
    {
        $incomes        = Incomes::orderBy('incomes.created_at', 'DESC')
                                    ->select('incomes.*')
                                    ->get();

        $income_find    = Incomes::orderBy('incomes.created_at', 'DESC')
                                    ->select('incomes.*')
                                    ->find($id);

        $accounts       = Accounts::where('parent_account_type_id',3)
                                    ->whereNotIn('id', [2,3,4,5,6,7,8,9,10,11])
                                    ->where('status', 1)
                                    ->get();

        $paid_accounts  = Accounts::where('account_type_id', 4)
                                    ->whereNotIn('id', [2,3])
                                    ->where('status', 1)
                                    ->get();

        return view('income::income_edit', compact('incomes', 'income_find', 'paid_accounts', 'accounts'));
    }

    public function categoryEdit($id)
    {
        $income_categories      = IncomeCategories::where('income_categories.id', '!=', 1)
                                                    ->where('income_categories.id', '!=', 2)
                                                    ->get();
                                                    
        $incomes                = Incomes::leftjoin('income_categories', 'income_categories.id', 'incomes.income_category_id')
                                                    ->orderBy('incomes.created_at', 'DESC')
                                                    ->select('incomes.*',
                                                     'income_categories.name as category_name')
                                                    ->get();

        $income_category_find   = IncomeCategories::find($id);

        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();

        return view('income::income_category_edit', compact('income_categories', 'incomes', 'income_category_find', 'paid_accounts'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'income_date'          => 'required',
            'amount'               => 'required',
            'paid_through'         => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $income                        = Incomes::find($id);
            $income->income_date           = date('Y-m-d', strtotime($data['income_date']));
            $income->customer_id           = $data['customer_id'];
            $income->amount                = $data['amount'];
            $income->paid_through_id       = $data['paid_through'];
            $income->account_id       	   = $data['account_id'];
            $income->account_information   = $data['account_information'];
            $income->note                  = $data['note'];
            $income->updated_by            = $user_id;

            if ($income->save())
            {   
                $jour_ent_debit     = JournalEntries::where('income_id', $income->id)
                                        ->where('transaction_head', 'income')
                                        ->where('debit_credit', 1)
                                        ->first();

                $jour_ent_credit    = JournalEntries::where('income_id', $income->id)
                                        ->where('transaction_head', 'income')
                                        ->where('debit_credit', 0)
                                        ->first();
                //Financial Accounting Part Start
                debitUpdate($jour_ent_debit['id'], $customer_id=$data['customer_id'], $date=$data['income_date'], $account_id=$data['paid_through'], $amount=$data['amount'], $note=$data['note'], $transaction_head='income', $income_id=$income->id, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                creditUpdate($jour_ent_credit['id'], $customer_id=$data['customer_id'], $date=$data['income_date'], $account_id=$data['account_id'], $amount=$data['amount'], $note=$data['note'], $transaction_head='income', $income_id=$income->id, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //Financial Accounting Part End

                DB::commit();
                return redirect()->route('incomes_index')->with("success","Income Updated Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function categoryUpdate(Request $request, $id)
    {
        $rules = array(
            'income_category_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $income_categories              = IncomeCategories::find($id);
            $income_categories->name        = $data['income_category_name'];
            $income_categories->updated_by  = $user_id;

            if ($income_categories->save())
            {   
                DB::commit();
                return redirect()->route('incomes_index')->with("success","Category Updated Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function destroy($id)
    {
    }

    public function incomeListLoad()
    {
        $data           = Incomes::leftjoin('income_categories', 'income_categories.id', 'incomes.income_category_id')
                                ->orderBy('incomes.income_date', 'DESC')
                                ->select('incomes.*',
                                         'income_categories.name as category_name')
                                ->take(200)
                                ->get();

        return Response::json($data);
    }

    public function incomeListSearch($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data           = Incomes::leftjoin('income_categories', 'income_categories.id', 'incomes.income_category_id')
                                ->where('income_categories.name', 'LIKE', "%$id%")
                                ->orWhere('incomes.income_date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number, $table_id) {
                                    return $query->orWhere('incomes.income_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('incomes.created_at', 'DESC')
                                ->select('incomes.*',
                                         'income_categories.name as category_name')
                                ->take(100)
                                ->get();
        }
        else
        {
            $data           = Incomes::leftjoin('income_categories', 'income_categories.id', 'incomes.income_category_id')
                                ->orderBy('incomes.created_at', 'DESC')
                                ->select('incomes.*',
                                         'income_categories.name as category_name')
                                ->take(100)
                                ->get();
        }
        
        return Response::json($data);
    }
}
