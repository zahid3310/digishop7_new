@extends('layouts.app')

@section('title', 'Edit Issue To DSR')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Order</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">Edit Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                            Payment Successfull !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                            Payment Not Added !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('issues_update', $find_issue['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                {{ csrf_field() }}

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 625px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-8 input_fields_wrap getMultipleRow">
                                        @foreach($find_issue_entries as $key => $value)
                                            <div style="margin-bottom: 0px !important" class="row di_{{$key}}">
                                                <div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-12">
                                                    <label class="show-xs" for="productname">Category *</label>
                                                    <select style="width: 100%" name="product_category[]" class="inner form-control single_select2 productCategory" id="product_category_id_{{$key}}" onchange="getProductList({{$key}})" required>
                                                        <option value="{{ $value['product_id'] }}">{{ $value->product->name }}</option>
                                                    </select>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-4 col-md-4 col-sm-6 col-12">
                                                    <label class="show-xs" for="productname">Product *</label>
                                                    <select style="width: 100%" name="product_entries[]" class="inner form-control select2 productEntries" id="product_entries_{{$key}}" onchange="getItemPrice({{$key}})" required>
                                                        <option value="{{ $value['item_id'] }}">
                                                        {{ $value['item_name'] . ' ' . '( ' . str_pad($value['product_code'], 6, "0", STR_PAD_LEFT) . ' )' }}
                                                        </option>
                                                    </select>
                                                    <span id="stock_show_{{$key}}" style="color: black">{{ 'Stock : '.round($value['stock_in_hand'], 2) . ' ( ' . $value->convertedUnit->name . ' )' }}</span>
                                                </div>

                                                <input type="hidden" name="stock[]" class="inner form-control" id="stock_{{$key}}" value="{{ $value['stock_in_hand'] + $value['quantity'] }}" oninput="calculateActualAmount({{$key}})" readonly />
                                                <input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_{{$key}}" value="{{ $value['main_unit_id'] }}" />
                                                <input type="hidden" class="inner form-control" id="main_unit_name_{{$key}}" />

                                                <div style="padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                                    <label class="show-xs" for="productname">Unit</label>
                                                    <select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_{{$key}}" required onchange="getConversionParam({{$key}})">
                                                    <option value="{{ $value['conversion_unit_id'] }}">{{ $value->convertedUnit->name }}</option>
                                                    </select>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <label class="show-xs" for="productname">Qty *</label>
                                                    <input type="text" name="quantity[]" class="inner form-control quantityCheck quantity" id="quantity_{{$key}}" value="{{$value['quantity']}}" placeholder="Quantity" oninput="calculateActualAmount({{$key}})" required/>
                                                </div>
                                                
                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <label class="show-xs" for="productname">Action</label>
                                                    <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field" data-val="{{$key}}"></i>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 625px;padding-top: 13px" class="col-md-4">
                                        <div style="margin-bottom: 5px;margin-left: 1px;" class="form-group row">
                                            <i id="add_field_button" style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button col-md-3"></i>
                                        </div>

                                        <hr>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Date *</label>
                                            <div class="col-md-8">
                                                <input id="issue_date" name="issue_date" type="text" value="{{ date('d-m-Y', strtotime($find_issue['issue_date'])) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">DSR Name *</label>
                                            <div class="col-md-8">
                                                <select style="width: 75%" id="sr_id" name="sr_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9" required>
                                                    <option value="{{ $find_issue['sr_id'] }}">{{ $find_issue['sr_name'] }}</option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Op. Balance</label>
                                            <div class="col-md-8">
                                                <input id="balance" name="balance" type="text" value="" class="form-control" value="0">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Note</label>
                                            <div class="col-md-8">
                                                <input id="issue_note" name="issue_note" type="text" class="form-control" value="{{ $find_issue['issue_note'] }}">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Barcode</label>
                                            <div class="col-md-8">
                                                <input type="text" name="product_code" class="inner form-control" id="product_code" placeholder="Scan Barcode" oninput="PosScannerSearch()" autofocus />
                                                <p id="alertMessage" style="display: none">No Product Found !</p>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-4 col-form-label">Sub Total</label>
                                            <div class="col-md-8">
                                                <input type="text" id="subTotalQty" class="form-control">
                                                <input style="display: none" type="text" id="subTotalQtyShow" name="total_quantity" readonly>
                                            </div>
                                        </div>

                                        <br>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Update & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('issues_index') }}">Close</a></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>

                                    <div style="display: none;" class="row justify-content-end">
                                        <div class="col-lg-1 col-md-2 form-group">
                                            <input id="pos_add_button" type="button" class="btn btn-success btn-block inner add_field_button_pos" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New DSR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control commonCustomerClass">
                        </div>
                    </div>

                    <input id="contact_type" name="contact_type" type="hidden" value="4" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Reference</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name1" name="customer_name" type="text" class="form-control commonReferenceClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number1" name="mobile_number" type="text" class="form-control commonReferenceClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address1" name="address" type="text" class="form-control commonReferenceClass">
                        </div>
                    </div>

                    <input id="contact_type1" name="contact_type" type="hidden" value="3" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url    = $('.site_url').val();

            $("#sr_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 4 && result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#sr_id_1").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 4 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#issue_number").select2({
                ajax: { 
                url:  site_url + '/srissues/issues-list/',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            var srId      = $("#sr_id").val();

            $.get(site_url + '/srissues/calculate-opening-balance/' + srId, function(data){

                $("#balance").empty();
                $("#balance").val(data);    
            });

            calculateActualAmount();
        });
    </script>

    <script type="text/javascript">
        function PosScannerSearch()
        {
            $("#alertMessage").hide();

            var site_url      = $(".site_url").val();
            var read_code     = $("#product_code").val();

            if (read_code.length == 6)
            {
                var product_code  = read_code.replace(/^0+/, '');
            }
            else
            {
                var product_code  = '0';
            }

            if(product_code != 0)
            {
                $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code, function(data_pos){

                    if ($.isEmptyObject(data_pos))
                    {   
                        $('input[name=product_code').val('');
                        $("#alertMessage").show();
                    }
                    else
                    {   
                        var state = 0;

                        $('.productEntries').each(function()
                        {
                            var entry_id    = $(this).val();
                            var value_x     = $(this).prop("id");

                            if (data_pos.id == entry_id)
                            {
                                var explode          = value_x.split('_');
                                var quantity_id      = explode[2];
                                var quantity         = $('#quantity_'+quantity_id).val();
                                var inc_quantity     = parseFloat(quantity) + 1;

                                $('#quantity_'+quantity_id).val(inc_quantity);
                                $('#quantity_'+quantity_id).change();
                                $('input[name=product_code').val('');
                                calculateActualAmount(quantity_id);

                                state++;
                            }

                            if (entry_id == '')
                            {   
                                var explode          = value_x.split('_');
                                var quantity_id      = explode[2];
                                
                                ProductEntriesList(quantity_id);

                                state++;
                            }
                        });

                        if (state == 0)
                        {   
                            $("#pos_add_button").click();
                        }
                        else
                        {
                            return false;
                        }
                    }

                });
            } 
        }
        
        function ProductCategoryList(x)
        {
            var site_url = $(".site_url").val();

            $.get(site_url + '/bills/product/category/list', function(data){

                var category_list  = '';
               
                $.each(data, function(i, data_list)
                {
                    category_list += '<option value = "' +  data_list.id + '">' + data_list.name + '</option>';
                });

                $(".productCategory").append(category_list);

            });
        }

        function ProductEntriesList(x,entry_id) 
        {
            //For getting item commission information from items table start
            var site_url = $(".site_url").val();

            $.get(site_url + '/bills/product/entries/category-list/invoice/'+ entry_id, function(data){
                var list5           = '';
                var list7           = '';
                var checkProduct    = [];

                $.each(data, function(i, data)
                {   
                    if (data.stock_in_hand != null)
                    {
                        var stock = data.stock_in_hand;
                    }
                    else
                    {
                        var stock = 0;
                    }

                    if (data.product_type == 2)
                    {
                        var productName = data.name + ' - ' + data.variations;
                    }
                    else
                    {
                        var productName = data.name;
                    }

                    // $('.productEntries').each(function()
                    // {
                    //     var entry_id    = $(this).val();

                    //     if(checkValue(entry_id, checkProduct) == 'Not exist')
                    //     {
                    //         checkProduct.push(entry_id);
                    //     }
                        
                    // });

                    // if(checkValue(data.id, checkProduct) == 'Not exist')
                    // {

                        list5 += '<option value = "' +  data.id + '">' + productName + ' ( ' + pad(data.product_code, 6) + ' )' + '</option>';
                    // }
                });

                list7 += '<option value = "">' + '--Select Product--' +'</option>';

                $("#product_entries_"+x).empty();
                $("#product_entries_"+x).append(list7);
                $("#product_entries_"+x).append(list5);

                var read_code     = $("#product_code").val();

                if (read_code)
                {
                    var product_code  = read_code.replace(/^0+/, '');
                }
                else
                {
                    var product_code  = '';
                }

                if(product_code)
                {
                    $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code, function(data_pos){

                        $('#product_entries_'+x).val(data_pos.id).change();
                    });

                    $('input[name=product_code').val('');
                } 
            });
        }

        function checkValue(value,arr)
        {
            var status = 'Not exist';
             
            for(var i=0; i<arr.length; i++)
            {
                var name = arr[i];
                if(name == value)
                {
                    status = 'Exist';
                    break;
                }
            }

            return status;
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                    $("#stock_show_"+x).html('Stock : ' + convertedStock + ' ( ' + convertedUnitName + ' )');
                    $("#stock_"+x).val(convertedStock);

                    calculateActualAmount(x);
                }

            });
        }
        
        function getProductList(x) 
        {
            var entry_id  = $("#product_category_id_"+x).val();
            ProductEntriesList(x,entry_id);
        }

        function getItemPrice(x)
        {
            //For getting item commission information from items table start
            var entry_id  = $("#product_entries_"+x).val();
            var site_url  = $(".site_url").val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    var list    = '';
                    $.each(data.unit_conversions, function(i, data_list)
                    {   
                        list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                    });

                    $("#unit_id_"+x).empty();
                    $("#unit_id_"+x).append(list);

                    if (data.product_entries.stock_in_hand == '' || data.product_entries.stock_in_hand == null)
                    {
                        var stockInHand  = 0;
                    }
                    else
                    {
                        var stockInHand  = data.product_entries.stock_in_hand;
                    }

                    if (data.product_entries.unit_id == null)
                    {
                        var unit  = '';
                    }
                    else
                    {
                        var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                    }

                    $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
                    $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' ' + unit);
                    $("#main_unit_id_"+x).val(data.product_entries.unit_id);
                    $("#main_unit_name_"+x).val(data.product_entries.unit_name);
                    $("#quantity_"+x).val(0);
      
                    calculateActualAmount(x);
                });
            }
            // calculateActualAmount(x);
        }

        function pad (str, max)
        {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = $("#contact_type").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                            $('.commonCustomerClass').val('');
                        }
                        
                        $("#sr_id").empty();
                        $('#sr_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });   
    </script>

    <script type="text/javascript">
        var max_fields       = 500;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = {{$entries_count}};
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {   
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var category_label       = '<label class="hidden-xs" for="productname">Category *</label>\n';
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var stock_label         = '<label class="hidden-xs" for="productname">Stock</label>\n';
                    var unit_label          = '<label class="hidden-xs" for="productname">Unit</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }
                else
                {
                    var category_label       = '';
                    var product_label       = '';
                    var stock_label         = '';
                    var unit_label          = '';
                    var quantity_label      = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                // ProductEntriesList(x);
                // ProductCategoryList(x)

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                
                                                    '<div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Category *</label>\n' +
                                                        category_label +
                                                        '<select style="width: 100%" name="product_category[]" class="inner form-control single_select2 productCategory" id="product_category_id_'+x+'" onchange="getProductList('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Category--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +
                                                    
                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-4 col-md-4 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" readonly />\n' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +

                                                    '<div style="padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                        unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_'+x+'" required onchange="getConversionParam('+x+')">\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control quantity" id="quantity_'+x+'" placeholder="Quantity" oninput="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
                                            var site_url  = $('.site_url').val();
                                            $("#product_category_id_"+x).select2({
                                                ajax: { 
                                                url:  site_url + '/products/product-category-list',
                                                type: "get",
                                                dataType: 'json',
                                                delay: 250,
                                                data: function (params) {
                                                    return {
                                                        searchTerm: params.term // search term
                                                    };
                                                },
                                                processResults: function (response) {
                                                    return {
                                                        results: response
                                                    };
                                                },
                                                cache: true
                                                }
                                            });
            }                                    
        });
        //For apending another rows end

        //For apending another rows start
        $(add_button_pos).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var category_label       = '<label class="hidden-xs" for="productname">Category *</label>\n';
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var stock_label         = '<label class="hidden-xs" for="productname">Stock</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButton()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var category_label       = '';
                    var product_label       = '';
                    var stock_label         = '';
                    var quantity_label      = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                // ProductEntriesList(x);
                ProductCategoryList(x)
                
                $('.getMultipleRow').append(' ' + '<div class="row di_'+x+'">' +
                                                    
                                                    '<div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Category *</label>\n' +
                                                        category_label +
                                                        '<select style="width: 100%" name="product_category[]" class="inner form-control single_select2 productCategory" id="product_category_id_'+x+'" onchange="getProductList('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Category--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +
                                                    
                                                    '<div style="margin-bottom: 5px" class="col-lg-4 col-md-4 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Stock</label>\n' +
                                                        stock_label +
                                                        '<input type="text" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" readonly />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control quantity" id="quantity_'+x+'" placeholder="Quantity" oninput="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

            }
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            var quantity                = $("#quantity_"+x).val();
            var stock                   = $("#stock_"+x).val();

            if (quantity == '')
            {
                var quantityCal         = 1;
            }
            else
            {
                var quantityCal         = $("#quantity_"+x).val();
            }

            //Checking Overselling Start
            var check_quantity  = parseFloat(quantity);
            var check_stock     = parseFloat(stock);

            if (check_quantity > check_stock)
            {   
                $("#quantity_"+x).val(check_stock);
            }
            //Checking Overselling End

            //Calculating Subtotal Amount
            var total       = 0;

            $('.quantity').each(function()
            {
                total       += parseFloat($(this).val());
            });

            $("#subTotalQty").val(total);
            $("#subTotalQtyShow").val(total);
        }
    </script>

    <script type="text/javascript">
        $(document).on("change", "#customer_id" , function() {

            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            $.get(site_url + '/invoices/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data);
                
            });
        });
    </script>
@endsection