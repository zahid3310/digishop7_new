@extends('layouts.app')

@section('title', 'All Issues')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">All Issues</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Issues</a></li>
                                    <li class="breadcrumb-item active">All Issues</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                            Payment Successfull !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                            Payment Not Added !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div style="min-height: 300px" class="card">
                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {!! Session::get('success') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('unsuccess'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! Session::get('unsuccess') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('errors'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! 'Some required fields are missing..!! Please try again..' !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            <div class="card-body table-responsive">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">From Date</label>
                                            <div class="col-md-8">
                                                <input style="cursor: pointer" id="search_from_date" type="date" class="form-control" value="<?= date("Y-m-d") ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">To Date</label>
                                            <div class="col-md-8">
                                                <input style="cursor: pointer" id="search_to_date" type="date" class="form-control" value="<?= date("Y-m-d") ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">DSR </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="sr_id" class="form-control select2">
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Issue# </label>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <select style="width: 73%" id="issue_number" name="issue_number" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                       <option value="0">All</option>
                                                    </select>
                                                    <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="searchPayment()">
                                                        <i class="bx bx-search font-size-24"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th>Issue#</th>
                                            <th>DSR</th>
                                            <!-- <th>Total Qty.</th> -->
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="issue_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Make New Payment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div style="padding: 0px" class="form-group col-md-4 col-4">
                            <label for="example-text-input" class="col-md-12 col-form-label">Receivable</label>
                            <div class="col-md-12">
                                <input id="receivable" type="text" class="form-control" readonly>
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group col-md-4 col-4">
                            <label for="example-text-input" class="col-md-12 col-form-label">Received</label>
                            <div class="col-md-12">
                                <input id="received" type="text" class="form-control" readonly>
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group col-md-4 col-4">
                            <label for="example-text-input" class="col-md-12 col-form-label">Dues</label>
                            <div class="col-md-12">
                                <input id="dues" type="text" class="form-control" readonly>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <input id="customer_id" name="customer_id" type="hidden">
                    <input id="find_invoice_id" name="invoice_id" type="hidden">

                    <div class="form-group row">
                        <label for="productname" class="col-md-2 col-form-label">Payment Date *</label>
                        <div class="col-md-10">
                            <input style="cursor: pointer" id="payment_date" name="payment_date" type="date" class="form-control" required>
                        </div>
                    </div>

                    <div class="inner-repeater">
                        <div style="margin-bottom: 0px !important" data-repeater-list="inner-group" class="inner form-group row">
                            <div class="inner col-lg-12 input_fields_wrap_payment getMultipleRowPayment">
                                <div style="margin-bottom: 0px !important;" class="row di_payment_0">
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-end">
                            <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="modal-footer">
                        <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Make Payment</button>
                        <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url    = $('.site_url').val();

            $("#sr_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 4 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#issue_number").select2({
                ajax: { 
                url:  site_url + '/srissues/issues-list/',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $.get(site_url + '/srissues/issues/list/load', function(data){

                issueList(data);
                
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var site_url                = $('.site_url').val();
            var search_from_date        = $('#search_from_date').val();
            var search_to_date          = $('#search_to_date').val();
            var search_sr               = $('#sr_id').val();
            var search_issue_number     = $('#issue_number').val();

            if (search_from_date != '')
            {
                var from_date = $('#search_from_date').val();
            }
            else
            {
                var from_date = 0;
            }

            if (search_to_date != '')
            {
                var to_date = $('#search_to_date').val();
            }
            else
            {
                var to_date = 0;
            }

            if (search_sr != '')
            {
                var sr = $('#sr_id').val();
            }
            else
            {
                var sr = 0;
            }

            if (search_issue_number != '')
            {
                var issue = $('#issue_number').val();
            }
            else
            {
                var issue = 0;
            }

            $.get(site_url + '/srissues/issue/search/list/' + from_date + '/' + to_date + '/' + sr + '/' +issue, function(data){

                issueList(data);

            });
        }
    </script>

    <script type="text/javascript">
        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>

    <script type="text/javascript">
        function issueList(data)
        {   
            var issue_list = '';
            var sl_no        = 1;
            $.each(data, function(i, issue_data)
            {
                var serial          = parseFloat(i) + 1;
                var site_url        = $('.site_url').val();
                var edit_url        = site_url + '/srissues/edit/' + issue_data.id;
                var print_url       = site_url + '/srissues/show/' + issue_data.id;

                if (issue_data.sr_name != null)
                {
                    var sr  = issue_data.sr_name;
                }
                else
                {
                    var sr  = issue_data.sr_name;
                }

                issue_list += '<tr>' +
                                    '<td>' +
                                        sl_no +
                                    '</td>' +
                                    '<td>' +
                                       formatDate(issue_data.issue_date) +
                                    '</td>' +
                                    '<td>' +
                                        'SIN - ' + issue_data.issue_number.padStart(6, '0') +
                                    '</td>' +
                                    '<td>' +
                                        sr + 
                                    '</td>' +
                                    // '<td>' +
                                    //    issue_data.total_quantity +
                                    // '</td>' +
                                    '<td>' +
                                        '<div class="dropdown">' +
                                            '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                            '</a>' +
                                            '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                '@if(Auth::user()->role == 1)' +
                                                '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                '@endif' +
                                                '<a class="dropdown-item" href="' + print_url +'" target="_blank">' + 'Show' + '</a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</td>' +
                                '</tr>';

                                sl_no++;
            });

            $("#issue_list").empty();
            $("#issue_list").append(issue_list);
        }
    </script>
@endsection