<?php

namespace Modules\Discounts\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\Bills;
use App\Models\InvoiceEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\ExpenseCategories;
use App\Models\Expenses;
use App\Models\Transactions;
use App\Models\Discounts;
use App\Models\DiscountProducts;
use App\Models\Users;
use DB;
use Response;

class DiscountsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $products               = ProductEntries::orderBy('product_entries.created_at', 'DESC')
                                        ->where('product_entries.status', 1)
                                        ->select('product_entries.*')
                                        ->get();

        $coupon_code            = Discounts::orderBy('discounts.coupon_code', 'DESC')
                                        ->select('discounts.coupon_code')
                                        ->first();

        $discounts             = Discounts::orderBy('discounts.coupon_code', 'DESC')
                                        ->select('discounts.*')
                                        ->get();

        $coupon_number          = $coupon_code != null ? $coupon_code['coupon_code'] + 1 : 1001;

        return view('discounts::index', compact('products', 'coupon_number', 'discounts'));
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('expenses::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'type'              => 'required',
            'discount_type'     => 'required',
            'amount'            => 'required',
            'issue_date'        => 'required',
            'expire_date'       => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $data_find                      = Discounts::orderBy('coupon_code', 'DESC')->first();
            $coupon_number                  = $data_find != null ? $data_find['coupon_code'] + 1 : 1001;
   
            $discounts                      = new Discounts;
            $discounts->type                = $data['type'];

            if ($data['card_number'] == null)
            {
                $discounts->coupon_code     = $data['coupon_code'];
            }
            
            $discounts->card_number         = $data['card_number'];
            $discounts->name                = $data['customer_name'];
            $discounts->phone_number        = $data['customer_phone'];
            $discounts->discount_type       = $data['discount_type'];
            $discounts->discount_amount     = $data['amount'];
            $discounts->issue_date          = date('Y-m-d', strtotime($data['issue_date']));
            $discounts->expire_date         = date('Y-m-d', strtotime($data['expire_date']));
            $discounts->note                = $data['note'];
            $discounts->status              = $data['status'];
            $discounts->created_by          = $user_id;

            if ($discounts->save())
            {   
                if (isset($data['product_id']))
                {
                    foreach ($data['product_id'] as $key => $value)
                    {
                        $discount_products = [
                            'discount_id'      => $discounts['id'],
                            'product_id'       => $value,
                            'created_by'       => $user_id,
                        ];
                    }

                    DB::table('discount_products')->insert($discount_products);
                }

                DB::commit();
                return back()->with("success","Discount Created Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('expenses::show');
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $discount_products      = DiscountProducts::leftjoin('product_entries', 'product_entries.id', 'discount_products.product_id')
                                        ->where('discount_products.discount_id', $id)
                                        ->orderBy('discount_products.discount_id', 'DESC')
                                        ->select('discount_products.*', 'product_entries.name as product_name')
                                        ->get();

        $discounts              = Discounts::orderBy('discounts.coupon_code', 'DESC')
                                        ->select('discounts.*')
                                        ->get();

        foreach ($discount_products as $key => $value)
        {
            $product_ids[]  = $value['product_id'];
        }

        if (!empty($product_ids))
        {
            $products               = ProductEntries::where('product_entries.status', 1)
                                        ->whereNotIn('product_entries.id', $product_ids)
                                        ->orderBy('product_entries.created_at', 'DESC')
                                        ->select('product_entries.*')
                                        ->get();
        }
        else
        {
            $products               = ProductEntries::where('product_entries.status', 1)
                                        ->orderBy('product_entries.created_at', 'DESC')
                                        ->select('product_entries.*')
                                        ->get();
        }

        $find_discount          = Discounts::find($id);

        return view('discounts::edit', compact('products', 'discounts', 'find_discount', 'discount_products'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End  

        $rules = array(
            'type'              => 'required',
            'discount_type'     => 'required',
            'amount'            => 'required',
            'issue_date'        => 'required',
            'expire_date'       => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $discounts                      = Discounts::find($id);
            $discounts->type                = $data['type'];
            $discounts->card_number         = $data['card_number'];
            $discounts->name                = $data['customer_name'];
            $discounts->phone_number        = $data['customer_phone'];
            $discounts->discount_type       = $data['discount_type'];
            $discounts->discount_amount     = $data['amount'];
            $discounts->issue_date          = date('Y-m-d', strtotime($data['issue_date']));
            $discounts->expire_date         = date('Y-m-d', strtotime($data['expire_date']));
            $discounts->note                = $data['note'];
            $discounts->status              = $data['status'];
            $discounts->updated_by          = $user_id;

            if ($discounts->save())
            {   
 
                $old_data           = DiscountProducts::where('discount_products.discount_id', $id)
                                            ->delete();

                if (isset($data['product_id']))
                {
                    foreach ($data['product_id'] as $key => $value)
                    {
                        $discount_products[] = [
                            'discount_id'      => $discounts['id'],
                            'product_id'       => $value,
                            'created_by'       => $user_id,
                        ];
                    }

                    DB::table('discount_products')->insert($discount_products);
                }

                DB::commit();
                return redirect()->route('discounts_index')->with("success","Discount Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function destroy($id)
    {
    }
}
