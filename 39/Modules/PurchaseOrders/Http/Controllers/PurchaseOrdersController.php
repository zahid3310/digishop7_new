<?php

namespace Modules\PurchaseOrders\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderEntries;
use App\Models\ProductEntries;
use App\Models\Users;
use Response;
use DB;

class PurchaseOrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('purchaseorders::index');
    }

    public function allBills()
    {
        return view('purchaseorders::all_bills');
    }

    public function create()
    {
        return view('purchaseorders::create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'selling_date'          => 'required|date',
            'vendor_id'             => 'required|integer',
            'product_entries.*'     => 'required|integer',
            'amount.*'              => 'required|numeric',
            'vendor_id'             => 'required|integer',
            'bill_note'             => 'nullable|string',
            'selling_date'          => 'required|date',
            'product_entries.*'     => 'required|integer',
            'rate.*'                => 'required|numeric',
            'quantity.*'            => 'required|numeric',
            'discount_type.*'       => 'required|integer',
            'discount_amount.*'     => 'nullable|numeric',
            'amount.*'              => 'required|numeric',
            'total_amount'          => 'required|numeric',
            'total_discount_type'   => 'nullable|integer',
            'vat_type'              => 'nullable|numeric',
            'vat_amount'            => 'nullable|numeric',
            'total_discount_amount' => 'nullable|numeric',
            'total_discount_note'   => 'nullable|string',
            'cash_given'            => 'nullable|numeric',
            'change_amount'         => 'nullable|numeric',
            'coupon_code'           => 'nullable|numeric',
            'account_information.*' => 'nullable|string',
            'paid_through.*'        => 'nullable|integer',
            'note.*'                => 'nullable|string',
            'amount_paid.*'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            // $adjustment     = $data['adjustment_amount'];
            $vat            = $data['vat_amount'];
            // $tax            = $data['tax_amount'];

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            $data_find                      = PurchaseOrder::orderBy('created_at', 'DESC')->first();
            $bill_number                    = $data_find != null ? $data_find['bill_number'] + 1 : 1;

            $bill                           = new PurchaseOrder;
            $bill->bill_number              = $bill_number;
            $bill->vendor_id                = $data['vendor_id'];
            $bill->bill_date                = date('Y-m-d', strtotime($data['selling_date']));
            $bill->bill_amount              = $data['total_amount'];
            $bill->due_amount               = $data['total_amount'];
            $bill->total_discount           = $discount;
            $bill->bill_note                = $data['bill_note'];
            $bill->total_vat                = $vat;
            $bill->vat_type                 = $data['vat_type'];
            $bill->total_discount_type      = $data['total_discount_type'];
            $bill->total_discount_amount    = $data['total_discount_amount'];
            $bill->total_discount_note      = $data['total_discount_note'];
            $bill->created_by               = $user_id;

            if ($bill->save())
            {
                foreach ($data['product_entries'] as $key1 => $value1)
                {   
                    $product_buy_price       = ProductEntries::find($value1);

                    $bill_entries[] = [
                        'bill_id'            => $bill['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value1,
                        'vendor_id'          => $bill['vendor_id'],
                        'pcs'                => $data['pcs'][$key1] != null ? $data['pcs'][$key1] : 0,
                        'cartoon'            => $data['cartoon'][$key1] != null ? $data['cartoon'][$key1] : 0,
                        'quantity'           => $data['quantity'][$key1],
                        'rate'               => $data['rate'][$key1],
                        'total_amount'       => $data['amount'][$key1],
                        'discount_type'      => $data['discount_type'][$key1],
                        'discount_amount'    => $data['discount'][$key1],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('purchase_order_entries')->insert($bill_entries);

                DB::commit();

                if ($data['print'] == 1)
                {
                    return redirect()->route('purchase_orders_all_po')->with("success","Purchase Created Successfully !!");
                }
                else
                {
                    return redirect()->route('purchase_orders_show', $bill['id']);
                }
                
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        $bill       = PurchaseOrder::leftjoin('customers', 'customers.id', 'purchase_orders.vendor_id')
                                ->select('purchase_orders.*',
                                         'customers.name as customer_name',
                                         'customers.address as address',
                                         'customers.phone as phone')
                                ->find($id);

        $entries    = PurchaseOrderEntries::leftjoin('products', 'products.id', 'purchase_order_entries.product_id')
                                ->leftjoin('product_entries', 'product_entries.id', 'purchase_order_entries.product_entry_id')
                                ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                ->where('purchase_order_entries.bill_id', $id)
                                ->select('purchase_order_entries.*',
                                         'product_entries.product_type as product_type',
                                         'product_entries.name as product_entry_name',
                                         'product_entries.product_code as product_code',
                                         'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                         'sizes.height as height',
                                         'sizes.width as width',
                                         'products.name as product_name')
                                ->get();  
                     
        $user_info  = Users::find(1);

        return view('purchaseorders::show', compact('entries', 'bill', 'user_info'));
    }

    public function edit($id)
    {
        $bills                  = PurchaseOrder::leftjoin('customers', 'customers.id', 'purchase_orders.vendor_id')
                                        ->orderBy('purchase_orders.created_at', 'DESC')
                                        ->select('purchase_orders.*',
                                                 'customers.name as vendor_name')
                                        ->get();

        $find_bill              = PurchaseOrder::leftjoin('customers', 'customers.id', 'purchase_orders.vendor_id')
                                        ->orderBy('purchase_orders.created_at', 'DESC')
                                        ->select('purchase_orders.*',
                                                 'customers.id as vendor_id',
                                                 'customers.name as vendor_name')
                                        ->find($id);

        $find_bill_entries      = PurchaseOrderEntries::leftjoin('customers', 'customers.id', 'purchase_order_entries.vendor_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'purchase_order_entries.product_entry_id')
                                        ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                        ->where('purchase_order_entries.bill_id', $id)
                                        ->select('purchase_order_entries.*',
                                                 'customers.id as vendor_id',
                                                 'customers.name as vendor_name',
                                                 'product_entries.id as item_id',
                                                 'product_entries.product_code as product_code',
                                                 'product_entries.stock_in_hand as stock_in_hand',
                                                 'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                                 'sizes.height as height',
                                                 'sizes.width as width',
                                                 'product_entries.name as item_name')
                                        ->get();

        $entries_count          = $find_bill_entries->count();

        return view('purchaseorders::edit', compact('bills', 'find_bill', 'find_bill_entries', 'entries_count'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'selling_date'          => 'required|date',
            'vendor_id'             => 'required|integer',
            'product_entries.*'     => 'required|integer',
            'amount.*'              => 'required|numeric',
            'vendor_id'             => 'required|integer',
            'bill_note'             => 'nullable|string',
            'selling_date'          => 'required|date',
            'product_entries.*'     => 'required|integer',
            'rate.*'                => 'required|numeric',
            'quantity.*'            => 'required|numeric',
            'discount_type.*'       => 'required|integer',
            'discount_amount.*'     => 'nullable|numeric',
            'amount.*'              => 'required|numeric',
            'total_amount'          => 'required|numeric',
            'total_discount_type'   => 'nullable|integer',
            'vat_type'              => 'nullable|numeric',
            'vat_amount'            => 'nullable|numeric',
            'total_discount_amount' => 'nullable|numeric',
            'total_discount_note'   => 'nullable|string',
            'cash_given'            => 'nullable|numeric',
            'change_amount'         => 'nullable|numeric',
            'coupon_code'           => 'nullable|numeric',
            'account_information.*' => 'nullable|string',
            'paid_through.*'        => 'nullable|integer',
            'note.*'                => 'nullable|string',
            'amount_paid.*'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            $bill           = PurchaseOrder::find($id);

            //Calculate Due Amount

                if ($data['total_amount'] > $bill['bill_amount']) 
                {
                    $bill_dues = $bill['due_amount'] + ($data['total_amount'] - $bill['bill_amount']);

                }
                
                if ($data['total_amount'] < $bill['bill_amount'])
                {
                    $bill_dues = $bill['due_amount'] - ($bill['bill_amount'] - $data['total_amount']);
                }

                if ($data['total_amount'] == $bill['bill_amount'])
                {
                    $bill_dues = $bill['due_amount'];
                }

            $bill->vendor_id                = $data['vendor_id'];
            $bill->bill_date                = date('Y-m-d', strtotime($data['selling_date']));
            $bill->bill_amount              = $data['total_amount'];
            $bill->due_amount               = $bill_dues;
            $bill->total_discount           = $discount;
            $bill->bill_note                = $data['bill_note'];
            $bill->total_vat                = $vat;
            $bill->vat_type                 = $data['vat_type'];
            $bill->total_discount_type      = $data['total_discount_type'];
            $bill->total_discount_amount    = $data['total_discount_amount'];
            $bill->total_discount_note      = $data['total_discount_note'];
            $bill->updated_by               = $user_id;

            if ($bill->save())
            {
                $item_id                = PurchaseOrderEntries::where('bill_id', $bill['id'])->get();
                $item_delete            = PurchaseOrderEntries::where('bill_id', $bill['id'])->delete();

                foreach ($data['product_entries'] as $key1 => $value1)
                {   
                    $product_buy_price      = ProductEntries::find($value1);

                    $bill_entries[] = [
                        'bill_id'            => $bill['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value1,
                        'vendor_id'          => $bill['vendor_id'],
                        'pcs'                => $data['pcs'][$key1] != null ? $data['pcs'][$key1] : 0,
                        'cartoon'            => $data['cartoon'][$key1] != null ? $data['cartoon'][$key1] : 0,
                        'rate'               => $data['rate'][$key1],
                        'quantity'           => $data['quantity'][$key1],
                        'total_amount'       => $data['amount'][$key1],
                        'discount_type'      => $data['discount_type'][$key1],
                        'discount_amount'    => $data['discount'][$key1],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('purchase_order_entries')->insert($bill_entries);

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return redirect()->route('purchase_orders_all_po')->with("success","Purchase Updated Successfully !!");
                }
                else
                {
                    return redirect()->route('purchase_orders_show', $bill['id']);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function billListLoad()
    {
        $data           = PurchaseOrder::leftjoin('customers', 'customers.id', 'purchase_orders.vendor_id')
                                ->leftjoin('purchase_order_entries', 'purchase_order_entries.bill_id', 'purchase_orders.id')
                                ->orderBy('purchase_orders.created_at', 'DESC')
                                ->select('purchase_orders.*',
                                         'customers.contact_type as contact_type',
                                         'customers.name as customer_name')
                                ->distinct('purchase_orders.id')
                                ->take(100)
                                ->get();

        return Response::json($data);
    }

    public function billListSearch($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data           = Bills::leftjoin('purchase_return', 'purchase_return.bill_id', 'bills.id')
                                ->leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                ->leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                ->where('bills.type', 1)
                                ->where('customers.name', 'LIKE', "%$id%")
                                ->orWhere('bills.bill_date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number, $table_id) {
                                    return $query->orWhere('bills.bill_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('bills.created_at', 'DESC')
                                ->select('bills.*',
                                         'purchase_return.id as return_id',
                                         'customers.contact_type as contact_type',
                                         'customers.name as customer_name')
                                ->distinct('bills.id')
                                ->take(100)
                                ->get();
        }
        else
        {
            $data           = Bills::leftjoin('purchase_return', 'purchase_return.bill_id', 'bills.id')
                                ->leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                ->leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                ->where('bills.type', 1)
                                ->orderBy('bills.created_at', 'DESC')
                                ->select('bills.*',
                                         'purchase_return.id as return_id',
                                         'customers.contact_type as contact_type',
                                         'customers.name as customer_name')
                                ->distinct('bills.id')
                                ->take(100)
                                ->get();
        }
        
        return Response::json($data);
    }
}
