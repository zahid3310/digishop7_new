<?php

namespace Modules\Payments\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use Validator;
use Auth;

//Models
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\Bills;
use App\Models\InvoiceEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\JournalEntries;
use App\Models\Accounts;
use App\Models\Settlements;
use App\Models\Users;
use GuzzleHttp\Client;
use Response;
use DB;

class PaymentsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('payments::index');
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        // $debit      = JournalEntries::where('customer_id', 536)
        //                         ->whereIn('transaction_head', ['sales', 'customer-opening-balance', 'ayment-receive'])
        //                         ->where('debit_credit', 1)
        //                         ->sum('amount');

        // $credit     = JournalEntries::where('customer_id', 536)
        //                             ->whereIn('transaction_head', ['payment-made', 'customer-settlement', 'sales-return'])
        //                             ->where('debit_credit', 0)
        //                             ->sum('amount');

        // $balance    = $debit - $credit;

        // dd($balance);

        $branch_id      = Auth::user()->branch_id;
        $customers      = Customers::where('contact_type', 0)
                                    ->orWhere('contact_type', 1)
                                    ->orWhere('contact_type', 2)
                                    ->orderBy('created_at', 'DESC')
                                    ->get();

        $customers      = $customers->where('branch_id', $branch_id);

        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $find_customer  = Customers::where('id', $customer_id)->first();
        $paid_accounts  = Accounts::where('account_type_id', 4)
                                    ->whereNotIn('id', [2,3])
                                    ->where('status', 1)
                                    ->get();

        return view('payments::create', compact('customers', 'customer_id', 'find_customer', 'paid_accounts'));
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_id'   => 'required',
            'type'          => 'required',
            'payment_date'  => 'required',
            'amount'        => 'required',
            'paid.*'        => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id          = Auth::user()->branch_id;
        $user_id            = Auth::user()->id;
        $data               = $request->all();
        $discount_amount    = $data['discount_amount'] != null ? $data['discount_amount'] : 0;

        if ($data['amount'] == 0 || $data['amount'] == null || $data['payment_date'] == null)
        {
            return redirect()->back()->withInput();
        }

        DB::beginTransaction();

        try{
            $data_find                  = Payments::orderBy('id', 'DESC')->first();
            $payment_number             = $data_find != null ? $data_find['payment_number'] + 1 : 1;
            
            $payment                    = new Payments;
            $payment->payment_number    = $payment_number;
            $payment->customer_id       = $data['customer_id'];
            $payment->payment_date      = date('Y-m-d', strtotime($data['payment_date']));
            $payment->amount            = $data['amount'];
            $payment->discount_amount   = $discount_amount;
            $payment->paid_through      = $data['paid_through'];
            $payment->note              = $data['note'];
            $payment->type              = $data['type'];
            $payment->branch_id         = $branch_id;
            $payment->created_by        = $user_id;

            $find_customer              = $data['customer_id'];
            $find_type                  = $data['type'];

            if ($payment->save())
            {   
                if ($data['type'] == 0)
                {
                    //Financial Accounting Start
                        debit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=$data['paid_through'], $amount=$data['amount'], $note=$data['note'], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                        credit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=8, $amount=$data['amount'], $note=$data['note'], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

                        if ($discount_amount > 0) 
                        {
                            credit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=null, $amount=$discount_amount, $note=$data['note'], $transaction_head='discount', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                        }
                    //Financial Accounting End
                    
                     $cus_info  = Customers::find($data['customer_id']);
                    
                    customerBalanceUpdate($data['customer_id']);
                    
                    $cus_info1  = Customers::find($data['customer_id']);
                    $previous_due   = $cus_info1['balance'];

                    if($request->sms == 1)
                    {
                        
                        $previousDue  = number_format($previous_due,2,'.',',');
                        $discountAmount  = number_format($discount_amount,2,'.',',');
                        $amount  = number_format($data['amount'],2,'.',',');

                        $message    = 'আসসালামু আলাইকুম। নূর স্যানিটারীতে আপনাকে স্বাগতম। আপনার বর্তমান বাকী :'.$previousDue.', ছাড় :'.$discountAmount.', বর্তমান জমা : '.$amount.'। যেকোন তথ্যের জন্য কল করুন:শোরুম: 01317-349 956 , এমডি: 01733- 846 666, ধন্যবাদ, আবার আসবেন।';
                        
                        $mobile     =   $cus_info['phone'];
                        
                        
                        $client              = new Client();
                        $sms                 = $client->request("GET", "https://isms.mimsms.com/smsapi?api_key=C2001461630321bd3b85b1.32669167&type=text&contacts=$mobile&senderid=8809601003452&msg=$message");
                        // $user       = Users::find(1);
                        // $response   = Http::get('https://digishop7.com/digishop-billing/send-mim-sms/'.$user['billing_id'].'?mobile='.$mobile.'&message='.$message);
                    }
                }
                
                if ($data['type'] == 1)
                {
                    //Financial Accounting Start
                        debit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=9, $amount=$data['amount'], $note=$data['note'], $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                        credit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=$data['paid_through'], $amount=$data['amount'], $note=$data['note'], $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

                        if ($discount_amount > 0) 
                        {
                            credit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=null, $amount=$discount_amount, $note=$data['note'], $transaction_head='discount', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                        }
                    //Financial Accounting End

                    supplierBalanceUpdate($data['customer_id']);
                }

                if ($data['type'] == 2)
                {
                    //Financial Accounting Start
                        debit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=$data['paid_through'], $amount=$data['amount'], $note=$data['note'], $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                        credit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=9, $amount=$data['amount'], $note=$data['note'], $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    //Financial Accounting End

                    employeeBalanceUpdate($data['customer_id']);
                }

                DB::commit();
                return back()->with("success","Payment Created Successfully !!")->with("find_customer", $find_customer)->with('find_type', $find_type)->with('payment_date', $data['payment_date']);
            }
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return back()->with("unsuccess","Not Added")->with('find_customer', $find_customer)->with('find_type', $find_type);
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $payment        = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                ->select('payments.*',
                                         'customers.name as customer_name',
                                         'customers.address as address',
                                         'customers.phone as phone')
                                ->find($id);

        if ($payment['type'] == 0)
        {
            $entries    = PaymentEntries::leftjoin('invoices', 'invoices.id', 'payment_entries.invoice_id')
                                ->where('payment_entries.payment_id', $id)
                                ->select('payment_entries.*', 'invoices.invoice_number as number')
                                ->get();
        }
        else
        {
            $entries    = PaymentEntries::leftjoin('bills', 'bills.id', 'payment_entries.bill_id')
                                ->where('payment_entries.payment_id', $id)
                                ->select('payment_entries.*', 'bills.bill_number as number')
                                ->get();
        }

        $user_info  = userDetails();

        return view('payments::show', compact('entries', 'payment', 'user_info'));
    }

    public function edit($id)
    {
        dd($entries);
        return view('payments::edit');
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        DB::beginTransaction();

        try{
            $payment  = Payments::find($id);

            if ($payment->type == 0)
            {
                $payment_delete         = Payments::where('id', $id)->delete();
                $delete_transactions    = JournalEntries::where('payment_id', $id)->whereIn('transaction_head', ['payment-receive', 'discount'])->delete();

                customerBalanceUpdate($payment['customer_id']);
            }

            if ($payment->type == 1)
            {
                $payment_delete         = Payments::where('id', $id)->delete();
                $delete_transactions    = JournalEntries::where('payment_id', $id)->whereIn('transaction_head', ['payment-made', 'discount'])->delete();
                
                supplierBalanceUpdate($payment['customer_id']);
            }

            DB::commit();
            return back()->with("success","Payment Deleted Successfully !!");

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Deleted");
        }      
    }
    
    public function paymentListCustomer()
    {
        $branch_id  = Auth::user()->branch_id;
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Customers::orderBy('customers.created_at', 'DESC')
                                    ->select('customers.*')
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Customers::where('customers.name', 'LIKE', "%$search%")
                                    ->orWhere('customers.phone', 'LIKE', "%$search%")
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->select('customers.*')
                                    ->get();
        }

        $data       = array();
        $i          = 0;
        $fetchData  = $fetchData->where('branch_id', $branch_id);
        foreach ($fetchData as $key => $value)
        {
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "phone" =>'', "contact_type" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name'], "phone" =>$value['phone'], "contact_type" =>$value['contact_type']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function paymentListAjax()
    {
        $branch_id  = Auth::user()->branch_id;
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Payments::orderBy('payments.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Payments::where('payments.payment_number', 'LIKE', "%$search%")
                                    ->orderBy('payments.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data       = array();
        $i          = 0;
        $fetchData  = $fetchData->where('branch_id', $branch_id);
        foreach ($fetchData as $key => $value)
        {
            $paymentNumber  = 'PM - '.str_pad($value['payment_number'], 6, "0", STR_PAD_LEFT);

            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[]         = array("id"=>$value['id'], "text"=>$paymentNumber);

            $i++;
        }
   
        return Response::json($data);
    }

    public function paymentSearchList($from_date, $to_date, $customer, $paymentNumber)
    {
        $branch_id                      = Auth()->user()->branch_id;
        $search_by_from_date            = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date              = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
        $search_by_customer             = $customer != 0 ? $customer : 0;
        $search_by_payment_number       = $paymentNumber != 0 ? $paymentNumber : 0;

        $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                            ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                            ->when($search_by_from_date != 0 && $search_by_to_date != 0, function ($query) use ($search_by_from_date, $search_by_to_date) {
                                return $query->whereBetween('payments.payment_date', [$search_by_from_date, $search_by_to_date]);
                            })
                            ->when($search_by_customer != 0, function ($query) use ($search_by_customer) {
                                return $query->where('customers.id', $search_by_customer);
                            })
                            ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                return $query->where('payments.payment_number', $search_by_payment_number);
                            })
                            ->where('branch_id', $branch_id)
                            ->orderBy('payments.created_at', 'DESC')
                            ->select('payments.*',
                                     'accounts.account_name as accounts_name',
                                     'customers.name as customer_name',
                                     'customers.phone as phone')
                            ->distinct('payments.id')
                            ->get();

        return Response::json($data);
    }

    public function contactList($id)
    {
        $branch_id  = Auth()->user()->branch_id;
        $customers  = Customers::find($id);
        $debit      = JournalEntries::where('customer_id', $id)
                                ->whereIn('transaction_head', ['sales', 'customer-opening-balance', 'payment-made', 'customer-settlement'])
                                ->where('debit_credit', 1)
                                ->sum('amount');

        $credit     = JournalEntries::where('customer_id', $id)
                                ->whereIn('transaction_head', ['payment-receive', 'sales-return', 'discount'])
                                ->where('debit_credit', 0)
                                ->sum('amount');

        $data['name']           = $customers['name'];
        $data['phone']          = $customers['phone'];
        $data['address']        = $customers['address'];
        $data['receivable']     = $debit;
        $data['received']       = $credit;
        $data['dues']           = $customers['balance'];

        return Response::json($data);
    }

    public function contactListBill($id)
    {
        $branch_id  = Auth()->user()->branch_id;
        $customers  = Customers::find($id);
        $debit      = JournalEntries::where('customer_id', $id)
                                ->whereIn('transaction_head', ['production', 'purchase', 'supplier-opening-balance', 'payment-receive', 'supplier-settlement'])
                                ->where('debit_credit', 1)
                                ->sum('amount');

        $credit     = JournalEntries::where('customer_id', $id)
                                ->whereIn('transaction_head', ['payment-made', 'purchase-return', 'discount'])
                                ->where('debit_credit', 0)
                                ->sum('amount');

        $data['name']           = $customers['name'];
        $data['phone']          = $customers['phone'];
        $data['address']        = $customers['address'];
        $data['payable']        = $debit;
        $data['paid']           = $credit;
        $data['dues']           = $customers['balance'];

        return Response::json($data);
    }

    public function contactListEmployee($id)
    {
        $branch_id  = Auth()->user()->branch_id;
        $debit      = JournalEntries::where('customer_id', $id)
                                ->whereIn('transaction_head', ['employee-salary'])
                                ->where('debit_credit', 1)
                                ->where('branch_id', $branch_id)
                                ->sum('amount');

        $credit     = JournalEntries::where('customer_id', $id)
                                ->whereIn('transaction_head', ['payment-made'])
                                ->where('debit_credit', 0)
                                ->where('branch_id', $branch_id)
                                ->sum('amount');

        $data['name']           = $customers['name'];
        $data['phone']          = $customers->department_id != null ? $customers->department->name : '';
        $data['address']        = $customers['designation'];
        $data['payable']        = $debit;
        $data['paid']           = $credit;
        $data['dues']           = $customers['balance'];

        return Response::json($data);
    }

    public function paymentList($id)
    {
        $branch_id  = Auth::user()->branch_id;

        if ($id == 0)
        {
            $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                                ->where('payments.branch_id', $branch_id)
                                ->orderBy('payments.created_at', 'DESC')
                                ->select('payments.*',
                                          'accounts.account_name as accounts_name', 
                                          'customers.name as customer_name')
                                ->orderBy('payments.id', 'DESC')
                                ->get();
        }
        else
        {
            $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                                ->where('payments.customer_id', $id)
                                ->where('payments.branch_id', $branch_id)
                                ->orderBy('payments.created_at', 'DESC')
                                ->select('payments.*', 
                                          'accounts.account_name as accounts_name', 
                                          'customers.name as customer_name')
                       
                                ->get();
        }

        return Response::json($data);
    }

    public function paymentListSearch($id)
    {
        $branch_id                  = Auth::user()->branch_id;
        $search_by_date             = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers  = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                                ->where('customers.name', 'LIKE', "%$id%")
                                ->orWhere('payments.payment_date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                    return $query->orWhere('payments.payment_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('payments.created_at', 'DESC')
                                ->select('payments.*', 
                                          'accounts.account_name as accounts_name', 
                                          'customers.name as customer_name')
                                ->get();
        }
        else
        {
            $data   = Payments::join('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'accounts.account_name as accounts_name', 
                                                  'customers.name as customer_name')
                                        ->get();
        }

        $data   = $data->where('branch_id', $branch_id);

        return Response::json($data);
    }

    public function settlement()
    {   
        $branch_id      = Auth::user()->branch_id;
        $customers      = Customers::whereIn('contact_type', [0,1])
                                    ->where('balance', '<', 0)
                                    ->where('branch_id', $branch_id)
                                    ->orderBy('created_at', 'DESC')
                                    ->get();
                                    
        $paid_accounts  = Accounts::where('account_type_id', 4)
                                    ->whereNotIn('id', [2,3])
                                    ->where('status', 1)
                                    ->get();

        return view('payments::settlement_index', compact('customers', 'paid_accounts'));
    }

    public function settlementStore(Request $request)
    {
        $rules = array(
            'customer_id'       => 'required',
            'type'              => 'required',
            'settlement_date'   => 'required',
            'amount'            => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id      = Auth::user()->branch_id;
        $user_id        = Auth::user()->id;
        $data           = $request->all();
        $find_customer  = Customers::find($data['customer_id']);

        if ($data['amount'] == 0 || $data['amount'] == null || $data['settlement_date'] == null)
        {
            return redirect()->back()->withInput();
        }

        if ($data['type'] == 0)
        {
            if ($find_customer['customer_advance_payment'] > $data['amount'])
            {
                return redirect()->back()->withInput();
            }
        }

        if ($data['type'] == 1)
        {
            if ($find_customer['supplier_advance_payment'] > $data['amount'])
            {
                return redirect()->back()->withInput();
            }
        }
        

        DB::beginTransaction();

        try{
            $settlement                    = new Settlements;
            $settlement->customer_id       = $data['customer_id'];
            $settlement->date              = date('Y-m-d', strtotime($data['settlement_date']));
            $settlement->amount            = $data['amount'];
            $settlement->paid_through_id   = $data['paid_through'];
            $settlement->note              = $data['note'];
            $settlement->type              = $data['type'];
            $settlement->branch_id         = $branch_id;
            $settlement->created_by        = $user_id;

            if ($settlement->save())
            {   
                if ($data['type'] == 0)
                {   
                    //Financial Accounting Start
                        debit($customer_id=$data['customer_id'], $date=$data['settlement_date'], $account_id=9, $amount=$data['amount'], $note=$data['note'], $transaction_head='customer-settlement', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=$settlement->id, $sales_return_id=null, $purchase_return_id=null);
                        credit($customer_id=$data['customer_id'], $date=$data['settlement_date'], $account_id=$data['paid_through'], $amount=$data['amount'], $note=$data['note'], $transaction_head='customer-settlement', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=$settlement->id, $sales_return_id=null, $purchase_return_id=null);
                    //Financial Accounting End

                    customerBalanceUpdate($data['customer_id']);
                }
                
                if ($data['type'] == 1)
                {
                    //Financial Accounting Start
                        debit($customer_id=$data['customer_id'], $date=$data['settlement_date'], $account_id=$data['paid_through'], $amount=$data['amount'], $note=$data['note'], $transaction_head='supplier-settlement', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=$settlement->id, $sales_return_id=null, $purchase_return_id=null);
                        credit($customer_id=$data['customer_id'], $date=$data['settlement_date'], $account_id=11, $amount=$data['amount'], $note=$data['note'], $transaction_head='supplier-settlement', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=$settlement->id, $sales_return_id=null, $purchase_return_id=null);
                    //Financial Accounting End

                    supplierBalanceUpdate($data['customer_id']);
                }

                DB::commit();
                return back()->with("success","Settlement Created Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return back()->with("unsuccess","Not Added")->with('find_customer', $find_customer)->with('find_type', $find_type);
        }
    }

    public function settlementEdit($id)
    {
        $find_settlement    = Settlements::find($id);
        $customers          = Customers::where('id', $find_settlement->customer_id)->get();
        $paid_accounts      = Accounts::where('account_type_id', 4)
                                        ->whereNotIn('id', [2,3])
                                        ->where('status', 1)
                                        ->get();

        $find_paid_account  = JournalEntries::where('settlement_id', $id)
                                        ->where('debit_credit', 1)
                                        ->select('account_id')
                                        ->first();

        return view('payments::settlement_edit', compact('find_settlement', 'customers', 'paid_accounts', 'find_paid_account'));
    }
    
    public function settlementShow($id)
    {
        $find_settlement    = Settlements::join('customers', 'customers.id', 'settlements.customer_id')->leftjoin('accounts', 'accounts.id', 'settlements.paid_through_id')->find($id);
        $user_info  = userDetails();
        return view('payments::settlement_show', compact('find_settlement','user_info'));
    }

    public function settlementUpdate(Request $request, $id)
    {
        $rules = array(
            'customer_id'       => 'required',
            'type'              => 'required',
            'settlement_date'   => 'required',
            'amount'            => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id        = Auth::user()->id;
        $data           = $request->all();
        $find_customer  = Customers::find($data['customer_id']);

        if ($data['amount'] == 0 || $data['amount'] == null || $data['settlement_date'] == null)
        {
            return redirect()->back()->withInput();
        }

        if ($data['type'] == 0)
        {
            if ($find_customer['customer_advance_payment'] > $data['amount'])
            {
                return redirect()->back()->withInput();
            }
        }

        if ($data['type'] == 1)
        {
            if ($find_customer['supplier_advance_payment'] > $data['amount'])
            {
                return redirect()->back()->withInput();
            }
        }

        DB::beginTransaction();

        try{
            $settlement                    = Settlements::find($id);
            $settlement->customer_id       = $data['customer_id'];
            $settlement->date              = date('Y-m-d', strtotime($data['settlement_date']));
            $settlement->amount            = $data['amount'];
            $settlement->paid_through_id   = $data['paid_through'];
            $settlement->note              = $data['note'];
            $settlement->type              = $data['type'];
            $settlement->created_by        = $user_id;

            if ($settlement->save())
            {   
                if ($data['type'] == 0)
                {    
                    $jour_ent_debit     = JournalEntries::where('settlement_id', $id)
                                            ->where('debit_credit', 1)
                                            ->first();

                    $jour_ent_credit    = JournalEntries::where('settlement_id', $id)
                                            ->where('debit_credit', 0)
                                            ->first();
                    //Financial Accounting Start
                        debitUpdate($jour_ent_credit['id'], $customer_id=$data['customer_id'], $date=$data['settlement_date'], $account_id=9, $amount=$data['amount'], $note=$data['note'], $transaction_head='customer-settlement', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=$settlement->id, $sales_return_id=null, $purchase_return_id=null);
                        creditUpdate($jour_ent_debit['id'], $customer_id=$data['customer_id'], $date=$data['settlement_date'], $account_id=$data['paid_through'], $amount=$data['amount'], $note=$data['note'], $transaction_head='customer-settlement', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=$settlement->id, $sales_return_id=null, $purchase_return_id=null);
                    //Financial Accounting End

                    customerBalanceUpdate($data['customer_id']);

                    $type = '?settlement_type=0';
                }
                
                if ($data['type'] == 1)
                {   
                    $jour_ent_debit     = JournalEntries::where('settlement_id', $id)
                                            ->where('debit_credit', 1)
                                            ->first();

                    $jour_ent_credit    = JournalEntries::where('settlement_id', $id)
                                            ->where('debit_credit', 0)
                                            ->first();
                    //Financial Accounting Start
                        debitUpdate($jour_ent_debit['id'], $customer_id=$data['customer_id'], $date=$data['settlement_date'], $account_id=$data['paid_through'], $amount=$data['amount'], $note=$data['note'], $transaction_head='supplier-settlement', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=$settlement->id, $sales_return_id=null, $purchase_return_id=null);
                        creditUpdate($jour_ent_credit['id'], $customer_id=$data['customer_id'], $date=$data['settlement_date'], $account_id=11, $amount=$data['amount'], $note=$data['note'], $transaction_head='supplier-settlement', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=$settlement->id, $sales_return_id=null, $purchase_return_id=null);
                    //Financial Accounting End

                    supplierBalanceUpdate($data['customer_id']);

                    $type = '?settlement_type=1';
                }

                DB::commit();
                return redirect()->to('payments/settlements/create'.$type)->with("success","Settlement Updated Successfully !!");
            }
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return redirect()->to('payments/settlements/create'.$type)->with("unsuccess","Not Added")->with('find_customer', $find_customer)->with('find_type', $find_type);
        }
    }

    public function settlementDelete($id)
    {
        DB::beginTransaction();

        try{
            $settlement             = Settlements::find($id);
            $delete_settlement      = Settlements::where('id', $id)->delete();
            $delete_jour_entries    = JournalEntries::where('settlement_id', $id)->delete();

            if ($settlement['type'] == 0)
            {
                $type = '?settlement_type=0';
                customerBalanceUpdate($settlement['customer_id']);
            }

            if ($settlement['type'] == 1)
            {
                $type = '?settlement_type=1';
                supplierBalanceUpdate($settlement['customer_id']);
            }

            DB::commit();
            return redirect()->to('payments/settlements/create'.$type)->with("success","Settlement Deleted Successfully !!");
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return redirect()->to('payments/settlements/create'.$type)->with("unsuccess","Not Added");
        }
    }

    public function getSettlementData($id)
    {
        $branch_id  = Auth::user()->branch_id;
        $data       = Settlements::leftjoin('customers', 'customers.id', 'settlements.customer_id')
                                ->leftjoin('accounts', 'accounts.id', 'settlements.paid_through_id')
                                ->where('settlements.branch_id', $branch_id)
                                ->orderBy('settlements.created_at', 'DESC')
                                ->selectRaw('settlements.*, customers.name as customer_name, accounts.account_name as paid_through_account_name')
                                ->get();

        return Response::json($data);
    }

    public function getBalance($id)
    {
        $info = Customers::find($id);

        if ($info['contact_type'] == 0)
        {
            $data['type']       = 0;
            $data['balance']    = $info['balance'] < 0 ? abs($info['balance']) : 0;
        }

        if ($info['contact_type'] == 1)
        {
            $data['type']       = 1;
            $data['balance']    = $info['balance'] < 0 ? abs($info['balance']) : 0;
        }

        return Response::json($data);
    }
}