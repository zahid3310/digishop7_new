@extends('layouts.app')

@section('title', 'Stock Transfer')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Transfer Stock </h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Stock Transfer</a></li>
                                    <li class="breadcrumb-item active">Stock Transfer</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('stock_transfer_store') }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Transfer Date *</label>
                                        <input id="transfer_date" name="transfer_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Transfer To *</label>
                                        <select style="width: 100%" id="transfer_to" name="transfer_to" class="form-control single_select2" required>
                                           <option value="">--Transfer To--</option>
                                           @if($branches->count() > 0)
                                           @foreach($branches as $branche)
                                           @if($branche->id != Auth::user()->branch_id)
                                           <option value="{{ $branche->id }}">{{ $branche->name }}</option>
                                           @endif
                                           @endforeach
                                           @endif
                                        </select> 
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label"></label>
                                        <i id="add_field_button" style="padding: 0.68rem 0.75rem !important;margin-top: 14px" class="fas fa-plus btn btn-success btn-block inner add_field_button col-md-3"></i>
                                    </div>

                                    <hr>

                                    <div style="background-color: #D2D2D2;height: 415px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        <div class="row di_0">
                                        </div>
                                    </div>

                                </div>

                                <br>

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('stock_transfer_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Transfers</h4>

                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Date</th>
                                            <th>Transfer From</th>
                                            <th>Transfer To</th>
                                            <th>Note</th>
                                            <th style="text-align: center">Quantity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $i = 1 ;$transfersDate = 0; ?>
                                        
                                        @if(!empty($transfers) && ($transfers->count() > 0))
                                        @foreach($transfers as $key => $transfer)

                                        <?php if ($transfersDate !=  date('d-m-Y', strtotime($transfer['date']))): ?>
                                             
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ date('d-m-Y', strtotime($transfer['date'])) }}</td>
                                                <td>{{ $transfer->transfer_from != null ? $transfer->transferFrom->name : '' }}</td>
                                                <td>{{ $transfer->transfer_to != null ? $transfer->transferTo->name : '' }}</td>
                                                <td>{{ $transfer->note }}</td>
                                                <td style="text-align: center">{{ $transfer->quantity }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <!-- <a class="dropdown-item" href="{{ route('stock_transfer_edit', $transfer['id']) }}">Edit</a> -->
                                                            <a style="cursor:pointer;" class="dropdown-item transfer"  data-toggle="modal" 
                                                            data-date="{{$transfer['date']}}"
                                                            data-transfer-from="{{ $transfer->transfer_from != null ? $transfer->transferFrom->name : '' }}"
                                                            data-transfer-to="{{ $transfer->transfer_to != null ? $transfer->transferTo->name : '' }}" 
                                                            data-target="#transferModal">View All Transfers</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endif ?>
                                        <?php $transfersDate = date('d-m-Y', strtotime($transfer['date'])) ?>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="transferModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="data-date">Transfer List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Transfer From</th>
                                            <th>Transfer To</th>
                                            <th>Product Name</th>
                                            <th style="text-align: center">Quantity</th>
                                        </tr>
                                    </thead>

                                    <tbody id="transfer_list">
                                        
                                    </tbody>
                                </table>
                </div>

                <div class="modal-footer">
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        
        var site_url  = $('.site_url').val();

        $(".productList").select2({
            ajax: { 
            url:  site_url + '/reports/sales-statement/product-list',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['id'] != 0)
                {
                    return result['text'];
                }
            },
        });

        $('#add_field_button').click();
    });

    function getItemPrice(x)
    {
        var site_url    = $(".site_url").val();
        var entry_id    = $("#product_entries_"+x).val();
        var cartoon_val = $("#cartoon_"+x).val();
        var pcs_val     = $("#pcs_"+x).val();

        if(entry_id)
        {   
            if ($('#isCollected_'+x).is(":checked"))
            {
                $('#isCollectedVal_'+x).val(entry_id);
            }
            else
            {
                $('#isCollectedVal_'+x).val(0);
            }

            $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                if (data.type == 1)
                {
                    var pcs_equivalent_sft  = (parseFloat(data.height)*parseFloat(data.width))/144;
                    var stock_in_pcs        = parseFloat(data.stock_in_hand)/parseFloat(pcs_equivalent_sft);
                    var stock_in_cart       = parseFloat(stock_in_pcs)/parseFloat(data.pcs_per_cartoon);

                    //
                        if (data.stock_in_hand == null)
                        {
                            var stockInHand  = 0;
                        }
                        else
                        {
                            var stockInHand  = data.stock_in_hand;
                        }

                    //
                        if (cartoon_val == '')
                        {
                            var cartoonVal  = 0;
                        }
                        else
                        {
                            var cartoonVal  = parseFloat(cartoon_val);
                        }

                        if (cartoon_val == 0)
                        {
                            var cartoonVal  = 0;
                        }
                        else
                        {
                            var cartoonVal  = parseFloat(cartoon_val);
                        }

                    //
                        if (pcs_val == '')
                        {
                            var pcsVal  = 0;
                        }
                        else
                        {
                            var pcsVal  = parseFloat(pcs_val);
                        }

                        if (pcs_val == 0)
                        {
                            var pcsVal  = 0;
                        }
                        else
                        {
                            var pcsVal  = parseFloat(pcs_val);
                        }

                    if (cartoonVal != 0)
                    {
                        var qtyVal1  = parseFloat(cartoonVal)*parseFloat(data.pcs_per_cartoon)*parseFloat(pcs_equivalent_sft);
                    }
                    else
                    {
                        var qtyVal1  = 0;
                    }

                    if (pcsVal != 0)
                    {
                        var qtyVal2  = parseFloat(pcsVal)*parseFloat(pcs_equivalent_sft);

                        if (cartoonVal == '')
                        {
                            var pcsToCartoon = parseFloat(pcsVal)/parseFloat(data.pcs_per_cartoon);

                            //x = 50; y = 15; res = x % y; x = (x - res) / y; [result = 3]
                            var cartAmount = (parseFloat(pcsVal) - (parseFloat(pcsVal)%parseFloat(data.pcs_per_cartoon)))/parseFloat(data.pcs_per_cartoon);
                            var pcsAmount  = parseFloat(pcsVal) - (parseFloat(cartAmount)*parseFloat(data.pcs_per_cartoon));

                            $("#cartoon_"+x).val(parseFloat(cartAmount).toFixed(2));
                            $("#pcs_"+x).val(parseFloat(pcsAmount).toFixed());
                        }
                    }
                    else
                    {
                        var qtyVal2  = 0;
                    }

                    var qtyVal = parseFloat(qtyVal1) + parseFloat(qtyVal2);

                    $("#rate_"+x).val(parseFloat(data.sell_price).toFixed(2));
                    $("#discount_"+x).val(0);
                    $("#quantity_"+x).val(parseFloat(qtyVal).toFixed(2));
                    $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
                    $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' SFT ' + ' | Purchase Price : ' + parseFloat(data.buy_price));
        
                    calculateActualAmount(x);
                }
                else
                {
                    //
                        if (data.stock_in_hand == null)
                        {
                            var stockInHand  = 0;
                        }
                        else
                        {
                            var stockInHand  = data.stock_in_hand;
                        }

                    //
                        if (data.unit_id != null)
                        {
                            var unit = '( ' + data.unit_name + ' )';
                        }
                        else
                        {
                            var unit = '';
                        }

                    $("#cartoon_"+x).val(0);
                    $("#pcs_"+x).val(0);
      
                    $("#rate_"+x).val(parseFloat(data.sell_price).toFixed(2));
                    $("#discount_"+x).val(0);
                    $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
                    $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + unit + ' | Purchase Price : ' + parseFloat(data.buy_price));
      
                    calculateActualAmount(x);
                }
            });
        }
    }

    function getItemPriceBackCalculation(x)
    {
        var site_url    = $(".site_url").val();
        var entry_id    = $("#product_entries_"+x).val();
        var sft_val     = $("#quantity_"+x).val();

        if(entry_id)
        {
            $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                if (data.type == 1)
                {
                    var pcs_equivalent_sft  = (parseFloat(data.height)*parseFloat(data.width))/144;
                    var stock_in_pcs        = parseFloat(data.stock_in_hand)/parseFloat(pcs_equivalent_sft);
                    var stock_in_cart       = parseFloat(stock_in_pcs)/parseFloat(data.pcs_per_cartoon);

                    //
                        if (sft_val == '')
                        {
                            var sftVal  = 0;
                        }
                        else
                        {
                            var sftVal  = parseFloat(sft_val);
                        }

                    var findPcs = parseFloat(sft_val)/parseFloat(pcs_equivalent_sft);

                    //x = 50; y = 15; res = x % y; x = (x - res) / y; [result = 3]
                    var cartVal = (parseFloat(findPcs) - (parseFloat(findPcs)%parseFloat(data.pcs_per_cartoon)))/parseFloat(data.pcs_per_cartoon);

                    var pcsVal  = parseFloat(findPcs) - (parseFloat(cartVal)*parseFloat(data.pcs_per_cartoon));

                    $("#cartoon_"+x).val(parseFloat(cartVal).toFixed(2));
                    $("#pcs_"+x).val(parseFloat(pcsVal).toFixed());
                }

                calculateActualAmount(x);
            });
        }
    }

    function pad(number, length)
    {
        var str = '' + number;
        while (str.length < length)
        {
            str = '0' + str;
        }
       
        return str;
    } 
</script>

<script type="text/javascript">
    var max_fields       = 50;                           //maximum input boxes allowed
    var wrapper          = $(".input_fields_wrap");      //Fields wrapper
    var add_button       = $(".add_field_button");       //Add button ID
    var add_button_pos   = $(".add_field_button_pos");   //Add button ID
    var index_no         = 1;

    //For apending another rows start
    var x = -1;
    $(add_button).click(function(e)
    {
        e.preventDefault();

        // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

        if(x < max_fields)
        {
            x++;

            var serial = x + 1;

            if (serial == x + 1)
            {
                var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                var transfer_from       = '<label class="hidden-xs" for="productname">Transfer From</label>\n';
                var cartoon_label       = '<label class="hidden-xs" for="productname">Cart</label>\n';
                    var pcs_label           = '<label class="hidden-xs" for="productname">PCS</label>\n';
                var quantity_label      = '<label class="hidden-xs" for="productname">SFT *</label>\n';
                var unit_label          = '<label class="hidden-xs" for="productname">Unit</label>\n';
                var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';
                var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                    action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                            '</div>\n';
            }
            else
            {
                var product_label       = '';
                var transfer_from       = '';
                var cartoon_label       = '';
                var pcs_label           = '';
                var quantity_label      = '';
                var unit_label          = '';
                var note_label          = '';
                var action_label        = '';

                var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                    action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                            '</div>\n';
            }

            $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                '<div style="margin-bottom: 5px" class="col-lg-4 col-md-4 col-sm-6 col-12">\n' +
                                                    '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                    product_label +
                                                    '<select style="width: 100%" name="product_entries[]" class="inner form-control select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                        '<option value="">' + '--Select Product--' + '</option>' +
                                                    '</select>\n' +
                                                    '<span id="stock_show_'+x+'" style="color: black;font-size: 10px">' + '</span>' +
                                                '</div>\n' +

                                                '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +
                                                '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +
                                                '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                    '<label style="display: none" class="show-xs" for="productname">SFT *</label>\n' +
                                                    quantity_label  +
                                                    '<input type="text" name="quantity[]" class="inner form-control" id="quantity_'+x+'" placeholder="Quantity" oninput="calculateActualAmount('+x+')" required />\n' +
                                                '</div>\n' +

                                                '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Cart</label>\n' +
                                                        cartoon_label +
                                                        '<input type="text" name="cartoon[]" class="inner form-control" id="cartoon_'+x+'" placeholder="Cartoon" oninput="getItemPrice('+x+')" />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">PCS</label>\n' +
                                                        pcs_label +
                                                        '<input type="text" name="pcs[]" class="inner form-control" id="pcs_'+x+'" placeholder="PCS" onchange="getItemPrice('+x+')" />\n' +
                                                    '</div>\n' +

                                                '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                    note_label +
                                                    '<input type="text" name="note[]" class="inner form-control" id="note_'+x+'" placeholder="Transfer Note" />\n' + 
                                                '</div>\n' + 
                                                
                                                add_btn +
                                                
                                            '</div>\n' 
                                        );

                                        $('.single_select2').select2();

                                        var site_url  = $('.site_url').val();

                                        $(".productEntries").select2({
                                            ajax: { 
                                            url:  site_url + '/bills/product-list-load-bill',
                                            type: "get",
                                            dataType: 'json',
                                            delay: 250,
                                            data: function (params) {
                                                return {
                                                    searchTerm: params.term // search term
                                                };
                                            },
                                            processResults: function (response) {
                                                return {
                                                    results: response
                                                };
                                            },
                                                cache: true
                                            },

                                            minimumInputLength: 0,
                                            escapeMarkup: function(result) {
                                                return result;
                                            },
                                            templateResult: function (result) {
                                                if (result.loading) return 'Searching...';

                                                if (result['id'] != 0)
                                                {
                                                    return result['text'];
                                                }
                                            },
                                        });
        }                                   
    });
    //For apending another rows end

    $(wrapper).on("click",".remove_field", function(e)
    {
        e.preventDefault();

        var x = $(this).attr("data-val");

        $('.di_'+x).remove(); x--;
    });

    function calculateActualAmount(x)
    {
        var quantity                = $("#quantity_"+x).val();
        var stock                   = $("#stock_"+x).val();
        
        if (quantity == '')
        {
            var quantityCal         = 1;
        }
        else
        {
            var quantityCal         = $("#quantity_"+x).val();
        }

        //Checking Overselling Start
        var check_quantity  = parseFloat(quantity);
        var check_stock     = parseFloat(stock);

        if (check_quantity > check_stock)
        {   
            $("#quantity_"+x).val(check_stock);
        }
        //Checking Overselling End
    }
</script>

<script type="text/javascript">
    function addButton()
    {
        $('.add_field_button').click();
    }
</script>

<script type="text/javascript">
    function preventDoubleClick()
    {
        $('.enableOnInput').prop('disabled', true)
        $('#FormSubmit').submit();
    }
</script>

<script type="text/javascript">
    $(document).on("click",".transfer",function(){
        var data_date           = $(this).attr('data-date');
        var data_transfer_from  = $(this).attr('data-transfer-from');
        var data_transfer_to    = $(this).attr('data-transfer-to');
        var site_url            = $('.site_url').val();

        if(data_date)
        {
            $.get(site_url + '/stocktransfers/transfer-list/'+ data_date, function(data){
               
                var transferlist  = '';
               
                    $.each(data, function(i, transfer_data){
                        
                            if(transfer_data.note != null)
                            {
                                transfer_note = transfer_data.note;
                            }else{
                               transfer_note = ''; 
                            }
                            transferlist    += '<tr><td>'+ transfer_data.date +' </td><td>'+ data_transfer_from +' </td><td>'+ data_transfer_to +' </td><td>'+ transfer_data.name +' </td><td style="text-align: center">'+ transfer_data.quantity +' </td></tr>';

                    });
                
                $("#transfer_list").empty();
                $("#transfer_list").append(transferlist);
            });
        }
    });
</script>
@endsection