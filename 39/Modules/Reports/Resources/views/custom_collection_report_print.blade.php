<!DOCTYPE html>
<html>

<head>
    <title>Summary Report</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">
</head>

<style type="text/css" media="print">        
    @page {
        size: auto;   
        margin: 15mm 5mm 5mm 10mm;
    }

    #print-footer {
        display: none;
        position: fixed;
        bottom: 0;
        left:0;
    }

    #hide-print {
        display: none;
    }

    #print-footer {
        display: none;
    }

    body {
        margin-bottom:10mm;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #ddd;
    }
    
    fontSizeC {
        font-size: 10px !important;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Daily Report</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date : {{ date('d-m-Y', strtotime($from_date)) }}</th>
                                </tr>
                            </thead>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="font-size: 10px;text-align: center;font-size: 15px" colspan="3">Income/Receive</th>

                                    <th style="font-size: 10px;text-align: center;font-size: 15px" colspan="4">Expense/Paid</th>
                                </tr>

                                <tr>
                                    <th style="font-size: 10px;text-align: center;width: 5%">SL</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">Description</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">Note</th>
                                    <th style="font-size: 10px;text-align: center;width: 5%">Total</th>


                                    <th style="font-size: 10px;text-align: center;width: 5%">SL</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">Description</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">Note</th>
                                    <th style="font-size: 10px;text-align: center;width: 5%">Total</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                @if(isset($result))
                                @for($i =0; $i < $count_sum; $i++)
                                <tr>
                                    @if(isset($result['credit'][$i]))
                                        <td style="font-size: 10px;text-align: center;">{{ $i + 1 }}</td>
                                        <td style="font-size: 10px;text-align: left;">{{ $result['credit'][$i]['customer'] != null ? $result['credit'][$i]['customer'] : '' }}  <strong>{{ $result['credit'][$i]['description'] }}</strong></td>
                                        <td style="font-size: 10px;text-align: left;"> <strong>{{ $result['credit'][$i]['note'] }}</strong></td>
                                        <td style="font-size: 10px;text-align: right;">{{ $result['credit'][$i]['amount'] != 0 ? number_format($result['credit'][$i]['amount'],0,'.',',') : '' }}</td>
                                    @else
                                        <td style="font-size: 10px;text-align: center;">{{ $i + 1 }}</td>
                                        <td style="font-size: 10px;text-align: left;"></td>
                                        <td style="font-size: 10px;text-align: right;"></td>
                                        <td style="font-size: 10px;text-align: right;"></td>
                                    @endif



                                    @if(isset($result['debit'][$i]))
                                    <td style="font-size: 10px;text-align: center;">{{ $i + 1 }}</td>
                                    <td style="font-size: 10px;text-align: left;">{{ $result['debit'][$i]['customer'] != null ? $result['debit'][$i]['customer'] : '' }} <strong>{{ $result['debit'][$i]['description'] }}</strong></td>
                                    <td style="font-size: 10px;text-align: left;"> <strong>{{ $result['debit'][$i]['note'] }}</strong></td>
                                    <td style="font-size: 10px;text-align: right;">{{ number_format($result['debit'][$i]['amount'],0,'.',',') }}</td>
                                    @else
                                    <td style="font-size: 10px;text-align: center;">{{ $i + 1 }}</td>
                                    <td style="font-size: 10px;text-align: left;"></td>
                                    <td style="font-size: 10px;text-align: right;"></td>
                                    <td style="font-size: 10px;text-align: right;"></td>
                                    @endif
                                </tr>
                                @endfor
                                @endif
                                
                                @if($PaySlipsAmount > 0)
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Salary</td>
                                    <td></td>
                                    <td style="text-align: right;">{{$PaySlipsAmount}}</td>
                                </tr>
                                @endif
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="3" style="text-align: right;">Total </th>
                                    <th colspan="1" style="text-align: right;">{{ number_format($credit_sum,0,'.',',')  }}</th>
                                    
                                    <th colspan="3" style="text-align: right;">Total</th>
                                    <th colspan="1" style="text-align: right;">{{ number_format($debit_sum + $PaySlipsAmount,0,'.',',')}}</th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>