<!DOCTYPE html>
<html>

<head>
    <title>Cash Book</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Cash Book</h6>
                    </div>

                    <div class="ibox-content">
                        <!-- Header Start -->
                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Header End -->

                        <!-- Sales Statement Start -->
                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">SL</th>
                                    <th style="text-align: center;width: 9%">DATE</th>
                                    <th style="text-align: center;width: 10%">NUMBER</th>
                                    <th style="text-align: center;width: 10%">HEAD</th>
                                    <th style="text-align: center;width: 10%">PARTICULARS</th>
                                    <th style="text-align: center;width: 8%">INCOME</th>
                                    <th style="text-align: center;width: 8%">EXPENSE</th>
                                    <th style="text-align: center;width: 10%">BALANCE</th>
                                    <th style="text-align: center;width: 10%">ENTRY BY</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php
                                    $i                = 1;
                                    $total_debit      = 0;
                                    $total_credit     = 0;
                                    $total_balance    = $opening_balance_result;
                                ?>

                                <tr>
                                    <th colspan="7" style="text-align: right;">Opening Balance</th>
                                    <th style="text-align: right;">{{ $opening_balance_result }}</th>
                                    <th style="text-align: right;"></th>
                                </tr>

                                @foreach($data as $key => $value)

                                <?php
                                    if ($value['type'] == 0)
                                    {
                                        $debit          = $value['amount'];
                                        $total_debit    = $total_debit + $value['amount'];
                                        $credit         = 0;
                                    }
                                    else
                                    {
                                        $credit         = $value['amount'];
                                        $total_credit   = $total_credit + $value['amount'];
                                        $debit          = 0;
                                    }

                                    $total_balance      = $total_balance + $credit - $debit;

                                    if ($value['account_head'] == 'sales')
                                    {
                                        $trans_number   = 'INV - '.str_pad($value->invoice->invoice_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'previoue-due-collection')
                                    {
                                        $trans_number   = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'sales-return-payment')
                                    {
                                        $trans_number   = '';
                                    }
                                    elseif ($value['account_head'] == 'purchase-return-payment')
                                    {
                                        $trans_number   = '';
                                    }
                                    elseif ($value['account_head'] == 'sales-return')
                                    {
                                        $trans_number   = 'SR - '.str_pad($value->salesReturn->sales_return_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'purchase')
                                    {
                                        $trans_number   = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'previoue-due-paid')
                                    {
                                        $trans_number   = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'purchase-return')
                                    {
                                        $trans_number   = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'payment')
                                    {
                                        $trans_number   = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'customer-opening-balance')
                                    {
                                        $trans_number   = '';
                                    }
                                    elseif ($value['account_head'] == 'supplier-opening-balance')
                                    {
                                        $trans_number   = '';
                                    }
                                ?>

                                <tr>
                                    <td style="text-align: center;">{{ $i }}</td>
                                    <td style="text-align: center;">{{ $value['date'] }}</td>
                                    <td style="text-align: center;">{{ $trans_number }}</td>
                                    <td style="text-align: center;">{{ $value['account_head'] }}</td>
                                    <td style="text-align: center;">{{ $value['note'] }}</td>
                                    <td style="text-align: right;">{{ $credit }}</td>
                                    <td style="text-align: right;">{{ $debit }}</td>
                                    <td style="text-align: right;">{{ $total_balance }}</td>
                                    <td style="text-align: left;">{{ $value->createdBy->name }}</td>
                                </tr>

                                <?php
                                    $i++;
                                ?>

                                @endforeach
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="5" style="text-align: right;">TOTAL</th>
                                    <th style="text-align: right;">{{ $total_credit }}</th>
                                    <th style="text-align: right;">{{ $total_debit }}</th>
                                    <th style="text-align: right;">{{ $total_balance }}</th>
                                    <th style="text-align: right;"></th>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- Sales Statement End -->

                        <!-- Footer Start -->
                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>