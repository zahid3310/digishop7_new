<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class UnitConversions extends Model
{  
    protected $table = "unit_conversions";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function mainUnitId()
    {
        return $this->belongsTo('App\Models\Units','main_unit_id');
    }

    public function convertedUnitId()
    {
        return $this->belongsTo('App\Models\Units','converted_unit_id');
    }

    public function productEntryId()
    {
        return $this->belongsTo('App\Models\ProductEntries','product_entry_id');
    }
}
