<!DOCTYPE html >
<html>

<head>
    <meta charset="utf-8">
    <title>Cash Memo</title>
    <link href="https://fonts.maateen.me/bangla/font.css" rel="stylesheet">

    <style>
        *{margin:0;padding:0;outline:0}

        body {
            font-size:14px;
            line-height:18px;
            color:#000;
            height:292mm;
            width: 203mm;/*297*/
            margin:0 auto;
            font-family: 'Bangla', Arial, sans-serif !important;  
        }

        table,th {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 10px;
        }

        td {
            border-left: 1px solid black;
            border-right: 1px solid black;
            padding: 10px;
        }

        .bg_templete{
            background:url(images/bg3.png) no-repeat;
            background-size: 100%;
            -webkit-print-color-adjust: exact;
        }

        .templete{
            width:90%;
            background:none;
            margin:0 auto;
        }
        
        .clear{
            overflow:hidden;
        }

        .text-center{
            text-align: center;
        }

        .text-end{
            text-align: end;
        }

        .item2 {
            grid-area: com_logo;
        }

        .item3 {
            grid-area: com_info;
        }

        .item4 {
            grid-area: other_info;
        }

        .grid-container {
            display: grid;
            grid-template-areas: "com_logo com_info other_info ";
            /*padding-top: 10px;*/
        }

        .grid-container div {
            text-align: center;
        }

        .grid-container-aline-end {
            text-align: end !important;
        }

        .grid-container-aline-start {
            text-align: start !important;
        }

        .com_logo_img {
            height: 80px;
            width: 80px;
        }

        .tr-height{
            padding: 10px;
        }

        .item11 { grid-area: sig_text2; }
        .item22 { grid-area: some_info; }
        .item33 { grid-area: sig1; }
        .item44 { grid-area: sig2; }
        .item55 { grid-area: sig_text1; }

        .grid-container-1 {
            display: grid;
            grid-template-areas:
            ' sig1 sig1 some_info some_info sig2 sig2'
            ' sig_text1 sig_text1 some_info some_info sig_text2 sig_text2';
        }

        .signaturesection1 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 45px;
        }

        .signaturesection2 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 45px;
        }

        .borderNone{border-left: 1px solid #fff;border-bottom:1px solid #fff ;}

        @page  {
            size: A4;
            page-break-after: always;
        }
    </style>
</head>

<body>
    <div class="bg_templete clear">

        <div class="templete clear">

            <br>
            <br>
            <p style="line-height: 2;text-align: center"><span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 20px">Cash Memo</span></p>
            <div class="grid-container">
                <div class="item2 grid-container-aline-start">
                    <img class="com_logo_img" src="<?php echo e(url('public/'.userDetails()->logo)); ?>"alt="qr sample"
                    />
                </div>

                <div class="item3">
                    <h2 style="line-height: 1;font-size: 60px"><?php echo e(userDetails()->organization_name); ?></h2>
                    <h4 style="line-height: 1;font-size: 20px"><?php echo e(userDetails()->address); ?></h4>
                    <!--<p style="line-height: 1;font-size: 20px"><?php echo e(userDetails()->contact_number); ?></p>-->
                     <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">
                                                Proprietor : 01733-846666 || Showroom : 01317-349956
                                            </p>
                    <p style="line-height: 1;font-size: 20px"><?php echo e(userDetails()->contact_email); ?></p>
                   
                    
                </div>

                <div class="item4 grid-container-aline-end">

                    <p style="padding-right: 25px;line-height: 1;font-size: 28px"></p>
                    
                </div>
            </div>

            <hr>
            
            <div style="padding-top: 5px;text-align: center" class="col-md-12"><input type="checkbox"> Customer Copy &nbsp;&nbsp;&nbsp;<input type="checkbox"> Office Copy</div>

            <div style="font-size: 18px;padding-top: 5px;line-height: 1.5">
                <strong>Memo No &nbsp;:  </strong><?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?> <strong style="float: right;">Date : <span><?php echo e(date('d-m-Y', strtotime($invoice['invoice_date']))); ?></span></strong>
            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong><?php echo e($invoice['customer_name'] != null ? $invoice['customer_name'] : $invoice['contact_name']); ?>

            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Address &nbsp;&nbsp;: </strong><?php echo e($invoice['customer_address'] != null ? $invoice['customer_address'] : $invoice['contact_address']); ?>

            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Phone &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong><?php echo e($invoice['customer_phone'] != null ? $invoice['customer_phone'] : $invoice['contact_phone']); ?>

            </div>

            <div style="padding-top: 10px;padding-bottom: 20px">
                <table style="width: 100%">
                    <tr>
                        <th style="font-size: 18px;width: 5%">SL</th>
                        <th style="font-size: 18px;width: 60%">Description</th>
                        <th style="font-size: 18px;width: 15%">QTY</th>
                        <th style="font-size: 18px;width: 10%">Rate</th>
                        <th style="font-size: 18px;width: 10%">Total</th>
                    </tr>

                    <?php if($entries->count() > 0): ?>

                    <?php
                        $total_amount                   = 0;
                    ?>

                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                        $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                        $variation_name = ProductVariationName($value['product_entry_id']);

                        if ($value['product_code'] != null)
                        {
                            $productCode  = ' - '.$value['product_code'];
                        }
                        else
                        {
                            $productCode  = '';
                        }

                        if ($value['product_name'] != null)
                        {
                            $category  = ' - '.$value['product_name'];
                        }
                        else
                        {
                            $category  = '';
                        }

                        if ($value['brand_name'] != null)
                        {
                            $brandName  = $value['brand_name'];
                        }
                        else
                        {
                            $brandName  = '';
                        }

                        if (($value['height'] != null) && ($value['width'] != null))
                        {
                            $dimension  = ' - '.$value['height'] . ' X ' . $value['width'];
                        }
                        else
                        {
                            $dimension  = '';
                        }

                        if ($value['unit_name'] != null)
                        {
                            $unit  = ' '.$value['unit_name'];
                        }
                        else
                        {
                            $unit  = ' SFT';
                        }

                        if ($variation_name != null)
                        {
                            $variation  = ' '.$variation_name;
                        }
                        else
                        {
                            $variation  = '';
                        }

                        $previous_due   = $invoice['previous_due_type'] == 2 ? -$invoice['previous_due'] : $invoice['previous_due'];
                        $net_paya   = $value['contact_type'] != 4 ? round($total_amount) : 0;
                        $paid       = $value['contact_type'] != 4 ? round($invoice['invoice_amount'] - $invoice['due_amount']) : 0;
                        $cash_given = $invoice['cash_given'];
                        $dues       = $value['contact_type'] != 4 ? round($net_paya - $paid) : 0;
                    ?>

                    <tr class="tr-height">
                        <td style="text-align: center"><?php echo e($key + 1); ?></td>
                        <td style="padding-left: 30px"><?php echo e($brandName . $productCode . $category . ' - ' . $value['product_entry_name'] . $dimension . $variation); ?></td>
                        <td style="text-align: center"><?php echo e($value['quantity'] . $unit); ?></td>
                        <td style="text-align: center"><?php echo e($value['contact_type'] != 4 ? $value['rate'] : ''); ?></td>
                        <td style="text-align: center"><?php echo e($value['contact_type'] != 4 ? round($value['total_amount'], 2) : ''); ?></td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

                    <?php
                        if ($invoice['vat_type'] == 0)
                        {
                            $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                        }
                        else
                        {
                            $vat_amount  = $invoice['total_vat'];
                        }

                        if ($invoice['total_discount_type'] == 0)
                        {
                            $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                        }
                        else
                        {
                            $discount_on_total_amount  = $invoice['total_discount_amount'];
                        }
                    ?>


                    <?php 

                       $Payable = $net_paya - $invoice['total_discount'];
                       $total_discount = $discount_on_total_amount + $invoice['total_discount'];

                    //   $previous_dues = $pre_dues + $invoice['due_amount'];

                    //   $total_net_payable = $Payable + $vat_amount + $previous_dues - $total_discount ;

                    //   $total_current_due = $total_net_payable - $cash_given ;

                     ?>

                    <tr>
                        <th style="text-align: left" colspan="2">In Words : <span style="font-weight: normal;"><?php echo e($value['contact_type'] != 4 ? numberTowords(round($net_paya - $invoice['total_discount'] - $discount_on_total_amount + $vat_amount)) : ''); ?></span></th>
                        <th></th>
                        <th style="text-align: right"><strong>Payable</strong></th>
                        <th style="text-align: center"><?php echo e($net_paya - $invoice['total_discount'] != 0 ? round($net_paya - $invoice['total_discount']) : ''); ?></th>
                    </tr>



                    <tr>
                        <th style="text-align: right" colspan="2" class="borderNone"><strong></strong></th>
                        <th style="text-align: right" colspan="2"><strong>Vat</strong></th>
                        <th style="text-align: center"><?php echo e(round($vat_amount)); ?></th>
                    </tr>

                    <tr>
                        <th style="text-align: right" colspan="2" class="borderNone"><strong></strong></th>
                        <th style="text-align: right" colspan="2"><strong>Total Discount</strong></th>
                        <th style="text-align: center"><?php echo e(round($discount_on_total_amount + $invoice['total_discount'])); ?></th>
                    </tr>

                     <tr>
                        <th style="text-align: right" colspan="2" class="borderNone"><strong></strong></th>
                        <th style="text-align: right" colspan="2"><strong>Previous Due</strong></th>
                        <th style="text-align: center"><?php echo e($previous_due != 0 ? round($previous_due - $invoice['return_amount']) : ''); ?></th>
                    </tr>

                    <tr>
                        <th style="text-align: right" colspan="2" class="borderNone"><strong></strong></th>
                        <th style="text-align: right" colspan="2"><strong>Net Payable</strong></th>
                        <th style="text-align: center"><?php echo e($previous_due + $invoice['invoice_amount'] != 0 ? round($previous_due + $invoice['invoice_amount']) : ''); ?></th>
                    </tr>

                    <tr>
                        <th style="text-align: right" colspan="2" class="borderNone"><strong></strong></th>
                        <th style="text-align: right" colspan="2"><strong>Net Payable Paid</strong></th>
                        <th style="text-align: center"><?php echo e($cash_given != 0 ? round($cash_given) : ''); ?></th>
                    </tr>
            
                    <?php
                    $amount = $previous_due + $invoice['invoice_amount'] - $cash_given != 0 ? round($previous_due + $invoice['invoice_amount'] - $cash_given) : '';
                    ?>
                    
                    <tr>
                        <th style="text-align: right" colspan="2" class="borderNone"><strong></strong></th>
                        <th style="text-align: right" colspan="2"><strong>Current Due</strong></th>
                        <th style="text-align: center"><?php echo e($previous_due + $invoice['invoice_amount'] - $cash_given != 0 ? round($previous_due + $invoice['invoice_amount'] - $cash_given) : ''); ?></th>
                    </tr>
                    
                </table>
            </div>

            <div class="grid-container-1">

                <div class="item33"></div>  
                <div class="item55"><p class="signaturesection1" style="font-size: 18px;text-align: center">Customer Signature</p></div>

                <div class="item22" style="text-align: center;">
                    <h3 style="font-size: 18px"> </h3>
                    <div>
                        <p style="font-size: 18px"></p>
                        <p style="font-size: 18px"> </p>
                    </div>
                </div>

                <div class="item44"></div>
                <div class="item1"><p class="signaturesection2" style="font-size: 18px;text-align: center">Authorization Signature</p></div>
            </div>
        </div>
    </div>
</body>

</html><?php /**PATH /home/digishop7/public_html/39/Modules/Invoices/Resources/views/show.blade.php ENDPATH**/ ?>