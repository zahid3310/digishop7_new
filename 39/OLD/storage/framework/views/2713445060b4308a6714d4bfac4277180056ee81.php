<!DOCTYPE html>
<html>

<head>
    <title>Daily Report</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: center; min-height: 82px">
                        <p style="font-size: 25px;line-height: 1"><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <?php if($user_info['address'] != null): ?>
                        <p style="font-size: 16px;line-height: .6"><?php echo e($user_info['address']); ?></p>
                        <?php endif; ?>
                        <?php if($user_info['contact_number'] != null): ?>
                        <p style="font-size: 16px;line-height: .6">phone : <?php echo e($user_info['contact_number']); ?></p>
                        <?php endif; ?>
                        <?php if($user_info['contact_email'] != null): ?>
                        <p style="font-size: 16px;line-height: .6">Email : <?php echo e($user_info['contact_email']); ?></p>
                        <?php endif; ?>
                        <?php if($user_info['website'] != null): ?>
                        <p style="font-size: 16px;line-height: .6">Website : <?php echo e($user_info['website']); ?></p>
                        <?php endif; ?>
                        <p style="font-size: 12px;line-height: .6"><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Daily Report</h6>
                    </div>

                    <div class="ibox-content">
                        <!-- Header Start -->
                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">From</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center"><?php echo e($from_date); ?> <strong>To</strong> <?php echo e($to_date); ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Header End -->

                        <!-- Sales Statement Start -->
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead class="theight">
                                        <tr style="background:#ddd;">
                                            <th style="text-align: center;width: 1%">Sl</th>
                                            <th style="text-align: center;width: 20%">Deposit</th>
                                            <th style="text-align: center;width: 10%">Amount</th>
                                        </tr>
                                    </thead>

                                    <tbody class="theight">

                                        <?php
                                            $total_income = 0;
                                        ?>
                                        <?php if($data['incomes']->count() > 0): ?>
                                        <?php $__currentLoopData = $data['incomes']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        
                                        <?php
                                            if ($value['account_head'] == 'sales')
                                            {
                                                $trans_number   = 'Received From Product Sales';
                                            }
                                            elseif ($value['account_head'] == 'previoue-due-collection')
                                            {
                                                $trans_number   = 'Previous Due Collection';
                                            }
                                            elseif ($value['account_head'] == 'sales-return-payment')
                                            {
                                                $trans_number   = 'Sales Return Payment';
                                            }
                                            elseif ($value['account_head'] == 'purchase-return-payment')
                                            {
                                                $trans_number   = 'Receive From Purchase Return';
                                            }
                                            elseif ($value['account_head'] == 'sales-return')
                                            {
                                                $trans_number   = 'Return of Items';
                                            }
                                            elseif ($value['account_head'] == 'purchase')
                                            {
                                                $trans_number   = 'Purchase of Items';
                                            }
                                            elseif ($value['account_head'] == 'previoue-due-paid')
                                            {
                                                $trans_number   = 'Previous Due Paid';
                                            }
                                            elseif ($value['account_head'] == 'purchase-return')
                                            {
                                                $trans_number   = 'Return of Purchase';
                                            }
                                            elseif ($value['account_head'] == 'payment')
                                            {
                                                $trans_number   = 'previous Due Payment';
                                            }
                                            elseif ($value['account_head'] == 'customer-opening-balance')
                                            {
                                                $trans_number   = 'Customer Opening Balance';
                                            }
                                            elseif ($value['account_head'] == 'supplier-opening-balance')
                                            {
                                                $trans_number   = 'Supplier Opening Balance';
                                            }
                                        ?>
                                        <tr>
                                            <td style="text-align:center;"><?php echo e($loop->index+1); ?></td>
                                            <td style="text-align: left;"><?php echo e($value->customer_id != null ? $value->customer->name : ''); ?> <?php echo e($trans_number); ?> </td>
                                            <td style="text-align: right;"><?php echo e($value['amount']); ?></td>
                                        </tr>

                                        <?php
                                            $total_income = $total_income + $value['amount'];
                                        ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>

                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="2" style="text-align: right;">Total</th>
                                            <th style="text-align: right;"><?php echo e($total_income); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead class="theight">
                                        <tr style="background:#ddd;">
                                            <th style="text-align: center;width: 1%">Sl</th>
                                            <th style="text-align: center;width: 20%">Withdraw</th>
                                            <th style="text-align: center;width: 10%">Amount</th>
                                        </tr>
                                    </thead>

                                    <tbody class="theight">

                                        <?php
                                            $total_expense = 0;
                                        ?>

                                        <?php if($data['expenses']->count() > 0): ?>
                                        <?php $__currentLoopData = $data['expenses']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        
                                        <?php
                                            if ($value1['account_head'] == 'sales')
                                            {
                                                $trans_number1   = 'Received From Product Sales';
                                            }
                                            elseif ($value1['account_head'] == 'previoue-due-collection')
                                            {
                                                $trans_number1   = 'Previous Due Collection';
                                            }
                                            elseif ($value1['account_head'] == 'sales-return-payment')
                                            {
                                                $trans_number1   = 'Sales Return Payment';
                                            }
                                            elseif ($value1['account_head'] == 'purchase-return-payment')
                                            {
                                                $trans_number1   = 'Receive From Purchase Return';
                                            }
                                            elseif ($value1['account_head'] == 'sales-return')
                                            {
                                                $trans_number1   = 'Return of Items';
                                            }
                                            elseif ($value1['account_head'] == 'purchase')
                                            {
                                                $trans_number1   = 'Purchase of Items';
                                            }
                                            elseif ($value1['account_head'] == 'previoue-due-paid')
                                            {
                                                $trans_number1   = 'Previous Due Paid';
                                            }
                                            elseif ($value1['account_head'] == 'purchase-return')
                                            {
                                                $trans_number1   = 'Return of Purchase';
                                            }
                                            elseif ($value1['account_head'] == 'payment')
                                            {
                                                $trans_number1   = 'previous Due Payment';
                                            }
                                            elseif ($value1['account_head'] == 'customer-opening-balance')
                                            {
                                                $trans_number1   = 'Customer Opening Balance';
                                            }
                                            elseif ($value1['account_head'] == 'supplier-opening-balance')
                                            {
                                                $trans_number1   = 'Supplier Opening Balance';
                                            }
                                        ?>
                                        <tr>
                                            <td style="text-align:center;"><?php echo e($loop->index+1); ?></td>
                                            <td style="text-align: left;"><?php echo e($value1->expense_id != null ? $value1->expense->expenseCategory->name : ''); ?> <?php echo e($trans_number1); ?> </td>
                                            <td style="text-align: right;"><?php echo e($value1['amount']); ?></td>
                                        </tr>

                                        <?php
                                            $total_expense = $total_expense + $value1['amount'];
                                        ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                    
                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="2" style="text-align: right;">Total</th>
                                            <th style="text-align: right;"><?php echo e($total_expense); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- Sales Statement End -->

                        <!-- Footer Start -->
                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/39/Modules/Reports/Resources/views/daily_report_print.blade.php ENDPATH**/ ?>