require('./bootstrap');
window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import CheckList from './components/CheckList.vue';
import CheckList2 from './components/CheckList2.vue';
import chartOfAccountsNew from './components/Accounts/chartOfAccountsNew.vue';
import chartOfAccountsList from './components/Accounts/chartOfAccounts.vue';
import chartOfAccountsEdit from './components/Accounts/chartOfAccountsEdit.vue';
import dashboardItems from './components/Dashboard/dashboard.vue';

import cashBook from './components/Reports/cashBook.vue';
import attendanceReport from './components/Reports/attendanceReport.vue';
import accountsVoucher from './components/Reports/accountsVoucher.vue';
import currentBalance from './components/Reports/currentBalance.vue';
import billingReportSummaryReport from './components/Reports/BillingReports/summaryReport.vue';
import billingReportDueReport from './components/Reports/BillingReports/dueReport.vue';
import billingReportCollectionReport from './components/Reports/BillingReports/collectionReport.vue';

export const routes = [
	{path: '/check', component: CheckList},
	{path: '/check2', component: CheckList2},
	{path: '/accounts/chart-of-accounts-new', component: chartOfAccountsNew},
	{path: '/accounts/chart-of-accounts', component: chartOfAccountsList},
	{path: '/accounts/chart-of-accounts-edit', component: chartOfAccountsEdit},
	{path: '/home', component: dashboardItems},
	{path: '/accountsreport/cash-book/print', component: cashBook},
	{path: '/accountsreport/accounts-voucher/print', component: accountsVoucher},
	{path: '/accountsreport/current-balance', component: currentBalance},
	{path: '/reports/attendance-report/print', component: attendanceReport},
	{path: '/billing/summary-report/print', component: billingReportSummaryReport},
	{path: '/billing/due-report', component: billingReportDueReport},
	{path: '/billing/collection-report/print', component: billingReportCollectionReport}
];

const router = new VueRouter({
	base: '/pos/billing/water-bill/',
	mode: 'history',
	routes
});

const app = new Vue({
	el: '#app',
	router
});
