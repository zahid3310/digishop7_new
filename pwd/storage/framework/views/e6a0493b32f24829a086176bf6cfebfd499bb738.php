

<?php $__env->startSection('title', 'Company Information'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Company Information</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Company Information</a></li>
                                    <li class="breadcrumb-item active">Manage Information</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<h4 class="card-title">All Branches</h4>

                                <br>

                                <table id="datatable" class="table table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Branch Name</th>
                                            <th>Address</th>
                                            <th>Contact Number</th>
                                            <th>Logo</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                    	<?php if(!empty($branches) && ($branches->count() > 0)): ?>
                                    	<?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                                        <tr>
	                                            <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e($branch['name']); ?></td>
                                                <td><?php echo e($branch['address']); ?></td>
                                                <td><?php echo e($branch['contact_number']); ?></td>
                                                <td>
                                                    <img style="height: 40px;width: 40px" src="<?php echo e(url('public/'.$branch->logo)); ?>">
                                                </td>
	                                            <td>
	                                            	<div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="<?php echo e(route('branch_edit', $branch['id'])); ?>">Edit</a>
                                                        </div>
                                                    </div>
	                                            </td>
	                                        </tr>
	                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                                    <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/pwd/Modules/Branch/Resources/views/index.blade.php ENDPATH**/ ?>