

<?php
    $title = 'Customers';
?>

<?php $__env->startSection('title', $title); ?>

<?php $__env->startPush('styles'); ?>
<style type="text/css">
    body[data-sidebar=dark].vertical-collpsed {
        min-height: 0px !important;
    }
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e($title); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Registers</a></li>
                                    <li class="breadcrumb-item active">Add <?php echo e($title); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('customers_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                <?php echo e(csrf_field()); ?>


                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Position</label>
                                        <input name="position" type="text" class="form-control" placeholder="Enter Position">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Organization Name *</label>
                                        <input name="customer_name" type="text" class="form-control" placeholder="Enter Name" required>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Admin Name</label>
                                        <input name="contact_person" type="text" class="form-control" placeholder="Enter Name">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Admin Mobile Number</label>
                                        <input name="mobile_number" type="number" class="form-control" placeholder="Enter Phone Number">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Admin Email</label>
                                        <input name="email" type="text" class="form-control" placeholder="Enter Email">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Address</label>
                                        <input name="address" type="text" class="form-control" placeholder="Enter Address">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Bill Type</label>
                                        <select style="cursor: pointer" name="bill_type" class="form-control" id="bill_type">
                                            <option value="" selected>--Select Bill Type--</option>
                                            <option value="1">Pre paid</option>
                                            <option value="2">Post Paid</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Bill Amount</label>
                                        <input name="bill_amount" type="text" class="form-control" placeholder="Enter Bill Amount">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Agreement Paper</label>
                                        <input name="image" type="file">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('customers_index')); ?>">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">All Customers</h4>
                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;text-align: center">SL</th>
                                            <th style="width: 15%;text-align: center">Organization Name</th>
                                            <th style="width: 15%;text-align: center">Admin Name</th>
                                            <th style="width: 35%;text-align: center">Address</th>
                                            <th style="width: 10%;text-align: center">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php if(!empty($customers) && ($customers->count() > 0)): ?>
                                        <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td style="text-align: center"><?php echo e($key + 1); ?></td>
                                                <td style="text-align: left"><?php echo e($customer['name']); ?></td>
                                                <td style="text-align: left"><?php echo e($customer['contact_person']); ?></td>
                                                <td style="text-align: left"><?php echo e($customer['address']); ?></td>
                                                <td style="text-align: center">
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="<?php echo e(route('customers_show', $customer['id'])); ?>" target="_blank">Details</a>
                                                            <a class="dropdown-item" href="<?php echo e(route('customers_edit', $customer['id'])); ?>">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    $( document ).ready(function() {
        
        // $('#vertical-menu-btn').click();
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/pwd/Modules/Customers/Resources/views/index.blade.php ENDPATH**/ ?>