

<?php $__env->startSection('title', 'Edit Cash Receipt Voucher'); ?>

<style type="text/css">
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Orders</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">New Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('cash_receipt_voucher_update', $data['voucherSummary']['id'])); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                <?php echo e(csrf_field()); ?>


                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Voucher Date *</label>
                                            <div class="col-md-8">
                                                <input id="voucher_date" name="voucher_date" type="text" value="<?php echo e(date('d-m-Y', strtotime($data['voucherSummary']['VoucherDate']))); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Voucher Number</label>
                                            <div class="col-md-8">
                                                <input id="voucher_number" name="voucher_number" type="text" class="form-control" value="<?php echo e($data['voucherSummary']['VoucherNumber']); ?>" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-5 col-form-label">Money Receipt No.</label>
                                            <div class="col-md-7">
                                                <input id="bill_challan_number" name="bill_challan_number" type="text" class="form-control" value="<?php echo e($data['voucherSummary']['MoneyReceiptNo']); ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-3 col-form-label">Narration</label>
                                            <div class="col-md-9">
                                                <input id="narration" name="narration" type="text" class="form-control" placeholder="Narration" value="<?php echo e($data['voucherSummary']['Narration']); ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label id="bbShow" for="productname" class="col-md-5 col-form-label">Available Cash Balance </label>
                                            <div class="col-md-7">
                                                <input id="cash_balance" name="cash_balance" type="text" value="<?php echo e(cachInHand()); ?>" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-1">
                                        <i id="add_field_button" style="padding: 0.68rem 2.5rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button col-md-3"></i>
                                    </div>
                                </div>


                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #FFD4A3;height: 30px;padding-top: 5px" class="col-md-12">
                                        <h5 style="text-align: center">Cash Receipt Voucher</h5>
                                    </div>
 
                                    <div style="background-color: #D2D2D2;height: 350px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        <?php $__currentLoopData = $data['accountTransaction']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $trans_val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <input type="hidden" name="edit_type_check[]" value="<?php echo e($trans_val->id); ?>">
                                        <div class="row di_<?php echo e($key); ?>">
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                                <label class="show-xs" for="productname">Account Head Name</label>
                                                <select style="width: 100%;cursor: pointer" name="account_head_name[]" class="inner form-control select2" id="account_head_name_<?php echo e($key); ?>" required>
                                                <option value="">--Select Account Head--</option>
                                                <?php if($data["accountsHeadList"]->count() > 0): ?>
                                                <?php $__currentLoopData = $data["accountsHeadList"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $accountsHeadList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($accountsHeadList->id); ?>" <?php echo e($trans_val->HeadID == $accountsHeadList->id ? 'selected' : ''); ?>><?php echo e($accountsHeadList->HeadName); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                                </select>
                                            </div>

                                            <div style="padding: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                                <label class="show-xs" for="productname">Project Name</label>
                                                <select style="width: 100%;cursor: pointer" name="project_name[]" class="inner form-control select2" id="project_name_<?php echo e($key); ?>">
                                                <option value="">--Select Project Name--</option>
                                                <?php if($data["projectList"]->count() > 0): ?>
                                                <?php $__currentLoopData = $data["projectList"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $projectList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($projectList->id); ?>" <?php echo e($trans_val->ProjectID == $projectList->id ? 'selected' : ''); ?>><?php echo e($projectList->ProjectName); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                                </select>
                                            </div>

                                            <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                                <label class="show-xs" for="productname">Register Name</label>
                                                <select style="width: 100%;cursor: pointer" name="register_name[]" class="inner form-control select2" id="register_name_<?php echo e($key); ?>">
                                                <option value="">--Select Register Name--</option>
                                                <?php if($data["registerList"]->count() > 0): ?>
                                                <?php $__currentLoopData = $data["registerList"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $registerList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($registerList->id); ?>" <?php echo e($trans_val->RegisterID == $registerList->id ? 'selected' : ''); ?>><?php echo e($registerList->ClientName); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                                </select>
                                            </div>

                                            <div style="padding: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                <label class="show-xs" for="productname">Particulars</label>
                                                <input type="text" name="particular[]" class="inner form-control" id="particular_<?php echo e($key); ?>" placeholder="Enter Particular" value="<?php echo e($trans_val->Particulars); ?>" />
                                            </div>

                                            <div class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                <label class="show-xs" for="productname">Amount</label>
                                                <input type="text" name="amount[]" class="inner form-control amount" id="amount_<?php echo e($key); ?>" placeholder="Enter Amount" required oninput="calculateActualAmount(<?php echo e($key); ?>)" value="<?php echo e($trans_val->CreditAmount); ?>" />
                                            </div>
                                            
                                            <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field">
                                                <label class="show-xs" for="productname">Action</label>
                                                <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"><?php echo e($key+1); ?></i>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 60px;padding-top: 13px" class="col-md-8">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('voucher_posting_index')); ?>">Close</a></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 60px;padding-top: 13px" class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Sub Total</label>
                                            <div class="col-md-7">
                                                <input type="text" id="subTotalBdt" class="form-control" value="<?php echo e($data['voucherSummary']['TotalAmount']); ?>">
                                                <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount" value="<?php echo e($data['voucherSummary']['TotalAmount']); ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url            = $('.site_url').val();

            // $('#add_field_button').click();
        });
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = <?php echo $data['accountTransaction']->count(); ?>;
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var account_head_label  = '<label class="hidden-xs" for="productname">Account Head Name</label>\n';
                    var project_name_label  = '<label class="hidden-xs" for="productname">Project Name</label>\n';
                    var register_name_label = '<label class="hidden-xs" for="productname">Register Name</label>\n';
                    var particulars_label   = '<label class="hidden-xs" for="productname">Particulars</label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Amount(BDT)</label>\n';  
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner">'+ ' ' + serial +'</i>' +
                                                '</div>\n';
                }
                else
                {
                    var account_head_label  = '';
                    var project_name_label  = '';
                    var register_name_label = '';
                    var particulars_label   = '';
                    var amount_label        = '';  
                    var action_label        = '';

                    var add_btn             = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner">'+ ' ' + serial +'</i>' +
                                                '</div>\n';
                }

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                        '<input type="hidden" name="edit_type_check[]" value="0">' +
                                                        '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Account Head Name</label>\n' +
                                                            account_head_label +
                                                            '<select style="width: 100%;cursor: pointer" name="account_head_name[]" class="inner form-control single_select2" id="account_head_name_'+x+'" required>\n' +
                                                            '<option value="">'+ '--Select Account Head--' +'</option>' +
                                                            '<?php if($data["accountsHeadList"]->count() > 0): ?>' +
                                                            '<?php $__currentLoopData = $data["accountsHeadList"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $accountsHeadList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>' +
                                                            '<option value="<?php echo e($accountsHeadList->id); ?>"><?php echo e($accountsHeadList->HeadName); ?></option>' +
                                                            '<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>' +
                                                            '<?php endif; ?>' +
                                                            '</select>\n' +
                                                        '</div>\n' +

                                                        '<div style="padding: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Project Name</label>\n' +
                                                            project_name_label +
                                                            '<select style="width: 100%;cursor: pointer" name="project_name[]" class="inner form-control single_select2" id="project_name_'+x+'">\n' +
                                                            '<option value="">'+ '--Select Project Name--' +'</option>' +
                                                            '<?php if($data["projectList"]->count() > 0): ?>' +
                                                            '<?php $__currentLoopData = $data["projectList"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $projectList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>' +
                                                            '<option value="<?php echo e($projectList->id); ?>"><?php echo e($projectList->ProjectName); ?></option>' +
                                                            '<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>' +
                                                            '<?php endif; ?>' +
                                                            '</select>\n' +
                                                        '</div>\n' +

                                                        '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Register Name</label>\n' +
                                                            register_name_label +
                                                            '<select style="width: 100%;cursor: pointer" name="register_name[]" class="inner form-control single_select2" id="register_name_'+x+'">\n' +
                                                            '<option value="">'+ '--Select Register Name--' +'</option>' +
                                                            '<?php if($data["registerList"]->count() > 0): ?>' +
                                                            '<?php $__currentLoopData = $data["registerList"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $registerList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>' +
                                                            '<option value="<?php echo e($registerList->id); ?>"><?php echo e($registerList->ClientName); ?></option>' +
                                                            '<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>' +
                                                            '<?php endif; ?>' +
                                                            '</select>\n' +
                                                        '</div>\n' +

                                                        '<div style="padding: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Particulars</label>\n' +
                                                            particulars_label  +
                                                            '<input type="text" name="particular[]" class="inner form-control" id="particular_'+x+'" placeholder="Enter Particular" />\n' +
                                                        '</div>\n' +

                                                        '<div class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Amount</label>\n' +
                                                            amount_label  +
                                                            '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Enter Amount" required oninput="calculateActualAmount('+x+')" />\n' +
                                                        '</div>\n' +
                                                        
                                                        add_btn +
                                                    '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }                                   
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            //Calculating Subtotal Amount
            var total = 0;
            $('.amount').each(function()
            {
                total += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(parseFloat(total).toFixed());
            $("#subTotalBdtShow").val(parseFloat(total).toFixed());
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/pwd/Modules/Invoices/Resources/views/voucherPosting/cash_receipt_voucher_edit.blade.php ENDPATH**/ ?>