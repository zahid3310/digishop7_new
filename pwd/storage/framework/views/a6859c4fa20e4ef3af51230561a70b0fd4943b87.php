<!DOCTYPE html>
<html>

<head>
    <title>Digishop Client Ledger Details</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-top:5mm;
            margin-bottom:5mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>  
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: center;line-height: .8">
                        <p style="font-size: 40px"><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p style="font-size: 20px"><?php echo e($user_info['address']); ?></p>
                        <p style="font-size: 20px"><?php echo e($user_info['contact_number']); ?></p>
                        <p style="font-size: 20px"><?php echo e($user_info['contact_email']); ?></p>
                        <p style="font-size: 14px;text-align: right"><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Digishop Client Ledger Details</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center;font-size: 12px !important">Date From</th>
                                    <th style="text-align: center;font-size: 12px !important">Client Name</th>
                                    <th style="text-align: center;font-size: 12px !important">Client Phone</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center;font-size: 12px !important"><?php echo e($from_date); ?> <strong>To</strong> <?php echo e($to_date); ?></td>
                                    
                                    <td style="text-align: center;font-size: 12px !important">
                                        <?php if($customer_name != null): ?>
                                            <?php echo e($customer_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>
                                    
                                    <td style="text-align: center;font-size: 12px !important">
                                        <?php if($customer_name != null): ?>
                                            <?php echo e($customer_name['phone']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >

                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 7%;vertical-align: bottom">Date</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom">Details</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom">Paid Through</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom">Trans#</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom">Debit</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom">Credit</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom">Balance</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr>
                                    <td style="text-align: right;font-size: 12px !important" colspan="6"><strong>Opening Balance</strong></a>
                                    </td>
                                    <td style="text-align: right;font-size: 12px !important;" colspan="1"><strong><?php echo e(number_format($opening_balance,0,'.',',')); ?></strong></td>
                                </tr>

                                <?php
                                    $i           = 1;
                                    $sub_total   = $opening_balance;
                                ?>
                                <?php if($data->count() > 0): ?>
                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php
                                    if ($value['debit_credit'] == 1)
                                    {
                                        $debit  = $value['amount'];
                                        $credit = 0;
                                    }
                                    else
                                    {
                                        $credit = $value['amount'];
                                        $debit  = 0;
                                    }

                                    $sub_total = $sub_total + $debit - $credit;

                                    if ($value['transaction_head'] == 'payment-receive')
                                    {
                                        $trans_number     = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_note = $value->note;
                                        $register         = $value->customer->name;
                                        $payment_methode  = $value->account->account_name;
                                        $text             = $value->note;
                                    }
                                    elseif ($value['transaction_head'] == 'payment-made')
                                    {
                                        $trans_number       = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = $value->sales_return_id != null ? 'Sales Return of items' : 'Previous Due Paid';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = $value->account->account_name;
                                        $text               = $value->note;
                                    }
                                    elseif ($value['transaction_head'] == 'discount')
                                    {
                                        $trans_number       = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = 'Discount/Less';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = '';
                                        $text               = 'Discount/Less';
                                    }
                                    elseif ($value['transaction_head'] == 'one-time-payment' || $value['transaction_head'] == 'service-charge' || $value['transaction_head'] == 'sms-purchase')
                                    {
                                        $trans_number     = '';
                                        $transaction_note = $value->note;
                                        $register         = $value->customer->name;
                                        $payment_methode  = '';
                                        $text             = $value->note;
                                    }
                                    else
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = '';
                                        $register           = '';
                                        $transaction_note   = '';
                                        $payment_methode    = '';
                                        $text               = '';
                                    } 
                                ?>

                                <tr>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle"><?php echo e(date('d-m-Y', strtotime($value['date']))); ?></a>
                                    </td>

                                    <td style="text-align: left;font-size: 12px !important;vertical-align: middle"><?php echo e($text); ?></td>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle"><?php echo e($payment_methode); ?></td>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle"><?php echo e($trans_number); ?></td>
                                    <td style="text-align: right;font-size: 12px !important;vertical-align: middle"><?php echo e(number_format($debit,0,'.',',')); ?></td>
                                    <td style="text-align: right;font-size: 12px !important;vertical-align: middle"><?php echo e(number_format($credit,0,'.',',')); ?></td>
                                    <td style="text-align: right;font-size: 12px !important;vertical-align: middle"><?php echo e(number_format($sub_total,0,'.',',')); ?></td>
                                </tr>
                             
                            <?php $i++; ?>
                                
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="6" style="text-align: right;font-size: 12px !important">TOTAL</th>
                                    <th colspan="1" style="text-align: right;font-size: 12px !important"><?php echo e(number_format($sub_total,0,'.',',')); ?></th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>©<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | <a href="https://cyberdynetechnologyltd.com/" target="_blank">Cyberdyne Technology Ltd.</a></strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/pwd/Modules/Reports/Resources/views/due_report_customer_details.blade.php ENDPATH**/ ?>