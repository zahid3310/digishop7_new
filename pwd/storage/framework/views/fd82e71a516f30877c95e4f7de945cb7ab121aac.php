

<?php $__env->startSection('title', 'Payment Voucher Print'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Billing</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Billing</a></li>
                                    <li class="breadcrumb-item active">Payment Voucher Print</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['address']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px"><?php echo e($user_info['contact_number']); ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <address>
                                            <strong>Paid By</strong><br>
                                            <?php echo e($payment->customer->ClientName); ?>

                                            <?php if($payment->customer->ClientAddress != null): ?>
                                               <br> <?php echo $payment->customer->ClientAddress; ?> <br>
                                            <?php endif; ?>
                                            <?php if($payment->customer->ClientAddress == null): ?>
                                                <br>
                                            <?php endif; ?>
                                            <?php echo e($payment->customer->ClientContactNumber != null ? $payment->customer->ClientContactNumber : ''); ?>

                                        </address>
                                    </div>

                                    <div class="col-sm-4">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div class="col-sm-4 text-sm-right">
                                        <address class="mt-2 mt-sm-0">
                                            <strong>Payment Date :</strong><br>
                                            <?php echo e(date('d-m-Y', strtotime($payment['payment_date']))); ?><br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <h3 class="font-size-15 font-weight-bold">Payment Details</h3>
                                </div>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                   
                                    <thead>
                                        <tr>
                                            <th>Payment Date</th>
                                            <th>Payment#</th>
                                            <th>Paid Through</th>
                                            <th style="text-align: right">Total</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td><?php echo e(date('d-m-Y', strtotime($payment['payment_date']))); ?></td>
                                            <td><?php echo e($payment['id']); ?></td>
                                            <td><?php echo e($payment->paid_through != null ? $payment->headName->HeadName : ''); ?></td>
                                            <td style="text-align: right"><?php echo e(number_format($payment['amount'],0,'.',',')); ?></td>
                                        </tr>        
                                    </tbody>
                                        
                                </table>

                                <?php if($payment['note'] != null): ?>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6><strong>Note :</strong> <?php echo e($payment['note']); ?></h6>
                                    </div>
                                </div>
                                <?php endif; ?>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/pwd/Modules/Payments/Resources/views/show.blade.php ENDPATH**/ ?>