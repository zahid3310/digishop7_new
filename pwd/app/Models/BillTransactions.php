<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class BillTransactions extends Model
{  
    protected $table = "bill_transactions";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Clients','customer_id');
    }

    public function headName()
    {
        return $this->belongsTo('App\Models\AccountHead','paid_through');
    }
}
