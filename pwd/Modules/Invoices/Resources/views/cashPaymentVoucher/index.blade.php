@extends('layouts.app')

@section('title', 'Cash Payment Voucher')

<style type="text/css">
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Orders</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">New Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('cash_payment_voucher_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                {{ csrf_field() }}

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Voucher Date *</label>
                                            <div class="col-md-8">
                                                <input id="voucher_date" name="voucher_date" type="text" value="{{ Session::has('VoucherDate') ? date('d-m-Y', strtotime(Session::get('VoucherDate'))) : date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Voucher Number</label>
                                            <div class="col-md-8">
                                                <input id="voucher_number" name="voucher_number" type="text" class="form-control" value="{{ $data['voucherNumber'] }}" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Bill/Challan No.</label>
                                            <div class="col-md-8">
                                                <input id="bill_challan_number" name="bill_challan_number" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-3 col-form-label">Narration</label>
                                            <div class="col-md-9">
                                                <input id="narration" name="narration" type="text" class="form-control" placeholder="Narration">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label id="bbShow" for="productname" class="col-md-5 col-form-label">Available Cash Balance </label>
                                            <div class="col-md-7">
                                                <input id="cash_balance" name="cash_balance" type="text" value="{{ cachInHand() }}" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-1">
                                        <i id="add_field_button" style="padding: 0.68rem 2.5rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button col-md-3"></i>
                                    </div>
                                </div>


                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #FFD4A3;height: 30px;padding-top: 5px" class="col-md-12">
                                        <h5 style="text-align: center">Cash Payment Voucher</h5>
                                    </div>
 
                                    <div style="background-color: #D2D2D2;height: 350px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        <div class="row di_0">
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 60px;padding-top: 13px" class="col-md-8">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Save & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('cash_payment_voucher_index') }}">Close</a></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 60px;padding-top: 13px" class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Sub Total</label>
                                            <div class="col-md-7">
                                                <input type="text" id="subTotalBdt" class="form-control">
                                                <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url            = $('.site_url').val();

            $('#add_field_button').click();
        });
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = -1;
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == x + 1)
                {
                    var account_head_label  = '<label class="hidden-xs" for="productname">Account Head Name</label>\n';
                    var project_name_label  = '<label class="hidden-xs" for="productname">Project Name</label>\n';
                    var register_name_label = '<label class="hidden-xs" for="productname">Register Name</label>\n';
                    var particulars_label   = '<label class="hidden-xs" for="productname">Particulars</label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Amount(BDT)</label>\n';  
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner">'+ ' ' + serial +'</i>' +
                                                '</div>\n';
                }
                else
                {
                    var account_head_label  = '';
                    var project_name_label  = '';
                    var register_name_label = '';
                    var particulars_label   = '';
                    var amount_label        = '';  
                    var action_label        = '';

                    var add_btn             = '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner">'+ ' ' + serial +'</i>' +
                                                '</div>\n';
                }

                $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                        '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Account Head Name</label>\n' +
                                                            account_head_label +
                                                            '<select style="width: 100%;cursor: pointer" name="account_head_name[]" class="inner form-control single_select2" id="account_head_name_'+x+'" required>\n' +
                                                            '<option value="">'+ '--Select Account Head--' +'</option>' +
                                                            '@if($data["accountsHeadList"]->count() > 0)' +
                                                            '@foreach($data["accountsHeadList"] as $accountsHeadList)' +
                                                            '<option value="{{ $accountsHeadList->id }}">{{ $accountsHeadList->HeadName }}</option>' +
                                                            '@endforeach' +
                                                            '@endif' +
                                                            '</select>\n' +
                                                        '</div>\n' +

                                                        '<div style="padding: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Project Name</label>\n' +
                                                            project_name_label +
                                                            '<select style="width: 100%;cursor: pointer" name="project_name[]" class="inner form-control single_select2" id="project_name_'+x+'">\n' +
                                                            '<option value="">'+ '--Select Project Name--' +'</option>' +
                                                            '@if($data["projectList"]->count() > 0)' +
                                                            '@foreach($data["projectList"] as $projectList)' +
                                                            '<option value="{{ $projectList->id }}">{{ $projectList->ProjectName }}</option>' +
                                                            '@endforeach' +
                                                            '@endif' +
                                                            '</select>\n' +
                                                        '</div>\n' +

                                                        '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Register Name</label>\n' +
                                                            register_name_label +
                                                            '<select style="width: 100%;cursor: pointer" name="register_name[]" class="inner form-control single_select2" id="register_name_'+x+'">\n' +
                                                            '<option value="">'+ '--Select Register Name--' +'</option>' +
                                                            '@if($data["registerList"]->count() > 0)' +
                                                            '@foreach($data["registerList"] as $registerList)' +
                                                            '<option value="{{ $registerList->id }}">{{ $registerList->ClientName }}</option>' +
                                                            '@endforeach' +
                                                            '@endif' +
                                                            '</select>\n' +
                                                        '</div>\n' +

                                                        '<div style="padding: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Particulars</label>\n' +
                                                            particulars_label  +
                                                            '<input type="text" name="particular[]" class="inner form-control" id="particular_'+x+'" placeholder="Enter Particular" />\n' +
                                                        '</div>\n' +

                                                        '<div class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                            '<label style="display: none" class="show-xs" for="productname">Amount</label>\n' +
                                                            amount_label  +
                                                            '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Enter Amount" required oninput="calculateActualAmount('+x+')" />\n' +
                                                        '</div>\n' +
                                                        
                                                        add_btn +
                                                    '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }                                   
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            //Calculating Subtotal Amount
            var total = 0;
            $('.amount').each(function()
            {
                total += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(parseFloat(total).toFixed());
            $("#subTotalBdtShow").val(parseFloat(total).toFixed());
        }
    </script>
@endsection