<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('partners')->group(function() {
    Route::get('/', 'PartnersController@index')->name('partners_index');
    Route::post('/store', 'PartnersController@store')->name('partners_store');
    Route::get('/edit/{id}', 'PartnersController@edit')->name('partners_edit');
    Route::post('/update/{id}', 'PartnersController@update')->name('partners_update');
    Route::get('/show/{id}', 'PartnersController@show')->name('partners_show');
});

Route::prefix('areas')->group(function() {
    Route::get('/index', 'PartnersController@areasIndex')->name('partners_areas_index');
    Route::post('/store', 'PartnersController@areasStore')->name('partners_areas_store');
    Route::get('/edit/{id}', 'PartnersController@areasEdit')->name('partners_areas_edit');
    Route::post('/update/{id}', 'PartnersController@areasUpdate')->name('partners_areas_update');
});