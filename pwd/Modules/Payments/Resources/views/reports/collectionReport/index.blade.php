@extends('layouts.app')

@section('title', 'Collection Report')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Collection Report</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Billing</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Collection Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="margin-left: 150px;margin-right: 150px" class="row d-print-none">
                    <div class="col-12">
                        <div class="card">
                            <div style="padding-bottom: 4px" class="card-body table-responsive">
                                <form method="get" action="{{ route('billing_collection_report_index_print') }}" enctype="multipart/form-data" target="_blank">
	                                <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label"> From Date </label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="input-group-append col-md-5">
                                                    <span style="border-radius: 0px" class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                    <input style="border-radius: 0px" type="text" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" value="{{ date('d-m-Y') }}" name="from_date">
                                                </div>

                                                <div class="input-group-append col-md-7">
                                                    <label style="text-align: right" for="productname" class="col-md-2 col-form-label"> To </label>
                                                    <span style="border-radius: 0px" class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                    <input style="border-radius: 0px" type="text" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" value="{{ date('d-m-Y') }}" name="to_date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Name/Company Name</label>
                                        <div class="col-md-9">
                                            <select id="customer_id" class="form-control select2" name="customer_id">
                                                <option value="0">All Customer</option>
                                            </select>
                                        </div>
                                    </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"></label>
	                                    <div class="col-md-9">
	                                        <button style="border-radius: 0px;background-color: #297049;color: white" class="btn" type="submit" target="_blank">
	                                        	Print
	                                    	</button>
	                                    </div>
	                                </div>
                            	</form>   
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
        $( document ).ready(function() {
            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/accountsreport/select2-api/get-registers-customers-api/',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });
    </script>
@endsection