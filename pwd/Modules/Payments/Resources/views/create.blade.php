@extends('layouts.app')

@section('title', 'Bill Collection')

<style type="text/css">
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>

@section('content')
    <div class="overlay"></div>

    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Orders</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">New Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->
                
                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div class="row">
                                <div style="margin-top: -10px !important;margin-bottom: -20px !important;padding-left: 35px" class="card-body col-md-4">
                                    <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                        <h6 style="padding: 10px;padding-bottom: 0px;color: black;">Control Area</h6>
                                        
                                        <div class="col-md-12">
                                            <hr style="margin-top: 0px !important">

                                            <div style="margin-bottom: 0.1rem !important" class="form-group row">
                                                <label class="col-md-4 col-form-label">Month</label>
                                                <div class="col-md-8">
                                                    <select style="cursor: pointer" id="month" class="form-control select2">
                                                        <option value="">--Select--</option>
                                                        <option value="Jan" {{ date('m') == 'Jan' ? 'selected' : '' }}>January</option>
                                                        <option value="Feb" {{ date('m') == 'Feb' ? 'selected' : '' }}>February</option>
                                                        <option value="Mar" {{ date('m') == 'Mar' ? 'selected' : '' }}>March</option>
                                                        <option value="Apr" {{ date('m') == 'Apr' ? 'selected' : '' }}>Appril</option>
                                                        <option value="May" {{ date('m') == 'May' ? 'selected' : '' }}>May</option>
                                                        <option value="Jun" {{ date('m') == 'Jun' ? 'selected' : '' }}>Jun</option>
                                                        <option value="Jul" {{ date('m') == 'Jul' ? 'selected' : '' }}>July</option>
                                                        <option value="Aug" {{ date('m') == 'Aug' ? 'selected' : '' }}>August</option>
                                                        <option value="Sep" {{ date('m') == 'Sep' ? 'selected' : '' }}>September</option>
                                                        <option value="Oct" {{ date('m') == 'Oct' ? 'selected' : '' }}>October</option>
                                                        <option value="Nov" {{ date('m') == 'Nov' ? 'selected' : '' }}>November</option>
                                                        <option value="Dec" {{ date('m') == 'Dec' ? 'selected' : '' }}>December</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div style="margin-bottom: 0.1rem !important" class="form-group row">
                                                <label class="col-md-4 col-form-label">Year</label>
                                                <div class="col-md-8">
                                                    <select style="cursor: pointer" id="year" class="form-control select2" name="year">
                                                        <option value="2021" {{ date('Y') == '2021' ? 'selected' : '' }}>2021</option>
                                                        <option value="2022" {{ date('Y') == '2022' ? 'selected' : '' }}>2022</option>
                                                        <option value="2023" {{ date('Y') == '2023' ? 'selected' : '' }}>2023</option>
                                                        <option value="2024" {{ date('Y') == '2024' ? 'selected' : '' }}>2024</option>
                                                        <option value="2025" {{ date('Y') == '2025' ? 'selected' : '' }}>2025</option>
                                                        <option value="2026" {{ date('Y') == '2026' ? 'selected' : '' }}>2026</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div style="margin-bottom: 0.1rem !important" class="form-group row">
                                                <label class="col-md-4 col-form-label">Customer</label>
                                                <div class="col-md-8">
                                                    <select id="customer_id" style="width: 100%" class="form-control select2" name="customer_id" onchange="getCustomerWise()">
                                                        <option value="0" selected>--All Customer--</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12" style="max-height: 455px;overflow-y: auto;overflow-x: auto">

                                            <hr>

                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th id="select-all" style="width: 10%;padding: 0.2rem !important" class="active">
                                                            <input id="selectAll" type="checkbox" class="select-all" onclick="checkVal()" />
                                                        </th>
                                                        <th style="width: 10%;font-size: 12px;padding: 0.2rem !important" class="warning">SL</th>
                                                        <th style="width: 80%;font-size: 12px;padding: 0.2rem !important" class="warning">Name/Company Name</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="voucherList"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: -10px !important;margin-bottom: -20px !important;padding-right: 35px" class="card-body col-md-8">
                                    @if(Session::has('success'))
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        {!! Session::get('success') !!}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    @endif

                                    @if(Session::has('unsuccess'))
                                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                        {!! Session::get('unsuccess') !!}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    @endif

                                    @if(Session::has('errors'))
                                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                        {!! 'Some required fields are missing..!! Please try again..' !!}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    @endif

                                    <form id="FormSubmit" action="{{ route('payments_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                        
                                    {{ csrf_field() }}

                                    <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                        <div class="col-md-5">
                                            <div style="margin-bottom: 5px" class="form-group row">
                                                <label for="productname" class="col-md-5 col-form-label">Collection Date</label>
                                                <div class="col-md-7">
                                                    <input id="payment_date" name="payment_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-7">
                                            <div style="margin-bottom: 5px" class="form-group row">
                                                <label for="productname" class="col-md-2 col-form-label">Note</label>
                                                <div class="col-md-10">
                                                    <input id="note" name="note" type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="margin-top: 5px" class="row">
                                        <div style="background-color: #FFD4A3;height: 30px;padding-top: 5px" class="col-md-12">
                                            <h5 style="text-align: center">BILL DETAILS</h5>
                                        </div>
     
                                        <div style="background-color: #D2D2D2;height: 500px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 5%;font-size: 12px;text-align: center">SL</th>
                                                        <th style="width: 15%;font-size: 12px;text-align: center">BILLING DATE</th>
                                                        <th style="width: 20%;font-size: 12px;text-align: center">CLIENT NAME</th>
                                                        <th style="width: 10%;font-size: 12px;text-align: center">RECEIVABLE</th>
                                                        <th style="width: 10%;font-size: 12px;text-align: center">RECEIVED</th>
                                                        <th style="width: 10%;font-size: 12px;text-align: center">DUES</th>
                                                        <th style="width: 15%;font-size: 12px;text-align: center">AMOUNT</th>
                                                        <th style="width: 15%;font-size: 12px;text-align: center">MR#</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="voucher_details"></tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div style="margin-top: 5px !important;padding-bottom: 0px !important" class="row">
                                        <div style="background-color: #F4F4F7;height: 60px;padding-top: 13px" class="col-md-12">
                                            <div class="form-group row">
                                                <div class="button-items col-lg-12 ">
                                                    <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                    
                                                    <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('payments_create') }}">Close</a></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/accountsreport/select2-api/get-registers-customers-api/',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            customerList(0);
        });

        function getCustomerWise()
        {
            var customerId  = $('#customer_id').val();
            customerList(customerId);
        }

        function customerList(customerId)
        {
            var site_url = $('.site_url').val();
            $.get(site_url + '/payments/all-customers?customer_id='+ customerId, function(data) {

                var list        = '';
                var totalItems  = 0;
                var sl          = 1;
                $.each(data, function(i, data_list)
                {
                    list += '<tr onclick="getVoucherDetails('+data_list.id+')">' +
                                '<td style="width: 10%;padding: 0.2rem !important" class="active">' +
                                    '<input id="checkbox_'+i+'" type="checkbox" class="select-item checkbox" onclick="checkValUnik()" />' +
                                    '<input id="check_status_'+i+'" type="hidden" name="check_box_id[]" class="checkboxId" value="0" />' +
                                '</td>' +
                                '<td style="width: 10%;font-size: 12px;padding: 0.2rem !important" class="success">'+  sl +'</td>' +
                                '<td style="width: 85%;font-size: 12px;padding: 0.2rem !important" class="success">'+  data_list.ClientName +'</td>' +
                            '</tr>';

                    totalItems++;
                    sl++;
                });

                $("#voucherList").empty();
                $("#voucherList").append(list);
                $("#voucherList").append('<tr>' +
                                            '<td style="width: 100%;font-size: 12px;text-align: right;font-weight: bold" class="success" colspan="3">'+ 'Total Clients' + ' &nbsp;&nbsp;&nbsp;' + totalItems +'</td>' + 
                                        '</tr>');
            });
        }
    </script>

    <script type="text/javascript">
        function getVoucherDetails(CustomerId)
        {
            $("body").addClass("loading"); 

            var site_url    = $(".site_url").val();
            var month       = $("#month").val();
            var year        = $("#year").val();

            $.get(site_url + '/payments/contact/list/'+ CustomerId + '?month=' + month + '&year=' + year, function(data) {

                var list    = '';
                var sl      = 1;
                $.each(data.invoices, function(i, data_list)
                {
                    list += '<tr style="table-layout:fixed;">' +
                                '<input type="hidden" value="'+data_list.id+'" name="bill_id[]" />' +
                                '<input type="hidden" value="'+data_list.month+'" name="month[]" />' +
                                '<input type="hidden" value="'+data_list.year+'" name="year[]" />' +
                                '<input type="hidden" value="'+data_list.customer_id+'" name="customer_id[]" />' + 
                                '<td style="font-size: 12px;padding: 0px">' + 
                                    '<input style="width: 100%;text-align: center;cursor: not-allowed" class="form-control" type="text" value="'+sl+'" readonly />' + 
                                '</td>' +
                                '<td style="font-size: 12px;padding: 0px">'+ 
                                    '<input style="width: 100%;text-align: center;cursor: not-allowed" class="form-control" type="text" value="'+data_list.month + ', ' + data_list.year +'" readonly />' +
                                '</td>' +
                                '<td style="font-size: 12px;padding: 0px">' +
                                    '<input style="text-align: left;cursor: not-allowed" class="form-control" type="text" value="'+data_list.ClientName+'" readonly />' + 
                                 '</td>' +
                                '<td style="font-size: 12px;padding: 0px">' + 
                                    '<input style="width: 100%;text-align: center;cursor: not-allowed" class="form-control" type="text" value="'+data_list.amount+'" readonly />' + 
                                '</td>' +
                                '<td style="font-size: 12px;padding: 0px">' + 
                                    '<input style="width: 100%;text-align: center;cursor: not-allowed" class="form-control" type="text" value="'+data_list.amount_paid+'" readonly />' + 
                                '</td>' +
                                '<td style="font-size: 12px;padding: 0px">' + 
                                    '<input style="width: 100%;text-align: center;cursor: not-allowed" class="form-control" type="text" value="'+data_list.amount_due+'" readonly />' + 
                                '</td>' +
                                '<td style="font-size: 12px;padding: 0px">' + 
                                    '<input style="width: 100%;text-align: right" class="form-control" type="text" value="0" name="amount[]" />' + 
                                '</td>' +
                                '<td style="font-size: 12px;padding: 0px">' + 
                                    '<input style="width: 100%;text-align: center" class="form-control" type="text" name="mr_number[]" value="" />' + 
                                '</td>' +
                            '</tr>';

                        sl++;
                });

                $("#voucher_details").empty();
                $("#voucher_details").append(list);

                $("body").removeClass("loading");
            });
        }
    </script>

    <script type="text/javascript">
        function checkVal()
        {
            if ($('#selectAll').is(":checked"))
            {
                $('.checkbox').prop('checked', true);
                $('.checkboxId').val(1);

                getVoucherDetails(0);
            }
            else
            {
                $('.checkbox').prop('checked', false);
                $('.checkboxId').val(0);

                $("#voucher_details").empty();
            }
        }

        function checkValUnik(i)
        {
            if ($('#checkbox_'+i).is(":checked") == true)
            {
                $('#check_status_'+i).val(1);
            }
            else
            {
                $('#check_status_'+i).val(0);
            }
        }
    </script>
@endsection