@extends('layouts.app')

<?php
    $title = 'Edit Client';
?>

@section('title', $title)

@push('styles')
<style type="text/css">
    body[data-sidebar=dark].vertical-collpsed {
        min-height: 0px !important;
    }
</style>
@endpush

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit {{ $title }}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Edit {{ $title }}</a></li>
                                    <li class="breadcrumb-item active">Edit {{ $title }}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('customers_update', $find_customer['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Position</label>
                                        <input name="position" type="text" class="form-control" placeholder="Enter Position" value="{{ $find_customer['position'] }}">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Organization Name *</label>
                                        <input name="customer_name" type="text" class="form-control" placeholder="Enter Name" value="{{ $find_customer['name'] }}" required>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class=" col-form-label">Admin Name</label>
                                        <input name="contact_person" type="text" class="form-control" placeholder="Enter Name" value="{{ $find_customer['contact_person'] }}">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Admin Mobile Number</label>
                                        <input name="mobile_number" type="number" class="form-control" placeholder="Enter Phone Number" value="{{ $find_customer['phone'] }}">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Admin Email</label>
                                        <input name="email" type="text" class="form-control" placeholder="Enter Email" value="{{ $find_customer['email'] }}">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Address</label>
                                        <input name="address" type="text" class="form-control" placeholder="Enter Address" value="{{ $find_customer['address'] }}">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Bill Type</label>
                                        <select style="cursor: pointer" name="bill_type" class="form-control" id="bill_type">
                                            <option value="" selected>--Select Bill Type--</option>
                                            <option value="1" {{ $find_customer['bill_type'] == 1 ? 'selected' : '' }}>Pre paid</option>
                                            <option value="2" {{ $find_customer['bill_type'] == 2 ? 'selected' : '' }}>Post Paid</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Bill Amount</label>
                                        <input name="bill_amount" type="text" class="form-control" placeholder="Enter Bill Amount" value="{{ $find_customer['bill_amount'] }}">
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="example-text-input" class="col-form-label">Agreement Paper</label>
                                        <input name="image" type="file">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Previous Image</label>
                                    <div class="col-md-10">
                                        @if($find_customer['image'] != null)
                                        <img style="height: 80px;width: 80px;padding: 0px" src="{{ url('public/'.$find_customer['image']) }}" class="form-control">
                                        @else
                                        <img style="height: 80px;width: 80px;padding: 0px" src="{{ url('public/default.png') }}" class="form-control">
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('customers_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">All Customers</h4>
                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;text-align: center">SL</th>
                                            <th style="width: 15%;text-align: center">Organization Name</th>
                                            <th style="width: 15%;text-align: center">Admin Name</th>
                                            <th style="width: 35%;text-align: center">Address</th>
                                            <th style="width: 10%;text-align: center">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($customers) && ($customers->count() > 0))
                                        @foreach($customers as $key => $customer)
                                            <tr>
                                                <td style="text-align: center">{{ $key + 1 }}</td>
                                                <td style="text-align: left">{{ $customer['name'] }}</td>
                                                <td style="text-align: left">{{ $customer['contact_person'] }}</td>
                                                <td style="text-align: left">{{ $customer['address'] }}</td>
                                                <td style="text-align: center">
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('customers_show', $customer['id']) }}" target="_blank">Details</a>
                                                            <a class="dropdown-item" href="{{ route('customers_edit', $customer['id']) }}">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            // $('#vertical-menu-btn').click();
        });
    </script>
@endsection