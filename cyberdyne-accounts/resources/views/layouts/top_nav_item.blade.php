<style type="text/css">
    .m-5 {
        margin: 0.7rem 0.1rem !important;
        font-size: 1.4rem !important; 
    }
</style>

<header style="background-color: #73C0DE" id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <div style="padding: 0rem !important" class="navbar-brand-box">
                <a href="{{ route('home') }}" class="logo logo-light">
                    <span class="logo-sm">
                        <img style="width: 40;height: 30px;margin-top: 15px" src="{{ url('public/'.userDetails()->logo) }}" alt="">
                    </span>
                    <span class="logo-lg">
                        @if(userDetails()->logo != null)
                        <img style="width: 50px;height: 50px;margin-top: 15px" src="{{ url('public/'.userDetails()->logo) }}" alt="">
                        @else
                        @endif
                    </span>
                </a>
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
                <i class="fa fa-fw fa-bars"></i>
            </button>
        </div>

        <div class="d-flex">
            <div class="dropdown d-inline-block">
                <button style="font-size: 0.8125rem !important" type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(Auth::user()->photo != null)
                    <img class="rounded-circle header-profile-user" src="{{ url('public/'. Auth::user()->photo) }}"
                        alt="Header Avatar">
                    @else
                    <img class="rounded-circle header-profile-user" src="{{ url('public/default.png') }}"
                        alt="Header Avatar">
                    @endif
                    <span class="d-none d-xl-inline-block ml-1">{{ Auth::user()->name }}</span>
                    <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{ route('users_edit_profile') }}"><i class="bx bx-user-circle font-size-16 align-middle mr-1"></i> Profile</a>
                    @if(Auth::user()->role == 1)
                    <a class="dropdown-item" href="{{ route('users_edit_settings') }}"><i class="bx bx-wrench font-size-16 align-middle mr-1"></i> Settings</a>
                    @endif
                    <a class="dropdown-item text-danger" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> Logout</a> 
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>