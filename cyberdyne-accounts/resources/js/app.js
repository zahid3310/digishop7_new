require('./bootstrap');
window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import CheckList from './components/CheckList.vue';
import CheckList2 from './components/CheckList2.vue';
import chartOfAccountsNew from './components/Accounts/chartOfAccountsNew.vue';
import chartOfAccountsList from './components/Accounts/chartOfAccounts.vue';
import chartOfAccountsEdit from './components/Accounts/chartOfAccountsEdit.vue';
import dashboardItems from './components/Dashboard/dashboard.vue';

// Reports Cash Book Start
	import cashBook from './components/Reports/cashBook.vue';
// Reports Cash Book End


export const routes = [
	{path: '/check', component: CheckList},
	{path: '/check2', component: CheckList2},
	{path: '/accounts/chart-of-accounts-new', component: chartOfAccountsNew},
	{path: '/accounts/chart-of-accounts', component: chartOfAccountsList},
	{path: '/accounts/chart-of-accounts-edit', component: chartOfAccountsEdit},
	{path: '/home', component: dashboardItems},
	{path: '/accountsreport/cash-book/print', component: cashBook}
];

const router = new VueRouter({
	base: '/cyberdyne-accounts/',
	mode: 'history',
	routes
});

const app = new Vue({
	el: '#app',
	router
});
