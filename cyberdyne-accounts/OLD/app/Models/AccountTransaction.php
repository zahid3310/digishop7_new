<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class AccountTransaction extends Model
{  
    protected $table = "tbl_acc_accounts_transaction";

    public function headName()
    {
        return $this->belongsTo('App\Models\AccountHead','HeadID');
    }

    public function cbaHeadName()
    {
        return $this->belongsTo('App\Models\AccountHead','CBAccount');
    }

    public function bankName()
    {
        return $this->belongsTo('App\Models\BankAccounts','HeadID');
    }

    public function projectName()
    {
        return $this->belongsTo('App\Models\Projects','ProjectID');
    }

    public function registerName()
    {
        return $this->belongsTo('App\Models\Clients','RegisterID');
    }
}
