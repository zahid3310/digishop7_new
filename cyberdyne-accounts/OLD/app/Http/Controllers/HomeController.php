<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\SalesReturn;
use App\Models\SalesReturnEntries;
use App\Models\PurchaseReturn;
use App\Models\PurchaseReturnEntries;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\Expenses;
use App\Models\ExpenseEntries;
use App\Models\Users;
use App\Models\VoucherSummary;
use App\Models\AccountTransaction;
use Carbon\Carbon;
use DB;
use Auth;
use Response;
use Artisan;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth()->user()->status == 1)
        {
            return view('home');
        }
        else
        {
            return back();
        }
    }

    public function dashboardItems()
    {
        $branch_id                          = Auth::user()->branch_id;
        $vouchers                           = VoucherSummary::get();
        $data['total_unposted_voucher']     = $vouchers->where('Status', 0)->count();
        $data['todays_posted_voucher']      = $vouchers->where('Status', 1)->where('VoucherDate', date('Y-m-d'))->count();
        $data['todays_unposted_voucher']    = $vouchers->where('Status', 0)->where('VoucherDate', date('Y-m-d'))->count();
        $data['todays_cash_receipt']        = AccountTransaction::where('VoucherDate', date('Y-m-d'))
                                                                ->where('PurposeAccHeadName', 'cash_ac')
                                                                ->where('DebitAmount', '>', 0)
                                                                ->sum('DebitAmount');
        $data['todays_cash_payment']        = AccountTransaction::where('VoucherDate', date('Y-m-d'))
                                                                ->where('PurposeAccHeadName', 'cash_ac')
                                                                ->where('CreditAmount', '>', 0)
                                                                ->sum('CreditAmount');
        $data['todays_bank_receipt']        = AccountTransaction::where('VoucherDate', date('Y-m-d'))
                                                                ->where('PurposeAccHeadName', 'bank_ac')
                                                                ->where('DebitAmount', '>', 0)
                                                                ->sum('DebitAmount');
        $data['todays_bank_payment']        = AccountTransaction::where('VoucherDate', date('Y-m-d'))
                                                                ->where('PurposeAccHeadName', 'bank_ac')
                                                                ->where('CreditAmount', '>', 0)
                                                                ->sum('CreditAmount');
        $data['this_month_cash_receipt']   = AccountTransaction::whereYear('VoucherDate', date('Y'))
                                                                ->where('PurposeAccHeadName', 'cash_ac')
                                                                ->where('DebitAmount', '>', 0)
                                                                ->select(DB::raw('MONTH(VoucherDate) month'),
                                                                         DB::raw('SUM(DebitAmount) total'))
                                                                ->groupBy(DB::raw('MONTH(VoucherDate)'))
                                                                ->sum('DebitAmount');
        $data['this_month_bank_receipt']    = AccountTransaction::whereYear('VoucherDate', date('Y'))
                                                                ->where('PurposeAccHeadName', 'bank_ac')
                                                                ->where('DebitAmount', '>', 0)
                                                                ->select(DB::raw('MONTH(VoucherDate) month'),
                                                                         DB::raw('SUM(DebitAmount) total'))
                                                                ->groupBy(DB::raw('MONTH(VoucherDate)'))
                                                                ->sum('DebitAmount');
        $data['this_month_cash_payment']    = AccountTransaction::whereYear('VoucherDate', date('Y'))
                                                                ->where('PurposeAccHeadName', 'cash_ac')
                                                                ->where('DebitAmount', '>', 0)
                                                                ->select(DB::raw('MONTH(VoucherDate) month'),
                                                                         DB::raw('SUM(DebitAmount) total'))
                                                                ->groupBy(DB::raw('MONTH(VoucherDate)'))
                                                                ->sum('DebitAmount');
        $data['this_month_bank_payment']    = AccountTransaction::whereYear('VoucherDate', date('Y'))
                                                                ->where('PurposeAccHeadName', 'bank_ac')
                                                                ->where('CreditAmount', '>', 0)
                                                                ->select(DB::raw('MONTH(VoucherDate) month'),
                                                                         DB::raw('SUM(CreditAmount) total'))
                                                                ->groupBy(DB::raw('MONTH(VoucherDate)'))
                                                                ->sum('CreditAmount');

        return Response::json($data);
    }

    public function stockOutItems()
    {
        $products   = ProductEntries::whereRaw('product_entries.stock_in_hand <= product_entries.alert_quantity')
                                ->count();

        return Response::json($products);
    }
}
