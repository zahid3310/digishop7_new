@extends('layouts.app')

@section('title', 'Product Report')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Product Report</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Product Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['organization_name'] }}</h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['address'] }}</p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['contact_number'] }}</p>
                                        <h4 style="line-height: 10px;text-align: center">Stock Report</h4>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="{{ route('stock_report_with_image_index') }}" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div class="col-lg-2 col-md-2 col-sm-6 col-12 d-print-none mb-1">
                                            <div class="input-daterange input-group">
                                                <select id="brand_id" style="width: 100" class="form-control select2" name="brand_id">
                                                    <option value="0" selected>-- All Brands --</option>
                                                    @if(!empty($brands) && ($brands->count() > 0))
                                                    @foreach($brands as $key => $value)
                                                        <option {{ isset($_GET['brand_id']) && ($_GET['brand_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-6 col-12 d-print-none mb-1">
                                            <div class="input-daterange input-group">
                                                <select id="product_id" style="width: 100" class="form-control select2" name="product_id">
                                                    <option value="0" selected>-- All Categories --</option>
                                                    @if(!empty($products) && ($products->count() > 0))
                                                    @foreach($products as $key => $value)
                                                        <option {{ isset($_GET['product_id']) && ($_GET['product_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-6 col-12 d-print-none mb-1">
                                            <div class="input-daterange input-group">
                                                <select id="size_id" style="width: 100" class="form-control select2" name="size_id">
                                                    <option value="0" selected>-- All Sizes --</option>
                                                    @if(!empty($sizes) && ($sizes->count() > 0))
                                                    @foreach($sizes as $key => $value)
                                                        <option {{ isset($_GET['size_id']) && ($_GET['size_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['height'] . ' X ' . $value['width'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-6 col-12 d-print-none mb-1">
                                            <div class="input-daterange input-group">
                                                <select id="product_code" style="width: 100" class="form-control select2" name="product_code">
                                                    @if($code == null)
                                                    <option value="0" selected>-- All Product --</option>
                                                    @else
                                                    <option value="{{ $code['id'] }}" selected>{{ $code['name'] }}</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-6 col-12 d-print-none mb-1">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="type">
                                                    <option {{ $type == 0 ? 'selected' : '' }} value="0">Show All</option>
                                                    <option {{ $type == 1 ? 'selected' : '' }} value="1">Available Stock</option>
                                                    <option {{ $type == 2 ? 'selected' : '' }} value="2">Stock Out</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-6 col-12 d-print-none margin-top-10-xs">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>

                                    </div>
                                </form>

                                <br>
                                @if(count($result) > 0)
                                @foreach($result as $key => $value)
                                <div class="col-md-12">
                                    <h4 style="text-align: center;">Brand : {{ $key }}</h4>
                                </div>

                                    @if(count($value) > 0)
                                    @foreach($value as $key1 => $value1)
                                    <div class="row">
                                        @if(count($value1) > 0)
                                        @foreach($value1 as $key2 => $value2)
                                            <div style="padding-bottom: 40px" class="col-md-12">
                                                <h5 style="text-align: center">Category : {{ $key1 }}</h5>
                                                <h5 style="text-align: center">Size : {{ $key2 }}</h5>
                                            </div>

                                            @if(count($value2) > 0)
                                            @foreach($value2 as $key3 => $value3)
                                            <div style="padding-bottom: 20px" class="col-md-2">
                                                @if($value3['image'] != null)
                                                <img style="height: 150px;width: 150px" src="{{ url('public/'.$value3['image']) }}">
                                                @else
                                                <img style="height: 150px;width: 150px" src="{{ url('public/default.png') }}">
                                                @endif
                                                <span style="text-align: center">{{ 'Code : '.$value3['product_code'] }}<br>{{ ' Stock SFT : '.$value3['stock_in_hand'] }}</span>
                                            </div>
                                            @endforeach
                                            @endif
                                        @endforeach
                                        @endif
                                    </div>
                                    @endforeach
                                    @endif
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {

        var site_url  = $('.site_url').val();

        $("#product_code").select2({
            ajax: { 
            url:  site_url + '/reports/sales-statement/product-list',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                return result['text'];
            },
        });
    });
</script>
@endsection
