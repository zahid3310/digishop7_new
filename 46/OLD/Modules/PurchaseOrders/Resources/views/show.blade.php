<!DOCTYPE html>
<html>

<head>
    <title>Purchase Order Print</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<style type="text/css">
    @page {
      size: A4 landscape;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                    <div class="row">
                        <div class="col-md-2 col-xs-12 col-sm-12"></div>
                        <div class="col-md-8 col-xs-12 col-sm-12">
                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 45px;font-weight: bold">{{ $user_info['organization_name'] }}</h2>
                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 28px;font-weight: bold">{{ 'Mobile : ' .$user_info['contact_number'] }}</p>
                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 28px;font-weight: bold">Email : <span style="font-weight: normal;font-size: 18px">{{ $user_info['contact_email'] }}</span></p>
                        </div>
                        <div class="col-md-2 col-xs-12 col-sm-12"></div>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 10px 15px 0px;">
                        <h6 style="font-size: 26px; margin: 0px;font-weight: bold;text-align: center">Purchase Order</h6>
                    </div>

                    <div style="padding-left: 25px" class="col-md-12"><input type="checkbox"> Supplier Copy</div>
                    <div style="padding-left: 25px;padding-bottom: 5px" class="col-md-12"><input type="checkbox"> Office Copy</div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <tbody class="theight">
                                <tr>
                                    <td>
                                        <span class="col-md-6" style="text-align: left;font-weight: bold;font-size: 15px">Purchase Order To</span>
                                        <span class="col-md-6" style="text-align: right;font-weight: bold;font-size: 15px">Date : {{ date('d-m-Y', strtotime($bill['bill_date'])) }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PO# : {{ str_pad($bill['bill_number'], 6, "0", STR_PAD_LEFT) }}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <tbody class="theight">
                                <tr>
                                    <td style="text-align: left;font-size: 18px">
                                        {{ $bill['customer_name'] }} <br>
                                        @if($bill['address'] != null) <?php echo $bill['address']; ?> <br> @endif
                                        <span>Mobile : </span> @if($bill['phone'] != null) <?php echo $bill['phone']; ?> @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">SL</th>
                                    <th style="text-align: center;width: 20%">Product name</th>
                                    <th style="text-align: center;width: 10%">Code</th>
                                    <th style="text-align: center;width: 10%">Size/Specific</th>
                                    <!-- <th style="text-align: center;width: 10%">Cartoon</th> -->
                                    <!-- <th style="text-align: center;width: 10%">PCS</th> -->
                                    <th style="text-align: center;width: 10%">Qty/SFT</th>
                                    <th style="text-align: center;width: 10%">Rate/SFT</th>
                                    <th style="text-align: center;width: 10%">Amount</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                @if($entries->count() > 0)
                                <?php
                                    $total_cartoon  = 0;
                                    $total_pcs      = 0;
                                    $total_sft      = 0;
                                    $total_amount   = 0;
                                ?>

                                @foreach($entries as $key => $value)

                                <?php
                                    $total_cartoon  = $total_cartoon + $value['cartoon'];
                                    $total_pcs      = $total_pcs + $value['pcs'];
                                    $total_sft      = $total_sft + $value['quantity'];
                                    $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                ?>

                                <tr>
                                    <td style="text-align: center;" rowspan="1">{{ $key + 1 }}</td>
                                    <td style="text-align: left;" rowspan="1">{{ $value['product_entry_name'] }}</td>

                                    <td style="text-align: left;">{{ $value['product_code'] }}</td>
                                    <td style="text-align: center;">{{ $value['height']. ' X ' .$value['width'] }}</td>
                                    <!-- <td style="text-align: center;">{{ $value['cartoon'] }}</td> -->
                                    <!-- <td style="text-align: center;">{{ $value['pcs'] }}</td> -->
                                    <td style="text-align: center;">{{ $value['quantity'] }}</td>
                                    <td style="text-align: center;">{{ $value['rate'] }}</td>
                                    <td style="text-align: center;">{{ $value['rate']*$value['quantity'] }}</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="4" style="text-align: right;">TOTAL</th>
                                    <!-- <th colspan="1" style="text-align: center;">{{ $total_cartoon }}</th> -->
                                    <!-- <th colspan="1" style="text-align: center;">{{ $total_pcs }}</th> -->
                                    <th colspan="1" style="text-align: center;">{{ $total_sft }}</th>
                                    <th colspan="1" style="text-align: center;"></th>
                                    <th colspan="1" style="text-align: center;">{{ $total_amount }}</th>
                                </tr>
                            </tfoot>
                        </table>

                        <table class="table-striped lastt" style="margin-top: 80px;width: 100%">
                            <tr>
                                <td style="width: 15%;text-align: center;border-top: 1px solid !important;border: none;font-size: 16px;">
                                Create By</td>
                                </td>

                                <td style="width: 70%;text-align: center;border: none;font-size: 16px;"></td>
                                  
                                <td style="width: 15%;text-align: center;border-top: 1px solid !important;border: none;font-size: 16px;">Order By</td>
                            </tr>
                       </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>