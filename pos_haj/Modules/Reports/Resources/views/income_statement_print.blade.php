<!DOCTYPE html>
<html>

<head>
    <title>Income Statement Print</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Income Statement</h6>
                    </div>

                    <div class="ibox-content">
                        <!-- Header Start -->
                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Header End -->

                        <!-- Sales Statement Start -->
                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center" colspan="20">
                                        <h6 style="font-size: 16px;font-weight: bold;color: black"> Statement of Sales </h6>
                                    </th>
                                </tr>
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 6%">T/DATE</th>
                                    <th style="text-align: center;width: 7%">SALES ID</th>
                                    <th style="text-align: center;width: 6%">ITEM CODE</th>
                                    <th style="text-align: center;width: 11%">NAME</th>
                                    <th style="text-align: center;width: 4%">U/M</th>
                                    <th style="text-align: center;width: 4%">Qty</th>
                                    <th style="text-align: center;width: 4%">UNIT PRICE</th>
                                    <th style="text-align: center;width: 5%">T/PRICE</th>
                                    <th style="text-align: center;width: 4%">DISCOUNT</th>
                                    <th style="text-align: center;width: 6%">T/PAYABLE</th>
                                    <th style="text-align: center;width: 4%">VAT</th>
                                    <th style="text-align: center;width: 4%">T/DISCOUNT</th>
                                    <th style="text-align: center;width: 4%">N/PAYABLE</th>
                                    <th style="text-align: center;width: 4%">PAID</th>
                                    <th style="text-align: center;width: 4%">DUE</th>
                                    <th style="text-align: center;width: 4%">PROFIT</th>
                                    <th style="text-align: center;width: 4%">LOSS</th>
                                    <th style="text-align: center;width: 10%">CUSTOMER NAME</th>
                                    <th style="text-align: center;width: 2%">TYPE</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i                  = 1;
                                    $total_t_price      = 0;
                                    $total_discount     = 0;
                                    $total_payable      = 0;
                                    $total_vat          = 0;
                                    $total_discount_t   = 0;
                                    $total_net_payable  = 0;
                                    $total_paid         = 0;
                                    $total_due          = 0;
                                    $total_profit       = 0;
                                    $total_loss         = 0;
                                ?>
                                @foreach($sales_data as $key => $value)
                                <tr>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $i }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['invoice_date'] }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['invoice_number'] }}</td>

                                    <td style="text-align: center;">{{ str_pad($value['invoice_entries'][0]['product_code'], 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: center;">{{ $value['invoice_entries'][0]['product_entry_name'] }}</td>
                                    <td style="text-align: center;">{{ $value['invoice_entries'][0]['unit_name'] }}</td>
                                    <td style="text-align: center;">{{ $value['invoice_entries'][0]['quantity'] }}</td>
                                    <td style="text-align: right;">{{ $value['invoice_entries'][0]['rate'] }}</td>
                                    <td style="text-align: right;">{{ $value['invoice_entries'][0]['rate'] * $value['invoice_entries'][0]['quantity'] }}</td>
                                    <td style="text-align: right;">
                                        <?php
                                            if (isset($value['invoice_entries'][0]['discount_type']))
                                            {
                                                if ($value['invoice_entries'][0]['discount_type'] == 0)
                                                {
                                                    $discountAmount0   = ($value['invoice_entries'][0]['rate'] * $value['invoice_entries'][0]['quantity'] * $value['invoice_entries'][0]['discount_amount'])/100;
                                                }
                                                else
                                                {
                                                    $discountAmount0   = $value['invoice_entries'][0]['discount_amount'];
                                                }
                                            }
                                            else
                                            {
                                                $discountAmount0  = 0;
                                            }
                                        ?>

                                        {{ $discountAmount0 }}
                                    </td>
                                    <td style="text-align: right;">{{ ($value['invoice_entries'][0]['rate'] * $value['invoice_entries'][0]['quantity']) - $discountAmount0 }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['vat_type'] == 0 ? $value['vat_perc'] : $value['vat']  }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">
                                        @if(isset($value['total_discount_type']))
                                            {{ $value['total_discount_type'] == 0 ? $value['total_discount_perc'] : $value['total_discount']  }}
                                        @else
                                            0
                                        @endif
                                    </td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['net_payable'] }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['paid_amount'] }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['net_payable'] - $value['paid_amount'] }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['profit_loss'] > 0 ? $value['profit_loss'] : 0  }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['profit_loss'] < 0 ? $value['profit_loss'] : 0  }}</td>

                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['customer_name'] }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['type'] }}</td>
                                </tr>
                                 
                                <?php
                                    $sub_total_t_price      = 0;
                                    $sub_total_discount     = 0;
                                    $sub_total_payable      = 0;
                                ?>

                                @foreach($value['invoice_entries'] as $key1 => $value1)

                                <?php
                                    if (isset($value1['discount_type']))
                                    {
                                        if ($value1['discount_type'] == 0)
                                        {
                                            $discountAmount   = ($value1['rate'] * $value1['quantity'] * $value1['discount_amount'])/100;
                                        }
                                        else
                                        {
                                            $discountAmount   = $value1['discount_amount'];
                                        }
                                    }
                                    else
                                    {
                                        $discountAmount   = 0;
                                    }

                                    $sub_total_t_price  = $sub_total_t_price + ($value1['rate'] * $value1['quantity']);
                                    $sub_total_discount = $sub_total_discount + $discountAmount;
                                    $sub_total_payable  = $sub_total_payable + (($value1['rate'] * $value1['quantity']) - $discountAmount);
                                ?>

                                @if($key1 != 0)
                                <tr>
                                    <td style="text-align: center;">{{ str_pad($value1['product_code'], 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: center;">{{ $value1['product_entry_name'] }}</td>
                                    <td style="text-align: center;">{{ $value1['unit_name'] }}</td>
                                    <td style="text-align: center;">{{ $value1['quantity'] }}</td>
                                    <td style="text-align: right;">{{ $value1['rate'] }}</td>
                                    <td style="text-align: right;">{{ $value1['rate'] * $value1['quantity'] }}</td>
                                    <td style="text-align: right;">
                                        <?php
                                            if ($value1['discount_type'] == 0)
                                            {
                                                $discountAmount   = ($value1['rate'] * $value1['quantity'] * $value1['discount_amount'])/100;
                                            }
                                            else
                                            {
                                                $discountAmount   = $value1['discount_amount'];
                                            }
                                        ?>

                                        {{ $discountAmount }}
                                    </td>
                                    <td style="text-align: right;">{{ ($value1['rate'] * $value1['quantity']) - $discountAmount }}</td>
                                </tr>

                                @endif
                                @endforeach

                                <?php
                                    $i++;

                                    $total_t_price      = $total_t_price + $sub_total_t_price;
                                    $total_discount     = $total_discount + $sub_total_discount;
                                    $total_payable      = $total_payable + $sub_total_payable;
                                    $total_vat          = $total_vat +  ($value['vat_type'] == 0 ? $value['vat_perc'] : $value['vat']);
                                    $total_discount_t   = $total_discount_t + ($value['total_discount_type'] == 0 ? $value['total_discount_perc'] : $value['total_discount']);
                                    $total_net_payable  = $total_net_payable + $value['net_payable'];
                                    $total_paid         = $total_paid + $value['paid_amount'];
                                    $total_due          = $total_due + ($value['net_payable'] - $value['paid_amount']);
                                    $total_profit       = $total_profit + ($value['profit_loss'] > 0 ? $value['profit_loss'] : 0);
                                    $total_loss         = $total_loss + ($value['profit_loss'] < 0 ? $value['profit_loss'] : 0);
                                ?>

                                @endforeach
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="8" style="text-align: right;">TOTAL</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_t_price }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_discount }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_payable }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_vat }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_discount_t }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_net_payable }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_paid }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_due }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_profit }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_loss }}</th>
                                    <th colspan="1" style="text-align: right;"></th>
                                    <th colspan="2" style="text-align: right;"></th>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- Sales Statement End -->

                        <!-- Purchase Statement Start -->
                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center" colspan="18">
                                        <h6 style="font-size: 16px;font-weight: bold;color: black"> Statement of Purchase </h6>
                                    </th>
                                </tr>
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 6%">T/DATE</th>
                                    <th style="text-align: center;width: 7%">Purchase ID</th>
                                    <th style="text-align: center;width: 6%">ITEM CODE</th>
                                    <th style="text-align: center;width: 13%">NAME</th>
                                    <th style="text-align: center;width: 4%">U/M</th>
                                    <th style="text-align: center;width: 4%">Qty</th>
                                    <th style="text-align: center;width: 4%">UNIT PRICE</th>
                                    <th style="text-align: center;width: 6%">T/PRICE</th>
                                    <th style="text-align: center;width: 4%">DISCOUNT</th>
                                    <th style="text-align: center;width: 6%">T/PAYABLE</th>
                                    <th style="text-align: center;width: 4%">VAT</th>
                                    <th style="text-align: center;width: 4%">T/DISCOUNT</th>
                                    <th style="text-align: center;width: 6%">N/PAYABLE</th>
                                    <th style="text-align: center;width: 6%">PAID</th>
                                    <th style="text-align: center;width: 5%">DUE</th>
                                    <th style="text-align: center;width: 10%">Supplier NAME</th>
                                    <th style="text-align: center;width: 2%">TYPE</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i                  = 1;
                                    $total_t_price      = 0;
                                    $total_discount     = 0;
                                    $total_payable      = 0;
                                    $total_vat          = 0;
                                    $total_discount_t   = 0;
                                    $total_net_payable  = 0;
                                    $total_paid         = 0;
                                    $total_due          = 0;
                                ?>
                                @foreach($purchase_data as $key => $value)
                                <tr>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $i }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['bill_date'] }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['bill_number'] }}</td>

                                    <td style="text-align: center;">{{ str_pad($value['bill_entries'][0]['product_code'], 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: center;">{{ $value['bill_entries'][0]['product_entry_name'] }}</td>
                                    <td style="text-align: center;">{{ $value['bill_entries'][0]['unit_name'] }}</td>
                                    <td style="text-align: center;">{{ $value['bill_entries'][0]['quantity'] }}</td>
                                    <td style="text-align: right;">{{ $value['bill_entries'][0]['rate'] }}</td>
                                    <td style="text-align: right;">{{ $value['bill_entries'][0]['rate'] * $value['bill_entries'][0]['quantity'] }}</td>
                                    <td style="text-align: right;">
                                        <?php
                                            if (isset($value['bill_entries'][0]['discount_type']))
                                            {
                                                if ($value['bill_entries'][0]['discount_type'] == 0)
                                                {
                                                    $discountAmount0   = ($value['bill_entries'][0]['rate'] * $value['bill_entries'][0]['quantity'] * $value['bill_entries'][0]['discount_amount'])/100;
                                                }
                                                else
                                                {
                                                    $discountAmount0   = $value['bill_entries'][0]['discount_amount'];
                                                }
                                            }
                                            else
                                            {
                                                $discountAmount0  = 0;
                                            }
                                        ?>

                                        {{ $discountAmount0 }}
                                    </td>
                                    <td style="text-align: right;">{{ ($value['bill_entries'][0]['rate'] * $value['bill_entries'][0]['quantity']) - $discountAmount0 }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['vat_type'] == 0 ? $value['vat_perc'] : $value['vat']  }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">
                                        @if(isset($value['total_discount_type']))
                                            {{ $value['total_discount_type'] == 0 ? $value['total_discount_perc'] : $value['total_discount']  }}
                                        @else
                                            0
                                        @endif
                                    </td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['net_payable'] }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['paid_amount'] }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['net_payable'] - $value['paid_amount'] }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['customer_name'] }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['type'] }}</td>
                                </tr>
                                 
                                <?php
                                    $sub_total_t_price      = 0;
                                    $sub_total_discount     = 0;
                                    $sub_total_payable      = 0;
                                ?>

                                @foreach($value['bill_entries'] as $key1 => $value1)

                                <?php
                                    if (isset($value1['discount_type']))
                                    {
                                        if ($value1['discount_type'] == 0)
                                        {
                                            $discountAmount   = ($value1['rate'] * $value1['quantity'] * $value1['discount_amount'])/100;
                                        }
                                        else
                                        {
                                            $discountAmount   = $value1['discount_amount'];
                                        }
                                    }
                                    else
                                    {
                                        $discountAmount   = 0;
                                    }

                                    $sub_total_t_price  = $sub_total_t_price + ($value1['rate'] * $value1['quantity']);
                                    $sub_total_discount = $sub_total_discount + $discountAmount;
                                    $sub_total_payable  = $sub_total_payable + (($value1['rate'] * $value1['quantity']) - $discountAmount);
                                ?>

                                @if($key1 != 0)
                                <tr>
                                    <td style="text-align: center;">{{ str_pad($value1['product_code'], 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: center;">{{ $value1['product_entry_name'] }}</td>
                                    <td style="text-align: center;">{{ $value1['unit_name'] }}</td>
                                    <td style="text-align: center;">{{ $value1['quantity'] }}</td>
                                    <td style="text-align: right;">{{ $value1['rate'] }}</td>
                                    <td style="text-align: right;">{{ $value1['rate'] * $value1['quantity'] }}</td>
                                    <td style="text-align: right;">
                                        <?php
                                            if ($value1['discount_type'] == 0)
                                            {
                                                $discountAmount   = ($value1['rate'] * $value1['quantity'] * $value1['discount_amount'])/100;
                                            }
                                            else
                                            {
                                                $discountAmount   = $value1['discount_amount'];
                                            }
                                        ?>

                                        {{ $discountAmount }}
                                    </td>
                                    <td style="text-align: right;">{{ ($value1['rate'] * $value1['quantity']) - $discountAmount }}</td>
                                </tr>

                                @endif
                                @endforeach

                                <?php
                                    $i++;

                                    $total_t_price      = $total_t_price + $sub_total_t_price;
                                    $total_discount     = $total_discount + $sub_total_discount;
                                    $total_payable      = $total_payable + $sub_total_payable;
                                    $total_vat          = $total_vat +  ($value['vat_type'] == 0 ? $value['vat_perc'] : $value['vat']);
                                    $total_discount_t   = $total_discount_t + ($value['total_discount_type'] == 0 ? $value['total_discount_perc'] : $value['total_discount']);
                                    $total_net_payable  = $total_net_payable + $value['net_payable'];
                                    $total_paid         = $total_paid + $value['paid_amount'];
                                    $total_due          = $total_due + ($value['net_payable'] - $value['paid_amount']);
                                ?>

                                @endforeach
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="8" style="text-align: right;">TOTAL</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_t_price }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_discount }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_payable }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_vat }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_discount_t }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_net_payable }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_paid }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_due }}</th>
                                    <th colspan="2" style="text-align: right;"></th>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- Purchase Statement End -->

                        <!-- Income Statement Start -->
                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center" colspan="18">
                                        <h6 style="font-size: 16px;font-weight: bold;color: black"> Statement of Income </h6>
                                    </th>
                                </tr>
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">SL</th>
                                    <th style="text-align: center;width: 10%">T/DATE</th>
                                    <th style="text-align: center;width: 10%">Income Number</th>
                                    <th style="text-align: center;width: 15%">Income Category</th>
                                    <th style="text-align: center;width: 13%">Income Note</th>
                                    <th style="text-align: center;width: 10%">Paid Through</th>
                                    <th style="text-align: center;width: 10%">Amount</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i              = 1;
                                    $total          = 0;
                                ?>
                                @foreach($incomes as $key => $value)
                                <tr>
                                    <td style="text-align: center;" >{{ $i }}</td>
                                    <td style="text-align: center;" >{{ date('d-m-Y', strtotime($value['income_date'])) }}</td>
                                    <td style="text-align: center;" >{{ str_pad($value['income_number'], 6, "0", STR_PAD_LEFT) }}</td>

                                    <td style="text-align: center;">{{ $value->incomeCategory->name }}</td>
                                    <td style="text-align: center;">{{ $value['note']}}</td>
                                    <td style="text-align: center;">{{ $value['paid_through_id'] != null ? $value->paidThrough->name : '' }}</td>
                                    <td style="text-align: right;">{{ $value['amount'] }}</td>
                                </tr>

                                <?php
                                    $i++;

                                    $total      = $total + $value['amount'];
                                ?>

                                @endforeach
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th style="text-align: right;" colspan="6">TOTAL</th>
                                    <th style="text-align: right;" colspan="1" >{{ $total }}</th>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- Income Statement End -->

                        <!-- Expense Statement Start -->
                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center" colspan="18">
                                        <h6 style="font-size: 16px;font-weight: bold;color: black"> Statement of Expense </h6>
                                    </th>
                                </tr>
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">SL</th>
                                    <th style="text-align: center;width: 10%">T/DATE</th>
                                    <th style="text-align: center;width: 10%">Expense Number</th>
                                    <th style="text-align: center;width: 15%">Expense Category</th>
                                    <th style="text-align: center;width: 13%">Expense Note</th>
                                    <th style="text-align: center;width: 10%">Paid Through</th>
                                    <th style="text-align: center;width: 10%">Amount</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i              = 1;
                                    $total          = 0;
                                ?>
                                @foreach($expenses as $key => $value)
                                <tr>
                                    <td style="text-align: center;" >{{ $i }}</td>
                                    <td style="text-align: center;" >{{ date('d-m-Y', strtotime($value['expense_date'])) }}</td>
                                    <td style="text-align: center;" >{{ str_pad($value['expense_number'], 6, "0", STR_PAD_LEFT) }}</td>

                                    <td style="text-align: center;">{{ $value->expense_category_name }}</td>
                                    <td style="text-align: center;">{{ $value['note']}}</td>
                                    <td style="text-align: center;">{{ $value['paid_through_id'] != null ? $value->paid_through_accounts_name : '' }}</td>
                                    <td style="text-align: right;">{{ $value['amount'] }}</td>
                                </tr>

                                <?php
                                    $i++;

                                    $total      = $total + $value['amount'];
                                ?>

                                @endforeach
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th style="text-align: right;" colspan="6">TOTAL</th>
                                    <th style="text-align: right;" colspan="1" >{{ $total }}</th>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- Expense Statement End -->

                        <!-- Footer Start -->
                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>