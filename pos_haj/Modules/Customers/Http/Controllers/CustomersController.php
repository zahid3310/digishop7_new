<?php

namespace Modules\Customers\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Customers;
use App\Models\CustomerTypes;
use App\Models\Users;
use Response;
use DB;

class CustomersController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $customers  = Customers::orderBy('created_at', 'DESC')->get();
        $users      = Users::orderBy('created_at', 'DESC')->get();
        $types      = CustomerTypes::orderBy('created_at', 'DESC')->get();

        return view('customers::index', compact('customers', 'users', 'types'));
    }

    public function create()
    {
        return view('customers::create');
    }

    public function store(Request $request)
    {
         //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                              = new Customers;
            $customers->name                        = $data['customer_name'];
            $customers->address                     = $data['address'];
            $customers->phone                       = $data['mobile_number'];
            $customers->nid_number                  = $data['nid_number'];
            $customers->alternative_contact         = $data['alternative_mobile_number'];
            $customers->code                        = $data['code'];
            
            $customers->guarantor_name              = $data['guarantor'];
            $customers->guarantor_address           = $data['guarantor_address'];
            $customers->guarantor_phone             = $data['guarantor_mobile_number'];

            $customers->guarantor2_name              = $data['guarantor2'];
            $customers->guarantor2_address           = $data['guarantor2_address'];
            $customers->guarantor2_phone             = $data['guarantor2_mobile_number'];

            $customers->contact_type                = $data['contact_type'];
            $customers->customer_type               = $data['customer_type'];
            $customers->joining_date                = date('Y-m-d', strtotime($data['joining_date']));
            $customers->designation                 = $data['designation'];
            $customers->salary                      = $data['salary'];
            $customers->user_id                     = $data['user_id'];

            if($request->hasFile('image'))
            {   
                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->image       = $logoUrl;
            }

             if($request->hasFile('guarantor1_image'))
            {   
                $companyLogo            = $request->file('guarantor1_image');
                $logoName               = uniqid().time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/guarantor/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->guarantor1_image       = $logoUrl;
            }

            if($request->hasFile('guarantor2_image'))
            {   
                $companyLogo            = $request->file('guarantor2_image');
                $logoName               =  uniqid().time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/guarantor/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->guarantor2_image       = $logoUrl;
            }

            if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
            {
                $customers->opening_balance         = $data['opening_balance'];
            }

            $customers->created_by                  = $user_id;

            if ($customers->save())
            {   
                if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
                {   
                    openingBalanceStore($data['opening_balance'], $customers['id'], $data['contact_type']);
                }

                DB::commit();
                
                if ((isset($data['contact_type_reference'])))
                {
                    return redirect()->route('customers_index','contact_type='.$data['contact_type_reference'])->with("success","Contact Added Successfully !!");
                }
                else
                {
                    return back()->with("success","Contact Added Successfully !!");
                }
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('customers::show');
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $find_customer  = Customers::find($id);
        $customers      = Customers::orderBy('created_at', 'DESC')->get();
        $users          = Users::orderBy('created_at', 'DESC')->get();
        $types          = CustomerTypes::orderBy('created_at', 'DESC')->get();

        return view('customers::edit', compact('customers', 'find_customer', 'users', 'types'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                              = Customers::find($id);
            $customers->name                        = $data['customer_name'];
            $customers->address                     = $data['address'];
            $customers->phone                       = $data['mobile_number'];
            $customers->nid_number                  = $data['nid_number'];
            $customers->alternative_contact         = $data['alternative_mobile_number'];
            $customers->code                        = $data['code'];
            $customers->guarantor_name              = $data['guarantor'];
            $customers->guarantor_address           = $data['guarantor_address'];
            $customers->guarantor_phone             = $data['guarantor_mobile_number'];
            $customers->guarantor2_name             = $data['guarantor2'];
            $customers->guarantor2_address          = $data['guarantor2_address'];
            $customers->guarantor2_phone            = $data['guarantor2_mobile_number'];
            $customers->contact_type                = $data['contact_type'];
            $customers->customer_type               = $data['customer_type'];
            $customers->joining_date                = date('Y-m-d', strtotime($data['joining_date']));
            $customers->designation                 = $data['designation'];
            $customers->salary                      = $data['salary'];
            $customers->user_id                     = $data['user_id'];

            if($request->hasFile('image'))
            {   
                if ($customers->image != null)
                {
                    unlink('public/'.$customers->image);
                }

                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->image       = $logoUrl;
            }
            
            if($request->hasFile('guarantor1_image'))
            {   
                if ($customers->guarantor1_image != null)
                {
                    unlink('public/'.$customers->guarantor1_image);
                }

                $companyLogo            = $request->file('guarantor1_image');
                $logoName               = uniqid().time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/guarantor/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->guarantor1_image       = $logoUrl;
            }

            if($request->hasFile('guarantor2_image'))
            {   
                if ($customers->guarantor2_image != null)
                {
                    unlink('public/'.$customers->guarantor2_image);
                }

                $companyLogo            = $request->file('guarantor2_image');
                $logoName               = uniqid().time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/guarantor/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->guarantor2_image       = $logoUrl;
            }

            if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
            {
                $customers->opening_balance         = $data['opening_balance'];
            }

            $customers->updated_by                  = $user_id;

            if ($customers->save())
            {   
                if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
                {   
                    openingBalanceUpdate($data['opening_balance'], $customers['id'], $data['contact_type']);
                }

                DB::commit();

                if ((isset($data['contact_type_reference'])))
                {
                    return redirect()->route('customers_index','contact_type='.$data['contact_type_reference'])->with("success","Contact Updated Successfully !!");
                }
                else
                {
                    return redirect()->route('customers_index')->with("success","Contact Updated Successfully !!");
                }
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function customerListLoad()
    {
        $data           = Customers::leftjoin('users', 'users.id', 'customers.user_id')
                                ->leftjoin('customer_types', 'customer_types.id', 'customers.customer_type')
                                ->orderBy('customers.created_at', 'DESC')
                                ->select('customers.*',
                                         'customer_types.name as type',
                                         'users.name as user_name')
                                ->take(500)
                                ->get();

        return Response::json($data);
    }

    public function customerListSearch($id)
    {
        if ($id != 'No_Text')
        {
            $data           = Customers::leftjoin('users', 'users.id', 'customers.user_id')
                                        ->leftjoin('customer_types', 'customer_types.id', 'customers.customer_type')
                                        ->where('customers.name', 'LIKE', "%$id%")
                                        ->orWhere('customers.phone', 'LIKE', "%$id%")
                                        ->orWhere('customers.alternative_contact', 'LIKE', "%$id%")
                                        ->orWhere('customers.address', 'LIKE', "%$id%")
                                        ->orWhere('customers.nid_number', 'LIKE', "%$id%")
                                        ->orderBy('customers.created_at', 'DESC')
                                        ->select('customers.*',
                                                 'customer_types.name as type',
                                                 'users.name as user_name')
                                        ->get();
        }
        else
        {
            $data           = Customers::leftjoin('users', 'users.id', 'customers.user_id')
                                        ->leftjoin('customer_types', 'customer_types.id', 'customers.customer_type')
                                        ->orderBy('customers.created_at', 'DESC')
                                        ->select('customers.*',
                                                 'customer_types.name as type',
                                                 'users.name as user_name')
                                        ->take(500)
                                        ->get();
        }

        return Response::json($data);
    }
}
