<?php

use Illuminate\Support\Facades\Route;
use App\Models\Users;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

	if (Cache::has('key'))
	{
	   Cache::get('key');
	} 
	else 
	{
	   Cache::put('key', 30, 20);
	}

    return view('auth.login');
});

Route::get('/clear', function() {
	$exitCode = Artisan::call('view:clear');
	$exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:cache');
    // $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('config:cache');
    return 'Clear and Config All';
});

Route::get('/get-active-customers/{billing_id}', 'HomeController@getCustomers');