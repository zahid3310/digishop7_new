<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceMeasurementValues extends Model
{
    protected $table = "service_measurement_values";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\TailorServices','service_id');
    }

    public function serviceCategory()
    {
        return $this->belongsTo('App\Models\ServiceMeasurement','service_measurement_id');
    }
}
