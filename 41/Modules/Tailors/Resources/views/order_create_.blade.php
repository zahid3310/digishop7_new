@extends('layouts.app')

@section('title', 'Orders')

<style type="text/css">
    body[data-sidebar=dark].vertical-collpsed {
        min-height: 0px !important;
    }
    
    .select2-container--default .select2-selection--multiple .select2-selection__rendered li {
        max-width: 100%;
        overflow: hidden;
        word-wrap: normal !important;
        white-space: normal;
        text-align: justify;
    }

    #cke_1_top {
        display: none;
    }

    #cke_113_top {
        display: none;
    }

    #cke_224_top {
        display: none;
    }

    #cke_335_top {
        display: none;
    }

    #cke_446_top {
        display: none;
    }

    #cke_557_top {
        display: none;
    }

    #cke_668_top {
        display: none;
    }

    #cke_779_top {
        display: none;
    }

    #cke_890_top {
        display: none;
    }

    #cke_1001_top {
        display: none;
    }
</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('tailors_order_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                {{ csrf_field() }}

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Customer *</label>
                                            <div class="col-md-8">
                                                <select style="width: 75%" id="customer_id" name="customer_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9" required>
                                                   <option value="1">Walk-In Customer</option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Reference</label>
                                            <div class="col-md-8">
                                                <select style="width: 75%" id="reference_id" name="reference_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                   <option value="">Select Reference</option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal1">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Assign To</label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="assign_to_id" name="assign_to_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                   <option value="">--Select Employee--</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Priority </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="priority_id" name="priority_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                   <option value="1">Normal</option>
                                                   <option value="2">High</option>
                                                   <option value="3">Argent</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-5 col-form-label">Order Date *</label>
                                            <div class="col-md-7">
                                                <input id="order_date" name="order_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-5 col-form-label">Delivery Date *</label>
                                            <div class="col-md-7">
                                                <input id="delivery_date" name="delivery_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>

                                         
                                    </div>

                                    <div class="col-md-2">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Status </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="status" name="status" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                   <option value="1">Preparing</option>
                                                   <option value="2">Ready</option>
                                                   <option value="3">Completed</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;" class="form-group row">
                                            <div class="col-md-4">
                                                <button id="add_field_button" style="padding: 10px 40px !important"  class="btn btn-success btn-block inner add_field_button col-md-4"> <i style="text-align:center;"  class="fas fa-plus"></i></button>
                                               
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div style="display: none">
                                    <select style="padding: 6px;border-radius: 4px;cursor: pointer" class="taxType" id="tax_type_0" name="tax_type" oninput="calculateActualAmount(0)">
                                        <option style="padding: 10px" value="1" selected>BDT</option>
                                        <option style="padding: 10px" value="0">%</option>
                                    </select>
                                    <input id="tax_amount_0" type="text" class="form-control width-xs taxAmount" name="tax_amount" value="0" oninput="calculateActualAmount(0)">
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 350px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        <div class="row di_0">
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height:90px;padding-top: 13px" class="col-md-12">

                                        <div class="row">
                                            <div class="col-md-2">
                                                <div style="margin-bottom: 5px" class="form-group">
                                                    <label style="text-align: right">Sub Total</label>
                                                    <input type="text" id="subTotalBdt" class="form-control">
                                                    <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount" readonly>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div style="margin-bottom: 5px" class="form-group">
                                                    <label style="text-align: right">Discount</label>

      
                                                        <select style="border-radius: 4px;cursor: pointer" class="form-control totalDiscountType" id="total_discount_type_0" name="total_discount_type" oninput="calculateActualAmount(0)">
                                                            <option style="padding: 10px" value="1" selected>BDT</option>
                                                            <option style="padding: 10px" value="0">%</option>
                                                        </select>
                                                </div>
                                            </div>

                                            <div class="col-md-1">
                                                <label style="text-align: right">Amount</label>
                                                <input id="total_discount_0" type="text" class="form-control totalDiscount" name="total_discount_amount" value="0" oninput="calculateActualAmount(0)">
                                            </div>

                                            <div class="col-md-3">
                                                <div style="margin-bottom: 5px" class="form-group">
                                                    <label style="text-align: right">Dis. Note</label>
                                              
                                                        <input id="total_discount_note" type="text" class="form-control width-xs" name="total_discount_note" value="{{ old('total_discount_note') }}" placeholder="Discount Note">
                                              
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div style="margin-bottom: 5px" class="form-group">
                                                    <label style="text-align: right">Total Payable</label>
                                                    
                                                        <input type="text" id="totalBdt" class="form-control">
                                                        <input style="display: none" type="text" id="totalBdtShow" name="total_amount" readonly>
                                                   
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                               <div style="margin-bottom: 5px" class="form-group">
                                                    <label style="text-align: right">Cash Given</label>
                                                   
                                                        <input type="text" class="form-control" id="cash_given" name="cash_given" placeholder="Cash Given" oninput="calculateChangeAmount()">
                                                    
                                                </div>
                                            </div>

                                        </div>

                                        </div>
                                    
                                    <div style="margin-bottom: 5px;display: none;" class="form-group row">
                                        <label style="text-align: right" class="col-md-5 col-form-label">VAT</label>

                                        <div style="padding-right: 0px" class="col-md-3">
                                            <select style="border-radius: 4px;cursor: pointer" class="form-control" id="vat_type_0" name="vat_type" oninput="calculateActualAmount(0)">
                                                <option style="padding: 10px" value="0" {{ Auth::user()->vat_type == 0 ? 'selected' : '' }}>%</option>
                                                <option style="padding: 10px" value="1" {{ Auth::user()->vat_type == 1 ? 'selected' : '' }}>BDT</option>
                                            </select>
                                        </div>

                                        <div style="padding-left: 0px" class="col-md-4">
                                            <input id="vat_amount_0" type="text" class="form-control width-xs" name="vat_amount" value="{{ Auth::user()->vat_amount }}" oninput="calculateActualAmount(0)">
                                        </div>
                                    </div>

                                    <div style="margin-bottom: 5px;display: none;" class="form-group row">
                                        <label style="text-align: right" class="col-md-5 col-form-label">Coupon</label>
                                        <div class="col-md-7">
                                            <input id="coupon_code" type="number" class="form-control width-xs couponCode" name="coupon_code" onchange="couponMembership()" placeholder="Coupon/Membership">
                                        </div>
                                    </div>

                                    <div style="margin-bottom: 5px;display:none;" class="form-group row">
                                        <label style="text-align: right" class="col-md-5 col-form-label">Change</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control" id="change_amount" name="change_amount" placeholder="Change Amount">
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>
                                </div>
                                
                                <div style="margin-top: 5px !important;padding-bottom: 0px !important" class="row">
                                    <div style="background-color: #D2D2D2;height: 60px;padding-top: 10px" class="col-md-12">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-4">
                                                <div style="margin-bottom: 5px" class="form-group row">
                                                    <div class="col-md-12">
                                                        <div style="padding-left: 0px" class="form-check form-check-right">
                                                            <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                            <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                                Non Masking
                                                            </label>
                                                        </div>

                                                        <div style="padding-left: 10px" class="form-check form-check-right">
                                                            <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                            <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                                Masking
                                                            </label>
                                                        </div>

                                                        <div style="padding-left: 10px" class="form-check form-check-right">
                                                            <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                            <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                                Voice
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="button-items col-lg-8 ">
                                                <button style="border-radius: 0px !important;float: right" class="btn btn-info waves-effect waves-light" type="button" data-toggle="modal" data-target="#myModal2" onclick="printInvoiceList()">Print Invoices</button>
                                                <button style="border-radius: 0px !important;float: right" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('invoices_index') }}">Close</a></button>
                                                <button style="border-radius: 0px !important;float: right" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Save & Print</button>
                                                <button style="border-radius: 0px !important;float: right" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control">
                        </div>
                    </div>

                    <input id="contact_type" name="contact_type" type="hidden" value="0" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Reference</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name1" name="customer_name" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number1" name="mobile_number" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address1" name="address" type="text" class="form-control">
                        </div>
                    </div>

                    <input id="contact_type1" name="contact_type" type="hidden" value="3" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal2" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Print Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label for="productname" class="col-md-4 col-form-label">Date</label>
                                <div class="col-md-8">
                                    <input style="cursor: pointer" id="search_date" type="date" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Customer </label>
                                <div class="col-md-8">
                                    <input id="customer" type="text" class="form-control"  placeholder="Enter Customer Name">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Search Invoice </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <input style="width: 95%" type="text" class="form-control col-lg-9 col-md-9 col-sm-9 col-9" id="invoiceNumber" placeholder="Enter Invoice ID">
                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="printInvoicesSearch()">
                                            <i class="bx bx-search font-size-24"></i>
                                        </span>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Order#</th>
                                <th>Customer</th>
                                <th>Amount</th>
                                <th>Paid</th>
                                <th>Due</th>
                                <th>Creator</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody id="print_invoice_list">
                        </tbody>
                    </table>
                </div>
                
                <div class="modal-footer">
                    <button id="CloseButton2" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    function what()
    {
        document.getElementById('output').innerHTML = location.search;
        $(".chosen-select").chosen();
    }
</script>

<script type="text/javascript">
    function onTestChange() {
        var key = window.event.keyCode;

        // If the user has pressed enter
        if (key === 13) {
            document.getElementById("txtArea").value = document.getElementById("txtArea").value + "<br>\n";
            // console.log(document.getElementById("txtArea").value);
            return false;
        }
        else {
            return true;
        }
    }
</script>

<script type="text/javascript">
    $( document ).ready(function() {
        $('#vertical-menu-btn').click();
        var site_url  = $('.site_url').val();

        $("#customer_id").select2({
            ajax: { 
            url:  site_url + '/invoices/customer/list/invoices',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['contact_type'] == 0)
                {
                    return result['text'];
                }
            },
        });
        
        $("#reference_id").select2({
            ajax: { 
            url:  site_url + '/invoices/customer/list/invoices',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['contact_type'] == 3)
                {
                    return result['text'];
                }
            },
        });
        
        $("#assign_to_id").select2({
            ajax: { 
            url:  site_url + '/invoices/customer/list/invoices',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['contact_type'] == 2)
                {
                    return result['text'];
                }
            },
        });
        
        $('#add_field_button').click();
        $('#add_field_button_payment').click();
        
        $("#product_0").select2({
            ajax: { 
            url:  site_url + '/bills/product-list-load-bill',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                return result['text'];
            },
        });
    });
</script>

<script type="text/javascript">
    $('#submitBtn').click(function() {
        
        var customer_name               = $("#customer_name").val();
        var address                     = $("#address").val();
        var mobile_number               = $("#mobile_number").val();
        var contact_type                = $("#contact_type").val();
        var site_url                    = $('.site_url').val();

        if (customer_name == '')
        {
            $('#mandatoryAlert').removeClass('hidden');
        }
        else
        {
            $.ajax({
                type:   'post',
                url:    site_url + '/invoices/customer/add/invoices',
                data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
    
                success: function (data) {
                    if(data != 0)
                    {
                        $('#CloseButton').click();
                    }
                    
                    $("#customer_id").empty();
                    $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                }
            });
        }
    });

    $('#submitBtn1').click(function() {
        
        var customer_name               = $("#customer_name1").val();
        var address                     = $("#address1").val();
        var mobile_number               = $("#mobile_number1").val();
        var contact_type                = $("#contact_type1").val();
        var site_url                    = $('.site_url').val();

        if (customer_name == '')
        {
            $('#mandatoryAlert').removeClass('hidden');
        }
        else
        {
            $.ajax({
                type:   'post',
                url:    site_url + '/invoices/customer/add/invoices',
                data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
    
                success: function (data) {
                    if(data != 0)
                    {
                        $('#CloseButton1').click();
                    }
                    
                    $("#reference_id").empty();
                    $('#reference_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                }
            });
        }
    });     
</script>

<script type="text/javascript">
    function ProductEntriesList(x) 
    {
        //For getting item commission information from items table start
        var site_url = $(".site_url").val();

        $.get(site_url + '/tailors/orders/service-list/', function(data){
            var list5 = '';
            var list7 = '';

            $.each(data, function(i, service_data)
            {
                list5 += '<option value = "' +  service_data.id + '">' + service_data.name  + '</option>';
            });

            list7 += '<option value = "">' + '--Select Service--' +'</option>';

            $("#product_entries_"+x).empty();
            $("#product_entries_"+x).append(list7);
            $("#product_entries_"+x).append(list5);
        });
    } 

    function Description(x) 
    {
        //For getting item commission information from items table start
        var site_url = $(".site_url").val();

        $.get(site_url + '/tailors/orders/description-list-load/', function(data){
            var list5 = '';
            // var list7 = '';

            $.each(data, function(i, description_data)
            {
                list5 += '<option value = "' +  description_data.descriptions + '">' + description_data.description_name + '-' + description_data.descriptions   + '</option>';
            });

            // list7 += '<option value = "">' + '--Select Description--' +'</option>';

            $("#description_"+x).empty();
            // $("#description_"+x).append(list7);
            $("#description_"+x).append(list5);
        });
    }

    function ProductList(x)
    {
        //For getting item commission information from items table start
        var site_url = $(".site_url").val();

        $.get(site_url + '/invoices/products/list/', function(data){
            var list5 = '';
            var list7 = '';

            $.each(data, function(i, product_data)
            {
                list5 += '<option value = "' +  product_data.id + '">' + product_data.name  + '</option>';
            });

            list7 += '<option value = "">' + '--Select Product--' +'</option>';

            $("#product_"+x).empty();
            $("#product_"+x).append(list7);
            $("#product_"+x).append(list5);
        });
    }

    function getItemPrice(x)
    {
        //For getting item commission information from items table start
        var entry_id  = $("#product_entries_"+x).val();
        var site_url  = $(".site_url").val();

        if(entry_id)
        {
            $.get(site_url + '/tailors/orders/get-service-price/'+ entry_id, function(data){

                $("#rate_"+x).val(data.services.price);
                $("#quantity_"+x).val(1);
                $("#discount_"+x).val(0);
  
                calculateActualAmount(x);

                var measurementList = '';
                $.each(data.service_measurements, function(i, measurement_list)
                {   
                    var measurementValueList = '';
                    $.each(data.service_measurement_values, function(a, measurement_values_list)
                    {   
                        var measurementValueList1 = '';
                        $.each(measurement_values_list, function(b, measurement_values_data)
                        {   
                            if (a == measurement_list.id)
                            {
                                measurementValueList1 += '<option value = "' +  measurement_values_data.id + '">' + measurement_values_data.value  + '</option>';
                            }
                        });

                        measurementValueList += measurementValueList1;  
                    });

                    measurementList += ' ' + '<div style="margin-bottom: 5px" class="col-lg-4 col-md-4 col-sm-6 col-12">\n' +
                                            '<label class="show-xs" for="productname" id="labelName_'+measurement_list.id+'">'+ measurement_list.name +'</label>\n' +
                                            '<select style="width: 100%" name="" class="inner form-control attrids" id="labelValue_'+measurement_list.id+'" onchange="setDescription('+measurement_list.id+','+x+')">\n' +
                                            '<option value = "">' + '--Select--' +'</option>' +
                                            measurementValueList +
                                            '</select>\n' +
                                        '</div>\n';
                });

                $("#measurement_list_"+x).empty();
                $("#measurement_list_"+x).append(measurementList);

            });
        }
        // calculateActualAmount(x);
    }

    function getItemDescription(x)
    {
        //For getting item commission information from items table start
        var entry_id  = $("#description_"+x).val();
        var site_url  = $(".site_url").val();

        if(entry_id)
        {
            $.get(site_url + '/tailors/orders/description-details/'+ entry_id, function(data){

                $(".description_details_"+x).val(data.descriptions);
            });
        }
        // calculateActualAmount(x);
    }

    function getProductPrice(x)
    {
        //For getting item commission information from items table start
        var entry_id  = $("#product_"+x).val();
        var site_url  = $(".site_url").val();

        if(entry_id)
        {
            $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                var list    = '';
                $.each(data.unit_conversions, function(i, data_list)
                {   
                    list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                });

                $("#unit_id_"+x).empty();
                $("#unit_id_"+x).append(list);

                if (data.product_entries.stock_in_hand == '' || data.product_entries.stock_in_hand == null)
                {
                    var stockInHand  = 0;
                }
                else
                {
                    var stockInHand  = data.product_entries.stock_in_hand;
                }

                if (data.product_entries.unit_id == null)
                {
                    var unit  = '';
                }
                else
                {
                    var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                }

                $("#price_"+x).val(parseFloat(data.product_entries.sell_price).toFixed(2));
                $("#gaz_"+x).val(1);
                $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
                $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' ' + unit);
                $("#main_unit_id_"+x).val(data.product_entries.unit_id);
                $("#main_unit_name_"+x).val(data.product_entries.unit_name);
                $("#discount_"+x).val(0);

                calculateActualAmount(x);

            });
        }
        
        //For getting item commission information from items table end
    }

    function pad (str, max)
    {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }

    function setDescription(id, x)
    {
        var old_val = $(".textArea").html();
        var AtName  = $("#labelName_"+id).html();
        var selID   = document.getElementById("labelValue_"+id);
        var AtVal   = selID.options[selID.selectedIndex].text;
        var result  = old_val + AtVal + ' - ' + AtName + '; ';

        $("#txtArea_"+).text('');
        $("#txtArea_"+).text(result);
    }
</script>

<script type="text/javascript">
    function addButton()
    {
        $('.add_field_button').click();
    }

    function addButtonPayment()
    {
        $('.add_field_button_payment').click();
    }
</script>

<script type="text/javascript">
    var max_fields       = 10;                           //maximum input boxes allowed
    var wrapper          = $(".input_fields_wrap");      //Fields wrapper
    var add_button       = $(".add_field_button");       //Add button ID
    var index_no         = 1;

    //For apending another rows start
    var x = -1;
    $(add_button).click(function(e)
    {
        e.preventDefault();

        // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

        if(x < max_fields)
        {   
            x++;

            var serial = x + 1;

            if (serial == 1)
            {
                var service_label       = '<label class="hidden-xs" for="productname">Service *</label>\n';
                var description       = '<label class="hidden-xs" for="productname">Description *</label>\n';
                var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                var unit_label          = '<label class="hidden-xs" for="productname">Unit</label>\n';
                var product_price       = '<label class="hidden-xs" for="productname">Price *</label>\n';
                var description_label   = '<label class="hidden-xs" for="productname">Description</label>\n';
                var measurement_label   = '<label class="hidden-xs" for="productname">Measurement*</label>\n';
                var gaz_label         = '<label class="hidden-xs" for="productname">Gaz *</label>\n';
                var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">Discount</label>\n';
                var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n';
                var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                    action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                            '</div>\n';
            }
            else
            {
                var service_label       = '';
                var description         = '';
                var product_label       = '';
                var unit_label          = '';
                var description_label   = '';
                var measurement_label   = '';
                var rate_label          = '';
                var product_price       = '';
                var gaz_label         = '';
                var quantity_label      = '';
                var discount_label      = '';
                var type_label          = '';
                var amount_label        = '';
                var action_label        = '';

                var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                    action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                            '</div>\n';
            }

            Description(x);
            ProductEntriesList(x);
            ProductList(x);

            $('.getMultipleRow').prepend(' ' + '<div class="row di_'+x+'">' +
                                                '<div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                    '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                    service_label +
                                                    '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                    '</select>\n' +
                                                '</div>\n' +

                                                '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                    '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                    rate_label +
                                                    '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                '</div>\n' +

                                                '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                    '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                    quantity_label  +
                                                    '<input type="text" name="service_quantity[]" class="inner form-control" id="quantity_'+x+'" placeholder="Quantity" oninput="calculateActualAmount('+x+')" required />\n' +
                                                '</div>\n' +

                                                '<div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                    '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                    product_label +
                                                    '<select style="width: 100%" name="product[]" class="inner form-control single_select2 product" id="product_'+x+'" onchange="getProductPrice('+x+')">\n' +
                                                    '</select>\n' +
                                                '</div>\n' +

                                                '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +
                                                '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +

                                                '<div class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                    '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                    unit_label +
                                                    '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_'+x+'" onchange="getConversionParam('+x+')">\n' +
                                                    '<option value="">Unit</option>'+
                                                    '</select>\n' +
                                                '</div>\n' +

                                                '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                    '<label style="display: none" class="show-xs" for="productname">Product Price *</label>\n' +
                                                    product_price +
                                                    '<input type="text" name="price[]" class="inner form-control" id="price_'+x+'" placeholder="Price" oninput="calculateActualAmount('+x+')" />\n' + 
                                                '</div>\n' +

                                                '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                    '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                    gaz_label  +
                                                    '<input type="text" name="quantity[]" class="inner form-control" id="gaz_'+x+'" placeholder="Gaz" oninput="calculateActualAmount('+x+')" />\n' +
                                                '</div>\n' +

                                                '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                    amount_label +
                                                    '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                '</div>\n' +

                                                '<div style="margin-bottom: 5px;padding-left: 26px" class="col-lg-4 col-md-4 col-sm-12 col-5">\n' +
                                                    '<label style="display: none" class="show-xs" for="productname">Measurement* </label>\n' +
                                                    measurement_label +
                                                    '<div style="padding-left: 0px;padding: 0px" class="row">\n'  + 
                                                      '<div style="padding-right: 0px" class="col-md-2">\n' +
                                                        '<input type="text" name="row_1[]" class="inner form-control" id="row_1_'+x+'" />\n' +
                                                        '<input type="text" name="row_2[]" class="inner form-control" id="row_2_'+x+'" />\n' +
                                                        '<input type="text" name="row_3[]" class="inner form-control" id="row_3_'+x+'" />\n' +
                                                        '<input type="text" name="row_4[]" class="inner form-control" id="row_4_'+x+'" />\n' +
                                                        '<input type="text" name="row_5[]" class="inner form-control" id="row_5_'+x+'" />\n' +
                                                        '<input type="text" name="row_6[]" class="inner form-control" id="row_6_'+x+'" />\n' +
                                                      '</div>\n' +
                                                      
                                                      '<div style="padding-left: 0px;padding-right: 0px" class="col-md-2">\n' +
                                                        '<input type="text" name="row_7[]" class="inner form-control" id="row_7_'+x+'" />\n' +
                                                        '<input type="text" name="row_8[]" class="inner form-control" id="row_8_'+x+'" />\n' +
                                                        '<input type="text" name="row_9[]" class="inner form-control" id="row_9_'+x+'" />\n' +
                                                        '<input type="text" name="row_10[]" class="inner form-control" id="row_10_'+x+'" />\n' +
                                                        '<input type="text" name="row_11[]" class="inner form-control" id="row_11_'+x+'" />\n' +
                                                        '<input type="text" name="row_12[]" class="inner form-control" id="row_12_'+x+'" />\n' +
                                                      '</div>\n' +
                                                      
                                                      '<div style="padding-left: 0px;padding-right: 0px" class="col-md-2">\n' +
                                                        '<input type="text" name="row_13[]" class="inner form-control" id="row_13_'+x+'" />\n' +
                                                        '<input type="text" name="row_14[]" class="inner form-control" id="row_14_'+x+'" />\n' +
                                                        '<input type="text" name="row_15[]" class="inner form-control" id="row_15_'+x+'" />\n' +
                                                        '<input type="text" name="row_16[]" class="inner form-control" id="row_16_'+x+'" />\n' +
                                                        '<input type="text" name="row_17[]" class="inner form-control" id="row_17_'+x+'" />\n' +
                                                        '<input type="text" name="row_18[]" class="inner form-control" id="row_18_'+x+'" />\n' +
                                                      '</div>\n' +
                                                      
                                                      '<div style="padding-left: 0px;padding-right: 0px" class="col-md-2">\n' +
                                                        '<input type="text" name="row_19[]" class="inner form-control" id="row_19_'+x+'" />\n' +
                                                        '<input type="text" name="row_20[]" class="inner form-control" id="row_20_'+x+'" />\n' +
                                                        '<input type="text" name="row_21[]" class="inner form-control" id="row_21_'+x+'" />\n' +
                                                        '<input type="text" name="row_22[]" class="inner form-control" id="row_22_'+x+'" />\n' +
                                                        '<input type="text" name="row_23[]" class="inner form-control" id="row_23_'+x+'" />\n' +
                                                        '<input type="text" name="row_24[]" class="inner form-control" id="row_24_'+x+'" />\n' +
                                                      '</div>\n' +
                                                      
                                                      '<div style="padding-left: 0px;padding-right: 0px" class="col-md-2">\n' +
                                                        '<input type="text" name="row_25[]" class="inner form-control" id="row_25_'+x+'" />\n' +
                                                        '<input type="text" name="row_26[]" class="inner form-control" id="row_26_'+x+'" />\n' +
                                                        '<input type="text" name="row_27[]" class="inner form-control" id="row_27_'+x+'" />\n' +
                                                        '<input type="text" name="row_28[]" class="inner form-control" id="row_28_'+x+'" />\n' +
                                                        '<input type="text" name="row_29[]" class="inner form-control" id="row_29_'+x+'" />\n' +
                                                        '<input type="text" name="row_30[]" class="inner form-control" id="row_30_'+x+'" />\n' +
                                                      '</div>\n' +
                                                      
                                                      '<div style="padding-left: 0px;padding-right: 0px" class="col-md-2">\n' +
                                                        '<input type="text" name="row_31[]" class="inner form-control" id="row_31_'+x+'" />\n' +
                                                        '<input type="text" name="row_32[]" class="inner form-control" id="row_32_'+x+'" />\n' +
                                                        '<input type="text" name="row_33[]" class="inner form-control" id="row_33_'+x+'" />\n' +
                                                        '<input type="text" name="row_34[]" class="inner form-control" id="row_34_'+x+'" />\n' +
                                                        '<input type="text" name="row_35[]" class="inner form-control" id="row_35_'+x+'" />\n' +
                                                        '<input type="text" name="row_36[]" class="inner form-control" id="row_36_'+x+'" />\n' +
                                                      '</div>\n' +
                                                    '</div>\n' + 
                                                '</div>\n' +

                                                '<div style="margin-bottom: 5px;" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Description</label>\n' +
                                                    description +
                                                    '<textarea cols="60" id="txtArea_'+x+'" rows="11" name="description[]" class="inner form-control textArea" placeholder="Description" />\n' + '</textarea>'  + 
                                                '</div>\n' + 

                                                '<div style="margin-bottom: 5px;" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' + 
                                                    '<div id="measurement_list_'+x+'" style="margin-bottom: 5px;" class="row">\n' + 
                                                    
                                                    '</div>\n' + 
                                                '</div>\n' +

                                                add_btn +
                                                
                                            '</div>\n'
                                        );

                                        $('.single_select2').select2();
        }                                    
    });
    //For apending another rows end

    $(wrapper).on("click",".remove_field", function(e)
    {
        e.preventDefault();

        var x = $(this).attr("data-val");

        $('.di_'+x).remove(); x--;

        calculateActualAmount(x);
    });

    function calculateActualAmount(x)
    {
        var rate                    = $("#rate_"+x).val();
        var price                    = $("#price_"+x).val();
        var quantity                = $("#quantity_"+x).val();
        var gaz                     = $("#gaz_"+x).val();
        var totalDiscount           = $("#total_discount_0").val();
        var totalDiscountType       = $("#total_discount_type_0").val();

        if (rate == '')
        {
            var rateCal             = 0;
        }
        else
        {
            var rateCal             = $("#rate_"+x).val();
        }

        if (price == '')
        {
            var priceCal             = 0;
        }
        else
        {
            var priceCal             = $("#price_"+x).val();
        }

        if (quantity == '')
        {
            var quantityCal         = 1;
        }
        else
        {
            var quantityCal         = $("#quantity_"+x).val();
        }

        if (gaz == '')
        {
            var gazCal         = 1;
        }
        else
        {
            var gazCal         = $("#gaz_"+x).val();
        }


        var AmountIn              =  ((parseFloat(rateCal)*parseFloat(quantityCal)) + (parseFloat(priceCal)*parseFloat(gazCal)));
 
        $("#amount_"+x).val(AmountIn);

        //Calculating Subtotal Amount
        var total       = 0;

        $('.amount').each(function()
        {
            total       += parseFloat($(this).val());
        });

        $("#subTotalBdt").val(total);
        $("#subTotalBdtShow").val(total);

        

        if (totalDiscount > 0)
        {   
            if (totalDiscountType == 0)
            {
                var totalDiscountTypeCal     = (parseFloat(totalDiscount)*(parseFloat(total)))/100;
            }
            else
            {
                var totalDiscountTypeCal     = $("#total_discount_0").val();
            }
        }
        else
        {
            var totalDiscountTypeCal     = 0;
        }

        var totalShow = parseFloat(total) - parseFloat(totalDiscountTypeCal);

        $("#totalBdtShow").val(totalShow);
        $("#totalBdt").val(parseFloat(totalShow));
        $("#amount_paid_0").val(parseFloat(totalShow));
    }
</script>

<script type="text/javascript">
    var max_fields_payment       = 50;                           //maximum input boxes allowed
    var wrapper_payment          = $(".input_fields_wrap_payment");      //Fields wrapper
    var add_button_payment       = $(".add_field_button_payment");       //Add button ID
    var index_no_payment         = 1;

    //For apending another rows start
    var y = -1;
    $(add_button_payment).click(function(e)
    {
        e.preventDefault();

        // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

        if(y < max_fields_payment)
        {   
            y++;

            var serial = y + 1;

            if (serial == 1)
            {
                var amount_paid_label   = '<label class="hidden-xs" for="productname">Amount Paid *</label>\n';
                var paid_through_label  = '<label class="hidden-xs" for="productname">Paid Through *</label>\n';
                var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button_payment">\n' + 
                                                '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                    action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonPayment()"></i>' +
                                            '</div>\n';
            }
            else
            {
                var amount_paid_label   = '';
                var paid_through_label  = '';
                var note_label          = '';
                var action_label        = '';

                var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field_payment" data-val="'+y+'">\n' + 
                                                '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                    action_label +
                                                    '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                            '</div>\n';
            }

            $('.getMultipleRowPayment').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_payment_'+y+'">' +
                                                '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                    '<label style="display: none" class="show-xs" for="productname">Amount Paid *</label>\n' +
                                                    amount_paid_label +
                                                    '<input type="text" name="amount_paid[]" class="form-control" id="amount_paid_'+y+'" value="0" required />\n' + 
                                                '</div>\n' +

                                                '<div style="" class="col-lg-4 col-md-14col-sm-6 col-6">\n' +
                                                    '<label style="display: none" class="show-xs" for="productname">Paid Through *</label>\n' +
                                                    paid_through_label  +
                                                    '<select id="paid_through_'+y+'" style="cursor: pointer" name="paid_through[]" class="form-control single_select2">\n' +
                                                        '@if(!empty($paid_accounts) && ($paid_accounts->count() > 0))\n' +
                                                        '@foreach($paid_accounts as $key => $paid_account)\n' +
                                                            '<option value="{{ $paid_account['id'] }}">{{ $paid_account['name'] }}</option>\n' +
                                                        '@endforeach\n' +
                                                        '@endif\n' +
                                                    '</select>\n' +
                                                '</div>\n' +

                                                '<div style="" class="col-lg-4 col-md-4 col-sm-6 col-6">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                    note_label +
                                                    '<input type="text" name="note[]" class="form-control" id="note_'+y+'" placeholder="note"/>\n' + 
                                                '</div>\n' + 
                                                
                                                add_btn +

                                            '</div>\n' 
                                        );

                                        $('.single_select2').select2();
            }                                    
    });
    //For apending another rows end

    $(wrapper_payment).on("click",".remove_field_payment", function(e)
    {
        e.preventDefault();

        var y = $(this).attr("data-val");

        $('.di_payment_'+y).remove(); y--;

        calculateActualAmount(y);
    });
</script>

<script type="text/javascript">
    $(document).on("change", "#defaultCheck2" , function() {

        var checkbox_value    = $("#defaultCheck2")[0].checked;

        if (checkbox_value == true)
        {
            $("#vatShow").show();
            $("#taxShow").show();
        }
        else
        {
            $("#vatShow").hide();
            $("#taxShow").hide();
        }
    });

    function couponMembership()
    {
        var site_url      = $(".site_url").val();
        var coupon_code   = $("#coupon_code").val();

        $('.DiscountType').val(1);
        $('.DiscountAmount').val(0);

        $.get(site_url + '/invoices/search/coupon-code/'+ coupon_code, function(data_coupon){

            if (data_coupon == '')
            {
                alert('Invalid Coupon Code or Membership Card !!');
                $('#coupon_code').val('');
            }
            else
            {
                var state = 0;

                $('.productEntries').each(function()
                {
                    var entry_id    = $(this).val();
                    var value_x     = $(this).prop("id");
                    
                    if (data_coupon[0].product_id != null)
                    {
                        for (var i = data_coupon.length - 1; i >= 0; i--)
                        {   
                            if (data_coupon[i].product_id == entry_id)
                            {
                                var explode    = value_x.split('_');
                                var di_id      = explode[2];

                                $('#discount_type_'+di_id).val(data_coupon[i].discount_type);
                                $('#discount_'+di_id).val(data_coupon[i].discount_amount);
                                
                                calculateActualAmount(di_id);

                                state++;
                            }
                        }
                    }
                    else
                    {
                        var explode    = value_x.split('_');
                        var di_id      = explode[2];

                        $('#discount_type_'+di_id).val(data_coupon[0].discount_type);
                        $('#discount_'+di_id).val(data_coupon[0].discount_amount);
                        
                        calculateActualAmount(di_id);
                    }    
                });
            }
        });
    }

    function calculateChangeAmount()
    {
        var totalAmount     = $("#totalBdtShow").val();
        var cashGiven       = $("#cash_given").val();
        var changeAmount    = parseFloat(cashGiven) - parseFloat(totalAmount);

        if (changeAmount > 0)
        {   
            $("#change_amount").val(0);
            $("#change_amount").val(changeAmount);
        }

        if (changeAmount < 0)
        {   
            $("#change_amount").val(0);
        }

        if (changeAmount == '')
        {
            $("#change_amount").val(0);
        }
    }
</script>

<script type="text/javascript">
    function printInvoiceList()
    {
        var site_url  = $('.site_url').val();

        $.get(site_url + '/invoices/print-invoices-list', function(data){

            var invoice_list = '';
            var sl_no        = 1;
            $.each(data, function(i, invoice_data)
            {
                var serial              = parseFloat(i) + 1;
                var site_url            = $('.site_url').val();
                var print_url_pos       = site_url + '/invoices/show-pos/' + invoice_data.id;

                if (invoice_data.customer_name != null)
                {
                    var customer  = invoice_data.customer_name;
                }
                else
                {
                    var customer  = invoice_data.contact_name;
                }

                invoice_list += '<tr>' +
                                    '<td>' +
                                        sl_no +
                                    '</td>' +
                                    '<td>' +
                                       formatDate(invoice_data.invoice_date) +
                                    '</td>' +
                                    '<td>' +
                                        'INV - ' + invoice_data.invoice_number.padStart(6, '0') +
                                    '</td>' +
                                    '<td>' +
                                        customer + 
                                    '</td>' +
                                    '<td>' +
                                       invoice_data.invoice_amount +
                                    '</td>' +
                                    '<td>' +
                                       (parseFloat(invoice_data.invoice_amount) - parseFloat(invoice_data.due_amount)) +
                                    '</td>' +
                                    '<td>' +
                                       invoice_data.due_amount +
                                    '</td>' +
                                    '<td>' +
                                       invoice_data.user_name +
                                    '</td>' +
                                    '<td>' +
                                        '<a href="'+ print_url_pos +'">' +
                                            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +'<i class="fa fa-print">' + '</i>' +
                                        '</a>' +
                                    '</td>' +
                                '</tr>';

                                sl_no++;
            });

            $("#print_invoice_list").empty();
            $("#print_invoice_list").append(invoice_list);
            
        });
    }

    function printInvoicesSearch()
    {
        var site_url            = $('.site_url').val();
        var date_val            = $('#search_date').val();
        var invoiceNumber_val   = $('#invoiceNumber').val();
        var customer_val        = $('#customer').val();

        if (date_val != '')
        {
            var date = $('#search_date').val();
        }
        else
        {
            var date  = 0;
        }

        if (invoiceNumber_val != '')
        {
            var invoiceNumber = $('#invoiceNumber').val();
        }
        else
        {
            var invoiceNumber  = 0;
        }

        if (customer_val != '')
        {
            var customer = $('#customer').val();
        }
        else
        {
            var customer  = 0;
        }

        $.get(site_url + '/invoices/print-invoices-search/' + date + '/' + customer + '/' + invoiceNumber , function(data){

            var invoice_list = '';
            var sl_no        = 1;
            $.each(data, function(i, invoice_data)
            {
                var serial              = parseFloat(i) + 1;
                var site_url            = $('.site_url').val();
                var print_url_pos       = site_url + '/invoices/show-pos/' + invoice_data.id;

                if (invoice_data.customer_name != null)
                {
                    var customer  = invoice_data.customer_name;
                }
                else
                {
                    var customer  = invoice_data.contact_name;
                }

                invoice_list += '<tr>' +
                                    '<td>' +
                                        sl_no +
                                    '</td>' +
                                    '<td>' +
                                       formatDate(invoice_data.invoice_date) +
                                    '</td>' +
                                    '<td>' +
                                        'INV - ' + invoice_data.invoice_number.padStart(6, '0') +
                                    '</td>' +
                                    '<td>' +
                                        customer + 
                                    '</td>' +
                                    '<td>' +
                                       invoice_data.invoice_amount +
                                    '</td>' +
                                    '<td>' +
                                       (parseFloat(invoice_data.invoice_amount) - parseFloat(invoice_data.due_amount)) +
                                    '</td>' +
                                    '<td>' +
                                       invoice_data.due_amount +
                                    '</td>' +
                                    '<td>' +
                                       invoice_data.user_name +
                                    '</td>' +
                                    '<td>' +
                                        '<a href="'+ print_url_pos +'">' +
                                            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +'<i class="fa fa-print">' + '</i>' +
                                        '</a>' +
                                    '</td>' +
                                '</tr>';

                                sl_no++;
            });

            $("#print_invoice_list").empty();
            $("#print_invoice_list").append(invoice_list);
            
        });
    }

    function pad(number, length)
    {
        var str = '' + number;
        while (str.length < length)
        {
            str = '0' + str;
        }
       
        return str;
    }

    function formatDate(date)
    {
        var d       = new Date(date),
            month   = '' + (d.getMonth() + 1),
            day     = '' + d.getDate(),
            year    = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day, month, year].join('-');
    }

    $(document).on("change", "#customer_id" , function() {

        var site_url        = $('.site_url').val();
        var customerId      = $("#customer_id").val();

        $.get(site_url + '/invoices/calculate-opening-balance/' + customerId, function(data){

            $("#balance").empty();
            $("#balance").val(data);
            
        });
    });
</script>
@endsection