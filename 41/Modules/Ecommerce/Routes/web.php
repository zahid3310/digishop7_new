<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('ecommerce/majorCategory')->group(function() {
    Route::get('/', 'EcommerceController@majorCategoryIndex')->name("ecommerce_major_category_index");
    Route::get('/create', 'EcommerceController@majorCategoryCreate')->name("ecommerce_major_category_create");
    Route::post('/store', 'EcommerceController@majorCategoryStore')->name("ecommerce_major_category_store");
    Route::get('/edit/{id}', 'EcommerceController@majorCategoryEdit')->name("ecommerce_major_category_edit");
    Route::post('/update/{id}', 'EcommerceController@majorCategoryUpdate')->name("ecommerce_major_category_update");
});

Route::prefix('ecommerce/asignMajorCategory')->group(function() {
    Route::get('/edit', 'EcommerceController@asignMajorCategoryEdit')->name("ecommerce_asign_major_category_edit");
    Route::post('/update', 'EcommerceController@asignMajorCategoryUpdate')->name("ecommerce_asign_major_category_update");
});

Route::prefix('ecommerce/productImages')->group(function() {
    Route::get('/edit', 'EcommerceController@productImagesEdit')->name("ecommerce_product_images_edit");
    Route::post('/update', 'EcommerceController@productImagesUpdate')->name("ecommerce_product_images_update");
    Route::get('/delete-product-image/{product_image_id}', 'EcommerceController@deleteProductImages');
});