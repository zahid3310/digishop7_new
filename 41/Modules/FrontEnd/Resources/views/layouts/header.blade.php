<h1 class="d-none">Cyberdyne Technology Ltd</h1>
<header class="header">

    <div class="header-top">
        <div class="container">
            <div class="header-left">
                <p class="welcome-msg">Welcome to {{ userDetails()->organization_name }}</p>
            </div>

            <div class="header-right">
                <!-- End of DropDown Menu -->
                <div class="dropdown dropdown-expanded d-lg-show">
                    <a href="#dropdown">Links</a>
                    <ul class="dropdown-box">
                        <li><a href="{{ route('about') }}">About</a></li>
                        <!-- <li><a href="#">Blog</a></li> -->
                        <!-- <li><a href="#">FAQ</a></li> -->
                        <!-- <li><a href="#">Newsletter</a></li> -->
                        <li><a href="{{ route('contact') }}">Contact</a></li>
                    </ul>
                </div>
                <!-- End of DropDownExpanded Menu -->
            </div>
        </div>
    </div>
    <!-- End of HeaderTop -->

    <div class="header-middle sticky-header fix-top sticky-content has-center">
        <div class="container">
            <div class="header-left">
                <a href="#" class="mobile-menu-toggle">
                    <i class="d-icon-bars2"></i>
                </a>
                <a href="{{ url('/') }}" class="logo d-none d-lg-block">
                    <img src="{{ url('public/'.userDetails()->logo) }}" alt="logo" width="60" height="60" />
                </a>
            </div>

            <div class="header-center">
                <a href="{{ url('/') }}" class="logo d-lg-none">
                    <img src="{{ url('public/'.userDetails()->logo) }}" alt="logo" width="163" height="39" />
                </a>
                <!-- End of Logo -->
                <div class="header-search hs-expanded">
                    <form action="{{ route('search')}}" method="POST" class="input-wrapper">
                        @csrf
                        <div class="select-box">
                            <select id="category" name="category" class="searchCategoryOptions">
                            </select>
                        </div>
                        <input type="text" class="form-control" name="search" id="search"
                            placeholder="Search your keyword..." required="">
                        <button class="btn btn-sm btn-search" type="submit"><i
                                class="d-icon-search"></i></button>
                    </form>
                </div>
                <!-- End of Header Search -->

                <nav class="main-nav">
                    <ul class="menu">
                        <li class="active">
                            <a href="{{ url('/') }}">Home</a>
                        </li>

                       <!--  <li class="">
                            <a href="{{ url('/') }}">Best Sellers</a>
                        </li>

                        <li class="">
                            <a href="{{ url('/') }}">New Additions</a>
                        </li> -->
                       
                    </ul>
                </nav>
            </div>

            <div class="header-right">

                @if(session('customer_id'))
                <a href="{{ route('customer_order_list') }}"> Dashboard</a>
                <span style="padding-right: 5px;padding-left: 5px;">||</span>
                @endif
                @if(session('customer_id'))
                <a href="{{ route('customer_logout') }}"> Logout</a>
                @else
                <a  href="{{ route('customer_login') }}"> <i class="d-icon-user"></i> Login</a>
                @endif
                <!-- End of Login -->
                <span class="divider"></span>
                <div class="dropdown cart-dropdown">
                    <a href="{{route('cart_page')}}" class="cart-toggle">
                        <span class="cart-label">
                            <span class="cart-name">My Cart</span>
                            <span class="cart-price totalCartPriceBdt">BDT 0.00</span>
                        </span>
                        <i class="minicart-icon">
                            <span class="cart-count totalCartItemBdt">0</span>
                        </i>
                    </a>
                    <!-- End of Cart Toggle -->
                    <div class="dropdown-box">
                        <div class="product product-cart-header">
                            <span class="product-cart-counts totalCartItem">0 items</span>
                        </div>
                        <div class="products scrollable showQuickShoppingCart">

                        </div>
                        <!-- End of Products  -->
                        <div class="cart-total">
                            <label>Subtotal:</label>
                            <span class="price totalCartPrice">0.00</span>
                        </div>
                        <!-- End of Cart Total -->
                        <div class="cart-action">
                         
                            <a href="{{ route('checkout') }}" class="btn btn-dark"><span>Checkout</span></a>
                            
                        </div>

                        <!-- End of Cart Action -->
                    </div>
                    <!-- End of Dropdown Box -->
                </div>

                <div class="header-search hs-toggle mobile-search">
                    <a href="#" class="search-toggle">
                        <i class="d-icon-search"></i>
                    </a>
                    <form action="#" class="input-wrapper">
                        <input type="text" class="form-control" name="search" autocomplete="off"
                            placeholder="Search your keyword..." required />
                        <button class="btn btn-search" type="submit">
                            <i class="d-icon-search"></i>
                        </button>
                    </form>
                </div>
                <!-- End of Header Search -->
            </div>

        </div>
    </div>

    <div class="header-bottom d-lg-show">
        <div class="container">
            <div class="inner-wrap">
                <div class="header-left">
                    <nav class="main-nav">
                        <ul class="menu">
                            <li class="active">
                                <a href="{{ url('/') }}">Home</a>
                            </li>

                           <!--  <li class="">
                                <a href="{{ url('/') }}">Best Sellers</a>
                            </li>

                            <li class="">
                                <a href="{{ url('/') }}">New Additions</a>
                            </li> -->
                            
                        </ul>
                    </nav>
                </div>
                
            </div>
        </div>
    </div>
</header>
<!-- End of Header -->

@section('scripts')

<script type="text/javascript">
    // Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

<script type="text/javascript">
    $( document ).ready(function() {
        var site_url = $(".site_url").val();

        $.get(site_url + '/major-category-list/', function(data){

            var detail_url  = site_url + '/product-list'
            // var list        = '';
            var list1       = '';

            list1       += '<option value="">' + 'All Categories' + '</option>';

            $.each(data, function(i, data_list)
            {   
                // var inside_list  = '';
                // $.each(data_list.product, function(i, data_list_product)
                // {
                //     inside_list += '<li>' +
                //                         '<a href="'+detail_url+'">' + data_list_product.name + '</a>' +
                //                     '</li>';
                // });

                // list    += '<li class="with-ul">' +
                //                 '<a href="'+detail_url+'">' + data_list.name + '<i class="fas fa-chevron-down"></i>' + '</a>' +

                //                 '<ul style="display: none">' + 
                //                     inside_list +
                //                 '</ul>' +
                //             '</li>';

                list1   += '<option value="'+data_list.id+'">' + data_list.name +'</option>';

                $.each(data_list.product, function(i, product_data_list)
                {
                    list1   += '<option value="'+product_data_list.id+'">' + '- ' + product_data_list.name +'</option>';
                });
                    
            });

            $(".searchCategoryOptions").empty();
            $(".searchCategoryOptions").append(list1);
            
        });
    });  
</script>

@endsection