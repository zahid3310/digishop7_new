@extends('frontend::layouts.app')

@section('main_body')
<!-- End Header -->
<main class="main">
            <div class="page-content mt-10 pt-4">
                <section class="contact-section">
                    <div class="container">
                        <div class="row">
                           @if(Session::has('unsuccess'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! Session::get('unsuccess') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif
                            
                            <div class="col-lg-6 col-md-6 col-xs-6">
                            	@if (Session::has('error'))
                            	<div class="alert alert-light alert-danger alert-icon alert-inline mb-4">
                                    <i class="fas fa-exclamation-triangle"></i>
                                    <h4 class="alert-title">Oh snap!</h4>
                                    {{Session::get('error')}}.
                                    <button type="button" class="btn btn-link btn-close">
                                        <i class="d-icon-times"></i>
                                    </button>
                                </div>
                                @endif

                                <form class="ml-lg-2 pt-8 pb-10 pl-4 pr-4 pl-lg-6 pr-lg-6 grey-section" action="{{route('login_submit')}}" method="POST">
                                	@csrf
                                    <h3 class="ls-m mb-1">Let’s Connect</h3>
                                    <div class="row">

                                        <div class="col-md-6 mb-4">
                                            <input class="form-control" name="username" type="text" placeholder="Enter Your Username *" required>
                                        </div>

                                        

                                        <div class="col-md-6 mb-4">
                                            <input class="form-control" name="password" type="password" placeholder="Enter Your Password *" required>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-md btn-primary mb-2">Login</button>
                                </form>
                            </div>

                            <div class="col-lg-6 col-md-6 col-xs-6">
                                @if (Session::has('registration_success'))
                                <div class="alert alert-message alert-light alert-primary alert-link mb-4">
                                    <h4 class="alert-title">{{Session::get('registration_success')}}.</h4>
                                    <p>Now you can login</p>
                                    <button type="button" class="btn btn-link btn-close">
                                        <i class="d-icon-times"></i>
                                    </button>
                                </div>
                                @endif

                                <form class="ml-lg-2 pt-8 pb-10 pl-4 pr-4 pl-lg-6 pr-lg-6 grey-section" action="{{route('customer_registration')}}" method="POST">
                                    @csrf
                                    <h3 class="ls-m mb-1">Customer Registration</h3>
                                    <div class="row">

                                        <div class="col-md-6 mb-4">
                                            <input class="form-control" name="name" type="text" placeholder="Enter Your FullName *" >
                                            @if ($errors->has('name'))
                                                <span style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <input class="form-control" name="username" type="text" placeholder="Enter Your Username *" >
                                           @if (Session::has('username_check'))
                                               <span style="color: red">{{Session::get('username_check')}}</span>
                                            @endif

                                            @if ($errors->has('username'))
                                                <span style="color: red">{{ $errors->first('username') }}</span>
                                            @endif
                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <input class="form-control" name="phone" type="text" placeholder="Enter Your Phone Number *" >
                                            @if ($errors->has('phone'))
                                                <span style="color: red">{{ $errors->first('phone') }}</span>
                                            @endif
                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <input class="form-control" name="email" type="text" placeholder="Enter Your Email">
                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <input class="form-control" name="password" type="password" placeholder="Password *" >
                                            @if ($errors->has('password'))
                                                <span style="color: red">{{ $errors->first('password') }}</span>
                                            @endif
                                        </div>

                                        <div class="col-md-6 mb-4">
                                            <button type="submit" class="btn btn-md btn-success mb-2">Registration</button>
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End About Section-->

            </div>
        </main>
<!-- End Main -->
@endsection