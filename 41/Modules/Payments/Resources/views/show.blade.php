@extends('layouts.app')

@section('title', 'Show Payment')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Payments</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Payments</a></li>
                                    <li class="breadcrumb-item active">Print</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if($user_info['header_image'] == null)
                                        <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-4 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">{{ $user_info['organization_name'] }}</h2>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_number'] }}</p>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12"></div>
                                    @else
                                        <img class="float-left" src="{{ url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <address>
                                            <strong>@if($payment['type'] == 0) Paid By : @else Paid To : @endif</strong><br>
                                            {{ $payment['customer_name'] }}
                                            @if($payment['address'] != null)
                                               <br> <?php echo $payment['address']; ?> <br>
                                            @endif
                                            @if($payment['address'] == null)
                                                <br>
                                            @endif
                                            {{ $payment['phone'] }}
                                        </address>
                                    </div>

                                    <div class="col-sm-4">
                                        <address>
                                            
                                        </address>
                                    </div>

                                    <div class="col-sm-4 text-sm-right">
                                        <address class="mt-2 mt-sm-0">
                                            <strong>Payment Date:</strong><br>
                                            {{ date('d-m-Y', strtotime($payment['payment_date'])) }}<br><br>
                                        </address>
                                    </div>
                                </div>

                                <div class="py-2 mt-3">
                                    <h3 class="font-size-15 font-weight-bold">Payment Details</h3>
                                </div>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                   
                                    <thead>
                                        <tr>
                                            <th>Payment Date</th>
                                            <th>Payment Number</th>
                                            <th>@if($payment['type'] == 0) Invoice Number @else Bill Number @endif</th>
                                            <th>Paid Through</th>
                                            <th style="text-align: right">Amount</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td>{{ date('d-m-Y', strtotime($payment['payment_date'])) }}</td>
                                            <td>{{ 'PM - ' . str_pad($payment['payment_number'], 6, "0", STR_PAD_LEFT) }}</td>
                                            <td>
                                                @if(!empty($entries) && ($entries->count() > 0))
                                                @foreach($entries as $key => $value)
                                                @if($payment['type'] == 0)
                                                    {{ 'INV - ' . str_pad($value['number'], 6, "0", STR_PAD_LEFT) }} <br>
                                                @else
                                                    {{ 'BILL - ' . str_pad($value['number'], 6, "0", STR_PAD_LEFT) }}
                                                @endif
                                                @endforeach
                                                @endif
                                            </td>
                                            <td>{{ $payment['paid_through'] == 1 ? 'Cash' : 'Others' }}</td>
                                            <td style="text-align: right">{{ number_format($payment['amount'],2,'.',',') }}</td>
                                        </tr>        
                                    </tbody>
                                        
                                </table>

                                @if($payment['note'] != null)
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6><strong>Note :</strong> {{ $payment['note'] }}</h6>
                                    </div>
                                </div>
                                @endif

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div style="position: fixed;bottom: 0">
                                    <!-- <h4 class="float-right font-size-16">Order # 12345</h4> -->
                                    <!-- <div class="col-md-4">
                                        <img class="float-left" src="{{ url('public/az-ai.png') }}" alt="logo" height="20"/>
                                    </div>

                                    <div class="col-md-4">
                                        <h2 style="text-align: center">{{ $user_info['organization_name'] }}</h2>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['address'] }}</p>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['contact_number'] }}</p>
                                    </div>

                                    <div class="col-md-4">
                                        <p style="line-height: 18px;text-align: right;padding: 0px">Phone - 01718937082<br>01711418731<br>01711418731</p>
                                    </div> -->

                                    @if($user_info['footer_image'] != null)
                                        <img class="float-left" src="{{ url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection