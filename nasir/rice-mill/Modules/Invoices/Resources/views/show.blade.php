@extends('layouts.app')

@section('title', 'Show')

<style>
    table,th {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 2px;
    }

    td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        border-bottom: 1px solid black;
        padding: 2px;
    }

    @page {
        size: A4;
        page-break-after: always;
    }

    /*.color1 {
        background-color: #E2E2E2;
    }
    .color2 {
        background-color: #E2EAE7;
    }
    .color3 {
        background-color: #E0EAE9;
    }
    .color4 {
        background-color: #E1E0DD;
    }
    .color5 {
        background-color: #D6DFDE;
    }*/

    .color1 {
        background-color: white;
    }
    .color2 {
        background-color: white;
    }
    .color3 {
        background-color: white;
    }
    .color4 {
        background-color: white;
    }
    .color5 {
        background-color: white;
    }
</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="padding: 10px;padding-top: 0px" class="row">
                    <div style="padding-bottom: 10px" class="d-print-none col-md-12">
                        <div class="float-right">
                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <p style="line-height: 1;text-align: center"><span style="font-size: 16px">বিসমিল্লাহির রাহমানির রাহিম</span></p>
                                <p style="line-height: 1;text-align: center"><span style="border: 1px solid gray;padding: 5px;border-radius: 25px;font-size: 20px">ক্যাশ মেমো</span></p>
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 45px;padding-top: 10px;font-weight: bold">{{ $user_info['organization_name'] }}</h2>
                                        <p style="line-height: 1;font-size: 20px;font-weight: bold" class="text-center">{{ $user_info['address'] }}</p>
                                    </div>
                                    <div style="text-align: right" class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        {{ QrCode::size(70)->generate("string") }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px"> {{ $user_info['address'] }} </p> -->
                                        <!-- <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">Mob: 01916555577, 01916555588 & 01916555599</p> -->
                                        <!-- <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 13px">Email: ratonandsons@gmail.com</p> -->
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 15px" class="col-md-8">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Invoice No : - </strong>{{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }} <br>
                                            <span style="font-weight: bold">Customer :   </span>{{ $invoice['customer_name'] }}
                                        </address>
                                    </div>
                                    <div style="font-size: 15px;" class="col-md-4 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Date :  </strong>
                                            {{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}<br><br>
                                        </address>
                                    </div>
                                    <div style="font-size: 15px" class="col-md-12">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Address : </strong> {{ $invoice->customer->address != null ? $invoice->customer->address : ''  }}
                                        </address>

                                        <address style="margin-bottom: 0px !important">
                                            <strong>Mobile : </strong> {{ $invoice->customer->phone != null ? $invoice->customer->phone : ''  }}
                                        </address>
                                    </div>
                                </div>

                                <div style="padding-top: 10px;padding-bottom: 20px">
                                    <table style="width: 100%">
                                        <colgroup>
                                            <col class="color1" />
                                            <col class="color1" />
                                            <col class="color2" />
                                            <col class="color3" span= "2" />
                                            <col class="color4" />
                                            <col class="color5" />
                                        </colgroup>
                                        <thead class="theight">
                                            <tr>
                                                <th style="font-size: 18px;width: 5%;text-align: center" rowspan="2">SL</th>
                                                <th style="font-size: 18px;width: 20%;text-align: center" rowspan="2">Item Description</th>
                                                <th style="font-size: 18px;width: 10%;text-align: center" rowspan="2">Bosta</th>
                                                <th style="font-size: 18px;width: 20%;text-align: center" colspan="2">Weight</th>
                                                <th style="font-size: 18px;width: 15%;text-align: center" rowspan="2">Rate</th>
                                                <th style="font-size: 18px;width: 10%;text-align: center" rowspan="2">Price</th>
                                            </tr>

                                            <tr>
                                                <th style="font-size: 18px;width: 10%;text-align: center">KG</th>
                                                <th style="font-size: 18px;width: 10%;text-align: center">Bosta</th>
                                            </tr>
                                        </thead>

                                        @if($entries->count() > 0)

                                        <?php
                                            $total_amount                   = 0;
                                        ?>

                                        @foreach($entries as $key => $value)
                                        <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $variation_name = ProductVariationName($value['product_entry_id']);

                                            if ($value['product_code'] != null)
                                            {
                                                $productCode  = ' - '.$value['product_code'];
                                            }
                                            else
                                            {
                                                $productCode  = '';
                                            }

                                            if ($value['product_name'] != null)
                                            {
                                                $category  = ' - '.$value['product_name'];
                                            }
                                            else
                                            {
                                                $category  = '';
                                            }

                                            if ($value['brand_name'] != null)
                                            {
                                                $brandName  = $value['brand_name'];
                                            }
                                            else
                                            {
                                                $brandName  = '';
                                            }

                                            if ($value['unit_name'] != null)
                                            {
                                                $unit  = ' '.$value['unit_name'];
                                            }
                                            else
                                            {
                                                $unit  = '';
                                            }

                                            if ($variation_name != null)
                                            {
                                                $variation  = ' '.$variation_name;
                                            }
                                            else
                                            {
                                                $variation  = '';
                                            }

                                            $pre_dues = $invoice['previous_due'];
                                            $net_paya = round($total_amount, 2);
                                            $paid     = round($invoice['invoice_amount'] - $invoice['due_amount'], 2);
                                            $dues     = round($net_paya - $paid, 2);
                                        ?>

                                        <tr class="tr-height">
                                            <td style="text-align: center">{{ $key + 1 }}</td>
                                            <td style="padding-left: 30px">{{ $value['product_entry_name'] . $productCode }}</td>
                                            <td style="text-align: center">{{ $value['quantity'] }}</td>
                                            <td style="text-align: center"></td>
                                            <td style="text-align: center">{{ $value['quantity'] }}</td>
                                            <td style="text-align: center">{{ $value['rate'] . $unit }}</td>
                                            <td style="text-align: center">{{ round($value['total_amount'], 2) }}</td>
                                        </tr>
                                        @endforeach
                                        @endif

                                        <?php
                                            if ($invoice['vat_type'] == 0)
                                            {
                                                $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $vat_amount  = $invoice['total_vat'];
                                            }

                                            if ($invoice['total_discount_type'] == 0)
                                            {
                                                $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $discount_on_total_amount  = $invoice['total_discount_amount'];
                                            }
                                        ?>

                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style="text-align: left;text-align: right" colspan="1"><strong>Total</strong></th>
                                            <th style="text-align: center">{{ $net_paya != 0 ? round($net_paya - $invoice['total_discount']) : '' }}</th>
                                        </tr>

                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style="text-align: right" colspan="1">Discount</strong></th>
                                            <th style="text-align: center">{{ round($discount_on_total_amount + $invoice['total_discount']) }}</th>
                                        </tr>

                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style="text-align: right" colspan="1">Payable</strong></th>
                                            <th style="text-align: center">{{ round($invoice['invoice_amount']) }}</th>
                                        </tr>

                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style="text-align: right" colspan="1"><strong>Previous Due</strong></th>
                                            <th style="text-align: center">{{ $pre_dues  != 0 ? round($pre_dues) : '' }}</th>
                                        </tr>

                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style="text-align: right" colspan="1"><strong>Total Due</strong></th>
                                            <th style="text-align: center">{{ $invoice['invoice_amount'] + $pre_dues ? round($invoice['invoice_amount'] + $pre_dues) : '' }}</th>
                                        </tr>

                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style="text-align: right" colspan="1"><strong>Paid</strong></th>
                                            <th style="text-align: center">{{ $paid != 0 ? round($paid, 2) : '' }}</th>
                                        </tr>

                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style="text-align: right" colspan="1"><strong>Due Balance</strong></th>
                                            <th style="text-align: center">{{ $invoice['invoice_amount'] + $pre_dues - $paid != 0 ? round($invoice['invoice_amount'] + $pre_dues - $paid) : '' }}</th>
                                        </tr>  
                                    </table>
                                </div>

                                <div class="grid-container-1">

                                    <div class="item33"></div>  
                                    <div class="item55"><p class="signaturesection1" style="font-size: 18px;text-align: center"></p></div>

                                    <div class="item22" style="text-align: center;">
                                        <div>
                                            <p style="font-size: 18px">বিঃ দ্রঃ - উল্লেখিত সমস্ত মাল পূর্ণ ওজনে সঠিক দরে নমুনা অনুযায়ী বুঝিয়ে পাইলাম । </p>
                                        </div>
                                    </div>

                                    <div class="item44"></div>
                                    <div class="item1"><p class="signaturesection2" style="font-size: 18px;text-align: center"></p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection
