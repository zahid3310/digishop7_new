@extends('layouts.app')

@section('title', 'Product Categories')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Product Categories</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Others</a></li>
                                    <li class="breadcrumb-item active">Product Categories</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                @if(Auth::user()->role == 1)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('products_category_store') }}" method="post" files="true" enctype="multipart/form-data">
            					{{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Product Category Name *</label>
                                        <input type="text" name="product_name" class="inner form-control" id="product_name" placeholder="Category Name" required />
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Status</label>
                                        <select style="cursor: pointer" class="form-control" name="status">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                    	<button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                    	<button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('products_category_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
                @endif

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<h4 class="card-title">All Categories</h4>

                                <br>

                                <table id="datatable" class="table table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Category Name</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                    	@if(!empty($products) && ($products->count() > 0))
                                    	@foreach($products as $key => $product)
	                                        <tr>
	                                            <td>{{ $key + 1 }}</td>
                                                <td>{{ $product['name'] }}</td>
                                                <td>{{ $product['status'] == 1 ? 'Active' : 'Inactive' }}</td>
	                                            <td>
	                                            	<div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('products_category_edit', $product['id']) }}">Edit</a>
                                                        </div>
                                                    </div>
	                                            </td>
	                                        </tr>
	                                    @endforeach
	                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection