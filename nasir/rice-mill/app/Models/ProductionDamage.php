<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ProductionDamage extends Model
{  
    protected $table = "production_damage";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function productionDamageEntries()
    {
         return $this->hasMany(ProductionDamageEntries::class, "production_damage_id");
    }
}
