@extends('layouts.app')

@section('title', 'Show')

<style type="text/css">
    @media print {
        #footerInvoice {
            position: fixed;
            bottom: 60;
        }
    }

    .textstyle {
        background-color: white; /* Changing background color */
        font-weight: bold; /* Making font bold */
        border-radius: 20px; /* Making border radius */
        border: 3px solid black; /* Making border radius */
        width: auto; /* Making auto-sizable width */
        height: auto; /* Making auto-sizable height */
        padding: 5px 10px 5px 10px; /* Making space around letters */
        font-size: 18px; /* Changing font size */
    }

    .column-bordered-table thead td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table tfoot tr {
        border-top: 1px solid #c3c3c3;
        border-bottom: 1px solid #c3c3c3;
    }
</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Reports</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Expense</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>

                                <div class="row">
                                    @if($user_info['header_image'] == null)
                                        <div class="col-md-2 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-8 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 35px">{{ $user_info['organization_name'] }}</h2>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 17px">{{ $user_info['address'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 17px;font-weight: bold">{{ $user_info['contact_email'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 17px">{{ $user_info['contact_number'] }}</p>
                                        </div>
                                        <div class="col-md-2 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-5 col-xs-12 col-sm-12"></div>
                                        <div class="col-md-2 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 20px;margin-bottom: 0px;text-align: center;font-size: 20px;" class="textstyle">Voucher</h2>
                                        </div>
                                        <div class="col-md-5 col-xs-12 col-sm-12"></div>
                                    @else
                                        <img class="float-left" src="{{ url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>

                                <div style="margin-top: 20px" class="row">
                                    
                                    <div class="col-sm-4 col-6"></div>

                                   <div class="col-sm-3 hidden-xs"></div>

                                    <div style="font-size: 16px" class="col-sm-5 col-6 text-sm-left">
                                        <address style="border-bottom: 2px solid black">
                                            <strong>Expense Number &nbsp;&nbsp;: </strong>
                                           EXP - 00000{{ $expenses->expense_number }}
                                        </address>
                                        <address>
                                            <strong>Date & Time : </strong>
                                            {{ date('d/m/Y', strtotime($expenses->expense_date)) }} &nbsp;&nbsp;&nbsp; Print Time : {{  date('h:i:s A', strtotime($expenses->created_at)) }}
                                        </address>
                                    </div>
                                </div>

                                <div style="border: 2px solid black;height: 325px" class="table-responsive">
                                    <table style="border-bottom: 1px solid gray" class="table column-bordered-table">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th>Expenses Category</th>
                                                <th>Description</th>
                                                <th class="text-right">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">

                                            @if(!empty($expenses) && ($expenses->count() > 0))

                                            <?php $sub_total = 0; ?>

                                            
                                            <tr>
                                                <td>{{$expenses->category_name}}</td>

                                                <td>{{$expenses->note}}</td>
                                                <td class="text-right">{{$expenses->amount}}</td>
                                            </tr>
                                            @endif
                                        
                                        </tbody>
                                    </table>

                                    <br>
                                    <br>
                                    <br>
                                    <br>
                        

                                    <div class="row">

                                        <div style="text-align: left;padding-left:20px!important" class="col-md-6">
                                            
                                            <span style="border-top: 1px solid black"> Accountant Signature </span>
                                        </div>

                                        <div style="text-align: right;padding-right: 20px;" class="col-md-6">

                                            <span style="border-top: 1px solid black"> Receive by </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection