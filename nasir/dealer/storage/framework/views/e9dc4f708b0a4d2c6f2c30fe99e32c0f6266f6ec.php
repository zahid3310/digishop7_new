

<?php $__env->startSection('title', 'Issue To DSR'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Orders</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">New Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('issues_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                <?php echo e(csrf_field()); ?>


                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 625px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-8 input_fields_wrap getMultipleRow">
                                        <div class="row di_0">
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 625px;padding-top: 13px" class="col-md-4">

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Date *</label>
                                            <div class="col-md-8">
                                                <input id="issue_date" name="issue_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">DSR Name *</label>
                                            <div class="col-md-8">
                                                <select style="width: 75%" id="sr_id" name="sr_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9" required>
                                                    <option value="">--Select DSR--</option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Previous Balance</label>
                                            <div class="col-md-8">
                                                <input id="balance" name="balance" type="text" value="" class="form-control" value="0">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Note</label>
                                            <div class="col-md-8">
                                                <input id="issue_note" name="issue_note" type="text" value="" class="form-control" placeholder="Note">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Barcode</label>
                                            <div class="col-md-8">
                                                <input type="text" name="product_code" class="inner form-control" id="product_code" placeholder="Scan Barcode" oninput="PosScannerSearch()" autofocus />
                                                <p id="alertMessage" style="display: none">No Product Found !</p>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-4 col-form-label">Total Quantity</label>
                                            <div class="col-md-8">
                                                <input type="text" id="subTotalQty" class="form-control">
                                                <input style="display: none" type="text" id="subTotalQtyShow" name="total_quantity" readonly>
                                            </div>
                                        </div>

                                        <br>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Save & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('issues_index')); ?>">Close</a></button>
                                              
                                                <button style="border-radius: 0px !important" class="btn btn-info waves-effect waves-light" type="button" data-toggle="modal" data-target="#myModal2" onclick="printIssueList()">Print Issues</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>

                                    <div style="display: none;" class="row justify-content-end">
                                        <div class="col-lg-1 col-md-2 form-group">
                                            <input id="pos_add_button" type="button" class="btn btn-success btn-block inner add_field_button_pos" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New DSR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control commonCustomerClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control commonCustomerClass">
                        </div>
                    </div>

                    <input id="contact_type" name="contact_type" type="hidden" value="4" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal2" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Print Issues</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label for="productname" class="col-md-4 col-form-label">Date</label>
                                <div class="col-md-8">
                                    <input style="cursor: pointer" id="search_date" type="date" class="form-control" value="<?= date("Y-m-d") ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">DSR </label>
                                <div class="col-md-8">
                                    <select style="width: 100%" id="sr_id_1" class="form-control select2">
                                        <option value="0">All</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Issue Number </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <select style="width: 73%" id="issue_number" name="issue_number" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                           <option value="0">All</option>
                                        </select>
                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="printIssuesSearch()">
                                            <i class="bx bx-search font-size-24"></i>
                                        </span>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Issue#</th>
                                <th>DSR</th>
                                <th>Total Qty.</th>
                                <th>Creator</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody id="print_issue_list">
                        </tbody>
                    </table>
                </div>
                
                <div class="modal-footer">
                    <button id="CloseButton2" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url    = $('.site_url').val();

            $("#sr_id").select2({
                ajax: { 
               
                url:  site_url + '/invoices/dsrcustomer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    console.log(response);
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 4 && result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#sr_id_1").select2({
              
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    console.log(response);
                    return {

                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 4 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#issue_number").select2({
                ajax: { 
                url:  site_url + '/srissues/issues-list/',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $('#add_field_button').click();
            ProductCategoryList();
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = $("#contact_type").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '<?php echo e(csrf_token()); ?>' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                            $('.commonCustomerClass').val('');
                        }
                        
                        $("#sr_id").empty();
                        $('#sr_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });     
    </script>

    <script type="text/javascript">
        function PosScannerSearch()
        {
            $("#alertMessage").hide();

            var site_url      = $(".site_url").val();
            var read_code     = $("#product_code").val();

            if (read_code.length == 6)
            {
                var product_code  = read_code.replace(/^0+/, '');
            }
            else
            {
                var product_code  = '0';
            }

            if(product_code != 0)
            {   
                $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code, function(data_pos){

                    if ($.isEmptyObject(data_pos))
                    {   
                        $('input[name=product_code').val('');
                        $("#alertMessage").show();
                    }
                    else
                    {   
                        var state = 0;

                        $('.productEntries').each(function()
                        {
                            var entry_id    = $(this).val();
                            var value_x     = $(this).prop("id");

                            if (data_pos.id == entry_id)
                            {
                                var explode          = value_x.split('_');
                                var quantity_id      = explode[2];
                                var quantity         = $('#quantity_'+quantity_id).val();
                                var inc_quantity     = parseFloat(quantity) + 1;

                                $('#quantity_'+quantity_id).val(inc_quantity);
                                $('#quantity_'+quantity_id).change();
                                $('input[name=product_code').val('');
                                calculateActualAmount(quantity_id);

                                state++;
                            }

                            if (entry_id == '')
                            {   
                                var explode          = value_x.split('_');
                                var quantity_id      = explode[2];
                                
                                ProductEntriesList(quantity_id);

                                state++;
                            }
                        });

                        if (state == 0)
                        {   
                            $("#pos_add_button").click();
                        }
                        else
                        {
                            return false;
                        }
                    }

                });
            } 
        }
        
        function ProductCategoryList(x)
        {
            var site_url = $(".site_url").val();

            $.get(site_url + '/bills/product/category/list', function(data){

                var category_list  = '';
               
                $.each(data, function(i, data_list)
                {
                    category_list += '<option value = "' +  data_list.id + '">' + data_list.name + '</option>';
                });

                $(".productCategory").append(category_list);

            });
        }

        function ProductEntriesList(x,entry_id) 
        {
            //For getting item commission information from items table start
            var site_url = $(".site_url").val();

            $.get(site_url + '/bills/product/entries/category-list/invoice/'+ entry_id, function(data){
                var list5           = '';
                var list7           = '';
                var checkProduct    = [];

                $.each(data, function(i, data)
                {   
                    if (data.stock_in_hand != null)
                    {
                        var stock = data.stock_in_hand;
                    }
                    else
                    {
                        var stock = 0;
                    }

                    if (data.product_type == 2)
                    {
                        var productName = data.name + ' - ' + data.variations;
                    }
                    else
                    {
                        var productName = data.name;
                    }

                    // $('.productEntries').each(function()
                    // {
                    //     var entry_id    = $(this).val();

                    //     if(checkValue(entry_id, checkProduct) == 'Not exist')
                    //     {
                    //         checkProduct.push(entry_id);
                    //     }
                        
                    // });

                    // if(checkValue(data.id, checkProduct) == 'Not exist')
                    // {

                        list5 += '<option value = "' +  data.id + '">' + productName + ' ( ' + pad(data.product_code, 6) + ' )' + '</option>';
                    // }
                });

                list7 += '<option value = "">' + '--Select Product--' +'</option>';

                $("#product_entries_"+x).empty();
                $("#product_entries_"+x).append(list7);
                $("#product_entries_"+x).append(list5);

                var read_code     = $("#product_code").val();

                if (read_code)
                {
                    var product_code  = read_code.replace(/^0+/, '');
                }
                else
                {
                    var product_code  = '';
                }

                if(product_code)
                {
                    $.get(site_url + '/invoices/invoice/pos/search/product/'+ product_code, function(data_pos){

                        $('#product_entries_'+x).val(data_pos.id).change();
                    });

                    $('input[name=product_code').val('');
                } 
            });
        }

        function checkValue(value,arr)
        {
            var status = 'Not exist';
             
            for(var i=0; i<arr.length; i++)
            {
                var name = arr[i];
                if(name == value)
                {
                    status = 'Exist';
                    break;
                }
            }

            return status;
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                    $("#stock_show_"+x).html('Stock : ' + convertedStock + ' ( ' + convertedUnitName + ' )');
                    $("#stock_"+x).val(convertedStock);

                    calculateActualAmount(x);
                }

            });
        }
        
        function getProductList(x) {
            var entry_id  = $("#product_category_id_"+x).val();
            ProductEntriesList(x,entry_id);
        }

        function getItemPrice(x)
        {
            //For getting item commission information from items table start
            var entry_id  = $("#product_entries_"+x).val();
            var site_url  = $(".site_url").val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    var list    = '';
                    $.each(data.unit_conversions, function(i, data_list)
                    {   
                        list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                    });

                    $("#unit_id_"+x).empty();
                    $("#unit_id_"+x).append(list);

                    if (data.product_entries.stock_in_hand == '' || data.product_entries.stock_in_hand == null)
                    {
                        var stockInHand  = 0;
                    }
                    else
                    {
                        var stockInHand  = data.product_entries.stock_in_hand;
                    }

                    if (data.product_entries.unit_id == null)
                    {
                        var unit  = '';
                    }
                    else
                    {
                        var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                    }

                    $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
                    $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' ' + unit);
                    $("#main_unit_id_"+x).val(data.product_entries.unit_id);
                    $("#main_unit_name_"+x).val(data.product_entries.unit_name);
                    $("#quantity_"+x).val(0);
      
                    calculateActualAmount(x);
                });
            }
            // calculateActualAmount(x);
        }

        function pad (str, max)
        {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = -1;
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {   
                x++;

                var serial = x + 1;

                if (serial == 1)
                {
                    var category_label       = '<label class="hidden-xs" for="productname">Category *</label>\n';
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var stock_label         = '<label class="hidden-xs" for="productname">Stock</label>\n';
                    var unit_label          = '<label class="hidden-xs" for="productname">Unit</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButton()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var category_label       = '';
                    var product_label       = '';
                    var stock_label         = '';
                    var unit_label          = '';
                    var quantity_label      = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                // ProductEntriesList(x);
                ProductCategoryList(x)

                $('.getMultipleRow').append(' ' + '<div class="row di_'+x+'">' +
                
                                                    '<div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Category *</label>\n' +
                                                        category_label +
                                                        '<select style="width: 100%" name="product_category[]" class="inner form-control single_select2 productCategory" id="product_category_id_'+x+'" onchange="getProductList('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Category--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +
                                                    
                                                    '<div style="margin-bottom: 5px" class="col-lg-4 col-md-4 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" readonly />\n' +

                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +

                                                    '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                        unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_'+x+'" required onchange="getConversionParam('+x+')">\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control quantity" id="quantity_'+x+'" placeholder="Quantity" oninput="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }                                    
        });
        //For apending another rows end

        //For apending another rows start
        $(add_button_pos).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == 1)
                {
                    var category_label       = '<label class="hidden-xs" for="productname">Category *</label>\n';
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var stock_label         = '<label class="hidden-xs" for="productname">Stock</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButton()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var category_label       = '';
                    var product_label       = '';
                    var stock_label         = '';
                    var quantity_label      = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                // ProductEntriesList(x);
                ProductCategoryList(x)
                
                $('.getMultipleRow').append(' ' + '<div class="row di_'+x+'">' +
                                                    
                                                    '<div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Category *</label>\n' +
                                                        category_label +
                                                        '<select style="width: 100%" name="product_category[]" class="inner form-control single_select2 productCategory" id="product_category_id_'+x+'" onchange="getProductList('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Category--' + '</option>' +
                                                        '</select>\n' +
                                                    '</div>\n' +
                                                    
                                                    '<div style="margin-bottom: 5px" class="col-lg-4 col-md-4 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Stock</label>\n' +
                                                        stock_label +
                                                        '<input type="text" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" readonly />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control quantity" id="quantity_'+x+'" placeholder="Quantity" oninput="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

            }
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            var quantity                = $("#quantity_"+x).val();
            var stock                   = $("#stock_"+x).val();

            if (quantity == '')
            {
                var quantityCal         = 1;
            }
            else
            {
                var quantityCal         = $("#quantity_"+x).val();
            }

            //Checking Overselling Start
            var check_quantity  = parseFloat(quantity);
            var check_stock     = parseFloat(stock);

            if (check_quantity > check_stock)
            {   
                $("#quantity_"+x).val(check_stock);
            }
            //Checking Overselling End

            //Calculating Subtotal Amount
            var total       = 0;

            $('.quantity').each(function()
            {
                total       += parseFloat($(this).val());
            });

            $("#subTotalQty").val(total);
            $("#subTotalQtyShow").val(total);
        }
    </script>

    <script type="text/javascript">
        function printIssueList()
        {
            var site_url  = $('.site_url').val();

            $.get(site_url + '/srissues/print-issues-list', function(data){

                var issue_list = '';
                var sl_no        = 1;
                $.each(data, function(i, issue_data)
                {
                    var serial              = parseFloat(i) + 1;
                    var site_url            = $('.site_url').val();
                    var print_url           = site_url + '/srissues/show/' + issue_data.id;

                    if (issue_data.sr_name != null)
                    {
                        var sr  = issue_data.sr_name;
                    }
                    else
                    {
                        var sr  = issue_datasr_name;
                    }

                    issue_list += '<tr>' +
                                        '<td>' +
                                            sl_no +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(issue_data.issue_date) +
                                        '</td>' +
                                        '<td>' +
                                            'SIN - ' + issue_data.issue_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            sr + 
                                        '</td>' +
                                        '<td>' +
                                           issue_data.total_quantity +
                                        '</td>' +
                                        '<td>' +
                                           issue_data.user_name +
                                        '</td>' +
                                        '<td>' +
                                            '<a href="'+ print_url +'">' +
                                                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +'<i class="fa fa-print">' + '</i>' +
                                            '</a>' +
                                        '</td>' +
                                    '</tr>';

                                    sl_no++;
                });

                $("#print_issue_list").empty();
                $("#print_issue_list").append(issue_list);
                
            });
        }

        function printIssuesSearch()
        {
            var site_url            = $('.site_url').val();
            var date_val            = $('#search_date').val();
            var issueNumber_val     = $('#issue_number').val();
            var sr_val              = $('#sr_id_1').val();

            if (date_val != '')
            {
                var date = $('#search_date').val();
            }
            else
            {
                var date  = 0;
            }

            if (issueNumber_val != '')
            {
                var issueNumber = $('#issue_number').val();
            }
            else
            {
                var issueNumber  = 0;
            }

            if (sr_val != '')
            {
                var sr = $('#sr_id_1').val();
            }
            else
            {
                var sr  = 0;
            }

            $.get(site_url + '/srissues/print-issues-search/' + date + '/' + sr + '/' + issueNumber , function(data){
                var issue_list = '';
                var sl_no        = 1;
                $.each(data, function(i, issue_data)
                {
                    var serial              = parseFloat(i) + 1;
                    var site_url            = $('.site_url').val();
                    var print_url           = site_url + '/srissues/show/' + issue_data.id;

                    if (issue_data.csr_name != null)
                    {
                        var sr  = issue_data.csr_name;
                    }
                    else
                    {
                        var sr  = issue_data.sr_name;
                    }

                    issue_list += '<tr>' +
                                        '<td>' +
                                            sl_no +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(issue_data.issue_date) +
                                        '</td>' +
                                        '<td>' +
                                            'SIN - ' + issue_data.issue_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            sr + 
                                        '</td>' +
                                        '<td>' +
                                           issue_data.total_quantity +
                                        '</td>' +
                                        '<td>' +
                                           issue_data.user_name +
                                        '</td>' +
                                        '<td>' +
                                            '<a href="'+ print_url +'">' +
                                                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +'<i class="fa fa-print">' + '</i>' +
                                            '</a>' +
                                        '</td>' +
                                    '</tr>';

                                    sl_no++;
                });

                $("#print_issue_list").empty();
                $("#print_issue_list").append(issue_list);
                
            });
        }

        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        $(document).on("change", "#sr_id" , function() {

            var site_url  = $('.site_url').val();
            var srId      = $("#sr_id").val();

            $.get(site_url + '/srissues/calculate-opening-balance/' + srId, function(data){

                $("#balance").empty();
                $("#balance").val(data);
                
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/47/Modules/SrIssues/Resources/views/index.blade.php ENDPATH**/ ?>