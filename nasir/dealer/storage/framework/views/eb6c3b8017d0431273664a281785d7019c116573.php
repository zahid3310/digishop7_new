

<?php $__env->startSection('title', 'Opening Stock'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Opening Stock</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Opening Stock</a></li>
                                    <li class="breadcrumb-item active">Opening Stock</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>
                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <div class="form-group mb-5">
                                    <label style="" class="show-xs" for="productname">Category *</label>
                                    <select class="inner form-control select2" id="product_category_id" onchange="getProductList()" required="">
                                        <option value="" hidden="">--Select product Category--</option>
                                        <?php if(!empty($product_category) && ($product_category->count() > 0)): ?>
                                        <?php $__currentLoopData = $product_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </select>
                                </div>

            					<form id="FormSubmit" action="<?php echo e(route('products_opening_stock_store')); ?>" method="post" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					<?php echo e(csrf_field()); ?>


                                
                                        <div class="form-group row opening-stock">
                                           
                                        </div>
                                 

                                    <div class="form-group row">
                                        <div class="col-md-4"></div>
                                        <div class="button-items col-md-2 pull-right">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('home')); ?>">Close</a></button>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                             

                             
                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }

        function getProductList() {
            var site_url = $(".site_url").val();

            var entry_id  = $("#product_category_id").val();
          

            $.get(site_url + '/products/product-category-wise-list/'+entry_id, function(data){
                var category_list  = '';
                $(".opening-stock").empty();
                $.each(data, function(i, data_list)
                {
                  

                    if (data_list.product_type == 2)
                    {
                        var variations =  data_list.variations;
                    }else
                    {
                        var variations =  '';
                    }
                   
                    category_list += `<label for="example-text-input" class="col-md-4 col-form-label">`+data_list.name+` `+variations+`</label>

                                        <div class="col-md-8">
                                                <input class="form-control mb-3" type="number" value="" name="stock_in_hand[]" id="stock_in_hand" placeholder="Enter Opening Stock" required>
                                                <input class="form-control" type="hidden" value="`+data_list.id+`" name="product_entry_id[]">
                                                <input class="form-control" type="hidden" value="`+data_list.product_id+`" name="product_id[]">
                                        </div>`;
                                    
                    
                });

                $(".opening-stock").append(category_list);

            });
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/47/Modules/Products/Resources/views/opening_stock.blade.php ENDPATH**/ ?>