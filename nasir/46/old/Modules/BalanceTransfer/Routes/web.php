<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('balancetransfer')->group(function() {
    Route::get('/', 'BalanceTransferController@index')->name('balance_transfer_index');
    Route::post('/store', 'BalanceTransferController@store')->name('balance_transfer_store');
    Route::get('/edit/{id}', 'BalanceTransferController@edit')->name('balance_transfer_edit');
    Route::post('/update/{id}', 'BalanceTransferController@update')->name('balance_transfer_update');
});
