<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('messages')->group(function() {
    Route::get('/', 'MessagesController@index')->name('messages_index');
    Route::post('/store', 'MessagesController@store')->name('messages_store');
    Route::get('/edit/{id}', 'MessagesController@edit')->name('messages_edit');
    Route::post('/update/{id}', 'MessagesController@update')->name('messages_update');

    Route::get('/send-index', 'MessagesController@messageSendIndex')->name('messages_send_index');
    Route::post('/send', 'MessagesController@sendSMS')->name('messages_send_send');
    Route::get('/contact-list-by-category/{message_id}/{cotact_category_id}', 'MessagesController@contactListByCategory')->name('messages_send_contact_list_by_category');
    Route::get('/send-list', 'MessagesController@messageSendList')->name('messages_send_list');
    Route::get('/delete-list', 'MessagesController@deleteSendList')->name('messages_delete_send_list');

    Route::get('/phone-book', 'MessagesController@phoneBookIndex')->name('messages_phone_book_index');
    Route::post('/phone-book-store', 'MessagesController@phoneBookStore')->name('messages_phone_book_store');
    Route::get('/phone-book-edit/{id}', 'MessagesController@phoneBookEdit')->name('messages_phone_book_edit');
    Route::post('/phone-book-update/{id}', 'MessagesController@phoneBookUpdate')->name('messages_phone_book_update');
    Route::get('/get-message/{message_id}', 'MessagesController@getMessage')->name('messages_send_get_message');
});
