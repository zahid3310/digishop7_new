<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductEntries extends Model
{
    protected $table = "product_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function productBranches()
    {
        return $this->hasMany(ProductBranches::class, "product_entry_id");
    }
}
