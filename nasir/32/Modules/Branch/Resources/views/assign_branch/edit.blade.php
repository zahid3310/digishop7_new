@extends('layouts.app')

@section('title', 'Assign Branch')

@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Assign Branch</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Basic Settings</a></li>
                                <li class="breadcrumb-item active">Assign Branch</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {!! Session::get('success') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('unsuccess'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! Session::get('unsuccess') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('errors'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! 'Some required fields are missing..!! Please try again..' !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            <form id="FormSubmit" action="{{ route('branch_assign_branch_update') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                @if((!empty($assign_branches)) && ($assign_branches->count() > 0))
                                @foreach($assign_branches as $key => $value)
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                        @if($key == 0)
                                        <label class="show-xs" for="productname">Product</label>
                                        @endif
                                        <select style="width: 100%;cursor: pointer" class="form-control" name="product_entry_id[]">
                                            <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-8 col-md-8 col-sm-12 col-12 form-group">
                                        @if($key == 0)
                                        <label class="show-xs" for="productname">Branch</label>
                                        @endif
                                        <select style="width: 100%;cursor: pointer" class="form-control select2 branches" name="branch_id[{{$value['id']}}][]" multiple>
                                            @if($value->productBranches->count() > 0)
                                            @foreach($value->productBranches as $key2 => $value2)
                                            <option value="{{$value2['branch_id']}}" selected>{{ $value2->branch->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @endforeach
                                @endif

                                <div class="form-group row">
                                    <div class="button-items col-md-3">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('home') }}">Close</a></button>
                                    </div>
                                    <div class="col-md-8"></div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        
        var site_url            = $('.site_url').val();

        $(".branches").select2({
            ajax: { 
            url:  site_url + '/branch/get-branch-list',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                return result['text'];
            },
        });
    });
</script>

<script type="text/javascript">
    function preventDoubleClick() {
        $('.enableOnInput').prop('disabled', true)
        $('#FormSubmit').submit();
    }
</script>
@endsection