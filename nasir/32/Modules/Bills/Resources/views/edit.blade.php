@extends('layouts.app')

@section('title', 'Edit Purchase')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Purchase</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Purchases</a></li>
                                    <li class="breadcrumb-item active">Edit Purchase</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('bills_update', $find_bill['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-3 col-form-label">{{ __('messages.supplier')}} *</label>
                                            <div class="col-md-9">
                                               <select style="width: 75%" id="customer_id" name="vendor_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" required>
                                                   <option value="{{ $find_bill['vendor_id'] }}" selected>{{ $find_bill['vendor_name'] }}</option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white!important;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">{{ $find_customer['supplier_advance_payment'] > 0 ? 'Rec.' : 'Paya.' }} </label>
                                            <div class="col-md-8">
                                                <input id="balance" name="previous_due" type="text" value="{{ $find_bill['previous_due'] }}" class="form-control" readonly>
                                                <input id="bbBalance" type="hidden" name="balance_type" class="form-control" value="{{ $find_customer['supplier_advance_payment'] > 0 ? 2 : 1 }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-3 col-form-label">{{ __('messages.note')}} </label>
                                            <div class="col-md-9">
                                                <input id="bill_note" name="bill_note" type="text" value="{{ $find_bill['bill_note'] }}" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">{{ __('messages.date')}} * </label>
                                            <div class="col-md-8">
                                                <input id="selling_date" name="selling_date" type="text" value="{{ date('d-m-Y', strtotime($find_bill['bill_date'])) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="col-md-2">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                                                    Add New Product
                                                </button>
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="col-md-6">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">{{ __('messages.major_category')}} *</label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="major_category_id" name="major_category_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" required>
                                                    <option value="{{ $find_bill['major_category_id'] != null ? $find_bill['major_category_id'] : 0 }}">{{ $find_bill['major_category_id'] != null ? $find_bill->majorCategory->name : 'All' }}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 415px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-9 input_fields_wrap getMultipleRow">
                                        @foreach($find_bill_entries as $key => $value)
                                            <div style="margin-bottom: 0px !important" class="row di_{{$key}}">
                                                <div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-12">
                                                    @if($key == 0)
                                                        <label class="hidden-xs" for="productname">{{ __('messages.product')}} *</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">{{ __('messages.product')}} *</label>
                                                    <select style="width: 100%" name="product_entries[]" class="inner form-control select2 productEntries" id="product_entries_{{$key}}" onchange="getItemPrice({{$key}})" required>
                                                        <option value="{{ $value['item_id'] }}">
                                                        {{ $value['item_name'] . ' ' . '( ' . str_pad($value['product_code'], 6, "0", STR_PAD_LEFT) . ' )' }}
                                                        </option>
                                                    </select>
                                                    <!-- <span id="stock_show_{{$key}}" style="color: black">{{ __('messages.stock')}}{{ ' : '.round($value['stock_in_hand'], 2) . ' ( ' . $value->unit->name . ' )' }}</span> -->
                                                </div>

                                                <input type="hidden" name="stock[]" class="inner form-control" id="stock_{{$key}}" placeholder="Stock" value="{{ $value['stock_in_hand'] }}" />
                                                <input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_{{$key}}" value="{{ $value['main_unit_id'] }}" />
                                                <input type="hidden" class="inner form-control" id="main_unit_name_{{$key}}" />

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">
                                                    @if($key == 0)
                                                        <label class="hidden-xs" for="productname">{{ __('messages.unit')}}</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">{{ __('messages.unit')}}</label>
                                                    <select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_{{$key}}" required onchange="getConversionParam({{$key}})">
                                                    <option value="{{ $value['conversion_unit_id'] }}">{{ $value->convertedUnit->name }}</option>
                                                    </select>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    @if($key == 0)
                                                        <label class="hidden-xs" for="productname">{{ __('messages.qty')}} *</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">{{ __('messages.qty')}} *</label>
                                                    <input type="text" name="quantity[]" class="inner form-control quantityCheck" id="quantity_{{$key}}" value="{{$value['quantity']}}" placeholder="Quantity" oninput="calculateActualAmount({{$key}})" required/>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    @if($key == 0)
                                                        <label class="hidden-xs" for="productname">{{ __('messages.rate')}} *</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">{{ __('messages.rate')}} *</label>
                                                    <input type="text" name="rate[]" class="inner form-control" id="rate_{{$key}}" value="{{$value['rate']}}" placeholder="Rate" oninput="calculateActualAmount({{$key}})" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                    
                                                    <div class="row">
                                                        <div  style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">
                                                            @if($key == 0)
                                                                <label class="hidden-xs" style="padding-top: 15px" for="productname"></label>
                                                            @endif
                                                            <label class="show-xs" style="display: none;padding-top: 17px" for="productname"></label>
                                                            <select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_{{$key}}" oninput="calculateActualAmount({{$key}})">
                                                                <option value="1" {{ $value['discount_type'] == 1 ? 'selected' : '' }}>BDT</option>
                                                                <option value="0" {{ $value['discount_type'] == 0 ? 'selected' : '' }}>%</option>
                                                            </select>
                                                        </div>

                                                        <div  style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">
                                                            @if($key == 0)
                                                                <label class="hidden-xs" style="padding-bottom: 0px" for="productname">{{ __('messages.discount')}}</label>
                                                            @endif
                                                            <label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">{{ __('messages.discount')}}</label>
                                                            <input type="text" name="discount[]" class="inner form-control" id="discount_{{$key}}" value="{{$value['discount_amount']}}" placeholder="Discount" oninput="calculateActualAmount({{$key}})"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    @if($key == 0)
                                                        <label class="hidden-xs" for="productname">{{ __('messages.total')}}</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">{{ __('messages.total')}}</label>
                                                    <input type="text" name="amount[]" class="inner form-control amount" id="amount_{{$key}}" value="{{$value['total_amount']}}" placeholder="Total" oninput="calculateActualAmount({{$key}})" />
                                                </div>
                                                
                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    @if($key == 0)
                                                        <label class="hidden-xs" for="productname">{{ __('messages.action')}}</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">{{ __('messages.action')}} *</label>
                                                    @if($key == 0)
                                                        <i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button"></i>
                                                    @else
                                                        <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field" data-val="{{$key}}"></i>
                                                    @endif 
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div style="display: none">
                                        <select style="padding: 6px;border-radius: 4px;cursor: pointer" class="taxType" id="tax_type_0" name="tax_type" oninput="calculateActualAmount(0)">
                                            <option style="padding: 10px" value="1" {{ $find_bill['tax_type'] == 1 ? 'selected' : '' }}>BDT</option>
                                            <option style="padding: 10px" value="0" {{ $find_bill['tax_type'] == 0 ? 'selected' : '' }}>%</option>
                                        </select>
                                        <input id="tax_amount_0" type="text" class="form-control width-xs taxAmount" name="tax_amount" value="{{ $find_bill['total_tax'] }}" oninput="calculateActualAmount(0)">
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 415px;padding-top: 20px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.sub_total')}}</label>
                                            <div class="col-md-7">
                                                <input type="text" id="subTotalBdt" class="form-control">
                                                <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.vat')}}</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control" id="vat_type_0" name="vat_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="1" {{ $find_bill['vat_type'] == 1 ? 'selected' : '' }}>BDT</option>
                                                    <option style="padding: 10px" value="0" {{ $find_bill['vat_type'] == 0 ? 'selected' : '' }}>%</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="vat_amount_0" type="text" class="form-control width-xs" name="vat_amount" value="{{ $find_bill['total_vat'] }}" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.discount')}}</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control totalDiscountType" id="total_discount_type_0" name="total_discount_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="1" {{ $find_bill['total_discount_type'] == 1 ? 'selected' : '' }}>BDT</option>
                                                    <option style="padding: 10px" value="0" {{ $find_bill['total_discount_type'] == 0 ? 'selected' : '' }}>%</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="total_discount_0" type="text" class="form-control totalDiscount" name="total_discount_amount" value="{{ $find_bill['total_discount_amount'] }}" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.dis_note')}}</label>
                                            <div class="col-md-7">
                                                <input id="total_discount_note" type="text" class="form-control width-xs" name="total_discount_note" value="{{ $find_bill['total_discount_note'] }}" placeholder="{{ __('messages.dis_note')}}">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.total_payable')}}</label>
                                            <div class="col-md-7">
                                                <input type="text" id="totalBdt" class="form-control" readonly>
                                                <input style="display: none" type="text" id="totalBdtShow" name="total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.cash_given')}}</label>
                                            <div class="col-md-7">
                                                <input id="cash_given" type="text" class="form-control" value="{{ $find_bill['cash_given'] }}" name="cash_given" oninput="calculateChangeAmount()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px;display: none" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Adjustment</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="adjustment" name="adjustment" placeholder="Adjustment" value="{{ $find_bill['adjusted_amount'] }}" oninput="calculateChangeAmount()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Advance</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="change_amount" name="change_amount" value="{{ $find_bill['change_amount'] }}" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.send_sms')}}</label>
                                            <div class="col-md-12">
                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        {{ __('messages.non_masking')}}
                                                    </label>
                                                </div>

                                                <div style="padding-left: 10px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        {{ __('messages.masking')}}
                                                    </label>
                                                </div>

                                                <div style="padding-left: 10px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        {{ __('messages.voice')}}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>

                                    <div style="display: none;" class="row justify-content-end">
                                        <div class="col-lg-1 col-md-2 form-group">
                                            <input id="pos_add_button" type="button" class="btn btn-success btn-block inner add_field_button_pos" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px !important;padding-bottom: 0px !important" class="row">
                                    <div style="background-color: #D2D2D2;height: 115px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-9 input_fields_wrap_payment getMultipleRowPayment">
                                        @if($payment_entries->count() > 0)
                                        @foreach($payment_entries as $key_payment => $payment_value)
                                        <div class="row di_payment_{{$key_payment}}">
                                            <div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                @if($key_payment == 0)
                                                    <label class="hidden-xs" for="productname">{{ __('messages.date')}} *</label>
                                                @endif
                                                <label style="display: none" class="show-xs" for="productname">{{ __('messages.date')}} *</label>
                                                <input id="payment_date" name="payment_date[]" type="text" value="{{ date('d-m-Y', strtotime($payment_value['payment_date'])) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                            </div>

                                            <div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                @if($key_payment == 0)
                                                    <label class="hidden-xs" for="productname">{{ __('messages.amount_paid')}} *</label>
                                                @endif
                                                <label style="display: none" class="show-xs" for="productname">{{ __('messages.amount_paid')}} *</label>
                                                <input type="text" name="amount_paid[]" class="form-control" id="amount_paid_{{$key_payment}}" value="{{$payment_value['amount']}}" required />
                                            </div>

                                            <div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                @if($key_payment == 0)
                                                    <label class="hidden-xs" for="productname">{{ __('messages.paid_through')}} *</label>
                                                @endif
                                                <label style="display: none" class="show-xs" for="productname">{{ __('messages.paid_through')}} *</label>
                                                <select id="paid_through_{{$key_payment}}" style="cursor: pointer" name="paid_through[]" class="form-control single_select2">
                                                    @if(!empty($paid_accounts) && ($paid_accounts->count() > 0))
                                                    @foreach($paid_accounts as $key => $paid_account)
                                                        <option {{ $paid_account['id'] == $payment_value['paid_through_id'] ? 'selected' : '' }} value="{{ $paid_account['id'] }}">{{ $paid_account['name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>

                                            <div style="" class="col-lg-2 col-md-3 col-sm-6 col-6">
                                                @if($key_payment == 0)
                                                    <label class="hidden-xs" for="productname">{{ __('messages.ac_info')}}.</label>
                                                @endif
                                                <label style="display: none" class="show-xs" for="productname">{{ __('messages.account_information')}}</label>
                                                <input type="text" name="account_information[]" class="form-control" id="account_information_{{$key_payment}}" value="{{$payment_value['account_information']}}"/>
                                            </div>

                                            <div style="" class="col-lg-2 col-md-3 col-sm-6 col-6">
                                                @if($key_payment == 0)
                                                    <label class="hidden-xs" for="productname">{{ __('messages.note')}}</label>
                                                @endif
                                                <label style="display: none" class="show-xs" for="productname">{{ __('messages.note')}}</label>
                                                <input type="text" name="note[]" class="form-control" id="note_{{$key_payment}}" value="{{$payment_value['note']}}"/>
                                            </div>

                                            <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group">
                                                @if($key_payment == 0)
                                                    <label class="hidden-xs" for="productname">{{ __('messages.action')}}</label>
                                                @endif
                                                <label style="display: none" class="show-xs" for="productname">{{ __('messages.action')}}</label>
                                                @if($key_payment == 0)
                                                <i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonPayment()"></i>
                                                @else
                                                <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field_payment" data-val="{{$key_payment}}"></i>
                                                @endif
                                            </div>
                                        </div>
                                        <input type="hidden" name="payment_id[]" value="{{ $payment_value['payment_id'] }}">
                                        @endforeach
                                        @endif

                                        <div style="display: none" class="row justify-content-end">
                                            <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                                <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 115px;padding-top: 10px" class="col-md-3">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">{{ __('messages.update')}}</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">{{ __('messages.update_print')}}</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('bills_index') }}">{{ __('messages.close')}}</a></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">{{ __('messages.add_new_supplier')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">{{ __('messages.name')}} *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control commonSupplierClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">{{ __('messages.mobile_no')}}</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control commonSupplierClass" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">{{ __('messages.address')}}</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control commonSupplierClass">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">{{ __('messages.save')}}</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">{{ __('messages.close')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">{{ __('messages.add_new_product')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">

                    <div class="row">
                        <input type="hidden" name="product_id" id="product_id" />
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">{{ __('messages.category')}} *</label>
                            <select style="width: 100%" id="product_category_id" name="product_category_id" class="form-control select2 col-lg-12 col-md-12 col-sm-12 col-12" required>
                               <option value="">--{{ __('messages.select_category')}}--</option>
                           </select>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">{{ __('messages.sub_category')}} *</label>
                            <select style="width: 100%" id="product_sub_category_id" name="product_sub_category_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10">
                               <option value="">--{{ __('messages.select_sub_category')}}--</option>
                            </select>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label for="productname">{{ __('messages.product_code')}}</label>
                            <input type="text" name="code" class="inner form-control" id="product_code" value="{{ $product_id != null ? str_pad($product_id[0]['product_code'] + 1, 6, "0", STR_PAD_LEFT) : str_pad(1, 6, "0", STR_PAD_LEFT) }}" readonly />
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label for="productname">{{ __('messages.product_name')}} *</label>
                            <input type="text" name="product_name" class="inner form-control" id="product_name" placeholder="{{ __('messages.product_name')}}" required />
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label for="productname">{{ __('messages.unit')}}</label>
                            <select id="unit_id" style="width: 100%" class="form-control select2 unit" name="unit_id">
                                <option value="">--{{ __('messages.select_unit')}}--</option>
                                @if(!empty($units))
                                    @foreach($units as $key => $unit)
                                    <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label for="productname">{{ __('messages.product_type')}}</label>
                            <select style="cursor: pointer" id="product_type" class="form-control" name="product_type">
                                <option value="1">{{ __('messages.single')}}</option>
                                <!-- <option value="2">Variable</option> -->
                            </select>
                        </div>

                        <div style="display: none" class="col-md-12 variationClass">
                            <hr style="margin-top: 0px !important">

                            <div data-repeater-list="inner-group" class="inner form-group mb-0 row">
                                <div class="inner col-lg-12 ml-md-auto input_fields_wrap_variation getMultipleRowVariation">

                                    <h5>Add Variations</h5>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 col-sm-6"></div>
                                <div class="col-lg-1 col-md-2 col-sm-6 col-12 form-group">
                                    <div id="add_field_button_variation" class="add_field_button_variation">
                                        <i class="btn btn-success btn-block bx bx-plus font-size-20"></i>
                                    </div>
                                </div>
                            </div>

                            <hr style="margin-top: 0px !important">
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label for="productname">Purchase Price *</label>
                            <input type="text" name="buying_price" class="inner form-control" id="buying_price" placeholder="Purchase Price" required />
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label for="productname">Sell Price *</label>
                            <input type="text" name="selling_price" class="inner form-control" id="selling_price" placeholder="Sell Price"required />
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label style="padding-left: 0px" for="productname" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Alert Quantity</label>
                            <input type="text" name="alert_quantity" class="inner form-control" id="alert_quantity" placeholder="Alert Quantity" />
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Status</label>
                            <select id="status" style="width: 100%" class="form-control select2" name="status">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                        <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            calculateActualAmount();

            var site_url            = $('.site_url').val();
            var major_category_id   = $('#major_category_id').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if ((result['contact_type'] == 1) && result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#product_category_id").select2({
                ajax: { 
                url:  site_url + '/products/product-category-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
                }
            });

            $("#product_category_id").select2({
                ajax: { 
                url:  site_url + '/products/product-category-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
                }
            });

            $(".productEntries").select2({
                ajax: { 
                url:  site_url + '/bills/product-list-load-bill/' + major_category_id,
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $('#add_field_button_payment').click();
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = 1;
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                            $('.commonSupplierClass').val('');
                        }
                        
                        $("#customer_id").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });

        $('#submitBtn1').click(function() {
            
            var product_category_id             = $("#product_category_id").val();
            var product_sub_category_id         = $("#product_sub_category_id").val();
            var status                          = $("#status").val();
            var product_name                    = $("#product_name").val();
            var product_code                    = $("#product_code").val();
            var unit_id                         = $("#unit_id").val();
            var product_type                    = $("#product_type").val();
            var buying_price                    = $("#buying_price").val();
            var selling_price                   = $("#selling_price").val();
            var alert_quantity                  = $("#alert_quantity").val();
            var site_url                        = $('.site_url').val();

            if (product_name == '' || product_category_id == '' || buying_price == '' || selling_price == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }

            $.ajax({
    
                type:   'get',
                url:    site_url + '/bills/from-bill/store/product/',
                data:   { product_category_id : product_category_id, product_sub_category_id : product_sub_category_id, status : status, product_name : product_name, product_code : product_code, unit_id : unit_id, product_type : product_type, buying_price : buying_price, selling_price : selling_price, alert_quantity : alert_quantity,_token: '{{csrf_token()}}' },
    
                success: function (data) {
                    console.log(data);
                    if(data != 0)
                    {
                        $('#CloseButton1').click();
                        $('#add_field_button').click();
                    }

                    $("#product_id").val(data.id);
                }
    
            });
        });     
    </script>

    <script type="text/javascript">
        // Get the input field
        var input = document.getElementById("product_code");

            // Execute a function when the user releases a key on the keyboard
            input.addEventListener("keyup", function(event) {
            // Number 13 is the "Enter" key on the keyboard
            if (event.keyCode === 13) {
                // Cancel the default action, if needed
                event.preventDefault();
                $("#alertMessage").hide();

                var site_url            = $(".site_url").val();
                var product_code_pos    = $("#product_code").val();

                if(product_code_pos != '')
                {
                    $.get(site_url + '/bills/bill-product-entries-list/'+ product_code_pos, function(data_pos){

                        if ($.isEmptyObject(data_pos))
                        {   
                            $('input[name=product_code').val('');
                            $("#alertMessage").show();
                        }
                        else
                        {
                            var state = 0;

                            $('.productEntries').each(function()
                            {
                                var entry_id    = $(this).val();
                                var value_x     = $(this).prop("id");

                                if (data_pos.id == entry_id)
                                {
                                    var explode          = value_x.split('_');
                                    var quantity_id      = explode[2];
                                    var quantity         = $('#quantity_'+quantity_id).val();
                                    var inc_quantity     = parseFloat(quantity) + 1;

                                    $('#quantity_'+quantity_id).val(inc_quantity);
                                    $('#quantity_'+quantity_id).change();
                                    $('input[name=product_code').val('');
                                    calculateActualAmount(quantity_id);

                                    state++;
                                }

                                if (entry_id == '')
                                {   
                                    var explode          = value_x.split('_');
                                    var quantity_id      = explode[2];
                                    
                                    ProductEntriesList(quantity_id);

                                    state++;
                                }
                            });

                            if (state == 0)
                            {   
                                $("#pos_add_button").click();
                            }
                            else
                            {
                                return false;
                            }
                        }
                    });
                } 
            }
        });

        function ProductEntriesList(x) 
        {
            //Barcode Scan Section Start
            var site_url            = $(".site_url").val();
            var product_code_pos    = $("#product_code").val();

            if (product_code_pos != undefined)
            {

                var product_code        = $("#product_code").val();
                $('input[name=product_code').val('');
            }
            else
            {
                var product_code        = $(".product_entries_"+x).val();
            }

            if (product_code)
            {
                $.get(site_url + '/bills/bill-product-entries-list/'+ product_code, function(data_pos){

                    if (data_pos.product_type == 2)
                    {
                        var variations  = ' - ' + data_pos.variations;
                    }
                    else
                    {
                        var variations  = '';
                    }

                    $('#product_entries_'+x).append('<option value="'+ data_pos.id +'" selected>'+ data_pos.name + variations + ' ' + '( ' + data_pos.product_code + ' )' +'</option>');
                    $('#product_entries_'+x).change();
                });
            }
        }
        
        function checkValue(value,arr)
        {
            var status = 'Not exist';
             
            for(var i=0; i<arr.length; i++)
            {
                var name = arr[i];
                if(name == value)
                {
                    status = 'Exist';
                    break;
                }
            }

            return status;
        }

        function getItemPrice(x)
        {
            //For getting item commission information from items table start
            var entry_id  = $("#product_entries_"+x).val();
            var site_url  = $(".site_url").val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    var list    = '';
                    $.each(data.unit_conversions, function(i, data_list)
                    {   
                        list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                    });

                    $("#unit_id_"+x).empty();
                    $("#unit_id_"+x).append(list);

                    if (data.product_entries.stock_in_hand == '' || data.product_entries.stock_in_hand == null)
                    {
                        var stockInHand  = 0;
                    }
                    else
                    {
                        var stockInHand  = data.product_entries.stock_in_hand;
                    }

                    if (data.product_entries.unit_id == null)
                    {
                        var unit  = '';
                    }
                    else
                    {
                        var unit  = ' ( ' + data.product_entries.unit_name + ' )';
                    }

                    $("#rate_"+x).val(parseFloat(data.product_entries.buy_price).toFixed(2));
                    $("#quantity_"+x).val(1);

                    if (data.branch_id == 1)
                    {
                        $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
                        $("#stock_show_"+x).html('{{ __('messages.stock')}} : ' + parseFloat(stockInHand).toFixed(2) + ' ' + unit);
                    }
                    else
                    {
                        if (data.branch_product != null)
                        {
                            $("#stock_"+x).val(parseFloat(data.branch_product.stock_in_hand).toFixed(2));
                            $("#stock_show_"+x).html('{{ __('messages.stock')}} : ' + parseFloat(data.branch_product.stock_in_hand).toFixed(2) + ' ' + unit);
                        }
                        else
                        {
                            $("#stock_"+x).val(0);
                            $("#stock_show_"+x).html('{{ __('messages.stock')}} : ' + '0' + ' ' + unit);
                        }
                    }

                    $("#main_unit_id_"+x).val(data.product_entries.unit_id);
                    $("#main_unit_name_"+x).val(data.product_entries.unit_name);
                    $("#discount_"+x).val(0);

                    calculateActualAmount(x);

                });
            }
            
            //For getting item commission information from items table end
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                    $("#stock_show_"+x).html('{{ __('messages.stock')}} : ' + convertedStock + ' ( ' + convertedUnitName + ' )');
                    $("#rate_"+x).val(parseFloat(data.purchase_price).toFixed(2));

                    calculateActualAmount(x);
                }

            });
        }

        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }

        function addButtonPayment()
        {
            $('.add_field_button_payment').click();
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = {{$entries_count}};
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == 1)
                {
                    var product_label       = '<label class="hidden-xs" for="productname">{{ __('messages.product')}} *</label>\n';
                    var unit_label          = '<label class="hidden-xs" for="productname">{{ __('messages.unit')}}</label>\n';
                    var stock_label         = '<label class="hidden-xs" for="productname">{{ __('messages.stock')}}</label>\n';
                    var rate_label          = '<label class="hidden-xs" for="productname">{{ __('messages.rate')}} *</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">{{ __('messages.qty')}} *</label>\n';
                    var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">{{ __('messages.discount')}}</label>\n';
                    var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 14px" for="productname"></label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">{{ __('messages.total')}}</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">{{ __('messages.action')}}</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButton()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var product_label       = '';
                    var unit_label          = '';
                    var stock_label         = '';
                    var rate_label          = '';
                    var quantity_label      = '';
                    var discount_label      = '';
                    var type_label          = '';
                    var amount_label        = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                ProductEntriesList(x);

                $('.getMultipleRow').append(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                            '<option value="">' + '--{{ __('messages.select_product')}}--' + '</option>' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                        unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_'+x+'" required onchange="getConversionParam('+x+')">\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control" id="quantity_'+x+'" placeholder="{{ __('messages.qty')}}" oninput="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">{{ __('messages.rate')}} *</label>\n' +
                                                        rate_label +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="{{ __('messages.rate')}}" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 14px" for="productname"></label>\n' +
                                                                type_label +
                                                                '<select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_'+x+'" oninput="calculateActualAmount('+x+')">\n' +
                                                                    '<option value="1">BDT</option>' +
                                                                    '<option value="0" selected>%</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" name="discount[]" class="inner form-control" id="discount_'+x+'" placeholder="{{ __('messages.discount')}}" oninput="calculateActualAmount('+x+')"/>\n' +
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">{{ __('messages.total')}}</label>\n' +
                                                        amount_label +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="{{ __('messages.total')}}" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

                var site_url            = $(".site_url").val();
                var major_category_id   = $('#major_category_id').val();
                $("#product_entries_"+x).select2({
                    ajax: { 
                    url:  site_url + '/bills/product-list-load-bill/' + major_category_id,
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                    },
                });
            }                                    
        });
        //For apending another rows end

        //For apending another rows start
        $(add_button_pos).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;

                var serial = x + 1;

                if (serial == 1)
                {
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var unit_label          = '<label class="hidden-xs" for="productname">Unit</label>\n';
                    var stock_label         = '<label class="hidden-xs" for="productname">Stock</label>\n';
                    var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                    var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">Discount</label>\n';
                    var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 14px" for="productname"></label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButton()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var product_label       = '';
                    var unit_label          = '';
                    var stock_label         = '';
                    var rate_label          = '';
                    var quantity_label      = '';
                    var discount_label      = '';
                    var type_label          = '';
                    var amount_label        = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                ProductEntriesList(x);

                $('.getMultipleRow').append(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                            '<option value="">' + '--Select Product--' + '</option>' +
                                                        '</select>\n' +
                                                        '<span id="stock_show_'+x+'" style="color: black">' + '</span>' +
                                                    '</div>\n' +

                                                    '<input type="hidden" name="stock[]" class="inner form-control" id="stock_'+x+'" placeholder="Stock" oninput="calculateActualAmount('+x+')" />\n' +
                                                    '<input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_'+x+'" />\n' +
                                                    '<input type="hidden" class="inner form-control" id="main_unit_name_'+x+'" />\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-12 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Unit</label>\n' +
                                                        unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_'+x+'" required onchange="getConversionParam('+x+')">\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control" id="quantity_'+x+'" placeholder="Quantity" oninput="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 14px" for="productname"></label>\n' +
                                                                type_label +
                                                                '<select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_'+x+'" oninput="calculateActualAmount('+x+')">\n' +
                                                                    '<option value="1">BDT</option>' +
                                                                    '<option value="0" selected>%</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" name="discount[]" class="inner form-control" id="discount_'+x+'" placeholder="Discount" oninput="calculateActualAmount('+x+')"/>\n' +
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                        amount_label +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

                var site_url            = $(".site_url").val();
                var major_category_id   = $('#major_category_id').val();
                $("#product_entries_"+x).select2({
                    ajax: { 
                    url:  site_url + '/bills/product-list-load-bill/' + major_category_id,
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                        cache: true
                    },

                    minimumInputLength: 0,
                    escapeMarkup: function(result) {
                        return result;
                    },
                    templateResult: function (result) {
                        if (result.loading) return 'Searching...';

                        return result['text'];
                    },
                });
            } 
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            var rate                    = $("#rate_"+x).val();
            var quantity                = $("#quantity_"+x).val();
            var stock                   = $("#stock_"+x).val();
            var discount                = $("#discount_"+x).val();
            var discountType            = $("#discount_type_"+x).val();
            var vatType                 = $("#vat_type_0").val();
            var vatAmount               = $("#vat_amount_0").val();
            var taxType                 = $("#tax_type_0").val();
            var taxAmount               = $("#tax_amount_0").val();
            var totalDiscount           = $("#total_discount_0").val();
            var totalDiscountType       = $("#total_discount_type_0").val();
            
            if (rate == '')
            {
                var rateCal             = 0;
            }
            else
            {
                var rateCal             = $("#rate_"+x).val();
            }

            if (quantity == '')
            {
                var quantityCal         = 1;
            }
            else
            {
                var quantityCal         = $("#quantity_"+x).val();
            }

            if (discount == '')
            {
                var discountCal         = 1;
            }
            else
            {
                var discountCal         = $("#discount_"+x).val();
            }

            if (discount == '')
            {
                var discountTypeCal     = 0;
            }
            else
            {
                if (discountType == 0)
                {
                    var discountTypeCal     = (parseFloat(discountCal)*parseFloat(rateCal)*parseFloat(quantityCal))/100;
                }
                else
                {
                    var discountTypeCal     = $("#discount_"+x).val();
                }
            }

            if (parseFloat(rateCal) > 0)
            {
                var AmountIn              =  (parseFloat(rateCal)*parseFloat(quantityCal)) - parseFloat(discountTypeCal);
                $("#amount_"+x).val(AmountIn);
            }

            //Calculating Subtotal Amount
            var total       = 0;

            $('.amount').each(function()
            {
                total       += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(total);
            $("#subTotalBdtShow").val(total);

            if (vatAmount == '')
            {   
                $("#vat_amount_0").val(0);
                var vatCal         = 0;
            }
            else
            {
                var vatCal         = $("#vat_amount_0").val();
            }

            if (vatType == 0)
            {
                var vatTypeCal     = (parseFloat(vatCal)*(parseFloat(total)))/100;
            }
            else
            {
                var vatTypeCal     = $("#vat_amount_0").val();
            }

            if (taxAmount == '')
            {   
                $("#tax_amount_0").val(0);
                var taxCal         = 1;
            }
            else
            {
                var taxCal         = $("#tax_amount_0").val();
            }

            if (taxType == 0)
            {
                var taxTypeCal     = (parseFloat(taxCal)*(parseFloat(total)))/100;
            }
            else
            {
                var taxTypeCal     = $("#tax_amount_0").val();
            }

            if (totalDiscount > 0)
            {   
                if (totalDiscountType == 0)
                {
                    var totalDiscountTypeCal     = (parseFloat(totalDiscount)*(parseFloat(total) + parseFloat(vatTypeCal)))/100;
                }
                else
                {
                    var totalDiscountTypeCal     = $("#total_discount_0").val();
                }
            }
            else
            {
                var totalDiscountTypeCal     = 0;
            }

            var totalShow = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountTypeCal);

            $("#totalBdtShow").val(totalShow);
            $("#totalBdt").val(parseFloat(totalShow));

            // calculateChangeAmount();
        }
    </script>

    <script type="text/javascript">
        var max_fields_payment       = 50;                                  //maximum input boxes allowed
        var wrapper_payment          = $(".input_fields_wrap_payment");      //Fields wrapper
        var add_button_payment       = $(".add_field_button_payment");       //Add button ID
        var index_no_payment         = 1;

        //For apending another rows start
        var y = {{$payment_entries_count}};
        $(add_button_payment).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(y < max_fields_payment)
            {   
                var serial = y + 1;

                if (serial == 1)
                {
                    var amount_paid_label   = '<label class="hidden-xs" for="productname">{{ __('messages.amount_paid')}} *</label>\n';
                    var date_label          = '<label class="hidden-xs" for="productname">{{ __('messages.date')}} *</label>\n';
                    var paid_through_label  = '<label class="hidden-xs" for="productname">{{ __('messages.paid_through')}} *</label>\n';
                    var account_info_label  = '<label class="hidden-xs" for="productname">{{ __('messages.ac_info')}}.</label>\n';
                    var note_label          = '<label class="hidden-xs" for="productname">{{ __('messages.note')}}</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">{{ __('messages.action')}}</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button_payment">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">{{ __('messages.action')}}</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonPayment()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var amount_paid_label   = '';
                    var date_label          = '';
                    var paid_through_label  = '';
                    var account_info_label  = '';
                    var note_label          = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field_payment" data-val="'+y+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">{{ __('messages.action')}}</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                $('.getMultipleRowPayment').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_payment_'+y+'">' +
                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">{{ __('messages.date')}} *</label>\n' +
                                                        date_label +
                                                        '<input id="payment_date" name="payment_date[]" type="text" value="{{ date("d-m-Y") }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">{{ __('messages.amount_paid')}} *</label>\n' +
                                                        amount_paid_label +
                                                        '<input type="text" name="amount_paid[]" class="form-control" id="amount_paid_'+y+'" value="0" required />\n' + 
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">{{ __('messages.paid_through')}} *</label>\n' +
                                                        paid_through_label  +
                                                        '<select id="paid_through_'+y+'" style="cursor: pointer" name="paid_through[]" class="form-control single_select2">\n' +
                                                            '@if(!empty($paid_accounts) && ($paid_accounts->count() > 0))\n' +
                                                            '@foreach($paid_accounts as $key => $paid_account)\n' +
                                                                '<option value="{{ $paid_account['id'] }}">{{ $paid_account['name'] }}</option>\n' +
                                                            '@endforeach\n' +
                                                            '@endif\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-2 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">{{ __('messages.account_information')}}</label>\n' +
                                                        account_info_label +
                                                        '<input type="text" name="account_information[]" class="form-control" id="account_information_'+y+'" placeholder="{{ __('messages.account_information')}}"/>\n' + 
                                                    '</div>\n' + 

                                                    '<div style="" class="col-lg-2 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">{{ __('messages.note')}}</label>\n' +
                                                        note_label +
                                                        '<input type="text" name="note[]" class="form-control" id="note_'+y+'" placeholder="{{ __('messages.note')}}"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    add_btn +

                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();

                y++;
            }                                    
        });
        //For apending another rows end

        $(wrapper_payment).on("click",".remove_field_payment", function(e)
        {
            e.preventDefault();

            var y = $(this).attr("data-val");

            $('.di_payment_'+y).remove(); y--;

            calculateActualAmount(y);
        });
    </script>

    <script type="text/javascript">
        $(document).on("change", "#defaultCheck2" , function() {

            var checkbox_value    = $("#defaultCheck2")[0].checked;

            if (checkbox_value == true)
            {
                $("#vatShow").show();
                $("#taxShow").show();
            }
            else
            {
                $("#vatShow").hide();
                $("#taxShow").hide();
            }
        });
    </script>

    <script type="text/javascript">
        function sellingPriceTaxType()
        {
            var tax_type  = $("#tax_type").val();

            if (tax_type == 2)
            {
                $("#serviceCharge").hide();
                $("#vatPercentage").hide();
                $("#service_charge").val('');
                $("#vat_percentage").val('');

                CalculateTotalSellPrice();
            }
            else
            {
                $("#serviceCharge").show();
                $("#vatPercentage").show();
                $("#service_charge").val('');
                $("#vat_percentage").val('');

                CalculateTotalSellPrice();
            }
        }

        $(document).on("change", "#defaultCheck2" , function() {

            var checkbox_value    = $("#defaultCheck2")[0].checked;

            if (checkbox_value == true)
            {
                $("#vatShow").show();
                $("#taxShow").show();
            }
            else
            {
                $("#vatShow").hide();
                $("#taxShow").hide();
            }
        });

        function CalculateTotalSellPrice()
        {
            var sell_price_call                 = $("#selling_price").val();
            var vat_percentage_call             = $("#vat_percentage").val();
            var service_charge_call             = $("#service_charge").val();

            if (sell_price_call == '')
            {
                var sell_price                  = 0;
            }
            else
            {
                var sell_price                  = $("#selling_price").val();
            }

            if (vat_percentage_call == '')
            {
                var vat_percentage              = 0;
            }
            else
            {
                var vat_percentage              = $("#vat_percentage").val();
            }

            if (service_charge_call == '')
            {
                var service_charge              = 0;
            }
            else
            {
                var service_charge              = $("#service_charge").val();
            }

            var vat_amount                      = (parseFloat(sell_price)*parseFloat(vat_percentage))/100;
            var service_charge_amount           = (parseFloat(sell_price)*parseFloat(service_charge))/100;
            var selling_price_exclusive_tax     = parseFloat(sell_price) - parseFloat(vat_amount) - parseFloat(service_charge_amount);

            $("#selling_price_exclusive_tax").val(selling_price_exclusive_tax);
        }

        function calculateChangeAmount()
        {
            var tAmount             = $("#totalBdtShow").val();
            var balance             = $("#balance").val();
            var cGiven              = $("#cash_given").val();
            var adjustment          = $("#adjustment").val();
            var type                = $("#bbBalance").val();

            if (tAmount != '')
            {
                var totalAmount     = $("#totalBdtShow").val();
            }
            else
            {
                var totalAmount     = 0;
            }

            if (adjustment != '')
            {
                var adjustmentAmount     = $("#adjustment").val();
            }
            else
            {
                var adjustmentAmount     = 0;
            }

            if (cGiven != '')
            {
                var cashGiven       = $("#cash_given").val();
            }
            else
            {
                var cashGiven       = 0;
            }

            if (balance != '')
            {
                var balanceAmount   = $("#balance").val();
            }
            else
            {
                var balanceAmount   = 0;
            }

            var paidAmount  = parseFloat(cashGiven) + parseFloat(adjustmentAmount);

            if (type == 1)
            {   
                var payaAmount  = parseFloat(balanceAmount) + parseFloat(totalAmount);
            }
            else
            {
                var payaAmount  = parseFloat(totalAmount);
            }

            var changeAmount            = parseFloat(paidAmount) - parseFloat(payaAmount);
            var changeAmountOriginal    = parseFloat(paidAmount) - parseFloat(totalAmount);
            
            if (parseFloat(paidAmount) <= parseFloat(totalAmount))
            { 
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(paidAmount));
            }

            if ((parseFloat(paidAmount) > parseFloat(totalAmount)) && (parseFloat(paidAmount) <= parseFloat(payaAmount)))
            {   
                $("#change_amount").val(0);
                $("#amount_paid_0").val(parseFloat(totalAmount));
            }

            if (parseFloat(paidAmount) > parseFloat(payaAmount))
            {   
                $("#change_amount").val(parseFloat(changeAmount));
                $("#amount_paid_0").val(parseFloat(totalAmount));
            }
        }

        $(document).on("change", "#major_category_id" , function() {
            var site_url            = $('.site_url').val();
            var major_category_id   = $('#major_category_id').val();
            $("#product_entries_0").select2({
                ajax: { 
                url:  site_url + '/bills/product-list-load-bill/' + major_category_id,
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        }); 

        $(document).on("change", "#customer_id" , function() {

            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            $.get(site_url + '/bills/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data.balance);

                if (data.type == 1)
                {
                    $("#bbShow").html('Paya.');
                    $("#bbBalance").val(data.type);
                }
                else
                {
                    $("#bbShow").html('Rec.');
                    $("#bbBalance").val(data.type);
                }
                
            });
        });

        function adjustAdvancePayment()
        {
            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            if ($('#adjustAdvancePayment').is(":checked"))
            {
                $("#adjustment").val(0);
                $("#customer_advance_amount").val(0);
                calculateChangeAmount();
            }
            else
            {
                $.get(site_url + '/bills/adjust-advance-payment/' + customerId, function(data){

                    var payable  = $("#totalBdtShow").val();

                    if (parseFloat(payable) <= parseFloat(data))
                    {
                        $("#adjustment").val(parseFloat(payable));
                    }
                    else
                    {
                        $("#adjustment").val(parseFloat(data));
                    }

                    $("#customer_advance_amount").val(parseFloat(data));
                    calculateChangeAmount();
                });
            }
        } 
    </script>
@endsection