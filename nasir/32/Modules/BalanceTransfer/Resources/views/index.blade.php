@extends('layouts.app')

@section('title', 'Balance Transfer')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.transfer_balance')}} </h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.transfer_balance')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.transfer_balance')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('balance_transfer_store') }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">{{ __('messages.transfer_date')}} *</label>
                                        <input id="transfer_date" name="transfer_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">{{ __('messages.transfer_from')}} *</label>
                                        <select style="width: 100%" id="transfer_from" name="transfer_from" class="form-control select2" required>
                                           <option value="">--{{ __('messages.select_account')}}--</option>
                                           @if($paid_accounts->count() > 0)
                                           @foreach($paid_accounts as $from_account)
                                           <option value="{{ $from_account->id }}">{{ $from_account->name }}</option>
                                           @endforeach
                                           @endif
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">{{ __('messages.transfer_to')}} *</label>
                                        <select style="width: 100%" id="transfer_to" name="transfer_to" class="form-control select2" required>
                                           <option value="">--{{ __('messages.select_account')}}--</option>
                                           @if($paid_accounts->count() > 0)
                                           @foreach($paid_accounts as $to_account)
                                           <option value="{{ $to_account->id }}">{{ $to_account->name }}</option>
                                           @endforeach
                                           @endif
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">{{ __('messages.note')}}</label>
                                        <input type="text" name="note" class="inner form-control" id="note" placeholder="{{ __('messages.transfer_note')}}" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">{{ __('messages.amount')}} *</label>
                                        <input type="text" name="amount" class="inner form-control" id="amount" placeholder="{{ __('messages.amount')}}" required />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">{{ __('messages.save')}}</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('balance_transfer_index') }}">{{ __('messages.close')}}</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">{{ __('messages.all_transfer')}}</h4>

                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.sl')}}#</th>
                                            <th>{{ __('messages.date')}}</th>
                                            <th>{{ __('messages.transfer_from')}}</th>
                                            <th>{{ __('messages.transfer_to')}}</th>
                                            <th>{{ __('messages.transfer_note')}}</th>
                                            <th>{{ __('messages.amount')}}</th>
                                            <th>{{ __('messages.action')}}</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($transfers) && ($transfers->count() > 0))
                                        @foreach($transfers as $key => $transfer)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ date('d-m-Y', strtotime($transfer['transaction_date'])) }}</td>
                                                <td>{{ $transfer->transfer_from != null ? $transfer->transferFrom->name : '' }}</td>
                                                <td>{{ $transfer->transfer_to != null ? $transfer->transferTo->name : '' }}</td>
                                                <td>{{ $transfer->note }}</td>
                                                <td>{{ $transfer->amount }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('balance_transfer_edit', $transfer['id']) }}">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection