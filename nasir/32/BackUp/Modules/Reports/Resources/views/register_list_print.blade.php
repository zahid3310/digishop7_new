<!DOCTYPE html>
<html>
<?php
    if (isset($_GET['type']) && ($_GET['type'] == 0))
    {
        $title = 'Buyer List';
    }

    if (isset($_GET['type']) && ($_GET['type'] == 1))
    {
        $title = 'Suppliers List';
    }
?>
<head>
    <title>{{ $title }} </title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<style type="text/css">
    @page {
      size: A4 landscape;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">{{ $title }} Print</h6>
                    </div>

                    <div class="ibox-content">

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">{{ __('messages.sl')}}</th>
                                    <th style="text-align: center;width: 25%">{{ __('messages.name')}}</th>
                                    <th style="text-align: center;width: 20%">{{ __('messages.address')}}</th>
                                    <th style="text-align: center;width: 10%">{{ __('messages.phone')}}</th>
                                    <th style="text-align: center;width: 10%">{{ __('messages.alt_phone')}}</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                @foreach($customers as $key => $value)
                                <tr>
                                    <td style="text-align: center">{{ $key + 1 }}</td>
                                    <td style="text-align: left">{{ $value['name'] }}</td>
                                    <td style="text-align: center;">{{ $value['address'] }}</td>
                                    <td style="text-align: center;">{{ $value['phone'] }}</td>
                                    <td style="text-align: center;">{{ $value['alternative_contact'] }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>