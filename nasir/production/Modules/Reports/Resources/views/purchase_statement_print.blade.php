<!DOCTYPE html>
<html>

<head>
    <title>{{ __('messages.purchase_statement')}}</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">{{ __('messages.purchase_statement')}}</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">{{ __('messages.from_date')}}</th>
                                    <th style="text-align: center">{{ __('messages.item_category')}}</th>
                                    <th style="text-align: center">{{ __('messages.purchase_type')}}</th>
                                    <th style="text-align: center">{{ __('messages.purchase_by')}}</th>
                                    <th style="text-align: center">{{ __('messages.purchase_id')}}</th>
                                    <th style="text-align: center">{{ __('messages.supplier_name')}}</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>{{ __('messages.to')}}</strong> {{ $to_date }}</td>
                                    <td style="text-align: center">
                                        @if($item_category_name != null)
                                            {{ $item_category_name['name'] }}
                                        @else
                                            {{ __('messages.all')}}
                                        @endif
                                    </td>

                                    <td style="text-align: center">
                                        @if($purchase_return == 0)
                                            {{ __('messages.all')}}
                                        @endif

                                        @if($purchase_return == 1)
                                            {{ __('messages.purchase')}}
                                        @endif

                                        @if($purchase_return == 2)
                                            {{ __('messages.purchase_return')}}
                                        @endif
                                    </td>

                                    <td style="text-align: center">
                                        @if($purchase_by_name != null)
                                            {{ $purchase_by_name['name'] }}
                                        @else
                                             {{ __('messages.all')}}
                                        @endif
                                    </td>

                                    <td style="text-align: center">
                                        @if($purchase_id != null)
                                            {{ 'BILL - ' . str_pad($purchase_id['bill_number'], 6, "0", STR_PAD_LEFT) }}
                                        @else
                                             {{ __('messages.all')}}
                                        @endif
                                    </td>

                                    <td style="text-align: center">
                                        @if($supplier_name != null)
                                            {{ $supplier_name['name'] }}
                                        @else
                                             {{ __('messages.all')}}
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%"> {{ __('messages.sl')}}</th>
                                    <th style="text-align: center;width: 6%"> {{ __('messages.T/date')}}</th>
                                    <th style="text-align: center;width: 7%"> {{ __('messages.purchase_id')}}</th>
                                    <th style="text-align: center;width: 6%"> {{ __('messages.item_code')}}</th>
                                    <th style="text-align: center;width: 13%"> {{ __('messages.name')}}</th>
                                    <th style="text-align: center;width: 4%"> {{ __('messages.u/m')}}</th>
                                    <th style="text-align: center;width: 4%"> {{ __('messages.qty')}}</th>
                                    <th style="text-align: center;width: 4%"> {{ __('messages.unit_price')}}</th>
                                    <th style="text-align: center;width: 6%"> {{ __('messages.T/price')}}</th>
                                    <th style="text-align: center;width: 4%"> {{ __('messages.discount')}}</th>
                                    <th style="text-align: center;width: 6%"> {{ __('messages.T/payable')}}</th>
                                    <th style="text-align: center;width: 4%"> {{ __('messages.vat')}}</th>
                                    <th style="text-align: center;width: 4%"> {{ __('messages.T/discount')}}</th>
                                    <th style="text-align: center;width: 6%"> {{ __('messages.N/payable')}}</th>
                                    <th style="text-align: center;width: 6%"> {{ __('messages.paid')}}</th>
                                    <th style="text-align: center;width: 5%"> {{ __('messages.due')}}</th>
                                    <th style="text-align: center;width: 10%"> {{ __('messages.supplier_name')}}</th>
                                    <th style="text-align: center;width: 2%"> {{ __('messages.type')}}</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i                  = 1;
                                    $total_t_price      = 0;
                                    $total_discount     = 0;
                                    $total_payable      = 0;
                                    $total_vat          = 0;
                                    $total_discount_t   = 0;
                                    $total_net_payable  = 0;
                                    $total_paid         = 0;
                                    $total_due          = 0;
                                ?>
                                @foreach($data as $key => $value)
                                <tr>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $i }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['bill_date'] }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['bill_number'] }} <br> {{ $value['bill_note'] }}</td>

                                    <td style="text-align: center;">{{ str_pad($value['bill_entries'][0]['product_code'], 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: center;">{{ $value['bill_entries'][0]['product_entry_name'] }}</td>
                                    <td style="text-align: center;">{{ $value['bill_entries'][0]['unit_name'] }}</td>
                                    <td style="text-align: center;">{{ $value['bill_entries'][0]['quantity'] }}</td>
                                    <td style="text-align: right;">{{ $value['bill_entries'][0]['rate'] }}</td>
                                    <td style="text-align: right;">{{ $value['bill_entries'][0]['rate'] * $value['bill_entries'][0]['quantity'] }}</td>
                                    <td style="text-align: right;">
                                        <?php
                                            if (isset($value['bill_entries'][0]['discount_type']))
                                            {
                                                if ($value['bill_entries'][0]['discount_type'] == 0)
                                                {
                                                    $discountAmount0   = ($value['bill_entries'][0]['rate'] * $value['bill_entries'][0]['quantity'] * $value['bill_entries'][0]['discount_amount'])/100;
                                                }
                                                else
                                                {
                                                    $discountAmount0   = $value['bill_entries'][0]['discount_amount'];
                                                }
                                            }
                                            else
                                            {
                                                $discountAmount0  = 0;
                                            }
                                        ?>

                                        {{ $discountAmount0 }}
                                    </td>
                                    <td style="text-align: right;">{{ ($value['bill_entries'][0]['rate'] * $value['bill_entries'][0]['quantity']) - $discountAmount0 }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['vat_type'] == 0 ? $value['vat_perc'] : $value['vat']  }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">
                                        @if(isset($value['total_discount_type']))
                                            {{ $value['total_discount_type'] == 0 ? $value['total_discount_perc'] : $value['total_discount']  }}
                                        @else
                                            0
                                        @endif
                                    </td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['net_payable'] }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['paid_amount'] }}</td>
                                    <td style="text-align: right;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['net_payable'] - $value['paid_amount'] }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['customer_name'] }}</td>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{ $value['row_span'] }}">{{ $value['type'] }}</td>
                                </tr>
                                 
                                <?php
                                    $sub_total_t_price      = 0;
                                    $sub_total_discount     = 0;
                                    $sub_total_payable      = 0;
                                ?>

                                @foreach($value['bill_entries'] as $key1 => $value1)

                                <?php
                                    if (isset($value1['discount_type']))
                                    {
                                        if ($value1['discount_type'] == 0)
                                        {
                                            $discountAmount   = ($value1['rate'] * $value1['quantity'] * $value1['discount_amount'])/100;
                                        }
                                        else
                                        {
                                            $discountAmount   = $value1['discount_amount'];
                                        }
                                    }
                                    else
                                    {
                                        $discountAmount   = 0;
                                    }

                                    $sub_total_t_price  = $sub_total_t_price + ($value1['rate'] * $value1['quantity']);
                                    $sub_total_discount = $sub_total_discount + $discountAmount;
                                    $sub_total_payable  = $sub_total_payable + (($value1['rate'] * $value1['quantity']) - $discountAmount);
                                ?>

                                @if($key1 != 0)
                                <tr>
                                    <td style="text-align: center;">{{ str_pad($value1['product_code'], 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: center;">{{ $value1['product_entry_name'] }}</td>
                                    <td style="text-align: center;">{{ $value1['unit_name'] }}</td>
                                    <td style="text-align: center;">{{ $value1['quantity'] }}</td>
                                    <td style="text-align: right;">{{ $value1['rate'] }}</td>
                                    <td style="text-align: right;">{{ $value1['rate'] * $value1['quantity'] }}</td>
                                    <td style="text-align: right;">
                                        <?php
                                            if ($value1['discount_type'] == 0)
                                            {
                                                $discountAmount   = ($value1['rate'] * $value1['quantity'] * $value1['discount_amount'])/100;
                                            }
                                            else
                                            {
                                                $discountAmount   = $value1['discount_amount'];
                                            }
                                        ?>

                                        {{ $discountAmount }}
                                    </td>
                                    <td style="text-align: right;">{{ ($value1['rate'] * $value1['quantity']) - $discountAmount }}</td>
                                </tr>

                                @endif
                                @endforeach

                                <?php
                                    $i++;

                                    $total_t_price      = $total_t_price + $sub_total_t_price;
                                    $total_discount     = $total_discount + $sub_total_discount;
                                    $total_payable      = $total_payable + $sub_total_payable;
                                    $total_vat          = $total_vat +  ($value['vat_type'] == 0 ? $value['vat_perc'] : $value['vat']);
                                    $total_discount_t   = $total_discount_t + ($value['total_discount_type'] == 0 ? $value['total_discount_perc'] : $value['total_discount']);
                                    $total_net_payable  = $total_net_payable + $value['net_payable'];
                                    $total_paid         = $total_paid + $value['paid_amount'];
                                    $total_due          = $total_due + ($value['net_payable'] - $value['paid_amount']);
                                ?>

                                @endforeach
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="8" style="text-align: right;"> {{ __('messages.total')}}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_t_price }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_discount }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_payable }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_vat }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_discount_t }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_net_payable }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_paid }}</th>
                                    <th colspan="1" style="text-align: right;">{{ $total_due }}</th>
                                    <th colspan="2" style="text-align: right;"></th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>