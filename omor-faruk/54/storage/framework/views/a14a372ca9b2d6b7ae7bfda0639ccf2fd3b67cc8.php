

<?php $__env->startSection('title', 'Loan Report'); ?>

<?php $__env->startSection('styles'); ?>
    <style type="text/css">
        @media  print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page  {
                margin: 0cm ! important;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Loan Report</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Loan Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['address']); ?></p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['contact_number']); ?></p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">Loan Statement</h4>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><strong>From</strong> <?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date))); ?> <strong>To</strong> <?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="<?php echo e(route('loan_report_index')); ?>" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div style="margin-bottom: 10px" class="col-lg-5 col-md-5 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="<?php echo e(isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('01-01-2021')); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="<?php echo e(isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date))); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        <div class="col-lg-5 col-md-5 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="category_id">
                                                    <option value="0" selected>--All Loan Registers--</option>
                                                    <?php if(!empty($customers) && ($customers->count() > 0)): ?>
                                                    <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option <?php echo e(isset($_GET['customer_id']) && ($_GET['customer_id'] == $value['id']) ? 'selected' : ''); ?> value="<?php echo e($value['id']); ?>"><?php echo e($value['name']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center">SL</th>
                                            <th style="text-align: center">Loan Register</th>
                                            <th style="text-align: center">Received</th>
                                            <th style="text-align: center">Paid</th>
                                            <th style="text-align: center">Balance</th>
                                            <th style="text-align: center" class="d-print-none">Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>

                                        <?php
                                            $i              = 1;  
                                            $total_loan     = 0;  
                                            $total_paid     = 0;  
                                            $total_balance  = 0;  
                                        ?>
                                        
                                        <?php if(!empty($data) && (count($data) > 0)): ?>
                                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td style="text-align: center;"><?php echo e($i); ?></td>
                                                <td style="text-align: left;"><?php echo e($value['customer_name']); ?> </td>
                                                <td style="text-align: right;"><?php echo e(number_format($value['income'],2,'.',',')); ?></td>
                                                <td style="text-align: right;"><?php echo e(number_format($value['expense'],2,'.',',')); ?></td>
                                                <td style="text-align: right;"><?php echo e(number_format($value['balance'],2,'.',',')); ?></td>
                                                <td style="text-align: center;" class="d-print-none">
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a style="cursor: pointer" class="dropdown-item" data-toggle="modal" data-target="#myModal_<?php echo e($value['customer_id']); ?>" >Details</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                            <?php 
                                                $i++;
                                                $total_loan     = $total_loan + $value['income'];  
                                                $total_paid     = $total_paid + $value['expense'];  
                                                $total_balance  = $total_balance + $value['balance']; 
                                            ?>

                                            <div id="myModal_<?php echo e($value['customer_id']); ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title mt-0" id="myModalLabel">Select Date Range</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <form method="get" action="<?php echo e(route('loan_report_details', $value['customer_id'])); ?>" enctype="multipart/form-data" target="_blank">

                                                        <div style="padding-top: 0px !important" class="modal-body">
                                                            <div style="margin-bottom: 0px !important" class="form-group row">
                                                                <label for="example-text-input" class="col-md-12 col-form-label">From Date *</label>
                                                                <div class="col-md-12">
                                                                    <input style="cursor: pointer" id="from_date" name="from_date" type="date" class="form-control" value="<?= date("2020-01-01") ?>" required>
                                                                </div>
                                                            </div>

                                                            <div style="margin-bottom: 0px !important" class="form-group row">
                                                                <label for="example-text-input" class="col-md-12 col-form-label">To Date *</label>
                                                                <div class="col-md-12">
                                                                    <input style="cursor: pointer" id="to_date" name="to_date" type="date" class="form-control" value="<?= date("Y-m-d") ?>" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Search</button>
                                                            <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>

                                    </tbody>
                                    <tr>
                                        <th style="text-align: right" colspan="2">Total</th>
                                        <th style="text-align: right"><?php echo e(number_format($total_loan,2,'.',',')); ?></th>
                                        <th style="text-align: right"><?php echo e(number_format($total_paid,2,'.',',')); ?></th>
                                        <th style="text-align: right"><?php echo e(number_format($total_balance,2,'.',',')); ?></th>
                                        <th style="text-align: right"></th>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/omor-faruk/54/Modules/Reports/Resources/views/loan_report.blade.php ENDPATH**/ ?>