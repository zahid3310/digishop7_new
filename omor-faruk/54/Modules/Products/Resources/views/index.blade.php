@extends('layouts.app')

@section('title', 'Products')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Create Product</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Products</a></li>
                                    <li class="breadcrumb-item active">Create Product</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('products_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Category *</label>
                                        <select style="width: 83%" id="product_category_id" name="product_category_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10" required>
                                           <option value="">--Select Category--</option>
                                        </select>

                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center;" class="form-control col-lg-2 col-md-2 col-sm-2 col-2" data-toggle="modal" data-target="#myModal">
                                            <i class="bx bx-plus font-size-24"></i>
                                        </span>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Product Code</label>
                                        <input type="text" name="code" class="inner form-control barCode" id="bar_code_0" value="{{ str_pad($code, 6, '0', STR_PAD_LEFT) }}" readonly />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Product Name *</label>
                                        <input type="text" name="product_name" class="inner form-control" id="product_name" placeholder="Product Name" required />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Brand </label>
                                        <select style="width: 83%" id="brand_id" name="brand_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10">
                                           <option value="">--Select Brand--</option>
                                        </select>

                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center;" class="form-control col-lg-2 col-md-2 col-sm-2 col-2" data-toggle="modal" data-target="#myModal1">
                                            <i class="bx bx-plus font-size-24"></i>
                                        </span>
                                    </div>


                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Unit</label>
                                        <select id="unit_id" style="width: 100%" class="form-control select2 unit" name="unit_id">
                                            <option value="">--Select Unit--</option>
                                            @if(!empty($units))
                                                @foreach($units as $key => $unit)
                                                <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-8 col-md-8 col-sm-8 col-8 col-form-label">Is IMEI ? </label>
                                        <div style="padding-right: 0px;padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12">
                                            <select style="cursor: pointer" id="warrenty_type" class="form-control" name="warrenty_type" onchange="enterEmi()">
                                                <option value="1">No</option>
                                                <option value="2">Yes</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-8 col-md-8 col-sm-8 col-8 col-form-label">Product Type</label>
                                        <div style="padding-right: 0px;padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12">
                                            <select style="cursor: pointer" id="product_type" class="form-control" name="product_type">
                                                <option value="1">Single</option>
                                                <option value="2">Variable</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Purchase Price *</label>
                                        <input type="text" name="buying_price" class="inner form-control" id="buying_price_0" placeholder="Purchase Price" required />
                                    </div>

                                    <div style="display: none" class="col-md-12 variationClass">
                                        <hr style="margin-top: 0px !important">

                                        <div data-repeater-list="inner-group" class="inner form-group mb-0 row">
                                            <div class="inner col-lg-12 ml-md-auto input_fields_wrap getMultipleRow">

                                                <h5>Add Variations</h5>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6 col-sm-6"></div>
                                            <div class="col-lg-1 col-md-2 col-sm-6 col-12 form-group">
                                                <div id="add_field_button" class="add_field_button">
                                                    <i class="btn btn-success btn-block bx bx-plus font-size-20"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <hr style="margin-top: 0px !important">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Sell Price *</label>
                                        <input type="text" name="selling_price" class="inner form-control" id="selling_price_0" placeholder="Sell Price" oninput="CalculateTotalSellPrice()" required />
                                    </div>


                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Stock Quantity</label>
                                        <input type="number" name="stock_quantity" class="inner form-control" id="stock_quantity" placeholder="Stock Quantity" oninput="enterEmi()" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Alert Quantity</label>
                                        <input type="number" name="alert_quantity" class="inner form-control" id="alert_quantity" placeholder="Alert Quantity" />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-8 col-md-8 col-sm-8 col-8 col-form-label">Status</label>
                                        <div style="padding-right: 0px;padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12">
                                            <select style="cursor: pointer" class="form-control" name="status">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div style="display: none" class="col-md-12 serialNumbers">
                                        <hr style="margin-top: 0px !important;margin-bottom: 10px !important">

                                        <h5>Add Serial Numbers</h5>

                                        <div id="sl_list" class="row">
                                            
                                        </div>

                                        <hr style="margin-top: 20px !important">
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('products_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Products</h4>

                                <br>

                                <table id="der" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">

                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Brand</th>
                                            <th>Purchase Price</th>
                                            <th>Sell Price</th>
                                            <th>Stock</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Product Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-bottom: 0px !important;padding-top: 0px !important" class="modal-body">

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Category Name *</label>
                        <div class="col-md-12">
                            <input id="category_name" name="category_name" type="text" class="form-control" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Brand</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-bottom: 0px !important;padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Brand Name *</label>
                        <div class="col-md-12">
                            <input id="brand_name" name="brand_name" type="text" class="form-control" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal2" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 style="color: black" class="modal-title mt-0" id="ProductName"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div class="row">
                        <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%">
                            <thead>
                                <tr>
                                    <th style="width: 10%;text-align: center">SL</th>
                                    <th style="width: 15%;text-align: center">Code</th>
                                    <th style="width: 30%;text-align: center">Serial Number</th>
                                    <th style="width: 15%;text-align: center">Stock In Hand</th>
                                </tr>
                            </thead>

                            <tbody id="warenty_product_list">
                            </tbody>
                        </table>
                    </div>
                
                    <div class="modal-footer">
                        <button id="CloseButton2" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $('#der').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: site_url + '/products/load-products-datatable',
                    type: 'GET',
                },
                columns: [
                    {data: 'sl'},
                    {data: 'product_code'},
                    {data: 'name'},
                    {data: 'category'},
                    {data: 'brand'},
                    {data: 'buy_price'},
                    {data: 'sell_price'},
                    {data: 'stock_in_hand'},
                    {data: 'status'},
                    {data: 'action'}
                ],
            });

            $("#product_category_id").select2({
                ajax: { 
                    url:  site_url + '/products/product-category-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#brand_id").select2({
                ajax: { 
                    url:  site_url + '/products/brand-brand-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#supplier_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0)
                    {
                        var type = 'Customer';
                    }

                    if (result['contact_type'] == 1)
                    {
                        var type = 'Supplier';
                    }

                    if (result['contact_type'] == 1)
                    {
                        return result['text'] + ' | ' + type;
                    }
                },
            });

            var tax_type  = $("#tax_type").val();

            if (tax_type == 2)
            {
                $("#serviceCharge").hide();
                $("#vatPercentage").hide();
            }
            else
            {
                $("#serviceCharge").show();
                $("#vatPercentage").show();
            }

            $('#add_field_button').click();
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var product_name  = $("#category_name").val();
            var site_url      = $('.site_url').val();

            if (product_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }

            $.ajax({
                type:   'get',
                url:    site_url + '/products/add-new-category/',
                data:   {  product_name : product_name, _token: '{{csrf_token()}}' },
    
                success: function (data) {
                    if(data != 0)
                    {
                        $('#CloseButton').click();
                    }
                    
                    $("#product_category_id").empty();
                    $('#product_category_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    $('#category_name').val('');
                }
            });
        }); 

        $('#submitBtn1').click(function() {
            
            var brand_name      = $("#brand_name").val();
            var site_url        = $('.site_url').val();

            if (brand_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }

            $.ajax({
                type:   'get',
                url:    site_url + '/products/add-new-brand/',
                data:   {  brand_name : brand_name, _token: '{{csrf_token()}}' },
    
                success: function (data) {
                    if(data != 0)
                    {
                        $('#CloseButton1').click();
                    }
                    
                    $("#brand_id").empty();
                    $('#brand_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    $('#brand_name').val('');
                }
            });
        });

        $('#submitBtn2').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = 1;
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton2').click();
                        }
                        
                        $("#supplier_id").empty();
                        $('#supplier_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });   
    </script>

    <script type="text/javascript">
        function sellingPriceTaxType()
        {
            var tax_type  = $("#tax_type").val();

            if (tax_type == 2)
            {
                $("#serviceCharge").hide();
                $("#vatPercentage").hide();
                $("#service_charge_0").val('');
                $("#vat_percentage_0").val('');

                CalculateTotalSellPrice();
            }
            else
            {
                $("#serviceCharge").show();
                $("#vatPercentage").show();
                $("#service_charge_0").val('');
                $("#vat_percentage_0").val('');

                CalculateTotalSellPrice();
            }
        }

        function CalculateTotalSellPrice()
        {
            var sell_price_call                 = $("#selling_price_0").val();
            var vat_percentage_call             = $("#vat_percentage_0").val();
            var service_charge_call             = $("#service_charge_0").val();

            if (sell_price_call == '')
            {
                var sell_price                  = 0;
            }
            else
            {
                var sell_price                  = $("#selling_price_0").val();
            }

            if (vat_percentage_call == '')
            {
                var vat_percentage          = 0;
            }
            else
            {
                var vat_percentage          = $("#vat_percentage_0").val();
            }

            if (service_charge_call == '')
            {
                var service_charge          = 0;
            }
            else
            {
                var service_charge          = $("#service_charge_0").val();
            }

            var vat_amount                  = (parseFloat(sell_price)*parseFloat(vat_percentage))/100;
            var service_charge_amount       = (parseFloat(sell_price)*parseFloat(service_charge))/100;
            var selling_price_exclusive_tax = parseFloat(sell_price) - parseFloat(vat_amount) - parseFloat(service_charge_amount);

            $("#selling_price_exclusive_tax_0").val(selling_price_exclusive_tax);
        }
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x       = 0;
        $(add_button).click(function(e)
        {   
            e.preventDefault();

            if(x < max_fields)
            {   
                x++;

                $('.getMultipleRow').append(' ' + '<div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_'+x+'">' +
                                                    '<div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">\n' +
                                                        '<select style="width: 100%" name="variation_id[]" class="inner form-control single_select2" id="variation_id_'+x+'" required onchange="variationValue('+x+')">\n' +
                                                            '<option>--Select Variations--</option>\n' +
                                                            '@if(!empty($variations) && ($variations->count() > 0))' +
                                                            '@foreach($variations as $key => $variation)' +
                                                                '<option value="{{ $variation['id'] }}">{{ $variation['name'] }}</option>\n' +
                                                            '@endforeach' +
                                                            '@endif' +
                                                        '</select>\n' +

                                                    '</div>\n' +

                                                    '<div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">\n' +
                                                        '<select style="width: 100%" name="variation_value[]" class="inner form-control single_select2" id="variation_value_'+x+'" required>\n' +
                                                            '<option>--Select Variation Value--</option>\n' +
                                                        '</select>\n' +

                                                    '</div>\n' +

                                                    '<div class="col-lg-1 col-md-2 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                        '<i class="btn btn-success btn-block bx bx-trash-alt font-size-20"></i>\n' + 
                                                    '</div>\n' + 
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;
        });
    </script>

    <script type="text/javascript">
        function variationValue(x)
        {
            var site_url        = $(".site_url").val();
            var variationId     = $("#variation_id_"+x).val();

            $.get(site_url + '/products/get-variation-value-list/'+ variationId, function(data){
                
                var list5 = '';
                var list7 = '';

                $.each(data, function(i, data_list)
                {   
                    list5 += '<option value = "' +  data_list.id + '">' + data_list.name + '</option>';
                });

                list7 += '<option value = "">' + '--Select Variation Value--' +'</option>';

                $("#variation_value_"+x).empty();
                $("#variation_value_"+x).append(list7);
                $("#variation_value_"+x).append(list5);

            });
        }

        $(document).on('change', '#product_type', function() {
            
            var productType = $('#product_type').val();

            if (productType == 2)
            {
                $(".variationClass").show();
            }

            if (productType == 1)
            {
                $(".variationClass").hide();
            }
        });
    </script>

    <script type="text/javascript">
        function enterEmi()
        {
            var warrentyType    = $("#warrenty_type").val();
            var stockQuantity   = $("#stock_quantity").val();
            var productCode     = $("#bar_code_0").val();
            
            if (warrentyType == 2)
            {   
                $(".serialNumbers").show();

                var sl_list = '';
                for(var i=0; i<stockQuantity; i++)
                {
                    var sl = parseFloat(i) + 1;

                    sl_list  += '<div class="col-md-2">\n' +
                                    '<label for="productname" class="col-form-label">' + 'Product Code' + '</label>\n' +
                                    '<input type="text" name="serial_code[]" class="inner form-control" id="serial_code_'+i+'" value="'+ pad(productCode, 6) +'" readonly />\n' + 
                                '</div>' +

                                '<div class="col-md-2">\n' +
                                    '<label for="productname" class="col-form-label">' + 'Serial Number' + '</label>\n' +
                                    '<input type="text" name="serial_number[]" class="inner form-control" id="serial_number" />\n' + 
                                '</div>\n';

                                productCode++;
                }

                $("#sl_list").empty();
                $("#sl_list").append(sl_list);
            }
            else
            {
                $(".serialNumbers").hide();
            }
        }

        function pad (str, max)
        {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>

    <script type="text/javascript">
        function getWarrentyProductList(x)
        {
            var productEntryId  = $("#view_list_id_"+x).val();

            var site_url  = $('.site_url').val();

            $.get(site_url + '/products/warrenty-products-list/' + productEntryId, function(data){

                var sl_product_list = '';
                var sl_no        = 1;
                $.each(data, function(i, sl_product_data)
                {   
                    if (sl_no == 1)
                    {
                        $("#ProductName").html(sl_product_data.product_name);
                    }

                    sl_product_list += '<tr>' +
                                        '<td style="text-align: center">' +
                                            sl_no +
                                        '</td>' +
                                        '<td style="text-align: center">' +
                                           pad(sl_product_data.product_code, 6) +
                                        '</td>' +
                                        '<td style="text-align: center">' +
                                            sl_product_data.serial_number +
                                        '</td>' +
                                        '<td style="text-align: center">' +
                                            sl_product_data.stock_in_hand + 
                                        '</td>' +
                                    '</tr>';

                                    sl_no++;
                });

                $("#warenty_product_list").empty();
                $("#warenty_product_list").append(sl_product_list);
            });
        }
    </script>
@endsection