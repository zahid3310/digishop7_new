<?php

namespace Modules\Products\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Units;
use App\Models\BillEntries;
use App\Models\Branches;
use App\Models\ProductVariations;
use App\Models\ProductVariationValues;
use App\Models\ProductVariationEntries;
use App\Models\WarrentyProducts;
use DB;
use Response;

class ProductsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $product                    = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.sell_price) as sell_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                 GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                 GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.warrenty_type) as warrenty_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                 GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                 GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                 GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                 GROUP_CONCAT(DISTINCT products.id) as product_id
                                                ')
                                    ->orderBy('product_entries.id', 'ASC')
                                    ->get();

        $data_find_entry_table      = ProductEntries::orderBy('id', 'DESC')->first();
        $data_find_warrenty_table   = WarrentyProducts::orderBy('id', 'DESC')->first();
        $entry_table_code           = $data_find_entry_table != null ? $data_find_entry_table['product_code'] + 1 : 1;     
        $warrenty_table_code        = $data_find_warrenty_table != null ? $data_find_warrenty_table['product_code'] + 1 : 1;     

        $code                       = $entry_table_code > $warrenty_table_code ? $entry_table_code : $warrenty_table_code;

        $product            = $product->sortBy('id')->all();
        $products           = collect($product);
        $units              = Units::orderBy('id', 'ASC')->get();
        $variations         = ProductVariations::orderBy('id', 'ASC')->get();

        return view('products::index', compact('products', 'code', 'units', 'variations'));
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_category_id'   => 'required',
            'buying_price'          => 'required',
            'selling_price'         => 'required',
            'product_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $data_find_entry_table      = ProductEntries::orderBy('id', 'DESC')->first();
            $data_find_warrenty_table   = WarrentyProducts::orderBy('id', 'DESC')->first();
            $entry_table_code           = $data_find_entry_table != null ? $data_find_entry_table['product_code'] + 1 : 1;     
            $warrenty_table_code        = $data_find_warrenty_table != null ? $data_find_warrenty_table['product_code'] + 1 : 1;     

            $code                       = $entry_table_code > $warrenty_table_code ? $entry_table_code : $warrenty_table_code;

            $product                    = new ProductEntries;
            $product->product_id        = $data['product_category_id'];
            $product->brand_id          = $data['brand_id'];
            $product->name              = $data['product_name'];
            $product->sell_price        = $data['selling_price'];
            $product->buy_price         = $data['buying_price'];

            if ($data['unit_id'] != null)
            {
                $product->unit_id       = $data['unit_id'];
            }

            if (($data['stock_quantity'] != null) && ($data['stock_quantity'] > 0))
            {
                $product->stock_in_hand   = $data['stock_quantity'];
                $product->opening_stock   = $data['stock_quantity'];
            }

            $product->status                        = $data['status'];
            $product->alert_quantity                = $data['alert_quantity'];
            $product->product_type                  = $data['product_type'];
            $product->warrenty_type                 = $data['warrenty_type'];

            if ($data['warrenty_type'] == 1)
            {
                $product->product_code      = $code;
            }

            $product->created_by                    = $user_id;

            if ($product->save())
            {   
                if ($data['product_type'] == 2)
                {
                    foreach ($data['variation_id'] as $key => $value)
                    {   
                        if (($value != null) && ($data['variation_value'] != null))
                        {
                            $variation_values[] = [
                                'product_entry_id'      => $product['id'],
                                'variation_id'          => $value,
                                'variation_value_id'    => $data['variation_value'][$key],
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];
                        }
                    }

                    DB::table('product_variation_entries')->insert($variation_values);
                }

                if (($data['warrenty_type'] == 2))
                {
                    $sl_code  = $code;
                    foreach ($data['serial_number'] as $key => $value)
                    {
                        $serial_entries[] = [
                            'product_id'        => $data['product_category_id'],
                            'product_entry_id'  => $product->id,
                            'product_code'      => $sl_code,
                            'serial_number'     => $value,
                            'stock_in_hand'     => 1,
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];

                        $sl_code++;
                    }

                    DB::table('warrenty_products')->insert($serial_entries);
                }

                DB::commit();
                return back()->with("success","Product Added Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $products           = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.sell_price) as sell_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                 GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                 GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.warrenty_type) as warrenty_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                 GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                 GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                 GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                 GROUP_CONCAT(DISTINCT products.id) as product_id
                                                ')
                                    ->orderBy('product_entries.id', 'ASC')
                                    ->get();

        $product_entries    = $products->find($id);

        $warrenty_products  = WarrentyProducts::where('product_entry_id', $id)->where('stock_in_hand', '>', 0)->get();

        $entry_count        = $product_entries->count();
        
        //Code For Finding Product Code Start
        $data_find_entry_table      = ProductEntries::orderBy('id', 'DESC')->first();
        $data_find_warrenty_table   = WarrentyProducts::orderBy('id', 'DESC')->first();
        $entry_table_code           = $data_find_entry_table != null ? $data_find_entry_table['product_code'] + 1 : 1;     
        $warrenty_table_code        = $data_find_warrenty_table != null ? $data_find_warrenty_table['product_code'] + 1 : 1;     

        $code                       = $entry_table_code > $warrenty_table_code ? $entry_table_code : $warrenty_table_code;
        //Code For Finding Product Code End

        $units              = Units::orderBy('id', 'ASC')->get();

        $variation_entries  = ProductVariationEntries::where('product_entry_id', $id)
                                                        ->orderBy('id', 'ASC')
                                                        ->get();

        $varia_entry_count  = $variation_entries->count();
        $variations         = ProductVariations::orderBy('id', 'ASC')->get();
        $variation_values   = ProductVariationValues::orderBy('id', 'ASC')->get();

        return view('products::edit', compact('products', 'product_entries', 'entry_count', 'code', 'units', 'variation_entries', 'varia_entry_count', 'variations', 'variation_values', 'warrenty_products'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_category_id'   => 'required',
            'buying_price'          => 'required',
            'selling_price'         => 'required',
            'product_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();
        
        try{
            $product                 = ProductEntries::find($id);
            $product->product_id     = $data['product_category_id'];
            $product->brand_id       = $data['brand_id'];
            // $product->supplier_id = $data['supplier_id'];
            $product->name           = $data['product_name'];
            $product->sell_price     = $data['selling_price'];
            $product->buy_price      = $data['buying_price'];

            if ($data['unit_id'] != null)
            {
            $product->unit_id    = $data['unit_id'];
            }

            $product->stock_in_hand  = $data['stock_quantity'];
            
            $product->status         = $data['status'];
            $product->alert_quantity = $data['alert_quantity'];
            $product->product_type   = $data['product_type'];
            $product->warrenty_type  = $data['warrenty_type'];
            $product->created_by     = $user_id;

            if ($product->save())
            {   
                if ($data['product_type'] == 2)
                {
                    $find_product_variation_entries = ProductVariationEntries::where('product_entry_id', $product['id'])->delete();
                    foreach ($data['variation_id'] as $key => $value)
                    {   
                        if (($value != null) && ($data['variation_value'] != null))
                        {
                            $variation_values[] = [
                                'product_entry_id'      => $product['id'],
                                'variation_id'          => $value,
                                'variation_value_id'    => $data['variation_value'][$key],
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];
                        }
                    }

                    DB::table('product_variation_entries')->insert($variation_values);
                }

                
                $data_find_entry_table      = ProductEntries::orderBy('id', 'DESC')->first();
                $data_find_warrenty_table   = WarrentyProducts::orderBy('id', 'DESC')->first();
                $entry_table_code           = $data_find_entry_table != null ? $data_find_entry_table['product_code'] + 1 : 1;     
                $warrenty_table_code        = $data_find_warrenty_table != null ? $data_find_warrenty_table['product_code'] + 1 : 1;     
                $code                       = $entry_table_code > $warrenty_table_code ? $entry_table_code : $warrenty_table_code;
                
                if (($data['warrenty_type'] == 2))
                {   
                    if (isset($data['warrenty_products_id']))
                    {   
                        $sl_code  = $code;
                        foreach ($data['warrenty_products_id'] as $key => $value)
                        {   
                            if($value != 0)
                            {
                                $update_warrenty                    = WarrentyProducts::find($value);
                                $update_warrenty->serial_number     = $data['serial_number'][$key];
                                $update_warrenty->save();
                            }
                            else
                            {
                                $serial_entries[] = [
                                    'product_id'        => $data['product_category_id'],
                                    'product_entry_id'  => $product->id,
                                    'product_code'      => $sl_code,
                                    'serial_number'     => null,
                                    'stock_in_hand'     => 1,
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                                ];
        
                                $sl_code++;
                            }
                        }
                        
                        if(isset($serial_entries))
                        {
                            DB::table('warrenty_products')->insert($serial_entries);
                        }
                    }
                }

                DB::commit();
                return redirect()->route('products_index')->with("success","Product Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function barcodePrint()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $bill_id            = isset($_GET['bill_id']) ? $_GET['bill_id'] : 0;

        $product_entries    = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                 GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                 GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product            = $product_entries->sortBy('name')->all();
        $product_entries    = collect($product);

        $bill_entries       = BillEntries::where('bill_entries.bill_id', $bill_id)
                                    ->select('bill_entries.*')
                                    ->get();

        $entry_count        = $bill_entries->count();

        $user_info          = userDetails();

        return view('products::barcodes', compact('product_entries', 'user_info', 'bill_entries', 'bill_id', 'entry_count'));
    }

    public function barcodePrintPrint(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_entry_id.*'  => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $product_entry_id   = $request->product_entry_id;
        $quantity           = $request->quantity;
        $print_type         = $request->type_id;
        $label_size         = $request->label_size;
        $margin             = 'margin-left: '.$request->margin.'px'.';margin-right: '.$request->margin.'px';
        $user_info          = userDetails();

        foreach ($request->product_entry_id as $key => $value)
        {
            $product_entries    = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->where('product_entries.id', $value)
                                    ->orderBy('product_entries.total_sold', 'DESC')
                                    ->select('product_entries.*', 'products.name as product_name')
                                    ->first();

            $data[$product_entries['id']]['product_name_show']      = isset($request->product_name) ? 1 : 0;
            $data[$product_entries['id']]['organization_name_show'] = isset($request->organization_name) ? 1 : 0;
            $data[$product_entries['id']]['sell_price_show']        = isset($request->product_price) ? 1 : 0;
            $data[$product_entries['id']]['name']                   = $product_entries['name'];
            $data[$product_entries['id']]['organization_name']      = $user_info['organization_name'];
            $data[$product_entries['id']]['product_code']           = $product_entries['product_code'];
            $data[$product_entries['id']]['sell_price']             = $product_entries['sell_price'];
            $data[$product_entries['id']]['quantity']               = $quantity[$key];
        }

        return view('products::barcodes_print', compact('product_entries', 'data', 'print_type', 'user_info', 'label_size', 'margin'));
    }

    public function barcodePrintAjax($product_id, $product_entry_id)
    {
        $tables             = TableNameByUsers();
        $table_id           = Auth::user()->associative_contact_id;

        $product_entries    = $tables['product_entries']
                                    ->where('product_entries_'.$table_id.'.product_id', $product_id)
                                    ->orderBy('product_entries_'.$table_id.'.total_sold', 'DESC')
                                    ->select('product_entries_'.$table_id.'.*')
                                    ->get();

        $product_entries->sortBy('name');


        return Response::json($product_entries);
    }

    public function productListAjax()
    {
        $user_id            = Auth::user()->id;
        $user_branch_id     = Auth::user()->branch_id;
        $user_role          = Auth::user()->role;

        $product_entries    = ProductEntries::where('product_entries.product_id', '!=', 1)->get();
        $product_entries->sortBy('name');

        return Response::json($product_entries);
    }

    public function producCategorytList()
    {
        $user_id            = Auth::user()->id;
        $user_branch_id     = Auth::user()->branch_id;
        $user_role          = Auth::user()->role;

        $data               = Products::orderBy('products.total_sold', 'DESC')
                                    ->where('products.id', '!=', 1)
                                    ->select('products.*')
                                    ->get();

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData      = Products::where('products.id', '!=', 1)
                                    ->orderBy('products.total_sold', 'DESC')
                                    ->take(50)
                                    ->get();
        }
        else
        { 
            $search         = $_GET['searchTerm'];   
            $fetchData      = Products::where('products.name', 'LIKE', "%$search%")
                                    ->where('products.id', '!=', 1)
                                    ->orderBy('products.total_sold', 'DESC')
                                    ->take(50)
                                    ->get();
        }

        $data = array();
        foreach ($fetchData as $key => $value)
        {
            $data[] = array("id"=>$value['id'], "text"=>$value['name']);
        }

        return Response::json($data);
    }

    public function productBrandList()
    {
        $user_id            = Auth::user()->id;
        $user_branch_id     = Auth::user()->branch_id;
        $user_role          = Auth::user()->role;

        $data               = Categories::orderBy('categories.created_at', 'DESC')
                                            ->select('categories.*')
                                            ->get();

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData      = Categories::orderBy('categories.created_at', 'DESC')
                                            ->take(50)
                                            ->get();
        }
        else
        { 
            $search         = $_GET['searchTerm'];   
            $fetchData      = Categories::where('categories.name', 'LIKE', "%$search%")
                                            ->orderBy('categories.created_at', 'DESC')
                                            ->take(50)
                                            ->get();
        }

        $data = array();
        foreach ($fetchData as $key => $value)
        {
            $data[] = array("id"=>$value['id'], "text"=>$value['name']);
        }

        return Response::json($data);
    }

    public function productStoreProduct(Request $request)
    {
        $user_id                = Auth::user()->id;
        $data                   = $request->all();

        $products               = new Products;
        $products->name         = $data['product_name'];
        $products->status       = 1;
        $products->created_by   = $user_id;

        if ($products->save())
        {   
            return Response::json($products);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function productStoreBrand(Request $request)
    {
        $user_id                = Auth::user()->id;
        $data                   = $request->all();

        $brand                  = new Categories;
        $brand->name            = $data['brand_name'];
        $brand->status          = 1;
        $brand->created_by      = $user_id;

        if ($brand->save())
        {   
            return Response::json($brand);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function openingStock()
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End

        $product_entries    = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->whereNull('product_entries.stock_in_hand')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                 GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                 GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                 GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                 GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product            = $product_entries->sortBy('name')->all();
        $product_entries    = collect($product);

        return view('products::opening_stock', compact('product_entries'));
    }

    public function storeOpeningStock(Request $request)
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End

        $rules = array(
            'product_entry_id'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            foreach ($data['stock_in_hand'] as $key => $value)
            {   
                if ($value != null)
                {
                    $product_entry                       = ProductEntries::find($data['product_entry_id'][$key]);
                    $product_entry->stock_in_hand        = $value;
                    $product_entry->opening_stock        = $value;
                    $product_entry->updated_by           = $user_id;
                    $product_entry->save();
                }
            }

            return back()->with("success","Stock Updated Successfully !!");

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function categoryIndex()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_role          = Auth::user()->role;
        $products           = Products::where('products.id', '!=', 1)->orderBy('products.id', 'DESC')->get();

        $units              = Units::orderBy('id', 'ASC')->get();

        return view('products::category_index', compact('products', 'units'));
    }

    public function categoryStore(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $product                    = new Products;
            $product->name              = $data['product_name'];
            $product->status            = 1;
            $product->created_by        = $user_id;
            
            if ($product->save())
            {
                return redirect()->route('products_category_index')->with("success","Product Category Created Successfully !!");
            }

        }catch (\Exception $exception){
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function categoryEdit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $products           = Products::where('products.id', '!=', 1)
                                        ->orderBy('products.id', 'DESC')
                                        ->get();

        $find_product       = Products::find($id);
        $units              = Units::orderBy('id', 'ASC')
                                        ->get();

        return view('products::category_edit', compact('products', 'find_product', 'units'));
    }

    public function categoryUpdate(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $product                    = Products::find($id);
            $product->name              = $data['product_name'];
            $product->updated_by        = $user_id;
            
            if ($product->save())
            {
                return redirect()->route('products_category_index')->with("success","Product Category Updated Successfully !!");
            }

        }catch (\Exception $exception){
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function unitsIndex()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $units              = Units::select('units.*')
                                    ->orderBy('units.id', 'DESC')
                                    ->get();

        return view('products::units_index', compact('units'));
    }

    public function unitsstore(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $table                    = new Units;
            $table->name              = $data['name'];
            $table->created_by        = $user_id;
            
            if ($table->save())
            {
                return redirect()->route('products_units_index')->with("success","Unit Created Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function unitsEdit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $units              = Units::select('units.*')
                                    ->orderBy('units.id', 'DESC')
                                    ->get();

        $find_unit          = Units::find($id);

        return view('products::units_edit', compact('find_unit', 'units'));
    }

    public function unitsUpdate(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $product                    = Units::find($id);
            $product->name              = $data['name'];
            $product->updated_by        = $user_id;
            
            if ($product->save())
            {
                return redirect()->route('products_units_index')->with("success","Unit Updated Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function variationValuesListLoad($id)
    {
        $data    = ProductVariationValues::where('product_variation_id', $id)->get();

        return Response::json($data);
    }

    //Product Variations

    public function variationsIndex()
    {
        $variations     = ProductVariations::leftjoin('product_variation_values', 'product_variation_values.product_variation_id', 'product_variations.id')
                                    ->groupBy('product_variations.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_variations.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_variations.name) as name,
                                                 GROUP_CONCAT(product_variation_values.name SEPARATOR ", ") as product_variation_values
                                                ')
                                    ->orderBy('product_variations.id', 'DESC')
                                    ->get();

        return view('products::variation_index', compact('variations'));
    }

    public function variationsstore(Request $request)
    {
        $rules = array(
            'name'              => 'required',
            'value_name.*'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $product_variation               = new ProductVariations;
            $product_variation->name         = $data['name'];
            $product_variation->created_by   = $user_id;
            
            if ($product_variation->save())
            {   
                foreach ($data['value_name'] as $key => $value)
                {
                    $variation_values[] = [
                        'product_variation_id'      => $product_variation['id'],
                        'name'                      => $value,
                        'created_by'                => $user_id,
                        'created_at'                => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('product_variation_values')->insert($variation_values);

                DB::commit();
                return redirect()->route('products_variations_index')->with("success","Variation Created Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function variationsEdit($id)
    {
        $variations                 = ProductVariations::leftjoin('product_variation_values', 'product_variation_values.product_variation_id', 'product_variations.id')
                                                        ->groupBy('product_variations.id')
                                                        ->selectRaw('GROUP_CONCAT(DISTINCT product_variations.id) as id,
                                                                     GROUP_CONCAT(DISTINCT product_variations.name) as name,
                                                                     GROUP_CONCAT(product_variation_values.name SEPARATOR ", ") as product_variation_values
                                                                    ')
                                                        ->orderBy('product_variations.id', 'DESC')
                                                        ->get();

        $find_variation             = ProductVariations::find($id);
        $variation_values           = ProductVariationValues::where('product_variation_id', $id)->get();
        $variation_values_count     = $variation_values->count();

        return view('products::variation_edit', compact('variations', 'find_variation', 'variation_values', 'variation_values_count'));
    }

    public function variationsUpdate(Request $request, $id)
    {
        $rules = array(
            'name'              => 'required',
            'value_name.*'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $product_variation               = ProductVariations::find($id);
            $product_variation->name         = $data['name'];
            $product_variation->updated_by   = $user_id;
            
            if ($product_variation->save())
            {   
                foreach ($data['value_name'] as $key => $value)
                {   
                    if ($data['type'][$key] == 0)
                    {
                        $find_old_data                = ProductVariationValues::find($data['value_ids'][$key]);
                        $find_old_data->name          = $value;
                        $find_old_data->updated_by    = $user_id;
                        $find_old_data->save();
                    }

                    if ($data['type'][$key] == 1)
                    {
                        $variation_values[] = [
                            'product_variation_id'      => $product_variation['id'],
                            'name'                      => $value,
                            'created_by'                => $user_id,
                            'created_at'                => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                if (isset($variation_values))
                {
                    DB::table('product_variation_values')->insert($variation_values);
                }

                DB::commit();
                return redirect()->route('products_variations_index')->with("success","Variation Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function productWarrentyList($id)
    {
        $data    = WarrentyProducts::leftjoin('product_entries', 'product_entries.id', 'warrenty_products.product_entry_id')
                                ->where('warrenty_products.product_entry_id', $id)
                                ->where('warrenty_products.stock_in_hand', '>', 0)
                                ->selectRaw('warrenty_products.*, product_entries.name as product_name')
                                ->get();

        return Response::json($data);
    }

    public function loadProductDatatable()
    {   
        ## Read value
        $draw            = $_GET['draw'];
        $row             = $_GET['start'];
        $rowperpage      = $_GET['length']; // Rows display per page
        $columnIndex     = $_GET['order'][0]['column']; // Column index
        $columnName      = $_GET['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_GET['order'][0]['dir']; // asc or desc
        $searchValue     = $_GET['search']['value']; // Search value

        ## Total number of records without filtering
        $totalRecords       = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->count();
    

        ## Total number of record with filtering
        $frecords = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->where('product_entries.name', 'LIKE', "%$searchValue%")
                                    ->orWhere('product_entries.product_code', 'LIKE', "%$searchValue%")
                                    ->orWhere('categories.name', 'LIKE', "%$searchValue%")
                                    ->orWhere('products.name', 'LIKE', "%$searchValue%")
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name
                                                ')
                                    ->orderBy('product_entries.id', 'ASC')
                                    ->get();

        $totalRecordwithFilter = $frecords->count();

        ## Fetch records
        $empQuery   = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->where('product_entries.name', 'LIKE', "%$searchValue%")
                                    ->orWhere('product_entries.product_code', 'LIKE', "%$searchValue%")
                                    ->orWhere('categories.name', 'LIKE', "%$searchValue%")
                                    ->orWhere('products.name', 'LIKE', "%$searchValue%")
                                    ->groupBy('product_entries.id')
                                    ->offset($row)
                                    ->limit($rowperpage)
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.sell_price) as sell_price,
                                                 GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_entries.warrenty_type) as warrenty_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                 GROUP_CONCAT(DISTINCT units.name) as unit_name,    
                                                 GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                 GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                 GROUP_CONCAT(DISTINCT products.id) as product_id
                                                ')
                                    ->orderBy('product_entries.id', 'ASC')
                                    ->get();

        $data = array();
        $i    = 0;
        foreach ($empQuery as $key => $value)
        {   
            $i++;

            if ($value['variations'] != null)
            {
                $product_name   = $value['name'].$value['variations'];
            }
            else
            {
                $product_name   = $value['name'];
            }

            if ($value['brand_name'] != null)
            {
                $brand_name   = $value['brand_name'];
            }
            else
            {
                $brand_name   = '';
            }

            if ($value['status'] == 1)
            {
                $status   = 'Active';
            }
            else
            {
                $status   = 'Inactive';
            }

            if ($value["warrenty_type"] == 2)
            {
                $cond   = '<a style="cursor: pointer" class="dropdown-item" data-toggle="modal" data-target="#myModal2" onclick="getWarrentyProductList('.$key.')">View List</a>
                                <input type="hidden" id="view_list_id_'.$key.'" value="'.$value["id"].'">';
            }
            else
            {
                $cond   = '';
            }

            $action  = '<div class="dropdown">
                            <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                <i class="mdi mdi-dots-horizontal font-size-18"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" style="">
                                '.$cond.'
                                <a class="dropdown-item" href="'.route("products_edit", $value["id"]).'">Edit</a>
                            </div>
                        </div>';

            $data[] = array( 
              "sl"            =>$i,
              "product_code"  =>str_pad($value['product_code'], 6, "0", STR_PAD_LEFT),
              "name"          =>$product_name,
              "category"      =>$value['category_name'],
              "brand"         =>$brand_name,
              "buy_price"     =>$value['buy_price'],
              "sell_price"    =>$value['sell_price'],
              "stock_in_hand" =>$value['stock_in_hand'],
              "status"        =>$status,
              "action"        =>$action,
           );
        }

        ## Response
        $response = array(
          "draw"                 => intval($draw),
          "iTotalRecords"        => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData"               => $data
        );

        return Response::json($response);
    }
}
