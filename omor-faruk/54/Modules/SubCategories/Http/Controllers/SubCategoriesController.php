<?php

namespace Modules\SubCategories\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;

class SubCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tables         = TableNameByUsers();
        $table_id       = Auth::user()->associative_contact_id;

        $categories     = $tables['categories']->where('status', 1)->orderBy('created_at', 'DESC')->get();
        $sub_categories = $tables['sub_categories']
                                ->join('categories_'.$table_id.'', 'categories_'.$table_id.'.id', 'sub_categories_'.$table_id.'.category_id')
                                ->orderBy('categories_'.$table_id.'.created_at', 'DESC')
                                ->select('sub_categories_'.$table_id.'.*', 'categories_'.$table_id.'.name as category_name')
                                ->get();

        return view('subcategories::index', compact('categories', 'sub_categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $tables                     = TableNameByUsers();
            $sub_category               = $tables['sub_categories'];
            $sub_category->name         = $data['name'];
            $sub_category->category_id  = $data['category_id'];
            $sub_category->status       = $data['status'];
            $sub_category->created_by   = $user_id;

            if ($sub_category->save())
            {   
                return back()->with("success","Sub Category Created Successfully !!");
            }else
            {
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){

            return back()->with("unsuccess","Not Added");
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $tables             = TableNameByUsers();
        $table_id           = Auth::user()->associative_contact_id;

        $categories         = $tables['categories']->where('status', 1)->orderBy('created_at', 'DESC')->get();
        $sub_categories     = $tables['sub_categories']
                                    ->join('categories_'.$table_id.'', 'categories_'.$table_id.'.id', 'sub_categories_'.$table_id.'.category_id')
                                    ->select('sub_categories_'.$table_id.'.*', 'categories_'.$table_id.'.name as category_name')
                                    ->orderBy('categories_'.$table_id.'.created_at', 'DESC')
                                    ->get();

        $find_sub_category  = $tables['sub_categories']
                                    ->join('categories_'.$table_id.'', 'categories_'.$table_id.'.id', 'sub_categories_'.$table_id.'.category_id')
                                    ->select('sub_categories_'.$table_id.'.*', 'categories_'.$table_id.'.name as category_name')
                                    ->find($id);

        return view('subcategories::edit', compact('categories', 'sub_categories', 'find_sub_category'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            $tables                     = TableNameByUsers();
            $sub_category               = $tables['sub_categories']->find($id);
            $sub_category->name         = $data['name'];
            $sub_category->category_id  = $data['category_id'];
            $sub_category->status       = $data['status'];
            $sub_category->updated_by   = $user_id;

            if ($sub_category->save())
            {   
                return redirect()->route('sub_categories_index')->with("success","Sub Category Updated Successfully !!");
            }else
            {
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){

            return back()->with("unsuccess","Not Added");
        }
    }
}
