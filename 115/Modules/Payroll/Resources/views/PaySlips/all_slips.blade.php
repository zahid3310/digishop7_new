@extends('layouts.app')

@section('title', 'List of Pay Slips')

@push('scripts')
<style type="text/css">
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>
@endpush

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">List of Pay Slips</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Payroll</a></li>
                                    <li class="breadcrumb-item active">List of Pay Slips</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {!! Session::get('success') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('unsuccess'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! Session::get('unsuccess') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('errors'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! 'Some required fields are missing..!! Please try again..' !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        <hr style="margin-top: 0px">
                            
                        <form id="FormSubmit" action="{{ route('pay_slip_list') }}" method="GET">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                    <label style="text-align: right" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Search </label>
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                    <select id="employee_id" style="width: 100" class="form-control select2" name="employee_id">
                                        <option value="{{ $customer_name != null ? $customer_name['name'] : '' }}">{{ $customer_name != null ? $customer_name['name'] : '--All--' }}</option>
                                    </select>
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                                </div>

                                <div class="col-lg-1 col-md-1 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                </div>
                            </div>
                        </form>

                        <hr style="margin-top: 0px">

                        <div class="card">
                            <div class="card-body table-responsive">
                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>SALARY FOR THE MONTH</th>
                                            <th>Employee Name</th>
                                            <th>Date</th>
                                            <th style="text-align: center">Paid</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($pay_slips) && ($pay_slips->count() > 0))
                                        @foreach($pay_slips as $key => $pay_slip)
                                            <?php
                                                if ($pay_slip->monthlySalarySheets->month == 1)
                                                {
                                                    $month  = 'January';
                                                }
                                                elseif ($pay_slip->monthlySalarySheets->month == 2)
                                                {
                                                    $month  = 'February';
                                                }
                                                elseif ($pay_slip->monthlySalarySheets->month == 3)
                                                {
                                                    $month  = 'March';
                                                }
                                                elseif ($pay_slip->monthlySalarySheets->month == 4)
                                                {
                                                    $month  = 'April';
                                                }
                                                elseif ($pay_slip->monthlySalarySheets->month == 5)
                                                {
                                                    $month  = 'May';
                                                }
                                                elseif ($pay_slip->monthlySalarySheets->month == 6)
                                                {
                                                    $month  = 'Jun';
                                                }
                                                elseif ($pay_slip->monthlySalarySheets->month == 7)
                                                {
                                                    $month  = 'July';
                                                }
                                                elseif ($pay_slip->monthlySalarySheets->month == 8)
                                                {
                                                    $month  = 'August';
                                                }
                                                elseif ($pay_slip->monthlySalarySheets->month == 9)
                                                {
                                                    $month  = 'September';
                                                }
                                                elseif ($pay_slip->monthlySalarySheets->month == 10)
                                                {
                                                    $month  = 'October';
                                                }
                                                elseif ($pay_slip->monthlySalarySheets->month == 11)
                                                {
                                                    $month  = 'November';
                                                }
                                                elseif ($pay_slip->monthlySalarySheets->month == 12)
                                                {
                                                    $month  = 'December';
                                                }
                                            ?>

                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $month . ', ' . $pay_slip->monthlySalarySheets->year }}</td>
                                                <td>{{ $pay_slip->employee->name }}</td>
                                                <td style="text-align: left">{{ date('d-m-Y', strtotime($pay_slip->date)) }}</td>
                                                <td style="text-align: right">{{ number_format($pay_slip->amount,2,'.',',') }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a style="cursor: pointer" href="{{ route('pay_slip_show', $pay_slip->id) }}" class="dropdown-item" target="_blank">Print</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {

        var site_url        = $('.site_url').val();

        $("#employee_id").select2({
            ajax: { 
            url:  site_url + '/invoices/customer/list/invoices',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['contact_type'] == 2 || result['id'] == 0)
                {
                    return result['text'];
                }
            },
        });
    });
</script>
@endsection