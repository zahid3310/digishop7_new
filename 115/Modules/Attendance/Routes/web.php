<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('attendance/import-user')->group(function() {
    Route::get('/', 'AttendanceController@index')->name('attendance_import_user_index');
    Route::get('/import-user', 'AttendanceController@importUser')->name('attendance_import_user');
    Route::post('/import-user-post', 'AttendanceController@importUserStore')->name('attendance_import_user_store');
    Route::post('/update-user-post', 'AttendanceController@importUserUpdate')->name('attendance_import_user_update');
    Route::get('/delete-user/{id}', 'AttendanceController@deleteUser')->name('attendance_delete_user');
	Route::get('/pull-data', 'AttendanceController@pullData')->name('attendance_pull_data');
});

Route::prefix('attendance/devices')->group(function() {
    Route::get('/', 'AttendanceController@deviceList')->name('attendance_devices_index');
    Route::post('/store', 'AttendanceController@deviceStore')->name('attendance_devices_store');
});

Route::prefix('attendance/manual-attendance')->group(function() {
    Route::get('/', 'AttendanceController@manualAttendanceIndex')->name('attendance_manual_attendance_index');
    Route::post('/store', 'AttendanceController@manualAttendanceStore')->name('attendance_manual_attendance_store');
});
