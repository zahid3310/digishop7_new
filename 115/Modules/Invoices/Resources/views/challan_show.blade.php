@extends('layouts.app')

@section('title', 'Print Cash Memo')

<style type="text/css">

.jol {
    position: absolute;
    left: 28%;
    height: 270px;
    top: 110px;
    opacity: 0.30;
    display: none;
}

address {
    margin-bottom: 0!important;
}
/*u {
    text-decoration-style: dotted!important;
    text-underline-offset: 1px;
}*/

.underline {
  text-decoration: none;
  background-image: linear-gradient(to right, #000 75%, transparent 75%);
  background-position: 0 1.04em;
  background-repeat: repeat-x;
  background-size: 8px 3px;
}

.table td, .table th {
    padding: 1.00rem!important;
}

.table-bordered thead td, .table-bordered thead th {
    border-bottom-width: 1px!important;
}

.table thead th {
    vertical-align: bottom;
    border-bottom: 1px solid #000!important;
}

.table-bordered td, .table-bordered th {
    border: 1px solid #000!important;
}


 @media print {
     
        address {
            margin-bottom: 0!important;
        }

        .table td, .table th {
            padding: 1.00rem!important;
 
        }
        
        .table-bordered tbody td, .table-bordered thead th {
            border-bottom-width: 1px!important;
        }
        
        .table thead th {
            vertical-align: bottom;
            border: 2px solid #000!important;
            /* background-color: #000 !important; */
        }
        
        .table-bordered tbody td, .table-bordered th {
            border: 1px solid #000!important;
        } 
    }

</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Cash Memo</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Cash Memo</a></li>
                                    <li class="breadcrumb-item active">Print</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div style="width: 100%;text-align:center;">
                                <span style="font-size: 25px;">বিসমিল্লাহির রাহমানির রাহীম </span><br> 
                                <span style="font-size: 15px;background-color: rgb(34, 34, 34);color: white;padding: 3px 14px;border-radius: 30%;">চালান </span>
                            </div>
                            <div class="row card-body" style="width: 100%;padding-bottom: 0px;">
                                <div style="width: 30%">
                                    <img style="height: 80px;text-align: -webkit-center;margin-left:50px;padding-left: 40px;" class="com_logo_img" src="{{ url('public/'.userDetails()->logo) }}"alt="qr sample"/><br>

                                    <span style="text-align: -webkit-center;margin-left: 75px;padding-left: 40px;"> ডিলার </span><br>
                                    <div style="border-top:1px solid;margin-left: 80px;margin-right: 122px;"></div>
                                    <div style="display: flex;margin-top:10px;">
                                        <img src="{{ url('public/images/cash_memo/sunpower.png') }}" alt="" style="height: 40px;width: 40px;padding-top: 5px;">
                                        <span style="padding-top: 10px;padding-left: 10px;">সান পাওয়ার সিরামিক কো লিঃ </span>
                                    </div>
                                    <div style="display: flex;">
                                        <img src="{{ url('public/images/cash_memo/akij.png') }}" alt="" style="height: 40px;width: 40px;padding-top: 5px;">
                                        <span style="padding-top: 10px;padding-left: 10px;">আকিজ সিরামিক্স লিঃ</span>
                                    </div>
                                    <div style="display: flex;">
                                        <img src="{{ url('public/images/cash_memo/mir.png') }}" alt="" style="height: 40px;width: 40px;padding-top: 5px;">
                                        <span style="padding-top: 10px;padding-left: 10px;">মীর সিরামিক্স লিঃ</span>
                                    </div>
                                    <div style="display: flex;">
                                        <img src="{{ url('public/images/cash_memo/kongfu.png') }}" alt="" style="height: 40px;width: 40px;padding-top: 5px;">
                                        <span style="padding-top: 6px;padding-left: 10px;">কুংফু সিরামিক্স </span>
                                    </div>
                                </div>
                                <div style="width: 70%">
                                    <div style="    text-align: inherit;font-size: 45px;">
                                        <span>{{ userDetails()->organization_name }}</span>
                                    </div>
                                    <div>
                                        <span style="padding: 5px 0px;font-size: 16px;background-color: black;color: white;padding: 3px 15px;border-radius: 35%;">এখানে দেশী-বিদেশী টাইলস পাইকারী ও খুচরা বিক্রয় করা হয়। </span>
                                    </div>
                                    <div style="width:100%;display: flex;padding-top:5px;">
                                        <div style="width:16%;"><span>মোঃ রুহুল আমিন</span></div>
                                        <div style="width:5%;"><img src="{{ url('public/images/cash_memo/bkash.png') }}" alt="" style="height: 30px;width: 30px;"></div>
                                        <div style="width:40%;"><span>&nbsp; 01713-861951,&nbsp; 01811-526689</span></div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div>
                                <p style="text-align: center;background-color: black;color: white;font-size: 17px;padding: 4px 0px;">মরহুম মোবারক চেয়ারম্যান সুপার মার্কেট, নবীনগর রোড, কোম্পানীগঞ্জ, কুমিল্লা।</p>
                            </div>
                            <div class="card-body row" style="display: flex; width:100%;padding-top: 0px;">
                                <div style="width: 20%;text-align: start;">
                                    <span style="font-size: 16px;padding-left: 20px;">চালান নং-&nbsp; {{ $challan['challan_number'] }} </span>
                                </div>
                                <div style="width: 64%;text-align:center;visibility:hidden;">
                                    <span style="font-size: 16px;"></span>
                                    
                                </div>
                                <div style="width: 16%;text-align: right;">
                                    <span style="font-size: 16px;">তারিখঃ &nbsp; {{ date('d-m-Y', strtotime($challan['date'])) }}</span>
                                </div>
                            </div>

                            <center>
                                <div style="margin: 0px 0px;">
                                <div class="card-body row" style="padding-top: 0px;padding-bottom: 5px;">
                                    <div style="display: flex;width:100%;">
                                        <span style="width: 9%;padding-left: 38px;font-size: 16px;background-color: black;color: white;padding-bottom: 2px;padding-top: 2px;">নামঃ &nbsp;</span>
                                        <span style="width: 91%;border: 1px solid;margin-left: 10px;font-size: 16px;font-weight:600;text-align: left;padding-left: 5px;"><strong>{{ $challan->invoice->customer_name != null ? $challan->invoice->customer_name : $challan->invoice->customer->name }}</strong></span>
                                    </div>
                                </div>
    
                                <div class="card-body row" style="padding-top: 0px;">
                                    <div style="display: flex;width:100%;">
                                        <span style="width: 9%;padding-left: 18px;font-size: 16px;background-color: black;color: white;padding-bottom: 2px;padding-top: 2px;">ঠিকানাঃ &nbsp;</span>
                                        <span style="width: 91%;border: 1px solid;margin-left: 10px;;font-size: 16px;font-weight:600;text-align: left;padding-left: 5px;"><strong>{{ $challan->invoice->customer_address != null ? $challan->invoice->customer_address : $challan->invoice->customer->address }}</strong></span>
                                    </div>
                                </div>
                            </div>
                            </center>

                                <div class="row" style="padding-top:20px;">

                                    <div class="col-md-12">

                                    <center>
                                        <div class="table-responsive">
                                     <table class="table table-bordered" style="line-height:0px;width: 990px;margin: 0px 20px;">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th class="text-center">SL</th>
                                                <th class="text-center">Description</th>
                                                <th class="text-center">SFT</th>
                                                <th class="text-center">Cartoon</th>
                                                <th class="text-center">PCS</th>
                                            </tr>

                                            @if($entries->count() > 0)

                    <?php
                        $total_stf   = 0;
                        $total_cart  = 0;
                        $total_pcs   = 0;
                    ?>

                    @foreach($entries as $key => $value)
                    <?php
                        if ($value->productEntries->type == 1)
                        {
                            $total_stf   = $total_stf + $value['quantity'];
                            $total_cart  = $total_cart + $value['cartoon'];
                            $total_pcs   = $total_pcs + $value['pcs'];
                        }
                        
                        $variation_name = ProductVariationName($value['product_entry_id']);

                        if ($value->productEntries->product_code != null)
                        {
                            $productCode  = ' - '.$value->productEntries->product_code;
                        }
                        else
                        {
                            $productCode  = '';
                        }

                        if ($value->productEntries->brand_id != null)
                        {
                            $brandName  = ' - '.$value->productEntries->brand->name;
                        }
                        else
                        {
                            $brandName  = '';
                        }

                        if ($value->productEntries->size_id != null)
                        {
                            $dimension  = ' - '.$value->productEntries->size->height . ' X ' . $value->productEntries->size->width;
                        }
                        else
                        {
                            $dimension  = '';
                        }

                        if ($value->productEntries->unit_id != null)
                        {
                            $unit  = ' '.$value->productEntries->unit->name;
                        }
                        else
                        {
                            $unit  = '';
                        }
                    ?>

                    <tr>
                        <td style="text-align: center">{{ $key + 1 }}</td>
                        <td>{{ $value->productEntries->name }} {{ $value->product->name . $productCode . $brandName . $dimension }} {{ $variation_name != null ? ' - ' . $variation_name : '' }}</td>

                        @if($value->productEntries->type == 1)
                        <td style="text-align: center">{{ $value['quantity'] }}</td>
                        <td style="text-align: center">{{ $value['cartoon'] }}</td>
                        <td style="text-align: center">{{ $value['pcs'] }}</td>
                        @else
                        <td style="text-align: center"></td>
                        <td style="text-align: center"></td>
                        <td style="text-align: center">{{ $value['quantity'] . $unit }}</td>
                        @endif
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <th style="text-align: right" colspan="2">Total </th>
                        <th style="text-align: center">{{ $total_stf }}</th>
                        <th style="text-align: center">{{ $total_cart }}</th>
                        <th style="text-align: center">{{ $total_pcs }}</th>
                    </tr>

                                        </thead>


                                 
                                    </table>
                                    </div>
                                    </center>
                                    <br>

                                    <div style="display: flex;dwidth:100%;padding-top: 50px;">
                                        <div style="width: 30%;padding-left:20px;">
                                            <span>........................................</span><br>
                                            <span style="font-size:16px;text-align:center;padding-left:5px;">ক্রেতার স্বাক্ষর</span>
                                        </div>
                                        <div style="width: 45%;text-align:center;">
                                            <div style="margin-bottom: 10px;"><span style="background-color: black;color:white; padding:3px 3px">বিঃ দ্রঃ বিক্রিত মাল সরবরাহের তারিখ হতে ৩০ দিনের মধ্যে পরিবর্তন যোগ্য। </span></div>
                                            <span style="background-color: black;color:white;padding:3px 3px;border-radius:50%;">শুক্রবার বন্ধ </span>
                                        </div>
                                        <div style="width: 25%;padding-right:20px;text-align:right;">
                                            <span>.........................................</span><br>
                                            <span style="font-size:16px;text-align:center;padding-right:5px;">বিক্রেতার স্বাক্ষর</span>
                                        </div>
                                    </div>
                            
                        </div>
                    </div> <!-- end col --><br><br>
                
            </div>
        </div>
    </div>
@endsection
